<?php
	extract($_REQUEST);
	include('includes/common.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cost Sheets</title>
        <link href="css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="resources/jquery-1.6.2.js"></script>
        <script>
            var ids = "<?php echo $unit_name; ?>";
            if(ids != "")
            { 
                var idss=ids.split(','); 
                                 
                for(var i=0; i<idss.length; i++)
                {
                    var func = 'js_set_value('+idss[i]+')';
                    window.setTimeout(func, 500);  
                }
            }
        
            //var selected_id = new Array, selected_name = new Array(),selected_job = new Array();
            var selected_id = new Array();
            var selected_name = new Array(); 
            var selected_job = new Array();
            
            // check_all_data    
            function check_all_data() {
                var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
                tbl_row_count = tbl_row_count - 1;
                for( var i = 1; i <= tbl_row_count; i++ ) {
                    js_set_value( i );
                }
            }
                
            function toggle( x, origColor ) {
                var newColor = 'yellow';
                if ( x.style ) {
                    x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
                }
            }
                
            function js_set_value( str ) 
            {
                toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
                
                if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) 
                {
                    selected_id.push( $('#txt_individual_id' + str).val() );
                    selected_name.push( $('#txt_individual' + str).val() );
                    //selected_job.push( $('#txt_individual_job' + str).val() );
                }
                else 
                {
                    for( var i = 0; i < selected_id.length; i++ ) 
                    {
                        if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
                    }
                    selected_id.splice( i, 1 );
                    selected_name.splice( i, 1 );
                    //selected_job.splice( i,1);
                }
                var id =''; 
                var name = '';
                for( var i = 0; i < selected_id.length; i++ ) 
                {
                    id += selected_id[i] + ',';
                    name += selected_name[i] + '*';
                    //job += selected_job[i] + '*';
                }
                id = id.substr( 0, id.length - 1 );
                name = name.substr( 0, name.length - 1 );
                //job = job.substr( 0, job.length - 1 );
                
                $('#txt_selected_id').val( id );
                $('#txt_selected').val( name );
                //$('#txt_selected_job').val( job );
            }
        
            function show_list()
            {
                document.getElementById('id_field').value=str;
            }
         
            function daysInMonth(month,year) 
            {
                return new Date(year, month, 0).getDate();
            }
        </script>
    </head>
    <body>
		<?php
        //extract($_REQUEST);
        //include('includes/common.php');
        if ($type=="buyer_sel")
        {
            ?>
            <input type="hidden" name="txt_selected_id" id="txt_selected_id" value="" />
            <input type="hidden" name="txt_selected"  id="txt_selected" width="650px" value="" />
            <div style="border:1px solid #7F9FFF; width:600px;">
                <div style="width:600px;" align="left">
                    <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" rules="all" >
                            <thead>
                                <th width="50" align="left">SL No</th>
                                <th width="130" align="left">Buyer Name</th>
                                <th width="120" align="left">Sub Con. Party</th>
                                <th width="100" align="left">Contact Person</th>
                                <th width="80" align="center">Contact NO.</th>
                                <th align="center">Email</th>
                            </thead>
                    </table>
                </div>	
                <div style="width:600px; overflow-y:scroll; min-height:50px; max-height:250px;" id="buyer_list_view" align="left">
                    <table  cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="tbl_list_search" rules="all" >
                    <?php
                        $i=1;
                        $sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1  order by buyer_name");
                        while ($selectResult = mysql_fetch_array($sql))
						{
                            if ($i%2==0) $bgcolor="#E9F3FF";
                            else $bgcolor="#FFFFFF";	
							?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onclick="js_set_value(<?php echo $i;?>)"> 
                                <td width="50" align="center"><?php echo "$i"; ?>
                                 <input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $selectResult['buyer_name']; ?>"/>
                                 <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult['id']; ?>"/>	
                                                             
                                </td>	
                                <td width="130">&nbsp;
                                    <?php 
                                        $frm="buyer_info"; 
                                        
                                        echo split_string($selectResult[buyer_name],13);
                                                                
                                    ?>
                                </td>
                                <td width="120">&nbsp; <?php
                                                        $frm=$replacement_lc[$selectResult[subcontract_party]];  
                                                        echo $frm;
                                                 ?>
                                </td>
                                <td width="100">&nbsp; <?php echo split_string($selectResult[contact_person],10);  ?></td>
                                <td width="80">&nbsp; <?php echo  split_string($selectResult[contact_no],11);  ?></td>
                                <td>&nbsp;<?php echo  split_string($selectResult[buyer_email],10);   ?></td>				
                            </tr>
							<?php
                            $i++;
                        }
                        ?>
                    </table>
                </div>
                <div style="width:600px;" align="left">
                    <table width="100%">
                        <tr>
                            <td align="center" colspan="6" height="30" valign="bottom">
                                <div style="width:100%"> 
                                        <div style="width:50%; float:left" align="left">
                                            <input type="checkbox" name="check_all" id="check_all" onclick="check_all_data()" /> Check / Uncheck All
                                        </div>
                                        <div style="width:50%; float:left" align="left">
                                        <input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                                        </div>
                                </div>
                            </td>
                        </tr>
                	</table>
                </div>
            </div>
            <?php
        }
        
        // company_sel
        if ($type=="company_sel")
        {
            ?>
            <input type="hidden" name="txt_selected_id" id="txt_selected_id" value="" />
            <input type="hidden" name="txt_selected"  id="txt_selected" width="650px" value="" />
            <div style="border:1px solid #7F9FFF; width:600px;">
                <div style="width:600px;" align="left">
                    <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" rules="all" >
                            <thead>
                                <th width="50" align="left">SL No</th>
                                <th width="130" align="left">Company Name</th>
                                <th width="120" align="left">Short Name</th>
                                <th width="100" align="left">Email</th>
                                <th width="80" align="center">Web</th>
                                <th align="center">Contact Person</th>
                            </thead>
                    </table>
                </div>	 
                <div style="width:600px; overflow-y:scroll; min-height:50px; max-height:250px;" id="buyer_list_view" align="left">
                    <table  cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="tbl_list_search" rules="all" >
                    <?php
                    $i=1;
                    $sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0  and status_active=1 order by company_name");
                         
                    while ($selectResult = mysql_fetch_array($sql))
                    {
                        if ($i%2==0) $bgcolor="#E9F3FF";
                        else $bgcolor="#FFFFFF";	
                        ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $selectResult['id'];?>" onclick="js_set_value(<?php echo $selectResult['id'];?>)"> 
                            <td width="50" align="center"><?php echo "$i"; ?>
                            <input type="hidden" name="txt_individual" id="txt_individual<?php echo $selectResult['id']; ?>" value="<?php echo $selectResult['company_name']; ?>"/>
                            <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $selectResult['id']; ?>" value="<?php echo $selectResult['id']; ?>"/>	
                            </td>
                            <td width="130">&nbsp;<?php echo split_string($selectResult[company_name],13); ?></td>
                            <td width="120">&nbsp;<?php $frm=$selectResult[company_short_name];  echo $frm; ?> </td>
                            <td width="100">&nbsp;<?php echo split_string($selectResult[email],10); ?></td>
                            <td width="80">&nbsp;<?php echo  split_string($selectResult[website],11); ?></td>
                            <td>&nbsp;<?php echo  split_string($selectResult[contract_person],10); ?></td>				
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </table>
                </div>
                <div style="width:600px;" align="left">
                    <table width="100%">
                        <tr>
                            <td align="center" colspan="6" height="30" valign="bottom">
                                <div style="width:100%"> 
                                        <div style="width:50%; float:left" align="left">
                                            <input type="checkbox" name="check_all" id="check_all" onclick="check_all_data()" /> Check / Uncheck All
                                        </div>
                                        <div style="width:50%; float:left" align="left">
                                        <input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                                        </div>
                                </div>
                            </td>
                        </tr>
                	</table>
                </div>	 
            </div>
            <?php
        }
        ?>
    </body>
</html>