<?php
session_start();

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_name=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0) $company_name="and id=".$_SESSION['logic_erp']["company_id"]; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>

<script>
		function set_date_range(mon)
		{
			 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = document.getElementById('cbo_year').value;
			var start_date="01" + "-" + mon  + "-" + year;
			var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
			document.getElementById('txt_date_from').value=start_date;
			document.getElementById('txt_date_to').value=to_date;		
		}
		
		function daysInMonth(month,year) 
		{
			return new Date(year, month, 0).getDate();
		}
		
		function generate_report(div,stype)
		{
			$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
			if (stype==1) // main call
	        {
				document.getElementById(div).innerHTML="";
				var cbo_company_name=document.getElementById('cbo_company_mst').value;
				var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
				var txt_style_name=document.getElementById('txt_style_name').value;
				var txt_date_from=document.getElementById('txt_date_from').value;
				var txt_date_to=document.getElementById('txt_date_to').value;
				var cbo_team_name=document.getElementById('cbo_team_name').value;
				var cbo_team_member=document.getElementById('cbo_team_member').value;
				var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_style_name+"_"+txt_date_from+"_"+txt_date_to+"_"+cbo_team_name+"_"+cbo_team_member;
			}
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			if (cbo_company_name==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
				return false; 
			}
			else if (txt_date_from==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select From Date....').fadeIn(1000);
				return false; 
			}
			else if (txt_date_to==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select To Date....').fadeIn(1000);
				return false; 
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var response=(xmlhttp.responseText).split('####');	
					document.getElementById(div).innerHTML=response[0];
					document.getElementById('print_div').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
					//document.getElementById(div).innerHTML=xmlhttp.responseText; <a href="includes/tmp_report_file/'+response[1]+'" >'+'<input type="button" value="Print Preview" style="width:100px" class="formbutton" >'+ '</a>
					$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox 
					$(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
					});
				}
			}
			xmlhttp.open("GET","includes/generate_germents_ex_factory_summary_report.php?data="+data+"&type=order_booking_status_report",true);
			xmlhttp.send();
		}
		
		
		
	function show_inner_filter(e)
    {
    	if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
 		if (unicode==13 )
		{
         	generate_report('report_container',2);
		}
    }
	
	function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function load_drop_down(type,team_id,div)
		{
			var strURL="includes/ajax_dropdown_loader.php?type="+escape(type)+"&team_id="+team_id;
			var req = getXMLHTTP();
			if (req) 
			{
				req.onreadystatechange = function() {
					if (req.readyState == 4) 
					{
							if (req.status == 200) 
							{	
								document.getElementById(div).innerHTML=req.responseText;
							} 
							else 
							{
								alert("There was a problem while using XMLHTTP:\n" + req.statusText);
							}
					}				
				}			
				req.open("GET", strURL, true);
				req.send(null);
			}
		}
	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.height="auto";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
		d.close();
		document.getElementById('scroll_body').style.overflow="scroll";
		document.getElementById('scroll_body').style.height="280px";
	}
	function change_color(v_id,e_color)
		{
			//alert(document.getElementById(v_id).bgColor);
			 
			if (document.getElementById(v_id).bgColor=="#33CC00")
			{
				document.getElementById(v_id).bgColor=e_color;
			}
			else
			{
				document.getElementById(v_id).bgColor="#33CC00";
			}
		}		
	
</script>
</head>


<body>
<div style="width:990px">
	<fieldset style="width:100%">
    <form>
    	<table width="100%" cellpadding="0" cellspacing="2" border="0">
        	<tr class="form_caption">
            	<td align="center" height="25" valign="middle"><font size="3">Order  Booking  Status Report</font></td><td colspan="8" align="center" height="25" valign="middle"> </td>
            </tr>
            <tr>
            	<td align="center" height="20" valign="middle">
                	<div id="messagebox" style="background:#F99" align="center"></div>
                </td>
            </tr>
            <tr>
            	<td>
                	<table class="rpt_table" width="990">
                    	<thead>
                        	<th>Company</th>
                            <th>Buyer</th>
                            <th>Team</th>
                            <th>Team Member</th>
                            <th>Style</th>
                            <th colspan="2">Date</th>
                            <th><input type="reset" name="res" id="res" value="Reset" style="width:80px" class="formbutton" /></th>
                        </thead>
                        <tr class="general">
                        	<td>
                                <select name="cbo_company_mst" id="cbo_company_mst"  style="width:130px" class="combo_boxes">
                                    <? if ($company_name=="") { ?>
                                    <option value="0">--- Select Company ---</option>
                                    <?
                                    }
                                    $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name"); //where is_deleted=0 and status=0
                                    $n=mysql_num_rows($mod_sql);
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                        if ($n==1) $company_combo=$r_mod["id"];
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </td>
                             <td>
                                <select name="cbo_buyer_name" id="cbo_buyer_name"  style="width:130px" class="combo_boxes">
                                    <? if ($buyer_name=="") { ?>
                                    <option value="0">--- All Buyer ---</option>
                                    <?
                                    }
                                    $mod_sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1 and subcontract_party=2 $buyer_name order by buyer_name");
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                        
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[buyer_name]" ?> </option>
                                    <?
                                    }
                                    
                                    ?>
                                </select>
                            
                            </td>
                            
                            <td>                
                                <select name="cbo_team_name" id="cbo_team_name" onchange="load_drop_down(2,this.value,'div_team')" style="width:130px" class="combo_boxes">
                                    <option value="0">--- All Team ---</option>
                                    <?
                                    $mod_sql= mysql_db_query($DB, "select id,team_name from lib_marketing_team where is_deleted=0 and status_active=1 order by team_name");
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["team_name"]){?> selected <?php }?>><? echo "$r_mod[team_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                    
                                </select>
                            
                            </td>
                            <td>
                                <div id="div_team">
                                    <select name="cbo_team_member" id="cbo_team_member"  style="width:130px" class="combo_boxes">
                                        <option value="0">--- Select Team Member ---</option>
                                        
                                    </select>
                                </div>
                            </td>
                            <td>
                                <input type="text" name="txt_style_name" id="txt_style_name"  class="text_boxes" style="width:120px" />
                            </td>
                            <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" disabled="disabled">
								<script type="text/javascript">
                                    $( "#txt_date_from" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> </td>
                            <td><input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" disabled="disabled">
                                <script type="text/javascript">
                                    $( "#txt_date_to" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> 
                            </td>
                            
                            <td width="5%">
                            	<input type="button" name="search" id="search" value="Show" onclick="generate_report('report_container',1)" style="width:80px" class="formbutton" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="8" align="left">
                  <?
                    	$c_year= date("Y");
                    	$p_year=$c_year-15;
					
                    
                    ?>
                    <select name="cbo_year" id="cbo_year"  style="width:80px" class="combo_boxes">
						 
						<?
						for($i=0;$i<21;$i++)
						{
						?>
						<option value=<? echo $p_year+$i;
						if ($c_year==$p_year+$i){?> selected <?php }?>><? echo $p_year+$i; ?> </option>
						<?
						}
						?>
					</select>
               
                    &nbsp;<input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="January" class="month_button" style="width:60px" />
                    &nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="February" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="March" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="April" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" style="width:60px" />
                    &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="June" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="July" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="August" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="September" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="October" class="month_button" style="width:60px"/>
                    &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="November" class="month_button" style="width:60px" />
                    &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="December" class="month_button" style="width:60px" />          
                 </td>
            </tr>            
         </table>
     </form>
     </fieldset>
    <fieldset style="width:100%" >
    	<!--<div align="center" id="print_div" style="width:100%">
        <div id="report_container" style="width:100%">-->
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="8" height="20" id="print_div" align="center"></td>
            </tr>
            <tr>
                <td colspan="8" id="report_container"></td>
            </tr>
        </table>
    </fieldset>
</div>

</body>
</html>
