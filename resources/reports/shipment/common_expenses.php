<? 
include('../../includes/common.php');
include('../../includes/array_function.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<link href="../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function numbersonly(myfield, e, dec)
	{
		var key;
		var keychar;
	
		if (window.event)
			key = window.event.keyCode;
		else if (e)
			key = e.which;
		else
			return true;
		keychar = String.fromCharCode(key);
	
		// control keys
		if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
		return true;
		
		// numbers
		else if ((("0123456789.").indexOf(keychar) > -1))
			return true;
		else
			return false;
	}	
</script>
</head>

<body>
<div>
<form name="search_order_frm"  id="search_order_frm">
	<fieldset style="width:650px">
	<div class="form_caption">Common Expenses Cost</div>
	<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="rpt_table">
		<thead>
			<th>Cost Heads</th>
            <th>Amount</th>
            <th>Amount Type</th>
        </thead>
        <tr>
        	<td>Fabric Conversion Cost</td>
            <td align="center"><input type="text" name="txt_fabric_conversion_amnt" id="txt_fabric_conversion_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_fabric_conversion_amount_type" id="cbo_fabric_conversion_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
     	<tr>
        	<td>Commercial Cost</td>
            <td align="center"><input type="text" name="txt_commercial_amnt" id="txt_commercial_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_commercial_amount_type" id="cbo_commercial_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
         <tr>
        	<td>Commission Cost</td>
            <td align="center"><input type="text" name="txt_commission_amnt" id="txt_commission_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_commission_amount_type" id="cbo_commission_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
          <tr>
        	<td>Testing Cost</td>
            <td align="center"><input type="text" name="txt_testing_amnt" id="txt_testing_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_testing_amount_type" id="cbo_testing_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
          <tr>
        	<td>Inspection Cost</td>
            <td align="center"><input type="text" name="txt_inspection_amnt" id="txt_inspection_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_inspection_amount_type" id="cbo_inspection_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
         <tr>
        	<td>Courier Cost</td>
            <td align="center"><input type="text" name="txt_courier_amnt" id="txt_courier_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_courier_amount_type" id="cbo_courier_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
         <tr>
        	<td>CM Cost</td>
            <td align="center"><input type="text" name="txt_cm_amnt" id="txt_cm_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_cm_amount_type" id="cbo_cm_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
         <tr>
        	<td>Operating Cost</td>
            <td align="center"><input type="text" name="txt_operating_amnt" id="txt_operating_amnt" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"></td>
        	<td align="center">
            	<select name="cbo_operating_amount_type" id="cbo_operating_amount_type" class="combo_boxes" style="width:140px"> 
                	<option value="1"> Fixed</option>
                    <option value="2" selected="selected"> % (Percent)</option>
                </select>
            </td>     
         </tr>
         <tr>
         	<td colspan="3" align="center">&nbsp;</td>
         </tr>   
         <tr>
         	<td colspan="3" align="center"><input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close"  style="width:100px"/></td>
         </tr>
	</table>
	</fieldset>
</form>
</div>
</body>
</html>