<?php
session_start();

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_name=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0) $company_name="and id=".$_SESSION['logic_erp']["company_id"]; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/popup_window.js"></script>
<script type="text/javascript" src="../../js/modal.js"></script>

<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>

<script>
		
		function generate_report(div,type)
		{
			$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
			document.getElementById(div).innerHTML="";
			
			var cbo_company_name=document.getElementById('cbo_company_mst').value;
			var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
			var txt_job_no=document.getElementById('txt_job_no').value;
			var txt_date_from=document.getElementById('txt_date_from').value;
			var txt_date_to=document.getElementById('txt_date_to').value;
			var hide_common_expenses=document.getElementById('hide_common_expenses').value;
			var hide_fabric_conversion=document.getElementById('hide_fabric_conversion').value;
			var hide_commercial=document.getElementById('hide_commercial').value;
			var hide_commission=document.getElementById('hide_commission').value;
			var hide_testing=document.getElementById('hide_testing').value;
			var hide_inspection=document.getElementById('hide_inspection').value;
			var hide_courier=document.getElementById('hide_courier').value;
			var hide_cm=document.getElementById('hide_cm').value;
			var hide_operating=document.getElementById('hide_operating').value;
			
			var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_job_no+"_"+txt_date_from+"_"+txt_date_to+"_"+hide_fabric_conversion+"_"+hide_commercial+"_"+hide_commission+"_"+hide_testing+"_"+hide_inspection+"_"+hide_courier+"_"+hide_cm+"_"+hide_operating;
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			if (cbo_company_name==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
				return false; 
			}
			else if (cbo_buyer_name==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Buyer....').fadeIn(1000);
				return false; 
			}
			else if (txt_job_no=="")
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Insert Job No....').fadeIn(1000);
				return false; 
			}
			else if (hide_common_expenses!=1)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Insert Common Expenses....').fadeIn(1000);
				return false; 
			}
			else if(txt_date_from=="" && txt_date_to!="")
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Insert From Date....').fadeIn(1000);
				return false; 
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var response=(xmlhttp.responseText).split('####');	
					document.getElementById(div).innerHTML=response[0];
					document.getElementById('data_panel').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
					//document.getElementById(div).innerHTML=xmlhttp.responseText; <a href="includes/tmp_report_file/'+response[1]+'" >'+'<input type="button" value="Print Preview" style="width:100px" class="formbutton" >'+ '</a>
					$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox 
					$(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
					});
				}
			}
			xmlhttp.open("GET","includes/generate_post_cost_analysis_report.php?data="+data+"&type="+type,true);
			xmlhttp.send();
		}
	
	function openmypage_job_info(page_link,title)
	{
		var company_name=document.getElementById('cbo_company_mst').value;
		var buyer_id=document.getElementById('cbo_buyer_name').value;
		var txt_date_from=document.getElementById('txt_date_from').value;
		var txt_date_to=document.getElementById('txt_date_to').value;
		if(company_name==0)
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_mst').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(10000);
			});		
		}
		else if(buyer_id==0)
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_buyer_name').focus();
			$(this).html('Please Select Buyer Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(10000);
			});		
		}
		else if(txt_date_from=="" && txt_date_to!="")
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_date_from').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(10000);
			});		
		}
		else
		{
		
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+'?com_name='+company_name+'&buyer_id='+buyer_id+'&txt_date_from='+txt_date_from+'&txt_date_to='+txt_date_to, title,  'width=600px,height=350px,center=1,resize=0,scrolling=0','../')
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var thee_loc = this.contentDoc.getElementById("txt_selected_id");
				var thee_mail = this.contentDoc.getElementById("txt_selected");
				//alert(thee_loc.value+thee_id.value);
				$('#txt_job_no').val(thee_loc.value);
			}
		}
	}
	
	function openmypage_common_expenses(page_link,title)
	{
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=700px,height=350px,center=1,resize=0,scrolling=0','../')
		
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var thee_loc_fab = this.contentDoc.getElementById("txt_fabric_conversion_amnt");
			var thee_mail_fab = this.contentDoc.getElementById("cbo_fabric_conversion_amount_type");
			var fabric_conversion=thee_mail_fab.value+"*"+thee_loc_fab.value;
			
			var thee_loc_commer = this.contentDoc.getElementById("txt_commercial_amnt");
			var thee_mail_commer = this.contentDoc.getElementById("cbo_commercial_amount_type");
			var commercial=thee_mail_commer.value+"*"+thee_loc_commer.value;
			
			var thee_loc_commis = this.contentDoc.getElementById("txt_commission_amnt");
			var thee_mail_commis = this.contentDoc.getElementById("cbo_commission_amount_type");
			var commission=thee_mail_commis.value+"*"+thee_loc_commis.value;
			
			var thee_loc_test = this.contentDoc.getElementById("txt_testing_amnt");
			var thee_mail_test = this.contentDoc.getElementById("cbo_testing_amount_type");
			var testing=thee_mail_test.value+"*"+thee_loc_test.value;
			
			var thee_loc_insp = this.contentDoc.getElementById("txt_inspection_amnt");
			var thee_mail_insp = this.contentDoc.getElementById("cbo_inspection_amount_type");
			var inspection=thee_mail_insp.value+"*"+thee_loc_insp.value;
			
			var thee_loc_cour = this.contentDoc.getElementById("txt_courier_amnt");
			var thee_mail_cour = this.contentDoc.getElementById("cbo_courier_amount_type");
			var courier=thee_mail_cour.value+"*"+thee_loc_cour.value;
			
			var thee_loc_cm = this.contentDoc.getElementById("txt_cm_amnt");
			var thee_mail_cm = this.contentDoc.getElementById("cbo_cm_amount_type");
			var cm=thee_mail_cm.value+"*"+thee_loc_cm.value;
			
			var thee_loc_oper = this.contentDoc.getElementById("txt_operating_amnt");
			var thee_mail_oper = this.contentDoc.getElementById("cbo_operating_amount_type");
			var operating=thee_mail_oper.value+"*"+thee_loc_oper.value;
			
			$('#hide_common_expenses').val('');
			$('#hide_fabric_conversion').val('');
			$('#hide_commercial').val('');
			$('#hide_commission').val('');
			$('#hide_testing').val('');
			$('#hide_inspection').val('');
			$('#hide_courier').val('');
			$('#hide_cm').val('');
			$('#hide_operating').val('');
			
			$('#hide_common_expenses').val(1);
			$('#hide_fabric_conversion').val(fabric_conversion);
			$('#hide_commercial').val(commercial);
			$('#hide_commission').val(commission);
			$('#hide_testing').val(testing);
			$('#hide_inspection').val(inspection);
			$('#hide_courier').val(courier);
			$('#hide_cm').val(cm);
			$('#hide_operating').val(operating);
		}
	}
	
	function new_window()
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
		d.close();
	}		
	function reset_field()
	{
			document.getElementById('data_panel').innerHTML="";
			document.getElementById('report_container').innerHTML="";
			document.getElementById('hide_common_expenses').value="";
	}
	function change_color(v_id,e_color)
	{
		//alert(document.getElementById(v_id).bgColor);
		 
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
</script>
</head>


<body>
<div style="width:990px">
    	<div>
            <div class="form_caption">
               Post Cost Analysis Report
            </div>
            <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
        </div>
	<fieldset style="width:100%">
    <form>
        <table class="rpt_table" width="950" cellpadding="0" cellspacing="0" border="0" align="center">
            <thead>
                <th>Company</th>
                <th>Buyer</th>
                <th colspan="2">Shipment Date</th>
                <th>Job No</th>
                <th><input type="reset" name="res" id="res" value="Reset" style="width:100px" class="formbutton" onclick="reset_field();"/>
                &nbsp;<input type="hidden" name="hide_common_expenses" id="hide_common_expenses" /></th>
            </thead>
            <tr class="general">
                <td>
                    <select name="cbo_company_mst" id="cbo_company_mst"  style="width:150px" class="combo_boxes">
                        <? if ($company_name=="") { ?>
                        <option value="0">--- Select Company ---</option>
                        <?
                        }
                        $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name"); //where is_deleted=0 and status=0
                        $n=mysql_num_rows($mod_sql);
                        while ($r_mod=mysql_fetch_array($mod_sql))
                        {
                            if ($n==1) $company_combo=$r_mod["id"];
                        ?>
                        <option value=<? echo $r_mod["id"];
                        if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                        <?
                        }
                        ?>
                    </select>
                </td>
                 <td>
                    <select name="cbo_buyer_name" id="cbo_buyer_name"  style="width:150px" class="combo_boxes">
                        <? if ($buyer_name=="") { ?>
                        <option value="0">--- Select Buyer ---</option>
                        <?
                        }
                        $mod_sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1 and subcontract_party=2 $buyer_name order by buyer_name");
                        while ($r_mod=mysql_fetch_array($mod_sql))
                        {
                            
                        ?>
                        <option value=<? echo $r_mod["id"];
                        if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[buyer_name]" ?> </option>
                        <?
                        }
                        
                        ?>
                    </select>
                
                </td>
                <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date">
                    <script type="text/javascript">
                        $( "#txt_date_from" ).datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    changeMonth: true,
                                    changeYear: true
                                });
                    
                    </script> </td>
                <td><input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date">
                    <script type="text/javascript">
                        $( "#txt_date_to" ).datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    changeMonth: true,
                                    changeYear: true
                                });
                    
                    </script> 
                </td>
                <td>
                    <input type="text" name="txt_job_no" id="txt_job_no"  class="text_boxes" style="width:170px" ondblclick="openmypage_job_info('search_job_no.php','Job Information'); return false" placeholder="Double Click For Job No" autocomplete="off" readonly="readonly" />
                </td>
                <td>
                     <input type="button" name="search" id="search" value="Common Expenses" style="width:130px" class="formbutton" onclick="openmypage_common_expenses('common_expenses.php','Common Expenses Information'); return false" />
                     &nbsp; <input type="button" name="search" id="search" value="Show" onclick="generate_report('report_container','post_cost_analysis_report')" style="width:80px" class="formbutton" />
                </td>
                <input type="hidden" name="hide_fabric_conversion" id="hide_fabric_conversion" />
                <input type="hidden" name="hide_commercial" id="hide_commercial" />
                <input type="hidden" name="hide_commission" id="hide_commission" />
                <input type="hidden" name="hide_testing" id="hide_testing" />
                <input type="hidden" name="hide_inspection" id="hide_inspection" />
                <input type="hidden" name="hide_courier" id="hide_courier" />
                <input type="hidden" name="hide_cm" id="hide_cm" />
                <input type="hidden" name="hide_operating" id="hide_operating" />
            </tr>
       	</table>
     </form>
     </fieldset>
    <fieldset style="width:100%" >
    	
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="8" height="20" id="data_panel" align="center"></td>
            </tr>
            <tr>
                <td colspan="8" id="report_container"></td>
            </tr>
        </table>
    </fieldset>
</div>

</body>
</html>
