<?

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 11-04-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1)
{
	if($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_cond=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_cond="";
	if($_SESSION['logic_erp']["company_id"]!=0) $company_cond=" and id=".$_SESSION['logic_erp']["company_id"]; else $company_cond="";
}
else
{
	$buyer_cond="";	$company_cond="";
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
	<script src="../../order/reports/includes/ajax_submit.js" type="text/javascript"></script>
	<script src="../../order/reports/includes/functions.js" type="text/javascript"></script>

	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
	
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    
	<script>
	function set_date_range(mon)
	{
		 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		//var year = currentTime.getFullYear();
		var year = document.getElementById('cbo_year').value;
		
		var start_date="01" + "-" + mon  + "-" + year;
		var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
		
		document.getElementById('txt_date_from').value=start_date;
		document.getElementById('txt_date_to').value=to_date;
				
	}
	
   	function daysInMonth(month,year) 
   	{
    	return new Date(year, month, 0).getDate();
	}
	
	function change_color(v_id,e_color)
	{
		
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	
function generate_report(div)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	
	document.getElementById(div).innerHTML="";
	
	var cbo_company_name=document.getElementById('cbo_company_mst').value;
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	var txt_style_name=document.getElementById('txt_style_name').value;
	
	var txt_date_from=document.getElementById('txt_date_from').value;
	var txt_date_to=document.getElementById('txt_date_to').value;
	
	var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_style_name+"_"+txt_date_from+"_"+txt_date_to;
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	
	if (cbo_company_name==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
		return false; }
		
	/*else if (cbo_buyer_name==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Buyer Name....').fadeIn(1000);
		return false; }	*/
		
	if (txt_date_from==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select From Date....').fadeIn(1000);
		return false; }
		
	else if (txt_date_to==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select To Date....').fadeIn(1000);
		return false; }
	
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET","includes/generate_report_order.php?data="+data+"&type=material_budget",true);
	xmlhttp.send();
}

function generate_report_details(div,id)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	
	document.getElementById(div).innerHTML="";
	 
	var data=id;
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET","includes/generate_report_order.php?data="+data+"&type=material_budget_details",true);
	xmlhttp.send();
}	
	
	function show_progress_report_detailss(id)
	{
		//alert(id);
		/*emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'shipdate_work_progress_report_popup.php?type='+type+'&job_number='+job_number, 'Work Progress Report Details', 'width=730px,height=400px,center=1,resize=0,scrolling=0','../')
		*/
		var data=id;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'includes/generate_report_order.php?type=material_budget_details&data='+id,'Details', 'width=800px,height=430px,center=1,resize=0,scrolling=0','../')
		emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("hid_print");
				//alert(thee_loc.value);
				//window.location=thee_loc.value;				 
			}	
		
	}
	function show_progress_report_detailss_order(id,po_id)
	{
		var data=id;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'includes/generate_report_order.php?type=material_budget_details_order&data='+id+'&po_id='+po_id,'Details', 'width=800px,height=430px,center=1,resize=0,scrolling=0','../')
		emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("hid_print");
			}	
		
	}
    function toggle( x, origColor ) {
		//alert(x+origColor);
			var newColor = 'green';
			if ( document.getElementById(x).style ) {
				document.getElementById(x).style.backgroundColor = ( newColor == document.getElementById(x).style.backgroundColor )? origColor : newColor;
			}
		}
    </script>	
	
</head>

<?php
include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
?>

<body onLoad="document.onkeydown = checkKeycode;">
<div style="width:1600px">
<form id="material_budget_frm">
	<fieldset style="width:60%" >
    	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
        	<tr class="form_caption"> 
            	<td colspan="14">Order Wise Budgeted Cost Report</td>
            </tr>
            <tr>
            	<td colspan="14" align="center" height="30" valign="middle">
                	<div id="messagebox" style="background:#F99" align="center"></div>
                </td>
            </tr>
         </table>   
         <table cellpadding="0" cellspacing="0" class="rpt_table" align="center">   
            <thead>              
              <th align="left">Company Name</th>              
              <th align="left">Buyer Name</th>              
              <th align="left">Style Name</th>             
              <th colspan="">Date From </th>             
              <th><input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" /></th>  
           </thead>
            <tr class="general">            	
                <td align="left">
                	<select name="cbo_company_mst" id="cbo_company_mst" style="width:160px" class="combo_boxes">
						<?
						if($company_cond==""){
						?>
                        <option value="0">--- Select Company ---</option>
						<?
						}
						$mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_cond order by company_name"); //where is_deleted=0 and status=0
						$num=mysql_num_rows($mod_sql);
						while ($r_mod=mysql_fetch_array($mod_sql))
						{
							if ($num==1)
							{
								$company_combo=$r_mod["id"];
							}
						?>
						<option value=<? echo $r_mod["id"];
						if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
						<?
						}
						?>
					</select>
                
                </td>               
                <td align="left">
                	<select name="cbo_buyer_name" id="cbo_buyer_name" style="width:160px" class="combo_boxes">
						<?
						if($buyer_cond==""){
						?>
                        <option value="0">--- Select Buyer ---</option>
						<?
						}
						$mod_sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1 $buyer_cond order by buyer_name");
						while ($r_mod=mysql_fetch_array($mod_sql))
						{
							
						?>
						<option value=<? echo $r_mod["id"];
						if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[buyer_name]" ?> </option>
						<?
						}
						
						?>
					</select>
                
                </td>               
                <td align="left">
                	<input type="text" name="txt_style_name" id="txt_style_name" class="text_boxes" style="width:120px" />
                </td>               
                <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:90px">
					<script type="text/javascript">
						$( "#txt_date_from" ).datepicker({
									dateFormat: 'dd-mm-yy',
									changeMonth: true,
									changeYear: true
								});
					
					</script>               
                To <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:90px">
					<script type="text/javascript">
						$( "#txt_date_to" ).datepicker({
									dateFormat: 'dd-mm-yy',
									changeMonth: true,
									changeYear: true
								});
					
					</script></td>
                <td width="74"><input type="button" name="search" id="search" value="Show" onclick="generate_report('report_container')" style="width:100px" class="formbutton" /></td>
               
            </tr>
            
        	<tr>
            	<td colspan="14" align="center" height="40" valign="bottom">
                	<select name="cbo_year" id="cbo_year"  style="width:80px" class="combo_boxes">
						<?
						$c_year= date("Y");
                    	$p_year=$c_year-10;
						for($i=0;$i<21;$i++)
						{
						?>
						<option value=<? echo $p_year+$i;
						if ($c_year==$p_year+$i){?> selected <?php }?>><? echo $p_year+$i; ?> </option>
						<?
						}
						?>
					</select>
                	<input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="Jan" class="month_button" />
                	&nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="Feb" class="month_button"/>
                    &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="Mar" class="month_button" />
                    &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="Apr" class="month_button" />
                    &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" />
                    &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="Jun" class="month_button" />
                    &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="Jul" class="month_button" />
                    &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="Aug" class="month_button" />
                    &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="Sep" class="month_button" />
                    &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="Oct" class="month_button" />
                    &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="Nov" class="month_button" />
                    &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="Dec" class="month_button" />
                   
                    
                </td>
            </tr>
           </table>
           </fieldset>
  </form>
</div> 
           <fieldset style="width:960px">
             <table cellpadding="0" cellspacing="2">             
                <tr>
                    <td colspan="10" id="report_container"></td>             
                </tr>
                 <tr>
                    <td colspan="10" id="report_container_bom"></td>                    
                </tr>
            </table>    	
   	 	  </fieldset>

</body>
</html>
