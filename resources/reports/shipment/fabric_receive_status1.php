<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
/* CSS Document */
table#table-body, table#table-header, table#table-footer
{
border-spacing:0;
border-collapse:collapse;
border:1px solid;
table-layout:fixed;
width:1000px;
border:1px solid #000;
}

table#table-header th
{

}

table#table-footer td
{

}

table#table-body td
{

}

table#table-body td, table#table-header th, table#table-footer td
{
border:1px solid #000;
width:100px;
height:25px;
overflow:hidden;
white-space:nowrap;	
}
div#header-container, div#footer-container
{
overflow:hidden;
}
div#scroll
{
width:1275px;
overflow:hidden;
max-height:150px;
padding-left:1px;
}

div#fake-scroll-container
{
width:1275px;
overflow:hidden;
position:relative;
}

div#y-fake-scroll
{
overflow-y:scroll; 
overflow-x:hidden;
background:transparent; 
position:absolute;
right:0;
position:absolute;
max-height:149px;
top:31px;
}

div#x-fake-scroll
{
height:40px;
margin-top:-23px;
overflow-x:auto;
overflow-y:hidden;
margin-top:expression('0px');/* IE 7 fix*/
height:expression('17px'); /* IE 7 fix*/
}

div#y-scroll
{
max-height:150px;
overflow-y:auto;
overflow-x:hidden;
overflow:scroll;
width:1010px;
padding:0px 1px 1px 1px;
}

div#header-container
{
padding:1px 1px 0 1px;
}

div#footer-container
{
padding:0 1px;
}
</style>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
	<script src="includes/ajax_submit.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>	
	<script>
	function scroll_varti_hori()
	{
		var YtableEmulator = document.getElementById('y-table-emulator');
var XtableEmulator = document.getElementById('x-table-emulator');
var table = document.getElementById('table-body');

YtableEmulator.style.height = table.clientHeight == 0 ? "330px" : table.clientHeight + "px";
XtableEmulator.style.width = table.clientWidth + "px";

var scrollablePanel = document.getElementById('scroll');
var headerContainer = document.getElementById('header-container');
var footerContainer = document.getElementById('footer-container');
var YfakeScrollablePanel = document.getElementById('y-fake-scroll');
var XfakeScrollablePanel = document.getElementById('x-fake-scroll');


YfakeScrollablePanel.style.top = headerContainer.clientHeight == 0 ? "34px" : headerContainer.clientHeight + "px";
scrollablePanel.onscroll = function (e) {
  	XfakeScrollablePanel.scrollTop = scrollablePanel.scrollTop;
}
YfakeScrollablePanel.onscroll = function (e) {
  	scrollablePanel.scrollTop = YfakeScrollablePanel.scrollTop;
}
XfakeScrollablePanel.onscroll = function (e) {
  	scrollablePanel.scrollLeft = XfakeScrollablePanel.scrollLeft;
	headerContainer.scrollLeft = XfakeScrollablePanel.scrollLeft;
	footerContainer.scrollLeft = XfakeScrollablePanel.scrollLeft;
}
	}
	
	function set_date_range(mon)
	{
		 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		
		var start_date="01" + "-" + mon  + "-" + year;
		var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
		
		document.getElementById('txt_date_from').value=start_date;
		document.getElementById('txt_date_to').value=to_date;
		 //alert(to_date);
		 generate_report('report_container');
	}
	
   	function daysInMonth(month,year) 
   	{
    	return new Date(year, month, 0).getDate();
	}
	
	
function generate_report(div)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	document.getElementById(div).innerHTML="";
	var cbo_company_name=document.getElementById('cbo_company_mst').value;
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	var txt_order_number=document.getElementById('txt_order_number').value;
	
	var txt_date_from=document.getElementById('txt_date_from').value;
	var txt_date_to=document.getElementById('txt_date_to').value;
	var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_order_number+"_"+txt_date_from+"_"+txt_date_to;
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	
	if (cbo_company_name==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
		return false; }
		
	else if (cbo_buyer_name==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Buyer Name....').fadeIn(1000);
		return false; }	
		
	if (txt_date_from==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select From Date....').fadeIn(1000);
		return false; }
		
	else if (txt_date_to==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select To Date....').fadeIn(1000);
		return false; }
	
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
				 scroll_varti_hori();
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET","includes/generate_fabric_receive_status.php?data="+data+"&type=generate_fabric_receive_status",true);
	xmlhttp.send();
}


	/*function show_progress_report_details(type,job_number)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'work_progress_report_popup.php?type='+type+'&job_number='+job_number, 'Work Progress Report Details', 'width=730px,height=400px,center=1,resize=0,scrolling=0','../')
	}
	
	function change_color(v_id,e_color)
	{
		//alert(v_id);
		//alert(e_color);
		//alert(document.getElementById(v_id).bgColor);
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	
	
	function openmypage(po_no_mst_id,order_no,production_type,entry_break_down_type,type,excess_percent)
	{
		var txt_date_from= document.getElementById('txt_date_from').value;
		var txt_date_to= document.getElementById('txt_date_to').value;
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	document.getElementById('report_container_detals').innerHTML="";
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById('report_container_detals').innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET",'color_size_wise_sewing_production_summary.php?po_no_mst_id='+po_no_mst_id+'&order_no='+order_no+'&production_type='+production_type+'&entry_break_down_type='+entry_break_down_type+'&txt_date_from='+txt_date_from+'&txt_date_to='+txt_date_to+'&type='+type+'&excess_percent='+excess_percent,true);
	xmlhttp.send();
	}*/
function openmypage2(po_no_mst_id,type)
{
//alert("type"+type);
emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'remark_veiw.php?po_no_mst_id='+po_no_mst_id+'&type='+type, 'Remarks Veiw', 'width=678px,height=450px,center=1,resize=0,scrolling=0','../')
}
	</script>
</head>
<?php
include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
?>
<body onLoad="document.onkeydown = checkKeycode;">
<div style="width:1310px">
<fieldset style="width:100%" >
<legend>Fillter Panel</legend>
        <table width="100%" cellpadding="0" cellspacing="2">
                <tr bgcolor="#CCCCCC">
                <td colspan="13" align="center" height="30" valign="middle"><font size="2"><strong>Fabric Receive Status</strong></font></td>
                </tr>
                <tr>
                <td colspan="13" align="center" height="30" valign="middle">
                <div id="messagebox" style="background:#F99" align="center"></div>
                </td>
                </tr>
                <tr>
                <td colspan="13"> 
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  Company Name<select name="cbo_company_mst" id="cbo_company_mst" onchange="generate_report('report_container')" style="width:150px" class="combo_boxes">
                <option value="0">--- All Company ---</option>
                <?
                $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 order by id"); //where is_deleted=0 and status=0
                while ($r_mod=mysql_fetch_array($mod_sql))
                {
                ?>
                <option value=<? echo $r_mod["id"];
                if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                <?
                }
                ?>
                </select>
                 Buyer Name
                <select name="cbo_buyer_name" id="cbo_buyer_name" onchange="generate_report('report_container')" style="width:140px" class="combo_boxes">
                <option value="0">--- All Buyer ---</option>
                <?
                $mod_sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1 and subcontract_party=2 order by id");
                while ($r_mod=mysql_fetch_array($mod_sql))
                {
                ?>
                <option value=<? echo $r_mod["id"];
                if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[buyer_name]" ?> </option>
                <?
                }
                ?>
                </select>
                Order No
                <input type="text" name="txt_order_number" id="txt_order_number" onkeyup="generate_report('report_container');" class="text_boxes" style="width:100px" />
                Date From 
                <?
                $date= date("d-m-Y");
                $arr=explode('-', $date);
                $day=$arr[0]-1;
                ?>
                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px" value="<?  //echo convert_to_mysql_date( date("Y-m-d", time() - 86400)); // echo $day."-".$arr[1]."-".$arr[2]; ?>">
                <script type="text/javascript">
                $( "#txt_date_from" ).datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
                });
               </script> 
                Date To 
                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px" value="<? // echo convert_to_mysql_date( date("Y-m-d", time() - 86400)); //echo $day."-".$arr[1]."-".$arr[2];?>">
                <script type="text/javascript">
                $( "#txt_date_to" ).datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
                });
                </script>
                <input type="button" name="show" id="show" onclick="generate_report('report_container');" class="formbutton" style="width:70px" value="Show" />
                </td>
                </tr>
                <tr>
                <td colspan="13" align="center" height="25" valign="bottom">
                     <input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="January" class="month_button" />
                &nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="February" class="month_button" />
                &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="March" class="month_button" />
                &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="April" class="month_button" />
                &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" />
                &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="June" class="month_button" />
                &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="July" class="month_button" />
                &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="August" class="month_button" />
                &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="September" class="month_button" />
                &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="October" class="month_button" />
                &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="November" class="month_button" />
                &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="December" class="month_button" />          
                 </td>
                </tr>
        </table>
<fieldset> 
<legend>Report Panel</legend>       
<table cellpadding="0" cellspacing="0" width="98%">
<tr>
<td colspan="13" height="20">
</td>
</tr>
<tr>
<td colspan="13" id="report_container"> 
</td>
</tr>
</table>
</fieldset>


<table>
<tr>
<td colspan="13" id="report_container_detals" border="1"  class="display"> 
</td>
</tr>
</table>
<a name="report_detals"></a>
</fieldset>
</div>

</body>
</html>
