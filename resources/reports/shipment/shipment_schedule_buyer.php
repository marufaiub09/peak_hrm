<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/popup_window.js"></script>
<script type="text/javascript" src="../../js/modal.js"></script>

<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>

<script type="text/javascript" src="../amcharts/flash/swfobject.js"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
		<!-- following scripts required for JavaScript version. The order is important! -->
		<script type="text/javascript" src="../amcharts/javascript/amcharts.js"></script>
		<script type="text/javascript" src="../amcharts/javascript/amfallback.js"></script>
		<script type="text/javascript" src="../amcharts/javascript/raphael.js"></script>
        <script language="javascript" type="text/javascript" src="../../includes/tablefilter.js"></script>
        <link href="../../includes/filtergrid.css" rel="stylesheet" type="text/css" />

<script>
		function set_date_range(mon)
		{
			 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
			
			  if (mon.substr(0,1)=="0") if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon; else id_id=mon;
			 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
			 
			
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = document.getElementById('cbo_year').value;
			var start_date="01" + "-" + mon  + "-" + year;
			var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
			document.getElementById('txt_date_from').value=start_date;
			document.getElementById('txt_date_to').value=to_date;
			//generate_report('report_container',1);
		}
		
		function daysInMonth(month,year) 
		{
			return new Date(year, month, 0).getDate();
		}
		
	function generate_report_main(e)
		{
			if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
			var inn=document.getElementById('fillter_check').value;
			if(inn=='')
			{
			generate_report('report_container',1)
			}
			if(inn==1)
			{
			show_inner_filter(unicode);
			}
		}
		
		function generate_report(div,stype)
		{
			$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
			if (stype==1) // main call
	        {
				document.getElementById(div).innerHTML="";
				var cbo_company_name=document.getElementById('cbo_company_mst').value;
				var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
				var cbo_order_status=2;
				var txt_date_from=document.getElementById('txt_date_from').value;
				var txt_date_to=document.getElementById('txt_date_to').value;
				var cbo_team_name=document.getElementById('cbo_team_name').value;
				var cbo_team_member=document.getElementById('cbo_team_member').value;
				var cbo_category_by=document.getElementById('cbo_category_by').value;
				
				var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_date_from+"_"+txt_date_to+"_"+cbo_order_status+"_"+cbo_team_name+"_"+cbo_team_member+"_"+cbo_category_by+"______";
			}
			else if(stype==2)
	        {
				
				var cbo_company_name=document.getElementById('cbo_company_mst').value;
				var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
				var cbo_order_status=2;
				var txt_date_from=document.getElementById('txt_date_from').value;
				var txt_date_to=document.getElementById('txt_date_to').value;
				var cbo_team_name=document.getElementById('cbo_team_name').value;
				var cbo_team_member=document.getElementById('cbo_team_member').value;
				var cbo_category_by=document.getElementById('cbo_category_by').value;
				var txt_job_number=document.getElementById('txt_job_number').value;
				var txt_po_number=document.getElementById('txt_po_number').value;
				var txt_style_number=document.getElementById('txt_style_number').value;
				var txt_order_qnty=document.getElementById('txt_order_qnty').value;
				var txt_agent_name=document.getElementById('txt_agent_name').value;
				var txt_item_name=document.getElementById('txt_item_name').value;
			
				var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_date_from+"_"+txt_date_to+"_"+cbo_order_status+"_"+cbo_team_name+"_"+cbo_team_member+"_"+cbo_category_by+"_"+txt_job_number+"_"+txt_po_number+"_"+txt_style_number+"_"+txt_order_qnty+"_"+txt_agent_name+"_"+txt_item_name;	
			}
			else
			{
				var cbo_company_name=document.getElementById('cbo_company_mst').value;
				var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
				var cbo_order_status=document.getElementById('cbo_order_status').value;
				var txt_date_from=document.getElementById('txt_date_from').value;
				var txt_date_to=document.getElementById('txt_date_to').value;
				var cbo_team_name=document.getElementById('cbo_team_name').value;
				var cbo_team_member=document.getElementById('cbo_team_member').value;
				var cbo_category_by=document.getElementById('cbo_category_by').value;
				var txt_job_number=document.getElementById('txt_job_number').value;
				var txt_po_number=document.getElementById('txt_po_number').value;
				var txt_style_number=document.getElementById('txt_style_number').value;
				var txt_order_qnty=document.getElementById('txt_order_qnty').value;
				var txt_agent_name=document.getElementById('txt_agent_name').value;
				var txt_item_name=document.getElementById('txt_item_name').value;
				var cbo_order_status=document.getElementById('cbo_order_status').value;
				var shipping_status=document.getElementById('shipping_status').value;
				//alert(cbo_order_status);
				var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_date_from+"_"+txt_date_to+"_"+cbo_order_status+"_"+cbo_team_name+"_"+cbo_team_member+"_"+cbo_category_by+"_"+txt_job_number+"_"+txt_po_number+"_"+txt_style_number+"_"+txt_order_qnty+"_"+txt_agent_name+"_"+txt_item_name+"_"+shipping_status;	
			}
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			/*if (cbo_company_name==0){
			$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
			return false; }*/
			if (txt_date_from==0){
			$("#messagebox").removeClass().addClass('messagebox').text('Please Select From Date....').fadeIn(1000);
			return false; }
			else if (txt_date_to==0){
			$("#messagebox").removeClass().addClass('messagebox').text('Please Select To Date....').fadeIn(1000);
			return false; }
			xmlhttp.onreadystatechange=function()
			{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
			var response=(xmlhttp.responseText).split('####');	
			document.getElementById(div).innerHTML=response[0];
			document.getElementById('print_div').innerHTML='<a href="'+response[1]+'" > Convert to : <img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /> </a>';
			//document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
			$(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			document.getElementById('fillter_check').value=1;
			/*var tableFilters = {
		          col_21:'select',
				 display_all_text: " ---Show All---",
				col_operation: {
								   id: ["tot_order_qnty","po_price","ex_qnty","short_acc_qnty","short_acc_value","tot_fab_rec","tot_cm"],
								   col: [12,14,16,17,18,19,20],
								   operation: ["sum","sum","sum","sum","sum","sum","sum"],
								   write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
								}	            
								}
				 setFilterGrid("table-body",-1,tableFilters);*/
			});
			}
			}
			xmlhttp.open("GET","includes/shipment_schedule_buyer.php?data="+data+"&type=shipment_schedule_report",true);
			xmlhttp.send();
		}
		
		function show_progress_report_details(type,job_number)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'work_progress_report_popup.php?type='+type+'&job_number='+job_number, 'Work Progress Report Details', 'width=730px,height=400px,center=1,resize=0,scrolling=0','../')
		}
		
		function change_color(v_id,e_color)
		{
			//alert(document.getElementById(v_id).bgColor);
			 
			if (document.getElementById(v_id).bgColor=="#33CC00")
			{
				document.getElementById(v_id).bgColor=e_color;
			}
			else
			{
				document.getElementById(v_id).bgColor="#33CC00";
			}
		}
		
		function openmypage(po_id)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'ex_factory_date_pop_up.php?po_id='+po_id, 'Ex-Factory Date Report', 'width=350px, height=350px, center=1, resize=0, scrolling=0', '../')
		}
		
		function openmypage2(remarks,type)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'remarks_pop_up.php?remarks='+remarks+'&type='+type, type, 'width=400px,height=400px,center=1,resize=0,scrolling=0','../')
		}
		
		function show_inner_filter(e)
		{
			//if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
			if (e==13 )
			{
				generate_report('report_container',2);
			}
		}
	
	
	function show_cost_sheet_popup(job_number,order_number)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'cost_sheet_popup_sliding.php?job_number='+job_number+'&order_number='+order_number, 'Work Progress Report Details', 'width=980px,height=450px,center=1,resize=0,scrolling=0','../')
		}
		
		function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function load_drop_down(type,team_id,div)
		{
		var strURL="includes/ajax_dropdown_loader.php?type="+escape(type)+"&team_id="+team_id;
		var req = getXMLHTTP();
		if (req) 
		{
			req.onreadystatechange = function() {
				if (req.readyState == 4) 
				{
						if (req.status == 200) 
						{	
							document.getElementById(div).innerHTML=req.responseText;
						} 
						else 
						{
							alert("There was a problem while using XMLHTTP:\n" + req.statusText);
						}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
		}
		
		function show_graph_on_popup(data1)
		{
			 
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'shipment_sch_graph_iframe.php?form_chart='+escape(data1), 'Shipment Schedule Graph', 'width=580px,height=460px,center=1,resize=0,scrolling=0','../')
		}
	
</script>
</head>

<?php
session_start();

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_name=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0) $company_name="and id=".$_SESSION['logic_erp']["company_id"]; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $buyer_name."sjhjhj".$company_name;

include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
?>

<body>
<div style="width:990px">
<form>
	<fieldset style="width:100%">
    	<table width="100%" cellpadding="0" cellspacing="2" border="0">
        	<tr class="form_caption">
            	<td align="center" height="25" valign="middle"><font size="3">Shipment Schedule Report</font></td><td colspan="8" align="center" height="25" valign="middle"> </td>
            </tr>
            <tr>
            	<td align="center" height="20" valign="middle">
                	<div id="messagebox" style="background:#F99" align="center"></div>
                </td>
            </tr>
            <tr>
            	<td>
                	<table class="rpt_table" width="990" cellpadding="0" cellspacing="0" border="0">
                    	<thead>
                        	<th>Company</th>
                            <th>Buyer</th>
                            <th>Team</th>
                            <th>Team Member</th>
                            <th colspan="2">Date</th>
                            <th>Date Category</th>
                            <th><input type="reset" name="reset" id="reset" value="Reset" style="width:80px" class="formbutton" /></th>
                        </thead>
                        <tr class="general">
                        	<td>
                                <select name="cbo_company_mst" id="cbo_company_mst"  style="width:130px" class="combo_boxes">
                                    <? if ($company_name=="") { ?>
                                    <option value="0">--- All Company ---</option>
                                    <?
                                    }
                                    $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name"); //where is_deleted=0 and status=0
                                    $n=mysql_num_rows($mod_sql);
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                        if ($n==1) $company_combo=$r_mod["id"];
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </td>
                             <td>
                                <select name="cbo_buyer_name" id="cbo_buyer_name"  style="width:130px" class="combo_boxes">
                                    <? if ($buyer_name=="") { ?>
                                    <option value="0">--- All Buyer ---</option>
                                    <?
                                    }
                                    $mod_sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1 and subcontract_party=2 $buyer_name order by buyer_name");
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                        
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[buyer_name]" ?> </option>
                                    <?
                                    }
                                    
                                    ?>
                                </select>
                            
                            </td>
                            
                            <td >                
                                <select name="cbo_team_name" id="cbo_team_name" onchange="load_drop_down(1,this.value,'div_team')" style="width:130px" class="combo_boxes">
                                    <option value="0">--- All Team ---</option>
                                    <?
                                    $mod_sql= mysql_db_query($DB, "select id,team_name from lib_marketing_team where is_deleted=0 and status_active=1 order by team_name");
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["team_name"]){?> selected <?php }?>><? echo "$r_mod[team_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                    
                                </select>
                            
                            </td>
                            <td>
                                <div id="div_team">
                                    <select name="cbo_team_member" id="cbo_team_member"  style="width:130px" class="combo_boxes">
                                        <option value="0">- Select Team Member -</option>
                                        
                                    </select>
                                </div>
                            </td>
                            <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:75px">
								<script type="text/javascript">
                                    $( "#txt_date_from" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> </td>
                            <td><input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:75px">
                                <script type="text/javascript">
                                    $( "#txt_date_to" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> 
                            </td>
                            <td>
                                <select name="cbo_category_by" id="cbo_category_by"  style="width:130px" class="combo_boxes">
                                    <option value="1">Ship Date Wise </option>
                                    <option value="2">PO Rec. Date Wise </option>
                                </select>
                            </td>
                            <td>
                            	<input type="button" name="search" id="search" value="Show" onclick="generate_report_main(13)" style="width:80px" class="formbutton" />
                                <input name="fillter_check" id="fillter_check" type="hidden" >
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td  height="20" valign="bottom" colspan="8" align="left">
                  <?
                    	$c_year= date("Y");
                    	$p_year=$c_year-15;
					
                    
                    ?>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <select name="cbo_year" id="cbo_year"  style="width:80px" class="combo_boxes">
						 
						<?
						for($i=0;$i<21;$i++)
						{
						?>
						<option value=<? echo $p_year+$i;
						if ($c_year==$p_year+$i){?> selected <?php }?>><? echo $p_year+$i; ?> </option>
						<?
						}
						?>
					</select>
               
                    &nbsp;<input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="January" class="month_button"  />
                    &nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="February" class="month_button" />
                    &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="March" class="month_button" />
                    &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="April" class="month_button" />
                    &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" />
                    &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="June" class="month_button" />
                    &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="July" class="month_button" />
                    &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="August" class="month_button" />
                    &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="September" class="month_button" />
                    &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="October" class="month_button" />
                    &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="November" class="month_button" />
                    &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="December" class="month_button" />          
                 </td>
            </tr>            
         </table>
     </fieldset>
     </form>
     <fieldset style="width:996px;">
         <table cellpadding="0" cellspacing="0">
            <tr>
            	<td width="15%"></td>
            	<td width="85%" height="20" id="print_div" align="left">
                </td>
            </tr>
            <tr>
            	<td colspan="16" id="report_container"> 
                </td>
            </tr>
        </table>
    </fieldset>



</div>

</body>
</html>
