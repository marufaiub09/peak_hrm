<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_name=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0) $company_name="and id=".$_SESSION['logic_erp']["company_id"]; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
	
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
    <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>		
	<script>
	
		function generate_report(div,type)
		{
			$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
			
			document.getElementById(div).innerHTML="";
			//document.getElementById('report_container_dtails').innerHTML="";
			var cbo_company_mst=document.getElementById('cbo_company_mst').value;
			var month=document.getElementById('month').value;
			var year=document.getElementById('year').value;
			var month1=document.getElementById('month1').value;
			var year1=document.getElementById('year1').value;	
			var data=cbo_company_mst+"_"+month+"_"+year+"_"+month1+"_"+year1;
			//alert(data);
			if(month==0 || month1==0)
			{
				$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						 $(this).html('Please select date range to search.').addClass('messageboxerror').fadeTo(900,1);
					});	
			}
			else
			{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var data_split=(xmlhttp.responseText).split('####');
					var link_data=data_split[1];
					//alert(link_data);
					document.getElementById('data_panel').innerHTML='<h3>Convert To &nbsp;<a href="includes/tmp_report_file/' + link_data + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a><h3>';
					document.getElementById(div).innerHTML=data_split[0];
					$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
					});
					//alert(xmlhttp.responseText);
					//document.getElementById(div).style.border="1px solid #A5ACB2";
				}
			}
			xmlhttp.open("GET","includes/generate_germents_ex_factory_summary_report.php?data="+data+"&type="+type,true);
			xmlhttp.send();
			}
		}
		
		function change_color(v_id,e_color)
		{
			//alert(document.getElementById(v_id).bgColor);
			 
			if (document.getElementById(v_id).bgColor=="#33CC00")
			{
				document.getElementById(v_id).bgColor=e_color;
			}
			else
			{
				document.getElementById(v_id).bgColor="#33CC00";
			}
		}
		
	</script>
</head>
<body>
<div style="width:1000px">
    <fieldset style="width:100%" >
    <form>
    <center>
    <div style="width:900px" >
        <table width="900px" border="0" cellpadding="0" cellspacing="0">
            <tr class="form_caption">
                <td colspan="13" align="center" height="30" valign="middle"><font size="3"><strong>Monthly Garments Ex-Factory Summary Report</strong></font></td>
            </tr>
            <tr>
                <td colspan="13" align="center" height="30" valign="middle">
                 <div id="messagebox" style="background:#F99" align="center"></div>
                </td>
            </tr>
		</table>
        <table class="rpt_table" width="700"> 
           	<thead>
            	<th width="200">Company</th>
                <th colspan="5">Month Range</th>
                <th><input type="reset" name="res" id="res" value="Reset" style="width:100px" class="formbutton" /></th>
            </thead>
            <tr class="general">
            	<td>
                	<select name="cbo_company_mst" id="cbo_company_mst"  style="width:95%" class="combo_boxes">
						<option value="0">--- All Company ---</option>
						<?
						$mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name"); //where is_deleted=0 and status=0
						$n=mysql_num_rows($mod_sql);
						while ($r_mod=mysql_fetch_array($mod_sql))
						{
							if ($n==1) $company_combo=$r_mod["id"];
						?>
						<option value=<? echo $r_mod["id"];
						if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
						<?
						}
						?>
					</select>
                </td>
                <td width="120">
               <select name='month' id="month"  style='width:100px; ' class="combo_boxes">
                <?            
                $month =array("0","January","February","March","April","May","June","July","August","September","October","November","December");
                for($i=0;$i<=12;$i++)
                {
                if($i==0) 
                {
                echo "<option value='$month[$i]'>Select </option>";
                }
                else 
				{
				echo "<option value='$i'>$month[$i] </option>";	
				}
                }
                ?>
                </select>
                </td>
                <td width="80">
                <select name='year' id="year"  style='width:80px; ' class="combo_boxes">
                
							<?php 
							$c_year=date("Y");
							$s_year=$c_year-25;
							$e_year=$c_year+10;
							
							 for ($year = $s_year; $year <= $e_year; $year++) { ?>
							<option value=<?php echo $year;
							if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
							<?php } ?>
						</select>
                </td>
                <td width="15">To</td>
        
               <td width="120">
                <?
				//$current_month= date('m');
				//$lastmonth=$current_month-1;
				//echo $lastmonth;
				?>
                <select name='month1' id="month1"  style='width:100px;' class="combo_boxes">
                <? 
				$month =array("0","January","February","March","April","May","June","July","August","September","October","November","December");
                for($i=0;$i<=12;$i++)
                {
                if($i==0) 
                {
                echo "<option value='$month[$i]'>Select </option>";
                }
                else 
				{
				echo "<option value='$i'>$month[$i] </option>";	
				}
                }
				?>
                </select>
                </td>
              
                <td width="90">
                		<select name='year1' id="year1"  style='width:80px;' class="combo_boxes">
                		 
							<?php 
							$c_year=date("Y");
							$s_year=$c_year-25;
							$e_year=$c_year+10;
							
							 for ($year = $s_year; $year <= $e_year; $year++) { ?>
							<option value=<?php echo $year;
							if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
							<?php } ?>
						</select>
                </td>
                <td width="5%" >
                <input name="button" type="button" style="width:100px" class="formbutton" value="Show" onclick="generate_report('report_container','germents_factory_summary');">
                </td>
            </tr>
          </table>
    </div>
    </center>
    </form>
    </fieldset>
    <fieldset style="width:100%" >
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="10" height="20" id="data_panel" align="center"></td>
            </tr>
            <tr>
                <td colspan="10" id="report_container"></td>
            </tr>
        </table>
    </fieldset>
</div>
</body>
</html>