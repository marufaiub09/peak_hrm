<link href="../../css/style_common.css" rel="stylesheet" type="text/css" />
<script src="includes/ajax_submit_report.js" type="text/javascript"></script>
<link type="text/css" href="../../juery/development-bundle/themes/base/jquery.ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="../../juery/development-bundle/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../juery/lib.js"></script>
	<script type="text/javascript" src="../../juery/development-bundle/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="../../juery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="../../juery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="../../juery/development-bundle/ui/jquery.ui.autocomplete.js"></script>
	<script type="text/javascript" src="../../juery/development-bundle/ui/jquery.ui.position.js"></script>


<body>

    <div id="page-wrap">
    
    <h1>Send Mail </h1>

	<form  action="javascript:fnc_send_mail()"  >
	<div align="center" id="messagebox"></div>
	   
	
		<div class="rowElem">
            <label for="req-name"> Name*:</label>
            <input type="text" id="txt_req_name" name="txt_req_name" class="text_boxes" minlength="2" value="" />
        </div>
        
        <div class="rowElem">
            <label for="req-email"> Email: &nbsp;    </label>
            <input type="text" name="txt_email" id="txt_email"  class="text_boxes" value="" />
        </div>
		
		   <div class="rowElem">
            <label for="req-email"> Subject:     </label>
            <input type="text" name="txt_subject"  id="txt_subject" class="text_boxes" value=""  style="width:200px"/>
        </div>
        
        <div class="rowElem">
		    <label></label><div id="changeTypeArea">
		      <div class="clear"></div>
    			
    			</div>
        </div>
        
        <div class="rowElem">
            <label for="URL-main"></label>
        </div>
        
		<div class="rowElem">
		  <label for="mult"></label>
		</div>
        
        <div id="addURLSArea"></div>
        
        <div id="curTextArea">
   		  <div class="rowElem">
   		    <label for="curText"></label>
    		</div>
        </div>
		
		<div class="rowElem" id="newTextArea">
		  <label for="newText">Message:</label>
		  <textarea cols="40" rows="8" name="txt_message" id="txt_message" class="text_area" minlength="2" style="height:170px"></textarea>
        </div>

		<div class="rowElem">
		  <label> &nbsp; </label>
		  <input type="submit" class="formbutton" value="Send Mail" />
        </div>
        
      <div class="rowElem">
	    <label></label>
        </div>
        				
	</form>
	
	</div>
	
	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-68528-29");
	pageTracker._initData();
	pageTracker._trackPageview();
	</script>

</body>
