<? 
include('../../includes/common.php');
include('../../includes/array_function.php');
extract($_GET);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<link href="../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../inventory/reports/includes/functions.js"></script>
<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>

<script>

function showResult_job(str,type,div)
{
	//alert(str+"------"+type);
	var txt_date_from="";
	var txt_date_to="";
	txt_date_from="<? echo $txt_date_from; ?>";
	txt_date_to="<? echo $txt_date_to; ?>";
if (str.length==0)
  {
  document.getElementById(div).innerHTML="";
  document.getElementById(div).style.border="0px";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
        document.getElementById(div).innerHTML=xmlhttp.responseText;   
    }
  }
// alert( type );
xmlhttp.open("GET","includes/generate_post_cost_analysis_report.php?data=" + trim( str ) + "&type=" + type+'&txt_date_from='+txt_date_from+'&txt_date_to='+txt_date_to,true);
xmlhttp.send();

}

var selected_id = new Array, selected_name = new Array();
		
		function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}


</script>

</head>

<body>
<div>
<form name="search_order_frm"  id="search_order_frm">
	<fieldset style="width:550px">
	<table width="300" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
		<thead>
			<th>Search By</th>
            <th>Search</th>
            <th>
            	<input type="reset" name="res" id="res" value="Reset" style="width:100px" class="formbutton" />
                <input type="hidden" readonly="readonly" name="txt_selected_id" id="txt_selected_id" /> 
                <input type="hidden" name="txt_selected" id="txt_selected" />
            </th>
        </thead>
        <tr class="general">
        	<td>
            	<select name="search_by" id="search_by" class="combo_boxes" style="width:160px"> 
                	<option value="1">Job No.</option>
                </select>
            </td>     
            <td><input type="text" value="" name="search" id="search" class="text_boxes" style="width:150px"  autocomplete=off /> </td> 
            <td><input type="button" name="show" onclick="showResult_job(document.getElementById('search').value+'_'+document.getElementById('search_by').value+'_'+<? echo $com_name; ?>+'_'+<? echo $buyer_id; ?>,'search_job_info','search_div_conf')" class="formbutton" value="Show" style="width:100px" />
			</td>
         </tr>   
	</table>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td>
				<div style="width:550px;"  id="search_div_conf" align="left">
					
				</div>
			</td>
        </tr>
    </table>
    <table width="100%" cellspacing="2" cellpadding="0" border="0">
        <tr>
			<td align="center" height="30" valign="bottom">
					<div style="width:100%"> 
							<div style="width:50%; float:left" align="left">
								<input type="checkbox" name="check_all" id="check_all" onclick="check_all_data()" /> Check / Uncheck All
							</div>
							<div style="width:50%; float:left" align="left">
							<input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close"  style="width:100px"/>
							</div>
					</div>
			</td>
		</tr>
	</table>
	</fieldset>
	</form>
</div>
</body>
</html>