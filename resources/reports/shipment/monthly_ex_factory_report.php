<?php
session_start();
include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_name=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0) $company_name="and id=".$_SESSION['logic_erp']["company_id"]; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
	<script src="includes/includes/ajax_submit.js" type="text/javascript"></script>
	<script src="includes/includes/functions.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
	
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>



	<script>
		function set_date_range(mon)
	{
		 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = document.getElementById('cbo_year').value;
		
		var start_date="01" + "-" + mon  + "-" + year;
		var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
		
		document.getElementById('txt_date_from').value=start_date;
		document.getElementById('txt_date_to').value=to_date;
		 //alert(to_date);
		 //generate_report('report_container');
	}
		
   		function daysInMonth(month,year) 
		{
			return new Date(year, month, 0).getDate();
		}
		
		function generate_report(div,report_div,stype)
		{//alert(stype);
			$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
			
			if (stype==1) // main call
			{
				document.getElementById(div).innerHTML="";
				
				var cbo_company_mst=document.getElementById('cbo_company_mst').value;
				var txt_date_from=document.getElementById('txt_date_from').value;
				var txt_date_to=document.getElementById('txt_date_to').value;
				
				var data=cbo_company_mst+"_"+txt_date_from+"_"+txt_date_to+"__";
			}
			else
			{
			
				var cbo_company_mst=document.getElementById('cbo_company_mst').value;
				var txt_date_from=document.getElementById('txt_date_from').value;
				var txt_date_to=document.getElementById('txt_date_to').value;//............New
				var txt_order_number=document.getElementById('txt_order_number').value;
				var txt_buyer_name=document.getElementById('txt_buyer_name').value;
				var txt_style_name=document.getElementById('txt_style_name').value;
				var txt_item_name=document.getElementById('txt_item_name').value;
				var txt_ship_date=document.getElementById('txt_ship_date').value;
				//var txt_ex_fac_date=document.getElementById('txt_ex_fac_date').value;
				var txt_po_qty=document.getElementById('txt_po_qty').value;
				//alert (txt_job_number,txt_ship_date,txt_order_qty);
				var data=cbo_company_mst+"_"+txt_date_from+"_"+txt_date_to+"_"+txt_order_number+"_"+txt_buyer_name+"_"+txt_style_name+"_"+txt_item_name+"_"+txt_ship_date+"_"+txt_po_qty;
				//alert(data);
			}
			
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
				
			if (cbo_company_mst==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
				return false; 
			}
				
			if (txt_date_from==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select From Date....').fadeIn(1000);
				return false; 
			}
					
			else if (txt_date_to==0)
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Please Select To Date....').fadeIn(1000);
				return false;
			}
				
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var data_split=(xmlhttp.responseText).split('####');
					var link_data=data_split[1];
	//echo "<div align='center'><h2>Convert To &nbsp;<a href='includes/tmp_report_file/".$name."'><img src='includes/logo/excel-logo.png' name='logo_id' id='logo_id' /></a></h2></div>";
					document.getElementById(report_div).innerHTML='<h3>Convert To &nbsp;<a href="includes/tmp_report_file/' + link_data + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a><h3>';
					document.getElementById(div).innerHTML=data_split[0];
					$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
						 set_percentage();
						 set_percentage_exf();
						 
					});
					// alert(xmlhttp.responseText);
					//document.getElementById(div).style.border="1px solid #A5ACB2";
				}
			}
			xmlhttp.open("GET","includes/monthly_ex_factory_report_details.php?data="+data+"&type=monthly_ex_factory_report_details",true);
			xmlhttp.send();
			
		}
		function set_percentage()
		{//alert("sdfsd"); 
			var len=(document.getElementById('total_i').value)-1; 
			
			var total_val=document.getElementById('total_val').innerHTML;
			var tot=0;
			var row_tot=0;
			for (j=1;j<=len;j++)
			{
				row_tot=((document.getElementById('value_'+j).innerHTML*100)/total_val).toFixed(2);
				tot=(tot*1)+(row_tot*1);
				document.getElementById('percentage_'+j).innerHTML=row_tot;
			}
			document.getElementById('total_i_cont').innerHTML=tot.toFixed(2);
		}
		
		function set_percentage_exf()
		{
			var len=(document.getElementById('total_i').value)-1;  
			var total_val=document.getElementById('total_val_ex').innerHTML;
			var tot=0; 
			var row_tot=0;
			for (j=1;j<=len;j++)
			{
				row_tot=((document.getElementById('value__ex_'+j).innerHTML*100)/total_val).toFixed(2);
				tot=(tot*1)+(row_tot*1);
				document.getElementById('percentage__ex_'+j).innerHTML=row_tot;
				
			}
			document.getElementById('total_i_cont_ex').innerHTML=tot.toFixed(2);
		}
		function show_inner_filter(e)
		{
			//alert(e);
			if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
			if (unicode==13 )
			{
				generate_report('report_container','data_panel',2);
			}
		}
		function change_color(v_id,e_color)
		{
			//alert(document.getElementById(v_id).bgColor);
			 
			if (document.getElementById(v_id).bgColor=="#33CC00")
			{
				document.getElementById(v_id).bgColor=e_color;
			}
			else
			{
				document.getElementById(v_id).bgColor="#33CC00";
			}
		}
	</script>
</head>
<body>
<div style="width:90%">
	<fieldset style="width:100%" >
        <table width="100%" cellpadding="0" cellspacing="2" align="center" id="table3">
        	<tr class="form_caption">
                <td align="center" height="30" valign="middle"><font size="3"><strong>Monthly Ex-Factory Report</strong></font></td>
            </tr>
            <tr>
                <td align="center" height="30" valign="middle">
                 <div id="messagebox" style="background:#F99" align="center"></div>
                </td>
            </tr>
           
            <tr>
            	<td align="center">
                	<table class="rpt_table" width="700">
                    	<thead>
                        	<th width="30%">Comapny</th>
                            <th width="40%" colspan="2">Date</th>
                        </thead>
                        <tr class="general">
                        	<td>
                                <select name="cbo_company_mst" id="cbo_company_mst"  style="width:95%" class="combo_boxes">
                                <option value="0">--- Select Company ---</option>
                                <?
                                $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name"); //where is_deleted=0 and status=0
                                $n=mysql_num_rows($mod_sql);
                                while ($r_mod=mysql_fetch_array($mod_sql))
                                {
                                    if ($n==1) $company_combo=$r_mod["id"];
                                ?>
                                <option value=<? echo $r_mod["id"];
                                if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                                <?
                                }
                                ?>
                                </select>
                            </td>
                            <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px">
								<script type="text/javascript">
                                    $( "#txt_date_from" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> </td>
                            <td><input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px">
                                <script type="text/javascript">
                                    $( "#txt_date_to" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> 
                            </td>
                            <td width="5%" align="center"><input type="button" name="show" id="show" onclick="generate_report('report_container','data_panel',1);" class="formbutton" style="width:80px;" value="Show" /></td>
                        </tr>
                     </table>
                 </td>
             </tr>
             <tr>
            	<td  height="20" valign="bottom" colspan="8" align="left">
                  <?
                    	$c_year= date("Y");
                    	$p_year=$c_year-15;
					
                    
                    ?>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <select name="cbo_year" id="cbo_year"  style="width:80px" class="combo_boxes">
						 
						<?
						for($i=0;$i<21;$i++)
						{
						?>
						<option value=<? echo $p_year+$i;
						if ($c_year==$p_year+$i){?> selected <?php }?>><? echo $p_year+$i; ?> </option>
						<?
						}
						?>
					</select>
               
                    &nbsp;<input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="January" class="month_button" />
                    &nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="February" class="month_button" />
                    &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="March" class="month_button" />
                    &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="April" class="month_button" />
                    &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" />
                    &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="June" class="month_button" />
                    &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="July" class="month_button" />
                    &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="August" class="month_button" />
                    &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="September" class="month_button" />
                    &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="October" class="month_button" />
                    &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="November" class="month_button" />
                    &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="December" class="month_button" />          
                 </td>
            </tr> 
             
        </table>
    </fieldset>
    <fieldset style="width:100%" >
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="8" height="10" id="data_panel" align="center"></td>
            </tr>
            <tr>
                <td colspan="8" id="report_container" align="left"></td>
            </tr>
        </table>
    </fieldset>
</div>
</body>
</html>