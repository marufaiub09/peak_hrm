<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	 
	<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
     
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    
	<script>
	function set_date_range(mon)
	{
		 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
			var year = document.getElementById('cbo_year').value;
		
		var start_date="01" + "-" + mon  + "-" + year;
		var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
		
		document.getElementById('txt_date_from').value=start_date;
		document.getElementById('txt_date_to').value=to_date;
		 //alert(to_date);
		// generate_report('report_container',1);
	}
	
   	function daysInMonth(month,year) 
   	{
    	return new Date(year, month, 0).getDate();
	}
	
	
function generate_report(div,stype)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	if (stype==1) // main call
	{
		document.getElementById(div).innerHTML="";
		
		var cbo_company_name=document.getElementById('cbo_company_mst').value;
		var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
		var txt_style_name=document.getElementById('txt_style_name').value;
		var txt_date_from=document.getElementById('txt_date_from').value;
		var txt_date_to=document.getElementById('txt_date_to').value;
		var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_style_name+"_"+txt_date_from+"_"+txt_date_to+"__";
	}
	else
	{
		//document.getElementById(div).innerHTML="";
		
		var cbo_company_name=document.getElementById('cbo_company_mst').value;
		var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
		var txt_style_name=document.getElementById('txt_style_name').value;
		var txt_date_from=document.getElementById('txt_date_from').value;
		var txt_date_to=document.getElementById('txt_date_to').value;
		var txt_job_number=document.getElementById('txt_job_number').value;
		var txt_style_number=document.getElementById('txt_style_number').value;
		var txt_order_qty=document.getElementById('txt_order_qty').value;
		var txt_ship_date=document.getElementById('txt_ship_date').value;
		var txt_agent_name=document.getElementById('txt_agent_name').value;
		//var txt_days_hand=document.getElementById('txt_days_hand').value;
		//alert (txt_job_number,txt_ship_date,txt_order_qty);
		var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_style_name+"_"+txt_date_from+"_"+txt_date_to+"_"+txt_job_number+"_"+txt_style_number+"_"+txt_order_qty+"_"+txt_ship_date+"_"+txt_agent_name;
	
	}
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (cbo_company_name==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
		return false; }
	/*	
	else if (cbo_buyer_name==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Buyer Name....').fadeIn(1000);
		return false; }	
	*/	
	if (txt_date_from==0||txt_date_to==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select Date Range....').fadeIn(1000);
		return false; }
		
		
	
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		//document.getElementById(div).innerHTML=""
			//document.getElementById(div).innerHTML=xmlhttp.responseText;
			var data_split=(xmlhttp.responseText).split('####');
				var link_data=data_split[1];
				//document.getElementById(report_div).innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
				//document.getElementById('print_div').innerHTML='<h3>Convert To &nbsp;<a href="includes/tmp_report_file/' + link_data + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a><h3>';
				//document.getElementById(div).innerHTML=data_split[0];
			
			var response=(xmlhttp.responseText).split('####');	
			document.getElementById(div).innerHTML=response[0];
			document.getElementById('print_div').innerHTML='<h3>Convert To &nbsp;<a href="'+response[1]+'" > <img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>';
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET","includes/generate_report_order.php?data="+data+"&type=shipment_date_wise_progress_report",true);
	xmlhttp.send();
}
	
	function show_progress_report_details(type,job_number)
	{
		 emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'shipdate_work_progress_report_popup.php?type='+type+'&job_number='+job_number, 'Work Progress Report Details', 'width=730px,height=400px,center=1,resize=0,scrolling=0','../')
		//emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'shipdate_work_progress_report_popup.php?type='+type+'&job_number='+job_number, 'Export LC Details', 'width=535px,height=320px,center=1,resize=0,scrolling=0','../')
	}
	
	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
 
	function show_inner_filter(e)
    {
    	if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
 		if (unicode==13 )
		{
         	generate_report('report_container',2);
		}
    }
	 
	</script>
    
    
</head>

<?php
session_start();

//echo $buyer_name."sjhjhj".$company_name;

include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');

if ($_SESSION['logic_erp']["data_level_secured"]==1)
{
	 if($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_cond=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_cond="";
	 if($_SESSION['logic_erp']["company_id"]!=0) $company_cond=" and id=".$_SESSION['logic_erp']["company_id"]; else $company_cond="";
}
else
{
	$buyer_cond="";	$company_cond="";
}

?>

<body>
<div style="width:950px">
	<fieldset style="width:100%">
    	<table width="100%" cellpadding="0" cellspacing="2">
        	<tr class="form_caption">
            	<td colspan="10" align="center" height="50" valign="middle"><font size="3">Shipment Date Wise Work Progress Report</font></td>
            </tr>
            <tr>
            	<td colspan="10" align="center" height="30" valign="middle">
                	<div id="messagebox" style="background:#F99" align="center"></div>
                </td>
            </tr>
            <tr>
            	<td align="center">
                	<table class="rpt_table" width="900">
                    	<thead>
                        	<th width="15%">Company</th>
                            <th width="15%">Buyer</th>
                            <th width="15%">Style</th>
                            <th width="20%" colspan="2">Date</th>
                        </thead>
                        <tr class="general">
                        	<td>
                                <select name="cbo_company_mst" id="cbo_company_mst"  style="width:95%" class="combo_boxes">
                                    <? if ($company_name=="") { ?>
                                    <option value="0">--- All Company ---</option>
                                    <?
                                    }
                                    $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_cond order by company_name"); //where is_deleted=0 and status=0
                                    $n=mysql_num_rows($mod_sql);
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                        if ($n==1) $company_combo=$r_mod["id"];
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </td>
                             <td>
                                <select name="cbo_buyer_name" id="cbo_buyer_name"  style="width:95%" class="combo_boxes">
                                    <? if ($buyer_name=="") { ?>
                                    <option value="0">--- All Buyer ---</option>
                                    <?
                                    }
                                    $mod_sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1 and subcontract_party=2 $buyer_cond order by buyer_name");
									 $n=mysql_num_rows($mod_sql);
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                      if ($n==1) $company_combo=$r_mod["id"];   
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[buyer_name]" ?> </option>
                                    <?
                                    }
                                    
                                    ?>
                                </select>
                            
                            </td>
                                                       
                            <td>
                                <input type="text" name="txt_style_name" id="txt_style_name" disabled="disabled" class="text_boxes" style="width:90%" />
                            </td>
                            <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px">
								<script type="text/javascript">
                                    $( "#txt_date_from" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> </td>
                            <td><input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px">
                                <script type="text/javascript">
                                    $( "#txt_date_to" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> 
                            </td>
                            
                            <td width="5%">
                            	<input type="button" name="search" id="search" value="Show" onclick="generate_report('report_container',1)" style="width:70px" class="formbutton" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="10"></tr>
            <tr>
            	<td  height="20" valign="bottom" colspan="8" align="left">
                  <?
                    	$c_year= date("Y");
                    	$p_year=$c_year-15;
					
                    
                    ?>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <select name="cbo_year" id="cbo_year"  style="width:80px" class="combo_boxes">
						 
						<?
						for($i=0;$i<21;$i++)
						{
						?>
						<option value=<? echo $p_year+$i;
						if ($c_year==$p_year+$i){?> selected <?php }?>><? echo $p_year+$i; ?> </option>
						<?
						}
						?>
					</select>
               
                    &nbsp;<input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="January" class="month_button" />
                    &nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="February" class="month_button" />
                    &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="March" class="month_button" />
                    &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="April" class="month_button" />
                    &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" />
                    &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="June" class="month_button" />
                    &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="July" class="month_button" />
                    &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="August" class="month_button" />
                    &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="September" class="month_button" />
                    &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="October" class="month_button" />
                    &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="November" class="month_button" />
                    &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="December" class="month_button" />          
                 </td>
            </tr>            
        </table>
   </fieldset>
   <fieldset style="width:100%">
         <table cellpadding="0" cellspacing="2">
            <tr>
            	<td colspan="10" height="20" id="print_div" align="left" style="font-weight:bold;">
                
                </td>
            </tr>
            <tr>
            	<td colspan="10" id="report_container"> 
                 
                </td>
            </tr>
        </table>
    	
    </fieldset>



</div>

</body>
</html>
