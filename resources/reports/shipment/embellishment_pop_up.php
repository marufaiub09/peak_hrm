<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript">
	//Do not change 
	function createObject() {
	var request_type;
	var browser = navigator.appName;
	if(browser == "Microsoft Internet Explorer"){
	request_type = new ActiveXObject("Microsoft.XMLHTTP");
	}else{
	request_type = new XMLHttpRequest();
	}
	return request_type;
	}

	var http = createObject();
	
	function generate_worder_report(str,cbo_company_name,cbo_supplier_id,cbo_item_category,txt_wo_number)
	{
		nocache = Math.random();
		
		http.open('get','../../order/save_update_common.php?action=generate_worder_report_pdf'+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_supplier_id='+cbo_supplier_id+
					'&txt_wo_number='+txt_wo_number+
					'&cbo_item_category='+cbo_item_category+
					'&str='+str+
					'&nocache ='+nocache);
		http.onreadystatechange = generate_worder_report_reply;
		http.send(null);
		
	}
	
	function generate_worder_report_reply() 
	{
		if(http.readyState == 4)
		{ 		
			var response =  http.responseText.split('####');
			 //alert( http.responseText);		
			if(response[2]==1){			
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(response[0]);
				d.close();	
			}
		}
	}	
	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	</script>
</head>
<body>
<?php

include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
//Supplier Details
$sql = "SELECT * FROM lib_supplier WHERE is_deleted = 0 ORDER BY supplier_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$supplier_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$supplier_details[$row['id']] = $row['supplier_name'];
}
// Item Details
$sqlt = "SELECT * FROM lib_trim WHERE is_deleted = 0 ORDER BY item_name ASC";
$resultt = mysql_query( $sqlt ) or die( $sqlt . "<br />" . mysql_error() );

$item_name_details = array();
$item_conver_factor = array();
while( $row_trim = mysql_fetch_assoc( $resultt ) ) {
	$item_name_details[$row_trim['id']] = $row_trim['item_name'];
	$item_conver_factor[$row_trim['id']] = $row_trim['conversion_factor'];
}
//Currency Details
$sql = "SELECT * FROM lib_list_currency ORDER BY currency_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$currency_details = array();
$conversion_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$currency_details[$row['id']] = $row['currency_name'];
	$conversion_details[$row['id']] = $row['is_domestic'];
}
//UOM  Details
$sql = "SELECT * FROM lib_unit WHERE is_deleted = 0 ORDER BY unit_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$uom_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$uom_details[$row['id']] = $row['unit_name'];
}


extract($_GET);
if($type=="Booking_info")
{
?>
<div align="center"> 
	<fieldset style="width:550px">
	<div style="width:500px">
        <table border="1"  align="center" class="rpt_table" rules="all" width="100%">
            <thead>
                <th width="40">SL</th>
                <th width="80">WO No.</th>
                <th width="80">WO Date</th>
                <th width="100">WO Qnty</th>
                <th>Supplier</th>
            </thead>
        </table>
     </div>
     <div style="width:500px; overflow-y:scroll; max-height:250px">
        <table border="1"  align="center" class="rpt_table" rules="all" width="100%">
<?	
			$sql="select a.wo_number, a.company_name, a.wo_date, a.supplier_id, b.supplier_order_quantity as booking_qnty, b.uom from wo_details_info a, wo_order_info b where a.wo_number=b.wo_number and b.wo_po_break_down_id='$po_id' and a.item_category=12 and b.service_type='$service_type' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 order by b.wo_number";
			$result=mysql_query($sql);
			$i=1; $total_wo_qnty=0;
			while ($row=mysql_fetch_array($result)) 			
			{
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";	
				if($uom_details[$row[uom]]=="Dzn")
				{
					$conv_qnty=12;
				}
				else
				{
					$conv_qnty=1;	
				}
				$wo_qnty=$row[booking_qnty]*$conv_qnty;
			?>
            <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                <td width="40"><? echo $i; ?></td>
                <td width="80"><a href="##" onclick="generate_worder_report(1,<? echo $row[company_name]; ?>,<? echo $row[supplier_id]; ?>,12,<? echo $row[wo_number]; ?>)"><? echo $row[wo_number]; ?></a></td>
                <td width="80" align="center"><? echo convert_to_mysql_date($row[wo_date]); ?></td>
                <td width="100" align="right"><? echo number_format($wo_qnty,2);$total_wo_qnty+=$wo_qnty; ?></td>
                <td><? echo $supplier_details[$row[supplier_id]]; ?></td>  
            </tr>
			<?
			$i++;
			}
            ?>
            <tfoot>
                <th align="right" colspan="3">Total</th>
                <th align="right"> <? echo number_format($total_wo_qnty,2); ?></th>
                <th>&nbsp;</th>
            </tfoot>
        </table>
    </div>
	</fieldset>
</div>
<?
}
if($type=="Booking_value_info")
{
?>
<div align="center"> 
	<fieldset style="width:550px">
	<div style="width:550px">
        <table border="1"  align="center" class="rpt_table" rules="all" width="100%">
            <thead>
                <th width="35">SL</th>
                <th width="60">WO No.</th>
                <th width="75">WO Date</th>
                <th width="70">Currency</th>
                <th width="100">Amount (Taka)</th>
                <th width="70">Conversion rate</th>
                <th>Amount (USD)</th>
            </thead>
        </table>
     </div>
     <div style="width:550px; overflow-y:scroll; max-height:250px">
        <table border="1" align="center" class="rpt_table" rules="all" width="100%">
<?	
			$sql="select a.wo_number, a.company_name, a.wo_date, sum(b.amount) as value, a.currency_id, a.supplier_id, a.conversion_rate from wo_details_info a, wo_order_info b where a.wo_number=b.wo_number and b.wo_po_break_down_id='$po_id' and a.item_category=12 and b.service_type='$service_type' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.wo_number order by a.wo_number";
			$result=mysql_query($sql);
			$i=1; $total_wo_value=0;
			while ($row=mysql_fetch_array($result)) 			
			{
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";	
					$wo_qnty=$row[booking_qnty]*$item_conver_factor[$item_id];
			?>
            <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                <td width="35"><? echo $i; ?></td>
                <td width="60"><a href="##" onclick="generate_worder_report(1,<? echo $row[company_name]; ?>,<? echo $row[supplier_id]; ?>,12,<? echo $row[wo_number]; ?>)"><? echo $row[wo_number]; ?></a></td>
                <td width="75" align="center"><? echo convert_to_mysql_date($row[wo_date]); ?></td>
                <td width="70" align="center"><? echo $currency_details[$row[currency_id]]; ?></td>
                <td width="100" align="right">
				<? 
					$amnt_usd=0; $conversion_rate='';
					if($conversion_details[$row[currency_id]]==1)
					{
						echo number_format($row[value],2); 
						$amnt_usd=$row[value]/$row[conversion_rate];
						$conversion_rate=$row[conversion_rate];
					}
					else
					{
						echo "&nbsp;";
						$amnt_usd=$row[value];
						$conversion_rate="&nbsp;";
					}
				?>
                </td>
                <td width="70" align="right"><? echo number_format($conversion_rate,2); ?></td>
                <td align="right"><? echo number_format($amnt_usd,2);$total_wo_value+=$amnt_usd; ?></td>  
            </tr>
			<?
			$i++;
			}
            ?>
            <tfoot>
                <th align="right" colspan="6">Total</th>
                <th align="right"> <? echo number_format($total_wo_value,2); ?></th>
            </tfoot>
        </table>
    </div>
	</fieldset>
</div>
<?
}

?>
</body>
</html>
