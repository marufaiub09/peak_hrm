<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	<script src="includes/ajax_submit.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	<script>
function generate_report(div)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	document.getElementById(div).innerHTML="";
	var txt_style_ref=document.getElementById('txt_style_ref').value;
	var cbo_company_name=document.getElementById('cbo_company_name').value;
    var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	//var data=txt_style_ref;
	//alert(txt_style_ref);
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
		}
  	}
  	xmlhttp.open("GET","includes/generate_color_and_size_breakdown.php?txt_style_ref="+txt_style_ref+"&cbo_company_name="+cbo_company_name+"&cbo_buyer_name="+cbo_buyer_name+"&type=style_ref_search",true);
	xmlhttp.send();
}
function js_set_value(style)
{
	document.getElementById('hidd_style').value=style;
}
</script>
</head>

<?php

include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
extract ( $_GET );
extract ( $_POST );
?>

<body>
        <div style="width:600px">
                <fieldset>
                        <table width="100%" cellpadding="0" cellspacing="2" border="0">
                                <tr>
                                <td>
                                Search Style Ref. 
                                <input type="text" name="txt_style_ref" id="txt_style_ref" class="text_boxes" width="100px" onkeyup="generate_report('report_container')"/>
                                </td>
                                <td>
                               Companay
                                <input type="text" name="cbo_company_name" id="cbo_company_name" class="text_boxes" width="100px" value="<? echo $cbo_company_name;?>"/>
                                </td>
                                <td>
                               Buyer
                                <input type="text" name="cbo_buyer_name" id="cbo_buyer_name" class="text_boxes" width="100px" value="<? echo $cbo_buyer_name;?>"/>
                                </td>
                                 <td>
                                Style
                                <input type="text" name="hidd_style" id="hidd_style" class="text_boxes" width="100px" />
                                </td>
                                </tr>
                        </table>
                </fieldset>
                
                <fieldset>
                        <table>
                                <tr>
                                <td id="report_container"> 
                                </td>
                                </tr>
                        </table>
                </fieldset>
        </div>
</body>
</html>
