<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>	
    
	<script>
function generate_report(div)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	
	    document.getElementById(div).innerHTML="";
		document.getElementById('report_container_dtails').innerHTML="";
		var month=document.getElementById('month').value;
		var year=document.getElementById('year').value;
		var month1=document.getElementById('month1').value;
		var year1=document.getElementById('year1').value;
		var cbo_company_mst=document.getElementById('cbo_company_mst').value;	

		var data=month+"_"+year+"_"+month1+"_"+year1+"_"+cbo_company_mst;
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	
	if (month==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select From Month....').fadeIn(1000);
		return false; }
		
	else if (month1==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select To Month....').fadeIn(1000);
		return false; }	
		
	
	
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		//alert(xmlhttp.responseText);
			document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			
		}
  	}
  	xmlhttp.open("GET","includes/generate_monthly_summary_report.php?data="+data+"&type=confirmed_order",true);
	xmlhttp.send();
}

function generate_report2(div)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	
	    document.getElementById(div).innerHTML="";
		document.getElementById('report_container_dtails').innerHTML="";
		var txt_date_from=document.getElementById('txt_date_from').value;
		var txt_date_to=document.getElementById('txt_date_to').value;
	    var data=txt_date_from+"_"+txt_date_to;
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET","includes/generate_count_wise_yarn_procurement_projection_report.php?data="+data+"&type=count_wise_yarn_procurement_projection_report",true);
	xmlhttp.send();
}

function generate_report_details(div,yarn_count_id,type)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	document.getElementById(div).innerHTML="";
	var month=document.getElementById('month').value;
	var year=document.getElementById('year').value;
    var month1=document.getElementById('month1').value;
	var year1=document.getElementById('year1').value;
	var cbo_company_mst=document.getElementById('cbo_company_mst').value;	

	var data=month+"_"+year+"_"+month1+"_"+year1+"_"+cbo_company_mst;
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
		}
  	}
  	xmlhttp.open("GET","includes/generate_count_wise_yarn_procurement_projection_report.php?data="+data+"&type="+type+"&yarn_count_id="+yarn_count_id,true);
	xmlhttp.send();
}


/*function generate_report_details2(div,type)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	alert (div);
	alert (type);
	document.getElementById(div).innerHTML="";
	var txt_date_from=document.getElementById('txt_date_from').value;
	var txt_date_to=document.getElementById('txt_date_to').value;
	var data=txt_date_from+"_"+txt_date_to;
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
		}
  	}
  	xmlhttp.open("GET","includes/generate_count_wise_yarn_procurement_projection_report.php?data="+data+"&type="+type,true);
	xmlhttp.send();
}*/

function openmypage(yarn_count_id,pipe_line_details)
{
alert (yarn_count_id);	
alert (pipe_line_details);
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'includes/generate_count_wise_yarn_procurement_projection_report.php?yarn_count_id='+yarn_count_id+'&type='+pipe_line_details, 'Pop Up', 'width=700px,height=450px,center=1,resize=0,scrolling=0',' ../')
}
	
	function change_color(v_id,e_color)
	{//alert("sdfsd");
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	</script>
</head>

<?php
session_start();
include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
if ($_SESSION['logic_erp']["data_level_secured"]==1)
{
	 if($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_cond=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_cond="";
	if($_SESSION['logic_erp']["company_id"]!=0) $company_cond=" and id=".$_SESSION['logic_erp']["company_id"]; else $company_cond="";
}
else
{
	$buyer_cond="";	$company_cond="";
}
?>

<body>
<div style="width:1040px">
	<fieldset style="width:100%">
    	<table width="100%" cellpadding="0" cellspacing="2" border="0">
        	<tr class="form_caption">
            	<td width="1000" align="center" height="20" valign="middle"><font size="3">Monthly Summary Report</font></td>
            </tr>
            <tr>
            	<td width="1000" align="center" height="20" valign="middle">
                	<div id="messagebox" style="background:#F99" align="center"></div>
               </td>
            </tr>
         
          <tr><td >
           <table class="rpt_table" width="700" align="center"> 
           
           	<thead>
            	<th width="200">Company</th>
                <th colspan="5">Month Range</th>
            </thead>
            <tr class="general">
            	<td>
                	<select name="cbo_company_mst" id="cbo_company_mst"  style="width:95%" class="combo_boxes">
						<option value="0">--- All Company ---</option>
						<?
						$mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_cond order by company_name"); //where is_deleted=0 and status=0
						$n=mysql_num_rows($mod_sql);
						while ($r_mod=mysql_fetch_array($mod_sql))
						{
							if ($n==1) $company_combo=$r_mod["id"];
						?>
						<option value=<? echo $r_mod["id"];
						if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
						<?
						}
						?>
					</select>
                </td>
                <td width="120">
               <select name='month' id="month"  style='width:100px; ' class="combo_boxes">
                <?            
                $month =array("0","January","February","March","April","May","June","July","August","September","October","November","December");
                for($i=0;$i<=12;$i++)
                {
                if($i==0) 
                {
                echo "<option value='$month[$i]'>Select </option>";
                }
                else 
				{
				echo "<option value='$i'>$month[$i] </option>";	
				}
                }
                ?>
                </select>
                </td>
                <td width="80">
                <select name='year' id="year"  style='width:80px; ' class="combo_boxes">
                
							<?php 
							$c_year=date("Y");
							$s_year=$c_year-25;
							$e_year=$c_year+10;
							
							 for ($year = $s_year; $year <= $e_year; $year++) { ?>
							<option value=<?php echo $year;
							if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
							<?php } ?>
						</select>
                </td>
                <td width="15">To</td>
        
               <td width="120">
                <?
				//$current_month= date('m');
				//$lastmonth=$current_month-1;
				//echo $lastmonth;
				?>
                <select name='month1' id="month1"  style='width:100px;' class="combo_boxes">
                <? 
				$month =array("0","January","February","March","April","May","June","July","August","September","October","November","December");
                for($i=0;$i<=12;$i++)
                {
                if($i==0) 
                {
                echo "<option value='$month[$i]'>Select </option>";
                }
                else 
				{
				echo "<option value='$i'>$month[$i] </option>";	
				}
                }
				?>
                </select>
                </td>
              
                <td width="90">
                		<select name='year1' id="year1"  style='width:80px;' class="combo_boxes">
                		 
							<?php 
							$c_year=date("Y");
							$s_year=$c_year-25;
							$e_year=$c_year+10;
							
							 for ($year = $s_year; $year <= $e_year; $year++) { ?>
							<option value=<?php echo $year;
							if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
							<?php } ?>
						</select>
                </td>
                <td width="5%">
                <input name="button" type="button" style="width:80px" class="formbutton" value="Show" onclick="generate_report('report_container')">
                </td>
            </tr>
          
      
           </td></tr></table> 
           </table>          
           </fieldset>
           
            <fieldset style="width:100%">
               <table cellpadding="0" cellspacing="2">
                  <tr>
                 		 <td colspan="10" id="report_container" valign="top"> </td>
                  </tr>
              </table>
               <table cellpadding="0" cellspacing="2" width="100%">
                  <tr>
                 		 <td colspan="10" id="report_container_dtails"> </td>
                  </tr>
              </table>
          </fieldset>
          
          <!-- <fieldset>
               <table cellpadding="0" cellspacing="2">
                  <tr>
                 		 <td colspan="10" id="report_container_dtails"> </td>
                  </tr>
              </table>
          </fieldset>

-->


</div>

</body>
</html>
