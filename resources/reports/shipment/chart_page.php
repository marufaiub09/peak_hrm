
	<? 
extract($_GET);
extract($_POST);
 
//$chart_data1=str_replace("*","N",$chart_data);
?>
<script type="text/javascript" src="../amcharts/flash/swfobject.js"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
		<!-- following scripts required for JavaScript version. The order is important! -->
		<script type="text/javascript" src="../amcharts/javascript/amcharts.js"></script>
		<script type="text/javascript" src="../amcharts/javascript/amfallback.js"></script>
		<script type="text/javascript" src="../amcharts/javascript/raphael.js"></script>
<script>
function show_chart_qnty()
{
			 document.getElementById('graph_header').innerHTML="Quantity Wise Chart";
			//chart_data= //document.getElementById('chart_data_value').value;
			var params = {
                bgcolor:"#FFFFFF"
                };

		    var flashVars = {
		        path: "../amcharts/flash/",              
              //  settings_file: "settings.xml",
            
        
				/* in most cases settings and data are loaded from files, but, as this require
				 all the files to be upladed to web server, we use inline data and settings here.*/
				 
		        // settings_file: "../sampleData/column_settings.xml",
		        // data_file: "../sampleData/column_data.xml"
		        
				chart_data: "<? echo substr($chart_data_qnty,0,-2); ?> ",
                chart_settings: "<settings><background><file></file></background><data_type>csv</data_type><legend><enabled>0</enabled></legend><pie><inner_radius>25</inner_radius><height>10</height><angle>10</angle><gradient></gradient></pie><animation><start_time>1</start_time><pull_out_time>1</pull_out_time></animation><data_labels><show>{title}</show><max_width>300</max_width></data_labels></settings>"
				 
				
			};

			// change 8 to 80 to test javascript version
            if (swfobject.hasFlashPlayerVersion("8")){
	    		swfobject.embedSWF("../amcharts/flash/ampie.swf", "chartdiv", "390", "380", "8.0.0", "../amcharts/flash/expressInstall.swf", flashVars, params);
	    	}
			else{
				var amFallback = new AmCharts.AmFallback();
				// amFallback.settingsFile = flashVars.settings_file;  		// doesn't support multiple settings files or additional_chart_settins as flash does
				// amFallback.dataFile = flashVars.data_file;
				amFallback.chartSettings = flashVars.chart_settings;
				amFallback.chartData = flashVars.chart_data;
				amFallback.type = "pie";
				amFallback.write("chartdiv");
			}	
}


function show_chart_value()
{
			 document.getElementById('graph_header').innerHTML="Value Wise Chart";
			//chart_data= //document.getElementById('chart_data_value').value;
			var params = {
                bgcolor:"#FFFFFF"
                };

		    var flashVars = {
		        path: "../amcharts/flash/",              
              //  settings_file: "settings.xml",
            
        
				/* in most cases settings and data are loaded from files, but, as this require
				 all the files to be upladed to web server, we use inline data and settings here.*/
				 
		        // settings_file: "../sampleData/column_settings.xml",
		        // data_file: "../sampleData/column_data.xml"
		        
				chart_data: "<? echo substr($chart_data_value,0,-2); ?> ",
                chart_settings: "<settings><background><file></file></background><data_type>csv</data_type><legend><enabled>0</enabled></legend><pie><inner_radius>15</inner_radius><height>7</height><angle>10</angle><gradient></gradient></pie><animation><start_time>1</start_time><pull_out_time>1</pull_out_time></animation><data_labels><show>{title}</show><max_width>100</max_width></data_labels></settings>"
			};

			// change 8 to 80 to test javascript version
            if (swfobject.hasFlashPlayerVersion("8")){
	    		swfobject.embedSWF("../amcharts/flash/ampie.swf", "chartdiv", "400", "380", "8.0.0", "../amcharts/flash/expressInstall.swf", flashVars, params);
	    	}
			else{
				var amFallback = new AmCharts.AmFallback();
				// amFallback.settingsFile = flashVars.settings_file;  		// doesn't support multiple settings files or additional_chart_settins as flash does
				// amFallback.dataFile = flashVars.data_file;
				amFallback.chartSettings = flashVars.chart_settings;
				amFallback.chartData = flashVars.chart_data;
				amFallback.type = "pie";
				amFallback.write("chartdiv");
			}	
}
window.onload = function()
{
	show_chart_value();
}
</script>


<div style="width:520px;">
 <div style="width:420px; height:30px;text-align:right ">
 	<div style="width:150px; float:left; text-align:left; font-size:12px; font-weight:bold" id="graph_header"></div><div style="float:left"><a href="##" onclick="show_chart_qnty()">Quantity Wise Chart</a>&nbsp;&nbsp;<a href="##" onclick="show_chart_value()">Value Wise Chart</a></div><br />
 </div>
		 

        <!-- chart is placed in this div. if you have more than one chart on a page, give unique id for each div -->
		<div id="chartdiv" style="width:580px; height:580px; "></div>
 
 

</div>