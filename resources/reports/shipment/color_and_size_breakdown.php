<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	<script src="includes/ajax_submit.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>	
	<script>
	function set_date_range(mon)
	{
		 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		
		var start_date="01" + "-" + mon  + "-" + year;
		var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
		
		document.getElementById('txt_date_from').value=start_date;
		document.getElementById('txt_date_to').value=to_date;
		 //alert(to_date);
		 generate_report('report_container');
	}
	
   	function daysInMonth(month,year) 
   	{
    	return new Date(year, month, 0).getDate();
	}
	
	
function generate_report(div)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	document.getElementById(div).innerHTML="";
	var cbo_company_name=document.getElementById('cbo_company_mst').value;
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	var txt_style_ref=document.getElementById('txt_style_ref').value;
	var txt_date_from=document.getElementById('txt_date_from').value;
	var txt_date_to=document.getElementById('txt_date_to').value;
	if( txt_date_to < txt_date_from)
	{
	alert('Invalid date Range');	
	}
	else
	{
	var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_style_ref+"_"+txt_date_from+"_"+txt_date_to;
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
			document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			
		}
  	}
  	xmlhttp.open("GET","includes/generate_color_and_size_breakdown.php?data="+data+"&type=generate_color_and_size_breakdown",true);
	xmlhttp.send();
	}//end else statement if( txt_date_to > txt_date_from)
}

	function change_color(v_id,e_color)
	{
		
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	
	function openmypage()
		{
			var cbo_company_name=document.getElementById('cbo_company_mst').value;
			var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
			if(cbo_company_name==0)
			{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			});	
			}
			else
			{
		    emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_style_ref.php?cbo_company_name='+cbo_company_name+'&cbo_buyer_name='+cbo_buyer_name, 'Report', 'width=678px,height=450px,center=1,resize=0,scrolling=0','../')
			
			emailwindow.onclose=function()
				{
					var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
					var theemail=this.contentDoc.getElementById("hidd_style") //Access form field with id="emailfield"
					document.getElementById('txt_style_ref').value=theemail.value;
					//alert('Monzu'+theemail.value);
					//production_cutting(theemail.value,1);
				}
			}
		}
	</script>
    
    
</head>

<?php

include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');

?>

<body onLoad="check_check_box()">
<div style="width:1100px">
<fieldset style="width:100%" >
<legend>Fillter Panel</legend>
        <table width="100%" cellpadding="0" cellspacing="2" >
                <tr bgcolor="#CCCCCC">
                <td colspan="13" align="center" height="50" valign="middle"><font size="3"><strong>Color And Size Breackdown Report</strong></font></td>
                </tr>
                <tr>
                <td colspan="13" align="center" height="30" valign="middle">
                <div id="messagebox" style="background:#F99" align="center"></div>
                </td>
                </tr>
                <tr>
                <td colspan="13"> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Company Name<select name="cbo_company_mst" id="cbo_company_mst"  style="width:150px" class="combo_boxes">
                <option value="0">--- Select Company ---</option>
                <?
                $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 order by id"); //where is_deleted=0 and status=0
                while ($r_mod=mysql_fetch_array($mod_sql))
                {
                ?>
                <option value=<? echo $r_mod["id"];
                if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                <?
                }
                ?>
                </select>
                Buyer Name
                
                <select name="cbo_buyer_name" id="cbo_buyer_name"  style="width:140px" class="combo_boxes">
                <option value="0">--- All Buyer ---</option>
                <?
                $mod_sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0 and status_active=1 order by id");
                while ($r_mod=mysql_fetch_array($mod_sql))
                {
                ?>
                <option value=<? echo $r_mod["id"];
                if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[buyer_name]" ?> </option>
                <?
                }
                ?>
                </select>
                Style Ref
				<input type="text" name="txt_style_ref" id="txt_style_ref" class="text_boxes" width="100px" ondblclick="openmypage()" />
                Date From 
                <?
                $date= date("d-m-Y");
                $arr=explode('-', $date);
                $day=$arr[0]-1;
                ?>
                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px" >
                <script type="text/javascript">
                $( "#txt_date_from" ).datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
                });
               </script> 
                Date To 
                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px" >
                <script type="text/javascript">
                $( "#txt_date_to" ).datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
                });
                </script>
                <input type="button" name="show" id="show" onclick="generate_report('report_container');" class="formbutton" style="width:70px" value="Show" />
                </td>
                </tr>
                <tr>
                <td align="center" height="25" valign="bottom">
                <input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="January" class="month_button" />
                &nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="February" class="month_button" />
                &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="March" class="month_button" />
                &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="April" class="month_button" />
                &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" />
                &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="June" class="month_button" />
                &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="July" class="month_button" />
                &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="August" class="month_button" />
                &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="September" class="month_button" />
                &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="October" class="month_button" />
                &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="November" class="month_button" />
                &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="December" class="month_button" />          
                </td>
                </tr>
        </table>
  
<fieldset> 
<legend>Report Panel</legend>       
<table cellpadding="0" cellspacing="0" width="98%">
<tr>
<td colspan="13" height="20">
</td>
</tr>
<tr>
<td colspan="13" id="report_container"> 
</td>
</tr>
</table>
</fieldset>

<table>
<tr>
<td colspan="13" id="report_container_detals" border="1"  class="display"> 
</td>
</tr>
</table>
<a name="report_detals"></a>
</fieldset>
</div>

</body>
</html>
