<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>	
	<script>
	function set_date_range(mon)
	{
		 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		
		var start_date="01" + "-" + mon  + "-" + year;
		var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
		
		document.getElementById('txt_date_from').value=start_date;
		document.getElementById('txt_date_to').value=to_date;
		 //alert(to_date);
		 generate_report('report_container');
	}
	
   	function daysInMonth(month,year) 
   	{
    	return new Date(year, month, 0).getDate();
	}
	
	
function generate_report(div,stype)
{
				$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
				if (stype==1) // main call
	            {
				document.getElementById(div).innerHTML="";
				var cbo_company_name=document.getElementById('cbo_company_mst').value;
				var data=cbo_company_name+"_____";
				}
				else
				{
				var cbo_company_name=document.getElementById('cbo_company_mst').value;
				var txt_job_number=document.getElementById('txt_job_number').value;
				var txt_po_number=document.getElementById('txt_po_number').value;
				var txt_style_number=document.getElementById('txt_style_number').value;
				var txt_order_qnty=document.getElementById('txt_order_qnty').value;
				var buyer_name=document.getElementById('buyer_name').value;
				var txt_item_name=document.getElementById('txt_item_name').value;
				var data=cbo_company_name+"_"+txt_job_number+"_"+txt_style_number+"_"+txt_po_number+"_"+txt_order_qnty+"_"+buyer_name;	
				}
							if (window.XMLHttpRequest)
							{// code for IE7+, Firefox, Chrome, Opera, Safari
								xmlhttp=new XMLHttpRequest();
							}
							else
							{// code for IE6, IE5
								xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							}
							
							if (cbo_company_name==0)
							{
								$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
								return false; 
							}
							xmlhttp.onreadystatechange=function()
							{
								if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									var response=(xmlhttp.responseText).split('####');	
									document.getElementById(div).innerHTML=response[0];
									document.getElementById('print_div').innerHTML='<h3>Convert To &nbsp;<a href="'+response[1]+'" > <img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a><h3>';
									//document.getElementById(div).innerHTML=xmlhttp.responseText;
									$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
									{ 
										 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
									});
									
								}
							}
							xmlhttp.open("GET","includes/generate_shipment_pending_report.php?data="+data+"&type=shipment_pending_report",true);
							xmlhttp.send();
}


	function show_progress_report_details(type,job_number)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'work_progress_report_popup.php?type='+type+'&job_number='+job_number, 'Work Progress Report Details', 'width=730px,height=400px,center=1,resize=0,scrolling=0','../')
	}
	
	function change_color(v_id,e_color)
	{
		//alert(v_id);
		//alert(e_color);
		//alert(document.getElementById(v_id).bgColor);
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	
	
	function openmypage(po_no_mst_id,order_no,production_type,entry_break_down_type,type,excess_percent)
	{
		var txt_date_from= document.getElementById('txt_date_from').value;
		var txt_date_to= document.getElementById('txt_date_to').value;
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	document.getElementById('report_container_detals').innerHTML="";
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById('report_container_detals').innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET",'color_size_wise_sewing_production_summary.php?po_no_mst_id='+po_no_mst_id+'&order_no='+order_no+'&production_type='+production_type+'&entry_break_down_type='+entry_break_down_type+'&txt_date_from='+txt_date_from+'&txt_date_to='+txt_date_to+'&type='+type+'&excess_percent='+excess_percent,true);
	xmlhttp.send();
	}
	
	
	
	
	function openmypage2(po_no_mst_id,order_no,production_type,entry_break_down_type,type,excess_percent)
		{
			//alert("po_no_mst_id"+po_no_mst_id);
			//alert("order_no"+order_no);
			//alert("production_type"+production_type);
			//alert("date_codition"+date_codition);
			//alert("order_qnty"+order_qnty);
			var txt_date_from= document.getElementById('txt_date_from').value;
			var txt_date_to= document.getElementById('txt_date_to').value;
		//emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'color_size_wise_sewing_production_summary.php?po_no_mst_id='+po_no_mst_id+'&order_no='+order_no+'&production_type='+production_type+'&entry_break_down_type='+entry_break_down_type+'&txt_date_from='+txt_date_from+'&txt_date_to='+txt_date_to+'&type='+type+'&excess_percent='+excess_percent, 'Report', 'width=678px,height=450px,center=1,resize=0,scrolling=0','../')
		
		//emailwindow.onclose=function()
				//{
					//alert("Monzu")
				//}
				window.open('color_size_wise_sewing_production_summary.php?po_no_mst_id='+po_no_mst_id+'&order_no='+order_no+'&production_type='+production_type+'&entry_break_down_type='+entry_break_down_type+'&txt_date_from='+txt_date_from+'&txt_date_to='+txt_date_to+'&type='+type+'&excess_percent='+excess_percent)  
		}
		
	function check_check_box()
	{
		
		document.getElementById('append_report').checked=true;
		
	}

	function show_inner_filter(e)
    {
    	if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
 		if (unicode==13 )
		{
         	generate_report('report_container',2);
		}
    }	
	
	
	function openmypage2(remarks,type)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'remarks_pop_up.php?remarks='+remarks+'&type='+type, type, 'width=350px,height=300px,center=1,resize=0,scrolling=0','../')
		}
	</script>
    
    
</head>

<?php
session_start();

include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_name=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0) $company_name="and id=".$_SESSION['logic_erp']["company_id"]; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

?>

<body>
<div style="width:500px;">
<fieldset style="width:100%" >
<legend>Fillter Panel</legend>
        <table width="100%" cellpadding="0" cellspacing="2" >
                <tr class="form_caption">
                <td align="center" height="50" valign="middle"><font size="3"><strong>Shipment Pending Report</strong></font></td>
                </tr>
                <tr>
                <td align="center" height="30" valign="middle">
                <div id="messagebox" style="background:#F99" align="center"></div>
                </td>
                </tr>
                <tr>
                	<td align="center">
                    	<table class="rpt_table" width="300">
                        	<thead>
                            	<th>Company Name</th>
                            </thead>
                            <tr class="general">
                            	<td>
                                	<select name="cbo_company_mst" id="cbo_company_mst"  style="width:95%" class="combo_boxes">
                                        <option value="0">--- Select Company ---</option>
                                        <?
                                        $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name"); //where is_deleted=0 and status=0
                                        $n=mysql_num_rows($mod_sql);
                                        while ($r_mod=mysql_fetch_array($mod_sql))
                                        {
                                            if ($n==1) $company_combo=$r_mod["id"];
                                        ?>
                                        <option value=<? echo $r_mod["id"];
                                        if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                                        <?
                                        }
                                        ?>
                                        </select>                                        
                                 </td>
                                 <td width="5%">
                                 	<input type="button" name="show" id="show" onclick="generate_report('report_container',1);" class="formbutton" style="width:80px" value="Show" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
 </fieldset>
<fieldset style="width:100%" > 
<legend>Report Panel</legend>       
<table cellpadding="0" cellspacing="0" width="98%">
<tr>
<td colspan="13" id="print_div" border="1"  class="display" align="left"> 
</td>
</tr>
</tr>
<tr>
<td colspan="13" id="report_container"> 
</td>
</tr>
</table>
</fieldset>


<!--<table>
<tr>
<td colspan="13" id="print_div" border="1"  class="display"> 
</td>
</tr>
<tr>
<td colspan="13" id="report_container_detals" border="1"  class="display"> 
</td>
</tr>
</table>-->
</fieldset>

</div>


</body>
</html>
