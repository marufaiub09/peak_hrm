<?php

include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
$po_no=$_GET['po_no'];
$po_no1=explode(",",$po_no);
$arry_num=count($po_no1);

$po_id=$_GET['po_id'];
$po_id1=explode(",",$po_id);

$ex_fac_date=$_GET['ex_fac_date'];
$ex_fac_date1=explode(",",$ex_fac_date);

$ex_fac_qty=$_GET['ex_fac_qty'];
$ex_fac_qty1=explode(",",$ex_fac_qty);
//echo $po_no1;
//print_r($po_no1);
//echo $arry_num;
//$pi_date=$_GET['pi_date'];
//print " po_id $po_no1[1]";


?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
    <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>	
	<script>
		function change_color(v_id,e_color)
		{
			//alert(document.getElementById(v_id).bgColor);
			 
			if (document.getElementById(v_id).bgColor=="#33CC00")
			{
				document.getElementById(v_id).bgColor=e_color;
			}
			else
			{
				document.getElementById(v_id).bgColor="#33CC00";
			}
		}
	</script>
    
    
</head>


<body>

<div style="width:650px" align="center">
<fieldset style="width:100%"  >
	<table border="1" class="rpt_table" align="center" width="100%" rules="all">
        <thead>
            <th>PO NO</th>
            <th>Ex Fac. Date</th>
            <th>Ex Fac. Qty</th>
            <th>Unit Price</th>
            <th>Ex Fac. Value</th>
       </thead>
		<?
		$i=0;
		while($i<= ($arry_num-1))
		{
			if ($i%2==0) 
				$bgcolor="#FFFFFF"; 
			else
				$bgcolor="#E9F3FF";
			
			$company_sql= mysql_db_query($DB, "select a.*, b.order_uom from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and a.id='$po_id1[$i]'");
			$row_date=mysql_fetch_array($company_sql);
			if(trim($po_no1[$i])!='')
			{
		?>
        <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
            <td><? echo $po_no1[$i]; ?></td>
            <td><? echo $ex_fac_date1[$i]; ?></td>
            <td>
				<? 
					echo number_format($ex_fac_qty1[$i],0);
					$total_ex_fac_qty1+=$ex_fac_qty1[$i];
				?>
            </td>
            <td>
				<? 
					if ($row_date[order_uom]==1)
					{
						echo number_format($row_date['unit_price'],2);
					}
					if ($row_date[order_uom]==2)
					{
						echo number_format($row_date['set_pc_rate'],2);
					}
					
				?>
            </td>
            <td>
				<? 
					if ($row_date[order_uom]==1)
					{
						$prod_value=$ex_fac_qty1[$i] * $row_date[unit_price];
						echo number_format($prod_value,2);
						$total_prod_value+=$prod_value;
					}
					if ($row_date[order_uom]==2)
					{
						$prod_value=$ex_fac_qty1[$i] * $row_date[set_pc_rate];
						echo number_format($prod_value,2);
						$total_prod_value+=$prod_value;
					}
					
				?>
            </td>
        </tr>
		<?
			}
		$i++;
		}
        ?>
        <tfoot>
            <th  colspan="2" align="right"><b>Total:</b></th>
            <th  align="center"><?php echo number_format($total_ex_fac_qty1,0); ?></th>
            <th  align="center"></th>
            <th  align="center"><?php echo number_format($total_prod_value,2); ?></th>
       </tfoot>
	</table>
</fieldset>

</div>


</body>
</html>
