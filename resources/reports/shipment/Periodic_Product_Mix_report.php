<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
	<script src="includes/includes/ajax_submit.js" type="text/javascript"></script>
	<script src="includes/includes/functions.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
	
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    
	<script>
	function set_date_range(mon)
	{
		 $('.month_button_selected').removeClass('month_button_selected').addClass('month_button');
		 if (mon.substr(0,1)=="0") id_id=mon.replace("0",""); else id_id=mon;
		 $('#btn_'+id_id).removeClass('month_button').addClass('month_button_selected');
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = document.getElementById('cbo_year').value;
		
		var start_date="01" + "-" + mon  + "-" + year;
		var to_date=daysInMonth(mon,year) + "-" + mon  + "-" + year;
		
		document.getElementById('txt_date_from').value=start_date;
		document.getElementById('txt_date_to').value=to_date;
		 //alert(to_date);
		// generate_report('report_container');
	}
	
   	function daysInMonth(month,year) 
   	{
    	return new Date(year, month, 0).getDate();
	}
	
	function WriteToFile()
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			//xmlhttp=new XMLHttpRequest();
			var fso = new XMLHttpRequest("Scripting.FileSystemObject");
	
		}
		else
		{// code for IE6, IE5
			//xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			var fso = new ActiveXObject("Scripting.FileSystemObject");
	
		}
	var s = fso.CreateTextFile("C:\\Test.txt", true);
	s.WriteLine('Hello');
	s.Close();
	}
	
function generate_report(div,convert_container)
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	
	document.getElementById(div).innerHTML="";
	document.getElementById('report_container_buyer').innerHTML="";
	document.getElementById('report_container_item').innerHTML="";
	
	var cbo_company_name=document.getElementById('cbo_company_mst').value;
	
	var txt_date_from=document.getElementById('txt_date_from').value;
	var txt_date_to=document.getElementById('txt_date_to').value;
	
	var data=cbo_company_name+"_"+txt_date_from+"_"+txt_date_to;
	//+"_"+cbo_agent_name;
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	
	
	
	if (cbo_company_name==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
		return false; }
		
	else if (txt_date_from==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a From Date....').fadeIn(1000);
		return false; }	
	
	else if (txt_date_to==0){
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select a To Date....').fadeIn(1000);
		return false; }	
		
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
  			var response = xmlhttp.responseText.split('####');
  			//alert(response[1]);
  			//$('#convert_container').html( 'Convert To : <a href="'+ response[1] + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>' );
  			document.getElementById(div).innerHTML=response[0];
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox+
			
			{ 
				$(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
				var grand_tot_per_val=0;
				var grand_tot_per_qnty=0;
				var tot_row=document.getElementById('tot_row_dep').value;
				var grand_tot_val=document.getElementById('grand_tot_val').innerHTML
				var grand_tot_val=grand_tot_val.replace(",",""); 
				var grand_tot_val=parseFloat(grand_tot_val);
				
				var grand_tot_qnty=document.getElementById('grand_tot_qnty').innerHTML
				var grand_tot_qnty=grand_tot_qnty.replace(",",""); 
				var grand_tot_qnty=parseFloat(grand_tot_qnty);
				for(i=1; i<=tot_row; i++)
				{
					
					var row_val_1=document.getElementById('row_val_'+i).innerHTML;
					var row_val_1=row_val_1.replace(",","");
					var row_val_1=parseFloat(row_val_1);
					var percent_val=row_val_1/grand_tot_val*100;
					var percent_val=percent_val.toFixed(2);
					document.getElementById('row_val_per_'+i).innerHTML= percent_val+'%';
					grand_tot_per_val+=percent_val*1;

					var row_qnty_1=document.getElementById('row_qnty_'+i).innerHTML;
					var row_qnty_1=row_qnty_1.replace(",","");
					var row_qnty_1=parseFloat(row_qnty_1);
					var percent_qnty=row_qnty_1/grand_tot_qnty*100;
					var percent_qnty=percent_qnty.toFixed(2)
					document.getElementById('row_qnty_per_'+i).innerHTML= percent_qnty+'%';
					grand_tot_per_qnty+=percent_qnty*1;

				}
				document.getElementById('grand_tot_per_val').innerHTML= Math.round(grand_tot_per_val)+'.00%';
				document.getElementById('grand_tot_per_qnty').innerHTML= Math.round(grand_tot_per_qnty)+'.00%';
			});
			// alert(xmlhttp.responseText);
			//document.getElementById(div).style.border="1px solid #A5ACB2";
		}
  	}
  	xmlhttp.open("GET","includes/Periodic_Product_Mix_report.php?data="+data+"&type=Periodic_Product_Mix_report",true);
	xmlhttp.send();
}

function generate_report_details(div,id)
{
	
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	
	document.getElementById(div).innerHTML="";
	document.getElementById('report_container_item').innerHTML="";
	var cbo_company_name=document.getElementById('cbo_company_mst').value;
	
	var txt_date_from=document.getElementById('txt_date_from').value;
	var txt_date_to=document.getElementById('txt_date_to').value;
	var product_dept=id;
	
	var data=cbo_company_name+"_"+txt_date_from+"_"+txt_date_to+"_"+product_dept;
	
	
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
				var grand_tot_per_val_b=0;
				var grand_tot_per_qnty_b=0;
			    var tot_row=document.getElementById('tot_row_buy').value;
				var grand_tot_val_b=document.getElementById('grand_tot_val_b').innerHTML
				var grand_tot_val_b=grand_tot_val_b.replace(",",""); 
				var grand_tot_val_b=parseFloat(grand_tot_val_b);
				var grand_tot_qnty_b=document.getElementById('grand_tot_qnty_b').innerHTML
				var grand_tot_qnty_b=grand_tot_qnty_b.replace(",",""); 
				var grand_tot_qnty_b=parseFloat(grand_tot_qnty_b);
				for(i=1; i<=tot_row; i++)
				{
					var row_val_b_1=document.getElementById('row_val_b_'+i).innerHTML;
					var row_val_b_1=row_val_b_1.replace(",","");
					var row_val_b_1=parseFloat(row_val_b_1);
					var percent_val_b=row_val_b_1/grand_tot_val_b*100;
					var percent_val_b=percent_val_b.toFixed(2);
					document.getElementById('row_val_per_b_'+i).innerHTML= percent_val_b+'%';
					grand_tot_per_val_b+=percent_val_b*1;

					var row_qnty_b_1=document.getElementById('row_qnty_b_'+i).innerHTML;
					var row_qnty_b_1=row_qnty_b_1.replace(",","");
					var row_qnty_b_1=parseFloat(row_qnty_b_1);
					var percent_qnty_b=row_qnty_b_1/grand_tot_qnty_b*100;
					var percent_qnty_b=percent_qnty_b.toFixed(2)
					document.getElementById('row_qnty_per_b_'+i).innerHTML= percent_qnty_b+'%';
					grand_tot_per_qnty_b+=percent_qnty_b*1;

				}
				document.getElementById('grand_tot_per_val_b').innerHTML= Math.round(grand_tot_per_qnty_b)+'.00%';
				document.getElementById('grand_tot_per_qnty_b').innerHTML= Math.round(grand_tot_per_qnty_b)+'.00%';
			});
			
		}
  	}
  	xmlhttp.open("GET","includes/Periodic_Product_Mix_report.php?data="+data+"&type=Periodic_Product_Mix_report_buyer",true);
	xmlhttp.send();
	
}	


function generate_report_details_bottom(div,b_id,p_id)
{
	
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	document.getElementById(div).innerHTML="";
	var cbo_company_name=document.getElementById('cbo_company_mst').value;
	var txt_date_from=document.getElementById('txt_date_from').value;
	var txt_date_to=document.getElementById('txt_date_to').value;
	var buyer_id=b_id;
	var product_id=p_id;
	var data=cbo_company_name+"_"+txt_date_from+"_"+txt_date_to+"_"+buyer_id+"_"+product_id;
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
				var grand_tot_per_val_c=0;
				var grand_tot_per_qnty_c=0;
				var tot_row=document.getElementById('tot_row_item').value;
				var grand_tot_val_c=document.getElementById('grand_tot_val_c').innerHTML
				var grand_tot_val_c=grand_tot_val_c.replace(",",""); 
				var grand_tot_val_c=parseFloat(grand_tot_val_c);
				var grand_tot_qnty_c=document.getElementById('grand_tot_qnty_c').innerHTML
				var grand_tot_qnty_c=grand_tot_qnty_c.replace(",",""); 
				var grand_tot_qnty_c=parseFloat(grand_tot_qnty_c);
				for(i=1; i<=tot_row; i++)
				{
					var row_val_c_1=document.getElementById('row_val_c_'+i).innerHTML;
					var row_val_c_1=row_val_c_1.replace(",","");
					var row_val_c_1=parseFloat(row_val_c_1);
					var percent_val_c=row_val_c_1/grand_tot_val_c*100;
					var percent_val_c=percent_val_c.toFixed(2);
					document.getElementById('row_val_per_c_'+i).innerHTML= percent_val_c+'%';
					grand_tot_per_val_c+=percent_val_c*1;
					var row_qnty_c_1=document.getElementById('row_qnty_c_'+i).innerHTML;
					var row_qnty_c_1=row_qnty_c_1.replace(",","");
					var row_qnty_c_1=parseFloat(row_qnty_c_1);
					var percent_qnty_c=row_qnty_c_1/grand_tot_qnty_c*100;
					var percent_qnty_c=percent_qnty_c.toFixed(2)
					document.getElementById('row_qnty_per_c_'+i).innerHTML= percent_qnty_c+'%';
					grand_tot_per_qnty_c+=percent_qnty_c*1;
				}
				document.getElementById('grand_tot_per_val_c').innerHTML= Math.round(grand_tot_per_qnty_c)+'.00%';
				document.getElementById('grand_tot_per_qnty_c').innerHTML= Math.round(grand_tot_per_qnty_c)+'.00%';
			});
			
		}
  	}
  	xmlhttp.open("GET","includes/Periodic_Product_Mix_report.php?data="+data+"&type=Periodic_Product_Mix_report_item",true);
	xmlhttp.send();
	
}	



	function change_color(v_id,e_color)
	{
		//alert(v_id);
		//alert(e_color);
		//alert(document.getElementById(v_id).bgColor);
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}

function show_progress_report_details(type,job_number)
{	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'work_progress_report_popup.php?type='+type+'&job_number='+job_number, 'Work Progress Report Details', 'width=730px,height=400px,center=1,resize=0,scrolling=0','../')
}
function change_color(v_id,e_color)
{
	if (document.getElementById(v_id).bgColor=="#33CC00")
	{
		document.getElementById(v_id).bgColor=e_color;
	}else
	{
		document.getElementById(v_id).bgColor="#33CC00";
	}
}


	</script>
</head>

<?php
session_start();
include('../../includes/common.php');
include('../../includes/array_function.php');
include('../../includes/common_functions.php');
if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_name=" and id=".$_SESSION['logic_erp']["buyer_id"]; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0) $company_name="and id=".$_SESSION['logic_erp']["company_id"]; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
?>

<body>
<div style="width:1000px">
	<fieldset style="width:98%">
    	<table width="100%" cellpadding="0" cellspacing="2" border="0">
        	<tr class="form_caption">
            	<td align="center" height="50" valign="middle"><font size="3">Periodic Product Mix report </font></td>
            </tr>
            <tr>
            	<td align="center" height="30" valign="middle">
                	<div id="messagebox" style="background:#F99" align="center"></div>
                </td>
            </tr>
            <tr>
            	<td align="center">
                	<table class="rpt_table" width="500">
                    	<thead>
                        	<th width="15%">Company</th>
                            <th width="20%" colspan="2">Date</th>
                        </thead>
                        <tr class="general">
                        	<td width="100" align="left">
                                <select name="cbo_company_mst" id="cbo_company_mst"  style="width:120px" class="combo_boxes">
                                    <option value="0">--- All Company ---</option>
                                    <?
                                    $mod_sql= mysql_db_query($DB, "select * from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name"); //where is_deleted=0 and status=0
									$n=mysql_num_rows($mod_sql);
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
										  if ($n==1) $company_combo=$r_mod["id"];   
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[company_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                </select>                            
                            </td>
                            <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px">
								<script type="text/javascript">
                                    $( "#txt_date_from" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> </td>
                            <td><input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px">
                                <script type="text/javascript">
                                    $( "#txt_date_to" ).datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                
                                </script> 
                            </td>
                            
                            <td width="5%">
                            	<input type="button" name="search" id="search" value="Show" onclick="generate_report('report_container','convert_container')" style="width:70px" class="formbutton" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="10"></tr>
            <tr>
            	<td  height="20" valign="bottom" colspan="8" align="left">
                  <?
                    	$c_year= date("Y");
                    	$p_year=$c_year-15;
					
                    
                    ?>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <select name="cbo_year" id="cbo_year"  style="width:80px" class="combo_boxes">
						 
						<?
						for($i=0;$i<21;$i++)
						{
						?>
						<option value=<? echo $p_year+$i;
						if ($c_year==$p_year+$i){?> selected <?php }?>><? echo $p_year+$i; ?> </option>
						<?
						}
						?>
					</select>
               
                    &nbsp;<input type="button" name="btn_1" onclick="set_date_range('01')" id="btn_1" value="January" class="month_button" />
                    &nbsp;<input type="button" name="btn_2" onclick="set_date_range('02')" id="btn_2" value="February" class="month_button" />
                    &nbsp;<input type="button" name="btn_3" onclick="set_date_range('03')" id="btn_3" value="March" class="month_button" />
                    &nbsp;<input type="button" name="btn_4" onclick="set_date_range('04')" id="btn_4" value="April" class="month_button" />
                    &nbsp;<input type="button" name="btn_5" onclick="set_date_range('05')" id="btn_5" value="May" class="month_button" />
                    &nbsp;<input type="button" name="btn_6" onclick="set_date_range('06')" id="btn_6" value="June" class="month_button" />
                    &nbsp;<input type="button" name="btn_7" onclick="set_date_range('07')" id="btn_7" value="July" class="month_button" />
                    &nbsp;<input type="button" name="btn_8" onclick="set_date_range('08')" id="btn_8" value="August" class="month_button" />
                    &nbsp;<input type="button" name="btn_9" onclick="set_date_range('09')" id="btn_9" value="September" class="month_button" />
                    &nbsp;<input type="button" name="btn_10" onclick="set_date_range('10')" id="btn_10" value="October" class="month_button" />
                    &nbsp;<input type="button" name="btn_11" onclick="set_date_range('11')" id="btn_11" value="November" class="month_button" />
                    &nbsp;<input type="button" name="btn_12" onclick="set_date_range('12')" id="btn_12" value="December" class="month_button" />          
                 </td>
            </tr> 
        </table>
     </fieldset>
     <fieldset>
         <table cellpadding="0" cellspacing="2">
             
             <tr>
             <td width="500"></td>
            	<td id="convert_container" align="left"> </td>
            </tr>
            <tr>
            	<td colspan="10" id="report_container"> 
                 
                </td>
            </tr>
           
             <tr>
            	<td colspan="10" id="report_container_buyer"> 
                 
                </td>
            </tr>
             <tr>
            	<td colspan="10" id="report_container_item"> 
                 
                </td>
            </tr>
        </table>
    	
    </fieldset>



</div>

</body>
</html>
