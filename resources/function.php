<?php
$con = mysql_connect( 'localhost', 'root', '' );
mysql_select_db( 'system_library', $con );

function SelectItem( $ItemName, $Id, $TableName, $Country ) {
	$dbName = "system_library";
	$sql = "select $ItemName, $Id from $TableName";
	$rs = mysql_query( $sql );
	$option = '';
	while( $row = mysql_fetch_array( $rs ) ) {
		if( $row[$ItemName] == $Country ) {
			$option .= "<option value='" . $row[$ItemName] . "' selected>" . $row[$ItemName] . "</option>";
		} else {
			$option .= "<option value='" . $row[$ItemName] . "'>" . $row[$ItemName] . "</option>";
		}
	}
	echo $option;
}

function ChangeDate( $Date ) {
	$Date = explode( " ", $Date );
	return $Date;
}

function ChangeDateFormat( $Date ) {
	if( $Date ) {
		$Date = ChangeDate( $Date );
		$Date = explode( "-", $Date[0] );
		$Date = $Date[2] . "/" . $Date[1] . "/" . $Date[0];
	} else {
		$Date = "mm/dd/yyyy";
	}
	return $Date;
}

function DbDate( $str ) {
	if( $str !="mm/dd/yyyy" ) {
		$str = str_replace( "/", "-", $str );
		$str = explode( "-", $str );
		if( strlen( $str[1]) != 2 ) { $str[1]="0".$str[1]; }
		if( strlen( $str[0]) != 2 ) { $str[0]="0".$str[0]; }
		$str = "$str[2]-$str[1]-$str[0]";
	} else {
		$str = '0000-00-00';
	}
	return $str;
}

function getRealIpAddr() {
	if( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {	//check ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {	//to check ip is pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

function WriteQuery( $sql ) {
	$ip = getRealIpAddr();

	$myFile = "../Log/" . $_SESSION["user_name"] . ".csv";
	$fh = fopen( $myFile, 'a' ) or die( "can't open file" );
	$stringData = $_SESSION["user_name"] . "," . $ip . "," . date( "F j, Y, g:i a" ) . "," . $sql . "\n";
	fwrite( $fh, $stringData );
	fclose( $fh );
}
?>