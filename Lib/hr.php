<?php
$host_connect = mysql_connect( 'localhost', 'root', '' );
if( !$host_connect ) {
	die( 'Could not connect: ' . mysql_error() );
} else {
	$db_connect = mysql_select_db( 'pim', $host_connect );
	if( !$db_connect ) {
		die( 'Can\'t use hrm : ' . mysql_error() );
	}
}

if( $_POST['EmpNo'] ) {
		$value = array(
		'EmpNo'							=> rand( 1, 1000 ),
		'status'						=> 1,
		'service_benefit'				=> 'Original DOJ',
		'salary_breakdown_method'		=> $_POST['breakdown_method'],
		'gross_salary'					=> isset( $_POST['gross_salary'] ) ? $_POST['gross_salary'] : 0,
		'salary_fraction_adjust_with'	=> $_POST['fraction_adjust_with']
	);
	$query = "INSERT INTO emp_details (
					EmpNo,
					status,
					service_benefit,
					salary_breakdown_method,
					gross_salary,
					salary_fraction_adjust_with
				) VALUES (
					" . $value['EmpNo'] . ",
					" . $value['status'] . ",
					'" . $value['service_benefit'] . "',
					'" . $value['salary_breakdown_method'] . "',
					" . $value['gross_salary'] . ",
					" . $value['salary_fraction_adjust_with'] . "
				)";
	
	$result = mysql_query( $query );
	if( !$result ) {
		die( 'Invalid query: ' . mysql_error() );
	}
	
	for( $i = 0; $i < count( $_POST['payroll_head'] ); $i++ ) {
		$percentage_formula = isset( $_POST['percentage_formula_$i'] ) ? $_POST['percentage_formula_$i'] : '';
		$base_head = isset( $_POST['base_head_$i'] ) ? $_POST['base_head_$i'] : 0;
		$proportionate_to_pay_days = isset( $_POST['proportionate'][$i] ) ? $_POST['proportionate'][$i] : 0;
		
		$query = "INSERT INTO salary_info (
						emp_code,
						payroll_head,
						type,
						percentage_formula,
						base_head,
						amount,
						proportionate_to_pay_days
					) VALUES (
						" . $value['EmpNo'] . ",
						" . $_POST['payroll_head'][$i] . ",
						'" . $_POST['type'][$i] . "',
						'" . $percentage_formula . "',
						" . $base_head . ",
						" . $_POST['amount'][$i] . ",
						" . $proportionate_to_pay_days . "
					)";
		
		$result = mysql_query( $query );
		if( !$result ) {
			die( 'Invalid query: ' . mysql_error() );
		}
	}
}

if( $_POST['EmpNo'] ) {
	$value = array(
		'emp_code'						=> rand( 1, 1000 ),
		'ot_entitled'					=> $_POST['over_duty'] =='OT' ? 1 : 0,
		'holiday_allowance_entitled'	=> $_POST['over_duty'] == 'Holiday' ? 1 : 0,
		'monthly_salary_based'			=> $_POST['salary_type'] == 'Monthly' ? 1 : 0,
		'weekly_wages_based'			=> $_POST['salary_type'] == 'Weekly' ? 1 : 0,
		'piece_rate_based'				=> $_POST['salary_type'] == 'Piece' ? 1 : 0,
		'piece_rate_based_date'			=> $_POST['salary_type'] == 'Piece' ? $_POST['piece_rate_date'] : '',
		'pf_entitled'					=> isset( $_POST['pf_entitled'] ) ? 1 : 0,
		'pf_entitled_date'				=> isset( $_POST['pf_entitled'] ) ? $_POST['pf_entitled_date'] : '',
		'group_insurance'				=> isset( $_POST['gi_entitled'] ) ? 1 : 0,
		'group_insurance_date'			=> isset( $_POST['gi_entitled'] ) ? $_POST['gi_entitled_date'] : ''
	);
	$query = "INSERT INTO entitlement (
					emp_code,
					ot_entitled,
					holiday_allowance_entitled,
					monthly_salary_based,
					weekly_wages_based,
					piece_rate_based,
					piece_rate_based_date,
					pf_entitled,
					pf_entitled_date,
					group_insurance,
					group_insurance_date
				) VALUES (
					" . $value['emp_code'] . ",
					" . $value['ot_entitled'] . ",
					" . $value['holiday_allowance_entitled'] . ",
					" . $value['monthly_salary_based'] . ",
					" . $value['weekly_wages_based'] . ",
					" . $value['piece_rate_based'] . ",
					'" . $value['piece_rate_based_date'] . "',
					" . $value['pf_entitled'] . ",
					'" . $value['pf_entitled_date'] . "',
					" . $value['group_insurance'] . ",
					'" . $value['group_insurance_date'] . "'
				)";
	
	$result = mysql_query( $query );
	if( !$result ) {
		die( 'Invalid query: ' . mysql_error() );
	}
	
	for( $i = 0; $i < count( $_POST['pf_nominee_name'] ); $i++ ) {
		$query = "INSERT INTO nominee (
						emp_code,
						name,
						relation,
						ratio,
						type
					) VALUES (
						" . $value['emp_code'] . ",
						'" . $_POST['pf_nominee_name'][$i] . "',
						'" . $_POST['pf_nominee_relation'][$i] . "',
						" . $_POST['pf_nominee_ratio'][$i] . ",
						0
					)";
		
		$result = mysql_query( $query );
		if( !$result ) {
			die( 'Invalid query: ' . mysql_error() );
		}
	}
	for( $i = 0; $i < count( $_POST['gi_nominee_name'] ); $i++ ) {
		$query = "INSERT INTO nominee (
						emp_code,
						name,
						relation,
						ratio,
						type
					) VALUES (
						" . $value['emp_code'] . ",
						'" . $_POST['gi_nominee_name'][$i] . "',
						'" . $_POST['gi_nominee_relation'][$i] . "',
						" . $_POST['gi_nominee_ratio'][$i] . ",
						1
					)";
		
		$result = mysql_query( $query );
		if( !$result ) {
			die( 'Invalid query: ' . mysql_error() );
		}
	}
	unset( $_REQUEST );
}
?>