<?
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
	
	<style type="text/css">
		#tooltip {
			position:absolute;
			border:1px solid #333;
			background:#f7f5d1;
			padding:2px 5px;
			color:#333;
			display:none;
		}
		.formbutton { width:100px; }
	</style>
    <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../includes/ajax_submit.js" type="text/javascript"></script>
	<script src="../includes/functions.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
    <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
  <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>

</head>
 

<body>
	<div align="center">
		<div style="width:500px; height:60px" align="center">
			<div><span style="text-size:12px;font-family:"times new roman", times, serif;"><strong>Password Changing</strong></span>
            <p id="permission_caption">
						<? 
                            echo "Your Permissions--> \n".$update;
                        ?>
         		</p>
			<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
		</div>
		<form name="change_pass" id="change_pass" method="" action="javascript:fnc_change_pass(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">
			<fieldset title="Files" style="width:500px;">
				<legend>Change Password</legend>
				<div style="width:100%; float:left;" align="center">
					<table>
						<tr>
							<td>User ID</td>
							<td>
								<input type="text" name="txt_user_id" id="txt_user_id" <? if( $_SESSION["user_level"] != 2 or $_SESSION["user_level"] != 4 ) { ?> readonly <? } ?> class="text_boxes" style="width:210px;" value="<? echo $_SESSION['logic_erp']["user_name"]; ?>"/>
								<a href="##" style="text-decoration:none; cursor: help; " class="tooltip" title="">
									<img src="images/11533979561754326101.png" height="18" width="20" onmouseover="if(t1)t1.Show(event,user_id_pass_change)" onmouseout="if(t1)t1.Hide(event)" />
								</a>
							</td>
						</tr>
						<tr>
							<td>Old Password</td>
							<td>
								<input type="password" name="txt_old_passwd" onkeyup="" id="txt_old_passwd" class="text_boxes" style="width:210px; height:20px" />
								<a href="##" style="text-decoration:none; cursor: help; " class="tooltip" title="">
									<img src="images/11533979561754326101.png" height="18" width="20" onmouseover="if(t1)t1.Show(event,user_pass_old)" onmouseout="if(t1)t1.Hide(event)" />
								</a>
							</td>
						</tr>
						<tr>
							<td>New Password</td>
							<td>
								<div style="width:100%">
									<input type="password" name="txt_new_passwd" id="txt_new_passwd" class="text_boxes" style="width:210px; height:20px" />
									<a href="##" style="text-decoration:none; cursor: help; " class="tooltip" title="">
										<img src="images/11533979561754326101.png" height="18" width="20" onmouseover="if(t1)t1.Show(event,user_pass_new)" onmouseout="if(t1)t1.Hide(event)" />
									</a>
								</div>
							</td>
						</tr>
						<tr>
							<td>Confirm Password</td>
							<td valign="top">
								<div style="width:100%">
									<input type="password" name="txt_conf_passwd" id="txt_conf_passwd" class="text_boxes" style="width:210px; height:20px" />
									<a href="##" style="text-decoration:none; cursor: help; " class="tooltip" title="">
										<img src="images/11533979561754326101.png" height="18" width="20" onmouseover="if(t1)t1.Show(event,user_pass_conf)" onmouseout="if(t1)t1.Hide(event)" />
									</a>
								</div>
							</td>
						</tr>
						<?php if( $_SESSION["user_level"] == 2 or $_SESSION["user_level"] == 4 ) { ?>
						<tr>
							<td>User Level</td>
							<td>
								<select name="cbo_user_level" id="cbo_user_level" class="combo_boxes" style="width:222px">
									<option value="0">---Select User Type---</option>
									<option value="1">General User</option>
									<option value="2">Admin User</option>
									<option value="3">Demo User</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Unit Name</td>
							<td>
								<select name="cbo_unit_name" id="cbo_unit_name" class="combo_boxes" style="width:222px">
									<option value="0">---Select User Type---</option>
									<option value="1">General User</option>
									<option value="2">Admin User</option>
									<option value="3">Demo User</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Bind to IP</td>
							<td><input type="text" name="txt_ip_addres" id="txt_ip_addres" class="text_boxes" style="width:220px"></td>
						</tr>
						<tr>
							<td>Expiry Date</td>
							<td>
								<input type="text" name="txt_exp_date" id="txt_exp_date" class="text_boxes" style="width:90px" />&nbsp;&nbsp;Status&nbsp;&nbsp;
								<select name="cbo_user_sts" id="cbo_user_sts" class="combo_boxes" style="width:74px" >
									<option	 value="1" >Active</option>
									<option	 value="2" >Inactive</option>
								</select>
							</td>
						</tr>
						<? } ?>
						<tr>
							<td>&nbsp;</td>
							<td align="left" style="padding-top:10px;">
								<input type="submit" name="save" id="save" class="formbutton" value="Save" />
								&nbsp;&nbsp;<input type="reset" name="reset" id="reset" class="formbutton" value="Refresh" />
							</td>
						</tr>
					</table>
				</div>
			</fieldset>	
		</form>
	</div>
</body>
</html>