<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
// Buyer or Party details
/*
*/
// company
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}
// location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}
// division
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}
// department
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}
// section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}
// subsection
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}
// designation
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>
    <head>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../includes/ajax_submit.js" type="text/javascript"></script>
        <script src="../includes/functions.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        
		<script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script language="javascript">
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
                $("#cbo_company_id,#cbo_location_id,#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselect({
                    //header: false, 
                    selectedText: "# of # selected",
                });
				$("#cbo_company_id,#cbo_location_id,#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselectfilter({ });	
            });
			
			// multi_up_check
			function multi_up_check(field,checkedItems)
			{
				var valArr =checkedItems .split(",");
				var i = 0;
				var size = valArr.length;
				for(i; i < size; i++)
				{
				  $("#"+field).multiselect("widget").find(":checkbox[value='"+valArr[i]+"']").attr("checked","checked");
				  $("#"+field+ " option[value='" + valArr[i] + "']").attr("selected", 1);
				  $("#"+field).multiselect("refresh");
				}
			}
			
			//Do not change 
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer") {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				}
				else request_type = new XMLHttpRequest();
				
				return request_type;
			}
			var http = createObject();
			
			// func_save_user_creation
			function func_save_user_creation(save_perm,edit_perm,delete_perm,approve_perm) 
			{
				//alert("su..re");
				//$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn( 1000 );
				var valid = true;
				
				/*
				var txt_user_id = encodeURI(document.getElementById('txt_user_id').value);
				var txt_passwd = encodeURI(document.getElementById('txt_passwd').value);
				var txt_conf_passwd = encodeURI(document.getElementById('txt_conf_passwd').value);
				var cbo_user_level = encodeURI(document.getElementById('cbo_user_level').value);
				var cbo_user_buyer = encodeURI(document.getElementById('cbo_user_buyer').value);
				var cbo_data_level_sec = encodeURI(document.getElementById('cbo_data_level_sec').value);
				
				var cbo_unit_name = encodeURI(document.getElementById('cbo_unit_name').value);
				var txt_ip_addres = encodeURI(document.getElementById('txt_ip_addres').value);
				var txt_exp_date = encodeURI(document.getElementById('txt_exp_date').value);
				var cbo_user_sts = encodeURI(document.getElementById('cbo_user_sts').value);
				var save_up = encodeURI(document.getElementById('save_up').value);
				*/
				if( $('#txt_user_id').val() == "" ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#txt_user_id').focus();
						$(this).html('Please Enter a unique user name').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#txt_password').val() == "" ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#txt_password').focus();
						$(this).html('Please Enter Password').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if($('#txt_password').val().length <4 ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#txt_password').focus();
						$(this).html('Please Enter a Password of at least 4 characters..').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#txt_confirm_password').val() == "" ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#txt_confirm_password').focus();
						$(this).html('Please Confirm  same password.').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#txt_confirm_password').val() != $('#txt_password').val() ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#txt_confirm_password').focus();
						$(this).html('New Password and Confirm password does not match.').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#cbo_user_level').val() == 0 ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#cbo_user_level').focus();
						$(this).html('Please Select an user level.').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#txt_ip_addres').val() != '' ) 
				{
					var ip = $('#txt_ip_addres').val();
					var ipsub = new Array();
					ipsub = ip.split(".");
					
					if( ipsub.length < 4 )
					{
						 valid = false;
					}
					else 
					{
						for( var i = 0; i < ipsub.length; i++ ) 
						{
							var sub = parseInt( ipsub[i] );
							if( ( isNaN( sub ) ) || ( i == 0 && sub == 0 ) || ( i == 3 && sub == 0 ) || ( sub < 0 ) || ( sub >255 ) ) 
							{
								valid = false;
							}
						}
					}
					
					if( valid == false ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$('#txt_ip_addres').focus();
							$(this).html('Please provide a correct IP address.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
					}
				}
				if( valid == true ) 
				{
					var data=	'&update_id='+$('#txt_update_id').val()+
								'&user_id='+$('#txt_user_id').val()+
								'&expiry_date='+$('#txt_expiry_date').val()+
								'&password='+$('#txt_password').val()+
								'&confirm_password='+$('#txt_confirm_password').val()+
								'&user_level='+$('#cbo_user_level').val()+
								'&data_level_security='+$('#cbo_data_level_security').val()+
								'&ip_addres='+$('#txt_ip_addres').val()+
								'&user_status='+$('#cbo_user_status').val()+
								'&company_id='+$('#cbo_company_id').val()+
								'&location_id='+$('#cbo_location_id').val()+
								'&division_id='+$('#cbo_division_id').val()+
								'&department_id='+$('#cbo_department_id').val()+
								'&section_id='+$('#cbo_section_id').val()+
								'&subsection_id='+$('#cbo_subsection_id').val()+
								'&emp_code='+$('#txt_emp_code').val();
								
					nocache = Math.random();
					http.open('get','../save_update_common.php?action=action_user_creation_costcenter_wise'+data+'&nocache='+nocache);
					http.onreadystatechange =response_user_creation_costcenter_wise;
					http.send( null );
				}
			}
			
			function response_user_creation_costcenter_wise() {
				if( http.readyState == 4 ) {
					var response = http.responseText.split('_');
					
					if( response[0] == 3 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
					}
					else if( response[0] == 1 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('User Created Successfully').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
							//save_activities_history('',"admin","create_user","insert","../");
							//document.getElementById('txt_update_id').value = "";
							reset_frm();
						});
					}
					else if( response[0] == 2 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Information Updated Successfully.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
							//save_activities_history('',"admin","create_user","update","../");
							//document.getElementById('txt_update_id').value = "";
						});
					}
					else if( response[0] == 7 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html(response[1]).addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
					}
					/*
					if( response[0] == 1 || response[0] == 2 ) 
					{
						document.getElementById('txt_user_id').value		= "";
						document.getElementById('txt_passwd').value			= "";
						document.getElementById('txt_conf_passwd').value	= "";
						document.getElementById('cbo_user_level').value		= 0;
						document.getElementById('cbo_user_buyer').value		= 0;
						document.getElementById('cbo_unit_name').value		= 0;
						document.getElementById('txt_ip_addres').value		= "";
						document.getElementById('txt_exp_date').value		= "";
						document.getElementById('cbo_user_sts').value		= 1;
					}
					*/
				}
			}
			
			// user_id_update
			function update_user_creation( id, type ) 
			{
				//alert("su..re");
				ajax.requestFile = '../get_data_update.php?getClientId=' + id + '&type=' + type; // Specifying which file to get
				ajax.onCompletion = response_update_user_creation;	// Specify function that will be executed after file has been found
				ajax.runAJAX();	
			}
			
			function response_update_user_creation() {
				var formObj = document.forms['frm_user_creation'];
				//alert(ajax.response);
				eval( ajax.response );
			}
			
			function reset_frm()
			{
				<!--$('#data_panel').hide();-->
				document.getElementById('frm_user_creation').reset();
			}
        
            function show_hide_buyer(str)
            {
                if (str==4)
                {
                    document.getElementById('buyer_user_tr').style.visibility="visible";
                }
                else
                {
                    document.getElementById('buyer_user_tr').value="0";
                    document.getElementById('buyer_user_tr').style.visibility="collapse";
                }
            }
            
            function openmypage(page_link,title)
            {
                var unit_name=document.getElementById('cbo_unit_name').value;
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+'&unit_name='+unit_name, title, 'width=630px,height=330px,center=1,resize=0,scrolling=0',' ')
                
                emailwindow.onclose=function()
                {
                    var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
                    var order_id=this.contentDoc.getElementById("txt_selected_id")
                    var order_no=this.contentDoc.getElementById("txt_selected") //Access form field with id="emailfield"
                    if (title=="Company Selection")
                    {
                        document.getElementById('cbo_unit_name_show').value=order_no.value;	
                        document.getElementById('cbo_unit_name').value=order_id.value;	
                    }
                    else
                    {
                        document.getElementById('cbo_user_buyer_show').value=order_no.value;	
                        document.getElementById('cbo_user_buyer').value=order_id.value;	
                    }
                }
            }
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
        </script>
    </head>
	<body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; margin-bottom:5px; margin-top:5px;">		
        <div style="height:60px;">
            <div class="form_caption" style="height:25px;">User Creation</div>
            <div id="permission_caption" style="height:15px;"><? echo "Your Permissions--> \n".$insert.$update.$delete;?></div>
            <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
        </div>
        <form name="frm_user_creation" id="frm_user_creation" action="javascript:func_save_user_creation(save_perm,edit_perm,delete_perm,approve_perm);">
            <fieldset>
                <fieldset><legend>User Info</legend>
                    <div style="border: 1px solid #555FFF; width:620px; padding-top:10px; padding-bottom:10px;" align="center">
                        <table align="center" width="580px" cellpadding="2" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td width="100px"><strong>User ID</strong></td>
                                    <td width="150px">
                                    	<input type="text" name="txt_user_id" id="txt_user_id" class="text_boxes" onBlur="update_user_creation(this.value,'type_user_creation')" style="width:150px;" />
                                    </td>
                                    <td width="10px">&nbsp;</td>
                                    <td width="170px"><strong>Expiry Date</strong></td>
                                    <td width="150px"><input type="text" name="txt_expiry_date" id="txt_expiry_date" class="datepicker" style="width:150px;" /></td>
                                </tr>
                                <tr>
                                    <td width="100px"><strong>Password</strong></td>
                                    <td width="150px"><input type="password" name="txt_password" id="txt_password" class="text_boxes" style="width:150px;" /></td>
                                    <td width="10px">&nbsp;</td>
                                    <td width="170px"><strong>Confirm Password</strong></td>
                                    <td width="150px"><input type="password" name="txt_confirm_password" id="txt_confirm_password" class="text_boxes" style="width:150px;" /></td>
                                </tr>
                                <tr>
                                    <td width="100px"><strong>User Level</strong></td>
                                    <td width="150px">
                                        <select name="cbo_user_level" id="cbo_user_level" class="combo_boxes" style="width:160px" >
                                            <option value="0">-- Select --</option>
                                            <option value="2">Admin User</option>
                                            <option value="1">General User</option>
                                            <option value="5">Self User</option>
                                            <option value="4">Buyer User</option>
                                            <option value="3">Demo User</option>
                                        </select>
                                    </td>
                                    <td width="10px">&nbsp;</td>
                                    <td width="170px"><strong>Data Level Security</strong></td>
                                    <td width="150px">
                                        <select name="cbo_data_level_security" id="cbo_data_level_security" class="combo_boxes" style="width:160px">
                                            <option value="0">Access All Data</option>
                                            <option value="1">Limited Access</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100px"><strong>Bind to IP</strong></td>
                                    <td width="150px">
                                        <input type="text" name="txt_ip_addres" id="txt_ip_addres" class="text_boxes" placeholder="Comma Seperate LAN and WAN IP" style="width:150px;" />
                                    </td>
                                    <td width="10px">&nbsp;</td>
                                    <td width="170px"><strong>Status</strong></td>
                                    <td width="150px">
                                        <select name="cbo_user_status" id="cbo_user_status" class="combo_boxes" style="width:160px;">
                                            <option value="1">Active</option>
                                            <option value="2">Inactive</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>           
                    </div>
                </fieldset>
                <div style="height:5px;"></div>
                <fieldset><legend>Cost Center Permission</legend>
                    <div style="border: 1px solid #555FFF; width:620px; padding-top:10px; padding-bottom:10px;" align="center">
                        <table width="570px" cellpadding="2" cellspacing="0" id="tbl_training" align="center">
                            <tbody>
                                <tr>
                                    <td width="100px"><strong>Company</strong></td>
                                    <td width="180px">
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" multiple="multiple" style="width:180px;">
                                            <?php 
                                                foreach($company_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </td>
                                    <td width="10px">&nbsp;</td>
                                    <td width="100px"><strong>Location</strong></td>
                                    <td width="180px">
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" multiple="multiple" style="width:180px;">
                                            <?php 
                                                foreach($location_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100px"><strong>Division</strong></td>
                                    <td width="180px">
                                        <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" multiple="multiple" style="width:180px;">
                                            <?php 
                                                foreach($division_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </td>
                                    <td width="10px">&nbsp;</td>
                                    <td width="100px"><strong>Department</strong></td>
                                    <td width="180px">
                                        <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" multiple="multiple" style="width:180px;">
                                            <?php 
                                                foreach($department_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100px"><strong>Section</strong></td>
                                    <td width="180px">
                                        <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" multiple="multiple" style="width:180px;">
                                            <?php 
                                                foreach($section_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </td>
                                    <td width="10px">&nbsp;</td>
                                    <td width="100px"><strong>Subsection</strong></td>
                                    <td width="180px">
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" multiple="multiple" style="width:180px;">
                                            <?php 
                                                foreach($subsection_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100px"><strong>System Code</strong></td>
                                    <td width="180px"><input type="text" name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:170px;" /></td>
                                    <td width="10px">&nbsp;</td>
                                    <!--
                                    <td width="100px"><strong>User Group</strong></td>
                                    <td width="150px">
                                        <select name="cbo_user_group" id="cbo_user_group" class="combo_boxes" style="width:180px;">
                                            <option value="0">-- select --</option>
                                            <?php 
												/*
                                                foreach($subsection_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php //echo $key; ?>"><?php //echo $value; ?></option>
                                                    <?php
                                                }
												*/
                                            ?>
                                        </select>
                                    </td>
                                    -->
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding-top:10px;">
                            <input type="submit" name="save" id="save" value="Save"  class="formbutton" style="width:100px;"/>
                            <input type="button" name="cancel" id="cancel" value="Refresh"  class="formbutton" onclick="reset_frm()" style="width:100px;" />
                            <input type="hidden" name="txt_update_id" id="txt_update_id" />
                        </div>
                        </div>
                    </fieldset>
                </fieldset>
            </form>  
        </div>
    </body>
</html>