<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../includes/common.php');
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
	
    <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../includes/ajax_submit.js" type="text/javascript"></script>
	<script src="../includes/functions.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
    <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center">
		<div align="center" style="width:1024px; position:relative; height:60px; margin-bottom:5px; margin-top:5px">
			<h3>Set User Privilege</h3>
            <span><p id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update.$delete;?></p></span>
			<div align="center" id="messagebox"></div>	
		</div>
		<form name="user_priv" id="user_priv" method="" action="javascript:fnc_user_priviledge(save_perm,edit_perm,delete_perm,approve_perm)">
			<fieldset style="width:1024px;">
				<legend>Select user and module</legend>
				<table width="100%">
					<tr>
						<td width="10%">User ID</td>
						<td width="25%">
							<select name="cbo_user_name" id="cbo_user_name" tabindex="1" style="width:250px" class="combo_boxes">
								<option value="0">-- Select --</option>
								<?php 
								$sql = "select * from user_passwd  where valid = 1 order by user_name";
								$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
								while( $r_mod = mysql_fetch_array( $result ) ) {
								?>
								<option value="<?php echo $r_mod["id"]; ?>" <?php if( $company_combo == $r_mod["id"] ) echo "selected"; ?>><?php echo "$r_mod[user_name]" ?> </option>
								<?php } ?>
							</select>
						</td>
						<td width="15%">Main Module Name</td>
						<td>
							<select name="cbo_main_module" tabindex="1"  id="cbo_main_module" style="width:250px"  class="combo_boxes" onchange="loadpage_data( document.getElementById('cbo_user_name').value, this.value );">
								<option value="0">-- Select --</option>
								<?php
								$mod_sql = mysql_query( "select * from main_module where status='1' order by m_mod_id" );
								while( $r_mod = mysql_fetch_array( $mod_sql ) ) {
								?>
								<option value="<?php echo $r_mod['m_mod_id']; ?>" <?php if( $company_combo == $r_mod['m_mod_id'] ) echo "selected"; ?>><?php echo "$r_mod[main_module]" ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
				</table>
			</fieldset>
			<div id="user_prev" align="center"></div>
		</form>
	</div>
</body>
</html>