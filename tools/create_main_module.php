<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";
?>
<style type="text/css">
	.formbutton { width:100px; }
</style>
	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../includes/ajax_submit.js" type="text/javascript"></script>
	<script src="../includes/functions.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
    <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>

<script>
	document.onkeydown = checkKeycode;
	
	
//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
} 

</script>
 <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>
<div align="center" style="width:1000px;">
	<div style="height:  width:100%" contenteditable="true">
		<h3>Main Module Entry and Edit</h3>
        <p id="permission_caption">
						<? 
                            echo "Your Permissions--> \n".$insert.$update.$delete;
                        ?>
         		</p>
         <div id="messagebox" style="background-color:#FF9999; color:#000000; width:550px" align="center"></div>
         <!--    <form action="" method="post">  
        <label for="username">Create a Username: </label>  
        <input type="text"  
           name="username"  
           id="username"  
           placeholder="4 <> 10"  
           pattern="[A-Za-z]{4,10}"  
           autofocus  
           required>  
        <button type="submit">Go </button>  
    </form>   -->
	</div>
	<fieldset style="width:500px">
		<legend>Main Module</legend>
		<form name="asd" id="asd" method="" action="javascript:fnc_main_module(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">
			<table>
				<tr>
					<td>Main Module Name</td>
					<td>
						<input type="text" name="txt_module_name" id="txt_module_name" class="text_boxes" style="width:250px" />
						<div id="livesearch" style="width:150px"></div>
					</td>
				</tr>
				<tr>
					<td>Main Module Link</td>
					<td><input type="text" name="txt_module_link" id="txt_module_link" class="text_boxes" style="width:250px" /></td>
				</tr>
				<tr>
				<td>Sequence</td>
				<td>
					<div style="float:left" id="mod_seq_mod_create">
						<div style="float:left"> 
							<input type="text" name="txt_module_seq" id="txt_module_seq" class="text_boxes" onKeyDown="javascript:checkKeycode(this.event,2)" style="width:100px" onkeypress="return numbersonly(this,event)" />
						</div>
						
						<div style="float:left"> 
							&nbsp;Status
							<select name="cbo_module_sts" id="cbo_module_sts"   class="combo_boxes" style="width:86px" >
								<option	 value="1" >Visible</option>
								<option	 value="2" >Not visible</option>
							</select>
						</div>
					</div>
				</td>
				</tr>
				<tr>
					<td><input type="text" value="" name="save_up" id="save_up" style="visibility:hidden"/></td>
					<td align="left" style="padding-top:10px;">
						<input type="submit" name="save" id="save" class="formbutton" value="Save" />
						&nbsp;&nbsp;<input type="reset" name="reset" id="reset" class="formbutton" value="Refresh" />
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	
	<div style="width:1000px; float:left; margin:auto" align="center">
		<fieldset style="width:500px">
			<legend>Enter search words or press space</legend>
			<form>
				<input type="text" size="30" class="text_boxes" onKeyUp="showResult(this.value,'1','livesearch1')" />
				<div style="width:450px; margin-top:10px" id="livesearch1" align="left"></div>
			</form>
		</fieldset>
		<?php // include('list_view.php'); ?>
	</div>
</div>