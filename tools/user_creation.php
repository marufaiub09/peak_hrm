<?php
	session_start();
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	extract($_GET);
	$permission=explode('_',$permission);
	 
	if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
	if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
	if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
	if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";
	
	//--------------------------------------------------------------------------------------------------------------------
	
	include('../includes/common.php');
	// Buyer or Party details
	/*
	$sql= "select * from lib_buyer where is_deleted=0  and status_active=1 order by buyer_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$party_details = array();
	
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$party_details[$row['id']] = array();
		foreach( $row AS $key => $value ) 
		{
			$party_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cost Sheets</title>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../includes/ajax_submit.js" type="text/javascript"></script>
        <script src="../includes/functions.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        <script language="javascript">
            function show_hide_buyer(str)
            {
                if (str==4)
                {
                    document.getElementById('buyer_user_tr').style.visibility="visible";
                }
                else
                {
                    document.getElementById('buyer_user_tr').value="0";
                    document.getElementById('buyer_user_tr').style.visibility="collapse";
                }
            }
            
            function openmypage(page_link,title)
            {
                var unit_name=document.getElementById('cbo_unit_name').value;
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+'&unit_name='+unit_name, title, 'width=630px,height=330px,center=1,resize=0,scrolling=0',' ')
                
                emailwindow.onclose=function()
                {
                    var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
                    var order_id=this.contentDoc.getElementById("txt_selected_id")
                    var order_no=this.contentDoc.getElementById("txt_selected") //Access form field with id="emailfield"
                    if (title=="Company Selection")
                    {
                        document.getElementById('cbo_unit_name_show').value=order_no.value;	
                        document.getElementById('cbo_unit_name').value=order_id.value;	
                    }
                    else
                    {
                        document.getElementById('cbo_user_buyer_show').value=order_no.value;	
                        document.getElementById('cbo_user_buyer').value=order_id.value;	
                    }
                }
            }
        </script>
        <script>
            var save_perm 		= <?php echo $permission[0]; ?>;
            var edit_perm 		= <?php echo $permission[1]; ?>;
            var delete_perm		= <?php echo $permission[2]; ?>;
            var approve_perm	= <?php echo $permission[3]; ?>;
        </script>
    </head>
    <body onLoad="">
        <div style="border:solid solid solid solid; border-width:2px; border-color:#FF0000;height:300px" align="center">
            <form name="user_creation_form" id="user_creation_form" method="POST" action="javascript:fnc_user_creation(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete="off">
                <div style="width:600px; margin-bottom:5px; margin-top:5px">
                    <div><span style="text-size:12px;font-family:"times new roman", times, serif;"><strong>User Management</strong></span>
                    <p id="permission_caption">
                        <?php
                            echo "Your Permissions--> \n".$insert.$update.$delete;
                        ?>
                    </p>
                    <div id="messagebox" align="center" style="width:550px"></div>	
                    </div>
                    <fieldset style="width:550px; visibility:collapse;">
                        <legend>Employee Information</legend>
                        <div style="width:50%; float:left; visibility:collapse" align="right">
                            <table>
                                <tr>
                                    <td>Employee ID</td>
                                    <td>
                                        <div style="width:100%;">
                                            <div style="float:left">
                                                <input type="text" name="txt_emp_id" tabindex="1" id="txt_emp_id" class="text_boxes" style="width:140px" onFocus="javascript: show_div('div_emp_id'); javascript:hide_div('div_user_id');" onBlur="javascript:get_emp_user_Data(this.value,3);" /></div><div class="tips_cls" id="div_emp_id" style="float:left"> <a href="##" style="text-decoration:none; cursor: help; " class="tooltip" title=""></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Employee Name</td>
                                    <td><input type="text" name="txt_emp_name" id="txt_emp_name" readonly class="text_boxes" style="width:140px" /></td>
                                </tr>
                            </table>
                        </div>
                        <div style=" width:50%; float:left; visibility:collapse" align="left">
                            <table>
                                <tr>
                                    <td>Designation</td>
                                    <td><input type="text" name="txt_desig" id="txt_desig" readonly class="text_boxes" style="width:150px" /></td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                    <td><input type="text" name="txt_dept" id="txt_dept" readonly class="text_boxes" style="width:150px" /></td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                    <fieldset style="width:550px;">
                        <legend> Accounts Information</legend>
                        <div style="width:100%; float:left;" align="center">
                            <table>
                                <tr>
                                    <td>User ID</td>
                                    <td>
                                        <div style="float:left">
                                            <input type="text" name="txt_user_id" tabindex="2" id="txt_user_id" class="text_boxes" style="width:220px" onBlur="user_id_update(this.value,'user_id_update')" />
                                        </div>
                                         
                                    </td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td>
                                        <div style="float:left">
                                            <input type="password" name="txt_passwd" id="txt_passwd" tabindex="3" class="text_boxes" style="width:220px"  />
                                        </div>
                                         
                                    </td>
                                </tr>
                                <tr>
                                    <td>Confirm Password</td>
                                    <td>
                                        <div style="float:left">
                                            <input type="password" name="txt_conf_passwd" id="txt_conf_passwd" tabindex="4" class="text_boxes" style="width:220px"  />
                                        </div>
                                         
                                    </td>
                                </tr>
                                <tr>
                                    <td>User Level</td>
                                    <td>
                                        <select name="cbo_user_level" id="cbo_user_level" class="combo_boxes" tabindex="5" style="width:232px" >
                                            <option value="0">-- Select --</option>
                                            <option value="1">General User</option>
                                            <option value="2">Admin User</option>
                                            <option value="3">Demo User</option>
                                            <option value="4">Buyer User</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr style="display:none">
                                    <td>Buyer Name</td>
                                    <td>
                                    <input type="text" name="cbo_user_buyer_show" placeholder="Double Click for Buyer" id="cbo_user_buyer_show" class="text_boxes" style="width:220px" ondblclick="openmypage('../user_comp_buyer_pop.php?type=buyer_sel','Buyer Selection')">
                                    <input type="hidden" name="cbo_user_buyer" id="cbo_user_buyer" class="text_boxes" style="width:220px" >
                                         
                                    </td>
                                </tr>
                                <tr>
                                    <td>Unit Name</td>
                                    <td>
                                    <input type="text" name="cbo_unit_name_show" id="cbo_unit_name_show" placeholder="Double Click for Company" class="text_boxes" style="width:220px" ondblclick="openmypage('../user_comp_buyer_pop.php?type=company_sel','Company Selection')" readonly="readonly">
                                    <input type="hidden" name="cbo_unit_name" id="cbo_unit_name" class="text_boxes" style="width:220px" >
                                     
                                    </td>
                                </tr>
                                <tr>
                                    <td>Data Level Security</td>
                                    <td>
                                        <select name="cbo_data_level_sec" id="cbo_data_level_sec" class="combo_boxes" tabindex="6" style="width:232px">
                                            <option value="0">Access All Data</option>
                                             <option value="1">Limited Access</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bind to IP </td>
                                    <td><input type="text" name="txt_ip_addres" placeholder="Comma Seperate LAN and WAN IP" id="txt_ip_addres" tabindex="7" class="text_boxes" style="width:220px"></td>
                                </tr>
                                <tr>
                                    <td>Expiry Date</td>
                                    <td>
                                        <input type="text" size="12" name="txt_exp_date" id="txt_exp_date" tabindex="8" class="text_boxes"  style="width:80px"/>
                                        <script type="text/javascript">
                                            $( "#txt_exp_date" ).datepicker({
                                            dateFormat: 'dd-mm-yy',
                                            changeMonth: true,
                                            changeYear: true
                                            });
                                        </script>
                                        &nbsp;Status
                                        <select name="cbo_user_sts" id="cbo_user_sts" class="combo_boxes" tabindex="9" style="width:92px">
                                            <option value="1">Active</option>
                                            <option value="2">Inactive</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;<input type="hidden" value="" name="save_up" id="save_up"/></td>
                                    <td align="center" style="padding-top:10px;">
                                        <input type="submit" name="save" id="save" tabindex="10" class="formbutton" value="Save" style="width:70px;" />
                                        <input type="reset" name="reset" id="reset" class="formbutton" value="Refresh" style="width:70px;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </body>
</html>