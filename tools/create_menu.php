<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
date_default_timezone_set('UTC');


?>
<style type="text/css">
	.formbutton { width:100px; }
</style>
    <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
 
	<script src="../includes/ajax_submit.js" type="text/javascript"></script>
	<script src="../includes/functions.js" type="text/javascript"></script>
 
	<script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
    <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
<script>
	document.onkeydown = checkKeycode;
	
	
//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
} 


</script>
<script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
</script>
<body>
	<div align="center" style="width:1000px; margin-top:0px">
		<div style="width:100%; height:80px">
			<h3>Menu Entry and Edit</h3>
             <p id="permission_caption">
						<? 
                            echo "Your Permissions--> \n".$insert.$update.$delete;
                        ?>
         		</p>
			<div id="messagebox" style="background-color:#FF9999; color:#000000; width:520px"  align="center"></div>
		</div>
		<fieldset style="width:520px">
			<legend> Menu Creation</legend>
			<form name="menu_create" id="menu_create" method="" action="javascript:fnc_main_menu(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete="off">
				<table>
					<tr>
						<td>Main Module Name</td>
						<td>
							<select name="cbo_module_name" id="cbo_module_name"  onChange="load_drop_down(this.value,'1','rootdiv')" class="combo_boxes" style="width:238px" >
								<option value="0">--Select One--</option>
								<?php
								$mod_sql= mysql_db_query($DB, "select * from main_module order by m_mod_id");
								while ($r_mod=mysql_fetch_array($mod_sql))
								{
								?>
								<option value=<?php echo $r_mod["m_mod_id"];
								if ($company_combo==$r_mod["m_mod_id"]){?> selected <?php }?>><?php echo "$r_mod[main_module]" ?> </option>
								<?php
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Menu Name</td>
						<td><input type="text" name="txt_menu_name" id="txt_menu_name" class="text_boxes" style="width:238px" /></td>
                        <td><input type="checkbox" name="chk_self_service" id="chk_self_service" />Self Service</td>
					</tr>
					<tr>
						<td>Menu Link</td>
						<td><input type="text" name="txt_menu_link" id="txt_menu_link" class="text_boxes" style="width:238px" /></td>
					</tr>
					<tr>
						<td>Root Menu</td>
						<td>
							<div id="rootdiv">
								<select name="cbo_root_menu" id="cbo_root_menu" class="combo_boxes" style="width:238px">
									<option value="0">--Select One--</option>
									<?php
									$mod_sql= mysql_db_query($DB, "select * from main_menu where position='1' order by m_menu_id");
									while ($r_mod=mysql_fetch_array($mod_sql))
									{
									?>
									<option value=<?php echo $r_mod["m_menu_id"];
									if ($company_combo==$r_mod["m_menu_id"]){?> selected <?php }?>><?php echo "$r_mod[menu_name]" ?> </option>
									<?php
									}
									?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Root Menu Under</td>
						<td>
							<div id="subrootdiv">
								<select name="cbo_root_menu_under" id="cbo_root_menu_under" onFocus="javascript:hide_div('div_user_id');" class="combo_boxes" style="width:238px">
									<option value="0">--Select One--</option>
									<?php
									$mod_sql= mysql_db_query($DB, "select * from main_menu where position='2' order by m_menu_id");
									while ($r_mod=mysql_fetch_array($mod_sql))
									{
									?>
									<option value=<?php echo $r_mod["m_menu_id"];
									if ($company_combo==$r_mod["m_menu_id"]){?> selected <?php }?>><?php echo "$r_mod[menu_name]" ?> </option>
									<?php
									}
									?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Sequence</td>
						<td>
							<div style="float:left" id="menu_seq_menu_create">
							<div style="float:left"> 
								<input type="text" name="txt_menu_seq" id="txt_menu_seq" class="text_boxes" onKeyDown="javascript:checkKeycode(this.event,1)" style="width:80px" onFocus=" javascript:show_div('div_user_id')" onKeyPress="return numbersonly(this,event)" />
							</div>
							<!--<div class="tips_cls" id="div_user_id" style="float:left"> 
								<a href="##" style="text-decoration:none; cursor: help;" class="tooltip"><img src="images/11533979561754326101.png" onMouseOver="if(t1)t1.Show(event,show_next_id)" onMouseOut="if(t1)t1.Hide(event)" height="18" width="20"/></a>
							</div>-->
							<div style="float:left"> 
								&nbsp;Status
								<select name="cbo_menu_sts" id="cbo_menu_sts" onFocus="javascript:hide_div('div_user_id');" class="combo_boxes" style="width:97px" >
									<option	 value="1" >Visible</option>
									<option	 value="2" >Not visible</option>
								</select>
							</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;<input type="text" value="" name="save_up" id="save_up" style="visibility:hidden"/></td>
						<td align="left" style="padding-top:10px;">
							<input type="submit" name="save" id="save" class="formbutton" value="Save" />
							&nbsp;&nbsp;<input type="reset" name="reset" id="reset" class="formbutton" value="Refresh" />
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
		<div style="width:1000px; padding-top:5px; float:left; margin:auto" align="center">
			<fieldset style="width:520px">
				<legend>Enter search words or press space</legend>
				<form>
					<input type="text" size="30" class="text_boxes" onKeyUp="showResult( this.value, 2, 'livesearch2' )" />
					<div style="width:450px; margin-top:10px" id="livesearch2" align="left"></div>
				</form>
			</fieldset>
		</div>
	</div>
</body>