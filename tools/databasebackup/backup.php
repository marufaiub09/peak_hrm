

<?php 
/***************************************
|	Developed By	: Arif Rupom
|	Date			: 09.03.2015
|	Developed By	: 
|	Date			: 
|	Purpose			: Visitor Information 
*****************************************/
include("../../includes/common.php");
include("../../includes/array_function.php");

// 1=limited access and 2=self user
if ($_SESSION['logic_erp']["data_level_secured"]==1 || $_SESSION['logic_erp']["data_level_secured"]==2) 
{
	// company_id
	if( $_SESSION['logic_erp']['company_id'] == 0 || $_SESSION['logic_erp']['company_id']=="") $user_company_id="";
	else $user_company_id="id in(".$_SESSION['logic_erp']['company_id'].") and";
}

// all access
if ($_SESSION['logic_erp']["data_level_secured"]==0) $user_company_id="";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
		<script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" type="text/css"  rel="stylesheet" media="screen">
        <script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
       	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
         
        <script src="../../resources/ui.tabs.paging.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../js/ajaxupload.3.5.js" ></script>
        
		<script type="text/javascript" src="../../js/driver.phonetic.js" ></script>
        <script type="text/javascript" src="../../js/driver.probhat.js" ></script>
        <script type="text/javascript" src="../../js/engine.js" ></script>
        
        <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script src="../../includes/update_status.js" type="text/javascript"></script>
		<script>
			
		function fnc_create_backup()
		{
		$('#datahtml').html('<img src="Loading2.gif" />');
		document.getElementById('msg').innerHTML='DataBase Backup Processing....';

		var d= $.ajax({
			url: "backup_db.php",
			async: false
		}).responseText
		
		var respnse=d.split("**");
		var w = window.open( respnse[1] , "#");
		$('#datahtml').hide();
		$('#msg').hide();
		document.getElementById('messagebox').innerHTML='Database Backup Successfully Completed...';
		return;
		}

        </script>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px;">		
            <div class="form_caption" style="width:100%; background-color:#A6CAF0; padding-top:5px; height:25px; border-radius:5px; color:#000;" align="center">Database backup</div>
            <div style="height:19px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; border-radius:5px; width:100%;" align="center"></div></div>
            <div id="container_compliance_tracker">
                    <fieldset style="width:420px;">
                    <legend>Database Backup</legend> 
                    <form name="databaseBackup_1" id="databaseBackup_1"  method="post"> 
                    <table width="400" align="center" border="0">
                        <tr style="display:none">
                            <td width="110" class="must_entry_caption"><b>Database Name</b></td>
                            <td>
                                <input type="text" class="text_boxes" name="dbName" id="dbName"  value="testhrmup" style="width:200px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input type="button" name="submit" onClick="fnc_create_backup()" value="Create Backup" class="formbutton" style="width:100px" />
                            </td>
                        </tr>
                    </table>
                     <div id='datahtml'></div>
                     <div id="msg" style="color:red; text-align:right;"></div>     
                     
                    </form>
                    </fieldset>
            	<div style="height:5px;">&nbsp;</div>   
            </div>
        </div>
    </body>
</html>