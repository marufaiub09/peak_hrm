<?php
/***************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 18.08.2014
*****************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
// Buyer or Party details
// company
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}
// location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}
// division
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}
// department
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}
// section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}
// subsection
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}
// designation
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}

//user_group_priviledge
$sql = "SELECT user_group_id,user_group_name FROM user_group_priviledge WHERE status_active=1 and is_deleted = 0 group by user_group_id ORDER BY user_group_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$user_group_dtls = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$user_group_dtls[$row['user_group_id']] = mysql_real_escape_string( $row['user_group_name'] );		
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Cost Sheets</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../includes/ajax_submit.js" type="text/javascript"></script>
        <script src="../includes/functions.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        
		<!--
        <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        -->
        
        <script language="javascript">
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
            });
			
			//Do not change 
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer") {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				}
				else request_type = new XMLHttpRequest();
				
				return request_type;
			}
			var http = createObject();
			
			// func_save_user_creation
			function func_save_user_creation(save_perm,edit_perm,delete_perm,approve_perm) 
			{
				//alert("su..re");
				$('#save').attr('disabled','disabled');
				
				var valid = true;
				
				/*
				var txt_user_id = encodeURI(document.getElementById('txt_user_id').value);
				var txt_passwd = encodeURI(document.getElementById('txt_passwd').value);
				var txt_conf_passwd = encodeURI(document.getElementById('txt_conf_passwd').value);
				var cbo_user_level = encodeURI(document.getElementById('cbo_user_level').value);
				var cbo_user_buyer = encodeURI(document.getElementById('cbo_user_buyer').value);
				var cbo_data_level_sec = encodeURI(document.getElementById('cbo_data_level_sec').value);
				
				var cbo_unit_name = encodeURI(document.getElementById('cbo_unit_name').value);
				var txt_ip_addres = encodeURI(document.getElementById('txt_ip_addres').value);
				var txt_exp_date = encodeURI(document.getElementById('txt_exp_date').value);
				var cbo_user_sts = encodeURI(document.getElementById('cbo_user_sts').value);
				var save_up = encodeURI(document.getElementById('save_up').value);
				*/
				
				if( $('#txt_user_id').val() == "" ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#save').removeAttr('disabled');
						$('#txt_user_id').focus();
						$(this).html('Please Enter a unique user name').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#txt_password').val() == "" ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#save').removeAttr('disabled');
						$('#txt_password').focus();
						$(this).html('Please Enter Password').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if($('#txt_password').val().length <4 ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#save').removeAttr('disabled');
						$('#txt_password').focus();
						$(this).html('Please Enter a Password of at least 4 characters..').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#txt_confirm_password').val() == "" ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#save').removeAttr('disabled');
						$('#txt_confirm_password').focus();
						$(this).html('Please Confirm  same password.').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#txt_confirm_password').val() != $('#txt_password').val() ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#save').removeAttr('disabled');
						$('#txt_confirm_password').focus();
						$(this).html('New Password and Confirm password does not match.').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#cbo_user_level').val() == 0 ) 
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#save').removeAttr('disabled');
						$('#cbo_user_level').focus();
						$(this).html('Please Select an user level.').addClass('messageboxerror').fadeTo( 900, 1 );
						$(this).fadeOut(5000);
					});
					valid = false;
				}
				else if( $('#cbo_data_level_security').val() == 2 ) 
				{
					if( $('#txt_emp_code').val() == "" )
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$('#save').removeAttr('disabled');
							$('#txt_emp_code').focus();
							$(this).html('Please select employee code.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
						valid = false;
					}
				}
				
				else if( $('#txt_ip_addres').val() != '' ) 
				{
					var ip = $('#txt_ip_addres').val();
					var ipsub = new Array();
					ipsub = ip.split(".");
					
					if( ipsub.length < 4 )
					{
						$('#save').removeAttr('disabled');
						 valid = false;
					}
					else 
					{
						for( var i = 0; i < ipsub.length; i++ ) 
						{
							var sub = parseInt( ipsub[i] );
							if( ( isNaN( sub ) ) || ( i == 0 && sub == 0 ) || ( i == 3 && sub == 0 ) || ( sub < 0 ) || ( sub >255 ) ) 
							{
								$('#save').removeAttr('disabled');
								valid = false;
							}
						}
					}
					
					if( valid == false ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$('#txt_ip_addres').focus();
							$('#save').removeAttr('disabled');
							$(this).html('Please provide a correct IP address.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
					}
				}
				if( valid == true ) 
				{
					var data=	'&update_id='+$('#txt_update_id').val()+
								'&user_id='+$('#txt_user_id').val()+
								'&emp_code='+$('#txt_emp_code').val()+
								'&password='+$('#txt_password').val()+
								'&confirm_password='+$('#txt_confirm_password').val()+
								'&user_level='+$('#cbo_user_level').val()+
								'&data_level_security='+$('#cbo_data_level_security').val()+
								'&email_address='+$('#txt_email_address').val()+
								'&ip_addres='+$('#txt_ip_addres').val()+
								'&expiry_date='+$('#txt_expiry_date').val()+
								'&user_status='+$('#cbo_user_status').val();
								
					
					var data_2='';
					var i=1;				
					if($('#cbo_data_level_security').val()==1)
					{
						var rowCount=$("#tbl_user_creation_costcenter tbody tr").length;
						//alert(rowCount); return;
						for(i;i<=rowCount;i++ )
						{
							if($('#cbo_company_id_'+i).val()==0)
							{
								$("#messagebox").fadeTo( 200, 0.1, function() {
									$('#save').removeAttr('disabled');
									$('#cbo_company_id_'+i).focus();
									$(this).html('Please select company.').addClass('messageboxerror').fadeTo( 900, 1 );
									$(this).fadeOut(5000);
								});
								return;	
							}
							else 
							{
								data_2 +=	$('#cbo_company_id_'+i).val()+'_'+ 
											$('#cbo_location_id_'+i).val()+'_'+ 
											$('#cbo_division_id_'+i).val()+'_'+ 
											$('#cbo_department_id_'+i).val()+'_'+ 
											$('#cbo_section_id_'+i).val() +'_'+ 
											$('#cbo_subsection_id_'+i).val()+'_'+
											$('#txt_emp_code_'+i).val()+'_'+
											$('#txt_db_row_id_'+i).val() +'|';
							}
						}
					}

					nocache = Math.random();
					http.open('get','../save_update_common.php?action=action_user_creation_costcenter_wise'+ data + '&data_2='+data_2 +'&nocache='+nocache);
					http.onreadystatechange =response_user_creation_costcenter_wise;
					http.send( null );
				}
			}
			
			function response_user_creation_costcenter_wise() 
			{
				if( http.readyState == 4 ) {
					var response = http.responseText.split('_');
					//alert(http.responseText);
					$('#save').removeAttr('disabled');
					if( response[0] == 3 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
					}
					else if( response[0] == 1 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('User Created Successfully').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
							reset_frm();
						});
					}
					else if( response[0] == 2 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Information Updated Successfully.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
							reset_frm();
						});
					}
					else if( response[0] == 7 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html(response[1]).addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
					}
				}
			}
			
			// onblur_user_creation
			function onblur_user_creation( id, type ) 
			{
				//alert("su..re");
				ajax.requestFile = '../get_data_update.php?getClientId=' + id + '&type=' + type; // Specifying which file to get
				ajax.onCompletion = response_update_user_creation;	// Specify function that will be executed after file has been found
				ajax.runAJAX();	
			}
			
			function response_update_user_creation() {
				var formObj = document.forms['frm_user_creation'];
				//alert(ajax.response);
				eval( ajax.response );
				change_data_level_security($('#cbo_data_level_security').val());
				if($('#txt_update_id').val()!="" && $('#cbo_data_level_security').val()==1)
				{
					populate_priviledge_costcenter($('#txt_update_id').val());
				}
			}
			
			function populate_priviledge_costcenter(id)
			{
				//alert("su..re");
				if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
				else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
				
				xmlhttp.onreadystatechange = function() {
					if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) 
					{
						//alert(xmlhttp.responseText);
						$('#tbl_user_creation_costcenter tbody tr').remove();
						//$('#tbl_user_creation_costcenter_tbody tr').remove();
						//document.getElementById("tbl_user_creation_costcenter").deleteRow(1);
						//document.getElementById('tbl_user_creation_costcenter_tbody').innerHTML =trim(xmlhttp.responseText);
						$("#tbl_user_creation_costcenter tbody:last").append(xmlhttp.responseText);	
					}
				}
				//xmlhttp.open( "GET", "../list_view.php?q=" + trim( str ) + "&qq=" + trim( str2 ) + "&type=" + type, true );
				xmlhttp.open( 'GET', '../list_view.php?type=generate_priviledge_costcenter' + '&user_id=' + id );
				xmlhttp.send();
			}
			
			function reset_frm()
			{
				document.getElementById('frm_user_creation').reset();
				$('#tbl_user_creation_costcenter tbody tr').remove();
				click_add_btn();			
			}
        
            function openpage_searchemp()
			{		
				//alert(page_link); '../hr_admin/search_employee_multiple_user_creation.php','Search Employee'
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','../hr_admin/search_employee_multiple_user_creation.php','Search Employee', 'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id");
					var thee_name = this.contentDoc.getElementById("txt_selected_name");
					var thee_designation = this.contentDoc.getElementById("txt_selected_designation");
					
					$('#txt_emp_code').val(thee_id.value);
					$('#txt_user_name').val(thee_name.value);
					$('#txt_designation').val(thee_designation.value);
				}
			}
			
            function openpage_searchemp_user_costcenter(id)
			{		
				//alert(id);
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','../hr_admin/search_employee_multiple_user_costcenter.php?company_id='+$('#cbo_company_id_'+id).val()+'&emp_code='+$('#txt_emp_code_'+id).val(),'Search Employee', 'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id");
					$('#txt_emp_code_'+id).val(thee_id.value);
					$('#hdn_emp_code_'+id).val( (thee_id.value).split(',').length );
					
					//varthee_id.split(',')
					//alert( (thee_id.value).split(',').length );
				}
			}
			
			function change_data_level_security(id)
			{
				//alert("su..re");
				if(id==0 || id==2)
				{
					$('#div_user_creation_costcenter').hide();
				}
				else 
				{
					$('#div_user_creation_costcenter').show();
				}
			}
			
			function click_add_btn()
			{
				//alert("su..re");
				var rowCount=$("#tbl_user_creation_costcenter tbody tr").length;
				rowCount++;

                $('#tbl_user_creation_costcenter tbody').append(
                    '<tr id="tr_id_' + rowCount + '"">'
						+'<td>'
							+'<select name="cbo_company_id[]" id="cbo_company_id_' + rowCount + '" class="combo_boxes" onchange="change_company(this.value,' + rowCount + ')" style="width:150px;">'
								+'<option value="0">-- Select --</option>'
								<?php 
									foreach($company_details as $key=>$value)
									{
										?>
										+'<option value="<?php echo $key; ?>"><?php echo $value; ?></option>'
										<?php
									}
								?>
							+'</select>'
						+'</td>'
						+'<td>'
							+'<select name="cbo_location_id[]" id="cbo_location_id_' + rowCount + '" class="combo_boxes" onchange="change_location(this.value,' + rowCount + ')" style="width:150px;">'
								+'<option value="0">-- Select --</option>'
							+'</select>'
						+'</td>'
                        +'<td>'
							+'<select name="cbo_division_id[]" id="cbo_division_id_' + rowCount + '" class="combo_boxes" onchange="change_division(this.value,' + rowCount + ')" style="width:150px;">'
								+'<option value="0">-- Select --</option>'
							+'</select>'
						+'</td>'
						+'<td>'
							+'<select name="cbo_department_id[]" id="cbo_department_id_' + rowCount + '" class="combo_boxes" onchange="change_department(this.value,' + rowCount + ')" style="width:150px;">'
								+'<option value="0">-- Select --</option>'
							+'</select>'
						+'</td>'
                        +'<td>'
							+'<select name="cbo_section_id[]" id="cbo_section_id_' + rowCount + '" class="combo_boxes" onchange="change_section(this.value,' + rowCount + ')" style="width:150px;">'
								+'<option value="0">-- Select --</option>'
							+'</select>'
						+'</td>'
						+'<td>'
							+'<select name="cbo_subsection_id" id="cbo_subsection_id_' + rowCount + '" class="combo_boxes" style="width:150px;">'
								+'<option value="0">-- Select --</option>'
							+'</select>'
						+'</td>'
						+'<td>'
							+'<input type="text" name="hdn_emp_code[]" id="hdn_emp_code_' + rowCount + '" class="text_boxes" placeholder="Browse" readonly="readonly" ondblclick="openpage_searchemp_user_costcenter(' + rowCount + ')" onmouseover="show_emp_code(' + rowCount + ');" style="width:150px;" />'
							+'<input type="hidden" name="txt_emp_code[]" id="txt_emp_code_' + rowCount + '" class="text_boxes" style="width:150px;" />'
						+'</td>'
						+'<td>'
							+'<input type="button" name="btn_remove" id="btn_remove" value="-" class="formbutton" onclick="click_remove_btn('+rowCount+');" style="width:70px;" />'
							+'<input type="hidden" name="txt_db_row_id[]" id="txt_db_row_id_' + rowCount + '" value="" class="text_boxes" style="width:70px;" />'
						+'</td>'
                    +'</tr>'
				);
			}
			
			function click_remove_btn(id)
			{
				//alert("su..re");
                var r=confirm("Are you sure want to REMOVE ROW !!");
				if(r==true)
				{
					if( $('#txt_db_row_id_'+id).val()=="" )
					{
						$('#tr_id_'+id).hide();
					}
					else 
					{
						$('#tr_id_'+id).hide();
						nocache = Math.random();
						http.open('get','../save_update_common.php?action=action_user_creation_costcenter_delete'+'&update_id='+$('#txt_update_id').val() + '&row_id='+$('#txt_db_row_id_'+id).val() +'&nocache='+nocache);
						http.onreadystatechange =response_user_creation_costcenter_delete;
						http.send( null );
					}
				}
			}
			
			function response_user_creation_costcenter_delete()
			{
				if( http.readyState == 4 ) 
				{
					var response = http.responseText.split('_');
					//alert(http.responseText);
					if( response[0] == 1 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Information Deleted Successfully.').addClass('messageboxerror').fadeTo( 900, 1 );
							$(this).fadeOut(5000);
						});
					}
				}
			}
			
			// change_company
			function change_company(id,cnt)
			{
				//alert("su..re");
				//alert(cnt);
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=type_change_company&id=' + id,
					success: function(html) {				
						$('#cbo_location_id_'+cnt).html( html )
					}
				});
				
				$('#cbo_division_id_'+cnt+' option[value!="0"]').remove();
				$('#cbo_department_id_'+cnt+' option[value!="0"]').remove();
				$('#cbo_section_id_'+cnt+' option[value!="0"]').remove();
				$('#cbo_subsection_id_'+cnt+' option[value!="0"]').remove();
			}
			
			// change_location
			function change_location(id,cnt)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=type_change_location&id=' + id,
					success: function(html) {				
						$('#cbo_division_id_'+cnt).html( html )
					}
				});
				
				$('#cbo_department_id_'+cnt+' option[value!="0"]').remove();
				$('#cbo_section_id_'+cnt+' option[value!="0"]').remove();
				$('#cbo_subsection_id_'+cnt+' option[value!="0"]').remove();
			}

			// change_division
			function change_division(id,cnt)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=type_change_division&id=' + id,
					success: function(html) {				
						$('#cbo_department_id_'+cnt).html( html )
					}
				});
				
				$('#cbo_section_id_'+cnt+' option[value!="0"]').remove();
				$('#cbo_subsection_id_'+cnt+' option[value!="0"]').remove();
			}
			
			// change_department
			function change_department(id,cnt)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=type_change_department&id=' + id,
					success: function(html) {				
						$('#cbo_section_id_'+cnt).html( html )
					}
				});
				
				$('#cbo_subsection_id_'+cnt+' option[value!="0"]').remove();
			}
			
			// change_section
			function change_section(id,cnt)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=type_change_section&id=' + id,
					success: function(html) {				
						$('#cbo_subsection_id_'+cnt).html( html )
					}
				});
			}
			
			function show_emp_code(id)
			{
				//alert("su..re");
				//$("#'txt_emp_code_'+id").css('word-wrap', 'break-word');
				document.getElementById('hdn_emp_code_'+id).title=$('#txt_emp_code_'+id).val();
				//document.getElementById('txt_emp_code_'+id).style.wordBreak;
				//$('#hdn_emp_code_'+id).css('word-wrap:break-word','70')=$('#txt_emp_code_'+id).val();
				
				//document.getElementById('divToolTip').style.visibility = 'visible';	
				//document.getElementById('divToolTip').innerHTML =$('#txt_emp_code_'+id).val();
			}
			/*
			function hide_emp_code(id)
			{
				document.getElementById('divToolTip').style.visibility = 'hidden';
			}
           	*/
		    var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
			
        </script>
		<style type="text/css">
        p{
            width:200px;
        }
        </style>			
        
    </head>
	<body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; margin-bottom:5px; margin-top:5px;">
        	<div class="form_caption" style="width:100%; background-color:#A6CAF0; padding-top:5px; height:25px; border-radius:5px; color:#000;" align="center">User Creation</div>
        	<div id="permission_caption" style="height:20px;"><? echo "Your Permissions--> \n".$insert.$update.$delete;?></div>	
            <div style="height:20px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; border-radius:5px; width:100%;" align="center"></div></div>
            <form name="frm_user_creation" id="frm_user_creation" action="javascript:func_save_user_creation(save_perm,edit_perm,delete_perm,approve_perm);">
                <fieldset>
                    <fieldset><legend>User Info</legend>
                        <div style="border: 1px solid #555FFF; width:700px; padding-top:10px; padding-bottom:10px;" align="center">
                            <table align="center" width="630px" cellpadding="1" cellspacing="0" border="0">
                                <tbody>
                                    <tr>
                                        <td width="130px" align="right"><strong>User ID :</strong></td>
                                        <td width="150px">
                                            <input type="text" name="txt_user_id" id="txt_user_id" class="text_boxes" onBlur="onblur_user_creation(this.value,'type_user_creation')" style="width:150px;" />
                                        </td>
                                        <td width="10px">&nbsp;</td>
                                        <td width="170px" align="right"><strong>System Code :</strong></td>
                                        <td width="170px"><input type="text" name="txt_emp_code" id="txt_emp_code" class="text_boxes" placeholder="Browse" readonly="readonly" ondblclick="openpage_searchemp()" style="width:170px;" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>User Full Name :</strong></td>
                                        <td>
                                            <input type="text" name="txt_user_name" id="txt_user_name" class="text_boxes" readonly="readonly" style="width:150px;" />
                                        </td>
                                        <td>&nbsp;</td>
                                        <td align="right"><strong>Designation :</strong></td>
                                        <td><input type="text" name="txt_designation" id="txt_designation" class="text_boxes" readonly="readonly" style="width:170px;" /></td>
                                    </tr>
                                    <tr>
                                        <td width="100px" align="right"><strong>Password :</strong></td>
                                        <td><input type="password" name="txt_password" id="txt_password" class="text_boxes" style="width:150px;" /></td>
                                        <td>&nbsp;</td>
                                        <td align="right"><strong>Confirm Password :</strong></td>
                                        <td><input type="password" name="txt_confirm_password" id="txt_confirm_password" class="text_boxes" style="width:170px;" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>User Level :</strong></td>
                                        <td>
                                            <select name="cbo_user_level" id="cbo_user_level" class="combo_boxes" style="width:160px" >
                                                <option value="0">-- Select --</option>
                                                <option value="1">Admin User</option>
                                                <?php 
                                                    foreach($user_group_dtls as $key=>$value)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php	
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td align="right"><strong>Data Level Security :</strong></td>
                                        <td>
                                            <select name="cbo_data_level_security" id="cbo_data_level_security" class="combo_boxes" onchange="change_data_level_security(this.value)" style="width:180px">
                                                <option value="">-- select --</option>
                                                <option value="0">Access All Data</option>
                                                <option value="1">Limited Access</option>
                                                <option value="2">Self Access</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr> 
                                        <td align="right"><strong>Bind to IP :</strong></td>
                                        <td>
                                            <input type="text" name="txt_ip_addres" id="txt_ip_addres" class="text_boxes" placeholder="Comma Seperate LAN and WAN IP" style="width:150px;" />
                                        </td>
                                        <td>&nbsp;</td>
                                        <td align="right"><strong>Expiry Date :</strong></td>
                                        <td><input type="text" name="txt_expiry_date" id="txt_expiry_date" class="datepicker" style="width:170px;" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right"><strong>Email Address :</strong></td>
                                        <td><input type="text" name="txt_email_address" id="txt_email_address" class="text_boxes" style="width:150px;" /></td>
                                        <td>&nbsp;</td>
                                        <td align="right"><strong>Status :</strong></td>
                                        <td>
                                            <select name="cbo_user_status" id="cbo_user_status" class="combo_boxes" style="width:180px;">
                                                <option value="1">Active</option>
                                                <option value="2">Inactive</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>           
                        </div>
                    </fieldset>
                    <div style="height:5px;"></div>
                    <fieldset><legend>Cost Center Permission</legend>
                    	<div id="div_user_creation_costcenter">
                        <table width="1020px" cellpadding="1" cellspacing="1" id="tbl_user_creation_costcenter" align="center" border="0">
                            <thead bgcolor="#A6CAF0">
                                <th width="150px"><strong>Company</strong></th>
                                <th width="150px"><strong>Location</strong></th>
                                <th width="150px"><strong>Division</strong></th>
                                <th width="150px"><strong>Department</strong></th>
                                <th width="150px"><strong>Section</strong></th>
                                <th width="150px"><strong>Subsection</strong></th>
                                <th width="150px"><strong>No. of Emp</strong></th>
                                <th width="70px">
                                    <input type="button" name="btn_add" id="btn_add" value="+" class="formbutton" onclick="click_add_btn();" style="width:70px;" />
                                </th>
                            </thead>
                            <tbody id="tbl_user_creation_costcenter_tbody">
                                <tr>
                                    <td>
                                        <select name="cbo_company_id[]" id="cbo_company_id_1" class="combo_boxes" onchange="change_company(this.value,1);" style="width:150px;">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                                foreach($company_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </td>
                                    <td width="150px">
                                        <select name="cbo_location_id[]" id="cbo_location_id_1" class="combo_boxes" onchange="change_location(this.value,1);" style="width:150px;">
                                            <option value="0">-- Select --</option>
                                            <?php
                                                /* 
                                                foreach($location_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                */
                                            ?>
                                        </select>
                                    </td>
                                    <td width="150px">
                                        <select name="cbo_division_id[]" id="cbo_division_id_1" class="combo_boxes" onchange="change_division(this.value,1);" style="width:150px;">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                                /*
                                                foreach($division_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                */
                                            ?>
                                        </select>
                                    </td>
                                    <td width="150px">
                                        <select name="cbo_department_id[]" id="cbo_department_id_1" class="combo_boxes" onchange="change_department(this.value,1)" style="width:150px;">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                                /*
                                                foreach($department_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                */
                                            ?>
                                        </select>
                                    </td>
                                    <td width="150px">
                                        <select name="cbo_section_id[]" id="cbo_section_id_1" class="combo_boxes" onchange="change_section(this.value,1)" style="width:150px;">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                                /*
                                                foreach($section_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                */
                                            ?>
                                        </select>
                                    </td>
                                    <td width="150px">
                                        <select name="cbo_subsection_id[]" id="cbo_subsection_id_1" class="combo_boxes" style="width:150px;">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                                /*
                                                foreach($subsection_details as $key=>$value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                */
                                            ?>
                                        </select>
                                    </td>
                                    <td width="150px">
                                        <input type="text" name="hdn_emp_code[]" id="hdn_emp_code_1" class="text_boxes" placeholder="Browse" readonly="readonly" ondblclick="openpage_searchemp_user_costcenter(1);" onmouseover="show_emp_code(1);" style="width:150px;" />
                                        <input type="hidden" name="txt_emp_code[]" id="txt_emp_code_1" class="text_boxes" style="width:150px;" />
                                    </td>
                                    <td> 
                                        <input type="hidden" name="txt_db_row_id[]" id="txt_db_row_id_1" value="" class="text_boxes" style="width:70px;" />
                                    </td>
                                </tr>                                
                            </tbody>
                        </table>
                        </div>
                        <div style="padding-top:10px;">
                            <input type="submit" name="save" id="save" value="Save"  class="formbutton" style="width:70px;"/>
                            <input type="button" name="cancel" id="cancel" value="Refresh"  class="formbutton" onclick="reset_frm()" style="width:70px;" />
                            <input type="hidden" name="txt_update_id" id="txt_update_id" class="text_boxes" readonly="readonly" />
                        </div>                       
                    </fieldset>
                </fieldset>
            </form>  
        </div>
    </body>
</html>