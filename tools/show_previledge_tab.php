<?php
session_start();
include('../includes/common.php');

extract( $_GET );

?>
<fieldset style="width:800px;">
	<legend>Priviledge Tab Info</legend>
	<table width="100%" height="66" border="0" cellpadding="0" cellspacing="2">
		<?php
			$emp_tab_arr=array(1=>"Basic Info",2=>"Photo",3=>"Address",4=>"Salary Info",5=>"Bank Salary",6=>"Entitlement",7=>"Experience",8=>"Education",9=>"Family Info",10=>"Policy Taging",11=>"Weekend",12=>"Doc Submitted",13=>"Skill Assign");
			$roster_tab_arr=array(1=>"Issue Item",2=>"Returned Item",3=>"Money Recovered");
			$variable_tab_arr=array(1=>"HRM",2=>"Report Signature");
			$bill_processing_tab_arr=array(1=>"Tiffin Bill Process",2=>"Night Allowance Process",3=>"Night Allowance Process",4=>"Bill Process With Salary");
			$candidate_evaluation_tab_arr=array(1=>"General Info",2=>"HR Observation",3=>"Technical Test",4=>"IE Department",5=>"Dept Comments");
			
			if($form_id==1){$tab_arr=$emp_tab_arr;}
			else if($form_id==2){$tab_arr=$roster_tab_arr;}
			else if($form_id==3){$tab_arr=$variable_tab_arr;}
			else if($form_id==4){$tab_arr=$bill_processing_tab_arr;}
			else if($form_id==5){$tab_arr=$candidate_evaluation_tab_arr;}
			else if($form_id==0){$tab_arr=array();}
		?>
		<tr>
			<th colspan="6" style="border:thin solid #000000;">Permission</th>
		</tr>
		<tr>
        	<table width="800" class="rpt_table" id="tbl_tab_info">
                        <thead>
                        	<th width="100px" style="border:thin solid #000000;">Tab Name</th>
                            <th width="100px" style="border:thin solid #000000;">Visibility</th>
                            <th width="100px" style="border:thin solid #000000;">Insert</th>
                            <th  width="100px" style="border:thin solid #000000;">Edit</th>
                            <th width="100px" style="border:thin solid #000000;">Delete</th>
                            <th  width="100px" style="border:thin solid #000000;">Approve</th>
                        </thead>
                        <?
				$sql = "SELECT * FROM user_priv_tab WHERE user_id = '$user_id' AND form_id = '$form_id' group by id";
				$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
				
				if(mysql_num_rows($result)==0)
				{
						$i=0;
                        foreach ($tab_arr as $key=>$val)
						{
							$i++;
						?>
                        <tr>
                        	 <td><? echo $val; ?>
                             <input type="hidden" name="txttabname_<? echo $i;?>" id="txttabname_<? echo $i;?>" value="<? echo $key; ?>" readonly="readonly" /> 
                             <input type="hidden" id="isupdate_<? echo $i;?>" name="isupdate_<? echo $i;?>" value="" /></td>
                            <td>  
                                <select name="cbovisibility_<? echo $i;?>" id="cbovisibility_<? echo $i;?>" tabindex="6" class="combo_boxes" style="width:100px;">
                                    <option value="1">Visible</option>
                                    <option value="2">Not Visible</option>
                                </select>
                            </td>
                            <td>
                                <select name="cboinsert_<? echo $i;?>" id="cboinsert_<? echo $i;?>" tabindex="7" class="combo_boxes" style="width:100px;" >
                                    <option value="1">Permitted</option>
                                    <option value="2">Not Permitted</option>
                                </select>
                            </td>
                            <td>
                                <select name="cboedit_<? echo $i;?>" id="cboedit_<? echo $i;?>" tabindex="8" class="combo_boxes" style="width:100px;">
                                    <option value="1">Permitted</option>
                                    <option value="2">Not Permitted</option>
                                </select>
                            </td>
                            <td>
                                <select name="cbodelete_<? echo $i;?>" id="cbodelete_<? echo $i;?>" tabindex="9" class="combo_boxes" style="width:100px;">
                                    <option value="1">Permitted</option>
                                    <option value="2">Not Permitted</option>
                                </select>
                            </td>
                            <td>
                                <select name="cboapprove_<? echo $i;?>" id="cboapprove_<? echo $i;?>" tabindex="10" class="combo_boxes" style="width:100px;">
                                    <option value="1">Permitted</option>
                                    <option value="2">Not Permitted</option>
                                </select>
                            </td>
                        </tr>
                        <?
						}
				}
				else
				{
					$i=0;
					while( $row = mysql_fetch_assoc( $result ) )
					 {
						$i++;
					?>
					 <tr>
                        	 <td><? echo $tab_arr[$row[tab_id]]; ?>
                             <input type="hidden" name="txttabname_<? echo $i;?>" id="txttabname_<? echo $i;?>" value="<? echo $row[tab_id]; ?>" readonly="readonly" /> 
                             <input type="hidden" id="isupdate_<? echo $i;?>" name="isupdate_<? echo $i;?>" value="<? echo $row[id]; ?>" /></td>
                            <td> 
                            		<? 
									$visi_arr=array(1=>"Visible",2=>"Not Visible");
									?> 
                                <select name="cbovisibility_<? echo $i;?>" id="cbovisibility_<? echo $i;?>" tabindex="6" class="combo_boxes" style="width:100px;">
                                			      <?
										 foreach($visi_arr as $key=>$val)
										{
										?>
								  
                                  <option value="<?php echo $key; ?>" <?php if($key==$row[visibility_prv]){echo "selected";}?>  ><?php echo $val; ?></option>
								  <?
										}
										?>

                                </select>
                            </td>
                            <td>
                            <?
                            $insert_arr=array(1=>"Permitted",2=>"Not Permitted");
							?>
                                <select name="cboinsert_<? echo $i;?>" id="cboinsert_<? echo $i;?>" tabindex="7" class="combo_boxes" style="width:100px;" >
                                      <?
										 foreach($insert_arr as $key=>$val)
										{
										?>
								  
                                  <option value="<?php echo $key; ?>" <?php if($key==$row[insert_prv]){echo "selected";}?>  ><?php echo $val; ?></option>
								  <?
										}
										?>
                                </select>
                            </td>
                            <td>
                             <?
                            $edit_arr=array(1=>"Permitted",2=>"Not Permitted");
							?>
                                <select name="cboedit_<? echo $i;?>" id="cboedit_<? echo $i;?>" tabindex="8" class="combo_boxes" style="width:100px;">
                                     <?
										 foreach($edit_arr as $key=>$val)
										{
										?>
								  
                                  <option value="<?php echo $key; ?>" <?php if($key==$row[edit_prv]){echo "selected";}?>  ><?php echo $val; ?></option>
								  <?
										}
										?>
                                </select>
                            </td>
                            <td>
                             <?
                            $delete_arr=array(1=>"Permitted",2=>"Not Permitted");
							?>
                                <select name="cbodelete_<? echo $i;?>" id="cbodelete_<? echo $i;?>" tabindex="9" class="combo_boxes" style="width:100px;">
                                   <?
										 foreach($delete_arr as $key=>$val)
										{
										?>
								  
                                  <option value="<?php echo $key; ?>" <?php if($key==$row[delete_prv]){echo "selected";}?>  ><?php echo $val; ?></option>
								  <?
										}
										?>
                                </select>
                            </td>
                            <td>
                             <?
                            $approve_arr=array(1=>"Permitted",2=>"Not Permitted");
							?>
                                <select name="cboapprove_<? echo $i;?>" id="cboapprove_<? echo $i;?>" tabindex="10" class="combo_boxes" style="width:100px;">
                                    <?
										 foreach($approve_arr as $key=>$val)
										{
										?>
								  
                                  <option value="<?php echo $key; ?>" <?php if($key==$row[approve_prv]){echo "selected";}?>  ><?php echo $val; ?></option>
								  <?
										}
										?>
                                </select>
                            </td>
                        </tr>
						<?
					 }
				}
						?>
            </table>
        </tr>
        
        <tr><td colspan="6" align="center"><input type="submit" name="save" id="save" tabindex="11" class="formbutton" value="Set Priviledge" /> </td></tr>
		<tr><td colspan="9" style="padding-top:10px;"><div id="list_priv_tab"></div></td></tr>
	</table>
</fieldset>