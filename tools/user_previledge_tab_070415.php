<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../includes/common.php');
include('../includes/array_function.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
	
    <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../includes/ajax_submit.js" type="text/javascript"></script>
	<script src="../includes/functions.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
    <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
     <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
        <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
/*	
$(document).ready(function(e) {
	  $("#cbo_tab_name").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
	});
 }); 
 */
 
$(document).ready(function(e) {
	  $("#cbotab_name").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
 });       
		
function loadpage_data_tab_permissions( user_id,form_id,tab_id)
 {
	//alert (tab_id);
	if( user_id == 0 || form_id == 0 || form_id == '' ) {
		document.getElementById("user_prev").innerHTML = "";
	}
	else {
		if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
		else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
		
		xmlhttp.onreadystatechange = function() {
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				document.getElementById("user_prev").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open( "GET", "show_previledge_tab.php?user_id=" + user_id + "&form_id=" + form_id + "&tab_id=" + tab_id, true );
		xmlhttp.send();
	}
}



//--------------------------------------------------Priviledge Management
function fnc_user_priviledge_tab_permissions(save_perm,edit_perm,delete_perm,approve_perm) 
{
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn( 1000 );
	
	var cbo_user_name			= escape( document.getElementById('cbo_user_name').value );
	var cbo_form_name			= escape( document.getElementById('cbo_form_name').value );
	
	var cbo_tab_name=""; var cbo_visibility=""; var cbo_insert=""; var cbo_edit=""; var cbo_delete=""; var cbo_approve=""; var is_update="";
	
			var tbl_row_count=$('#tbl_tab_info tr').length;
					
			for( s=1; s<tbl_row_count; s++) 
				{
					var tab_name = escape(document.getElementById('txttabname_'+s).value);
					var visibility = escape(document.getElementById('cbovisibility_'+s).value);
					var cboinsert = escape(document.getElementById('cboinsert_'+s).value);
					var cboedit = escape(document.getElementById('cboedit_'+s).value);
					var cbodelete = escape(document.getElementById('cbodelete_'+s).value);
					var cboapprove = escape(document.getElementById('cboapprove_'+s).value);
					var isupdate = escape(document.getElementById('isupdate_'+s).value);
					cbo_tab_name+=tab_name + ",";
					cbo_visibility+=visibility + ",";
					cbo_insert+=cboinsert + ",";
					cbo_edit+=cboedit + ",";
					cbo_delete+=cbodelete + ",";
					cbo_approve+=cboapprove + ",";
					is_update+=isupdate + ",";
				}
		
	//alert (cbo_visibility);return;
	if (save_perm==2 || edit_perm==2 || delete_perm==2) // save_perm,edit_perm,delete_perm
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have permission on this page.').fadeIn(1000);
	}
	else {
		nocache = Math.random();
		http.open( 'get', '../save_update_common.php?action=user_priviledge_tab_permissions&cbo_user_name=' + cbo_user_name + '&cbo_form_name=' + cbo_form_name
				  			+ '&cbo_tab_name=' + cbo_tab_name + '&cbo_visibility=' + cbo_visibility + '&cbo_insert=' + cbo_insert
							+ '&cbo_edit=' + cbo_edit + '&cbo_delete=' + cbo_delete + '&cbo_approve=' + cbo_approve + '&is_update=' + is_update  + '&tbl_row_count=' + tbl_row_count + '&nocache=' + nocache );
		http.onreadystatechange = fnc_user_priviledge_tab_permissions_response;
		http.send( null );
	}
}

function fnc_user_priviledge_tab_permissions_response() {
	if( http.readyState == 4 ) {
		//alert(http.responseText); 
		var response = http.responseText.split('_');
		
		if( response[0] == 2 )
		 {
			 $("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Priviledge for the selected Tab item is set for the selected user.' ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			
		}
		
	}
}


	</script>
</head>

<body style="font-family:verdana; font-size:11px;">

	<div align="center" style="width:1300px">
		<div align="center" id="messagebox"></div> 
	</div>
    
	<div align="center">
		<div align="center" style="width:1024px; position:relative; height:60px; margin-bottom:5px; margin-top:5px">
			<h3>Set User Privilege Tab</h3>
            <span><p id="permission_caption">
						<? 
                            echo "Your Permissions--> \n".$insert.$update.$delete;
                        ?>
         </p></span>
			<div align="center" id="messagebox"></div>	
		</div>
		<form name="user_priv_tab" id="user_priv_tab" method="" action="javascript:fnc_user_priviledge_tab_permissions(save_perm,edit_perm,delete_perm,approve_perm)">
			<fieldset style="width:900px;">
				<legend>Select User and Form and Tab</legend>
				<table width="100%">
					<tr>
						<td width="10%">User ID</td>
						<td>
							<select name="cbo_user_name" id="cbo_user_name" tabindex="1" style="width:180px" class="combo_boxes">
								<option value="0">-- Select --</option>
								<?php 
								$sql = "select * from user_passwd  where valid = 1 order by user_name";
								$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
								while( $r_mod = mysql_fetch_array( $result ) ) {
								?>
								<option value="<?php echo $r_mod["id"]; ?>" <?php if( $company_combo == $r_mod["id"] ) echo "selected"; ?>><?php echo "$r_mod[user_name]" ?> </option>
								<?php } ?>
							</select>
						</td>
						<td width="10%">Form Name</td>
						<td>
							<select name="cbo_form_name" tabindex="1"  id="cbo_form_name" style="width:180px"  class="combo_boxes" onchange="loadpage_data_tab_permissions( document.getElementById('cbo_user_name').value,this.value );">
								<?
								//$tab_wise_form_arr=array(0=>"--- Select ---",1=>"Employee Information",2=>"Resource Allocation",3=>"Variable Settings");
                                foreach($tab_wise_form_arr as $key=>$val)
                                {
                                ?>
                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                <?
                                }
                                ?> 
							</select>
						</td>
					</tr>
				</table>
			</fieldset>
			<div id="user_prev" align="center"></div>
		</form>
	</div>
</body>
</html>