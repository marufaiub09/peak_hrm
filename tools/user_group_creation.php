<?php
/***************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 18.08.2014
*****************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
// Buyer or Party details
// company
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}
// location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}
// division
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}
// department
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}
// section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}
// subsection
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}
// designation
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}

// main_module
$sql = "SELECT * FROM  main_module WHERE status = 1 ORDER BY main_module ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$main_module_dtls = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$main_module_dtls[$row['m_mod_id']] = mysql_real_escape_string( $row['main_module'] );		
}

// main_module
$sql = "SELECT user_group_id,user_group_name FROM  user_group_priviledge WHERE status_active = 1 group by user_group_id ORDER BY user_group_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$user_group_dtls = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$user_group_dtls[$row['user_group_id']] = mysql_real_escape_string( $row['user_group_name'] );		
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Cost Sheets</title>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../includes/ajax_submit.js" type="text/javascript"></script>
        <script src="../includes/functions.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        
		<script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script language="javascript">
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
                $("#cbo_company_id,#cbo_location_id,#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselect({
                    //header: false, 
                    selectedText: "# of # selected",
                });
				$("#cbo_company_id,#cbo_location_id,#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselectfilter({ });	
            });
			
			var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
			
			// multi_up_check
			function multi_up_check(field,checkedItems)
			{
				var valArr =checkedItems .split(",");
				var i = 0;
				var size = valArr.length;
				for(i; i < size; i++)
				{
				  $("#"+field).multiselect("widget").find(":checkbox[value='"+valArr[i]+"']").attr("checked","checked");
				  $("#"+field+ " option[value='" + valArr[i] + "']").attr("selected", 1);
				  $("#"+field).multiselect("refresh");
				}
			}
			
			//=================== user_group start ========================
			
			//Do not change 
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer") {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				}
				else request_type = new XMLHttpRequest();
				
				return request_type;
			}
			var http = createObject();
			
			function reset_frm()
			{
				document.getElementById('frm_user_group_creation').reset();
				$("#cbo_main_module").val(0);
				$("#cbo_main_menu option[value!='0']").remove();
				$("#cbo_sub_main_menu option[value!='0']").remove();
				$("#cbo_sub_menu option[value!='0']").remove();
				document.getElementById('load_list_priv').innerHTML = "";
			}
			
			function fnc_change_module(id)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=change_main_module&id=' + id,
					success: function(html) {				
						$('#cbo_main_menu').html( html )
					}
				});
				
				$("#cbo_sub_main_menu option[value!='0']").remove();
				$("#cbo_sub_menu option[value!='0']").remove();
			}
			
			function fnc_change_main_menu(id)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=change_main_menu&id=' + id,
					success: function(html) {				
						$('#cbo_sub_main_menu').html( html )
					}
				});
				$("#cbo_sub_menu option[value!='0']").remove();
			}
			
			function fnc_change_sub_main_menu(id)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "../get_data_update.php",
					data: 'type=change_sub_main_menu&id=' + id,
					success: function(html) {				
						$('#cbo_sub_menu').html( html )
					}
				});
			}

			function load_user_group_priviledge() 
			{
				//alert("su..re");
				/*
				var str=$('#cbo_main_module').val();
				var str2=$('#cbo_main_menu').val();
				var type=5;
				var div='load_list_priv';
				var str3=$('#txt_user_group_update_id').val();
				*/
				if( $('#txt_user_group').val()=="" ) 
				{
					//document.getElementById(div).innerHTML = "";
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#txt_user_group').focus();
						$(this).html('Please enter user group name.').addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					return;
					});
				}
				else if( $('#cbo_main_module').val()==0)
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#cbo_main_module').focus();
						$(this).html('Please select main module.').addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
						return;
					});
				}
				else 
				{
					if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
					else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
					xmlhttp.onreadystatechange = function() {
						if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) 
						{
							document.getElementById('load_list_priv').innerHTML = xmlhttp.responseText;
						}
					}
					//$('#load_list_priv').html('<img src="../resources/images/loading.gif" />');
					//$('#messagebox').removeClass().addClass('messagebox').text('Loading....').fadeIn(1000);
					xmlhttp.open( "GET", "../list_view.php?type=" + 5 + 
								"&main_module=" + $('#cbo_main_module').val() + 
								"&main_menu=" + $('#cbo_main_menu').val() + 
								"&sub_main_menu=" + $('#cbo_sub_main_menu').val()+ 
								"&sub_menu=" + $('#cbo_sub_menu').val()+ 
								"&update_id=" + $('#txt_user_group_update_id').val(), true );
					xmlhttp.send();
				}
			}
			
			// Check
			function Check(chk)
			{
				//alert(chk);
				var cnt=0;
				if(document.frm_user_group_creation.Check_ctr.checked==true)
				{
					for (i = 0; i < chk.length; i++)
					{
						chk[i].checked = true ;
						cnt++;
					}
				}
				else
				{			
					for (i = 0; i < chk.length; i++)
					chk[i].checked = false ;			
				}
				//$('#count_id').html('You Are Selected '+cnt+' Employees');
			}
			
			// count_emp
			function count_emp(chk)
			{
				//alert (chk);
				var cnt=0;
				for (i = 0; i < chk.length; i++){
					if(chk[i].checked) cnt++;
				}
				$('#count_id').html('You Are Selected '+cnt+' Employees');
			}
			
			// func_save_user_group_creation
			function func_save_user_group_creation()
			{
				if($('#txt_user_group').val()=="")
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$('#txt_user_group').focus();
						$(this).html('User group field is blank.').addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
					return false;
				}
				else if( $('#tbl_user_group_priviledge tbody input[type="checkbox"]:checked').length==0 )
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html('Please checked at least one information.').addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
					return false;
				}
				else
				{
					var counter = 0; 
					var data = '';
					$('#tbl_user_group_priviledge tbody input[type="checkbox"]:checked').each(function() {
						counter++;
						data += $(this).val() + 
								$('#cbo_visibility_'+counter).val()+'_'+ 
								$('#cbo_insert_'+counter).val()+'_'+ 
								$('#cbo_update_'+counter).val()+'_'+ 
								$('#cbo_delete_'+counter).val()+'_'+ 
								$('#cbo_approved_'+counter).val() +'_'+ 
								$('#txt_row_id_'+counter).val() +'|';
					});
					//alert(data);return;
					nocache = Math.random();
					http.open('get','../save_update_common.php?action=action_user_group_priviledge' + 
							'&data='+ data + 
							'&user_group_name='+$('#txt_user_group').val() + 
							'&update_id='+$('#txt_user_group_update_id').val() + 
							'&nocache='+nocache);
					http.onreadystatechange = user_group_priviledge;
					http.send(null);
				}
			}
			
			function user_group_priviledge() 
			{
				if(http.readyState == 4) 
				{
					var response = http.responseText.split('_');	
					//alert(http.responseText);
					if( response[0] == 1 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Data saved successfully.').addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
							load_user_group_priviledge();
							//reset_frm();
						});
						return;
					}
					if( response[0] == 2 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Updated successfully.').addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
							load_user_group_priviledge();
							//reset_frm();
						});
						return;
					}
					if( response[0] == 3 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Duplicate user group.').addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
							//reset_frm();
						});
						return;
					}
				}
			}
	
			function change_user_group(id)
			{
				//alert($('#cbo_user_group').val());
				if(id!=0)
				{
					$('#txt_user_group').val($("#cbo_user_group option:selected").text());
					$('#txt_user_group_update_id').val(id);
					//$('#txt_user_group_update_id').val($("#cbo_user_group option:selected").val());
				}
				else 
				{
					$('#txt_user_group').val('');
					$('#txt_user_group_update_id').val('');
				}
			}
			
			function set_user_group_priviledge()
			{
				//alert("su..re");
				/*
				var cbo_visibility	= $('#cbo_visibility').val();
				var cbo_insert		= $('#cbo_insert').val();
				var cbo_update		= $('#cbo_update').val();
				var cbo_delete		= $('#cbo_delete').val();
				var cbo_approved	= $('#cbo_approved').val();
				*/
				$('#tbl_user_group_priviledge tbody input[type="checkbox"]:checked').each(function(){
					//alert($(this).val());
					var counter=($(this).val()).split('_');
					$('#cbo_visibility_'+counter[0]).val( $('#cbo_visibility').val() );
					$('#cbo_insert_'+counter[0]).val( $('#cbo_insert').val() );
					$('#cbo_update_'+counter[0]).val( $('#cbo_update').val() );
					$('#cbo_delete_'+counter[0]).val( $('#cbo_delete').val() );
					$('#cbo_approved_'+counter[0]).val( $('#cbo_approved').val() );
					var counter='';
				});
			}
        </script>
    </head>
	<body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; margin-bottom:5px; margin-top:5px;">
        	<div class="form_caption" style="width:100%; background-color:#A6CAF0; padding-top:5px; height:25px; border-radius:5px; color:#000;" align="center">User Group Creation</div>
        	<div id="permission_caption" style="height:20px;"><? echo "Your Permissions--> \n".$insert.$update.$delete;?></div>	
            <div style="height:20px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; border-radius:5px; width:100%;" align="center"></div></div>
            <form name="frm_user_group_creation" id="frm_user_group_creation" action="javascript:func_save_user_group_creation(save_perm,edit_perm,delete_perm,approve_perm);">
                <fieldset>
                    <div align="center" style=" height:30px; vertical-align:middle;">
                        <strong>User Group : </strong>
                            <select name="cbo_user_group" id="cbo_user_group" class="combo_boxes" onchange="change_user_group(this.value)" style="width:160px;">
                                <option value="0">-- select for update --</option>
                                <?php 
                                foreach($user_group_dtls as $key=>$value)
                                {
                                    ?>
                                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                    <?php	
                                }
                                ?>
                            </select>                            
                        <strong>User Group : </strong>
                        <input type="text" name="txt_user_group" id="txt_user_group" class="text_boxes" placeholder="Write for create" style="width:150px;"  />
                        <input type="hidden" name="txt_user_group_update_id" id="txt_user_group_update_id" class="text_boxes" placeholder="hidden field" style="width:150px;"  />
                    </div>
                    <div style="width:1150px;">
                        <table align="center" width="1150px" cellpadding="1" cellspacing="0" rules="all" border="1" bordercolor="#7F7FFF">
                            <tr bgcolor="#A6CAF0"> 
                                <th colspan="6">Module And Menu</th>
                                <th colspan="5">Permission</th>
                                <th><input type="button" name="btn_reset" id="btn_reset" value="Reset"  class="formbutton" onclick="reset_frm()" style="width:70px;"/></th>
                            </tr>
                            <tr bgcolor="#A6CAF0"> 
                                <!--<input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.weekend_form.chk_list)" placeholder="Search" />-->
                                <th rowspan="2" valign="middle" width="30px">
                                    <input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.frm_user_group_creation.chk_list)" placeholder="Search" />
                                </th>
                                <th rowspan="2" valign="middle" width="50px">Sl. No</th>
                                <th width="150px">Main Module</th>
                                <th width="150px">Main Menu</th>
                                <th width="150px">Sub Main Menu</th>
                                <th width="150px">Sub Menu</th>
                                <th width="70px">Visibility</th>
                                <th width="70px">Insert</th>
                                <th width="70px">Update</th>
                                <th width="70px">Delete</th>
                                <th width="70px">Approved</th>
                                <th width="70px"><input type="button" name="btn_load" id="btn_load" value="Load"  class="formbutton" onclick="load_user_group_priviledge();" style="width:70px;"/></th>
                            </tr> 
                            <tr bgcolor="#A6CAF0"> 
                                <td>
                                    <select name="cbo_main_module" id="cbo_main_module" class="combo_boxes" onchange="fnc_change_module(this.value)" style="width:150px;"> 
                                        <option value=0>-- Select --</option>
                                        <?php 
                                            foreach($main_module_dtls as $key=>$value)
                                            {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td> 
                                    <select name="cbo_main_menu" id="cbo_main_menu" class="combo_boxes" onchange="fnc_change_main_menu(this.value)" style="width:150px;">
                                        <option value=0>-- Select --</option>
                                    </select>
                                </td>
                                <td> 
                                    <select name="cbo_sub_main_menu" id="cbo_sub_main_menu" class="combo_boxes" onchange="fnc_change_sub_main_menu(this.value)" style="width:150px;"> 									<option value=0>-- Select --</option>
                                    </select>
                                </td>
                                <td> 
                                    <select name="cbo_sub_menu" id="cbo_sub_menu" class="combo_boxes" style="width:150px;"> 
                                        <option value=0>-- Select --</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_visibility" id="cbo_visibility" class="combo_boxes" style="width:70px;"> 
                                        <option value=1>Yes</option>
                                        <option value=0>No</option>
                                    </select>
                                </td>
                                <td> 
                                    <select name="cbo_inser" id="cbo_insert" class="combo_boxes" style="width:70px;"> 
                                        <option value=1>Yes</option>
                                        <option value=0>No</option>
                                    </select>
                                </td>
                                <td> 
                                    <select name="cbo_update" id="cbo_update" class="combo_boxes" style="width:70px;"> 
                                        <option value=1>Yes</option>
                                        <option value=0>No</option>
                                    </select>
                                </td>
                                <td> 
                                    <select name="cbo_delete" id="cbo_delete" class="combo_boxes" style="width:70px;"> 
                                        <option value=1>Yes</option>
                                        <option value=0>No</option>
                                    </select>
                                </td>
                                <td> 
                                    <select name="cbo_approved" id="cbo_approved" class="combo_boxes" style="width:70px;"> 
                                        <option value=1>Yes</option>
                                        <option value=0>No</option>
                                    </select>
                                </td>
                                <td><input type="button" name="btn_set" id="btn_set" value="Set" class="formbutton" onclick="set_user_group_priviledge();" style="width:70px;"/></td>
                            </tr>
                        </table>
                        <div id="load_list_priv"></div>
                    </div>
                    <div style="padding-top:10px;">
                        <input type="submit" name="save" id="save" value="Save"  class="formbutton" style="width:70px;"/>
                        <input type="button" name="cancel" id="cancel" value="Refresh"  class="formbutton" style="width:70px;" />
                        <input type="hidden" name="txt_update_id" id="txt_update_id" />
                    </div>
                </fieldset>
            </form>  
        </div>
    </body>
</html>