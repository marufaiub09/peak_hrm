<?php 
	/***************************************
	|	Developed By	: Md. ekram
	|	Date			: 05.04.2015
	*****************************************/
	session_start();
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	include('../../includes/common.php');
	include('../../includes/array_function.php');
	include('../../includes/common_functions.php');
	extract($_REQUEST);
	extract($_GET);
	
	

	//company_details
	$sql = "SELECT id,company_name FROM lib_company WHERE $user_company_id status_active=1 and is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$company_details[$row['id']] = mysql_real_escape_string( $row['company_name'] );
	}
	
	//location_details
	$sql = "SELECT id, location_name FROM lib_location WHERE $user_location_id status_active=1 and is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$location_details[$row['id']] = mysql_real_escape_string( $row['location_name'] );
	}
	
	//division_details
	$sql = "SELECT id, division_name FROM lib_division WHERE $user_division_id status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$division_details[$row['id']] = mysql_real_escape_string( $row['division_name'] );
	}
	
	//department_details
	$sql = "SELECT id, department_name FROM lib_department WHERE $user_department_id status_active=1 and is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$department_details[$row['id']] = mysql_real_escape_string( $row['department_name'] );
	}
	
	//section_details
	$sql = "SELECT id, section_name FROM lib_section WHERE $user_section_id status_active=1 and is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$section_details[$row['id']] = mysql_real_escape_string( $row['section_name'] );
	}
	
	//subsection_details
	$sql = "SELECT id, subsection_name FROM lib_subsection WHERE $user_subsection_id status_active=1 and is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );
	}
	
	
	
//--------------------------------------------------------------------------------------------------------------------
	
	// lib_designation
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        
        
        <script src="../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    
        <script type="text/javascript">
            $(document).ready(function(e) {
                $("#cbo_user_name").multiselect({
                    selectedText: "# of # selected",
                });
                
                $("#cbo_user_name").multiselectfilter({ });
             
            });
		
			

	//onchange="populate_cost_center( 'location', this.value );"
	function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/pay_slip_report_fariha_worker.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_month_selector').html( html )
				}
			});
		}
				
function generate_priviledge_report()
{
	var error = false;
	
	div="report_container";
	data_panel="data_panel";
	/*if( $('#cbo_month_selector').val()*1 == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select The month.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}*/
	
	if( error == false )
	{
		$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
		var data = '&cbo_user_name=' + $('#cbo_user_name').val() 
				+ '&cbo_menu_name=' + $('#cbo_menu_name').val(); 
				
				
				//alert (data);
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{			
			var data_split=(xmlhttp.responseText).split('####');
			var link_data=data_split[1];			
		 	
			$('#data_panel').html('&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
			
			$('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="print_report()" value="Print" name="Print" class="formbutton" style="width:100px"/>' );
			
			document.getElementById(div).innerHTML=data_split[0];
			hide_search_panel();
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
		}
  	}
	xmlhttp.open("GET","includes/generate_user_privildge_report.php?type=user_previledge_report"+"&data="+data,true);
	//xmlhttp.open("GET","includes/pay_slip_report_head_off.php?types=pay_slip_ho"+"&data="+data,true); 
	xmlhttp.send();
	}
}


function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('report_container').innerHTML);
	d.close();

}


function print_report()
{
	$('#data_panel').hide();
	$('#messagebox').hide();
	$('#accordion_h1').hide();
	
  	window.print(document.getElementById('report_container').innerHTML);
	
	$('#data_panel').show();
	$('#messagebox').show();
	$('#accordion_h1').show();
}




</script>

</head>


<body>
<div align="left" style="width:100%;">
    <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
    <div id="content_search_panel" > 
<form>
	<fieldset>
    	
    	<table align="center" width="400px" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th>User Name</th>
                <th>Menu Name</th>
             </thead>
            <tr class="general">
                <td>
                     <select name="cbo_user_name" id="cbo_user_name" style="width:140px"  class="combo_boxes" multiple="multiple">
                                    <?
                                    $mod_sql= mysql_db_query($DB, "select id,user_name from user_passwd where valid=1  order by user_name"); 
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                       
                                    ?>
                                    <option value=<? echo $r_mod["id"];
                                    if ($company_combo==$r_mod["id"]){?> selected <?php }?>><? echo "$r_mod[user_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                </select>
                </td>
                <td>
                    <select name="cbo_menu_name" id="cbo_menu_name" style="width:170px"  class="combo_boxes">
                                	 
                                    <option value="0">--- All User ---</option>
                                    <?
										 
                                    $mod_sql= mysql_db_query($DB, "select m_menu_id,menu_name from main_menu where status=1  order by menu_name"); //where is_deleted=0 and status=0
                                    
                                    while ($r_mod=mysql_fetch_array($mod_sql))
                                    {
                                       
                                    ?>
                                    <option value=<? echo $r_mod["m_menu_id"];
                                    if ($company_combo==$r_mod["m_menu_id"]){?> selected <?php }?>><? echo "$r_mod[menu_name]" ?> </option>
                                    <?
                                    }
                                    ?>
                                </select>
                </td>
                
            </tr>
     
                    <tr>
            		<td align="center" colspan="4">
     <input type="button" name="search" id="search" value="Show" class="formbutton" style="width:100px" onclick="generate_priviledge_report()"/>	
     &nbsp;<input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></td>               
            </tr> 
      </table>
    </fieldset>
</form>
</div>
		<fieldset>
		<div id="data_panel" align="center"></div>
        <div id="report_container" align="center" style="background-color:#FFFFFF"></div>
        </fieldset>

</div>
</body>
</html>

