<?php
include('includes/common.php');
include('includes/array_function.php');

$sql = "SELECT * FROM lib_sample  ORDER BY sample_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$sample_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$sample_details[$row['id']] = $row['sample_name'];
}

$sql = "SELECT * FROM lib_garments_item   ORDER BY gmt_item_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$gmt_item_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$gmt_item_details[$row['id']] = $row['gmt_item_name'];
}

$sql = "SELECT * FROM lib_buyer  ORDER BY buyer_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$buyer_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$buyer_details[$row['id']] = $row['buyer_name'];
}

$sql = "SELECT * FROM lib_trim  ORDER BY item_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$trim_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$trim_details[$row['id']] = $row['item_name'];
}

$sql = "SELECT * FROM lib_marketing_team_member_info  ORDER BY team_member_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$team_member_name_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$team_member_name_details[$row['id']] = $row['team_member_name'];
}


$to="";
$sql = "SELECT * FROM mail_user_setup where status_active=1 and recipient_type<>2 ORDER BY recipient_type ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
while( $row = mysql_fetch_assoc( $result ) ) 
{
	if ($to=="")  $to=$row['email_address']; else $to=$to.", ".$row['email_address']; 
}

// $to .= 'sumon@logicsoftbd.com' . ', ';
//$to .= 'nas.mis@asrotex.com';

$subject = "Merchandising Approval Status Reminding";

$next_date=add_date(date("Y-m-d"),1);
$prev_date=add_date(date("Y-m-d"),-1);

ob_start();
 
 ?>
<table width="1050">
	<tr>
    	<td height="30" valign="top" align="left">
        	Dear Sir,
        </td>
    </tr>
	<tr>
    	<td valign="top" align="left">
        	Please be informmed that the following listed Approvals have only ONE DAY remaining for Approval. Please take necessary action for approval or If already Approved then please update PLATFORM Accordingly.
        </td>
    </tr>
    <tr>
    	<td height="40" valign="top" align="left">
        	TNA of PLATFORM
        </td>
    </tr>
    
    
    <tr>
    	<td valign="top" align="left">
        	 <table width="100%" cellpadding="0" cellspacing="0" border="1">
             	<tr>
                	<td colspan="9" height="40" valign="middle"><strong>Sample Approval Reminder:: Pending Approval List before Yesterday (<? echo convert_to_mysql_date($prev_date); ?>) for the following styles.</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Sample Type</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_sample_approval_info where target_approval_date<'$prev_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr> 
                	<td><? echo $i; ?></td><td><? echo $sample_details[$sql_rslt[sample_type_id]]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[sample_comments]; ?></td>
                   
                </tr>
                <? } ?>
                
                
                
                <tr>
                	<td colspan="9" height="40" valign="middle"><strong>Lab-Dip approval reminder: Pending Approval List before Yesterday (<? echo convert_to_mysql_date($prev_date); ?>) for the following styles</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Lap-Dip Name</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_lapdip_approval_info where lapdip_target_approval_date<'$prev_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr>
                	<td><? echo $i; ?></td><td><? echo $sql_rslt[po_color_name]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[lapdip_target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[lapdip_comments]; ?></td>
                   
                </tr>
                <? } ?>
                
                
                
                
                <tr>
                	<td colspan="9" height="40" valign="middle"><strong>Trims approval reminder: Pending Approval List before Yesterday (<? echo convert_to_mysql_date($prev_date); ?>) for the following styles.</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Trims Name</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_accessories_approval_info where target_approval_date<'$prev_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr>
                	<td><? echo $i; ?></td><td><? echo $trim_details[$sql_rslt[accessories_type_id]]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[accessories_comments]; ?></td>
                   
                </tr>
                <? } ?>
             </table>
        </td>
    </tr>
    
    <!--  TNA  -->
    
    <tr>
    	<td valign="top" align="left">
        	 <table width="100%" cellpadding="0" cellspacing="0" border="1">
             	<tr>
                	<td colspan="9" height="40" valign="middle"><strong>Sample Approval Reminder:: Yesterday (<? echo convert_to_mysql_date($prev_date); ?>) was the last approval getting date for the following styles</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Sample Type</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_sample_approval_info where target_approval_date='$prev_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr> 
                	<td><? echo $i; ?></td><td><? echo $sample_details[$sql_rslt[sample_type_id]]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[sample_comments]; ?></td>
                   
                </tr>
                <? } ?>
                
                
                
                <tr>
                	<td colspan="9" height="40" valign="middle"><strong>Lab-Dip approval reminder: Yesterday (<? echo convert_to_mysql_date($prev_date); ?>) was the last approval getting date of following styles.</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Lap-Dip Name</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_lapdip_approval_info where lapdip_target_approval_date='$prev_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr>
                	<td><? echo $i; ?></td><td><? echo $sql_rslt[po_color_name]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[lapdip_target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[lapdip_comments]; ?></td>
                   
                </tr>
                <? } ?>
                
                
                
                
                <tr>
                	<td colspan="9" height="40" valign="middle"><strong>Trims approval reminder: Yesterday (<? echo convert_to_mysql_date($prev_date); ?>) was the last approval getting date of following styles.</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Trims Name</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_accessories_approval_info where target_approval_date='$prev_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr>
                	<td><? echo $i; ?></td><td><? echo $trim_details[$sql_rslt[accessories_type_id]]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[accessories_comments]; ?></td>
                   
                </tr>
                <? } ?>
             </table>
        </td>
    </tr>
    
    <tr>
    	<td height="60" valign="top" align="center">
        	 
        </td>
    </tr>
    
    
    <tr>
    	<td valign="top" align="left">
        	 <table width="100%" cellpadding="0" cellspacing="0" border="1">
             	<tr>
                	<td colspan="9" height="40" valign="middle"><strong>Sample Approval Reminder:: Tomorrow (<? echo convert_to_mysql_date($next_date); ?>) is the last approval getting date for the following styles</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Sample Type</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_sample_approval_info where target_approval_date='$next_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr>
                	<td><? echo $i; ?></td><td><? echo $sample_details[$sql_rslt[sample_type_id]]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[sample_comments]; ?></td>
                   
                </tr>
                <? } ?>
                
                
                
                <tr>
                	<td colspan="9" height="40" valign="middle"><strong>Lab-Dip approval reminder: Tomorrow (<? echo convert_to_mysql_date($next_date); ?>) is the last approval getting date of following styles.</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Lap-Dip Name</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_lapdip_approval_info where lapdip_target_approval_date='$next_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr>
                	<td><? echo $i; ?></td><td><? echo $sql_rslt[po_color_name]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[lapdip_target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[lapdip_comments]; ?></td>
                   
                </tr>
                <? } ?>
                
                
                
                
                <tr>
                	<td colspan="9" height="40" valign="middle"><strong>Trims approval reminder: Tomorrow (<? echo convert_to_mysql_date($next_date); ?>) is the last approval getting date of following styles.</strong></td>
                </tr>
                <tr>
                	<td width="30"><strong>SL</strong></td><td  width="170"><strong>Trims Name</strong></td><td width="70"><strong>Job No</strong></td><td  width="230"><strong>Style</strong></td><td width="150"><strong>Dealing_marchant</strong></td><td width="150"><strong>Buyer</strong></td><td width="80"><strong>Target Date</strong></td><td width="100"><strong>Status</strong></td><td><strong>Comments</strong></td>
                   
                </tr>
             	<?
					
					$sql = "SELECT * from wo_po_accessories_approval_info where target_approval_date='$next_date' and approval_status<>3 and status_active=1 and is_deleted=0";  // Expiry Next Date
					$sql_exe = mysql_query( $sql );
					$i=0;
					while($sql_rslt = mysql_fetch_array( $sql_exe ))
					{
						$i++;
				?>
                <tr>
                	<td><? echo $i; ?></td><td><? echo $trim_details[$sql_rslt[accessories_type_id]]; ?></td><td><? echo $sql_rslt[job_no_mst]; ?></td>
                    <td><? echo split_string(return_field_value("style_ref_no","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'"),25); ?></td><td><? echo $team_member_name_details[return_field_value("dealing_marchant","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo $buyer_details[return_field_value("buyer_name","wo_po_details_master","job_no='$sql_rslt[job_no_mst]'")]; ?></td><td><? echo convert_to_mysql_date($sql_rslt[target_approval_date]); ?></td><td><? echo $approval_status[$sql_rslt[approval_status]]; ?></td><td><? echo $sql_rslt[accessories_comments]; ?></td>
                   
                </tr>
                <? } ?>
             </table>
        </td>
    </tr>
</table>

<?  
 
$message=ob_get_contents();

$headers  .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$headers .= 'From: platform_jmerp@jmfabric.com' . "\r\n"; // Sender's Email Address
$headers .= 'Return-Path: Admin <platform_jmerp@jmfabric.com> /n'; // Indicates Return-path
$headers .= 'Reply-To: Admin <platform_jmerp@jmfabric.com> /n'; // Reply-to Address


$headers .= 'X-Mailer: PHP/' . phpversion(); // For X-Mailer

if (mail($to,$subject,$message,$headers))
	echo "****Mail Sent.---".date("Y-m-d");
else
	echo "****Mail Not Sent.---".date("Y-m-d");
	
	
	
	//-------------------------------------------------------------------------------------------
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?> 