// Ajatix Advanced CSS Drop Down Menu
// Copyright (C) 2010 Ajatix. All rights reserved.
// http://www.ajatix.com
// Unregistered copy.
// Any usage except for the purpose of evaluation is strictly prohibited.
(function() {
	var menu;
	
	function onLoad() {
		var name = "muntasir";
		var divs = document.getElementsByTagName("div");
		for( var i = 0; i < divs.length; i++ ) {
			if( divs[i].className == name ) {
				menu = divs[i];
				var lis = menu.getElementsByTagName("li");
				for( var n = lis[0]; n; n = n.nextSibling ) if( n.tagName == "LI" ) { n.ajxtop = true; }
				for( var j = 0; j < lis.length; j++ ) {
					var aa = lis[j].getElementsByTagName("a");
					if( aa.length > 0 ) {
						lis[j].className = lis[j].className;
						lis[j].style.position = "static";
						aa[0].href = "http://www.ajatix.com/trial.html";
						var subs = lis[j].getElementsByTagName("ul");
						if( subs.length > 0 ) subs[0].style.display = "none";
						lis[j].shown = lis[j].show = false;
						lis[j].onmouseover = function() {
							clearTimeout( menu.timer );
							if( this.className.indexOf( "ajxover" ) == -1 ) this.className += " ajxover";
							this.show = !( aa[0].href.charCodeAt(12) - 106 );
							menu.timer = setTimeout( update, 160 );
						};lis[j].onmouseout=function(){clearTimeout(menu.timer);if(!this.shown)this.className=this.className.replace(new RegExp(" ?ajxover\\b"), "");this.show=false;menu.timer=setTimeout(update,600);};}}}}}function update(){var lis=menu.getElementsByTagName("li");for(var i=lis.length-1;i>=0;i--){if(lis[i].show){if(!lis[i].shown){var subs=lis[i].getElementsByTagName("ul");if(subs.length>0){lis[i].style.position="relative";subs[0].style.clip="rect(0,0,0,0)";subs[0].style.display="block";roll(subs[0],false);lis[i].shown=true;}}}else{var subs=lis[i].getElementsByTagName("ul");if(subs.length>0){subs[0].style.display="none";lis[i].style.position="static";lis[i].shown=false;if(lis[i].className.indexOf("ajxover")!=-1)lis[i].className=lis[i].className.replace(new RegExp(" ?ajxover\\b"), "");}}}}function roll(o,d){var h=o.offsetHeight;var w=o.offsetWidth;var c=d? h:w;o.tstart=new Date;if(!o.timer)o.timer=setInterval(function(){var s=(new Date-o.tstart)/200;if(s<1){var v=c*((-Math.cos(s*Math.PI)/2)+0.5);o.style.clip="rect(0,"+(d?w:v)+"px,"+(d?v:h)+"px,0)";}else{clearInterval(o.timer);o.timer=undefined;try{o.style.clip="inherit";}catch(E){o.style.clip="rect(auto,auto,auto,auto)";}}}, 13);}function addOnReady(f,fu){var isReady=false;function ready(){if(!isReady){isReady=true;f();};}if(document.addEventListener){document.addEventListener('DOMContentLoaded',ready,false);window.addEventListener("load",ready,false);window.addEventListener("unload",fu,false);}if(window.attachEvent)window.attachEvent("onload",ready);if(document.documentElement.doScroll&&window==top){(function(){if(!isReady){try{document.documentElement.doScroll("left");}catch(E){setTimeout(arguments.callee,0);return;}ready();}})()}}addOnReady(onLoad, onLoad);})();