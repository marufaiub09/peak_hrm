<?php
02	/*********************************************************************************
03	** The contents of this file are subject to the vtiger CRM Public License Version 1.0
04	 * ("License"); You may not use this file except in compliance with the License
05	 * The Original Code is:  vtiger CRM Open Source
06	 * The Initial Developer of the Original Code is vtiger.
07	 * Portions created by vtiger are Copyright (C) vtiger.
08	 * All Rights Reserved.
09	 *
10	********************************************************************************/
11	 
12	//Following variables will be used in include/fpdf/templates/body.php
13	 
14	//where the bottom line starts. If you change this bottom then all related changes will be done automatically
15	 
16	//If the product description is too long and you want to avoid the display clearly then you can change bottom =170 or products_per_page=5 or less.
17	 
18	//Enter values less than 170 if value > 170 then last lines will be displayed in next page
19	$bottom="130";//"130"
20	 
21	//how may products per page
22	$products_per_page="6";//used in modules/{PO/SO/Quotes/Invoice}/CreatePDF.php
23	 
24	 
25	//where the top line starts
26	$body_top="80";
27	 
28	//This is the y position where the nettotal starts, next fields will be displayed in bottom+n(6) pixels
29	$nettotal_y = $bottom+38;//"168";
30	$next_y = "6";
31	 
32	 
33	//Following variables will be used in modules/{PO/SO/Quotes/Invoice}/pdf_templates/footer.php
34	$top = $nettotal_y+48;//"216";
35	 
36	 
37	//if true then Description and Terms & Conditions will be displayed in all pages, false - displayed only in the last page
38	$display_desc_tc = 'true';
39	 
40	 
41	?>
