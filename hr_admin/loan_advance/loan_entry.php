<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 17-04-12
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
    <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
    <script src="../includes/functions.js" type="text/javascript"></script>
   
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
	<script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>

 	<script language="javascript">
	function openmypage(page_link,title)
		{
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=985px,height=380px,center=1,resize=0,scrolling=0','../')
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
			  	//alert(document.getElementById('txt_emp_code').value);
				//alert(theemail.value);
				emp_search_loan_application(theemail.value,"loan_application");
			
				setTimeout('showResult_search_loan("'+theemail.value+'","loan_app","loan_application_div")',150);
				
			}
		}
		function openmypage_recommend(page_link,title)
		{
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=970px,height=380px,center=1,resize=0,scrolling=0','../')
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
				var theename=this.contentDoc.getElementById("txt_selected_emp_name");
			  	//document.getElementById('txt_emp_code').value=theemail.value;
				//alert(theename.value);
				document.getElementById('hid_txt_recommended_by').value=theemail.value;
				document.getElementById('txt_recommended_by').value=theename.value;
				
			}
		}
		function openmypage_approved(page_link,title)
		{
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=970px,height=380px,center=1,resize=0,scrolling=0','../')
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
				var theename=this.contentDoc.getElementById("txt_selected_emp_name");
			  	//document.getElementById('txt_emp_code').value=theemail.value;
				//alert(theform+theemail.value);
				document.getElementById('txt_approved_by').value=theename.value;
				document.getElementById('hid_txt_approved_by').value=theemail.value;
				
			}
		}
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}		

function showResult_search_loan(str,type,div)
{
	//alert(str+type+div);
if (str.length==0)
  {
  document.getElementById(div).innerHTML="";
  document.getElementById(div).style.border="0px";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById(div).innerHTML=xmlhttp.responseText;
	//alert(xmlhttp.responseText);
    }
  }
//alert(str);
xmlhttp.open("GET","includes/list_view_loan.php?search_string="+str+"&type="+type,true);
xmlhttp.send();

}

function hidden_field_reset()
{
	document.getElementById('hid_txt_recommended_by').value="";
	document.getElementById('hid_txt_approved_by').value="";
	document.getElementById('save_up').value="";
	document.getElementById('loan_application_div').innerHTML="";
	document.getElementById('txt_emp_code').disabled=false;
	show_hide_approval(1);
}

function show_hide_approval(str)
{
	if (str==3)
	{
	
		document.getElementById('appr_cap').style.visibility="visible";
		document.getElementById('appr_fld').style.visibility="visible";
		document.getElementById('appr_tr').style.visibility="visible";
	}
	else
	{
		document.getElementById('appr_cap').style.visibility="collapse";
		document.getElementById('appr_fld').style.visibility="collapse";
		document.getElementById('appr_tr').style.visibility="collapse";
	}
}

</script>
<head>
<body style="font-family:verdana; font-size:11px;" onload="show_hide_approval(0)">
<div align="center">
    <div>
        <div class="form_caption">
            Loan Application
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$approve;
            ?>
       </span>
		<div id="messagebox" style="background-color:#FF9999; width:900px; color:#000000"></div>
	</div> 
	<!-- Start Field Set -->
<fieldset style="width:990px">
<legend>Loan Application</legend>
 <form name="loan_application" id="loan_application" action="javascript:fnc_loan_application(save_perm,edit_perm,delete_perm,approve_perm)" method="post"> 
     <!-- Start Field Set -->
	<fieldset style="width:800px ">
	<legend>Emplyoee Info</legend>
	<!-- Start Form -->
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="100" >
						Employee Code
					</td>
					<td width="130"> <!-- Select Calander -->
                     <input type="text" name="txt_emp_code" id="txt_emp_code" placeholder="Double Click to Search" ondblclick="openmypage('search_employee_loan.php','Employee Info'); return false" readonly="true" class="text_boxes" style="width:155px">
					</td>
					<td width="100">
						Name
					</td>
					<td width="130"> <!--  System Generated-->
					<input type="text" name="txt_emp_name" id="txt_emp_name" class="text_boxes" style="width:155px" readonly>
					</td>
					
				</tr>
	
				<tr>
					<td width="100" >
						Company
					</td>
					<td width="130"> <!--  Entry-->
					  <input type="text" name="txt_company" id="txt_company" class="text_boxes" style="width:155px" readonly>
					</td>
					<td width="100">
						Division
					</td>
					<td width="130"> <!-- Select & Display-->
					<input type="text" name="txt_division" id="txt_division" class="text_boxes" style="width:155px" readonly>
					</td>
					
			  </tr>
              <tr>
                <td width="100">
                    Designation
                </td>
                <td width="130"> <!--Entry -->
                  <input type="text" name="txt_designation" id="txt_designation" class="text_boxes" style="width:155px" readonly>
                </td>
                <td width="100">
                    Department
                </td>
                <td width="130"> <!--  Select-->
                  <input type="text" name="txt_department" id="txt_department" class="text_boxes" style="width:155px" readonly>					
                  </td>
            </tr>
			<tr>
                <td width="100" >
                Section 
                </td>
                <td width="130"> <!--  Select-->
                  <input type="text" name="txt_section" id="txt_section" class="text_boxes" style="width:155px" readonly>
                </td>
                <td width="100" >
                Sub Section 
                </td>
                <td width="130"> <!--  Select-->
                <input type="text" name="txt_sub_section" id="txt_sub_section" class="text_boxes" style="width:155px" readonly>
                </td>
			</tr>	
            <tr>
                <td width="100">
                    Basic Salary
                </td>
                <td width="130"> <!--Entry -->
                  <input type="text" name="txt_basic_salary" id="txt_basic_salary" class="text_boxes" style="width:155px" readonly>
                </td>
                <td width="100">
                    Gross Salary
                </td>
                <td width="130"> <!--  Select-->
                  <input type="text" name="txt_gross_salary" id="txt_gross_salary" class="text_boxes" style="width:155px" readonly>					
                  </td>
              </tr>
               <tr>
             	    <td width="100">
						Joining Date
					</td>
					<td width="130"> <input type="text" name="joining_date" id="joining_date" class="text_boxes" style="width:155px" readonly>
                    </td>
                    <td width="100">
						Job Duration
					</td>
					<td width="130"> <!--  Select-->
					  <input type="text" name="txt_duration" id="txt_duration" class="text_boxes" style="width:155px" readonly>					
					</td>
              </tr>
			</table>
			<!-- End Main Table -->
	</fieldset>
    <fieldset style="width:800px ">
    <legend>Loan Info</legend>
    	<table border="0" cellpadding="1" cellspacing="1" width="100%">
        	<tr>
        	 <td width="100">
						Loan Date
					</td>
					<td width="100"><input type="text" name="loan_date" id="loan_date" value="" class="text_boxes" style="width:155px"/>
							 <script type="text/javascript">
                                $( "#loan_date" ).datepicker({
                                dateFormat: 'dd-mm-yy',
                                changeMonth: true,
                                changeYear: true
                                });
                            </script>
                     </td>
                     <td width="100">
						Applied Amount
					</td>
					<td width="130"> <!--  Select-->
					  <input type="text" name="txt_loan_amount" id="txt_loan_amount" class="text_boxes" style="width:155px" onKeyPress=" return numbersonly(this,event)">					
					  </td>
              </tr>
                 <tr>
                 		<td width="100">
						Loan Purpose
					</td>
					<td width="130"> <!--  Select-->
                     <select name="txt_loan_purpose" id="txt_loan_purpose" class="combo_boxes" style="width:165px">
                      <?php
							foreach($loan_purpose as $key => $value)
							{
							?>
							<option value="<?php echo $key;?>"><?php echo $value; ?></option>
							<?php }?>
                     </select>
					</td>
                    <td width="100">
						Loan Type
					</td>
					<td width="130"> 
                    <select name="txt_loan_type" id="txt_loan_type" class="combo_boxes" style="width:165px">
                    	 <?php
						 
						 $sql_data="select * from  lib_payroll_head where  id in(21,44) order by id asc";
						 $sql_data_exe=mysql_query($sql_data);
						 
						 while($data=mysql_fetch_array($sql_data_exe))
						 {
							?>
							<option value="<?php echo $data[id];?>"><?php echo $data[custom_head]; ?></option>
							<?php }?>
                    </select>
					</td>
              </tr>
              <tr>
                    <td width="100">
						Payment Date
					</td>
					<td width="100">
                    <input type="text" name="payment_date" id="payment_date" value="" class="text_boxes" style="width:155px"/>
							 <script type="text/javascript">
                                $( "#payment_date" ).datepicker({
                                dateFormat: 'dd-mm-yy',
                                changeMonth: true,
                                changeYear: true
                                });
                            </script>
                     </td>
                     <td width="100">
						Recommended by
					</td>
					<td width="130"> <!--  Select-->
					  <input type="text" name="txt_recommended_by" id="txt_recommended_by" placeholder="Double Click to Search" ondblclick="openmypage_recommend('search_employee_loan.php','Employee Info'); return false" class="text_boxes" style="width:155px" readonly="readonly">					
					</td>
               </tr>
               <tr>
               		 <td width="100">
						Approved Status
					</td>
					<td width="130"> 
                    <select name="combo_approved_status" id="combo_approved_status" class="combo_boxes" style="width:165px" onchange="show_hide_approval(this.value)">
                    	 <?php
							foreach($approved_status as $key => $value)
							{
							?>
							<option value=<?php echo $key;?> <?php if( $key ==1) { ?> selected <?php } ?>><?php echo $value; ?></option>
							<?php }?>
                    </select>
					</td>
                    <td width="100" id="appr_cap">
						Approved by
					</td>
					<td width="130" id="appr_fld"> <!--  Select-->
					  <input type="text" name="txt_approved_by" id="txt_approved_by" placeholder="Double Click to Search" ondblclick="openmypage_approved('search_employee_loan.php','Employee Info'); return false" class="text_boxes" style="width:155px" readonly="readonly">					
					</td>
               </tr>
               <tr id="appr_tr">
                    <td width="100">
                        Approved Amount
                    </td>
                    <td width="130"> 
                      <input type="text" name="approved_amount" id="approved_amount" class="text_boxes" style="width:155px" onKeyPress=" return numbersonly(this,event)">					
                    </td>
                    <td width="100">
                        Approval Date
                    </td>
                    <td width="130"><input type="text" name="approval_date" id="approval_date" value="" class="text_boxes" style="width:155px"/>
                             <script type="text/javascript">
                                $( "#approval_date" ).datepicker({
                                dateFormat: 'dd-mm-yy',
                                changeMonth: true,
                                changeYear: true
                                });
                            </script>
                     </td>
                </tr>
        </table>
        <br />
        <table>
            <tr>
                <td>
                    <input type="hidden" name="hid_txt_recommended_by" id="hid_txt_recommended_by" />
                    <input type="hidden" name="hid_txt_approved_by" id="hid_txt_approved_by" />
                    <input type="hidden" name="save_up" id="save_up" />
                </td>
                <td colspan="4" align="center" class="button_container" width="100%">
                    <input type="submit" value=" Save" name="save" id="save" class="formbutton" style="width:100px"/>&nbsp;
                     <input type="reset" value="  Refresh  "  name="add" id="close" class="formbutton" onclick="hidden_field_reset()" style="width:100px"/>
                </td>
            </tr>
         </table>
    </fieldset>
    </form>
    <div id="loan_application_div" style="width:990px">
    </div>
</fieldset>
</div>
</body>
<html>