<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 17-04-12
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
    <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
    <script src="../includes/functions.js" type="text/javascript"></script>
   
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
    <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
    <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>

    
 	<script language="javascript">
	function openmypage(page_link,title)
		{
			var company_name=document.getElementById('cbo_company_id').value;
			if(company_name==0)
			{
				//alert("Please Select Company");
				$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_company_id').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(10000);
				});		
			}
			else
			{
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+'?com_name='+company_name, title, 'width=985px,height=380px,center=1,resize=0,scrolling=0','../')
				
				emailwindow.onclose=function()
				{
					var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
					var theemail=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
					//document.getElementById('txt_emp_code').value=theemail.value;
					//alert(theemail.value);
					emp_search_early_adjustment(theemail.value,"early_adjustment");
					setTimeout('show_hide(0)',150);
					
				}
			}
		}
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}		

function showResult_search_loan(str,type,div)
{
	//alert(str+type+div);
if (str.length==0)
  {
  document.getElementById(div).innerHTML="";
  document.getElementById(div).style.border="0px";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById(div).innerHTML=xmlhttp.responseText;
    }
  }
//alert(str);
xmlhttp.open("GET","includes/list_view_loan.php?search_string="+str+"&type="+type,true);
xmlhttp.send();

}

function hidden_field_reset()
{
	document.getElementById('save_up').value="";
	document.getElementById('tot_row').value="";
	document.getElementById('early_adjustment_div').innerHTML="";
	document.getElementById('txt_emp_code').disabled=false;
	show_hide(0);
}

function show_hide(str)
{
	var emp_code=document.getElementById('txt_emp_code').value;
	var tot_balance=document.getElementById('txt_loan_balance').value;
		if (str==2)
		{
			document.getElementById('show_resheduling').style.visibility="collapse";
			document.getElementById('show_resheduling').style.display="none";
			document.getElementById('show_resheduling').innerHTML="";
			$('#show_list').removeAttr('style');
		
			document.getElementById('appr_td').style.visibility="visible";
			document.getElementById('appr_td2').style.visibility="visible";
			document.getElementById('reshudule_tr').style.visibility="collapse";
			document.getElementById('appr_td').innerHTML="Cash Payment";
			document.getElementById('txt_cash').value=tot_balance;
			if(emp_code=="")
			{
				$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#txt_emp_code').focus();
				$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(10000);
				});	
				document.getElementById('combo_adjust_mode').value=0;
				return false;
			}
			else
			{
				//alert(emp_code);
				showResult_search_loan(emp_code,'installment_detail_list','show_list');
			}
			$('#txt_cash').attr("readonly","readonly");
		}
		else if(str==1)
		{
			document.getElementById('show_resheduling').style.visibility="collapse";
			document.getElementById('show_resheduling').style.display="none";
			document.getElementById('show_resheduling').innerHTML="";
			$('#show_list').removeAttr('style');
			
			document.getElementById('appr_td').style.visibility="collapse";
			document.getElementById('appr_td2').style.visibility="collapse";
			document.getElementById('reshudule_tr').style.visibility="collapse";
			document.getElementById('appr_td').innerHTML="Cash Payment";
			document.getElementById('txt_cash').value="";
			if(emp_code=="")
			{
				$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#txt_emp_code').focus();
				$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(10000);
				});	
				document.getElementById('combo_adjust_mode').value=0;
				return false;
			}
			else
			{
				//alert(emp_code);
				showResult_search_loan(emp_code,'installment_detail_list','show_list');
			}
		}
		else if(str==3)
		{
			document.getElementById('show_resheduling').style.visibility="collapse";
			document.getElementById('show_resheduling').style.display="none";
			document.getElementById('show_resheduling').innerHTML="";
			$('#show_list').removeAttr('style');
			
			document.getElementById('appr_td').style.visibility="visible";
			document.getElementById('appr_td2').style.visibility="visible";
			document.getElementById('reshudule_tr').style.visibility="collapse";
			document.getElementById('appr_td').innerHTML="Cash Payment";
			document.getElementById('txt_cash').value=tot_balance;
			if(emp_code=="")
			{
				$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#txt_emp_code').focus();
				$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(10000);
				});	
				document.getElementById('combo_adjust_mode').value=0;
				return false;
			}
			else
			{
				//alert(emp_code);
				showResult_search_loan(emp_code,'installment_detail_list','show_list');
			}
			
			$('#txt_cash').removeAttr('readonly');
		}
		else if(str==4)
		{
			document.getElementById('appr_td').style.visibility="visible";
			document.getElementById('appr_td2').style.visibility="visible";
			document.getElementById('reshudule_tr').style.visibility="visible";
			document.getElementById('appr_td').innerHTML="";
			document.getElementById('appr_td').innerHTML="Balance Amount";
			document.getElementById('txt_cash').value=tot_balance;
			if(emp_code=="")
			{
				$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#txt_emp_code').focus();
				$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(10000);
				});	
				document.getElementById('combo_adjust_mode').value=0;
				return false;
			}
			else
			{
				//alert(emp_code);
				showResult_search_loan(emp_code,'installment_detail_list','show_list');
			}
			
			//$('#txt_cash').removeAttr('readonly');
			$('#txt_cash').attr("readonly","readonly");
			
			document.getElementById('payment_date').value="";
			document.getElementById('txt_total_installment').value="";
		}
		else
		{
			document.getElementById('appr_td').style.visibility="collapse";
			document.getElementById('appr_td2').style.visibility="collapse";
			document.getElementById('txt_cash').value="";
			document.getElementById('show_list').innerHTML="";
		}
}

 function calculate_balance(i)
 {
	var bal_amnt=document.getElementById('txt_loan_balance').value;
	var adjustment_mode=document.getElementById('combo_adjust_mode').value;
	
	var val=0;
	var rest_bal=0;
	if(adjustment_mode==3)
	{
			var per_inst_amnt=document.getElementById('per_inst_amnt'+i).innerHTML;
			var per_adjust_amnt=document.getElementById('adjusted'+i).value;
			var txt_cash_amnt=document.getElementById('txt_cash').value;
			if(per_inst_amnt=="") per_inst_amnt=0; else per_inst_amnt=per_inst_amnt;
			if(per_adjust_amnt=="") per_adjust_amnt=0; else per_adjust_amnt=per_adjust_amnt;
			if(txt_cash_amnt=="") txt_cash_amnt=0; else txt_cash_amnt=txt_cash_amnt;
			 
			/*if( (per_adjust_amnt*1) > (per_inst_amnt*1))
			{
				alert("Over Recieved Not Allowed");
				document.getElementById('adjusted'+i).value="";
				var tot_row=document.getElementById('tot_row').value;
				for(var j=1; j<=tot_row;j++)
				{
					var adjust=document.getElementById('adjusted'+j).value;
					val=val*1+adjust*1;
				}
				document.getElementById('tot_adjustment').innerHTML=val;
				rest_bal=(bal_amnt*1) - (val*1);
				document.getElementById('txt_cash').value=rest_bal;				
				return false;
			}
			else if( (per_adjust_amnt*1) < (per_inst_amnt*1))
			{
				alert("Less Recieved Not Allowed");
				document.getElementById('adjusted'+i).value="";
				var tot_row=document.getElementById('tot_row').value;
				for(var j=1; j<=tot_row;j++)
				{
					var adjust=document.getElementById('adjusted'+j).value;
					val=val*1+adjust*1;
				}
				document.getElementById('tot_adjustment').innerHTML=val;
				rest_bal=(bal_amnt*1) - (val*1);
				document.getElementById('txt_cash').value=rest_bal;
				return false;
			}
			else
			{
				var tot_row=document.getElementById('tot_row').value;
				for(var j=1; j<=tot_row;j++)
				{
					var adjust=document.getElementById('adjusted'+j).value;
					val=val*1+adjust*1;
				}
				document.getElementById('tot_adjustment').innerHTML=val;
				rest_bal=(bal_amnt*1) - (val*1);
				document.getElementById('txt_cash').value=rest_bal;
			}*/
			
			var tot_row=document.getElementById('tot_row').value;
			var total_amount=(txt_cash_amnt*1+per_adjust_amnt*1);
			
			if(document.getElementById('adjusted'+i).value!="")
			{
				if(bal_amnt>total_amount || bal_amnt<total_amount)
				{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
						$('#adjusted'+i).focus();
						$(this).html('Please Type Exact Amount').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});	
					document.getElementById('adjusted'+i).value="";		
				}
			}
			for(var j=1; j<=tot_row;j++)
			{
				if(document.getElementById('adjusted'+j).value=="")
				{
					$('#adjusted'+j).attr("disabled","disabled");
				}
				else
				{
					$('#adjusted'+j).removeAttr('disabled');
				}
				if(document.getElementById('adjusted'+i).value=="")
				{
					$('#adjusted'+j).removeAttr('disabled');
				}
			}
	}
	else if(adjustment_mode==1)
	{
		var per_adjust_amnt=document.getElementById('adjusted'+i).value;
		var tot_row=document.getElementById('tot_row').value;
		//alert (per_adjust_amnt);
		if(document.getElementById('adjusted'+i).value!="")
		{
			if(bal_amnt>per_adjust_amnt || bal_amnt<per_adjust_amnt)
			{
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
					$('#adjusted'+i).focus();
					$(this).html('Please Type Exact Amount').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});	
				document.getElementById('adjusted'+i).value="";		
			}
		}
		//alert (tot_row);
		for(var j=1; j<=tot_row;j++)
		{
			//$('#adjusted'+j).removeAttr('disabled');
			if(document.getElementById('adjusted'+j).value=="")
			{
				$('#adjusted'+j).attr("disabled","disabled");
			}
			else
			{
				$('#adjusted'+j).removeAttr('disabled');
			}
			if(document.getElementById('adjusted'+i).value=="")
			{
				$('#adjusted'+j).removeAttr('disabled');
			}
			
		}
		//document.getElementById('adjusted'+i).value="";
		//document.getElementById('adjusted'+i).disabled=true;
	}
 }
 
 
function cal_amnt(total_installment)
{
	//alert(total_installment);	
	var emp_code=document.getElementById('txt_emp_code').value;
	var txt_loan_balance=document.getElementById('txt_loan_balance').value;
	var txt_cash=document.getElementById('txt_cash').value;
	var combo_adjust_mode=document.getElementById('combo_adjust_mode').value;
	var payment_date=document.getElementById('payment_date').value;
	
	if(emp_code=="")
	{
		alert("Please Select Employee Code");
		document.getElementById('txt_total_installment').value="";
	}
	else
	{	
		//document.getElementById('show_list').innerHTML="";
		//$('#show_list').removeAttr('style');
		//showResult_search_loan(emp_code,'installment_detail_list','show_list');
		
		document.getElementById('show_list').style.visibility="collapse";
		document.getElementById('show_list').style.display="none";
		//var tot_row=document.getElementById('tot_row').value;
		
		$('#show_resheduling').removeAttr('style');
		
		var data=txt_loan_balance+'_'+payment_date+'_'+total_installment;
		//alert (data);
		showResult_search_loan(data,'installment_revise','show_resheduling');
		
		/*if(inst_type==1) 
		{
			document.getElementById('txt_amount_per_inst').value=((app_amnt*1)/(ttl_instm*1)).toFixed(2);	
		}
		else
		{
			document.getElementById('txt_amount_per_inst').value=((app_amnt*1)/(ttl_instm*1)).toFixed(2);
			if(inst_rate=="")
			{
					alert("Please Insert Interest Rate");
					document.getElementById('txt_total_installment').value="";
			}
			else
			{
				var interest=0;
				var ttl_interest=0;
				var per_prin_inst_amnt=app_amnt*1/ttl_instm*1;
				for(var i=0;i<ttl_instm*1;i++)
				{
					interest=(app_amnt*inst_rate)/(100*12);
					app_amnt=app_amnt-per_prin_inst_amnt;
					ttl_interest=ttl_interest+interest;
					//alert(interest);
				}
				//alert("t"+ttl_interest);
				document.getElementById('text_total_interest').value=ttl_interest.toFixed(2);
				var total_amount=ttl_interest+app_amnt_ttl*1;
				document.getElementById('txt_total_amount').value=total_amount.toFixed(2);
			}

		}*/
	}
}

</script>
<head>
<body style="font-family:verdana; font-size:11px;" onload="show_hide(0)">
<div align="center">
    <div>
        <div class="form_caption">
           Loan Adjustment Re-Schedule
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;
            ?>
       </span>
		<div id="messagebox" style="background-color:#FF9999; width:900px; color:#000000"></div>
	</div> 
	<!-- Start Field Set -->
<fieldset style="width:900px ">
<legend>Early Loan Adjustment</legend>
 <form name="early_adjustment" id="early_adjustment" action="javascript:fnc_early_adjustment(save_perm,edit_perm,delete_perm,approve_perm)" method="post"> 
     <!-- Start Field Set -->
	<fieldset style="width:800px ">
	<legend>Emplyoee Loan Info</legend>
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
                	<td width="130" >
						Company Name
					</td>
                	<td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:165px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td width="130" >
						Employee Code
					</td>
					<td width="130"> <!-- Select Calander -->
                     <input type="text" name="txt_emp_code" id="txt_emp_code" placeholder="Double Click to Search" ondblclick="openmypage('search_employee_early_adjustment.php','Employee Info'); return false" readonly="true" class="text_boxes" style="width:155px" autocomplete="off">
					</td>
					
				</tr>
				<tr>
                	<td width="100">
						Employee Name
					</td>
					<td width="130"> <!--  System Generated-->
					<input type="text" name="txt_emp_name" id="txt_emp_name" class="text_boxes" style="width:155px" readonly>
					</td>
        	 		<td width="100">
						Location
					</td>
					<td width="100">
                     <input type="text" name="text_location" id="text_location" class="text_boxes" style="width:155px" readonly="readonly">
                     </td>
                    
             	 </tr>
                 <tr>
                 	<td width="100">
						Division
					</td>
					<td width="130"> 
					  <input type="text" name="txt_division" id="txt_division" class="text_boxes" style="width:155px" readonly="readonly">					
					</td>
                 	<td width="100">
						Department
					</td>
					<td width="130">
                    <input type="text" name="txt_department" id="txt_department" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
                   
              	</tr>
              	<tr>
                	 <td width="100">
						Designation
					</td>
					<td width="130"> <!--  Select-->
					  <input type="text" name="txt_designation" id="txt_designation" class="text_boxes" style="width:155px" readonly="readonly">					
					  </td>
                    <td width="100">
						Section
					</td>
					<td width="100">
                    <input type="text" name="txt_section" id="txt_section" class="text_boxes" style="width:155px" readonly="readonly"/>
          			</td>
             	 </tr>
              	 <tr>
                    <td width="100">
						Sub Section
					</td>
					<td width="130"> <!--  Select-->
					  <input type="text" name="txt_subsection" id="txt_subsection" class="text_boxes" style="width:155px" readonly="readonly">					
					  </td>
                    <td width="100">
						Loan Amount
					</td>
					<td width="130"> 
                    <input type="text" name="txt_loan_amount" id="txt_loan_amount" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
             	 </tr>
                 <tr>
                    <td width="100">
						Loan Adjusted
					</td>
					<td width="130"> 
                    <input type="text" name="txt_loan_adjusted" id="txt_loan_adjusted" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
                    <td width="100">
						Loan Balance
					</td>
					<td width="130"> 
                    <input type="text" name="txt_loan_balance" id="txt_loan_balance" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
             	 </tr>
                  <tr>
                    <td width="100">
						Basic Salary
					</td>
					<td width="130"> 
                    <input type="text" name="txt_basic_salary" id="txt_basic_salary" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
                    <td width="100">
						Gross Salary
					</td>
					<td width="130"> 
                    <input type="text" name="txt_gross_salary" id="txt_gross_salary" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
             	 </tr>
			</table>
			<!-- End Main Table -->
	</fieldset>
    <fieldset style="width:800px ">
    <legend>Adjustment Info</legend>
        <table cellpadding="0" cellspacing="5" width="100%">
        	<tr>
            	<td width="130">
					Adjustment Mode
				</td>
				<td width="130"> 
					<select name="combo_adjust_mode" id="combo_adjust_mode" class="combo_boxes" style="width:165px" onchange="show_hide(this.value)">
                    	 <?php
							foreach($adjust_mode as $key => $value)
							{
							?>
							<option value="<?php echo $key;?>"><?php echo $value; ?></option>
							<?php }?>
                    </select>							
				</td>
                <td width="130" id="appr_td">
					Cash Payment
				</td>
				<td width="130" id="appr_td2"> 
				   <input type="text" name="txt_cash" id="txt_cash" class="text_boxes" style="width:155px" readonly="readonly">
				</td>
            </tr>
            <tr id="reshudule_tr" style="visibility:hidden;">
            	<td width="100" id="payment_date1">Starting Month </td>
                <td width="100" id="payment_date2"> 
                 <input type="text" name="payment_date" id="payment_date" value="" class="text_boxes" style="width:155px"/>
							 <script type="text/javascript">
                                $( "#payment_date" ).datepicker({
                                dateFormat: '01-mm-yy',
                                changeMonth: true,
                                changeYear: true
                                });
                            </script> 
                </td>
                
                <td width="130" id="installment_td"> Total Installment </td>
                <td width="130" id="installment_td2"> 
                	<input type="text" name="txt_total_installment" id="txt_total_installment" class="text_boxes" style="width:155px" onKeyPress=" return numbersonly(this,event)" onblur="cal_amnt(this.value)"  autocomplete="off">					
                </td>
                
            </tr>
        </table>
        
        <div id="show_list" style="width:800px; visibility:visible;">
    	</div> 
        
        <div id="show_resheduling" style="width:600px; visibility:visible;">
    	</div>   
       <!-- <table cellpadding="0" cellspacing="1" width="100%" id="show_list">
        </table>-->
      <br />
        <table>
            <tr>
                <td colspan="4" align="center" class="button_container" width="800"><input type="hidden" name="save_up" id="save_up" />
                    <input type="submit" value=" Save " name="save" id="save" style="width:80px" class="formbutton"/>&nbsp;
                     <input type="reset" value="  Refresh  "  name="reset" id="reset" style="width:80px" class="formbutton" onclick="hidden_field_reset()"/>
                </td>
            </tr>
         </table>
    </fieldset>
    </form>
    <div id="early_adjustment_div" style="width:900px">
    </div>
</fieldset>
</div>
</body>
<html>