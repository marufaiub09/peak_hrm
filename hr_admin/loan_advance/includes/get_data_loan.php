<?php
session_start();
date_default_timezone_set('UTC');
include('../../../includes/common.php');
include('../../../includes/array_function.php');
	//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Payroll Head
	$sql = "SELECT * FROM lib_payroll_head ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_head_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$salary_head_details[$row['id']] = $row['custom_head'];
	}	
	//Employee Name
	$sql = "SELECT *, concat(first_name, ' ', middle_name, ' ', last_name) as name FROM hrm_employee WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_emp_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$var_hrm_emp_name[$row['emp_code']] = array();
		$var_hrm_emp_name[$row['emp_code']] = mysql_real_escape_string($row[name]);
	}
	// function determine duration between two dates
	function my_old($dob){
	$now = date('d-m-Y');
	$dob = explode('-', $dob);
	$now = explode('-', $now);
	$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
	if($now[0] < $dob[0]){
		$now[0] += $mnt[$now[1]-1];
		$now[1]--;
	}
	if($now[1] < $dob[1]){
		$now[1] += 12;
		$now[2]--;
	}
	if($now[2] < $dob[2]) return false;
	return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
}
//$duration = my_old('10-1-1979');
//print_r($age);
//printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);


 $type=$_GET["type"];
 $id=$_GET['getClientId'];
if ($type=="loan_application") // Employee for Loan Application
	{
		$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$id");
		$res = mysql_query("SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE emp_code = '$id' and is_deleted=0 and status_active=1")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
			echo "formObj.txt_company.value = '".mysql_real_escape_string($company_details[$inf["company_id"]])."';\n"; 
			echo "formObj.txt_division.value = '".mysql_real_escape_string($division_details[$inf["division_id"]])."';\n"; 
			echo "formObj.txt_designation.value = '".mysql_real_escape_string($designation_chart[$inf["designation_id"]])."';\n"; 
			echo "formObj.txt_department.value = '".mysql_real_escape_string($department_details[$inf["department_id"]])."';\n"; 
			echo "formObj.txt_section.value = '".$section_details[$inf["section_id"]]."';\n"; 
			echo "formObj.txt_sub_section.value = '".$subsection_details[$inf["subsection_id"]]."';\n"; 
			echo "formObj.txt_basic_salary.value = '".$basic_salary."';\n"; 
			echo "formObj.txt_gross_salary.value = '".mysql_real_escape_string($inf["gross_salary"])."';\n"; 
			echo "formObj.joining_date.value = '".mysql_real_escape_string($inf["joining_date"])."';\n"; 
			$joining_date=convert_to_mysql_date($inf['joining_date']);
			$duration = my_old($joining_date);
			$year=$duration[year]." Years, ";
			$month=$duration[mnt]." Months, ";
			$day=$duration[day]." Days";
			$job_duration=$year.$month.$day;
			echo "formObj.txt_duration.value = '".$job_duration."';\n";  
			//echo "formObj.save_up_po.value = '".$inf["id"]."';\n"; 

		} 
		
	}
	
if($type=="loan_application_update")
{
	  $res = mysql_query("SELECT * FROM hrm_loan_application WHERE id = '$id' and is_deleted=0 and status_active=1")  or die(mysql_error());
	
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.loan_date.value = '".convert_to_mysql_date($inf["loan_date"])."';\n";    
			echo "formObj.txt_loan_amount.value = '".mysql_real_escape_string($inf["loan_amount"])."';\n";    
			echo "formObj.payment_date.value = '".convert_to_mysql_date($inf["payment_date"])."';\n"; 
			echo "formObj.txt_loan_type.value = '".$salary_head_details[$inf["loan_type"]]."';\n"; 
			echo "formObj.txt_loan_purpose.value = '".mysql_real_escape_string($inf["loan_purpose"])."';\n"; 
			echo "formObj.combo_approved_status.value = '".mysql_real_escape_string($inf["approved_status"])."';\n"; 
			echo "formObj.txt_approved_by.value = '".mysql_real_escape_string($var_hrm_emp_name[$inf["approved_by"]])."';\n"; 
			echo "formObj.txt_recommended_by.value = '".mysql_real_escape_string($var_hrm_emp_name[$inf["recommended_by"]])."';\n"; 
			echo "formObj.hid_txt_approved_by.value = '".mysql_real_escape_string($inf["approved_by"])."';\n"; 
			echo "formObj.hid_txt_recommended_by.value = '".mysql_real_escape_string($inf["recommended_by"])."';\n"; 
			echo "formObj.approved_amount.value = '".mysql_real_escape_string($inf["approved_amount"])."';\n"; 
			echo "formObj.approval_date.value = '".convert_to_mysql_date($inf["approval_date"])."';\n"; 
			echo "formObj.save_up.value = '".$inf["id"]."';\n"; 
			echo "$('#txt_emp_code').attr('disabled','true')".";\n";
			
			/*echo "document.getElementById('loan_date').value 				= '".convert_to_mysql_date($inf["loan_date"])."';\n";
			echo "document.getElementById('txt_loan_amount').value 			= '".mysql_real_escape_string($inf["loan_amount"])."';\n";
			echo "document.getElementById('txt_loan_purpose').value 		= '".mysql_real_escape_string($inf["loan_purpose"])."';\n";
			echo "document.getElementById('txt_loan_type').value 			= '".mysql_real_escape_string($inf["loan_type"])."';\n";
			echo "document.getElementById('payment_date').value 			= '".convert_to_mysql_date($inf["payment_date"])."';\n";
			echo "document.getElementById('txt_recommended_by').value 		= '".mysql_real_escape_string($var_hrm_emp_name[$inf["recommended_by"]])."';\n";
			echo "document.getElementById('combo_approved_status').value 	= '".mysql_real_escape_string($inf["approved_status"])."';\n";
			echo "document.getElementById('txt_approved_by').value 			= '".mysql_real_escape_string($inf["approved_by"])."';\n";
			echo "document.getElementById('approved_amount').value 			= '".mysql_real_escape_string($inf["approved_amount"])."';\n";
			echo "document.getElementById('approval_date').value 			= '".convert_to_mysql_date($inf["approval_date"])."';\n";
			echo "document.getElementById('hid_txt_approved_by').value 			= '".mysql_real_escape_string($inf["approved_by"])."';\n";
			echo "document.getElementById('hid_txt_recommended_by').value 		= '".mysql_real_escape_string($inf["recommended_by"])."';\n";
			echo "document.getElementById('save_up').value 					= '".$inf["id"]."';\n";
			echo "$('#txt_emp_code').attr('disabled','true')".";\n";*/
		} 
		
}
if($type=="payback_scheduling")
{
	$res = mysql_query("SELECT * FROM hrm_loan_application WHERE emp_code = '$id' and is_deleted=0 and status_active=1")  or die(mysql_error());
	
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($var_hrm_emp_name[$inf["emp_code"]])."';\n";  
			echo "formObj.loan_date.value = '".convert_to_mysql_date($inf["loan_date"])."';\n";    
			echo "formObj.txt_loan_amount.value = '".mysql_real_escape_string($inf["loan_amount"])."';\n";    
			echo "formObj.payment_date.value = '".convert_to_mysql_date($inf["payment_date"])."';\n"; 
			echo "formObj.txt_loan_type.value = '".$salary_head_details[$loan_type[$inf["loan_type"]]]."';\n"; 
			echo "formObj.txt_loan_purpose.value = '".mysql_real_escape_string($loan_purpose[$inf["loan_purpose"]])."';\n"; 
			echo "formObj.txt_approved_by.value = '".mysql_real_escape_string($var_hrm_emp_name[$inf["approved_by"]])."';\n"; 
			echo "formObj.txt_recommended_by.value = '".mysql_real_escape_string($var_hrm_emp_name[$inf["recommended_by"]])."';\n"; 
			echo "formObj.txt_approved_amount.value = '".mysql_real_escape_string($inf["approved_amount"])."';\n"; 
			//echo "formObj.approval_date.value = '".mysql_real_escape_string($inf["approval_date"])."';\n"; 
			
		} 
}
if($type=="payback_scheduling_update")
{
	$res = mysql_query("SELECT * FROM hrm_loan_payback_scheduling_mst WHERE id = '$id' and is_deleted=0 and status_active=1")  or die(mysql_error());
	
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.combo_interest_type.value = '".$inf["interest_type"]."';\n";    
			echo "formObj.txt_interest_rate.value = '".mysql_real_escape_string($inf["interest_rate"])."';\n";    
			echo "formObj.txt_total_installment.value = '".$inf["total_installment"]."';\n"; 
			echo "formObj.text_total_interest.value = '".mysql_real_escape_string($inf["total_interest"])."';\n"; 
			echo "formObj.txt_total_amount.value = '".mysql_real_escape_string($inf["ttl_amnt_with_ints"])."';\n"; 
			echo "formObj.combo_adjustment_source.value = '".$inf["adjustment_source"]."';\n"; 
			echo "formObj.txt_starting_month.value = '".$inf["starting_month"]."';\n"; 
			echo "$('#txt_starting_month').attr('disabled','true')".";\n";
			echo "formObj.combo_adjustment_advice.value = '".mysql_real_escape_string($inf["adjustment_advice"])."';\n"; 
			echo "formObj.txt_amount_per_inst.value = '".mysql_real_escape_string($inf["per_installment_amnt_prin"])."';\n"; 
			echo "formObj.txt_install_no_for_inst.value = '".mysql_real_escape_string($inf["num_isnt_for_int"])."';\n"; 
			echo "formObj.save_up.value = '".$inf["id"]."';\n"; 
			echo "$('#txt_emp_code').attr('disabled','true')".";\n";
			echo "show_hide_interest($inf[adjustment_advice]);";
			
			if(mysql_real_escape_string($inf["adjustment_advice"])==0)
			{
				echo "$('#combo_adjustment_advice').attr('disabled','true')".";\n";
			}
		} 
}
if ($type=="early_adjustment") // Employee for Loan Application
	{
		$balance_amount=0;
		$loan_amount=return_field_value("ttl_amnt_with_ints","hrm_loan_payback_scheduling_mst","status_active=1 and emp_code=$id  and is_deleted=0");
		$adjusted_amount=return_field_value("sum(total_amnt_per_inst)","hrm_loan_payback_scheduling_details", "emp_code='$id' and type=0 and is_deleted=0 and is_paid<>0 and status_active=1 group by emp_code");		
		$balance_amount=$loan_amount-$adjusted_amount;
		
		$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$id");
		$gross_salary = return_field_value("gross_salary","hrm_employee","emp_code=$id");
		
		$res = mysql_query("SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE emp_code = '$id' and is_deleted=0 and status_active=1")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.cbo_company_id.value = '".mysql_real_escape_string($inf["company_id"])."';\n";
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
			echo "formObj.text_location.value = '".$location_details[$inf["location_id"]]."';\n";
			echo "formObj.txt_division.value = '".$division_details[$inf["division_id"]]."';\n"; 
			echo "formObj.txt_department.value = '".$department_details[$inf["department_id"]]."';\n";
			echo "formObj.txt_designation.value = '".$designation_chart[$inf["designation_id"]]."';\n"; 
			echo "formObj.txt_section.value = '".$section_details[$inf["section_id"]]."';\n"; 
			echo "formObj.txt_subsection.value = '".$subsection_details[$inf["subsection_id"]]."';\n";
			echo "formObj.txt_loan_amount.value = '".mysql_real_escape_string($loan_amount)."';\n"; 
			echo "formObj.txt_loan_adjusted.value = '".mysql_real_escape_string($adjusted_amount)."';\n"; 
			echo "formObj.txt_loan_balance.value = '".mysql_real_escape_string($balance_amount)."';\n";  
			echo "formObj.txt_basic_salary.value = '".mysql_real_escape_string($basic_salary)."';\n"; 
			echo "formObj.txt_gross_salary.value = '".mysql_real_escape_string($gross_salary)."';\n"; 

		} 
		
	}
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}


?> 