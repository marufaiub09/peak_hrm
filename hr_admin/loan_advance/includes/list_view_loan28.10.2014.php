<!--<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" />-->
<?php

include('../../../includes/common.php');
include('../../../includes/array_function.php');

$search_string=trim($_GET["search_string"]);
$type=$_GET["type"];

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Employee Name
$sql = "SELECT *, concat(first_name, ' ', middle_name, ' ', last_name) as name FROM hrm_employee WHERE is_deleted = 0 and status_active=1 ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$var_hrm_emp_name = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$var_hrm_emp_name[$row['emp_code']] = array();
	$var_hrm_emp_name[$row['emp_code']] = mysql_real_escape_string($row[name]);
}


// Loan Application	
if($type=="loan_app")
{?>
<div>
    <div style="width:990px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="80" align="left"><strong>Emp. Code</strong></th>
                <th width="80" align="left"><strong>Loan Date</strong></th>
                <th width="80" align="left"><strong>Loan Amount</strong></th>
                <th width="80" align="left"><strong>Payment Date</strong></th>
                <th width="80" align="left"><strong>Loan Type</strong></th>
                <th width="80" align="left"><strong>Loan Purpose</strong></th>
                <th width="100" align="left"><strong>Recomended By</strong></th>
                <th width="100" align="left"><strong>Approved Status</strong></th>
                <th width="100" align="left"><strong>Approved By</strong></th>
                <th width="100" align="left"><strong>Approved Amount</strong></th>
                <th align="left"><strong>Approval Date</strong></th>
            </thead>
		</table>
	</div>	
	<div style="width:990px; overflow-y:scroll; min-height:50px; max-height:250px;" id="loan_entry_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >

	<?php
        $i=1;
        $sql="select * from hrm_loan_application where emp_code='$search_string' and status_active=1 and is_deleted=0";
        $result=mysql_query($sql);
        while($row=mysql_fetch_assoc($result))
        {
    	   if($row[approved_by]==""){ ?>
            <tr style="text-decoration:none; cursor:pointer" onclick="loan_application_update('<?php echo $row[id];?>','loan_application_update');" title="Click For Update">
          <?
           }
           else
           {
           ?>
           <tr style="text-decoration:none;" title="No Update">
	     <? } ?> 
               <td width="80" ><?php echo $row[emp_code];?></td>
               <td width="80" ><?php echo convert_to_mysql_date($row[loan_date]);?></td>
               <td width="80"  align="right"><?php echo $row[loan_amount];?></td>
               <td width="80" ><?php echo $row[payment_date];?></td>
               <td width="80" ><?php echo $advance_loan_type[$row[loan_type]];?></td>
               <td width="80" ><?php echo $loan_purpose[$row[loan_purpose]];?></td>
               <td width="100" ><?php echo $var_hrm_emp_name[$row[recommended_by]];?></td>
               <td width="100" ><?php echo $approved_status[$row[approved_status]];?></td>
               <td width="100" ><?php echo $var_hrm_emp_name[$row[approved_by]];?></td>
               <td width="100" ><?php echo $row[approved_amount];?></td>
               <td><?php echo convert_to_mysql_date($row[approval_date]);?></td>
            </tr>
		<?php		
        $i++;}
	?>
		</table>
	</div>
</div>
<?	
}
//Payback Scheduling
if($type=="payback_scheduling")
{ ?>
<div>
    <div style="width:990px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="80" align="left"><strong>Emp. Code</strong></th>
                <th width="80" align="left"><strong>Approved Amount</strong></th>
                <th width="80" align="left"><strong>Interest Type</strong></th>
                <th width="70" align="left"><strong>Interest Rate</strong></th>
                <th width="60" align="left"><strong>Total Install.</strong></th>
                <th width="80" align="left"><strong>Total Interest</strong></th>
                <th width="90" align="left"><strong>Total Amount (With Interest)</strong></th>
                <th width="90" align="left"><strong>Adjustment Source</strong></th>
                <th width="80" align="left"><strong>Starting Month</strong></th>
                <th width="100" align="left"><strong>Adjustment Advice</strong></th>
                <th width="80" align="left"><strong>Install. No. for Inter.</strong></th>
                <th align="left"><strong>Install. Amount (Prin.)</strong></th>
            </thead>
		</table>
	</div>	
	<div style="width:990px; overflow-y:scroll; min-height:50px; max-height:250px;" id="loan_entry_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >

<?php 
$i=1;
	$adjustment_arr=array(0=>"",1=>"Interest Included",2=>"Principal first",3=>"Interest First");
	$sql="select * from hrm_loan_payback_scheduling_mst where emp_code='$search_string' and status_active=1 and is_deleted=0";
	$result=mysql_query($sql);
	while($row=mysql_fetch_assoc($result))
	{
?>
            <tr style="text-decoration:none; cursor:pointer" onclick="payback_scheduling_update('<?php echo $row[id];?>','payback_scheduling_update');" title="Click For Update">
               <td width="80"><?php echo $row[emp_code];?></td> 
               <td width="80" align="right"><?php echo $row[approved_amount];?></td>
               <td width="80"><?php echo $interest_type[$row[interest_type]];?></td>
               <td width="70" align="right"><?php echo $row[interest_rate];?></td>
               <td width="60" align="right"><?php echo $row[total_installment];?></td>
               <td width="80" align="right"><?php echo $row[total_interest];?></td>
               <td width="90" align="right"><?php echo $row[ttl_amnt_with_ints];?></td>
               <td width="90"><?php echo $adjustment_source[$row[adjustment_source]];?></td>
               <td width="80"><?php echo $row[starting_month];?></td>
               <td width="100"><?php echo $adjustment_arr[$row[adjustment_advice]];?></td>
               <td width="80" align="right"><?php echo $row[num_isnt_for_int];?></td>
               <td align="right"><?php echo $row[per_installment_amnt_prin];?></td>
           </tr>
            <!--<?php?><input type="button" value="Print Schedule" onclick="see_schedule( //echo $row[id]; )" class="formbutton"/>-->
<?php	
$i++;}
?>
		</table>
    </div>
</div>
<?php
}
if($type=="installment_detail_list")
{?>
<div>
    <div style="width:800px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="130" align="left"><strong>Installment No.</strong></th>
                <th width="160" align="left"><strong>Installment Month</strong></th>
                <th width="160" align="left"><strong>Installment Amount</strong></th>
                <th width="160" align="left"><strong>Adjusted Amount</strong></th>
                <th align="left"><strong>&nbsp;</strong></th>
                <!--<th align="left"><strong>Adjusted Mode</strong></th>-->
            </thead>
		</table>
	</div>	
	<div style="width:800px; overflow-y:scroll; min-height:50px; max-height:250px;" id="loan_entry_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >

<?php 
		$sql="select * from hrm_loan_payback_scheduling_details where emp_code='$search_string' and status_active=1 and is_deleted=0 and type=0 ";// and is_paid=0
		$result=mysql_query($sql);
		$i=1;
		$total_amount=0;
		while($row=mysql_fetch_assoc($result))
		{
?>
            <tr>
                <td width="130">&nbsp;<?php echo $i;?></td> 
                <td width="160"><?php echo $months[$row['month']].", ".$row['year'];?></td>
                <td width="160" align="right" id="per_inst_amnt<? echo $i; ?>"><?php echo number_format($row['total_amnt_per_inst'],2,'.','');
                			$total_amount=$total_amount+$row['total_amnt_per_inst'];?></td>
                <td width="160" align="center"><input class="text_boxes" style="width:80px;text-align:right;"  name="adjusted<? echo $i; ?>" id="adjusted<? echo $i; ?>" type="text" onkeypress="return numbersonly(this,event)" value="" onchange="calculate_balance(<?php echo $i;?>)"/></td>
                <td>&nbsp;<input type="hidden" name="tot_id<? echo $i; ?>" id="tot_id<? echo $i; ?>" value="<? echo $row['id']; ?>" /></td>
             </tr>
<?php	
		$i++;}
?><input type="hidden" name="tot_row" id="tot_row" value="<? echo $i-1; ?>" />
           <tfoot>
                <th></th>
                <th align="center">Total:</th>
                <th align="right"><?php echo number_format($total_amount,2,'.',''); ?></th>
                <th align="center" id="tot_adjustment"><?php echo "0.00"; ?></th>
                <th>&nbsp;</th>
            </tfoot>
    	</table>
    </div>
</div>
<?php 
}
//---------------END Manual Attendence Search---------------------------------//
function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}
?>

