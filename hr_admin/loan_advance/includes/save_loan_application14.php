<?php
session_start();

date_default_timezone_set('UTC');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
$date=time();
$user_name=$_SESSION['logic_erp']["user_name"];

include('../../../includes/common.php');

extract($_GET);
if($action=="loan_application")
{
	$loan_date=convert_to_mysql_date($loan_date);
	$payment_date=convert_to_mysql_date($payment_date);
	$approval_date=convert_to_mysql_date($approval_date);	
   		if  ($isupdate=="")
		{
		 
			$id_field_name = "id";
			$table_name = "hrm_loan_application";
			$id= return_next_id($id_field_name,$table_name);
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 

			$sql = "INSERT INTO hrm_loan_application(
						id,
						emp_code,
						basic_salary,
						gross_salary,
						joining_date,
						loan_date,
						loan_amount,
						payment_date,
						loan_type,
						loan_purpose,
						approved_by,
						recommended_by,
						approved_status,
						approved_amount,
						approval_date,
						inserted_by,
						insert_date
					) VALUES(
						'$id',
						'".mysql_real_escape_string($txt_emp_code)."',
						'".mysql_real_escape_string($txt_basic_salary)."',
						'".mysql_real_escape_string($txt_gross_salary)."',
						'".mysql_real_escape_string($joining_date)."',
						'".mysql_real_escape_string($loan_date)."',
						'".mysql_real_escape_string($txt_loan_amount)."',
						'".mysql_real_escape_string($payment_date)."',
						'".mysql_real_escape_string($txt_loan_type)."',
						'".mysql_real_escape_string($txt_loan_purpose)."',
						'".mysql_real_escape_string($hid_txt_approved_by)."',
						'".mysql_real_escape_string($hid_txt_recommended_by)."',
						'".mysql_real_escape_string($combo_approved_status)."',
						'".mysql_real_escape_string($approved_amount)."',
						'".mysql_real_escape_string($approval_date)."',
						'".mysql_real_escape_string($user_name)."',
						'$date'
						)";
			
			mysql_query( $sql ) or die (mysql_error());	
			
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo "1";
			exit();
			
		}
		else 
		{
				mysql_query("SET CHARACTER SET utf8");
				mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
				
				$sql = "UPDATE hrm_loan_application
					 SET
						loan_date='".mysql_real_escape_string($loan_date)."',
						loan_amount='".mysql_real_escape_string($txt_loan_amount)."',
						payment_date='".mysql_real_escape_string($payment_date)."',
						loan_type='".mysql_real_escape_string($txt_loan_type)."',
						loan_purpose='".mysql_real_escape_string($txt_loan_purpose)."',
						approved_by='".mysql_real_escape_string($hid_txt_approved_by)."',
						recommended_by='".mysql_real_escape_string($hid_txt_recommended_by)."',
						approved_status='".mysql_real_escape_string($combo_approved_status)."',
						approved_amount='".mysql_real_escape_string($approved_amount)."',
						approval_date='".mysql_real_escape_string($approval_date)."',
						updated_by='".mysql_real_escape_string($user_name)."',
						update_date='$date'
						WHERE id= '$isupdate'";
						
				mysql_query( $sql ) or die (mysql_error());
				
				$sql_history=encrypt($sql, "logic_erp_2011_2012");
				$_SESSION['sql_history']=$sql_history;
				echo "2";		
				exit();
		 }
		 
}
if($action=="payback_scheduling")
{
	$txt_starting_month=convert_to_mysql_date($txt_starting_month);
	
	$all_query='';
	
   		if  ($isupdate=="")
		{
		 
			$id_field_name = "id";
			$table_name = "hrm_loan_payback_scheduling_mst";
			$id= return_next_id($id_field_name,$table_name);
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			$loan_id = return_field_value("id","hrm_loan_application","emp_code=$txt_emp_code and is_deleted=0 and status_active=1 and approved_status=3");
							   
			$sql = "INSERT INTO hrm_loan_payback_scheduling_mst(
						id,
						loan_id,
						emp_code,
						approved_amount,
						interest_type,
						total_installment,
						interest_rate,
						total_interest,
						ttl_amnt_with_ints,
						adjustment_source,
						starting_month,
						adjustment_advice,
						per_installment_amnt_prin,
						num_isnt_for_int,
						inserted_by,
						insert_date
					) VALUES(
						'$id',
						'$loan_id',
						'".mysql_real_escape_string($txt_emp_code)."',
						'".mysql_real_escape_string($txt_approved_amount)."',
						'".mysql_real_escape_string($combo_interest_type)."',
						'".mysql_real_escape_string($txt_total_installment)."',
						'".mysql_real_escape_string($txt_interest_rate)."',
						'".mysql_real_escape_string($text_total_interest)."',
						'".mysql_real_escape_string($txt_total_amount)."',
						'".mysql_real_escape_string($combo_adjustment_source)."',
						'".mysql_real_escape_string($txt_starting_month)."',
						'".mysql_real_escape_string($combo_adjustment_advice)."',
						'".mysql_real_escape_string($txt_amount_per_inst)."',
						'".mysql_real_escape_string($txt_install_no_for_inst)."',
						'".mysql_real_escape_string($user_name)."',
						'$date'
						)";
			
			mysql_query( $sql ) or die (mysql_error());	
			$all_query.=$sql;
			
			$payback_mst_id = return_field_value("id","hrm_loan_payback_scheduling_mst","emp_code=$txt_emp_code and is_deleted=0 and status_active=1");
			if($combo_adjustment_advice==2)
			{
				$prin_amnt_per_inst=$txt_approved_amount/$txt_install_no_for_inst;
				$interest_no_install=$txt_total_installment-$txt_install_no_for_inst;
				
				
				for($i=0;$i<$txt_total_installment;$i++)
				{
					$month=date("m",strtotime(add_month($txt_starting_month,$i)));
					$year=date("Y",strtotime(add_month($txt_starting_month,$i)));
					
					$pay_date=$year."-".$month."-"."01";
					
					if($i>=$txt_install_no_for_inst)
					{
						$interest_per_installment=$text_total_interest/$interest_no_install;
						$prin_amnt_per_inst=0;
						$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							inserted_by,
							insert_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($prin_amnt_per_inst)."',
							'".mysql_real_escape_string($interest_per_installment)."',
							'".mysql_real_escape_string($interest_per_installment)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
					}
					else
					{
						$interest_per_install=0;
						$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							inserted_by,
							insert_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($prin_amnt_per_inst)."',
							'".mysql_real_escape_string($interest_per_install)."',
							'".mysql_real_escape_string($prin_amnt_per_inst)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
					}
					
				}
			}
			else if($combo_adjustment_advice==3)
			{				
				$interest_per_install=$text_total_interest/$txt_install_no_for_inst;
				$prin_no_install=$txt_total_installment-$txt_install_no_for_inst;
				$prin_amnt_per_inst=$txt_approved_amount/$prin_no_install;
				
				for($i=0;$i<$txt_total_installment;$i++)
				{
					$month=date("m",strtotime(add_month($txt_starting_month,$i)));
					$year=date("Y",strtotime(add_month($txt_starting_month,$i)));
					
					$pay_date=$year."-".$month."-"."01";
					
					if($i>=$txt_install_no_for_inst)
					{
						$prin_amnt_per_installment=$txt_approved_amount/$prin_no_install;
						$interest_per_install=0;
						$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							inserted_by,
							insert_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($prin_amnt_per_installment)."',
							'".mysql_real_escape_string($interest_per_install)."',
							'".mysql_real_escape_string($prin_amnt_per_installment)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
					}
					else
					{
						$prin_amnt_per_inst=0;
						$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							inserted_by,
							insert_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($prin_amnt_per_inst)."',
							'".mysql_real_escape_string($interest_per_install)."',
							'".mysql_real_escape_string($interest_per_install)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
					}
					
				}
			}
			else
			{
				$interest=0;
				for($i=0;$i<$txt_total_installment;$i++)
				{
					$month=date("m",strtotime(add_month($txt_starting_month,$i)));
					$year=date("Y",strtotime(add_month($txt_starting_month,$i)));
					
					$pay_date=$year."-".$month."-"."01";
					
					$interest=($txt_approved_amount*$txt_interest_rate)/(100*12);
					$txt_approved_amount=$txt_approved_amount-$txt_amount_per_inst;
					$total_amnt_per_inst=$interest+$txt_amount_per_inst;
					$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							inserted_by,
							insert_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($txt_amount_per_inst)."',
							'".mysql_real_escape_string($interest)."',
							'".mysql_real_escape_string($total_amnt_per_inst)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
				}
			}
		
		$sql_history=encrypt($all_query, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "1";
		exit();
	}
	else 
	{
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			
			$sql = "UPDATE hrm_loan_payback_scheduling_mst
				 SET
					interest_type='$combo_interest_type',
					total_installment='$txt_total_installment',
					interest_rate='$txt_interest_rate',
					total_interest='$text_total_interest',
					ttl_amnt_with_ints='$txt_total_amount',
					adjustment_source='$combo_adjustment_source',
					starting_month='$txt_starting_month',
					adjustment_advice='$combo_adjustment_advice',
					per_installment_amnt_prin='$txt_amount_per_inst',
					num_isnt_for_int='$txt_install_no_for_inst',
					updated_by='".mysql_real_escape_string($user_name)."',
					update_date='$date'
					WHERE id= '$isupdate'";
					
				mysql_query( $sql ) or die (mysql_error());
				$all_query=$sql;
				
				$query="Delete from hrm_loan_payback_scheduling_details where emp_code='$txt_emp_code'";
				$result=mysql_query($query);
				$all_query.=$query;
				
				$payback_mst_id = return_field_value("id","hrm_loan_payback_scheduling_mst","emp_code=$txt_emp_code and is_deleted=0 and status_active=1");
	
				if($combo_adjustment_advice==2)
				{
					$prin_amnt_per_inst=$txt_approved_amount/$txt_install_no_for_inst;
					$interest_no_install=$txt_total_installment-$txt_install_no_for_inst;
			
			
					for($i=0;$i<$txt_total_installment;$i++)
					{
						$month=date("m",strtotime(add_month($txt_starting_month,$i)));
						$year=date("Y",strtotime(add_month($txt_starting_month,$i)));
						
						$pay_date=$year."-".$month."-"."01";
						
						if($i>=$txt_install_no_for_inst)
						{
							$interest_per_installment=$text_total_interest/$interest_no_install;
							$prin_amnt_per_inst=0;
							$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
								id,
								payback_mst_id,
								emp_code,
								month,
								year,
								pay_date,
								principal_amnt,
								interest,
								total_amnt_per_inst,
								updated_by,
								update_date
							) VALUES(
								'',
								'$payback_mst_id',
								'".mysql_real_escape_string($txt_emp_code)."',
								'".mysql_real_escape_string($month)."',
								'".mysql_real_escape_string($year)."',
								'".mysql_real_escape_string($pay_date)."',
								'".mysql_real_escape_string($prin_amnt_per_inst)."',
								'".mysql_real_escape_string($interest_per_installment)."',
								'".mysql_real_escape_string($interest_per_installment)."',
								'".mysql_real_escape_string($user_name)."',
								'$date'
								)";
						mysql_query( $sql2 ) or die (mysql_error());
						$all_query.=$sql2;
						}
						else
						{
							$interest_per_install=0;
							$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
								id,
								payback_mst_id,
								emp_code,
								month,
								year,
								pay_date,
								principal_amnt,
								interest,
								total_amnt_per_inst,
								updated_by,
								update_date
							) VALUES(
								'',
								'$payback_mst_id',
								'".mysql_real_escape_string($txt_emp_code)."',
								'".mysql_real_escape_string($month)."',
								".mysql_real_escape_string($year)."',
								'".mysql_real_escape_string($pay_date)."',
								'".mysql_real_escape_string($prin_amnt_per_inst)."',
								'".mysql_real_escape_string($interest_per_install)."',
								'".mysql_real_escape_string($prin_amnt_per_inst)."',
								'".mysql_real_escape_string($user_name)."',
								'$date'
								)";
						mysql_query( $sql2 ) or die (mysql_error());
						$all_query.=$sql2;
						}
				
					}
			}
			else if($combo_adjustment_advice==3)
			{				
				$interest_per_install=$text_total_interest/$txt_install_no_for_inst;
				$prin_no_install=$txt_total_installment-$txt_install_no_for_inst;
				$prin_amnt_per_inst=$txt_approved_amount/$prin_no_install;
				
				for($i=0;$i<$txt_total_installment;$i++)
				{
					$month=date("m",strtotime(add_month($txt_starting_month,$i)));
					$year=date("Y",strtotime(add_month($txt_starting_month,$i)));
					
					$pay_date=$year."-".$month."-"."01";
					
					if($i>=$txt_install_no_for_inst)
					{
						$prin_amnt_per_installment=$txt_approved_amount/$prin_no_install;
						$interest_per_install=0;
						$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							updated_by,
							update_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($prin_amnt_per_installment)."',
							'".mysql_real_escape_string($interest_per_install)."',
							'".mysql_real_escape_string($prin_amnt_per_installment)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
					}
					else
					{
						$prin_amnt_per_inst=0;
						$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							updated_by,
							update_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($prin_amnt_per_inst)."',
							'".mysql_real_escape_string($interest_per_install)."',
							'".mysql_real_escape_string($interest_per_install)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
					}
				
				}
			}
			else
			{
				$interest=0;
				for($i=0;$i<$txt_total_installment;$i++)
				{
					$month=date("m",strtotime(add_month($txt_starting_month,$i)));
					$year=date("Y",strtotime(add_month($txt_starting_month,$i)));
					
					$pay_date=$year."-".$month."-"."01";
					
					$interest=($txt_approved_amount*$txt_interest_rate)/(100*12);
					$txt_approved_amount=$txt_approved_amount-$txt_amount_per_inst;
					$total_amnt_per_inst=$interest+$txt_amount_per_inst;
					$sql2 = "INSERT INTO hrm_loan_payback_scheduling_details(
							id,
							payback_mst_id,
							emp_code,
							month,
							year,
							pay_date,
							principal_amnt,
							interest,
							total_amnt_per_inst,
							updated_by,
							update_date
						) VALUES(
							'',
							'$payback_mst_id',
							'".mysql_real_escape_string($txt_emp_code)."',
							'".mysql_real_escape_string($month)."',
							'".mysql_real_escape_string($year)."',
							'".mysql_real_escape_string($pay_date)."',
							'".mysql_real_escape_string($txt_amount_per_inst)."',
							'".mysql_real_escape_string($interest)."',
							'".mysql_real_escape_string($total_amnt_per_inst)."',
							'".mysql_real_escape_string($user_name)."',
							'$date'
							)";
					mysql_query( $sql2 ) or die (mysql_error());
					$all_query.=$sql2;
				}
			}
			
			$sql_history=encrypt($all_query, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;			
			echo "2";		
			exit();
	}
		
}


if($action=="early_adjustment")
{
	$all_query='';
	$tot_paid=explode("_",$tot_paid);
	for($i=0;$i<$tot_row;$i++)
	{
		$val = explode(",", $tot_paid[$i]);
		$sql = "UPDATE hrm_loan_payback_scheduling_details
				 SET
					is_paid='".$val[1]."'
					WHERE id= '".$val[2]."' and emp_code='$txt_emp_code'
				";
		mysql_query($sql);
		$all_query.=$sql;
	}
	
	$sql_history=encrypt($all_query, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;			
	echo "1";
	exit();
}
function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}	

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}
?>
