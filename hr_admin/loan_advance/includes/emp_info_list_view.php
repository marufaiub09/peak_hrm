<?php
include('../../../includes/common.php');
//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
$search_string=$_GET["search_string"];
$search_string2=$_GET["search_string2"];
$type=$_GET["type"];
$company_name=$_GET["com_name"];

// Employee information for Resource Allocation
if($type=="emp_loan_entry")
{
 $search_string=trim($search_string);
?>
<div>
    <div style="width:930px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="30" align="left"><strong>SL</strong></th>
                <th width="80" align="left"><strong>Emp. Code</strong></th>
                <th width="70" align="left"><strong>Emp. ID</strong></th>
                <th width="90" align="left"><strong>Punch Card</strong></th>
                <th width="130" align="left"><strong>Emp. Name</strong></th>
                <th width="130" align="left"><strong>Designation</strong></th>
                <th width="120" align="left"><strong>Company Name</strong></th>
                <th width="90" align="left"><strong>Division</strong></th>
                <th width="90" align="left"><strong>Department</strong></th>
                <th align="left"><strong>Section</strong></th><input type="hidden" name="id_field" id="id_field" />
            </thead>
		</table>
	</div>	
	<div style="width:930px; overflow-y:scroll; min-height:50px; max-height:230px;" id="reprocess_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
	
<?php
$i=1;
if ($search_string2==0){
		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE emp_code in($search_string) and is_deleted=0 and status_active=1 order by emp_code");
}
elseif ($search_string2==1){

		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE concat(first_name,' ',middle_name,' ',last_name) like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code");
}
elseif ($search_string2==2){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.custom_designation
		from hrm_employee a, lib_designation d
		where a.designation_id=d.id and d.custom_designation like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");
}
elseif ($search_string2==3){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.division_name
		from hrm_employee a, lib_division d
		where a.division_id=d.id and d.division_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");
}
elseif ($search_string2==4){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.department_name
		from hrm_employee a, lib_department d
		where a.department_id=d.id and d.department_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");
}
elseif ($search_string2==5){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.section_name
		from hrm_employee a, lib_section d
		where a.section_id=d.id and d.section_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");
}
elseif ($search_string2==6){
		$sql=mysql_db_query($DB,"SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE id_card_no in($search_string) and is_deleted=0 and status_active=1 order by emp_code");
}
elseif ($search_string2==7){
		$sql= mysql_db_query($DB,"SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE punch_card_no like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code");
}

	while ($selectResult = mysql_fetch_array($sql))
			{
?>
				 <tr style="text-decoration:none; cursor:pointer" onclick="js_set_value(<?php echo $i; ?>); parent.emailwindow.hide();"> 
					<td width="30" align="center"><?php echo "$i"; ?></td>	
					<td width="80" align="center"><?php echo "$selectResult[emp_code]"; ?> <input type="hidden" name="txt_emp_code<? echo $i; ?>" id="txt_emp_code<? echo $i; ?>" value="<?php echo "$selectResult[emp_code]"; ?>" /></td><input type="hidden" name="txt_emp_name<? echo $i; ?>" id="txt_emp_name<? echo $i; ?>" value="<?php echo "$selectResult[name]"; ?>" />	
                    <td width="70"><?php echo $selectResult[id_card_no]; ?>
                    <td width="90"><? echo $selectResult[punch_card_no]; ?></td>
                    <td width="130"><?php echo $selectResult[name];//"<a href='##' style='text-decoration:none' onclick='js_set_value($i); parent.emailwindow.hide();'>$selectResult[name]</a>"; ?> </td>	
					<td width="130"><? echo $designation_chart[$selectResult[designation_id]]; ?></td>
					<td width="120"><?php echo $company_details[$selectResult[company_id]];?></td>	
					<td width="90"><?php echo $division_details[$selectResult[division_id]];  ?></td>	
					<td width="90"><?php echo $department_details[$selectResult[department_id]]; ?></td>	
					<td><?php echo $section_details[$selectResult[section_id]]; ?></td>	
				</tr>
				<?php
				$i++;
				}
				?>
		</table>
    </div>
</div>
<?php 	
}
// Employee Information for Payback Sheduling
if($type=="emp_payback_sheduling")
{
 $search_string=trim($search_string);
?>
<div>
    <div style="width:930px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="35" align="left"><strong>SL</strong></th>
                <th width="80" align="left"><strong>Emp. Code</strong></th>
                <th width="70" align="left"><strong>Emp. ID</strong></th>
                <th width="85" align="left"><strong>Punch Card</strong></th>
                <th width="130" align="left"><strong>Emp. Name</strong></th>
                <th width="130" align="left"><strong>Designation</strong></th>
                <th width="120" align="left"><strong>Company Name</strong></th>
                <th width="90" align="left"><strong>Division</strong></th>
                <th width="90" align="left"><strong>Department</strong></th>
                <th align="left"><strong>Section</strong></th><input type="hidden" name="id_field" id="id_field" />
            </thead>
		</table>
	</div>	
	<div style="width:930px; overflow-y:scroll; min-height:50px; max-height:230px;" id="reprocess_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >

<?php
$i=1;
if ($search_string2==0){
		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE emp_code like '%$search_string%' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
}
elseif ($search_string2==1){

		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE concat(first_name,' ',middle_name,' ',last_name) like '%$search_string%' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
}
elseif ($search_string2==2){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.custom_designation
		from hrm_employee a, lib_designation d
		where a.designation_id=d.id and d.custom_designation like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==3){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.division_name
		from hrm_employee a, lib_division d
		where a.division_id=d.id and d.division_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==4){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.department_name
		from hrm_employee a, lib_department d
		where a.department_id=d.id and d.department_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==5){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.section_name
		from hrm_employee a, lib_section d
		where a.section_id=d.id and d.section_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==6){
		$sql=mysql_db_query($DB,"SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE id_card_no like '%$search_string%' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
}
elseif ($search_string2==7){
		$sql= mysql_db_query($DB,"SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE punch_card_no like '%$search_string%' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
}

		while ($selectResult = mysql_fetch_array($sql))
			{
?>
				 <tr style="text-decoration:none; cursor:pointer" onclick="js_set_value(<? echo $i; ?>); parent.emailwindow.hide();"> 
					<td width="35" align="center"><?php echo "$i"; ?></td>	
					<td width="80" align="center"><?php echo "$selectResult[emp_code]"; ?> <input type="hidden" name="txt_emp_code<? echo $i; ?>" id="txt_emp_code<? echo $i; ?>" value="<?php echo "$selectResult[emp_code]"; ?>" /></td><input type="hidden" name="txt_emp_name<? echo $i; ?>" id="txt_emp_name<? echo $i; ?>" value="<?php echo "$selectResult[name]"; ?>" />	
					<td width="70"><?php echo $selectResult[id_card_no]; ?>
                    <td width="85"><? echo $selectResult[punch_card_no]; ?></td>
                    <td width="130"><?php echo $selectResult[name];//"<a href='##' style='text-decoration:none' onclick='js_set_value($i); parent.emailwindow.hide();'>$selectResult[name]</a>"; ?> </td>	
					<td width="130"><? echo $designation_chart[$selectResult[designation_id]]; ?></td>
					<td width="120"><?php echo $company_details[$selectResult[company_id]];  ?></td>	
					<td width="90"><?php echo $division_details[$selectResult[division_id]];  ?></td>	
					<td width="90"><?php echo $department_details[$selectResult[department_id]]; ?></td>	
					<td><?php echo $section_details[$selectResult[section_id]]; ?></td>	
				</tr>
				<?php
				$i++;
				}
				?>
		</table>
<?php 
}
// Employee Information for Early Adjustment
if($type=="emp_early_adjustment")
{
	
?>
<div>
    <div style="width:930px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="30" align="left"><strong>SL</strong></th>
                <th width="80" align="left"><strong>Emp. Code</strong></th>
                <th width="70" align="left"><strong>Emp. ID</strong></th>
                <th width="90" align="left"><strong>Punch Card</strong></th>
                <th width="130" align="left"><strong>Emp. Name</strong></th>
                <th width="130" align="left"><strong>Designation</strong></th>
                <th width="120" align="left"><strong>Company Name</strong></th>
                <th width="90" align="left"><strong>Division</strong></th>
                <th width="90" align="left"><strong>Department</strong></th>
                <th align="left"><strong>Section</strong></th><input type="hidden" name="id_field" id="id_field" />
            </thead>
		</table>
	</div>	
	<div style="width:930px; overflow-y:scroll; min-height:50px; max-height:260px;" id="reprocess_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
	
<?php
$i=1;
if ($search_string2==0){
		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE emp_code like '%$search_string' and company_id='$company_name' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
}
elseif ($search_string2==1){

		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE concat(first_name,' ',middle_name,' ',last_name) like '%$search_string%' and company_id='$company_name' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
}
elseif ($search_string2==2){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.custom_designation
		from hrm_employee a, lib_designation d
		where a.designation_id=d.id and d.custom_designation like '%$search_string%' and company_id='$company_name' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==3){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.division_name
		from hrm_employee a, lib_division d
		where a.division_id=d.id and d.division_name like '%$search_string%' and company_id='$company_name' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==4){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.department_name
		from hrm_employee a, lib_department d
		where a.department_id=d.id and d.department_name like '%$search_string%' and company_id='$company_name' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==5){
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.section_name
		from hrm_employee a, lib_section d
		where a.section_id=d.id and d.section_name like '%$search_string%' and company_id='$company_name' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
}
elseif ($search_string2==6){
		$sql=mysql_db_query($DB,"SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE id_card_no like '%$search_string%' and company_id='$company_name' and is_deleted=0 and status_active=1 order by emp_code");
}
elseif ($search_string2==7){
		$sql= mysql_db_query($DB,"SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE punch_card_no like '%$search_string%' and company_id='$company_name' and is_deleted=0 and status_active=1 order by emp_code");
}

	while ($selectResult = mysql_fetch_array($sql))
			{
?>
				 <tr style="text-decoration:none; cursor:pointer"  onclick="js_set_value(<?php echo $i; ?>); parent.emailwindow.hide();"> 
					<td width="30" align="center"><?php echo "$i"; ?></td>	
					<td width="80" align="center"><?php echo "$selectResult[emp_code]"; ?> <input type="hidden" name="txt_emp_code<? echo $i; ?>" id="txt_emp_code<? echo $i; ?>" value="<?php echo "$selectResult[emp_code]"; ?>" /></td><input type="hidden" name="txt_emp_name<? echo $i; ?>" id="txt_emp_name<? echo $i; ?>" value="<?php echo "$selectResult[name]"; ?>" />	
					<td width="70"><?php echo $selectResult[id_card_no]; ?>
                    <td width="90"><? echo $selectResult[punch_card_no]; ?></td>
                    <td width="130"><?php echo $selectResult[name]; ?> </td>	
					<td width="130"><? echo $designation_chart[$selectResult[designation_id]]; ?></td>
					<td width="120"><?php echo $company_details[$selectResult[company_id]]; ?></td>	
					<td width="90"><?php echo $division_details[$selectResult[division_id]];  ?></td>	
					<td width="90"><?php echo $department_details[$selectResult[department_id]]; ?></td>	
					<td><?php echo $section_details[$selectResult[section_id]]; ?></td>	
				 </tr>
				<?php
				$i++;
				}
				?>
		</table>
     </div>
</div>
<?php 
}
function return_field_value($fdata,$tdata,$cdata)
{
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}
?>

