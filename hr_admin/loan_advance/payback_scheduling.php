<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 17-04-12
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
    <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
    <script src="../includes/functions.js" type="text/javascript"></script>
   
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
    <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
    <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>

    
 	<script language="javascript">
	function openmypage(page_link,title)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=985px,height=380px,center=1,resize=0,scrolling=0','../')
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
			  	//document.getElementById('txt_emp_code').value=theemail.value;
				//alert(theemail.value);
				emp_search_payback_scheduling(theemail.value,"payback_scheduling");
				setTimeout(showResult_search_loan(theemail.value,"payback_scheduling","payback_scheduling_div"),150);
				
				//showResult_search_loan(theemail.value,"payback_scheduling","payback_scheduling_div");
			}
		}
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}		

function showResult_search_loan(str,type,div)
{
	//alert(str+type+div);
if (str.length==0)
  {
  document.getElementById(div).innerHTML="";
  document.getElementById(div).style.border="0px";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById(div).innerHTML=xmlhttp.responseText;
    }
  }
//alert(str);
xmlhttp.open("GET","includes/list_view_loan.php?search_string="+str+"&type="+type,true);
xmlhttp.send();

}

function hidden_field_reset()
{
	document.getElementById('save_up').value="";
	document.getElementById('payback_scheduling_div').innerHTML="";
	document.getElementById('txt_emp_code').disabled=false;
	show_hide_interest(0);
}

function show_hide_interest(str)
{
	
	if (str==2 || str==3)
	{
		document.getElementById('appr_tr').style.visibility="visible";
	}
	else
	{
		document.getElementById('appr_tr').style.visibility="collapse";
		document.getElementById('txt_install_no_for_inst').value="";
	}
}

function disabled_interest(str)
{
	if (str==1)
	{
		document.getElementById('txt_total_installment').value="";
		document.getElementById('txt_interest_rate').value="";
		document.getElementById('txt_interest_rate').disabled=true;
		document.getElementById('text_total_interest').value="";
		document.getElementById('text_total_interest').disabled=true;
		document.getElementById('txt_total_amount').value="";
		document.getElementById('txt_total_amount').disabled=true;
		document.getElementById('combo_adjustment_advice').value=0;
		document.getElementById('combo_adjustment_advice').disabled=true;
		
	}
	else
	{
		document.getElementById('txt_interest_rate').disabled=false;
		document.getElementById('text_total_interest').disabled=false;
		document.getElementById('txt_total_amount').disabled=false;
		document.getElementById('combo_adjustment_advice').disabled=false;
	}
}
function cal_amnt(instm)
{
	//alert(inst);	
	var emp_code=document.getElementById('txt_emp_code').value;
	var app_amnt=document.getElementById('txt_approved_amount').value;
	var app_amnt_ttl=document.getElementById('txt_approved_amount').value;
	var ttl_instm=document.getElementById('txt_total_installment').value;
	var inst_rate=document.getElementById('txt_interest_rate').value;
	var inst_type=document.getElementById('combo_interest_type').value;
	if(emp_code=="")
	{
		alert("Please Select Employee Code");
		document.getElementById('txt_total_installment').value="";
	}
	else
	{
		if(inst_type==1)
		{
			document.getElementById('txt_amount_per_inst').value=((app_amnt*1)/(ttl_instm*1)).toFixed(2);	
		}
		else
		{
			document.getElementById('txt_amount_per_inst').value=((app_amnt*1)/(ttl_instm*1)).toFixed(2);
			if(inst_rate=="")
			{
					alert("Please Insert Interest Rate");
					document.getElementById('txt_total_installment').value="";
			}
			else
			{
				var interest=0;
				var ttl_interest=0;
				var per_prin_inst_amnt=app_amnt*1/ttl_instm*1;
				for(var i=0;i<ttl_instm*1;i++)
				{
					interest=(app_amnt*inst_rate)/(100*12);
					app_amnt=app_amnt-per_prin_inst_amnt;
					ttl_interest=ttl_interest+interest;
					//alert(interest);
				}
				//alert("t"+ttl_interest);
				document.getElementById('text_total_interest').value=ttl_interest.toFixed(2);
				var total_amount=ttl_interest+app_amnt_ttl*1;
				document.getElementById('txt_total_amount').value=total_amount.toFixed(2);
			}

		}
	}
}
function see_schedule(str)
{
	window.open("includes/payback_schedule_details.php?str="+str);
	//emailwindow=dhtmlmodal.open('EmailBox', 'iframe', "includes/payback_schedule_details.php?str="+str, title, 'width=690px,height=380px,center=1,resize=0,scrolling=0','../')
}
</script>
<head>
<body style="font-family:verdana; font-size:11px;" onload="show_hide_interest(0)">
<div align="center">
	 <div>
        <div class="form_caption">
            Payback Scheduling
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;
            ?>
       </span>
		<div id="messagebox" style="background-color:#FF9999; width:900px; color:#000000"></div>
	</div> 
	<!-- Start Field Set -->
<fieldset style="width:990px ">
<legend>Payback Scheduling</legend>
 <form name="payback_scheduling" id="payback_scheduling" action="javascript:fnc_payback_scheduling(save_perm,edit_perm,delete_perm,approve_perm)" method="post"> 
     <!-- Start Field Set -->
	<fieldset style="width:800px ">
	<legend>Emplyoee Loan Info</legend>
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="130" >
						Employee Code
					</td>
					<td width="130"> <!-- Select Calander -->
                     <input type="text" name="txt_emp_code" id="txt_emp_code" placeholder="Double Click to Search" ondblclick="openmypage('search_employee_payback.php','Employee Info'); return false" readonly="true" class="text_boxes" style="width:155px" autocomplete="off">
					</td>
					<td width="100">
						Name
					</td>
					<td width="130"> <!--  System Generated-->
					<input type="text" name="txt_emp_name" id="txt_emp_name" class="text_boxes" style="width:155px" readonly>
					</td>
				</tr>
				<tr>
        	 		<td width="100">
						Loan Date
					</td>
					<td width="100">
                     <input type="text" name="loan_date" id="loan_date" class="text_boxes" style="width:155px" readonly="readonly">
                     </td>
                     <td width="100">
						Applied Amount
					</td>
					<td width="130"> 
					  <input type="text" name="txt_loan_amount" id="txt_loan_amount" class="text_boxes" style="width:155px" readonly="readonly">					
					  </td>
             	 </tr>
                 <tr>
                 		<td width="100">
						Loan Purpose
					</td>
					<td width="130">
                    <input type="text" name="txt_loan_purpose" id="txt_loan_purpose" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
                    <td width="100">
						Loan Type
					</td>
					<td width="130"> 
                    <input type="text" name="txt_loan_type" id="txt_loan_type" class="text_boxes" style="width:155px" readonly="readonly">
					</td>
              	</tr>
              	<tr>
                    <td width="100">
						Payment Date
					</td>
					<td width="100">
                    <input type="text" name="payment_date" id="payment_date" class="text_boxes" style="width:155px" readonly="readonly"/>
          			</td>
                     <td width="100">
						Approved by
					</td>
					<td width="130"> <!--  Select-->
					  <input type="text" name="txt_approved_by" id="txt_approved_by" class="text_boxes" style="width:155px" readonly="readonly">					
					  </td>
             	 </tr>
              	 <tr>
                     <td width="100">
						Recommended by
					</td>
					<td width="130"> <!--  Select-->
					  <input type="text" name="txt_recommended_by" id="txt_recommended_by" class="text_boxes" style="width:155px" readonly="readonly">					
					  </td>
             	 </tr>
			</table>
			<!-- End Main Table -->
	</fieldset>
    <fieldset style="width:800px ">
    <legend>Payback Info</legend>
        <table cellpadding="0" cellspacing="0" width="100%">
        	<tr>
            	<td width="130">
					Approved Amount
				</td>
				<td width="130"> 
					<input type="text" name="txt_approved_amount" id="txt_approved_amount" class="text_boxes" style="width:155px" readonly="readonly">					
				</td>
                <td width="100">
					Interest Type
				</td>
				<td width="130"> 
                    <select name="combo_interest_type" id="combo_interest_type" class="combo_boxes" style="width:165px" onchange="disabled_interest(this.value)">
                    	 <?php
							foreach($interest_type as $key => $value)
							{
							?>
							<option value=<?php echo $key;?> <?php if( $key ==3) { ?> selected <?php } ?>><?php echo $value; ?></option>
							<?php }?>
                    </select>					
				</td>
            </tr>
            <tr>
            	<td width="100">
					Interest Rate
				</td>
				<td width="130"> 
					<input type="text" name="txt_interest_rate" id="txt_interest_rate" class="text_boxes" style="width:155px" onKeyPress=" return numbersonly(this,event)" >					
				</td>
            	<td width="100">
					Total Installment
				</td>
				<td width="130"> 
					<input type="text" name="txt_total_installment" id="txt_total_installment" class="text_boxes" style="width:155px" onKeyPress=" return numbersonly(this,event)" onkeyup="cal_amnt(this.value)" autocomplete="off">					
				</td>
            	
            </tr>
            <tr>
            	 <td width="100">
					Total Interest 
				</td>
				<td width="130"> 
					<input type="text" name="text_total_interest" id="text_total_interest" class="text_boxes" style="width:155px" readonly="readonly">					
				</td>
            	<td width="130">
					Total Amount (With Interest)
				</td>
				<td width="130"> 
					<input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes" style="width:155px" readonly="readonly">					
				</td>
                
            </tr>
            <tr>
            	<td width="100">
					Adjustment Source 
				</td>
				<td width="130"> 
                    <select name="combo_adjustment_source" id="combo_adjustment_source" class="combo_boxes" style="width:165px">
                    	 <?php
							foreach($adjustment_source as $key => $value)
							{
							?>
							<option value="<?php echo $key;?>"><?php echo $value; ?></option>
							<?php }?>
                    </select>					
				</td>
            	<td width="100">
					Starting Month
				</td>
				<td width="130"> 
					<!--<input type="text" name="txt_starting_month" id="txt_starting_month" class="text_boxes" style="width:155px" readonly="readonly">
                    		<script type="text/javascript">
                                $( "#txt_starting_month" ).datepicker({
                                dateFormat: 'dd-mm-yy',
                                changeMonth: true,
                                changeYear: true
                                });
                            </script>		-->			
                            
                    <select name="txt_starting_month" id="txt_starting_month"  class="combo_boxes" style="width:165px">							
                    <option value="0">---- Select Month ----</option>
                        <?
                        $sql_month= mysql_db_query($DB, "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id  and b.type=1 and b.is_locked=0 ");
                        while ($r_month=mysql_fetch_array($sql_month))
                        {
                        ?>
                        <option value=<? echo "$r_month[actual_starting_date]"; ?>><? echo $r_month[starting_date]; ?> </option>
                        <?
                        }
                        ?>
                    </select>
				</td>
            </tr>
             <tr>
             	<td width="100">
					Adjustment Advice 
				</td>
				<td width="130"> 
                    <select name="combo_adjustment_advice" id="combo_adjustment_advice" class="combo_boxes" style="width:165px" onchange="show_hide_interest(this.value)">
                    	 <?php
							foreach($adjustment_advice as $key => $value)
							{
							?>
							<option value="<?php echo $key;?>"><?php echo $value; ?></option>
							<?php }?>
                    </select>					
				</td>
            	<td width="130">
					Installment Amount (Principal) 
				</td>
				<td width="130"> 
					<input type="text" name="txt_amount_per_inst" id="txt_amount_per_inst" class="text_boxes" style="width:155px" readonly="readonly">					
				</td>
            </tr>
             <tr id="appr_tr">
             	<td width="100">
					Install. No. for Int. 
				</td>
				<td width="130"> 
                   <input type="text" name="txt_install_no_for_inst" id="txt_install_no_for_inst" class="text_boxes" style="width:155px" onKeyPress=" return numbersonly(this,event)">				
				</td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td colspan="4" align="center" class="button_container" width="800"><input type="hidden" name="save_up" id="save_up" />
                    <input type="submit" value="Save" name="save" id="save" style="width:80px" class="formbutton"/>&nbsp;
                     <input type="reset" value="  Refresh  " style="width:80px"  name="add" id="close" class="formbutton" onclick="hidden_field_reset()"/>
                </td>
            </tr>
         </table>
    </fieldset>
    </form>
    <div id="payback_scheduling_div" style="width:990px">
    </div>
</fieldset>
</div>
</body>
<html>