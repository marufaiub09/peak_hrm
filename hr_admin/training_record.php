<?
/***************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 08.12.2013
*****************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../includes/common.php');
include("../includes/array_function.php");

// company
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}
// location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}
// division
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}
// department
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}
// section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}
// subsection
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}
// designation
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}
// training name
$sql = "SELECT * FROM lib_training WHERE status=1 and is_deleted = 0 and status_active=1 ORDER BY training_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$training_name = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$training_name[$row['id']] = mysql_real_escape_string($row['training_name']);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="includes/functions.js" type="text/javascript" ></script>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../includes/tablefilter.js"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        
        
         <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
  <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
				
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
			//header: false, 
			selectedText: "# of # selected",
			});
			
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});			
				
				
				
            });
            function populate_data() 
            {
                $('#data_panel').html('<img src="../resources/images/loading.gif" />');
                var category 			= $('#cbo_emp_category').val();
                var company_id 			= $('#cbo_company_id').val();
                var location_id 		= $('#location_id').val();
				var division_id 		= $('#division_id').val();
                var department_id 		= $('#department_id').val();
                var section_id 			= $('#section_id').val();
                var subsection_id 		= $('#subsection_id').val();			
                var designation_id 		= $('#designation_id').val();
				var empcode			= $('#emp_code').val();
               // var id_card 			= $('#txt_id_card').val();
				
				//alert(division_id);
                var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&division_id='+division_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&empcode='+empcode;
                $.ajax({
                    type: "POST",
                    url: "employee_info_data.php?"+passdata,
                    data: 'form=training_record',
                    success: function(data) {
                        $('#data_panel').html( data );	
                        var tableFilters = {col_0: "none"}
                        //setFilterGrid("tbl_training_list_view",-1,tableFilters);				
                    }
                });
            }		
			//new add ekram	
			function Check(chk)
			{
				var cnt=0;
				if(document.frm_training_record.Check_ctr.checked==true){
				for (i = 0; i < chk.length; i++){
				chk[i].checked = true ;
				cnt++;
				}
				}else{			
					for (i = 0; i < chk.length; i++)
					chk[i].checked = false ;			
				}
				$('#count_id').html('You Are Selected '+cnt+' Employees');
			}
			function count_emp(chk){
				var cnt=0;
				$('#tbl_training_list_view tbody input[type="checkbox"]:checked').each(function() {
						cnt++;
					});
				$('#count_id').html('You Are Selected '+cnt+' Employees');
			}
			function openmypage_employee_info()
					{			
						// 'search_employee_multiple.php','Employee Information'
						emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
						emailwindow.onclose=function()
						{
							var thee_loc = this.contentDoc.getElementById("txt_selected");
							var thee_id = this.contentDoc.getElementById("txt_selected_id");				
							$('#emp_code').val(thee_id.value);
						}
					}
			function openmypage_training_record(page_link,title)
			{
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("data").value;
					var datas=thee_loc.split(",");
					//alert(thee_loc);
					//$('#emp_code').val(thee_id.value);
					$('#training_id').val(datas[0]);
					$('#training_date').val(datas[1]);
					$('#training_place').val(datas[2]);
					$('#provided_by').val(datas[3]);
					$('#trainer_name').val(datas[4]);
					$('#duration').val(datas[5]);
					$('#cost').val(datas[6]);
					$('#system_id').val(datas[7]);
					$('#is_update').val(datas[7]);
					showResult_multi(datas[7],'','training_list_view','tbl_list_view');
				}
			}
			function reset_frm()
			{
				document.getElementById('frm_training_record').reset();
				$("#count_id").html('');
				$("#tbl_list_view").html('');
				$("#data_panel").html('');
				//alert("ok");
				/*
				$('#training_id').val(0);
				$('#training_date').val('');
				$('#training_place').val(0);
				$('#provided_by').val('');
				$('#trainer_name').val('');
				$('#duration').val('');
				$('#cost').val('');
				*/
			}
        </script>
        <style>
            .formbutton { width:100px; }
            #reactivation_form * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px;">		
        <div style="height:48px;">
            <div class="form_caption">Compliance Training Record</div>
            <span id="permission_caption"><? echo "Your Permissions--> \n".$insert;?></span>
            <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
        </div>
        <form name="frm_training_record" id="frm_training_record" action="javascript:func_training_record(save_perm,edit_perm,delete_perm,approve_perm);" >
            <fieldset style="padding:10px; margin-top:150;">
                <table width="60%" cellpadding="0" cellspacing="2" class="rpt_table" id="tbl_training">
                	<thead>
                        <th>Training Name</th>
                        <th>Training Date</th>
                        <th>Training Place</th>
                        <th>Provided By</th>
                        <th>Trainer Name</th>
                        <th>Duration Days</th>
                        <th>Cost</th>
                        <th>Remarks</th>
                        <th>System ID</th>
                    </thead>
                    <tbody>
                    	<tr>
                            <td>
                            <select name="training_id" id="training_id" class="combo_boxes" style="width:130px">
                                	<option value="0">-- select --</option>
                                    <?php foreach( $training_name AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                </select>
                            </td>
                            <td><input type="text" name="training_date" id="training_date" class="datepicker" style="width:120px" /></td>
                            <td><select name="training_place" id="training_place" class="combo_boxes" style="width:120px">
                                	<option value="0">-- select --</option>
                                    <option value="1">In-house</option>
                                    <option value="2">Outside</option>
                                </select>
                            </td>
                            <td><input type="text" name="provided_by" id="provided_by" class="text_boxes" style="width:120px" /></td>
                            <td><input type="text" name="trainer_name" id="trainer_name" class="text_boxes" style="width:120px" /></td>
                            <td><input type="text" name="duration" id="duration" class="text_boxes" onKeyPress="return numbersonly(this, event);" style="width:120px" /></td>
                            <td><input type="text" name="cost" id="cost" class="text_boxes" onKeyPress="return numbersonly(this, event);"  value="0" style="width:120px" /></td>
                            <td><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:120px" /></td>
                        	<td>
                            	<input type="text" name="system_id" id="system_id" class="text_boxes" placeholder="browse" ondblclick="openmypage_training_record('search_training_record.php','training_record'); return false"style="width:80px;" />
                                <input type="hidden" name="is_update" id="is_update" value="" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset style="padding:10px;">
            <table class="rpt_table" style="width:60%;" border="0" cellpadding="0" cellspacing="2">
                <thead>
                	<th> Category</th>
                    <th>Company</th>
                    <th>Location</th>
                    <th>Division</th>
                    <th>Department</th>
                    <th>Section</th>
                    <th>SubSection</th>
                    <th>Designation</th> 
                    <th>Employee Code</th>                   			
                    <td><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" /></td>                  
                </thead>
                <tbody>
                    <tr class="general">
                        <td><select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                                <option value="">All Category</option>
                                <? foreach($employee_category as $key=>$val){?>
                                <option value="<? echo $key; ?>"><? echo $val; ?></option><? } ?>
                            </select>
                        </td>
                        <td><select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:120px">
                                <? if($company_cond==""){ ?>
                                <option value="0">-- Select --</option>
                                <?php } foreach( $company_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?>><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td><select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                              <option value="0">-- Select --</option>
                                <?php foreach( $location_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                          </select>
                        </td>
                        <td><select name="division_id" id="division_id" class="combo_boxes" style="width:120px" multiple="multiple">
                            <!--  <option value="0">-- Select --</option>-->
                                <?php foreach( $division_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                          </select>
                        </td>
                        <td><select name="department_id" id="department_id" class="combo_boxes" style="width:120px" multiple="multiple">
                               <!--  <option value="0">-- Select --</option>-->
                                <?php foreach( $department_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td><select name="section_id" id="section_id" class="combo_boxes" style="width:120px" multiple="multiple">
                               <!--  <option value="0">-- Select --</option>-->
                                <?php foreach( $section_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td id="subsection"><select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px" multiple="multiple">
                               <!--  <option value="0">-- Select --</option>-->
                                <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td> 
                        <td id="designation"><select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" multiple="multiple">
                              <!--  <option value="0">-- Select --</option>-->
                                <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>   
                        <td><input type="text" name="emp_code" id="emp_code" class="text_boxes" style="width:100px" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" /></td>                               
                        <td><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()" /></td>
                    </tr>
              </tbody>
            </table> 
            <div style="padding-top:10px;">
                <input type="submit" name="save" id="save" value="Save"  class="formbutton"/>
                <input type="button" name="cancel" id="cancel" value="Refresh"  class="formbutton" onclick="reset_frm()" />
            </div>
            </fieldset>
            <fieldset>
                <span id="count_id" style="font-weight:bold"></span>
                <div id="tbl_list_view"></div>
            	<div id="data_panel" style="margin-top:14px;"></div>
            </fieldset>
        </form>
        </div>
    </body>
</html>