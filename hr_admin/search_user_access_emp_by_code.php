<?php
	/***************************************
	|	Developed By	: Md. Nuruzzaman
	|	Date			: 19.11.2014
	*****************************************/

	session_start();
	include('../includes/common.php');
	include('../includes/array_function.php');
	extract($_REQUEST);
	
	// 1=limited access and 2=self user
	if ($_SESSION['logic_erp']["data_level_secured"]==1 || $_SESSION['logic_erp']["data_level_secured"]==2) 
	{
		// company_id
		if( $_SESSION['logic_erp']['company_id'] == 0 || $_SESSION['logic_erp']['company_id']==""){ $user_company_id=""; }
		else { $user_company_id="id in(".$_SESSION['logic_erp']['company_id'].") and"; }
		
		// location_id
		if( $_SESSION['logic_erp']['location_id'] == 0 || $_SESSION['logic_erp']['location_id']=="") { $user_location_id=""; }
		else { $user_location_id="id in(".$_SESSION['logic_erp']['location_id'].") and"; }
		
		// division_id
		if( $_SESSION['logic_erp']['division_id'] == 0 || $_SESSION['logic_erp']['division_id']=="") { $user_division_id=""; }
		else { $user_division_id="id in(".$_SESSION['logic_erp']['division_id'].") and"; }
		
		// department_id
		if( $_SESSION['logic_erp']['department_id'] == 0 || $_SESSION['logic_erp']['department_id']=="") { $user_department_id=""; }
		else { $user_department_id="id in(".$_SESSION['logic_erp']['department_id'].") and"; }
		
		// section_id
		if( $_SESSION['logic_erp']['section_id'] == 0 || $_SESSION['logic_erp']['section_id']=="") { $user_section_id=""; }
		else { $user_section_id="id in(".$_SESSION['logic_erp']['section_id'].") and"; }
		
		// subsection_id
		if( $_SESSION['logic_erp']['subsection_id'] == 0 || $_SESSION['logic_erp']['subsection_id']=="") { $user_subsection_id=""; }
		else { $user_subsection_id="id in(".$_SESSION['logic_erp']['subsection_id'].") and"; }
	}
	
	// all access
	else if ($_SESSION['logic_erp']["data_level_secured"]==0) 
	{
		$user_company_id="";
		$user_location_id="";
		$user_division_id="";
		$user_department_id="";
		$user_section_id="";
		$user_subsection_id="";
	}

	//company_details
	$sql = "SELECT id,company_name FROM lib_company WHERE $user_company_id status_active=1 and is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$company_details[$row['id']] = mysql_real_escape_string( $row['company_name'] );
	}
	
	//location_details
	$sql = "SELECT id, location_name FROM lib_location WHERE $user_location_id status_active=1 and is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$location_details[$row['id']] = mysql_real_escape_string( $row['location_name'] );
	}
	
	//division_details
	$sql = "SELECT id, division_name FROM lib_division WHERE $user_division_id status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$division_details[$row['id']] = mysql_real_escape_string( $row['division_name'] );
	}
	
	//department_details
	$sql = "SELECT id, department_name FROM lib_department WHERE $user_department_id status_active=1 and is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$department_details[$row['id']] = mysql_real_escape_string( $row['department_name'] );
	}
	
	//section_details
	$sql = "SELECT id, section_name FROM lib_section WHERE $user_section_id status_active=1 and is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$section_details[$row['id']] = mysql_real_escape_string( $row['section_name'] );
	}
	
	//subsection_details
	$sql = "SELECT id, subsection_name FROM lib_subsection WHERE $user_subsection_id status_active=1 and is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );
	}
	
//-------------------------------------------------------------------------------------------------------------------// 	
 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="includes/functions.js"></script>
        
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../includes/tablefilter.js"></script>
        
        <script>
        
        //Numeric Value allow field script
        function numbersonly(myfield, e, dec)
        {
            var key;
            var keychar;
        
            if (window.event) key = window.event.keyCode;
            else if (e) key = e.which;
            else return true;
            keychar = String.fromCharCode(key);
        
            // control keys
            if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
            return true;
            
            // numbers
            else if ((("0123456789.,-").indexOf(keychar) > -1)) return true;
            else return false;
        }
        
        function search_populate(str)
        {
             //alert(str);
            if(str==0)
            {
                document.getElementById('search_by_td_up').innerHTML="System Code";	
                document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" />';
            }
            else if(str==1)
            {
                document.getElementById('search_by_td_up').innerHTML="Employee Name";	
                document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
            }
            else if(str==2)
            {
                var designation = '<option value="0">--- Select ---</option>';
                <?php
                $designation_sql= mysql_db_query($DB, "select * from lib_designation where is_deleted=0  and status_active=1 order by level ");
                while ($row=mysql_fetch_array($designation_sql))
                {
                    echo "designation += '<option value=\"$row[id]\">".mysql_real_escape_string($row[custom_designation])."</option>';";
                }
                ?>
                document.getElementById('search_by_td_up').innerHTML="Select Designation";
                document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ designation +'</select>'; 
            }	
            else if(str==3)
            {
                var division = '<option value="0">--- Select ---</option>';
                <?php
				foreach( $division_details AS $key=>$value )
				{ 
				echo "division += '<option value=\"$key\">".mysql_real_escape_string($value)."</option>';";
				}
				?>
                document.getElementById('search_by_td_up').innerHTML="Select Division";
                document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ division +'</select>'; 
            }	
            else if(str==4)
            {
                var department = '<option value="0">--- Select ---</option>';
                <?php
				foreach( $department_details AS $key=>$value )
				{ 
				echo "department += '<option value=\"$key\">".mysql_real_escape_string($value)."</option>';";
				}
				?>
                document.getElementById('search_by_td_up').innerHTML="Select Department";
                document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ department +'</select>'; 
            }	
           else if(str==5)
            {
                var section = '<option value="0">--- Select ---</option>';
                <?php
				foreach( $section_details AS $key=>$value )
				{ 
				echo "section += '<option value=\"$key\">".mysql_real_escape_string($value)."</option>';";
				}
				?>
                document.getElementById('search_by_td_up').innerHTML="Select Section";
                document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ section +'</select>'; 
            }
            else if(str==6)
            {
                document.getElementById('search_by_td_up').innerHTML="ID Card No";	
                document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
            }
            else if(str==7)
            {
                document.getElementById('search_by_td_up').innerHTML="Punch Card No";	
                document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" />';
            }
            else if(str==8)
            {
                document.getElementById('search_by_td_up').innerHTML="System Code";	
                document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" />';
            }
            
            //new add
        else if(str==9)
            {
                var location = '<option value="0">--- Select ---</option>';
                <?php
				foreach( $location_details AS $key=>$value )
				{ 
				echo "location += '<option value=\"$key\">".mysql_real_escape_string($value)."</option>';";
				}
				?>
                document.getElementById('search_by_td_up').innerHTML="Select Location";
                document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ location +'</select>'; 
            }	
            
        }
        
		var salaryreproces='<? echo $salaryreproces; ?>';
		var gen='<? echo $gen; ?>';
        
        function fn_check()
        {
            
			var company=document.getElementById('cbo_company_name').value;
            var category=document.getElementById('cbo_category_name').value;
            var search_by=document.getElementById('cbo_search_by').value;
            var search_field=document.getElementById('txt_search_common').value;
                
            if(search_field=="")
            {
                //alert("su..re");
                if(company==0)
                { 
                    alert("Please Select Company Name."); 
                    return false;
                }
                else
                {
                   $("#messagebox").removeClass().addClass('messagebox').text('Please wait....').fadeIn(1000);
				   var param=company+"_"+category+"_"+search_by+"_empinfoform_"+salaryreproces+"_"+gen;
                    showResult_multi(search_field,param,'user_access_emp_search','td_show_result');
                }
            }
            else
            {
                $("#messagebox").removeClass().addClass('messagebox').text('Please wait....').fadeIn(1000);
				var param=company+"_"+category+"_"+search_by+"_empinfoform_"+salaryreproces+"_"+gen;
                showResult_multi(search_field,param,'user_access_emp_search','td_show_result');
            }
        }
            
        var selected_id = new Array();
		var selected_name = new Array();
                function check_all_data() {
                    var tbl_row_count = document.getElementById( 'table_body' ).rows.length;
                    //alert(tbl_row_count);
                    tbl_row_count = tbl_row_count - 0;
                    for( var i = 1; i <= tbl_row_count; i++ ) {
                        js_set_value( i );
                    }
                }
                
                function toggle( x, origColor ) {
                    var newColor = 'yellow';
                    if ( x.style ) {
                        x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
                    }
                }
                
                function js_set_value( str ) {
                    //alert(str);
					toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
                    
                    if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
                        selected_id.push( $('#txt_individual_id' + str).val() );
                        selected_name.push( $('#txt_individual' + str).val() );
                    }
                    else {
                        for( var i = 0; i < selected_id.length; i++ ) {
                            if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
                        }
                        selected_id.splice( i, 1 );
                        selected_name.splice( i, 1 );
                    }
                    var id =''; 
					var name = '';
                    for( var i = 0; i < selected_id.length; i++ ) {
                        id += selected_id[i] + ',';
                        name += selected_name[i] + '*';
                    }
                    id = id.substr( 0, id.length - 1 );
                    name = name.substr( 0, name.length - 1 );
                    
                    $('#txt_selected_id').val( id );
                    $('#txt_selected').val( name );
					
					$('#txt_selected_name').val($('#txt_individual' + str).val());
					$('#txt_selected_designation').val($('#txt_hidden_designation' + str).val());
                }
        </script>
    </head>
    <body>
        <div align="center">
        <div style="height:18px; padding-bottom:5px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:950px;" align="center"></div></div>
            <form name="search_order_frm"  id="search_order_frm" autocomplete="off">
                <fieldset style="width:930px">
                    <table class="rpt_table" width="750" cellspacing="2" cellpadding="0" border="0">
                        <thead>
                            <th width="100" align="center">Company&nbsp; </th>
                            <th width="100" align="center">Category&nbsp; </th>
                            <th width="100" align="center">Search By</th>
                            <th width="100" align="center" id="search_by_td_up">System Code</th>
                            <th width="100" align="center"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>	          
                        </thead>
                        <tbody>
                            <tr class="general">
                            <td width="150" align="center">
                                <select name="cbo_company_name" id="cbo_company_name" class="combo_boxes"<?php if($company_id!=0) echo ' disabled="disabled"';?> style="width:170px" >
                                    <option value="0">-- Select --</option>
                                    <?php 
                                    foreach( $company_details AS $key=>$value )
                                    { 
                                    ?>
                                    <option value="<?php echo $key; ?>"<? if($company_id==$key) echo "selected"; ?>><?php echo $value; ?></option>
                                    <?php 
                                    } 
                                    ?>
                                </select>
                            </td>
                            <td width="150" align="center">
                                <select name="cbo_category_name" id="cbo_category_name" class="combo_boxes"<? if($category_id!="") echo ' disabled="disabled"';?> style="width:170px">
									<?php
                                    if($category_id=="")
                                    {
                                    ?>
                                        <option value="" selected="selected">All Category</option>
                                        <?php
                                        foreach($employee_category as $key=>$val)
                                        {
                                        ?>
                                        <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                        <?
                                        }
                                    }
                                    else
                                    {
                                        foreach($employee_category as $key=>$val)
                                        {
                                        ?>
                                        <option value="<? echo $key; ?>"<? if($category_id==$key) echo "selected"; ?>><? echo $val; ?></option>
                                        <?
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td width="150" align="center">
                                <select name="cbo_search_by" id="cbo_search_by" class="combo_boxes" style="width:150px" onchange="search_populate(this.value)">
                                    <option value="0">Employee Code</option>
                                    <option value="6">Employee ID</option>
                                    <option value="1">Employee Name</option>
                                    <option value="2">Designation</option>
                                    <option value="9">Location</option>
                                    <option value="3">Division</option>
                                    <option value="4">Department</option>
                                    <option value="5">Section</option>
                                    <option value="7">Punch Card</option>
                                    <option value="8">Separated</option>
                                </select>
                            </td>
                            <td width="150" align="center" id="search_field_td_up">
                            	<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" placeholder="Enter Here" onkeypress="return numbersonly(this,event)" autocomplete="off" /></td>
                            <td width="150" align="center">
                                <input type="button" name="show_button" id="show_button" class="formbutton" value="Show" onclick="fn_check()" style="width:100px;" /></td>
                                <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" /><!-- Hidden field -->
                            </tr>
                    	<tbody>
                    </table>
                    <div style="border:none" id="td_show_result"></div>
                    <table width="930">
                        <tfoot>
                            <th align="center" height="30" valign="bottom">
                                <div style="width:100%"> 
                                    <div style="width:50%; float:left" align="left">
                                        <input type="checkbox" name="check_all" id="check_all" onclick="check_all_data()" /> Check / Uncheck All
                                        <input type="hidden" readonly="readonly" name="txt_selected_id" id="txt_selected_id" /> 
                                        <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" />
                                        <input type="hidden" readonly="readonly" style="width:250px" class="text_boxes" name="txt_selected" id="txt_selected" />
                                        <input type="hidden" name="txt_selected_name" id="txt_selected_name" />
                                        <input type="hidden" name="txt_selected_designation" id="txt_selected_designation" />
                                    </div>
                                    <div style="width:50%; float:left" align="left">
                                        <input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px"/>
                                    </div>
                                </div>
                            </th>
                        </tfoot>
                    </table>
                </fieldset>
            </form>    
        </div>
    </body>
</html>