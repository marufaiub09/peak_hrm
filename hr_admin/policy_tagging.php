<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
//echo $permission;
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
include('../includes/array_function.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}	
	
//overtime_policy
$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$overtime_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$overtime_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}	
}

//duty roster policy
$sql = "SELECT distinct(roster_id) as id,roster_rule_name FROM lib_duty_roster_policy WHERE is_deleted = 0 and status_active=1 ORDER BY roster_rule_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$roster_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$roster_policy[$row['id']] = mysql_real_escape_string( $row['roster_rule_name'] );
}


//leave_policy
$sql = "SELECT * FROM lib_policy_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$leave_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$leave_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $row[id] AND is_deleted = 0 ORDER BY policy_id, id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$leave_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$leave_policy[$row['id']]['definition'][$row2['leave_type']] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$leave_policy[$row['id']]['definition'][$row2['leave_type']][$key2] = mysql_real_escape_string( $value2 );
		}
	}
}

//maternity_policy
$sql = "SELECT * FROM lib_policy_maternity_leave WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$maternity_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$maternity_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$maternity_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}	
}

//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//Holiday incentive policy
$sql = "SELECT * FROM lib_policy_holiday_incentive WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$holiday_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$holiday_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$holiday_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//Attendance bonus policy 
$sql = "SELECT * FROM lib_policy_attendance_bonus WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$attendance_bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$attendance_bonus_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$attendance_bonus_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//absent deduction policy 
$sql = "SELECT * FROM lib_policy_absent_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$absent_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$absent_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$absent_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//late deduction policy 
$sql = "SELECT * FROM lib_policy_late_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$late_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$late_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$late_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//Bonus policy 
$sql = "SELECT * FROM lib_bonus_policy WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$bonus_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$bonus_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//Tiffin policy 
$sql = "SELECT * FROM lib_policy_tiffin_bill_mst WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$tiffin_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$tiffin_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$tiffin_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

// Allowance policy 
$sql = "SELECT * FROM  lib_policy_allowance WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$allowance_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$allowance_policy[$row['policy_id']] = array();
	foreach( $row AS $key => $value ) {
		$allowance_policy[$row['policy_id']][$key] = mysql_real_escape_string( $value );
	}
}


//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$salary_breakdown_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$i = 0;
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][$i] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$salary_breakdown_policy[$row['id']]['definition'][$i][$key2] = mysql_real_escape_string( $value2 );
		}
		$i++;
	}
}


//early deduction policy 
$sql = "SELECT * FROM lib_policy_early_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$early_out_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$early_out_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$early_out_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
	<script type="text/javascript" src="../js/modal.js"></script>
 	<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
 	
    <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
	<script type="text/javascript" src="../includes/tablefilter.js"></script>

    
	<script type="text/javascript" charset="utf-8">
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
				
		function openmypage(page_link,title)
			{
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=400px,center=1,resize=0,scrolling=0',' ');	
				emailwindow.onclose=function()
				{					
					var policy_id=this.contentDoc.getElementById("policy_id").value;
					var emp_code=this.contentDoc.getElementById("emp_code").value;
					var type=this.contentDoc.getElementById("type").value;
					if(emp_code!=""){
						var passdata ='policy_id='+policy_id+'&emp_code='+emp_code+'&type='+type;
						$.ajax({
							type: "POST",
							url: "includes/save_update_hr_admin.php?"+passdata,
							data: 'action=policy_deattached',
							success: function(data){							
								openmypage('policy_tagging_pop_up.php?emp_code='+emp_code,'Employee Policy Tagging'); 
								return false;
							}
						});
					}
				}
			}
			
function chk_yes_no()
{
	$('#policy_id').removeAttr("disabled","disabled");
	if(document.policy_tagging_form.chk_yes.checked==true && document.policy_tagging_form.chk_no.checked==true )
	{
		alert ("Select Any One Check Box");
		$('#chk_yes').removeAttr("checked");
		$('#chk_no').removeAttr("checked");
		$('#policy_id').attr("disabled","disabled");
	} 
}
				
		function populate_data() {
			//<input type="checkbox" name="chk_yes" id="chk_yes" />Y&nbsp;<input type="checkbox" name="chk_no" id="chk_no" />N</th>
			if(document.policy_tagging_form.chk_yes.checked==true)
			{
				var chk_policy_yes=1;
			}
			else
			{
				var chk_policy_yes=0;
			}
			if(document.policy_tagging_form.chk_no.checked==true)
			{
				var chk_policy_no=1;
			}
			else
			{
				var chk_policy_no=0;
			}
			
			var category 			= $('#cbo_emp_category').val();
			var company_id 			= $('#cbo_company_id').val();
			var location_id 		= $('#location_id').val();
			var division_id 		= $('#division_id').val();
			var department_id 		= $('#department_id').val();
			var section_id 			= $('#section_id').val();
			var subsection_id 		= $('#subsection_id').val();
			var policy_id 			= $('#policy_id').val();
			var cbo_salary_based 	= $('#cbo_salary_based').val();
			var designation_id 		= $('#designation_id').val();
			
			var passdata = '&category='+category+'&company_id='+company_id+'&location_id='+location_id+'&division_id='+division_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&policy_id='+policy_id+'&cbo_salary_based='+cbo_salary_based+'&designation_id='+designation_id+'&chk_policy_yes='+chk_policy_yes+'&chk_policy_no='+chk_policy_no+'&empcode=';
			//alert(passdata);
			if(company_id==0){
				$("#messagebox").fadeTo(200,0.1,function(){			
					$(this).html('Please Select Company Name.').addClass('messageboxerror').fadeTo(900,1);
					$('#cbo_company_id').focus();
					$(this).fadeOut(5000);
				});	
			}
			else{
				$('#policy_tagging').html('<img src="../resources/images/loading.gif" />');
				nocache = Math.random();
				http.open( 'GET', 'employee_info_data.php?form=policy_tagging' + passdata + '&nocache=' + nocache );
				http.onreadystatechange = function(){
					$('#policy_tagging').html( http.responseText );
					var tableFilters = {col_0: "none",col_1: "none"}
					setFilterGrid("tbl_policy_tagging",-1,tableFilters);				
				};
				http.send(null);
			}
		}
		
		
		function reset_form() {
			$('#tbl_policy_selection tbody input[name="policy[]"]:checked').each(function() {
				$(this).attr( 'checked', false );
			});
			$('#policy_tagging input[type="checkbox"]:checked').each(function() {
				$(this).attr( 'checked', false );
				$('select[name="' + $(this).val() + '_policy"]').val( 0 );
			});
		}
		
		function Check(chk)
		{
			var cnt=0;
			
			if(document.policy_tagging_form.Check_ctr.checked==true){
				for (i = 0; i < chk.length; i++){
					chk[i].checked = true ;
					cnt++;
				}
			}else{			
				for (i = 0; i < chk.length; i++)
				chk[i].checked = false ;			
			}
			$('#count_id').html('You Are Selected '+cnt+' Employees');
		}
		
 		
		function count_emp(chk){
			var cnt=0;
			$('#tbl_policy_tagging tbody input[type="checkbox"]:checked').each(function() {
					cnt++;
				});
			$('#count_id').html('You Are Selected '+cnt+' Employees');
		}
		
		
		
	</script>
	<style>
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
		.formbutton { width:100px; }
		#tbl_policy_selection select { width:250px; }
	.formbutton1 {width:100px; }
    </style>
</head>

<body style="font-family:verdana; font-size:11px;" >
	<div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px">
    <div>
    	<div class="form_caption">
		Policy Tagging with Employee
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form name="policy_tagging_form" id="policy_tagging_form" action="javascript:fnc_policy_tagging(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset>
			<legend>Policy Selection</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2" id="tbl_policy_selection">
				<thead>
					<tr>
						<th class="header">&nbsp;</th>
						<th class="header">Policy Name</th>
						<th class="header">Rule Name</th>
						<th class="header">Rule Description</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="checkbox" name="policy[]" value="overtime" /></td>
						<td>1.Overtime Policy</td>
						<td align="center">
							<select name="overtime_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $overtime_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>
							</select>                            
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="holiday_incentive" /></td>
						<td>2.Holiday Incentive</td>
						<td align="center">
							<select name="holiday_incentive_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $holiday_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="duty_roster" /></td>
						<td>3.Duty Roster Policy</td>
						<td align="center">
							<select name="duty_roster_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $roster_policy AS $key=>$value ) {  ?>
								<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
								<?php } ?>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="leave" /></td>
						<td>4.Leave Policy</td>
						<td align="center">
							<select name="leave_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
								<?php foreach( $leave_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="maternity_leave" /></td>
						<td>5.Maternity Leave Policy</td>
						<td align="center">
							<select name="maternity_leave_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $maternity_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>                                
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="attendance_bonus" /></td>
						<td>6.Attendance Bonus Policy</td>
						<td align="center">
							<select name="attendance_bonus_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $attendance_bonus_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>  
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="absent_deduction" /></td>
						<td>7.Absent Deduction Policy</td>
						<td align="center">
							<select name="absent_deduction_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $absent_deduction_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?> 
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
                    <tr>
						<td><input type="checkbox" name="policy[]" value="late_deduction" /></td>
						<td>8.Late Deduction Policy</td>
						<td align="center">
							<select name="late_deduction_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $late_deduction_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?> 
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="bonus" /></td>
						<td>9.Bonus Policy</td>
						<td align="center">
							<select name="bonus_policy" id="bonus_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $bonus_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?> 
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="tax" /></td>
						<td>10.Tax Policy</td>
						<td align="center">
							<select name="tax_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" value="shift" /></td>
						<td>11.Shift Policy</td>
						<td align="center">
							<select name="shift_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
								<?php foreach( $shift_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['shift_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
                    <tr>
                    <td><input type="checkbox" name="policy[]" id="tiffin"  value="tiffin" /></td>
                    <td>12.Tiffin Bill Policy</td>
                    <td align="center">
                        <select name="tiffin_policy" id="tiffin_policy" class="combo_boxes" > 
                            <option value="0">-- Select --</option>
                            <?php foreach( $tiffin_policy AS $policy ){ if( $policy['status_active'] == 1 ) { ?>
                            <option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
                            <?php } }?>
                        </select>
                    </td>
                    <td align="center">&nbsp; </td>
                    </tr>
                    <tr>
                    <td><input type="checkbox" name="policy[]" id="allowance"  value="allowance" /></td>
                    <td>13.Allowance Policy</td>
                    <td align="center">
                        <select name="allowance_policy" id="allowance_policy" class="combo_boxes" > 
                            <option value="0">-- Select --</option>
                            <?php foreach( $allowance_policy AS $policy ){ if( $policy['status_active'] == 1 ) { ?>
                            <option value="<?php echo $policy['policy_id']; ?>"><?php echo $policy['policy_name']; ?></option>
                            <?php } }?>
                        </select>
                    </td>
                    <td align="center">&nbsp; </td>
                    </tr>
                    <tr>
                    <td><input type="checkbox" name="policy[]" id="early_deduction"  value="early_deduction" /></td>
                    <td>14.Early Out Deduction Policy</td>
                    <td align="center">
                        <select name="early_deduction_policy" id="early_deduction_policy" class="combo_boxes" > 
                            <option value="0">-- Select --</option>
                            <?php foreach( $early_out_deduction_policy AS $policy ){ if( $policy['status_active'] == 1 ) { ?>
                            <option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
                            <?php } }?>
                        </select>
                    </td>
                    <td align="center">&nbsp; </td>
                    </tr>
				</tbody>
			</table>
		</fieldset>
        <div id="search_panel">
        <fieldset>
        <table class="rpt_table" style="width:60%;" border="0" cellpadding="0" cellspacing="2">
				<thead>			                
                	<th> Category</th>
					<th> Company</th>
					<th> Location</th>
                    <th> Division</th>
					<th> Department</th>
                    <th> Section</th>
                    <th> SubSection</th>
                    <th> Designation</th>
                    <th>Policy Tagged<br />
                    <input type="checkbox" name="chk_yes" id="chk_yes" onclick="chk_yes_no()" />Yes&nbsp;<input type="checkbox" name="chk_no" id="chk_no" onclick="chk_yes_no()"/>No
                    </th>	
                    <th>Salary Based</th>				
					<td><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" style="width:70px" /></td>                  
				</thead>
                <tr class="general">					
                    <td>
                        <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                            <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?>  
                        </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:130px">
                        	<?
								if($company_cond=="")
								{
							?>
                                <option value="0">-- Select --</option>
                                <?php } foreach( $company_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?> ><?php echo $value; ?></option>
                                <?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
                        <select name="division_id" id="division_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:120px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td> 
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td id="policy">
						<select name="policy_id" id="policy_id" class="combo_boxes" style="width:120px" disabled="disabled">
                       	 	<option value="">--Select--</option> 	 	 	 	 	 	 	 	 	 	
                            <option value="overtime_policy">Overtime Policy</option>
                            <option value="holiday_incentive_policy">Holiday Incentive</option>
                            <option value="duty_roster_policy">Duty Roster Policy</option>
                            <option value="leave_policy">Leave Policy</option>
                            <option value="maternity_leave_policy">Maternity Leave Policy</option>
                            <option value="attendance_bonus_policy">Attendance Bonus Policy</option>
                            <option value="absent_deduction_policy">Absent Deduction Policy</option>
                            <option value="late_deduction_policy">Late Deduction Policy</option>
                            <option value="bonus_policy">Bonus Policy</option>
                            <option value="tax_policy">Tax Policy</option>
                            <option value="shift_policy">Shift Policy</option>
                            <option value="tiffin_policy">Tiffin Policy</option>
                            <option value="allowance_policy">Allowance Policy</option>
                            <option value="early_deduction_policy">Early Out Deduction Policy</option>
                            <option value="0">No Policy</option>
						</select>
					</td> 
                    <td>
                        <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:100px" >							
                            <option value="">-- Select --</option>
                            <?php 
                            foreach( $salary_based_arr AS $key=>$value )
                            { 
                            ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php 
                            } 
                            ?>
                        </select>
                    </td>
                                
                    <!--<td id="policy">
						<select name="policy_tagged_id" id="policy_tagged_id" class="combo_boxes" style="width:120px">
                       	 	<option value="">--Select--</option> 	 	 	 	 	 	 	 	 	 	
                            <option value="overtime_policy">Overtime Policy</option>
                            <option value="holiday_incentive_policy">Holiday Incentive</option>
                            <option value="duty_roster_policy">Duty Roster Policy</option>
                            <option value="leave_policy">Leave Policy</option>
                            <option value="maternity_leave_policy">Maternity Leave Policy</option>
                            <option value="attendance_bonus_policy">Attendance Bonus Policy</option>
                            <option value="absent_deduction_policy">Absent Deduction Policy</option>
                            <option value="late_deduction_policy">Late Deduction Policy</option>
                            <option value="bonus_policy">Bonus Policy</option>
                            <option value="tax_policy">Tax Policy</option>
                            <option value="shift_policy">Shift Policy</option>
                            <option value="tiffin_policy">Tiffin Policy</option>
                            <option value="allowance_policy">Allowance Policy</option>
                             <option value="early_deduction_policy">Early Out Deduction Policy</option>
                            <option value="0">No Policy</option>
						</select>
					</td>    -->      
					<td><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()"  style="width:70px" /></td>
				</tr> 
          </table>  
          </fieldset>      
	  </div>
        
		<fieldset><span id="count_id" style="font-weight:bold">&nbsp;</span><div id="policy_tagging" align="center" class="demo_jui" style="margin:4px 0;"></div></fieldset>
		<div align="center">
			<input type="hidden" name="employees" id="employees" value="" />
			<input type="submit" name="save" id="save" value="Save" class="formbutton" />&nbsp;&nbsp;
			<input type="button" name="reset" id="reset" value="Reset" class="formbutton" onClick="reset_form()" />
		</div>
	</form>
    </div>
</body>
</html>
<?php mysql_close( $host_connect ); ?>