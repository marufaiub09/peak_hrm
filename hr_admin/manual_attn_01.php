<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
 
include('../includes/common.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<script src="includes/functions.js" type="text/javascript"></script>
<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>

	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
 
    <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script src="../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
    
    <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
	<script type="text/javascript" src="../includes/tablefilter.js"></script>
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
	<script type="text/javascript" src="../js/modal.js"></script>
  <!-- <script type="text/javascript" src="../js/light-table-filter.js"></script>-->
	
<style type="text/css">
 
.style1 {font-family: Verdana, Arial, Helvetica, sans-serif}
 
</style>

<script type="text/javascript" charset="utf-8">
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	
		var location_details = division_details = department_details = section_details =emp_code_details=policy_details=id_details= new Array();
		var selected_id = new Array ;
		var selected_id_attnd = new Array ;
	
	<?php
				
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
		
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_details[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}	
	
	$sql = "SELECT * FROM hrm_employee WHERE status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_code_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_code_details[$row['id']] = mysql_real_escape_string($row['emp_code']);
	}
	
	$sql = "SELECT * FROM hrm_employee WHERE status_active=1 and id_card_no!='' ORDER BY id_card_no ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$id_details = array();
	while( $row = mysql_fetch_array( $result ) ) { 
		$id_details[$row['emp_code']] = mysql_real_escape_string($row['id_card_no']);
	}
	//print_r($id_details);
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$policy_details[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}

$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$sub_section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$sub_section_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}	
					
		?>
//All Check field functionality------------------------------------------//	
function fn_checked(chk_id,active_id,type){
	if($('#'+chk_id).attr('checked'))
		{
			document.getElementById(active_id).disabled=false;			
		}
	else
		{
			if(type=='cbo'){
				document.getElementById(active_id).value='0';}
			else{
				document.getElementById(active_id).value='';
			}
			document.getElementById(active_id).disabled=true;
		}	
	
}
//------------------END Check field---------------------------------//	
//ekram
function openmypage_employee_info(page_link,title)
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0','')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#cbo_search_by_2_id').val(thee_id.value);
			}
		}	
	
	
			
	$(document).ready(function() {
	$('#txt_in_time').timepicker({								 
	showMillisec: true,
	timeFormat: 'hh:mm:ss'
	//ampm: true
	});
	});
	$(document).ready(function() {
	$('#txt_out_time').timepicker({	  
	showMillisec: true,
	timeFormat: 'hh:mm:ss'
	//ampm: true
	});
	});
	$(document).ready(function() {
	$('#txt_select_date').datepicker({
	dateFormat: 'dd-mm-yy',
	//ampm: true
	});
	});
	
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
	function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "report/hrm/hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
	function populate_value(str)
		{
			//alert(str);
			var section='<option value=0>--select--</option>>';
			if(str==1)
				{
					<?php
						foreach( $section_details AS $key => $value ) {						
							echo "section += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('search_by_td_up').innerHTML="Select Section";
					document.getElementById('search_field_td_up').innerHTML='<select name="cbo_search_by_2_id" style="width:110px" class="combo_boxes" id="cbo_search_by_2_id">'+section+'</select>';
				}
				
				var sub_section='<option value=0>--select--</option>>';//subsection add by ekram
			if(str==7)
				{
					<?php
						foreach( $sub_section_details AS $key => $value ) {						
							echo "sub_section += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('search_by_td_up').innerHTML="Select Subsection";
					document.getElementById('search_field_td_up').innerHTML='<select name="cbo_search_by_2_id" style="width:110px" class="combo_boxes" id="cbo_search_by_2_id">'+sub_section+'</select>';
				}
				
			var designation='<option value=0>--select--</option>>';
			if(str==2)
				{
					<?php
						foreach( $designation_details AS $key => $value ) {						
							echo "designation += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('search_by_td_up').innerHTML="Select Designation";
					document.getElementById('search_field_td_up').innerHTML='<select name="cbo_search_by_2_id" style="width:110px" class="combo_boxes" id="cbo_search_by_2_id">'+designation +'</select>'; 
				}
			//var emp_code='<option value=0>--select--</option>>';
			if(str==3)
				{
		document.getElementById('search_by_td_up').innerHTML="Employee Code";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="cbo_search_by_2_id" id="cbo_search_by_2_id" class="text_boxes" style="width:100px" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(\'search_employee_multiple.php\',\'Employee Information\'); return false" placeholder="Double Click For Search"/>';
				}
			
			//var id_card_no='<option value=0>--select--</option>>';
			if(str==4)
				{
					document.getElementById('search_by_td_up').innerHTML="Employee ID";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="cbo_search_by_2_id" id="cbo_search_by_2_id" class="text_boxes" style="width:100px" />';
				}
			
			var policy='<option value=0>--select--</option>>';
			if(str==5)
				{
					<?php
						foreach( $policy_details AS $key => $value ) {						
							echo "policy += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('search_by_td_up').innerHTML="Select policy";
					document.getElementById('search_field_td_up').innerHTML='<select name="cbo_search_by_2_id" style="width:110px" class="combo_boxes" id="cbo_search_by_2_id">'+policy +'</select>'; 
				}
		
			var department='<option value=0>--select--</option>>';
			if(str==6)
				{
					<?php
						foreach( $department_details AS $key => $value ) {						
							echo "department += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('search_by_td_up').innerHTML="Select Department";
					document.getElementById('search_field_td_up').innerHTML='<select name="cbo_search_by_2_id" style="width:110px" class="combo_boxes" id="cbo_search_by_2_id">'+department +'</select>'; 
				}
				
				
		}

	function checkAll(field)
	{
		 selected_id.length = 0;
		 selected_id_attnd.length=0;
		$('#selected_id_i').val( '' );
		$('#selected_id_emp').val( '' );
	//	alert($('#selected_id_i').val());
	 	 
							 
		//if ($('#chk_all').is(':checked'))
		if ($('#chk_all').val()==0)
		{	
			$('#chk_all').val('1');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{	
				$('#chk_all_'+i).attr('checked', 'checked');
				js_set_value( i );
				
			}
		}
		else
		{
			$('#chk_all').val('0');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{
				$('#chk_all_'+i).removeAttr("checked");
				//js_set_value( i );				
			}
			document.getElementById('show_count_list').innerHTML="You have selected "+ selected_id_attnd.length +" Employees";
		}
	}
	
	function js_set_value( str ) 
	{
		if( jQuery.inArray( str, selected_id ) == -1 ) 
		{
			selected_id.push( str );
		}
		else 
		{
			for( var i = 0; i < selected_id.length; i++ ) 
			{
				if( selected_id[i] == str ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id = '';
		for( var i = 0; i < selected_id.length; i++ ) 
		{
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_i').val( id );
		js_set_value_attnd( str );
	
		
	}
	
	function js_set_value_attnd( str ) 
	{
		if ($('#selected_id_emp').val()=="") selected_id_attnd.length = 0;
		if( jQuery.inArray(  $('#attnd_id_' + str).val(), selected_id_attnd ) == -1 ) 
		{
			selected_id_attnd.push( $('#attnd_id_' + str).val() );
		}
		else 
		{
			for( var i = 0; i < selected_id_attnd.length; i++ ) 
			{
				if( selected_id_attnd[i] == $('#attnd_id_' + str).val() ) break;
			}
			selected_id_attnd.splice( i, 1 );
		}
		var id  = '';
		for( var i = 0; i < selected_id_attnd.length; i++ ) 
		{
			id += selected_id_attnd[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_emp').val( id );
		document.getElementById('show_count_list').innerHTML="You have selected "+ selected_id_attnd.length +" Employees";
	}
	
	function fn_add_values_in_html()
	{
		$("#messagebox").html('<img src="../images/loading.gif" />').fadeIn(1000);
		
		if($('#chk_next_day').attr('checked')){
			var is_next_day = '1';
		}
		else
			var is_next_day = '0';
		
		if ($('#selected_id_i').val()=="")
		{ 
			alert ('No Employee or Attendance is Selected'); return false; 
		}
		else if ($('#cbo_day_status').val()==0)
		{ 
			alert ('Select Day Status for Effect'); return false; 
		}		
		else
		{
			var txt_from_date=$('#txt_from_date').val();
			var txt_to_date=$('#txt_to_date').val();
			
			var data=$('#selected_id_i').val()+"__"+$('#selected_id_emp').val()+"__"+$('#cbo_day_status').val()+"__"+$('#txt_in_time').val()+"__"+$('#txt_out_time').val()+"__"+ is_next_day +"__"+$('#cbo_att_policy').val()+"__"+$('#txt_remarks').val();
			//alert(data);
			$.ajax({type: "POST",url: "includes/manual_attendance_data.php?type=manual_attendance_data_set&data="+data+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date,
				
				success: function(response){
					eval(response);
					$("#messagebox").removeClass().addClass('messagebox').text('Attendace Data ADD in Done').fadeIn(1000);
				}
			});				
					
		}
	}
	
</script>


<script>
//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

</script>
<style>

.verticalText 
{               
    /* IE-only DX filter */
    writing-mode: tb-rl;
    filter: flipv fliph;

    /* Safari/Chrome function */
    -webkit-transform: rotate(270deg);

    /* works in latest FX builds */
	
    -moz-transform: rotate(270deg);
}

</style>

</head>

<body style="font-family:verdana; font-size:11px;">
 <div align="center">	
    <div>
    	<div class="form_caption">
		Manual Attendance Entry
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
    <form name="frm_manual_attn" id="frm_manual_attn" >		
    <fieldset style="width:1300px">
      <legend> Search Panel</legend>			
			<table class="rpt_table"  width="1200" cellspacing="0">
            	<thead>	
            		<tr>
                    <th width="160">Company Name</th>				    
                    <th width="100">Location </th>				    
                    <th width="125"> Search By </th>
                    <th width="100" align="center" id="search_by_td_up">Employee Code</th>
				   
                    <th width="220">Date</th>		      		
                    <th width="120">Category</th>
                    <th width="100">Status</th>				                   
                    <th width="90">Emp Status</th> 
                    <th> <input type="reset" name="reset" id="reset" value="Reset" class="formbutton" style="width:70px" />                  
                    <input type="hidden" name="selected_id_i" id="selected_id_i" />
                    <input type="hidden" name="selected_id_emp" id="selected_id_emp" /></th>
            	</thead>
			<tr class="general" align="center">					
					<td>					 
					    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:150px" >
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                              <?php
							   		}
									$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 $company_cond ORDER BY company_name ASC";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
									$numrows=mysql_num_rows($result);
									while( $row = mysql_fetch_assoc( $result ) ) {										
								?>							
							<option value="<?php echo $row['id']; ?>" <? if($numrows==1)echo "selected"; ?> ><?php echo $row['company_name']; ?></option>
								<?php 
									} 
								?>
						</select>
			       </td>
					
					<td>					  
					    <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                          <?php  
                                        $sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
                                        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                                        while( $row = mysql_fetch_assoc( $result ) ) {										
                                    ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['location_name']; ?></option>
                          <?php 
                                        } 
                                    ?>
                      </select>
			        </td>
			  		<td>
                      <select name="cbo_search_by_1" id="cbo_search_by_1" class="combo_boxes" style="width:90px" onchange="populate_value( this.value );">
                            <option value="0">---Select---</option>
                            <option value="1">Sectoin</option>
                            <option value="7">Subsection</option>
                            <option value="2">Designation</option>
                            <option value="3">Employee Code</option>
                            <option value="4">Employee ID</option>
                            <option value="5">Attendence Policy</option>
                            <option value="6">Department</option>
                            
                      </select></td>
			  		<td id="search_field_td_up"><select name="cbo_search_by_2_id" id="cbo_search_by_2_id" class="combo_boxes" style="width:90px">
			  		  <option></option>
		  		    </select></td>
					<td>
				      	<input name="txt_from_date" id="txt_from_date" type="text" value=""  class="datepicker" style="width:70px" placeholder="From Date"  />
				      -	<input name="txt_to_date" id="txt_to_date" type="text"  class="datepicker" value="" style="width:70px" placeholder="To Date"  />
					</td>
                    <td><select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:100px">
                         	<option value="">All Category</option>
                            <option value="0">Top Management</option>
							<option value="1">Mid Management</option>
							<option value="2">Non Management</option>
                         </select></td>
                    <td align="center">
                    	<select name="cbo_status" id="cbo_status" class="combo_boxes">
                          <option value="0">All Status</option>
                          <option value="1">Absent</option>
                          <option value="2">Late</option>
                          <!--<option value="3">Leave</option>-->
                          <option value="4">Present</option>
                          <!--<option value="5">Present+Leave</option>-->
                          <option value="6">Weekend</option>
                          <option value="8">Questionable</option>
                          <option value="9">Questionable IN</option>
                          <option value="10">Questionable OUT</option>
                    	</select>
                   </td>
                   <td align="center">
                   		<select name="cbo_emp_status" id="cbo_emp_status" class="combo_boxes" style="width:80px" >
                           <option value="1">Regular</option>
                           <option value="0">Separated</option>
                    </select>
                   </td>
                  <td><input type="button" name="search" id="search" value="Search" class="formbutton" style="width:70px" onclick="javascript: fn_search_manual_attendence()" /></td>
              </tr>                
            </table>         
           <!-- <legend id="show_count_list"></legend>-->
            <table class="rpt_table" width="85%" cellspacing="1" >
			<thead>
            		<th width="170">Day Status</th>				    
                    <th width="150">In Time</th>				    
                    <th width="120">Out Time</th>				                     
                    <th width="70">Next Day</th>		      		                 
                    <!-- <th width="150">
		      		Shift Policy</th>-->               
                    <th width="115"> Remarks</th>				                    
                    <td align="left" valign="left"><input type="reset" value="  Refresh  " name="add" id="close" style="width:100px" class="formbutton"/></td>
            </thead>
            <tr align="center" class="general">
           	<td>
               	<input name="chk_day_status" id="chk_day_status" type="checkbox" value="" onclick="fn_checked('chk_day_status','cbo_day_status','cbo')" />
               	<select name="cbo_day_status" id="cbo_day_status" class="combo_boxes" style="width:135px" disabled="disabled">
               		   <option value="0"></option>
                       <option value="A">Absent</option>
                       <option value="PL">Present(Late)</option>
                       <option value="P">Present(No Late)</option>
               		   <option value="W">Weekend</option>
               		   <option value="CH">Holiday</option>
                       <option value="GD">General Duty</option>
       		    </select>
              </td>
              <td><input name="chk_in_time" id="chk_in_time" type="checkbox" value="" onclick="fn_checked('chk_in_time','txt_in_time','txt')" />
                <input name="txt_in_time" id="txt_in_time" type="text"  class="text_boxes" style="width:70px" disabled="disabled"/>
              </td>
              <td><input id="chk_out_time" name="chk_out_time" type="checkbox" value="" onclick="fn_checked('chk_out_time','txt_out_time','txt')" />
                <input name="txt_out_time" id="txt_out_time" type="text"  class="text_boxes" style="width:70px"  disabled="disabled" /></td>
               
               <td><input name="chk_next_day" id="chk_next_day" type="checkbox" value="" /></td>
                 
                <input name="chk_att_policy" id="chk_att_policy" type="hidden" value="" onclick="fn_checked('chk_att_policy','cbo_att_policy','cbo')" />
                <input type="hidden" name="cbo_att_policy" id="cbo_att_policy" class="combo_boxes" style="width:100px" disabled="disabled" value="0" >
                <!--   <td> 	<option value="0"></option>
                    	<?php /*?><?php
                        	foreach($policy_details as $key => $val){
						?>
                        		<option value="<?php echo $key; ?>"><? echo $val; ?></option>        
                        <?php
							}
						
						?><?php */?>
                	</select>                                        
                </td>-->
              
                <td><input name="txt_remarks" id="txt_remarks" type="text"  class="text_boxes" style="width:250px" /></td>
                <td style="text-align:left" align="left">
                	<input type="button" name="search" id="search" value=" Add " class="formbutton" style="width:100px" onclick="fn_add_values_in_html()" />&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
			</table>
            </fieldset> 
            <fieldset style="width:1090px">
                 <span id="show_count_list" style="font-weight:bold">&nbsp;</span>             
      			<div style="width:100%; float:left; margin:auto" align="center" id="manual_attendence_list_container"></div><br/>                      
                <div align="center"><input type="button" value="Update Attendance" name="update" id="update" class="formbutton" style="width:130px" onclick="javascript:fn_update_attn_manual(save_perm,edit_perm,delete_perm,approve_perm)" /></div>                     			
      		</fieldset>
      </form>
      </div>     
</body>
</html>
