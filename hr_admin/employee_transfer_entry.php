<?
/***************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 09.06.2014
*****************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../includes/common.php');
include("../includes/array_function.php");

// company
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}
// location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}
// division
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}
// department
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}
// section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}
// subsection
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}
// designation
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}
// training name
$sql = "SELECT * FROM lib_training WHERE status=1 and is_deleted = 0 and status_active=1 ORDER BY training_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$training_name = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$training_name[$row['id']] = mysql_real_escape_string($row['training_name']);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        <script src="includes/functions.js" type="text/javascript" ></script>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../includes/tablefilter.js"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        
        <script type="text/javascript">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
            });
			
			// createObject
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			var http = createObject();
			var field_array = new Array();
			
			//Numeric Value allow field script
			function numbersonly(myfield, e, dec)
			{
				var key;
				var keychar;
				
				if (window.event)
				key = window.event.keyCode;
				else if (e) key = e.which;
				else return true;
				keychar = String.fromCharCode(key);
				
				// control keys
				if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
				return true;
				
				// numbers
				else if ((("0123456789.,-").indexOf(keychar) > -1)) return true;
				else return false;
			}
			
            // populate_list_view
			function populate_list_view(str) 
            {
				//alert("su..re");
			    $.ajax({
                    type: "POST",					
                    url: "includes/list_view.php",
                    data: 'form=frm_employee_transfer_entry'+'&emp_code='+str,
                    success: function(data) {
                        $('#data_panel').html( data );
                    }
                });
            }		
			
			// func_save_employee_transfer_entry
			function func_save_employee_transfer_entry(save_perm,edit_perm,delete_perm,approve_perm)
			{
				//alert("su..re");
				if($('#txt_id_card_no').val()=="" && $('#txt_emp_code').val()=="")
				{
                    if($('#txt_id_card_no').val()=="")
                    {
                        $('#messagebox').fadeTo( 200, 0.1, function() {
                            $('#txt_id_card_no').focus();
                            $(this).html('Please enter employee id card.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                    }
                    else
                    {
                        $('#messagebox').fadeTo( 200, 0.1, function() {
                            $('#txt_emp_code').focus();
                            $(this).html('Please enter employee code.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                    }
				}
				else if($('#txt_effective_date').val()=="")
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_effective_date').focus();
						$(this).html('Please enter effective date.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else 
				{
					$('#messagebox').removeClass().addClass('messagebox').text('Please wait....').fadeIn(1000);
					var emp_info=$('#hidden_emp_info').val().split('_');
					var data=	'&emp_code='+emp_info[0]+
								'&pre_company_id='+emp_info[1]+
								'&pre_location_id='+emp_info[2]+
								'&pre_division_id='+emp_info[3]+
								'&pre_department_id='+emp_info[4]+
								'&pre_section_id='+emp_info[5]+
								'&pre_subsection_id='+emp_info[6]+
								'&company_id='+$('#cbo_company_id').val()+
								'&location_id='+$('#cbo_location_id').val()+
								'&division_id='+$('#cbo_division_id').val()+
								'&department_id='+$('#cbo_department_id').val()+
								'&section_id='+$('#cbo_section_id').val()+
								'&subsection_id='+$('#cbo_subsection_id').val()+
								'&effective_date='+$('#txt_effective_date').val()+
								'&update_id='+$('#update_id').val();
								
					nocache = Math.random();
					http.open('GET','includes/save_update_hr_admin.php?action=action_employee_transfer_entry'+data+'&nocache=' + nocache);
					http.onreadystatechange = response_employee_transfer_entry;
					http.send(null);
				}	
            }
				
    		function response_employee_transfer_entry()
			{
				if(http.readyState == 4)
				{ 		
					var response = http.responseText.split('###');
					if(response[0]==1)
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html( 'Data Saved Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						populate_list_view(response[1]);
						refresh_to_info();
					}
					if(response[0]==2)
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html( 'Data Updated Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						populate_list_view(response[1]);
						refresh_to_info();
					}
					if(response[0]==3)
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html( 'Duplicate Entry!!!!!!!!!!!!!!!!!!!' ).addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						populate_list_view(response[1]);
						//refresh_to_info();
					}
				}
			}
			
			function refresh_to_info()
			{
				$('#cbo_company_id').val(0);
				$('#cbo_location_id').val(0);
				$('#cbo_division_id').val(0);
				$('#cbo_department_id').val(0);
				$('#cbo_section_id').val(0);
				$('#cbo_subsection_id').val(0);
				$('#txt_effective_date').val("");
			}

			// emp_code
            function openmypage_employee_info()
            {			
				//alert("su..re..");
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_employee_single_by_code.php', 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
                emailwindow.onclose=function()
                {
				   	//alert("su..re..");
					var thee_id = this.contentDoc.getElementById("txt_hidden_emp_code");
					var thee_ids = this.contentDoc.getElementById("hidden_emp_code");
					$('#hidden_emp_info').val(thee_ids.value);
					var emp_info=(thee_id.value).split('_');
					$('#txt_id_card_no').val(emp_info[0]);
					$('#txt_emp_code').val(emp_info[1]);
					$('#txt_emp_name').val(emp_info[2]);
					$('#txt_company_id').val(emp_info[3]);
					$('#txt_location_id').val(emp_info[4]);
					$('#txt_division_id').val(emp_info[5]);
					$('#txt_department_id').val(emp_info[6]);
					$('#txt_section_id').val(emp_info[7]);
					$('#txt_subsection_id').val(emp_info[8]);
					$('#txt_designation').val(emp_info[9]);
					$('#txt_joining_date').val(emp_info[10]);
					
					populate_list_view(emp_info[1]);
                }
            }
			
			// id_card_no	
            function openmypage_employee_info_id_card()
            {			
				//alert("su..re..");
				emailwindow=dhtmlmodal.open('EmailBox','iframe','search_employee_single_by_id.php','Employee Information','width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
                emailwindow.onclose=function()
                {
				   	//alert("su..re..");
					var thee_id = this.contentDoc.getElementById("txt_hidden_id_card_no");
					var thee_ids = this.contentDoc.getElementById("hidden_id_card_no");
					$('#hidden_emp_info').val(thee_ids.value);
					var emp_info=(thee_id.value).split('_');
					$('#txt_id_card_no').val(emp_info[0]);
					$('#txt_emp_code').val(emp_info[1]);
					$('#txt_emp_name').val(emp_info[2]);
					$('#txt_company_id').val(emp_info[3]);
					$('#txt_location_id').val(emp_info[4]);
					$('#txt_division_id').val(emp_info[5]);
					$('#txt_department_id').val(emp_info[6]);
					$('#txt_section_id').val(emp_info[7]);
					$('#txt_subsection_id').val(emp_info[8]);
					$('#txt_designation').val(emp_info[9]);
					$('#txt_joining_date').val(emp_info[10]);
					
					populate_list_view(emp_info[1]);
                }
            }
			
			// js_set_value_list_view
			function js_set_value_list_view(str)
			{
				//alert("su..re");
				var emp_info=str.split('_');
				$('#update_id').val(emp_info[0]);
				$('#cbo_company_id').val(emp_info[1]);
				$('#cbo_location_id').val(emp_info[2]);
				$('#cbo_division_id').val(emp_info[3]);
				$('#cbo_department_id').val(emp_info[4]);
				$('#cbo_section_id').val(emp_info[5]);
				$('#cbo_subsection_id').val(emp_info[6]);
				$('#txt_effective_date').val(emp_info[7]);
			}
			
			// reset_form
			function reset_frm()
			{
				<!--$('#data_panel').hide();-->
				document.getElementById('frm_employee_transfer_entry').reset();
			}
			
			function on_key_down( from, evt  )
			{
				if (evt!=13){ var unicode=evt.keyCode? evt.keyCode : evt.charCode } else {unicode=13;}
				//alert(unicode)
				if(unicode!=40) return;
				
				if( from==1 )// ID CARD
				{
					var id_card=$('#txt_id_card_no').val();
					var emp_code=$('#txt_emp_code').val();
					$.ajax({
						type: "POST",
						url: "includes/list_view.php",
						data: 'link=link_employee_transfer_entry&emp_code=' + emp_code+'&id_card_no=' + id_card,
						success: function(data) {				
						eval(data);
						//$('#new').css( "visibility", "visible" );
						//enable_tabs();
						populate_list_view($('#txt_emp_code').val());	
						}
					});
				}
			}
        </script>
        <style>
            .formbutton { width:100px; }
            #reactivation_form * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px;">		
        <div style="height:58px;">
            <div class="form_caption">Employee Transfer Entry</div>
            <span id="permission_caption"><? echo "Your Permissions--> \n".$insert;?></span>
            <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
        </div>
        <form name="frm_employee_transfer_entry" id="frm_employee_transfer_entry" action="javascript:func_save_employee_transfer_entry(save_perm,edit_perm,delete_perm,approve_perm);" >
            <fieldset style="padding:10px"><legend>Employee Transfer</legend>
                <fieldset>
                    <div style="border: 1px solid #555FFF; width:600px;" align="center">
                        <table width="580px" cellpadding="2" cellspacing="0" id="tbl_training" align="center">
                            <tbody>
                                <tr height="25px">
                                    <td width="120px"><strong>ID Card No</strong></td>
                                    <td width="150px">
                                        <input type="text" name="txt_id_card_no" id="txt_id_card_no" class="text_boxes" onkeyup="on_key_down(1, event);" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" style="width:150px;" />
                                    </td>
                                    <td width="120px"><strong>System Code</strong></td>
                                    <td width="150px">
                                        <input type="text" name="txt_emp_code" id="txt_emp_code" class="text_boxes" onkeyup="on_key_down(1, event);" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" style="width:150px" />
                                    </td>
                                </tr>                              
                                <tr height="25px">
                                    <td width="140px"><strong>Employee Name</strong></td>
                                    <td width="440px" colspan="3"><input type="text" name="txt_emp_name" id="txt_emp_name" class="text_boxes" readonly="readonly" style="width:440px" /></td>
                                </tr>
                                <tr height="25px">
                                    <td><strong>Designation</strong></td>
                                    <td><input type="text" name="txt_designation" id="txt_designation" class="text_boxes" readonly="readonly" /></td>
                                    <td><strong>Joining Date</strong></td>
                                    <td><input type="text" name="txt_joining_date" id="txt_joining_date" class="text_boxes" readonly="readonly" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </fieldset>
                    <div style="height:5px;"></div>
                    <fieldset>
                   <div style="border: 1px solid #555FFF; width:600px;">
                    <table width="600px" cellpadding="" cellspacing="" border="0">
                        <tr>
                            <td align="center" width="300px"><strong>From</strong></td>
                            <td align="center" width="300px"><strong>To</strong></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td width="100px"><strong>Company</strong></td>
                                        <td width="200px"><input type="text" name="txt_company_id" id="txt_company_id" readonly="readonly" style="width:200px;" class="text_boxes"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><strong>Location</strong></td>
                                        <td width="200px"><input type="text" name="txt_location_id" id="txt_location_id" readonly="readonly" style="width:200px;" class="text_boxes"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><strong>Division</strong></td>
                                        <td width="200px"><input type="text" name="txt_division_id" id="txt_division_id" readonly="readonly" style="width:200px;" class="text_boxes"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><strong>Department</strong></td>
                                        <td width="200px"><input type="text" name="txt_department_id" id="txt_department_id" readonly="readonly" style="width:200px;" class="text_boxes"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><strong>Section</strong></td>
                                        <td width="200px"><input type="text" name="txt_section_id" id="txt_section_id" readonly="readonly" style="width:200px;" class="text_boxes"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><strong>Subsection</strong></td>
                                        <td width="200px"><input type="text" name="txt_subsection_id" id="txt_subsection_id" readonly="readonly" style="width:200px;" class="text_boxes"/></td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><strong>&nbsp;</strong></td>
                                        <td width="200px">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td width="120px"><strong>Company</strong></td>
                                        <td width="180px">
                                            <select name="cbo_company_id" id="cbo_company_id" style="width:180px;" class="combo_boxes">
                                                <option value="0">-- select --</option>
                                                <?php 
                                                    foreach($company_details as $key=>$value)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120px"><strong>Location</strong></td>
                                        <td width="180px">
                                            <select name="cbo_location_id" id="cbo_location_id" style="width:180px;" class="combo_boxes">
                                                <option value="0">-- select --</option>
                                                <?php 
                                                    foreach($location_details as $key=>$value)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120px"><strong>Division</strong></td>
                                        <td width="180px">
                                            <select name="cbo_division_id" id="cbo_division_id" style="width:180px;" class="combo_boxes">
                                                <option value="0">-- select --</option>
                                                <?php 
                                                    foreach($division_details as $key=>$value)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120px"><strong>Department</strong></td>
                                        <td width="180px">
                                            <select name="cbo_department_id" id="cbo_department_id" style="width:180px;" class="combo_boxes">
                                                <option value="0">-- select --</option>
                                                <?php 
                                                    foreach($department_details as $key=>$value)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Section</strong></td>
                                        <td width="180px">
                                            <select name="cbo_section_id" id="cbo_section_id" style="width:180px;" class="combo_boxes">
                                                <option value="0">-- select --</option>
                                                <?php 
                                                    foreach($section_details as $key=>$value)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120px"><strong>Subsection</strong></td>
                                        <td width="180px">
                                            <select name="cbo_subsection_id" id="cbo_subsection_id" style="width:180px;" class="combo_boxes">
                                                <option value="0">-- select --</option>
                                                <?php 
                                                    foreach($subsection_details as $key=>$value)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120px"><strong>Effective Date</strong></td>
                                        <td width="180px"><input type="text" name="txt_effective_date" id="txt_effective_date" class="datepicker" style="width:167px;" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </div>
                    <div style="padding-top:10px;">
                        <input type="submit" name="save" id="save" value="Save"  class="formbutton"/>
                        <input type="button" name="cancel" id="cancel" value="Refresh"  class="formbutton" onclick="reset_frm()" />
                        <input type="hidden" name="hidden_emp_info" id="hidden_emp_info" />
                        <input type="hidden" name="update_id" id="update_id" />
                    </div>
                </fieldset>
            </fieldset>
        </form>  
        <span id="count_id" style="font-weight:bold"></span>
        <div id="tbl_list_view"></div>
        <div id="data_panel" style="margin-top:14px;"></div>
		</div>
    </body>
</html>