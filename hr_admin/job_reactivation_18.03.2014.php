<?
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 04-04-12
	multiselect added By
	Name : Ekram
	Date : 19-11-2013
		
######################################*/



session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../includes/common.php');
include("../includes/array_function.php");

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
	
	<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
	
    <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
	<script type="text/javascript" src="../includes/tablefilter.js"></script>
    
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
	<script type="text/javascript" src="../js/modal.js"></script>
    
    <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
				
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
			
	$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
 });		
			
			
			
			
			$('#chk_all').click(function() {
				$('input', oTable.fnGetNodes()).attr('checked', this.checked);
			});
		});
		
		function populate_data() 
		{
			$('#data_panel').html('<img src="../resources/images/loading.gif" />');
			var category 			= $('#cbo_emp_category').val();
			var company_id 			= $('#cbo_company_id').val();
			var location_id 		= $('#location_id').val();
			var division_id 		= $('#division_id').val();
			var department_id 		= $('#department_id').val();
			var section_id 			= $('#section_id').val();
			var subsection_id 		= $('#subsection_id').val();			
			var designation_id 		= $('#designation_id').val();
			var id_card 			= $('#id_card').val();
			var emp_code 			= $('#emp_code').val();
			
			var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&division_id='+division_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&id_card='+id_card+'&empcode='+emp_code;
			
			$.ajax({
				type: "POST",
				url: "employee_info_data.php?"+passdata,
				data: 'form=job_reactivation',
				success: function(data) {
					$('#data_panel').html( data );	
					var tableFilters = {col_0: "none"}
					setFilterGrid("tbl_reactivation",-1,tableFilters);				
				}
			});
		}		
		
	/*function Check(chk)
		{			
			var rowCount = $('#tbl_reactivation tr').length;						
			if(document.reactivation_form.Check_ctr.checked==true){
				for (i = 0; i < rowCount; i++){
					chk[i].checked = true ;	
					var d=(i*1)+1;
					document.getElementById('count_id').innerHTML="You Are Selected "+ d +" Employees";				
				}
			}else{			
				for (i = 0; i < rowCount; i++)
				chk[i].checked = false ;
				rowCount=0;		
				//document.getElementById('count_id').innerHTML="You Are Selected 0 Employees";	
			}
			//document.getElementById('count_id').innerHTML="";
			//document.getElementById('count_id').innerHTML="You Are Selected "+cnt+" Employees";
			$('#count_id').html('You Are Selected '+rowCount+' Employees');
		}
	function count_emp(chk){
		var cnt=0;
		
		var rowCount = $('#tbl_reactivation tr').length;
		for (i = 0; i < rowCount; i++){
			if(chk[i].checked) cnt++;
			document.getElementById('count_id').innerHTML="You Are Selected "+cnt+" Employees";
		}
		
		//document.getElementById('count_id').innerHTML="";
		
		//$('#count_id').html('You Are Selected '+cnt+' Employees');
	}*/
		
//new add ekram	
function Check(chk)
		{
			var cnt=0;
			
			if(document.reactivation_form.Check_ctr.checked==true){
				
				for (i = 0; i < chk.length; i++){
					chk[i].checked = true ;
					cnt++;
					
				}
			}else{			
				for (i = 0; i < chk.length; i++)
				chk[i].checked = false ;			
			}
			$('#count_id').html('You Are Selected '+cnt+' Employees');
		}
		
	function count_emp(chk){
		var cnt=0;
		$('#tbl_reactivation tbody input[type="checkbox"]:checked').each(function() {
				cnt++;
			});
			document.getElementById('cnt_val').value=cnt;
		$('#count_id').html('You Are Selected '+cnt+' Employees');
	}

	function openmypage_employee_info(page_link,title)
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#emp_code').val(thee_id.value);
			}
		}	
	function openmypage_employee_info_id_card(page_link,title)
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#id_card').val(thee_id.value);
			}
		}    //Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

    
    
    
    </script>
	<style>
		.formbutton { width:100px; }
		#reactivation_form * { font-family:verdana; font-size:11px; }
	</style>
</head>
<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px">		
    <div>
    	<div class="form_caption">Employee Job Reactivation</div>
        <span id="permission_caption"><? echo "Your Permissions--> \n".$insert;?></span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> - Employee Job Reactivation</h3>

	<form name="reactivation_form" id="reactivation_form" method="POST">
    <div id="content_search_panel" >
		<fieldset style="padding:10px;">
			<table width="1000px" cellpadding="0" cellspacing="2">
				<tr>
					<td width="8%">Re-join Date:</td><input type="hidden" id="cnt_val" name="cnt_val" />
					<td width="17%"><input type="text" name="rejoin_date" id="rejoin_date" value="" class="datepicker" style="width:180px" /></td>
					<td width="13%">Service Benefit from:</td>
					<td width="62%" id="">
						<select name="service_benefit_from" id="service_benefit_from" class="combo_boxes" style="width:180px">
							<option value="0">Original DOJ</option>
							<option value="1">Re-join Date</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
        <fieldset style="padding:10px;">
        <table class="rpt_table" style="width:60%;" border="0" cellpadding="0" cellspacing="2">
				<thead>			                
                	<th> Category</th>
					<th> Company</th>
					<th> Location</th>
                    <th> Division</th>
					<th> Department</th>
                    <th> Section</th>
                    <th> SubSection</th>
                    <th> Designation</th>
                    <th> ID Card No</th> 
                   	<th> System Code</th>                   			
					<td><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" /></td>                  
				</thead>
                <tr class="general">					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:120px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
                  <td>
                        <select name="division_id" id="division_id" class="combo_boxes" style="width:120px" multiple="multiple">
                          <option value="0">-- Select --</option>
                            <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px" multiple="multiple">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:120px" multiple="multiple">
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px" multiple="multiple">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td> 
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" multiple="multiple">
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td>
                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card('search_employee_multiple_by_id.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" />
                    </td>
                   <td>
			 <input type="text" name="emp_code" id="emp_code" class="text_boxes" style="width:100px" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info('search_employee_multiple.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" /></td>                               
					<td><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()" /></td>
				</tr> 
          </table> 
        </fieldset>
        </div>
		<fieldset><span id="count_id" style="font-weight:bold"></span><div id="data_panel" style="margin-top:4px;"></div>
		<div style="width:100%; margin-top:10px; text-align:center;">
			<input type="button" name="reactivate" value="Reactivate" class="formbutton" onclick="fnc_job_reactivation(save_perm,edit_perm,delete_perm,approve_perm,document.getElementById('cnt_val').value)" />
		</div>
        </fieldset>
	</form>
</body>
</html>