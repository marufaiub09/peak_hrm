<?
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date :17/04/2012
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond."xcz";
//--------------------------------------------------------------------------------------------------------------------
date_default_timezone_set('UTC');
include('../includes/common.php');
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>	

<style type="text/css">
div.examplel {
	background: #333;
	width: 200px;
	height: 50px;
	-moz-border-radius: 40px;
	-webkit-border-radius: 40px;
	border-radius: 40px;
} 
.examplel p {
       padding: 15px 15px 0 15px;
	   font-size:28px; color:#FFF;
}
div.example2 {
	background:#333;
	width: 400px;
	height: 50px;
	-moz-border-radius: 0px;
	-webkit-border-radius: 0px;
	border-radius: 0px;
} 
.example2 p {
       padding: 15px 15px 0 15px;
	   font-size:28px; color:#FFF;
}
   @page { size 8.5in 11in; margin: 0.5in; }
    div.page { page-break-after: always; background-color:#FFF;}
	</style>
	<script src="includes/functions.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
	
    <script src="../resources/jquery-1.6.1.js" type="text/javascript"></script>
    <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>	
   
   <link href="../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
    
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
    <script type="text/javascript" src="../js/modal.js"></script>
    
   <script type="text/javascript">
  
	function openmypage_employee_info(page_link,title)
		{
			// alert(page_link);
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,'width=1000px,height=400px,center=1,resize=0,scrolling=0','')
			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");
				
				$('#txt_emp_code').val(thee_id.value);
				//alert(thee_loc.value+thee_id.value);
				//return_item_drop_down(thee_id.value,"confirmation_letter_emp_info","confirm_div");
				fnc_confirmation_letter(thee_id.value,"confirmation_letter_emp_info");
			}
		}
		
		function new_window()
		{
			var w = window.open("Surprise", "#");
			var d = w.document.open();
			d.write (document.getElementById('confirm_div').innerHTML);
			d.close();
		}	
	
</script>

</head>
<body>
<div align="center" style="width:100%; height:40px; margin:5px 0;">
        <div class="form_caption">
             Confirmation Letter
        </div>
		<div align="center" id="messagebox"></div>
	</div>
<form id="frm_confirmation_letter" action="" autocomplete="off" method="post">
<fieldset>
<legend>Search</legend>
		<div align="center">
        <table>
				<tr>
					<td colspan="2" align="">Search Employee:</td>
                    <td colspan="2" align=""><input type="text" placeholder="Double Click For Search"  name="txt_emp_code" id="txt_emp_code" value="" class="text_boxes" size="40" ondblclick="openmypage_employee_info('search_emp_info_conf.php','Employee Information'); return false" readonly="readonly"/></td>
					<td>&nbsp;</td>	
                    <td></td>			
				</tr>
        </table>
        </div>
</fieldset>
</form>
<div id="data_panel1" align="center"></div>
<div id="confirm_div" style="margin-top:5px; width:100%;"></div>
</body>
</html>