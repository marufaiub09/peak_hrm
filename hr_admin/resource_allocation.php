<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 31-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
include('../includes/array_function.php');
$ConsumptionTabIndex=-1;
extract($_POST);
if(!$TabIndexNo) { $TabIndexNo=0; }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Resource Allocation</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen">
    
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/popup_window.js"></script>
	<script type="text/javascript" src="../js/modal.js"></script>
    
    <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>

	<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
	<script src="../resources/ui.tabs.paging.js" type="text/javascript"></script>
    
    <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
    <script src="includes/functions.js" type="text/javascript"></script>
    
	<script type="text/javascript">
        $(document).ready(function() {
            $('#example').tabs();
            $('#example').tabs('paging', { cycle: true, follow: true } );
            $('#example').tabs('select',<?php echo $TabIndexNo; ?>);
        });
    </script>
    
    <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>

        
	<script>
	
		function openmypage(page_link,title)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=390px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
			  	//document.getElementById('txt_emp_code').value=theemail.value;
				//alert(theemail.value);
				
				emp_search_resource_allocation(theemail.value,1);
				//showResult_search(document.getElementById("txt_emp_code").value,"resource_issue_item","list_view");
				setTimeout('showResult_search(document.getElementById("txt_emp_code").value,"resource_issue_item","list_view")',150);
			}
		}
		
	</script>
	
</head>
<body>
<div align="center">
	<div align="center" style="width:900px; position:relative; height:30px; margin-bottom:3px; margin-top:3px">
        <div class="form_caption">
           Resource Allocation
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;
            ?>
        </span>
        <div id="messagebox" style="background-color:#FF9999; color:#000000; width:900px" align="center"></div>
	</div>
    
     <div id="examples" align="center" style="width:900px;font-size:11px;">
     <form name="item_resource_allocation" id="item_resource_allocation" method="post" style="margin-top:25px"> 

     <!-- Start Field Set -->
	<fieldset style="width:880px ">
	<legend>HRM  Module</legend>
	<!-- Start Form -->
			<table cellpadding="0" cellspacing="1" width="100%">
				
				<tr>
					<td width="100" >
						Employee Code
					</td>
					<td width="130"> <!-- Select Calander search_employee_resource -->
                     <input name="txt_emp_code" id="txt_emp_code" placeholder="Double Click to Search" ondblclick="openmypage('search_employee.php','Employee Info'); return false" readonly class="text_boxes" style="width:155px">
					</td>
					<td width="100">
						Name
					</td>
					<td width="130"> <!--  System Generated-->
					<input name="txt_emp_name" id="txt_emp_name" class="text_boxes" style="width:155px" readonly>
					</td>
					
				</tr>
			
			<tr>
					<td width="100" >
						Company
					</td>
					<td width="130"> <!--  Entry-->
					  <input name="txt_company" id="txt_company" class="text_boxes" style="width:155px" readonly>
					</td>
					<td width="100">
						Division
					</td>
					<td width="130"> <!-- Select & Display-->
					<input name="txt_division" id="txt_division" class="text_boxes" style="width:155px" readonly>
					</td>
					
			  </tr>
              <tr>
              <td width="100">
						Designation
					</td>
					<td width="130"> <!--Entry -->
					  <input name="txt_designation" id="txt_designation" class="text_boxes" style="width:155px" readonly>
					</td>
                    <td width="100">
						Department
					</td>
					<td width="130"> <!--  Select-->
					  <input name="txt_department" id="txt_department" class="text_boxes" style="width:155px" readonly>					
					  </td>
              </tr>
			
			<tr>
					<td width="100" >
					Section 
					</td>
					<td width="130"> <!--  Select-->
					  <input name="txt_section" id="txt_section" class="text_boxes" style="width:155px" readonly>
					</td>
					<td width="100" >
					Sub Section 
					</td>
					<td width="130"> <!--  Select-->
					<input name="txt_sub_section" id="txt_sub_section" class="text_boxes" style="width:155px" readonly>
					</td>
					</tr>	
			</table>
			<!-- End Main Table -->
	</fieldset>
    </form>
	</div>
   
    <div id="example" align="center" style="width:1100px; margin-top:20px; font-size:11px;">
         <ul class="tabs">
			<li><a href="#issue_item" onClick="showResult_search(document.getElementById('txt_emp_code').value,'resource_issue_item','list_view')">Issue Item</a></li>
			<li><a href="#return_item" onClick="showResult_search(document.getElementById('txt_emp_code').value,'return_item','list_view1')">Return Item</a></li>
			<li><a href="#money_recovery" onClick="showResult_search_hidden(document.getElementById('txt_emp_code').value,'resource_money_recovery','list_view2')">Money Recovery</a></li>
			
		</ul>
		<div id="issue_item"> <?php include('resource_allocation/issue_item.php'); ?></div>
		<div id="return_item"> <?php include('resource_allocation/return_item.php'); ?></div>
		<div id="money_recovery"> <?php include('resource_allocation/item_money_recovery.php'); ?></div>
    </div>
</div>
</body>
</html>