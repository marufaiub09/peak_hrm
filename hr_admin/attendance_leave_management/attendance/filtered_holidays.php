<?php
include('../../../includes/common.php');
?>

<table id="tbl_holiday_list" style="width:100%;" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
					<thead>
						<tr>
							<th>From Date</th>
							<th>To Date</th>
							<th>Holiday Name</th>
							<th>Type</th>
							<th>Compensate for</th>
						</tr>
					</thead>
					<tbody>
<?						

$sql = "SELECT *
		FROM lib_holiday
		WHERE company_id IN ( $_POST[companies] )
		GROUP BY from_date, to_date, holiday_name, type, compensate_for
		HAVING count(*) = " . ( substr_count( $_POST['companies'], ',' ) + 1 );

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

while( $row = mysql_fetch_assoc( $result ) ) {
?>
<tr class="general">
	<td><input type="text" name="from_date[]" class="datepicker" value="<?php echo convert_to_mysql_date( $row['from_date'] ); ?>" /></td>
	<td><input type="text" name="to_date[]" class="datepicker" value="<?php echo convert_to_mysql_date( $row['to_date'] ); ?>" /></td>
	<td><input type="text" name="holiday_name[]" class="text_boxes" value="<?php echo $row['holiday_name']; ?>" style="width:270px;" /></td>
	<td>
		<select name="type[]" class="combo_boxes">
			<option value="0" <?php if( $row['type'] == 0 ) echo "selected"; ?>>Holiday</option>
			<option value="1" <?php if( $row['type'] == 1 ) echo "selected"; ?>>Compensatory</option>
            <option value="2" <?php if( $row['type'] == 2 ) echo "selected"; ?>>Festival holiday</option>
		</select>
	</td>
	<td><input type="text" name="compensate_for[]" class="datepicker" value="<?php if( $row['compensate_for'] != '0000-00-00' ) echo convert_to_mysql_date( $row['compensate_for'] ); ?>" /></td>
</tr>
<?php
}
?>

	</tbody>
</table>