<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract($_REQUEST);


//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']]=$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
	
	//Shift policy
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_details[$row['id']] = $row['shift_name'];
	}
	//Roster policy
	$sql = "SELECT * FROM lib_duty_roster_policy WHERE is_deleted = 0 group by roster_id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$roster_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$roster_details[$row['roster_id']] = $row['roster_rule_name'];
	}
	//overtime policy
	$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$overtime_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$overtime_details[$row['id']] = $row['policy_name'];
	}

if ($type==1){
$search_string=trim($search_string);
?>
<div>
    <div style="width:930px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="35" align="left"><strong>SL</strong></th>
                <th width="80" align="left"><strong>Emp. Code</strong></th>
                <th width="70" align="left"><strong>Emp. ID</strong></th>
                <th width="85" align="left"><strong>Punch Card</strong></th>
                <th width="130" align="left"><strong>Emp. Name</strong></th>
                <th width="130" align="left"><strong>Designation</strong></th>
                <th width="120" align="left"><strong>Company Name</strong></th>
                <th width="90" align="left"><strong>Division</strong></th>
                <th width="90" align="left"><strong>Department</strong></th>
                <th align="left"><strong>Section</strong></th><input type="hidden" name="id_field" id="id_field" />
            </thead>
		</table>
	</div>	
	<div style="width:930px; overflow-y:scroll; min-height:50px; max-height:230px;" id="resource_allocation_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
	
<?php
$i=1;
if ($search_string2==0){
		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name						
					FROM hrm_employee
					WHERE emp_code like '%$search_string' and is_deleted=0 and status_active=1 order by emp_code";		
		
}
elseif ($search_string2==1){
		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE concat(first_name,' ',middle_name,' ',last_name) like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code";
}
elseif ($search_string2==2){
		$sql= "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.custom_designation,a.*
		from hrm_employee a, lib_designation d
		where a.designation_id=d.id and d.custom_designation like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}
elseif ($search_string2==3){
		$sql= "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.division_name,a.*
		from hrm_employee a, lib_division d
		where a.division_id=d.id and d.division_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}
elseif ($search_string2==4){
		$sql= "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.department_name,a.*
		from hrm_employee a, lib_department d
		where a.department_id=d.id and d.department_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}
elseif ($search_string2==5){
		$sql= "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.section_name,a.*
		from hrm_employee a, lib_section d
		where a.section_id=d.id and d.section_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}
elseif ($search_string2==6){
		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE id_card_no like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code";
}
elseif ($search_string2==7){
		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE punch_card_no like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code";
}

$sql_result = mysql_db_query($DB, $sql) or die(mysql_error()."<br />".$sql);

while ($selectResult = mysql_fetch_array($sql_result)){
?>
				<tr style="text-decoration:none; cursor:pointer" onclick='js_set_value("<? echo "$selectResult[emp_code]"; ?>"); parent.emailwindow.hide();'> 
                    <td width="35"><?php echo $i; ?></td>	
					<td width="80"><?php echo $selectResult[emp_code]; ?> 
                    <td width="70"><?php echo $selectResult[id_card_no]; ?> 	
					<td width="85"><? echo $selectResult[punch_card_no]; ?></td>
                    <td width="130"><?php echo $selectResult[name]; ?> </td>	
					<td width="130"><? echo $designation_chart[$selectResult[designation_id]]; ?></td>
					<td width="120"><?php echo $company_details[$selectResult[company_id]]; ?></td>	
					<td width="90"><?php echo $division_details[$selectResult[division_id]]; ?></td>	
					<td width="90"><?php echo $department_details[$selectResult[department_id]]; ?></td>	
					<td><?php echo $section_details[$selectResult[section_id]]; ?></td>	
				</tr>
				<?php
				$i++;
				}
				?>
		</table>
	</div>
 </div>
<?
}

if ($type==2)
{
	$curr_month=date("Y-m-d");
	$prev_month=date("Y-m-d"."%",strtotime("-2 Months"));
		$sql= "select * from hrm_movement_register where emp_code='$search_string' and from_date>='".$prev_month."' and is_deleted=0  and status_active=1  and is_locked=0 order by move_current_date desc";
		//echo $sql;die;
		$sql_result = mysql_db_query($DB, $sql) or die(mysql_error()."<br />".$sql);
		$num_rows = mysql_num_rows($sql_result);
		$i=1;
		if($num_rows>0)
		{
?>
<div id="div_movment_view">
    <div style="width:850px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
				<thead>
					<th width="40" align="left"><strong>SL No</strong></th>
					<th width="80" align="left"><strong>Emp Code</strong></th>
                    <th width="130" align="left"><strong>Emp Name</strong></th>
					<th width="80" align="left"><strong>From Date</strong></th>
					<th width="80" align="left"><strong>To Date</strong></th>
                    <th width="150" align="left"><strong>Movement Purpose</strong></th>
					<th width="80" align="left"><strong>Destination</strong></th>
					<th width="80" align="left"><strong>Start Time</strong></th>
                    <th align="left"><strong>End Time</strong></th>
				</thead>
		</table>
	</div>	
	<div style="width:850px; overflow-y:scroll; min-height:50px; max-height:250px;" id="movment_list_view" align="left">
    	<table  cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		
	<? 
	
		while ($selectResult = mysql_fetch_array($sql_result))
		{

	?>
		
		<tr style="text-decoration:none; cursor:pointer" onclick="emp_search_movement('<? echo $selectResult['id']; ?>',3)">
			<td width="40"><? echo $i; ?></td>
			<td width="80"><? echo $selectResult[emp_code];?></td>
			<td width="130">
				<?		$d="CONCAT(first_name, ' ',middle_name, ' ',last_name) AS name";
						$dd="hrm_employee";
						$ddd="emp_code='$selectResult[emp_code]'";
						
						$old_mod= return_field_value($d,$dd,$ddd);
					echo $old_mod;?>
			</td>
			<td width="80"><? echo convert_to_mysql_date($selectResult[from_date]); ?></td>
			<td width="80"><? echo convert_to_mysql_date($selectResult[to_date]); ?></td>
			<td width="150"><? echo $selectResult[movement_purpose]; ?></td>
			<td width="80"><? echo $selectResult[destination_place]; ?></td>
			<td width="80"><? echo $selectResult[start_time]; ?></td>
			<td><? echo $selectResult[end_time]; ?></td>
		</tr>
		<? $i++;} ?>
	</table>
	</div>
</div>	
		<?
	}
}


//-----------------------Manual Duty Roster Search list------------------------------------------//
if ($type=="search_list_duty_roster"){
	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
	//echo $company_id."su..";die;
	//echo ("cat=".$category.", com=".$company_id.", loc=".$location_id.", div=".$division_id.", dep=".$department_id.", sec=".$section_id.",sub=".$subsection_id.", des=".$designation_id.", fdt=".$from_date.", tdt=".$to_date.", gp=".$group_by_id.", or=".$order_by_id.", id=".$id_card.", emp=".$emp_code ); die;
	
	if ($category=='') 					$category=""; 				else $category="and e.category='$category'";
	if ($company_id==0) 				$company_id=""; 			else $company_id="and e.company_id='$company_id'";
	if ($location_id==0) 				$location_id=""; 			else $location_id="and e.location_id='$location_id'";
	if ($division_id==0) 				$division_id=""; 			else $division_id="and e.division_id in('$division_id')";
	if ($department_id==0) 				$department_id=""; 			else $department_id="and e.department_id in('$department_id')";
	if ($section_id==0) 				$section_id=""; 			else $section_id="and e.section_id in('$section_id')";
	if ($subsection_id==0) 				$subsection_id="";	 		else $subsection_id="and e.subsection_id in('$subsection_id')";
	if ($designation_id==0)				$designation_id=""; 		else $designation_id="and e.designation_id in('$designation_id')";
	if ($from_date==0 || $to_date==0) 	$roster_date=""; 			else $roster_date="and r.shift_date BETWEEN '".convert_to_mysql_date($from_date)."' and '".convert_to_mysql_date($to_date)."'";
	if ($id_card=='') 					$id_card_no="";				else $id_card_no="and e.id_card_no in($id_card_no)";
	if ($emp_code=='') 					$emp_code=""; 				else $emp_code="and e.emp_code in('$emp_code')";
	if ($cbo_attn_policy==0) 			$shift_policy=""; 			else $shift_policy="and r.current_shift ='$cbo_attn_policy'";	
	
	/*	
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and emp.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($temp_id==0) $temp_name=""; else $temp_name="and emp.".$temp_name."='$temp_id'";
	if ($txt_from_date==0 || $txt_to_date==0) $roster_date=""; else $roster_date="and ros.shift_date BETWEEN '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."'";
	if ($cbo_emp_category=='') $category=""; else $category="and emp.category='$cbo_emp_category'";
	*/
	?>
    <div align="left">
        <table cellspacing="0" cellpadding="0" width="1280" class="rpt_table" >
            <thead>	
                <th align="center" width="30"><input type="checkbox" name="chk_all" id="chk_all" value="0" onclick="checkAll( this.value )" /></th>
                <th width="100" >Emp Code</th>
                <th width="200">ID Card</th>
                <th width="150">Employee Name</th>
                <th width="100">Designation</th>
                <th width="100">Section</th>
                <th width="100">Shift Date</th>
                <th width="100">Current Shift</th> 	
                <th width="100">Prev Shift</th>
                <th width="100">Roster Policy</th>
                <th width="100">OT Policy</th>
                <th width="100">Remarks</th>  
            </thead>
        </table>
    </div>        
    <div style="overflow-y:scroll; max-height:180px;" align="left">
        <table cellspacing="0" cellpadding="0" width="1280" class="rpt_table" >
            <?php	
            //Shift policy
            /*
            $sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0";
            $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
            
            $shift_details = array();
            while( $row = mysql_fetch_assoc( $result ) ) {
            $shift_details[$row['id']] = $row['shift_name'];
            }
            //Roster policy
            $sql = "SELECT * FROM lib_duty_roster_policy WHERE is_deleted = 0 group by roster_id";
            $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
            
            $roster_details = array();
            while( $row = mysql_fetch_assoc( $result ) ) {
            $roster_details[$row['roster_id']] = $row['roster_rule_name'];
            }
            //overtime policy
            $sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0";
            $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
            
            $overtime_details = array();
            while( $row = mysql_fetch_assoc( $result ) ) {
            $overtime_details[$row['id']] = $row['policy_name'];
            }
            */
            
            $sql="select CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS name, e.punch_card_no,e.id_card_no,e.designation_id,e.section_id, r.* 
              from hrm_duty_roster_process r, hrm_employee e 
              where 
              e.status_active=1 and 
              e.is_deleted=0 and 
              r.emp_code=e.emp_code 
              $category		   
              $company_id 
              $location_id 
              $division_id
              $department_id		   
              $section_id 
              $subsection_id 
              $designation_id
              $roster_date 
              $id_card_no
              $emp_code
              $shift_policy";
              
            //echo $sql;die; 
            $result = mysql_db_query( $DB, $sql ) or die( $sql . "<br />" . mysql_error() );
            $i=0;
            while( $row = mysql_fetch_assoc( $result ) ) 
            {
                //echo $shift_details[1]." su..";die;
                $i++;
                if ($i%2==0) $bgcolor="#E9F3FF";
                else $bgcolor="#FFFFFF";
                
                $j=0;
                ?>
                <tr style="text-decoration:none" bgcolor="<?php echo "$bgcolor"; ?>"> 
                    <td align="center" id="<? echo "A".$i.$j++; ?>" width="30"><input type="checkbox" name="chk_all" id="chk_all_<?php echo $i; ?>" onclick="js_set_value( <? echo $i; ?> )" value="<?php echo $row['id']; ?>" /><input type="hidden" name="attnd_id" id="attnd_id_<? echo $i; ?>" value="<?php echo $row['id']; ?>" /></td>
                    <td id="<? echo "B".$i.$j++; ?>" width="100"><?php echo $row['emp_code']; ?></td>
                    <td id="<? echo "D".$i.$j++; ?>" width="200">
						<?php if( $row['id_card_no'] != '' ) echo $row['id_card_no']; ?>
                        <input type="hidden" id="<? echo "punch_card".$i.($j-1); ?>" value="<? echo $row['punch_card_no'];?>" />
                    </td>
                    <td id="<? echo "E".$i.$j++; ?>" width="150" title="<? echo $row['name']; ?>"><?php if( $row['name'] != '' ){ if(strlen($row['name'])>25) echo substr($row['name'],0,15)."..."; else	echo $row['name']; } ?>  </td>	
                    <td id="<? echo "F".$i.$j++; ?>" width="100" title="<? echo $designation_chart[$row['designation_id']]; ?>"><?php if( $row['designation_id'] != '0' ){ if(strlen($designation_chart[$row['designation_id']])>14) echo substr($designation_chart[$row['designation_id']],0,14)."..."; else	echo $designation_chart[$row['designation_id']]; } ?></td> 
                    <td id="<? echo "G".$i.$j++; ?>" width="100" title="<? echo $section_details[$row['section_id']]; ?>"><?php echo split_string($section_details[$row['section_id']],14); ?></td>
                    <td id="<? echo "H".$i.$j++; ?>" width="100"><?php echo $row['shift_date']; ?></td>
                    <td id="<? echo "I".$i.$j++; ?>" width="100">
                        <?php echo $shift_details[$row['current_shift']]; ?>
                    </td> 
                    <td id="<? echo "J".$i.$j++; ?>" width="100">
					<?php echo $shift_details[$row['prev_shift']]; ?>
                     <input type="hidden" id="<? echo "current_shift".$i.($j-1); ?>" value="<? echo $row['current_shift'];?>" />
                    </td>
                    <td id="<? echo "K".$i.$j++; ?>" width="100"><?php echo $roster_details[$row['roster_id']]; ?></td>
                    <td id="<? echo "K".$i.$j++; ?>" width="100"><?php echo $overtime_details[$row['overtime_policy']]; ?></td>
                    <td id="<? echo "L".$i.$j++; ?>" width="100"><?  ?></td> 	 	
                </tr>
            <?php 
            } 
            ?>
            <input type="hidden" name="total_value_i" id="total_value_i" value="<? echo $i; ?>" />
        </table>      
    </div>	
	<?
    exit();
}

if($type=="search_off_day_ot_emp"){
extract($_POST);
// Search by id_card_no
$exp_id_card=explode(",",$id_card);
foreach($exp_id_card as $card){$card_no.="'".$card."',";}
$id_card_no=substr($card_no,0,-1);
//echo $id_card_no;die;
?>
	
        
        <table cellspacing="0" border="1" rules="all" cellpadding="0" width="1350" class="rpt_table">
			<thead>				
                <th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.off_day_ot_form.chk_list)" /></th>
                <th width="100">Code</th>
                <th width="100">ID Card</th>
                <th width="140">Employee Name</th>
                <th width="130">Designation</th>
                <th width="100">Date</th>
                <th width="100">OT Hr.</th>
                <th width="130">Location</th>
                <th width="130">Division</th>
                <th width="130">Department</th>
                <th width="130">Section</th>
                <th>Subsection</th>
                
           </thead>            
		</table>
<div style="width:1350px;overflow-y:scroll; max-height:180px;" id="manual_attendence_list_container">
   
	<table cellspacing="0" cellpadding="0" width="1332" border="1" rules="all" class="rpt_table" id="table_body">
 
 <?   
	
	$i=1;
	
	$all_date='';
	$given_ot_array=array();
	for($j=1;$j<=$correct_criteria_counter;$j++)
	{ 
		$criteria="criteria".$j;
		$dateData=explode("_",$$criteria);
		if($all_date=='') $all_date="'".convert_to_mysql_date($dateData[0])."'"; else $all_date.=",'".convert_to_mysql_date($dateData[0])."'";
		//if($given_ot=='') $given_ot="$dateData[1]";else $given_ot.=",$dateData[1]"; 
		$given_ot_array[convert_to_mysql_date($dateData[0])]=$dateData[1];
	}
	//print_r($given_ot_array);die;
	
	$overtime_emp = array();
	$sql_over_time="select * from hrm_off_day_ot where ot_date in($all_date)";
	$result_overtime = mysql_query( $sql_over_time ) or die( $sql_over_time . "<br />" . mysql_error() );
	while( $row_over_ot = mysql_fetch_array( $result_overtime ) ) 
	{
		$overtime_emp[$row_over_ot[emp_code]][$row_over_ot['ot_date']]=$row_over_ot[total_ot];
	}
	//print_r($overtime_emp);die;
	
	//print_r($given_ot_array);die;
	$rpt_company=$company_id;
	if ($category=='') $category=""; else $category="and b.category='$category'";
	if ($company_id==0) $company_id=""; else $company_id="and b.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and b.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and b.division_id in('$division_id')";
	if ($department_id==0) $department_id=""; else $department_id="and b.department_id in('$department_id')";
	if ($section_id==0) $section_id=""; else $section_id="and b.section_id in('$section_id')";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and b.subsection_id in('$subsection_id')";
	if ($designation_id==0) $designation_id=""; else $designation_id="and b.designation_id in('$designation_id')";
	if ($id_card=='') $id_card_no=""; else $id_card_no="and b.id_card_no in($id_card_no)";
	if ($emp_code=='') $emp_code=""; else $emp_code="and a.emp_code in('$emp_code')";
    
	
					
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$rpt_company' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$adjust_out_time=$res['adjust_out_time'];
			$applicable_above_salary=$res['applicable_above_salary'];
			$in_out_time_format=$res['in_out_time_format']; 
			$adjust_in_time = $res['adjust_in_time'];
		}
	
	
	
	//and a.is_regular_day=0
	$sql = "SELECT CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name,
			b.id_card_no,b.emp_code,b.designation_id,b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,
			a.attnd_date,a.sign_in_time,a.sign_out_time,a.total_over_time_min
			FROM 
			hrm_attendance a, hrm_employee b
			WHERE 
			a.attnd_date in($all_date)
			and a.total_over_time_min!=0
			and  a.status in('GH','FH','CH','W')
			and a.emp_code=b.emp_code 
			and b.status_active=1
			$category
			$emp_code
			$id_card_no
			$department_id 
			$location_id 
			$division_id 
			$section_id 
			$company_id  
			$subsection_id
			order by attnd_date"; 
			//echo $sql;die;
			//echo $sql;die;group by $dynamic_groupby $orderby";and  a.status='W'
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
	{ 
		
		if ($i%2==0) $bgcolor="#EEEEEE"; 
		else $bgcolor="#FFFFFF";
		
		
		$given_ot=$given_ot_array[$row_emp[attnd_date]];
		$overtime=get_buyer_ot_hr($row_emp[total_over_time_min],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440) ;							
									
			if($given_ot=="" || $given_ot==0)
			{
				
				$show_overtime=$overtime;
				
			}
			else
			{
				/*//check given ot is greater or smaller then actual overtime
				if($given_ot > $overtime){
					
					$show_overtime=$overtime;
					
				}else{*/
						
					$show_overtime=$given_ot;
					
				/*}*/
				
			}
									
									
	?>
	
   			 <tr bgcolor="<? echo $bgcolor;?>">
					<td  width="20"><input type="checkbox"  id="chk_list" name="chk_list[]" value="<?php echo $row_emp['emp_code']; ?>,<?php echo $row_emp['attnd_date']; ?>" onclick="count_emp(document.off_day_ot_form.chk_list)" /></td>
                    <td width="100"><? echo $row_emp[emp_code]; ?></td>
					<td width="100"><? echo $row_emp[id_card_no];?></td>
					<td width="140"><? echo $row_emp[name];?></td>
					<td width="130"><? echo $designation_chart[$row_emp['designation_id']];?></td>
                    <td width="100"><? echo convert_to_mysql_date($row_emp[attnd_date]); ?></td>
                   <? if($overtime_emp[$row_emp[emp_code]][$row_emp['attnd_date']]==""){
				   ?>
                    <td align="center" width="100">
                    	<input type="text" name="over_time_hr[]" id="over_time_hr_<?php echo $i; ?>" value="<?php 
							echo $show_overtime; 
						?>" onKeyPress="return numbersonly(this, event);"  style="width:60px;" class="text_boxes_numeric" 
                        onBlur="fnc_validate_overtime(this.value,'<?php echo $show_overtime; ?>','over_time_hr_<?php echo $i; ?>');" />
                   </td>
                   <? }else{
				   ?>
				   <td align="center" width="100">
                    	<input type="text" name="over_time_hr[]" id="over_time_hr_<?php echo $i; ?>" value="<?php 
							echo $overtime_emp[$row_emp[emp_code]][$row_emp['attnd_date']]; 
						?>" onKeyPress="return numbersonly(this, event);"  style="width:60px; background-color:#EFA1D9" class="text_boxes_numeric" 
                        onBlur="fnc_validate_overtime(this.value,'<?php echo $overtime;//$overtime_emp[$row_emp[emp_code]][$row_emp['attnd_date']]; ?>','over_time_hr_<?php echo $i; ?>');"/>
                   </td>
				   <?
				   }
				   ?>
                    <td width="130"><?php echo $location_details[$row_emp[location_id]]; ?></td>
                    <td width="130"><?  echo $division_details[$row_emp[division_id]]; ?> </td>
                    <td width="130" ><? echo $department_details[$row_emp[department_id]]; ?> </td>
                    <td width="130"><? echo $section_details[$row_emp[section_id]]; ?> </td>
                    <td><? echo $subsection_details[$row_emp['subsection_id']]; ?> </td>
				</tr>


<?
$i++;

}//end while
    
  ?> 
	</table>

</div>
<?

	exit();
}








function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>