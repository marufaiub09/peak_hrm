function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

function fnc_movement_register(save_perm,edit_perm,delete_perm,approve_perm) {
	var txt_emp_code 			= escape(document.getElementById('txt_emp_code').value);	
	var txt_from_date 			= escape(document.getElementById('txt_from_date').value);
	var txt_to_date 			= escape(document.getElementById('txt_to_date').value);
	var txt_movement_purpose 	= escape(document.getElementById('txt_movement_purpose').value);
	var txt_destination 		= escape(document.getElementById('txt_destination').value);
	var txt_start_time 			= escape(document.getElementById('txt_start_time').value);
	var txt_return_time 		= escape(document.getElementById('txt_return_time').value);
	var txt_adviced_by 			= escape(document.getElementById('txt_adviced_by').value);
	var txt_entry_number		=escape(document.getElementById('txt_entry_number').value);
	var save_up 				= escape(document.getElementById('save_up').value);
	//for update	
	var hidden_from_date 	= escape(document.getElementById('hidden_from_date').value);
	var hidden_to_date 	= escape(document.getElementById('hidden_to_date').value);
	var cbo_ignor_early_out=document.getElementById('cbo_ignor_early_out').value;
	
	
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	
	if( save_up=="" && save_perm==2){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up!="" && edit_perm==2){						
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
				$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
			});		
		}
		
	else if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Information').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_from_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select Occurance Date').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_to_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_to_date').focus();
			$(this).html('Please Select Occurance Date').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	else{					
		nocache = Math.random();
		//alert(nocache);
		http.open('get','includes/save_update_attendance.php?action=movement_register&isupdate='+save_up+
					'&txt_emp_code='+txt_emp_code+
					'&txt_from_date='+txt_from_date+
					'&txt_to_date='+txt_to_date+
					'&txt_movement_purpose='+txt_movement_purpose+
					'&txt_destination='+txt_destination+
					'&txt_start_time='+txt_start_time+
					'&txt_return_time='+txt_return_time+
					'&txt_adviced_by='+txt_adviced_by+
					'&hidden_from_date='+hidden_from_date+
					'&hidden_to_date='+hidden_to_date+
					'&txt_entry_number='+txt_entry_number+
					'&cbo_ignor_early_out='+cbo_ignor_early_out);
		http.onreadystatechange = movement_information_reply;
		http.send(null); 
	}
}

function movement_information_reply() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');		
		//alert(http.responseText);
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Data, Please Check.').addClass('messageboxerror').fadeTo(900,1);				 
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully and duplicate date '+response[2]).addClass('messagebox_ok').fadeTo(900,1);
				if(showResult_search($("#txt_emp_code").val(),"s",2,"movement_list_container"))
				{
					save_activities_history('',"hrm","movment_register","update","../../../");
				}
				document.getElementById('txt_entry_number').value=response[1];
				document.getElementById('save_up').value="";			
				//refresh('move_tbl_id'); //clear the employee info
				
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully and duplicate date '+response[2]).addClass('messagebox_ok').fadeTo(900,1);				
				/*if(showResult_search($("#txt_emp_code").val(),"s",2,"movement_list_container"))
				{
					save_activities_history('',"hrm","movment_register","insert","../../../");
				}*/
				showResult_search($("#txt_emp_code").val(),"s",2,"movement_list_container");
				document.getElementById('txt_entry_number').value=response[1];
				document.getElementById('save_up').value="";			
				//refresh('move_tbl_id'); //clear the employee info
				
			});
			
		}
	}
}

function fnc_weekend(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false; var data = ''; var employee_counter = 0; var employees = '';
	
	if(save_perm==2){
		error = true;		
		$(this).html('You Do Not Have Save Permission.').addClass('messageboxerror').fadeTo(900,1);
	}	
	if( $('#weekend_id').val() == '' ) {
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#weekend').focus();
			$(this).html('Please select at least one weekend').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&weekends=' + $('#weekend_id').val();
	
	if( error == false ) {
		$('#tbl_employees tbody input[type="checkbox"]:checked').each(function() {
			employee_counter++;
			employees += $(this).val() + '|';
		});
		
		if( employee_counter == 0 ) {
			error = true;
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Please select at least one employee').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if(employee_counter > 500)
			{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Too Many Employee You Are Selected!!!Your Limit 500.').addClass('messageboxerror').fadeTo(900,1);
			});	
			}
		else {
			employees = employees.substr( 0, employees.length - 1 );
			data += '&employees=' + employees;
		}
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'POST', 'includes/save_update_attendance.php?action=weekend' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_weekend;
		http.send(null);
	}
}

function response_weekend() {		//Weekend Response ( Save / Update )
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data saved successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}

function fnc_holiday(save_perm,edit_perm,delete_perm,approve_perm) {			//Holiday Response ( Save / Update )
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false; var data = ''; var holiday_counter = 0; var holidays = '';
	
	if (save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if( $('#company_id').val() == '' ) {
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#company').focus();
			$(this).html('Please select at least one company').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&company=' + $('#company_id').val();
	
	if( error == false ) {
		var rowCount = $('#tbl_holiday tbody tr').length;
		for(var i=1;i<=rowCount;i++)
		{
			holiday_counter++;
			if($('#from_date_'+i).val()=='' || $('#to_date_'+i).val()=='' || $('#holiday_name_'+i).val()=='' || ( $('#type_'+i).val()==1 && $('#compensate_for_'+i).val()=='' ) )
			{
				$("#messagebox").fadeTo( 200, 0.1, function() {					
					$(this).html('Please Check Your Information.').addClass('messageboxerror').fadeTo(1100,1);
				});
				return;
			}
			else
			{
				holidays += escape( $('#from_date_'+i).val() )+"*"+escape( $('#to_date_'+i).val() )+"*"+escape( $('#holiday_name_'+i).val() )+"*"+escape( $('#type_'+i).val() )+"*"+escape( $('#compensate_for_'+i).val() )+ '|';
			}
		}
		holidays = holidays.substr( 0, holidays.length - 1 );
						
		if( holiday_counter == 0 ) {
			error = true;
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#tbl_holiday tbody tr:first').find('input[name="from_date[]"]').focus();
				$(this).html('Please provide at least one correct holiday ( From date, To date and Holiday Name are mandatory )').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else {
			holidays = holidays.substr( 0, holidays.length - 1 );
			data += '&holidays=' + holidays;
		}
	}
		
	if( error == false ) {
		
		nocache = Math.random();
		http.open( 'POST', 'includes/save_update_attendance.php?action=holiday' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_holiday;
		http.send(null);
					
	}
}


function response_holiday() {		//Holiday Response ( Save / Update )
	if(http.readyState == 4) {		
		if( http.responseText == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data saved successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"hrm","define_holiday","insert","../../../");
				$(this).fadeOut(5000);
			});
		}		
		else{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Problem Occured!!Data Not Save.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		
	}
}

 
//--------------------------------Manual attendence search-----------------//
	function fn_search_roster_process(){
		
		var temp_id=0;
		var temp_name=0;
		
		var category 			= $('#cbo_emp_category').val();
		var company_id 			= $('#cbo_company_id').val();
		var location_id 		= $('#location_id').val();
		var division_id 		= $('#division_id').val();
		var department_id 		= $('#department_id').val();
		var section_id 			= $('#section_id').val();
		var subsection_id 		= $('#subsection_id').val();			
		var designation_id 		= $('#designation_id').val();
		var from_date			= $('#from_date').val();
		var to_date				= $('#to_date').val();
		var group_by_id		 	= $('#group_by_id').val();
		var order_by_id 		= $('#order_by_id').val();
		var id_card		 		= $('#id_card_no').val();
		var emp_code 			= $('#emp_code').val();
		
		//alert("cat="+category+", com="+company_id+", loc="+location_id+", div="+division_id+", dep="+department_id+", sec="+section_id+",sub="+subsection_id+", des="+designation_id+", fdt="+from_date+", tdt="+to_date+", gp="+group_by_id+", or="+order_by_id+", id="+id_card+", emp="+emp_code);
		
		
		/*var cbo_company_id=$('#cbo_company_id').val();
		var location_id=$('#location_id').val();	
		var txt_from_date=$('#txt_from_date').val();
		var txt_to_date=$('#txt_to_date').val();
		var cbo_emp_category=$('#cbo_emp_category').val();*/
		
		var cbo_status=$('#cbo_status').val();
		 
		$('#selected_id_i').val(''); 
		$('#selected_id_emp').val(''); //initialize
		selected_id = new Array ; //initialize
		selected_id_attnd = new Array ; //initialize
		
		//$("#messagebox").removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
		$("#messagebox").removeClass().addClass('messagebox').html('<img src="../../../images/loading.gif" />').fadeIn(1000);
		
			/*if($('#cbo_search_by_1').val()==1)
			{
				var section_id=$('#cbo_search_by_2_id').val();
				temp_id=section_id;
				temp_name="section_id";
			}
			else if($('#cbo_search_by_1').val()==2)
			{
				var designation_id=$('#cbo_search_by_2_id').val();
				temp_id=designation_id;
				temp_name="designation_id";
			}
			else if($('#cbo_search_by_1').val()==3)
			{
				var emp_code=$('#cbo_search_by_2_id').val();
				temp_id=emp_code;
				temp_name="emp_code";
			}
			else if($('#cbo_search_by_1').val()==4)
			{
				var emp_code=$('#cbo_search_by_2_id').val();
				temp_id=emp_code;
				temp_name="emp_code";
			}
			else if($('#cbo_search_by_1').val()==5)
			{
				var policy_shift_id=$('#cbo_search_by_2_id').val();
				temp_id=policy_shift_id;
				temp_name="policy_shift_id";
			}
			else if($('#cbo_search_by_1').val()==6)
			{
				var department_id=$('#cbo_search_by_2_id').val();
				temp_id=department_id;
				temp_name="department_id";
			}*/
		
		if(cbo_company_id==0)
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_id').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if(from_date=='')
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_from_date').focus();
				$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if( to_date=='')
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_to_date').focus();
				$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}	
		else
		{
			var data='type=search_list_duty_roster&category='+category+
			'&company_id='+company_id+
			'&location_id='+location_id+
			'&division_id='+division_id+
			'&department_id='+department_id+
			'&section_id='+section_id+
			'&subsection_id='+subsection_id+
			'&designation_id='+designation_id+
			'&from_date='+from_date+
			'&to_date='+to_date+
			'&group_by_id='+group_by_id+			
			'&order_by_id='+order_by_id+
			'&id_card='+id_card+
			'&emp_code='+emp_code+
			
			'&temp_id='+temp_id+
			'&temp_name='+temp_name;
			
			http.open('POST','includes/list_view.php',true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", data.length);
			http.setRequestHeader("Connection", "close");
			http.onreadystatechange = show_fn_search_roster_process;
			http.send(data);
		}
	}
	function show_fn_search_roster_process()
	{
		if(http.readyState == 4) {
			document.getElementById('roster_list_container').innerHTML=http.responseText;
			$("#messagebox").removeClass().addClass('messagebox').text('Generated Successfuly....').fadeIn(1000);
		}
		else if(http.readyState == 3){
			$("#messagebox").removeClass().addClass('messagebox').html('<img src="../../../images/loading.gif" />').fadeIn(1000);
		}
		else
			$("#messagebox").removeClass().addClass('messagebox').text('Problem Occured....').fadeIn(1000);
	}
	
	
	
//manual duty roster updated here
	function fn_update_roster_manual(save_perm,edit_perm,delete_perm,approve_perm)
  	{
	  //validating message	
	  $("#messagebox").removeClass().addClass('messagebox').html('<img src="../../../images/loading.gif" />').fadeIn(1000);
	  
	  if (edit_perm==2 || save_perm==2){
				$("#messagebox").removeClass().addClass('messagebox').text('You do not have save & update permission').fadeIn(1000);
	  }
	  
	  var row_id  = ($('#selected_id_i').val()).split(',');
	  var roster_id = ($('#selected_id_emp').val()).split(',');
	  var counts = row_id.length;
	  //alert(count);
	  if($('#selected_id_i').val()==''){
		  	$("#messagebox").removeClass('messagebox').addClass('messagebox').text('Validating.....Atleast 1 employee have to select!!!!').fadeIn(1000);
	  }
	 
	  var duty_roster_process; 
	  var current_shift; 
	  var remarks;
	  var emp_code;	
	  var shift_date;
	  var punch_card;
	
	  for(i=0;i<counts;i++){
		  
		if(i==0){
			emp_code				=$('#B'+row_id[i]+1).html();
			duty_roster_process 	=roster_id[i];
			current_shift			=$('#current_shift'+row_id[i]+7).val();
			shift_date				=$('#H'+row_id[i]+6).html();
			punch_card				=$('#punch_card'+row_id[i]+2).val();
			remarks					=$('#L'+row_id[i]+11).html();
		  }
		else
		{
			emp_code				+="__"+$('#B'+row_id[i]+1).html();
			duty_roster_process 	+="__"+roster_id[i];
			current_shift			+="__"+$('#current_shift'+row_id[i]+7).val();
			shift_date				+="__"+$('#H'+row_id[i]+6).html();
			punch_card				+="__"+$('#punch_card'+row_id[i]+2).val();
			remarks					+="__"+$('#L'+row_id[i]+11).html();		
		}
	}
		
		var data = 'action=update_roster_process_table&duty_roster_process='+duty_roster_process
					+'&current_shift='+current_shift
					+'&emp_code='+emp_code
					+'&shift_date='+shift_date
					+'&punch_card='+punch_card
					+'&remarks='+remarks
					+'&counts='+counts;
		
		http.open('POST','includes/manual_duty_roster.php',true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", data.length);
		http.setRequestHeader("Connection", "close");
		http.onreadystatechange = response_fn_update_roster_manual;
		http.send(data);

  }
 

//Manual Roster Process Response ( Update )
function response_fn_update_roster_manual() {
	if(http.readyState == 4) {
		var response = http.responseText.split('###');	
		//alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				save_activities_history('','hrm','manual_roster_process','update','../../../');
			});			
		}
		else {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Manual Roster Process Problem Occured.' ).addClass('messagebox_ok').fadeTo(900,1);
			});
		}		
	}
}



function fnc_holiday_allowance(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false; var data = ''; var employee_counter = 0; var employees = '';
	
	if(save_perm==2){
		error = true;		
		$(this).html('You Do Not Have Save Permission.').addClass('messageboxerror').fadeTo(900,1);
	}	
	if( $('#weekend_id').val() == '' ) {
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#weekend').focus();
			$(this).html('Please select at least one weekend').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&weekends=' + $('#weekend_id').val();
		data += '&honame=' + $('#ho_name').val();
		data += '&hotype=' + $('#hidden_type').val();
		data += '&compfor=' + $('#comp_for').val();
	//alert (data);
	if( error == false ) {
		$('#tbl_employees tbody input[type="checkbox"]:checked').each(function() {
			employee_counter++;
			employees += $(this).val() + '|';
		});
		
		if( employee_counter == 0 ) {
			error = true;
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Please select at least one employee').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if(employee_counter > 500)
			{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Too Many Employee You Are Selected!!!Your Limit 500.').addClass('messageboxerror').fadeTo(900,1);
			});	
			}
		else {
			employees = employees.substr( 0, employees.length - 1 );
			data += '&employees=' + employees;
		}
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'POST', 'includes/save_update_attendance.php?action=save_holiday_allowance' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_holiday_allowance;
		http.send(null);
	}
}

function response_holiday_allowance() {		//Weekend Response ( Save / Update )
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data saved successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
	