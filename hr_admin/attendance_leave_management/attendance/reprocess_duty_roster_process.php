<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//-------------------------------------------------------------------------------------------------------------------- 

include('../../../includes/common.php');
include('../../../includes/array_function.php');

if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
//company_details
$sql = "SELECT * FROM lib_company WHERE status_active=1 and is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Duty Roster Process</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>

    
    
<script language="javascript">
	var save_perm = <? echo $permission[0]; ?>;
	var edit_perm = <? echo $permission[1]; ?>;
	var delete_perm = <? echo $permission[2]; ?>;
	var approve_perm = <? echo $permission[3]; ?>;
	
	
$(document).ready(function(e) {

	$( ".datepicker" ).datepicker({
	dateFormat: 'dd-mm-yy',
	changeMonth: true,
	changeYear: true
});

});

function reset_button()
{
	document.getElementById('save_up').value="";
}


function fnc_duty_roster_process(save_perm,edit_perm,delete_perm,approve_perm)
{
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_roster_rule 	= escape(document.getElementById('cbo_roster_rule').value);
	var from_date 			= escape(document.getElementById('from_date').value);
	var to_date 			= escape(document.getElementById('to_date').value);
	var emp_code 			= escape(document.getElementById('txt_emp_code').value);
	var data;
	
	//$("#messagebox").removeClass().addClass('messagebox').text('Roster Processing....').fadeIn(1000);
	$("#messagebox").removeClass().addClass('messagebox').html('<img src="../../../images/loading.gif" />').fadeIn(1000);
	
	if (save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if( cbo_company_name == 0 ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#cbo_company_name').focus();
			$(this).html('Please select at least one company').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( cbo_roster_rule == 0 ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#cbo_roster_rule').focus();
			$(this).html('Please select one Roster Policy').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( from_date == "" ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#from_date').focus();
			$(this).html('Please select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( to_date == "" ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#to_date').focus();
			$(this).html('Please select To Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else
	{
		data=cbo_company_name+","+cbo_roster_rule+","+from_date+","+to_date;
		nocache = Math.random();
		http.open( 'GET', 'includes/roster_process.php?action=roster_process&data=' + data + '&emp_code='+emp_code+'&nocache=' + nocache );
		http.onreadystatechange = response_roster_process;
		http.send(null);
	}
}

function response_roster_process() {		//Holiday Response ( Save / Update )
	if(http.readyState == 4) {
		var response = http.responseText;	
	 	//alert(http.responseText);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Roster Process Completed successfully.').addClass('messagebox').fadeTo(900,1);
				save_activities_history('',"hrm","duty_roster_process","insert","../../../");
				$(this).fadeOut(5000);
			});
		}
		
		else if (response==4)
		{
			
			alert('The Period You Selected Is Locked......');
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('The Period You Selected Is Locked.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if (response==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Database is Busy, please try after some time.').addClass('messagebox_ok').fadeTo(900,1);
			});
		}
	}
}



function openpage_searchemp(page_link,title)
		{		
 			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php', 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../') 
			//emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php', title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#txt_emp_code').val(thee_id.value);	
			}
		}



function generate_list_view(selected_id){
		
		$.ajax({
				type: "GET",
				url: "../../includes/get_data_update.php?",
				data: 'type=list_view&period=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});
		}	
</script>

</head>

<body style="font-family:verdana; font-size:11px;">

	<div  style="width:900px;">
    
  	<div style="width:550px;float:left">
         <div>
            <div class="form_caption"> Duty Roster Re-Process</div>
            <div align="center"><span id="permission_caption"><? echo "Your Permissions--> \n".$insert;  ?> </span></div>
           <div id="messagebox" style="background-color:#FF9999; color:#000000; width:550px" align="center"></div>
		</div>
		<!-- Start Field Set -->
		<fieldset style="width:550px ">
			<legend>Duty Roster Process</legend>
			<!-- Start Form -->
			<form name="duty_roster_policy" id="duty_roster_policy" method="" autocomplete="off" action="javascript:fnc_duty_roster_process(save_perm,edit_perm,delete_perm,approve_perm)">
				<fieldset>
					<div style="width:550px; overflow:auto" >
						<table width="100%" border="0">
                            <tr>
                                <td width="130" >
                                    Company
                                </td>
                                <td width="130"> 
                                 <select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:150px">
                                 	<?
									if($company_cond=="")
									{
									?>
                                     <option value='0'>Select</option>
									 
                                     <?php
									}
                                     while($row=mysql_fetch_assoc($result))
                                     {?>
                                      <option value="<?php echo $row['id']; ?>"><?php echo mysql_real_escape_string($row['company_name']); ?></option>
                                 <?php } ?>
                                 </select>
                                </td>
								<td width="130">Roster Rule Name</td>
								<td width="130">
                                	<select name="cbo_roster_rule" id="cbo_roster_rule" class="combo_boxes" style="width:150px">
                                 		<option value='0'>Select</option>
                                        <?php
											$sql=mysql_db_query($DB,"select roster_rule_name,roster_id from lib_duty_roster_policy where status_active=1 and is_deleted=0 group by roster_rule_name");
											while($row=mysql_fetch_assoc($sql))
											 {?>
											  <option value="<?php echo $row['roster_id']; ?>"><?php echo mysql_real_escape_string($row['roster_rule_name']); ?></option>
										 <?php } ?>
                                    </select>
								</td>
                              </tr>
                              <tr>
								<td width="130">Roster Period</td>
                              	<td><input type="text" name="from_date" id="from_date" value="" class="datepicker" style="width:140px" placeholder="From Date" onchange="generate_list_view(this.value)"/>
								</td>
							   <td width="130">To</td>
								<td width="130"><input type="text" name="to_date" id="to_date" value="" class="datepicker" style="width:140px" placeholder="To Date"/>
										
								</td>
                              </tr>
                              <tr>
                                <td>Employee Code</td>
                                <td colspan="2"><input name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:240px " placeholder="Double Click To Search" readonly ondblclick="openpage_searchemp()" ></td>
                                <td>&nbsp;</td>
                              </tr>
                                </tr>
                                	<td height="15px"><input type="hidden" name="save_up" id="save_up" ></td>
                                
                            	<tr>
                                    <td colspan="4" align="center" class="button_container">
                                        <input type="submit" value="Process" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton" onClick="reset_button()"/>	
                                    </td>				
                            	</tr>
						</table>
					</div>
				</fieldset>
				<div style="height:10px;"></div>
				<fieldset>
					<div style="width:550px;" >
						<table cellspacing="1" width="100%">
                            <tr>
                                <td>
                                    <div id="duty_roster">
                                    </div>
                                </td>
                            </tr>		
						</table>
					</div>
				</fieldset>
				
			</form>
			<!-- End Form -->
		</fieldset>
		<!-- End Field Set -->
	</div>
    
     <div id="data_panel"  class="demo_jui" style="margin-top:40px; width:300px; float:right"></div>
  </div> 
</body>
</html>


