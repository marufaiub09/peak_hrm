<?php
 
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script src="../../../js/popup_window.js" type="text/javascript"></script>
	<script src="../../../js/modal.js" type="text/javascript"></script>
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	
    <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>
    
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			get_datepicker();
		});
		
		function get_datepicker() {
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		}
		//var rowCount = $('#myTable tr').length;
		function add_holiday() {
			var rowCount = $('#tbl_holiday tbody tr').length+1;			
			$('#tbl_holiday tbody').append(
				'<tr>'
					+ '<td><input type="text" name="from_date[]" id="from_date_'+rowCount+'" class="datepicker" value="" /></td>'
					+ '<td><input type="text" name="to_date[]" id="to_date_'+rowCount+'" class="datepicker" value="" /></td>'
					+ '<td><input type="text" name="holiday_name[]" id="holiday_name_'+rowCount+'" class="text_boxes" value="" style="width:270px;" /></td>'
					+ '<td>'
						+ '<select name="type[]" id="type_'+rowCount+'" class="combo_boxes">'
							+ '<option value="0">Holiday</option>'
							+ '<option value="1">Compensatory</option>'
						+ '</select>'
					+ '</td>'
					+ '<td><input type="text" name="compensate_for[]" id="compensate_for_'+rowCount+'" class="datepicker" value="" /></td>'
				+ '</tr>'
			);
			get_datepicker();
		}
		
		function filter_holidays( companies ) {
			$.ajax({
				type: 'POST',
				url: 'filtered_holidays.php',
				data: 'companies=' + companies,
				success: function( filtered_holidays ) {
					if( filtered_holidays.length > 0 ) {
						$('#container_div').html( filtered_holidays );
						get_datepicker();
					}
					else {
						$('#container_div').html( '' );						
						$(this).html('No holidays defined for the selected companies.').addClass('messageboxerror').fadeTo(900,1);
					}
				}
			});
		}
		
		function company_multiselect( page_link, title ) {
			emailwindow = dhtmlmodal.open( 'EmailBox', 'iframe', page_link, title, 'width=560px,height=400px,center=1,resize=0,scrolling=0', '../../' );
			
			emailwindow.onclose = function() {
				document.getElementById( 'company' ).value = this.contentDoc.getElementById("txt_selected").value;
				document.getElementById( 'company_id' ).value = this.contentDoc.getElementById("txt_selected_id").value;
				filter_holidays( this.contentDoc.getElementById("txt_selected_id").value );
			}
		}
	</script>
	<style type="text/css">
		#tbl_holiday_list tbody input[type="text"] { width:100px; }
		#tbl_holiday_list tbody select { width:112px; }
		#tbl_holiday tbody input[type="text"] { width:100px; }
		#tbl_holiday tbody select { width:112px; }
		.formbutton { width:100px; }
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center">
        <div>
            <div class="form_caption">
               Holiday
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update;
                ?>
           </span>
			<div style="width:740px" align="center" id="messagebox"></div>	
		</div>	
		<form id="holiday_form" action="javascript:fnc_holiday(save_perm,edit_perm,delete_perm,approve_perm);" autocomplete="off" method="POST">
			<fieldset style="width:742px;">
				<legend>Company</legend>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td>Company:</td>
						<td>
							<input type="text" name="company" id="company" class="text_boxes" value="" style="width:400px;" placeholder="Double click for companies" ondblclick="company_multiselect( 'company_multiselect.php', 'Select Company/(s)' ); return false;" />
							<input type="hidden" name="company_id" id="company_id" value="" />
						</td>
					</tr>
				</table>
			</fieldset>
			<fieldset style="width:742px;">
				<legend>Holiday</legend>
				<table id="tbl_holiday" style="width:100%;" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
					<thead>
						<tr>
							<th>From Date</th>
							<th>To Date</th>
							<th>Holiday Name</th>
							<th>Type</th>
							<th>Compensate for</th>
						</tr>
					</thead>
					<tbody>
						<tr class="general">
							<td><input type="text" name="from_date[]" id="from_date_1" class="datepicker" value="" /></td>
							<td><input type="text" name="to_date[]" id="to_date_1" class="datepicker" value="" /></td>
							<td><input type="text" name="holiday_name[]" id="holiday_name_1" class="text_boxes" value="" style="width:270px;" /></td>
							<td>
								<select name="type[]" id="type_1" class="combo_boxes">
									<option value="0">Holiday</option>
									<option value="1">Compensatory</option>
                                    <option value="2">Festival holiday</option>
								</select>
							</td>
							<td><input type="text" name="compensate_for[]" id="compensate_for_1" class="datepicker" value="" /></td>
						</tr>
					</tbody>
                    
					<tfoot>
                    	<tr><td style="height:10px" colspan="5"></td></tr>
						<tr class="button_container">
							<td colspan="2"><input type="button" name="add" class="formbutton" value="Add" onclick="add_holiday();" /></td>
							<td align="center">
								<input type="submit" name="submit" id="submit" class="formbutton" value="Save" />&nbsp;&nbsp;
								<input type="button" name="cancel" id="cancel" class="formbutton" value="Cancel" />
							</td>
							<td colspan="2">&nbsp;</td>
						</tr>
					</tfoot>
				</table>
                <div id="container_div"></div>
			</fieldset>
		</form>
	</div>
</body>
</html>