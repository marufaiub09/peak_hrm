<?
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 05-04-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include("../../../includes/array_function.php");

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0  ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script src="../../../js/popup_window.js" type="text/javascript"></script>
        <script src="../../../js/modal.js" type="text/javascript"></script>
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
        
         <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
  <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    
        <script type="text/javascript" charset="utf-8">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
            
            
            function populate_data() {
                $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                var category 			= $('#cbo_emp_category').val();
                var company_id 			= $('#cbo_company_id').val();
                var location_id 		= $('#location_id').val();
                var division_id 		= $('#division_id').val();
                var department_id 		= $('#department_id').val();
                var section_id 			= $('#section_id').val();
                var subsection_id 		= $('#subsection_id').val();			
                var designation_id 		= $('#designation_id').val();
                var empcode				=$('#emcode_filter').val();
                
                var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&division_id='+division_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&empcode=';
                    
                $.ajax({
                    type: "POST",
                    url: "../../employee_info_data.php?"+passdata,
                    data: 'form=weekend',
                    success: function(data) {
                        $('#data_panel').html( data );	
                        var tableFilters = {col_0: "none"}
                        setFilterGrid("tbl_employees",-1,tableFilters);				
                    }
                });
            }
            
            function weekend_multiselect( page_link, title ) {
                emailwindow = dhtmlmodal.open( 'EmailBox', 'iframe', page_link, title, 'width=560px,height=400px,center=1,resize=0,scrolling=0', '../../' );
                
                emailwindow.onclose = function() {
                    document.getElementById( 'weekend' ).value = this.contentDoc.getElementById("txt_selected").value;
                    document.getElementById( 'weekend_id' ).value = this.contentDoc.getElementById("txt_selected_id").value;
                }
            }
            
        function Check(chk)
            {
                var cnt=0;
                if(document.weekend_form.Check_ctr.checked==true){
                    for (i = 0; i < chk.length; i++){
                        chk[i].checked = true ;
                        cnt++;
                    }
                }else{			
                    for (i = 0; i < chk.length; i++)
                    chk[i].checked = false ;			
                }
                $('#count_id').html('You Are Selected '+cnt+' Employees');
            }
        function count_emp(chk){
            var cnt=0;
            for (i = 0; i < chk.length; i++){
                if(chk[i].checked) cnt++;
            }
            $('#count_id').html('You Are Selected '+cnt+' Employees');
        }
         
		 
		 
		 
		   $(document).ready(function(e) {
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
			//header: false, 
			selectedText: "# of # selected",
			});
			
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});		
        });    
        </script>
        <style type="text/css">
            .formbutton { width:100px; }
            #select_header select { width:100%; }		
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">	
        <div align="center" style="width:100%; position:relative; height:40px; margin:5px 0;">	
            <div style="height:60px;">
                <div class="form_caption">Weekend </div>
                <span id="permission_caption">
                <? echo "Your Permissions--> \n".$insert;?>		
                </span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div>
            </div>
            <form name="weekend_form"  id="weekend_form"  autocomplete="off" method="POST">
                <fieldset><legend>Weekend Entry</legend>
                    <table width="550px" border="0" cellpadding="0" cellspacing="2">
                        <tr>
                            <td>Weekend:</td>
                            <td>
                                <input type="text" name="weekend" id="weekend" class="text_boxes" value="" style="width:400px;" placeholder="Double click for weekends" ondblclick="weekend_multiselect( 'weekend_multiselect.php', 'Select Weekend/(s)' ); return false;" readonly="readonly" />
                                <input type="hidden" name="weekend_id" id="weekend_id" value="" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
               <fieldset>		
                <table class="rpt_table" style="width:1120px;" border="0" cellpadding="0" cellspacing="1">
                        <thead>			                
                            <th width="140px">Select</th>
                            <th width="140px">Company</th>
                            <th width="140px">Location</th>
                            <th width="140px">Division</th>
                            <th width="140px">Department</th>
                            <th width="140px">Section</th>
                            <th width="140px">SubSection</th>
                            <th width="140px">Designation</th> 
                            <th><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" style="width:70px;" /></th>                   			
                        </thead>
                        <tr class="general">					
                            <td>
                                 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                                    <option value="0">All</option>
                                    <option value="1">Defined</option>							
                                    <option value="2">Not Defined</option>
                                 </select>
                            </td>
                            <td>
                                <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
                                    <? if($company_cond=="")
                                       { 
                                        ?>
                                    <option value="0">-- Select --</option>
                                    <?php } foreach( $company_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?> ><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>
                                <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                                  <option value="0">-- Select --</option>
                                    <?php foreach( $location_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                              </select>
                          </td>
                            <td>
                                <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                 <!-- <option value="0">-- Select --</option>-->
                                    <?php foreach( $division_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                              </select>
                          </td>
                            <td>
                                <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                    <!--<option value="0">-- Select --</option>-->
                                    <?php foreach( $department_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>
                                <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                   <!-- <option value="0">-- Select --</option>-->
                                    <?php foreach( $section_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td id="subsection">
                                <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                   <!-- <option value="0">-- Select --</option>-->
                                    <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </td> 
                            <td id="designation">
                                <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                   <!-- <option value="0">-- Select --</option>-->
                                    <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </td> 
                            <td><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()" style="width:70px;" /></td>                           
                        </tr> 
                    </table>
                </fieldset>        
                <fieldset>        	
                    <span id="count_id" style="font-weight:bold">&nbsp;</span>
                    <div id="data_panel" style="margin-top:10px;" align="center"></div>        
                    <div style="margin-top:10px; text-align:center;">
                        <input type="button" name="save" class="formbutton" value="Save" onclick="javascript:fnc_weekend(save_perm,edit_perm,delete_perm,approve_perm);" />
                    </div>
               </fieldset>     
            </form>
            </div>  
    </body>
</html>