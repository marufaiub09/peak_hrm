<?
/*######################################
	Completed By
	Name : Ekram
	Date : 9-12-2013
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include("../../../includes/array_function.php");

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0  ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
	
	 
	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
    <script src="includes/functions.js" type="text/javascript"></script>
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
    <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>
    <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	  <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});			
			
		});			
	
	
	
	$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});	
	
 });		
	

//ekram
/*function openmypage_employee_info(page_link,title)
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#emp_code').val(thee_id.value);
			}
		}*/	
	function openmypage_employee_info(page_link,title)
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}	
function openmypage_employee_info_id_card(page_link,title)
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#id_card').val(thee_id.value);
			}
		}	
		
	/*function fn_frm_refresh(){
		window.location.reload(true);		
	}*/
	
	function Check(chk)
		{
			var cnt=0;
			//alert(chk);
			if(document.off_day_ot_form.Check_ctr.checked==true){
				for (i = 0; i < chk.length; i++){
					chk[i].checked = true ;
					cnt++;
				}
			}else{			
				for (i = 0; i < chk.length; i++)
				chk[i].checked = false ;			
			}
			$('#count_id').html('You Are Selected '+cnt+' Employees');
			
		}
		
	function count_emp(chk){
		var cnt=0;
		$('#table_body tbody input[type="checkbox"]:checked').each(function() {
				cnt++;
			});
			document.getElementById('cnt_val').value=cnt;
			//alert(cnt);
		$('#count_id').html('You Are Selected '+cnt+' Employees');
	}
	
	



		
//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
	
	

var line_counter = 1;
function add_rows() 
{

var rowCount = $('#ot_increment_table  tbody tr').length;


			line_counter=rowCount;
			line_counter++;
			//alert(line_counter);
			$('#ot_increment_table > tbody:last').append(
				'<tr id="myTableRow">'
					+ '<td><input type="text" name="search_date[]" id="search_date_' + line_counter + '"  class="datepicker"  style="width:100px" onchange="date_validate( this.value,'+line_counter+')"/></td>'
					+ '<td><input type="text" name="txt_ot_hour[]" id="txt_ot_hour_' + line_counter + '" class="text_boxes" style="width:100px" onKeyPress="return numbersonly(this, event);"/></td>'+ '</tr>'
			);
			
			
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		}



//cross date and cross year validation
function date_validate( type,line_counter ) {
	
	//alert(type);
	var first_date=$('#search_date_1').val();
	var last_date=$('#search_date_' + line_counter).val();
	//alert(last_date);
	var txt_date_from_array=first_date.split("-");
	var txt_date_to_array=last_date.split("-");
	
	
	/*if(first_date==''){ 
	
		$("#messagebox").removeClass().addClass('messagebox').text('Please Select First Date  Field....').fadeIn(1000);
		$("#messagebox").removeClass().addClass('messagebox').fadeOut(5000);
		document.getElementById('first_date').value="";
		return false; 
	}*/
	
	if(txt_date_from_array[1]!=txt_date_to_array[1] || txt_date_from_array[2]!=txt_date_to_array[2]){
		alert("Cross Date is Not Allowed");
		$('#search_date_' + line_counter).val('');
	
	}
	
	
}


		
//remove payroll rowes start
function  remove_rows() {
	var rowCount = $('#ot_increment_table  tbody tr').length;
	if(rowCount!=1){
	$("#ot_increment_table tr:last").remove();

	}

}	


//remove payroll rowes end
	
	
	function populate_data() {
			$('#data_panel').html('<img src="../resources/images/loading.gif" />');
			
			correct_criteria_counter = 0;
			var category 			= $('#cbo_emp_category').val();
			var company_id 			= $('#cbo_company_id').val();
			var location_id 		= $('#location_id').val();
			var division_id 		= $('#division_id').val();
			var department_id 		= $('#department_id').val();
			var section_id 			= $('#section_id').val();
			var subsection_id 		= $('#subsection_id').val();			
			var designation_id 		= $('#designation_id').val();
			var id_card 			= $('#id_card').val();
			var emp_code 			= $('#emp_code').val();
		
			var passdata='type=search_off_day_ot_emp&category='+category+'&company_id='+company_id+'&location_id='+location_id+'&division_id='+division_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&id_card='+id_card+'&emp_code='+emp_code;
			
		$('#ot_increment_table tbody tr').each( function() {	
		correct_criteria_counter++;
		passdata += '&criteria' + correct_criteria_counter + '=' + $(this).find('input[name="search_date[]"]').val()+ '_' + $(this).find('input[name="txt_ot_hour[]"]').val();
		
	});
			passdata +='&correct_criteria_counter=' +correct_criteria_counter;
			
			//alert(passdata);
			http.open('POST','includes/list_view.php',false);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", passdata.length);
			http.setRequestHeader("Connection", "close");
			http.onreadystatechange = show_search_list_manual_attendence;
			http.send(passdata);
		}

	
	function show_search_list_manual_attendence()
	{
		if(http.readyState == 4) {
			var response = http.responseText;
			//alert(response);
			document.getElementById('manual_attendence_list_container').innerHTML=http.responseText;
			
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$(this).html('Generated Successfuly.......').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(2000);
		});	
			
			//$("#messagebox").removeClass().addClass('messagebox').text('Generated Successfuly....').fadeIn(5000);
			//$("#messagebox").removeClass().addClass('messagebox').text('Generated Successfuly....').fadeOut(3000);
			//$(this).fadeOut(5000);
   		
	}
	
}
	
		
	
    
    
   //Employee off Day ot ( Save / Update )
function fnc_of_day_ot() {

	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	var error = false, 
	counter = 0, 
	data = '&chk_values=';
	passdata=0;
	var employee_counter = 0;
	correct_criteria_counter = 0;
		
		$('#table_body tbody tr').each(function() {
			if($(this).find('input[name="chk_list[]"]').is(':checked'))
			{
				var ot_hr=$(this).find('input[name="over_time_hr[]"]').val();
				data += $(this).find('input[name="chk_list[]"]').val()+','+ot_hr+ '_';
				employee_counter++;
			}
			
		});
			
		data = data.substr( 0, data.length - 1 );
		//alert(data);		
			
		if( employee_counter == 0 ) {
				error = true;
				alert("Please Select At Least One Employee");
				$("#messagebox").removeClass().addClass('messageboxerror').text('Please select at least one employee.').fadeIn(1000);							
			}
		
		
		
		
		$('#ot_increment_table tbody tr').each( function() {	
		correct_criteria_counter++;
		passdata += '&criteria' + correct_criteria_counter + '=' + $(this).find('input[name="search_date[]"]').val()+ '_' + $(this).find('input[name="txt_ot_hour[]"]').val();
		
	});
			passdata +='&correct_criteria_counter=' +correct_criteria_counter;
			
		//alert(passdata);	
		if( error == false ){
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_hr_admin.php?action=off_day_ot'+ data +passdata+'&nocache=' + nocache );
			http.onreadystatechange = response_off_day_ot;
			http.send(null);
		}
	}


 //Employee Job Separation Response ( Save / Update )
function response_off_day_ot() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' employee/(s) has been Inserted successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			
			});
			//populate_data();
		}
		
	}
}   
    
function fnc_validate_overtime(giv_val,fld_val,fld_id){
	
	//alert(giv_val+"="+fld_val+"="+fld_id);
	
	var ginen_value=giv_val;
	var field_value=fld_val;
	//var field_id=fld_id;
	
	if(ginen_value*1 > field_value*1){
		
		alert("Given overtime must be lower than actual overtime");
		//$(fld_id).val(field_value);
		document.getElementById(fld_id).value=field_value;
		return;
	
	}


}
    
    
    </script>
	<style>
		.formbutton { width:100px; }
		#separation_form * { font-family:verdana; font-size:11px; }
	</style>
</head>
<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px">		
     <div>
    	<div class="form_caption">
		 Off-Day OT Distribution
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form name="off_day_ot_form" id="off_day_ot_form" action="" method="POST">
        <fieldset style="padding:10px;">
        <table class="rpt_table" style="width:60%;" border="0" cellpadding="0" cellspacing="2">
				<thead>			                
                	<th> Category</th>
					<th> Company</th>
					<th> Location</th>
                    <th> Division</th>
					<th> Department</th>
                    <th> Section</th>
                    <th> SubSection</th>
                    <th> Designation</th>
                    <th>ID Card No</th>
                    <th>Enter Emp Code</th>                  			
				</thead>
                <tr class="general">					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                         	 <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:120px">
                        	<? if($company_cond=="")
								   { 
									?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
                   <td>
                        <select name="division_id" id="division_id" class="combo_boxes" style="width:120px" multiple="multiple">
                          <!--<option value="0">-- Select --</option>-->
                            <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px"  multiple="multiple">
							<!--<option value="0">-- Select --</option>-->
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:120px"  multiple="multiple">
							<!--<option value="0">-- Select --</option>-->
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px"  multiple="multiple">
                       	 	<!--<option value="0">-- Select --</option>-->
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td> 
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px"  multiple="multiple">
							<!--<option value="0">-- Select --</option>-->
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td> 
                    <td>
                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                    </td>
                	<td align="center"><input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" /></td>                   
				</tr>
                <tr>
                	<td colspan="10" align="center">
                    	
                   		 &nbsp;<input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" />
                    </td>                  
                </tr> 
          </table> 
        </fieldset>
        <fieldset style="padding:10px;width:40%">
			<table width="40%" cellpadding="0" cellspacing="0" align="center" class="rpt_table" rules="all" id="ot_increment_table">
                <thead>			                
                    <th>Date</th>
                    <th>OT Hr.</th>
                    <th>
                        <input type="button" id="add_line" name="add_line" class="formbutton" style="width:100px" value="Add" onclick="add_rows();"/>
                        <input type="button" id="line_remove" name="line_remove" class="formbutton" style="width:100px" value="Remove" onclick="remove_rows();" />
                        <input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()" />
                    </th>
                </thead>
                <tbody>
                  <tr>
                  	<td><input type="text" name="search_date[]" id="search_date_1" value="" class="datepicker"  style="width:100px" onchange="date_validate(this.value,1);"
                   /></td>
                    <td><input type="text" name="txt_ot_hour[]" id="txt_ot_hour_1" value="" onKeyPress="return numbersonly(this, event);" class="text_boxes"  style="width:100px"  /></td>
                  </tr>
                </tbody>
			</table>
		</fieldset>  
		<fieldset>
        <span id="count_id" style="font-weight:bold">&nbsp;</span>
        <div id="manual_attendence_list_container" style="margin-top:4px;"></div>
	<!--	<div style="width:100%; margin-top:4px; text-align:center;">-->
        <div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px">	 
			<input type="button" name="Save" value="Save" class="formbutton" onclick="fnc_of_day_ot()" />
			&nbsp;<input  type="reset" name="reset" value="Reset" class="formbutton"  />
            </div>
            <input type="hidden" id="cnt_val" name="cnt_val" />
        </fieldset>
	</form>
    </div>
</body>
</html>