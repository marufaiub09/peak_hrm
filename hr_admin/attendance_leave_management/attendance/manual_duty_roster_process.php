<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
 
include('../../../includes/common.php');
include('../../../includes/array_function.php');


if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}




//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 order by trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$policy_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$policy_details[$row['id']] = mysql_real_escape_string($row['shift_name']);
}

	

if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<script src="includes/functions.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>

	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
 
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
    
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>
    
    
 <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
	
<style type="text/css"> 
	.style1 {font-family: Verdana, Arial, Helvetica, sans-serif} 
</style>

<script type="text/javascript" charset="utf-8">
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	
		var location_details = division_details = department_details = section_details =emp_code_details=policy_details=id_details= new Array();
		var selected_id = new Array ;
		var selected_id_attnd = new Array ;
		
	$(document).ready(function(e) {
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
		//header: false, 
		selectedText: "# of # selected",
		});
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});	
	}); 
	
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
	function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "report/hrm/hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
	function populate_value(str)
		{
			var section='<option value=0>--select--</option>>';
			if(str==1)
				{
					<?php
						foreach( $section_details AS $key => $value ) {						
							echo "section += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=section;
				}
				
			var designation='<option value=0>--select--</option>>';
			if(str==2)
				{
					<?php
						foreach( $designation_details AS $key => $value ) {						
							echo "designation += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=designation;
				}
			var emp_code='<option value=0>--select--</option>>';
			if(str==3)
				{
					<?php
						foreach( $emp_code_details AS $key => $value ) {						
							echo "emp_code += '<option value=\"$value\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=emp_code;
				}
			
			var id_card_no='<option value=0>--select--</option>>';
			if(str==4)
				{
					<?php
						foreach( $id_details AS $key => $value ) {						
							echo "id_card_no += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=id_card_no;
				}
			var policy='<option value=0>--select--</option>>';
			if(str==5)
				{
					<?php
						foreach( $policy_details AS $key => $value ) {						
							echo "policy += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=policy;
				}
				
			var department='<option value=0>--select--</option>>';
			if(str==6)
				{
					<?php
						foreach( $department_details AS $key => $value ) {						
							echo "department += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=department;
				}
		}

	function checkAll(field)
	{
		selected_id.length = 0;
		selected_id_attnd.length=0;
		$('#selected_id_i').val( '' );
		$('#selected_id_emp').val( '' );
		//alert($('#selected_id_i').val());
	 	 
							 
		//if ($('#chk_all').is(':checked'))
		if ($('#chk_all').val()==0)
		{	
			$('#chk_all').val('1');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{	
				$('#chk_all_'+i).attr('checked', 'checked');
				js_set_value( i );				
			}
		}
		else
		{
			$('#chk_all').val('0');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{
				$('#chk_all_'+i).removeAttr("checked");
				// js_set_value( i );				
			}
		}
	}
	
	function js_set_value( str ) 
	{
		if( jQuery.inArray( str, selected_id ) == -1 ) 
		{
			selected_id.push( str );
		}
		else 
		{
			for( var i = 0; i < selected_id.length; i++ ) 
			{
				if( selected_id[i] == str ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id = '';
		for( var i = 0; i < selected_id.length; i++ ) 
		{
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_i').val( id );
		
		js_set_value_attnd( str );
	}
	
	function js_set_value_attnd( str ) 
	{
		if( jQuery.inArray(  $('#attnd_id_' + str).val(), selected_id_attnd ) == -1 ) 
		{
			selected_id_attnd.push( $('#attnd_id_' + str).val() );
		}
		else 
		{
			for( var i = 0; i < selected_id_attnd.length; i++ ) 
			{
				if( selected_id_attnd[i] == $('#attnd_id_' + str).val() ) break;
			}
			selected_id_attnd.splice( i, 1 );
		}
		var id  = '';
		for( var i = 0; i < selected_id_attnd.length; i++ ) 
		{
			id += selected_id_attnd[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_emp').val( id );
	}
	
	function fn_add_values_in_html()
	{
		$("#messagebox").html('<img src="../../../images/loading.gif" />').fadeIn(1000);
		
		if ($('#selected_id_i').val()=="")
		{ 
			alert ('No Employee is Selected'); return false; 
		}			
		else
		{
			var data=$('#selected_id_i').val()+"__"+$('#selected_id_emp').val()+"__"+$('#cbo_shift_policy').val()+"__"+$('#txt_remarks').val();
			//alert($('#cbo_shift_policy').val());
			//alert(data);
			$.ajax({type: "POST",url: "includes/manual_duty_roster.php?action=manual_roster_update&data="+data,
				success: function(response){
					eval(response);
					$("#messagebox").removeClass().addClass('messagebox').text('Roster Data add Done').fadeIn(1000);
				}
			});				
					
		}
	}
	
function openmypage_employee_info()
	{			
		//alert("okss");
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}	
function openmypage_employee_info_id_card()
		{			
			//alert("okss");
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe','../../search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#id_card_no').val(thee_id.value);
			}
		} 
		
		
		
		
function generate_list_view(selected_id){
	
	var page_link="../../includes/get_data_update.php?period=";
	var id=selected_id;
	var title="Lock Status";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+id+'&type=list_view', title,'width=400px,height=300px,center=1,resize=0,scrolling=0','../../')
	
	emailwindow.onclose=function(){};		
			
		} 
    
	   
</script>

</head>

<body style="font-family:verdana; font-size:11px;">
 <div style="width:100%;" align="center">	
 
    <div style="height:58px;">
    	<!--<div class="form_caption">Temporary Shift Assignment</div>-->
        <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update;?></span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
    
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu(this.id,'content_search_panel','')">-Temporary Shift Assignment</h3>
    <form name="frm_manual_roster" id="frm_manual_roster" >		
    <fieldset>
    <div id="content_search_panel" >	
			<table class="rpt_table"  width="1000" cellspacing="0">
            	<thead>	
            		<th align="center"><strong>Category</strong></th>
                    <th align="center"><strong>Company</strong></th>
                    <th align="center"><strong>Location</strong></th>
                    <th align="center"><strong>Division</strong></th>
                    <th align="center"><strong>Department</strong></th>
                    <th align="center"><strong>Section</strong></th>
                    <th align="center"><strong>SubSection</strong></th>
                    <th align="center"><strong>Designation</strong></th>
            	</thead>
                <tr class="general">
                    <td>
                         <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:120px">
                           <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
                    </td>
                    <td>
                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:130px">
                            <? if($company_cond=="")
                            { 
                            ?>
                            <option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
                            <option value="<?php echo $company['id']; ?>" <? if(count($company_details)==1)echo"selected"; ?> ><?php echo $company['company_name']; ?></option>
                            <?php } } ?>
                        </select>
                    </td>
                    <td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:130px">
                            <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
                            <option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
                            <?php } } ?>
                        </select>
                    </td>
                    <td align="center" id="division">
                    <select name="division_id" id="division_id" class="combo_boxes" style="width:110px" multiple="multiple">
                        <!--<option value="0">-- All --</option>-->
                        <?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
                    <option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    <td align="center" id="department">
                    <select name="department_id" id="department_id" class="combo_boxes" style="width:110px" multiple="multiple">
                      <!--<option value="0">-- All --</option>-->
                        <?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
                    <option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    
                    <td align="center" id="section">
                    <select name="section_id" id="section_id" class="combo_boxes" style="width:110px" multiple="multiple">
                   <!--<option value="0">-- All --</option>-->
                        <?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
                    <option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px"  multiple="multiple">
                       	 	<option value="0">-- All --</option>
							<?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
                            <option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
                            <?php } } ?>
						</select>
					</td>
                    <td align="center" id="designation">
                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:110px" multiple="multiple">
                       <!--<option value="0">-- All --</option>-->
                        <?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
                    <option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                </tr>
          </table>
          <div style="height:8px;"></div>
          <table align="center" class="rpt_table"  width="850" cellspacing="0">
            	<thead>	
            		<th align="center"><strong>Attn Policy</strong></th>
                    <th align="center"><strong>Date</strong></th>
                    <th align="center"><strong>Group By</strong></th>
                    <th align="center"><strong>Order By</strong></th>
                    <th align="center"><strong>ID Card No</strong></th>
                    <th align="center"><strong>System Code</strong></th>				                   
                    <td>
                    	<input type="reset" name="reset" id="reset" value="Reset" class="formbutton" style="width:70px" />                   
                        <input type="hidden" name="selected_id_i" id="selected_id_i" /> <!-- search list row id-->
                        <input type="hidden" name="selected_id_emp" id="selected_id_emp" /><!-- hrm_duty_roster_process id-->
                    </td>
            	</thead>
                <tr class="general">
                    <td>
                        <select id="cbo_attn_policy" name="cbo_attn_policy" class="combo_boxes" style="width:120px">
                            <option value="0">-- select --</option>
                            <?php foreach($policy_details as $key=>$value)
                            {
                            ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php	
                            }
                            ?>
                        </select> 
                    </td> 
  <td><input type="text" name="from_date" id="from_date" value="" class="datepicker" placeholder="From" style="width:80px" onchange="generate_list_view(this.value)"/>
  -<input type="text" name="to_date" id="to_date" value="" class="datepicker" placeholder="To" style="width:80px"/> </td>
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="id_card_no" id="id_card_no" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                    </td>
                    <td>
                        <input type="text" name="emp_code" id="emp_code" class="text_boxes" style="width:100px" placeholder="Double Click To Search" autocomplete="off" ondblclick="openmypage_employee_info();"   />
                    </td>                    
                  
                  <td><input type="button" name="search" id="search" value="Search" class="formbutton" style="width:70px" onclick="javascript: fn_search_roster_process()" /></td>
              </tr>
              <tr height="10px"></tr>                
            </table>
           </div> 
            
            
                <fieldset style="width:500px;"> <legend>Add Panel</legend>
                        <table class="rpt_table" width="50%" cellspacing="1" align="center">
                                <thead>
                                        <th width="170">Shift Policy</th>				    
                                        <th width="115"> Remarks</th>				                    
                                        <td align="left" valign="left"><input type="reset" value="  Refresh  " name="add" id="close" style="width:100px" class="formbutton"/></td>
                                </thead>
                                <tr align="center" class="general">
                                <td>
                                    <select name="cbo_shift_policy" id="cbo_shift_policy" class="combo_boxes" style="width:135px" >
                                           <option value="0">-- Select --</option>
                                           <?
                                                $sql=mysql_db_query($DB,"select * from lib_policy_shift where status_active=1");
                                                while($res=mysql_fetch_array($sql)){
                                                ?> <option value="<? echo $res[id]; ?>"><? echo $res[shift_name]; ?></option> <?	
                                                }
                                           ?>
                                    </select>
                                  </td>                
                                    <td><input name="txt_remarks" id="txt_remarks" type="text"  class="text_boxes" style="width:250px" /></td>
                                    <td style="text-align:left" align="left">
                                        <input type="button" name="search" id="search" value=" Add " class="formbutton" style="width:100px" onclick="fn_add_values_in_html()" />
                                    </td>
                                </tr>
                        </table>
                </fieldset>
          	</fieldset> 
            <fieldset>
      			<div style="width:100%; float:left; margin:auto" align="center" id="roster_list_container"></div><br/>                      
                <div align="center" style="width:100%;"><input type="button" value="Update Roster" name="update" id="update" class="formbutton" style="width:100px" onclick="javascript:fn_update_roster_manual(save_perm,edit_perm,delete_perm,approve_perm)" /></div> 
            </fieldset>     
      </form>
      </div>     
</body>
</html>
