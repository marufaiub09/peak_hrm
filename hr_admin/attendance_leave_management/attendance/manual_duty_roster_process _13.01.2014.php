<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
 
include('../../../includes/common.php');
include('../../../includes/array_function.php');


if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<script src="includes/functions.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>

	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
 
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
	
<style type="text/css"> 
	.style1 {font-family: Verdana, Arial, Helvetica, sans-serif} 
</style>

<script type="text/javascript" charset="utf-8">
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	
		var location_details = division_details = department_details = section_details =emp_code_details=policy_details=id_details= new Array();
		var selected_id = new Array ;
		var selected_id_attnd = new Array ;
	
	<?php
				
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
		
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY level ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_details[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}	
	
	$sql = "SELECT * FROM hrm_employee WHERE status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_code_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_code_details[$row['id']] = mysql_real_escape_string($row['emp_code']);
	}
	
	$sql = "SELECT * FROM hrm_employee WHERE status_active=1 and id_card_no!='' ORDER BY id_card_no ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$id_details = array();
	while( $row = mysql_fetch_array( $result ) ) { 
		$id_details[$row['emp_code']] = mysql_real_escape_string($row['id_card_no']);
	}
	//print_r($id_details);
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$policy_details[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	
					
	?>
		
	
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
	function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "report/hrm/hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
	function populate_value(str)
		{
			var section='<option value=0>--select--</option>>';
			if(str==1)
				{
					<?php
						foreach( $section_details AS $key => $value ) {						
							echo "section += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=section;
				}
				
			var designation='<option value=0>--select--</option>>';
			if(str==2)
				{
					<?php
						foreach( $designation_details AS $key => $value ) {						
							echo "designation += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=designation;
				}
			var emp_code='<option value=0>--select--</option>>';
			if(str==3)
				{
					<?php
						foreach( $emp_code_details AS $key => $value ) {						
							echo "emp_code += '<option value=\"$value\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=emp_code;
				}
			
			var id_card_no='<option value=0>--select--</option>>';
			if(str==4)
				{
					<?php
						foreach( $id_details AS $key => $value ) {						
							echo "id_card_no += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=id_card_no;
				}
			var policy='<option value=0>--select--</option>>';
			if(str==5)
				{
					<?php
						foreach( $policy_details AS $key => $value ) {						
							echo "policy += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=policy;
				}
				
			var department='<option value=0>--select--</option>>';
			if(str==6)
				{
					<?php
						foreach( $department_details AS $key => $value ) {						
							echo "department += '<option value=\"$key\">".$value."</option>';";
						}
					?>
					document.getElementById('cbo_search_by_2_id').innerHTML=department;
				}
				
				
		}

	function checkAll(field)
	{
		selected_id.length = 0;
		selected_id_attnd.length=0;
		$('#selected_id_i').val( '' );
		$('#selected_id_emp').val( '' );
		//alert($('#selected_id_i').val());
	 	 
							 
		//if ($('#chk_all').is(':checked'))
		if ($('#chk_all').val()==0)
		{	
			$('#chk_all').val('1');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{	
				$('#chk_all_'+i).attr('checked', 'checked');
				js_set_value( i );				
			}
		}
		else
		{
			$('#chk_all').val('0');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{
				$('#chk_all_'+i).removeAttr("checked");
				// js_set_value( i );				
			}
		}
	}
	
	function js_set_value( str ) 
	{
		if( jQuery.inArray( str, selected_id ) == -1 ) 
		{
			selected_id.push( str );
		}
		else 
		{
			for( var i = 0; i < selected_id.length; i++ ) 
			{
				if( selected_id[i] == str ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id = '';
		for( var i = 0; i < selected_id.length; i++ ) 
		{
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_i').val( id );
		
		js_set_value_attnd( str );
	}
	
	function js_set_value_attnd( str ) 
	{
		if( jQuery.inArray(  $('#attnd_id_' + str).val(), selected_id_attnd ) == -1 ) 
		{
			selected_id_attnd.push( $('#attnd_id_' + str).val() );
		}
		else 
		{
			for( var i = 0; i < selected_id_attnd.length; i++ ) 
			{
				if( selected_id_attnd[i] == $('#attnd_id_' + str).val() ) break;
			}
			selected_id_attnd.splice( i, 1 );
		}
		var id  = '';
		for( var i = 0; i < selected_id_attnd.length; i++ ) 
		{
			id += selected_id_attnd[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_emp').val( id );
	}
	
	function fn_add_values_in_html()
	{
		$("#messagebox").html('<img src="../../../images/loading.gif" />').fadeIn(1000);
		
		if ($('#selected_id_i').val()=="")
		{ 
			alert ('No Employee is Selected'); return false; 
		}			
		else
		{
			var data=$('#selected_id_i').val()+"__"+$('#selected_id_emp').val()+"__"+$('#cbo_shift_policy').val()+"__"+$('#txt_remarks').val();
			//alert(data);
			$.ajax({type: "POST",url: "includes/manual_duty_roster.php?action=manual_roster_update&data="+data,
				success: function(response){
					eval(response);
					$("#messagebox").removeClass().addClass('messagebox').text('Roster Data add Done').fadeIn(1000);
				}
			});				
					
		}
	}
	
</script>

</head>

<body style="font-family:verdana; font-size:11px;">
 <div style="width:1100px;" align="center">	
    <div>
    	<div class="form_caption">
		Temporary Shift Assignment</div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
    <form name="frm_manual_roster" id="frm_manual_roster" >		
    <fieldset>
      <legend> Search Panel</legend>			
			<table class="rpt_table"  width="1080" cellspacing="0">
            	<thead>	
            		<th width="190">Company Name</th>				    
                    <th width="110">Location </th>				    
                    <th width="280"> Search By </th>				   
                    <th width="200">Date</th>		      		
                    <th width="120">Category</th>				                   
                    <td>                   
                    <input type="hidden" name="selected_id_i" id="selected_id_i" /> <!-- search list row id-->
                    <input type="hidden" name="selected_id_emp" id="selected_id_emp" /><!-- hrm_duty_roster_process id--></td>
            	</thead>
			<tr class="general" align="center">					
					<td>					 
					    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:180px" >
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                              <?php
							   		}
									$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 $company_cond ORDER BY company_name ASC";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );									
									while( $row = mysql_fetch_assoc( $result ) ) {										
								?>							
							<option value="<?php echo $row['id']; ?>"><?php echo $row['company_name']; ?></option>
								<?php 
									} 
								?>
						</select>
			       </td>
					
					<td>					  
					    <select name="location_id" id="location_id" class="combo_boxes" style="width:150px">
                          <option value="0">-- Select --</option>
                          <?php  
                                        $sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
                                        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                                        while( $row = mysql_fetch_assoc( $result ) ) {										
                                    ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['location_name']; ?></option>
                          <?php 
                                        } 
                                    ?>
                      </select>
			        </td>
			  		<td>
                      <select name="cbo_search_by_1" id="cbo_search_by_1" class="combo_boxes" style="width:120px" onchange="populate_value( this.value );">
                            <option value="0"></option>
                            <option value="1">Sectoin</option>
                            <option value="2">Designation</option>
                            <option value="3">Employee Code</option>
                            <option value="4">Employee ID</option>
                            <option value="5">Shift Policy</option>
                            <option value="6">Department</option>
                      </select>
                            <select name="cbo_search_by_2_id" id="cbo_search_by_2_id" class="combo_boxes" style="width:120px">  
                            <option></option>                        
                     </select>
       				</td>
					<td>
				      <input name="txt_from_date" id="txt_from_date" type="text" value=""  class="datepicker" style="width:70px" placeholder="From Date"  />
				      -				      <input name="txt_to_date" id="txt_to_date" type="text"  class="datepicker" value="" style="width:70px" placeholder="To Date"  />
					</td>
                    <td><select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select></td>
                  
                  <td><input type="button" name="search" id="search" value="Search" class="formbutton" style="width:70px" onclick="javascript: fn_search_roster_process()" /></td>
              </tr>                
            </table>         
            <legend>Add Panel</legend>
            <table class="rpt_table" width="50%" cellspacing="1" align="center">
			<thead>
            		<th width="170">Shift Policy</th>				    
                    <th width="115"> Remarks</th>				                    
                    <td align="left" valign="left"><input type="reset" value="  Refresh  " name="add" id="close" style="width:100px" class="formbutton"/></td>
            </thead>
            <tr align="center" class="general">
           	<td>
            	<select name="cbo_shift_policy" id="cbo_shift_policy" class="combo_boxes" style="width:135px" >
   		      		   <option value="0">-- Select --</option>
					   <?
					   		$sql=mysql_db_query($DB,"select * from lib_policy_shift where status_active=1");
							while($res=mysql_fetch_array($sql)){
							?> <option value="<? echo $res[id]; ?>"><? echo $res[shift_name]; ?></option> <?	
							}
                       ?>
       		    </select>
              </td>                
              	<td><input name="txt_remarks" id="txt_remarks" type="text"  class="text_boxes" style="width:250px" /></td>
              	<td style="text-align:left" align="left">
                	<input type="button" name="search" id="search" value=" Add " class="formbutton" style="width:100px" onclick="fn_add_values_in_html()" />
            	</td>
            </tr>
			</table>
            </fieldset> 
            <fieldset>             
      			<div style="width:100%; float:left; margin:auto" align="center" id="roster_list_container"></div><br/>                      
                <div align="center"><input type="button" value="Update Roster" name="update" id="update" class="formbutton" style="width:130px" onclick="javascript:fn_update_roster_manual(save_perm,edit_perm,delete_perm,approve_perm)" /></div>                     			
      		</fieldset>
      </form>
      </div>     
</body>
</html>
