<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 17-04-12
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//-------------------------------------------------------------------------------------------------------------------- 

date_default_timezone_set('UTC');
include('../../../includes/common.php');
include('../../../includes/array_function.php');


if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
    <script src="includes/functions.js" type="text/javascript"></script>

	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

	<link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
	
	<script src="../../../resources/jquery_ui/jquery-1.4.4.min.js" type="text/javascript"></script>
    <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
    <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
	<script>
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		
		function refresh(form)
		 {			
			//window.location.href = unescape(window.location.pathname);
			$('#'+form).find(':input').each(function() {
            var type = this.type;
            var tag = this.tagName.toLowerCase(); // normalize case            
            if (type == 'text' || type == 'password' || tag == 'textarea')
                this.value = "";            
            else if (type == 'checkbox' || type == 'radio')
            this.checked = false;           
            else if (type == 'select-one')
                this.selectedIndex = 0;
            else if (type == 'hidden')
                this.value = "";
        	});			
			
			if(form=="movement_register"){  $('#movement_list_container').html('');  }
		 }
		 
		 
		function openmypage(page_link,title)
		{
						
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0', '../../')
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("txt_selected_emp").value //Access form field with id="emailfield"
			    document.getElementById('save_up').value="";
				//emp_search_descipline(theemail.value,1);
				movement_emp_search_fn(emp_code,1);
				//alert(theemail.value);
				$('#movement_list_container').html('');
				showResult_search(emp_code,"s",2,"movement_list_container");

			}
		}
		
		
	$(document).ready(function(e) {
        
   
	$( ".datepicker" ).datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});								
						
 });		
	
   
   function generate_list_view(selected_id){
	
	var page_link="../../includes/get_data_update.php?period=";
	var id=selected_id;
	var title="Lock Status";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+id+'&type=list_view', title,'width=400px,height=300px,center=1,resize=0,scrolling=0','../../')
	
	emailwindow.onclose=function(){};		
			
		} 
    
    
    
    
    </script>

</head>

<body style="font-family:verdana; font-size:11px;" onload="">
<div align="center">
	<div>
        <div class="form_caption"> Movement Register</div>
         <div align="center"><span id="permission_caption"> <? echo "Your Permissions--> \n".$insert.$update; ?></span></div>   
        <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
    </div>
	<!-- Start Field Set -->
	
	<fieldset style="width:900px;">
	<legend>HRM Module</legend>
	<!-- Start Form -->
	
	<form name="movement_register" id="movement_register" autocomplete="off" method="" action="javascript:fnc_movement_register(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">
			<!-- Start Main Table -->
			<table cellpadding="0" border="0" cellspacing="0" width="100%">			
			<tr>
				<td colspan="6" align="center">
			
				<fieldset>
				<table cellpadding="0" border="0" cellspacing="2" id="move_tbl_id">
					<tr>
						<td width="110" >
							Emp Code
						</td>
						<td width="150"> <!-- Select Calander -->
						 <input name="txt_emp_code" id="txt_emp_code" placeholder="Double Click to Search" ondblclick="openmypage('../../search_employee.php','Employee Info'); return false" readonly="true" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Name
						</td>
						<td width="150"> <!--  System Generated-->
						<input name="txt_emp_name" id="txt_emp_name" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Designation
						</td>
						<td width="150"> <!--Entry -->
						  <input name="txt_designation" id="txt_designation" class="text_boxes" style="width:155px">
						</td>
					</tr>
					
				
				<tr>
						<td width="110" height="24" >
							Company						</td>
						<td width="150"> <!--  Entry-->
						  <input name="txt_company" id="txt_company" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Division
						</td>
						<td width="150"> <!-- Select & Display-->
						<input name="txt_division" id="txt_division" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Department
						</td>
						<td width="150"> <!--  Select-->
						  <input name="txt_department" id="txt_department" class="text_boxes" style="width:155px">					
				  </td>
				  </tr>
				
				<tr>
						<td width="110" >
						Section 
						</td>
						<td width="150"> <!--  Select-->
						  <input name="txt_section" id="txt_section" class="text_boxes" style="width:155px">
						</td>
						<td width="110" >
						Sub Section 
						</td>
						<td width="150"> <!--  Select-->
						<input name="txt_sub_section" id="txt_sub_section" class="text_boxes" style="width:155px">
						</td>
						<td width="110" >						
						</td>
						<td width="150"> <!--  Select-->
						
						</td>
				  </tr>				
				</table>
			</fieldset>
			</td>
				</tr>			
			<tr>
			<td height="10px" colspan="6"></td>
			</tr>
			<tr>
				<td colspan="6" align="center">
					<fieldset>
					<table width="620" cellpadding="0" cellspacing="2" id="details_tbl_id">
						<tr>
							<td width="175" align="left">
								From Date
							</td>
							<td width="150"> <!--  Select-->
							 	<input name="txt_from_date" id="txt_from_date"  style="width:126px" class="datepicker">	<!-- onchange="generate_list_view(this.value)"-->							
								
							</td>
							<td width="175"  >
								To Date
							</td>
							<td width="150"> <!--  Select-->
								<input name="txt_to_date" id="txt_to_date" class="datepicker" style="width:126px"></td>
					 </tr>
					
					<tr>
						<td width="150">
						Movement Purpose
						</td>
						<td colspan="3"> <!--  Select-->
						  <textarea name="txt_movement_purpose"  id="txt_movement_purpose" class="text_area" style="width:439px;"></textarea>
						</td>						
				  </tr>
				  <tr>
				  		<td width="150"> <!--  Select-->
							Destination/Place
						</td>
						<td  >
							<input type="text" name="txt_destination" id="txt_destination" class="text_boxes" style="width:126px" />
						</td>
                        <td width="150"> <!--  Select-->
						Delete Late Present
						</td>
						<td>
                        	<select name="cbo_ignor_late" id="cbo_ignor_late" style="width:135px" class="combo_boxes">
                            	<? 
									foreach ($is_deleted as $id=>$value) //0-no
									{
										?>
                                       <option value="<? echo $id; ?>"><? echo $value; ?></option>
                                        <?
									}
                            	?>
							</select>
						</td>
				  </tr>
				<tr>
					<td width="175" align="left">
								Delete Early Out
							</td>
							<td width="150"> <!--  Select-->
                            	<select name="cbo_ignor_early_out" id="cbo_ignor_early_out" style="width:135px" class="combo_boxes">
                            	<? 
									foreach ($is_deleted as $id=>$value) //0-no
									{
										?>
                                       <option value="<? echo $id; ?>"><? echo $value; ?></option>
                                        <?
									}
                            	?>
							</select>
							</td>
							<td width="175"  >
								Start Time
							</td>
							<td width="150"> <!--  Select-->
                            	<input name="txt_start_time" id="txt_start_time" class="text_boxes" style="width:126px">
							 	<script type="text/javascript">
									$('#txt_start_time').timepicker({
										ampm: true
									});
								</script>
							</td>
				  	</tr>
					<tr>
						<td width="150">
						Return Time
						</td>
						<td >
                        	<input name="txt_return_time" id="txt_return_time" class="text_boxes" style="width:126px">
								<script type="text/javascript">
									$('#txt_return_time').timepicker({
										ampm: true
									});
								</script>
						</td>
						<td width="150">
						Adviced By
						</td>
						<td width="170"> <!--  Select-->
						 <input name="txt_adviced_by" id="txt_adviced_by" class="text_boxes" style="width:126px" disabled="disabled"> 
						</td>
				 	 </tr>
                     <tr>
						<td width="150">
						Entry Number
						</td>
						<td >
                        	 <input name="txt_entry_number" id="txt_entry_number" class="text_boxes" style="width:126px" readonly="readonly">
						</td>
						<td width="150">
						</td>
						<td width="170"> <!--  Select-->
                        	      
						</td>
				 	 </tr>
						<tr><td style="height:10px"></td></tr>
						<tr>
							<td colspan="4" align="center" class="button_container">
								<input type="submit" value="Save" name="Save" id="Save" style="width:100px" class="formbutton"/>&nbsp;
								<input type="button" value="Refresh" name="add" id="close" style="width:100px" class="formbutton" onclick="javascript:refresh('movement_register');"/>	
                                <!-- hidden field for update -->
                                <input type="hidden" name="hidden_from_date" id="hidden_from_date" value="" />
                                <input type="hidden" name="hidden_to_date" id="hidden_to_date" value="" />
                                <input type="hidden" name="save_up" id="save_up" />
                                <!-- end -->
							</td>
						</tr>
					</table>
					</fieldset>
					</td>
					</tr>
					<tr>
						<td align="center">
							<fieldset id="movement_list_container" style="margin-top:10px" >							
							</fieldset>
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
	</div>
</body>
</html>
