<?php
include('../../../includes/common.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cost Sheets</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
   
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
	
    <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
    
    <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
	<script type="text/javascript" charset="utf-8">

function fnc_close()
{
	var tot_row=$('#tbl_holiday_allowance tbody tr').length;
	var form_date="";var to_date=""; var ho_name=""; var ho_type=""; var comp_for="";
	
	 for(var i=1; i<=tot_row; i++)
		{
			if(i>1)
			{
			 form_date +=",";
			 to_date +=",";
			 ho_name +=",";
			 ho_type +=",";
			 comp_for +=",";
			}
			form_date += $("#fromdate_"+i).val();
			to_date += $("#todate_"+i).val();
			ho_name += $("#holidayname_"+i).val();
			ho_type += $("#type_"+i).val();
			comp_for += $("#compensatefor_"+i).val();
		}
		document.getElementById('hidden_form_date').value=form_date;
		document.getElementById('hidden_to_date').value=to_date;
		document.getElementById('hidden_ho_name').value=ho_name;
		document.getElementById('hidden_ho_type').value=ho_type;
		document.getElementById('hidden_comp_for').value=comp_for;
		
		parent.emailwindow.hide()
}

function date_diff( interval, date_form, date_to  )//from_date, no_of_days )
{
	var split_date = date_form.split("-");
	if(split_date[0].length!=4) date_form = split_date[2]+"-"+split_date[1]+"-"+split_date[0];
	
	var split_date = date_to.split("-");
	if(split_date[0].length!=4) date_to = split_date[2]+"-"+split_date[1]+"-"+split_date[0];
	
	
	if (!n_format) var n_format=1;
	//var interval="d";
	var millisecond=1;
	var second=millisecond*1000;
	var minute=second*60;
	var hour=minute*60;
	var day=hour*24;
	//var month=day*30;
	var year=day*365;
	date_form=new Date (date_form);
	date_to=new Date (date_to);
	var newDate;
	//var dVal=dateObj.valueOf();
	switch(interval) {
		case "d": newDate=Math.ceil((date_form.getTime()-date_to.getTime())/(day));  break;
		case "y": newDate=Math.ceil((date_form.getTime()-date_to.getTime())/(year));  break;
	}
	 
		if (newDate>0) return newDate; else return ((newDate)*(-1));
	 
}



function add_days( dateObj, byMany, n_format, target_field  )//from_date, no_of_days )
{
	if(!target_field) var target_field="";
	var temp_date="";
	var split_date = dateObj.split("-");
	if(split_date[0].length!=4) dateObj = split_date[2]+"-"+split_date[1]+"-"+split_date[0];
	if (!n_format) var n_format=1;
	var timeU="d";
	var millisecond=1;
	var second=millisecond*1000;
	var minute=second*60;
	var hour=minute*60;
	var day=hour*24;
	var year=day*365;
	dateObj=new Date (dateObj);
	var newDate;
	var dVal=dateObj.valueOf();
	switch(timeU) {
		case "ms": newDate=new Date(dVal+millisecond*byMany); break;
		case "s": newDate=new Date(dVal+second*byMany); break;
		case "mi": newDate=new Date(dVal+minute*byMany); break;
		case "h": newDate=new Date(dVal+hour*byMany); break;
		case "d": newDate=new Date(dVal+day*byMany); break;
		case "y": newDate=new Date(dVal+year*byMany); break;
	}
	if (n_format==1)
		temp_date= $.datepicker.formatDate('dd-mm-yy', newDate);
		
	if (n_format==2)
		temp_date= $.datepicker.formatDate('yy-mm-dd', newDate);
		
	if(target_field!="") document.getElementById(target_field).value=temp_date;	
	else return temp_date;
		
	//return dateFormat(newDate,"yyyy/mm/dd");
}


function reset_form()
 {
	$('#txt_fromdate').val( '' );
	$('#txt_todate').val( '' );
	$('#txt_holidayname').val('');
	$('#txt_type').val( 0 );
	$('#txt_compensatefor').val( '' );
}

function add_row()
{
	if($("#txt_fromdate").val()=='')
	{
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_fromdate').focus();
			$(this).html('Please select from date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($("#txt_todate").val()==0)
	{
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_todate').focus();
			$(this).html('Please select To date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($("#txt_holidayname").val()=='')
	{
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_holidayname').focus();
			$(this).html('Please select holiday name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else
	{
	var row_num=$('#tbl_holiday_allowance tr').length;
	var form_date = $("#txt_fromdate").val();
	var to_date = $("#txt_todate").val();
	var holidayname = $("#txt_holidayname").val();
	var hotype = $("#txt_type").val();
	var compfor = $("#txt_compensatefor").val();
	
	var get_days = date_diff( 'd', form_date, to_date )+1;
	var ad=0;
	for(var i=1; i<=get_days; i++)
	{
		row_num++;
		var form_date_r=add_days(form_date, ad);
		
		$('#tbl_holiday_allowance').append(
		'<tr>'
			+ '<td><input type="text" name="fromdate_' + row_num + '" id="fromdate_' + row_num + '" value="' + form_date_r +'" class="text_boxes" readonly="readonly" /></td>'
			+ '<td><input type="text" name="todate_' + row_num + '" id="todate_' + row_num + '" value="' + to_date +'" class="text_boxes" readonly="readonly" /></td>'
			+ '<td><input type="text" name="holidayname_' + row_num + '" id="holidayname_' + row_num + '" value="' + holidayname +'" class="text_boxes" readonly="readonly" /></td>'
			+ '<td>'
				+ '<select name="type_' + row_num + '" id="type_' + row_num + '" class="combo_boxes" disabled="disabled">'
					+ '<option value="0">Holiday</option>'
					+ '<option value="1">Compensatory</option>'
					+ '<option value="2">Festival holiday</option>'
				+ '</select>'
			+ '</td>'
			+ '<td><input type="text" name="compensatefor_' + row_num + '" id="compensatefor_' + row_num + '" value="' + compfor +'" class="text_boxes" readonly="readonly"/></td>'
		+ '</tr>'
		);
		$('#type_' + row_num).val(hotype);
		ad++;
	}
	 reset_form();
	}
}
	</script>
</head>

<body>
    <div align="center">
    <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
    <fieldset style="width:750px">
    <table id="tbl_holiday_list" style="width:750px;" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                    <th width="50px">From Date</th>
                    <th width="50px">To Date</th>
                    <th width="200px">Holiday Name</th>
                    <th width="70px">Type</th>
                    <th width="50px">Compensate for</th>
                    <th width="50px">&nbsp;</th>
            </thead>
                    <tr class="general">
                            <td><input type="text" name="txt_fromdate" id="txt_fromdate" class="datepicker" value="" /></td>
                            <td><input type="text" name="txt_todate" id="txt_todate" class="datepicker" value="" /></td>
                            <td><input type="text" name="txt_holidayname" id="txt_holidayname" class="text_boxes" value="" style="width:200px;" /></td>
                            <td>
                            <select name="txt_type" id="txt_type" class="combo_boxes">
                                <option value="0">Holiday</option>
                                <option value="1">Compensatory</option>
                                <option value="2">Festival holiday</option>
                            </select>
                            </td>
                            <td><input type="text" name="txt_compensatefor" id="txt_compensatefor" class="datepicker" value="" /></td>
                            <td><input type="button" name="add" id="add" class="formbutton" value="Add" onclick="add_row()" /> </td>
                    </tr>
    </table>
                                 <input type="hidden" name="hidden_form_date" id="hidden_form_date">
                                 <input type="hidden" name="hidden_to_date" id="hidden_to_date">
                                 <input type="hidden" name="hidden_ho_name" id="hidden_ho_name">
                                 <input type="hidden" name="hidden_ho_type" id="hidden_ho_type">
                                 <input type="hidden" name="hidden_comp_for" id="hidden_comp_for">
               
    </fieldset>
    </div>
    <div align="center">
     <fieldset style="width:750px">
    <table id="tbl_holiday_allowance" style="width:700px;" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
    
    </table>
       				<div style="width:100%"> 
                        <div style="width:100%; float:left" align="center">
                        <input type="button" name="close" onclick="fnc_close();" class="formbutton" value="Close" />
                        </div>
                    </div>
    </fieldset>
    </div>
</body>
<script>

$(document).ready(function() {
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
		});
</script>

<?

function add_time($event_time,$event_length)
{
	$timestamp = strtotime("$event_time");
	$etime = strtotime("+$event_length minutes", $timestamp);
	$etime=date('H:i:s', $etime);
	return $etime;
}

?>