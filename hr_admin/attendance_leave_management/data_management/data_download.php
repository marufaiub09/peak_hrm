<?php
session_start();
// Fuad
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

$user_id=$_SESSION['logic_erp']["user_id"];
include('../../../includes/common.php');

if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
	<script type="text/javascript" src="includes/ajaxfileupload.js" ></script>
    
	<script>
		
		function ajaxFileUpload()
			{
				$("#loading").ajaxStart(function(){
					$(this).show();
				}).ajaxComplete(function(){
					$(this).hide();
				});				
				
				$.ajaxFileUpload({
						url:'includes/doajaxfileupload.php', 
						secureuri:false,
						fileElementId:'fileToUpload',
						dataType: 'json',
						success: function (data, status)
						{
							if(typeof(data.error) != 'undefined')
							{
								if(data.error != '')
								{
									alert(data.error);
								}else
								{
									alert(data.msg);
								}
							}
						},
						error: function (data, status, e)
						{
							alert(e);
						}
					}
				)
				return false;
			}
	 
	
    function fn_file_browse(db_id)
	{
		//alert(db_id);
		var split_db_id = db_id.split('_');		
		if(split_db_id[1]==4)
		{  
			$('#tr_file_upload').removeAttr('style');
		}
		else
		{
			$('#tr_file_upload').attr('style','display:none');
		}
				
	}
    
	
	
	function generate_list_view(selected_id){
		
		$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&period=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});
		}
	
	
	
	
    </script>
	
    
	<style>
        input.file 
		{
			position: relative;
			text-align: right;
			-moz-opacity:0 ;
			filter:alpha(opacity: 0);
			opacity: 0;
			z-index: 2;
		}
    </style>
    
</head>

<body style="font-family:verdana; font-size:11px;"  >
<div  style="width:850px;">

<div style="width:530px;float:left"> 
	<div align="center" style="width:530px; position:relative; height:40px; margin:5px 0;" class="form_caption">
		Data Download / Syncronization
		<div align="center" id="messagebox"></div>	
	</div>	
    <img id="loading" src="loading.gif" style="display:none;">
	<form action="javascript:fnc_data_download();" id="frm_data_download_form" method="post" enctype="multipart/form-data" >
		<fieldset style="width:500px;">
			<legend>Data Download</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="4" align="center">
						<input type="checkbox" name="all_data" id="all_data" /> Include old Data in this Download<br />
					<hr />
					</td>
				</tr>
				<tr>
					<td>Reader Model</td>
					<td colspan="3" align="left">
						<select name="cbo_reader_model" class="combo_boxes" style="width:360px" id="cbo_reader_model" onchange="fn_file_browse(this.value);">
							<option value="0" >--- Select One Reader ---</option>
							<?
									$company_sql= mysql_db_query($DB, "select *  from reader_configuration where is_deleted=0 and status_active=1  and user_name_list='$user_id' order by reader_model");
									while ($r_company=mysql_fetch_array($company_sql))
									{
										$reader_model='"'.$r_company[id].'"';
									?>
									<option value=<? echo $r_company[id]."_".$r_company[database_type];
									if ($cbo_item1==$r_company[id]){?> selected <?php }?> ><? echo "$r_company[reader_model]" ?> </option>
									<?
									}
							?>
						</select>
					</td>
				</tr>
               
                <tr id="tr_file_upload" style="display:none">
					<td>File</td>
					<td colspan="3" align="left"><input type="file" name="fileToUpload" id="fileToUpload" style="width:250px" onchange="return ajaxFileUpload();" /></td>                    			    
				</tr>
				
                <tr style="display:none">
					<td>Downlaod Date From</td>
					<td><input type="text" name="txt_date_from" id="txt_date_from" class="text_boxes" value="" style="width:100px;" onchange="generate_list_view(this.value)"/>
						<script type="text/javascript">
							$( "#txt_date_from" ).datepicker({
								dateFormat: 'dd-mm-yy',
								changeMonth: true,
								changeYear: true
							});
						</script>
					</td>
					<td>Downlaod Date To</td>
					<td><input type="text" name="txt_date_to" id="txt_date_to" class="text_boxes" value="" style="width:100px;" />
					<script type="text/javascript">
						$( "#txt_date_to" ).datepicker({
									dateFormat: 'dd-mm-yy',
									changeMonth: true,
									changeYear: true
								});
					//	alert(document.getElementById('lastname').value)
					</script>
					</td>
				</tr>
				<tr>
					<td colspan="4" height="60" valign="middle" align="center"><input type="submit" name="save_d" id="save_d" value="Start Download/Syncronize" class="formbutton" style="width:270px; height:45px" />
					</td>
					
				</tr>
				<tr>
					<td colspan="4" height="40" valign="middle" align="center" id="download_anim">
					
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	</div>
  <div align="center" style="font-size:18px; width:300px; position:relative; height:40px; margin:5px 0;"></div>	
    <div id="data_panel"  class="demo_jui" style="margin-top:10px; width:300px; float:right"></div>
</div>
</body>
</html>	