<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 17-04-12
		
######################################*/
session_start();

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}



include('../../../includes/common.php');
if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	 
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script>
function countUp() {

 month = 'Jan';      // change the month to the one you want in the same format as shown
 date = '01';        // change the date to the one you want in the same format as shown
 year = '2002';      // change the year to the one you want in the same format as shown

 theDate = month + ' ' + date + ' ' + year;

   now = new Date();
   setdate = new Date(theDate);

   timer = (now - setdate) / 1000 / 60 / 60 / 24;
   timer = Math.round(timer);

document.getElementById("secs").innerHTML=timer;

  }


function openpage_searchemp(page_link,title)
		{		
			var company=document.getElementById('cbo_company_name').value;
			var location=document.getElementById('cbo_location_name').value;
			if(company==0 || company==''){alert("Please Select The Company.");return false;}
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_attn_data_reprocess.php?company='+company+'&location='+location, title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')
			emailwindow.onclose=function()
			{
				//alert("ok");
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");
				$('#txt_emp_code').val(thee_id.value);	
			}
		}

</script>

</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center">
	<div align="center" style="width:500px; position:relative; height:40px; margin:5px 0;" class="form_caption">
		Attendance Data Re-Process
	</div>	
	<form id="holiday_form" action="javascript:fnc_data_reprocess();" method="POST">
		<fieldset style="width:500px;">
			<legend>Data Re-Process</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td> Select Company Name</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:250px ">
                        	<? if($company_cond=="")
							   { 
								?> 
								<option value="0">--- Select Company Name ---</option>
							<?
							   }
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 $company_cond order by company_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr style="display:none">
					<td> Select Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:150px ">
							<option value="0">--- Select Location Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
                     
					</td>
				</tr>
                <tr>
                	<td>Delete Manual Attendance</td>
                    <td colspan=""> 
                    	<select name="cbo_keep_manual" id="cbo_keep_manual" class="combo_boxes" style="width:250px ">
							<option value="0">No</option>
							<option value="1">Yes</option>
					  </select>
                    </td>
                </tr>
				<tr>
					<td> From Date&nbsp;</td>
					<td>
						<input type="text" name="txt_from_date" id="txt_from_date" class="text_boxes" style="width:80px" />
						<script type="text/javascript">
							$( "#txt_from_date" ).datepicker({
								dateFormat: 'dd-mm-yy',
								changeMonth: true,
								changeYear: true
							});
						</script>
						 &nbsp;&nbsp;&nbsp;To Date <input type="text" name="txt_to_date" id="txt_to_date" class="text_boxes" style="width:84px" />
						 <script type="text/javascript">
							$( "#txt_to_date" ).datepicker({
									dateFormat: 'dd-mm-yy',
									changeMonth: true,
									changeYear: true
								});
					//	alert(document.getElementById('lastname').value)
					</script>
					</td>
				</tr>
                <tr>
					<td> Select Employee</td>
					<td>
						<input name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:240px " placeholder="Double Click To Search" readonly ondblclick="openpage_searchemp('../../search_employee_attn_data_reprocess.php','Search Employee')" >			
					  
					</td>
				</tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" name="data_process" id="data_process" value="Start Data Re-Processing" style="width:270px; height:45px" class="formbutton" /> <input type="hidden" onclick="countUp()" /></td>
					
				</tr>
				<tr style="visibility:hidden">
					<td colspan="2" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<div align="center" id="messagebox"></div>
						<div id="process_anim"></div>
					</td>
				</tr>
			</table>
		</fieldset>
		
	</form>
	</div>
</body>
</html>