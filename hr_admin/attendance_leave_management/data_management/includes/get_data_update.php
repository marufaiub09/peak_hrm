<?php
session_start();
date_default_timezone_set('UTC');
include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
/* Replace the data in these two lines with data for your db connection */
$type=$_GET["type"];

if(isset($_GET['getClientId']))
{  
	
	
	if ($type==1) // Employee for Disciplinary Info
	{
		//Designation array
			$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$designation_chart = array();
			while( $row = mysql_fetch_assoc( $result ) ) 
			{
				$designation_chart[$row['id']] = $row['custom_designation'];
			}
			
		//Company array
			$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$company_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$company_details[$row['id']] = $row['company_name'];
			}
		//Department array
			$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$department_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$department_details[$row['id']] = $row['department_name'];
			}
		//Diviion
			$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$division_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$division_details[$row['id']] = $row['division_name'];
			}
		//Location
			$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$location_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$location_details[$row['id']] =$row['location_name'];
			}
		//Section
		$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$section_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$section_details[$row['id']] = $row['section_name'];
			}
		//Sub Section
			$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$subsection_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$subsection_details[$row['id']] = $row['subsection_name'];
			}
		
		
	$res = mysql_query("SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
					FROM hrm_employee AS emp
					LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
					WHERE emp.emp_code = '".$_GET['getClientId']."' and emp.is_deleted=0 and emp.status_active=1 order by emp.emp_code")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
			echo "formObj.txt_designation.value = '".mysql_real_escape_string($designation_chart[$inf["designation_id"]])."';\n";    
			echo "formObj.txt_company.value = '".mysql_real_escape_string($company_details[$inf["company_id"]])."';\n"; 
			echo "formObj.txt_division.value = '".mysql_real_escape_string($division_details[$inf["division_id"]])."';\n"; 
			echo "formObj.txt_department.value = '".mysql_real_escape_string($department_details[$inf["department_id"]])."';\n"; 
			echo "formObj.txt_section.value = '".mysql_real_escape_string($section_details[$inf["section_id"]])."';\n"; 
		} 
	}
	
	if ($type==2) // Disc for Disciplinary Info
	{
		$res = mysql_query("SELECT * from hrm_disciplinary_info_mst
					WHERE emp_code = '".$_GET['getClientId']."' and withdrawn_date='0000-00-00' order by id desc")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_occurrence_date.value = '".$inf["occurrence_date"]."';\n";    
			echo "formObj.txt_action_date.value = '".$inf["action_date"]."';\n";    
			echo "formObj.txt_occurrence_details.value = '".mysql_real_escape_string($inf["occurrence_details"])."';\n";    
			echo "formObj.txt_investigating_members.value = '".mysql_real_escape_string($inf["investigating_members"])."';\n"; 
			echo "formObj.txt_investigation.value = '".mysql_real_escape_string($inf["investigation"])."';\n"; 
			echo "formObj.cbo_action_taken.value = '".mysql_real_escape_string($inf["action_taken"])."';\n"; 
			echo "formObj.txt_withdawn_date.value = '".$inf["withdrawn_date"]."';\n"; 
			echo "formObj.save_up_disc.value = '".$inf["id"]."';\n";  
			//echo "formObj.save_up_po.value = '".$inf["id"]."';\n"; 
		} 
		else{
			echo "formObj.txt_occurrence_date.value = '';\n";    
			echo "formObj.txt_action_date.value = '';\n";    
			echo "formObj.txt_occurrence_details.value = '';\n";    
			echo "formObj.txt_investigating_members.value = '';\n"; 
			echo "formObj.txt_investigation.value = '';\n"; 
			echo "formObj.cbo_action_taken.value = '';\n"; 
			echo "formObj.txt_withdawn_date.value = '';\n"; 
			echo "formObj.save_up_disc.value = '';\n";  
		}
		
	}
	
	if ($type==3) // Disc for Disciplinary Info
	{
		$res = mysql_query("SELECT * from hrm_movement_register
					WHERE emp_code = '".$_GET['getClientId']."' and position=1")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_from_date.value = '".convert_to_mysql_date($inf["from_date"])."';\n";    
			echo "formObj.txt_to_date.value = '".convert_to_mysql_date($inf["to_date"])."';\n";    
			echo "formObj.txt_movement_purpose.value = '".mysql_real_escape_string($inf["movement_purpose"])."';\n";    
			echo "formObj.txt_destination.value = '".mysql_real_escape_string($inf["destination_place"])."';\n"; 
			echo "formObj.txt_start_time.value = '".$inf["start_time"]."';\n"; 
			echo "formObj.txt_return_time.value = '".$inf["end_time"]."';\n"; 
			echo "formObj.txt_adviced_by.value = '".mysql_real_escape_string($inf["adviced_by"])."';\n"; 
			echo "formObj.txt_entry_date.value = '".convert_to_mysql_date($inf["entry_date"])."';\n";  
			echo "formObj.save_up_move.value = '".$inf["row_id"]."';\n"; 
			echo "formObj.Save.value = 'Update';\n"; 
		}
	}
}	

	
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}


//list_view
if($type=="list_view")
{		
	extract($_GET);
	extract($_POST);
	
	
	 $year=date("Y",strtotime(($period)));
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.actual_starting_date like '$year-%%'  and b.type=1";
	
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	?>
	 <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:12px;" width="300" rules="all"  align="center">
         <thead> 
            	<tr>
                	<th colspan="3">Salary Period Lock Status</th>
                </tr>
            <tr>
                
                <th width="120">Start Date</th>
                <th width="120">End Date</th>
                <th width="80">Status</th>
            </tr>
		</thead> 
        <?
		$r=0;
	while( $row = mysql_fetch_assoc( $result ) ) {
    
	if ($row[is_locked]==1) $bgcolor="#FF0000"; else $bgcolor="#EEEEEE"; 
				
	
	?>
        <tr bgcolor="<? echo $bgcolor;?>"  height="20">
        
            <td width="120"><? echo $row[starting_date]; ?></td>
            <td width="120"><? echo $row[ending_date]; ?></td>
            <td width="80"><?  if($row[is_locked]==1) echo "Locked"; else echo ""; ?></td>
        
        </tr>
				<?
				$r++;
			
	}	
	?>	
	 </table>
     <?
}

?> 
