<?php
session_start();
include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

$e_date = time();
$user_only = $_SESSION['logic_erp']["user_name"];
$user_id_id=$_SESSION['logic_erp']["user_id"];

extract( $_GET );

extract( $_REQUEST );

if ( check_database_process_status()==1 )
{
	echo "5";
	die;
}


if($txt_to_date=='' || $txt_to_date==0)$txt_to_date=add_date(add_month($txt_date,1),-1);


if( check_period_status($txt_date, $txt_to_date)==1)
{
	echo "4";
	die;
}



// Shift Details
$shift_lists = array();
$sql_shift = "SELECT id,shift_start,shift_end,shift_type,grace_minutes,exit_buffer_minutes,first_break_start,first_break_end,second_break_start,second_break_end,lunch_time_start,lunch_time_end,early_out_start,entry_restriction_start,cross_date FROM lib_policy_shift";
$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
while( $row_shift = mysql_fetch_array( $result_shift ) ) 
{
	$shift_lists[$row_shift[id]]['shift_start']=$row_shift[shift_start];
	$shift_lists[$row_shift[id]]['shift_end']=$row_shift[shift_end];
	$shift_lists[$row_shift[id]]['shift_type']=$row_shift[shift_type];
	$shift_lists[$row_shift[id]]['in_grace_minutes'] =add_time( $row_shift[shift_start], $row_shift[grace_minutes]); // Add In Grace Time with In Time
	$shift_lists[$row_shift[id]]['exit_buffer_minutes']=$row_shift[exit_buffer_minutes];
	
	$shift_lists[$row_shift[id]]['tiffin_start']=$row_shift[first_break_start];
	$shift_lists[$row_shift[id]]['tiffin_ends']=$row_shift[first_break_end];
	$shift_lists[$row_shift[id]]['dinner_start']=$row_shift[second_break_start];
	$shift_lists[$row_shift[id]]['dinner_ends']=$row_shift[second_break_end];
	
	$shift_lists[$row_shift[id]]['lunch_start']=$row_shift[lunch_time_start];
	$shift_lists[$row_shift[id]]['lunch_break_min']=datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]); 
	$shift_lists[$row_shift[id]]['lunch_break_ends']=$row_shift[lunch_time_end]; 
	
	$shift_lists[$row_shift[id]]['total_working_min']=datediff(n,$row_shift[shift_start],$row_shift[shift_end])-datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]);
	$shift_lists[$row_shift[id]]['early_out_start']=$row_shift[early_out_start];
	$shift_lists[$row_shift[id]]['entry_restriction_start']=$row_shift[entry_restriction_start];
	$shift_lists[$row_shift[id]]['cross_date']=$row_shift[cross_date];
}

$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
while($res=mysql_fetch_array($result_qr))		
{
	$ot_fraction = $res['allow_ot_fraction'];
	$ot_start_min = $res['ot_start_minute'];
	$one_hr_ot_unit = $res['one_hour_ot_unit'];
	$adjust_out_time=$res['adjust_out_time'];
	$applicable_above_salary=$res['applicable_above_salary'];
	$in_out_time_format=$res['in_out_time_format'];
}
//$variable_settings=get_variable_settings( $company_id, $txt_date, $txt_date );
		
$qr="select a.*, b.* from lib_policy_tiffin_bill_mst a, lib_policy_tiffin_bill_dtls b where a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and a.id=b.policy_id  order by b.policy_id asc,b.must_stay desc"; //and earning_head=9 
$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
$tiffin_bill_policy=array();
while($res=mysql_fetch_array($result_qr))		
{
	if(!in_array( $res[policy_id], $pol ))
	{
		$pol[]=$res[policy_id];
		$i=0;
	}
	$tiffin_bill_policy[$res[policy_id]][$i]['must_stay']=$res[must_stay];
	$tiffin_bill_policy[$res[policy_id]][$i]['bill_amt']=$res[bill_amt];
	$tiffin_bill_policy[$res[policy_id]][$i]['late_applicable']=$res[late_applicable];
	$i++;
}

$qr="select * from lib_policy_allowance where status_active=1 and is_deleted=0  and earning_head in ( 40 )  and day_type in ( 1,2 ) order by policy_id asc,must_stay desc";  //and pay_with_salary=2
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	$allowance_policy_bill=array();
	while($res=mysql_fetch_array($result_qr))		
	{
		if(!in_array( $res[policy_id], $pol ))
		{
			$pol[]=$res[policy_id];
			$i=0;
		}
		$allowance_policy_bill[$res[policy_id]][$i]['must_stay']=$res[must_stay];
		//$allowance_policy_bill[$res[policy_id]][$i]['bill_amt']=$res[bill_amt];
		$allowance_policy_bill[$res[policy_id]][$i]['late_applicable']=$res[late_applicable];
		$allowance_policy_bill[$res[policy_id]][$i]['formula_type']=$res[formula_type];
		$allowance_policy_bill[$res[policy_id]][$i]['value']=$res[value];
		$allowance_policy_bill[$res[policy_id]][$i]['base_head']=$res[base_head];
		$allowance_policy_bill[$res[policy_id]][$i]['day_type']=$res[day_type];
		$allowance_policy_bill[$res[policy_id]][$i]['pay_with_salary']=$res[pay_with_salary];
		$i++;
	}

$qr="select * from lib_policy_allowance where status_active=1 and is_deleted=0 and earning_head in ( 8,41 ) and day_type in ( 1,3,4) order by policy_id asc,must_stay desc"; //and pay_with_salary=2 
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	$holiday_policy_bill=array();
	$pol=array();
	while($res=mysql_fetch_array($result_qr))		
	{
		if(!in_array( $res[policy_id], $pol ))
		{
			$pol[]=$res[policy_id];
			$i=0;
		}
		$holiday_policy_bill[$res[policy_id]][$i]['must_stay']=$res[must_stay];
		//$allowance_policy_bill[$res[policy_id]][$i]['bill_amt']=$res[bill_amt];
		$holiday_policy_bill[$res[policy_id]][$i]['late_applicable']=$res[late_applicable];
		$holiday_policy_bill[$res[policy_id]][$i]['formula_type']=$res[formula_type];
		$holiday_policy_bill[$res[policy_id]][$i]['value']=$res[value];
		$holiday_policy_bill[$res[policy_id]][$i]['base_head']=$res[base_head];
		$holiday_policy_bill[$res[policy_id]][$i]['day_type']=$res[day_type];
		$holiday_policy_bill[$res[policy_id]][$i]['pay_with_salary']=$res[pay_with_salary];
		$i++;
	}

$salary_breakdown = array();
	$sql_sal="select * from hrm_employee_salary  ";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_breakdown[$row_sal[emp_code]][$row_sal[payroll_head]]=$row_sal[amount];
	}		

$overtime_salary=convert_to_mysql_date($txt_date);
$overtime_salary_period=explode("-",$overtime_salary);
$overtime_salary_period_show=$overtime_salary_period[0]."-".$overtime_salary_period[1]."-"."01";

$cbo_salary_periods_search=convert_to_mysql_date($txt_date);
$company_id;
//$action="night_allowance_process";

if( $action=="tiffin_process" )
{
		mysql_query( "update database_status_process set status=1" );
		
		if ($txt_emp_code_tiffin!="") { $txt_emp_co=explode(",",$txt_emp_code_tiffin); $txt_emp_code_tiffin="'".implode("','",$txt_emp_co)."'"; }
		
		if(trim($txt_emp_code_tiffin)!="") $emp_code_search=" and a.emp_code in ( $txt_emp_code_tiffin )";
		
		$sql = "SELECT a.id as attid,b.salary_grade,b.tiffin_policy,b.id_card_no,b.emp_code,a.attnd_date,a.sign_in_time,a.sign_out_time,a.total_over_time_min, a.r_sign_in_time,a.r_sign_out_time,a.status,a.is_next_day,a.policy_shift_id,a.is_manually_updated FROM hrm_attendance a, hrm_employee b
		WHERE 
		a.attnd_date like '$cbo_salary_periods_search' and 
		a.emp_code=b.emp_code  and b.tiffin_policy!=0
		and b.status_active=1  $emp_code_search
		$orderby";
		//echo $sql;die;
		 
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
		{ 
			$night_shift_work=0;
			$min=0;
			$hr=0;
			
			if(in_array($row_emp['policy_shift_id'],$force_night_shift)) $shift_lists[$row_emp['policy_shift_id']]['cross_date']=1;
			 
			if( $row_emp[sign_out_time]!="00:00:00")
			{
				if($row_emp[is_manually_updated]==1 && $row_emp[is_next_day]==0 && $row_emp[r_sign_in_time]>$row_emp[sign_out_time] && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==0 )
				{
					$timeDiffout=datediff( "n", $row_emp[r_sign_out_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time]);
					//$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
				}
				else if( $row_emp[is_next_day]==0 )
				{
					$min= datediff( "n", $row_emp[r_sign_out_time], $row_emp[sign_out_time]);
				}
				else if($row_emp[is_next_day]==1 && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==0 )
				{
					$timeDiffout=datediff( "n", $row_emp[r_sign_out_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time]);
					//$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
				}
				else if($row_emp[is_next_day]==1 && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==1 ) // Shift work at NIGHT
				{
					$timeDiffout=datediff( "n", $row_emp[sign_in_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time]);
					//$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
					if( $min>539 ) $night_shift_work=1;
				}
				else $min= datediff( "n", $row_emp[r_sign_out_time], $row_emp[sign_out_time]);
				$hr=floor($min/60).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
			}
			
			
			//echo $min;die;
			
			
			$tiffpolicy=$row_emp[tiffin_policy];
			$amount=0;
		//print_r($tiffin_bill_policy);die;
			foreach( $tiffin_bill_policy[$tiffpolicy] as $key )
			{
				//echo $key[late_applicable]; die;
				 if( $row_emp[status]=="D" && $key[late_applicable]==1)
				 {
					 $amount=0;
					 break 0;
				 }
				 if( $min>=$key[must_stay] && $night_shift_work==0 )
				 {
					 $amount=$key[bill_amt];
					 $total_amount+=$amount;
					 break 0;
				 }
				 else if($min>=$key[must_stay] && $night_shift_work==1)
				 {
					 $amount=$key[bill_amt];
					 $total_amount+=$amount;
					 break 0;
				 }
				 else $amount=0;
			}
			
			//echo $tiffin_reprocess;
			
			/*if($tiffin_reprocess==1)
			{
				$id_arr[]=$row_emp[attid];
				$data_array_up[$row_emp[attid]] =explode("*",("1*".$amount.""));
			}
			else
			{*/
				if( $amount>0 ) // Ready to update
				{
					$id_arr[]=$row_emp[attid];
					$data_array_up[$row_emp[attid]] =explode("*",("1*".$amount.""));
				}
				else
				{
					$id_arr[]=$row_emp[attid];
					$data_array_up[$row_emp[attid]] =explode("*",("0*".$amount.""));
				}
			//}
		}
		//echo "sohel";die;
	//print_r($data_array_up); die;
		 
		$field_array_up="is_get_tiffin_allowance*today_tiffin_allowance";
		if( count($id_arr)>0 ) mysql_query( bulk_update_sql_statement( "hrm_attendance", "id", $field_array_up, $data_array_up, $id_arr ));
		
		mysql_query( "update database_status_process set status=0" );
		echo "2";
		exit();
}

if( $action=="night_allowance_process" )
{
		mysql_query( "update database_status_process set status=1" );
		
		if ($txt_emp_code_night!="") { $txt_emp_co=explode(",",$txt_emp_code_night); $txt_emp_code_night="'".implode("','",$txt_emp_co)."'"; }
		
		if(trim($txt_emp_code_night)!="") $emp_code_search=" and a.emp_code in ( $txt_emp_code_night )";
		
		
		$sql = "SELECT a.id as attid,b.salary_grade,b.tiffin_policy,b.allowance_policy,b.gross_salary,b.id_card_no,b.emp_code,a.attnd_date,a.sign_in_time,a.sign_out_time,a.total_over_time_min, a.r_sign_in_time,a.r_sign_out_time,a.status,a.is_next_day,a.policy_shift_id,a.is_manually_updated,a.is_regular_day FROM hrm_attendance a, hrm_employee b WHERE 
		a.attnd_date like '$cbo_salary_periods_search' and 
		a.company_id=$company_id and  b.allowance_policy!=0 and
		a.emp_code=b.emp_code 
		and b.status_active=1 $emp_code_search ";
		
		//echo $sql;die;
		//and a.emp_code=0010016
		$days_in_month=cal_days_in_month(CAL_GREGORIAN, date("m",strtotime($cbo_salary_periods_search)), date("Y",strtotime($cbo_salary_periods_search)));
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
		{ 
			$salary_breakdown[$row_emp[emp_code]][20]=$row_emp[gross_salary];
			$min=0;
			$hr=0;
			if( $row_emp[sign_out_time]!="00:00:00")
			{
				if( $row_emp[is_manually_updated]==1 && $row_emp[is_next_day]==0 && $row_emp[r_sign_in_time]>$row_emp[sign_out_time] && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==0 )
				{
					$timeDiffout=datediff( "n", $row_emp[r_sign_out_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time])+1;
					//$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
				}
				else if( $row_emp[is_manually_updated]==1 && $row_emp[is_next_day]==1 && $row_emp[r_sign_in_time]>$row_emp[sign_out_time] && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==0 )
				{
					$timeDiffout=datediff( "n", $row_emp[r_sign_out_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time])+1;
					//$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
				}
				else if($row_emp[is_next_day]==1 && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==0 )
				{
					$timeDiffout=datediff( "n", $row_emp[r_sign_out_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time])+1;
					//$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
				}
				else if( $row_emp[is_next_day]==0 )
				{
					$min= datediff( "n", $row_emp[r_sign_out_time], $row_emp[sign_out_time]);
					//echo $min.",";
				}
				else $min= datediff( "n", $row_emp[r_sign_out_time], $row_emp[sign_out_time]);
				$hr=floor($min/60).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
			}
			
			//echo $min;die;
			$allowancepolicy=$row_emp[allowance_policy];
			$amount=0;
			/*$allowance_policy_bill[$res[policy_id]][$i]['must_stay']=$res[must_stay];
			$allowance_policy_bill[$res[policy_id]][$i]['late_applicable']=$res[late_applicable];
			$allowance_policy_bill[$res[policy_id]][$i]['formula_type']=$res[formula_type];
			$allowance_policy_bill[$res[policy_id]][$i]['value']=$res[value];
			$allowance_policy_bill[$res[policy_id]][$i]['base_head']=$res[base_head];
			$allowance_policy_bill[$res[policy_id]][$i]['day_type']=$res[day_type];
			*/
			
			foreach( $allowance_policy_bill[$allowancepolicy] as $key )
			{
				 if( $row_emp[status]=="D" && $key[late_applicable]==1)
				 {
					 $amount=0;
					 break 0;
				 }
				// echo $min."=".$key[must_stay]."=".$key[day_type]."=".$row_emp[is_regular_day];die;
				 
				 if($min>=$key[must_stay] && $key[day_type]==1)// all type
				 {
					if($key[formula_type]==1) // fixed
					 	$amount=$key[value];
					else if($key[formula_type]==2) //%
					 	$amount=( ($salary_breakdown[$row_emp[emp_code]][$key[base_head]]/$days_in_month)*$key[value])/100;//  $key[value];
					else
						$amount=$key[value];
						
					 $total_amount+=$amount;
					 break 0;
				 }
				 else if($min>=$key[must_stay]  && $key[day_type]==2 && $row_emp[is_regular_day]==1 )// General type
				 {
					 if($key[formula_type]==1) // fixed
					 	$amount=$key[value];
					else if($key[formula_type]==2) //%
					 	$amount=( ($salary_breakdown[$row_emp[emp_code]][$key[base_head]]/$days_in_month)*$key[value])/100;//  $key[value];
					else
						$amount=$key[value];
						
						//echo $amount;die;
						
					 $total_amount+=$amount;
					 break 0;
				 }
				 else if($min>=$key[must_stay]  && $key[day_type]==3 && $row_emp[is_regular_day]==0 )// General type
				 {
					 if($key[formula_type]==1) // fixed
					 	$amount=$key[value];
					else if($key[formula_type]==2) //%
					 	$amount=( ($salary_breakdown[$row_emp[emp_code]][$key[base_head]]/$days_in_month)*$key[value])/100;//  $key[value];
					else
						$amount=$key[value];
					 $total_amount+=$amount;
					 break 0;
				 }
				 else $amount=0;
			}
			
			//echo $amount;die;
			
			if( $amount>0 ) // Ready to update
			{
				$id_arr[]=$row_emp[attid];
				$data_array_up[$row_emp[attid]] =explode("*",("1*".$amount.""));
			}
			else
			{
				$id_arr[]=$row_emp[attid];
				$data_array_up[$row_emp[attid]] =explode("*",("0*".$amount.""));
			}
		}
		//echo "sohel";die;
		//print_r($data_array_up); die;
		
		$field_array_up="is_get_night_allowance*today_night_allowance ";
		if( count($id_arr)>0 ) mysql_query( bulk_update_sql_statement( "hrm_attendance", "id", $field_array_up, $data_array_up, $id_arr ));
		mysql_query( "update database_status_process set status=0" );
		echo "2";
		die;
}

if( $action=="holiday_allowance_process" )
{
		mysql_query( "update database_status_process set status=1" );
		
		if ($txt_emp_code_holiday!="") { $txt_emp_co=explode(",",$txt_emp_code_holiday); $txt_emp_code_holiday="'".implode("','",$txt_emp_co)."'"; }
		
		if(trim($txt_emp_code_holiday)!="") $emp_code_search=" and a.emp_code in ( $txt_emp_code_holiday )";
		
		$sql = "SELECT a.id as attid,b.salary_grade,b.tiffin_policy,b.allowance_policy,b.gross_salary,b.id_card_no,b.emp_code,a.attnd_date,a.sign_in_time,a.sign_out_time,a.total_over_time_min, a.r_sign_in_time,a.r_sign_out_time,a.status,a.is_next_day,a.policy_shift_id, a.is_manually_updated,a.is_regular_day FROM hrm_attendance a, hrm_employee b WHERE 
		a.attnd_date like '$cbo_salary_periods_search' and 
		a.company_id=$company_id and  b.allowance_policy!=0 and
		a.emp_code=b.emp_code and is_regular_day=0
		and b.status_active=1 $emp_code_search ";
		$days_in_month=cal_days_in_month(CAL_GREGORIAN, date("m",strtotime($cbo_salary_periods_search)), date("Y",strtotime($cbo_salary_periods_search)));
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
		{
			
			$salary_breakdown[$row_emp[emp_code]][20]=$row_emp[gross_salary];
			$min=0;
			$hr=0;
			if( $row_emp[sign_out_time]!="00:00:00")
			{
				if( $row_emp[is_next_day]==0 )
				{
					$min= datediff( "n", $row_emp[sign_in_time], $row_emp[sign_out_time]);
				}
				else if($row_emp[is_next_day]==1 && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==0 )
				{
					$timeDiffout=datediff( "n", $row_emp[sign_in_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time]);
				}
				else if($row_emp[is_next_day]==0 && $row_emp[r_sign_out_time]>$row_emp[sign_out_time] && $shift_lists[$row_emp['policy_shift_id']]['cross_date']==0 )
				{
					$timeDiffout=datediff( "n", $row_emp[sign_in_time],"23:59:59");
					$p_time="00:00:00";
					$min=$timeDiffout+datediff( "n", $p_time,$row_emp[sign_out_time]);
				}
				else $min= datediff( "n", $row_emp[sign_in_time], $row_emp[sign_out_time]);
				$hr=floor($min/60).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
			}
			$allowancepolicy=$row_emp[allowance_policy];
			$amount=0;
			
			if( $row_emp[status]=="GH" || $row_emp[status]=="CH" || $row_emp[status]=="FH" ) $row_emp[status]="GH";
			foreach( $holiday_policy_bill[$allowancepolicy] as $key )
			{
				 if( $row_emp[status]=="D" && $key[late_applicable]==1)
				 {
					 $amount=0;
					 break 0;
				 }
				
				 if($min>=$key[must_stay] && $key[day_type]==1)// all type
				 {
					if($key[formula_type]==1) // fixed
					 	$amount=$key[value];
					else if($key[formula_type]==2) //%
					 	$amount=( ($salary_breakdown[$row_emp[emp_code]][$key[base_head]]/$days_in_month)*$key[value])/100;//  $key[value];
					else
						$amount=$key[value];
						
					 $total_amount+=$amount;
					 break 0;
				 }
				 else if($min>=$key[must_stay]  && $key[day_type]==2 && $row_emp[is_regular_day]==1 )// General type
				 {
					 if($key[formula_type]==1) // fixed
					 	$amount=$key[value];
					else if($key[formula_type]==2) //%
					 	$amount=( ($salary_breakdown[$row_emp[emp_code]][$key[base_head]]/$days_in_month)*$key[value])/100;//  $key[value];
					else
						$amount=$key[value];
					 $total_amount+=$amount;
					 break 0;
				 }
				 else if($min>=$key[must_stay]  && $key[day_type]==3 && $row_emp[is_regular_day]==0 && $row_emp[status]=="W" )// Offff type
				 {
					 if($key[formula_type]==1) // fixed
					 	$amount=$key[value];
					else if($key[formula_type]==2) //%
					 	$amount=( ($salary_breakdown[$row_emp[emp_code]][$key[base_head]]/$days_in_month)*$key[value])/100;//  $key[value];
					else
						$amount=$key[value];
					 $total_amount+=$amount;
					 break 0;
				 }
				 else if($min>=$key[must_stay]  && $key[day_type]==4 && $row_emp[is_regular_day]==0 && $row_emp[status]=="GH" )// Offff type
				 {
					 if($key[formula_type]==1) // fixed
					 	$amount=$key[value];
					else if($key[formula_type]==2) //%
					 	$amount=( ($salary_breakdown[$row_emp[emp_code]][$key[base_head]]/$days_in_month)*$key[value])/100;//  $key[value];
					else
						$amount=$key[value];
					 $total_amount+=$amount;
					 break 0;
				 }
				 else $amount=0;
			}
			
			if( $amount>0 ) // Ready to update
			{
				$id_arr[]=$row_emp[attid];
				$data_array_up[$row_emp[attid]] =explode("*",("".$amount.""));
			}
			else
			{
				$id_arr[]=$row_emp[attid];
				$data_array_up[$row_emp[attid]] =explode("*",("".$amount.""));
			}
		}
		
		$field_array_up="today_holiday_allowance ";
		if( count($id_arr)>0 ) mysql_query( bulk_update_sql_statement( "hrm_attendance", "id", $field_array_up, $data_array_up, $id_arr ));
		mysql_query( "update database_status_process set status=0" );
		echo "2";
		die;
}

function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
}

function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>