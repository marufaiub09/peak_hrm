function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

function fnc_data_download()
{
	
	var all_data 	= "";
	var txt_date_from 	= escape(document.getElementById('txt_date_from').value);
	var txt_date_to 	= escape(document.getElementById('txt_date_to').value);
	var cbo_reader_model= escape(document.getElementById('cbo_reader_model').value);
 
	
	if (document.getElementById('all_data').checked==true)
	{
		all_data=1;
	}
	else
	{
		all_data=0;
	}
	//alert(cbo_reader_model);
	$("#messagebox").removeClass().addClass('messagebox').text('Data Syncroniziing....').fadeIn(1000);

	if (txt_date_to=="")
	{
		document.getElementById('txt_date_to').value=txt_date_from;
	}
	if($('#cbo_reader_model').val()==0)
	{								
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_reader_model').focus();
			$(this).html('Please Select Reader Model').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
		 $("#download_anim").html('<img src="../../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('post','includes/save_update_attendance.php?action=data_download&all_data='+all_data+
					'&txt_date_from='+txt_date_from+
					'&txt_date_to='+txt_date_to+
					'&cbo_reader_model='+cbo_reader_model);
		http.onreadystatechange = data_download_reply;
		http.send(null); 
	}
}

function data_download_reply() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		
	//alert(response);
	
		if (response==9)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Sorry,... You are not permitted to download or syncronize Data.').addClass('messageboxerror').fadeTo(900,1);
				// document.getElementById('save_up').value="";
			});
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Syncronized Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
	}
}	


//Data Processing 


function fnc_data_process()
{
var r=confirm("Do you need to change shift time for today ? \n \n Press Cancle for Process. Otherwise Ok.")
if (r==true)
  {
  return;
  }
else
  {
	var cbo_location_name	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name	= escape(document.getElementById('cbo_company_name').value);
	var txt_from_date		= escape(document.getElementById('txt_from_date').value);
	var txt_to_date 		= escape(document.getElementById('txt_to_date').value);
	
	// alert(cbo_location_name);
	$("#messagebox").removeClass().addClass('messagebox').text('Data Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_from_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_to_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_to_date').focus();
			$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
		 $("#process_anim").html('<img src="../../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('get','includes/data_processing.php?action=data_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&txt_to_date='+txt_to_date+
					'&txt_from_date='+txt_from_date);
		http.onreadystatechange = data_process_reply;
		http.send(null); 
	}
 }
}

function data_process_reply() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		
// alert(response);
	
		if (response==9)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Sorry,... You are not permitted to download or syncronize Data.').addClass('messageboxerror').fadeTo(900,1);
				// document.getElementById('save_up').value="";
			});
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Processed Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
	}
}	


function fnc_data_reprocess()
{
	/*var r=confirm("All manual attendance entry will be removed on given date. ? \n \n Press OK to Continue. Otherwise Cancel to abort.")
	if (r==false)
  	{
  		return;
  	}*/
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var txt_from_date 	= escape(document.getElementById('txt_from_date').value);
	var txt_to_date 	= escape(document.getElementById('txt_to_date').value);
	var txt_emp_code= escape(document.getElementById('txt_emp_code').value);
	var cbo_keep_manual=document.getElementById('cbo_keep_manual').value;
	// alert(cbo_location_name);
	$("#messagebox").removeClass().addClass('messagebox').text('Data Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_from_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_to_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_to_date').focus();
			$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_emp_code').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee for Data Re-Process').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{
		 $("#process_anim").html('<img src="../../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('get','includes/data_processing.php?action=data_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&txt_to_date='+txt_to_date+
					'&txt_from_date='+txt_from_date+
					'&txt_emp_code='+txt_emp_code+
					'&cbo_keep_manual='+cbo_keep_manual);
		http.onreadystatechange = data_re_process_reply;
		http.send(null); 
	}
}

function data_re_process_reply() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;		
 		//alert(response);			
		if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Processed Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
	}
}	



var timercount = 0;
var timestart  = null;
 
function showtimer() {
	if(timercount) {
		clearTimeout(timercount);
		clockID = 0;
	}
	if(!timestart){
		timestart = new Date();
	}
	var timeend = new Date();
	var timedifference = timeend.getTime() - timestart.getTime();
	timeend.setTime(timedifference);
	var minutes_passed = timeend.getMinutes();
	if(minutes_passed < 10){
		minutes_passed = "0" + minutes_passed;
	}
	var seconds_passed = timeend.getSeconds();
	if(seconds_passed < 10){
		seconds_passed = "0" + seconds_passed;
	}
	document.timeform.timetextarea.value = minutes_passed + ":" + seconds_passed;
	timercount = setTimeout("showtimer()", 1000);
}
 
function sw_start(){
	if(!timercount){
	timestart   = new Date();
	document.timeform.timetextarea.value = "00:00";
	document.timeform.laptime.value = "";
	timercount  = setTimeout("showtimer()", 1000);
	}
	else{
	var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var minutes_passed = timeend.getMinutes();
		if(minutes_passed < 10){
			minutes_passed = "0" + minutes_passed;
		}
		var seconds_passed = timeend.getSeconds();
		if(seconds_passed < 10){
			seconds_passed = "0" + seconds_passed;
		}
		var milliseconds_passed = timeend.getMilliseconds();
		if(milliseconds_passed < 10){
			milliseconds_passed = "00" + milliseconds_passed;
		}
		else if(milliseconds_passed < 100){
			milliseconds_passed = "0" + milliseconds_passed;
		}
		document.timeform.laptime.value = minutes_passed + ":" + seconds_passed + "." + milliseconds_passed;
	}
}

function Stop() {
	if(timercount) {
		clearTimeout(timercount);
		timercount  = 0;
		var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var minutes_passed = timeend.getMinutes();
		if(minutes_passed < 10){
			minutes_passed = "0" + minutes_passed;
		}
		var seconds_passed = timeend.getSeconds();
		if(seconds_passed < 10){
			seconds_passed = "0" + seconds_passed;
		}
		var milliseconds_passed = timeend.getMilliseconds();
		if(milliseconds_passed < 10){
			milliseconds_passed = "00" + milliseconds_passed;
		}
		else if(milliseconds_passed < 100){
			milliseconds_passed = "0" + milliseconds_passed;
		}
		document.timeform.timetextarea.value = minutes_passed + ":" + seconds_passed + "." + milliseconds_passed;
	}
	timestart = null;
}
 
function Reset() {
	timestart = null;
	document.timeform.timetextarea.value = "00:00";
	document.timeform.laptime.value = "";
}
 
