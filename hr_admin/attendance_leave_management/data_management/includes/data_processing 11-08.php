<?php

session_start();
include('../../../../includes/common.php');
include('../../../../includes/array_function.php');

$e_date = time();
$user_only = $_SESSION['logic_erp']["user_name"];
$user_id_id=$_SESSION['logic_erp']["user_id"];

extract( $_GET );

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Division
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$section_details = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$section_details[$row['id']] = $row['section_name'];
		}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Weekly Holiday
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$weekend_lists[$row['emp_code']] = $row['weekend'];
	}
//Govt Holiday 

//SELECT a.type, b.* from lib_holiday_details b, lib_holiday a where a.id=b.holiday_id
	$sql = "SELECT a.type, b.* from lib_holiday_details b, lib_holiday a  WHERE a.id=b.holiday_id and b.is_deleted = 0 ORDER BY b.holiday_date ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday_lists = array();
	$holiday_type = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday_lists[] = $row['holiday_date'];
		$holiday_type[$row['holiday_date']]=$row['type'];
	}
 
// Disciplinary Info
	function check_dsciplinary_info($emp_code, $current_date)
	{
		$sql = "SELECT * FROM hrm_disciplinary_info_mst WHERE emp_code=$emp_code and action_date>=$current_date and action_taken=1 ORDER BY id ASC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		while( $row = mysql_fetch_assoc( $result ) ) {
			if ( $row['withdrawn_date']=='0000-00-00')
			{
				$suspended="SP";
				return $suspended;
				exit();
			}
			else if($row['withdrawn_date']<=$current_date )
			{
				$suspended="SP";
				return $suspended;
				exit();
			}
		}
	}

// Movement Register
	$sql = "SELECT * FROM hrm_movement_register WHERE  is_deleted=0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$movement_register = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$movement_register[$row['emp_code']][$row['move_current_date']] = $row['move_current_date'];
	}

// Shift Details
	$shift_lists = array();
	$sql_shift = "SELECT * FROM lib_policy_shift";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$shift_lists[$row_shift[id]]['shift_start']=$row_shift[shift_start];
		$shift_lists[$row_shift[id]]['shift_end']=$row_shift[shift_end];
		$shift_lists[$row_shift[id]]['shift_type']=$row_shift[shift_type];
		$shift_lists[$row_shift[id]]['in_grace_minutes'] =add_time( $row_shift[shift_start], $row_shift[grace_minutes]); // Add In Grace Time with In Time
		$shift_lists[$row_shift[id]]['exit_buffer_minutes']=$row_shift[exit_buffer_minutes];
		
		$shift_lists[$row_shift[id]]['tiffin_start']=$row_shift[first_break_start];
		$shift_lists[$row_shift[id]]['tiffin_ends']=$row_shift[first_break_end];
		$shift_lists[$row_shift[id]]['dinner_start']=$row_shift[second_break_start];
		$shift_lists[$row_shift[id]]['dinner_ends']=$row_shift[second_break_end];
		
		$shift_lists[$row_shift[id]]['lunch_start']=$row_shift[lunch_time_start];
		$shift_lists[$row_shift[id]]['lunch_break_min']=datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]); 
		$shift_lists[$row_shift[id]]['lunch_break_ends']=$row_shift[lunch_time_end]; 
		
		$shift_lists[$row_shift[id]]['total_working_min']=datediff(n,$row_shift[shift_start],$row_shift[shift_end])-datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]);
		$shift_lists[$row_shift[id]]['early_out_start']=$row_shift[early_out_start];
		$shift_lists[$row_shift[id]]['entry_restriction_start']=$row_shift[entry_restriction_start];
	
	}

// OT POLICY Details
	$ot_policy = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$ot_policy[$row_shift[id]]['overtime_rate']=$row_shift[overtime_rate];
		$ot_policy[$row_shift[id]]['overtime_calculation_rule']=$row_shift[overtime_calculation_rule];
		$ot_policy[$row_shift[id]]['max_overtime']=$row_shift[max_overtime];
	}
	
 
$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$var_hrm_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$var_hrm_chart[$row['company_name']] = array();
	$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
	$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
	$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
	$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
	$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
	$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
	$var_hrm_chart[$row['company_name']]['dinner_ot_treatment'] = mysql_real_escape_string( $row['dinner_ot_treatment'] );
	$var_hrm_chart[$row['company_name']]['dinner_ot_treatment_time'] = mysql_real_escape_string( $row['dinner_ot_treatment_time'] );
}
/*
Used Short  Terms 
A=Absent, P=Present, D=Late/Delay, CL,ML,SL,EL...=All Leaves,SP=Suspended, H=Holiday, W=Weekend, R=Regular Day, MR=Movement Register

*/
// Processing Started Here

if ($action=="data_processing")
{    
	
	$txt_from_date_prev = add_date(convert_to_mysql_date( $txt_from_date ),-1);
	$txt_from_date = convert_to_mysql_date( $txt_from_date );
	$txt_to_date = convert_to_mysql_date( $txt_to_date );
	$txt_to_date_next = add_date(convert_to_mysql_date( $txt_to_date ),1);
	
	if ($cbo_company_name==0) $cbo_company_name="%%"; else $cbo_company_name=$cbo_company_name;
	if($cbo_location_name==0) $cbo_location_name="%%"; else $cbo_location_name=$cbo_location_name;
	
	$tot_days=daysdiff($txt_from_date, $txt_to_date);
	 
	
	if(trim($txt_emp_code)=="")  // New Procees
	{
		 $EditSqltmp="insert into hrm_raw_data_attnd (cid,dtime,ctime,purpose,entrytime,slno,processed,user_id,company_id,location_id,shift_policy_id,shift_type,is_next_day,shift_start,shift_end,graced_minutes,exit_buffer_minutes,entry_restriction_start) ( select cid,dtime,ctime,purpose,entrytime,slno,processed,user_id,a.company_id,a.location_id,a.shift_policy_id,a.shift_type,is_next_day,shift_start,shift_end,graced_minutes,exit_buffer_minutes,entry_restriction_start from hrm_raw_data_attnd_backup a where a.dtime between '$txt_from_date' and '$txt_to_date_next')"; 
		 
		$ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		
		$EditSqltmp="delete a.* from hrm_raw_data_attnd_backup a where  a.dtime between '$txt_from_date' and '$txt_to_date_next'"; 
		 $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		  
		$str_cond="where company_id like '$cbo_company_name' and location_id like '$cbo_location_name'";
		$str_raw=" and processed=0";
		 
		 $EditSqltmp="insert into hrm_attendance_raw ( select * from hrm_attendance a where a.attnd_date between '$txt_from_date' and '$txt_to_date')"; 
		 $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		
		 $EditSqltmp="delete from hrm_attendance where attnd_date between '$txt_from_date' and '$txt_to_date'"; 
		 $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
	}
	else    // Re-process
	{
		 $EditSqltmp="insert into hrm_raw_data_attnd (cid,dtime,ctime,purpose,entrytime,slno,processed,user_id,company_id,location_id,shift_policy_id,shift_type,is_next_day,shift_start,shift_end,graced_minutes,exit_buffer_minutes,entry_restriction_start) ( select cid,dtime,ctime,purpose,entrytime,slno,processed,user_id,a.company_id,a.location_id,a.shift_policy_id,a.shift_type,is_next_day,shift_start,shift_end,graced_minutes,exit_buffer_minutes,entry_restriction_start from hrm_raw_data_attnd_backup a , hrm_employee b where a.cid=b.punch_card_no and b.emp_code in ( $txt_emp_code )  and a.dtime between '$txt_from_date' and '$txt_to_date_next')"; 
		 
		 $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		 
		  $EditSqltmp="delete a.* from hrm_raw_data_attnd_backup a , hrm_employee b where a.cid=b.punch_card_no and b.emp_code in ( $txt_emp_code )  and a.dtime between '$txt_from_date' and '$txt_to_date_next'"; 
		  $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		  
		 $str_cond="where emp_code in ( $txt_emp_code ) and company_id like '$cbo_company_name' and location_id like '$cbo_location_name'";
		 $str_raw=" and processed=0"; 
		 
		 $EditSqltmp="insert into hrm_attendance_raw ( select * from hrm_attendance a where a.emp_code in ( $txt_emp_code ) and a.attnd_date between '$txt_to_date_next' and '$txt_to_date_next')"; 
		// $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		 
		// $EditSql="update hrm_attendance_raw set sign_in_time='',sign_out_time='',r_sign_in_time='',r_in_time_graced='',r_sign_out_time='',r_out_time_buffer='',total_over_time_min='', late_time_min='', status='',policy_shift_id='', is_questionable='',policy_overtime_id='',early_out_min='',is_get_tiffin_allowance='',is_get_night_allowance=''  ,is_next_day='' where attnd_date between '$txt_from_date' and '$txt_to_date'"; 
			 
	//	$ExeEditSql=mysql_db_query($DB,$EditSql);
		 
		 $EditSqltmp="delete from hrm_attendance where  attnd_date between '$txt_from_date' and '$txt_to_date' and emp_code in ($txt_emp_code)"; 
		  $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		  
		  
	}
	///echo "ss"; die;
 
	$sql_emp_abs="select * from hrm_employee $str_cond and shift_policy<>0  order by emp_code desc";  // where status_active=1 and is_deleted=0 and overtime_policy<>0
	$exe_sql_emp_abs=mysql_db_query($DB, $sql_emp_abs);
	$emp_rows=mysql_num_rows($exe_sql_emp_abs);
	
	while ($row_abs = mysql_fetch_array($exe_sql_emp_abs))
	{
		//Shift Get Current  In and Out Time Policy for This EMplie
		
		$r_in_time="";
		$r_out_time="";
		$r_shift_type="";
		$in_grace_minutes="";
		$exit_buffer_minutes="";
		$shift_id=$row_abs[shift_policy]; 
		$ot_pol_id=$row_abs[overtime_policy];
			
		 $r_in_time=$shift_lists[$shift_id]['shift_start'];
		 $r_out_time=$shift_lists[$shift_id]['shift_end'];
		 $r_shift_type=$shift_lists[$shift_id]['shift_type'];
		 $in_grace_minutes =$shift_lists[$shift_id]['in_grace_minutes']; // Add  Grace Time with In Time
		 $exit_buffer_minutes=$shift_lists[$shift_id]['exit_buffer_minutes'];
		
		for($i=1; $i<=$tot_days;$i++) // Loop for every days for each employee
		{
			$tmp_day_sts="";
			$is_regular_day=0;  // Set as not Regular working days
			
			$current_date=add_date($txt_from_date,$i-1);
			$weekday_name = date('l', strtotime($current_date));
		//echo $current_date."--".$weekday_name; die;
			// Check if Data Exist in Attendance Table

			$sql_check_attn="select * from hrm_attendance_raw where emp_code='$row_abs[emp_code]' and attnd_date='$current_date' ";			
			$exe_sql_check_attn=mysql_db_query($DB, $sql_check_attn);
			$row_attn=mysql_num_rows($exe_sql_check_attn);
			
			if ($row_attn<1) // if not Exist , Insert as new Row
			{
				
					// Leave checking here  (As per Day sts is Saved Here Checking First)
					$sql = "SELECT * FROM hrm_leave_transaction_details WHERE emp_code='$row_abs[emp_code]' and leave_date='$current_date' and is_deleted = 0 and is_locked= 0 ORDER BY emp_code ASC";
					
					$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
					if($row = mysql_fetch_array($result))
					{
						$is_regular_day=1; // Set as  Regular working days
						
						$tmp_day_sts = $row['leave_status'];
						if ($tmp_day_sts=="Holiday")
						{
							if ($holiday_type[$current_date]==0) $tmp_day_sts="GH"; else if ($holiday_type[$current_date]==1) $tmp_day_sts="CH"; else if ($holiday_type[$current_date]==2) $tmp_day_sts="FH";
							//$tmp_day_sts="GH";	
							$is_regular_day=0; // Set as  Regular working days
						}
						else if ($tmp_day_sts=="Weekend")
						{
							$tmp_day_sts="W";	
							$is_regular_day=0; // Set as  Regular working days
						}
					}
					
				// Govt Holi checking here
				
					else if ( in_array( $current_date, $holiday_lists ))
					{
						if ($holiday_type[$current_date]==0) $tmp_day_sts="GH"; else if ($holiday_type[$current_date]==1) $tmp_day_sts="CH"; else if ($holiday_type[$current_date]==2) $tmp_day_sts="FH";
						$is_regular_day=0; // Set as  Regular working days
					}
				// Weekly Holi checking here
					else if ( $weekday_name==$weekend_lists[$row_abs['emp_code']] )
					{
						$tmp_day_sts="W";
						$is_regular_day=0; // Set as  Regular working days
					}
				// Otherwise set as Absent days
					else
					{
						$tmp_day_sts="A";
						$is_regular_day=1; // Set as  Regular working days
					}
					if($tmp_day_sts=="A")
					{
						$tmp=check_dsciplinary_info($row_abs[emp_code], $current_date);
						if ($tmp!="")
						{
							$tmp_day_sts=$tmp;
							$tmp="";
							$is_regular_day=1; // Set as  Regular working days
						}
						
						$tmp=$movement_register[$row_abs[emp_code]][$current_date];  //check_movement_info($row_abs[emp_code], $current_date);
						if ($tmp!="")
						{
							$tmp_day_sts="MR";
							$tmp="";
							$is_regular_day=1; // Set as  Regular working days
						}
					}
					//echo "$row_abs[name]"."$row_abs[status_active]"."<br>";
					//echo  $tmp_day_sts; die;
					if ($row_abs[status_active]==0) // In active Employee
					{
						$d="separated_from";
						$dd="hrm_separation";
						$ddd="emp_code='$row_abs[emp_code]' order by id desc limit 1";
						$old_mod= return_field_value($d,$dd,$ddd);
						
						if (strtotime($old_mod)>strtotime($current_date))
						{	
							if($row_id=="")  $row_id = return_next_id( "id", "hrm_attendance_raw" ); else $row_id=$row_id+1;
							 
							
							$sql = "INSERT INTO hrm_attendance_raw (id,emp_code,attnd_date,r_sign_in_time,r_in_time_graced,r_sign_out_time,r_out_time_buffer,status,is_regular_day,policy_shift_id,policy_overtime_id,roster_policy_id,is_manually_updated,company_id,location_id,division_id,department_id,section_id,subsection_id,designation_id,weekday_name)			
							 VALUES ('$row_id','$row_abs[emp_code]','$current_date','$r_in_time11','$in_grace_minutes11','$r_out_time11','$exit_buffer_minutes11','$tmp_day_sts','$is_regular_day','$shift_id11','$row_abs11[overtime_policy]','$roster',0,'$row_abs[company_id]','$row_abs[location_id]','$row_abs[division_id]','$row_abs[department_id]','$row_abs[section_id]','$row_abs[subsection_id]','$row_abs[designation_id]','$weekday_name')";
							 mysql_query( $sql ) or die (mysql_error());
						}
					}
					else  // Regular Employee
					{
					 	
						if (strtotime($row_abs[joining_date])<=strtotime($current_date))
						{
							/*$id_field_name = "id";
							$table_name = "hrm_attendance";
							$row_id = return_next_id( $id_field_name, $table_name );*/
							if($row_id=="")  $row_id = return_next_id( "id", "hrm_attendance_raw" ); else $row_id=$row_id+1;
							
							$sql = "INSERT INTO hrm_attendance_raw(id,emp_code,attnd_date,r_sign_in_time,r_in_time_graced,r_sign_out_time,r_out_time_buffer,status,is_regular_day,policy_shift_id,policy_overtime_id,roster_policy_id,is_manually_updated,company_id,location_id,division_id,department_id,section_id,subsection_id,designation_id,weekday_name)			
								 VALUES ('$row_id','$row_abs[emp_code]','$current_date','$r_in_time11','$in_grace_minutes11','$r_out_time11','$exit_buffer_minutes11','$tmp_day_sts','$is_regular_day','$shift_id11','$row_abs11[overtime_policy]','$roster',0,'$row_abs[company_id]','$row_abs[location_id]','$row_abs[division_id]','$row_abs[department_id]','$row_abs[section_id]','$row_abs[subsection_id]','$row_abs[designation_id]','$weekday_name')";
							 mysql_query( $sql ) or die (mysql_error());
						}
						  //echo "sumon--".$sql;die;
					}
					
				 $tmp_day_sts="";
				 $is_regular_day=""; 
				 $weekday_name="";
				
			}// No Updates is available for Attendance here
		
		}
		//echo $tot_days;
		//exit();
	}
	 
 
	 
	//Blank -Make All Employee available to Attendance Table End
 	
	//Check raw Data to update daily atttendance with time starts
	 
$sql_emps="select * from hrm_employee $str_cond and shift_policy<>0   order by emp_code asc";   //and overtime_policy<>0
$exe_sql_emps=mysql_db_query($DB, $sql_emps);
$rowsss=mysql_num_rows($exe_sql_emps);
$t=1;
	
while ($r_emps_data = mysql_fetch_array($exe_sql_emps))
{	
	$m_last_punch_time_night=$ot_policy[$r_emps_data[overtime_policy]]['max_overtime'];
	if($m_last_punch_time_night=="") $m_last_punch_time_night="01:00:00";
	
	$m_overtime_calculation_rule=  $ot_policy[$r_emps_data[overtime_policy]]['overtime_calculation_rule'];
	if ($m_overtime_calculation_rule=="")$m_overtime_calculation_rule=1;
	$tot_days=$tot_days+1;
	for($r=1; $r<=$tot_days; $r++)
	{	
		$attnd_date =add_date($txt_from_date,$r-1); //change_date_for_process(add_date($txt_from_date,$r-1));
 		
		$sql_id_emp_prs="select * from hrm_raw_data_attnd where dtime='$attnd_date' $str_raw  and cid='$r_emps_data[punch_card_no]' order by ctime asc";
		
		$exe_sql_id_emp_prs=mysql_db_query($DB, $sql_id_emp_prs);
		$rowss=mysql_num_rows($exe_sql_id_emp_prs);
 		 
		while ($r_id_prs = mysql_fetch_array($exe_sql_id_emp_prs))
		{	
			$r_in_time=$r_id_prs[shift_start];
			$r_out_time=$r_id_prs[shift_end];
			$is_next_day=$r_id_prs[is_next_day];
			$r_shift_type=$r_id_prs[shift_type];
			$r_in_graced_time =add_time( $r_id_prs[shift_start], $r_id_prs[graced_minutes]); // Add In Grace Time with In Time
			$r_in_grace_minutes= $r_id_prs[graced_minutes];
			$r_exit_buffer_minutes=$r_id_prs[exit_buffer_minutes];
			
			$r_tiffin_start=$shift_lists[$r_id_prs[shift_policy_id]]['tiffin_start'];
			$r_tiffin_ends=$shift_lists[$r_id_prs[shift_policy_id]]['tiffin_ends'];
			$r_dinner_start=$shift_lists[$r_id_prs[shift_policy_id]]['dinner_start'];
			$r_dinner_ends=$shift_lists[$r_id_prs[shift_policy_id]]['dinner_ends']; 
			$r_early_out_start=$shift_lists[$r_id_prs[shift_policy_id]]['early_out_start'];
			$r_lunch_start=$shift_lists[$r_id_prs[shift_policy_id]]['lunch_start'];
			$max_in_punch_time_allowed=$shift_lists[$r_id_prs[shift_policy_id]]['entry_restriction_start'];
			 
			$max_in_time_buffer_before=datediff(n,$m_last_punch_time_night,$r_in_time);
			$max_day_close_time="23:59:59";
			
			$tmp_log_time=$r_id_prs[ctime]; //change_time_for_process();
			$tmp_log_date= $attnd_date;
			// echo  $tmp_log_time." ".$tot_days;
			$is_prev_day_log=0;
			
	
			
			if ($is_next_day==0)  // Out and in time in same date 
			{
				 
				if ( $tmp_log_time>$m_last_punch_time_night and $tmp_log_time<=$max_day_close_time ) 
				{
					$timeDiffin=datediff(n,$r_in_time,$tmp_log_time);
					if ($timeDiffin >=-$max_in_time_buffer_before and $timeDiffin<=$max_in_punch_time_allowed) 
					{
						$gen_in_time=$tmp_log_time;
						
						if ($timeDiffin>$r_in_grace_minutes)
						{
							$gen_late_time=$timeDiffin; //+$r_in_grace_minutes;
							$timeDiffin="";
						}
					}
					 
					else //if ($timeDiffout<=120 or $timeDiffout>=120) // Regular Out Time Setting Here
					{
						$gen_out_time=$tmp_log_time;
						
						if ($r_dinner_ends<=$gen_out_time)
						{
							$get_tiffin_allowance=1;
							$get_dinner_allowance=1;
						}
						elseif ($r_tiffin_ends<=$gen_out_time)
						{
							$get_tiffin_allowance=1;
							$get_dinner_allowance=0;
						}
						else
						{
							$get_tiffin_allowance=0;
							$get_dinner_allowance=0;
						}
						
						$timeDiffout=datediff(n,$r_out_time,$tmp_log_time);
						if ($timeDiffout>0)
						{
							$gen_ot_time=$timeDiffout;
						}
						else
						{
							$gen_early_out=abs($timeDiffout);
						}
					}
				}
				else  // Mid Night Punch // Out time for Previous Day
				{
					$gen_out_time=$tmp_log_time;
					$timeDiffout=datediff(n,$r_out_time,$max_day_close_time);
					$p_time="00:00:00";
					$timeDiffout=$timeDiffout+datediff(n,$p_time,$tmp_log_time);
					$get_tiffin_allowance=1;
					$get_dinner_allowance=1;
				//	echo $get_tiffin_allowance." sas ".$get_dinner_allowance."  ".$gen_out_time; die;
					if ($timeDiffout>0)
					{
						$gen_ot_time=$timeDiffout;
					}
					else
					{
						$gen_early_out=abs($timeDiffout);
					}
					$is_prev_day_log=1;
				}
			}
			else  // Next Day	starts
			{
				$timeDiffin=datediff(n,$r_in_time,$tmp_log_time);
				
				if ($timeDiffin >=-$max_in_time_buffer_before and $timeDiffin<=$max_in_punch_time_allowed) 
				{
					$gen_in_time=$tmp_log_time;
					if ($timeDiffin>$r_in_grace_minutes)
					{
						$gen_late_time=$timeDiffin; //+$r_in_grace_minutes;
						$timeDiffin="";
					}
				}
				else //if ($timeDiffout<=120 or $timeDiffout>=120) // Regular Out Time Setting Here
				{
					$gen_out_time=$tmp_log_time;
					$is_prev_day_log=1;
					$timeDiffout=datediff(n,$r_out_time,$tmp_log_time);
					if ($timeDiffout>0)
					{
						$gen_ot_time=$timeDiffout;
					}
					else
					{
						$gen_early_out=abs($timeDiffout);
					}
				}
			}// Next Day ends
			//echo $gen_ot_time."<br/>".$attnd_date."=";	
			$att_date=$attnd_date; //change_date_for_process_sql($attnd_date);

			// Start Searching Prev Attenandance in Att Table
			if ($is_prev_day_log==0)
			{	
				$sql_raw_attendance="select * from hrm_attendance_raw where emp_code='$r_emps_data[emp_code]' and  attnd_date='$att_date' ";
			}
			else
			{
				$p_date=add_date($att_date,-1);
				$sql_raw_attendance="select * from hrm_attendance_raw where emp_code='$r_emps_data[emp_code]' and attnd_date='$p_date' ";
			}
			$exe_sql_raw_attendance=mysql_db_query($DB, $sql_raw_attendance);
			$row_num=mysql_num_rows($exe_sql_raw_attendance);
			$row_data_prs = mysql_fetch_array($exe_sql_raw_attendance);
			
			if ($row_num!=0)
			{	
				$prev_in_time=$row_data_prs[sign_in_time];
				$prev_late_min=$row_data_prs[late_time_min];
				
				$prev_out_time=$row_data_prs[sign_out_time];
				$prev_ot_min=$row_data_prs[total_over_time_min];
				$prev_status=$row_data_prs[status];
				// echo "iid: $r_id_prs[cid]  $is_prev_day_log GEn In: $gen_in_time Prev In : $prev_in_time GEn Out: $gen_out_time Prev Out : $prev_out_time  Gen OT $gen_ot_time \n ";
				if ($gen_in_time!="")
				{
					if ($gen_in_time>$prev_in_time && $prev_in_time!="00:00:00")
					{
						$gen_in_time=$prev_in_time;
						$gen_late_time=$prev_late_min;
						$status="P";
					}
					$gen_out_time=$prev_out_time;
					$status="P";
				}
				else   // here to check double out time
				{ 
					$gen_in_time=$prev_in_time;
					$gen_late_time=$prev_late_min;
					$status="P";
				}
				
				if ($gen_late_time>$r_in_grace_minutes)
				{
					$status="D";
				}
				else
				{
					$gen_late_time=0;
				}
				//Set Questionable start
				if ($prev_in_time!="00:00:00" and  $gen_out_time!="00:00:00")
				{
					$is_questionable=0;
				}
				else if ($prev_out_time!="00:00:00" and  $gen_in_time!="00:00:00")
				{
					$is_questionable=0;
				}
				else
				{
					$is_questionable=1;
				}
				
				// Calculate OT
				//echo $row_data_prs[is_regular_day];
				//if ($r_emps_data[ot_entitled]==0) $gen_ot_time=0;
				
				if ($r_emps_data[ot_entitled]==1)
				{
					if ($is_questionable==0) // Regular Present 
					{
						if ($row_data_prs[is_regular_day]==1) // Regular working Day
						{
							
							if ( $m_overtime_calculation_rule==0)  // OT Rules and Regulations
							{
								
								if($is_prev_day_log==0) // Current Date
								{
									
									$gen_ot_time=(datediff(n, $gen_in_time,$gen_out_time)-($shift_lists[$r_id_prs[shift_policy_id]]['total_working_min']-$shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min'])); 
									//echo $gen_ot_time."ss".$shift_lists[$r_emps_data[shift_policy]]['total_working_min'];die;
								}
								else   // Date Crossover OT
								{
									$timeDiffout=datediff(n,$gen_in_time,$max_day_close_time);
									$p_time="00:00:00";
									$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
									$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['total_working_min']+$shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
								}
								 
							} 
							else $gen_ot_time=$gen_ot_time;																								
						}
						else // Not Regular Day -- Holiday
						{
							if ( $m_overtime_calculation_rule==0)  // OT Rules and Regulations
							{
								if($is_prev_day_log==0) // Current Date
								{
									$gen_ot_time=datediff(n, $gen_in_time,$gen_out_time)-$shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min'];
								}
								else
								{
									$timeDiffout=datediff(n,$gen_in_time,$max_day_close_time);
									$p_time="00:00:00";
									$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
									$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
								} 
							}
							else // OT Rules 2nd
							{
								if($is_prev_day_log==0) // Current Date
								{
									if ($gen_out_time<=$shift_lists[$row_shift[id]]['lunch_start'])
									{
										$gen_ot_time=datediff(n, $r_in_time,$gen_out_time);
									}
									else if ($gen_out_time>=$shift_lists[$r_id_prs[shift_policy_id]]['lunch_start'] && $gen_out_time<=$shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_ends'])
									{
										$gen_ot_time=datediff(n, $r_in_time,$shift_lists[$r_id_prs[shift_policy_id]]['lunch_start']);
									}
									else
										$gen_ot_time=datediff(n, $r_in_time,$gen_out_time)-$shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min'];
								}
								else
								{
									$timeDiffout=datediff(n,$r_in_time,$max_day_close_time);
									$p_time="00:00:00";
									$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
									$gen_ot_time=$timeDiffout-($shift_lists[$r_id_prs[shift_policy_id]]['lunch_break_min']);
								} 
							}
							//$gen_ot_time=$gen_ot_time;	
						}
					}
					else // Questionalbe
					{
						if ( $m_overtime_calculation_rule==0 )
						{
							$gen_ot_time=0;
						}
						else
						{
							if ($row_data_prs[is_regular_day]==1) // Regular working Day
							{
								$gen_ot_time=$gen_ot_time;
							}
							else $gen_ot_time=0;
						}
					} // OT Ends
				}
				else  // Not OT Entitled
				{
					 $gen_ot_time=0;
				}
				
				if ($get_dinner_allowance==1 && $gen_ot_time!=0)
				{     
					if ($var_hrm_chart[$r_id_prs['company_id']]['dinner_ot_treatment']!=0)
					{
						if ($var_hrm_chart[$r_id_prs['company_id']]['dinner_ot_treatment']==1) // Add OT
							$gen_ot_time=$gen_ot_time+$var_hrm_chart[$r_id_prs['company_id']]['dinner_ot_treatment_time']; 
						 else 
							$gen_ot_time=$gen_ot_time-$var_hrm_chart[$r_id_prs['company_id']]['dinner_ot_treatment_time']; 	 
					}
				}
				
			  	//echo $gen_ot_time."--".$exit_buffer_minutes." \n";
				
				if ( $exit_buffer_minutes >= $gen_ot_time ) $gen_ot_time=0;
				
				if ($prev_status=="W" || $prev_status=="GH" || $prev_status=="CL" || $prev_status=="SL" || $prev_status=="ML" || $prev_status=="EL" || $prev_status=="LWP" || $prev_status=="SpL" || $prev_status=="EdL" || $prev_status=="MR" ) $status=$prev_status;
				
				if ($gen_ot_time=="" or $gen_ot_time<0) $gen_ot_time=0;
				 
				if ($gen_late_time=="") $gen_late_time=0;
				 
				if ($gen_early_out=="") $gen_early_out=0;
				
				$sid=$row_data_prs[id];
			  // echo $gen_ot_time."--";
				$EditSql="update hrm_attendance_raw set sign_in_time='$gen_in_time',sign_out_time='$gen_out_time',r_sign_in_time='$r_in_time',r_in_time_graced='$r_in_graced_time',r_sign_out_time='$r_out_time',r_out_time_buffer='$r_exit_buffer_minutes',total_over_time_min='$gen_ot_time', late_time_min='$gen_late_time', status='$status',policy_shift_id='$r_id_prs[shift_policy_id]', is_questionable='$is_questionable',policy_overtime_id='$r_emps_data[overtime_policy]',early_out_min='$gen_early_out',is_get_tiffin_allowance='$get_tiffin_allowance',is_get_night_allowance='$get_dinner_allowance'  ,is_next_day='$is_prev_day_log' where id='$row_data_prs[id]'"; 
				// echo  "as".$EditSql; die;
				$ExeEditSql=mysql_db_query($DB,$EditSql);
				
				$gen_in_time="";
				$gen_out_time="";
				$gen_ot_time="";
				$gen_late_time="";
				$prev_in_time="";
				$prev_out_time="";
				$status="";
				$is_questionable=""; 
				$gen_early_out="";
				$get_tiffin_allowance="";
				$get_dinner_allowance="";
			}// Start Searching Proev Attenandance in Att Table
		}// while loop r_id_prs
	} // for loop end
 	$t=$t+1;
}// loop for employee */
	
	 $EditSqltmp="insert into hrm_attendance (emp_code,attnd_date,sign_in_time,sign_out_time,is_next_day,r_sign_in_time,r_in_time_graced,r_sign_out_time,r_out_time_buffer,sign_out_time_slot_1,sign_out_time_slot_2,sign_out_time_slot_3,sign_out_time_slot_4,sign_out_time_slot_5,status,late_time_min,early_out_min,is_regular_day,total_over_time_min,over_time_slot_1_min,over_time_slot_2_min,over_time_slot_3_min,over_time_slot_4_min,over_time_slot_5_min,is_get_tiffin_allowance,is_get_night_allowance,today_tiffin_allowance,today_night_allowance,lunch_break_min,is_questionable,remarks,weekday_name,policy_shift_id,policy_overtime_id,roster_policy_id,is_manually_updated,company_id,location_id,division_id,department_id,section_id,subsection_id,designation_id) (select emp_code,attnd_date,sign_in_time,sign_out_time,is_next_day,r_sign_in_time,r_in_time_graced,r_sign_out_time,r_out_time_buffer,sign_out_time_slot_1,sign_out_time_slot_2,sign_out_time_slot_3,sign_out_time_slot_4,sign_out_time_slot_5,status,late_time_min,early_out_min,is_regular_day,total_over_time_min,over_time_slot_1_min,over_time_slot_2_min,over_time_slot_3_min,over_time_slot_4_min,over_time_slot_5_min,is_get_tiffin_allowance,is_get_night_allowance,today_tiffin_allowance,today_night_allowance,lunch_break_min,is_questionable,remarks,weekday_name,policy_shift_id,policy_overtime_id,roster_policy_id,is_manually_updated,company_id,location_id,division_id,department_id,section_id,subsection_id,designation_id from hrm_attendance_raw)"; 
	  $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
				 
		 
 				 $EditSqltmp="insert into hrm_raw_data_attnd_backup (cid,dtime,ctime,purpose,entrytime,slno,processed,user_id,company_id,location_id,shift_policy_id,shift_type,is_next_day,shift_start,shift_end,graced_minutes,exit_buffer_minutes,entry_restriction_start) (select cid,dtime,ctime,purpose,entrytime,slno,processed,user_id,company_id,location_id,shift_policy_id,shift_type,is_next_day,shift_start,shift_end,graced_minutes,exit_buffer_minutes,entry_restriction_start from hrm_raw_data_attnd)"; 
				 $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
		 
				 $EditSqltmp="delete from hrm_attendance_raw"; 
				 $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
				 
				 $EditSqltmp="delete from hrm_raw_data_attnd"; 
				 $ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
				 
				 
				 
				 
				 
				 
	//Check raw Data to update daily atttendance with time ends
	echo "2";
	exit();  
}

// Functions required for this processing
function daysdiff($date1,$date2) 
{ 
	$diff = (strtotime($date2) - strtotime($date1));
	
	$days = floor($diff / (1*60*60*24)+1);
	return $days; 		
}

	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

function add_time($event_time,$event_length)
{
	$timestamp = strtotime("$event_time");
	$etime = strtotime("+$event_length minutes", $timestamp);
	$etime=date('H:i:s', $etime);
	return $etime;

}




function change_date_for_process( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "-", $date_field );
		return $dates[1] . "/" . $dates[2] . "/" . $dates[0];
	}
	else return $date_field;
}
function change_date_for_process_sql( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "/", $date_field );
		return $dates[2] . "-" . $dates[0] . "-" . $dates[1];
	}
	else return $date_field;
}
function change_time_for_process( $time_field ) {
	if( $time_field != '' ) {
		$times = explode( "-", $time_field );
		return $times[0] . ":" . $times[1] . ":" . $times[2];
	}
	else return $time_field;
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>