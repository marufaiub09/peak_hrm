<?php
date_default_timezone_set('UTC');
include('../../../includes/common.php');
include('../../../includes/array_function.php');
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>

<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

	<link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
	
<script src="../../../resources/jquery_ui/jquery-1.4.4.min.js" type="text/javascript"></script>
<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
	<script>
	
		function openmypage(page_link,title)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=690px,height=380px,center=1,resize=0,scrolling=0', '../../')
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
			   
				emp_search_descipline(theemail.value,1);
			 	//alert(theemail.value);
				setTimeout('showResult_search(document.getElementById("txt_emp_code").value,"s",2,"movement_list_container")',150);

			}
		}
		
	</script>

</head>

<body style="font-family:verdana; font-size:11px;" onload="">
<div align="center" style="width:900px;">
	<div style="height:40px; width:100%; margin-bottom:10px;">
		Movement Register
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<!-- Start Field Set -->
	
	<fieldset style="width:900px ">
	<legend>HRM Module</legend>
	<!-- Start Form -->
	
	<form name="movement_register" id="movement_register" autocomplete="off" method="" action="javascript:fnc_movement_register()" onSubmit="">
			<!-- Start Main Table -->
			<table cellpadding="0" border="0" cellspacing="0" width="100%">
			
			<!-- monzu -->
			
			
			<tr>
			<td colspan="6" align="center">
			<!-- monzu -->	
				<fieldset>
				<table cellpadding="0" border="0" cellspacing="2">
					<tr>
						<td width="110" >
							Emp Code
						</td>
						<td width="150"> <!-- Select Calander -->
						 <input name="txt_emp_code" id="txt_emp_code" placeholder="Double Click to Search" ondblclick="openmypage('search_employee.php','Employee Info'); return false" readonly="true" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Name
						</td>
						<td width="150"> <!--  System Generated-->
						<input name="txt_emp_name" id="txt_emp_name" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Designation
						</td>
						<td width="150"> <!--Entry -->
						  <input name="txt_designation" id="txt_designation" class="text_boxes" style="width:155px">
						</td>
					</tr>
					
				
				<tr>
						<td width="110" height="24" >
							Company						</td>
						<td width="150"> <!--  Entry-->
						  <input name="txt_company" id="txt_company" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Division
						</td>
						<td width="150"> <!-- Select & Display-->
						<input name="txt_division" id="txt_division" class="text_boxes" style="width:155px">
						</td>
						<td width="110">
							Department
						</td>
						<td width="150"> <!--  Select-->
						  <input name="txt_department" id="txt_department" class="text_boxes" style="width:155px">					
				  </td>
				  </tr>
				
				<tr>
						<td width="110" >
						Section 
						</td>
						<td width="150"> <!--  Select-->
						  <input name="txt_section" id="txt_section" class="text_boxes" style="width:155px">
						</td>
						<td width="110" >
						Sub Section 
						</td>
						<td width="150"> <!--  Select-->
						<input name="txt_sub_section" id="txt_sub_section" class="text_boxes" style="width:155px">
						</td>
						<td width="110" >
						<input type="hidden" name="save_up" id="save_up" />
						</td>
						<td width="150"> <!--  Select-->
						
						</td>
				  </tr>
				  
				
				
				
				</table>
				</fieldset>
			</td>
				</tr>
			
			
			<tr>
			<td height="10px" colspan="6"></td>
			</tr>
			<tr>
				<td colspan="6" align="center">
					<fieldset>
					<table width="620" cellpadding="0" cellspacing="2">
						<tr>
							<td width="175" align="left">
								From Date
							</td>
							<td width="150"> <!--  Select-->
							 	<input name="txt_from_date" id="txt_from_date" class="text_boxes" style="width:126px"> 
								<input type="hidden" name="save_up_move" id="save_up_move" />
								<script type="text/javascript">
									$( "#txt_from_date" ).datepicker({
												dateFormat: 'dd-mm-yy',
												changeMonth: true,
												changeYear: true
											});
								//	alert(document.getElementById('lastname').value)
								</script>
							</td>
							<td width="175"  >
								To Date
							</td>
							<td width="150"> <!--  Select-->
								<input name="txt_to_date" id="txt_to_date" class="text_boxes" style="width:126px">
								<script type="text/javascript">
									$( "#txt_to_date" ).datepicker({
												dateFormat: 'dd-mm-yy',
												changeMonth: true,
												changeYear: true
											});
								//	alert(document.getElementById('lastname').value)
								</script>
							</td>
							
					  </tr>
					
					<tr>
						<td width="150">
						Movement Purpose
						</td>
						<td colspan="3"> <!--  Select-->
						  <textarea name="txt_movement_purpose"  id="txt_movement_purpose" class="text_area" style="width:439px;"></textarea>
						</td>
						
				  </tr>
				  <tr>
				  	<td width="150"> <!--  Select-->
						Destination/Place
						</td>
						<td colspan="3" >
						<input type="text" name="txt_destination" id="txt_destination" class="text_boxes" style="width:437px" />
						</td>
				  </tr>
				<tr>
					<td width="175" align="left">
								Start Time
							</td>
							<td width="150"> <!--  Select-->
							 	<input name="txt_start_time" id="txt_start_time" class="text_boxes" style="width:126px">
							
							 	<script type="text/javascript">
									$('#txt_start_time').timepicker({
										ampm: true
									});
								</script>
							</td>
							<td width="175"  >
								Return Time
							</td>
							<td width="150"> <!--  Select-->
								<input name="txt_return_time" id="txt_return_time" class="text_boxes" style="width:126px">
								<script type="text/javascript">
									$('#txt_return_time').timepicker({
										ampm: true
									});
								</script>
							</td>
				  </tr>
				
				<tr>
						<td width="150">
						Adviced By
						</td>
						<td >
						  <input name="txt_adviced_by" id="txt_adviced_by" placeholder="Double Click Here" class="text_boxes" style="width:126px">
						</td>
						<td width="150">
						Entry Date
						</td>
						<td width="170"> <!--  Select-->
						  <input name="txt_entry_date" id="txt_entry_date" class="text_boxes" style="width:126px">
						  <script type="text/javascript">
									$( "#txt_entry_date" ).datepicker({
												dateFormat: 'yy-mm-dd',
												changeMonth: true,
												changeYear: true
											});
								//	alert(document.getElementById('lastname').value)
								</script>
						</td>
						
				  </tr>
				
						<tr>
							<td colspan="4" height="40" valign="middle" align="center">
								<input type="submit" value=" Save" name="Save" id="Save" style="width:100px" class="formbutton"/>&nbsp;
								<input type="reset" value="  Refresh  " name="add" id="close" style="width:100px" class="formbutton"/>	
							</td>
						</tr>
						</table>
						</fieldset>
					</td>
					</tr>
					<tr>
						<td align="center">
							<fieldset id="movement_list_container" style="margin-top:10px" >
							
							</fieldset>
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
	</div>
</body>
</html>
