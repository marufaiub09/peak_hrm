
<?php
/*######################################

	Completed By
	Name : Md.Ekram Hossain
	Date : 13-12-2012
		
######################################*/

include('../../../includes/common.php');
//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}

 
	/*$sql = "select a.id as mstid,a.emp_code,a.new_salary_grade,a.new_salary_rule,a.new_gross_salary,b.* from hrm_increment_mst a,hrm_increment_dtls b where a.id=b.mst_id and a.is_approved=2 and a.initial=0 and a.effective_date= '2014-07-01' order by b.id asc";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{ 
		$val=0;
		$val=( $row[new_gross_salary]*$row[percentage_formula] )/100;
		//echo "==".$val;
		 mysql_query("update hrm_increment_dtls set per_head_increment='$val' where id=$row[id] ");
	}*/
	
	/*query for leave update
	 $sql="select emp_code,Amount from sheet1 "; 
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$results="update hrm_employee_salary_bank set salary_amount='$row[Amount]' where emp_code='$row[emp_code]'";
		$shows=implode('',$results);
		$total=mysql_query( $shows );
	}*/
	
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Raw data update</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Logic Payroll Software" />
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
   	<script src="includes/ajax_submit_attendance.js" type="text/javascript"></script>
  
    <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css">
    <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>

    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
<script>
		
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
			$('.timepicker').timepicker({
				timeFormat:'hh:mm:ss',
				addSliderAccess: true,
				sliderAccessArgs: { touchonly: false }				
			});
	});
		
	function openpage_searchemp(page_link,title)
		{
			
			var company=document.getElementById('cbo_company_name').value;
			var location=document.getElementById('cbo_location_name').value;
			if(company==0 || company==''){alert("Please Select The Company.");return false;}
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?company='+company+'&location='+location, title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");				
				$('#txt_emp_code').val(thee_id.value);
				//punch_card_no	
			}
		}
	
	function fn_raw_data_update()
	{
		
	
		var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
		var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
		var shift_policy 		= escape(document.getElementById('shift_policy').value);
		var shift_start 		= escape(document.getElementById('shift_start').value);
		var shift_end 			= escape(document.getElementById('shift_end').value);
 		var txt_date 			= escape(document.getElementById('txt_date').value);
		var txt_emp_code 		= escape(document.getElementById('txt_emp_code').value);
		
		var skillsSelect = document.getElementById("shift_policy");
		var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
		var txt_to_date='';
		//var selected_dates=document.getElementById('txt_date').value;
 		//alert(selected_dates);
		//if ($('#change_process_shift').is(":checked")) var change_process_shift=1;
		//else var change_process_shift=0;
		if ($('#chk_attendance_table').attr('checked')=="checked")
			var chk_attendance_table=1
		else
			var chk_attendance_table=0; 
		 
 		if($('#cbo_company_name').val()==0)
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_name').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if($('#shift_policy').val()==0)
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#shift_policy').focus();
				$(this).html('Please Select Shift').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if($('#txt_date').val()=="")
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_date').focus();
				$(this).html('Please Select Date').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else{
				
				if(confirm(" Read carefully the following points \n\n#You are going to change the shift time of shift  "+selectedText+" on Date "+txt_date+"\n#System will delete previous Processed Attn. Data of selected date if any.\n# After Raw Data update, Attn. Process is must for selected date.\n\n Press Ok for Continue Otherwise Cancel"))
					
				{
					$("#process_anim").html('<img src="../../../images/loading2.gif" />').fadeIn(1000);
					nocache = Math.random();
					http.open('get','includes/raw_data_process.php?action=raw_data_update'+
								'&cbo_location_name='+cbo_location_name+
								'&cbo_company_name='+cbo_company_name+
								'&shift_policy='+shift_policy+
								'&txt_date='+txt_date+
								'&shift_start='+shift_start+
								'&shift_end='+shift_end+						
								'&txt_emp_code='+txt_emp_code+
								'&chk_attendance_table='+chk_attendance_table+
								'&txt_to_date='+txt_to_date
								 );
								//'&change_process_shift='+change_process_shift+
					http.onreadystatechange = raw_data_process_reply;
					http.send(null); 
				}
			 
			 
			
		}
	}

function raw_data_process_reply() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Raw Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
				$(this).fadeOut(3000);
			});
		}
		else if(response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Error!!! Raw Data Not Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
				$(this).fadeOut(3000);
			});
		}
		else if (response==4)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('The Period You Selected Is Locked.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				$(this).css('background-color', 'red');
				$(this).fadeOut(3000);
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Database is Busy, please try after some time.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				$(this).css('background-color', 'red');
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		
 	}
}		
		

function fn_shift_policy_info(shift_policy)
{
	
	http.open('post','includes/raw_data_process.php?action=shift_policy_info&shift_policy='+shift_policy);
	http.onreadystatechange=function(){
		eval(http.response);
	}
	http.send(null);
}

//new ekram
	

function generate_list_view(selected_id){
		
		$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&period=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});
		}	
		
	
		
</script>
 </head>
 <body style="font-family:verdana; font-size:11px;">
	 <div  style="width:850px;">

<div style="width:530px;float:left">   
    	<div style="width:530px;">
            <div class="form_caption">
            	Raw Data Update
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update;?>		
            </span>
         </div>   
         <div id="messagebox" style="background-color:#FF9999; color:#000000;width:530px" align="center" ></div>
 	<form id="raw_data_update_form"  method="post">
		<fieldset style="width:500px;">
			<legend>Raw Data </legend>
			<!--<div align="left"><strong>Note: Please At first check and update shift policy before process.</strong></div>-->
		  <table  border="0" cellpadding="0" cellspacing="1" width="450">
            	<tr>
					<td> Select Company Name</td>
					<td>
						<select  name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:250px ">
                        	<? if($company_cond=="")
							   { 
								?> 
								<option value="0">--- Select Company Name ---</option>
							<?
							   }
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 $company_cond order by company_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td> Select Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Location Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
                <tr>
                	<td height="20">Shift Policy</td>
                    <td height="20">
                        <select name="shift_policy" id="shift_policy" class="combo_boxes" style="width:250px;" onchange="fn_shift_policy_info(this.value)">
                                <option value="0">---Select---</option>
								<?php  foreach( $shift_policy as $key=>$value ):?>
                                    <option value=<? echo "$key"; ?>> <? echo "$value" ;?> </option>				
                                <?php endforeach; ?> 
                         </select>
                    </td>
                </tr>
                <tr>
                  <td height="20">Shift Start</td>
                  <td height="20"><input type="text" name="shift_start" id="shift_start" value="" class="timepicker text_boxes" style="width:110px;" />
                  <input type="checkbox" name="chk_attendance_table" id="chk_attendance_table" />Apply to Not Punch Employee
                  
                  </td>
                </tr>
                <tr>
                  <td height="20">Shift End</td>
                  <td height="20"><input type="text" name="shift_end" id="shift_end" value="" class="timepicker text_boxes" style="width:110px;" /></td>
                </tr>
                <tr>
                	<td>Date</td>
                    <td>
                    	<input type="text" name="txt_date" id="txt_date" class="datepicker" style="width:110px;" placeholder="Select Date"  onchange="generate_list_view(this.value)"/>
                     </td>
                </tr>
                 
                <!--<tr>
                	 <td>Change On Process Shift policy</td>
                     <td><input type="checkbox" name="change_process_shift" id="change_process_shift" /></td>
                </tr>-->
                
            	<tr>
                	
                    <td>Search Employee</td>
                    <td>
                    	<input name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:250px " placeholder="Double Click To Search" readonly="readonly" ondblclick="openpage_searchemp('../../search_employee_multiple_by_id.php','Search Employee')" >
                        <input type="hidden" id="punch_card_no" value="" /> 
                  </td>
                    
                  </tr>
                 <tr>
					<td colspan="2" height="10"></td>
				</tr>
                 
                <tr>
					<td colspan="2" align="center" class="button_container">
                    	<input type="button" id="data_process" value="Raw Data Processing" style="width:270px; height:45px" class="formbutton" onclick="fn_raw_data_update()" />
                    </td>
				</tr>
            </table>
 			<div id="process_anim"></div>
	</fieldset>
	</form>
</div>
  <div align="center" style="font-size:18px; width:300px; position:relative; height:40px; margin:5px 0;"></div>	
    <div id="data_panel"  class="demo_jui" style="margin-top:10px; width:300px; float:right"></div>
</div>
</body>
</html>