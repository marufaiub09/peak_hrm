<html>
<head>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" />
</head>

<?php
date_default_timezone_set('UTC');
include('../includes/common.php');
?>
<body style="font-family:Verdana; font-size:11px;">	
	<form name="sales_contract" action="" method="post">	
	<div align="center" style="width:900px">
		<label class="form_header"> Manual Attendance Entry </label>
	</div>
	<div align="center" style="width:900px">
		<fieldset style="width:900px">
			<legend>Manual Attendance Entry </legend>
			<table width="100%" cellspacing="0">
				<tr bgcolor="#CCCCCC">
					<td height="15" width="180" valign="top" align="center">Company Name </td>
					<td width="150" valign="top" align="center">Section </td>
					<td width="150" valign="top" align="center">Department </td>
					<td width="150" valign="top" align="center">Designation </td>
					<td width="100" valign="top" align="center">From </td>
					<td width="100" valign="top" align="center">To </td>
					
					<td>
						<input type="submit" value="   Populate   " name="save" id="save" class="formbutton" />
					</td>
				</tr>
				<tr>
					<td>
						<select name="company_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="section_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="department_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="designation" id="select" style="width:130px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<input name="from_date" id="from_date" class="text_boxes" style="width:130px"/>
					</td>
					<td>
						<input name="to_date" id="to_date" class="text_boxes" style="width:130px"/>
					</td>
				</tr>
				<tr>	
					<td colspan="7" height="20">
					<hr>
					</td>
				</tr>
			</table>
			
			<table width="100%">
				<tr>
					<td width="130" valign="top">Actual In Time </td>
					<td width="" valign="top">Actual Out Time </td>
					<td width="" valign="top">Change Day Status </td>
					<td width="" valign="top" align="center">Crossed Date Work </td>
				</tr>
				<tr>
					<td><input name="actual_in_time" id="actual_in_time" class="text_boxes" style="width:130px"/></td>
					<td><input name="actual_out_time" id="actual_out_time" class="text_boxes" style="width:130px"/></td>
					<td>
						<select name="change_day_status" id="select" style="width:130px" class="combo_boxes">
							<option value="0">Treat as Absent</option>
							<option value="0">Treat as Present</option>
							<option value="0">Treat as Suspend</option>
							<option value="0">Treat as Regular Day</option>
							<option value="0">Treat as off Day</option>
						</select>
					</td>
					<td align="center">
						<input type="checkbox"/>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	
	<div align="center" style="width:950px">
		<fieldset style="width:900px">
			<table width="100%" cellspacing="0">
				<tr bgcolor="#999999">
					<td width="50">Check Box</td>
					<td width="70">Emp. ID</td>
					
					<td width="150">Employee Name</td>
					<td width="70">Designation</td>
					<td width="70">Division</td>
					<td width="70">Department</td>
					<td width="70">Section</td>
					<td width="50">Attnd. Date</td>
					<td width="50">Shift Name</td>
					<td width="50">In Time</td>
					<td width="50">Out Time</td>
					<td width="50">Day Status</td>
					<td width="50">OT Entitled</td>
					<td width="50">OT Min</td>
					<td width="50">Shift In</td>
					<td width="50">Shift Out</td>
				</tr>
				<tr>
					<td>
						<select name="company_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="company_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="company_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="shift_name" id="select" style="width:130px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<input name="actual_out_time" id="actual_out_time" class="text_boxes" style="width:130px"/>
					</td>
					<td colspan="11">
						<input name="actual_out_time" id="actual_out_time" class="text_boxes" style="width:130px"/>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
</form>
</body>
</html>




