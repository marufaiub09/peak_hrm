﻿<?php
function get_dates( $from_date, $to_date ) {
	$dateMonthYearArr = array();
	$fromDateTS = strtotime( $from_date );
	$toDateTS = strtotime( $to_date );
	
	for( $currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24) ) {
		//$currentDateStr = date( "Y-m-d", $currentDateTS );
		//$dateMonthYearArr[] = $currentDateStr;
		$dateMonthYearArr[] = $currentDateTS;
	}
	
	return $dateMonthYearArr;
}
$leave_days_requested = get_dates( $_REQUEST['from_date'], $_REQUEST['to_date'] );

$host_connect = mysql_connect( 'localhost', 'root', '' ) or die( mysql_error() );
$db_connect = mysql_select_db( 'pim', $host_connect ) or die( mysql_error() );

$sql = "SELECT weekend FROM weekend emp_code = '$_REQUEST[emp_code]' ORDER BY change_date DESC LIMIT 1";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

if( mysql_num_rows( $result ) > 0 ) {
	$row = mysql_fetch_assoc( $result );
	$weekends = array(
					array( 1, "Friday" ),
					array( 2, "Saturday" ),
					array( 3, "Sunday" ),
					array( 4, "Monday" ),
					array( 5, "Tuesday" ),
					array( 6, "Wednesday" ),
					array( 7, "Thursday" )
				);
	for( $i = 0; $i < count( $weekends ); $i++ ) {
		if( $weekends[$i][0] == $row['weekend'] ) {
			$weekend = $weekends[$i][1];
			break;
		}
	}
}

for( $i = 0; $i < count( $leave_days_requested ); $i++ ) {
	if( date( "l", $leave_days_requested[$i] ) == $weekend ) {
		
		break;
	}
}
echo json_encode( $_REQUEST );
?>