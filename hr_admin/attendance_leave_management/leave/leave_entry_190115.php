<?php
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 07-04-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include("../../../includes/array_function.php");

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0  ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/ajax_submit_leave.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
         <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
 		 <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" charset="utf-8">
        
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
            
            $(document).ready(function(e) {
                $("#department_id,#section_id,#subsection_id,#designation_id,#division_id").multiselect({
                    //header: false, 
                    selectedText: "# of # selected",
                });
			$("#department_id,#section_id,#subsection_id,#designation_id,#division_id").multiselectfilter({
	
	});		
				
            });
            
            $(document).ready(function() {
                //populate_data();			
                $('.datepicker').datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
				hide_search_panel();
            });
                
            function reset_field(){
                $('#from_date').val('');
                $('#to_date').val('');
                $('#leave_type').val('');
                $('#remarks').val('');
                //populate_data();
                $('#count_id').html('');
                $('input[name=chk_list]').is(':checked')
                {
                    $('input[name=chk_list]').attr('checked', false);
                }
            }
            
                
            function openmypage_leave_bl(page_link,title)
            {
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=400px,center=1,resize=0,scrolling=0',' ../../');	
                emailwindow.onclose=function()
                {					
                    var policy_id=this.contentDoc.getElementById("policy_id").value;
                    var emp_code=this.contentDoc.getElementById("emp_code").value;
                    var type=this.contentDoc.getElementById("type").value;
                    
                }
            }	
                
                        
            function populate_data() 
            {
                $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                var category 			= $('#cbo_emp_category').val();
                var company_id 			= $('#company_id').val();
                var location_id 		= $('#location_id').val();
                var division_id 		= $('#division_id').val();
                var department_id 		= $('#department_id').val();
                var section_id 			= $('#section_id').val();
                var subsection_id 		= $('#subsection_id').val();			
                var designation_id 		= $('#designation_id').val();
                var id_card		 		= $('#id_card').val();
                var emp_code_cost 		= $('#emp_code_cost').val();
                //alert(id_card);
                
                var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&division_id='+division_id+'&id_card='+id_card+'&emp_code_cost='+emp_code_cost;
                            
                $.ajax({
                    type: "POST",
                    url: "../../employee_info_data.php?"+passdata,
                    data: 'form=leave_entry',
                    success: function(data) {
                        $('#data_panel').html( data );
                    }
                });
            }
            
            function show_inner_filter_result(e){						
                if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
                if (unicode==13 )
                {				
                    var category 			= $('#cbo_emp_category').val();
                    var company_id 			= $('#cbo_company_id').val();
                    var location_id 		= $('#location_id').val();
                    var department_id 		= $('#department_id').val();
                    var section_id 			= $('#section_id').val();
                    var subsection_id 		= $('#subsection_id').val();			
                    var designation_id 		= $('#designation_id').val();
                    var empcode				=$('#emcode_filter').val();
                    
                    var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&empcode='+empcode;
                    
                    $.ajax({
                    type: "POST",
                    url: "../../employee_info_data.php?"+passdata,
                    data: 'form=leave_entry',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#emcode_filter').focus();
                        }
                    });
                }
            }
        
            function check_date_range() {
                var employee_counter=0;
                $('#tbl_employees tbody input[type="checkbox"]:checked').each(function() {
                    employee_counter++;				
                });
                
                if( $('#from_date').val() == '') {
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                            $('#from_date').focus();
                            $(this).html('Please Select Date Range.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                }
                else if( $('#to_date').val() == '' ) {
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                            $('#to_date').focus();
                            $(this).html('Please Select Date Range.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                }
                else if(employee_counter==0){
                    $('#messagebox').fadeTo( 200, 0.1, function() {						
                            $(this).html('Please Select The Employees.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                }
                else
                {	
                    var from_date_parts = $('#from_date').val().split( '-' );
                    var to_date_parts = $('#to_date').val().split( '-' );	
                    
                    if( from_date_parts[0] > to_date_parts[0] && from_date_parts[1] >= to_date_parts[1] && from_date_parts[2] >= to_date_parts[2]) {
                        $('#messagebox').fadeTo( 200, 0.1, function() {
                            $('#to_date').focus();
                            $(this).html('Cross date leave is not allowed.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                        return false;
                    }
                    else{
							var employee_counter=0;
							var data="";
							$('#tbl_employees tbody input[type="checkbox"]:checked').each(function() {
								employee_counter++;
								data += $(this).val() + '|';
							});	
							
						 var d= $.ajax({
							  url: "includes/save_update_leave.php?action=leave_entry_attn_check&emp_code="+data+"&from_date="+$('#from_date').val()+"&to_date="+$('#to_date').val(),
							  async: false
							}).responseText
							var resp=d.split("***");
							
							if( resp[0]==1 )
							{
								//alert(d);
								//confirm('The Following Dates and Employees have Attendance status\n'+resp[1]);
								alert('The Following Dates and Employees have Attendance status\n'+resp[1]);
								return; 
							}
							 	
								
                        fnc_leave_entry(save_perm,edit_perm,delete_perm,approve_perm);
                        /*$('#messagebox').fadeTo( 200, 0.1, function() {						
                            $(this).html('').addClass('messageboxerror').fadeTo(100,1);
                        });*/	
                    }
                }
            }
        
            function openpage_searchemp(page_link,title,type)
            {		
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=400px,center=1,resize=0,scrolling=0','../../')
                emailwindow.onclose=function()
                {
                    var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
                    var emp_code=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"
                    //if(type==1)	
                    //{			
                        var from_date = this.contentDoc.getElementById('txt_form_date');
                        var to_date = this.contentDoc.getElementById('txt_to_date');
                        var leave_type = this.contentDoc.getElementById('txt_leave_type');
                        var remarks = this.contentDoc.getElementById('txt_remarks');
                        var txt_id = this.contentDoc.getElementById('txt_id');				
                        $('#txt_emp_code').val(emp_code.value);
                        $('#before_leave_type').val(leave_type.value);
                        $('#before_from_date').val(from_date.value);
                        $('#before_to_date').val(to_date.value);
                        $('#before_remarks').val(remarks.value);
                        $('#txt_id').val(txt_id.value);	
                    //}
                    //else
                    //{
                        //$('#emp_code_cost').val(emp_code.value);	
                    //}
                }
            }  
        
            function set_date(str)
            {		 
                document.getElementById('to_date').value=str;
            }
        
            function Check(chk)
            {
				var cnt=0;
                if(document.leave_entry_form.Check_ctr.checked==true){
                    for (i = 0; i < chk.length; i++){
                        chk[i].checked = true ;
                        cnt++;
                    }
                }else{			
                    for (i = 0; i < chk.length; i++)
                    chk[i].checked = false ;			
                }
                
                $('#count_id').html('You Are Selected '+cnt+' Employees');
            }
        
            function count_emp(chk,j)
            {
               
			   
			    var leave_type=$('#leave_type').val();
                var from_date=$('#from_date').val();
                var to_date=$('#to_date').val();
                if(leave_type=="" || from_date=="" || to_date=="")
                {
                    alert("Please Select Leave Type and Date Range");
                    $('#chk_list'+j).removeAttr("checked");
                    return;
                }
                
               var cnt=0;
                $('#tbl_employees tbody input[type="checkbox"]:checked').each(function() {
                        cnt++;
                    });
                    
                $('#count_id').html('You Are Selected '+cnt+' Employees');
            }
         
			//new add
            /*
            function count_leave_balace(checked,str,i)
            {
                
                if(checked==true)
                {
                    $('#from_date').attr("disabled", "disabled");
                    $('#to_date').attr("disabled", "disabled");
                    
                    var result =return_global_ajax_value(str);//alert(list_view_orders);return;
                    var response=result.split("_");
                    
                    if(response[0]==1)
                    {
                        //alert("The following "+response[1]+" Leave Criteria Excced The Limit");
                        alert("Given Leave days cross the balance limit");
                        $('#chk_list'+i).removeAttr("checked");
                        $('#from_date').attr("disabled", false);
                        $('#to_date').attr("disabled", false);
                        count_emp(document.leave_entry_form.chk_list,i);
                    }
                }
                if(checked==false){
                    $('#from_date').attr("disabled", false);
                    $('#to_date').attr("disabled", false);
                }
                
            }	
                    
            function return_global_ajax_value(str)
            {
                var emp_code=str;
                var leave_type=$('#leave_type').val();
                var from_date=$('#from_date').val();
                var to_date=$('#to_date').val();
                
                //new add for weekend
                    
                var no_of_days=date_diff( 'd', from_date, to_date  )+1;
                
                return $.ajax({
                      url: "includes/get_data_update.php?emp_code="+emp_code+'&type=leave_validate&leave_type='+leave_type+'&no_of_days='+no_of_days,
                      async: false
                    }).responseText
            }
            
            function date_diff( interval, date_form, date_to  )//from_date, no_of_days )
            {
                var split_date = date_form.split("-");
                if(split_date[0].length!=4) date_form = split_date[2]+"-"+split_date[1]+"-"+split_date[0];
                
                var split_date = date_to.split("-");
                if(split_date[0].length!=4) date_to = split_date[2]+"-"+split_date[1]+"-"+split_date[0];
                
                
                if (!n_format) var n_format=1;
                //var interval="d";
                var millisecond=1;
                var second=millisecond*1000;
                var minute=second*60;
                var hour=minute*60;
                var day=hour*24;
                //var month=day*30;
                var year=day*365;
                date_form=new Date (date_form);
                date_to=new Date (date_to);
                var newDate;
                //var dVal=dateObj.valueOf();
                switch(interval) {
                    case "d": newDate=Math.ceil((date_form.getTime()-date_to.getTime())/(day));  break;
                    case "y": newDate=Math.ceil((date_form.getTime()-date_to.getTime())/(year));  break;
                }
                 
                    if (newDate>0) return newDate; else return ((newDate)*(-1));
                 
            }
            
            */	
            function openmypage_employee_info(page_link,title)
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#emp_code_cost').val(thee_id.value);
                }
            }
                
            function openmypage_employee_info_id_card(page_link,title)
            {			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','../../search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }    
        </script>
        <style>
            .formbutton { width:100px; }
            #select_header select { width:100%; }
            .formbutton1 {width:100px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px">
            <div>
                <div class="form_caption">Leave Entry</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$delete;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
            </div>
            <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Leave Delete</h3>
            <form name="leave_entry_form" id="leave_entry_form" method="POST" autocomplete="off">
                <div id="content_search_panel" >
                    <fieldset><legend>Leave Delete</legend>
                        <table width="720px" cellpadding="0" cellspacing="1" border="0" class="rpt_table">
                            <thead>
                                <th width="140px">Employee Code</th>
                                <th width="140px">Leave Type</th>
                                <th width="140px" colspan="2">Date Range</th>
                                <th width="140px" colspan="2">Delete Date Range</th>
                                <th width="160px">Remarks</th>
                            </thead>
                            <tbody>
                                <tr> 
                                    <td>
                                        <input type="text" name="txt_emp_code" id="txt_emp_code" class="text_boxes" placeholder="Double Click To Search" ondblclick="openpage_searchemp('search_employee.php','Search Employee','1')" readonly="readonly" style="width:140px;"  />
                                    </td>
                                    <td> 
                                        <select name="before_leave_type" id="before_leave_type" class="combo_boxes" disabled="disabled" style="width:140px;" >
                                        <option value=""></option>
                                        <option value="CL">Casual Leave</option>
                                        <option value="SL">Sick Leave</option>
                                        <option value="EL">Earned Leave</option>
                                        <option value="LWP">Leave Without Pay</option>
                                        <option value="RL">Recreation Leave</option>
                                        <option value="SpL">Special Leave</option>
                                        <option value="EdL">Education Leave</option>
                                    </select>
                                    </td> 
                                    <td> 
                                        <input type="text" name="before_from_date" id="before_from_date"  placeholder="From date" class="datepicker" readonly="readonly" disabled="disabled" style="width:70px"/>
                                    </td>
                                    <td> 
                                        <input type="text" name="before_to_date" id="before_to_date" placeholder="To date" class="datepicker" readonly="readonly"  disabled="disabled" style="width:70px;"/>
                                    </td>
                                    <td> 
                                        <input type="text" name="new_before_from_date" id="new_before_from_date"  placeholder="From date" class="datepicker" readonly="readonly" style="width:70px" />
                                    </td>
                                    <td> 
                                        <input type="text" name="new_before_to_date" id="new_before_to_date" placeholder="To date" class="datepicker" readonly="readonly" style="width:70px;" />
                                    </td>
                                    <td> 
                                        <input  type="text" name="before_remarks" id="before_remarks" class="text_boxes" readonly="readonly" style="width:160px;"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="height:8px;"></div>
                        <div align="center">
                            <input type="button" id="isinactive_btn" name="isinactive_btn" value="Delete" class="formbutton"  onclick="fn_leave_delete();" style="width:100px;"/>
                            <input type="hidden" name="txt_id" id="txt_id" />
                        </div>
                    </fieldset>
                </div>
                <fieldset><legend>Leave Entry</legend>
                    <table width="440px" cellpadding="0" cellspacing="1" border="0" class="rpt_table">
                        <thead>
                            <th width="140px">Leave Type</th>
                            <th width="140px" colspan="2">Date Range</th>
                            <th width="160px">Remarks</th>
                        </thead>
                        <tbody> 
                            <tr> 
                                <td>
                                    <select name="leave_type" id="leave_type" class="combo_boxes"  style="width:140px">
                                        <option value="CL">Casual Leave</option>
                                        <option value="SL">Sick Leave</option>
                                        <option value="EL">Earned Leave</option>
                                        <option value="LWP">Leave Without Pay</option>
                                        <option value="RL">Recreation Leave</option>
                                        <option value="SpL">Special Leave</option>
                                        <option value="EdL">Education Leave</option>
                                    </select>
                                </td> 
                                <td> 
                                    <input type="text" name="from_date" id="from_date" class="datepicker" value="" placeholder="From date" onchange="set_date(this.value);" style="width:70px;" />
                                </td>
                                <td>
                                    <input type="text" name="to_date" id="to_date" class="datepicker" value="" placeholder="To date" style="width:70px;" />
                                </td>
                                <td>
                                    <input  type="text" name="remarks" id="remarks" class="text_boxes" style="width:160px"  />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset> 
                <fieldset><span id="count_id" style="font-weight:bold">&nbsp;</span>
                    <table class="rpt_table" width="1120px" border="0" cellpadding="0" cellspacing="1">
                        <thead>
                            <th width="140px">Category</th>
                            <th width="140px">Company</th>
                            <th width="140px">Location</th>
                            <th width="140px">Division</th>
                            <th width="140px">Department</th>
                            <th width="140px">Section</th>
                            <th width="140px">SubSection</th>
                            <th width="140px">Designation</th>
                        </thead>
                        <tbody> 
                            <tr class="general">
                                <td> 
                                    <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                                    <option value="">All Category</option>
                                    <?
                                    foreach($employee_category as $key=>$val)
                                    {
                                    ?>
                                    <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                    <?
                                    }
                                    ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                                        <? if($company_cond=="")
                                        { 
                                        ?>
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        } 
                                        foreach( $company_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        foreach( $location_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php 
                                        foreach( $division_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php 
                                        foreach( $department_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="section_id" id="section_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php 
                                        foreach( $section_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="subsection">
                                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                      <!-- <option value="0">-- Select --</option>-->
                                        <?php 
                                        foreach( $subsection_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="designation">
                                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php foreach( $designation_chart AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>                
                        </tbody>
                    </table>
                    <div style="height:8px;"></div>
                    <table class="rpt_table" width="380px" border="0" cellpadding="0" cellspacing="1">
                    	<thead> 
                            <th width="140px">ID Card No</th>
                            <th width="140px">System Code</th>
                            <th width="100px"><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" style="100px;" /></th>
                        </thead>
                        <tbody> 
                        	<tr> 
                                <td>
                                    <input type="text" name="id_card" id="id_card" class="text_boxes" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" style="width:140px" />
                                </td>
                                <td>
                                    <input type="text" name="emp_code_cost" id="emp_code_cost" class="text_boxes" placeholder="Double Click To Search" ondblclick="openmypage_employee_info();" style="width:140px" />
                                </td>
                                <td><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()" style="width:100px" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="height:8px;"></div>
                    <div id="data_panel" align="center" ></div>		
                    <div align="center">
                        <input type="button" name="save" id="save" class="formbutton" value="Save" onclick="return check_date_range();" />&nbsp;&nbsp;
                        <input type="reset" name="reset" id="reset" class="formbutton" value="Reset" />		
                    </div>
                </fieldset>
            </form>
        </div>
    </body>
</html>