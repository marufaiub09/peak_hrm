<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../includes/functions.js"></script>
<script>


function search_populate(str)
{
	 //alert(str);
	if(str==0)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Employee Code";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==1)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Employee Name";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==2)
	{
		var designation = '<option value="0">--- Select ---</option>';
		<?php
		$designation_sql= mysql_db_query($DB, "select * from lib_designation where is_deleted=0  and status_active=1 order by level ");
		while ($row=mysql_fetch_array($designation_sql))
		{
			echo "designation += '<option value=\"$row[id]\">".mysql_real_escape_string($row[custom_designation])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Designation";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ designation +'</select>'; 
	}	
	else if(str==3)
	{
		var division = '<option value="0">--- Select ---</option>';
		<?php
		$division_sql= mysql_db_query($DB, "select * from lib_division where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($division_sql))
		{
			echo "division += '<option value=\"$row[id]\">".mysql_real_escape_string($row[division_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Division";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ division +'</select>'; 
	}	
	else if(str==4)
	{
		var department = '<option value="0">--- Select ---</option>';
		<?php
		$department_sql= mysql_db_query($DB, "select * from lib_department where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($department_sql))
		{
			echo "department += '<option value=\"$row[id]\">".mysql_real_escape_string($row[department_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Department";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ department +'</select>'; 
	}	
   else if(str==5)
	{
		var section = '<option value="0">--- Select ---</option>';
		<?php
		$section_sql= mysql_db_query($DB, "select * from lib_section where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($section_sql))
		{
			echo "section += '<option value=\"$row[id]\">".mysql_real_escape_string($row[section_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Section";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ section +'</select>'; 
	}	
}

function showResult_source_transport(str,type,div)
{
	
var str2=document.getElementById('cbo_search_by').value;

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
        document.getElementById(div).innerHTML=xmlhttp.responseText;   
    }
  }
// alert( type );
xmlhttp.open("GET","../../includes/transport_list_view.php?search_string="+str+"&search_string2="+str2+"&type="+type,true);
xmlhttp.send();

}

var selected_id = new Array, selected_name = new Array();
		
		function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}


</script>

</head>

<? 
include('../../../includes/common.php');
include('../../../includes/array_function.php');
?>
<body>
<div>
<form name="search_order_frm"  id="search_order_frm">
	<fieldset style="width:950px;">
	<table cellspacing="0" cellpadding="0" class="rpt_table" align="center">
        <thead>
            <th><strong>Search By</strong></th>
            <th><strong>Search</strong></th>
            <th>
                <input type="hidden" readonly="readonly" name="txt_selected_id" id="txt_selected_id" /> 
                <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" />
                <input type="hidden" readonly="readonly" style="width:250px" class="text_boxes" name="txt_selected" id="txt_selected" />
            </th>
        </thead>
		<tr class="general">
			<td>
				<select name="cbo_search_by" id="cbo_search_by" class="combo_boxes" style="width:150px"  >
                    <option value="1">Employee Name</option>
                    <option value="0">Employee Code</option>
                    <option value="2">Designation</option>
                    <option value="3">Division</option>
                    <option value="4">Department</option>
                    <option value="5">Section</option>
                    <option value="6">Employee ID</option>
                    <option value="7">Punch Card</option>
				</select> 
             </td>
             <td><input type="text" name="search_text" id="search_text" class="text_boxes" style="width:150px" autocomplete=off /></td>
			 <td>
             	<input type="button" name="button" id="button" value="Show" onclick="showResult_source_transport(document.getElementById('search_text').value,'search_emp_info_conf','search_div_leave_app')" style="width:100px" class="formbutton" />
             </td>
		</tr>
		
    </table>
    <table >
    	<tr>
			<td>
				<div style="width:950px;" id="search_div_leave_app" align="center">
				</div>
			</td>
        </tr>
        <tr>
			<td align="center" height="30" valign="bottom">
                <div> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onclick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                        <input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                </div>
			</td>
		</tr>
	</table>
	</fieldset>
	</form>
</div>
</body>
</html>