<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}	
	
?>	
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_leave.js" type="text/javascript"></script>
 	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
	<script type="text/javascript" src="../../../includes/tablefilter.js"></script>

	<script type="text/javascript" charset="utf-8">
				
		function fnCreateSelect( aData ) {
			var r = '<select class="combo_boxes"><option value=""></option>', i, iLen = aData.length;
			for( i = 0 ; i < iLen ; i++ ) {
				r += '<option value="' + aData[i] + '">' + aData[i] + '</option>';
			}
			return r + '</select>';
		}
		
 		function populate_data() {
			
			var category 			= $('#cbo_emp_category').val();
			var company_id 			= $('#cbo_company_id').val();
			var location_id 		= $('#location_id').val();
			var department_id 		= $('#department_id').val();
			var section_id 			= $('#section_id').val();
			var subsection_id 		= $('#subsection_id').val();
 			var designation_id 		= $('#designation_id').val();
			var passdata = '&category='+category+'&company_id='+company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id;

			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			nocache = Math.random();
			http.open( 'GET', '../../employee_info_data.php?form=opening_leave_entry' + passdata + '&nocache=' + nocache );
			http.onreadystatechange = function(){
				$('#data_panel').html( http.responseText );
				var tableFilters = {col_0: "none"}
				setFilterGrid("tbl_employees",-1,tableFilters);				
			};
			http.send(null);
			
			/*
			$.ajax({
				type: "POST",
				url: "../../employee_info_data.php?"+passdata,
				data: 'form=opening_leave_entry',
				success: function(data) {
					$('#data_panel').html( data );					
					}				
			});*/
		}
		
		function enable_leave_entry( emp_code ) {
			reset_form();
			$.ajax({
				type: "POST",
				url: "../../employee_info_data.php",
				data: 'data=opening_leave_balance&code=' + emp_code,
				success: function( data ) {
					eval(data);
					/*
					var policy_data = unserialize( data );
					reset_form();
					if( typeof( policy_data[0]['id'] ) == 'undefined' ) {
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$(this).html( policy_data[0] ).addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(10000);
						});
					}
					else {
						$('#emp_code').val( emp_code );
						for( var key in policy_data ) {
							$('#' + policy_data[key]['leave_type']).removeAttr( 'disabled' ).val( policy_data[key]['max_limit'] );
							$('#' + policy_data[key]['leave_type'] + '_limit').val( policy_data[key]['max_limit'] );
						}
					}*/
				}
			});
		}
		
		function reset_form() {
			var fields = new Array( 'CL', 'SL', 'EL', 'LWP', 'RL', 'SpL', 'EdL' );
			for( var i = 0; i < fields.length; i++ ) {
				$('#' + fields[i]).attr( 'disabled', 'disabled' );
				$('#' + fields[i]).val( 'Days' );
				$('#' + fields[i] + '_limit').val( '' );
			}
		}
		
	//numeric field script
	function numbersonly(myfield, e, dec)
	{
		var key;
		var keychar;
	
		if (window.event)
			key = window.event.keyCode;
		else if (e)
			key = e.which;
		else
			return true;
		keychar = String.fromCharCode(key);
	
		// control keys
		if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
		return true;
		
		// numbers
		else if ((("0123456789").indexOf(keychar) > -1))
			return true;
		else
			return false;
	}	
		
		
	</script>
	<style>
		.formbutton { width:100px; }
		#select_header select { width:100%; }
		.text_boxes{ width:110px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="position:relative; height:40px; margin:5px 0; width:100%;">
		<h2>Opening Leave Entry</h2>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
 	</div>
	<form id="opening_leave_balance_form" action="javascript:fnc_opening_leave_balance();" method="POST" autocomplete="off">
		<div id="search_panel1" align="center">	
            <fieldset><legend>Leave Entry</legend>
			<table width="100%" cellpadding="0" cellspacing="1" border="0">
				<tr>
					<td>Casual Leave:</td>
					<td><input type="text" name="CL" id="CL" class="text_boxes" value="" placeholder="Days" disabled="disabled" onKeyPress="return numbersonly(this, event);" /><input type="hidden" name="CL_limit" id="CL_limit" value="" /></td>
					<td>Sick Leave:</td>
					<td><input type="text" name="SL" id="SL" class="text_boxes" value="" placeholder="Days" disabled="disabled" onKeyPress="return numbersonly(this, event);" /><input type="hidden" name="SL_limit" id="SL_limit" value="" /></td>
					<td>Earned Leave:</td>
					<td><input type="text" name="EL" id="EL" class="text_boxes" value="" placeholder="Days" disabled="disabled" onKeyPress="return numbersonly(this, event);" /><input type="hidden" name="EL_limit" id="EL_limit" value="" /></td>
					<td>Leave Without Pay:</td>
					<td><input type="text" name="LWP" id="LWP" class="text_boxes" value="" placeholder="Days" disabled="disabled" onKeyPress="return numbersonly(this, event);" /><input type="hidden" name="LWP_limit" id="LWP_limit" value="" /></td>
				</tr>
				<tr>
					<td>Recreation Leave:</td>
					<td><input type="text" name="RL" id="RL" class="text_boxes" value="" placeholder="Days" disabled="disabled" onKeyPress="return numbersonly(this, event);" /><input type="hidden" name="RL_limit" id="RL_limit" value="" /></td>
					<td>Special Leave:</td>
					<td><input type="text" name="SpL" id="SpL" class="text_boxes" value="" placeholder="Days" disabled="disabled" onKeyPress="return numbersonly(this, event);" /><input type="hidden" name="SpL_limit" id="SpL_limit" value="" /></td>
					<td>Education Leave:</td>
					<td><input type="text" name="EdL" id="EdL" class="text_boxes" value="" placeholder="Days" disabled="disabled" onKeyPress="return numbersonly(this, event);" /><input type="hidden" name="EdL_limit" id="EdL_limit" value="" /></td>
					<td colspan="2"><input type="hidden" name="emp_code" id="emp_code" value="" /></td>
				</tr>
			</table>
		</fieldset>
        </div>
        <div id="search_panel2" align="center">
        <fieldset>
        <table class="rpt_table" style="width:1050px;" border="0" cellpadding="0" cellspacing="1">
				<thead>			                
                	<th width="140px"> Category</th>
					<th width="140px"> Company</th>
					<th width="140px"> Location</th>
					<th width="140px"> Department</th>
                    <th width="140px"> Section</th>
                    <th width="140px"> SubSection</th>
                    <th width="140px"> Designation</th>
 					<td><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" style="width:70px" /></td>                  
				</thead>
                <tr class="general">					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
                        	<?
								if($company_cond=="")
								{
							?>
                                <option value="0">-- Select --</option>
                                <?php } foreach( $company_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?> ><?php echo $value; ?></option>
                                <?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:140px">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:140px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td> 
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()"  style="width:70px" /></td>
				</tr> 
          </table>  
          </fieldset>      
	  	</div>
        <fieldset><div id="data_panel" class="demo_jui" align="center" style="margin-top:10px;"></div></fieldset>
        <div style="margin-top:10px; text-align:center;">
			<input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
			<input type="button" name="reset" id="reset" class="formbutton" value="Reset" />
		</div>
	</form>
</body>
</html>
<?php mysql_close( $host_connect ); ?>