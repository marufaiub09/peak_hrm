<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 08-04-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');


$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee WHERE is_deleted = 0 AND status_active = 1 $company_cond";	
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}
//company
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_leave.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../includes/functions.js" type="text/javascript"></script>
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	
		var emp_basic = new Array();
		<?php
		foreach( $emp_basic AS $emp_code => $emp ) {
			echo "emp_basic['$emp_code'] = new Array();\n";
			foreach( $emp AS $key => $value ) {
				echo "emp_basic['$emp_code']['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			populate_data();
			
			$( ".datepicker" ).datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
			
			$.fn.sort_select_box = function() {
				// Get options from select box
				var my_options = $("#" + this.attr('id') + ' option');
				// sort alphabetically
				my_options.sort(function( a, b ) {
				if( a.text > b.text ) return 1;
					else if( a.text < b.text ) return -1;
					else return 0
				})
				//replace with sorted my_options;
				$(this).empty().append( my_options );
				
				// clearing any selections
				$("#" + this.attr('id') + " option").attr( 'selected', false );
			}
		});
		
		/*function populate_employees( company_id ) {
			var options = '<option value="0">-- Select an employee --</option>';
			for( var emp_code in emp_basic ) {
				if( emp_basic[emp_code]['company_id'] == company_id && emp_basic[emp_code]['sex'] == 1 ) {
					options += '<option value="' + emp_code + '">' + emp_basic[emp_code]['name'] + '</option>';
				}
			}
			$('#emp_code').html( options );
			$('#emp_code').sort_select_box();
		}*/
		
		function show_employee( emp_code ) {
			$.ajax({				   
				type: "POST",
				url: "leave_data.php",
				data: 'form=maternity_leave_emp_info&code=' + emp_code,
				success: function( data ) {
					var emp_info = unserialize( data );
					
					$('#name_container').html( emp_info['name'] );
					$('#designation_container').html( emp_info['designation'] );
					$('#location_container').html( emp_info['location'] );
					$('#division_container').html( emp_info['division'] );
					$('#department_container').html( emp_info['department'] );
					$('#section_container').html( emp_info['section'] );
					$('#max_days').val( emp_info['max_limit'] );
				}
			});
		}
		
		function reset_form() {
			$("#maternity_leave_entry_form").trigger('reset'); //jquery
			
		}
		
		function convert_from_mysql_date( date_field ) {			
			var dates = date_field.split( "-" );
			return dates[2] + "-" + dates[1] + "-" + dates[0];
		}
		
		function show_maternity_leave_entry( id ) {
			$.ajax({
				type: "POST",
				url: "leave_data.php",
				data: 'form=maternity_leave_one&leave_id=' + id,
				success: function( data ) {
					var leave_info = unserialize( data );
					
					var split_string=id.split('&');
					$('#leave_id').val( split_string[0] );
					$('#leave_transaction_id').val( split_string[1] );
					$('#company_id').val( leave_info['company_id'] );
					populate_employees( leave_info['company_id'] );
					$('#emp_code').val( leave_info['emp_code'] );
					$('#name_container').html( leave_info['name'] );
					$('#designation_container').html( leave_info['designation'] );
					$('#location_container').html( leave_info['location'] );
					$('#division_container').html( leave_info['division'] );
					$('#department_container').html( leave_info['department'] );
					$('#section_container').html( leave_info['section'] );
					$('#days_required').val( leave_info['days_required'] );
					$('#leave_start_date').val( convert_from_mysql_date( leave_info['leave_start_date'] ) );
					$('#leave_end_date').val( convert_from_mysql_date( leave_info['leave_end_date'] ) );
					if( Number( leave_info['disbursing_amount'] ) != 0 ) $('#disbursing_amount').val( leave_info['disbursing_amount'] );
					else $('#disbursing_amount').val( '' );
					$('#est_delivery_date').val( convert_from_mysql_date( leave_info['est_delivery_date'] ) );
					if( leave_info['act_delivery_date'] != '0000-00-00' ) $('#act_delivery_date').val( convert_from_mysql_date( leave_info['act_delivery_date'] ) );
					else $('#act_delivery_date').val( '' );
				}
			});
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "leave_data.php",
				data: 'form=maternity_leave_all',
				success: function(data) {
					$('#data_panel').html( data );					
				}
			});
		}
		
		function print_pay_sheet() {
			if( $('#leave_id').val() == 0 ) alert( 'Please select a leave first' );
			else alert( 'You opted to print pay sheet of leave ' + $('#leave_id').val() );
		}
	
    	function calculate_date()
		{			
			var thisDate=($('#leave_start_date').val()).split('-');
			var in_date=thisDate[2]+'-'+thisDate[1]+'-'+thisDate[0];
			var days=($('#days_required').val())-1;
			var max_days=($('#max_days').val())-1;
			if(days*1 > max_days*1)
			{
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#max_days').focus();
					$(this).html('Required Days Cross The Max Limit('+max_days+') in Policy.').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
				return;
			}
			var date = add_days(in_date,days);	
			var split_date=date.split('-');			
			var res_date=split_date[2]+'-'+split_date[1]+'-'+split_date[0];
			$('#leave_end_date').val(res_date);
			
			//disburshment amount
			var days = $('#days_required').val();
			var emp_code = $('#emp_code').val();
			$.ajax({type: "POST",url: "leave_data.php",data: 'form=disburshment_amount&emp_code='+emp_code+'&days='+days,			
				success: function(data) 
				{
					$('#disbursing_amount').val( data*1 );					
				}
			});	
		}    
    
		function openpage_searchemp(page_link,title)
		{		
			emailwindow = dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=430px,center=1,resize=0,scrolling=0','../../')
			emailwindow.onclose = function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"						
				var company_id=this.contentDoc.getElementById("txt_company_id")
				$('#emp_code').val(emp_code.value);	
				$('#company_id').val(company_id.value);	
				show_employee( emp_code.value );			
			}
		}
		
		//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
  
	
    </script>
	<style>
		.formbutton { width:120px; }
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
	</style>
</head>
<body style="font-family:verdana; font-size:11px;">
    <div align="center" style="width:1084px; position:relative; height:40px; margin:5px 0;">
    	<div class="form_caption">
		Maternity Leave Entry
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>	
	<form id="maternity_leave_entry_form" action="javascript:fnc_maternity_leave_entry(save_perm,edit_perm,delete_perm,approve_perm);" method="POST" autocomplete="off">
		<fieldset style="width:1062px;">
			<legend>Search Employee</legend>
			<table width="100%" cellpadding="0" cellspacing="2" border="0">            	
				<tr>
                	<td width="9%">Employee:</td>
					<td width="23%">
						<!--<select name="emp_code" id="emp_code" class="combo_boxes" onchange="show_employee( this.value );">
							<option value="0">-- Select an employee --</option>
						</select>-->
                        <input type="text" name="emp_code" id="emp_code" class="text_boxes" placeholder="Double Click To Search" ondblclick="openpage_searchemp('search_employee_maternity.php','Search Employee')" readonly="readonly"  />
                        <!--for hrm_maternity_leave table update-->
						<input type="hidden" name="leave_id" id="leave_id" value="0" />
                        <!--for hrm_leave_transaction table update-->
                        <input type="hidden" name="leave_transaction_id" id="leave_transaction_id" value="0" />
					</td>
					<td width="7%">Company:</td>
					<td width="61%">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:180px" disabled="disabled"> <!--onchange="populate_employees( this.value );"-->
							<option value="0">-- Select company --</option>
							<?php foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
							<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
							<?php } } ?>
						</select>
					</td>					
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:1062px;">
			<legend>Leave Information</legend>
			<table width="100%" cellpadding="0" cellspacing="2" border="0">
				<tr>
					<td>Name:</td>
					<td id="name_container"></td>
					<td>Designation:</td>
					<td id="designation_container"></td>
					<td>Location:</td>
					<td id="location_container"></td>
				</tr>
				<tr>
					<td>Division:</td>
					<td id="division_container"></td>
					<td>Department:</td>
					<td id="department_container"></td>
					<td>Section:</td>
					<td id="section_container"></td>
				</tr>
				<tr>
					<td>Days Required:</td>
					<td><input type="text" name="days_required" id="days_required" class="text_boxes" value="" onkeyup="calculate_date()" onkeypress="return numbersonly(this,event)" /><input type="text" id="max_days" /></td>
					<td>Leave Start Date:</td>
					<td><input type="text" name="leave_start_date" id="leave_start_date" class="datepicker" value="" onchange="calculate_date()" /></td>
					<td>Leave End Date:</td>
					<td><input type="text" name="leave_end_date" id="leave_end_date" class="datepicker" value="" disabled="disabled" /></td>
				</tr>
				<tr>
					<td>Disbursing Amount:</td>
					<td><input type="text" name="disbursing_amount" id="disbursing_amount" class="text_boxes" value="" onkeypress="return numbersonly(this,event)" /></td>
					<td>Estimated Delivery Date:</td>
					<td><input type="text" name="est_delivery_date" id="est_delivery_date" class="datepicker" value="" /></td>
					<td>Actual Delivery Date:</td>
					<td><input type="text" name="act_delivery_date" id="act_delivery_date" class="datepicker" value="" /></td>
				</tr>
				<tr>
					<td colspan="6" align="center" style="padding-top:10px;">
						<input type="submit" name="save" id="save" class="formbutton" value="Save" />
						<input type="reset" name="reset" id="reset" class="formbutton" value="Reset" />
						<input type="button" name="print" id="print" class="formbutton" value="Print Pay Sheet" onclick="print_pay_sheet();" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<fieldset style="width:1064px">
    	<div id="data_panel" style="width:100%; margin-top:10px; font-family:verdana; font-size:11px;"></div>
    </fieldset>
</body>
</html>