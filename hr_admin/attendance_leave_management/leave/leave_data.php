<?php
session_start();
include('../../../includes/common.php');
extract( $_POST );

	$sql = "SELECT *, concat(first_name,' ',middle_name, ' ', last_name) as name FROM hrm_employee where status_active=1 and sex=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//maternity_leave polciy
$sql = "SELECT * FROM lib_policy_maternity_leave ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$maternity_leave_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$maternity_leave_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$maternity_leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//maternity_leave
$sql = "SELECT * FROM hrm_maternity_leave ORDER BY emp_code ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$maternity_leave = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$maternity_leave[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$maternity_leave[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

if( isset( $form ) && $form == 'maternity_leave_all' ) { ?>
<div>
    <div  style="width:1064px;">
        <table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="maternity_leave"> 
            <thead>			
                    <th width="235">Employee</th>
                    <th width="200">Days Required</th>
                    <th width="200">Leave Start Date</th>
                    <th width="200">Leave End Date</th>
                    <th width="200">Disbursing Amount</th>			
            </thead>
        </table>
    </div>	
    <div style="width:1064px; overflow-y:scroll; max-height:210px;" align="left">
        <table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="maternity_leave"> 			
                
                <?php 
				foreach( $maternity_leave AS $mtl ) {						
					if($emp_basic[$mtl['emp_code']]['name'])
					{
                    	$trans_mt_id=return_field_value("id","hrm_leave_transaction","leave_type='ML' and emp_code=$mtl[emp_code]");
                    	$mat_id_tr_id="'".$mtl['id'].'&'.$trans_mt_id."'";
						?>
						<tr style="text-decoration:none; cursor:pointer" onclick="show_maternity_leave_entry(<?php echo $mat_id_tr_id;  ?>);">
							<td width="235" align="center"><?php echo $emp_basic[$mtl['emp_code']]['name']; ?></td>
							<td width="200" align="center"><?php echo $mtl['days_required']; ?></td>
							<td width="200" align="center"><?php if( $mtl['leave_start_date'] != '0000-00-00' ) echo convert_to_mysql_date( $mtl['leave_start_date'] ); ?></td>
							<td width="200" align="center"><?php if( $mtl['leave_end_date'] != '0000-00-00' ) echo convert_to_mysql_date( $mtl['leave_end_date'] ); ?></td>
							<td width="200" align="center"><?php echo $mtl['disbursing_amount']; ?></td>
						</tr>
                <?php 
					}
				}
				?>		
        </table>
    </div>
</div>
<?php
exit();
}
else if( isset( $form ) && $form == 'maternity_leave_one' ) {
	$leave_info = $maternity_leave[$leave_id];
	$leave_info['name']			= $emp_basic[$maternity_leave[$leave_id]['emp_code']]['name'];
	$leave_info['designation']	= $designation_chart[$emp_basic[$maternity_leave[$leave_id]['emp_code']]['designation_id']]['custom_designation'];
	$leave_info['company_id']	= $emp_basic[$maternity_leave[$leave_id]['emp_code']]['company_id'];
	$leave_info['location']		= $location_details[$emp_basic[$maternity_leave[$leave_id]['emp_code']]['location_id']]['location_name'];
	$leave_info['division']		= $division_details[$emp_basic[$maternity_leave[$leave_id]['emp_code']]['division_id']]['division_name'];
	$leave_info['department']	= $department_details[$emp_basic[$maternity_leave[$leave_id]['emp_code']]['department_id']]['department_name'];
	$leave_info['section']		= $section_details[$emp_basic[$maternity_leave[$leave_id]['emp_code']]['section_id']]['section_name'];
	
	print_r( serialize( $leave_info ) );
	exit();
}
else if( isset( $form ) && $form == 'maternity_leave_emp_info' ) {
	$emp_info = array();
	$emp_info['name'] = $emp_basic[$code]['name'];
	$emp_info['designation'] = $designation_chart[$emp_basic[$code]['designation_id']]['custom_designation'];
	$emp_info['location'] = $location_details[$emp_basic[$code]['location_id']]['location_name'];
	$emp_info['division'] = $division_details[$emp_basic[$code]['division_id']]['division_name'];
	$emp_info['department'] = $department_details[$emp_basic[$code]['department_id']]['department_name'];
	$emp_info['section'] = $section_details[$emp_basic[$code]['section_id']]['section_name'];
	$emp_info['max_limit'] = $maternity_leave_policy[$emp_basic[$code]['maternity_leave_policy']]['max_limit'];
	
	print_r( serialize( $emp_info ) );
	exit();
}


else if( isset( $form ) && $form == 'disburshment_amount' ) {
	 
	
	//payment_calculation payment_disbursement 
	$payment_calculation = $maternity_leave_policy[$emp_basic[$emp_code]['maternity_leave_policy']]['payment_calculation'];
	if($payment_calculation==0)
	{
		$sql = "select sum(payable_days),sum(net_payable_amount) from hrm_salary_mst where emp_code='$emp_code' order by salary_periods DESC limit 3";
		$result = mysql_query($sql) or die(mysql_error());
		$row = mysql_fetch_array($result); 
		$per_day_salary = $row[1]/$row[0];
		$disburshment_amount = $days*$per_day_salary;		
	}
	else
	{		
		$per_day_salary = $emp_basic[$emp_code]['gross_salary']/30;
 		$disburshment_amount = $days*$per_day_salary;
	}
	
	echo $disburshment_amount;
	exit(); 
}


function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata"; 
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}

?>