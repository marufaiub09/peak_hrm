<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 04-04-12
		
######################################*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
date_default_timezone_set('asia/dhaka');
include('../../../includes/common.php');

//selft user
if ($_SESSION['logic_erp']["data_level_secured"]==2) 
{
	$sql = "SELECT id_card_no,category,designation_id FROM hrm_employee WHERE emp_code='".$_SESSION['logic_erp']['priv_emp_code']."' and status_active=1 and is_deleted = 0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$seft_info = mysql_fetch_array( $result );
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>	
        <style type="text/css" media="print">
			@page { size 8.5in 11in; margin: 0.5in; margin-bottom:0 }
			div.page { page-break-after: always; background-color:#FFF;}
        </style>
    
        <script src="includes/functions.js" type="text/javascript"></script>
        <script src="includes/ajax_submit_leave.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
        <script src="../../../resources/jquery-1.6.1.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>	
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
                
                var usrType='<?php echo $_SESSION['logic_erp']["data_level_secured"];?>'
				if(usrType==2)
				{
					$('#txt_emp_code').val('<?php echo $_SESSION['logic_erp']["priv_emp_code"];?>');
					$('#txt_emp_code').attr("disabled","disabled");
				}
               // user_data_level_secured(usrType);
            });
		
			// user_data_level_secured
			function user_data_level_secured(usrType)
			{
				//alert("su..re");
				if(usrType==2)
				{
					$('#txt_emp_code').attr("disabled","disabled");
				}
			}
		
        function openmypage_employee_info(page_link,title)
        {
            emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,'width=1000px,height=400px,center=1,resize=0,scrolling=0','../../')
            emailwindow.onclose=function()
            {
                var thee_loc = this.contentDoc.getElementById("txt_selected");
                var thee_id = this.contentDoc.getElementById("txt_selected_id");
                
                var thee_id_from_date =$('#from_date').val();
                var thee_id_to_date = $('#to_date').val();
                
                var emp_code=thee_id.value; 
                var from_to_date= thee_id_from_date+"***"+thee_id_to_date;
                //fnc_leave_application_eng(emp_code,from_to_date,"leave_application_english");
                //fnc_leave_application_eng(thee_id.value,"leave_application_english");
                //alert(from_to_date); 
                $('#txt_emp_code').val(emp_code);
                
            }
        }
        
        
        
        function new_window()
        {
            var w = window.open("Surprise", "#");
            var d = w.document.open();
            d.write (document.getElementById('confirm_div').innerHTML);
            d.close();
        }	
    </script>
    
    </head>
    <body>
        <div align="center" style="width:100%; position:relative; margin-bottom:5px; margin-top:5px;">
            <div class="form_caption" style="width:620px; background-color:#A6CAF0; padding-top:5px; height:25px; border-radius:5px; color:#000;" align="center">Leave Application English</div>
            <div style="height:20px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; border-radius:5px; width:620px;" align="center"></div></div>
            <form id="frm_leave_application" action="" autocomplete="off" method="post">
                <fieldset style="width:600px;"><!--<legend>Search</legend>-->
                    <table cellpadding="0" cellspacing="0" class="rpt_table" width="50%" rules="all" border="1" bordercolor="#7F7FFF">
                        <thead>
                            <th><strong>Search Employee</strong></th>
                            <th><strong>Leave From Date</strong></th>
                            <th><strong>Leave To Date</strong></th>
                            <th><input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton"   style="width:70px;"/></th>
                        </thead>
                        <tr class="general">
                            <td><input type="text" placeholder="Double Click For Search" style="width:150px" name="txt_emp_code" id="txt_emp_code" value="" class="text_boxes" size="40" ondblclick="openmypage_employee_info('../../search_user_access_emp_by_code.php','Employee Information'); return false" readonly="readonly" /></td>
                            <td><input type="text" name="from_date" id="from_date"  class="datepicker" /></td>
                            <td><input type="text" name="to_date" id="to_date"  class="datepicker" /></td>
                            <td><input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton"  style="width:70px;" onclick="javascript:fnc_leave_application_eng(document.getElementById('txt_emp_code').value,document.getElementById('from_date').value+'***'+document.getElementById('to_date').value,'leave_application_english')"/></td>
                        </tr>
                    </table>
                </fieldset>
            </form>
            <div id="data_panel1" align="center"></div>
            <div id="confirm_div" style="margin-top:5px;" align="center" class="page"></div>
        </div>
    </body>
</html>
