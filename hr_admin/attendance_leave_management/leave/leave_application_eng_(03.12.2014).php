<?
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date : 04-04-12
		
######################################*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

date_default_timezone_set('UTC');
include('../../../includes/common.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>	

		<style type="text/css" media="print">
		
			   @page { size 8.5in 11in; margin: 0.5in; margin-bottom:0 }
				div.page { page-break-after: always; background-color:#FFF;}
		</style>

	<script src="includes/functions.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_leave.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
	
    <script src="../../../resources/jquery-1.6.1.js" type="text/javascript"></script>
    <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>	
   
    <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
    
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>

    <script type="text/javascript">
	


	function openmypage_employee_info(page_link,title)
	{
		// alert(page_link);
		
		/*$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
		var error = false;
		
		if( $('#from_date').val()==0){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#from_date').focus();
		$(this).html('Please Select The From Date.').addClass('messageboxerror').fadeTo(900,1);
		});	
		}
		
		else if( $('#to_date').val()==0){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#to_date').focus();
		$(this).html('Please Select The To Date.').addClass('messageboxerror').fadeTo(900,1);
		});
		}
		
		
		'width=750px,height=290px
		else{
			
			
*/			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,'width=1000px,height=400px,center=1,resize=0,scrolling=0','../../')
		//}
		emailwindow.onclose=function()
		{
			
			
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");
			
			var thee_id_from_date =$('#from_date').val();
			var thee_id_to_date = $('#to_date').val();
			
			var emp_code=thee_id.value; 
			var from_to_date= thee_id_from_date+"***"+thee_id_to_date;
			//fnc_leave_application_eng(emp_code,from_to_date,"leave_application_english");
			//fnc_leave_application_eng(thee_id.value,"leave_application_english");
			//alert(from_to_date); 
			$('#txt_emp_code').val(emp_code);
			
		}
	}
	
	
	
	function new_window()
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write (document.getElementById('confirm_div').innerHTML);
		d.close();
	}	
</script>

</head>
<body>
	<div align="center" style="width:100%; margin:5px 0;">
        <div class="form_caption">
            Leave Application Form
        </div>
		<div align="center" id="messagebox"></div>
	</div>
<div align="center">
<form id="frm_leave_application" action="" autocomplete="off" method="post">
<fieldset style="width:600px;">
<legend>Search</legend>
	<div align="center">
        <table cellpadding="0" cellspacing="0" class="rpt_table" width="50%">
            <thead>
                <th><strong>Search Employee</strong></th>
                <th><strong>Leave From Date</strong></th>
                <th><strong>Leave To Date</strong></th>
            </thead>
            <tr class="general">
                <td><input type="text" placeholder="Double Click For Search" style="width:200px" name="txt_emp_code" id="txt_emp_code" value="" class="text_boxes" size="40" ondblclick="openmypage_employee_info('../../search_employee_multiple.php','Employee Information'); return false" readonly="readonly" /></td>
               
                <td><input type="text" name="from_date" id="from_date"  class="datepicker" />
 				 </td>
                  <td><input type="text" name="to_date" id="to_date"  class="datepicker" />
				 </td>
            </tr>
             <tr>   
           <td colspan="6"  style="padding-top:10px;" align="center">
           <div class="button_container"> 
          <input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton"  style="width:130px;" onclick="javascript:fnc_leave_application_eng(document.getElementById('txt_emp_code').value,document.getElementById('from_date').value+'***'+document.getElementById('to_date').value,'leave_application_english')"/>
          <input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton"   style="width:130px;"/>
           
           </div>
           </td>
        </tr>
        </table>
    </div>
</fieldset>
</form>
</div>
<div id="data_panel1" align="center"></div>
<div id="confirm_div" style="margin-top:5px;" align="center" class="page"></div>
	
</body>
</html>

<script>
$( ".datepicker" ).datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});

</script>