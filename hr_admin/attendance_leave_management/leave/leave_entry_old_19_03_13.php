<?php
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 07-04-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include("../../../includes/array_function.php");

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0  ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_leave.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
    <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
				  				
		$(document).ready(function() {
			//populate_data();			
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
			
			
		function reset_field(){
			$('#from_date').val('');
			$('#to_date').val('');
			$('#leave_type').val('');
			$('#remarks').val('');
			//populate_data();
			$('#count_id').html('');
			$('input[name=chk_list]').is(':checked')
			{
				$('input[name=chk_list]').attr('checked', false);
			}
		}
			
					
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			
			var category 			= $('#cbo_emp_category').val();
			var company_id 			= $('#cbo_company_id').val();
			var location_id 		= $('#location_id').val();
			var department_id 		= $('#department_id').val();
			var section_id 			= $('#section_id').val();
			var subsection_id 		= $('#subsection_id').val();			
			var designation_id 		= $('#designation_id').val();
			
			var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&empcode=';
						
			$.ajax({
				type: "POST",
				url: "../../employee_info_data.php?"+passdata,
				data: 'form=leave_entry',
				success: function(data) {
					$('#data_panel').html( data );
				}
			});
		}
		
		function show_inner_filter_result(e){						
    		if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
			if (unicode==13 )
			{				
				var category 			= $('#cbo_emp_category').val();
				var company_id 			= $('#cbo_company_id').val();
				var location_id 		= $('#location_id').val();
				var department_id 		= $('#department_id').val();
				var section_id 			= $('#section_id').val();
				var subsection_id 		= $('#subsection_id').val();			
				var designation_id 		= $('#designation_id').val();
				var empcode				=$('#emcode_filter').val();
				
				var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&empcode='+empcode;
				
				$.ajax({
				type: "POST",
				url: "../../employee_info_data.php?"+passdata,
				data: 'form=leave_entry',
				success: function(data) {
					$('#data_panel').html( data );
					$('#emcode_filter').focus();
					}
				});
			}
    	}	
	
		function check_date_range() {
			var employee_counter=0;
			$('#tbl_employees tbody input[type="checkbox"]:checked').each(function() {
				employee_counter++;				
			});
			
			if( $('#from_date').val() == '') {
				$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#from_date').focus();
						$(this).html('Please Select Date Range.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
			}
			else if( $('#to_date').val() == '' ) {
				$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#to_date').focus();
						$(this).html('Please Select Date Range.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
			}
			else if(employee_counter==0){
				$('#messagebox').fadeTo( 200, 0.1, function() {						
						$(this).html('Please Select The Employees.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
			}
			else
			{	
				var from_date_parts = $('#from_date').val().split( '-' );
				var to_date_parts = $('#to_date').val().split( '-' );	
				
				if( from_date_parts[0] > to_date_parts[0] && from_date_parts[1] >= to_date_parts[1] && from_date_parts[2] >= to_date_parts[2]) {
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#to_date').focus();
						$(this).html('Cross date leave is not allowed.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
					return false;
				}
				else{				
					fnc_leave_entry(save_perm,edit_perm,delete_perm,approve_perm);
					/*$('#messagebox').fadeTo( 200, 0.1, function() {						
						$(this).html('').addClass('messageboxerror').fadeTo(100,1);
					});*/	
				}
			}
		}
	
    
    function openpage_searchemp(page_link,title)
		{		
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=400px,center=1,resize=0,scrolling=0','../../')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("txt_selected_emp") //Access form field with id="emailfield"				
				var from_date = this.contentDoc.getElementById('txt_form_date');
				var to_date = this.contentDoc.getElementById('txt_to_date');
				var leave_type = this.contentDoc.getElementById('txt_leave_type');
				var remarks = this.contentDoc.getElementById('txt_remarks');				
				$('#txt_emp_code').val(emp_code.value);
				$('#before_leave_type').val(leave_type.value);
				$('#before_from_date').val(from_date.value);
				$('#before_to_date').val(to_date.value);
				$('#before_remarks').val(remarks.value);			
			}
		}  
    
    function set_date(str)
	{		 
		document.getElementById('to_date').value=str;
	}
    
	function Check(chk)
		{
			var cnt=0;
			if(document.leave_entry_form.Check_ctr.checked==true){
				for (i = 0; i < chk.length; i++){
					chk[i].checked = true ;
					cnt++;
				}
			}else{			
				for (i = 0; i < chk.length; i++)
				chk[i].checked = false ;			
			}
			$('#count_id').html('You Are Selected '+cnt+' Employees');
		}
	function count_emp(chk){
		var cnt=0;
		for (i = 0; i < chk.length; i++){
			if(chk[i].checked) cnt++;
		}
		$('#count_id').html('You Are Selected '+cnt+' Employees');
	}
	
    </script>
	<style>
		.formbutton { width:100px; }
		#select_header select { width:100%; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px">	     
    <div>
    	<div class="form_caption">
		Leave Entry
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$delete;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form name="leave_entry_form" id="leave_entry_form" method="POST" autocomplete="off">
          <fieldset>
            <legend>Leave Delete</legend>
            <table width="100%" cellpadding="0" cellspacing="2" border="0">
				<tr>
                	<td width="24%">Emp Code : 
               	    <input type="text" name="txt_emp_code" id="txt_emp_code" class="text_boxes" placeholder="Double Click To Search" ondblclick="openpage_searchemp('search_employee.php','Search Employee')" readonly="readonly"  /></td>
					<td width="21%">
						Leave Type:
						<select name="before_leave_type" id="before_leave_type" class="combo_boxes" disabled="disabled" >
                        	<option value=""></option>
							<option value="CL">Casual Leave</option>
							<option value="SL">Sick Leave</option>
							<option value="EL">Earned Leave</option>
							<option value="LWP">Leave Without Pay</option>
							<option value="RL">Recreation Leave</option>
							<option value="SpL">Special Leave</option>
							<option value="EdL">Education Leave</option>
						</select>
					</td>
					<td width="26%">
						Date Range:
						<input type="text" name="before_from_date" id="before_from_date"  placeholder="From date" style="width:75px" class="datepicker" readonly="readonly" />&nbsp;&nbsp; 
                        <input type="text" name="before_to_date" id="before_to_date" placeholder="To date" style="width:75px;" class="datepicker" readonly="readonly" /></td>					
					<td width="5%">Remarks:</td>
                    <td width="24%"><input  type="text" name="before_remarks" id="before_remarks" class="text_boxes" readonly="readonly"></textarea>&nbsp;
                    				<input type="button" id="isinactive_btn" name="isinactive_btn" value="Delete" class="formbutton" style="width:70px;" onclick="fn_leave_delete();" /> </td>
				</tr>
			</table>	
            </fieldset>
            <fieldset>
			<legend>Leave Entry</legend>
			<table width="100%" cellpadding="0" cellspacing="2" border="0">
				<tr>
					<td width="25%">
						Leave Type:
						<select name="leave_type" id="leave_type" class="combo_boxes" style="width:170px">
							<option value="CL">Casual Leave</option>
							<option value="SL">Sick Leave</option>
							<option value="EL">Earned Leave</option>
							<option value="LWP">Leave Without Pay</option>
							<option value="RL">Recreation Leave</option>
							<option value="SpL">Special Leave</option>
							<option value="EdL">Education Leave</option>
						</select>
					</td>
					<td width="44%">
						Date Range:
						<input type="text" name="from_date" id="from_date" class="datepicker" value="" placeholder="From date" onchange="set_date(this.value);" />
						<input type="text" name="to_date" id="to_date" class="datepicker" value="" placeholder="To date" />
					</td>
					<td width="7%">Remarks:</td>
					<td width="24%"><input  type="text" name="remarks" id="remarks" class="text_boxes" style="width:250px"  /></td>
				</tr>
			</table>
            <div style="padding:3px; margin-top:20px">
            <table class="rpt_table" style="width:60%;" border="0" cellpadding="0" cellspacing="2">
				<thead>			                
                	<th> Category</th>
					<th> Company</th>
					<th> Location</th>
					<th> Department</th>
                    <th> Section</th>
                    <th> SubSection</th>
                    <th> Designation</th>                    			
					<td><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" /></td>                  
				</thead>
                <tr class="general">					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                         	<option value="">All Category</option>
                            <option value="0">Top Management</option>
							<option value="1">Mid Management</option>
							<option value="2">Non Management</option>
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:150px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:120px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td> 
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>                            
					<td><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()" /></td>
				</tr> 
          </table>
          </div>
		</fieldset> 
        <fieldset>      
       	<span id="count_id" style="font-weight:bold">&nbsp;</span>
		<div id="data_panel" align="center" ></div>		
        <div align="center">
        <input type="button" name="save" id="save" class="formbutton" value="Save" onclick="return check_date_range();" />&nbsp;&nbsp;
        <input type="reset" name="reset" id="reset" class="formbutton" value="Reset" />		
        </div>
        </fieldset>
	</form>
</body>
</html>