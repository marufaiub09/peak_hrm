//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function date_difference( date1, date2, interval ) {
	var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
	date1 = new Date( date1 );
	date2 = new Date( date2 );
	var timediff = date2 - date1 + 1 * day;
	if( isNaN( timediff ) ) return NaN;
	switch( interval ) {
		case "years"	: return date2.getFullYear() - date1.getFullYear();
		case "months"	: return ( date2.getFullYear() * 12 + date2.getMonth() ) - ( date1.getFullYear() * 12 + date1.getMonth() );
		case "weeks"	: return Math.floor( timediff / week );
		case "days"		: return Math.floor( timediff / day ); 
		case "hours"	: return Math.floor( timediff / hour ); 
		case "minutes"	: return Math.floor( timediff / minute );
		case "seconds"	: return Math.floor( timediff / second );
		default			: return undefined;
	}
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

//Opening Leave Balance ( Save / Update )
function fnc_opening_leave_balance() {
	var data = '&leave=', error = false, leave_counter = 0, fields = new Array( 'CL', 'SL', 'EL', 'LWP', 'RL', 'SpL', 'EdL' );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	for( var i = 0; i < fields.length; i++ ) {
		if( $('#' + fields[i]).val() != '' ) {
			if( parseInt( escape( $('#' + fields[i]).val() ) ) > parseInt( escape( $('#' + fields[i] + '_limit').val() ) ) ) {
				error = true;
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#' + fields[i]).focus();
					$(this).html('Opening Balance can not exceed limit.').addClass('messageboxerror').fadeTo(900,1);
				});
			}
			else {
				leave_counter++;
				if($('#' + fields[i]).val()=='Days') var value=0;
				else var value=$('#' + fields[i]).val();
				data += fields[i] + '_' + value + '|';
			}
		}
	}
	if( leave_counter == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$(this).html('Please provide at least one leave balance.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data = data.substr( 0, data.length - 1 );
	
	data += '&emp_code=' + $('#emp_code').val();
	
	//alert(data);return;
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_leave.php?action=opening_leave_balance' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_opening_leave_balance;
		http.send(null);
	}
}
//Opening Leave Balance Response ( Save / Update )
function response_opening_leave_balance() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data saved successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"hrm","opening_leave_balance","update","../../../");
				$(this).fadeOut(5000);
			});
		}
		reset_form();
	}
}
//Maternity Leave Entry ( Save / Update )
function fnc_maternity_leave_entry() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	//alert(save_perm);
	if( $('#emp_code').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#emp_code').focus();
			$(this).html('Please select an employee.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#days_required').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#days_required').focus();
			$(this).html('Please provide days required.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#leave_start_date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#leave_start_date').focus();
			$(this).html('Please provide leave start date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#leave_end_date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#leave_end_date').focus();
			$(this).html('Please provide leave end date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#est_delivery_date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#est_delivery_date').focus();
			$(this).html('Please provide estimated delivery date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		nocache = Math.random();
		var data = '';
		data += '&leave_id=' + $('#leave_id').val() + '&leave_transaction_id=' + $('#leave_transaction_id').val() + '&emp_code=' + $('#emp_code').val() + '&days_required=' + $('#days_required').val()
				+ '&leave_start_date=' + $('#leave_start_date').val() + '&leave_end_date=' + $('#leave_end_date').val() + '&disbursing_amount=' + $('#disbursing_amount').val()
				+ '&est_delivery_date=' + $('#est_delivery_date').val() + '&act_delivery_date=' + $('#act_delivery_date').val();
		
		http.open( 'GET', 'includes/save_update_leave.php?action=maternity_leave_entry' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_maternity_leave_entry;
		http.send(null);
	}
}
//Maternity Leave Entry Response ( Save / Update )
function response_maternity_leave_entry() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data saved successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"hrm","maternity_leave_entry","save","../../../");
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data updated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"hrm","maternity_leave_entry","update","../../../");
				$(this).fadeOut(5000);
			});
		}
		populate_data();
	}
}
//Leave Entry ( Save / Update )
function fnc_leave_entry(save_perm,edit_perm,delete_perm,approve_perm) {
	
	var data = '&leave_type=' + $('#leave_type').val(), error = false, employee_counter = 0, isstatus = 0;
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if( save_perm==2){	
		error = true;
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}	
	if( $('#from_date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#from_date').focus();
			$(this).html('Please provide from date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&from_date=' + $('#from_date').val();
	
	if( $('#to_date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#to_date').focus();
			$(this).html('Please provide to date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&to_date=' + $('#to_date').val();
	
	data += '&remarks=' + escape( $('#remarks').val() );
	
	if( error == false ) {		
			data += '&employees=';
			$('#tbl_employees tbody input[type="checkbox"]:checked').each(function() {
				employee_counter++;
				data += $(this).val() + '|';
			});		
		
		//alert(data);return;
		if( employee_counter == 0 ) {
				error = true;
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).html('Please select at least one employee.').addClass('messageboxerror').fadeTo(900,1);
				});
			}
			else data = data.substr( 0, data.length - 1 );
		}	
		
		if( error == false ) {
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_leave.php?action=leave_entry' + data + '&isstatus='+ isstatus + '&nocache=' + nocache );
			http.onreadystatechange = response_leave_entry;
			http.send(null);
		}
}
//Leave Entry Response ( Save / Update )
function response_leave_entry() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response[2]);return;
				
		var msg='';
		if( (response[0]-response[1])!=0) {
			msg = (response[0]-response[1]) + ' Employee\'s leave entry successfully Done. ';
			alert(msg);
		}
		else if(response[1] > 0){
			msg += ' Employee\'s ( ' + response[2] + ' ) leave not saved. ';
			alert(msg);
			
		}
		else if(response[3]!=''){
			 msg += ' Employee\'s ( ' + response[3] + ' ) no leave Policy. ';
			 alert(msg);
		}
		//var msg = response[0] + ' emp\'s leave entry successfully Done. ' + response[1] + ' emp\'s ( ' + response[2] + ' ) leave not saved.' + 'emp\'s ( ' + response[3] + ' ) no leave Policy.';
		var not_save_emp=response[2].split(",");
		for (var k=0; k<not_save_emp.length; k++)
		{
			$('#id_card_'+not_save_emp[k]).attr('bgcolor','red');
		}
		
		save_activities_history('',"hrm","leave_entry","update","../../../");		
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html(msg).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(3000);
			reset_field();			
		});
	}
}

 //leave delete
 function fn_leave_delete(){

	 var txt_emp_code=escape($('#txt_emp_code').val());
	 var before_leave_type=escape($('#before_leave_type').val());
	 var before_from_date=escape($('#before_from_date').val());
	 var before_to_date=escape($('#before_to_date').val());
	 var before_remarks=escape($('#before_remarks').val());
	 var txt_id=escape($('#txt_id').val());
	 var new_before_from_date=escape($('#new_before_from_date').val());
	 var new_before_to_date=escape($('#new_before_to_date').val());
	
	//alert(new_before_from_date);
	 $('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	 error = false;
	 
	 if(txt_emp_code=='' || before_leave_type=='' || before_from_date=='' || before_to_date==''){
	 	error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_emp_code').focus();
			$(this).html('Please provide Employee code, Leave type, Date info.').addClass('messageboxerror').fadeTo(900,1);
		});
	 }
	 if( $('#new_before_from_date').val() == '' || $('#new_before_to_date').val()=='' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#new_before_from_date').focus();
			$(this).html('Please provide Deleted Date Range.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	 else{		
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_leave.php?action=leave_entry' + 
					  '&emp_code='+ txt_emp_code + 
					  '&before_leave_type='+ before_leave_type + 
					  '&before_from_date='+ before_from_date + 
					  '&before_to_date='+ before_to_date + 
					  '&before_remarks='+ before_remarks +
					  '&txt_id='+ txt_id +
					  '&new_before_from_date='+ new_before_from_date +
					  '&new_before_to_date='+ new_before_to_date + 
					  '&isstatus=delete' + 
					  '&nocache=' + nocache );
			http.onreadystatechange = response_fn_leave_delete;
			http.send(null);
	 }
 }	 
	 
	function response_fn_leave_delete(){
		if(http.readyState == 4) { 
		 	var response = http.responseText;
		 	//alert(response);
		if(response==1){
			 var msg = 'Leave information of ' + response[0] + ' employee have been Deleted successfully.';
			 $("#messagebox").fadeTo( 200, 0.1, function() {
				save_activities_history('',"hrm","leave_delete","delete","../../../");
				$(this).html(msg).addClass('messagebox_ok').fadeTo(900,1);			
				});
		 	}
		 if(response==2){
			 var msg = 'No Employee have been Deleted.';
			 $("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html(msg).addClass('messagebox_ok').fadeTo(900,1);			
				});
		 	}
		 if(response==3){
		 var msg = 'Sorry!!!You must have to select the start or end date';
		 $("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html(msg).addClass('messagebox_ok').fadeTo(900,1);			
			});
		}
		}
	 }
	 
	 

 //Leave Application Bangla-----------------------//
 function fnc_leave_application(str1,str2,type,leave_type){
		//alert(leave_type);
		//alert(type);
		ajax.requestFile = "../../hrm_bangla_report_data.php?search_string1="+str1+"&search_string2="+str2+"&type="+type+"&leave_type="+leave_type;	// Specifying which file to get
		ajax.onCompletion = show_fnc_leave_application;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}
 
function show_fnc_leave_application()
{
	 //alert(ajax.response);	 
	var response=(ajax.response).split('####');
	//$('#data_panel2').html( '<a href="' + response[1] + '"> Print  </a>' + response[0] );
	//document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	//document.getElementById('data_panel1').appendChild('<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
	$('#data_panel1').html( '<br>&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
	$('#data_panel1').append( '&nbsp;<a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="DOC Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
	//$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
	//$('#data_panel1').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );						
	document.getElementById('confirm_div').innerHTML=response[0];
}
//-------------------------END search----------------------------//

//--------------Leave Application English-----------------------//
 function fnc_leave_application_eng(str1,str2,type)
 {
	//alert("su..re");
	//alert(str2);
	ajax.requestFile ="../../hrm_bangla_report_data.php?search_string1="+str1+"&search_string2="+str2+"&type="+type;	// Specifying which file to get
	ajax.onCompletion = show_fnc_leave_application_eng;	// Specify function that will be executed after file has been found
	ajax.runAJAX();		
}


function show_fnc_leave_application_eng()
{
	//alert(ajax.response);
	var response=(ajax.response).split('####');
	$('#data_panel1').html( '<br><b>Convert To </b><a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="DOC Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
	 document.getElementById('confirm_div').innerHTML=response[0];
	
}
//-------------------------END search----------------------------//


//--------------Leave Closing Year-----------------------//
 function fnc_leave_year_close(){
		
		 var cbo_company_name	= escape($('#cbo_company_name').val());
		 var cbo_location_name	= escape($('#cbo_location_name').val());
		 var cbo_closing_year	= escape($('#cbo_closing_year').val());
		 var cbo_new_year		= escape($('#cbo_new_year').val());
		
		// $('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
		 error = false;
		
		if( cbo_company_name==0 ){
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_company_name').focus();
				$(this).html('Please Select Company.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if(cbo_closing_year==0 ){
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_closing_year').focus();
				$(this).html('Please Select Closing Year. ').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( cbo_new_year==0 ){
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_new_year').focus();
				$(this).html('Please Select New Year. ').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if(cbo_closing_year==cbo_new_year && cbo_closing_year!=0 && cbo_new_year!=0 ){
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_closing_year').focus();
				$(this).html('Closing Year and New Year can not be Same.').addClass('messageboxerror').fadeTo(900,1);
			});
		}

		if(error == false)
		{		
			$("#process_anim").html('<img src="../../../images/loading2.gif" />').fadeIn(1000);
			nocache = Math.random();
			var data = 'action=leave_closing' +'&cbo_company_name='+ cbo_company_name +  '&cbo_location_name='+ cbo_location_name + '&cbo_closing_year='+ cbo_closing_year + '&cbo_new_year='+ cbo_new_year;	
			$.ajax({ type: 'GET', 
					 url: 'includes/leave_closing_process.php',
					 data : data,
					 success: function(data){
						 		$('#messagebox').removeClass().addClass('messagebox').text('Leave Year Clossing Successfully Done').fadeIn(1000);
							}
			});
			
	 	}
	 	
}



function fnc_earn_leave_process()
{
	//var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	//var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_earn_leave_periods 	= escape(document.getElementById('cbo_earn_leave_periods').value);
	/*var cbo_ot_rate_rounding 	= escape(document.getElementById('cbo_ot_rate_rounding').value);
	var txt_precision=escape(document.getElementById('txt_precision').value);*/
	//alert(cbo_earn_leave_periods);return;
	
	$("#messagebox").removeClass().addClass('messagebox').text('Earn leave Processing....').fadeIn(1000);

   if($('#cbo_earn_leave_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_earn_leave_periods').focus();
			$(this).html('Please Select Earn Leave Periods').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else
	{	
		$("#process_anim").html('<img src="../../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/earn_leave_process.php?action=earn_leave_closing'+
					'&cbo_earn_leave_periods='+cbo_earn_leave_periods
					);
		http.onreadystatechange = fnc_earn_leave_process_reply;
		http.send(null); 
	}
}

function fnc_earn_leave_process_reply() 
{
	if(http.readyState == 4)
	{ 		
		var response = (http.responseText).split('*');			
	 	//alert(http.responseText);	
		//NO PROCESS FOR OPEN YEAR";3
		//NO PROCESS WITHOUT OPEN YEAR";4
		if (response[1]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Earn Leave Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('NO PROCESS FOR OPEN YEAR.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==4)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('NO PROCESS WITHOUT OPEN YEAR.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(3000);
			});
		}
	}
}	


//-------------------------END ----------------------------//

