<?php
date_default_timezone_set('UTC');
session_start();

include('../../../../includes/common.php');
$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );
extract( $_POST );

function search_in_array_by_key_value( $array, $key, $value ) {
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

function check_date_in_range( $search_date, $start_date, $end_date ) {
	$search_date_ts	= strtotime( $search_date );
	$start_date_ts	= strtotime( $start_date );
	$end_date_ts	= strtotime( $end_date );
	
	if( $search_date_ts >= $start_date_ts && $search_date_ts <= $end_date_ts ) return true;
	else return false;
}

//Opening Leave Entry ( Save / Update )
if( $action == "opening_leave_balance" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$leaves = explode( "|", $leave );
	
	for( $j = 0; $j < count( $leaves ); $j++ ) {
		$leave = explode( "_", $leaves[$j] );
		
		$sql = "SELECT emp.emp_code, emp.leave_policy, yar.id as leave_year, ld.max_limit
				FROM hrm_employee AS emp
				LEFT JOIN lib_policy_leave_definition AS ld ON ( ld.policy_id = emp.leave_policy AND ld.leave_type = '" . $leave[0] . "' )
				LEFT JOIN lib_policy_year AS yar ON (yar.type=0 and yar.is_locked=0)
				WHERE emp.emp_code = '$emp_code'";
		
		
		$row = mysql_fetch_assoc( mysql_query( $sql ) ) or die( $sql . "<br />" . mysql_error() );
		
		if( $row['max_limit'] == '' ) $row['max_limit'] = 0;
		if( $row['leave_year'] == '' ) $row['leave_year'] = 0;
		
		//check if there is an entry
		$sql2 = "SELECT id FROM hrm_leave_balance
				WHERE
					emp_code = '$emp_code'
					AND leave_year = $row[leave_year]
					AND leave_type = '" . $leave[0] . "'";
		//echo $sql;die;
		$result2 = mysql_query( $sql2 ) or die( $sql . "<br />" . implode("_", $row) . "<br />" . $sql2 . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result2 ) > 0 ) {
			$sql3 = "UPDATE hrm_leave_balance
					SET
						leave_policy = $row[leave_policy],
						leave_limit = $leave[1],
						balance = $leave[1] - leave_availed
					WHERE
						emp_code = '$emp_code'
						AND leave_year = $row[leave_year]
						AND leave_type = '" . $leave[0] . "'";
		}
		else {
			$sql3 = "INSERT INTO hrm_leave_balance (
						leave_year,
						year_start,
						emp_code,
						leave_policy,
						leave_type,
						leave_limit,
						leave_availed,
						balance
					) VALUES (
						$row[leave_year],
						" . date("Y") . ",
						'$emp_code',
						$row[leave_policy],
						'" . $leave[0] . "',
						$row[max_limit],
						0,
						$leave[1]
					)";
		}
		
		$result3 = mysql_query( $sql3 ) or die( $sql . "<br />" . implode("_", $row) . "<br />" . $sql3 . "<br />" . mysql_error() );
	}
	$all_query=$sql3;
	$sql_history=encrypt($all_query, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo "1";
	exit();
}
//Maternity Leave Entry ( Save / Update )
if( $action == "maternity_leave_entry" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$table_name = "hrm_maternity_leave";
	if( $disbursing_amount == '' ) $disbursing_amount = 0;

	$unsaved_employees = array();
	$leave_type='ML';
	
	
	//maternity_leave polciy
	$sql = "SELECT * FROM lib_policy_maternity_leave ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$maternity_leave_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$maternity_leave_policy[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$maternity_leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}

	$sql = "SELECT *, concat(first_name,' ',middle_name, ' ', last_name) as name FROM hrm_employee where status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}
	//working length
	$working_length = GetDays( $emp_basic[$row['emp_code']]['joining_date'], date("Y-m-d") );
	if($working_length <= $maternity_leave_policy[$emp_basic[$code]['maternity_leave_policy']]['min_service_length'])
	{
		echo "3_".$maternity_leave_policy[$emp_basic[$code]['maternity_leave_policy']]['min_service_length'];
		exit();
	}
	
	 
	//holiday
	$sql = "SELECT * FROM lib_holiday WHERE is_deleted = 0 AND status_active = 1 ORDER BY company_id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday[$row['company_id']][] = $row;
	}
		
	//weekend
	$sql = "SELECT * FROM hrm_weekend where emp_code='$emp_code' ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic[$row['emp_code']]['weekend'] = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']]['weekend'][] = $row['weekend'];
	}
	
	$days = GetDays( $leave_start_date, $leave_end_date );
	
		$company_id = $emp_basic[$emp_code]['company_id'];
		$emp_holiday = $holiday[$company_id];
		
		//if leave policy is tagged with employee, try to calculate leave
		if( isset( $emp_basic[$emp_code]['maternity_leave_policy'] ) && $emp_basic[$emp_code]['maternity_leave_policy'] != 0 ) {
			//if leave type is defined with employee, calulate leave
			$sql = "SELECT * FROM lib_policy_maternity_leave WHERE id = " . $emp_basic[$emp_code]['maternity_leave_policy'];
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
				
			if( mysql_num_rows( $result ) > 0 ) {
				$leave_policy_details = mysql_fetch_assoc( $result );
				
				//define textual weekends of employee
				$textual_weekends = $emp_basic[$emp_code]['weekend'];
				//define status of the applied days
				$dates = array(); $holidays = 0; $weekends = 0;
				for( $i = 0; $i < count( $days ); $i++ ) {
					$weekday = date( 'l', strtotime( $days[$i] ) );
					if( in_array( $weekday, $textual_weekends ) ) { $dates[$days[$i]][0] = 'Weekend'; $dates[$days[$i]][1] = 'Weekend'; $weekends++; }
					else {
						$holiday_flag = false;
						for( $j = 0; $j < count( $emp_holiday ); $j++ ) {
							if( check_date_in_range( $days[$i], $emp_holiday[$j]['from_date'], $emp_holiday[$j]['to_date'] ) ) {
								$holiday_flag = true;
								$dates[$days[$i]][0] = 'Holiday'; $dates[$days[$i]][1] = 'Holiday';
								$holidays++;
								break;
							}
						}
						if( $holiday_flag == false ) $dates[$days[$i]][0] = 'Working Day'; $dates[$days[$i]][1] = $leave_type;
					}
				}
				//calculate total days
				$total_days = 0;
				if( $leave_policy_details['off_day_leave_count'] == 0 ) {		//Excluding off-days
					foreach( $dates AS $key => $value ) {
						if( $dates[$key][1] == $leave_type ) $total_days++;
					}
				}
				else if( $leave_policy_details['off_day_leave_count'] == 1 ) {	//Include in-between, preceding and succeeding off-days
					$preceding_off_day = false;
					$preceding_off_days = array();
					$preceding_calculation = true;
					$preceding_day = date( 'Y-m-d', strtotime( $days[0] ) - 86400 );
					while( $preceding_calculation ) {
						if( in_array( $preceding_day, $textual_weekends ) ) {
							$preceding_off_day = true;
							$weekends++;
							$preceding_off_days[$preceding_day] = array( 'Weekend', $leave_type );
							$preceding_day = date( 'Y-m-d', strtotime( $preceding_day ) - 86400 );
						}
						else {
							for( $k = 0; $k < count( $emp_holiday ); $k++ ) {
								if( check_date_in_range( $preceding_day, $emp_holiday[$k]['from_date'], $emp_holiday[$k]['to_date'] ) ) {
									$preceding_off_day = true;
									$holidays++;
									$preceding_off_days[$preceding_day] = array( 'Holiday', $leave_type );
									$preceding_day = date( 'Y-m-d', strtotime( $preceding_day ) - 86400 );
									break;
								}
								else $preceding_calculation = false;
							}
						}
					}
					$succeeding_off_day = false;
					$succeeding_off_days = array();
					$succeeding_calculation = true;
					$succeeding_day = date( 'Y-m-d', strtotime( $days[count( $days ) - 1] ) + 86400 );
					while( $succeeding_calculation ) {
						if( in_array( $succeeding_day, $textual_weekends ) ) {
							$succeeding_off_day = true;
							$weekends++;
							$succeeding_off_days[$succeeding_day] = array( 'Weekend', $leave_type );
							$succeeding_day = date( 'Y-m-d', strtotime( $succeeding_day ) + 86400 );
						}
						else {
							for( $k = 0; $k < count( $emp_holiday ); $k++ ) {
								if( check_date_in_range( $succeeding_day, $emp_holiday[$k]['from_date'], $emp_holiday[$k]['to_date'] ) ) {
									$succeeding_off_day = true;
									$holidays++;
									$succeeding_off_days[$succeeding_day] = array( 'Holiday', $leave_type );
									$succeeding_day = date( 'Y-m-d', strtotime( $succeeding_day ) + 86400 );
									break;
								}
								else $succeeding_calculation = false;
							}
						}
					}
					if( $preceding_off_day == true && $succeeding_off_day == true ) {
						$dates = array_merge( $preceding_off_days, $dates, $succeeding_off_days );
					}
					foreach( $dates AS $key => $value ) {
						$dates[$key][1] = $leave_type;
						$total_days++;
					}
				}
				else if( $leave_policy_details['off_day_leave_count'] == 2 ) {	//Include in-between off-days only
					foreach( $dates AS $key => $value ) {
						$dates[$key][1] = $leave_type;
						$total_days++;
					}
				}
				
				$first_day = ''; $last_day = '';
				$l = 0;
				foreach( $dates AS $date => $status ) {
					if( $l == 0 ) {
						$first_day = $date;
						$last_day = $date;						
					}
					else $last_day = $date;					
					$l++;
				}
				
				if( $leave_id == 0 ) 
					{	//Insert
					
						$first_day_overlap_check = check_uniqueness( "leave_date", "hrm_leave_transaction_details", "emp_code='$emp_code' AND leave_date = '$first_day'", 0 );
						$last_day_overlap_check = check_uniqueness( "leave_date", "hrm_leave_transaction_details", "emp_code='$emp_code' AND leave_date = '$last_day'", 0 );
						
						if( $first_day_overlap_check == false || $last_day_overlap_check == false ) {
						$unsaved_employees[] = $emp_code;
						}
						else 
						{
						
								$mat_tbl_id = return_next_id( "id", $table_name );
									
								$sql = "INSERT INTO $table_name (
												id,
												emp_code,
												days_required,
												leave_start_date,
												leave_end_date,
												disbursing_amount,
												est_delivery_date,
												act_delivery_date
											) VALUES (
												$mat_tbl_id,
												'$emp_code',
												$days_required,
												'" . convert_to_mysql_date( $leave_start_date ) . "',
												'" . convert_to_mysql_date( $leave_end_date ) . "',
												$disbursing_amount,
												'" . convert_to_mysql_date( $est_delivery_date ) . "',
												'" . convert_to_mysql_date( $act_delivery_date ) . "'
											)";
								$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
								$all_query.=$sql;
								$leave_id = return_next_id( "id", "hrm_leave_transaction" );
								$sql = "INSERT INTO hrm_leave_transaction (
											id,
											emp_code,
											leave_type,
											leave_policy_id,
											from_date,
											to_date,
											total_days,
											remarks,
											holidays,
											weekends
										) VALUES (
											$leave_id,
											'$emp_code',
											'$leave_type',
											'" . $emp_basic[$emp_code]['maternity_leave_policy'] . "',
											'" . convert_to_mysql_date( $leave_start_date ) . "',
											'" . convert_to_mysql_date( $leave_end_date ) . "',
											$total_days,
											'$remarks',
											$holidays,
											$weekends
										)";
										
								$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
								$all_query.=$sql;
								
								foreach( $dates AS $date => $status ) 
									{
										$id = return_next_id( "id", "hrm_leave_transaction_details" );
										$sql = "INSERT INTO hrm_leave_transaction_details (
														id,
														emp_code,
														leave_id,
														leave_type,
														leave_date,
														day_status,
														leave_status
													) VALUES (
														$id,
														'$emp_code',
														$leave_id,
														'0',
														'$date',
														'$status[0]',
														'$status[1]'
													)";
										$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
										$all_query.=$sql;	
										mysql_query("update hrm_attendance set status='$status[1]', late_time_min=0 where attnd_date='$date' and emp_code='$emp_code'") or die (mysql_error());
									}
																
								echo "1_$leave_id";
								exit();
							}
						}
								
						else {	//Update-------------------------------------------------------
								
									$sql = "UPDATE $table_name
											SET
												days_required		= $days_required,
												leave_start_date	= '" . convert_to_mysql_date( $leave_start_date ) . "',
												leave_end_date		= '" . convert_to_mysql_date( $leave_end_date ) . "',
												disbursing_amount	= $disbursing_amount,
												est_delivery_date	= '" . convert_to_mysql_date( $est_delivery_date ) . "',
												act_delivery_date	= '" . convert_to_mysql_date( $act_delivery_date ) . "'
											WHERE
												emp_code	= '$emp_code'
												AND id		= $leave_id";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );										
									$all_query.=$sql;						
									$sql = "UPDATE hrm_leave_transaction
										SET
											from_date='" . convert_to_mysql_date( $leave_start_date ) . "',
											to_date='" . convert_to_mysql_date( $leave_end_date ) . "',
											total_days=$total_days,
											remarks='$remarks',
											holidays=$holidays,
											weekends=$weekends
										WHERE 
											emp_code	= '$emp_code'
											AND id		= $leave_transaction_id";										
										
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );	
									$all_query.=$sql;	
									$sql="delete from hrm_leave_transaction_details where emp_code='$emp_code' and leave_id='$leave_transaction_id' and leave_type=0";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
									
									foreach( $dates AS $date => $status ) 
										{
											$id = return_next_id( "id", "hrm_leave_transaction_details" );
											$sql = "INSERT INTO hrm_leave_transaction_details (
														id,
														emp_code,
														leave_id,
														leave_type,
														leave_date,
														day_status,
														leave_status
													) VALUES (
														$id,
														'$emp_code',
														$leave_id,
														'0',
														'$date',
														'$status[0]',
														'$status[1]'
													)";
											$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
											$all_query.=$sql;
											mysql_query("update hrm_attendance set status='$status[1]', late_time_min=0 where attnd_date='$date' and emp_code='$emp_code'") or die (mysql_error());
										}
									
									$sql_history=encrypt($all_query, "logic_erp_2011_2012");
									$_SESSION['sql_history']=$sql_history;
									echo "2_$leave_id";
									exit();
								}					
			
			}
			
	}		
		
}

//Leave Entry ( Save / Update )
if( $action == "leave_entry" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	$all_query='';
	if($isstatus == "delete"){
		$before_from_date=convert_to_mysql_date($before_from_date);
		$before_to_date=convert_to_mysql_date($before_to_date);
		
		$select_sql=mysql_query("select id, total_days from hrm_leave_transaction where emp_code=$emp_code and leave_type='$before_leave_type' and from_date='$before_from_date' and to_date='$before_to_date'");		
		if($row=mysql_fetch_array($select_sql)){
			$leave_id=$row['id'];
			$total_days=$row['total_days'];		
			
			$update_sql_tr = "UPDATE hrm_leave_transaction set status_active=0, is_deleted=1, is_locked=1 WHERE id=$leave_id";
			mysql_query( $update_sql_tr ) or die( $update_sql_tr . "<br />" . mysql_error() );		
			$all_query=$update_sql_tr;
			$update_sql_dtls = "UPDATE hrm_leave_transaction_details set status_active=0, is_deleted=1, is_locked=1 WHERE leave_id=$leave_id";
			mysql_query( $update_sql_dtls ) or die( $update_sql_dtls . "<br />" . mysql_error() );
			$all_query.=$update_sql_dtls;
			$balance=return_field_value("balance","hrm_leave_balance","emp_code=$emp_code and leave_type='$before_leave_type' and status_active=1");
			$leave_availed=return_field_value("leave_availed","hrm_leave_balance","emp_code=$emp_code and leave_type='$before_leave_type' and status_active=1");
			$update_balance=$balance+$total_days;
			$update_leave_availed=$leave_availed-$total_days;
			$update_sql = "UPDATE hrm_leave_balance set leave_availed=$update_leave_availed,balance=$update_balance WHERE emp_code=$emp_code and leave_type='$before_leave_type'";
			mysql_query( $update_sql ) or die( $update_sql . "<br />" . mysql_error() );
			$all_query.=$update_sql;
			$sql_history=encrypt($all_query, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo "1";
			exit();
		}
		echo "2";
		exit();
	}
	
		
	$unsaved_employees = array();
	$no_policy_emp = array();
	$employees = explode( "|", $employees );
	for( $ct = 0; $ct < count( $employees ); $ct++ ) {
		if($ct==0)
			{
				$emp_code_all = $employees[$ct];
			}
		else{	
				$emp_code_all .=','. $employees[$ct];
			}
	}
	//print_r($emp_code_all);die;
	//emp_basic	
	$sql = "SELECT * FROM hrm_employee where emp_code in ($emp_code_all) and status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}		
	
	//policy_tagging
	/*$sql = "SELECT * FROM hrm_employee ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']]['leave_policy'] = $row['leave_policy'];
	}*/
	//holiday
	$sql = "SELECT * FROM lib_holiday WHERE is_deleted = 0 AND status_active = 1 ORDER BY company_id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday[$row['company_id']][] = $row;
	}
	//weekend
	$sql = "SELECT * FROM hrm_weekend ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic[$row['emp_code']]['weekend'] = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']]['weekend'][] = $row['weekend'];
	}
	
	$days = GetDays( $from_date, $to_date );
	
	for( $emp = 0; $emp < count( $employees ); $emp++ ) {
		$emp_code = $employees[$emp];
		$company_id = $emp_basic[$emp_code]['company_id'];
		$emp_holiday = $holiday[$company_id];
		
		//if leave policy is tagged with employee, try to calculate leave
		if( isset( $emp_basic[$emp_code]['leave_policy'] ) && $emp_basic[$emp_code]['leave_policy'] != 0 ) {
			//if leave type is defined with employee, calulate leave
			$sql = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = " . $emp_basic[$emp_code]['leave_policy'] . " AND leave_type = '$leave_type'";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			if( mysql_num_rows( $result ) > 0 ) {
				$leave_policy_details = mysql_fetch_assoc( $result );
				
				//define textual weekends of employee
				$textual_weekends = $emp_basic[$emp_code]['weekend'];
				//define status of the applied days
				$dates = array(); $holidays = 0; $weekends = 0;
				for( $i = 0; $i < count( $days ); $i++ ) {
					$weekday = date( 'l', strtotime( $days[$i] ) );
					if( in_array( $weekday, $textual_weekends ) ) { $dates[$days[$i]][0] = 'Weekend'; $dates[$days[$i]][1] = 'Weekend'; $weekends++; }
					else {
						$holiday_flag = false;
						for( $j = 0; $j < count( $emp_holiday ); $j++ ) {
							if( check_date_in_range( $days[$i], $emp_holiday[$j]['from_date'], $emp_holiday[$j]['to_date'] ) ) {
								$holiday_flag = true;
								$dates[$days[$i]][0] = 'Holiday'; $dates[$days[$i]][1] = 'Holiday';
								$holidays++;
								break;
							}
						}
						if( $holiday_flag == false ) $dates[$days[$i]][0] = 'Working Day'; $dates[$days[$i]][1] = $leave_type;
					}
				}
				//calculate total days
				$total_days = 0;
				if( $leave_policy_details['off_day_leave_count'] == 0 ) {		//Excluding off-days
					foreach( $dates AS $key => $value ) {
						if( $dates[$key][1] == $leave_type ) $total_days++;
					}
				}
				else if( $leave_policy_details['off_day_leave_count'] == 1 ) {	//Include in-between, preceding and succeeding off-days
					$preceding_off_day = false;
					$preceding_off_days = array();
					$preceding_calculation = true;
					$preceding_day = date( 'Y-m-d', strtotime( $days[0] ) - 86400 );
					while( $preceding_calculation ) {
						if( in_array( $preceding_day, $textual_weekends ) ) {
							$preceding_off_day = true;
							$weekends++;
							$preceding_off_days[$preceding_day] = array( 'Weekend', $leave_type );
							$preceding_day = date( 'Y-m-d', strtotime( $preceding_day ) - 86400 );
						}
						else {
							for( $k = 0; $k < count( $emp_holiday ); $k++ ) {
								if( check_date_in_range( $preceding_day, $emp_holiday[$k]['from_date'], $emp_holiday[$k]['to_date'] ) ) {
									$preceding_off_day = true;
									$holidays++;
									$preceding_off_days[$preceding_day] = array( 'Holiday', $leave_type );
									$preceding_day = date( 'Y-m-d', strtotime( $preceding_day ) - 86400 );
									break;
								}
								else $preceding_calculation = false;
							}
						}
					}
					$succeeding_off_day = false;
					$succeeding_off_days = array();
					$succeeding_calculation = true;
					$succeeding_day = date( 'Y-m-d', strtotime( $days[count( $days ) - 1] ) + 86400 );
					while( $succeeding_calculation ) {
						if( in_array( $succeeding_day, $textual_weekends ) ) {
							$succeeding_off_day = true;
							$weekends++;
							$succeeding_off_days[$succeeding_day] = array( 'Weekend', $leave_type );
							$succeeding_day = date( 'Y-m-d', strtotime( $succeeding_day ) + 86400 );
						}
						else {
							for( $m = 0; $m < count( $emp_holiday ); $m++ ) {
								if( check_date_in_range( $succeeding_day, $emp_holiday[$m]['from_date'], $emp_holiday[$m]['to_date'] ) ) {
									$succeeding_off_day = true;
									$holidays++;
									$succeeding_off_days[$succeeding_day] = array( 'Holiday', $leave_type );
									$succeeding_day = date( 'Y-m-d', strtotime( $succeeding_day ) + 86400 );
									break;
								}
								else $succeeding_calculation = false;
							}
						}
					}
					if( $preceding_off_day == true && $succeeding_off_day == true ) {
						$dates = array_merge( $preceding_off_days, $dates, $succeeding_off_days );
					}
					foreach( $dates AS $key => $value ) {
						$dates[$key][1] = $leave_type;
						$total_days++;
					}
				}
				else if( $leave_policy_details['off_day_leave_count'] == 2 ) {	//Include in-between off-days only
					foreach( $dates AS $key => $value ) {
						$dates[$key][1] = $leave_type;
						$total_days++;
					}
				}
				
				$first_day = ''; $last_day = '';
				$l = 0;
				
				foreach( $dates AS $date => $status ) {
					if( $l == 0 ) {
						$first_day = $date;
						$last_day = $date;
					}
					else $last_day = $date;
					$l++;
				}
				
				$first_day_overlap_check = check_uniqueness( "leave_date", "hrm_leave_transaction_details", "emp_code='$emp_code' AND leave_date = '$first_day' and status_active=1 and is_deleted=0 and is_locked=0", 0 );
				$last_day_overlap_check = check_uniqueness( "leave_date", "hrm_leave_transaction_details", "emp_code='$emp_code' AND leave_date = '$last_day' and status_active=1 and is_deleted=0 and is_locked=0", 0 );
				
				if( $first_day_overlap_check == false || $last_day_overlap_check == false ) {
					$unsaved_employees[] = $emp_code;
				}
				else {
					//leave check for available----------------------------//					
					$leave_balance=return_field_value("balance","hrm_leave_balance","emp_code='$emp_code' and leave_type='$leave_type' and is_locked=0 and is_deleted=0 and status_active=1");			
										
					if($total_days<=$leave_balance)
					{					
						$leave_id = return_next_id( "id", "hrm_leave_transaction" );
						$sql = "INSERT INTO hrm_leave_transaction (
									id,
									emp_code,
									leave_type,
									leave_policy_id,
									from_date,
									to_date,
									total_days,
									remarks,
									holidays,
									weekends
								) VALUES (
									$leave_id,
									'$emp_code',
									'$leave_type',
									'" . $emp_basic[$emp_code]['leave_policy'] . "',
									'" . convert_to_mysql_date( $from_date ) . "',
									'" . convert_to_mysql_date( $to_date ) . "',
									$total_days,
									'$remarks',
									$holidays,
									$weekends
								)";
								
						$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
						$all_query.=$sql;
						foreach( $dates AS $date => $status ) {
						$id = return_next_id( "id", "hrm_leave_transaction_details" );
						$sql = "INSERT INTO hrm_leave_transaction_details (
										id,
										emp_code,
										leave_id,
										leave_type,
										leave_date,
										day_status,
										leave_status
									) VALUES (
										$id,
										'$emp_code',
										'$leave_id',
										'1',
										'$date',
										'$status[0]',
										'$status[1]'
									)";
						$all_query.=$sql;
						$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );				

						if($status[1]=="Weekend")$day_status="W";else $day_status=$status[1]; 				
						mysql_query("update hrm_attendance set status='$day_status', late_time_min=0 where attnd_date='$date' and emp_code='$emp_code'") or die (mysql_error());
						}
					
					//leave check for available----------------------------//						
					$total_leave=return_field_value("SUM(total_days)","hrm_leave_transaction","emp_code='$emp_code' and leave_type='$leave_type' and status_active=1");
					if($total_leave==''){$total_leave=0;}else{$total_leave=$total_leave;}					
					
					$leave_limit=return_field_value("leave_limit","hrm_leave_balance","emp_code='$emp_code' and leave_type='$leave_type' and leave_year in (select id from lib_policy_year where type=0 and is_locked=0 and status_active=1) and is_locked=0 and status_active=1 and is_deleted=0");
					$balance = $leave_limit - $total_leave;
					
					$leave_balance_update_sql="update hrm_leave_balance set leave_availed='$total_leave', balance='$balance' where emp_code='$emp_code' and leave_type = '$leave_type' and year_start=" . date("Y") . " and leave_year in (select id from lib_policy_year where type=0 and is_locked=0 and status_active=1) and status_active=1";					
					$all_query.=$leave_balance_update_sql;
					mysql_query( $leave_balance_update_sql ) or die( $leave_balance_update_sql . "<br />" . mysql_error() );
					
					}
				else $unsaved_employees[] = $emp_code;				
			    }
			}
			else $unsaved_employees[] = $emp_code;
		}
		else {$unsaved_employees[] = $emp_code;$no_policy_emp[] = $emp_code;}
	}
	echo count( $employees ) . '_' . count( $unsaved_employees ) . '_' . implode( $unsaved_employees ). '_' . implode( $no_policy_emp );

	$sql_history=encrypt($all_query, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	exit();
}

function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata"; 
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}

?>