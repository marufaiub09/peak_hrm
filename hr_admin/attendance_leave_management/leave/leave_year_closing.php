<?php

session_start();
include('../../../includes/common.php');

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_leave.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	<script>
		function countUp()
		{		
			month = 'Jan';      // change the month to the one you want in the same format as shown
			date = '01';        // change the date to the one you want in the same format as shown
			year = '2002';      // change the year to the one you want in the same format as shown
			theDate = month + ' ' + date + ' ' + year;
			now = new Date();
			setdate = new Date(theDate);
			timer = (now - setdate) / 1000 / 60 / 60 / 24;
			timer = Math.round(timer);
			document.getElementById("secs").innerHTML=timer;
		 }

</script>

</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center">
	<div align="center" style="width:500px; position:relative; height:40px; margin:5px 0;" class="form_caption">
		Leave Year Closing
	</div>	
	<form id="leave_closing_form" method="GET">
		<fieldset style="width:500px;">
			<legend>Leave Year Closing Process</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td> Select Company Name</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:250px ">
                        	<? if($company_cond=="")
						   		{ 
								?> 
								<option value="0">----- Select -----</option>
							<?
								}
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 $company_cond order by company_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td> Select Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:250px ">
							<option value="0">----- Select -----</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td> Select Close Year</td>
					<td>
						<select name="cbo_closing_year" id="cbo_closing_year" class="combo_boxes" style="width:250px ">
							<option value="0">----- Select -----</option>
							<?
								$sql= mysql_db_query($DB, "select name,id from lib_policy_year where type=0 and is_deleted=0  and status_active=1 and is_locked=0 order by name");
								while ($row=mysql_fetch_array($sql))
								{
								?>
									<option value = <? echo $row["id"];?> > <? echo "$row[name]" ?> </option>								
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td> Select New Year</td>
					<td>
						<select name="cbo_new_year" id="cbo_new_year" class="combo_boxes" style="width:250px ">
							<option value="0">----- Select -----</option>
							<?
								$sql= mysql_db_query($DB, "select name,id from lib_policy_year where type=0 and is_deleted=0  and status_active=1 and is_locked=0 order by name");
								while ($row=mysql_fetch_array($sql))
								{
								?>
									<option value = <? echo $row["id"];?> > <? echo "$row[name]" ?> </option>								
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="button" name="data_process" id="data_process" value="Start Year Closing Process" style="width:270px; height:45px" class="formbutton" onclick="fnc_leave_year_close();" /> <input type="hidden" onclick="countUp()" /></td>
					
				</tr>
				<tr style="visibility:hidden">
					<td colspan="2" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<div align="center" id="messagebox"></div>
						<div id="process_anim"></div>
					</td>
				</tr>
			</table>
		</fieldset>
		
	</form>
	</div>
</body>
</html>