<? 
include('../../../includes/common.php');
extract( $_POST );
extract( $_GET );
	
	$emp_code=$_GET['emp_code'];
	//policy findout for specific enployee
	$sql = "SELECT * FROM hrm_employee WHERE emp_code=$emp_code and status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$rows=mysql_fetch_array($result);
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	//leave_policy
$sql = "SELECT * FROM lib_policy_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$leave_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$leave_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $row[id] AND is_deleted = 0 ORDER BY policy_id, id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$leave_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$leave_policy[$row['id']]['definition'][$row2['leave_type']] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$leave_policy[$row['id']]['definition'][$row2['leave_type']][$key2] = mysql_real_escape_string( $value2 );
		}
	}
}
//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//maternity_policy
$sql = "SELECT * FROM lib_policy_maternity_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$maternity_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$maternity_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$maternity_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//overtime_policy
$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$overtime_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$overtime_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	
}

//Holiday incentive policy
$sql = "SELECT * FROM lib_policy_holiday_incentive WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$holiday_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$holiday_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$holiday_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//duty roster policy
$sql = "SELECT distinct(roster_id) as id,roster_rule_name FROM lib_duty_roster_policy WHERE is_deleted = 0 and status_active=1 ORDER BY roster_rule_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$roster_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$roster_policy[$row['id']] = mysql_real_escape_string( $row['roster_rule_name'] );
}

//Attendance bonus policy 
$sql = "SELECT * FROM lib_policy_attendance_bonus WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$attendance_bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$attendance_bonus_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$attendance_bonus_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//absent deduction policy 
$sql = "SELECT * FROM lib_policy_absent_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$absent_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$absent_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$absent_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//late deduction policy 
$sql = "SELECT * FROM lib_policy_late_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$late_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$late_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$late_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$salary_breakdown_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$i = 0;
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][$i] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$salary_breakdown_policy[$row['id']]['definition'][$i][$key2] = mysql_real_escape_string( $value2 );
		}
		$i++;
	}
}



//Bonus policy 
$sql = "SELECT * FROM lib_bonus_policy WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$bonus_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );
}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/functions.js"></script>
<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../../js/popup_window.js"></script>
<script type="text/javascript" src="../../../js/modal.js"></script>


<script>

function new_window()
	{
		//alert("ekram");
		//document.getElementById('scroll_body').style.overflow="auto";
		//document.getElementById('scroll_body').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	
		d.close();
		//w.print() ;
		//document.getElementById('scroll_body').style.overflowY="scroll";
		//document.getElementById('scroll_body').style.maxHeight="230px";
	}	


</script>


</head>
<body>
<div style="width:900px" align="center"><input type="button" value="Print Preview" onclick="new_window()" style="width:100px"  class="formbutton"/>

 <div id="report_container" align="center" style="width:100%"> 

<div align="center" >
<form name="search_order_frm"  id="search_order_frm" autocomplete="off">
	<fieldset style="width:900px">
    
   <?
   $sql_leave_bl="select * from hrm_leave_balance where emp_code='$emp_code' and status_active=1 and is_deleted=0";
  //echo  $sql_leave_bl;die;
   $result_leave_bl=mysql_query($sql_leave_bl);
		  
		  $leave_lm_cl=0;$leave_la_cl=0;$leave_bl_cl=0;
		  $leave_lm_sl=0;$leave_la_sl=0;$leave_bl_sl=0;
		  $leave_lm_lwp=0;$leave_la_lwp=0;$leave_bl_lwp=0;
		  $leave_lm_rl=0;$leave_la_rl=0;$leave_bl_rl=0;
		  $leave_lm_spl=0;$leave_la_spl=0;$leave_bl_spl=0;
		  $leave_lm_el=0;$leave_la_el=0;$leave_bl_el=0;
		  $leave_lm_edl=0;$leave_la_edl=0;$leave_bl_edl=0;
		  
		  while($row_leave_bl=mysql_fetch_assoc($result_leave_bl))
		  {
		  	if($row_leave_bl[leave_type]=="CL") {
			  $leave_lm_cl=$row_leave_bl[leave_limit];
			  $leave_la_cl=$row_leave_bl[leave_availed];
			  $leave_bl_cl=$row_leave_bl[balance];
			  }
			 // echo $leave_lm_cl."-". $leave_la_cl."-".$leave_bl_cl;
		  	if($row_leave_bl[leave_type]=="SL") {
			  $leave_lm_sl=$row_leave_bl[leave_limit];
			  $leave_la_sl=$row_leave_bl[leave_availed];
			  $leave_bl_sl=$row_leave_bl[balance];
			  }
		  	if($row_leave_bl[leave_type]=="LWP") {
			  $leave_lm_lwp=$row_leave_bl[leave_limit];
			  $leave_la_lwp=$row_leave_bl[leave_availed];
			  $leave_bl_lwp=$row_leave_bl[balance];
			  }
			   if($row_leave_bl[leave_type]=="RL") {
			  $leave_lm_rl=$row_leave_bl[leave_limit];
			  $leave_la_rl=$row_leave_bl[leave_availed];
			  $leave_bl_rl=$row_leave_bl[balance];
			  }
		  	if($row_leave_bl[leave_type]=="SpL") {
			  $leave_lm_spl=$row_leave_bl[leave_limit];
			  $leave_la_spl=$row_leave_bl[leave_availed];
			  $leave_bl_spl=$row_leave_bl[balance];
			  }
			if($row_leave_bl[leave_type]=="EL") {
			  $leave_lm_el=$row_leave_bl[leave_limit];
			  $leave_la_el=$row_leave_bl[leave_availed];
			  $leave_bl_el=$row_leave_bl[balance];
			  }
			 if($row_leave_bl[leave_type]=="EdL") {
			  $leave_lm_edl=$row_leave_bl[leave_limit];
			  $leave_la_edl=$row_leave_bl[leave_availed];
			  $leave_bl_edl=$row_leave_bl[balance];
			  }
		  }
   ?> 
    
	<table width="900" cellspacing="0" cellpadding="0" border="1">
		<thead>
        	<tr>
                <th align="center" width="100px"><b>ID Card</b></th>
                <th align="center" width="170px"><b>Name</b></th>
                <th align="center" width="150px"><b>Designation</b></th>
                <th align="center" width="90px"><b>Joining Date</b></th>
                
                
                <th align="center" width="110px"><b>Leave Status</b></th>
                <th align="center" width="40px"><b>CL</b></th>
                <th align="center" width="40px"><b>SL</b></th>
                <th align="center" width="40px"><b>LWP</b></th>
                <th align="center" width="40px"><b>RL</b></th>
                <th align="center" width="40px"><b>SPL</b></th>
                <th align="center" width="40px"><b>EL</b></th>
                <th align="center" width="40px"><b>EDL</b></th>
            </tr>
        </thead>
        <tr style="font-size:12px">
        	<td align="center" rowspan="3"><?php echo $id_card_no;?></td>
            <td align="center" rowspan="3"><?php echo $name;?></td>
            <td align="center" rowspan="3"><?php echo $designation_chart[$designation_id];?></td>
            <td align="center" rowspan="3"><?php echo $joining_date;?></td>
            
        	<td align="center"><b>Entitlement</b></td>
            <td align="center"><?php echo $leave_lm_cl;?></td>
            <td align="center"><?php echo $leave_lm_sl;?></td>
            <td align="center"><?php echo $leave_lm_lwp;?></td>
            <td align="center"><?php echo $leave_lm_rl;?></td>
            <td align="center"><?php echo $leave_lm_spl;?></td>
            <td align="center"><?php echo $leave_lm_el;?></td>
            <td align="center"><?php echo $leave_lm_edl;?></td>
        </tr>
        <tr style="font-size:12px">
        	<td align="center"><b>Taken/Enjoyed</b></td>
            <td align="center"><?php echo $leave_la_cl;?></td>
            <td align="center"><?php echo $leave_la_sl;?></td>
            <td align="center"><?php echo $leave_la_lwp;?></td>
            <td align="center"><?php echo $leave_la_rl;?></td>
            <td align="center"><?php echo $leave_la_spl;?></td>
            <td align="center"><?php echo $leave_la_el;?></td>
            <td align="center"><?php echo $leave_la_edl;?></td>
        </tr>
         <tr style="font-size:12px">
        	<td align="center"><b>Balance</b></td>
            <td align="center"><?php echo $leave_bl_cl;?></td>
            <td align="center"><?php echo $leave_bl_sl;?></td>
            <td align="center"><?php echo $leave_bl_lwp;?></td>
            <td align="center"><?php echo $leave_bl_rl;?></td>
            <td align="center"><?php echo $leave_bl_spl;?></td>
            <td align="center"><?php echo $leave_bl_el;?></td>
            <td align="center"><?php echo $leave_bl_edl;?></td>
        </tr>
        
	</table>
	</fieldset>
   
	</form>
</div>

<div style="height:20px;">
</div>



  <div align="center">  
<fieldset style="width:900px;">
	
    <div align="center">
        		
            <table width="200" cellspacing="0" cellpadding="0" border="1" align="left">
                <thead>
                    <tr>
                    	<th align="center" colspan="3"><b>Casual Leave</b></th>
                    </tr>
                    <tr>
                    	<th align="center"><b>Sl</b></th>
                        <th align="center"><b>Date Range</b></th>
                         <th align="center"><b>Days</b></th>
                    </tr>
                </thead>
                <?
				$i=1;
   				$sql="select * from hrm_leave_transaction where emp_code='$emp_code' and leave_type='CL' and status_active=1 and is_deleted=0";
				$result=mysql_query($sql);
				while($row_emp=mysql_fetch_assoc($result)){
				?>
                    <tr style="font-size:12px">
                    <td align="center" width="20"><?php echo $i;?></td>
                    <td align="left" width="120"><?php echo convert_to_mysql_date($row_emp[from_date])."&nbsp;To&nbsp;".convert_to_mysql_date($row_emp[to_date]);?></td>
                    <td align="center" width="50"><?php echo $row_emp[total_days];?></td>
                    </tr>
                    <?
				$i++;
				}
				?>
            </table>
            <table width="200" cellspacing="0" cellpadding="0" border="1" align="left" style="margin-left:20px;">
                <thead>
                    <tr>
                    	<th align="center" colspan="3"><b>Sick Leave</b></th>
                    </tr>
                    <tr>
                    	<th align="center"><b>Sl</b></th>
                        <th align="center"><b>Date Range</b></th>
                         <th align="center"><b>Days</b></th>
                    </tr>
                </thead>
                <?
				$s=1;
   				$sqls="select * from hrm_leave_transaction where emp_code='$emp_code' and leave_type='SL' and status_active=1 and is_deleted=0";
				$results=mysql_query($sqls);
				while($row_emp=mysql_fetch_assoc($results)){
				?>
                    <tr style="font-size:12px">
                    <td align="center" width="20"><?php echo $s;?></td>
                    <td align="left" width="120"><?php echo convert_to_mysql_date($row_emp[from_date])."&nbsp;To&nbsp;".convert_to_mysql_date($row_emp[to_date]);?></td>
                    <td align="center" width="50"><?php echo $row_emp[total_days];?></td>
                    </tr>
                    <?
				$s++;
				}
				
				?>
            </table>
            <table width="200" cellspacing="0" cellpadding="0" border="1" align="left" style="margin-left:20px;">
                <thead>
                    <tr>
                    	<th align="center" colspan="3"><b>Leave without Pay</b></th>
                    </tr>
                    <tr>
                    	<th align="center"><b>Sl</b></th>
                        <th align="center"><b>Date Range</b></th>
                         <th align="center"><b>Days</b></th>
                    </tr>
                </thead>
                <?
				$s=1;
   				$sqls="select * from hrm_leave_transaction where emp_code='$emp_code' and leave_type='LWP' and status_active=1 and is_deleted=0";
				$results=mysql_query($sqls);
				while($row_emp=mysql_fetch_assoc($results)){
				 
				?>
                    <tr style="font-size:12px">
                    <td align="center" width="20"><?php echo $s;?></td>
                    <td align="left" width="120"><?php echo convert_to_mysql_date($row_emp[from_date])."&nbsp;To&nbsp;".convert_to_mysql_date($row_emp[to_date]);?></td>
                    <td align="center" width="50"><?php echo $row_emp[total_days];?></td>
                    </tr>
                    <?
				$s++;
				}
				
				?>
            </table>
      
             <table width="200" cellspacing="0" cellpadding="0" border="1" align="left" style="margin-left:20px;">
                <thead>
                    <tr>
                    	<th align="center" colspan="3"><b>Recreation Leave</b></th>
                    </tr>
                    <tr>
                    	<th align="center"><b>Sl</b></th>
                        <th align="center"><b>Date Range</b></th>
                         <th align="center"><b>Days</b></th>
                    </tr>
                </thead>
                <?
				$s=1;
   				$sqls="select * from hrm_leave_transaction where emp_code='$emp_code' and leave_type='RL' and status_active=1 and is_deleted=0";
				$results=mysql_query($sqls);
				while($row_emp=mysql_fetch_assoc($results)){
				 
				?>
                    <tr style="font-size:12px">
                    <td align="center" width="20"><?php echo $s;?></td>
                    <td align="left" width="120"><?php echo convert_to_mysql_date($row_emp[from_date])."&nbsp;To&nbsp;".convert_to_mysql_date($row_emp[to_date]);?></td>
                    <td align="center" width="50"><?php echo $row_emp[total_days];?></td>
                    </tr>
                    <?
				$s++;
				}
				
				?>
            </table>
   </div>
   
 
<div align="left" style="float:left;width:900px; height:20px">
</div>

 <div align="left" style="float:left;width:900px;">
             <table width="200" cellspacing="0" cellpadding="0" border="1" align="left">
                <thead>
                    <tr>
                    	<th align="center" colspan="3"><b>Special Leave</b></th>
                    </tr>
                    <tr>
                    	<th align="center"><b>Sl</b></th>
                        <th align="center"><b>Date Range</b></th>
                         <th align="center"><b>Days</b></th>
                    </tr>
                </thead>
                <?
				$s=1;
   				$sqls="select * from hrm_leave_transaction where emp_code='$emp_code' and leave_type='SPL' and status_active=1 and is_deleted=0";
				$results=mysql_query($sqls);
				while($row_emp=mysql_fetch_assoc($results)){
				?>
                    <tr style="font-size:12px">
                    <td align="center" width="20"><?php echo $s;?></td>
                    <td align="left" width="120"><?php echo convert_to_mysql_date($row_emp[from_date])."&nbsp;To&nbsp;".convert_to_mysql_date($row_emp[to_date]);?></td>
                    <td align="center" width="50"><?php echo $row_emp[total_days];?></td>
                    </tr>
                    <?
				$s++;
				}
				
				?>
            </table>
            <table width="200" cellspacing="0" cellpadding="0" border="1" align="left" style="margin-left:20px;">
                <thead>
                    <tr>
                    	<th align="center" colspan="3"><b>Earned Leave</b></th>
                    </tr>
                    <tr>
                    	<th align="center"><b>Sl</b></th>
                        <th align="center"><b>Date Range</b></th>
                         <th align="center"><b>Days</b></th>
                    </tr>
                </thead>
                <?
				$s=1;
   				$sqls="select * from hrm_leave_transaction where emp_code='$emp_code' and leave_type='EL' and status_active=1 and is_deleted=0";
				$results=mysql_query($sqls);
				while($row_emp=mysql_fetch_assoc($results)){
				 
				?>
                    <tr style="font-size:12px">
                    <td align="center" width="20"><?php echo $s;?></td>
                    <td align="left" width="120"><?php echo convert_to_mysql_date($row_emp[from_date])."&nbsp;To&nbsp;".convert_to_mysql_date($row_emp[to_date]);?></td>
                    <td align="center" width="50"><?php echo $row_emp[total_days];?></td>
                    </tr>
                    <?
				$s++;
				}
				
				?>
            </table>
      
             <table width="200" cellspacing="0" cellpadding="0" border="1" align="left" style="margin-left:20px;">
                <thead>
                    <tr>
                    	<th align="center" colspan="3"><b>Education Leave</b></th>
                    </tr>
                    <tr>
                    	<th align="center"><b>Sl</b></th>
                        <th align="center"><b>Date Range</b></th>
                         <th align="center"><b>Days</b></th>
                    </tr>
                </thead>
                <?
				$s=1;
   				$sqls="select * from hrm_leave_transaction where emp_code='$emp_code' and leave_type='EDL' and status_active=1 and is_deleted=0";
				$results=mysql_query($sqls);
				while($row_emp=mysql_fetch_assoc($results)){
				 
				?>
                    <tr style="font-size:12px">
                    <td align="center" width="20"><?php echo $s;?></td>
                    <td align="left" width="120"><?php echo convert_to_mysql_date($row_emp[from_date])."&nbsp;To&nbsp;".convert_to_mysql_date($row_emp[to_date]);?></td>
                    <td align="center" width="50"><?php echo $row_emp[total_days];?></td>
                    </tr>
                    <?
				$s++;
				}
				
				?>
            </table>
    
    </div>

  </fieldset>
</div>
</div>
</div>
</body>
