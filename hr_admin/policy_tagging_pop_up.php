<? 
	include('../includes/common.php');
	$emp_code=$_GET['emp_code'];
	//policy findout for specific enployee
	$sql = "SELECT * FROM hrm_employee WHERE emp_code=$emp_code and status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$rows=mysql_fetch_array($result);
	
	
	//leave_policy
$sql = "SELECT * FROM lib_policy_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$leave_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$leave_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $row[id] AND is_deleted = 0 ORDER BY policy_id, id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$leave_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$leave_policy[$row['id']]['definition'][$row2['leave_type']] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$leave_policy[$row['id']]['definition'][$row2['leave_type']][$key2] = mysql_real_escape_string( $value2 );
		}
	}
}
//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//maternity_policy
$sql = "SELECT * FROM lib_policy_maternity_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$maternity_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$maternity_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$maternity_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//overtime_policy
$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$overtime_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$overtime_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	
}

//Holiday incentive policy
$sql = "SELECT * FROM lib_policy_holiday_incentive WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$holiday_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$holiday_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$holiday_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//duty roster policy
$sql = "SELECT distinct(roster_id) as id,roster_rule_name FROM lib_duty_roster_policy WHERE is_deleted = 0 and status_active=1 ORDER BY roster_rule_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$roster_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$roster_policy[$row['id']] = mysql_real_escape_string( $row['roster_rule_name'] );
}

//Attendance bonus policy 
$sql = "SELECT * FROM lib_policy_attendance_bonus WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$attendance_bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$attendance_bonus_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$attendance_bonus_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//absent deduction policy 
$sql = "SELECT * FROM lib_policy_absent_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$absent_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$absent_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$absent_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//late deduction policy 
$sql = "SELECT * FROM lib_policy_late_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$late_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$late_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$late_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$salary_breakdown_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$i = 0;
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][$i] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$salary_breakdown_policy[$row['id']]['definition'][$i][$key2] = mysql_real_escape_string( $value2 );
		}
		$i++;
	}
}

//Bonus policy 
$sql = "SELECT * FROM lib_bonus_policy WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$bonus_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );
}

//Tiffin policy 
$sql = "SELECT * FROM lib_policy_tiffin_bill_mst WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$tiffin_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$tiffin_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );
}

// Allowance policy 
$sql = "SELECT * FROM  lib_policy_allowance WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$allowance_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$allowance_policy[$row['policy_id']] = mysql_real_escape_string( $row['policy_name'] );
}



//early deduction policy 
$sql = "SELECT * FROM lib_policy_early_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$early_out_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$early_out_deduction_policy[$row['id']]= mysql_real_escape_string( $row['policy_name'] );
}

	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<link href="../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/functions.js"></script>
<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/popup_window.js"></script>
<script type="text/javascript" src="../js/modal.js"></script>
<script>

function fn_remove_policy(policy_id,emp_code,type)
		{
			document.getElementById('policy_id').value=policy_id;
			document.getElementById('emp_code').value=emp_code;
			document.getElementById('type').value=type;
			parent.emailwindow.hide();			
		}

	
</script>


</head>
<body>
<div align="center">
<form name="search_order_frm"  id="search_order_frm" autocomplete="off">
	<fieldset style="width:500px">
	<table width="500" cellspacing="2" cellpadding="0" border="0">
		<tr>
			<td width="390" align="center"><h2>Policy Tagging with Employee Report</h2>
		    <hr /></td>
		</tr>
		<tr> 	 	 	 	 	 	 	 	 	
			<td align="center">
            	<!-- Only Two policy tagging here -->               
                <table>
                	<tr>
                    	<td>1. Overtime Policy</td>
                        <td><input type="text" name="txt_overtime_policy" id="txt_overtime_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $overtime_policy_id=$rows["overtime_policy"]; if($overtime_policy_id!=0) { echo $overtime_policy[$overtime_policy_id]["policy_name"];}else{echo"0";} ?>" /><input type="button" id="btn_ot_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $overtime_policy_id; ?>,'<? echo $emp_code; ?>',1)"  /></td>
                    </tr>
                    <tr>
                    	<td>2. Holiday Incentive</td>
                        <td><input type="text" name="txt_holiday_policy" id="txt_holiday_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $holiday_incentive_policy=$rows["holiday_incentive_policy"]; if($holiday_incentive_policy!=0) { echo $holiday_policy[$holiday_incentive_policy]["policy_name"];}else{echo "0";} ?>" /><input type="button" id="btn_hi_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $holiday_incentive_policy; ?>,'<? echo $emp_code; ?>',2)"  /></td>
                    </tr>
                    <tr>
                    	<td>3. Duty Roster Policy</td>
                        <td><input type="text" name="txt_rosterduty_policy" id="txt_rosterduty_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $duty_roster_policy=$rows["duty_roster_policy"]; if($duty_roster_policy!=0) {echo $roster_policy[$rows["duty_roster_policy"]];}else{echo"0";} ?>"/><input type="button" id="btn_dr_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $rows["duty_roster_policy"]; ?>,'<? echo $emp_code; ?>',3)"  /></td>
                    </tr>
                    <tr>
                    	<td>4. Leave Policy</td>
                        <td><input type="text" name="txt_leave_policy" id="txt_leave_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $leave_policy_id=$rows["leave_policy"]; if($leave_policy_id!=0) {echo $leave_policy[$leave_policy_id]["policy_name"];} else echo "0"; ?>" /><input type="button" id="btn_lp_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $leave_policy_id; ?>,'<? echo $emp_code; ?>',4)"  /></td>
                    </tr>
                    <tr>
                    	<td>5. Maternity Leave Policy</td>
                        <td><input type="text" name="txt_maternity_policy" id="txt_maternity_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $maternity_leave_policy=$rows["maternity_leave_policy"]; if($maternity_leave_policy!=0) {echo $maternity_policy[$maternity_leave_policy]["policy_name"];}else{echo"0";} ?>" /><input type="button" id="btn_ml_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $maternity_leave_policy; ?>,'<? echo $emp_code; ?>',5)"  /></td>
                    </tr>
                    <tr>
                    	<td>6. Attendance Bonus Policy</td>
                        <td><input type="text" name="txt_attd_bonus_policy" id="txt_attd_bonus_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $attendance_bonus_policy_id=$rows["attendance_bonus_policy"]; if($attendance_bonus_policy_id!=0) {echo $attendance_bonus_policy[$attendance_bonus_policy_id]["policy_name"];}else{echo"0";} ?>" /><input type="button" id="btn_ab_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $attendance_bonus_policy_id; ?>,'<? echo $emp_code; ?>',6)"  /></td>
                    </tr>
                    <tr>
                    	<td>7. Absent Deduction Policy</td>
                        <td><input type="text" name="txt_absent_deduct_policy" id="txt_absent_deduct_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $absent_deduction_policy_id=$rows["absent_deduction_policy"]; if($absent_deduction_policy_id!=0) {echo $absent_deduction_policy[$absent_deduction_policy_id]["policy_name"];}else{echo"0";} ?>" /><input type="button" id="btn_ad_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $absent_deduction_policy_id; ?>,'<? echo $emp_code; ?>',7)"  /></td>
                    </tr>
                    <tr>
                    	<td>8. Late Deduction Policy</td>
                        <td><input type="text" name="txt_late_deduct_policy" id="txt_late_deduct_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $late_deduction_policy_id=$rows["late_deduction_policy"]; if($late_deduction_policy_id!=0) {echo $late_deduction_policy[$late_deduction_policy_id]["policy_name"];}else{echo"0";} ?>" /><input type="button" id="btn_ld_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $late_deduction_policy_id; ?>,'<? echo $emp_code; ?>',8)"  /></td>
                    </tr>
                    <tr>
                    	<td>9. Bonus Policy</td>
                        <td><input type="text" name="txt_bonus_policy" id="txt_bonus_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $bonuspolicy=$rows["bonus_policy"]; if($bonuspolicy!=0) {echo $bonus_policy[$rows["bonus_policy"]];}else{echo"0";} ?>" /><input type="button" id="btn_bp_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $rows["bonus_policy"]; ?>,'<? echo $emp_code; ?>',9)"  /></td>
                    </tr>
                    <tr>
                    	<td>10. Tax Policy</td>
                        <td><input type="text" name="txt_tax_policy" id="txt_tax_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $tax_policy=$rows["tax_policy"]; if($tax_policy!=0) {echo $rows["tax_policy"];}else{echo"0";} ?>" /><input type="button" id="btn_tp_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $rows["tax_policy"]; ?>,'<? echo $emp_code; ?>',10)"  /></td>
                    </tr>
                    <tr>
                    	<td>11. Shift Policy</td>
                        <td><input type="text" name="txt_shift_policy" id="txt_shift_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $shift_policy_id=$rows["shift_policy"]; if($shift_policy_id!=0){echo $shift_policy[$shift_policy_id]["shift_name"];} else {echo "0";} ?>"/></td>
                    </tr>
                    <tr>
                    	<td>12. Tiffin Policy</td>
                        <td><input type="text" name="txt_tiffin_policy" id="txt_tiffin_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $tiffinpolicy=$rows["tiffin_policy"]; if($tiffinpolicy!=0){echo $tiffin_policy[$rows["tiffin_policy"]];} else {echo "0";} ?>"/><input type="button" id="btn_tp_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $rows["tiffin_policy"]; ?>,'<? echo $emp_code; ?>',11)"  /></td>
                    </tr> 
                    <tr>
                    	<td>13. Allowance Policy</td>
                        <td><input type="text" name="txt_allowance_policy" id="txt_allowance_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $allowancepolicy=$rows["allowance_policy"]; if($allowancepolicy!=0){echo $allowance_policy[$rows["allowance_policy"]];} else {echo "0";} ?>"/><input type="button" id="btn_tp_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $rows["allowance_policy"]; ?>,'<? echo $emp_code; ?>',12)"  /></td>
                    </tr>
                    <tr>
                    	<td>14. Early Out Deduction Policy</td>
                        <td><input type="text" name="txt_allowance_policy" id="txt_allowance_policy" class="text_boxes" style="width:200px" readonly="readonly" value="<? $earlydeductionpolicy=$rows["early_deduction_policy"]; if($earlydeductionpolicy!=0){echo $early_out_deduction_policy[$rows["early_deduction_policy"]];} else {echo "0";} ?>"/><input type="button" id="btn_tp_remove" class="formbutton" value="Remove" onclick="javascript:fn_remove_policy(<? echo $rows["early_deduction_policy"]; ?>,'<? echo $emp_code; ?>',13)"  /></td>
                    </tr>                       
                </table>
            </td>
		</tr>
	</table>
	</fieldset>
    <input type="hidden" id="policy_id" value="" />
    <input type="hidden" id="emp_code" value="" />
    <input type="hidden" id="type" value="" />
	</form>
</div>

</body>
