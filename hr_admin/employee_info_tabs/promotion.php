<script type="text/javascript">
	<?php
	if( isset( $_REQUEST['status'] ) && $_REQUEST['status'] == 2 ) {
		mysql_select_db( 'pim', $host_connect );
		$counter = mysql_num_rows( mysql_query( "SELECT * FROM promotion_info WHERE EmpNo = '" . $_REQUEST['emp_code'] . "'" ) );
		if( $counter > 0 ) {
			echo "var promotion_counter = " . $counter . ";";
		} else {
			echo "var promotion_counter = 1;";
		}
	} else {
		echo "var promotion_counter = 1;";
	}
	?>
	
	function add_promotion( promotion_id ) {
		if( promotion_id == promotion_counter ) {
			promotion_counter++;
			
			$('#tbl_promotion tbody').append(
				'<tr align="center">'
					+ '<td><input	type="text"	name="PromotionDate[]"			id="PromotionDate' + promotion_counter + '"			value=""	class="datepicker"	onfocus="add_promotion( ' + promotion_counter + ' );" /></td>'
					+ '<td><input	type="text"	name="PayScale[]"				id="PayScale' + promotion_counter + '"				value="" /></td>'
					+ '<td><input	type="text"	name="JoiningDate[]"			id="promJoiningDate' + promotion_counter + '"			value=""	class="datepicker"	onfocus="getdatepicker();" /></td>'
					+ '<td><input	type="text"	name="PromotedDesignation[]"	id="PromotedDesignation' + promotion_counter + '"	value="" /></td>'
				+ '</tr>'
			);
		}
		getdatepicker();
	}
</script>
<table id="tbl_promotion" style="width:100%;" border="0" cellpadding="0" cellspacing="3">
	<thead>
		<tr>
			<th class="header">Last Designation</th>
			<th class="header">New Designation Level</th>
			<th class="header">New Designation</th>
			<th class="header">Effective Date</th>
			<th class="header">Increment Applicable</th>
			<th class="header">Increment Entry</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td valign="top">Last Designation</td>
			<td valign="top">
				<select name="new_designation_level">
					<?php
					mysql_select_db( 'system_library', $host_connect );
					$result = mysql_query( "SELECT DISTINCT level FROM designation_info ORDER BY level ASC" );
					if( !$result ) {
						die( 'Invalid query: ' . mysql_error() );
					}
					while( $row = mysql_fetch_array( $result ) ) {
						echo "<option value=\"" . $row['level'] . "\">" . $row['level'] . "</option>";
					}
					?>
				</select>
			</td>
			<td valign="top">
				<select name="new_designation">
				<?php
					 
					$result = mysql_query( "SELECT CustomDesignation FROM designation_info ORDER BY level ASC" );
					 
					while( $row = mysql_fetch_assoc( $result ) ) {
						echo "<option value=\"" . $row['CustomDesignation'] . "\">" . $row['CustomDesignation'] . "</option>";
					}
					?>
				</select>
			</td>
			<td valign="top"><input type="text" name="effective_date" value="" class="datepicker" /></td>
			<td valign="top"><input type="checkbox" name="increment_applicable" value="1" /></td>
			<td valign="top"><a href="#">Enter</a></td>
		</tr>
	</tbody>
</table>