<?php
include('../../includes/common.php');

//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown where is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = $row;
	
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head   ORDER BY custom_head ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<script type="text/javascript">
    var head_counter = 0, payroll_heads = new Array(), payroll_options = '<option value="0"></option>';
	<?php 
	foreach( $payroll_heads AS $payroll_id => $payroll ) {
		if( $payroll['type'] != 3 || $payroll['type'] != 4 || $payroll['type'] != 2 ) {
			echo "payroll_options += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';\n";
			
			echo "payroll_heads[$payroll_id] = new Array();\n";
			foreach( $payroll AS $key => $value ) {
				if( $key == 'id' ) echo "payroll_heads[$payroll_id]['$key'] = $value;\n";
				else echo "payroll_heads[$payroll_id]['$key'] = '".$value."';\n";
			}
		}
	}
	?>
	 
	$(document).ready(function() {
		$( "#formula_editor" ).hide();
	});
	
		
	function add_salary_breakdown() {
		var head_counter=$('#salary tbody tr').length
		head_counter++;
		$('#salary tbody').append(
			'<tr>'
				+ '<td>'
					+ '<select name="payroll_head[]" id="payroll_head_' + head_counter + '" class="combo_boxes">'
						+ payroll_options
					+ '</select>'
				+ '</td>'
				+ '<td>'
					+ '<select name="type[]" id="type_' + head_counter + '" onchange="change_type( ' + head_counter + ' )" class="combo_boxes">'
						+ '<option value="%">%</option>'
						+ '<option value="Fixed">Fixed</option>'
						+ '<option value="Formula">Formula</option>'
					+ '</select>'
				+ '</td>'
				+ '<td><input type="text" name="percentage_formula[]" id="percentage_formula_' + head_counter + '" onfocus="add_formula( ' + head_counter + ' );" onblur="calculate_amount( ' + head_counter + ' );" value="" class="text_boxes numbers" /></td>'
				+ '<td>'
					+ '<select name="base_head[]" id="base_head_' + head_counter + '" onchange="calculate_amount( ' + head_counter + ' );" class="combo_boxes">'
						+ payroll_options
					+ '</select>'
				+ '</td>'
				+ '<td><input type="text" name="amount[]" id="amount_' + head_counter + '" value="0" onblur="calculate_total();" class="text_boxes numbers" disabled /></td>'
			+ '</tr>'
		);
		change_type( head_counter );
	}
	
	function salary_breakdown_policy_change( policy_id ) {
		if( policy_id != 0 ) {
			$.ajax({
				type: "POST",
				url: "employee_info_tabs/salary_rule.php?policy_id=" + policy_id,
				success: function( html ) {
					$('#salary tbody').html( html );
					head_counter = $('#salary tbody tr').length;
				}
			});
		}
		else {
			$('#salary tbody input[type="text"]').each(function() { $(this).removeAttr( 'disabled' ); });
			$('#salary tbody select').each(function() { $(this).removeAttr( 'disabled' ); });
		}
	}
	
	function calculate_amount( counter ) {
		var relatee = $('#base_head_' + counter).val();
		var add_value = 0;
		
		for( var i = 1; i <= head_counter; i++ ) {
			if( $('#payroll_head_' + i).val() == relatee ) {
				add_value = $('#amount_' + i).val() * $('#percentage_formula_' + counter).val() / 100;
				$('#amount_' + counter).val( add_value.toFixed( 2 ) );
				break;
			}
		}
		calculate_total();
	}
	
	function calculate_all() {
 		if( $('#gross_salary').val() > 0 ) {
			for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
				if( $('#payroll_head_' + i).val() == 0 ) {
					$('#payroll_head_' + i).parent().parent().remove();
					head_counter--;
					for( var j = i; j <= $('#salary tbody tr').length; j++ ) {
						//alert('hi');//need to update the next ids if the deleted id is not the last
					}
				}
				else if( $('#type_' + i).val() == 'Fixed' && $('#amount_' + i).val() < 1 ) {
					alert('Please provide the fixed values first.');
					return false;
				}
				else if( $('#type_' + i).val() == 'Fixed' ) {
					$('#amount_' + i).val( parseFloat( $('#amount_' + i).val() ).toFixed( 2 ) );
				}
			} 
			for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
				if( $('#type_' + i).val() == 'Formula' ) {
					var formula = $('#percentage_formula_' + i).val();
					var formula2 = '';
					var abbreviation = '';
					for( var j = 0; j < formula.length; j++ ) {
						if( formula.charCodeAt( j ) >= 40 && formula.charCodeAt( j ) <= 57 && formula.charCodeAt( j ) != 44 ) {
							if( abbreviation != '' ) {
								if( abbreviation == 'GS' ) {
									formula2 += $('#gross_salary').val();
								}
								else {
									for( var k = 0; k < payroll_heads.length; k++ ) {
										if( payroll_heads[k] != undefined && payroll_heads[k]['abbreviation'] == abbreviation ) {
											for( var l = 1; l <= $('#salary tbody tr').length; l++ ) {
												if( $('#payroll_head_' + l).val() == payroll_heads[k]['id'] ) {
													if( parseInt( $('#amount_' + l).val() ) < 1 ) {
														alert( 'Please define ' + payroll_heads[k]['custom_head'] + ' first.' );
														return false;
													}
													else formula2 += parseInt( $('#amount_' + l).val() );
													break;
												}
											}
											break;
										}
									}
								}
								abbreviation = '';
							}
							formula2 += formula[j];
						}
						else abbreviation += formula[j];
					} 
					jQuery.globalEval( 'var formula_value = ' + formula2 );
					$('#amount_' + i).val( formula_value.toFixed( 2 ) );
				}
				else if( $('#type_' + i).val() == '%' ) {
					if( $('#base_head_' + i).val() == 20 ) {
						var percentage_value = $('#gross_salary').val() * $('#percentage_formula_' + i).val() / 100;
						$('#amount_' + i).val( parseFloat( percentage_value ).toFixed( 2 ) );
					}
					else {
						for( var j = 1; j <= $('#salary tbody tr').length; j++ ) {
							if( $('#payroll_head_' + j).val() == $('#base_head_' + i).val() ) {
								if( parseInt( $('#amount_' + j).val() ) < 1 ) {
									alert( 'Undefined.' );
									return false;
								}
								else {
									var percentage_value = $('#amount_' + j).val() * $('#percentage_formula_' + i).val() / 100;
									$('#amount_' + i).val( parseFloat( percentage_value ).toFixed( 2 ) );
								}
								break;
							}
						}
					}
				}
			}
		}
		else alert( 'To calculate salary please provide gross salary' );
		
		calculate_total();
	}
	
	function calculate_total() {
		var earning = 0;
		var deduction = 0;
		
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			for( var j = 1; j <= payroll_heads.length; j++ ) {
				if( $('#payroll_head_' + i).val() != 0 && $('#payroll_head_' + i).val() == payroll_heads[j]['id'] ) {
					if( payroll_heads[j]['type'] == 0 ) earning += parseFloat( $('#amount_' + i).val() );
					else deduction += parseFloat( $('#amount_' + i).val() );
					break;
				}
			}
		}
		$('#total_salary').html( parseFloat( earning - deduction ).toFixed( 2 ) );
	}
		
	function change_type( counter ) {
		switch( $("#type_" + counter).val() ) {
			case '%':
				$('#percentage_container').html('Percentage');
				$("#percentage_formula_" + counter).removeAttr( 'disabled' );
				$("#base_head_" + counter).removeAttr( 'disabled' );
				$("#amount_" + counter).attr( 'disabled', 'disabled' );
				break;
			case 'Fixed':
				$("#percentage_formula_" + counter).attr( 'disabled', 'disabled' );
				$("#base_head_" + counter).val( '0' );
				$("#base_head_" + counter).attr( 'disabled', 'disabled' );
				$("#percentage_formula_" + counter).val( '' );
				$("#amount_" + counter).removeAttr( 'disabled' );
				break;
			case 'Formula':
				$('#percentage_container').html('Formula');
				$("#percentage_formula_" + counter).removeAttr( 'disabled' );
				$("#percentage_formula_" + counter).val( '' );
				$("#base_head_" + counter).val( '0' );
				$("#base_head_" + counter).attr( 'disabled', 'disabled' );
				$("#amount_" + counter).attr( 'disabled', 'disabled' );
				break;
		}
	}
	
	function add_formula( counter ) {
		if( $("#type_" + counter).val() == 'Formula' ) {
			$('input[name="formula"]').val( $("#percentage_formula_" + counter).val() );
			$( "#formula_editor" ).dialog({
				modal: true,
				width: 348,
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
						$("#percentage_formula_" + counter).val( $('input[name="formula"]').val() )
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				}
			});
		}
	}
	
	function write_formula( btn_name ) {
		if( btn_name != 'backspace' ) $('input[name="formula"]').val( $('input[name="formula"]').val() + $('input[name="' + btn_name + '"]').attr('fval') );
		else {
			var text = $('input[name="formula"]').val();
			var backSpace = text.substr( 0, text.length - 1 );
			$('input[name="formula"]').val( backSpace );
		}
	}
	
	function reset_salary_form() {
		$('#salary tbody').html( '' );
		document.getElementById('emp_salary_form').reset();
	}
	
</script>
<style type="text/css">
	#emp_salary_form input[type="text"] { width:195px; }
	#emp_salary_form select { width:205px; }
   .operator { width:50px; padding:2px 0; }
</style>
<form id="emp_salary_form" action="javascript:fnc_emp_salary(save_perm,edit_perm,delete_perm,approve_perm)" method="POST" autocomplete="off">
	<table border="0" cellpadding="0" cellspacing="2" style="width:100%;">
		<tr>
			<td width="8%">Salary Grade:</td>
			<td width="13%"><input type="text" name="salary_grade" id="salary_grade" class="text_boxes" value="" style="width:100px" /></td>
			<td width="6%">Policy:</td>
			<td width="18%">
				<select name="salary_rule" id="salary_rule" class="combo_boxes" onchange="salary_breakdown_policy_change( this.value )" style="width:170px"  >
					<option value="0">---Select-----</option>
					<?php foreach( $salary_breakdown_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
					<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
					<?php } } ?>
				</select>
			</td>
			<td width="9%">Gross Salary:</td>
			<td width="17%"><input type="text" name="gross_salary" id="gross_salary" class="text_boxes numbers" value="" style="width:150px"  /></td>
            <td>Bank Salary</td>
            <td width=""><input type="text" name="bank_salary" id="bank_salary" class="text_boxes numbers" value="" style="width:150px"  /></td>
		</tr>
	</table>
	<table id="salary" border="0" cellpadding="0" cellspacing="2" style="width:100%; margin-top:20px;">
		<thead>
			<tr>
				<th class="header">Payroll Head</th>
				<th class="header">Type</th>
				<th class="header" id="percentage_container">Percentage</th>
				<th class="header">Base Head</th>
				<th class="header">Amount</th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<td colspan="3" style="padding-top:10px;"><input type="button" id="Add" class="formbutton" value="Add" onclick="add_salary_breakdown()" /></td>
				<td align="center" style="padding-top:10px;"><input type="button" id="calculate" class="formbutton" value="Calculate" onclick="calculate_all()" /></td>
				<td align="justify" style="padding-top:10px;">Net Amount: <span id="total_salary" style="float:right;">0.00</span></td>
			</tr>
			<tr>
				<td colspan="5" align="center" style="padding-top:10px;">
                	<input type="hidden" name="hidden_head_count" value="" />&nbsp;&nbsp;
					<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
					<input type="reset" name="cancel" id="cancel" class="formbutton" value="Cancel" onclick="reset_salary_form()" />
				</td>
			</tr>
		</tfoot>
	</table>
</form>
<div id="formula_editor" title="Formula Editor">
	<table width="100%">
		<tr>
			<td><input type="text" name="formula" value="" style="padding:5px; width:308px; margin-bottom:4px;" readonly /></td>
		</tr>
		<tr>
			<td>
				<input type="button" name="brace_start" value="(" fval="(" class="operator" onclick="write_formula( 'brace_start' );" />
				<input type="button" name="brace_end" value=")" fval=")" class="operator" onclick="write_formula( 'brace_end' );" />
				<input type="button" name="plus" value="+" fval="+" class="operator" onclick="write_formula( 'plus' );" />
				<input type="button" name="minus" value="-" fval="-" class="operator" onclick="write_formula( 'minus' );" />
				<input type="button" name="multiply" value="*" fval="*" class="operator" onclick="write_formula( 'multiply' );" />
				<input type="button" name="divide" value="/" fval="/" class="operator" onclick="write_formula( 'divide' );" />
				<?php for( $i = 0; $i <= 9; $i++ ) { ?>
				<input type="button" name="number_<?php echo $i; ?>" value="<?php echo $i; ?>" fval="<?php echo $i; ?>" class="operator" onclick="write_formula( 'number_<?php echo $i; ?>' );" />
				<?php } ?>
				<input type="button" name="point" value="." fval="." class="operator" onclick="write_formula( 'point' );" />
				<input type="button" name="backspace" value="&larr;" class="operator" onclick="write_formula( 'backspace' );" />
			</td>
		</tr>
		<tr>
			<td>
				<?php foreach( $payroll_heads AS $payroll_id => $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
				<input type="button" name="<?php echo $payroll['custom_head']; ?>" value="<?php echo $payroll['custom_head'] . " (" . $payroll['abbreviation'] . ")"; ?>" fval="<?php echo $payroll['abbreviation']; ?>" style="width:158px; padding:2px 0;" onclick="write_formula( '<?php echo $payroll['custom_head']; ?>' );" />
				<?php } } ?>
			</td>
		</tr>
	</table>
</div>
<script>

show_emp_salary();
 
</script>	
