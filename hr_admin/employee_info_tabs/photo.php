<style>
	#upload {
		padding:5px;
		font-weight:bold; font-size:1.3em;
		font-family:Arial, Helvetica, sans-serif;
		text-align:center;
		background:#999999;
		color:#ffffff;
		border:1px solid #999999;
		width:100px;
		cursor:pointer !important;
		-moz-border-radius, -webkit-border-radius, -border-radius:5px;
	}
	.darkbg { background:#ddd !important; }
	#status { font-family:Arial; padding:5px; }
	#files{ list-style:none; }
	#files li{ margin-top:5px; }
	.success{ border:1px solid #CCCCCC; color:#660000; float:left; }
	#files_btm{ list-style:none; }
	.error{ background:#FFFFFF; border:1px solid #CCCCCC; }
</style>
<script type="text/javascript">
	var del_file;
	$(function() {
		$('#upload').click(function() {
			upload_image();
			show_emp_photo();
		});
	});
	
	function upload_image() {
		
		var module = "HRM";
		var category = 0; 
		var stretched_width = 200; 
		var thumbnail_width = 120;
		
		//if( $('#upload').val==""){
		
		new AjaxUpload( $('#upload'), {
			action: '../includes/upload_resource.php?resource_type=photo&module=' + module + '&category=' + category
					+ '&stretched_width=' + stretched_width + '&thumbnail_width=' + thumbnail_width,
			name: 'uploadfile',
			onSubmit: function( file, ext ) {
				if( !( ext && /^(jpg|png|jpeg|bmp)$/.test( ext ) ) ) {
					// extension is not allowed 
					$('#status').text('Only JPG, PNG or bmp files are allowed');
					return false;
				}
				$('#status').html('<img src="../images/loading.gif" />');
				

			},
			onComplete: function( file, response ) {
				//On completion clear the status
				$('#status').text('');				
				//Add uploaded file to list
				$('<li></li>').appendTo('#files').html( '<img src="../' + response + '" height="150px" width="140px" alt="" />' ).addClass('success');
				$('#upload').unbind('click');
			}
		});
	
	//}
}
	
</script>
<table width="100%" height="100%">
	<tr>
       <input type="hidden" name="save_up_photo" id="save_up_photo" value="" />
		<td title="Click on any Image to Enlarge" align="center" width="200" ><? echo $tab_permission_data[1]['visibility_prv']; ?>Uploaded Images</td>
        <td title="Click on any Image to Enlarge" align="center" >Uploaded Images</td>
	</tr>
	<tr>
		<td title="Click to Enlarge" align="center" height="300" width="200" valign="middle">
			<div id="files" title="Click to Enlarge" style="width:100%; border:1px solid; height:100%; background-color:" align="center">
				<div id="files" style="float:left" align="center"></div>
			</div>
		</td>
        <td title="Click to Enlarge" align="left" height="300" width="720" valign="top">
        <div style="float:left; width:100%; height:100%" align="center">
			<div id="webcam_img" style="width:350px; float:left; border:1px solid; height:100%; background-color:" align="center"></div>
			
            <div id="upload_results" style="width:350px; float:left; border:1px solid; height:100%; background-color:" align="center"></div>
            </div>
		</td>
	</tr>
	<tr>
		<td align="center"><div id="status" align="center"></div>
			<div style="padding-top:5px">
				<div id="upload" emp_code="">Select Image</div>
			</div>
			<div style="width:100px; padding-top:5px" align="center"></div>
		</td>
        <td align="center"><div id="status" align="center"></div>
			<div style="padding-top:5px">
          
		<input type=button value="Start Web Cam" onClick="start_cam()">
          &nbsp;&nbsp;
				<!--  <input type=button value="Configure..." onClick="webcam.configure()">  -->
		&nbsp;&nbsp;
		<input type=button value="Capture" onClick="webcam.freeze()">
		&nbsp;&nbsp;
		<input type=button value="Upload" onClick="do_upload()">
		&nbsp;&nbsp;
		<input type=button value="Reset" onClick="webcam.reset()">
        
        
			</div>
			<div style="width:100px; padding-top:5px" align="center"></div>
		</td>
	</tr>
</table>
<script language="JavaScript">
		webcam.set_hook( 'onComplete', 'my_completion_handler' );
		
		function do_upload() {
			// upload to server
			document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
			webcam.upload();
		}
		
		function my_completion_handler(msg) {
			// extract URL out of PHP output
			if (msg.match(/(http\:\/\/\S+)/)) {
				var image_url = RegExp.$1;
				// show JPEG image in page
				document.getElementById('upload_results').innerHTML = 
					 '<img src="' + image_url + '">';
				//style="width:380px; float:left; border:1px solid; height:100%; background-color:"
				// reset camera for another shot
				webcam.reset();
			}
			else alert("PHP Error: " + msg);
		}
		function start_cam()
		{
			document.getElementById('webcam_img').innerHTML= webcam.get_html(350, 290);	
		}
		
		
if(user_priv_tab[2]['insert_prv']==2)
	$('#upload').hide(500);
else
	$('#upload').show(500);

if(user_priv_tab[2]['edit_prv']==2)
	$('#upload').hide(500);
else
	$('#upload').show(500);
	
	
	</script>
<script>show_emp_photo();</script>	
