<script type="text/javascript">
	var family_counter = 0; emp_relation= new Array(), relation_options = '';
	
	$(document).ready(function() {
		show_emp_family();
	});
	
	function get_datepicker() {
		$('.datepicker').datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	}
	<?php 
	include("../../includes/array_function.php");
	foreach( $emp_relation AS $key => $value ) {
		echo "relation_options += '<option value=\"$key\">$value</option>';\n";
	}
	?>
	function add_family( family_id ) {
		if( family_id == family_counter ) {
			family_counter++;			
			$('#tbl_family tbody').append(
				'<tr>'
					+ '<td><input type="text" name="fname[]" id="fname_' + family_counter + '" value="" class="text_boxes" onfocus="add_family( ' + family_counter + ' );" /></td>'
					<!--+ '<td><input type="text" name="frelation[]" id="frelation_' + family_counter + '" value="" class="text_boxes" /></td>'-->
					+ '<td>'
					+ '<select name="frelation[]" id="frelation_' + family_counter + '" class="combo_boxes">'
						+ relation_options
					+ '</select>'
				+ '</td>'
					+ '<td><input type="text" name="fdob[]" id="fdob_' + family_counter + '" value="" class="datepicker" onclick="get_datepicker();" /></td>'
					+ '<td><input type="text" name="fage[]" id="fage_' + family_counter + '" value="" class="text_boxes" readonly="readonly" onfocus="calculate_age('+family_counter+');" /></td>'
					+ '<td><input type="text" name="foccupation[]" id="foccupation_' + family_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><input type="text" name="fcontact_no[]" id="fcontact_no_' + family_counter + '" value="" class="text_boxes" onkeypress="return numbersonly(this,event)" /></td>'
				+ '</tr>'
			);
		}
	}
	
	function reset_family_form() {
		$('#tbl_family tbody').html( '' );
		family_counter = 0;
		add_family( 0 );
	}
</script>
<style type="text/css">
	#tbl_family tbody input[type="text"] { width:159px; }
</style>
<form id="emp_family_form" action="javascript:fnc_emp_family(save_perm,edit_perm,delete_perm,approve_perm)" method="POST" >
	<table id="tbl_family" style="width:100%;" border="0" cellpadding="0" cellspacing="3">
		<thead>
			<tr>
            	 <input type="hidden" name="save_up_familyinfo" id="save_up_familyinfo" value="" />
				<th class="header">Name</th>
				<th class="header">Relation</th>
				<th class="header">Date of Birth</th>
				<th class="header">Age</th>
				<th class="header">Occupation</th>
				<th class="header">Contact No</th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<td colspan="6" align="center" style="padding-top:10px;">
					<input type="submit" id="save_addres_family" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
					<input type="button" name="cancel" class="formbutton" value="Refresh" onclick="reset_family_form()" />
				</td>
			</tr>
		</tfoot>
	</table>
</form>
<script>
show_emp_family();


if(user_priv_tab[9]['insert_prv']==2)
	$('#save_addres_family').hide(500);
else
	$('#save_addres_family').show(500);

if(user_priv_tab[9]['edit_prv']==2)
	$('#save_addres_family').hide(500);
else
	$('#save_addres_family').show(500);
</script>	
