<?php

include('../../includes/common.php');
	
//overtime_policy
$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$overtime_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$overtime_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	
}

//leave_policy
$sql = "SELECT * FROM lib_policy_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$leave_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$leave_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $row[id] AND is_deleted = 0 ORDER BY policy_id, id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$leave_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$leave_policy[$row['id']]['definition'][$row2['leave_type']] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$leave_policy[$row['id']]['definition'][$row2['leave_type']][$key2] = mysql_real_escape_string( $value2 );
		}
	}
}

//maternity_policy
$sql = "SELECT * FROM lib_policy_maternity_leave WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$maternity_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$maternity_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$maternity_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	
}

//duty roster policy
$sql = "SELECT distinct(roster_id) as id,roster_rule_name FROM lib_duty_roster_policy WHERE is_deleted = 0 and status_active=1 ORDER BY roster_rule_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$roster_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$roster_policy[$row['id']] = mysql_real_escape_string( $row['roster_rule_name'] );
}

//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//Holiday incentive policy
$sql = "SELECT * FROM lib_policy_holiday_incentive WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$holiday_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$holiday_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$holiday_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//Attendance bonus policy 
$sql = "SELECT * FROM lib_policy_attendance_bonus WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$attendance_bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$attendance_bonus_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$attendance_bonus_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//early deduction policy 
$sql = "SELECT * FROM lib_policy_early_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$early_out_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$early_out_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$early_out_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//late deduction policy 
$sql = "SELECT * FROM lib_policy_late_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$late_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$late_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$late_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$salary_breakdown_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$i = 0;
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][$i] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$salary_breakdown_policy[$row['id']]['definition'][$i][$key2] = mysql_real_escape_string( $value2 );
		}
		$i++;
	}
}


//Bonus policy 
$sql = "SELECT * FROM lib_bonus_policy WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$bonus_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$bonus_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//Tiffin policy 
$sql = "SELECT * FROM lib_policy_tiffin_bill_mst WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$tiffin_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$tiffin_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$tiffin_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

// Allowance policy 
$sql = "SELECT * FROM  lib_policy_allowance WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$allowance_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$allowance_policy[$row['policy_id']] = array();
	foreach( $row AS $key => $value ) {
		$allowance_policy[$row['policy_id']][$key] = mysql_real_escape_string( $value );
	}
}


//absent deduction policy 
$sql = "SELECT * FROM lib_policy_absent_deduction WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$absent_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$absent_deduction_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$absent_deduction_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}





?>

	<script type="text/javascript" charset="utf-8">	
	
	
$('.datepicker').datepicker({
dateFormat: 'dd-mm-yy',
changeMonth: true,
changeYear: true
});
	
		function reset_form() {
			$('#tbl_policy_selection tbody input[name="policy[]"]:checked').each(function() {
				$(this).attr( 'checked', false );
			});
			$('#policy_tagging input[type="checkbox"]:checked').each(function() {
				$(this).attr( 'checked', false );
				$('select[name="' + $(this).val() + '_policy"]').val( 0 );
			});
			
			$('#overtime_policy').val(0);
			$('#holiday_incentive_policy').val(0);
			$('#duty_roster_policy').val(0);
			$('#leave_policy').val(0);
			$('#maternity_leave_policy').val(0);
			$('#attendance_bonus_policy').val(0);
			$('#absent_deduction_policy').val(0);
			$('#late_deduction_policy').val(0);
			$('#bonus_policy').val(0);
			$('#tax_policy').val(0);
			$('#shift_policy').val(0);
			$('#tiffin_policy').val(0);
		}
		
		//alert(user_priv_tab[10]['edit_prv']);	

	</script>
	<style>
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
		.formbutton { width:100px; }
		#tbl_policy_selection select { width:250px; }
	.formbutton1 {width:100px; }
    </style>

<form id="policy_tagging_form" action="javascript:fnc_policy_tag_save(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
	<fieldset>
			<legend>Policy Selection</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2" id="tbl_policy_selection">
				<thead>
					<tr>
						<th class="header">&nbsp;</th>
						<th class="header">Policy Name</th>
						<th class="header">Rule Name</th>
						<th class="header">Rule Description</th>
					</tr>
				</thead>
				<tbody>
					<tr>
                    	<input type="hidden" name="save_up_policytaging" id="save_up_policytaging" value="" />
						<td><input type="checkbox" name="policy[]" id="overtime" value="overtime" /></td>
						<td>1.Overtime Policy</td>
						<td align="center">
							<select name="overtime_policy" id="overtime_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $overtime_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>
							</select>                            
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="holiday_incentive" value="holiday_incentive" /></td>
						<td>2.Holiday Incentive</td>
						<td align="center">
							<select name="holiday_incentive_policy" id="holiday_incentive_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $holiday_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="duty_roster" value="duty_roster" /></td>
						<td>3.Duty Roster Policy</td>
						<td align="center">
							<select name="duty_roster_policy" id="duty_roster_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $roster_policy AS $key=>$value ) {  ?>
								<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
								<?php } ?>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="leave" value="leave" /></td>
						<td>4.Leave Policy</td>
						<td align="center">
							<select name="leave_policy" id="leave_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
								<?php foreach( $leave_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="maternity_leave" value="maternity_leave"/></td>
						<td>5.Maternity Leave Policy</td>
						<td align="center">
							<select name="maternity_leave_policy" id="maternity_leave_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $maternity_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>                                
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="attendance_bonus" value="attendance_bonus" /></td>
						<td>6.Attendance Bonus Policy</td>
						<td align="center">
							<select name="attendance_bonus_policy"  id="attendance_bonus_policy"  class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $attendance_bonus_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?>  
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="absent_deduction" value="absent_deduction" /></td>
						<td>7.Absent Deduction Policy</td>
						<td align="center">
							<select name="absent_deduction_policy" id="absent_deduction_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $absent_deduction_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?> 
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
                    <tr>
						<td><input type="checkbox" name="policy[]" id="late_deduction"  value="late_deduction" /></td>
						<td>8.Late Deduction Policy</td>
						<td align="center">
							<select name="late_deduction_policy" id="late_deduction_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $late_deduction_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?> 
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="bonus"  value="bonus" /></td>
						<td>9.Bonus Policy</td>
						<td align="center">
							<select name="bonus_policy" id="bonus_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
                                <?php foreach( $bonus_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } } ?> 
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="tax" value="tax" /></td>
						<td>10.Tax Policy</td>
						<td align="center">
							<select name="tax_policy" id="tax_policy" class="combo_boxes">
								<option value="0">-- Select --</option>
							</select>
						</td>
						<td align="center">&nbsp;</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="policy[]" id="shift"  value="shift" /></td>
						<td>11.Shift Policy</td>
						<td align="center">
							<select name="shift_policy" id="shift_policy" class="combo_boxes" onchange="openmypage_fixed_roster(this.value,'fixed_roster');" > 
								<option value="0">-- Select --</option>
								<?php foreach( $shift_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['shift_name']; ?></option>
								<?php } } ?>
							</select>
                            <input type="text" id="txt_date" style="display:none" class="datepicker" name="txt_date" placeholder="Enter Date" size="8" >
						</td>
						<td align="center">&nbsp; </td>
					</tr>
                    <tr>
                    <td><input type="checkbox" name="policy[]" id="tiffin"  value="tiffin" /></td>
                    <td>12.Tiffin Bill Policy</td>
                    <td align="center">
                        <select name="tiffin_policy" id="tiffin_policy" class="combo_boxes" > 
                            <option value="0">-- Select --</option>
                            <?php foreach( $tiffin_policy AS $policy ){ if( $policy['status_active'] == 1 ) { ?>
                            <option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
                            <?php } }?>
                        </select>
                    </td>
                    <td align="center">&nbsp; </td>
                    </tr>
                    <tr>
                    <td><input type="checkbox" name="policy[]" id="allowance"  value="allowance" /></td>
                    <td>13.Allowance Policy</td>
                    <td align="center">
                        <select name="allowance_policy" id="allowance_policy" class="combo_boxes" > 
                            <option value="0">-- Select --</option>
                            <?php foreach( $allowance_policy AS $policy ){ if( $policy['status_active'] == 1 ) { ?>
                            <option value="<?php echo $policy['policy_id']; ?>"><?php echo $policy['policy_name']; ?></option>
                            <?php } }?>
                        </select>
                    </td>
                    <td align="center">&nbsp; </td>
                    </tr>
                    <tr>
                    <td><input type="checkbox" name="policy[]" id="early_deduction"  value="early_deduction" /></td>
                    <td>14.Early Out Deduction Policy</td>
                    <td align="center">
                        <select name="early_deduction_policy" id="early_deduction_policy" class="combo_boxes" > 
                            <option value="0">-- Select --</option>
                            <?php foreach( $early_out_deduction_policy AS $policy ){ if( $policy['status_active'] == 1 ) { ?>
                            <option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
                            <?php } }?>
                        </select>
                    </td>
                    <td align="center">&nbsp; </td>
                    </tr>
				</tbody>
			</table>
		</fieldset>
	<div id="policy_tagging" align="center" class="demo_jui" style="margin:10px 0;"></div>
		<div align="center">
			<input type="hidden" name="employees" id="employees" value="" />
			<input type="submit" name="save" id="save_addres_policy" value="Save" class="formbutton" />&nbsp;&nbsp;
			<input type="button" name="reset" id="reset" value="Refresh" class="formbutton" onClick="reset_form()" />
		</div>
</form>

<script>
show_policy_taging();

if(user_priv_tab[10]['insert_prv']==2)
	$('#save_addres_policy').hide(500);
else
	$('#save_addres_policy').show(500);

if(user_priv_tab[10]['edit_prv']==2)
	$('#save_addres_policy').hide(500);
else
	$('#save_addres_policy').show(500);
	
</script>	
 
