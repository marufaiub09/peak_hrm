<?php
/*######################################

	Completed By
	Name : Ekram
	Date : 15/06/2013
		
######################################*/
include('../../includes/common.php');
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);



$sql = "SELECT * FROM lib_hrm_documents   ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$document_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$document_chart[$row['id']] = mysql_real_escape_string( $row['document_name'] );		
	}


//--------------------------------------------------------------------------------------------------------------------

?>
<script type="text/javascript">
 //Numeric Value allow field script

	var designation_id=document.getElementById('designation_id').value;
	var emp_code=document.getElementById('emp_code').value;
		

function numbersonly(myfield, e, dec)
{
	
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
 
 
$('.datepicker').datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
		
$(document).ready(function() {
showResult_multi(designation_id,emp_code,'list_of_essential_docs','docs_list');       
});		
		

//-----------------------------------update document hrm------------------------------//
function get_Data_update_docs(id,type)   
	{ 
	 	//alert(id);
		ajax.requestFile = 'includes/get_data_update.php?getClientId='+id+'&type='+type;	// Specifying which file to get
		ajax.onCompletion = showreporttemplate;	// Specify function that will be executed after file has been found
		ajax.runAJAX();	
	}
	
	function showreporttemplate()
	{
		var formObj = document.forms['document_submit_form'];
		//alert(ajax.response);
		eval(ajax.response);
		//showResult(document.getElementById('txt_mst_id').value,'1','unit_list_view');
	}
	
	$( '#document_submit_form' ).submit( function( e ){
		//alert("su..re"); return; 
		e.preventDefault();
		
		var emp_code = $('#emp_code').val();
		var document_name = $('#document_name').val();
		var submitted_copy = $('#submitted_copy').val();
		var Sub_date = $('#Sub_date').val();
		var designation_id = $('#designation_id').val();
		var save_up =$('#save_up').val();
		var save_up_document =$('#save_up_document_submit').val();
		var file_location =$('#hdn_file_location').val();
		
		if( $('#document_name').val()== 0 )
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#document_name').focus();
				$(this).html('Select document name.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			return;
		
		}
		else if( $('#submitted_copy').val()=='' )
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#submitted_copy').focus();
				$(this).html('Submitted copy is blank.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			return;
		}
		else if( $('#Sub_date').val()=='' )
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#Sub_date').focus();
				$(this).html('Submitted date is blank.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			return;
		}
		else 
		{
			$.ajax({
				url: 'includes/save_update_hr_admin.php?action=action_doc_submit'+'&isupdate='+save_up +'&emp_code='+emp_code +'&document_name='+document_name +'&submitted_copy='+submitted_copy +'&Sub_date='+Sub_date +'&designation_id='+designation_id+'&file_location='+file_location,
				type: 'POST',
				data: new FormData( this ),
				processData: false,
				contentType: false,
				success: function(data) 
				{
					//alert("su..re");
					var response = data.split('_');	
					//alert(http.responseText);
					if( response[0] == 3 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
							showResult_multi(designation_id,emp_code,'list_of_essential_docs','docs_list');
							$(this).fadeOut(5000);
							reset_form('document_submit_form');
						});
					}		
					else if( response[0] == 4 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
							showResult_multi(designation_id,emp_code,'list_of_essential_docs','docs_list');
							document.getElementById('save_up').value="";
							$(this).fadeOut(5000);
							document.getElementById('document_submit_form').reset();
						});
					}
				}
			});
		}
	  });
	  
//--------------------------end---------------------------//	
</script>
     <div class="form_caption" align="center">Employee Submitted Document's</div>
	<form name="document_submit_form"  id="document_submit_form"  autocomplete="off" method="POST">
		<fieldset style="width:1000px;">
			<legend>Document's</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
                	<input type="hidden" name="save_up_document_submit" id="save_up_document_submit" value="" />
					<td>Document Name</td>
					<td id="td_id">
					</td>
                    <td>Submitted Copy</td>
                    <td>
					<input type="text" name="submitted_copy" id="submitted_copy" class="text_boxes" value="" style="width:140px;"  onKeyPress=" return numbersonly(this,event)"/>
					</td>
                    <td>Submitted Date</td>
					<td><input type="text" name="Sub_date" id="Sub_date" class="datepicker" value=""  /></td>
                    <td>File Upload</td>
                    <td>
                    	<input type="file" name="file_upload" id="file_upload" />
                        <input type="hidden" name="hdn_file_location" id="hdn_file_location" class="text_boxes" value=""  />
                    </td>
			 </tr>
             </table>
             <div style="padding-top:10px;" align="center"> 
                <!--<input type="button" id="save_addres_doc" name="save" class="formbutton" value="Save" onclick="javascript:save_update_submitted_docs(save_perm,edit_perm,delete_perm,approve_perm);" />-->
                <input type="submit" id="save_addres_doc" name="save" class="formbutton" value="Save"/>
                <input type="reset" name="refresh" class="formbutton" value="Refresh"  />
                <input type="hidden" name="save_up" id="save_up" value="" />
             </div>
       </fieldset>     
	</form>
    <div id="docs_list" align="center" style="width:900px;"></div>

<script>
	show_submitted_docs();
	if(user_priv_tab[12]['insert_prv']==2) $('#save_addres_doc').hide(500);
	else $('#save_addres_doc').show(500);
	if(user_priv_tab[12]['edit_prv']==2) $('#save_addres_doc').hide(500);
	else $('#save_addres_doc').show(500);
</script>	
