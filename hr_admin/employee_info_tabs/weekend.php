<?
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 05-04-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

//--------------------------------------------------------------------------------------------------------------------

?>

	
	<script type="text/javascript" charset="utf-8">
				
	function weekend_multiselect( page_link, title ) {
			emailwindow = dhtmlmodal.open( 'EmailBox', 'iframe', page_link, title, 'width=560px,height=400px,center=1,resize=0,scrolling=0', '../../' );
			
			emailwindow.onclose = function() {
				document.getElementById( 'weekend' ).value = this.contentDoc.getElementById("txt_selected").value;
				document.getElementById( 'weekend_id' ).value = this.contentDoc.getElementById("txt_selected_id").value;
			}
		}
				
	</script>
	<style type="text/css">
		.formbutton { width:100px; }
		#select_header select { width:100%; }		
	</style>

     
	<form name="weekend_form"  id="weekend_form"  autocomplete="off" method="POST">
		<fieldset style="width:100%">
			<legend>Weekend Entry</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
                	<input type="hidden" name="save_up_weekend" id="save_up_weekend" value="" />
					<td>Weekend:</td>
					<td>
						<input type="text" name="weekend" id="weekend" class="text_boxes" value="" style="width:400px;" placeholder="Double click for weekends" ondblclick="weekend_multiselect( 'employee_info_tabs/weekend_multiselect.php', 'Select Weekend/(s)' ); return false;" readonly="readonly" />
						<input type="hidden" name="weekend_id" id="weekend_id" value="" />
					</td>
				</tr>
			</table>
            <br />       	
			<input type="button" id="save_addres_week" name="save" class="formbutton" value="Save" onclick="javascript:fnc_weekend(save_perm,edit_perm,delete_perm,approve_perm);" />
       </fieldset>     
	</form>
<script>
show_weekend();

if(user_priv_tab[11]['insert_prv']==2)
	$('#save_addres_week').hide(500);
else
	$('#save_addres_week').show(500);

if(user_priv_tab[11]['edit_prv']==2)
	$('#save_addres_week').hide(500);
else
	$('#save_addres_week').show(500);
</script>	
