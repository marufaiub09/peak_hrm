<script type="text/javascript">
	var experience_counter = 0;	
	
	$(document).ready(function() {
		show_emp_experience();
	});
	
	function get_datepicker() {
		$('.datepicker').datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	}
	
	/*$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});*/
	
	//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

			
	function add_experience( experience_id ) {
		if( experience_id == experience_counter ) {
			experience_counter++;
			
			$('#tbl_experience tbody').append(
				'<tr>'
					+ '<td><input type="text" name="organization_name[]" id="organization_name_' + experience_counter + '" value="" class="text_boxes" onfocus="add_experience( ' + experience_counter + ' );" /></td>'
					+ '<td><input type="text" name="designation[]" id="designation_' + experience_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><input type="text" name="joining_date[]" id="joining_date_' + experience_counter + '" value="" class="datepicker" onfocus="get_datepicker()" readonly="readonly" /></td>'
					+ '<td><input type="text" name="resigning_date[]" id="resigning_date_' + experience_counter + '" value="" class="datepicker" onfocus="get_datepicker();" onchange="service_length(resigning_date_' + experience_counter+',joining_date_' + experience_counter+','+experience_counter+');" readonly="readonly" /></td>'
					+ '<td><input type="text" name="service_length[]" id="service_length_' + experience_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><input type="text" name="gross_salary[]" id="gross_salary_' + experience_counter + '" value="" class="text_boxes numbers" onkeypress="return numbersonly(this,event)"/></td>'
				+ '</tr>'
			);
		}
		get_datepicker();
	}
	
	function reset_experience_form() 
	{
		$('#tbl_experience tbody').html( '' );
		experience_counter = 0;
		add_experience( 0 );
	}
	

	function service_length(date1,date2,count){
			var date1=$('#resigning_date_'+count).val();
			var date2=$('#joining_date_'+count).val();

			var date1_split=date1.split('-');
			date1=date1_split[2]+'-'+date1_split[1]+'-'+date1_split[0];
			
			var date2_split=date2.split('-');
			date2=date2_split[2]+'-'+date2_split[1]+'-'+date2_split[0];
			
			date1= new Date(date1);	
			date2= new Date(date2);	
			
			/*
			var date1_ms = date1.getTime();
			var date2_ms = date2.getTime();
			var date_diff = Math.abs(date1.getTime() - date2.getTime());
			var num_years 	= parseInt(date_diff/31536000000);
			var num_months 	= parseInt((date_diff % 31536000000)/2628000000);
			var num_days 	= parseInt(((date_diff % 31536000000) % 2628000000)/86400000);
			*/
			var one_day 		= 1000 * 60 * 60 * 24			
			var difference_ms 	= new Date(date1 - date2)			
			var num_years 		= parseInt(difference_ms/31536000000);
			var num_months 		= parseInt((difference_ms % 31536000000)/2628000000);
			var num_days 		= parseInt(((difference_ms % 31536000000) % 2628000000)/86400000);
						
			$('#service_length_'+count).val( num_years+' Years '+num_months+' Months '+num_days+' Days');
			
				if ( date1 < date2 )
				{
					document.getElementById('service_length_'+count).style.backgroundColor="red";
				}
				else
				{
					document.getElementById('service_length_'+count).style.backgroundColor="";
				}
		}

		function parseDate(input) 
		{
			  var parts = input.match(/(\d+)/g);
			  // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
			  return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
		}

</script>
<style type="text/css">
	#tbl_experience tbody input[type="text"] { width:159px; }
</style>
<form id="emp_experience_form" action="javascript:fnc_emp_experience(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
	<table id="tbl_experience" style="width:100%;" border="0" cellpadding="0" cellspacing="3">
		<thead>
			<tr>
            	 <input type="hidden" name="save_up_experience" id="save_up_experience" value="" />
				<th class="header">Organization Name</th>
				<th class="header">Designation</th>
				<th class="header">Joining Date</th>
				<th class="header">Resigning Date</th>
				<th class="header">Service Length</th>
				<th class="header">Gross Salary</th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<td colspan="6" align="center" style="padding-top:10px;">
					<input type="submit" id="save_addres" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
					<input type="button" name="cancel" id="cancel" class="formbutton" value="Refresh" onclick="reset_experience_form()" />
				</td>
			</tr>
		</tfoot>
	</table>
</form>
<script>
show_emp_experience();

if(user_priv_tab[7]['insert_prv']==2)
	$('#save_addres').hide(500);
else
	$('#save_addres').show(500);

if(user_priv_tab[7]['edit_prv']==2)
	$('#save_addres').hide(500);
else
	$('#save_addres').show(500);
</script>	

