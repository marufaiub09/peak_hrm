<?php
/************************************
|
|	Completed By	: Md. Nuruzzaman
|	Date			: 25-10-2014
|		
*************************************/
	include('../../includes/common.php');
	session_start();
	
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	extract($_REQUEST);
//--------------------------------------------------------------------------------------------------------------------
?>
<script type="text/javascript">
	var company_id=document.getElementById('company_id').value;
	$(document).ready(function() {
		$('.datepicker').datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
		
		showResult_multi('',company_id,'list_view_official_docs','docs_lists');       
	});	
	
	// get_Data_update_office_docs
	function get_Data_update_office_docs(id,doc_id,date)   
	{ 
	 	//alert("su..re");
	 	$('#file_types').removeAttr('disabled','disabled');
		$('.image_uploader').removeAttr('disabled','disabled');
		
		$('#update_id').val(id);
		$('#document_id').val(doc_id);
		$('#date').val(date);
	}
	
	// save_update_submitted_docs_office
	function save_update_submitted_docs_office()
	{
		//alert("su..re");
		if( $('#document_id').val()==0 )
		{
			$("#messagebox").fadeTo( 200, 0.1, function(){
				$('#document_id').focus();
				$(this).html('Select document name.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			return;
		}
		else if( $('#date').val()=='' )
		{
			$("#messagebox").fadeTo( 200, 0.1, function(){
				$('#date').focus();
				$(this).html('Select date.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			return;
		}
		else 
		{
			http.open( 'GET', 'includes/save_update_hr_admin.php?action=save_office_documents'+'&company_id='+$('#company_id').val()+'&document_id='+$('#document_id').val()+'&date='+$('#date').val()+'&update_id='+$('#update_id').val());
			 http.onreadystatechange=response_save_office_documents;
			http.send(null);
		}
	}
	
	// response_save_office_documents
	function response_save_office_documents()
	{
		if(http.readyState == 4) 
		{
			var response = http.responseText.split('_');	
			//alert(http.responseText);
			if( response[0] == 3 ) 
			{
				$("#messagebox").fadeTo( 200, 0.1, function() {
					$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
				reset_form('document_submit_form_office');
				$('#file_types').attr('disabled','disabled');
				$('.image_uploader').attr('disabled','disabled');
				showResult_multi('',company_id,'list_view_official_docs','docs_lists'); 
			}
			if( response[0] == 4 ) 
			{
				$("#messagebox").fadeTo( 200, 0.1, function() {
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
				reset_form('document_submit_form_office');
				$('#file_types').attr('disabled','disabled');
				$('.image_uploader').attr('disabled','disabled');
				showResult_multi('',company_id,'list_view_official_docs','docs_lists'); 
			}
		}
	}
	
	// file_uploader	
	function file_uploader ( url, mst_id, det_id, form, file_type, is_multi, show_button )
	{
		//alert("su..re");
		// file type image
		if (file_type==1 )
		{
			$('#file_types').val(1);
		}
		
		// file type document
		if (file_type==2 )
		{
			$('#file_types').val(2);
		}
		
		if (file_type==0 )
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#file_type').focus();
				$(this).html('Select file type.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			}); 
			return;
		}
		if (!is_multi) var is_multi=0;
		if (!show_button) var show_button=1;
		
		if (mst_id=="" || mst_id==0 ) 
		{
			 $('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			 { 
				$(this).html('Please Select or Save any Information before File Upload.').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
	
			 });
			return false;
		}
		
		var im_url="";
		if (url.length==3)im_url=""; else if (url.length==6)im_url="../"; else if (url.length==9)im_url="../../"; else if (url.length==12)im_url="../../../"; 
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', url+'includes/common_functions_for_js.php?action=file_uploader&det_id='+det_id+'&form='+form+'&is_multi='+is_multi+'&mst_id='+mst_id+'&file_type='+file_type+'&show_button='+show_button, 'File Uploader', 'width=640px,height=330px,center=1,resize=0,scrolling=0', im_url )
		
		emailwindow.onclose=function()
		{
			//alert("su..re");
			//showResult_multi(designation_id,emp_code,'list_of_essential_docs','docs_list');
			/*
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var order_id=this.contentDoc.getElementById("txt_selected_id")
			var order_no=this.contentDoc.getElementById("txt_selected") //Access form field with id="emailfield"
			if (title=="Company Selection")
			{
				document.getElementById('cbo_unit_name_show').value=order_no.value;	
				document.getElementById('cbo_unit_name').value=order_id.value;	
			}
			else
			{
				document.getElementById('cbo_user_buyer_show').value=order_no.value;	
				document.getElementById('cbo_user_buyer').value=order_id.value;	
			}
			*/
		}
	}
</script>
     <div class="form_caption" align="center">Office Submitted Document's</div>
	<form name="document_submit_form_office"  id="document_submit_form_office"  autocomplete="off" method="POST">
		<fieldset style="width:850px;"><!--<legend>Document's</legend>-->
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>Name</td>
					<td id="td_ids"></td>
                    <td>Date</td>
					<td><input type="text" name="date" id="date" class="datepicker" value=""  /></td>
                    <td>File Type</td>
					<td>
                    	<select name="file_types" id="file_types" class="combo_boxes" style="width:100px;" disabled="disabled">
                        	<option value="0">-- select --</option>
                            <option value="1">Image</option>
                            <option value="2">Document</option>
                        </select>
                    </td>

                    <td align="right">File Upload</td>
                    <td>
                        <input type="button" class="image_uploader" style="width:172px" value="CLICK TO ADD/VIEW IMAGE" onClick="file_uploader ( '../', document.getElementById('update_id').value,'', document.getElementById('company_id').value, document.getElementById('file_types').value,1)" disabled="disabled">
                    </td>
			 </tr>
			</table>
             <div style="padding-top:10px;" align="center"> 
                <input type="button" id="save_office_doc" name="save" class="formbutton" value="Save" onclick="javascript:save_update_submitted_docs_office(save_perm,edit_perm,delete_perm,approve_perm);" />
                <input type="reset" name="refresh" class="formbutton" value="Refresh"  />
                <input type="hidden" name="update_id" id="update_id" value="" />
             </div>
       </fieldset>     
	</form>
<script>
	show_submitted_docs_office();
	if(user_priv_tab[12]['insert_prv']==2) $('#save_addres_doc').hide(500);
	else $('#save_addres_doc').show(500);
	if(user_priv_tab[12]['edit_prv']==2) $('#save_addres_doc').hide(500);
	else $('#save_addres_doc').show(500);
</script>
<div style="height:10px;"></div>	
<div id="docs_lists"  align="left" style="width:470px;"></div>