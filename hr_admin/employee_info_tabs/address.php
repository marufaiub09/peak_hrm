<?php
include('../../includes/common.php');

 
	$sql = "SELECT * FROM lib_list_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_list[$row['id']] = $row;
	}
 
 
	$sql = "SELECT * FROM lib_list_district WHERE  status_active=1 and is_deleted = 0 ORDER BY division_id, district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$district_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$district_list[$row['id']] = $row;
	}
 
 
	$sql = "SELECT * FROM lib_list_district WHERE  status_active=1 and is_deleted = 0 ORDER BY division_id, district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$district_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$district_list[$row['id']] = $row;
	}
	
	
 
?>
<script type="text/javascript">
	var division_list	= new Array();
	var district_list	= new Array();
	<?php
	foreach( $division_list AS $division_id => $division ) {
		echo "division_list[$division_id] = new Array();\n";
		foreach( $division AS $key => $value ) {
			if( $key == 'id' ) echo "division_list[$division_id]['$key'] = ".mysql_real_escape_string($value).";\n";
			else echo "division_list[$division_id]['$key'] = '" . htmlentities( $value, ENT_QUOTES ) . "';\n";
		}
	}
	foreach( $district_list AS $district_id => $district ) {
		echo "district_list[$district_id] = new Array();\n";
		foreach( $district AS $key => $value ) {
			if( $key == 'id' || $key == 'division_id' ) echo "district_list[$district_id]['$key'] = $value;\n";
			else echo "district_list[$district_id]['$key'] = '" . htmlentities( $value, ENT_QUOTES ) . "';\n";
		}
	}
	?>
	
	$(document).ready(function() {
		$('#same').click(function() {
			var fields = new Array( 'house_no','road_no','division_id','district_id','thana','village','post_code','phone_no','mobile_no','email' );
			
			if( $(this).is(':checked') ) {
				for( var i = 0; i < fields.length; i++ ) {
					$('#permanent_' + fields[i]).val( $('#present_' + fields[i]).val() );
					$('#permanent_' + fields[i]).attr('disabled', 'disabled');
				}
			}
			else {
				for( var i = 0; i < fields.length; i++ ) {
					$('#permanent_' + fields[i]).removeAttr('disabled');
				}
			}
		});
		
		$('#present_district_id').change(function() {
			var district = $(this).val();
			if( district == 0 ) $('#present_division_id').val( 0 );
			else $('#present_division_id').val( district_list[district]['division_id'] );
		});
	});
	
	function district_change( district_id, type ) {
		if( district_id == 0 ) $('#' + type + '_division_id').val( 0 );
		else $('#' + type + '_division_id').val( district_list[district_id]['division_id'] );
	}
	
	$(document).ready(function(){
                $(".bangla").bnKb({
                    'switchkey': {"webkit":"k","mozilla":"y","safari":"k","chrome":"k","msie":"y"},
                    'driver': phonetic
                });
            });
			
			//$(".bangla").css('background-color', '#E1F5A9');
			
							//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
//alert(user_priv_tab[3]['edit_prv']);
</script>
<style type='text/css'>
	#emp_address_form input[type="text"] { width:180px; }
	#emp_address_form select { width:191px; }
</style>
<form id="emp_address_form" action="javascript:fnc_emp_address(save_perm,edit_perm,delete_perm,approve_perm)" method="POST"  >
	<table style="width:100%;" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr><th colspan="2" class="header">Present Address</th></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td width="35%">Village/Area</td>
						<td>
                        <input type="hidden" name="save_up_address" id="save_up_address" value="" />
                        <input type="text" name="present_village" id="present_village" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Village/Area(Bangla)</td>
					  <td><input type="text" name="present_village_bangla" id="present_village_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>House No</td>
						<td><input type="text" name="present_house_no" id="present_house_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>House No(Bangla)</td>
					  <td><input type="text" name="present_house_no_bangla" id="present_house_no_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Road No</td>
						<td><input type="text" name="present_road_no" id="present_road_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Road No(Bangla)</td>
					  <td><input type="text" name="present_road_no_bangla" id="present_road_no_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Post Office / Code</td>
						<td><input type="text" name="present_post_code" id="present_post_code" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Post Office / Code(Bangla)</td>
					  <td><input type="text" name="present_post_code_bangla" id="present_post_code_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Thana</td>
						<td><input type="text" name="present_thana" id="present_thana" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Thana(Bangla)</td>
					  <td><input type="text" name="present_thana_bangla" id="present_thana_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>District</td>
						<td>
							<select id="present_district_id" name="present_district_id" class="combo_boxes" onchange="district_change( this.value, 'present' )">
								<option value="0">-- Select --</option>
								<?php
								$division = 0;
								foreach( $district_list AS $district ) {
									if( $district['division_id'] != $division ) {
										if( $division != 0 ) echo "</optgroup>";
										$division = $district['division_id'];
										echo "<optgroup label=\"" . $division_list[$district['division_id']]['division_name'] . "\">";
									}
								?>
								<option value="<?php echo $district['id']; ?>"><?php echo $district['district_name']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
					  <td>District(Bangla)</td>
					  <td>                      
					  	<input type="text" name="present_district_id_bangla" id="present_district_id_bangla" class="bangla" value="" style="background-color:#E1F5A9;" />  
				      </td>
				  </tr>
					<tr>
						<td>Division</td>
						<td>
							<select name="present_division_id" id="present_division_id" class="combo_boxes">
								<option value="0">-- Select --</option>
								<?php foreach( $division_list AS $division ) { if( $division['status_active'] == 1 ) { ?>
								<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
					</tr>
					<tr>
					  <td>Division(Bangla)</td>
					  <td>                      	
                        <input type="text" name="present_division_id_bangla" id="present_division_id_bangla" class="bangla" value="" style="background-color:#E1F5A9;" />
					  </td>
				  </tr>
					<tr>
						<td>Land Phone</td>
						<td><input type="text" name="present_phone_no" id="present_phone_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
						<td>Cell Phone</td>
						<td><input type="text" name="present_mobile_no" id="present_mobile_no" class="text_boxes" value="" onkeypress="return numbersonly(this,event)" /></td>
					</tr>
					<tr>
						<td>E-Mail</td>
						<td><input type="email" name="present_email" id="present_email" class="text_boxes" value="" /></td>
					</tr>
				</table>
</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr><th colspan="2" class="header">Permanent Address</th></tr>
					<tr>
						<td width="35%">&nbsp;</td>
						<td>
							<input type="checkbox" name="same" id="same" value="yes" style="float:left;" />
							<div style="float:left;">Same as present</div>
						</td>
					</tr>
					<tr>
						<td>Village/Area</td>
						<td><input type="text" name="permanent_village" id="permanent_village" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Village/Area(Bangla)</td>
					  <td><input type="text" name="permanent_village_bangla" id="permanent_village_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>House No</td>
						<td><input type="text" name="permanent_house_no" id="permanent_house_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>House No(Bangla)</td>
					  <td><input type="text" name="permanent_house_no_bangla" id="permanent_house_no_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Road No</td>
						<td><input type="text" name="permanent_road_no" id="permanent_road_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Road No(Bangla)</td>
					  <td><input type="text" name="permanent_road_no_bangla" id="permanent_road_no_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Post Office / Code</td>
						<td><input type="text" name="permanent_post_code" id="permanent_post_code" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Post Office / Code(Bangla)</td>
					  <td><input type="text" name="permanent_post_code_bangla" id="permanent_post_code_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Thana</td>
						<td><input type="text" name="permanent_thana" id="permanent_thana" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Thana(Bangla)</td>
					  <td><input type="text" name="permanent_thana_bangla" id="permanent_thana_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>District</td>
						<td>
							<select id="permanent_district_id" name="permanent_district_id" class="combo_boxes" onchange="district_change( this.value, 'permanent' )">
								<option value="0">-- Select --</option>
								<?php
								$division = 0;
								foreach( $district_list AS $district ) {
									if( $district['division_id'] != $division ) {
										if( $division != 0 ) echo "</optgroup>";
										$division = $district['division_id'];
										echo "<optgroup label=\"" . $division_list[$district['division_id']]['division_name'] . "\">";
									}
								?>
								<option value="<?php echo $district['id']; ?>"><?php echo $district['district_name']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
					  <td>District(Bangla)</td>
					  <td>                      	
					   	<input type="text" name="permanent_district_id_bangla" id="permanent_district_id_bangla" class="bangla" value="" style="background-color:#E1F5A9;" />
                       </td>
				  </tr>
					<tr>
						<td>Division</td>
						<td>
							<select name="permanent_division_id" id="permanent_division_id" class="combo_boxes">
								<option value="0">-- Select --</option>								
								<?php foreach( $division_list AS $division ) { if( $division['status_active'] == 1 ) { ?>
								<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
					</tr>
					<tr>
					  <td>Division(Bangla)</td>
					  <td>                      	
                        <input type="text" name="permanent_division_id_bangla" id="permanent_division_id_bangla" class="bangla" value="" style="background-color:#E1F5A9;" />
					   </td>
				  </tr>
					<tr>
						<td>Land Phone</td>
						<td><input type="text" name="permanent_phone_no" id="permanent_phone_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
						<td>Cell Phone</td>
						<td><input type="text" name="permanent_mobile_no" id="permanent_mobile_no" class="text_boxes" value="" onkeypress="return numbersonly(this,event)" /></td>
					</tr>
					<tr>
						<td>E-Mail</td>
						<td><input type="email" name="permanent_email" id="permanent_email" class="text_boxes" value="" /></td>
					</tr>
				</table>
</td>
			<td valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr><th colspan="2" class="header">Alternate Address</th></tr>
					<tr>
						<td width="35%">C/O</td>
						<td><input type="text" name="alternate_co" id="alternate_co" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>C/O(Bangla)</td>
					  <td><input type="text" name="alternate_co_bangla" id="alternate_co_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Village/Area</td>
						<td><input type="text" name="alternate_village" id="alternate_village" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Village/Area(Bangla)</td>
					  <td><input type="text" name="alternate_village_bangla" id="alternate_village_bangla" class="bangla" value="" style="background-color:#E1F5A9;"/></td>
				  </tr>
					<tr>
						<td>House No</td>
						<td><input type="text" name="alternate_house_no" id="alternate_house_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>House No(Bangla)</td>
					  <td><input type="text" name="alternate_house_no_bangla" id="alternate_house_no_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Road No</td>
						<td><input type="text" name="alternate_road_no" id="alternate_road_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Road No(Bangla)</td>
					  <td><input type="text" name="alternate_road_no_bangla" id="alternate_road_no_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Post Office / Code</td>
						<td><input type="text" name="alternate_post_code" id="alternate_post_code" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Post Office / Code(Bangla)</td>
					  <td><input type="text" name="alternate_post_code_bangla" id="alternate_post_code_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>Thana</td>
						<td><input type="text" name="alternate_thana" id="alternate_thana" class="text_boxes" value="" /></td>
					</tr>
					<tr>
					  <td>Thana(Bangla)</td>
					  <td><input type="text" name="alternate_thana_bangla" id="alternate_thana_bangla" class="bangla" value="" style="background-color:#E1F5A9;" /></td>
				  </tr>
					<tr>
						<td>District</td>
						<td>
							<select id="alternate_district_id" name="alternate_district_id" class="combo_boxes" onchange="district_change( this.value, 'alternate' )">
								<option value="0">-- Select --</option>
								<?php
								$division = 0;
								foreach( $district_list AS $district ) {
									if( $district['division_id'] != $division ) {
										if( $division != 0 ) echo "</optgroup>";
										$division = $district['division_id'];
										echo "<optgroup label=\"" . $division_list[$district['division_id']]['division_name'] . "\">";
									}
								?>
								<option value="<?php echo $district['id']; ?>"><?php echo $district['district_name']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
					  <td>District(Bangla)</td>
					  <td>                      	
					  	<input type="text" name="alternate_district_id_bangla" id="alternate_district_id_bangla" class="bangla" value="" style="background-color:#E1F5A9;" />
                      </td>
				  </tr>
					<tr>
						<td>Division</td>
						<td>
							<select name="alternate_division_id" id="alternate_division_id" class="combo_boxes">
								<option value="0">-- Select --</option>
								<?php foreach( $division_list AS $division ) { if( $division['status_active'] == 1 ) { ?>
								<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
								<?php } } ?>
							</select>
						</td>
					</tr>
					<tr>
					  <td>Division(Bangla)</td>
					  <td>
                      	<input type="text" name="alternate_division_id_bangla" id="alternate_division_id_bangla" class="bangla" value="" style="background-color:#E1F5A9;" />                     
					   </td>
				  </tr>
					<tr>
						<td>Land Phone</td>
						<td><input type="text" name="alternate_phone_no" id="alternate_phone_no" class="text_boxes" value="" /></td>
					</tr>
					<tr>
						<td>Cell Phone</td>
						<td><input type="text" name="alternate_mobile_no" id="alternate_mobile_no" class="text_boxes" value="" onkeypress="return numbersonly(this,event)" /></td>
					</tr>
					<tr>
						<td>E-Mail</td>
						<td><input type="email" name="alternate_email" id="alternate_email" class="text_boxes" value="" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center" style="padding-top:10px;">
				<input type="submit" id="save_addres_addr" name="save" class="formbutton" value="Save" />
				<input type="reset" name="cancel" id="cancel" class="formbutton" value="Refresh" style="margin-right:22px;" />
			</td>
		</tr>
	</table>
</form>
 
<script>
show_emp_address();

if(user_priv_tab[3]['insert_prv']==2)
	$('#save_addres_addr').hide(500);
else
	$('#save_addres_addr').show(500);

if(user_priv_tab[3]['edit_prv']==2)
	$('#save_addres_addr').hide(500);
else
	$('#save_addres_addr').show(500);
		
</script>	

