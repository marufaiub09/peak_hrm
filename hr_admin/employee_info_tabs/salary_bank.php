<?php
include('../../includes/common.php');

//bank_info
$sql = "SELECT * FROM lib_bank WHERE  status_active=1 and is_deleted = 0 ORDER BY bank_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$bank_info = array();
$bank_info_a = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$bank_info[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$bank_info[$row['id']][$key] = mysql_real_escape_string($value );
		$bank_info_a[$row['id']] =mysql_real_escape_string($row['bank_name']);
	}
}
	/*
	$emp_code=$_SESSION['emp_code'];
	echo $emp_code;
	$sql="SELECT gross_salary FROM hrm_employee where emp_code='$emp_code'";
	$result_gross_salary=mysql_query($sql) or die( $sql . "<br />" . mysql_error() );
	while($row=mysql_fetch_assoc($result_gross_salary))
	{
		$salary=$row['gross_salary'];
	}
	*/
?>
<script>
	//Numeric Value allow field script
	function numbersonly(myfield, e, dec)
	{
		var key;
		var keychar;
	
		if (window.event)
			key = window.event.keyCode;
		else if (e)
			key = e.which;
		else
			return true;
		keychar = String.fromCharCode(key);
	
		// control keys
		if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
		return true;
		
		// numbers
		else if ((("0123456789.,-").indexOf(keychar) > -1))
			return true;
		else
			return false;
	}
	function func_set_bank_salary(bank,branch,account,amount,tax,sequence,update_id,emp_code,g_salary,su)
	{
		//alert(bank+', '+branch+', '+account+', '+amount);
		// assigned value using JQuery
		$('#bank_id').val(bank);
		$('#branch_name').val(branch);
		$('#account_no').val(account);
		$('#salary_amount').val(amount);
		$('#show_tax').val(tax);
		$('#sequence').val(sequence);
		$('#update_id').val(update_id);
		$('#gross_sal').val(g_salary);
		$('#prev_tax_val').val(tax);
		$('#save_up_bank_sal').val(emp_code);
		$('#total_bank_salary').val( $('#update_total').val()-amount);
		
		/*
		// assigned value using Javascript
		document.getElementById('bank_id').value=bank;
		document.getElementById('branch_name').value=branch;
		document.getElementById('account_no').value=account;
		document.getElementById('salary_amount').value=amount;
		document.getElementById('show_tax').value=tax;
		document.getElementById('sequence').value=sequence;
		document.getElementById('update_id').value=update_id;
		*/
	}
	function check_tax_entry( tval )
	{
		if( tval==1 && $('#prev_tax_val').val()!=1)
		{
			var col=$('#salary_bank_list_view tr').length;// salary_bank_list_view
			for(var i=1; i<col; i++ )
			{
				if( $('#tax_info_'+i).html()=="Yes")
				{
					$('#show_tax').val(0);
					alert('Tax Already Showed in One Bank Salary');
					return;
				}
			}
		}
	}
	
	//alert(user_priv_tab[5]['edit_prv']);
	
	
function del_bank(save_perm,edit_perm,delete_perm,approve_perm)
{
	if( delete_perm==2)
	{		
		//error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Delete Permission.').addClass('messageboxerror').fadeTo(900,1);
		});	
		return;	
	}
	
	del_id=$('#update_id').val();
	
	if(del_id==0 || del_id==""){
		alert('Please Select a bank To Delete');
		return;
	}else{
		//alert("ekram");
		//return;
		$.ajax({
				type: "POST",
				url: "includes/get_data_update.php?&del_id="+del_id,
				data: 'link=del_bank',
				success: function(data) {				
				eval(data);
				$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Deleted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				//showResult_multi($('#emp_code').val(),'','update_list_view','tbl_list_view');
				show_emp_salary_bank(0);
				
			});
				
		}
	
	});	
	
	
  }
	
}
		
</script>
<style type='text/css'>
	#emp_salary_bank_form input[type="text"] { width:180px; }
	#emp_salary_bank_form select { width:191px; }
</style>
<form id="emp_salary_bank_form" action="javascript:fnc_emp_salary_bank(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
	<table align="center" style="width:30%;" border="0" cellpadding="0" cellspacing="2">
        <tbody>
            <tr>
                <td>Salary Bank</td>
                <td>:</td>
                <td>
                 <input type="hidden" name="save_up_bank_sal" id="save_up_bank_sal" value="" />
                    <select name="bank_id" id="bank_id" class="combo_boxes" onchange="fn_branch_name(this.value)" style="width:192px;">
                        <option value="0">-- Select --</option>
                        <?php foreach( $bank_info AS $bank ){ ?>
                        <option value="<?php echo $bank['id'];?>"><?php echo $bank['bank_name']; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Branch Name</td>
                <td>:</td>
                <td><input type="text" name="branch_name" id="branch_name" value="" readonly="readonly" class="text_boxes" /></td>
            </tr>
            <tr>
                <td>Account No</td>
                <td>:</td>
                <td><input type="text" name="account_no" id="account_no" value="" class="text_boxes" onkeypress="return numbersonly(this,event)" /></td>
            </tr>
            <tr>
                <td>Salary Amount</td>
                <td>:</td>
                <td><input type="text" name="salary_amount" id="salary_amount" value="" class="text_boxes" onkeypress="return numbersonly(this,event)" /></td>
            </tr>
            <tr>
                <td>Show Tax</td>
                <td>:</td>
                <td>
                    <select name="show_tax" id="show_tax" class="combo_boxes" onchange="check_tax_entry( this.value )" style="width:192px;">
                        <option value="0">-- select --</option>
                        <option value="1">Yes</option>
                        <option value="2">No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Sequence</td>
                <td>:</td>
                <td><input type="text" name="sequence" id="sequence" value="" class="text_boxes" onkeypress="return numbersonly(this,event)" /></td>
            </tr>
            <tr>
                <td colspan="3" align="left" style="padding-top:10px;">
                    <input type="submit" id="save_addres_bank" name="save" class="formbutton" value="Save" />
                    <input type="button" name="cancel" id="cancel" class="formbutton" value="Refresh" onclick="calculate_list_total( 1 )" />
                    <input type="button" name="delete" id="delete" class="formbutton" value="Delete" onclick="del_bank(save_perm,edit_perm,delete_perm,approve_perm)" />
                </td>
            </tr>
        </tbody>
        <tfoot>
            
            <tr>
            	<td colspan="3">
                <input type="hidden" name="update_id" id="update_id" class="text_boxes"/>
                <input type="hidden" name="gross_sal" id="gross_sal" value="" class="text_boxes" />
                <input type="hidden" name="prev_tax_val" id="prev_tax_val" value="" class="text_boxes" />
                <input type="hidden" name="update_total" id="update_total" value="" class="text_boxes" />
                <input type="hidden" name="total_bank_salary" id="total_bank_salary" value="" class="text_boxes" />
              
                 </td>
            </tr>
        </tfoot> 
	</table>
</form>
<script>
show_emp_salary_bank(0)

</script>
<div id="tbl_list_view"></div>
<script>eval( $('#Cancel').click());

if(user_priv_tab[5]['insert_prv']==2)
	$('#save_addres_bank').hide(500);
else
	$('#save_addres_bank').show(500);

if(user_priv_tab[5]['edit_prv']==2)
	$('#save_addres_bank').hide(500);
else
	$('#save_addres_bank').show(500);
	
</script>
 