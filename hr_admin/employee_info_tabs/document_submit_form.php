<?
/*######################################

	Completed By
	Name : Ekram
	Date : 15/06/2013
		
######################################*/
include('../../includes/common.php');
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);



$sql = "SELECT * FROM lib_hrm_documents   ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$document_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$document_chart[$row['id']] = mysql_real_escape_string( $row['document_name'] );		
	}


//--------------------------------------------------------------------------------------------------------------------

?>
<script type="text/javascript">
 //Numeric Value allow field script

	var designation_id=document.getElementById('designation_id').value;
	var emp_code=document.getElementById('emp_code').value;
		

function numbersonly(myfield, e, dec)
{
	
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
 
 
$('.datepicker').datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
		
$(document).ready(function() {
showResult_multi(designation_id,emp_code,'list_of_essential_docs','docs_list');       
});		
		
//-----------------------------------update document hrm------------------------------//
function get_Data_update_docs(id,type)   
	{ 
	 	$('#file_type').removeAttr('disabled','disabled');
		$('.image_uploader').removeAttr('disabled','disabled');
		
		//alert(id);
		ajax.requestFile = 'includes/get_data_update.php?getClientId='+id+'&type='+type;	// Specifying which file to get
		ajax.onCompletion = showreporttemplate;	// Specify function that will be executed after file has been found
		ajax.runAJAX();	
	}
	
	function showreporttemplate()
	{
		var formObj = document.forms['document_submit_form'];
		//alert(ajax.response);
		eval(ajax.response);
		//showResult(document.getElementById('txt_mst_id').value,'1','unit_list_view');
	}  


//--------------------------end---------------------------//
// file_uploader ( '../', document.getElementById('save_up').value,'', 'document_submit_form', 0 ,1)
function file_uploader ( url, mst_id, det_id, form, file_type, is_multi, show_button )
{
	//alert("su..re"); return;
	// file type image
	if (file_type==1 )
	{
		$('#file_type').val(1);
	}
	
	// file type document
	if (file_type==2 )
	{
		$('#file_type').val(2);
	}
	
	if (file_type==0 )
	{
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#file_type').focus();
			$(this).html('Select file type.').addClass('messagebox_ok').fadeTo(900,1);
			//save_activities_history('','hrm','employee information','save','../');
			$(this).fadeOut(5000);
		}); 
		return;
	}
	if (!is_multi) var is_multi=0;
	if (!show_button) var show_button=1;
	
	if (mst_id=="" || mst_id==0 ) 
	{
		 $('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
		 { 
			$(this).html('Please Select or Save any Information before File Upload.').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);

		 });
		return false;
	}
	
	var im_url="";
	if (url.length==3)im_url=""; else if (url.length==6)im_url="../"; else if (url.length==9)im_url="../../"; else if (url.length==12)im_url="../../../"; 
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', url+'includes/common_functions_for_js.php?action=file_uploader&det_id='+det_id+'&form='+form+'&is_multi='+is_multi+'&mst_id='+mst_id+'&file_type='+file_type+'&show_button='+show_button, 'File Uploader', 'width=640px,height=330px,center=1,resize=0,scrolling=0', im_url )
	
	emailwindow.onclose=function(){ }
}
	
</script>
     <div class="form_caption" align="center">Employee Submitted Document's</div>
	<form name="document_submit_form"  id="document_submit_form"  autocomplete="off" method="POST">
		<fieldset style="width:1050px;"><!--<legend>Document's</legend>-->
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
                	<input type="hidden" name="save_up_document_submit" id="save_up_document_submit" value="" />
					<td>Name</td>
					<td id="td_id"></td>
                    <td>No of Copy</td>
                    	<td>
					<input type="text" name="submitted_copy" id="submitted_copy" class="text_boxes" value="" style="width:140px;"  onKeyPress=" return numbersonly(this,event)"/>
					</td>
                    <td>Date</td>
					<td><input type="text" name="Sub_date" id="Sub_date" class="datepicker" value=""  /></td>
                    <td>File Type</td>
					<td>
                    	<select name="file_type" id="file_type" class="combo_boxes" style="width:100px;" disabled="disabled">
                        	<option value="0">-- select --</option>
                            <option value="1">Image</option>
                            <option value="2">Document</option>
                        </select>
                    </td>

                    <td align="right">File Upload</td>
                    <td>
                        <input type="button" class="image_uploader" style="width:172px" value="CLICK TO ADD/VIEW IMAGE" onClick="file_uploader ( '../', document.getElementById('save_up').value,'', document.getElementById('emp_code').value, document.getElementById('file_type').value ,1)" disabled="disabled">
                    </td>
			 </tr>
			</table>
             <div style="padding-top:10px;" align="center"> 
                <input type="button" id="save_addres_doc" name="save" class="formbutton" value="Save" onclick="javascript:save_update_submitted_docs(save_perm,edit_perm,delete_perm,approve_perm);" />
                <input type="reset" name="refresh" class="formbutton" value="Refresh"  />
                <input type="hidden" name="save_up" id="save_up" value="" />
             </div>
       </fieldset>     
	</form>
<script>
show_submitted_docs();

if(user_priv_tab[12]['insert_prv']==2)
	$('#save_addres_doc').hide(500);
else
	$('#save_addres_doc').show(500);

if(user_priv_tab[12]['edit_prv']==2)
	$('#save_addres_doc').hide(500);
else
	$('#save_addres_doc').show(500);
</script>
<div style="height:10px;"></div>	
<div id="docs_list"  align="left" style="width:710px;"></div>