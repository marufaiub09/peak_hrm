<?php

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE  status_active=1 and is_deleted = 0 ORDER BY level,trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//company_details
$sql = "SELECT * FROM lib_company WHERE  status_active=1 and is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE  status_active=1 and is_deleted = 0 $location_cond ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE  status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE  status_active=1 and is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE  status_active=1 and is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE  status_active=1 and is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql_idcard="select card_pattern,pattern_integret from variable_settings_hrm where status_active=1 and is_deleted=0";
	$result_idcard = mysql_query( $sql_idcard ) or die( $sql_idcard . "<br />" . mysql_error() );
	while($row_id=mysql_fetch_array($result_idcard))		
	{
		$card_pattern_val = $row_id['card_pattern'];
		$pattern_integret_val = $row_id['pattern_integret'];
	}
?>
<script type="text/javascript">
	var designation_chart = new Array();
	<?php
	foreach( $designation_chart AS $designation_id => $designation ) {
		echo "designation_chart[$designation_id] = new Array();\n";
		foreach( $designation AS $key => $value ) {
			if( $key == 'id' || $key == 'level' ) echo "designation_chart[$designation_id]['$key'] = $value;\n";
			else echo "designation_chart[$designation_id]['$key'] = '$value';\n";
		}
	}
	?>
	function fn_dob(){
		$('#dob').change(function() {
			var dob=$('#dob').val();
			var dob_split=dob.split('-');
			var dob_date=dob_split[2]+'-'+dob_split[1]+'-'+dob_split[0];			
			var pday = new Date();
			var bday = new Date( dob_date );
			var ydiff = pday.getFullYear() - bday.getFullYear();
			var mdiff = pday.getMonth() - bday.getMonth();
			if( mdiff < 0 ) {
				mdiff += 12;
				ydiff--;
			}			
			var get_age=$('#lblAge').html( 'About ' + ydiff + ' years ' + mdiff + ' months' );
			$('#hidden_age').val(ydiff);
			if(ydiff*1<18){
				document.getElementById('lblAge').style.backgroundColor="red";
			}else
				document.getElementById('lblAge').style.backgroundColor="";
		});
	}
	
		$.fn.sort_select_box = function() {
			// Get options from select box
			var my_options = $("#" + this.attr('id') + ' option');
			// sort alphabetically
			my_options.sort(function( a, b ) {
			if( a.text > b.text ) return 1;
				else if( a.text < b.text ) return -1;
				else return 0
			})
			//replace with sorted my_options;
			$(this).empty().append( my_options );
			
			// clearing any selections
			$("#" + this.attr('id') + " option").attr( 'selected', false );
		}
	

	function populate_designations( designation_level ) {
		var options = '<option value="0">-- Select --</option>';
		for( var i = 0; i < designation_chart.length; i++ ) {
				if( designation_chart[i] != undefined && designation_chart[i]['level'] == designation_level && designation_chart[i]['status_active'] == 1 ) {
						options += '<option value="' + designation_chart[i]['id'] + '">' + designation_chart[i]['custom_designation'] + '</option>';
				}
		}
		$('#designation_id').html( options );
		$('#designation_id').sort_select_box();
	}
	
	function fn_confirm_date()
	{
		var emp_cat = $('#category').val();	
		if($('#joining_date').val()=="")return;
		var joining_date = ($('#joining_date').val()).split('-'); 
		
		joining_date=joining_date[2]+"-"+joining_date[1]+"-"+joining_date[0];
		
		//alert(joining_date);
		if(emp_cat*1==0 || emp_cat*1==1 )
		{
			 con_date=add_days(joining_date,180);
			 con_date=con_date.split('-');
			 confirmation_automated=con_date[2]+"-"+con_date[1]+"-"+con_date[0];
			$('#confirmation_date').val(confirmation_automated);
		}
		if(emp_cat*1==4)
		{
			//$('#confirmation_date').val(add_days(joining_date,90));
			
			 con_date=add_days(joining_date,180);
			 con_date=con_date.split('-');
			 confirmation_automated=con_date[2]+"-"+con_date[1]+"-"+con_date[0];
			$('#confirmation_date').val(confirmation_automated);
		}		 
		if(emp_cat*1==2)
		{
			//$('#confirmation_date').val(add_days(joining_date,90));
			
			 con_date=add_days(joining_date,90);
			 con_date=con_date.split('-');
			 confirmation_automated=con_date[2]+"-"+con_date[1]+"-"+con_date[0];
			$('#confirmation_date').val(confirmation_automated);
		}
		
		//show_hide_trs(emp_cat);
		
	}
	

function  show_hide_trs(emp_cat){
		//alert(emp_cat)
		if(emp_cat==3){
			$('#old_remarks').hide();
			$('#new_remarks').show();
			
			
		}else{
		
			$('#old_remarks').show();
			$('#new_remarks').hide();
			
		}
//alert(emp_cat);
}

	//$(".bangla").css('background-color', '#E1F5A9');
		
</script>
<style>
	#basic_info input[type="text"] { width:153px; }
	#basic_info select { width:165px; }
	#cost_center select { width:169px; }
</style>
<form id="emp_basic_form" action="javascript:fnc_emp_basic(save_perm,edit_perm,delete_perm,approve_perm)" method="POST" >
	<table style="width:1100px;" border="0" cellpadding="0" cellspacing="2">
		<tr>
			<td>
				ID Card No:
				<!--<img align="right" id="new" src="../resources/images/new.jpg" title="New Employee" alt="New" style="margin-top:3px; cursor:pointer; visibility:hidden;" />-->
			</td>
			<td><?
                    if($card_pattern_val==1 || $card_pattern_val==2)
					{
						if($pattern_integret_val==1)
						{
							?>
							<input type="text" name="id_card_no" id="id_card_no" class="text_boxes" value="" onkeyup="on_key_down(1, event);" placeholder="Search"  ondblclick="openpage_search_idcard('search_id_card.php','Search Employee')"/>
							<?
						}
						else
						{
							?>
							<input type="text" name="id_card_no" id="id_card_no" class="text_boxes" onkeyup="on_key_down(1, event);" value="" placeholder="Search" readonly="readonly" ondblclick="openpage_search_idcard('search_id_card.php','Search Employee')"/>
							<?	
						}
					}
					else if($card_pattern_val==3)
					{
						// echo $pattern_integret_val;
						?>
                        <input type="text" name="id_card_no" id="id_card_no" class="text_boxes" onkeyup="on_key_down(1, event);" value="" placeholder="Search" readonly="readonly" ondblclick="openpage_search_idcard('search_id_card.php','Search Employee')"/>
						<?	
					}
					else if($card_pattern_val==4)
					{
						?>
                         <input type="text" name="id_card_no" id="id_card_no" class="text_boxes" onkeyup="on_key_down(1, event);" value="" placeholder="Search" readonly="readonly" ondblclick="openpage_search_idcard('search_id_card.php','Search Employee')"/>
						<?	
					}
					?>
            		</td>
			<td>Employee Code:</td>
			<td>
                <input type="hidden" id="txt_status_salary" />
                <input type="text" name="emp_code" id="emp_code" class="text_boxes" value="" placeholder="Search" readonly ondblclick="openpage_searchemp('search_employee.php','Search Employee')" />
            </td>
			<td>Punch Card No:</td>
			<td><input type="text" name="punch_card_no" id="punch_card_no" class="text_boxes" value="" /></td>
		</tr>
		<tr>
		  <td>Employee Name:</td>
		  <td><input type="text" name="first_name" id="first_name" class="text_boxes" value="" placeholder="First Name" /></td>
		  <td><input type="text" name="middle_name" id="middle_name" class="text_boxes" value="" placeholder="Middle Name" /></td>
		  <td><input type="text" name="last_name" id="last_name" class="text_boxes" value="" placeholder="Last Name" /></td>
		  <td>Employee Name(Bangla):</td>
		  <td><input type="text" name="full_name_bangla" id="full_name_bangla" class="bangla" value="" placeholder="First Name" /></td>
	  </tr>
		<tr>
		  <td>Father's Name:</td>
		  <td><input type="text" name="father_name" id="father_name" class="text_boxes" value="" /></td>
		  <td>Father's Name(Bangla):</td>
		  <td><input type="text" name="father_name_bangla" id="father_name_bangla" class="bangla" value="" /></td>
		  <td>Attendance Info Restricted</td>
		  <td>
          	<select id="status_attendance" name="status_attendance" class="combo_boxes">
            	<option value="0">Yes</option>
                <option value="1" selected="selected">No</option>
            </select>
          </td>
	  </tr>
		<tr>
			<td>Mother's Name:</td>
			<td><input type="text" name="mother_name" id="mother_name" class="text_boxes" value="" /></td>
			<td>Mother's Name(Bangla):</td>
			<td><input type="text" name="mother_name_bangla" id="mother_name_bangla" class="bangla" value="" /></td>
			<td>Salary Info Restricted</td>
			<td>
          	<select id="status_salary" name="status_salary" class="combo_boxes">
            	<option value="0">Yes</option>
                <option value="1" selected="selected">No</option>
            </select>
            </td>
		</tr>
		<tr>
			<td>Birth Place:</td>
			<td><input type="text" name="birth_place" id="birth_place" class="text_boxes" value="" /></td>
			<td>Date of Birth:</td>
			<td><input type="text" name="dob" id="dob" class="datepicker" value="" onblur="fn_dob();" /></td>
			<td>Age:</td>
			<td><span id="lblAge"></span></td><input type="hidden" name="hidden_age" id="hidden_age" />
		</tr>
		<tr>
			<td>Religion:</td>
			<td>
				<select name="religion" id="religion" class="combo_boxes">
					<option value="Islam">Islam</option>
					<option value="Hindu">Hindu</option>
					<option value="Christian">Christian</option>
					<option value="Buddhist">Buddhist</option>
                    <option value="Others">Others</option>
				</select>
			</td>
			<td>Marital Status:</td>
			<td>
				<select name="marital_status" id="marital_status" class="combo_boxes">
					<option value="0">Single</option>
					<option value="1">Married</option>
					<option value="2">Separated</option>
					<option value="3">Widow</option>
				</select>
			</td>
			<td>Sex:</td>
			<td>
				<select name="sex" id="sex" class="combo_boxes">
					<option value="0">Male</option>
					<option value="1">Female</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Nationality:</td>
			<td><input type="text" name="nationality" id="nationality" class="text_boxes" value="Bangladeshi" /></td>
			<td>National ID:</td>
			<td><input type="text" name="national_id" id="national_id" class="text_boxes numbers" value="" /></td>
			<td>Passport No:</td>
			<td><input type="text" name="passport_no" id="passport_no" class="text_boxes" value="" /></td>
		</tr>
		<tr>
			<td>Employee Category:</td>
			<td>
				<select name="category" id="category" class="combo_boxes" onchange="fn_confirm_date(); show_hide_trs(this.value)" >
					<?
						foreach($employee_category as $key=>$val)
						{
                    ?>
                    		<option value="<? echo $key; ?>" <? if($key==2){ echo "selected";} ?>><? echo $val; ?></option>
                    <?
						}
                    ?>                   
				</select>
			</td>
			<td>Designation Level:</td>
			<td>
				<select name="designation_level" id="designation_level" class="combo_boxes" onchange="populate_designations( this.value )">
					<option value="0">-- Select --</option>
					<?php
					$sql = "SELECT DISTINCT(system_designation), level FROM lib_designation where  status_active=1 and is_deleted=0 ORDER BY level";
					$result = mysql_query( $sql );
					while( $row = mysql_fetch_array( $result ) ) {
					?>
					<option value="<?php echo $row['level']; ?>"><?php echo $row['level'] . ". " . $row['system_designation']; ?></option>
					<?php } ?>
				</select>
			</td>
			<td>Designation:</td>
			<td>
				<select name="designation_id" id="designation_id" class="combo_boxes">
					<option value="0">-- Select --</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Functional Superior:</td>
			<td>
				<select name="functional_superior" id="functional_superior" class="combo_boxes">
					<option value="0">-- Select --</option>
					<?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
					<option value="<?php echo $designation['id']; ?>"><?php echo $designation['level'] . ". " . $designation['custom_designation']; ?></option>
					<?php } } ?>
				</select>
			</td>
			<td>Admin Superior:</td>
			<td>
				<select name="admin_superior" id="admin_superior" class="combo_boxes">
					<option value="0">-- Select --</option>
					<?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
					<option value="<?php echo $designation['id']; ?>"><?php echo $designation['level'] . ". " . $designation['custom_designation']; ?></option>
					<?php } } ?>
				</select>
			</td>
			<td>Blood Group:</td>
			<td>
				<select name="blood_group" id="blood_group" class="combo_boxes">
                	<option value="0">-- Select --</option>					
					<?
						foreach($blood_group as $key=>$val)
						{
                    ?>
                    		<option value="<? echo $key; ?>"><? echo $val; ?></option>
                    
                    <? } ?>                   
				</select>
            </td>
		</tr>
		<tr>
			<td>Joining Date:</td>
			<td><input type="text" name="joining_date" id="joining_date" class="datepicker" value="" readonly="readonly" onchange="fn_confirm_date()" /></td>
			<td>Confirmation Date:</td>
			<td><input type="text" name="confirmation_date" id="confirmation_date" class="datepicker" value="" readonly="readonly" /></td>
			<td>Skill Rank</td>
			<td>
            	<select name="skill_rank" id="skill_rank" class="combo_boxes">
					<option value="0">-- Select --</option>					
					<?
						foreach($skill_rank_arr as $key=>$val)
						{
                    ?>
                    		<option value="<? echo $key; ?>"><? echo $val; ?></option>
                    <? } ?> 
				</select>
            </td>
		</tr>
        <tr id="old_remarks">
            <td>Remark:</td>
            <td colspan="6"><textarea name="remark" id="remark" class="text_area" style="width:930px; height:18px; resize:none;"></textarea></td>
        </tr>
        <tr id="new_remarks" style="display:none">
        	<td>Remark:</td>
            <td colspan=""><textarea name="remark" id="remark" class="text_area" style="height:18px; resize:none;"></textarea></td>
            <td>Contract Start</td>
            <td><input type="text" name="Contract_start_date" id="Contract_start_date" class="datepicker" value="" readonly="readonly" /></td>
            <td>Contract Expire</td>
            <td><input type="text" name="Contract_end_date" id="Contract_end_date" class="datepicker" value="" readonly="readonly" /></td>
        </tr>
	</table>
	<table id="cost_center" style="width:97%;" border="0" cellpadding="0" cellspacing="2">
<tr>
			<th class="header">Company</th>
			<th class="header">Location</th>
			<th class="header">Division</th>
			<th class="header">Department</th>
			<th class="header">Section</th>
			<th class="header">Subsection</th>
		</tr>
		<tr>
			<td>
				<select name="company_id" id="company_id" class="combo_boxes" onchange="populate_cost_center( 'location', this.value );" ><!--onchange="populate_cost_center( 'location', this.value );"-->
                	<?php
						if($company_cond=="")
						{
					?>
					<option value="0">-- Select --</option>
					<?php 
						}
					foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
					<option value="<?php echo $company['id']; ?>" <? if(count($company_details)==1)echo "selected"; ?>><?php echo $company['company_name']; ?></option>
					<?php } } ?>
				</select>
			</td>
			<td>
				<select name="location_id" id="location_id" class="combo_boxes" onchange="populate_cost_center( 'division', this.value );" ><!--onchange="populate_cost_center( 'division', this.value );"-->
					
					<option value="0">-- Select --</option>
					<?php 
					foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
					<option value="<?php echo $location['id']; ?>" <? if(count($location_details)==1)echo "selected"; ?>><?php echo $location['location_name']; ?></option>
					<?php } } ?>
                    
           </select>
			</td>
			<td>
				<select name="division_id" id="division_id" class="combo_boxes" onchange="populate_cost_center( 'department', this.value );" ><!--onchange="populate_cost_center( 'department', this.value );"--><!--onchange="load_drop_down(this.value,'deperment_drop','department_td_id')"-->
					<option value="0">-- Select --</option>
					<?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
					<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
					<?php } } ?>
				</select>
			</td>
			<td id="department_td_id">
				<select name="department_id" id="department_id" class="combo_boxes" onchange="populate_cost_center( 'section', this.value );" ><!--onchange="populate_cost_center( 'section', this.value );"--><!--onchange="load_drop_down(this.value,'section_drop','section_td_id')"-->
					<option value="0">-- Select --</option>
					<?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
					<option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
					<?php } } ?>
				</select>
			</td>
			<td id="section_td_id">
				<select name="section_id" id="section_id" class="combo_boxes" onchange="populate_cost_center( 'subsection', this.value );"><!--onchange="populate_cost_center( 'subsection', this.value );"--><!--onchange="load_drop_down(this.value,'sub_section_drop','sub_section_td_id')"-->
					<option value="0">-- Select --</option>
					<?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
					<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
					<?php } } ?>
				</select>
			</td>
			<td id="sub_section_td_id">
				<select name="subsection_id" id="subsection_id" class="combo_boxes">
					<option value="0">-- Select --</option>
					<?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
					<option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
					<?php } } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="center" style="padding-top:5px;">
               <div id="emp_status_active" align="left" style="margin-top:5px; font-style:italic; color:#F00; font-size:14px;"></div>
               <!--<input type="text" name="emp_status_active" id="emp_status_active" />-->
				<input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
				<input type="button" name="cancel" id="cancel" class="formbutton" value="Refresh" style="margin-right:22px;" onclick="javascript:callurl.load( 'main_body', 'page_container.php?m=hr_admin/employee_info.php?permission=1_1_1_1', false, '', '' )" />                
			</td>
		</tr>
	</table>
</form>

<script>


if(user_priv_tab[1]['insert_prv']==2)
	$('#save').hide(500);
else
	$('#save').show(500);

if(user_priv_tab[1]['edit_prv']==2)
	$('#save').hide(500);
else
	$('#save').show(500);
		
</script>	
