<script type="text/javascript">
	var education_counter = 1;
	
	$(document).ready(function() {
		show_emp_education();
	});
	
	//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

	
	function add_education( education_id ) {
		if( education_id == education_counter ) {
			education_counter++;
			
			$('#tbl_education tbody').append(
				'<tr>'
					+ '<td><input type="text" name="exam_name[]" id="exam_name_' + education_counter + '" value="" class="text_boxes" onfocus="add_education( ' + education_counter + ' );" /></td>'
					+ '<td><input type="text" name="board[]" id="board_' + education_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><input type="text" name="institution[]" id="institution_' + education_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><input type="text" name="discipline[]" id="discipline_' + education_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><input type="text" name="major_subject[]" id="major_subject_' + education_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><input type="text" name="passing_year[]" id="passing_year_' + education_counter + '" value="" class="text_boxes numbers" onkeypress="return numbersonly(this,event)" /></td>'
					+ '<td><input type="text" name="result[]" id="result_' + education_counter + '" value="" class="text_boxes" /></td>'
					+ '<td><select name="edu_nature[]" id="edu_nature_' + education_counter + '" value="" class="combo_boxes"><option value="0">Academic</option><option value="1">Professional</option><option value="2">Training</option></select></td>'
				+ '</tr>'
			);
		}
	}
	
	function reset_education_form() {
		$('#tbl_education tbody').html( '' );
		education_counter = 0;
		add_education( 0 );
	}
</script>
<style type="text/css">
	#tbl_education tbody input[type="text"] { width:134px; }
</style>
<form id="emp_ducation_form" action="javascript:fnc_emp_education(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
	<table id="tbl_education" style="width:95%" border="0" cellpadding="0" cellspacing="3">
		<thead>
			<tr>
            	<input type="hidden" name="save_up_education" id="save_up_education" value="" />
				<th class="header">Course Name</th>
				<th class="header">Board</th>
				<th class="header">Institution</th>
				<th class="header">Discipline</th>
				<th class="header">Major Subject</th>
				<th class="header"> Year</th>
				<th class="header">Result</th>
               <th class="header">Education Nature</th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<td colspan="8" align="center" style="padding-top:10px;">
					<input type="submit" id="save_addres_edu" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
					<input type="button" name="cancel" class="formbutton" value="Refresh" onclick="reset_education_form()" />
				</td>
			</tr>
		</tfoot>
	</table>
</form>
<script>
show_emp_education();


if(user_priv_tab[8]['insert_prv']==2)
	$('#save_addres_edu').hide(500);
else
	$('#save_addres_edu').show(500);

if(user_priv_tab[8]['edit_prv']==2)
	$('#save_addres_edu').hide(500);
else
	$('#save_addres_edu').show(500);
</script>	

