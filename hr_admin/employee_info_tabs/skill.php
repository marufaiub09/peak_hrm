<script type="text/javascript">
	var skill_counter = 0;
	var skill_options=''; 

	$(document).ready(function() {
		show_emp_skill();
		
	});
	
	function get_datepicker() {
		$('.datepicker').datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
	}
	
	<?php 
		include("../../includes/common.php");
		include("../../includes/array_function.php");
		
		$sql="SELECT * FROM lib_skill WHERE is_deleted = 0 ORDER BY skill_name ASC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$skill_details = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
		$skill_details[$row['id']] = mysql_real_escape_string( $row['skill_name'] );
		}
	
		foreach( $skill_details AS $key => $value ) {
		echo "skill_options += '<option value=\"$key\">$value</option>';\n";
		}
	?>

	function add_skill( skill_id ) {
		if( skill_id == skill_counter ) {
			skill_counter++;			
			$('#tbl_skill tbody').append(
			'<tr>'
				+ '<td>'
					+'<input type="text" name="skill_name[]" id="skill_name_' + skill_counter + '" class="text_boxes" placeholder="Search" readonly  onfocus="add_skill( ' + skill_counter + ' );" ondblclick="openpage_searchskill( ' + skill_counter + ');" style="width:180px;" />'
					+'<input type="hidden" name="skill_id[]" id="skill_id_' + skill_counter + '" />'
				+'</td>'
				+ '<td>'
					+ '<select name="current_assign[]" id="current_assign_' + skill_counter + '" class="combo_boxes" style="width:120px;">'
					+'<option value="0">-- Select --</option>'
					+'<option value="1">Yes</option>'
					+'<option value="2">No</option>'
					+ '</select>'
				+ '</td>'
				+ '<td><input type="text" name="eff_date[]" id="eff_date_' + skill_counter + '" value="" class="datepicker" onclick="get_datepicker();" style="width:120px;" /></td>'
			+ '</tr>'
			);
		}
		get_datepicker();
	}
	
	function reset_skill_form() {
		$('#tbl_skill tbody').html( '' );
		skill_counter = 0;
		add_skill( 0 );
	}
	
	function openpage_searchskill(rid)
	{
		//alert(rid);
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_employee_skill.php'+'?rid='+rid, 'search skill', 'width=450px,height=420px,center=1,resize=0,scrolling=0',' ')
		emailwindow.onclose=function()
		{
			//alert("su...re..");
			var theemail=this.contentDoc.getElementById("sname").value;
			var datas=theemail.split(",");
			//alert(datas[1]);
			$('#skill_name_'+datas[2]+'').val(datas[0]);
			$('#skill_id_'+datas[2]+'').val(datas[1]);
		}
	}

	
</script>
<form id="emp_skill_form" action="javascript:fnc_emp_skill(save_perm,edit_perm,delete_perm,approve_perm)" method="POST" >
	<table id="tbl_skill" style="width:450px;" border="0" cellpadding="0" cellspacing="3">
    	<input type="hidden" name="save_up_skill" id="save_up_skill" value="" />
		<thead>
			<tr>
				<th class="header">Skill Name</th>
				<th class="header">Current Assign</th>
				<th class="header">Effective Date</th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
            
				<td colspan="3" align="center" style="padding-top:10px;">
					<input type="submit" id="save_addres_skill" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
					<input type="button" name="cancel" class="formbutton" value="Refresh" onclick="reset_skill_form()" />
				</td>
			</tr>
		</tfoot>
	</table>
</form>
<script>
show_emp_skill();

if(user_priv_tab[13]['insert_prv']==2)
	$('#save_addres_skill').hide(500);
else
	$('#save_addres_skill').show(500);

if(user_priv_tab[13]['edit_prv']==2)
	$('#save_addres_skill').hide(500);
else
	$('#save_addres_skill').show(500);
</script>	
