<?php
/***********************************
| Developed by	: Md. Nuruzzaman 
| Date			: 23.01.2014
***********************************/

session_start();
include('../includes/common.php');
include('../includes/array_function.php');

extract($_POST);
extract($_GET);
$search_string=trim($_GET["search_string"]);
$search_string2=trim($_GET["search_string2"]);
$type=$_REQUEST["type"];
$com_name=$_GET["com_name"];


if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

	$search_string=$_GET["m_company"];
	$m_buyer=$_GET["m_buyer"];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Cost Sheets</title>
        
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="includes/functions.js"></script>
        
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../includes/tablefilter.js"></script>
        <script>
            function js_set_value_skill(str)
            {
                //alert(str);
                document.getElementById('sname').value=str;
                parent.emailwindow.hide();
            }
        </script>
    </head>
    <body>
    <table width="400" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table">
        <thead>
            <th width="40">SL</th>
            <th width="360">Skill Name</th>
        </thead>
    </table>

    <div style="max-height:245px;overflow-y:scroll; width:420px;" id="search_div">	
        <table width="400" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table" id="table_body">
        <tbody>
			<?php
			//echo $rid;die;
            $i=1;
            $sql="SELECT id, skill_name FROM lib_skill WHERE is_deleted = 0 ORDER BY skill_name ASC";		
            $result=mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
            while ($row= mysql_fetch_assoc($result)){
            
            //for employee information ---- do not deleted	
            //if($explode_string[3]=="empinfoform") $paramt = "js_set_value('".$i.'_'.$selectResult[emp_code]."')"; else $paramt="fn_row_select('".$i.'_'.$selectResult[emp_code]."')";		
            if ($i%2==0)  
            $bgcolor="#E9F3FF";
            else
            $bgcolor="#FFFFFF";	
            ?>
            <tr bgcolor="<? echo $bgcolor; ?>" style="cursor:pointer" class="trColor_<? echo $i;?>" onclick="js_set_value_skill('<?php	echo $row['skill_name'].','.$row['id'].','. $rid; ?>');" > <!--js_set_value(this.id);-->
                <td width="40"><?php echo "$i"; ?><input type="hidden" name="sname" id="sname" /></td>	
                <td width="360"><?php echo $row['skill_name']; ?></td>	
            </tr>
            <?php $i++; }?>
            </tbody>
        </table>
	</div>
	<script>
		var tableFilters ={
			col_0: "none",
		}
		setFilterGrid("table_body",-1,tableFilters);
    </script>
</body>
</html>   
