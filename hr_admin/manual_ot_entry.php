<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../includes/common.php');
include('../includes/array_function.php');


//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

<script src="includes/functions.js" type="text/javascript"></script>
<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>

<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>

    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/popup_window.js"></script>
    <script type="text/javascript" src="../js/modal.js"></script>
    
    <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
    <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    

<script type="text/javascript">


		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;


$(document).ready(function() {
	$( ".datepicker" ).datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});	

            
$(document).ready(function(e) 
{
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
	//header: false, 
	selectedText: "# of # selected",
	});
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});	
});

	
	function openmypage_employee_info()
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");
			$('#emp_code').val(thee_id.value);
		}
	}
	
	function openmypage_employee_info_id_card()
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
			$('#id_card').val(thee_id.value);
		}
	}		



function fn_search_manual_ot()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_date').focus();
			$(this).html('Please select Overtime Date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else if( $('#cbo_shift_name').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_shift_name').focus();
			$(this).html('Please select Shift Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_shift_name').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_shift_name').focus();
			$(this).html('Please select Shift Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_adjustment_type').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_adjustment_type').focus();
			$(this).html('Please select Adjustment Type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#txt_adjustment_mins').val() == ""  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_adjustment_mins').focus();
			$(this).html('Please select Adjustment Minutes.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else
	{
		 data =  '&cbo_shift_name=' + $('#cbo_shift_name').val() 
				+ '&cbo_adjustment_type=' + $('#cbo_adjustment_type').val()
				+ '&txt_adjustment_mins=' + $('#txt_adjustment_mins').val()
				+ '&txt_date=' + $('#txt_date').val()
				+ '&cbo_emp_category=' + $('#cbo_emp_category').val() 
				+ '&cbo_company_id=' + $('#cbo_company_id').val() 
				+ '&location_id=' + $('#location_id').val()
				+ '&division_id=' + $('#division_id').val() 
				+ '&department_id=' + $('#department_id').val()
				+ '&section_id=' + $('#section_id').val() 
				+ '&subsection_id=' + $('#subsection_id').val() 
				+ '&designation_id=' + $('#designation_id').val()
				+ '&id_card=' + $('#id_card').val() 
				+ '&emp_code=' + $('#emp_code').val();
				
		nocache = Math.random();
		http.open( 'GET', 'includes/ot_adjustment_data.php?action=ot_adjustment' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fn_search_manual_ot;
		http.send(null);		
		
	}
		
}

function response_fn_search_manual_ot() {	
	if(http.readyState == 4) {
		//var response = http.responseText.split('####');		
		 document.getElementById('data_panel').innerHTML=http.responseText;
		 $("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Data Populated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}



function fn_net_ot_cal(adj_value,ot,type,position){
	if(type==1) 
		var net_ot = ot*1+adj_value*1;
	else
		var net_ot = ot*1-adj_value*1;	
		
	document.getElementById(position).innerHTML = net_ot*1;	
}


function Check(chk)
		{
			var cnt=0;
			
			if(document.frm_ot_adjustment.Check_ctr.checked==true){
				for (i = 0; i < chk.length; i++){
					chk[i].checked = true ;
					cnt++;
				}
			}else{			
				for (i = 0; i < chk.length; i++)
				chk[i].checked = false ;			
			}
			$('#count_id').html('You Are Selected '+cnt+' Employees');
		}
		
	function count_emp(chk){
		var cnt=0;
		$('#tbl_ot_adjustment tbody input[type="checkbox"]:checked').each(function() {
				cnt++;
			});
		$('#count_id').html('You Are Selected '+cnt+' Employees');
	}



function fn_save_ot_adjustment(save_perm,edit_perm,delete_perm,approve_perm){
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false,val=0; employee_counter = 0, data = '';
	var employee_code='',ot_min='',adj_min='',net_ot_min='';
		
	
	if(save_perm==2){
		error = true;		
		$(this).html('You Do Not Have Save Permission.').addClass('messageboxerror').fadeTo(900,1);
	}
	else if( $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_date').focus();
			$(this).html('Please select Overtime Date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else if( $('#cbo_shift_name').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_shift_name').focus();
			$(this).html('Please select Shift Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_shift_name').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_shift_name').focus();
			$(this).html('Please select Shift Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_adjustment_type').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_adjustment_type').focus();
			$(this).html('Please select Adjustment Type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#txt_adjustment_mins').val() == ""  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_adjustment_mins').focus();
			$(this).html('Please select Adjustment Minutes.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0  ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {		
		$('#tbl_ot_adjustment tbody input[type="checkbox"]:checked').each(function() {
				val = $(this).val();
				employee_counter++;
				
				employee_code 	+=$('#B'+val).html()+"_";
				ot_min			+=$('#L'+val).html()+"_"; 	
				adj_min			+=$('#txt_adjst_min_html'+val).val()+"_";
				net_ot_min		+=$('#O'+val).html()+"_";
			});
			
			employee_code 	= employee_code.substr( 0, employee_code.length - 1 );
			ot_min 			= ot_min.substr( 0, ot_min.length - 1 );
			adj_min 		= adj_min.substr( 0, adj_min.length - 1 );
			net_ot_min 		= net_ot_min.substr( 0, net_ot_min.length - 1 );
				
			if( employee_counter == 0 ) {
				error = true;
				$("#messagebox").removeClass().addClass('messageboxerror').text('Please select at least one employee.').fadeIn(1000);							
			}
			else data = '&txt_date=' + $('#txt_date').val() + 
						'&cbo_shift_name=' + $('#cbo_shift_name').val() +
						'&txt_remarks=' + $('#txt_remarks').val() +
						'&cbo_adjustment_type=' + $('#cbo_adjustment_type').val() +
						'&txt_adjustment_mins=' + $('#txt_adjustment_mins').val() +
						'&employee_code=' + employee_code +
						'&ot_min=' + ot_min +
						'&adj_min=' + adj_min +
						'&net_ot_min=' + net_ot_min; 
						
		
		if( error == false ){
			nocache = Math.random();
			http.open( 'POST', 'includes/ot_adjustment_data.php?action=ot_adjustment_save' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_fn_save_ot_adjustment;
			http.send(null);
			
		}
	}
	
}

function response_fn_save_ot_adjustment() {
	if(http.readyState == 4) {
		var response = http.responseText.split('###');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Insert Successfully Done.' ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				save_activities_history('','hrm','ot_adjustment','save','../');
			});			
		}
		else {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Error!!OT Adjustment Save Problem Occured!!!!!!' ).addClass('messagebox_ok').fadeTo(900,1);
			});
		}		
	}
}


//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


</script>



</head>	


<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:100%">
	<div>
    	<div class="form_caption">Manual Overtime Adjustment</div>
        <span id="permission_caption"> <? echo "Your Permissions--> \n".$insert.$update;?></span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
  <fieldset>
	<form id="frm_ot_adjustment" name="frm_ot_adjustment" action="" >
		<div align="center" style="width:100%;">
			<table cellpadding="0" cellspacing="2" width="1000" align="center" >
				<tr>
					<td width="119" >Overtime Date</td>
					<td> 
					 	<input type="text" placeholder="Select Date" name="txt_date" id="txt_date" class="datepicker" style="width:150px" value="">
                    </td>
					<td>Shift Name</td>
					<td width="221"><select name="cbo_shift_name" id="cbo_shift_name" class="combo_boxes" style="width:160px">
					  <option value="0">-- Select --</option>
					  <?php foreach( $shift_policy AS $shift ) { ?>
					  <option value="<?php echo $shift['id']; ?>"><?php echo $shift['shift_name']; ?></option>
					  <?php } ?>
				    </select></td>
					<td width="63" rowspan="2">Remarks</td>
					<td width="237" rowspan="2"><textarea name="txt_remarks" id="txt_remarks" cols="30" class="text_area"></textarea></td>			  
              	</tr>
			
		      	<tr>
                      <td>Adjustment Type</td>
                      <td width="203">
                          <select name="cbo_adjustment_type" id="cbo_adjustment_type" class="combo_boxes" style="width:160px">
                            <?
                                    foreach($value_status as $key=>$val){
                                ?>
                            <option value="<? echo $key; ?>" > <? echo $val; ?></option>
                            <? } ?>
                          </select>
                      </td>
                      <td width="141">Adjustment Mins</td>
                      <td><input type="text" id="txt_adjustment_mins" name="txt_adjustment_mins" class="text_boxes" style="width:150px" onkeypress="return numbersonly(this,event)" /></td>
                </tr>
                
	      </table>
	</div>
    	
 	<br />
    
    <div align="center">
	  <table width="1050"  cellpadding="0" cellspacing="0" class="rpt_table" align="center">
				<thead>                    
                    <th width="120px"><strong>Category</strong></th>
					<th width="120px"><strong>Company</strong></th>
					<th width="120px"><strong>Location</strong></th>
                    <th width="120px"><strong>Division</strong></th>
					<th width="120px"><strong>Department</strong></th>
                    <th width="120px"><strong>Section</strong></th>
                    <th width="120px"><strong>SubSection</strong></th>
                    <th width="120px"><strong>Designation</strong></th>
				</thead>
                <tr class="general">					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:120px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:120px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
                    <td>
                        <select name="division_id" id="division_id" class="combo_boxes" style="width:120px" multiple="multiple">
                        <?php foreach( $division_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px" multiple="multiple" >
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:120px" multiple="multiple" >
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px" multiple="multiple" >
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" multiple="multiple" >
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>                   
				</tr>
			</table>
            <div style="height:8px;"></div>
            <table class="rpt_table" style="800" border="0" cellpadding="0" cellspacing="1" align="centre"> 
                <thead>
                    <th width="150px">ID Card No</th>
                    <th width="150px">System Code</th>                  			
                    <th bgcolor="#A6CAF0"><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:70px" /></th> 
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:150px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                        </td>
                        <td>
                            <input type="text" name="emp_code" id="emp_code" class="text_boxes" style="width:150px" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" autocomplete="off" />
                        </td>                           
                        <td bgcolor="#A6CAF0"><input type="button" name="search" id="search" value="Show" class="formbutton" style="width:70px" onclick="javascript: fn_search_manual_ot()" /></td>
                    </tr> 
                </tbody>
            </table> 
 		</div>
        <span id="count_id" style="font-weight:bold">&nbsp;</span>
 		<br/>            
             <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>       
		<br/>		
            <input type="button" value=" Save" name="add" id="close" class="formbutton" style="width:100px" onclick="fn_save_ot_adjustment(save_perm,edit_perm,delete_perm,approve_perm)" />&nbsp;
            <input type="reset" value="  Refresh  " name="add" id="close" class="formbutton" style="width:100px" />			
           
	  </form>
	</fieldset>
    
</div>
</body>
</html>
