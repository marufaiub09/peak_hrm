<?
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 04-04-12
	multiselect added By
	Name : Ekram
	Date : 19-11-2013
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
include("../includes/array_function.php");

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0  ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../includes/tablefilter.js"></script>
        
         <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        
		<script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        
        
        <script type="text/javascript">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
            
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });			
                
            });
					
		$(document).ready(function(e) 
		{
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
			//header: false, 
			selectedText: "# of # selected",
			});
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
			
			});	
		});		
        
            function populate_data() 
            {
                $('#data_panel').html('<img src="../resources/images/loading.gif" />');
                var category 			= $('#cbo_emp_category').val();
                var company_id 			= $('#cbo_company_id').val();
                var location_id 		= $('#location_id').val();
                var division_id 		= $('#division_id').val();
                var department_id 		= $('#department_id').val();
                var section_id 			= $('#section_id').val();
                var subsection_id 		= $('#subsection_id').val();			
                var designation_id 		= $('#designation_id').val();
                var id_card 			= $('#id_card').val();
                var empcode 			= $('#emp_code').val();
                //alert(id_card);
                var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&division_id='+division_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&id_card='+id_card+'&empcode='+empcode;
                
                $.ajax({
                    type: "POST",
                    url: "employee_info_data.php?"+passdata,
                    data: 'form=job_separation',
                    success: function(data) {
                        $('#data_panel').html( data );
                        var tableFilters = {col_0: "none"}
                        setFilterGrid("tbl_separation",-1,tableFilters);
                        hide_search_panel();				
                    }
                });
            }		
            
            function fn_frm_refresh(){
                window.location.reload(true);		
            }
        
            function Check(chk)
            {
                var cnt=0;
                
                if(document.separation_form.Check_ctr.checked==true){
                    for (i = 0; i < chk.length; i++){
                        chk[i].checked = true ;
                        cnt++;
                    }
                }else{			
                    for (i = 0; i < chk.length; i++)
                    chk[i].checked = false ;			
                }
                $('#count_id').html('You Are Selected '+cnt+' Employees');
                
            }
            
            function count_emp(chk)
            {
                var cnt=0;
                $('#tbl_separation tbody input[type="checkbox"]:checked').each(function() {
                        cnt++;
                    });
                    document.getElementById('cnt_val').value=cnt;
                    //alert(cnt);
                $('#count_id').html('You Are Selected '+cnt+' Employees');
            }
        
            //ekram
            function openmypage_employee_info()
            {			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");
                    $('#emp_code').val(thee_id.value);
                }
            }
    
            function openmypage_employee_info_id_card()
            {			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }
            
            //Numeric Value allow field script
            function numbersonly(myfield, e, dec)
            {
                var key;
                var keychar;
            
                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);
            
                // control keys
                if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
                return true;
                
                // numbers
                else if ((("0123456789.,-").indexOf(keychar) > -1))
                    return true;
                else
                    return false;
            }
        
            //new 2014-03-10
            function populate_value(str)
            {
                if(str==1 || str==0)
                {
                    $('#separation_period').val( 0 ).attr("disabled",false);
                    
                }
                else
                {
                    $('#separation_period').val( 0 ).attr("disabled", "disabled");
                }
            }
			
			
			
function generate_list_view(selected_id){
	
	var page_link="includes/get_data_update.php?period=";
	var id=selected_id;
	var title="Lock Status";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+id+'&type=list_view', title,'width=400px,height=300px,center=1,resize=0,scrolling=0','')
	
	emailwindow.onclose=function(){};		
			
		}
		
        </script>
        <style>
            .formbutton { width:100px; }
            #separation_form * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px;">		
            <div>
                <div class="form_caption">Employee Job Separation</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
            </div>
            <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> - Employee Job Separation</h3>
            <div id="content_search_panel" >
                <form name="separation_form" id="separation_form" action="" method="POST">
                    <fieldset style="padding:10px;">
                        <table width="1000px" cellpadding="0" cellspacing="1" align="center">
                            <tr>
                                <td width="100">Effective  From:</td><input type="hidden" id="cnt_val" name="cnt_val" />
                                <td><input type="text" name="separated_from" id="separated_from" value="" class="datepicker"  style="width:100px;"  onchange="generate_list_view(this.value)"/></td>
                                <td width="120">&nbsp;Separation Type:</td>
                                <td>
                                    <select name="separation_type" id="separation_type" class="combo_boxes" onchange="populate_value(this.value)"  style="width:120px;">
                                        <option value="0">-- Select --</option>
                                        <option value="1">Resignation</option>
                                        <option value="2">Retirement</option>
                                        <option value="3">Disability</option>
                                        <option value="4">Death</option>
                                        <option value="5">Terminated</option>
                                        <option value="6">Un-noticed</option>
                                    </select>
                                </td>
                                <td width="120">&nbsp;Separation Period</td>
                                <td>
                                    <select name="separation_period" id="separation_period" class="combo_boxes"  style="width:120px;">
                                        <option value="0">--Select--</option>
                                        <?
                                        $company_sql= mysql_db_query($DB, "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id  and b.type=1 and b.is_locked=0 and a.is_locked=0");
                                        while ($r_company=mysql_fetch_array($company_sql))
                                        {
                                        ?>
                                        <option value=<? echo "$r_company[actual_starting_date]"."**"."$r_company[actual_ending_date]";
                                        if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? $ex=explode(" ",$r_company[starting_date]); echo "$ex[0]"; ?> </option>
                                        <?
                                        }
                                        ?>
                                    </select>	
                                </td>
                                <td width="150">&nbsp;Cause of Separation:</td>
                                <td><textarea name="cause_of_separation" id="cause_of_separation" class="text_area" style="width:150px; height:30px;"></textarea></td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset style="padding:10px;">
                        <table class="rpt_table" style="900px" border="0" cellpadding="0" cellspacing="1" align="center">
                            <thead>			                
                                <th width="100"> Category</th>
                                <th width="100"> Company</th>
                                <th width="100"> Location</th>
                                <th width="100"> Division</th>
                                <th width="100"> Department</th>
                                <th width="100"> Section</th>
                                <th width="100"> SubSection</th>
                                <th width="100"> Designation</th>
                            </thead>
                            <tr class="general">					
                                <td>
                                    <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                                    <option value="">All Category</option>
                                    <?
                                    foreach($employee_category as $key=>$val)
                                    {
                                    ?>
                                    <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                    <?
                                    }
                                    ?> 
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:120px">
                                    <? if($company_cond=="")
                                    { 
                                    ?>
                                    <option value="0">-- Select --</option>
                                    <?php } foreach( $company_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                                    <option value="0">-- Select --</option>
                                    <?php foreach( $location_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="division_id" id="division_id" class="combo_boxes" style="width:120px" multiple="multiple">
                                    <!--<option value="0">-- Select --</option>-->
                                    <?php foreach( $division_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="department_id" id="department_id" class="combo_boxes" style="width:120px"  multiple="multiple">
                                     <!--<option value="0">-- Select --</option>-->
                                    <?php foreach( $department_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="section_id" id="section_id" class="combo_boxes" style="width:120px"  multiple="multiple">
                                     <!--<option value="0">-- Select --</option>-->
                                    <?php foreach( $section_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td id="subsection">
                                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px"  multiple="multiple">
                                    <!--<option value="0">-- Select --</option>-->
                                    <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td> 
                                <td id="designation">
                                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px"  multiple="multiple">
                                    <!--<option value="0">-- Select --</option>-->
                                    <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td> 
                            </tr>
						</table>
                        <div style="height:8px;"></div>
                        <table class="rpt_table" style="600" border="0" cellpadding="0" cellspacing="1" align="centre"> 
                            <thead>
                                <th>ID Card No</th>
                                <th>System Code</th>                  			
                                <th bgcolor="#A6CAF0"><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" /></th> 
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>
                                    <td>
                                        <input type="text" name="emp_code" id="emp_code" class="text_boxes" style="width:100px" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>                           
                                    <td bgcolor="#A6CAF0"><input type="button" name="search" id="search" value="Search" class="formbutton" onclick="populate_data()" /></td>
                                </tr> 
                            </tbody>
                        </table> 
                    </fieldset>
                </form>
            </div>
            <fieldset><span id="count_id" style="font-weight:bold">&nbsp;</span><div id="data_panel" style="margin-top:4px;"></div>
                <div style="width:100%; margin-top:4px; text-align:center;"> 
                    <input type="button" name="separate" value="Separate" class="formbutton" onclick="fnc_job_separation(save_perm,edit_perm,delete_perm,approve_perm,document.getElementById('cnt_val').value)" />&nbsp; 
                </div>
            </fieldset>
		</div>
    </body>
</html>