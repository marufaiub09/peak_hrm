<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date :17/04/2012
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../includes/common.php');
include('../includes/array_function.php');

//company_details
$sql = "SELECT * FROM lib_company WHERE status_active=1 and is_deleted = 0 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Transport Users</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

<link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/popup_window.js"></script>
<script type="text/javascript" src="../js/modal.js"></script>

<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>

<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
<script src="includes/ajax_submit_hr_admin.js"></script>
<script src="includes/functions.js"></script>

<script>
	var save_perm = <? echo $permission[0]; ?>;
	var edit_perm = <? echo $permission[1]; ?>;
	var delete_perm = <? echo $permission[2]; ?>;
	var approve_perm = <? echo $permission[3]; ?>;
</script>

<script type="text/javascript">
// emp search for transport
		function openmypage_employee_info(page_link,title)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+'?com_name='+document.getElementById('cbo_company_name').value, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0','')
			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");
				//alert(thee_loc.value+thee_id.value);
				var txt_vehicle_no=document.getElementById('txt_vehicle_no').value;
				if(txt_vehicle_no=="")
				{
					showResult_search_transport(" ",thee_id.value,"transport_emp_info","transport_div");
				}
				else
				{
					showResult_search_transport(document.getElementById('txt_vehicle_no').value,thee_id.value,"transport_emp_info","transport_div");	
				}
				//vehicle_detail_info(thee_id.value,"vehicle_save_up");
			}
		}
// Employee list for specific vehicle 
		function openmypage_vehicle(page_link,title)
		{
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+'?com_name='+document.getElementById('cbo_company_name').value+'&location_name='+document.getElementById('cbo_company_location').value, title,  'width=990px,height=350px,center=1,resize=0,scrolling=0','')
			
			emailwindow.onclose=function()
			{
				
				var theemail=this.contentDoc.getElementById("txt_selected_vehicle") //Access form field with id="emailfield"
				//alert(theemail.value);
			vehicle_detail_info(theemail.value,"vehicle_list");
			
	 showResult_search_transport(document.getElementById('cbo_company_name').value, theemail.value ,"transport_vehicle_emp_info","transport_div");
				
			}
		}
		
// ----------search vehicle list-------------------------
function emp_search()
{
	var transport_type=document.getElementById('cbo_transport_type').value;
	if(transport_type==0)
	{
		if($('#cbo_transport_type').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_transport_type').focus();
			$(this).html('Please select transport type').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
		}
			return false;
	}
	else
	{
		openmypage_vehicle('search_vehicle_user_list.php','Allocated Vehicle List');	
	}
}

function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}	
// check uncheck all
function fnCheckUnCheckAll(checkVal)
    {
        for (Looper=0; Looper < document.forms[0].length ; Looper++ )
        {
            var strType = document.forms[0].elements[Looper].type;
            if (strType=="checkbox")
            {
                
                document.forms[0].elements[Looper].checked=checkVal;
            }   
        }
        check_count();
    }

// count checkboxes
function check_count(){
	
	var tmp="";
	var capacity=document.getElementById('txt_capacity').value;	
	if(capacity=="")
	{
	if($('#txt_capacity').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_capacity').focus();
			$(this).html('Please Insert Vehicle Capacity').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	document.getElementById('txt_booked').value="";
	 for (Looper=0; Looper < document.forms[0].length ; Looper++ )
        {
            var strType = document.forms[0].elements[Looper].type;
            if (strType=="checkbox")
            {
                
                document.forms[0].elements[Looper].checked=false;
            }  
        }
		 document.getElementById('check_all').checked=false;
	}
	else
	{
			var myform = document.getElementById('form_transport');
			var inputTags = myform.getElementsByTagName('input');
			var chkCnt = 0;
			var length = inputTags.length;
			for (var i=0; i<length; i++) {
				 if (inputTags[i].type == 'checkbox' && inputTags[i].checked) {
					 chkCnt++;
					 var emp_array=inputTags[i].value;
						tmp+=emp_array+",";
				 }
			}
					//alert(chkCnt);
				/*var total=document.form_transport.checkbox.length;
				var chkCnt = 0;
				//var total=document.getElementById('transport_div').rows.length;
				//alert(total);
				for(var i=0; i<total; i++)
				{
					if(document.form_transport.checkbox[i].checked)
					{
							chkCnt++;
							var emp_array=document.form_transport.checkbox[i].value;
								tmp+=emp_array+",";	
					}
				}	*/
				document.getElementById('txt_booked').value=chkCnt;
				document.getElementById('txt_remainning').value=(capacity*1)-(chkCnt*1);
				document.getElementById('emp_code').value=tmp;
	}
} 

</script>

</head>


<body>
<div align="center">
    	<div style="width:920px">
            <div class="form_caption">
               Transport Users
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update;
                ?>
            </span>
            <div style="height:12px;">
            <div id="messagebox" style="background-color:#FF9999; color:#000000;" align="center"></div>
            </div>
        </div>
	<!-- Start Field Set -->
	
<fieldset style="width:920px">
	<legend>HRM Module</legend>
	<!-- Start Form -->
	
	<form name="form_transport" id="form_transport" method="" action="javascript:fnc_transport(save_perm,edit_perm,delete_perm,approve_perm);">
			<!-- Start Main Table -->
            <fieldset>
			<table cellpadding="1" cellspacing="3" width="100%">
				
				<tr>
					<td width="110" >Transport Type</td>
                    <td width="130"> 
					 <select name="cbo_transport_type" id="cbo_transport_type" class="combo_boxes" style="width:155px">
                     <option value='0'>-- Select Transport --</option>
                     <?php
                     foreach($transport_type as $id=>$value)
                     {?>
                      <option value="<?php echo $id; ?>"><?php echo $value; ?></option>
                     <?php } ?>
                     </select>
					</td>
                    <td width="110" >Vehicle No.</td>
					<td width="130"> 
					 <input type="text" name="txt_vehicle_no" id="txt_vehicle_no" placeholder="Double click for search" class="text_boxes" style="width:145px" ondblclick="emp_search(); return false" autocomplete="off">
					</td>
            		<td width="110">
						Route
					</td>
					<td width="130"> 
					<input type="text" name="txt_route" id="txt_route" class="text_boxes" style="width:145px">
					</td>
				</tr>
				<tr>
					<td width="110">
						Stoppage
					</td>
					<td width="130">
					  <input type="text" name="txt_stoppage" id="txt_stoppage" class="text_boxes" style="width:145px">
					</td>
					<td width="110" >
						Vehicle Capacity
					</td>
					<td width="130">
					 <input type="text" name="txt_capacity" id="txt_capacity" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)">
					</td>
                	<td width="110">
						Booked
					</td>
					<td width="130"> 
					<input type="text" name="txt_booked" id="txt_booked" class="text_boxes_numeric" style="width:145px" readonly="">
					</td>
				</tr>
                <tr>
					<td width="110">
						Remaining
					</td>
					<td width="130"> 
					  <input type="text" name="txt_remainning" id="txt_remainning" class="text_boxes_numeric" style="width:145px" readonly="">
					</td>
                <td width="110">
						Driver's Name
					</td>
					<td width="130"> 
					  <input type="text" name="txt_driver_name" id="txt_driver_name" class="text_boxes" style="width:145px">
					</td>
                <td width="110">
					Driving	License No.
					</td>
					<td width="130"> 
					  <input type="text" name="txt_license_no" id="txt_license_no" class="text_boxes" style="width:145px">
					</td>
                </tr>
                <tr>
                     <td width="120">
						License Expired Date
					</td>
					<td width="130"> 
					  <input name="license_expired_date" id="license_expired_date" class="text_boxes" style="width:145px">
                      						<script type="text/javascript">
                                                    $( "#license_expired_date" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               </script>	
					</td>
                     <td width="110">
						Driver's Mobile No.
					</td>
					<td width="130"> 
					  <input type="text" name="txt_mobile_no" id="txt_mobile_no" class="text_boxes_numeric" style="width:145px" onKeyPress="return numbersonly(this,event)"/>
					</td>
                </tr>
             </table><!-- End Main Table -->
              <table>
                    	 <tr>
                         <td><input type="hidden" name="emp_code" id="emp_code" /><input type="hidden" name="emp_code_data" id="emp_code_data" />
                           <input type="hidden" name="save_up" id="save_up" /></td>
							<!--/*<td  colspan="2" align="center">
								<input type="submit" value=" Save" name="add" id="close" class="formbutton"/>&nbsp;
								<input type="reset" value="  Refresh  " name="add" id="close" class="formbutton"/>			
							</td>*/-->
						</tr>
			</table>
			</fieldset>
<br />
<div style="width:920px;">
	<!--<fieldset>-->
	<table cellspacing="0" cellpadding="0" width="880" class="rpt_table" border="0">
		<thead>
        	<th width="35">&nbsp;</th>
			<th width="140" >Emp Code</th>
			<th width="110">Emp Name</th>
            <th width="100">Comp. Name</th>
			<th width="100">Designation</th>
			<th width="100">Division</th>
			<th width="100">Department</th>
			<th width="90">Section</th>
			<th>Sub Section</th>
		</thead>
        
	<tr class="general">
    	<td width="35">&nbsp;</td>
		<td>
			<input type="text" name="txt_emp_code" placeholder="Double click for search" class="text_boxes" style="width:140px" ondblclick="openmypage_employee_info('search_employee_multiple.php','Employee Information'); return false" autocomplete="off" />
		</td>
		<td>
			<input type="text" name="txt_emp_name"  class="text_boxes" style="width:110px"  />
		</td>
        <td>
			<input type="text" name="txt_company_name"  class="text_boxes" style="width:80px"   />
		</td>
		<td>
		  <input name="txt_emp_designation" type="text"  class="text_boxes" style="width:80px"  />	
		</td>
		<td>
			<input type="text" name="txt_emp_division"  class="text_boxes" style="width:80px"   />
		</td>
		<td>
			<input type="text" name="txt_emp_department"  class="text_boxes" style="width:80px"   />
		</td>
		<td>
			<input type="text" name="txt_emp_section"  class="text_boxes" style="width:80px"   />
		</td>
		<td>
			<input type="text" name="txt_emp_sub_section"  class="text_boxes" style="width:70px"   />
		</td>
	</tr>
	</table>
    <div style="width:930px; overflow-y:scroll; min-height:50px; max-height:250px;" id="transport_div">
    </div>
 </div> 
 	</form>
	<!-- End Form -->
     <table border="0" width="100%">
        <tr>
            <td width="43%" height="30" valign="bottom" class="button_container">
                <input form="form_all" type="checkbox" name="check_all" id="check_all" value=""  onclick="fnCheckUnCheckAll(this.checked)"/> Check / Uncheck All
           </td>
           <td class="button_container">
           		<input form="form_transport" type="submit" value=" Save" name="add" id="close" class="formbutton" style="width:100px"/>&nbsp;
				<input type="reset" form="form_transport" value="  Refresh  " name="add" id="close" class="formbutton" style="width:100px"/> 
           </td>
        </tr>
    </table>
    </fieldset>
    </div>
</body>
</html>

// Transport Detail Information
function fnc_transport(save_perm,edit_perm,delete_perm,approve_perm)
{
	var cbo_company_name=escape(document.getElementById('cbo_company_name').value);
	var cbo_company_location= escape(document.getElementById('cbo_company_location').value);
	var txt_vehicle_no= escape(document.getElementById('txt_vehicle_no').value);
	var txt_route= escape(document.getElementById('txt_route').value);
	var txt_stoppage= escape(document.getElementById('txt_stoppage').value);
	var txt_capacity= escape(document.getElementById('txt_capacity').value);
	var txt_booked=escape(document.getElementById('txt_booked').value);
	var txt_remainning= escape(document.getElementById('txt_remainning').value);
	var txt_driver_name= escape(document.getElementById('txt_driver_name').value);
	var txt_license_no= escape(document.getElementById('txt_license_no').value);
	var license_expired_date= escape(document.getElementById('license_expired_date').value);
	var txt_mobile_no= escape(document.getElementById('txt_mobile_no').value);
	var save_up= escape(document.getElementById('save_up').value);
	var emp_code=escape(document.getElementById('emp_code').value);//hidden field
	
	//alert(emp_code);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#cbo_company_name').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#cbo_company_location').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_location').focus();
			$(this).html('Please Select Company Location').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_vehicle_no').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_vehicle_no').focus();
			$(this).html('Please Insert Vehicle No.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	
	else if($('#txt_route').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_route').focus();
			$(this).html('Please Insert Route Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_stoppage').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_stoppage').focus();
			$(this).html('Please Insert Stoppage Place').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_capacity').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_capacity').focus();
			$(this).html('Please Insert Vehicle Capacity').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_driver_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_driver_name').focus();
			$(this).html('Please Insert Driver\'s Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_license_no').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_license_no').focus();
			$(this).html('Please Insert Driving License No.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#license_expired_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#license_expired_date').focus();
			$(this).html('Please Insert Expired Licencse Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else
	{
		nocache = Math.random();
		http.open('GET','resource_allocation/save_resource_allocation.php?action=transport&isupdate='+save_up+
				    
					'&cbo_company_name='+cbo_company_name+
					'&cbo_company_location='+cbo_company_location+
				    '&txt_vehicle_no='+txt_vehicle_no+
					'&txt_route='+txt_route+
					'&txt_stoppage='+txt_stoppage+
					'&txt_capacity='+txt_capacity+
					'&txt_booked='+txt_booked+
				    '&txt_remainning='+txt_remainning+
					'&txt_driver_name='+txt_driver_name+
					'&txt_license_no='+txt_license_no+
					'&license_expired_date='+license_expired_date+
					'&txt_mobile_no='+txt_mobile_no+
					'&emp_code='+emp_code+
					'&nocache='+nocache);
		http.onreadystatechange = fnc_transport_reply_info;
		http.send(null); 
 	}	
	
}

function fnc_transport_reply_info() {
	if(http.readyState == 4) {
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Transport Detail have been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","transport_users","insert","../");
			});
			
	
		showResult_search_transport(document.getElementById("cbo_company_name").value,document.getElementById("txt_vehicle_no").value,"transport_detail","transport_div");
		 document.getElementById('check_all').checked=false;
		document.getElementById('cbo_company_name').value=0;
		document.getElementById('cbo_company_location').value=0;
		document.getElementById('txt_vehicle_no').value="";
		document.getElementById('txt_route').value="";
		document.getElementById('txt_stoppage').value="";
		document.getElementById('txt_capacity').value="";
		document.getElementById('txt_booked').value="";
		document.getElementById('txt_remainning').value="";
		document.getElementById('txt_driver_name').value="";
		document.getElementById('txt_license_no').value="";
		document.getElementById('license_expired_date').value="";
		document.getElementById('txt_mobile_no').value="";
		document.getElementById('save_up').value="";
		document.getElementById('emp_code').value="";
		document.getElementById('emp_code_data').value=" ";	
		
		}
		if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Transport Detail have been updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","transport_users","update","../");
			});
			showResult_search_transport(document.getElementById("cbo_company_name").value,document.getElementById("txt_vehicle_no").value,"transport_detail","transport_div");
		document.getElementById('check_all').checked=false;
		document.getElementById('cbo_company_name').value=0;
		document.getElementById('cbo_company_location').value=0;
		document.getElementById('txt_vehicle_no').value="";
		document.getElementById('txt_route').value="";
		document.getElementById('txt_stoppage').value="";
		document.getElementById('txt_capacity').value="";
		document.getElementById('txt_booked').value="";
		document.getElementById('txt_remainning').value="";
		document.getElementById('txt_driver_name').value="";
		document.getElementById('txt_license_no').value="";
		document.getElementById('license_expired_date').value="";
		document.getElementById('txt_mobile_no').value="";
		document.getElementById('save_up').value="";
		document.getElementById('emp_code').value="";
		document.getElementById('emp_code_data').value=" ";	
		
		}
		
		//reset_form();
		
		
	}
}

