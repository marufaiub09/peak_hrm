<?php
/*######################################
	Completed By
	Name : Md. sohel
	Date : 31-03-12
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
include('../includes/array_function.php');
$ConsumptionTabIndex=-1;
extract($_POST);
if(!$TabIndexNo) { $TabIndexNo=0; }


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}
	
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}
	
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);

	/*<!--while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$division_details[$row['id']][$key] = mysql_real_escape_string( $value );-->*/
		//}
	}
	
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	//$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
	
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	//$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY system_designation ASC";
	$sql = "SELECT DISTINCT level,system_designation FROM lib_designation WHERE is_deleted = 0 ORDER BY system_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_level = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_level[$row['level']] = mysql_real_escape_string( $row['system_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Resource Allocation</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen">
    
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/popup_window.js"></script>
	<script type="text/javascript" src="../js/modal.js"></script>
    
    <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>

	<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
	<script src="../resources/ui.tabs.paging.js" type="text/javascript"></script>
    
    <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
    <script src="includes/functions.js" type="text/javascript"></script>
    
    <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="../includes/tablefilter.js"></script>
    
	<script type="text/javascript">
        $(document).ready(function() {
            $('#example').tabs();
            $('#example').tabs('paging', { cycle: true, follow: true } );
            $('#example').tabs('select',<?php echo $TabIndexNo; ?>);
        });
    </script>
    
    <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>
	<script>
	
	
	function numbersonly(myfield, e, dec)
	{
		var key;
		var keychar;
		
		if (window.event)
		key = window.event.keyCode;
		else if (e)
		key = e.which;
		else
		return true;
		keychar = String.fromCharCode(key);
		// control keys
		if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
		return true;
		// numbers
		else if ((("0123456789").indexOf(keychar) > -1))
		return true;
		else
		return false;
	}	
	
function valide_company()
	{
		if($('#cbo_company_id').val()=="0")
		{
			document.getElementById('cbo_location_id').value="0";
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_name').focus();
				$(this).html('Please Select Company First').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});	
		}	
	}
	
	
function valide_division()
	{
		if($('#cbo_division_id').val()=="0")
		{
			document.getElementById('cbo_department_id').value="0";
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_name').focus();
				$(this).html('Please Select Division First').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});	
		}	
	}
	
function valide_department()
	{
		if($('#cbo_department_id').val()=="0")
		{
			document.getElementById('cbo_section_id').value="0";
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_name').focus();
				$(this).html('Please Select Department First').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});	
		}	
	}
	
function valide_section()
	{
		if($('#cbo_section_id').val()=="0")
		{
			document.getElementById('cbo_subsection_id').value="0";
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_name').focus();
				$(this).html('Please Select Section First').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});	
		}	
	}
	
function change_color(v_id,e_color)
	{
		var tot_row=$('#man_table tbody tr').length;
		if (document.getElementById('search_'+v_id).bgColor=="#33CC00")
		{
			document.getElementById('search_'+v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById('search_'+v_id).bgColor="#33CC00";
		}
		
		var bgcolor='';
		for(var i=1;i<=tot_row;i++)
		{
			if(i!=v_id) 
			{
				if (i%2==0) bgcolor="#E9F3FF"; else bgcolor="#FFFFFF";	
				document.getElementById('search_'+i).bgColor=bgcolor;
			}
		}
		
	}
	
	/*function color_tr_over_stock_qnty(all_prod_id)
	{
		var all_prod_id=all_prod_id.split(",");
		for (var i = 0; i < all_prod_id.length; i++) 
		{
			prod_id = all_prod_id[i];
			$("#details_part").find('tr').each(function()
			{
				var text = $(this).find('td:first').text();
				if(text==prod_id)
				{
					$(this).css("background-color", "#FF0000");
				}
			});
		}
	}*/
	
	
	function hidden_field_reset()
	{
		$('#save_update').val('');
		
		$('#save').removeAttr("disabled","disabled");
		$('#save').attr("class","formbutton");
	}
		
		
	</script>
	
</head>
<body>

<div align="center">
	<div align="center" style="width:900px; position:relative; height:30px; margin-bottom:3px; margin-top:3px">
        <div class="form_caption">
           Budgeted Man Power Setup
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;
            ?>
        </span>
        <div id="messagebox" style="background-color:#FF9999; color:#000000; width:900px" align="center"></div>
	</div>
    
     <div id="examples" align="center" style="width:900px;font-size:11px;">
     
     <form name="man_power_setup" id="man_power_setup" method="post" style="margin-top:25px" > <!--javascript:fnc_manpower_setup(save_perm,edit_perm,delete_perm);-->
	<fieldset style="width:880px ">
	<legend>Budgeted Man Power Setup</legend>
	<!-- Start Form -->
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="100" >Company Name</td>
					<td width="170"> 
                     <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:165px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td width="100">Location</td>
					<td width="170"> 
					<select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:165px" onChange="valide_company();">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
					</td>
				</tr>
				<tr>
					<td>Division</td>
					<td> 
					  <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:165px" >
							<option value="0">-- Select --</option>
							<?php foreach( $division_details AS $key=>$value ) { ?>
						<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
					<?php } ?>
						</select>
					</td>
					<td >Department</td>
					<td>
					<select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:165px" onChange="valide_division();">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
			  </tr>
              <tr>
              		<td>Section</td>
					<td>
					  <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:165px" onChange="valide_department();">
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td>Subsection</td>
					<td> 
					 <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:165px" onChange="valide_section();">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>					
					  </td>
              </tr>
			  <tr>
					<td>Employee Catg. </td>
					<td>
					  <!-- <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:165px">
                         	<option value="">-- Select --</option>
 							<?php //foreach( $employee_category AS $key=>$value ){ ?>
							<option value="<?php //echo $key; ?>"><?php //echo $value; ?></option>
							<?php //} ?>                        
                        </select>-->
                        
                         <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:165px">
                         <option value="">-- Select --</option>
                         	<?
						foreach($employee_category as $key=>$val)
						{
                    ?>
                    		<option value="<? echo $key; ?>"><? echo $val; ?></option>
                    <?
						}
                    ?>   
                         </select>

					</td>
					<td>Desig Level</td>
					<td> 
					<select name="cbo_desig_level" id="cbo_desig_level" class="combo_boxes" style="width:165px">
                         	<option value="0">-- Select --</option>
 							<?php foreach( $designation_level AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>                        
                        </select>
					</td>
              </tr>
              <tr>
                  	<td>Designation</td>
					<td> 
					<select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:165px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
					</select>
					</td>
  					<td>
					No. of Post
					</td>
					<td> 
					<input type="text" name="txt_no_post" id="txt_no_post" class="text_boxes" style="width:155px" onKeyPress="return numbersonly(this,event)">
                    <input type="hidden" name="save_update" id="save_update" />
                    <input type="hidden" name="emp_code" id="emp_code" />
					</td>
			  </tr>	
			</table>
            <br />
            <table>
                <tr>
                    <td colspan="4" align="center" class="button_container" width="100%">
                        <input type="button" value=" Save" name="save" id="save" class="formbutton" onClick="fnc_manpower_setup(save_perm,edit_perm,delete_perm);" style="width:100px"/>&nbsp;
                        <input type="button" value=" Update" name="update" id="update" class="formbutton" onClick="fnc_manpower_setup(save_perm,edit_perm,delete_perm);" style="width:100px"/>&nbsp;
                         <input type="button" value="Delete" name="delete" id="delete" class="formbutton" onClick="fnc_manpower_setup(save_perm,edit_perm,delete_perm,5);" style="width:100px"/>&nbsp;
                         <input type="reset" value="Refresh"  name="add" id="close" class="formbutton" onclick="hidden_field_reset()" style="width:100px"/>
                        <!-- <input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="ot_based_on_populate();"  style="width:150px"/>-->
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right" width="100%">
                    Total Post:
                    </td>
                    <?
					$sql1="select id,sum(no_of_post) as no_of_post from budgeted_manpower_setup where status_active=1 and is_deleted=0";
					$total_result=mysql_query($sql1);
					while( $row = mysql_fetch_assoc( $total_result ) ) 
					{
						$tot=$row['no_of_post'];
					}
					?>
                    <td>
                    <input type="text" name="txt_total_post" id="txt_total_post" class="text_boxes" style="width:100px" value="<? echo $tot; ?>" readonly >
                    </td>
                </tr>
             </table>
	</fieldset>
    </form>
	</div>
     <div style="width:930px; overflow-y:scroll; min-height:50px; max-height:250px;" id="manpower_setup_div">
     <script>
	 showResult_search_transport(document.getElementById("cbo_company_id").value,document.getElementById("cbo_emp_category").value,"manpower_setup_detail","manpower_setup_div");
	
	 </script>
    </div>
</div>
</body>
</html>