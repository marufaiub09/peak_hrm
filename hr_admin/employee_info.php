<?php
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 31-03-12
		
######################################*/


session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

//echo $permission;

$permission=explode('_',$permission);

 
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; 
	else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")";
	else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../includes/common.php');
include("../includes/array_function.php");

//$emp_tab_arr=array(0=>"All Tab",1=>"Basic Info",2=>"Photo",3=>"Address",4=>"Salary Info",5=>"Bank Salary",6=>"Entitlement",7=>"Experience",8=>"Education",9=>"Family Info",10=>"Policy Taging",11=>"Weekend",12=>"Doc Submitted",13=>"Skill Assign");
// Tab Permission
$sql = "SELECT * FROM user_priv_tab WHERE  user_id='".$_SESSION['logic_erp']['user_id']."' and form_id=1 ORDER BY tab_id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$tab_permission_data = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	 $tab_permission_data[$row[tab_id]]['visibility_prv']=$row[visibility_prv];
	 $tab_permission_data[$row[tab_id]]['insert_prv']=$row[insert_prv];
	 $tab_permission_data[$row[tab_id]]['edit_prv']=$row[edit_prv];
	 $tab_permission_data[$row[tab_id]]['delete_prv']=$row[delete_prv];
	 $tab_permission_data[$row[tab_id]]['approve_prv']=$row[approve_prv];
}


//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE status_active=1 and is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE  status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE  status_active=1 and is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE  status_active=1 and is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE  status_active=1 and is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_list
$sql = "SELECT * FROM lib_list_division WHERE  status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_list = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_list[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_list[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../includes/update_status.js" type="text/javascript"></script>
	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../includes/functions.js"></script>
    
	<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script src="../resources/ui.tabs.paging.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/ajaxupload.3.5.js" ></script>
	
    <script type="text/javascript" src="../js/driver.phonetic.js" ></script>
    <script type="text/javascript" src="../js/driver.probhat.js" ></script>
    <script type="text/javascript" src="../js/engine.js" ></script>
    
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
    <script type="text/javascript" src="../js/modal.js"></script>
    <script type="text/javascript" src="../js/webcam.js"></script>
    
	<script language="JavaScript">
		webcam.set_api_url( 'webcam_upload.php' );
		webcam.set_quality( 90 ); // JPEG quality (1 - 100)
		webcam.set_shutter_sound( true ); // play shutter click sound
	</script> 
       
	<script type="text/javascript">
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		var is_data_level_secured="";
		var joining_date_gross_salary_condition = <? echo $joining_date_gross_salary_condition; ?>;
				
		
		$(document).ready(function(){
			$(".bangla").bnKb({
				'switchkey': {"webkit":"k","mozilla":"y","safari":"k","chrome":"k","msie":"y"},
				'driver': phonetic
			});
		});
		var user_priv_tab=new Array();
		<?
		for($j=1; $j<15; $j++)
		{
			echo "user_priv_tab[$j] = new Array();\n";
			echo "user_priv_tab[$j]['insert_prv'] = '".$tab_permission_data[$j]['insert_prv']."';\n";
			echo "user_priv_tab[$j]['edit_prv'] = '".$tab_permission_data[$j]['edit_prv']."';\n";
		}
		?>
		var company_details = location_details = division_details = department_details = section_details = subsection_details = new Array();
		
		<?php
		foreach( $company_details AS $company_id => $company ) {
			echo "company_details[$company_id] = new Array();\n";
			foreach( $company AS $key => $value ) {
				if( $key == 'id' || $key == 'country' || $key == 'group_id' ) echo "company_details[$company_id]['$key'] = $value;\n";
				else echo "company_details[$company_id]['$key'] = '$value';\n";
			}
		}
		foreach( $location_details AS $location_id => $location ) {
			echo "location_details[$location_id] = new Array();\n";
			foreach( $location AS $key => $value ) {
				if( $key == 'id' || $key == 'country' || $key == 'company_id' ) echo "location_details[$location_id]['$key'] = $value;\n";
				else echo "location_details[$location_id]['$key'] = '$value';\n";
			}
		}
		foreach( $division_details AS $division_id => $division ) {
			echo "division_details[$division_id] = new Array();\n";
			foreach( $division AS $key => $value ) {
				if( $key == 'id' || $key == 'location_id' ) echo "division_details[$division_id]['$key'] = $value;\n";
				else echo "division_details[$division_id]['$key'] = '$value';\n";
			}
		}
		foreach( $department_details AS $department_id => $department ) {
			echo "department_details[$department_id] = new Array();\n";
			foreach( $department AS $key => $value ) {
				if( $key == 'id' || $key == 'division_id' ) echo "department_details[$department_id]['$key'] = $value;\n";
				else echo "department_details[$department_id]['$key'] = '$value';\n";
			}
		}
		foreach( $section_details AS $section_id => $section ) {
			echo "section_details[$section_id] = new Array();\n";
			foreach( $section AS $key => $value ) {
				if( $key == 'id' || $key == 'department_id' ) echo "section_details[$section_id]['$key'] = $value;\n";
				else echo "section_details[$section_id]['$key'] = '$value';\n";
			}
		}
		foreach( $subsection_details AS $subsection_id => $subsection ) {
			echo "subsection_details[$subsection_id] = new Array();\n";
			foreach( $subsection AS $key => $value ) {
				if( $key == 'id' || $key == 'section_id' ) echo "subsection_details[$subsection_id]['$key'] = $value;\n";
				else echo "subsection_details[$subsection_id]['$key'] = '$value';\n";
			}
		}
		?>		

		function openpage_searchemp(page_link,title)
		{		
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("txt_selected_emp").value; //Access form field with id="emailfield"				
				//var ref_name=this.contentDoc.getElementById("txt_ref_name").value;			
				//emp_code=theemail.value;
				$.ajax({
					type: "POST",
					url: "includes/get_data_update.php",
					data: 'link=basic&emp_code=' + emp_code,
					success: function(data) {				
					eval(data);
					$('#new').css( "visibility", "visible" );
					enable_tabs();	
					}
				});
				$('#employee_info').tabs( "option", "selected", 0 );
				if(joining_date_gross_salary_condition==0)
				{
				if(is_data_level_secured==1){ $('#joining_date').attr("disabled","disabled")}
				}
				//document.location.href = "#employee_info";
				/*
				var tabs = new Array( 'photo_link','address_link','salary_link','salary_bank_link','entitlement_link','experience_link','education_link','family_link','policy_taging_link' );
				for( var i = 0; i < tabs.length; i++ ) 
				{
					var link = $('#' + tabs[i]).attr('href').split('?');
					$('#' + tabs[i]).attr( 'href', link[0] + '?emp_code=' + emp_code );
				}
				*/			
			}
		}
		
		function on_key_down( from, evt  )
		{
			if (evt!=13) { var unicode=evt.keyCode? evt.keyCode : evt.charCode } else {unicode=13;}
			//alert(unicode)
			if(unicode!=40) return;

			if( from==1 )// ID CARD
			{
				var id_card=$('#id_card_no').val();
				var emp_code=$('#emp_code').val();
				
				$.ajax({
					type: "POST",
					url: "includes/get_data_update.php",
					data: 'link=basic&emp_code=' + emp_code+'&id_card_no=' + id_card,
					success: function(data) {				
					eval(data);
					$('#new').css( "visibility", "visible" );
					enable_tabs();	
					}
				});
				//new
				/*
				$.ajax({
						type: "POST",
						url: "includes/get_data_update.php",
						data: 'link=photo&emp_code=' + emp_code+'&id_card_no=' + id_card,
						success: function(data) {				
						eval(data);
						
						}
					});
				*/
			}
			$('#employee_info').tabs( "option", "selected", 0 );
			//if(is_data_level_secured==1){ $('#joining_date').attr("disabled","disabled")}
			if(joining_date_gross_salary_condition==0)
			{
				if(is_data_level_secured==1)
				{
					if($("#joining_date").val()!='')
					{
						$('#joining_date').attr("disabled","disabled")
					}
				}
			}
			
			if(joining_date_gross_salary_condition==0)
			{
				if(is_data_level_secured==1)
				{ 
					if($("#gross_salary").val()!='')
					{
						$('#gross_salary').attr("disabled","disabled")
					}
				}
			}
		}
		
		$(document).ready(function() {
			$('#employee_info').tabs({
				cache:true,
				selectOnAdd:true,
				disabled: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13]
			});
			
			function init() {
				$('#employee_info').tabs('paging', { cycle:true, follow:true, followOnSelect:true, selectOnAdd:true });
			}
			init();
			
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
			
			$('#criteria1').change(function() {
				if( $(this).val() == 0 )		var opt = populate_cost_center2( 'company', 0 );
				else if( $(this).val() == 1 )	var opt = populate_cost_center2( 'location', 0 );
				else if( $(this).val() == 2 )	var opt = populate_cost_center2( 'division', 0 );
				else if( $(this).val() == 3 )	var opt = populate_cost_center2( 'department', 0 );
				else if( $(this).val() == 4 )	var opt = populate_cost_center2( 'section', 0 );
				else if( $(this).val() == 5 )	var opt = populate_cost_center2( 'designation', 0 );
			});
		});
		
		function calculate_age( field ) 
		{
			var dob = $('#fdob_'+field).val();
			//alert (dob);
			var dates = dob.split( '-' );
			var start = new Date(dates[2] + '-' + dates[1] + '-' + dates[0]);
			
			var end = new Date();
			var diff = new Date(end - start);
			
			var years = Math.floor( diff/1000/60/60/24/365 );
			var months = Math.floor( ( diff/1000/60/60/24/365 - years ) * 12 );
			var days = Math.floor( ( ( diff/1000/60/60/24/365 - years ) * 12 - months ) * 30 );
			//return years + 'y ' + months + 'm ' + days + 'd';
			
			$('#fage_'+field).val(years);
		}
		
		function search_employees() 
		{
			if( $('#criteria11').val() == 0 && $('#criteria22').val() == '' ) {
				alert('Please provide at least one search criteria.');
				return false;
			}
			else 
			{
				$('#data_panel').html('<img src="../resources/images/loading.gif" />');
				$.ajax({
					type: "POST",
					url: "employee_info_data.php",
					data: 'criteria1=' + $('#criteria1').val() + '&criteria11=' + $('#criteria11').val()
					+ '&criteria2=' + $('#criteria2').val() + '&criteria22=' + $('#criteria22').val(),
					success: function(data) {
						$('#data_panel').html( data );
						$('#data_panel table').dataTable({
							"bJQueryUI": true,
							"bPaginate": false,
							"aoColumns": [ null, { "sType": "html" }, null, null, null, null, null, null, null, null,null ],
							"aaSorting": [[ 0, "asc" ]],
							"oLanguage": { "sSearch": "Search all columns:" },
							"sScrollX": "100%",
							"sScrollY": "250px",
							"sScrollXInner": "150%",
							"bScrollCollapse": true
						});
					}
				});
			}
		}
		<? 
		echo "is_data_level_secured='".$_SESSION['logic_erp']["data_level_secured"]."';";
		//if($_SESSION['logic_erp']["data_level_secured"]==1) $tab_permission_data[4]['visibility_prv']=2;
		?>
		//alert(is_data_level_secured);
		function enable_tabs() 
		{
			//alert(is_data_level_secured);
			$('#employee_info').tabs('enable', 1);
			$('#employee_info').tabs('enable', 2);
			//$('#employee_info').tabs('enable', 3);
			if(is_data_level_secured!=1)
			{
				$('#employee_info').tabs('enable', 3);
				$('#employee_info').tabs('enable', 4);
			}
			else if( is_data_level_secured==1 && $('#status_salary').val()==1)
			{
				$('#employee_info').tabs('enable', 3);
				$('#employee_info').tabs('enable', 4);
				
				$('#status_attendance').removeAttr('disabled');
				$('#status_salary').removeAttr('disabled');
			}
			else //if( is_data_level_secured==1 && $('#txt_status_salary').val()==0)
			{
				$('#employee_info').tabs('disable', 3);
				$('#employee_info').tabs('disable', 4);
				
				$('#status_attendance').attr("disabled","disabled");
				$('#status_salary').attr("disabled","disabled");
			}
			
			if(joining_date_gross_salary_condition==0)
			{
				if(is_data_level_secured==1)
				{
					if($("#joining_date").val()!='')
					{
					 	$('#joining_date').attr("disabled","disabled")
					}
				}
			}
			
			if(joining_date_gross_salary_condition==0)
			{
				if(is_data_level_secured==1)
				{ 
					if($("#gross_salary").val()!='')
					{
						$('#gross_salary').attr("disabled","disabled")
					}
				}
			}
			
			/*
			if(is_data_level_secured==0 && $('#txt_status_salary').val()==1)
			{
				$('#employee_info').tabs('disable', 3);
				$('#employee_info').tabs('disable', 4);
			}
			*/
			$('#employee_info').tabs('enable', 5);
			$('#employee_info').tabs('enable', 6);
			$('#employee_info').tabs('enable', 7);
			$('#employee_info').tabs('enable', 8);
			$('#employee_info').tabs('enable', 9);
			$('#employee_info').tabs('enable', 10);
			$('#employee_info').tabs('enable', 11);
			$('#employee_info').tabs('enable', 12);
			$('#employee_info').tabs('enable', 13);
		}
		
		function populate_cost_center( child, parent_id, selected_id ) 
		{
			//alert(selected_id);
			selected_id = ( selected_id == undefined ) ? 0 : selected_id;
			$.ajax({
				type: "POST",
				url: "employee_info_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html );
				}
			});
		}
		
		function populate_cost_center2( child, parent_id ) 
		{
			$.ajax({
				type: "POST",
				url: "employee_info_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id,
				success: function( html ) {
					$('#criteria11').html( html );
				}
			});
		}
		
		function show_emp_photo(emp_code) 
		{	
			$.ajax({
					type: "POST",
					url: "includes/get_data_update.php",
					data: 'link=photo',
					success: function(data) {					
					$('#files').html( data );
					}
				});
		}
		
		function show_emp_address() 
		{		
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=address',
				success: function(data) {				
				eval(data);
				}
			});			
		}
		
		function show_emp_salary() 
		{
			var designation_id=$("#designation_id").val();
 			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php?&designation_id="+designation_id,
				data: 'link=salary',
				success: function(data) {				
				eval(data);
				//save_sal_grade();
				}
			});
			 
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=salary_breakdown',
				success: function(data) {				
				eval(data);
				head_counter=$("#hidden_head_count").val();				
				for( var i = 1; i <= head_counter; i++ ) change_type( i );
					calculate_total();
					if(joining_date_gross_salary_condition==0)
					{
						if(is_data_level_secured==1)
						{ 
							if($("#gross_salary").val()!='')
							{
								$('#gross_salary').attr("disabled","disabled")
							}
						}
					}					
				}
			});
		}
		
		function calculate_list_total( frm )
		{
			if(frm==1)
			{
				$('#bank_id').val(0);
				$('#branch_name').val('');
				$('#account_no').val('');
				$('#salary_amount').val('');
				$('#show_tax').val(0);
				$('#sequence').val('');
				$('#update_id').val('');
				$('#prev_tax_val').val('');
				return;
			}
			var total=0;
			var col=$('#salary_bank_list_view tr').length;// salary_bank_list_view
			//alert(col)
			for(var i=1; i<col; i++ )
			{
				 
				total =total*1+( $('#salary_break'+i).html()*1);  
			}
			$('#update_total').val(total);
		}
		// salary_bank
		function show_emp_salary_bank( bn ) 
		{
			if(!bn) var bn=1;
			if(!$('#gross_salary').val()) eval( $('#salary_link').click() );
			else if(($('#gross_salary').val()*1)<1) eval( $('#salary_link').click() );

			showResult_multi($('#emp_code').val(),'','update_list_view','tbl_list_view');
			/*
			$('#bank_id').val(0);
			$('#branch_name').val('');
			$('#account_no').val('');
			$('#salary_amount').val('');
			$('#show_tax').val(0);
			$('#sequence').val('');
			*/
			$('#gross_sal').val($('#gross_salary').val());
			$('#total_bank_salary').val($('#bank_total_last').val());
			$('#update_total').val($('#bank_total_last').val());
			/*
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=salary_bank',
				success: function(data) {
					eval(data);	
					//$('#tbl_list_view').html(data);
					 
					if(bn!=0) calculate_list_total( 0 )	;
				}
			});
			*/
		}
		
		function emp_salary_bank_load()
		{
			showResult_multi($('#emp_code').val(),'','update_list_view','tbl_list_view');
			
			$('#bank_id').val(0);
			$('#branch_name').val('');
			$('#account_no').val('');
			$('#salary_amount').val('');
			$('#show_tax').val(0);
			$('#sequence').val('');
			
			$('#bank_total_last').val($('#db_total_bank').val())
			$('#gross_sal').val($('#gross_salary').val());
			$('#total_bank_salary').val($('#db_total_bank').val());
			$('#update_total').val($('#db_total_bank').val());
		}
		
		function show_emp_entitlement() 
		{
			$.ajax({ 
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=entitlement',
				success: function(data) {
				reset_entitlement_form();
				eval(data);					
				}
			});
		}
		
		function show_emp_experience() 
		{		
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=experience',
				success: function(data) {
					reset_experience_form();	
					eval(data);				
				}
			});
		}
		
		function show_emp_education() 
		{								
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=education',
				success: function(data) {
					reset_education_form();
					eval(data);					
				}
			});
		}
		
		function show_emp_family() 
		{							
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=family',
				success: function(data) {
					reset_family_form();
					eval(data);					
				}
			});
		}
		
		function show_policy_taging() 
		{			
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=policy_taging',
				success: function(data) {				
				eval(data);
				}
				});			
		}
	
		function show_weekend() 
		{			
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=weekend',
				success: function(data) {				
				eval(data);
				}
				});			
		}
		
		function fn_remove_picture(image_link)
		{
			//alert(image_link);
			if(confirm('Are You sure to Delete Image?'))
			{
				var split_image=image_link.split('_');
				//alert(split_image);
				var emp_code=split_image[1];
				$.ajax({type: "GET",
				url: "includes/save_update_hr_admin.php?action=image_remove&emp_code="+emp_code+'&image_link='+image_link,
				success: function(msg){		
						$('#messagebox').fadeTo( 200, 0.1, function() {						
						$(this).html(msg).addClass('messagebox').fadeTo(900,1);
						show_emp_photo(emp_code);
						});
					}
				});
			}
		}
	
		function fn_branch_name(val)
		{
			 $.ajax({
				type: "GET",
				url: "includes/get_data_update.php",
				data: "type=branch_name&getClientId="+val,
				success: function(msg){
					$('#branch_name').val(msg);
				}
			 });
		}
	
		function show_submitted_docs() 
		{
			/*load_drop_down(designation_id,'submit_docs','td_id');*/
			reset_form('document_submit_form');
			$('#file_type').attr('disabled','disabled');
			$('.image_uploader').attr('disabled','disabled');
			
			$.ajax({
			type: "POST",
			url: "includes/get_data_update.php",
			data: 'link=submit_docs&designation_id=' + designation_id,
			success: function(data) {
					//alert(data);	
					$('#td_id').html(data);			
				//eval(data);
				}
			});			
		}
		
		function show_submitted_docs_office() 
		{
			//alert("su..re");return;
			reset_form('document_submit_form_office');
			$('#file_types').attr('disabled','disabled');
			$('.image_uploader').attr('disabled','disabled');
			
			$.ajax({
			type: "POST",
			url: "includes/get_data_update.php",
			data: 'link=submit_docs_office&designation_id=' + designation_id,
			success: function(data) {
					//alert(data);	
					$('#td_ids').html(data);			
				//eval(data);
				}
			});
		}
	
		function show_emp_skill() 
		{
			$.ajax({
			type: "POST",
			url: "includes/get_data_update.php",
			data: 'link=skill',
			success: function(data) {
				reset_skill_form();			
				eval(data);
				}
			});			
		}
	
		function showResult_multi(str,str2,type,div)
		{
			//alert(str);	
			/*
			if (str.length==0)
			  {
			  document.getElementById(div).innerHTML="";
			  document.getElementById(div).style.border="0px";
			  return;
			  }*/
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById(div).innerHTML=xmlhttp.responseText;
					document.getElementById(div).style.border="1px solid #A5ACB2";
				}
			}
			 //alert(str);
			xmlhttp.open("GET","includes/list_view.php?search_string="+str+"&search_string2="+str2+"&type="+type,true);
			xmlhttp.send();
		}	
	
		$(document).ready(function() {
			$('#cancel').click(function() {		//#new
				window.location.reload(true);			
			});
		});

		//Numeric Value allow field script
		function numbersonly(myfield, e, dec)
		{
			var key;
			var keychar;
			
			if (window.event)
			key = window.event.keyCode;
			else if (e)
			key = e.which;
			else
			return true;
			keychar = String.fromCharCode(key);
			
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			
			// numbers
			else if ((("0123456789.,-").indexOf(keychar) > -1))
			return true;
			else
			return false;
		}
		//$(".bangla").css('background-color', '#E1F5A9');
	
		function openpage_search_idcard(page_link,title)
		{		
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("txt_selected_emp").value; //Access form field with id="emailfield"	
				
				
				$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=basic&emp_code=' + emp_code,
				success: function(data) {				
				eval(data);
				$('#new').css( "visibility", "visible" );
				enable_tabs();	
				}
			});
			$('#employee_info').tabs( "option", "selected", 0 );
			
			if(joining_date_gross_salary_condition==0)
			{
			if(is_data_level_secured==1){ $('#joining_date').attr("disabled","disabled")}
			}
			//document.location.href = "#employee_info";
			
			/*var tabs = new Array( 'photo_link','address_link','salary_link','salary_bank_link','entitlement_link','experience_link','education_link','family_link','policy_taging_link' );
			for( var i = 0; i < tabs.length; i++ ) 
				{
					var link = $('#' + tabs[i]).attr('href').split('?');
					$('#' + tabs[i]).attr( 'href', link[0] + '?emp_code=' + emp_code );
				}*/	
			}
		}
		
		function openpage_search_reference(page_link,title)
		{		
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("txt_selected_emp").value; //Access form field with id="emailfield"				
				//var ref_name=this.contentDoc.getElementById("txt_ref_name").value;			
				//emp_code=theemail.value;
				$('#emp_ref_code').val(emp_code);
				$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=references&emp_code=' + emp_code,
				success: function(data) {				
				eval(data);
				}
			});
			
			}
		}
		
		function openpage_functional_superior(page_link,title)
		{		
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("txt_selected_emp").value; //Access form field with id="emailfield"				
				//var ref_name=this.contentDoc.getElementById("txt_ref_name").value;			
				//emp_code=theemail.value;
				$('#functional_superior').val(emp_code);
				$.ajax({
				type: "POST",
				url: "includes/get_data_update.php",
				data: 'link=functional_superior&emp_code=' + emp_code,
				success: function(data) {				
				eval(data);
				}
			});
			
			}
		}		
		
		function openmypage_fixed_roster(str,type)
		{
			var str2=$('#emp_code').val();
			
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					//alert (xmlhttp.responseText);
					var split_val=xmlhttp.responseText.split('_');
					if(split_val[0]==1)
					{
						alert ('Change OT Policy if Shift Change');
						$('#overtime_policy').val('');
						$('#duty_roster_policy').val('');
						$("#duty_roster").prop("checked", false);
					}
					
					if(split_val[0]==2)
					{
						var r=confirm('System Will Delete Roster Process...... \n If OK, Date Field Will be Available Beside Shift Policy');
						if (r==true)
						{
							$('#txt_date').removeAttr('style');
							$('#overtime_policy').val('');
							$('#duty_roster_policy').val('');
							$("#duty_roster").prop("checked", false);
						}
						else
						{
							$('#txt_date').attr('style="display:none"');
							$('#shift_policy').val(split_val[1]);
						}
					}
					if(split_val[0]==3)
					{
						alert ('Change OT Policy if Shift Change');
						$('#overtime_policy').val('');
						$('#duty_roster_policy').val('');
						$("#duty_roster").prop("checked", true);
					}
				}
			}
			xmlhttp.open("GET","includes/get_data_update.php?search_string="+str+"&search_string2="+str2+"&type="+type,true);
			xmlhttp.send();
		}

		//new add ekram
		function openpage_last_increment(title)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'includes/last_increment_pop_up.php?emp_code='+$('#emp_code').val(), title, 'width=900px,height=350px,center=1,resize=0,scrolling=0','../');		
		}    
   
    </script>
	<style type='text/css'>
		#filter_panel input[type="text"] { width:140px; }
		#filter_panel select { width:151px; }
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
		.formbutton { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
		.numbers { text-align:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; position:relative; height:50px; margin-bottom:5px; margin-top:5px">
    	<div class="form_caption" style="width:100%;">Employee Information</div>       
    	<div><span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update; ?></span></div>
        <div style="height:19px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; border-radius:5px; width:1200px;" align="center"></div></div>
        <div id="employee_info" style="width:1200px;" align="center">
            <ul class="tabs">
                <? if( $tab_permission_data[1]['visibility_prv']!=2)
                { ?>
                <li><a							href="#basic_info">Basic Info</a></li>
                <? } ?>
                <? if( $tab_permission_data[2]['visibility_prv']!=2)
                { ?>
                <li><a id="photo_link"			href="employee_info_tabs/photo.php"			onclick="show_emp_photo()">Photo</a></li>
                <? } if( $tab_permission_data[3]['visibility_prv']!=2)
                { ?>
                <li><a id="address_link"		href="employee_info_tabs/address.php"		onclick="show_emp_address()">Address</a></li>
                <? }  if( $tab_permission_data[4]['visibility_prv']!=2)
                { ?>
                <li><a id="salary_link"			href="employee_info_tabs/salary_info.php"	onclick="show_emp_salary()">Salary Info</a></li>
                <? } if( $tab_permission_data[5]['visibility_prv']!=2)
                { ?>
                <li><a id="salary_bank_link"	href="employee_info_tabs/salary_bank.php"	onclick="show_emp_salary_bank()">Bank Salary</a></li>
                <? } if( $tab_permission_data[6]['visibility_prv']!=2)
                { ?>
                <li><a id="entitlement_link"	href="employee_info_tabs/entitlement.php"	onclick="show_emp_entitlement()">Entitlement</a></li><!-- show_emp_entitlement(); this function called in entitlement.php page -->
                <? } if( $tab_permission_data[7]['visibility_prv']!=2)
                { ?>
                <li><a id="experience_link"		href="employee_info_tabs/experience.php"	onclick="show_emp_experience()">Experience</a></li><!-- show_emp_experience(); this function called in experience.php page -->
                <? } if( $tab_permission_data[8]['visibility_prv']!=2)
                { ?>
                <li><a id="education_link"		href="employee_info_tabs/education.php"		onclick="show_emp_education()">Education</a></li><!-- show_emp_education(); this function called in education.php page -->
                <? }  if( $tab_permission_data[9]['visibility_prv']!=2)
                { ?>
                <li><a id="family_link"			href="employee_info_tabs/family_info.php"	onclick="show_emp_family()">Family Info</a></li><!-- show_emp_family(); this function called in family_info.php page -->
                <? }  if( $tab_permission_data[10]['visibility_prv']!=2)
                { ?>
                <li><a id="policy_taging_link"	href="employee_info_tabs/policy_taging.php"	onclick="show_policy_taging()">Policy Taging</a></li><!--  -->
               <? }  if( $tab_permission_data[11]['visibility_prv']!=2)
                { ?>
                <li><a id="weekend_link"		href="employee_info_tabs/weekend.php"		onclick="show_weekend()">Weekend</a></li>
                 <!--<li><a id="attch_docs"			href="#"		onclick="attch_documents()">Attach Docs</a></li>-->
                <? }  if( $tab_permission_data[12]['visibility_prv']!=2)
                { ?>
                <li><a id="attch_docs"			href="employee_info_tabs/document_submit_form.php"		onclick="show_submitted_docs()">Doc Employee</a></li>
                <? } if( $tab_permission_data[13]['visibility_prv']!=2)
                { ?>
                <li><a id="attch_docs_office"		href="employee_info_tabs/document_submit_form_office.php"		onclick="show_submitted_docs_office()">Doc Office</a></li>
                <? } if( $tab_permission_data[14]['visibility_prv']!=2)
                { ?>
                <li><a id="skill_assign"		href="employee_info_tabs/skill.php"		onclick="show_emp_skill()">Skill Assign</a></li>
                <? } ?>
            </ul>
            <div id="basic_info" align="center">
                <?php include_once( "employee_info_tabs/emp_basic.php" ); ?>
            </div>
        </div>
        <!--
        <div id="filter_panel" class="ui-widget-content ui-corner-all" style="width:99%; padding:8px; margin-top:10px;">
            <table width="100%" cellspacing="2" cellpadding="0" border="0">
                <tr>
                    <td>Search criteria 1:</td>
                    <td>
                        <select name="criteria1" id="criteria1" class="combo_boxes">
                            <option value="0">by Company</option>
                            <option value="1">by Location</option>
                            <option value="2">by Division</option>
                            <option value="3">by Department</option>
                            <option value="4">by Section</option>
                            <option value="5">by Subsection</option>
                        </select>
                        <select name="criteria11" id="criteria11" class="combo_boxes">
                            <option value="0">-- Select --</option>
                            <?php /*?><?php foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?><?php */?>
                            <option value="<?php /*?><?php echo $company['id']; ?><?php */?>"><?php /*?><?php echo $company['company_name']; ?><?php */?></option>
                            <?php /*?><?php } } ?><?php */?>
                        </select>
                    </td>
                    <td>Search criteria 2:</td>
                    <td>
                        <select name="criteria2" id="criteria2" class="combo_boxes">
                            <option value="0">by Employee Code</option>
                            <option value="1">by Employee Name</option>
                            <option value="2">by Employee Designation</option>
                            <option value="3">by Employee National ID</option>
                            <option value="4">by Employee ID Card No.</option>
                            <option value="5">by Employee Passport No.</option>
                            <option value="6">by Employee Punch Card No.</option>
                        </select>
                        <input type="text" name="criteria22" id="criteria22" class="text_boxes" value="" />
                    </td>
                    <td><input type="button" name="search" id="search" class="formbutton" value="Search" onClick="search_employees()" /></td>
                </tr>
            </table>
        </div>
        -->
    </div>
	<div id="data_panel" class="demo_jui" align="center" style="width:1150px; margin-top:10px;"></div>
</body>
</html>