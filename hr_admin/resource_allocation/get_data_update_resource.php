<?php
session_start();
date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');
/* Replace the data in these two lines with data for your db connection */
 $type=$_GET["type"];
 $id=$_GET['getClientId'];
 
 //Designation array
	$sql = "SELECT id,custom_designation FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT id,company_name FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT id,department_name FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT id,division_name FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT id,location_name FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
	$sql = "SELECT id,section_name FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT id,subsection_name FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
 
if(isset($id))
{  
	
	
	if ($type==1) // Employee for Resource Allocation
	{
		
 	$res = mysql_query("SELECT id,emp_code,company_id,location_id,division_id,department_id,section_id,subsection_id,designation_id, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE emp_code = '".$_GET['getClientId']."' and is_deleted=0 and status_active=1")  or die(mysql_error());				
 		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
			echo "formObj.txt_designation.value = '".mysql_real_escape_string($designation_chart[$inf["designation_id"]])."';\n";    
			echo "formObj.txt_company.value = '".mysql_real_escape_string($company_details[$inf["company_id"]])."';\n"; 
			echo "formObj.txt_division.value = '".mysql_real_escape_string($division_details[$inf["division_id"]])."';\n"; 
			echo "formObj.txt_department.value = '".mysql_real_escape_string($department_details[$inf["department_id"]])."';\n"; 
			echo "formObj.txt_section.value = '".$section_details[$inf["section_id"]]."';\n"; 
			echo "formObj.txt_sub_section.value = '".$subsection_details[$inf["subsection_id"]]."';\n";  
			//echo "formObj.save_up_po.value = '".$inf["id"]."';\n"; 
		} 
	}
	
	
else if($type=="issue_item_update")
	{
	    $sql="select * from hrm_resource_issue_item_details where id='$id' and status_active=1 and is_deleted=0";
		//echo $sql;die;
		$result=mysql_query($sql) or die(mysql_error());
		
		if($inf = mysql_fetch_array($result))
		{
			$txt_issue_date=convert_to_mysql_date($inf['issue_date']);
			 
			echo "formObj.txt_issue_date.value = '".$txt_issue_date."';\n";
			echo "document.getElementById('resource_category').value ='".mysql_real_escape_string($inf['resource_category'])."';\n"; 
			echo "formObj.txt_item.value = '".mysql_real_escape_string($inf['issue_item_name'])."';\n";    
			echo "formObj.txt_rate.value = '".mysql_real_escape_string($inf['issue_item_rate'])."';\n";    
			echo "formObj.txt_qnty_issued.value = '".mysql_real_escape_string($inf['issue_item_quantity'])."';\n"; 
			echo "formObj.txt_amount.value = '".mysql_real_escape_string($inf['amount'])."';\n"; 
			echo "formObj.save_up.value = '".$inf['id']."';\n"; 
		} 
		
	}
	
	
	
	
	else if($type=="return_item_update")
	{
		$sql="select * from hrm_resource_return_item_detail where id='$id' and status_active=1 and is_deleted=0";
		$result=mysql_query($sql) or die(mysql_error());
		
		if($inf = mysql_fetch_array($result))
		{
			$return_date=convert_to_mysql_date($inf['return_date']);
			$issue_qnty=return_field_value("sum(issue_item_quantity)","hrm_resource_issue_item_details","emp_code='$inf[emp_code]' and issue_item_name='$inf[return_item_name]' and status_active=1 and is_deleted=0");

			echo "formObj.txt_returned_date.value = '".$return_date."';\n"; 
			echo "document.getElementById('return_resource_category').value ='".mysql_real_escape_string($inf['resource_category'])."';\n";  
			 	 	
			echo "return_item_drop_down('$inf[emp_code]'+'_'+$inf[resource_category]+'_'+'$inf[return_item_name]','text_item','item_name1');\n";
			 
			echo "formObj.txt_item_return = '".mysql_real_escape_string($inf['return_item_name'])."';\n";    
			echo "formObj.txt_quty_return.value = '".mysql_real_escape_string($inf['return_item_quantity'])."';\n"; 
			echo "formObj.txt_balance_qnty.value = '".mysql_real_escape_string($inf['balance_qnty'])."';\n";
			echo "formObj.txt_balance_amount.value = '".mysql_real_escape_string($inf['balance_amount'])."';\n"; 
			echo "formObj.qnty.value = '".$issue_qnty."';\n"; 
			echo "formObj.save_up0.value = '".$inf['id']."';\n";
		} 
		
	}
	
	
	
	else if($type=="money_recovery_update")
	{
		$sql="select * from hrm_resource_money_recover where id='$id' and status_active=1 and is_deleted=0";
		$result=mysql_query($sql) or die(mysql_error());
		
		if($inf = mysql_fetch_array($result))
		{
			$recovered_date=convert_to_mysql_date($inf['recovery_date']);
			 
			echo "formObj.recovered_date.value = '".$recovered_date."';\n";    
			echo "formObj.txt_net_recovered.value = '".mysql_real_escape_string($inf['recovery_amount'])."';\n";    
			echo "formObj.cbo_salary_head.value = '".mysql_real_escape_string($inf['salary_head'])."';\n";    
			echo "formObj.cbo_salary_period.value = '".mysql_real_escape_string($inf['salary_period'])."';\n"; 
			echo "formObj.save_up1.value = '".$inf['id']."';\n"; 
		} 
		
	}
	
	else if($type=="hidden_amount")
	{
		$sql="SELECT sum(amount) FROM `hrm_resource_issue_item_details` WHERE `emp_code`='$id' and `status_active`=1 and `is_deleted`=0";
		$result=mysql_query($sql) or die(mysql_error());
			if($inf = mysql_fetch_array($result))
			{
				echo "formObj.total_amount.value = '".mysql_real_escape_string($inf[0])."';\n"; 
			}
			//----------------------------------
	$sql="select distinct(issue_item_name), emp_code from hrm_resource_issue_item_details where emp_code='$id' and status_active=1 and is_deleted=0 ORDER BY issue_item_name";
	$result=mysql_query($sql);
	while ($selectResult=mysql_fetch_assoc($result)){
		
	$sql1="select sum(issue_item_quantity),sum(amount) from hrm_resource_issue_item_details where emp_code='$id'and issue_item_name='".$selectResult['issue_item_name']."'  and status_active=1 and is_deleted=0 ORDER BY issue_item_name";
	$result1=mysql_query($sql1);
	$row=mysql_fetch_array($result1);	

	$sql2="select sum(return_item_quantity),sum(balance_amount),emp_code, return_item_name from hrm_resource_return_item_detail where emp_code='$id' and return_item_name='".$selectResult['issue_item_name']."' and status_active=1 and is_deleted=0";
	$result2=mysql_query($sql2);
	$row2=mysql_fetch_array($result2);
	
	$issue_rate=$row[1]/$row[0];
	$return_amount+=$row2[0]*$issue_rate;

$i++;}
		echo "formObj.total_recovered.value = '".$return_amount."';\n";
			//----------------------------------
		$sql1="SELECT sum(recovery_amount) FROM `hrm_resource_money_recover` WHERE `emp_code`='$id' and `status_active`=1 and `is_deleted`=0";
		$result1=mysql_query($sql1) or die(mysql_error());
			
			if($inf1 = mysql_fetch_array($result1))
			{
				$total_recovered=mysql_real_escape_string($inf1[0])+$return_amount;
				echo "formObj.total_recovered.value = '".$total_recovered."';\n";	
			}
			else
			{
				echo "formObj.total_recovered.value = '".$return_amount."';\n";
			}
		
	}
	
	else if($type=="vehicle_list")
	{
	  $sql="select * from hrm_transport_mst where vehicle_no='$id' and status_active=1 and is_deleted=0";
		$result=mysql_query($sql) or die(mysql_error());
		if($inf = mysql_fetch_array($result))
		{
			$license_expired_date=convert_to_mysql_date($inf['license_expired_date']);
			
		/*	echo "formObj.cbo_company_name.value = '".mysql_real_escape_string($inf['company_id'])."';\n";    
			echo "formObj.cbo_company_location.value = '".mysql_real_escape_string($inf['locaion_id'])."';\n";    
			echo "formObj.txt_vehicle_no.value = '".mysql_real_escape_string($inf['vehicle_no'])."';\n"; 
			echo "formObj.txt_route.value = '".mysql_real_escape_string($inf['route'])."';\n";    
			echo "formObj.txt_stoppage.value = '".mysql_real_escape_string($inf['stoppage'])."';\n";    
			echo "formObj.txt_capacity.value = '".mysql_real_escape_string($inf['vehicle_capacity'])."';\n"; 
			echo "formObj.txt_booked.value = '".mysql_real_escape_string($inf['booked'])."';\n";    
			echo "formObj.txt_remainning.value = '".mysql_real_escape_string($inf['remainning'])."';\n";    
			echo "formObj.txt_driver_name.value = '".mysql_real_escape_string($inf['driver_name'])."';\n";
			echo "formObj.txt_license_no.value = '".mysql_real_escape_string($inf['driving_license_no'])."';\n";
			echo "formObj.license_expired_date.value = '".$license_expired_date."';\n";
			echo "formObj.txt_mobile_no.value = '".mysql_real_escape_string($inf['driver_mobile_no'])."';\n";*/
			
			echo "$('#transport_type').val('".mysql_real_escape_string($inf["transport_type"])."');\n";	
			echo "$('#txt_vehicle_no').val('".mysql_real_escape_string($inf["vehicle_no"])."');\n";	
			echo "$('#txt_route').val('".mysql_real_escape_string($inf["route"])."');\n";	
			echo "$('#txt_stoppage').val('".mysql_real_escape_string($inf["stoppage"])."');\n";	
			echo "$('#txt_capacity').val('".mysql_real_escape_string($inf["vehicle_capacity"])."');\n";
			echo "$('#txt_remainning').val('".mysql_real_escape_string($inf["remainning"])."');\n";		
			echo "$('#txt_booked').val('".mysql_real_escape_string($inf["booked"])."');\n";	
			echo "$('#txt_driver_name').val('".mysql_real_escape_string($inf["driver_name"])."');\n";	
			echo "$('#txt_license_no').val('".mysql_real_escape_string($inf["driving_license_no"])."');\n";	
			echo "$('#license_expired_date').val('".$license_expired_date."');\n";
			echo "$('#txt_mobile_no').val('".mysql_real_escape_string($inf["driver_mobile_no"])."');\n";
			//echo "$('#save_up').val('".mysql_real_escape_string($inf["id"])."');\n";	
		} 
		
		//query for hidden field emp_code_data
		$query="select * from hrm_transport_employee_list where vehicle_no='$id' and status_active=1 and is_deleted=0";
		$result=mysql_query($query) or die(mysql_error());
				if(mysql_num_rows($result)!=0)
				{	
					$emp_code_data="";
					while($row=mysql_fetch_assoc($result))
					{
					if($emp_code_data=="")$emp_code_data=$row['emp_code']; else $emp_code_data.=",".$row['emp_code'];
					}
					echo "formObj.emp_code_data.value = '".$emp_code_data."';\n";
					echo "formObj.emp_code.value = '".$emp_code_data."';\n";
					/*$i=1;
					echo "formObj.save_up.value = '".$i."';\n";*/
				}
				else
				{	
				$ff="";
					echo "formObj.emp_code_data.value = '".$ff."';\n";
					echo "formObj.emp_code.value = '".$ff."';\n";
					echo "formObj.save_up.value = '".$ff."';\n";
				}
	}
	/*else if($type=="vehicle_save_up")
	{
		$ff="";
				echo "formObj.save_up.value = '".$ff."';\n";
	}*/
	
}	


	
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?> 