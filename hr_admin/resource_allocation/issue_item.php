  <script type="text/javascript">
 
 function onlyNum(event)
{
	var k = event.keyCode;
	if(k==0) k=event.which;
	//alert(k);
	
	if((k>=48 && k<=57) || k==8 || k==46)
	{
		return true;
	}
	else 
	{
		return false;
	}
}
						
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}		
						
function calculate()
{
	var rate=document.getElementById('txt_rate').value;	
	var quantity=document.getElementById('txt_qnty_issued').value;
	document.getElementById('txt_amount').value=rate*quantity;
}

  </script>
    <table cellpadding="0" cellspacing="0" width="100%">
			
			<tr>
				<td width="30%" valign="top">
					
					<fieldset>
                     <form name="iss" id="iss">
					<legend> Issue</legend>
					<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >				
						<thead>
							<th align="center" width="145px"><strong>Issue Date</strong></th>
                            <th align="center" width="145px"><strong>Resource Catg.</strong></th>
                            <th align="center" width="145"><strong>Item Description</strong></th>
                            <th align="center" width="145px"><strong>Rate</strong></th>
                            <th align="center" width="145px"><strong>Issue Qnty</strong></th>
                            <th align="center" width="145px"><strong>Amount</strong></th>
                        </thead> 
                          <tr class="general">
							<td>
								<input type="text" name="txt_issue_date" form="item_resource_allocation" id="txt_issue_date" class="text_boxes" style="width:145px" />
									<script type="text/javascript">
                                            $( "#txt_issue_date" ).datepicker({
                                            dateFormat: 'dd-mm-yy',
                                            changeMonth: true,
                                            changeYear: true
                                            });
                                      </script>					
							</td>
							
                            <td>
                               <div id="text_item_category_load"> 
                                <select name="resource_category" id="resource_category" class="combo_boxes" style="width:145px" >							
                                   <option value="0">-- Select --</option>
                                    <?php foreach( $resource_alo_category AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                 </select>
                                   </div>
                   			</td>
                          
                            <td>
								<input type="text" name="txt_item" form="item_resource_allocation" id="txt_item" class="text_boxes" style="width:145px" />						
							</td>
							<td>
								<input type="text" name="txt_rate" form="item_resource_allocation" id="txt_rate" class="text_boxes_numeric" style="width:145px;" onKeyPress=" return numbersonly(this,event)" onkeyup="calculate()" />						
							</td>
							<td>
								<input type="text" name="txt_qnty_issued" form="item_resource_allocation" id="txt_qnty_issued" class="text_boxes_numeric" style="width:145px;"
                                onKeyPress=" return numbersonly(this,event)" onkeyup="calculate()" />						
							</td>
							<td>
								<input type="text" name="txt_amount" form="item_resource_allocation" id="txt_amount" class="text_boxes_numeric" style="width:145px;" readonly="" />						
							</td>
						</tr>
					</table>
                    </form>
					</fieldset>
				</td>
		    </tr>
			
	</table>
    <br />
     <table>
        <tr>
            <td>
            	<input type="hidden" name="save_up" id="save_up" form="item_resource_allocation" />
            </td>
            <td width="100%" align="center" class="button_container">
                <input type="submit" value=" Save" name="save" id="save" class="formbutton" onclick="fnc_resource_allocation(save_perm,edit_perm,delete_perm,approve_perm);"  style="width:100px"/>&nbsp;
                 <input type="reset" value="  Refresh  " form="item_resource_allocation" name="add" id="close" class="formbutton"  style="width:100px"/>
            </td>
        </tr>
      </table>
<div id="list_view" style="width:850px; overflow:auto" align="center">
</div>
	
