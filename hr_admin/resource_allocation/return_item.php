 <script type="text/javascript">
 function onlyNum(event)
						{
							var k = event.keyCode;
							if(k==0) k=event.which;
							//alert(k);
							
							if((k>=48 && k<=57) || k==8 || k==46)
							{
								return true;
							}
							else 
							{
								return false;
							}
						}	
						
	function calculate()
	{
			var rate=document.getElementById('txt_rate').value;	
			var quantity=document.getElementById('txt_qnty_issued').value;
			document.getElementById('txt_amount').value=rate*quantity;
	}
	
	function calculate2()
	{
		
			var return_qnty=escape(document.getElementById('txt_quty_return').value);
			var balance_qnty=escape(document.getElementById('txt_balance_qnty').value);
			var balance_amount=escape(document.getElementById('txt_balance_amount').value);
			
			//hiiden issue item value
			var hid_iss_qnty=escape(document.getElementById('qnty').value);
			var hid_iss_rate=escape(document.getElementById('hi_rate').value);	
			var hid_iss_amount=escape(document.getElementById('hi_amount').value);
			var save_up0=escape(document.getElementById('save_up0').value);
			// hidden return item value
			var hid_re_qnty=escape(document.getElementById('re_qnty').value);	
			var hid_bl_amount=escape(document.getElementById('bl_amount').value);
			
			var return1_qnty=(return_qnty*1)+(hid_re_qnty*1);// total return qnty
			
			var item1=document.getElementById('txt_item_return').value;
			//alert(return1_qnty);
			if(item1=="")
			{
				alert("Select Item Name.");	
				document.getElementById('txt_item_return').focus;
			}
			if(save_up0=="")
			{
					if((return1_qnty*1)>(hid_iss_qnty*1))
					{
						alert("Return Item Exceed issue Item");
						document.getElementById('txt_quty_return').value="";
						document.getElementById('txt_balance_qnty').value="";
						document.getElementById('txt_balance_amount').value="";
						document.getElementById('txt_quty_return').focus();
					}
					else
					{
						
						document.getElementById('txt_balance_qnty').value=(hid_iss_qnty*1)-(return1_qnty*1);
						document.getElementById('txt_balance_amount').value=((hid_iss_qnty*1)-(return1_qnty*1))*(hid_iss_rate*1);
						
					}
					
			}
			else
			{
					if((return_qnty*1)>=(hid_re_qnty*1))	
					{
						if((return_qnty*1)>(hid_iss_qnty*1))
						{
							alert("Return Item Exceed issue Item");
							document.getElementById('txt_quty_return').value="";
							document.getElementById('txt_balance_qnty').value="";
							document.getElementById('txt_balance_amount').value="";
							document.getElementById('txt_quty_return').focus();	
						}
						else
						{
							document.getElementById('txt_balance_qnty').value=(hid_iss_qnty*1)-(return_qnty*1);
							document.getElementById('txt_balance_amount').value=((hid_iss_qnty*1)-(return_qnty*1))*(hid_iss_rate*1);
						}
					}
				
					else
					{
						document.getElementById('txt_balance_qnty').value=(hid_iss_qnty*1)-(return_qnty*1);
						document.getElementById('txt_balance_amount').value=((hid_iss_qnty*1)-(return_qnty*1))*(hid_iss_rate*1);
					}
				
			}
			
			
	}
	
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}		
  </script>
  
    <table cellpadding="0" cellspacing="0" width="100%">
			
			<tr>
				<td width="30%" valign="top">
					<fieldset>
					<legend>Returned</legend>
					<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >				
					  <thead>
						<th align="center" width="145px"><strong>Returned Date </strong></th>
                        <th align="center" width="145px"><strong>Resource Catg. </strong></th>
                        <th align="center" width="145px"><strong>Item Description</strong></th>
                        <th align="center" width="145px"><strong>Qnty. Returned</strong></th>
                        <th align="center" width="145px"><strong>Balance Qnty.</strong></th>
                        <th align="center" width="145px"><strong>Balance Amount</strong></th>
                      </thead>
                      <tr class="general">
						<td>
							<input type="text" form="item_resource_allocation" name="txt_returned_date" id="txt_returned_date" class="text_boxes" style="width:145px; " />
                            			<script type="text/javascript">
                                                    $( "#txt_returned_date" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                           </script>						
						</td>
                        
                        
                        	  <td>
                               <div id="text_return_item_category_load">
                                <select name="return_resource_category" id="return_resource_category" class="combo_boxes" style="width:145px" onchange="return_item_drop_down(document.getElementById('txt_emp_code').value+'_'+this.value,'text_item','item_name1');">							
                                   <option value="0">-- Select --</option>
                                    <?php foreach( $resource_alo_category AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                 </select>
                                 </div>
                   			</td>
							<td>
                            <div id="item_name1"> 
								<select form="item_resource_allocation" name="txt_item_return" id="txt_item_return" class="combo_boxes" style="width:145px">
                                	<option value="0">---Select---</option>
                                </select>	
                            </div>				
							</td>
                            <td>
                            
							<input type="text" form="item_resource_allocation" name="txt_quty_return" id="txt_quty_return" class="text_boxes_numeric" style="width:145px; "  onKeyPress=" return numbersonly(this,event)" onkeyup="calculate2();" />
                           
					  </td>
                      <td>
							<input type="text" form="item_resource_allocation" name="txt_balance_qnty" id="txt_balance_qnty" class="text_boxes_numeric" readonly="" style="width:145px; " />
					  </td>
                      
						<td>
							<input type="text" form="item_resource_allocation" name="txt_balance_amount" id="txt_balance_amount" class="text_boxes_numeric" style="width:145px; " readonly="" />
					  </td>
						</tr>
					
					</table>			  
					</fieldset>
					</td>
				
		    </tr>
			
	</table>
    <br />
	<table>	
    	<tr>
        	<td>				
            <input type="hidden" form="item_resource_allocation" name="qnty" id="qnty" /> 
            <input type="hidden" form="item_resource_allocation" name="hi_rate" id="hi_rate" />	
            <input type="hidden" form="item_resource_allocation" name="hi_amount" id="hi_amount" />
            
            <input type="hidden" form="item_resource_allocation" name="re_qnty" id="re_qnty" />	
            <input type="hidden" form="item_resource_allocation" name="bl_amount" id="bl_amount" />
            
            <input type="hidden" form="item_resource_allocation" name="save_up0" id="save_up0" />    </td>           
	  						
        	<td align="center" class="button_container" width="100%">
                <input type="submit" value=" Save" name="save" id="save" class="formbutton" onclick="fnc_resource_return_item(save_perm,edit_perm,delete_perm,approve_perm);" style="width:100px" />&nbsp;
                <input type="reset" form="item_resource_allocation" value="  Refresh  " name="add" id="close" class="formbutton" style="width:100px" />			
       		</td>
    	</tr>
   </table>
<div id="list_view1" style="width:850px; overflow:auto" align="center">
</div>
	<!-- End Field Set -->
	
	

