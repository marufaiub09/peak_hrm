<?
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date :17/04/2012
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond."xcz";
//--------------------------------------------------------------------------------------------------------------------
date_default_timezone_set('UTC');
include('../includes/common.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>	

	<script src="includes/functions.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
	
    <script src="../resources/jquery-1.6.1.js" type="text/javascript"></script>
    <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>	
   
   
    <link href="../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
    
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
    <script type="text/javascript" src="../js/modal.js"></script>
    
   <script type="text/javascript">
	
	
    //numeric field script
    function numbersonly(myfield, e, dec)
    {
        var key;
        var keychar;
    
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);
    
        // control keys
        if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
        return true;
        
        // numbers
        else if ((("0123456789,").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }	
	
	function openmypage_employee_info(page_link,title)
		{
			// alert(page_link);
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=400px,center=1,resize=0,scrolling=0','')
			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");
				//alert(thee_loc.value+thee_id.value);
				//return_item_drop_down(thee_id.value,"confirmation_letter_emp_info","confirm_div");
				//fnc_confirmation_letter(thee_id.value,"confirmation_letter_emp_info");
				$('#txt_emp_code').val(thee_id.value);
			}
		}	
		
		function new_window()
		{
			var w = window.open("Surprise", "#");
			var d = w.document.open();
			d.write (document.getElementById('data_panel2').innerHTML);
			d.close();
		}	
	
	
</script>
<style type="text/css">

   @page { size 8.5in 11in; margin: 0.5in; }
    div.page { page-break-after: always; background-color:#FFF;}
	</style>

</head>
<body>
    <div class="form_caption" style="width:100%; background-color:#A6CAF0; padding-top:5px; height:25px; border-radius:5px; color:#000;" align="center">Employment</div>
    <div style="height:20px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; border-radius:5px; width:100%;" align="center"></div></div>
    <form id="frm_appmnt_letter" action="" autocomplete="off" method="post">
        <fieldset><!--<legend>Search</legend>-->
            <div align="center">
                <table>
                        <tr>
                            <td colspan="2" align="">&nbsp;</td>
                            <td colspan="2" align=""><input type="text" name="txt_emp_code" id="txt_emp_code" value="" class="text_boxes" size="40" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info('search_emp_info_conf.php','Employee Information'); return false" placeholder="Double Click For Search" /></td>
                            <td><input type="button" name="submit" id="submit" value="Show" class="formbutton" onclick="fn_search_result_employment();" style="width:100px"/>
                            <input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px"/></td>
                            <td></td>					
                        </tr>
                </table>
            </div>
        </fieldset>
        <div id="data_panel1" align="center"></div>
        <div id="data_panel2"></div>
    </form>
</body>
</html>

<!--

if(isset($_POST['submit']))
{
		
	$emp_code = $_POST['txt_emp_code'];
	$emp_code_explode=explode(",",$emp_code);
	for($i=0;$i<count($emp_code_explode);$i++){
			$sql="select *  from hrm_employee  where emp_code='".$emp_code_explode[$i] ."' and status_active=1 and is_deleted=0";
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		while($row=mysql_fetch_array($result)){
			$designation=return_field_value("custom_designation","lib_designation","id=$row[designation_id]");
			$section_id=return_field_value("section_id","hrm_employee_job","emp_code=$row[emp_code]");
			$section=return_field_value("section_name","lib_section","id=$section_id");
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$house_rent = return_field_value("amount","hrm_employee_salary","payroll_head=2 and emp_code=$row[emp_code]");
			$medical_allowance = return_field_value("amount","hrm_employee_salary","payroll_head=3 and emp_code=$row[emp_code]");
			$others = return_field_value("amount","hrm_employee_salary","payroll_head=4 and emp_code=$row[emp_code]");
			$gross=$basic_salary+$house_rent+$medical_allowance+$others;
			$overtime=round(($basic_salary/26)/8,3);
	
			$dob=$row["dob"];
			$dob_explode=explode("-",$dob);
			$day=$dob_explode[2];
			$month=$dob_explode[1];
			$year=$dob_explode[0];
	
			$marital_status=$row["marital_status"];
			if($marital_status==1){$status='বিবাহিত';}else{$status='অবিবাহিত';}
			$salary=return_field_value("sum(amount)","hrm_employee_salary","emp_code=$row[emp_code] and status_active=1 and is_deleted=0 group by emp_code");
			
			$education=return_field_value("exam_name","hrm_employee_education","emp_code=$row[emp_code] and status_active=1 and is_deleted=0 order by id ASC limit 1");
	
	
	$html="<div align=\"center\" style=\"width:800\">
		<table width=\"950\" height=\"auto\"  bgcolor=\"#FFFFFF\" >
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
		  <td width=\"406\" rowspan=\"3\" align=\"left\"><img src=\"../resources/images/asro_logo.png\" width=\"146\" height=\"66\" /> </td>
		  <td align=\"left\" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><u>নিয়োগ</u></b><br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPLOYMENT</b></td>
		</tr>
		<tr>
			<td width=\"534\" align=\"left\">&nbsp;rdtertertertert</td>
		</tr>
		<tr>
			<td width=\"534\" align=\"left\">&nbsp;rdtertertertert</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>			
		<tr>
			<td width=\"406\"><b>Unit: ASROTEX LTD.</b></td>
			<td width=\"534\">&nbsp;</td>
		</tr>
		<tr>
			<td colspan=\"2\">	
				<table width=\"100%\">
				  <tr>
					<td width=\"10%\">নাম (Name)</td>
					<td width=\"18%\">: ". mysql_real_escape_string($row["full_name_bangla"]) ."</td>
				  </tr>
				  <tr>
					<td width=\"9%\">পিতার নাম (Father's Name)</td>
					<td width=\"22%\">: ". mysql_real_escape_string($row["father_name_bangla"]) ."</td>
				  </tr>
				  <tr>
					<td width=\"3%\">মাতার নাম (Mother's Name)</td>
					<td width=\"22%\">: ". mysql_real_escape_string($row["mother_name_bangla"]) ."</td>
				  </tr>
				</table>
			  </td>
			</tr>		
			<tr>
			<td>";	
			$pres_address_sql="select * from hrm_employee_address where address_type=0 and emp_code=$row[emp_code]";			
			$pres_address_result = mysql_query( $pres_address_sql ) or die( $pres_address_sql . "<br />" . mysql_error() );
			while($pres_row=mysql_fetch_array($pres_address_result))
			{
			 
			 $html .="				
					<table width=\"100%\">
						<tr>
							<td width=\"43%\" colspan=\"4\">বর্তমান ঠিকানা (Present Address):</td>								
						</tr>							
						<tr>
								<td>বাড়ী নং</td>
								<td>:......". mysql_real_escape_string($pres_row["house_no_bangla"]) ."......</td> 
								<td>রোড নং</td> 
								<td>:......". mysql_real_escape_string($pres_row["road_no_bangla"]) ."......</td>
							</tr>
							<tr>
								<td>মহল্লা/গ্রাম</td>
								<td>......". mysql_real_escape_string($pres_row["village_bangla"]) ."......</td> 
								<td>ডাকঘর</td> 
								<td>:......". mysql_real_escape_string($pres_row["post_code_bangla"]) ."......</td>
							</tr>
							<tr>
								<td>থানা</td>
								<td>:......". mysql_real_escape_string($pres_row["thana_bangla"]) ."......</td> 
								<td>জেলা</td> 
								<td>:......". mysql_real_escape_string($pres_row["district_id_bangla"] )."......</td>
							</tr>
					</table>";
					
				$pres_mobile_no=mysql_real_escape_string($pres_row["mobile_no"]);
				$pres_phone_no=mysql_real_escape_string($pres_row["phone_no"]);
			}
				
			$html .="</td>
					 <td>";
			
			$perm_address_sql="select * from hrm_employee_address where address_type=1 and emp_code=$row[emp_code]";
			$perm_address_result = mysql_query( $perm_address_sql ) or die( $perm_address_sql . "<br />" . mysql_error() );
			while($perm_row=mysql_fetch_array($perm_address_result))
			{
				
			$html .="<table width=\"100%\">
							<tr>
								<td width=\"43%\" colspan=\"4\">স্থায়ী ঠিকানা (Permanent Address):</td>								
							</tr>							
							<tr>
								<td>বাড়ী নং</td>
								<td>:......". mysql_real_escape_string($perm_row["house_no_bangla"]) ."......</td> 
								<td>রোড নং</td> 
								<td>:......". mysql_real_escape_string($perm_row["road_no_bangla"] )."......</td>
							</tr>
							<tr>
								<td>মহল্লা/গ্রাম</td>
								<td>:......". mysql_real_escape_string($perm_row["village_bangla"]) ."......</td> 
								<td>ডাকঘর</td> 
								<td>:......". mysql_real_escape_string($perm_row["post_code_bangla"]) ."......</td>
							</tr>
							<tr>
								<td>থানা</td>
								<td>:......". mysql_real_escape_string($perm_row["thana_bangla"]) ."......</td> 
								<td>জেলা</td> 
								<td>:......". mysql_real_escape_string($perm_row["district_id_bangla"]) ."......</td>
							</tr>
						</table>";
						
				$perm_mobile_no=mysql_real_escape_string($perm_row["mobile_no"]);
				$perm_phone_no=mysql_real_escape_string($perm_row["phone_no"]);
			}
			
		   $html .="</td>
				  	</tr>
				  	<tr>					
						<td colspan=\"2\">
						<table width=\"100%\">														
							<tr>
								<td>জরুরী যোগাযোগ নং(Emergency Contact)</td>
								<td>: ফোন......". $pres_phone_no ."......</td> 
								<td>মোবাইল:......". $pres_mobile_no."......</td> 
								<td></td>
							</tr>
							<tr>
								<td>জন্ম তারিখ/বয়স (Date Of Birth)</td>
								<td colspan=\"3\">:......". $day ."...... 
								/......". $month ."......
								/......". $year ."......</td>
							</tr>
							<tr>
								<td>বৈবাহিক অবস্থা (Marital Status)</td>
								<td>:........". $status ."...........................</td> 
								<td>শিক্ষাগত যোগ্যতা (Educational Qualificaton)</td> 
								<td>:........". $education .".......</td>
							</tr>
						</table>					
					</td>
				  </tr>
				  <tr>	
					<td>পূর্ব অভিজ্ঞতা (Previous Experience):</td>
					<td></td>
				  </tr>
				<tr>					
					<td colspan=\"2\">
						<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\" width=\"100%\">												
							<tr align=\"center\">
								<td></td>
								<td>কোম্পানির নাম (Name of Factory)</td> 
								<td>পদবী (Post)</td> 
								<td>বেতন (Salary)</td>
								<td>হইতে (From)</td>
								<td>পর্যন্ত (To)</td>
							</tr>";
					
					$experience_sql="select * from hrm_employee_experience where emp_code=$row[emp_code] and status_active=1 and is_deleted=0";		
					$experience_result = mysql_query( $experience_sql ) or die( $experience_sql . "<br />" . mysql_error() );
					$count=0;$check=0;
					while($experience_row=mysql_fetch_array($experience_result))
					{	
						$count++;	
						$html .="<tr>								
								<td>".$count."</td>
								<td>".$experience_row["organization_name"]."</td>
								<td>".$experience_row["designation"]."</td>
								<td>".$experience_row["gross_salary"]."</td>
								<td>".$experience_row["joining_date"]."</td>
								<td>".$experience_row["resigning_date"]."</td>
							</tr>";
											
					}
					$check=5-$count;					
					while($check<=5)
					{						
						$html .="<tr>																
								<td>".$check."</td> 	
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>";
						$check++;
					}
		
				$html .= "</table>					
					</td>
				  </tr> 				
		
		<tr>
		  <td colspan=\"2\">আমি এই মর্মে ঘোষনা করিতেছি যে, আমার জানামতে উপরোক্ত তথ্যাবলী নির্ভূল এবং সত্য। মিথ্যা প্রমানিত হলে কর্তৃপক্ষ আমার বিরুদ্ধে যে কোন আইনানুগ ব্যবস্থা গ্রহন করতে পারবেন। আমার ঠিকানা পরিবর্তিত হলে ০৩(তিন) দিনের মধ্যে পারসোনেল বিভাগে জানাতে বাধ্য থাকব।</td>		  
		</tr>
		<tr>
		  <td>তারিখ.............</td>
		   <td colspan=\"2\" align=\"right\"><p>------------------------------------<br />
			আবেদনকারী (Applicant)&nbsp;&nbsp;&nbsp;&nbsp;</p></td>		  
		</tr>		
		
		<tr>
		  <td colspan=\"2\" align=\"center\">প্রার্থীর যোগ্যতা ও বয়স যাচাই করা হয়েছে। নিয়োগদানের জন্য সুপারিশ করা হইল। <br />(Applicant's competency and age has been verified. Recommended for employment)</td>		  
		</tr>
		<tr>		  
		   <td align=\"left\"><p>------------------------------------<br />
			Departmental Head&nbsp;&nbsp;&nbsp;&nbsp;</p></td>		
			<td align=\"right\"><p>------------------------------------<br />
			HR Department&nbsp;&nbsp;&nbsp;&nbsp;</p></td>		
		</tr>
		<tr>					
			<td colspan=\"2\">
				<table width=\"100%\" style=\" border:1px solid;\" cellpadding=\"0\" cellspacing=\"0\">														
					<tr>								
						<td colspan=\"6\" align=\"center\" style=\" border:1px solid;\"> OFFICIAL USE ONLY </td>
					</tr>
					<tr>
						<td>Card No</td>
						<td>:....". mysql_real_escape_string($row["id_card_no"]) .".......</td>
						<td>Designation</td>
						<td>:....". $designation ."......</td>
						<td>Section</td>
						<td>:....". $section ."......</td>								
					</tr>
					<tr>
						<td>Join Date</td>
						<td>:....". $dob ."......</td>
						<td>Salary TK</td>
						<td colspan=\"2\">:....". $salary ."......(Taka)</td>
						<td></td>													
					</tr>
					<tr>
						<td>Remarks</td>
						<td colspan=\"5\">:....". mysql_real_escape_string($row["remark"]) .".....</td>																
					</tr>
					<tr>
						<td>Reference</td>
						<td colspan=\"5\">:....". mysql_real_escape_string($row["id_card_no"]) ."......</td>																
					</tr>
					<tr>								
						<td colspan=\"6\" >&nbsp;</td>
					</tr>
					<tr>
					  	<td colspan=\"3\" align=\"left\">Signature...................................</td>
					 	<td colspan=\"3\" align=\"right\">Authorised Signature...................................</td>								  
					</tr>
				</table>					
			</td>
		  </tr>		  	
		</table>
		</div>";
		} 
		echo $html;	
	  
	?>	
		<br \>		
	<!?
	}
}
	
?!>	
-->