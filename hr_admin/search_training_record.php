<?php 
/*************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 08.12.2013
**************************************/ 
session_start();
include('../includes/common.php');
include('../includes/array_function.php');
extract($_REQUEST);

if ($_SESSION['logic_erp']["data_level_secured"]==1){
if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else{
$buyer_name="";
$company_name="";
}
$search_string=$_GET["m_company"];
$m_buyer=$_GET["m_buyer"];

$sql="SELECT id,training_name FROM  lib_training";
$result=mysql_query($sql);
$training_name=array();
while($row=mysql_fetch_assoc($result)){$training_name[$row['id']]=$row['training_name'];}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="includes/functions.js"></script>
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../includes/tablefilter.js"></script>
        <script>
            function func_set_training_record(str){
                $('#data').val(str);
                parent.emailwindow.hide();
            }
        </script>
	</head>
    <body>
        <div align="center">
            <form name="search_order_frm"  id="search_order_frm" autocomplete="off">
                <fieldset style="width:930px">
                    <table class="rpt_table" width="900" cellspacing="2" cellpadding="0" border="0">
                        <thead>
                            <th>Sl</th>
                            <th>Training Name</th>
                            <th>Training Date</th>
                            <th>Training Place</th>
                            <th>Provided By</th>
                            <th>Trainer Name</th>
                            <th>Duration Days</th>
                            <th>Cost</th><input type="hidden" name="data" id="data"/>
                        </thead>
                        <tbody>
							<?php 	
                            $sql="SELECT * FROM hrm_training_mst";
                            $result=mysql_query($sql) or die(mysql_error());
                            $sl=1;
                            while($row=mysql_fetch_assoc($result))
                            {
                                if ($sl%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                                if($row['training_place']==1) $place="In-house"; else $place="Outside";		
                            ?>
                           <tr bgcolor="<?php echo "$bgcolor"; ?>" style="cursor:pointer" onclick="func_set_training_record('<?php echo $row['training_id'].",".convert_to_mysql_date($row['training_date']).",".$row['training_place'].",".$row['provided_by'].",".$row['trainer_name'].",".$row['duration'].",".$row['cost'].",".$row['id'] ?>')">
                                <td><?php echo $sl;?></td>
                                <td><?php echo $training_name[$row['training_id']];?></td>
                                <td><?php echo $row['training_date'];?></td>
                                <td><?php echo $place;?></td>
                                <td><?php echo $row['provided_by'];?></td>
                                <td><?php echo $row['trainer_name'];?></td>
                                <td><?php echo $row['duration'];?></td>
                                <td><?php echo $row['cost'];?></td>
                            </tr><?php $sl++;}?>
                         </tbody>
                    </table>
                    <div style="border:none" id="td_show_result"></div>
                    <table width="930">
                        <tfoot>
                            <th align="center" height="30" valign="bottom">
                                <div style="width:100%"> 
                                <div style="width:50%; float:left" align="left">
                                    <input type="hidden" readonly="readonly" name="txt_selected_id" id="txt_selected_id" /> 
                                    <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" />
                                    <input type="hidden" readonly="readonly" style="width:250px" class="text_boxes" name="txt_selected" id="txt_selected" />
                                </div>
                            </th>
                        </tfoot>
					</table>
                </fieldset>
            </form>    
        </div>
    </body>
</html>