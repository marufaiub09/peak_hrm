<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
 
include('../includes/common.php');
include('../includes/array_function.php');

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}

$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}

$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}

$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}

$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}

$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}

$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}

$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
}
	

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Untitled Document</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        
        <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
        
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
        
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script src="../includes/tablefilter.js" type="text/javascript"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script src="../js/popup_window.js" type="text/javascript"></script>
        <script src="../js/modal.js" type="text/javascript"></script>
        
        <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
         <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
 		 <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" charset="utf-8">
        
        var save_perm = <? echo $permission[0]; ?>;
        var edit_perm = <? echo $permission[1]; ?>;
        var delete_perm = <? echo $permission[2]; ?>;
        var approve_perm = <? echo $permission[3]; ?>;
       
	   
        $(document).ready(function(e) {
			$("#division_id,#department_id,#section_id,#subsection_id").multiselect({
			//header: false, 
			selectedText: "# of # selected",
			});
			$("#division_id,#department_id,#section_id,#subsection_id").multiselectfilter({
	
	});	
			
        }); 
        
     
        
        $(document).ready(function(){
			$(".datepicker").datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
			});
        });
        
       
        
        //Numeric Value allow field script
        function numbersonly(myfield, e, dec)
        {
			var key;
			var keychar;
			if (window.event)
			key = window.event.keyCode;
			else if (e)
			key = e.which;
			else
			return true;
			keychar = String.fromCharCode(key);
			
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			
			// numbers
			else if ((("0123456789.").indexOf(keychar) > -1))
			return true;
			else
			return false;
        }
        
       
        
        
  function fn_update_attn(){	
	
		var emp_category	= $('#emp_category').val();
		var company_id		= $('#company_id').val();
		var location_id		= $('#location_id').val();
		var division_id		= $('#division_id').val();
		var department_id	= $('#department_id').val();
		var section_id		= $('#section_id').val();
		var subsection_id	= $('#subsection_id').val();
		var join_status		= $('#join_status').val(); 
		var txt_date		= $('#txt_date').val();  
		var txt_attn_date	= $('#txt_attn_date').val();       
        
       
	   
	 $("#messagebox").removeClass().addClass('messagebox').text('Data Processing....').fadeIn(1000);

	if($('#company_id').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#company_id').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#join_status').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#join_status').focus();
			$(this).html('Please Select Status').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else if($('#join_status').val()!=0 && $('#txt_date').val()==""){
		
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_date').focus();
				$(this).html('Please Select Date Of Status').addClass('messageboxerror').fadeTo(900,1);
			});
	}
	else if($('#txt_attn_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_attn_date').focus();
			$(this).html('Please Select Date Of Attendance').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else{	
		 $("#process_anim").html('<img src="../../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('get','includes/custom_manual_attendance.php?action=custom_attn_processing'+
					'&emp_category='+emp_category+
					'&company_id='+company_id+
					'&location_id='+location_id+
					'&division_id='+division_id+
					'&department_id='+department_id+
					'&section_id='+section_id+
					'&subsection_id='+subsection_id+
					'&join_status='+join_status+
					'&txt_date='+txt_date+
					'&txt_attn_date='+txt_attn_date
					);
		http.onreadystatechange = data_process_reply;
		http.send(null); 
	}

}

function data_process_reply() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		
 //alert(response);return;
	
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Sorry,... Manual Attendance Process Interrupted.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(1000);
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Process Successfully Done.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(1000);
			});
		}
		
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Your selected date is not a Holiday, please Select a Holiday to make General Duty.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(1000);
			});
		}
	}
}	
        
 function active_inacive()
 {
	 if($('#join_status').val()==0)
	 {
		 $('#txt_date').val('');
		 $('#txt_date').attr('disabled','disabled');
	 }
	 else
	 {
		 $('#txt_date').removeAttr('disabled','disabled');	
	 }
 }
        
        
        </script>
        <style type="text/css">
			.style1 
			{
				font-family: Verdana, Arial, Helvetica, sans-serif
			}
			
			.verticalText 
			{               
				/* IE-only DX filter */
				writing-mode: tb-rl;
				filter: flipv fliph;
				/* Safari/Chrome function */
				-webkit-transform: rotate(270deg);
				/* works in latest FX builds */
				-moz-transform: rotate(270deg);
			}
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%;">	 
            <div style="width:100%;">
                <div class="form_caption">Manual Attendance For Off Day To General</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
                <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu(this.id,'content_search_panel','')">-Manual Attendance For Off Day To General</h3>
            </div>
            <div id="content_search_panel" >
                <form name="frm_manual_attn" id="frm_manual_attn" >		
                    <fieldset><legend> Search Panel</legend>
                        <table class="rpt_table"  width="980px" cellspacing="0" rules="all" cellpadding="0" border="1">
                            <thead>
                           		<tr>
                                <th width="140">Category</th>
                                <th width="140">Company Name</th>				    
                                <th width="140">Location</th>
                                <th width="140">Division</th>				    
                                <th width="140">Department</th>
                                <th width="140">Section</th>				    
                                <th width="140">Subsection</th>
                                </tr>
                            </thead>
                          <tbody>
                                <tr>
                                    <td>
                                        <select name="emp_category" id="emp_category" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td>
                                        <select name="company_id" id="company_id" class="combo_boxes" style="width:140px" >
                                            <? if($company_cond=="")
                                            { 
                                            ?>
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="location_id" id="location_id" class="combo_boxes" style="width:140px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                           <!-- <option value="0">-- Select --</option>-->
                                            <?php foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <!-- <option value="0">-- Select --</option>-->
                                            <?php foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <!-- <option value="0">-- Select --</option>-->
                                            <?php foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                           <!-- <option value="0">-- Select --</option>-->
                                            <?php foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                        		</tr>
                              </tbody>
                          </table>
                          <div style="height:8px;"></div>
                          	<table class="rpt_table"  width="400" cellspacing="0" rules="all" cellpadding="0" border="1">
                             <thead>
                             <tr>
                                <th width="100">Joining Status</th>
                                <th width="100">Date</th>
                                <th width="100">Atten Date</th>
                              </tr>
                            </thead>
                                <tr>
                                    <td width="100">
                                    <select name="join_status" id="join_status" class="combo_boxes" style="width:150px" onchange="active_inacive();">
                                        <option value="0">----Select----</option>
                                        <option value="1">Before Joining</option>
                                        <option value="2">After Joining</option>
                                    </select>
                                    </td>
                                    <td width="100"><input name="txt_date" id="txt_date" type="text"   class="datepicker" style="width:90px" disabled="disabled" /> </td> 
                                    <td width="100"><input name="txt_attn_date" id="txt_attn_date" type="text"   class="datepicker" style="width:90px" /> </td>
                                </tr>
                        </table>
                        <div align="center" style="height:30px; padding-top:12px;">
                            <input type="button" value="Update Attendance" name="update" id="update" class="formbutton" style="width:130px" onclick="javascript:fn_update_attn()" />                        </div>
                    </fieldset>
                </form>
            </div>
        </div>     
    </body>
</html>
