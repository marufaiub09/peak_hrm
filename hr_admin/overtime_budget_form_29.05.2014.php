<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
//--------------------------------------------------------------------------------------------------------------------
include('../includes/common.php');
include('../includes/array_function.php');

if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script src="includes/functions.js" type="text/javascript"></script>
<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>

	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
 
    <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script src="../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
    
    <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
	<script type="text/javascript" src="../includes/tablefilter.js"></script>
    <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
	<script type="text/javascript" src="../js/modal.js"></script>
  <!-- <script type="text/javascript" src="../js/light-table-filter.js"></script>-->
  
     <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
	
<style type="text/css">
 .style1 {font-family: Verdana, Arial, Helvetica, sans-serif}
</style>

<script type="text/javascript" charset="utf-8">
	var save_perm = <? echo $permission[0]; ?>;
	var edit_perm = <? echo $permission[1]; ?>;
	var delete_perm = <? echo $permission[2]; ?>;
	var approve_perm = <? echo $permission[3]; ?>;

	var location_details = division_details = department_details = section_details =emp_code_details=policy_details=id_details= new Array();
	var selected_id = new Array ;
	var selected_id_attnd = new Array ;
	
	<?php
				
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
		
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_details[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}	
	
	$sql = "SELECT * FROM hrm_employee WHERE status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$emp_code_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_code_details[$row['id']] = mysql_real_escape_string($row['emp_code']);
	}
	
	$sql = "SELECT * FROM hrm_employee WHERE status_active=1 and id_card_no!='' ORDER BY id_card_no ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$id_details = array();
	while( $row = mysql_fetch_array( $result ) ) { 
		$id_details[$row['emp_code']] = mysql_real_escape_string($row['id_card_no']);
	}
	//print_r($id_details);
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$policy_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$policy_details[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}

$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}	
	
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
	
	
//variable settings
//$replacement_lc=array(0=>"--- Select ---",1=>"Yes",2=>"No");
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		$reqn_based_ot= $res['reqn_based_ot'];
		
	}
				
	?>
var reqn_based_ot = <? echo $reqn_based_ot; ?>;
//alert (reqn_based_ot);
//http request
function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

//--------------------------------Manual attendence search-----------------//
function fn_search_manual_attendence_ot( vfrom ){
		
		if(!vfrom) var vfrom=0;
		var temp_id=0,temp_name=0;
		
		var cbo_company_id=$('#cbo_company_id').val();
		var location_id=$('#location_id').val();
		var department_id=$('#department_id').val();
		var section_id=$('#section_id').val();
		var subsection_id=$('#subsection_id').val();
		var designation_id=$('#designation_id').val();
		var shift_id=$('#shift_id').val();
		var txt_from_date=$('#txt_from_date').val();
		var txt_to_date=$('#txt_from_date').val();
		//var txt_to_date=$('#txt_to_date').val();
		var cbo_emp_category=$('#cbo_emp_category').val();
		var cbo_emp_status=$('#cbo_emp_status').val();
		var txt_out_time_hour=$('#txt_out_time_hour').val();
		var txt_out_time_minute=$('#txt_out_time_minute').val();
		var id_card=$('#id_card').val();
		var emp_code=$('#emp_code').val();
		
		$('#selected_id_i').val(''); 
		$('#selected_id_emp').val(''); //initialize
		selected_id = new Array ; //initialize
		selected_id_attnd = new Array ; //initialize
		
		$("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
		
			if($('#cbo_search_by_1').val()==1)
			{
				var section_id=$('#cbo_search_id').val();
				temp_id=section_id;
				temp_name="section_id";
			}
			else if($('#cbo_search_by_1').val()==2)
			{
				var designation_id=$('#cbo_search_id').val();
				temp_id=designation_id;
				temp_name="designation_id";
			}
			else if($('#cbo_search_by_1').val()==3)
			{
				var emp_code=$('#cbo_search_id').val();
				temp_id=emp_code;
				temp_name="emp_code";
			}
			else if($('#cbo_search_by_1').val()==4)
			{
				var id_card_no=$('#cbo_search_id').val();
				temp_id=id_card_no;
				temp_name="id_card_no";
			}
			else if($('#cbo_search_by_1').val()==5)
			{
				var policy_shift_id=$('#cbo_search_id').val();
				temp_id=policy_shift_id;
				temp_name="shift_policy";
			}
			else if($('#cbo_search_by_1').val()==6)
			{
				var department_id=$('#cbo_search_id').val();
				temp_id=department_id;
				temp_name="department_id";
			}
			else if($('#cbo_search_by_1').val()==7)
			{
				var subsection_id=$('#cbo_search_id').val();
				temp_id=subsection_id;
				temp_name="subsection_id";
			}
		
		if(cbo_company_id==0)
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_id').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if(txt_from_date=='')
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_from_date').focus();
				$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if( txt_to_date=='')
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_to_date').focus();
				$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if( txt_out_time_hour=='')
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_out_time_hour').focus();
				$(this).html('Please Select Budgeted OT').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}		
		else
		{ 
			var data='type=search_list_overtime_budget_info&cbo_company_id='+cbo_company_id+'&location_id='+location_id+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date+'&cbo_emp_category='+cbo_emp_category+'&temp_id='+temp_id+'&temp_name='+temp_name+'&cbo_emp_status='+cbo_emp_status+'&txt_out_time_hour='+txt_out_time_hour+'&txt_out_time_minute='+txt_out_time_minute+'&vfrom='+vfrom+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&shift_id='+shift_id+'&id_card='+id_card+'&emp_code='+emp_code;
			//alert(data);
			http.open('POST','includes/list_view.php',false);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", data.length);
			http.setRequestHeader("Connection", "close");
			http.onreadystatechange = show_search_list_manual_attendence;
			http.send(data);
		}
	}
function show_search_list_manual_attendence()
	{
		if(http.readyState == 4) 
		{
			var response =http.responseText.split('####');
			$('#data_panel2').html( response[0] );
			$('#data_panel3').html( response[2] );
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
			});
			var tableFilters = 
			{
				col_0: "none",
				col_20: "none",
				col_21: "none",
				col_22: "none",
			}
			setFilterGrid("table_body",-1,tableFilters);
		}
		else
		{
			$("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
		}
	}
	
	
function new_window_short()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel3').innerHTML);
	d.close();
}



//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}



function openmypage_ot_employee(page_link,title)
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=970px,height=370px,center=1,resize=0,scrolling=0','')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}	
function openmypage_employee_info_id_card(page_link,title)
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe','search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#id_card').val(thee_id.value);
			}
		}	
//All Check field functionality------------------------------------------//	

/*function fn_checked(chk_id,active_id,type){
	if($('#'+chk_id).attr('checked'))
		{
			document.getElementById(active_id).disabled=false;			
		}
	else
		{
			if(type=='cbo'){
				document.getElementById(active_id).value='0';}
			else{
				document.getElementById(active_id).value='';
			}
			document.getElementById(active_id).disabled=true;
		}	
}*/

//------------------END Check field---------------------------------//	
//ekram
function openmypage_employee_info(page_link,title)
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0','')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			//$('#cbo_search_id').val(thee_id.value);
			document.getElementById('cbo_search_id').value=thee_id.value;
		}
	}	
	
	$(document).ready(function() {
	$('#txt_in_time').timepicker({								 
	showMillisec: true,
	timeFormat: 'hh:mm:ss'
	//ampm: true
	});
	});
	$(document).ready(function() {
	$('#txt_out_time').timepicker({	  
	showMillisec: true,
	timeFormat: 'hh:mm:ss'
	//ampm: true
	});
	});
	$(document).ready(function() {
	$('#txt_select_date').datepicker({
	dateFormat: 'dd-mm-yy',
	//ampm: true
	});
	});
	
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
function populate_cost_center( child, parent_id, selected_id ) {
		$.ajax({
			type: "POST",
			url: "report/hrm/hrm_data.php",
			data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
			success: function( html ) {
				$('#' + child + '_id').html( html )
			}
		});
	}


function checkAll(field)
	{
		 selected_id.length = 0;
		 selected_id_attnd.length=0;
		$('#selected_id_i').val( '' );
		$('#selected_id_emp').val( '' );
		//alert($('#selected_id_i').val());
		//if ($('#chk_all').is(':checked'))
		
		if ($('#chk_all').val()==0)
		{	
			$('#chk_all').val('1');
			for (i = 1; i <= $("#table_body tr").length-1; i++)
			{
				//if(!$('#chk_all_'+i).attr("disabled","disabled"))
				//{	
				//var isDisabled = $('#chk_all_'+i).prop('disabled');
				//alert (isDisabled);
				if($('#chk_all_'+i).prop('disabled')==false)
				{
					$('#chk_all_'+i).attr('checked', 'checked');
					js_set_value( i );
				}
				//}
			}
		}
		else
		{
			$('#chk_all').val('0');
			for (i = 1;  i <= $("#table_body tr").length-1; i++)
			{
				$('#chk_all_'+i).removeAttr("checked");
				$('#emp_code_dtl').val( '' );
				//js_set_value( i );				
			}
			document.getElementById('show_count_list').innerHTML="You have selected "+ selected_id_attnd.length +" Employees";
		}
	}
	
function js_set_value( str ) 
	{
		str=document.getElementById('B'+str).innerHTML+"__"+trim(document.getElementById('G'+str).innerHTML);
		//alert(str);
		if( jQuery.inArray( str, selected_id ) == -1 ) 
		{
			selected_id.push( str );
		}
		else 
		{
			for( var i = 0; i < selected_id.length; i++ ) 
			{
				if( selected_id[i] == str ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id = '';
		for( var i = 0; i < selected_id.length; i++ ) 
		{
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#emp_code_dtl').val( id );
		//js_set_value_attnd( str );
		document.getElementById('show_count_list').innerHTML="You have selected "+ selected_id.length +" Employees";
	}
	
	
/*	
function js_set_value_attnd( str ) 
	{
		if ($('#selected_id_emp').val()=="") selected_id_attnd.length = 0;
		if( jQuery.inArray(  $('#attnd_id_' + str).val(), selected_id_attnd ) == -1 ) 
		{
			selected_id_attnd.push( $('#attnd_id_' + str).val() );
		}
		else 
		{
			for( var i = 0; i < selected_id_attnd.length; i++ ) 
			{
				if( selected_id_attnd[i] == $('#attnd_id_' + str).val() ) break;
			}
			selected_id_attnd.splice( i, 1 );
		}
		var id  = '';
		for( var i = 0; i < selected_id_attnd.length; i++ ) 
		{
			id += selected_id_attnd[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_emp').val( id );
		document.getElementById('show_count_list').innerHTML="You have selected "+ selected_id_attnd.length +" Employees";
	}
	*/
/*function fn_add_values_in_html()
	{
		$("#messagebox").html('<img src="../images/loading.gif" />').fadeIn(1000);
		
		if($('#chk_next_day').attr('checked')){
			var is_next_day = '1';
		}
		else
			var is_next_day = '0';
		
		if ($('#selected_id_i').val()=="")
		{ 
			alert ('No Employee or Attendance is Selected'); return false; 
		}
		else if ($('#cbo_day_status').val()==0)
		{ 
			alert ('Select Day Status for Effect'); return false; 
		}		
		else
		{
			var txt_from_date=$('#txt_from_date').val();
			var txt_to_date=$('#txt_to_date').val();
			
			var data=$('#selected_id_i').val()+"__"+$('#selected_id_emp').val()+"__"+$('#cbo_day_status').val()+"__"+$('#txt_in_time').val()+"__"+$('#txt_out_time').val()+"__"+ is_next_day +"__"+$('#cbo_att_policy').val()+"__"+$('#txt_remarks').val();
			//alert(data);
			$.ajax({type: "POST",url: "includes/manual_attendance_data.php?type=manual_attendance_data_set&data="+data+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date,
				
				success: function(response){
					eval(response);
					$("#messagebox").removeClass().addClass('messagebox').text('Attendace Data ADD in Done').fadeIn(1000);
				}
			});				
					
		}
	}
	*/

function fnc_move_cursor(val,id,field_id,lnth,max_val)
	{
		var str_length=val.length;
		if(str_length==lnth)
		{
			$('#'+field_id).select();
			$('#'+field_id).focus();
		}
		if(val>max_val)
		{
			document.getElementById(id).value=max_val;
		}
	}

	
function openmypage(page_link,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px,height=450px,center=1,resize=0,scrolling=0', '')
		emailwindow.onclose=function()
		{
			var theemail=this.contentDoc.getElementById("txt_selected_emp"); 
			var respons = theemail.value.split('_' );
			//alert(respons[2]);
			document.getElementById('system_id').value=respons[0];
			document.getElementById('confarm_save_update').value=respons[0];	
			document.getElementById('cbo_company_id').value=respons[1];	
			document.getElementById('location_id').value=respons[2];
			
			document.getElementById('department_id').value=respons[3];
			document.getElementById('section_id').value=respons[4];
			document.getElementById('subsection_id').value=respons[5];
			document.getElementById('designation_id').value=respons[6];
			document.getElementById('shift_id').value=respons[7];
			
			document.getElementById('txt_from_date').value=respons[8];
			document.getElementById('txt_to_date').value=respons[9];
			document.getElementById('cbo_emp_category').value=respons[10];
			document.getElementById('cbo_emp_status').value=respons[11];
			document.getElementById('txt_out_time_hour').value=respons[12];
			document.getElementById('txt_out_time_minute').value=respons[13];
			
			/*var system_id=this.contentDoc.getElementById("txt_system_id_val"); 
			var emp_code=this.contentDoc.getElementById("txt_selected_empcode"); 
			var company=this.contentDoc.getElementById("txt_company_id_val"); 
			var hour=this.contentDoc.getElementById("txt_hour_val"); 
			var minute=this.contentDoc.getElementById("txt_minute_val"); 
			
			var respons = system_id.value.split('*' );
			var company = company.value.split('*' );
			var hour = hour.value.split('*' );
			var minute = minute.value.split('*' );
			
			document.getElementById('system_id').value=respons[0];
			document.getElementById('confarm_save_update').value=respons[0];
			document.getElementById('emp_code').value=emp_code.value;
			document.getElementById('cbo_company_id').value=company[0];
			document.getElementById('txt_out_time_hour').value=hour[0];
			document.getElementById('txt_out_time_minute').value=minute[0];*/
			
			fn_search_manual_attendence(respons[0]);	
			//showResult_search(ot_table_id,"s",2,"movement_list_container");
		}
	}
	
	

$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});	
	
 });    
	
	

function generate_list_view(selected_id){
	
	var page_link="includes/get_data_update.php?period=";
	var id=selected_id;
	var title="Lock Status";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+id+'&type=list_view', title,'width=400px,height=300px,center=1,resize=0,scrolling=0','')
	
				
	/*$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&id=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});*/
	emailwindow.onclose=function(){};		
			
		}
		
		


function fn_print_report()
{
	var category_id		= $('#cbo_emp_category').val();
	var company_id		= $('#cbo_company_id').val();
	var location_id		= $('#location_id').val();
	var department_id	= $('#department_id').val();
	var section_id		= $('#section_id').val();
	var subsection_id	= $('#subsection_id').val();
	var designation_id	= $('#designation_id').val();
	
	var txt_from_date	= $('#txt_from_date').val();
	var shift_id		= $('#shift_id').val();
	var emp_status		= $('#cbo_emp_status').val();
	var id_card			= $('#id_card').val();
	var emp_code		= $('#emp_code').val();
	var time_hour		= $('#txt_out_time_hour').val();
	var time_minute		= $('#txt_out_time_minute').val();
	
	var error = false, data = '';
	
	if( $('#cbo_company_id').val() == 0 ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() 
		{
			$('#cbo_company_id').focus();
			$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( $('#txt_from_date').val() == "" ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() 
		{
			$('#txt_from_date').focus();
			$(this).html('Please select date.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	if( error == false ) 
	{
		$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
		var data='&category_id='+category_id+
				'&company_id='+company_id+
				'&location_id='+location_id+
				'&department_id='+department_id+
				'&section_id='+section_id+
				'&subsection_id='+subsection_id+
				'&designation_id='+designation_id+
				'&txt_from_date='+txt_from_date+
				'&shift_id='+shift_id+
				'&emp_status='+emp_status+
				'&id_card='+id_card+
				'&emp_code='+emp_code+
				'&time_hour='+time_hour+
				'&time_minute='+time_minute;
		
		nocache = Math.random();
		http.open( 'GET', 'includes/ot_requisition.php?type=ot_based_on_report_print' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fn_print_report;
		http.send(null); 
	}
}

function response_fn_print_report() 
{	
	if(http.readyState == 4) 
	{
		var response = http.responseText.split('####');
		//alert(response[0]);		
		/*$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
		$('#data_panel').append( '<input type="button" onclick="new_window_short()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );*/
		$('#data_panel4').html( response[0] );
		
		$("#messagebox").fadeTo( 200, 0.1, function(){
		$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
		$(this).fadeOut(5000);
		});
		
		new_window_report_print();
		
		var func = 'new_window_report_print()';
		window.setTimeout(func, 500);  
	}
	
}

	
function new_window_report_print()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel4').innerHTML);
	d.close();
}


	
	
</script>

<style>

.verticalText 
{               
    /* IE-only DX filter */
    writing-mode: tb-rl;
    filter: flipv fliph;

    /* Safari/Chrome function */
    -webkit-transform: rotate(270deg);

    /* works in latest FX builds */
	
    -moz-transform: rotate(270deg);
}

</style>

</head>

<body style="font-family:verdana; font-size:11px;">
 <div align="center" style="width:100%;">	
    <div>
    	<div class="form_caption">OT Requisition Form</div>
        <span id="permission_caption"> <? echo "Your Permissions--> \n".$insert.$update; ?>	 </span><br />
        <span id="permission_caption" style="font-style:italic; font-size:18px; color:#FF0000;">
         <?  if($reqn_based_ot!=1){ echo "As per variable settings Requisition not required.";die;}?>	
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
    <form name="frm_manual_attn" id="frm_manual_attn" >		
        <fieldset>
        <legend> Search Panel</legend>
        <!--<div align="center">Search:<input type="text" id="system_id" name="system_id" value="" class="text_boxes" readonly="readonly" placeholder="Double Click to Search" ondblclick="openmypage('includes/ot_popup/ot_search_employee.php','Employee Info');"/></div>	-->		
        <table class="rpt_table"  width="980" cellspacing="1">
            <thead>	
            	<th width="140">Category</th>
                <th width="140">Company Name</th>				    
                <th width="140">Location </th>				    
                <th width="140">Department</th>
                <th width="140" >Section</th>
                <th width="140" >Sub Section</th>
                <th width="140" >Designation</th>
                <input type="hidden" name="selected_id_i" id="selected_id_i" />
                <input type="hidden" name="selected_id_emp" id="selected_id_emp" />
                <input type="hidden" name="confarm_save_update" id="confarm_save_update" />
            </thead>
            <tr class="general" align="center">	
            	<td>
                <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                        <option value="">All Category</option>
                        <?
                        foreach($employee_category as $key=>$val)
                        {
                        ?>
                        <option value="<? echo $key; ?>"><? echo $val; ?></option>
                        <?
                        }
                        ?> 
                     </select>
                </td>				
                <td>					 
                    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px" >
                        <? if($company_cond=="")
                           { 
                            ?>
                        <option value="0">-- Select --</option>
                          <?php
                                }
                                $sql = "SELECT * FROM lib_company WHERE is_deleted = 0 $company_cond ORDER BY company_name ASC";
                                $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
                                $numrows=mysql_num_rows($result);
                                while( $row = mysql_fetch_assoc( $result ) ) {										
                            ?>							
                        <option value="<?php echo $row['id']; ?>" <? if($numrows==1)echo "selected"; ?> ><?php echo $row['company_name']; ?></option>
                            <?php 
                                } 
                            ?>
                    </select>
                </td>
                <td>					  
                    <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                      <option value="0">-- Select --</option>
                      <?php  
                            $sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
                            $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                            while( $row = mysql_fetch_assoc( $result ) ) {										
                       ?>
                      <option value="<?php echo $row['id']; ?>"><?php echo $row['location_name']; ?></option>
                      <?php 
                            } 
                        ?>
                  </select>
                </td>
                <td>
                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                       <!-- <option value="0">-- Select --</option>-->
                        <?php foreach( $department_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                       <!-- <option value="0">-- Select --</option>-->
                        <?php foreach( $section_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td id="subsection">
                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple">
                       <!-- <option value="0">-- Select --</option>-->
                        <?php foreach( $subsection_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td id="designation">
                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                       <!-- <option value="0">-- Select --</option>-->
                        <?php foreach( $designation_chart AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        </table>
        <div style="height:8px;"></div>
        <table class="rpt_table"  width="800" cellspacing="1">
            <thead>	
                <th width="100">Date</th>		      		
                <th width="100">Shift Policy</th>
                <th width="100">Emp Status</th>
                <th width="100">ID Card No</th> 
                <th width="120">System Code</th>
                <th width="130">Budgeted OT</th>
                <th> <input type="reset" name="reset" id="reset" value="Reset" class="formbutton" style="width:70px" />  </th>
            </thead>
            <tr>
                <td>
                    <input name="txt_from_date" id="txt_from_date" type="text" value="<? echo convert_to_mysql_date(date("Y-m-d",time())); ?>"  class="datepicker" style="width:100px" placeholder="From Date"  onchange="generate_list_view(this.value)" readonly="readonly" />
                  <input name="txt_to_date" id="txt_to_date" type="text" disabled="disabled" class="datepicker" value="<? //echo convert_to_mysql_date(date("Y-m-d",time())); ?>" style="width:70px; visibility:hidden; display:none;" placeholder="To Date"  />
                </td>
               <td>
                    <select name="shift_id" id="shift_id" class="combo_boxes" style="width:100px" >
                        <option value="0">-- Select --</option>
                        <?php foreach( $shift_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td align="center">
                    <select name="cbo_emp_status" id="cbo_emp_status" class="combo_boxes" style="width:100px" >
                       <option value="1">Regular</option>
                       <option value="0">Separated</option>
                    </select>
                </td>
                <td>
                    <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                </td>
                <td align="center">
                    <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:120px" ondblclick="openmypage_ot_employee('ot_employee_info.php','Employee Information')" placeholder="Double Click To Search" autocomplete="off" onkeypress="return numbersonly(this,event)"/>
                </td>
                <td>
                    H:<input name="txt_out_time_hour" id="txt_out_time_hour" type="text"  class="text_boxes" style="width:30px" onkeyup="fnc_move_cursor(this.value,'txt_out_time_hour','txt_out_time_minute',2,23);"  onkeypress="return numbersonly(this,event)"/>
                    M:<input name="txt_out_time_minute" id="txt_out_time_minute" type="text"  class="text_boxes" style="width:30px" onkeyup="fnc_move_cursor(this.value,'txt_out_time_minute','txt_out_time_minute',2,59);" onkeypress="return numbersonly(this,event)"/>
                </td>
                <td align="center"><input type="button" name="search" id="search" value="Search" class="formbutton" style="width:70px" onclick="javascript: fn_search_manual_attendence_ot()" />
                </td>
            </tr>                
        </table> 
        </fieldset>
        <fieldset> 
            <span id="show_count_list" style="font-weight:bold">&nbsp;</span>             
            <div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>                
            <div align="center">
            <input type="button" value="Save" name="update" id="update" class="formbutton" style="width:100px" onclick="javascript:fn_update_ot_manual(save_perm,edit_perm,delete_perm,approve_perm)" />
            &nbsp;&nbsp;<input type="button" onclick="new_window_short()" value="Print Questionable Employee" name="Print" class="formbutton" style="width:200px; "/>
            &nbsp;&nbsp;
            <input type="button" value="Print Requisition" name="print_report" id="print_report" class="formbutton" style="width:120px" onclick="javascript: fn_print_report()" />
            <!--&nbsp;&nbsp;
            <input type="hidden" value="Process OT" name="update" id="update" class="formbutton" style="width:100px" onclick="javascript: fn_process_attendance()" />-->
            </div> 
        </fieldset>
        <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden; display:none;">&nbsp;</div>
        <div id="data_panel4" align="center" class="demo_jui2" style="margin-top:10px; visibility:hidden; display:none;"></div>
        </form>
      </div>     
</body>
</html>
<script>
if(reqn_based_ot==0 || reqn_based_ot==2)
{
	$('#update').attr("disabled","disabled")
	$('#update').removeAttr('class');
	//$('#update').addClass('disabled');
	
	$(document).ready(function () {
	$(document).click(function (e) {
	fire(e);
	});
	});
}

		
</script>	
