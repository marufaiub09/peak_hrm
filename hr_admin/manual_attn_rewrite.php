<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);

if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
 
include('../includes/common.php');
include('../includes/array_function.php');

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}

$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}

$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}

$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}

$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}

$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}

$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}

$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
}
	
$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement",6=>"Govt. Holiday",7=>"Week-end");

$search_by=array(	0=>"-- Select --",
					5=>"Attendence Policy",
					6=>"Department",
					2=>"Designation",
					3=>"Employee Code",
					4=>"Employee ID",
					1=>"Sectoin",
					7=>"Subsection");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Untitled Document</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        
        <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
        
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
        
        <link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script src="../includes/tablefilter.js" type="text/javascript"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script src="../js/popup_window.js" type="text/javascript"></script>
        <script src="../js/modal.js" type="text/javascript"></script>
        
        <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" charset="utf-8">
        
        var save_perm = <? echo $permission[0]; ?>;
        var edit_perm = <? echo $permission[1]; ?>;
        var delete_perm = <? echo $permission[2]; ?>;
        var approve_perm = <? echo $permission[3]; ?>;
        
        var location_details = division_details = department_details = section_details =emp_code_details=policy_details=id_details= new Array();
        var selected_id = new Array ;
        var selected_id_attnd = new Array ;
        
        $(document).ready(function(e) {
			$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselect({
			//header: false, 
			selectedText: "# of # selected",
			});
        }); 
        
        //All Check field functionality------------------------------------------//	
        function fn_checked(chk_id,active_id_1,active_id_2,active_id_3,type)
        {
			//alert(chk_id+', '+active_id_1+', '+active_id_2+', '+type);
			if($('#'+chk_id).attr('checked'))
			{
				document.getElementById(active_id_1).disabled=false;
				document.getElementById(active_id_2).disabled=false;
				document.getElementById(active_id_3).disabled=false;			
			}
			else
			{
				if(type=='cbo')
				{
					document.getElementById(active_id_1).value='0';
					document.getElementById(active_id_2).value='0';
					document.getElementById(active_id_3).value='0';
				}
				else
				{
					document.getElementById(active_id_1).value='';
					document.getElementById(active_id_2).value='';
					document.getElementById(active_id_3).value='';
				}
				document.getElementById(active_id_1).disabled=true;
				document.getElementById(active_id_2).disabled=true;
				document.getElementById(active_id_3).disabled=true;
			}	
        }
        //All Check field functionality------------------------------------------//	
        function fn_checked_ss(chk_id,active_id,type)
        {
			//alert(chk_id+', '+active_id_1+', '+active_id_2+', '+type);
			if($('#'+chk_id).attr('checked'))
			{
				document.getElementById(active_id).disabled=false;
				//document.getElementById(active_id_2).disabled=false;			
			}
			else
			{
				if(type=='cbo')
				{
					document.getElementById(active_id).value='0';
					//document.getElementById(active_id_2).value='0';
				}
				else
				{
					document.getElementById(active_id).value='';
					//document.getElementById(active_id_2).value='';
				}
				document.getElementById(active_id).disabled=true;
				//document.getElementById(active_id_2).disabled=true;
			}	
        }
        
        function fnc_move_cursor(val,id,field_id,lnth,max_val)
        {
			//alert(id);
			var str_length=val.length;
			
			if(str_length==lnth)
			{
				$('#'+field_id).select();
				$('#'+field_id).focus();
			}
			
			if(val>max_val)
			{
				document.getElementById(id).value=max_val;
			}
        }
        
        //------------------END Check field---------------------------------//	
        //ekram
        function openmypage_employee_info(page_link,title)
        {			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0','')			
			emailwindow.onclose=function()
			{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#cbo_search_by_2_id').val(thee_id.value);
			}
        }	
        
        /*	$(document).ready(function() {
        $('#txt_in_time').timepicker({								 
        showMillisec: true,
        timeFormat: 'hh:mm:ss'
        //ampm: true
        });
        });
        */
        /*	$(document).ready(function() {
        $('#txt_out_time').timepicker({	  
        showMillisec: true,
        timeFormat: 'hh:mm:ss'
        //ampm: true
        });
        });
        */	
        $(document).ready(function(){
			$('#txt_select_date').datepicker({
			dateFormat: 'dd-mm-yy',
			//ampm: true
			});
        });
        
        $(document).ready(function(){
			$(".datepicker").datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
			});
        });
        
        function populate_cost_center( child, parent_id, selected_id ) 
		{
			$.ajax({
				type: "POST",
				url: "report/hrm/hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html )
				{
					$('#' + child + '_id').html( html )
				}
			});
        }
        
		function checkAll(field)
		{
			selected_id.length = 0;
			selected_id_attnd.length=0;
			$('#selected_id_i').val( '' );
			$('#selected_id_emp').val( '' );
			//	alert($('#selected_id_i').val());
			
			//if ($('#chk_all').is(':checked'))
			if ($('#chk_all').val()==0)
			{	
				$('#chk_all').val('1');
				for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
				{	
					$('#chk_all_'+i).attr('checked', 'checked');
					js_set_value( i );
				}
			}
			else
			{
				$('#chk_all').val('0');
				for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
				{
					$('#chk_all_'+i).removeAttr("checked");
					//js_set_value( i );				
				}
				document.getElementById('show_count_list').innerHTML="You have selected "+ selected_id_attnd.length +" Employees";
			}
        }
        
        function js_set_value( str ) 
        {
			if( jQuery.inArray( str, selected_id ) == -1 ) 
			{
			selected_id.push( str );
			}
			else 
			{
				for( var i = 0; i < selected_id.length; i++ ) 
				{
					if( selected_id[i] == str ) break;
				}
					selected_id.splice( i, 1 );
			}
			
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) 
			{
				id += selected_id[i] + ',';
			}
			
			id = id.substr( 0, id.length - 1 );
			$('#selected_id_i').val( id );
			js_set_value_attnd( str );
        }
        
        function js_set_value_attnd( str ) 
        {
			if ($('#selected_id_emp').val()=="") selected_id_attnd.length = 0;
			if( jQuery.inArray(  $('#attnd_id_' + str).val(), selected_id_attnd ) == -1 ) 
			{
				selected_id_attnd.push( $('#attnd_id_' + str).val() );
			}
			else 
			{
				for( var i = 0; i < selected_id_attnd.length; i++ ) 
				{
				if( selected_id_attnd[i] == $('#attnd_id_' + str).val() ) break;
				}
				selected_id_attnd.splice( i, 1 );
			}
			
			var id  = '';
			for( var i = 0; i < selected_id_attnd.length; i++ ) 
			{
				id += selected_id_attnd[i] + ',';
			}
			
			id = id.substr( 0, id.length - 1 );
			$('#selected_id_emp').val( id );
			document.getElementById('show_count_list').innerHTML="You have selected "+ selected_id_attnd.length +" Employees";
        }
        
        function fn_add_values_in_html()
        {
			$("#messagebox").html('<img src="../images/loading.gif" />').fadeIn(1000);
			
			if($('#chk_next_day').attr('checked'))
			{
				var is_next_day = '1';
			}
			else var is_next_day = '0';
			
			if ($('#selected_id_i').val()=="")
			{ 
				alert ('No Employee or Attendance is Selected'); return false; 
			}
			else if ($('#cbo_day_status').val()==0)
			{ 
				alert ('Select Day Status for Effect'); return false; 
			}		
			else
			{
				var txt_from_date=$('#txt_from_date').val();
				var txt_to_date=$('#txt_to_date').val();
				
				var in_time_hours=$('#txt_in_time_hours').val();
				var in_time_minuties=$('#txt_in_time_minuties').val();
				var in_time_seconds=$('#txt_in_time_seconds').val();
				if(in_time_hours=="") in_time_hours="00"; else  in_time_hours=$('#txt_in_time_hours').val();
				
				if(in_time_minuties=="") in_time_minuties="00"; else in_time_minuties=$('#txt_in_time_minuties').val();
				if(in_time_seconds=="") in_time_seconds="00"; else in_time_seconds=$('#txt_in_time_seconds').val();
				var in_time=(in_time_hours.length == 2 ? in_time_hours : "0"+in_time_hours)+':'+(in_time_minuties.length == 2 ? in_time_minuties : "0"+in_time_minuties)+':'+(in_time_seconds.length == 2 ? in_time_seconds : "0"+in_time_seconds);
				
				var out_time_hours=$('#txt_out_time_hours').val();
				var out_time_minuties=$('#txt_out_time_minuties').val();
				var out_time_seconds=$('#txt_out_time_seconds').val();
				if(out_time_hours=="") out_time_hours="00"; else out_time_hours=$('#txt_out_time_hours').val();
				if(out_time_minuties=="") out_time_minuties="00"; else out_time_minuties=$('#txt_out_time_minuties').val();
				if(out_time_seconds=="") out_time_seconds="00"; else out_time_seconds=$('#txt_out_time_seconds').val();
				//var out_time=out_time_hours+':'+out_time_minuties+':'+out_time_seconds;
				var out_time=(out_time_hours.length == 2 ? out_time_hours : "0"+out_time_hours)+':'+(out_time_minuties.length == 2 ? out_time_minuties : "0"+out_time_minuties)+':'+(out_time_seconds.length == 2 ? out_time_seconds : "0"+out_time_seconds);
				//alert($('#selected_id_i').val()); 
				var data=$('#selected_id_i').val()+"__"+$('#selected_id_emp').val()+"__"+$('#cbo_day_status').val()+"__"+in_time+"__"+out_time+"__"+$('#txt_out_time').val()+"__"+ is_next_day +"__"+$('#cbo_att_policy').val()+"__"+$('#txt_remarks').val();

				$.ajax({type: "POST",url: "includes/manual_attendance_data.php?type=manual_attendance_data_set&data="+data+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date,
					success: function(response)
					{
						eval(response);
						$("#messagebox").removeClass().addClass('messagebox').text('Attendace Data ADD in Done').fadeIn(1000);
					}
				});				
			}
        }
        
        function fn_add_values_in_html_on_save()
		{
			//$("#messagebox").html('<img src="../images/loading.gif" />').fadeIn(1000);
			
			if($('#chk_next_day').attr('checked'))
			{
				var is_next_day = '1';
			}
			else var is_next_day = '0';
			
			if ($('#selected_id_i').val()=="")
			{ 
				alert ('No Employee or Attendance is Selected'); return false; 
			}
			else if ($('#cbo_day_status').val()==0)
			{ 
				alert ('Select Day Status for Effect'); return false; 
			}		
			else
			{
				var txt_from_date=$('#txt_from_date').val();
				var txt_to_date=$('#txt_to_date').val();
				var new_id=($('#selected_id_i').val()).split(",");
				var in_time_all='';
				var out_time_all='';
				for(var k=0; k< new_id.length; k++)
				{
					//alert($('#in_time_hours_'+new_id[k]).val()+"___"+new_id[k]);
					if(in_time_all=='') in_time_all=$('#in_time_hours_'+new_id[k]).val()+":"+$('#in_time_minuties_'+new_id[k]).val()+":"+$('#in_time_seconds_'+new_id[k]).val();
					else  in_time_all=in_time_all+"*"+$('#in_time_hours_'+new_id[k]).val()+":"+$('#in_time_minuties_'+new_id[k]).val()+":"+$('#in_time_seconds_'+new_id[k]).val();
					
					if(out_time_all=='') out_time_all=$('#out_time_hours_'+new_id[k]).val()+":"+$('#out_time_minuties_'+new_id[k]).val()+":"+$('#out_time_seconds_'+new_id[k]).val();
					else  out_time_all=out_time_all+"*"+$('#out_time_hours_'+new_id[k]).val()+":"+$('#out_time_minuties_'+new_id[k]).val()+":"+$('#out_time_seconds_'+new_id[k]).val();
				}
				var check_on_save=1;
				var data=$('#selected_id_i').val()+"__"+$('#selected_id_emp').val()+"__"+$('#cbo_day_status').val()+"__"+in_time_all+"__"+out_time_all+"__"+$('#txt_out_time').val()+"__"+ is_next_day +"__"+$('#cbo_att_policy').val()+"__"+$('#txt_remarks').val()+"__"+check_on_save;
				//alert(data);
				var d= $.ajax({
				url: "includes/manual_attendance_data.php?type=manual_attendance_data_set&data="+data+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date,
				async: false
				}).responseText;
				eval(d);
				return;
				/*
				$.ajax({type: "POST",url: "includes/manual_attendance_data.php?type=manual_attendance_data_set&data="+data+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date,
				
				success: function(response){
				eval(response);
				$("#messagebox").removeClass().addClass('messagebox').text('Attendace Data ADD in Done').fadeIn(1000);
				}
				});	*/			
				
			}
		}
        
        //Numeric Value allow field script
        function numbersonly(myfield, e, dec)
        {
			var key;
			var keychar;
			if (window.event)
			key = window.event.keyCode;
			else if (e)
			key = e.which;
			else
			return true;
			keychar = String.fromCharCode(key);
			
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			
			// numbers
			else if ((("0123456789.").indexOf(keychar) > -1))
			return true;
			else
			return false;
        }
        
        function openmypage_employee_info(page_link,title)
        {			
			//alert("su..re");
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");
				$('#txt_emp_code').val(thee_id.value);
			}
        }
        
        function openmypage_employee_info_id_card(page_link,title)
        {			
			//alert("su..re");
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0',' ')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#txt_id_card').val(thee_id.value);
			}
        }
        </script>
        <style type="text/css">
			.style1 
			{
				font-family: Verdana, Arial, Helvetica, sans-serif
			}
			
			.verticalText 
			{               
				/* IE-only DX filter */
				writing-mode: tb-rl;
				filter: flipv fliph;
				/* Safari/Chrome function */
				-webkit-transform: rotate(270deg);
				/* works in latest FX builds */
				-moz-transform: rotate(270deg);
			}
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center">	
            <div>
                <div class="form_caption">Manual Attendance Entry</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
                <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu(this.id,'content_search_panel','')">-Manual Attendance Entry</h3>
            </div>
            <div id="content_search_panel" >
                <form name="frm_manual_attn" id="frm_manual_attn" >		
                    <fieldset style="width:1300px"><legend> Search Panel</legend>			
                        <table class="rpt_table"  width="1200" cellspacing="1">
                            <thead>
                                <th width="150">Category</th>
                                <th width="150">Company Name</th>				    
                                <th width="150">Location</th>
                                <th width="150">Division</th>				    
                                <th width="150">Department</th>
                                <th width="150">Section</th>				    
                                <th width="150">Subsection</th>
                                <th width="150">Designation</th>				    
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:150px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:150px" >
                                            <? if($company_cond=="")
                                            { 
                                            ?>
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:150px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:150px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:150px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:150px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:150px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="designation">
                                        <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:150px" multiple="multiple">
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">&nbsp;</td> 
                                </tr>
                            </tbody>
                        </table>
                        <table class="rpt_table"  width="950" cellspacing="1">
                            <thead>
                                <th width="180" colspan="2">Date</th>
                                <th width="150">Attn Policy</th>				    
                                <th width="150">Attn Status</th>
                                <th width="150">Emp Status</th>				    
                                <th width="150">ID Card No</th>
                                <th width="150">System Code</th>
                                <th width="70"><input type="reset" name="reset" id="reset" value="Reset" class="formbutton" style="width:70px" /></th>				    
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input name="txt_from_date" id="txt_from_date" type="text" value=""  class="datepicker" style="width:90px" placeholder="From Date"  /> </td>
                                    <td><input name="txt_to_date" id="txt_to_date" type="text"  class="datepicker" value="" style="width:90px" placeholder="To Date"  /></td>
                                    <td>
                                        <select id="cbo_attn_policy" name="cbo_attn_policy" class="combo_boxes" style="width:150px">
                                            <option value="0">-- select --</option>
                                            <?php foreach($shift_details as $key=>$value)
                                            {
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php	
                                            }
                                            ?>
                                        </select> 
                                    </td> 
                                    <td> 
                                        <select id="cbo_attn_status" name="cbo_attn_status" class="combo_boxes" style="width:150px">
                                            <option value="">-- select --</option>
                                            <?php 
                                            foreach($attn_status_arr as $key=>$value)
                                            {
                                            ?>
                                            <option value="<?php echo $key; ?>"<? if($key==0) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php										
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td> 
                                        <select name="cbo_emp_status" id="cbo_emp_status" class="combo_boxes" style="width:150px" >
                                            <option value="1">Regular</option>
                                            <option value="0">Separated</option>
                                        </select>
                                    </td> 
                                    <td> 
                                        <input type="text" name="txt_id_card" id="txt_id_card" class="text_boxes" style="width:150px" ondblclick="openmypage_employee_info_id_card('search_employee_multiple_by_id.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td> 
                                    <td>
                                        <input type="text" name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:150px" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info('search_employee_multiple.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>
                                    <td> 
                                        <input type="button" name="search" id="search" value="Search" class="formbutton" style="width:70px" onclick="javascript: fn_search_manual_attendence()"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8" align="center" >&nbsp; 
                                        <input type="hidden" name="selected_id_i" id="selected_id_i" />
                                        <input type="hidden" name="selected_id_emp" id="selected_id_emp" />
                                    </td>                    	
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
            <form>
                <fieldset>
                    <table class="rpt_table" width="1000px" cellspacing="1" >
                        <thead>
                            <th width="170">Day Status</th>				    
                            <th width="150">In Time</th>				    
                            <th width="150">Out Time</th>				                     
                            <th width="70">Next Day</th>               
                            <th width="115"> Remarks</th>				                    
                            <td align="left" valign="left" width="70px"><input type="reset" value="  Refresh  " name="add" id="close" style="width:70px" class="formbutton"/></td>
                        </thead>
                        <tbody>
                            <tr align="center" class="general">
                            <td>
                                <input name="chk_day_status" id="chk_day_status" type="checkbox" value="" onclick="fn_checked_ss('chk_day_status','cbo_day_status','cbo')" />
                                <select name="cbo_day_status" id="cbo_day_status" class="combo_boxes" style="width:135px" disabled="disabled">
                                    <option value="0"></option>
                                    <option value="A">Absent</option>
                                    <option value="PL">Present(Late)</option>
                                    <option value="P">Present(No Late)</option>
                                    <option value="W">Weekend</option>
                                    <option value="CH">Holiday</option>
                                    <option value="GD">General Duty</option>
                                </select>
                            </td>
                            <td>
                                <input name="chk_in_time" id="chk_in_time" type="checkbox" value="" onclick="fn_checked('chk_in_time','txt_in_time_hours','txt_in_time_minuties','txt_in_time_seconds','txt')" />
                                <input type="text" name="txt_in_time_hours" id="txt_in_time_hours" class="text_boxes_numeric" placeholder="HH" disabled="disabled" style="width:20px;" onKeyPress="return numbersonly(this, event);" onKeyUp="fnc_move_cursor(this.value,'txt_in_time_hours','txt_in_time_minuties',2,23);" /> :
                                <input type="text" name="txt_in_time_minuties" id="txt_in_time_minuties" class="text_boxes_numeric" placeholder="MM" disabled="disabled" style="width:20px;" onKeyPress="return numbersonly(this, event);" onKeyUp="fnc_move_cursor(this.value,'txt_in_time_minuties','txt_in_time_seconds',2,59)" /> :
                                <input type="text" name="txt_in_time_seconds" id="txt_in_time_seconds" class="text_boxes_numeric" placeholder="SS" disabled="disabled" style="width:20px;" onKeyPress="return numbersonly(this, event);" onKeyUp="fnc_move_cursor(this.value,'txt_in_time_seconds','txt_in_time_hours',2,59)" />             
                            </td>
                            <td>
                                <input id="chk_out_time" name="chk_out_time" type="checkbox" value="" onclick="fn_checked('chk_out_time','txt_out_time_hours','txt_out_time_minuties','txt_out_time_seconds','txt')" />
                                <input type="text" name="txt_out_time_hours" id="txt_out_time_hours" class="text_boxes_numeric" placeholder="HH" disabled="disabled" style="width:20px;" onKeyPress="return numbersonly(this, event);" onKeyUp="fnc_move_cursor(this.value,'txt_out_time_hours','txt_out_time_minuties',2,23);" /> :
                                <input type="text" name="txt_out_time_minuties" id="txt_out_time_minuties" class="text_boxes_numeric" placeholder="MM" disabled="disabled" style="width:20px;" onKeyPress="return numbersonly(this, event);" onKeyUp="fnc_move_cursor(this.value,'txt_out_time_minuties','txt_out_time_seconds',2,59)" /> :
                                <input type="text" name="txt_out_time_seconds" id="txt_out_time_seconds" class="text_boxes_numeric" placeholder="SS" disabled="disabled" style="width:20px;" onKeyPress="return numbersonly(this, event);" onKeyUp="fnc_move_cursor(this.value,'txt_out_time_seconds','txt_out_time_hours',2,59)" />
                            </td>
                            <td><input name="chk_next_day" id="chk_next_day" type="checkbox" value="" /></td>
                                <input name="chk_att_policy" id="chk_att_policy" type="hidden" value="" onclick="fn_checked_ss('chk_att_policy','cbo_att_policy','',cbo')" />
                                <input type="hidden" name="cbo_att_policy" id="cbo_att_policy" class="combo_boxes" style="width:100px" disabled="disabled" value="0" >
                                <?php /*?><?php
                                foreach($policy_details as $key => $val){
                                ?>
                                <option value="<?php echo $key; ?>"><? echo $val; ?></option>        
                                <?php
                                }
                                
                                ?><?php */?>
                                </select>
                            <td><input name="txt_remarks" id="txt_remarks" type="text"  class="text_boxes" style="width:250px" /></td>
                            <td style="text-align:left" align="left" width="70px">
                                <input type="button" name="search" id="search" value=" Add " class="formbutton" style="width:70px" onclick="fn_add_values_in_html()" />
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            </form>
            <fieldset style="width:1090px">
                <span id="show_count_list" style="font-weight:bold">&nbsp;</span>             
                <div style="width:100%; float:left; margin:auto" align="center" id="manual_attendence_list_container"></div><br/>                      
                <div align="center"><input type="button" value="Update Attendance" name="update" id="update" class="formbutton" style="width:130px" onclick="javascript:fn_update_attn_manual(save_perm,edit_perm,delete_perm,approve_perm)" /></div>                     			
            </fieldset>
        </div>     
    </body>
</html>
