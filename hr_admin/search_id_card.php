<? 
session_start();
include('../includes/common.php');
include('../includes/array_function.php');

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}



	$search_string=$_GET["m_company"];
	$m_buyer=$_GET["m_buyer"];
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="includes/functions.js"></script>

<link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../includes/tablefilter.js"></script>


<script>

//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


function search_populate(str)
{
	 //alert(str);
	if(str==0)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Employee Code";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==1)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Employee Name";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==2)
	{
		var designation = '<option value="0">--- Select ---</option>';
		<?php
		$designation_sql= mysql_db_query($DB, "select * from lib_designation where is_deleted=0  and status_active=1 order by level ");
		while ($row=mysql_fetch_array($designation_sql))
		{
			echo "designation += '<option value=\"$row[id]\">".mysql_real_escape_string($row[custom_designation])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Designation";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ designation +'</select>'; 
	}	
	else if(str==3)
	{
		var division = '<option value="0">--- Select ---</option>';
		<?php
		$division_sql= mysql_db_query($DB, "select * from lib_division where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($division_sql))
		{
			echo "division += '<option value=\"$row[id]\">".mysql_real_escape_string($row[division_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Division";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ division +'</select>'; 
	}	
	else if(str==4)
	{
		var department = '<option value="0">--- Select ---</option>';
		<?php
		$department_sql= mysql_db_query($DB, "select * from lib_department where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($department_sql))
		{
			echo "department += '<option value=\"$row[id]\">".mysql_real_escape_string($row[department_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Department";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ department +'</select>'; 
	}	
   else if(str==5)
	{
		var section = '<option value="0">--- Select ---</option>';
		<?php
		$section_sql= mysql_db_query($DB, "select * from lib_section where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($section_sql))
		{
			echo "section += '<option value=\"$row[id]\">".mysql_real_escape_string($row[section_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Section";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ section +'</select>'; 
	}
	else if(str==6)
	{
		//alert (str);
		document.getElementById('search_by_td_up').innerHTML="Enter Employee ID";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==7)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Punch Card No";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)"/>';
	}
	
 else if(str==8)
	{
		var location = '<option value="0">--- Select ---</option>';
		<?php
		$section_sql= mysql_db_query($DB, "select * from lib_location where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($section_sql))
		{
			echo "location += '<option value=\"$row[id]\">".mysql_real_escape_string($row[location_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Location";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ location +'</select>'; 
	}	
	
else if(str==9)
	{
		var subsection = '<option value="0">--- Select ---</option>';
		<?php
		$section_sql= mysql_db_query($DB, "select * from lib_subsection where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($section_sql))
		{
			echo "subsection += '<option value=\"$row[id]\">".mysql_real_escape_string($row[subsection_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Sub Section";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ subsection +'</select>'; 
	}
	
else if(str==10)
	{
		document.getElementById('search_by_td_up').innerHTML="EnterEmp Code";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}	
}

function fn_check()
	{
		$("#messagebox").removeClass().addClass('messagebox').text('Please wait....').fadeIn(1000);
		var company=document.getElementById('cbo_company_name').value;
		var category=document.getElementById('cbo_category_name').value;
		var search_by=document.getElementById('cbo_search_by').value;
		var search_field=document.getElementById('txt_search_common').value;
		var param=company+"_"+category+"_"+search_by+"_empinfoform";
		//if(company==0){ alert("Please Select Company Name."); return false;}
		//else{
			showResult_multi(search_field,param,1,'td_show_result');
		//}
	}

function js_set_value(str)
{
	var split_str=str.split('_');
	var emp_code = split_str[1];	
	document.getElementById('txt_selected_emp').value=emp_code;
	document.getElementById('txt_selected_name').value=str;
	parent.emailwindow.hide();	
}

</script>
</head>

<body>
<div>
<div style="height:18px; padding-bottom:5px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:1060px;" align="center"></div></div>
<form name="search_order_frm"  id="search_order_frm" autocomplete="off">
	<fieldset style="width:1040px">
	<table class="rpt_table" width="750" cellspacing="2" cellpadding="0" border="0" align="center">
		<thead>
		  <th width="100" align="center">Company&nbsp; </th>
		  <th width="100" align="center">Category&nbsp; </th>
		  <th width="100" align="center">Search By</th>
		  <th width="100" align="center" id="search_by_td_up">Enter Employee ID</th>
          <th width="100" align="center"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>		          
		</thead>
        <tr class="general">
          <td width="150" align="center">
         
          <select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:170px">
          	<? if($company_cond=="")
				{ 
			?>
            <option value="0">-- Select --</option>
            <?
				}
            		$sql= mysql_db_query($DB, "select * from lib_company where status_active=1 and is_deleted=0 $company_cond order by company_name");
                	$numrow=mysql_num_rows($sql);
					while ($selectResult = mysql_fetch_array($sql))
						{
					?>
            <option value="<?php echo $selectResult["id"]; ?>" <? if($numrow==1)echo "selected"; ?> ><?php echo $selectResult["company_name"]; ?></option>
            <?
						}
					?>
          </select></td>
          <td width="150" align="center"><select name="cbo_category_name" id="cbo_category_name" class="combo_boxes" style="width:170px">
            <option value="">-- Select --</option>
            <?
						foreach($employee_category as $key=>$val)
						{
                    ?>
            <option value="<? echo $key; ?>" ><? echo $val; ?></option>
            <?
						}
                    ?>
          </select></td>
          <td width="150" align="center"><select name="cbo_search_by" id="cbo_search_by" class="combo_boxes" style="width:150px" onchange="search_populate(this.value)">
           		<option value="6">Employee ID</option>
                <option value="0">Employee Code</option>
                <option value="1">Employee Name</option>
                <option value="7">Punch Card</option>
				<option value="2">Designation</option>
                <option value="8">Location</option>
                <option value="3">Division</option>
                <option value="4">Department</option>
                <option value="5">Section</option>
                <option value="9">Sub Section</option>
                <option value="10">Seperated</option>
           
            
          </select></td>
          <td width="150" align="center" id="search_field_td_up"><input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" placeholder="Press Space" /></td> <!--onkeypress="return numbersonly(this,event)"-->
          <td width="150" align="center"><input type="button" name="show_button" id="show_button" class="formbutton" value="Show" onclick="fn_check()" style="width:100px;" /></td>
          <!-- Hidden field -->
          <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" />
          <input type="hidden" name="txt_selected_name" id="txt_selected_name" />
		</tr>
   </table>
   		<hr />    
    	<div style="width:100%" id="td_show_result"></div>   
	</fieldset>
	</form>    
</div>
</body>

