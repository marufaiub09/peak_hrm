<?php
include('../../includes/common.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}
	
if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Ekram" />
	<meta name="description" content="Logic Payroll Software" />
	
	<!--<script src="includes/ajax_submit_salary.js" type="text/javascript"></script>-->
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	<script>
	
function countUp() {

 month = 'Jan';      // change the month to the one you want in the same format as shown
 date = '01';        // change the date to the one you want in the same format as shown
 year = '2002';      // change the year to the one you want in the same format as shown

 theDate = month + ' ' + date + ' ' + year;

   now = new Date();
   setdate = new Date(theDate);

   timer = (now - setdate) / 1000 / 60 / 60 / 24;
   timer = Math.round(timer);

document.getElementById("secs").innerHTML=timer;

  }
  
  
 function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array(); 
  
  
  
  
  function generate_list_view(selected_id){
	 
		var selected_id_spl=selected_id.split("_"); 
		 var selected_id=selected_id_spl[2];
		
		$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&id=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});
		}	





function fnc_compliance_salary_process()
{
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_salary_periods 	= escape(document.getElementById('cbo_salary_periods').value);
	var cbo_ot_rate_rounding 	= escape(document.getElementById('cbo_ot_rate_rounding').value);
	var txt_precision=escape(document.getElementById('txt_precision').value);
	// alert(txt_precision);return;
	$("#messagebox").removeClass().addClass('messagebox').text('Salary Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else
	{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/salary_processing_compliance.php?action=salary_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_salary_periods='+cbo_salary_periods+
					'&cbo_ot_rate_rounding='+cbo_ot_rate_rounding+
					'&txt_precision='+txt_precision
					);
		http.onreadystatechange = compliance_process_salary_reply;
		http.send(null); 
	}
}

function compliance_process_salary_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	//alert(response[1]);return;			
		if (response[1]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Salary Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Can Not Process Salary, Duplicate Process for this Periods.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==3)
		{
			
			alert('Please Lock The Actual salry First Of This Period.');
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Please Lock The Actual salry First Of This Period.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(3000);
				$(this).css('background-color', 'red');
			});
		}
		else if (response[1]==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Database is Busy, please try after some time.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				$(this).css('background-color', 'red');
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}				
	}
}	





</script>

</head>

<body style="font-family:verdana; font-size:11px;">
<div  style="width:850px;">

<div style="width:500px;float:left">
	<div align="center" style="font-size:18px; width:500px; position:relative; height:40px; margin:5px 0;">
		Compliance Salary Processing Unit</div>	
	<form id="holiday_form" action="javascript:fnc_compliance_salary_process();" method="POST">
		<fieldset style="width:500px;">
			<legend>Compliance Salary Process</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td> Select Company Name</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Company Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 order by company_name");
								$numrows=mysql_num_rows($company_sql);
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value="<? echo $r_company["id"];?>" <? if($numrows==1)echo "selected"; ?> ><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td > Select Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Location Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td> Select Salary Period</td>
					<td>
						<select name="cbo_salary_periods" id="cbo_salary_periods" class="combo_boxes" style="width:250px " onchange="generate_list_view(this.value)">
							<option value="0">--- Select Salary Period ---</option>
							<?
								$company_sql= mysql_db_query($DB, "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id  and b.type=1 and b.is_locked=0 and a.is_locked=1");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo "$r_company[actual_starting_date]"."_"."$r_company[actual_ending_date]"."_"."$r_company[year_id]";
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? $ex=explode(" ",$r_company[starting_date]); echo "$ex[0]"; ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
                <tr>
					<td> OT Rate Rounding Policy</td>
					<td>
						<input type="text" class="text_boxes_numeric" style="width:45px" value="2" id="txt_precision" />&nbsp;&nbsp; Digit&nbsp;&nbsp;<select name="cbo_ot_rate_rounding" id="cbo_ot_rate_rounding" class="combo_boxes" style="width:150px ">
							<option value="1"> Rounding</option>
                            <option value="2"> Round UP</option>
                            <option value="3"> Round Down</option>
                            <option value="4"> Cut</option>
					  </select>
                      
					</td>
				</tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" name="data_process" id="data_process" value="Start Compliance Salary Processing" style="width:270px; height:45px" class="formbutton" /> <input type="hidden" onclick="countUp()" /></td>
					
				</tr>
				<tr style="visibility:hidden">
					<td colspan="2" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<div align="center" id="messagebox"></div>
						<div id="process_anim"></div>
					</td>
				</tr>
			</table>
		</fieldset>
		
	</form>
	</div>
  <div align="center" style="font-size:18px; width:300px; position:relative; height:40px; margin:5px 0;"></div>	
    <div id="data_panel"  class="demo_jui" style="margin-top:10px; width:300px; float:right"></div>
</div>
</body>
</html>