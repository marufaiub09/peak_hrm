<?php
/************************************
|	Completed  By : Md. Nuruzzaman
|	Dated         : 24.03.2014 
*************************************/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Untitled Document</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(e) {
                $("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselect({
                //header: false, 
                selectedText: "# of # selected",
                });
            });
            $(document).ready(function() {
                $(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
                });
            });
			
			// createObject
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
            
            // checkbox check
            function Check(chk)
            {
                var cnt=0;
                if(document.frm_payment_lock.Check_ctr.checked==true)
                {
                    for (i = 0; i < chk.length; i++)
                    {
                        chk[i].checked = true ;
                        cnt++;
                    }
                }
                else
                {			
                    for (i = 0; i < chk.length; i++)
                    chk[i].checked = false ;			
                }
                $('#count_id').html('You Are Selected '+cnt+' Employees');
            }
            
            //count_emp
            function count_emp(chk,j)
            {
                var cnt=0;
                for (i = 0; i < chk.length; i++)
				{
                    if(chk[i].checked) {cnt++;}
                }
                $('#count_id').html('You Are Selected '+cnt+' Employees');
            }
            
            // fnc_payment_lock_show
            function fnc_payment_lock_show()
            {
                //alert("su..re");
                var category_id		= $('#cbo_category_id').val();
                var company_id		= $('#cbo_company_id').val();
                var location_id		= $('#cbo_location_id').val();
                var division_id		= $('#cbo_division_id').val();
                var department_id	= $('#cbo_department_id').val();
                var section_id 		= $('#cbo_section_id').val();
                var subsection_id	= $('#cbo_subsection_id').val();
                var designation_id	= $('#cbo_designation_id').val();
                
                var date			= $('#txt_date').val();
                var bill_type		= $('#cbo_bill_type').val();
                
                if(date=="")
                {
                    //alert("su..re");
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#txt_date').focus();
                        $(this).html('Please select date.').addClass('messageboxerror').fadeTo(900,1);
                        $(this).fadeOut(5000);
                    });
                }
                else if(bill_type==0)
                {
                    //alert("su..re");
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#cbo_bill_type').focus();
                        $(this).html('Please select bill type.').addClass('messageboxerror').fadeTo(900,1);
                        $(this).fadeOut(5000);
                    });
                }
                else
                {
                    //alert("su..re");
                    var data=	'&category_id='+category_id+
                                '&company_id='+company_id+
                                '&location_id='+location_id+
                                '&division_id='+division_id+
                                '&department_id='+department_id+
                                '&section_id='+section_id+
                                '&subsection_id='+subsection_id+
                                '&designation_id='+designation_id+
                                '&date='+date+
                                '&bill_type='+bill_type;
                                
                    $.ajax({
                        type: "POST",
                        url: "includes/generate_payment_lock.php?"+data,
                        data: 'form=payment_lock',
                        success: function(data) 
                        {
                            $('#data_panel').html( data );
                        }
                    });
                }
            }
			// fnc_payment_lock
            function fnc_payment_lock()
            {
                var date=$('#hdn_date').val();
                var bill_type=$('#hdn_bill_type').val();
                var tbl_lenght=$('#tbl_payment_lock tbody tr').length;
				
                var emp_code="";
                for(var i=1; i<=tbl_lenght; i++)
                {
                    if(document.getElementById('chk_list'+i).checked)
                    {
                         emp_code+=document.getElementById('chk_list'+i).value+",";
                    }
                }
                //alert(emp_code);
                http.open('GET','includes/generate_payment_lock.php?action=finaly_payment_lock&emp_code='+emp_code+'&date='+ date+'&bill_type='+ bill_type);
                http.onreadystatechange = response_finaly_payment_lock;
                http.send(null);
            }
            // response_finaly_payment_lock
            function response_finaly_payment_lock() 
            {
                if(http.readyState == 4) 
                {   
                    //alert(http.responseText)
                    var response = http.responseText;
                    if( response == 1 ) {
                        $("#messagebox").fadeTo( 200, 0.1, function(){
                            $(this).html('Tiffin payment locked successfully.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                    }
                    if( response == 2 ) {
                        $("#messagebox").fadeTo( 200, 0.1, function(){
                            $(this).html('Night allowance payment locked successfully.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                    }
                }
            }
        </script>
		<style type="text/css">
			.style1 {font-family: Verdana, Arial, Helvetica, sans-serif}
		</style>
    </head>
    <body>	
        <div style="width:1250px;" align="center">
            <div style="width:1210px;">
                <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:1210px;" align="center"></div></div>
                <h3 align="left" style="top:1px;width:1210" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Payment Lock</h3>
            </div>
            <form id="frm_payment_lock" name="frm_payment_lock" autocomplete="off" method="POST">
                <div id="content_search_panel" >
                    <fieldset id="filter_panel" style="width:1150px" >
                        <table align="center" width="1150" cellpadding="0" cellspacing="1" border="0" class="rpt_table">
                            <thead>
                                <th width="140px">Category</th>
                                <th width="140px">Company</th>
                                <th width="140px">Location</th>
                                <th width="140px">Division</th>
                                <th width="140px">Department</th>
                                <th width="140px">Section</th>
                                <th width="140px">SubSection</th>
                                <th width="140px">Designation</th>   
                            </thead>
                            <tbody>
                                <tr class="general">
                                    <td>
                                        <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
                                            <?php 
                                            if($company_cond=="")
                                            { 
                                            ?>
                                                <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_division_id" id="cbo_division_id" style="width:140px" multiple="multiple" >
                                            <?php 
                                            foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>        
                                    <td>
                                        <select name="cbo_department_id" id="cbo_department_id" style="width:140px" multiple="multiple">
											<?php 
                                            foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_section_id" id="cbo_section_id" style="width:140px" multiple="multiple" >
                                            <?php 
                                            foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" style="width:140px" multiple="multiple" >
                                            <?php 
                                            foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_designation_id" id="cbo_designation_id" style="width:140px;" multiple="multiple" >
                                            <?php 
                                            foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>  
                                </tr>
                            </tbody>
                        </table>
                        <div style="height:10px;"></div>
                        <table class="rpt_table"  width="310" cellspacing="1" cellpadding="1" align="center">
                            <thead>
                                <th width="100">Date</th>				    
                                <th width="140">Bill Type</th>
                                <th width="70"><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:70px" /></th>				    
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="10" style="width:100px" /></td>
                                    <td>
                                        <select id="cbo_bill_type" name="cbo_bill_type" class="combo_boxes" style="width:140px;">
                                            <?php
                                            foreach($payment_lock_arr as $key=>$value)
                                            {
                                            ?>
                                                <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                            <?php										
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td bgcolor="#A6CAF0">
                                    	<input type="button" name="search" id="search" value="Show" class="formbutton" onclick="fnc_payment_lock_show()" style="width:70px" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </div>
                <span id="count_id" style="font-weight:bold">&nbsp;</span>
                <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
                <div id="report_container" align="center" class="demo_jui2" style="margin-top:10px;"></div>
             
                <div style="width:1600px; float:left; margin:auto" align="center" id="div_list_view"></div>	
                <div align="center" style="width:1250px;float:left;">
                    <input type="button" id="approve_btn" name="approve_btn" class="formbutton" style="width:150px" value="Lock Payment" onclick="fnc_payment_lock();" />
                </div>   
            </form>
        </div>    
    </body>
</html>