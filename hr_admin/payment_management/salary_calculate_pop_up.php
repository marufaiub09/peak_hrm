<?php	
    include('../../includes/common.php');
	$emp_code=$_GET['emp_code'];
	//salary findout for specific enployee
	$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee WHERE emp_code=$emp_code and status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$emp_details=mysql_fetch_assoc($result);
	
	//salary breakdown info	
	$sql = "SELECT * FROM hrm_employee_salary WHERE emp_code=$emp_code and status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$count_rows = mysql_num_rows($result);
	$salary_details=array();
	while($rows=mysql_fetch_assoc($result))
	{
		$salary_details[$rows['emp_code']]=$rows;
		foreach( $rows as $key=>$val ){
			$salary_details[$rows['emp_code']][$key]=mysql_real_escape_string( $val );
		}
	}
	
	//library payroll head	
	$sql = "SELECT * FROM lib_payroll_head WHERE status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$payroll_details=array();
	while($rows=mysql_fetch_assoc($result))
	{
		$payroll_details[$rows['id']] = array();
		foreach( $rows AS $key => $value ) {
			$payroll_details[$rows['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	
	$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_breakdown_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$salary_breakdown_policy[$row['id']] = $row;
		
		$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		$salary_breakdown_policy[$row['id']]['definition'] = array();
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
		}
	}
	
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<link href="../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/functions.js"></script>
<script type="text/javascript" src="../../js/tooltip.js"></script>

<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>


<script>
function salary_breakdown_policy_change( policy_id ) {
		if( policy_id != 0 ) {
			$.ajax({
				type: "POST",
				url: "salary_rule.php?policy_id=" + policy_id,
				success: function( html ) {
					$('#salary tbody').html( html );
					head_counter = $('#salary tbody tr').length;
				}
			});
		}
		else {
			$('#salary tbody input[type="text"]').each(function() { $(this).removeAttr( 'disabled' ); });
			$('#salary tbody select').each(function() { $(this).removeAttr( 'disabled' ); });
		}
	}

</script>

</head>


<body>
<div align="center">
<form name="search_order_frm"  id="search_order_frm" autocomplete="off">
	<fieldset style="width:750px">
	<table width="700" cellspacing="2" cellpadding="0" border="0">
		<tr>
			<td width="390" align="center"><h2>Salary Break Down Info</h2></td>
        </tr>
        <tr>
        	<td>    
            <b>Emp Code : </b><?php echo $emp_details["emp_code"]?>  <b>Emp Name : </b><?php echo $emp_details["name"]?>
		    <hr /></td>
		</tr>
        <tr>
        	<td>    
            <b>Salary Grade:</b> <input type="text" name="salary_grade" id="salary_grade" class="text_boxes" value="<?php echo $emp_details["salary_rule"]?>" style="width:100px" />  
            <b>Policy :</b>
            <select name="salary_rule" id="salary_rule" class="combo_boxes" style="width:120px" onchange="salary_breakdown_policy_change( this.value )">
              <option value="0">Custom Policy</option>
              <?php foreach( $salary_breakdown_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
              <option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
              <?php } } ?>
            </select>
            <b>Gross Salary :</b>  <input type="text" name="gross_salary" id="gross_salary" class="text_boxes numbers" value="<?php echo $emp_details["gross_salary"]?>" style="width:100px" />  
            <input type="button" id="btn_cal" name="btn_cal" value="Calculate" class="formbutton" />
		    <hr /></td>
		</tr>
		<tr> 	 	 	 	 	 	 	 	 	
			<td align="center">            	     
                <table width="100%" id="salary">                	
                    <tr>
                    	<td style="width:120px"><b>Payroll Head</b></td>                    
                    	<td style="width:120px"><b>Type</b></td> 
                        <td id="percentage_container" style="width:120px"><b>Percentage</b></td> 
                        <td style="width:120px"><b>Base Head</b></td> 
                        <td style="width:120px"><b>Amount</b></td> 
					</tr>
					<?php
					$sql = "SELECT * FROM hrm_employee_salary WHERE emp_code=$emp_code and status_active=1 ORDER BY emp_code ASC";
					$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );				
					while($rows=mysql_fetch_assoc($result))
					{
					?>                    
                    <tr>
                    	<td style="width:120px"><?php echo $payroll_details[$rows['payroll_head']]['system_head']; ?></td>                    
                    	<td style="width:120px"><?php echo $rows['type']; ?></td> 
                        <td style="width:120px"><?php echo $rows['percentage_formula']; ?></td> 
                        <td style="width:120px"><?php if($rows['base_head']>0){echo $payroll_details[$rows['base_head']]['system_head'];} else echo "0"; ?></td> 
                        <td style="width:120px"><input type="text" name="gross_salary" id="gross_salary" style="width:100px" class="text_boxes numbers" value="<?php echo $rows['amount']; ?>" <?php if($rows['type']!='Fixed') echo "disabled" ; ?> /></td> 	 	 	
                    </tr>                   
                    <?php 					
					}
					?>
                    <tr>
                    	<td colspan="5"><hr /></td>
                    </tr>
                    <tr>
                    	<td style="width:120px"></td>                    
                    	<td style="width:120px"></td> 
                        <td style="width:120px"></td> 
                        <td style="width:120px">Total</td> 
                        <td style="width:120px"><?php echo $emp_details['gross_salary']; ?></td>
                    </tr>                    
                </table>
            </td>
		</tr>
	</table>
	</fieldset>
	</form>
</div>

</body>
</html>