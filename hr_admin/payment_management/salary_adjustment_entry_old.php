<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------
	
	date_default_timezone_set('UTC');
	include('../../includes/common.php');
	include('../../includes/array_function.php');
	

	$sql = "SELECT * FROM lib_policy_year WHERE status_active=1 and is_deleted = 0 and type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">

<script type="text/javascript"></script>
	
	<script src="includes/ajax_submit_salary.js" type="text/javascript"></script>
	
	<script src="includes/functions.js" type="text/javascript"></script>
    <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />	
    <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>

 	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
<script>
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		
		function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_month_selector').html( html )
				}
			});
		}
		
	//for html view
	var selected_id = new Array ;
	var selected_id_attnd = new Array ;
	function checkAll(field)
	{
		 selected_id.length = 0;
		 selected_id_attnd.length=0;
		$('#selected_id_i').val( '' );
		$('#selected_id_emp').val( '' );
		//	alert($('#selected_id_i').val());
	 	 
							 
		//if ($('#chk_all').is(':checked'))
		if ($('#chk_all').val()==0)
		{	
			$('#chk_all').val('1');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{	
				if(i>10){
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
						$(this).html('You can select only 10 employee at a time.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
					$('#chk_all_'+i).removeAttr("checked");
					return false;
				}
				$('#chk_all_'+i).attr('checked', 'checked');
				js_set_value( i );				
			}
		}
		else
		{
			$('#chk_all').val('0');
			for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{
				$('#chk_all_'+i).removeAttr("checked");
				// js_set_value( i );				
			}
		}
	}
	//push/pop selected_id array value
	function js_set_value( str ) 
	{
		alert(str);
		if( jQuery.inArray( str, selected_id ) == -1 ) 
		{
			
			if(selected_id.length>9){
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$(this).html('You can select only 10 employee at a time.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			$('#chk_all_'+str).removeAttr("checked");
			return false;
			}
			
			selected_id.push( str );
		}
		else 
		{
			for( var i = 0; i < selected_id.length; i++ ) 
			{
				if( selected_id[i] == str ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id  = '';
		for( var i = 0; i < selected_id.length; i++ ) 
		{
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		$('#selected_id_i').val( id );
		
		js_set_value_attnd( str );
	}
	//push/pop selected_id_attnd array value
	function js_set_value_attnd( str ) 
	{
		
		if( jQuery.inArray(  $('#attnd_id_' + str).val(), selected_id_attnd ) == -1 ) 
		{
			
			if(selected_id_attnd.length>9){			
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
					$(this).html('You can select only 10 employee at a time.').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
				$('#chk_all_'+str).removeAttr("checked");
				return false;
			}
			
			selected_id_attnd.push( $('#attnd_id_' + str).val() );
		}
		else 
		{
			for( var i = 0; i < selected_id_attnd.length; i++ ) 
			{
				if( selected_id_attnd[i] == $('#attnd_id_' + str).val() ) break;
			}
			selected_id_attnd.splice( i, 1 );
		}
		var id  = '';
		for( var i = 0; i < selected_id_attnd.length; i++ ) 
		{
			id += selected_id_attnd[i] + ',';
		}		
		id = id.substr( 0, id.length - 1 );
		
		$('#selected_id_emp').val( id );return;
	}
		
		
		function fn_check()
		{			
			
			selected_id.length = "";
		 	selected_id_attnd.length = "";
			var cbo_year_selector=document.getElementById('cbo_year_selector').value;
			var cbo_month_selector=document.getElementById('cbo_month_selector').value;
			var cbo_sal_head=document.getElementById('cbo_sal_head').value;
			
			var txt_emp_code=document.getElementById('txt_emp_code').value;
			var cbo_company_name=document.getElementById('cbo_company_name').value;
			var cbo_location_id=document.getElementById('cbo_location_id').value;
			var cbo_department_id=document.getElementById('cbo_department_id').value;
			var cbo_section_id=document.getElementById('cbo_section_id').value;
			var cbo_subsection_id=document.getElementById('cbo_subsection_id').value;
			var cbo_is_adjust=document.getElementById('cbo_is_adjust').value;
			//alert(txt_emp_code);
			var param = cbo_company_name+"_"+cbo_location_id+"_"+cbo_department_id+"_"+cbo_section_id+"_"+cbo_subsection_id+"_"+cbo_year_selector+"_"+cbo_month_selector+"_"+cbo_is_adjust+"_"+cbo_sal_head;//+"_"+txt_emp_code
					
			if(cbo_year_selector==0 ){
				
				$("#messagebox").fadeTo(200,0.1,function(){
					$('#cbo_year_selector').focus();
					$(this).html('Please Select Salary Period.').addClass('messagebox_ok').fadeTo(1000,1);
					$(this).fadeOut(3000);
				});
			}
			else if(cbo_month_selector==0){
				$("#messagebox").fadeTo(200,0.1,function(){
					$('#cbo_month_selector').focus();
					$(this).html('Please Select Salary Month.').addClass('messagebox_ok').fadeTo(1000,1);
					$(this).fadeOut(3000);
				});
			}
			else if(cbo_month_selector==0){
				$("#messagebox").fadeTo(200,0.1,function(){
					$('#cbo_month_selector').focus();
					$(this).html('Please Select Salary Month.').addClass('messagebox_ok').fadeTo(1000,1);
					$(this).fadeOut(3000);
				});
			}
			else{
				$('#div_list_view').html('<img src="../../resources/images/loading.gif" />');
				showResult_search(param,txt_emp_code,'sadjustment_search_list','div_list_view');
			}
			
		}
	
	function openmypage_employee_info(page_link,title)
		{
			// alert(page_link);
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0','../')
			
			emailwindow.onclose=function()
			{				
				var thee_id = this.contentDoc.getElementById("txt_selected_id").value;
				$('#txt_emp_code').val(thee_id);				
			}
		}
		
		
	function openpage_searchemp()
		{		
			var title="Search Employee";
			var cbo_year_selector=$('#cbo_year_selector').val();
			var cbo_month_selector=$('#cbo_month_selector').val();
			
			if(cbo_year_selector==0 ){
				
				$("#messagebox").fadeTo(200,0.1,function(){
					$('#cbo_year_selector').focus();
					$(this).html('Please Select Salary Period.').addClass('messagebox_ok').fadeTo(1000,1);
					$(this).fadeOut(3000);
				});
				return;
			}
			else if(cbo_month_selector==0){
				$("#messagebox").fadeTo(200,0.1,function(){
					$('#cbo_month_selector').focus();
					$(this).html('Please Select Salary Month.').addClass('messagebox_ok').fadeTo(1000,1);
					$(this).fadeOut(3000);
				});
				return;
			}
						
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_employee.php?cbo_year_selector='+cbo_year_selector+'&cbo_month_selector='+cbo_month_selector, title, 'width=1050px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var emp_code=this.contentDoc.getElementById("hidden_emp_code").value //Access form field with id="emailfield"				
				var period = this.contentDoc.getElementById('cbo_month_selector').value;
				var amount = this.contentDoc.getElementById('hidden_amount').value;
								
				$('#txt_demp_code').val(emp_code);
				$('#txt_adjusts_period').val(period);
				$('#txt_adjusts_amount').val(amount);				
					
			}
		} 
	
	
	function fn_select_row(val,tr)
	{
		if(val*1>0){			
			if( $("#"+tr+" input[type=checkbox]").is(':checked') ){}
			else{
				 	$('#'+tr).css("background-color","green");
					$("#"+tr+" input[type=checkbox]").attr('checked',true);
					js_set_value(tr);
				}
		}else{
			$('#'+tr).css("background-color","");
			$("#"+tr+" input[type=checkbox]").attr('checked',false);
			js_set_value(tr);
		}
	}
	
	
	
</script>

	
</head>

<body style="font-family:verdana; font-size:11px;">	
        <div align="center">
    	<div class="form_caption">Salary Adjustment Entry</div>
        <span id="permission_caption">
        	<? echo "Your Permissions--> \n".$insert;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
		</div>	
		<fieldset style="width:1200px ">			
        <!-- Start Form -->	
		<form name="frm_adjusment" id="frm_adjusment" method="POST" enctype="multipart/form-data" autocomplete="off" >
               
		<div style="float:left; width:650px">                     	
		<fieldset> 
        <legend>Salary Adjusted </legend>	
        <table width="600" class="rpt_table" border="0" cellpadding="0" cellspacing="0">		
			<thead>	
            	<tr>		
                  <th width="259" >Salary Period</th>
                  <th width="163" >Salary Head</th>
                  <th width="167" align="left">Show in Salary Sheet </th>                  
                </tr>
                <tr>		
                  <td width="259" >
                  		<select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" > 					  
                 		 	<option value="0">-- Select --</option>
                        	<?php foreach( $policy_year AS $key=>$value ){ ?>
                        	<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        	<?php } ?>
                		</select>
                        <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes">							
                        	<option value="0">-- Select --</option>
                    	</select>
                  </td>
                  <td width="163" >
                  		<select id="cbo_sal_head" name="cbo_sal_head" class="combo_boxes" style="width:130px" >
                            <option value="0">-- Select --</option>
							 <? 
							 	$sql=mysql_query("select id,custom_head,is_applicable,salary_head from lib_payroll_head where salary_head=0 and is_applicable=1 and status_active=1 and is_deleted=0"); 
							 	while($row=mysql_fetch_array($sql)){ 
							?>
									<option value="<?php echo $row[id]; ?>"><?php echo $row[custom_head]; ?></option>
							<?	
							 	}
							?>       
                        	
                        </select>
                  </td>
                  <td width="167"><input type="checkbox" name="chk_salary_sheet" id="chk_salary_sheet" /></td>                  
                </tr>			          
			</table>
            </fieldset> 	
      	</div>  		
	  	<div>
        	<fieldset>
            	<legend>Delete Salary Adjusted Record</legend>
            	<table class="rpt_table" border="0" cellpadding="0" cellspacing="0">
                	<thead>
                       <tr> 
                          <th>Emp Code</th>
                          <th>Salary Period</th>
                          <th>Amount</th>
                          <th>&nbsp;</th>
                       </tr>   
              	  </thead>
                	<tr>
                    	<td><input type="text" name="txt_demp_code" id="txt_demp_code" style="width:130px" class="text_boxes" placeholder="Double Click To Search" ondblclick="openpage_searchemp()" readonly="readonly"  /></td>
                        <td><input type="text" id="txt_adjusts_period" name="txt_adjusts_period" class="text_boxes" style="width:120px" disabled="disabled"  /></td>
                    	<td><input type="text" id="txt_adjusts_amount" name="txt_adjusts_amount" class="text_boxes" style="width:120px" disabled="disabled"  /></td>                       
                        <td><input type="button" id="btn_delete" name="btn_delete" class="formbutton" style="width:100px" value="Delete" onclick="javascript:fn_adjustment_record_delete()"  /></td>
                    </tr>
                </table>
            </fieldset>
        </div>	
      <fieldset>
			<legend>Search Panel</legend>
        		<table width="900"  class="rpt_table" border="0" cellpadding="0" cellspacing="0">
					<thead>
					  <tr>
                      	<th>Select</th>
					    <th>Company Name</th>
					    <th>Location&nbsp;</th>
					    <th>Department&nbsp;&nbsp;</th>
					    <th>Section&nbsp;</th>
					    <th>Sub Section&nbsp;&nbsp; </th>
					    <th>Emp Code&nbsp;</th>
					    <th valign="middle"><input type="reset" id="reset_btn" name="reset_btn" class="formbutton" style="width:100px" value="Reset" /></th>
				      </tr>
					  <tr>
                      		<td width="220"> 
							  <select name="cbo_is_adjust" id="cbo_is_adjust" class="combo_boxes" style="width:130px">
                                <!--<option value="1" selected="selected">-- All --</option>-->
                                <option value="2"> New Entry </option> 
                                <option value="3"> Previous Entry </option>                                
                       	 	  </select>
							</td>                      
							<td width="220"> 
							  <select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:130px">
                                <option value="0">-- Select --</option>
                                <?
                                $sql= mysql_db_query($DB, "select * from lib_company where status_active=1 and is_deleted=0 order by company_name");
								$numrows=mysql_num_rows($sql);
                                while ($selectResult = mysql_fetch_array($sql))
                                    {
                                ?>
                              	<option value="<?php echo $selectResult["id"]; ?>" <? if($numrows==1)echo "selected"; ?>><?php echo $selectResult["company_name"]; ?></option>
                                <?
                                    }
                                ?>
                       	 	</select>
							</td>
                            <td width="220"><select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:130px" >
                                <option value="0">-- Select --</option>
                                   	<?
									$sql= mysql_db_query($DB, "select * from lib_location where status_active=1 and is_deleted=0 order by location_name");
									while ($selectResult = mysql_fetch_array($sql))
										{
									?>
                                   <option value="<?php echo $selectResult["id"]; ?>"><?php echo $selectResult["location_name"]; ?></option>
									<?
                                        }
                                    ?>
                                </select>
							</td>
                            <td width="220"><select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:130px" >
                                <option value="0">-- Select --</option>
                                	<?
									$sql= mysql_db_query($DB, "select * from lib_department where status_active=1 and is_deleted=0 order by department_name");
									while ($selectResult = mysql_fetch_array($sql))
										{
									?>
                                   <option value="<?php echo $selectResult["id"]; ?>"><?php echo $selectResult["department_name"]; ?></option>
									<?
                                        }
                                    ?>
							</select>
							</td>
                            <td width="220"> 
                              <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:130px" >
                                    <option value="0">-- Select --</option>
                                    <?
									$sql= mysql_db_query($DB, "select * from lib_section where status_active=1 and is_deleted=0 order by section_name");
									while ($selectResult = mysql_fetch_array($sql))
										{
									?>
                                   <option value="<?php echo $selectResult["id"]; ?>"><?php echo $selectResult["section_name"]; ?></option>
									<?
                                        }
                                    ?>
                                </select>
							</td>
                            <td width="220"><select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:130px" >
                                  <option value="0">-- Select --</option>
                                    <?
									$sql= mysql_db_query($DB, "select * from lib_subsection where status_active=1 and is_deleted=0 order by subsection_name");
									while ($selectResult = mysql_fetch_array($sql))
										{
									?>
                                   <option value="<?php echo $selectResult["id"]; ?>"><?php echo $selectResult["subsection_name"]; ?></option>
									<?
                                        }
                                    ?>
                                </select>
							</td>                           
                            <td width="220">
                              <input type="text" placeholder="Double Click For Search"  name="txt_emp_code" id="txt_emp_code" value="" class="text_boxes" size="20" ondblclick="openmypage_employee_info('../search_employee_multiple.php','Employee Information'); return false" readonly="readonly"/>
							</td>
                            <td width="150" valign="middle">
                            	<input type="button" id="sub_btn" name="sub_btn" class="formbutton" style="width:100px" value="Search" onclick="fn_check();" />
                            	<input type="hidden" name="selected_id_i" id="selected_id_i" />
                    			<input type="hidden" name="selected_id_emp" id="selected_id_emp" />
                      		</td>
					</tr></thead>
				</table>
		  </fieldset>
    
	<!-- End Form --> 
    	<div style="width:1400" align="center" id="div_list_view"></div>	
        <div align="center" ><input type="button" id="update_btn" name="update_btn" class="formbutton" style="width:150px" value="Save Adjustment" onclick="javascript:fn_salary_adjustment_entry();" /></div>   
    </form>
    </fieldset>
	<!-- End Field Set -->	
    
</body>
</html>
