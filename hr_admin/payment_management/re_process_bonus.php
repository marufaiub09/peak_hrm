<?php
include('../../includes/common.php');
include('../../includes/array_function.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_salary.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
		
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
	<script>
	
function countUp() {

 month = 'Jan';      // change the month to the one you want in the same format as shown
 date = '01';        // change the date to the one you want in the same format as shown
 year = '2002';      // change the year to the one you want in the same format as shown

 theDate = month + ' ' + date + ' ' + year;

   now = new Date();
   setdate = new Date(theDate);

   timer = (now - setdate) / 1000 / 60 / 60 / 24;
   timer = Math.round(timer);

document.getElementById("secs").innerHTML=timer;

  }
$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		
		
function openpage_searchemp(page_link,title)
		{		
			var company=document.getElementById('cbo_company_name').value;
			var txt_join_date=document.getElementById('txt_join_date').value;	
			var salary_periods=document.getElementById('cbo_salary_periods').value;
			var cbo_festival_type=document.getElementById('cbo_festival_type').value;
							
			if(company==0 || company=='') { alert("Please Select The Company."); return false; }
			if(cbo_festival_type==0 || salary_periods=='') { alert("Please Select The Festival Type."); return false; }
			if(salary_periods==0 || salary_periods=='') { alert("Please Select The Salary Period."); return false; }
			if(txt_join_date==0 || salary_periods=='') { alert("Please Select The Joining Date."); return false; }
			
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../search_employee_multiple.php?company='+company+'&location=0', title, 'width=1100px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#txt_emp_code').val(thee_id.value);	
			}
		}		
		
		
	
 
 function generate_list_view(selected_id){
	
	var selected_id_spl=selected_id.split("_"); 
   var selected_id=selected_id_spl[2];
	
	$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&id=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});
		}		
		
</script>

</head>

<body style="font-family:verdana; font-size:11px;">
<div  style="width:850px;">

<div style="width:500px;float:left">
	<div align="center" style="font-size:18px; width:500px; position:relative; height:40px; margin:5px 0;">
		Bonus Re-Process</div>	
	<form id="holiday_form" action="javascript:fnc_regular_re_process_bonus();" method="POST">
		<fieldset style="width:500px;">
			<legend>Bonus Re-Process</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td> Select Company Name</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:300px ">
							<option value="0">--- Select Company Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 order by company_name");
								$numrows=mysql_num_rows($company_sql);
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value="<? echo $r_company["id"];?>" <? if($numrows==1)echo "selected"; ?>><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td>Festival Type</td>
					<td>
						<select name="cbo_festival_type" id="cbo_festival_type" class="combo_boxes" style="width:300px ">
							<?php
							foreach($bonus_festival_list as $key=>$value):
							?>
							<option value=<? echo "$key"; ?>> <? echo "$value" ;?> </option>							
							<?php		
							endforeach;
							?>
							 
					  </select>	
					</td>
				</tr>
				<tr>
					<td> Select Salary Period</td>
					<td>
						<select name="cbo_salary_periods" id="cbo_salary_periods" class="combo_boxes" style="width:150px " onchange="generate_list_view(this.value)">
							<option value="0">--- Select Salary Period ---</option>
							<?
								$company_sql= mysql_db_query($DB, "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id  and b.type=1 and b.is_locked=0 and a.is_locked=0");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo "$r_company[actual_starting_date]"."_"."$r_company[actual_ending_date]"."_".$r_company[year_id] ;
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[starting_date]"."-"."$r_company[ending_date]"; ?> </option>
								<?
								}
							?>
					  </select>	&nbsp;Pay Date&nbsp;<input type="text" class="datepicker" style="width:75px" name="txt_pay_date" id="txt_pay_date" />
					</td>
				</tr>
                <tr>
					<td colspan="2" align="left">Process Bonus for Employees, who joined Before&nbsp;&nbsp; 
                      <input type="text" class="datepicker" style="width:75px" name="txt_join_date" id="txt_join_date" /> </td>
				</tr>
               <tr>
				  <td>Employee Code</td>
				  <td><input name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:240px " placeholder="Double Click To Search" readonly ondblclick="openpage_searchemp('../search_employee_multiple.php','Search Employee')" ></td>
			  </tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" name="data_process" id="data_process" value="Start Processing" style="width:270px; height:45px" class="formbutton" /> <input type="hidden" onclick="countUp()" /></td>
					
				</tr>
				<tr style="visibility:hidden">
					<td colspan="2" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<div align="center" id="messagebox"></div>
						<div id="process_anim"></div>
					</td>
				</tr>
			</table>
		</fieldset>
		
	</form>
	 </div>
  <div align="center" style="font-size:18px; width:300px; position:relative; height:40px; margin:5px 0;"></div>	
    <div id="data_panel"  class="demo_jui" style="margin-top:10px; width:300px; float:right"></div>
</div>
</body>
</html>