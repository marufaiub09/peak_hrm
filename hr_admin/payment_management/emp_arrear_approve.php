<?php
include('../../includes/common.php');
extract($_REQUEST);

$months = array('01'=>'January','02'=>'February','03'=>'March','40'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');

$current_gross_salary = return_field_value("gross_salary","hrm_employee","emp_code=$emp_code");
$last_month_salary =  return_field_value("salary_periods","hrm_salary_mst","emp_code=$emp_code");

$get_month = datediff('m',$effective_date,$last_month_salary)+1;



	//arrear array
	$sql = "SELECT * FROM hrm_arrear_earning_mst mst,hrm_arrear_earning_dtls dtls WHERE increment_mst_id =$incr_mst_id and mst.id=dtls.mst_id ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$arrear_array = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$arrear_array[$row['payment_month']] = $row['amount'];
	}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title></title>
<head>

	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
    
<script type="text/javascript">

//-----------salary brak down check---------------//
	
	function fn_datepicker()
	{
		$(document).ready(function() {
            $('.datepicker').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true
			});
        });	
	}	
			
	function fn_amount_total()
	{
		var amount=0;
		var row = $('#tbl_arrear_earning tbody tr').length;	
		while(row>0){
			amount += $('#txt_arrear_amount_'+row).val()*1;
			row--;
		}
		$("#td_total_amount").html(amount);
	}
	
	var year_arrange='';
	
	<?
		//year drop down arrange
		$c_year=date("Y");
		$s_year=$c_year-5;
		$e_year=$c_year+5;
		
		for ($year = $s_year; $year <= $e_year; $year++) {
			if($c_year==$year)$sel ='Selected'; else $sel='';
			echo "year_arrange += '<option value=\"$year\" $sel>$year</option>';";
		}
	
	?>
	
	
	function fn_row_add(){
		head_counter = $('#tbl_arrear_earning tbody tr').length+1;
		$('#tbl_arrear_earning tbody').append(
			'<tr class="general">'
				+ '<td><select name="cbo_year_selector[]" id="cbo_year_selector_' + head_counter + '" class="combo_boxes" style="width:100px" >'+ year_arrange  +
				+ '</select></td>'
				+ '<td><select name="txt_arrear_date[]" id="txt_arrear_date_' + head_counter + '" class="combo_boxes" >'
				+		'<option value="0">--Select --</option>'
				+		'<option value="01">January</option>'
				+		'<option value="02">February</option>'
				+		'<option value="03">March</option>'
				+		'<option value="04">April</option>'
				+		'<option value="05">May</option>'
				+		'<option value="06">June</option>'
				+		'<option value="07">July</option>'
				+		'<option value="08">August</option>'
				+		'<option value="09">September</option>'
				+		'<option value="10">October</option>'
				+		'<option value="11">November</option>'
				+		'<option value="12">December</option>'
				+ '</select></td>'
				+ '<td><input type="text" name="txt_arrear_amount[]" id="txt_arrear_amount_' + head_counter + '" value="" class="text_boxes" onBlur="fn_amount_total()" onkeypress="return numbersonly(this,event)" /></td>'
				+ '<td></td>'
			+ '</tr>'
						
		);
		
	}
	
	
	function fn_emp_arrear_bill(){
							   
					var date_arr = new Array();
					for( var i = 1; i <= $('#tbl_arrear_earning tbody tr').length; i++ ) {
						var arrer_year = $('#cbo_year_selector_'+i).val();
						var arr_date = arrer_year+'-'+$('#txt_arrear_date_'+i).val()+'-01';
						
						if( arr_date <= $('#txt_process_salary_date').val() && $("#txt_arrear_amount_"+i).val()!='')
						{
							alert("Please Correct The Arrear Year/Month.");return;
						}
						else if($('#txt_arrear_date_'+i).val() !='0' && $("#txt_arrear_amount_"+i).val()=='')
						{
							alert("Please Input The Arrear Amount.");return;
						}
						else if($('#txt_arrear_date_'+i).val() =='0' && $("#txt_arrear_amount_"+i).val()!='')
						{
							alert("Please Input The Arrear Year/Month.");return;
						}
						
						if( jQuery.inArray( arr_date, date_arr ) == -1 ) 
						{
							date_arr[i-1]=arr_date;
						}
						else
						{
							alert("Please Input The Currect Arrear Year/Month.");return;
						}
						
						
					}
					
					if($('#txt_total_arrear').val()*1 != $('#td_total_amount').html()*1)
					{
						alert("Warning!!!Arrear Not Properly Defined");			
					}
					else
					{
						var date_amount = '',data_amount_final='';
						for( var i = 1; i <= $('#tbl_arrear_earning tbody tr').length; i++ ) {
							if( $("#txt_arrear_date_"+i).val() != '' && $("#txt_arrear_amount_"+i) != '' ) {
									if(i==1){
										date_amount += $("#txt_arrear_date_"+i).val()+"___"+$('#txt_arrear_amount_'+i).val();
									}else{
										date_amount += "*"+$("#txt_arrear_date_"+i).val()+"___"+$('#txt_arrear_amount_'+i).val();
									}
						  		}
							}
						//concate here
						data_amount_final = $('#txt_effective_edate').val()+"*"+ $('#txt_increment_amount').val()+"*"+$('#txt_total_arrear').val()+"*"+date_amount;
							
					 }
														
					$("#hidden_date_amount").val(data_amount_final);
					parent.emailwindow.hide();	
					
	}
	
	
</script>    
 
</head>
<body>
<fieldset style="width:700px;">
<div>
<form id="frm_emp_arrear" action="" method="POST">
	<table id="tbl_arrear" class="rpt_table" border="0" cellpadding="0" cellspacing="0" style="width:100%; border:1px solid #000;" rules="all">
        <thead>    
            <tr>
              <th>Last Salary Month(Process)</th>
                <th><input id="txt_process_salary_date" name="txt_process_salary_date" style="width:120px"  class="text_boxes" value="<? echo $last_month_salary; ?>" /></th>
                <th>Effective Date</th>
              	<th><input id="txt_effective_edate" name="txt_effective_edate" style="width:120px"  class="text_boxes" value="<? echo $effective_date; ?>"  /></th>
            </tr>           
            <tr>
                <th>Increment Amount</th>
                <th><input id="txt_increment_amount" name="txt_increment_amount" style="width:120px" class="text_boxes" value="<? echo $increment_amount; ?>"  /></th>
                <th>Total Arrear</th>
              	<th><input type="text" id="txt_total_arrear" name="txt_total_arrear" style="width:120px"  class="text_boxes" value="<? if($get_month*$increment_amount>0) echo $get_month*$increment_amount; else echo "0"; ?>" onkeypress="return numbersonly(this,event)" /></th>                
            </tr>
        </thead>                       
	</table>
    <br />
    <div align="center" style="font-size:14px; font-weight:bold;">Arrear Month(Total)</div>
    <table id="tbl_arrear_earning" align="center" class="rpt_table" border="0" cellpadding="0" cellspacing="0" style="width:600px; border:1px solid #000;" rules="all">
    		<thead>            	
                <tr>
                  <th width="">Select Year </th>
                    <th width="">Select Month</th>
                    <th width="">Arrear Amount</th>
                    <th width=""><input type="button" id="txt_add_row_0" name="txt_add_row[]"  class="formbutton" value="ADD ROW" onClick="fn_row_add()" /></th> 
                </tr>                   
            </thead> 
            <tbody>
            	<? 
				$i=1;
				$td_incr_amount=0;
				foreach($arrear_array as $key=>$val){ 
						$explode_val = explode("-",$key);
				?>
                	<tr class="general">
                	  <td><select name="cbo_year_selector" id="cbo_year_selector_<? echo $i; ?>" class="combo_boxes" style="width:100px" >
                	    <option value="0">-- Select --</option>
                	    <?php 
                                $c_year=date("Y");
                                $s_year=$c_year-5;
                                $e_year=$c_year+5;
                                
                                 for ($year = $s_year; $year <= $e_year; $year++) { ?>
                	    <option value=<?php echo $year;
                                if ($year==$explode_val[0]){?> selected <?php }?>><?php echo $year; ?></option>
                	    <?php } ?>
              	    </select></td>
                        <td>
                        <!--<input type="text" id="txt_arrear_date_<? echo $i; ?>" name="txt_arrear_date[]" class="datepicker" onClick="fn_datepicker();" value="<? echo $key; ?>" />-->
                            <select name="txt_arrear_date[]" id="txt_arrear_date_<? echo $i; ?>" class="combo_boxes" > 
                                <option value="0">-- Select --</option>
                                <? foreach($months as $id=>$res){ ?>
                                		<option value="<? echo $id; ?>" <? if($id==$explode_val[1])echo "selected";?>><? echo $res; ?></option>
								<? } ?>                                
                            </select>                        
                        </td>
                        <td><input type="text" id="txt_arrear_amount_<? echo $i; ?>" name="txt_arrear_amount[]"  class="text_boxes" onBlur="fn_amount_total()" value="<? echo $val; ?>" onkeypress="return numbersonly(this,event)" /></td>
                        <td></td>
            		</tr>
                <? $td_incr_amount +=$val; $i++; } ?>
            </tbody>
            <tfoot>
                	<tr>
                	  <th>&nbsp;</th>
                	  <th>Total</th>
                    <th id="td_total_amount"><? echo $td_incr_amount; ?></th>
                    <th>&nbsp;</th>
                    <input type="hidden" id="hidden_date_amount" name="hidden_date_amount" value="" />
            </tfoot>
            
    </table>
    
    <div align="center" style="padding-top:10px;"><input type="button" name="close" class="formbutton" style="width:100px" value="Close" onclick='fn_emp_arrear_bill();' /></div>

</form>
</div>
</fieldset>
</body>
</html>


<?

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}

?>