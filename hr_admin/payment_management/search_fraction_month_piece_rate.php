<? 
include('../../includes/common.php');
include('../../includes/array_function.php');
extract($_GET);
extract($_POST);


//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
	
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/functions.js"></script>
<script>
	function js_set_value(str)
	{
		//alert (str);
		document.getElementById('hidden_id').value=str;
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div>
<form name="search_order_frm"  id="search_order_frm">
	<fieldset style="width:930px">
    
	  <div>
            <div style="width:930px;" align="left">
                <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
                    <thead>
                        <th width="50">SL</th>
                        <th width="120">Company</th>
                        <th width="120">Location</th>
                        <th width="120">Effective Periods</th>
                        <th width="120">Attend Date On</th>
                        <th width="120">OT Included</th>
                        <th width="120">Abs Deduction</th>
                        <th width="120">Late Deductione</th>
                    </thead>
                </table>
            </div>	
            <div style="width:930px; min-height:50px; max-height:210px;" id="resource_allocation_list_view" align="left">
                <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="tbl_list_search" >
                
        <?php
        	$i=1;
            $arr=array(1=>'Yes',2=>'No');
            $sql= mysql_db_query($DB, "SELECT * FROM hrm_fraction_month_salary WHERE company_id=$company and salary_type=1");
            while ($selectResult = mysql_fetch_array($sql))
                {
                    $param = $selectResult['id']."_".$selectResult['company_id']."_".$selectResult['location_id']."_".$selectResult['category']."_".$selectResult['effective_periods']."_".convert_to_mysql_date($selectResult['attend_date_on'])."_".convert_to_mysql_date($selectResult['ot_date_on'])."_".$selectResult['ot_included']."_".$selectResult['ot_hrs_up_to']."_".$selectResult['absent_deduction']."_".$selectResult['late_deduction']."_".$selectResult['gross_salary_limit']."_".$selectResult['rounding_amount']."_".$selectResult['leave_days_included']."_".$selectResult['holi_days_included']."_".$selectResult['remarks'];
                    
        	?>
                        <tr style="text-decoration:none; cursor:pointer" id="search<?php echo $i; ?>"  onclick="js_set_value('<? echo $param; ?>')">
                            <td width="50"><?php echo $i; ?><input type="hidden" id="hidden_id" value=""  /></td>	
                            <td width="120"><?php echo $company_details[$selectResult[company_id]]; ?></td>
                            <td width="120"><?php echo $location_details[$selectResult[location_id]]; ?></td>
                            <td width="120"><?php echo convert_to_mysql_date($selectResult[effective_periods]); ?></td>
                            <td width="120"><?php echo convert_to_mysql_date($selectResult[attend_date_on]); ?></td>
                            <td width="120"><?php echo $arr[$selectResult['ot_included']];?></td> 	
                            <td width="120"><?php echo $arr[$selectResult['absent_deduction']]; ?></td>
                            <td width="120"><?php echo $arr[$selectResult['late_deduction']];  ?></td>
                        </tr>
                        <?php
                        $i++;
                      }                    
                    ?>
                </table>
            </div>
         </div>	
	</fieldset>
	</form>
</div>
</body>
</html>