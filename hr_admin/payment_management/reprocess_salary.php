<?php
include('../../includes/common.php');
include('../../includes/array_function.php');
if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}
	
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
	
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}	


	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_salary.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
		
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
    
	<script>
	
	function countUp() {
	
		month = 'Jan';      // change the month to the one you want in the same format as shown
		date = '01';        // change the date to the one you want in the same format as shown
		year = '2002';      // change the year to the one you want in the same format as shown
		
		theDate = month + ' ' + date + ' ' + year;
		
		now = new Date();
		setdate = new Date(theDate);
		
		timer = (now - setdate) / 1000 / 60 / 60 / 24;
		timer = Math.round(timer);
		
		document.getElementById("secs").innerHTML=timer;
		
	}


	function openpage_searchemp(page_link,title)
		{		
			var company=document.getElementById('cbo_company_name').value;
			var location=document.getElementById('cbo_location_name').value;	
			var salary_periods=document.getElementById('cbo_salary_periods').value;
							
			if(company==0 || company=='') { alert("Please Select The Company."); return false; }
			if(salary_periods==0 || salary_periods=='') { alert("Please Select The Salary Period."); return false; }
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../search_employee_multiple_salary_process.php?company='+company+'&location='+location+'&salaryreproces=1', title, 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#txt_emp_code').val(thee_id.value);	
			}
		}

	function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/salary_processing.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_salary_periods').html( html )
				}
			});
		}

	
	
	function generate_list_view(selected_id)
	{
	
		var selected_id_spl=selected_id.split("_"); 
		var selected_id=selected_id_spl[2];
		
		$.ajax({
			type: "GET",
			url: "includes/get_data_update.php?",
			data: 'type=list_view&id=' + selected_id,
			success: function( html ) {
				$('#data_panel').html( html )
				
			}
		});
	}

</script>

</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:950px;">

<div style="width:600px;float:left;">
	<div align="center" style="font-size:18px; width:600px; position:relative; height:40px; margin:5px 0;">Cost Center Wise Salary Process</div>	
	<form id="holiday_form" action="javascript:fnc_regular_reprocess_salary();" method="POST">
		<fieldset style="width:600px;">
			<legend>Salary Re Process</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="5">
				<tr>
					<td colspan="4" height="20"></td>
				</tr>
				<tr>
					<td>Company</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:150px ">
							<option value="0">--- Select Company Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 order by company_name");
								$numrows=mysql_num_rows($company_sql);
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value="<? echo $r_company["id"];?>" <? if($numrows==1)echo "selected"; ?>><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
                    <td >Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:150px">
							<option value="0">--- Select Location Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
                <tr>
                    <td>Division</td>
                    <td>
                        <select name="division_id" id="division_id" class="combo_boxes" style="width:150px">
                        <option value="0">-- Select --</option>
                        <?php foreach( $division_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </td>
                    <td>Department</td>
                    <td>
                        <select name="department_id" id="department_id" class="combo_boxes" style="width:150px">
                        <option value="0">-- Select --</option>
                        <?php foreach( $department_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Section</td>
                    <td>
                        <select name="section_id" id="section_id" class="combo_boxes" style="width:150px" >
                        <option value="0">-- Select --</option>
                        <?php foreach( $section_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </td>
                    <td>Sub-Section</td>
                    <td>
                        <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:150px">
                        <option value="0">-- Select --</option>
                        <?php foreach( $subsection_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </td> 
				</tr>
                <tr> 
                    <td>Designation</td>
                    <td>
                        <select name="designation_id" id="designation_id" class="combo_boxes" style="width:150px" >
                        <option value="0">-- Select --</option>
                        <?php foreach( $designation_chart AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </td>
                    <td>Category</td>
                    <td>
                        <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:150px">
                        <option value="">All Category</option>
                        <?
                        foreach($employee_category as $key=>$val)
                        {
                        ?>
                        <option value="<? echo $key; ?>"><? echo $val; ?></option>
                        <?
                        }
                        ?>  
                        </select>
                    </td>
                </tr>
				<!--<tr style="display:none">
				  <td>Select Salary Year</td>
				  <td>
                	<select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value),generate_list_view(this.value)" style="width:250px" > 					  
                 		 <option value="0">-- Select Salary Year--</option>
                        <?php/* foreach( $policy_year AS $key=>$value ){ */?>
                        <option value="<?php /*echo $key; ?>"><?php echo $value; */?></option>
                        <?php // } ?>
                	</select>
                </td>
			  </tr>-->
                
                
                <tr>
                    <td> Select Salary Period</td>
                    <!--<td>
                    <select name="cbo_salary_periods" id="cbo_salary_periods" class="combo_boxes" style="width:250px ">
                    
                    </select>	
                    </td>-->
                    <td>
                        <select name="cbo_salary_periods" id="cbo_salary_periods" class="combo_boxes" style="width:150px " onchange="generate_list_view(this.value)">
                        <option value="0">--- Select Salary Period ---</option>
                        <?
                        $company_sql= mysql_db_query($DB, "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id  and b.type=1 and b.is_locked=0 and a.is_locked=0");
                        while ($r_company=mysql_fetch_array($company_sql))
                        {
                        ?>
                        <option value=<? echo "$r_company[actual_starting_date]"."_"."$r_company[actual_ending_date]"."_"."$r_company[year_id]";
                        if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? $ex=explode(" ",$r_company[starting_date]); echo "$ex[0]"; ?> </option>
                        <?
                        }
                        ?>
                        </select>	
                    </td>
                    <td>Employee Code</td>
                    <td>
                    	<input name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:140px " placeholder="Double Click To Search" readonly ondblclick="openpage_searchemp('../search_employee_multiple_salary_process.php?salaryreproces=1','Search Employee')" >
                    </td>
                </tr>
				
                <tr>
                    <td> OT Rate Rounding Policy</td>
                    <td>
                    	<input type="text" class="text_boxes_numeric" style="width:45px" value="2" id="txt_precision" />&nbsp;&nbsp; Digit&nbsp;&nbsp;
                    </td>
                    <td> </td>
                    <td>    
                        <select name="cbo_ot_rate_rounding" id="cbo_ot_rate_rounding" class="combo_boxes" style="width:150px ">
                        <option value="1"> Rounding</option>
                        <option value="2"> Round UP</option>
                        <option value="3"> Round Down</option>
                        <option value="4"> Cut</option>
                        </select>
                    </td>
                </tr>
                
				<tr>
					<td colspan="4" height="20"></td>
				</tr>
				<tr>
					<td colspan="4" align="center">
                    <input type="submit" name="data_process" id="data_process" value="Start Salary Re Processing" style="width:270px; height:45px" class="formbutton" /> <input type="hidden" onclick="countUp()" />
                    </td>
				</tr>
				<tr style="visibility:hidden">
					<td colspan="4" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
                    <td colspan="4" align="center">
                        <div align="center" id="messagebox"></div>
                        <div id="process_anim"></div>
                    </td>
				</tr>
			</table>
		</fieldset>
	</form>
	 </div>
     
    
        <div align="center" style="font-size:18px; width:300px; position:relative; height:40px; margin:5px 0;"></div>	
        <div id="data_panel"  class="demo_jui" style="margin-top:10px; width:300px; float:right"></div>
   
    
</div>
</body>
</html>