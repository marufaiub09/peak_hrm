<?php
include('../../includes/common.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}
if ( check_database_process_status()==1 )
{
	echo server_busy_process(); 
	die;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_salary.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../resources/jquery_dataTable/jquery.dataTables.js"></script>
     <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
    
    
</head>

	<script>
		
		function openpage_searchemp(page_link,title)
		{		
			var company=document.getElementById('cbo_company_name').value;
			var location=document.getElementById('cbo_location_name').value;	
			var salary_periods=document.getElementById('cbo_salary_periods').value;
			
			var piece_rate_emp=1;
			
							
			if(company==0 || company=='') { alert("Please Select The Company."); return false; }
			if(salary_periods==0 || salary_periods=='') { alert("Please Select The Salary Period."); return false; }
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../search_employee_multiple_piecerate.php?company='+company
			+'&location='+location+'&salaryreproces='+piece_rate_emp, title, 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#txt_emp_code').val(thee_id.value);	
			}
		}

	
	function generate_list_view(selected_id){
	
		 var selected_id_spl=selected_id.split("_"); 
		 var selected_id=selected_id_spl[2];
		 
	$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&id=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});
		}

	
	
	</script>



<body style="font-family:verdana; font-size:11px;">
<div  style="width:850px;">

<div style="width:500px;float:left">
	<div align="center" style="font-size:18px; width:500px; position:relative; height:40px; margin:5px 0;">
		Production Salary Processing Unit</div>	
	<form id="holiday_form" action="javascript: fnc_regular_process_production_salary();" >
		<fieldset style="width:500px;">
			<legend>Production Salary Process</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td> Select Company Name</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Company Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 order by company_name");
								$numrows=mysql_num_rows($company_sql);
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value="<? echo $r_company["id"];?>" <? if($numrows==1)echo "selected"; ?> ><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td > Select Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Location Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
                
				<tr>
					<td> Select Salary Period</td>
					<td>
						<select name="cbo_salary_periods" id="cbo_salary_periods" class="combo_boxes" style="width:150px " onchange="generate_list_view(this.value)">
							<option value="0">--- Select Salary Period ---</option>
							<?
								$company_sql= mysql_db_query($DB, "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id  and b.type=1 and b.is_locked=0 and a.is_locked=0");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo "$r_company[actual_starting_date]"."_"."$r_company[actual_ending_date]"."_"."$r_company[year_id]";
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? $ex=explode(" ",$r_company[starting_date]); echo "$ex[0]"; ?> </option>
								<?
								}
							?>
					  </select>	&nbsp;Re-process<input type="checkbox" name="chk_reprocess" id="chk_reprocess" />
					</td>
				</tr>
                <tr>
				  <td>Employee Code</td>
				  <td><input name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:240px " placeholder="Double Click To Search" readonly ondblclick="openpage_searchemp('../search_employee_multiple_piecerate.php?','Search Employee')" ></td>
			  </tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" name="data_process" id="data_process" value="Start Processing" style="width:270px; height:45px" class="formbutton" /> </td>
					
				</tr>
				<tr style="visibility:hidden">
					<td colspan="2" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<div align="center" id="messagebox"></div>
						<div id="process_anim"></div>
					</td>
				</tr>
			</table>
		</fieldset>
		
	</form>
	</div>
    <div align="center" style="font-size:18px; width:300px; position:relative; height:40px; margin:5px 0;"></div>	
    <div id="data_panel"  class="demo_jui" style="margin-top:10px; width:300px; float:right"></div>
</body>
</html>