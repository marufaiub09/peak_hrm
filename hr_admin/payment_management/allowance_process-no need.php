<?php
include('../../includes/common.php');
if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}

	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_salary.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
		
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
    
	<script>
	
function countUp() {

 month = 'Jan';      // change the month to the one you want in the same format as shown
 date = '01';        // change the date to the one you want in the same format as shown
 year = '2002';      // change the year to the one you want in the same format as shown

 theDate = month + ' ' + date + ' ' + year;

   now = new Date();
   setdate = new Date(theDate);

   timer = (now - setdate) / 1000 / 60 / 60 / 24;
   timer = Math.round(timer);

document.getElementById("secs").innerHTML=timer;

  }


	function openpage_searchemp(page_link,title)
		{		
			var company=document.getElementById('cbo_company_name').value;
			var location=document.getElementById('cbo_location_name').value;	
			var salary_periods=document.getElementById('cbo_salary_periods').value;
							
			if(company==0 || company=='') { alert("Please Select The Company."); return false; }
			if(salary_periods==0 || salary_periods=='') { alert("Please Select The Salary Period."); return false; }
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../search_employee_multiple_salary_process.php?company='+company+'&location='+location+'&salaryreproces=1', title, 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#txt_emp_code').val(thee_id.value);	
			}
		}

	function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/salary_processing.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_salary_periods').html( html )
				}
			});
		}


</script>

</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center">
	<div align="center" style="font-size:18px; width:500px; position:relative; height:40px; margin:5px 0;">Cost Center Wise Salary Process</div>	
	<form id="holiday_form" action="javascript:fnc_allowance_process_salary();" method="POST">
		<fieldset style="width:500px;">
			<legend>Salary Re Process</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td align="right"> Select Company Name</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Company Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 order by company_name");
								$numrows=mysql_num_rows($company_sql);
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value="<? echo $r_company["id"];?>" <? if($numrows==1)echo "selected"; ?>><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td  align="right"> Select Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:250px">
							<option value="0">--- Select Location Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				 
                
				<tr>
					<td align="right"> Allowance Date From</td>
					<td><input type="text" name="txt_from_date" id="txt_from_date" class="datepicker" style="width:100px" />&nbsp;&nbsp;To &nbsp;<input type="text" name="txt_to_date" id="txt_to_date" class="datepicker" style="width:100px" /></td>
				</tr>
				<tr>
				  <td align="right">Employee Code</td>
				  <td><input name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:240px " placeholder="Double Click To Search" readonly ondblclick="openpage_searchemp('../search_employee_multiple_salary_process.php?salaryreproces=1','Search Employee')" ></td>
			  </tr>
              <tr>
					<td align="right"> OT Rate Rounding Policy</td>
					<td>
						<input type="text" class="text_boxes_numeric" style="width:45px" value="2" id="txt_precision" />&nbsp;&nbsp; Digit&nbsp;&nbsp;<select name="cbo_ot_rate_rounding" id="cbo_ot_rate_rounding" class="combo_boxes" style="width:150px ">
							<option value="1"> Rounding</option>
                            <option value="2"> Round UP</option>
                            <option value="3"> Round Down</option>
                            <option value="4"> Cut</option>
					  </select>
                      
					</td>
				</tr>
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" name="data_process" id="data_process" value="Start Salary Re Processing" style="width:270px; height:45px" class="formbutton" /> <input type="hidden" onclick="countUp()" /></td>
					
				</tr>
				<tr style="visibility:hidden">
					<td colspan="2" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<div align="center" id="messagebox"></div>
						<div id="process_anim"></div>
					</td>
				</tr>
			</table>
		</fieldset>
		
	</form>
	</div>
</body>
<script>
$(document).ready(function() {
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});
</script>
</html>