<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');
if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}

//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = $row;
	
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
		
	<script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
		
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../js/modal.js"></script>
    
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var head_counter = 0, salary_heads = all_heads = '<option value="0">&nbsp;</option>';
		<?php 
		foreach( $payroll_heads AS $payroll_id => $payroll ) {
			if( $payroll['salary_head'] == 1 ) echo "salary_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';";
			echo "all_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';";
		}
		?>
		
		
		$(document).ready(function() {
            $('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});			
			
        });
	
	function daysInMonth(month,year) 
	{
		var m = [31,28,31,30,31,30,31,31,30,31,30,31];
		if (month != 2) return m[month - 1];
		if (year%4 != 0) return m[1];
		if (year%100 == 0 && year%400 != 0) return m[1];
		return m[1] + 1;
	}
		
	function openpage_search_fraction_month(page_link,title)
		{		
			var company=document.getElementById('cbo_company_name').value;
			var location=document.getElementById('cbo_location_name').value;
			if(company==0 || company==''){$("#messagebox").removeClass().addClass('messagebox').text('Please Select The Company.').fadeIn(1000);return false;}
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_fraction_month.php?company='+company+'&location='+location, title, 'width=985px,height=400px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				//2_1_1_0_2014-05-01_2014-05-08_2014-05-06_1_120_1_1_10000_500_
				
				var thee_loc = this.contentDoc.getElementById("hidden_id").value;
				var split_arr=thee_loc.split('_');
				$('#txt_system_id').val(split_arr[0]);
				$('#cbo_company_name').val(split_arr[1]);
				$('#cbo_location_name').val(split_arr[2]);
				$('#cbo_emp_category').val(split_arr[3]);
				
				var arr_split = split_arr[4].split('-');
				var lastdayinmonth = daysInMonth(arr_split[1],arr_split[0]);
				var new_salary_periods=split_arr[4]+'_'+arr_split[0]+'-'+arr_split[1]+'-'+lastdayinmonth; 
				//alert (new_salary_periods);
				$('#cbo_salary_periods').val(new_salary_periods);
				
				$('#txt_attnd_date').val(split_arr[5]);
				$('#txt_ot_ason_date').val(split_arr[6]);
				$('#cbo_ot_included').val(split_arr[7]);
				//var ot_hrs = split_arr[8]/60;
				$('#txt_ot_hrs').val(split_arr[8]);
				$('#cbo_adsent_deduction').val(split_arr[9]);
				$('#cbo_late_deduction').val(split_arr[10]);
				
				$('#txt_salary_limit').val(split_arr[11]);
				$('#txt_rounding_amount').val(split_arr[12]);
				$('#cbo_advance_deduction').val(split_arr[13]);
				
				$('#txt_remarks').val(split_arr[14]);	
			}
		}
	
	
	function fnc_fraction_salary_process(save_perm,edit_perm,delete_perm,approve_perm)
	 {
		var cbo_company_name 		=  $("#cbo_company_name").val();
		var cbo_location_name 		=  $("#cbo_location_name").val();
		
		var cbo_salary_periods 		=  $("#cbo_salary_periods").val();	
		var txt_attnd_date 			=  $("#txt_attnd_date").val();
		var txt_ot_ason_date 		=  $("#txt_ot_ason_date").val();
		var txt_remarks	 			=  $("#txt_remarks").val();
		var cbo_ot_included 		=  $("#cbo_ot_included").val();
		var txt_ot_hrs 				=  $("#txt_ot_hrs").val();
		var cbo_adsent_deduction 	=  $("#cbo_adsent_deduction").val();
		var cbo_late_deduction 		=  $("#cbo_late_deduction").val();
		var cbo_emp_category 		=  $("#cbo_emp_category").val();
		var txt_system_id 			=  $("#txt_system_id").val();
		var txt_salary_limit 		=  $("#txt_salary_limit").val();
		var txt_rounding_amount 	=  $("#txt_rounding_amount").val();
		var cbo_deduct_loan 		=  $("#cbo_advance_deduction").val();
		
		
		
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$(this).html('Process Started, Please wait...').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
			
		if($('#cbo_company_name').val()==0){						
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_name').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});		
		}
		else if($('#cbo_salary_periods').val()==0)
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_salary_periods').focus();
				$(this).html('Please Select Effective Period.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if($('#txt_attnd_date').val()==0)
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_attnd_date').focus();
				$(this).html('Please Select Attendance Date On.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}		 
		else
		{		  
			var data = '&cbo_company_name='+cbo_company_name+
					   '&cbo_location_name='+cbo_location_name+
					   '&cbo_salary_periods='+cbo_salary_periods+
					   '&txt_attnd_date='+txt_attnd_date+
					   '&txt_ot_ason_date='+txt_ot_ason_date+
					   '&txt_remarks='+txt_remarks+
					   '&cbo_ot_included='+cbo_ot_included+
					   '&txt_ot_hrs='+txt_ot_hrs+
					   '&cbo_adsent_deduction='+cbo_adsent_deduction+
					   '&cbo_late_deduction='+cbo_late_deduction+
					   '&cbo_emp_category='+cbo_emp_category+
					   '&txt_salary_limit='+txt_salary_limit+
					   '&txt_rounding_amount='+txt_rounding_amount+
					   '&cbo_deduct_loan='+cbo_deduct_loan+
					   '&txt_system_id='+txt_system_id;			 
					  
			//alert (data); return;     
			//fraction_salary_processing_fakir_fashion
			$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
			nocache = Math.random();
			http.open('GET','includes/fraction_salary_processing.php?action=fraction_month_salary'+ data + '&nocache='+nocache);				
						
			http.onreadystatechange = fnc_fraction_salary_process_response;
			http.send(null); 
		}
	 }
	 
	 
	function fnc_fraction_salary_process_response() {
		if(http.readyState == 4) {
			var response = (http.responseText).split('*');	
			$("#process_anim").html('');
			if( response[1] == 9 ) {
				$("#messagebox").fadeTo( 200, 0.1, function() {
					$(this).html('Salary Period and Attendance Date Does not Match.').addClass('messagebox_ok').fadeTo( 900, 1 );
				});
			}
			if( response[1] == 1 ) {
				$("#messagebox").fadeTo( 200, 0.1, function() {
					$(this).html('Fraction Salary Process Successfully Done.').addClass('messagebox_ok').fadeTo( 900, 1 );
				});
			}
			if( response[1] == 2 ) {
				$("#messagebox").fadeTo( 200, 0.1, function() {
					$(this).html('Caution!!!Already Fraction Salary Process This Periods.').addClass('messagebox_ok').fadeTo( 900, 1 );
				});
			}
			
		}
	}
	
	
function generate_list_view(selected_id){
	
	var year_id =  $("#year_id").val();
		 var selected_id_spl=selected_id.split("_"); 
		 //var selected_id=selected_id_spl[2];
		 
		 var selected_id=year_id;
		 
	$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&id=' + selected_id,
				success: function( html ) {
					$('#data_panel2').html( html )
					
				}
			});
		}
		
									//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

function fn_change_othrs_field( myval )
{
	//alert (myval);
	if(myval==2)
	{
		$('#txt_ot_hrs').attr("disabled","disabled")
	}
	else
	{
		//$('#txt_ot_hrs').attr("disabled","disabled")
		$('#txt_ot_hrs').removeAttr('disabled');
	}
	
}



function fnc_change_date( myval )
{
	//alert (myval);
	$('#txt_ot_ason_date').val(myval);
	
}



function fnc_change_ot_date( myval )
{
	var txt_attnd_date 	=  $("#txt_attnd_date").val();
	//alert (myval);
	if(myval>txt_attnd_date)
	{
		alert ('Ot As On Date not greater than Salary As On Date.' );
		$('#txt_ot_ason_date').val(txt_attnd_date);
	}
}

	
					
	</script>
	 
</head>

<body style="font-family:verdana; font-size:11px;">

	<div style="width:100%;" align="center">
    
    	<div id="messagebox" style="background-color:#FF9999; color:#000000; width:900px" align="center"></div>

<div style="width:800px;float:left;"> 
   
    	<div class="form_caption">Fraction Month Salary Process</div>
        <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update.$delete; ?> </span>
		
		
	<form id="salary_breakdown_policy_form" action="javascript:fnc_fraction_salary_process(save_perm,edit_perm,delete_perm,approve_perm)" method="POST" autocomplete="off" >		
		<fieldset style="width:800px; margin-top:10px;">
			<legend>Fraction Salary Information</legend>
			<table width="800" >
            	<tr>
                	<td colspan="6" align="left"><b>Discloser</b><br />
                    	 1. This Process will Generate Fraction Salary for Selected Days Only.<br />
                         2. Here Attendance Bonus will not be Calculated.<br />
                         3. Separated Employee Will not Come Here.<br />
                         4. This Salary Amount will be adjusted as advance with Monthly Salary.<br />
                         5. Piece Rate Employee will not be included here.
                	</td>
                </tr>
                <tr>
                     <td colspan="7">&nbsp;</td>                                 	  
               	</tr>
                <tr>
                     <td colspan="6"  align="center"><b>System ID</b>                   
                      	<input name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:170px " placeholder="Double Click To Reprocess" readonly ondblclick="openpage_search_fraction_month('search_fraction_month.php','Search Fraction Month')" >
                     </td>            	  
               	</tr>
            	<tr>
            	  <td><b>Company Name</b></td>
            	  <td>
                  	<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:180px">
                        	<? if($company_cond=="")
							   { 
								?> 
								<option value="0">----- Select -----</option>
							<?
							   }
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 $company_cond order by company_name");
								$numrows=mysql_num_rows($company_sql);
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value="<? echo $r_company["id"];?>" <? if($numrows==1)echo "selected"; ?>><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
                  </td>
            	  <td><b>Location Name</b></td>
            	  <td>
                  	<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:180px ">
							<option value="0">----- Select -----</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
				    </select>	
                  </td>            	  
          	  </tr>
            	<tr>
                <td><b>Emp Category</b></td>  
                <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:180px;">
                         	 <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
            	  <td><b>Effective Period </b></td>            	  
                	<td><select name="cbo_salary_periods" id="cbo_salary_periods" class="combo_boxes" style="width:180px" onchange="generate_list_view(this.value)">
						<option value="0">----- Select -----</option>
							<?
								$company_sql= mysql_db_query($DB, "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id  and b.type=1 and b.is_locked=0 and a.is_locked=0");
								while ($r_company=mysql_fetch_array($company_sql))
								{
									$year_id=$r_company[year_id];
								?>
								<option value=<? echo "$r_company[actual_starting_date]"."_"."$r_company[actual_ending_date]";//"."_"."$r_company[year_id]
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[starting_date]"."-"."$r_company[ending_date]"; ?> </option>
								<?
								}
							?>
					  </select>		
                      
                      <input type="hidden" id="year_id" value="<? echo $year_id;?>" />		
                    </td>
                	
                </tr>
                <tr>
                 <td><b>Salary As On Date </b></td>
                  <td><input type="text" style="width:170px;" id="txt_attnd_date" name="txt_attnd_date" class="datepicker" placeholder="Select Date"  onchange="fnc_change_date(this.value);"/></td>
                  <td><b>Ot As On Date </b></td>
                  <td><input type="text" style="width:170px;" id="txt_ot_ason_date" name="txt_ot_ason_date" class="datepicker" placeholder="Select Date" onchange="fnc_change_ot_date(this.value);"/></td>
                </tr>
                
                <tr>
                <td><b>Absent Deduction </b></td>
                    <td>
                    	<select name="cbo_adsent_deduction" id="cbo_adsent_deduction" class="combo_boxes" style="width:180px">
                      		<option value="1">Yes</option>	
                            <option value="2">No</option>
                    	</select>
                    </td>
                  <td><b>Late Deduction </b></td>
                  <td><select name="cbo_late_deduction" id="cbo_late_deduction" class="combo_boxes" style="width:180px">
                    <option value="1">Yes</option>
                    <option value="2">No</option>
                  </select></td>
                  
                </tr>
                
                <tr>
                	 <td><b>OT Included </b></td>            	  
                	<td>
                    	<select name="cbo_ot_included" id="cbo_ot_included" class="combo_boxes" style="width:180px" onchange="fn_change_othrs_field(this.value);">
							<option value="1">Yes</option>	
                            <option value="2">No</option>							
					  	</select>
                    </td>
                     <td><b>OT Hrs Up to</b></td>            	  
                	<td>
                    	<input type="text" style="width:170px;" id="txt_ot_hrs" name="txt_ot_hrs" class="text_boxes" onkeypress="return numbersonly(this,event)"  />
                    </td>
                </tr>
                
                
                <tr>
                	 <td><b>Entitle Gross Salary Limit</b></td>            	  
                	<td>
                    	<input type="text" style="width:170px;" id="txt_salary_limit" name="txt_salary_limit" class="text_boxes" onkeypress="return numbersonly(this,event)" />
                    </td>
                     <td><b>Amt. Rounding To</b></td>            	  
                	<td>
                    	<input type="text" style="width:170px;" id="txt_rounding_amount" name="txt_rounding_amount" class="text_boxes" onkeypress="return numbersonly(this,event)" />
                    </td>
                </tr>
                
                
                
                <tr>
                  <td><b>Advance Deduction</b></td>
                  <td>
                 		 <select name="cbo_advance_deduction" id="cbo_advance_deduction" class="combo_boxes" style="width:180px" onchange="fn_change_othrs_field(this.value);">
							<option value="1">Yes</option>	
                            <option value="2">No</option>							
					  	</select>
                  </td>
                  <td><b>Remarks</b></td>
                  <td><input type="text" id="txt_remarks" name="txt_remarks" class="text_boxes" style="width:170px;"  /></td>
                </tr>                
                <tr>
                     <td colspan="6">&nbsp;</td>                                 	  
               	</tr>
                <tr>
                	<td colspan="6" align="center">
                    <input type="submit" name="data_process" id="data_process" value="Start Salary Processing" style="width:270px; height:45px" class="formbutton" /> 
                    	 
                	</td>
                </tr>                
				<tr>
					<td colspan="6" align="center">					
						<div id="process_anim"></div>
					</td>
				</tr>
            </table>
            <br />
            
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:800px; margin-top:15px;"></div>
    </div>
    <div style="height:60px;"></div>
    <div id="data_panel2"  class="demo_jui" style="margin-top:10px; width:250px; float:right"></div>
    </div>
</body>
</html>