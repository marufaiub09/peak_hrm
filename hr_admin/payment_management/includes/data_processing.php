<?php

session_start();
include('../../../../includes/common.php');
include('../../../../includes/array_function.php');

$e_date = time();
$user_only = $_SESSION['logic_erp']["user_name"];
$user_id_id=$_SESSION['logic_erp']["user_id"];

extract( $_GET );

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Division
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Weekly Holiday
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$weekend_lists[] = $row['weekend'];
	}
//Govt Holiday
	$sql = "SELECT * FROM lib_holiday_details WHERE is_deleted = 0 ORDER BY holiday_date ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday_lists[] = $row['holiday_date'];
	}
//Att+OT+shift Policy
	$sql = "SELECT * FROM hrm_policy_tagging WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_policy = array();
	$duty_roster_policy = array();
	$overtime_policy=array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_policy[$row['emp_code']] = $row['shift_policy'];
		$duty_roster_policy[$row['emp_code']] = $row['duty_roster_policy'];
		$overtime_policy[$row['emp_code']] = $row['overtime_policy'];
	}
// Disciplinary Info
function check_dsciplinary_info($emp_code, $current_date)
{
	$sql = "SELECT * FROM hrm_disciplinary_info_mst WHERE emp_code=$emp_code and action_date>=$current_date and action_taken=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		if ( $row['withdrawn_date']=='0000-00-00')

		{
			$suspended="SP";
			return $suspended;
			exit();
		}
		else if($row['withdrawn_date']<=$current_date )
		{
			$suspended="SP";
			return $suspended;
			exit();
			
		}
	}
}
// Movement Register
function check_movement_info($emp_code, $current_date)
{
	$sql = "SELECT * FROM hrm_movement_register WHERE emp_code='$emp_code' and is_deleted=0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if ($current_date==$row[move_current_date])
		{
			$suspended="MR";
			return $suspended;
			exit();
		}
	}
}

/*
Used Short  Terms 
A=Absent, P=Present, D=Late/Delay, CL,ML,SL,EL...=All Leaves,SP=Suspended, H=Holiday, W=Weekend, R=Regular Day, MR=Movement Register

*/
// Processing Started Here

if ($action=="data_processing")
{   
	$txt_from_date = convert_to_mysql_date( $txt_from_date );
	$txt_to_date = convert_to_mysql_date( $txt_to_date );
	
	$tot_days=daysdiff($txt_from_date, $txt_to_date);
 	
	// $EditSqltmp="delete from hrm_employee  where emp_code not in (0000068,0000854,0000032,0001693,0000056,0002090,0000054,0000052,0000058,0001684,0000078,0000022,0000062,0000059,0002098,0000090,0000045,0000013,0001766,0001765,0001763,0001346,0000020,0000015,0000024,0001353,0001351,0002095,0000002,0001348,0000046,0001767,0001753,0001757,0000041,0000012,0000050,0000030,0000040,0000043,0000081,0000080,0000082,0001764,0000084,0001761,0000003,0000079,0000087,0000086,0000044,0000053,0001704,0001352,0000014,0000071,0000063,0000077,0000010,0000085,0000074,0000065,0001347,0000072,0001349,0002089,0002092,0002083,0001759,0001366,0001536,0000025,0002097,0000048,0000008,0000057,0000021,0000038,0000067,0000854,0000015,0000016,0001755,0001687,0000009,0000064,0001758,0001356,0001762,0001685,0001703,0001700,0001358,0001689,0001760,0001690,0001357,0000073,0000011,0000042,0001754)"; 
	 //$ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
	 //echo "select CONCAT(a.first_name, ' ', a.middle_name, ' ', a.last_name) AS name , a.status_active, a.designation_id, b.* from hrm_employee a,hrm_employee_job b, hrm_policy_tagging c where a.emp_code=b.emp_code and a.emp_code=c.emp_code   order by a.emp_code desc";
	 
	//Blank -Make All Employee available to Attendance Table Start
	$sql_emp_abs="select CONCAT(a.first_name, ' ', a.middle_name, ' ', a.last_name) AS name , a.status_active, a.designation_id, b.* from hrm_employee a,hrm_employee_job b, hrm_policy_tagging c where a.emp_code=b.emp_code and a.emp_code=c.emp_code   order by a.emp_code desc" ; // and a.emp_code='0000012'  in (select emp_code from hrm_policy_tagging) and a.emp_code='0001764' ";
	
	$exe_sql_emp_abs=mysql_db_query($DB, $sql_emp_abs);
	$emp_rows=mysql_num_rows($exe_sql_emp_abs);
	
	while ($row_abs = mysql_fetch_array($exe_sql_emp_abs))
	{
		//Shift Get Current  In and Out Time Policy for This EMplie
		$r_in_time="";
		$r_out_time="";
		$r_shift_type="";
		$in_grace_minutes="";
		$exit_buffer_minutes="";
		$shift_id=$shift_policy[$row_abs[emp_code]];
	
		$sql_shift = "SELECT * FROM lib_policy_shift where id=$shift_id";
		$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
		while( $row_shift = mysql_fetch_array( $result_shift ) ) 
		{
			$r_in_time=$row_shift[shift_start];
			$r_out_time=$row_shift[shift_end];
			$r_shift_type=$row_shift[shift_type];
			$in_grace_minutes =add_time( $row_shift[shift_start], $row_shift[grace_minutes]); // Add In Grace Time with In Time
			$exit_buffer_minutes=$row_shift[exit_buffer_minutes];
		}
		
		for($i=1; $i<=$tot_days;$i++) // Loop for every days for each employee
		{
			$tmp_day_sts="";
			$is_regular_day=0;  // Set as not Regular working days
			
			$current_date=add_date($txt_from_date,$i-1);
			$weekday_name = date('l', strtotime($current_date));
		
			// Check if Data Exist in Attendance Table
			$sql_check_attn="select * from hrm_attendance where emp_code='$row_abs[emp_code]' and attnd_date='$current_date' ";
			$exe_sql_check_attn=mysql_db_query($DB, $sql_check_attn);
			$row_attn=mysql_num_rows($exe_sql_check_attn);
			
			if ($row_attn<1) // if not Exist , Insert as new Row
			{
				// Leave checking here  (As per Day sts is Saved Here Checking First)
					$sql = "SELECT * FROM hrm_leave_transaction_details WHERE emp_code='$row_abs[emp_code]' and leave_date='$current_date' and is_deleted = 0 and is_locked= 0 ORDER BY emp_code ASC";
					$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
					if($row = mysql_fetch_array($result))
					{
						$tmp_day_sts = $row['leave_status'];
						if ($tmp_day_sts=="Holiday")
						{
							$tmp_day_sts="H";	
						}
						else if ($tmp_day_sts=="Weekend")
						{
							$tmp_day_sts="W";	
						}
					}
				// Govt Holi checking here
				
					else if ( in_array( $current_date, $holiday_lists ))
					{
						$tmp_day_sts="H";
					}
				// Weekly Holi checking here
					else if (   in_array( $weekday_name, $weekend_lists ))
					{
						$tmp_day_sts="W";
					}
				// Otherwise set as Absent days
					else
					{
						$tmp_day_sts="A";
						$is_regular_day=1; // Set as  Regular working days
					}
					
					$tmp=check_dsciplinary_info($row_abs[emp_code], $current_date);
					if ($tmp!="")
					{
						$tmp_day_sts=$tmp;
						$tmp="";
					}
					
					$tmp=check_movement_info($row_abs[emp_code], $current_date);
					if ($tmp!="")
					{
						$tmp_day_sts=$tmp;
						$tmp="";
					}
					
					/*	
					if ($tmp_day_sts=="A")
					{
						$tmp=check_dsciplinary_info($row_abs[emp_code], $current_date);
						if ($tmp!="")
						{
							$tmp_day_sts=$tmp;
							$tmp="";
						}
						else
						{
							$tmp=check_movement_info($row_abs[emp_code], $current_date);
							if ($tmp!="")
							{
								$tmp_day_sts=$tmp;
								$tmp="";
							}
						}
					}*/
					//echo "$row_abs[name]"."$row_abs[status_active]"."<br>";
					
					//if ($row_abs[status_active]==0) // In active Employee
					//{
						 
					/*	$d="separated_from";
						$dd="hrm_separation";
						$ddd="emp_code='$row_abs[emp_code]'";
						$old_mod= return_field_value($d,$dd,$ddd);
						
						if (strtotime($old_mod)>strtotime($current_date))
						{	
							//echo "Sumon $old_mod ";
					//exit();
							$id_field_name = "id";
							$table_name = "hrm_attendance";
							$row_id = return_next_id( $id_field_name, $table_name );
								
							 $sql = "INSERT INTO hrm_attendance (id,emp_code,attnd_date,r_sign_in_time,r_in_time_graced,r_sign_out_time,r_out_time_buffer,status,is_regular_day,policy_shift_id,policy_overtime_id,roster_policy_id,is_manually_updated,company_id,location_id,division_id,department_id,section_id,subsection_id,designation_id,weekday_name)			
								 VALUES ('$row_id','$row_abs[emp_code]','$current_date','$r_in_time','$in_grace_minutes','$r_out_time','$exit_buffer_minutes','$tmp_day_sts','$is_regular_day','$shift_id','$ot_policy','$roster',0,'$row_abs[company_id]','$row_abs[location_id]','$row_abs[division_id]','$row_abs[department_id]','[$row_abs[section_id]','$row_abs[subsection_id]','$row_abs[designation_id]','$weekday_name')";
							 mysql_query( $sql ) or die (mysql_error());
						}
					//}*/
					//else  // Regular Employee
					//{
					 	$id_field_name = "id";
						$table_name = "hrm_attendance";
						$row_id = return_next_id( $id_field_name, $table_name );
							
						 $sql = "INSERT INTO hrm_attendance (id,emp_code,attnd_date,r_sign_in_time,r_in_time_graced,r_sign_out_time,r_out_time_buffer,status,is_regular_day,policy_shift_id,policy_overtime_id,roster_policy_id,is_manually_updated,company_id,location_id,division_id,department_id,section_id,subsection_id,designation_id,weekday_name)			
							 VALUES ('$row_id','$row_abs[emp_code]','$current_date','$r_in_time','$in_grace_minutes','$r_out_time','$exit_buffer_minutes','$tmp_day_sts','$is_regular_day','$shift_id','$ot_policy','$roster',0,'$row_abs[company_id]','$row_abs[location_id]','$row_abs[division_id]','$row_abs[department_id]','$row_abs[section_id]','$row_abs[subsection_id]','$row_abs[designation_id]','$weekday_name')";
						 mysql_query( $sql ) or die (mysql_error());
					//}
				 $tmp_day_sts="";
				 $is_regular_day=""; 
				 $weekday_name="";
				
			}// No Updates is available for Attendance here
		
		}
		//echo $tot_days;
		//exit();
	}
	//Blank -Make All Employee available to Attendance Table End
	  
 
	 
	//Check raw Data to update daily atttendance with time starts
	 
$sql_emps="select punch_card_no,emp_code from hrm_employee   order by emp_code asc"; //where emp_code='0000012' 
$exe_sql_emps=mysql_db_query($DB, $sql_emps);
$rowsss=mysql_num_rows($exe_sql_emps);
 $t=1;

while ($r_emps_data = mysql_fetch_array($exe_sql_emps))
{	
	for($r=1; $r<=$tot_days; $r++)
	{	
		$attnd_date =change_date_for_process(add_date($txt_from_date,$r-1));
//convert_to_mysql_date($attnd_date)

		$sql_id_emp_prs="select * from hrm_raw_data_attnd where dtime='$attnd_date' and processed=0 and cid='$r_emps_data[punch_card_no]' order by ctime asc";
		$exe_sql_id_emp_prs=mysql_db_query($DB, $sql_id_emp_prs);
		$rowss=mysql_num_rows($exe_sql_id_emp_prs);
	// echo "$rowss asd $attnd_date \n";
	// exit();
		//echo "asd $rowss \n";
  //echo "$attnd_date Sumon $rowss  card- $r_emps_data[punch_card_no] \n";
	 //exit();
		while ($r_id_prs = mysql_fetch_array($exe_sql_id_emp_prs))
		{	
			$r_in_time=$r_id_prs[shift_start];
			$r_out_time=$r_id_prs[shift_end];
			$is_next_day=$r_id_prs[is_next_day];
			$r_shift_type=$r_id_prs[shift_type];
			$r_in_graced_time =add_time( $r_id_prs[shift_start], $r_id_prs[graced_minutes]); // Add In Grace Time with In Time
			$r_in_grace_minutes= $r_id_prs[graced_minutes];
			$r_exit_buffer_minutes=$r_id_prs[exit_buffer_minutes];
			
			$tmp_log_time=change_time_for_process($r_id_prs[ctime]);
			$tmp_log_date= $attnd_date;
			
			
			
			$max_last_punch_time_night="03:59:59"; // GLOBAL
			$max_in_time_buffer_before=90;
			$max_day_close_time="23:59:59";
			$max_in_punch_time_allowed=180;
			$is_prev_day_log=0;
				
			if ($is_next_day==0)  // Out and in time in same date 
			{
				if ($tmp_log_time>$m_last_punch_time_night and $tmp_log_time<=$max_day_close_time)
				{
					$timeDiffin=datediff(n,$r_in_time,$tmp_log_time);
					if ($timeDiffin >=-$max_in_time_buffer_before and $timeDiffin<=$max_in_punch_time_allowed) 
					{
						$gen_in_time=$tmp_log_time;
						
						if ($timeDiffin>$r_in_grace_minutes)
						{
							$gen_late_time=$timeDiffin; //+$r_in_grace_minutes;
							$timeDiffin="";
						}
					}
					else //if ($timeDiffout<=120 or $timeDiffout>=120) // Regular Out Time Setting Here
					{
						$gen_out_time=$tmp_log_time;
						
						$timeDiffout=datediff(n,$r_out_time,$tmp_log_time);
						if ($timeDiffout>0)
						{
							$gen_ot_time=$timeDiffout;
						}
					}
				}
				else  // Mid Night Punch // Out time for Previous Day
				{
					$gen_out_time=$tmp_log_time;
					$timeDiffout=datediff(n,$r_out_time,$max_day_close_time);
					$p_time="00:00:00";
					$timeDiffout=$timeDiffout+datediff(n,$p_time,$tmp_log_time);
					
					if ($timeDiffout>0)
					{
						$gen_ot_time=$timeDiffout;
					}
					$is_prev_day_log=1;
				}
			}
			else  // Next Day	 starts
			{
				$timeDiffin=datediff(n,$r_in_time,$tmp_log_time);
				if ($timeDiffin >=-$max_in_time_buffer_before and $timeDiffin<=$max_in_punch_time_allowed) 
				{
					$gen_in_time=$tmp_log_time;
					
					if ($timeDiffin>$r_in_grace_minutes)
					{
						$gen_late_time=$timeDiffin; //+$r_in_grace_minutes;
						$timeDiffin="";
					}
				}
				else //if ($timeDiffout<=120 or $timeDiffout>=120) // Regular Out Time Setting Here
				{
					$gen_out_time=$tmp_log_time;
					
					$timeDiffout=datediff(n,$r_out_time,$tmp_log_time);
					if ($timeDiffout>0)
					{
						$gen_ot_time=$timeDiffout;
					}
				}
				
			}
			
			// Next Day ends
			//echo "In: $gen_in_time Late $gen_late_time \n";
			  //exit();
			//echo "ID: $r_id_prs[cid] Date:$tmp_log_date Time: $tmp_log_time <br>\n";
			
			//$d="emp_code";
			//$dd="hrm_employee";
			//$ddd="punch_card_no='$r_id_prs[cid]'";
			
			//$old_mod= return_field_value($d,$dd,$ddd);
			
			$att_date=change_date_for_process_sql($attnd_date);
			 
			//echo "$att_date";
			// Start Searching Proev Attenandance in Att Table
			if ($is_prev_day_log==0)
			{	
				$sql_raw_attendance="select * from hrm_attendance where emp_code='$r_emps_data[emp_code]' and  attnd_date='$att_date' ";
			}
			else
			{
				//$pr_date=$r_id_prs[logdate];
				$p_date=add_date($att_date,-1);
				$sql_raw_attendance="select * from hrm_attendance where emp_code='$r_emps_data[emp_code]' and attnd_date='$p_date' ";
			}
			//echo "In:$sql_raw_attendance Out: $r_id_prs[cid] ";
			// exit();
			// echo "$t Hello $att_date \n";
	//exit(); 
			$exe_sql_raw_attendance=mysql_db_query($DB, $sql_raw_attendance);
			$row_num=mysql_num_rows($exe_sql_raw_attendance);
			$row_data_prs = mysql_fetch_array($exe_sql_raw_attendance);
			
			if ($row_num!=0)
			{	
				
				$prev_in_time=$row_data_prs[sign_in_time];
				$prev_late_min=$row_data_prs[late_time_min];
				
				$prev_out_time=$row_data_prs[sign_out_time];
				$prev_ot_min=$row_data_prs[total_over_time_min];
				$prev_status=$row_data_prs[status];
				//echo "iid: $r_id_prs[cid]  $is_prev_day_log GEn In: $gen_in_time Prev In : $prev_in_time GEn Out: $gen_out_time Prev Out : $prev_out_time \n ";
				if ($gen_in_time!="")
				{
					//$gen_in_time=$gen_in_time;
					
					if ($gen_in_time>$prev_in_time && $prev_in_time!="00:00:00")
					{
						$gen_in_time=$prev_in_time;
						$gen_late_time=$prev_late_min;
						$status="P";
						
					}
					$gen_out_time=$prev_out_time;
					$status="P";
				}
				else
				{
					//$gen_out_time=$gen_out_time;
					//$gen_ot_time=$gen_ot_time;
					$gen_in_time=$prev_in_time;
					$gen_late_time=$prev_late_min;
					$status="P";
				}
				
				if ($gen_late_time>$r_in_grace_minutes)
				{
					$status="D";
				}
				else
				{
					$gen_late_time=0;
				}
				//Set Questionable start
				if ($prev_in_time!="00:00:00" and  $gen_out_time!="00:00:00")
				{
					$is_questionable=0;
				}
				else if ($prev_out_time!="00:00:00" and  $gen_in_time!="00:00:00")
				{
					$is_questionable=0;
				}
				else
				{
					$is_questionable=1;
				}
				
				//if ($p_status=="W" or $p_status=="H")
				if ($row_data_prs[is_regular_day]==0)  
				{
					if ($questionable==0)
					{
						$gen_ot_time1=datediff(n,$gen_in_time,$r_out_time);
						$gen_ot_time=$gen_ot_time1+$gen_ot_time;
					}
					$status=$prev_status;
				}  // Holiday OT 
				
				if ($gen_ot_time=="")
				{
					$gen_ot_time=0;
				}
				if ($gen_late_time=="")
				{
					$gen_late_time=0;
				}
				
				$sid=$row_data_prs[id];
					//echo " rowid: $row_data_prs[id] <br>";
					//echo "$rowss asd $attnd_date time $gen_in_time \n";
				$EditSql="update hrm_attendance set sign_in_time='$gen_in_time',sign_out_time='$gen_out_time', total_over_time_min='$gen_ot_time', late_time_min='$gen_late_time', status='$status', is_questionable='$is_questionable' where id='$row_data_prs[id]'"; 
				$ExeEditSql=mysql_db_query($DB,$EditSql);
				//echo "$gen_in_time Sumon $r_emps_data[punch_card_no]";
	//exit();
				//$EditSqltmp="update hrm_raw_data_attnd set processed='1' where cl='$r_id_prs[cl]'"; 
				//$ExeEditSqltmp=mysql_db_query($DB,$EditSqltmp);
				//echo "success";
				$gen_in_time="";
				$gen_out_time="";
				$gen_ot_time="";
				$gen_late_time="";
				$prev_in_time="";
				$prev_out_time="";
				$status="";
				$is_questionable=""; 
				
			}// Start Searching Proev Attenandance in Att Table
			
		}// while loop r_id_prs
		
	} // for loop end
 $t=$t+1;
}// loop for employee */
	//Check raw Data to update daily atttendance with time ends
	echo "2";
	exit();  
}

// Functions required for this processing
function daysdiff($date1,$date2) 
{ 
	$diff = (strtotime($date2) - strtotime($date1));
	
	$days = floor($diff / (1*60*60*24)+1);
	return $days; 		
}

	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

function add_time($event_time,$event_length)
{
	
	$timestamp = strtotime("$event_time");
	$etime = strtotime("+$event_length minutes", $timestamp);
	$etime=date('H:i:s', $etime);
	return $etime;

}




function change_date_for_process( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "-", $date_field );
		return $dates[1] . "/" . $dates[2] . "/" . $dates[0];
	}
	else return $date_field;
}
function change_date_for_process_sql( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "/", $date_field );
		return $dates[2] . "-" . $dates[0] . "-" . $dates[1];
	}
	else return $date_field;
}
function change_time_for_process( $time_field ) {
	if( $time_field != '' ) {
		$times = explode( "-", $time_field );
		return $times[0] . ":" . $times[1] . ":" . $times[2];
	}
	else return $time_field;
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>