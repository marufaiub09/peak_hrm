<?php

session_start();
include('../../../includes/common.php');
include('../../../includes/array_function.php');

$e_date = time();
$user_only = $_SESSION['logic_erp']["user_name"];
$user_id_id=$_SESSION['logic_erp']["user_id"];

extract( $_GET );

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Division
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Weekly Holiday
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$weekend_lists[] = $row['weekend'];
	}
//Govt Holiday
	$sql = "SELECT * FROM lib_holiday_details WHERE is_deleted = 0 ORDER BY holiday_date ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday_lists[] = $row['holiday_date'];
	}
//Att+OT+shift Policy
	$sql = "SELECT * FROM hrm_policy_tagging WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_policy = array();
	$duty_roster_policy = array();
	$overtime_policy=array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_policy[$row['emp_code']] = $row['shift_policy'];
		$duty_roster_policy[$row['emp_code']] = $row['duty_roster_policy'];
		$overtime_policy[$row['emp_code']] = $row['overtime_policy'];
	}
// Disciplinary Info
function check_dsciplinary_info($emp_code, $current_date)
{
	$sql = "SELECT * FROM hrm_disciplinary_info_mst WHERE emp_code=$emp_code and action_date>=$current_date and action_taken=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		if ( $row['withdrawn_date']=='0000-00-00')

		{
			$suspended="SP";
			return $suspended;
			exit();
		}
		else if($row['withdrawn_date']<=$current_date )
		{
			$suspended="SP";
			return $suspended;
			exit();
			
		}
	}
}
// Movement Register
function check_movement_info($emp_code, $current_date)
{
	$sql = "SELECT * FROM hrm_movement_register WHERE emp_code='$emp_code' and is_deleted=0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if ($current_date==$row[move_current_date])
		{
			$suspended="MR";
			return $suspended;
			exit();
		}
	}
}

// Salary Breakdown Employee Wise
	$salary_breakdown = array();
	$sql_sal="select * from hrm_employee_salary";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_breakdown[$row_sal[emp_code]][$row_sal[payroll_head]]=$row_sal[amount];
		  /*
		if ($row_sal[payroll_head]==1) $salary_breakdown[$row_sal[emp_code]]['basic_salary']=$row_sal[amount];
		else if ($row_sal[payroll_head]==2) $salary_breakdown[$row_sal[emp_code]]['house_rent']=$row_sal[amount];
		else if ($row_sal[payroll_head]==3) $salary_breakdown[$row_sal[emp_code]]['medical']=$row_sal[amount];
		else if ($row_sal[payroll_head]==4) $salary_breakdown[$row_sal[emp_code]]['conveyence']=$row_sal[amount];
		else if ($row_sal[payroll_head]==19) $salary_breakdown[$row_sal[emp_code]]['others']=$row_sal[amount];
		else if ($row_sal[payroll_head]==21) $salary_breakdown[$row_sal[emp_code]]['advance_deduction']=$row_sal[amount];
		else if ($row_sal[payroll_head]==31) $salary_breakdown[$row_sal[emp_code]]['others_deduction']=$row_sal[amount];
		else if ($row_sal[payroll_head]==22) $salary_breakdown[$row_sal[emp_code]]['advance_tax_deduction']=$row_sal[amount]; */
		//else 
	}
// All Salary Head for This Company	
	$salary_head_list = array();
	$sql_sal="select * from lib_payroll_head where is_applicable=1 and status_active=1 and is_deleted=0 order by id";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	$i=0;
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_head_list_head[$row_sal[payroll_head]]=$row_sal[payroll_head];
		  $salary_head_list_i[$i]=$row_sal[payroll_head];
		  $i++;
	}
/*
Used Short  Terms 
A=Absent, P=Present, D=Late/Delay, CL,ML,SL,EL...=All Leaves,SP=Suspended, H=Holiday, W=Weekend, R=Regular Day, MR=Movement Register

*/
// Processing Started Here

if ($action=="salary_processing")
{
	$cbo_salary_periods = explode('_', $cbo_salary_periods ); 
	$txt_from_date = convert_to_mysql_date( $cbo_salary_periods[0] );
	$txt_to_date = convert_to_mysql_date( $cbo_salary_periods[1] );
	$i=0;
	
 	$tot_days=daysdiff($txt_from_date, $txt_to_date);
	
 	 
	$sql_emp="select  * from hrm_employee order by emp_code desc"; //and a.emp_code='0000038'  and a.emp_code='0000015' 
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	$emp_salary_sheet=array();
	$emp_extra_details=array();
	while ($row_emp = mysql_fetch_array($exe_sql_emp))
	{ 
		// EMp Master Qry starts 
		$basic_salary=0; $house_rent=0; $medical=0; $others=0; $others_deduction=0;$advance_deduction=0; $gross_salary=0; $daily_gross_salary=0;$daily_basic_salary=0;
		$i++;
		
		if ($row_emp[status_active]==0) // In active Employee
		{
			$d="separated_from";
			$dd="hrm_separation";
			$ddd="emp_code='$row_emp[emp_code]'";
			$old_mod= return_field_value($d,$dd,$ddd);
			
			if (strtotime($old_mod)>strtotime($current_date))
			{
				
			}
		}
		
		$emp_salary_sheet[$row_emp[emp_code]][0]=$row_emp[gross_salary];
		
		for($j=0; $j<count($salary_head_list_i);$j++)
		{
			if($salary_head_list_i[$j]==1) $emp_salary_sheet[$row_emp[emp_code]][1]= $salary_breakdown[$row_emp[emp_code]][1];  // Basic
			else if($salary_head_list_i[$j]==2) $emp_salary_sheet[$row_emp[emp_code]][2]= $salary_breakdown[$row_emp[emp_code]][2];  // House Rent
			else if($salary_head_list_i[$j]==3) $emp_salary_sheet[$row_emp[emp_code]][3]= $salary_breakdown[$row_emp[emp_code]][3];  // Medical
			else if($salary_head_list_i[$j]==4) $emp_salary_sheet[$row_emp[emp_code]][4]= $salary_breakdown[$row_emp[emp_code]][4];  // Conveyence
			else if($salary_head_list_i[$j]==5) // General Over Time Buyer
			{
				$emp_salary_sheet[$row_emp[emp_code]][5]= get_total_ot_hr( $row_emp[emp_code],$txt_from_date, $txt_to_date,0 );  
				$emp_extra_details[$row_emp[emp_code]]['got_rate']= get_total_ot_rate( $row_emp[emp_code],$overtime_policy[$row_emp[emp_code]] ); 
				$emp_extra_details[$row_emp[emp_code]]['got_amount']=$emp_ot_details[$row_emp[emp_code]]['rate']*$emp_salary_sheet[$row_emp[emp_code]][5];
			}
			else if($salary_head_list_i[$j]==6) // Extra Over Time
			{
				$emp_salary_sheet[$row_emp[emp_code]][6]= get_total_ot_hr($row_emp[emp_code],$txt_from_date, $txt_to_date,1);  
				$emp_extra_details[$row_emp[emp_code]]['eot_rate']= get_total_ot_rate( $row_emp[emp_code],$overtime_policy[$row_emp[emp_code]] ); 
				$emp_extra_details[$row_emp[emp_code]]['eot_amount']=$emp_ot_details[$row_emp[emp_code]]['rate']*$emp_salary_sheet[$row_emp[emp_code]][5];
			}
			else if($salary_head_list_i[$j]==8) // Holiday Allowance
			{
				$emp_salary_sheet[$row_emp[emp_code]][6]= get_total_ot_hr($row_emp[emp_code],$txt_from_date, $txt_to_date,1);  
				$emp_extra_details[$row_emp[emp_code]]['nof_holi']= get_total_ot_rate( $row_emp[emp_code],$overtime_policy[$row_emp[emp_code]] ); 
				$emp_extra_details[$row_emp[emp_code]]['amount']=$emp_ot_details[$row_emp[emp_code]]['rate']*$emp_salary_sheet[$row_emp[emp_code]][5];
			}
			
		}
		
		
		 
		$hourly_ot_rate=($basic_salary/208)*2; // from Var Setting
		$daily_gross_salary=$gross_salary/$tot_days; // from Var Setting
		$daily_basic_salary=$basic_salary/$tot_days; // from Var Setting
		
		$sql_attnd=" SELECT sum(CASE WHEN total_over_time_min <>0 THEN total_over_time_min END) AS 'total_over_time_min', count(CASE WHEN status ='P' THEN emp_code END) AS 'present',count(CASE WHEN status ='A' THEN emp_code END) AS 'absent',count(CASE WHEN status ='D' THEN emp_code END) AS 'late',count(CASE WHEN status ='H' THEN emp_code END) AS 'holiday',count(CASE WHEN status ='W' THEN emp_code END) AS 'weekday',count(CASE WHEN status ='MR' THEN emp_code END) AS 'movement',count(CASE WHEN status ='CL' THEN emp_code END) AS 'cl',count(CASE WHEN status ='EL' THEN emp_code END) AS 'el',count(CASE WHEN status ='SL' THEN emp_code END) AS 'sl',count(CASE WHEN status ='ML' THEN emp_code END) AS 'ml',count(CASE WHEN status ='SpL' THEN emp_code END) AS 'spl',count(CASE WHEN status ='LWP' THEN emp_code END) AS 'lwp'  from hrm_attendance where emp_code='$row_emp[emp_code]' and attnd_date between '$cbo_salary_periods[0]' and '$cbo_salary_periods[1]' ";
		$exe_sql_attnd=mysql_db_query($DB, $sql_attnd);
		if ($row_attnd = mysql_fetch_array($exe_sql_attnd))
		{
			 $prsent_days=$row_attnd[present];
			 $absent_days=$row_attnd[absent];
			 $late_days=$row_attnd[late];
			 $holiday_days=$row_attnd[holiday];
			 $weekday_days=$row_attnd[weekday];
			 $movement_days=$row_attnd[movement];
			 $c_leave_days=$row_attnd[cl];
			 $s_leave_days=$row_attnd[sl];
			 $e_leave_days=$row_attnd[el];
			 $m_leave_days=$row_attnd[ml];
			 $spl_leave_days=$row_attnd[spl];
			 $lwp_leave_days=$row_attnd[lwp];
			 $total_over_time_min=$row_attnd[total_over_time_min];
			 $total_cal_days=$prsent_days+$absent_days+$late_days+$holiday_days+$weekday_days+$movement_days+$c_leave_days+ $s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days+$lwp_leave_days;
			 
		}
		if (daysdiff(convert_to_mysql_date($row_emp[joining_date]),$txt_to_date)<30)//if (date("d-m-Y",strtotime($txt_from_date))< date("d-m-Y",strtotime(convert_to_mysql_date($row_emp[joining_date]))))
		 	$total_payable_days=daysdiff(convert_to_mysql_date($row_emp[joining_date]),$txt_to_date);//-($absent_days+$lwp_leave_days);
		else
			$total_payable_days=$tot_days;//-($absent_days+$lwp_leave_days);
	 //echo "$tot_days"."<br> $total_payable_days".daysdiff(convert_to_mysql_date($row_emp[joining_date]),$txt_to_date); die;
		
		// All Earning Heads Calculation here
		
		
		$total_leave_days=$c_leave_days+$s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days;
		$total_earning_amount=$basic_salary+$medical+$house_rent+$others+$conveyence;
		
		// All Deduction Heads Here
		
		if ($absent_days>0) $abs_amount=$daily_basic_salary*$absent_days;    // from var sett Gross
		if ($lwp_leave_days>0) $lwp_amount=$daily_basic_salary*$lwp_leave_days;  // from var sett Gross
		if ($late_days>2) { $late_days_cal=(int)($late_days/3); $late_amount=$daily_basic_salary*$late_days_cal; }  // from var sett 3
		
		
		$net_deduction_amount=$abs_amount+$lwp_amount+$late_amount+$advance_tax_deduction+$others_deduction+$advance_deduction; 
		$net_payable_amount= ($total_payable_days*$daily_gross_salary)-$net_deduction_amount;
		$total_payable_days=$total_payable_days-($absent_days+$lwp_leave_days);
	 	
		// Insert into payslip_mst
		$row_id = return_next_id( "id", "hrm_salary_mst" );
		$sql = "INSERT INTO hrm_salary_mst (id,pay_slip_mst_id,emp_code,gross_salary,basic_salary,medical_allowance,house_rent,conveyence,total_earning_amount,total_deduction_amount,is_with_over_time,over_time_hour,over_time_rate,net_payable_amount,total_abs_days,total_late_days,total_lwp_days,total_holiday,total_weekly_holiday,daily_basic_salary,daily_gross_salary,total_working_days,total_calendar_days,total_late_deduction_amount,total_absent_deduction_amount,total_lwp_deduction_amount,total_leave_days,company_id,location_id,division_id,department_id,section_id,subsection_id,advance_deduction)			
							 VALUES ('$row_id','$pay_slip_mst_id','$row_emp[emp_code]','$gross_salary','$basic_salary','$medical','$house_rent','$others','$total_earning_amount','$net_deduction_amount','$is_with_over_time','$total_over_time_min','$hourly_ot_rate','$net_payable_amount','$absent_days','$late_days','$lwp_leave_days','$total_holiday','$weekday_days','$daily_basic_salary','$daily_gross_salary','$total_payable_days','$tot_days','$late_amount','$abs_amount','$lwp_amount','$total_leave_days','$row_emp[company_id]','$row_emp[location_id]','$row_emp[division_id]','$row_emp[department_id]','$row_emp[section_id]','$row_emp[subsection_id]','$advance_deduction')";
						 mysql_query( $sql ) or die (mysql_error());
		
		$gross_salary=0;$daily_basic_salary=0;$daily_gross_salary=0;$total_payable_days=0;$late_amount=0;$abs_amount=0;$lwp_amount=0;$total_leave_days=0;
		$basic_salary=0;$hourly_ot_rate=0;$net_payable_amount=0;$absent_days=0;$late_days=0;$lwp_leave_days=0;$total_holiday=0;$weekday_days=0;$total_leave_days=0;
		$medical=0; $house_rent=0;$conveyence=0; $total_payable_days=0; $total_earning_amount=0;$net_deduction_amount=0;$is_with_over_time=0;$total_over_time_min=0;
		$advance_tax_deduction=0;  
		
		$sql_sal="select * from hrm_employee_salary where emp_code='$row_emp[emp_code]'";
		$exe_sql_sal=mysql_db_query($DB, $sql_sal);
		while ($row_sal = mysql_fetch_array($exe_sql_sal))
		{
			$id = return_next_id( "id", "hrm_salary_dtls" );
			
			if ($row_sal[payroll_head]==1){ $is_deductive=0; $head_type=1;}
			else if ($row_sal[payroll_head]==2){ $is_deductive=0;$head_type=1;}
			else if ($row_sal[payroll_head]==3) {$is_deductive=0;$head_type=1;}
			else if ($row_sal[payroll_head]==4) {$is_deductive=0;$head_type=1;}
			else if ($row_sal[payroll_head]==19){$is_deductive=0;$head_type=0;}
			else if ($row_sal[payroll_head]==21) { $is_deductive=1;$head_type=0;}
			else if ($row_sal[payroll_head]==31){ $is_deductive=1;$head_type=0;}
			else if ($row_sal[payroll_head]==22) {$is_deductive=1;$head_type=0;}
			
			$sql="INSERT INTO hrm_salary_dtls (id,salary_mst_id,emp_code,salary_head_id,pay_amount,is_deducted,head_type,pay_slip_id) values ('$id','$row_id','$row_emp[emp_code]','$row_sal[payroll_head]','$row_sal[amount]','$is_deductive','$head_type','$id')";
			mysql_query( $sql ) or die (mysql_error());
			
		}
		 //.$sql;
	//if ($i==10) { exit(); }
	
	} // EMp Master Qry Ends 
  echo "$tot_days sss ";
	
	
}

// Functions required for this processing
function daysdiff($date1,$date2) 
{ 
	$diff = (strtotime($date2) - strtotime($date1));
	
	$days = floor($diff / (1*60*60*24)+1);
	return $days; 		
}

	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

function add_time($event_time,$event_length)
{
	
	$timestamp = strtotime("$event_time");
	$etime = strtotime("+$event_length minutes", $timestamp);
	$etime=date('H:i:s', $etime);
	return $etime;

}




function change_date_for_process( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "-", $date_field );
		return $dates[1] . "/" . $dates[2] . "/" . $dates[0];
	}
	else return $date_field;
}
function change_date_for_process_sql( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "/", $date_field );
		return $dates[2] . "-" . $dates[0] . "-" . $dates[1];
	}
	else return $date_field;
}
function change_time_for_process( $time_field ) {
	if( $time_field != '' ) {
		$times = explode( "-", $time_field );
		return $times[0] . ":" . $times[1] . ":" . $times[2];
	}
	else return $time_field;
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>