 
<?php

session_start();
include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');
include('salary_functions.php');

$e_date = time();
$user_only = $_SESSION['logic_erp']["user_name"];
$user_id_id=$_SESSION['logic_erp']["user_id"];

extract( $_GET );
  
//Weekly Holiday
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$weekend_lists[] = $row['weekend'];
	}
//Govt Holiday
	$sql = "SELECT b.holiday_date,a.type FROM lib_holiday a, lib_holiday_details b WHERE a.id=b.holiday_id and a.is_deleted = 0 and b.holiday_date between '".convert_to_mysql_date($cbo_salary_periods[0])."' and '".convert_to_mysql_date($cbo_salary_periods[1])."' ORDER BY b.holiday_date ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$i=0;
	$holiday_lists = array();$holiday_lists_type = array(); $holiday_lists_all = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
			$holiday_lists[] = $row[holiday_date];
			$holiday_lists_type[$row[holiday_date]] = $row[type];
			$holiday_lists_all[$row[type]][$i]= $row[holiday_date];
			$i++;
		
	}
 
// Shift Details
	$shift_lists = array(); 
	$sql_shift = "SELECT * FROM lib_policy_shift";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$shift_lists[$row_shift[id]]['shift_start']=$row_shift[shift_start];
		$shift_lists[$row_shift[id]]['shift_end']=$row_shift[shift_end];
		$shift_lists[$row_shift[id]]['shift_type']=$row_shift[shift_type];
		$shift_lists[$row_shift[id]]['in_grace_minutes'] =add_time( $row_shift[shift_start], $row_shift[grace_minutes]); // Add In Grace Time with In Time
		$shift_lists[$row_shift[id]]['exit_buffer_minutes']=$row_shift[exit_buffer_minutes];
		
		$shift_lists[$row_shift[id]]['tiffin_start']=$row_shift[first_break_start];
		$shift_lists[$row_shift[id]]['tiffin_ends']=$row_shift[first_break_end];
		$shift_lists[$row_shift[id]]['dinner_start']=$row_shift[second_break_start];
		$shift_lists[$row_shift[id]]['dinner_ends']=$row_shift[second_break_end];
		
		$shift_lists[$row_shift[id]]['lunch_start']=$row_shift[lunch_time_start];
		$shift_lists[$row_shift[id]]['lunch_break_min']=datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]); 
		$shift_lists[$row_shift[id]]['total_working_min']=datediff(n,$row_shift[shift_start],$row_shift[shift_end])-datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]);
		$shift_lists[$row_shift[id]]['early_out_start']=$row_shift[early_out_start];
		$shift_lists[$row_shift[id]]['entry_restriction_start']=$row_shift[entry_restriction_start];
	
	}

// OT POLICY Details
	$ot_policy = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$ot_policy[$row_shift[id]]['overtime_rate']=$row_shift[overtime_rate];
		$ot_policy[$row_shift[id]]['overtime_calculation_rule']=$row_shift[overtime_calculation_rule];
		$ot_policy[$row_shift[id]]['max_overtime']=$row_shift[max_overtime];
	}
// OT SLab POLICY Details
	$ot_policy_slab = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime_slab order by policy_id,overtime_head asc";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	$i=0;
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		if (in_array($row_shift[policy_id],$pol)) $i=$i+1; else $i=0; 
		$pol[]=$row_shift[policy_id];
		$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]][$i]['overtime_slot']=$row_shift[overtime_slot];
		$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]][$i]['overtime_multiplier']=$row_shift[overtime_multiplier];
		//$ot_policy_slab[$row_shift[policy_id]][$i]['overtime_head']['overtime_head']=$row_shift[overtime_head];
	}
 

// Variable Settiings
$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$var_hrm_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$var_hrm_chart[$row['company_name']] = array();
	$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
	$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
	$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
	$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
	$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
	$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
}

// Holiday Incentive Policy
$sql = "SELECT * FROM lib_policy_holiday_incentive ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$var_holiday_mst = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$var_holiday_mst[$row['id']]['pay_for'] = mysql_real_escape_string( $row['pay_for'] );
	$var_holiday_mst[$row['id']]['min_working_hour'] = mysql_real_escape_string( $row['min_working_hour'] );
	$var_holiday_mst[$row['id']]['earning_head'] = mysql_real_escape_string( $row['earning_head'] );
}

// Holiday Details POLICY Details
	$var_holiday_dtls = array();
	$sql_shift = "SELECT * FROM lib_policy_holiday_incentive_definition order by policy_id asc";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	$i=0;
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		if (in_array($row_shift[policy_id],$pol)) $i=$i+1; else $i=0; 
		$pol[]=$row_shift[policy_id];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['parameter_type']=$row_shift[parameter_type];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['percentage_formula']=$row_shift[percentage_formula];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['base_head']=$row_shift[base_head];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['amount']=$row_shift[amount];
	}

// Arrear Calculations
$cbo_salary_periods = explode('_', $cbo_salary_periods ); 
 
 
// All Salary Head for This Company	
	$salary_head_list = array();
	$salary_head_list_i= array();
	$salary_head_type= array();
	$sql_sal="select * from lib_payroll_head where is_applicable=1 and status_active=1 and is_deleted=0 order by id";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	$i=0;
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_head_list_head[$row_sal[payroll_head]]=$row_sal[id];
		  $salary_head_list_i[$i]=$row_sal[id];
		  $salary_head_type[$i]=$row_sal[type];
		  $i++;
	}


/*
// Late Deduction Plociy Details
$sql = "select * from  lib_policy_late_deduction_criteria order by id asc";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$late_deduction_policy_dtls = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$late_deduction_policy_dtls[$row['id']]['late_slot'] =  $row['late_slot'] ;
	$late_deduction_policy_dtls[$row['id']]['parameter_type'] =  $row['parameter_type'] ;
	$late_deduction_policy_dtls[$row['id']]['percentage_formula'] =  $row['percentage_formula'] ;
	$late_deduction_policy_dtls[$row['id']]['base_head'] =  $row['base_head'] ;
	$late_deduction_policy_dtls[$row['id']]['amount'] =  $row['amount'] ;  
}


 */
 /// Duty Roster and general shift and ot policy arrray
	$roster_ot_policy_array=array();
	if (trim($txt_emp_code)!="")
	{
		$ssemp_cond=" and a.emp_code in ($txt_emp_code)";
	}
	$txt_from_date_prev_tmp = add_date(convert_to_mysql_date( $txt_from_date ),-1);
	$txt_to_date_next_tmp = add_date(convert_to_mysql_date( $txt_to_date ),1);
	$tot_days_tmp=daysdiff($txt_from_date_prev_tmp, $txt_to_date_next_tmp);
	
	$sql = "SELECT a.current_shift,a.overtime_policy,a.shift_date,a.emp_code FROM hrm_duty_roster_process a, hrm_employee b where b.duty_roster_policy!=0 and a.emp_code=b.emp_code and a.shift_date between '".  $txt_from_date_prev_tmp ."' and '". $txt_to_date_next_tmp ."' $ssemp_cond";
	 
	$sql_exe = mysql_query( $sql );
	while( $sql_rslt = mysql_fetch_array( $sql_exe ) )
	{
		$roster_ot_policy_array[$sql_rslt[emp_code]][$sql_rslt[shift_date]]['ot']= $sql_rslt[overtime_policy];
		$roster_ot_policy_array[$sql_rslt[emp_code]][$sql_rslt[shift_date]]['shift']= $sql_rslt[current_shift];
	}
/*
Used Short  Terms 
A=Absent, P=Present, D=Late/Delay, CL,ML,SL,EL...=All Leaves,SP=Suspended, H=Holiday, W=Weekend, R=Regular Day, MR=Movement Register
*/
// Processing Started Here

if ($action=="salary_pre_processing")
{
	set_time_limit(0);
	ignore_user_abort(1);
	 
	//$cbo_salary_periods = explode('_', $cbo_salary_periods ); 
	$txt_from_date = convert_to_mysql_date( $cbo_salary_periods[0] );
	$txt_to_date = convert_to_mysql_date( $cbo_salary_periods[1] );
	
	$i=0;
	
 	$tot_days=daysdiff($txt_from_date, $txt_to_date);
	
	if(trim($txt_emp_code)==""){ //new salary process
		$sql_emp="select * from hrm_employee where salary_rule<>0 order by emp_code desc"; 
		$str_cond="";
	}
	else //salary re process
	{
	 	$delete_dtls_id=mysql_query("delete from hrm_salary_pay_days where emp_code in ($txt_emp_code) and salary_period='$txt_from_date'");
		$str_cond=" and emp.emp_code in ($txt_emp_code)";
		//$sql_emp="select * from hrm_employee where emp_code in ($txt_emp_code) and salary_rule<>0 order by emp_code desc";
	} 
	
	$sql_emp=" SELECT   
				count(CASE WHEN a.status ='P' THEN a.emp_code END) AS 'present',
				count(CASE WHEN a.status ='P' and a.is_regular_day=0 THEN a.emp_code END) AS 'gn_present',
				count(CASE WHEN a.status ='A' THEN a.emp_code END) AS 'absent',
				count(CASE WHEN a.status ='A' and a.is_regular_day=0  THEN a.emp_code END) AS 'gn_absent',
				count(CASE WHEN a.status ='D' THEN a.emp_code END) AS 'late',
				count(CASE WHEN a.status ='D' and a.is_regular_day=0  THEN a.emp_code END) AS 'gn_late',
				count(CASE WHEN a.status ='GH' THEN a.emp_code END) AS 'g_holiday',
				count(CASE WHEN a.status ='FH' THEN a.emp_code END) AS 'f_holiday',
				count(CASE WHEN a.status ='CH' THEN a.emp_code END) AS 'c_holiday',
				count(CASE WHEN a.status ='W' THEN a.emp_code END) AS 'weekday',
				count(CASE WHEN a.status ='MR' THEN a.emp_code END) AS 'movement',
				count(CASE WHEN a.status ='CL' THEN a.emp_code END) AS 'cl',
				count(CASE WHEN a.status ='FL' THEN a.emp_code END) AS 'fl',
				count(CASE WHEN a.status ='EL' THEN a.emp_code END) AS 'el',
				count(CASE WHEN a.status ='SL' THEN a.emp_code END) AS 'sl',
				count(CASE WHEN a.status ='ML' THEN a.emp_code END) AS 'ml',
				count(CASE WHEN a.status ='SpL' THEN a.emp_code END) AS 'spl',
				count(CASE WHEN a.status ='LWP' THEN a.emp_code END) AS 'lwp',
				count(CASE WHEN a.status ='SP' THEN a.emp_code END) AS 'sp', 
				count(CASE WHEN a.is_regular_day=0 THEN a.emp_code END) AS 'off_day',
				emp.* 
			from 
				hrm_employee emp, hrm_attendance a
			where 
				 a.emp_code=emp.emp_code and 
				a.attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' $str_cond group by emp.emp_code";
	 
		$exe_sql_emp=mysql_db_query($DB, $sql_emp);
		$emp_salary_sheet=array();
		$emp_extra_details=array();
		$no_process=array();
		while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
		{ 
			$txt_from_date = convert_to_mysql_date( $cbo_salary_periods[0] );
			$txt_to_date = convert_to_mysql_date( $cbo_salary_periods[1] );
			
			$total_ot_hr_nrd=0;
			 
			 $present_days=$row_emp[present];
			 $absent_days=$row_emp[absent];
			 $late_days=$row_emp[late];
			 $g_holiday_days=$row_emp[g_holiday];
			 $f_holiday_days=$row_emp[f_holiday];
			 $c_holiday_days=$row_emp[c_holiday];
			 $weekday_days=$row_emp[weekday];
			 $movement_days=$row_emp[movement];
			 $c_leave_days=$row_emp[cl];
			 $s_leave_days=$row_emp[sl];
			 $e_leave_days=$row_emp[el];
			 $m_leave_days=$row_emp[ml];
			 $f_leave_days=$row_emp[fl];
			 $spl_leave_days=$row_emp[spl];
			 $lwp_leave_days=$row_emp[lwp];
			 $suspended_days=$row_emp[sp];
			 $total_cal_days=$present_days+$absent_days+$late_days+$g_holiday_days+$f_holiday_days+$c_holiday_days+$weekday_days+$movement_days+$c_leave_days+ $s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days+$lwp_leave_days+$f_leave_days+$suspended_days;
			 $total_leave_days=$c_leave_days+$s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days+ $lwp_leave_days+$f_leave_days; 
		
		
			//for buyer salary
			$gn_present_days=$present_days-$row_emp[gn_present];
			$gn_absent_days=$absent_days-$row_emp[gn_absent];
			$gn_late_days=$late_days-$row_emp[gn_late];
			
			$total_off_day=$row_emp[off_day];
			$gn_g_holiday_days=count($holiday_lists_all[0]);
			$gn_f_holiday_days=count($holiday_lists_all[1]);
			$gn_weekday_days=$total_off_day-($gn_g_holiday_days+$gn_f_holiday_days);
			 
	
			 /*
			if ($emp_condition==2) // New
				$total_payable_days=daysdiff(convert_to_mysql_date($row_emp[joining_date]),$txt_to_date);	//New Join 
			else if ($emp_condition==1) // Resign
				$total_payable_days=daysdiff($txt_from_date,convert_to_mysql_date($old_mod));	// Resign
			else
				$total_payable_days=$tot_days;	//Regular
			
			$calendar_days=$tot_days;
			$not_payable_days=$calendar_days-$total_payable_days;
			$total_working_days=$calendar_days-($g_holiday_days+$f_holiday_days+$c_holiday_days+$weekday_days+$not_payable_days);
			$gn_total_working_days=$calendar_days-($gn_g_holiday_days+$gn_f_holiday_days+$c_holiday_days+$gn_weekday_days+$not_payable_days);	//buyer
			$net_payable_days=$calendar_days-($absent_days+$lwp_leave_days+$not_payable_days);
			$gn_net_payable_days=$calendar_days-($gn_absent_days+$lwp_leave_days+$not_payable_days);	//buyer
			 */
		
			// Start Traversing Slary Head and calcutae corresspndiong amont
			
			for($j=0; $j<count($salary_head_list_i);$j++)
			{
				if($salary_head_list_i[$j]==5 ) // Over Time
				{
					print_r($salary_head_list_i);
					echo  "L=".$late_days."P-".$present_days."A-".$absent_days;die;	
				 
					$sql_data="select * from  lib_policy_overtime_slab where policy_id=$row_emp[overtime_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$old_hr=0;
					$old_slot=0;
					$ot_slot=0;
					$u=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$u++;
						$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
						$ot_multiplier=$sql_data_rslt[overtime_multiplier];
						
						$total_ot_hr= get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' and is_regular_day=1",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
						$total_ot_hr=$total_ot_hr-$old_hr;
						$old_hr= $old_hr+$total_ot_hr;
						
						if($sql_data_rslt[overtime_head]==5)
						{
							$total_ot_hr_nrd = get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' and is_regular_day=0",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
							$total_ot_hr_nrd_amount = $total_ot_hr_nrd*$ot_rate*$ot_multiplier;
							$emp_salary_sheet[$row_emp[emp_code]][5]= $emp_salary_sheet[$row_emp[emp_code]][5]+($ot_rate*$ot_multiplier*$total_ot_hr);  
							$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+$total_ot_hr;
							$emp_extra_details[$row_emp[emp_code]][5]['rate']=$ot_rate;
						}
					}
					//echo $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']."-".$row_emp[emp_code]; die;
					$ot_slot=0;
				}
				else if($salary_head_list_i[$j]==6 ) // Extra Over Time
				{
					$sql_data="select * from  lib_policy_overtime_slab where policy_id=$row_emp[overtime_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$old_hr=0;
					$old_slot=0;
					$u=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$u++;
						$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
						$ot_multiplier=$sql_data_rslt[overtime_multiplier];
						
						$total_ot_hr= get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."'",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
						$total_ot_hr=$total_ot_hr-$old_hr;
						$old_hr= $old_hr+$total_ot_hr; 
	 
						if($sql_data_rslt[overtime_head]==6)
						{
							$emp_salary_sheet[$row_emp[emp_code]][6]= $emp_salary_sheet[$row_emp[emp_code]][6]+($ot_rate*$ot_multiplier*$total_ot_hr)+$total_ot_hr_nrd_amount;  
							$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$total_ot_hr+$total_ot_hr_nrd; 
							$emp_extra_details[$row_emp[emp_code]][6]['rate']=$ot_rate;
						}
					}
				}
				else if( $salary_head_list_i[$j]==7) // Further Extra Over Time
				{
					$sql_data="select * from  lib_policy_overtime_slab where policy_id=$row_emp[overtime_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$old_hr=0;
					$old_slot=0;
					$u=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$u++;
						$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
						$ot_multiplier=$sql_data_rslt[overtime_multiplier];
						
						$total_ot_hr= get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."'",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
						$total_ot_hr=$total_ot_hr-$old_hr;
						$old_hr= $old_hr+$total_ot_hr; 
						//echo $total_ot_hr."yy".$ot_slot."--";
						if($sql_data_rslt[overtime_head]==7)
						{
							$emp_salary_sheet[$row_emp[emp_code]][7]= $emp_salary_sheet[$row_emp[emp_code]][7]+($ot_rate*$ot_multiplier*$total_ot_hr);  
							$emp_extra_details[$row_emp[emp_code]][7]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][7]['ot_hr']+$total_ot_hr; 
							$emp_extra_details[$row_emp[emp_code]][7]['rate']=$ot_rate;
						}
					} //echo $old_hr.".--".$emp_salary_sheet[$row_emp[emp_code]][7]."-amount-hr-".$emp_extra_details[$row_emp[emp_code]][7]['ot_hr']; die;
				}
				else if($salary_head_list_i[$j]==8) // Holiday Allowance
				{
					$holidays=explode('_', get_total_payable_holidays_amount( $row_emp[emp_code], $txt_from_date, $txt_to_date, "'CH','FH','GH','W'", $var_holiday_mst, $var_holiday_dtls,  $row_emp[holiday_allowance_entitled], $emp_salary_sheet, $total_payable_days)) ; 
					
					$emp_salary_sheet[$row_emp[emp_code]][8]= $holidays[1]; //get_total_ot_hr($row_emp[emp_code],$txt_from_date, $txt_to_date,1);  
					$emp_extra_details[$row_emp[emp_code]][8]['nof_holi']= $holidays[0]; //get_total_ot_rate( $row_emp[emp_code],$overtime_policy[$row_emp[emp_code]] ); 
					
				}
				else if($salary_head_list_i[$j]==9) $emp_salary_sheet[$row_emp[emp_code]][9]= $salary_breakdown[$row_emp[emp_code]][9];   // Tiffin Allowance
				 
				else if($salary_head_list_i[$j]==35)  // Attendance Bonus  Rules
				{	$buyer_att_bonus_amount=0;
					if( $emp_condition==0)// Regular Worker
					{
						$sql_data="select * from  lib_policy_attendance_bonus_definition where policy_id=$row_emp[attendance_bonus_policy] order by id asc";
						$sql_data_exe=mysql_query($sql_data); 
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							if($sql_data_rslt[leave_criteria]>= $total_leave_days && $sql_data_rslt[late_criteria]>=$late_days  &&   $sql_data_rslt[absent_criteria]>=$absent_days )
							{
								$att_bonus_amount=$sql_data_rslt[amount];break; 
							}
 						}  
						$emp_salary_sheet[$row_emp[emp_code]][35]=$att_bonus_amount; // $salary_breakdown[$row_emp[emp_code]][35];  
						$att_bonus_amount=0;
 						
						$sql_data_exe=mysql_query($sql_data);  
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
 							//for buyer salary sheet attendanc bonus
							if($sql_data_rslt[leave_criteria]>= $total_leave_days && $sql_data_rslt[late_criteria]>=$gn_late_days  && $sql_data_rslt[absent_criteria]>=$gn_absent_days )
							{
								$buyer_att_bonus_amount=$sql_data_rslt[amount];break;
							}
						}  
					}
					else  // New and Resign Worker ---------------NO Bonus
					{
						 $emp_salary_sheet[$row_emp[emp_code]][35]=0; // $salary_breakdown[$row_emp[emp_code]][35];  
						 $att_bonus_amount=0;
					}
					
				}
				else if($salary_head_list_i[$j]==36) $emp_salary_sheet[$row_emp[emp_code]][36]= $salary_breakdown[$row_emp[emp_code]][36];   // Night Allowance  Rules
				 
				else if($salary_head_list_i[$j]==23) // Late Deduction  
				{
					
					$sql_data="select * from  lib_policy_late_deduction_criteria where policy_id=$row_emp[late_deduction_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data); 
					$late_deduct_amnt=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						if ($late_days>= $sql_data_rslt[late_slot])
						{ 
							$days_to_deduct= floor($late_days/$sql_data_rslt[late_slot]); 
							if ( $sql_data_rslt[parameter_type]==0) // Percentage
							{
								$late_deduct_amnt=(($sql_data_rslt[percentage_formula]*($salary_breakdown[$row_emp[emp_code]][$sql_data_rslt[base_head]]/$tot_days))/100)*$days_to_deduct;  
								break;
							}
							else if ( $sql_data_rslt[parameter_type]==2) // Fixed
							{
								$late_deduct_amnt=$sql_data_rslt[amount]*$days_to_deduct;
								break;
							}
						}
 					}  
 					$emp_salary_sheet[$row_emp[emp_code]][23]= $late_deduct_amnt; 
					
 					//buyer late deduction
					$sql_data_exe=mysql_query($sql_data); 
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$gn_late_deduct_amnt=0;
						if ($gn_late_days>= $sql_data_rslt[late_slot])
						{ 
 							$days_to_deduct= floor($gn_late_days/$sql_data_rslt[late_slot]); 
							if ( $sql_data_rslt[parameter_type]==0) // Percentage
							{
								$gn_late_deduct_amnt=(($sql_data_rslt[percentage_formula]*($salary_breakdown[$row_emp[emp_code]][$sql_data_rslt[base_head]]/$tot_days))/100)*$days_to_deduct;  
								break;
							}
							else if ( $sql_data_rslt[parameter_type]==2) // Fixed
							{
								$gn_late_deduct_amnt=$sql_data_rslt[amount]*$days_to_deduct;
								break;
							}
						 }
					 }
  					//echo   $emp_salary_sheet[$row_emp[emp_code]][23]; die;
				}
				
				else if($salary_head_list_i[$j]==24) // Absent Deduction  
				{
					 
					$sql_data="select * from  lib_policy_absent_deduction_definition where policy_id=$row_emp[absent_deduction_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$abs_deduct_amnt=0; 
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						if ($absent_days > 0)
						{ 
							if ( $sql_data_rslt[parameter_type]==0)
							{
								$abs_deduct_amnt=(($sql_data_rslt[percentage_formula]*($salary_breakdown[$row_emp[emp_code]][$sql_data_rslt[base_head]]/$tot_days))/100)*$absent_days;
								break;
							}
							else if ( $sql_data_rslt[parameter_type]==2)
							{
								$abs_deduct_amnt=$sql_data_rslt[amount]*$absent_days;
								break;
							}
						}
 					}  
					$emp_salary_sheet[$row_emp[emp_code]][24]= $abs_deduct_amnt; 
 					
					$sql_data_exe=mysql_query($sql_data);
					$gn_abs_deduct_amnt=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
 						if ($gn_absent_days > 0)
						{ 
							if ( $sql_data_rslt[parameter_type]==0)
							{
								$gn_abs_deduct_amnt=(($sql_data_rslt[percentage_formula]*($salary_breakdown[$row_emp[emp_code]][$sql_data_rslt[base_head]]/$tot_days))/100)*$gn_absent_days;
								break;
							}
							else if ( $sql_data_rslt[parameter_type]==2)
							{
								$gn_abs_deduct_amnt=$sql_data_rslt[amount]*$gn_absent_days;
								break;
							}
						}
 					}  
					
					//echo $emp_salary_sheet[$row_emp[emp_code]][24]; die;
				} 
				else if($salary_head_list_i[$j]==29) // Leave Without Pay
				{
					$sql_data="select * from  lib_policy_leave_definition where policy_id=$row_emp[leave_policy] and leave_type='LWP' order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$lwp_deduct_amnt=0; 
					 
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$policy_def=explode('_',$sql_data_rslt[deduction]);
						
						if ($lwp_leave_days > 0)
						{ 
							if ( $policy_def[0]==0) // 100%
							{
								$lwp_deduct_amnt=($salary_breakdown[$row_emp[emp_code]][$policy_def[1]]/$tot_days)*$lwp_leave_days;
								break;
							}
							else if ( $policy_def[0]==1) //50%
							{
								$lwp_deduct_amnt=(($policy_def[0]*($salary_breakdown[$row_emp[emp_code]][$policy_def[1]]/$tot_days))/100)*$lwp_leave_days;
								break;
							}
							else if ( $policy_def[0]==2) // Fixed
							{
								$lwp_deduct_amnt=$policy_def[1]*$lwp_leave_days;
								break;
							}
						}
					}  
					
					$emp_salary_sheet[$row_emp[emp_code]][29]= $lwp_deduct_amnt; 
					   $lwp_deduct_amnt=0;
				}
				 
				 
			} // End of For Loop
			// Calculate total 
		 
			// Save Data to Table From Here
			 
			if( $row_emp[ot_entitled]==1) // Ot Entitled
			{
				$is_with_over_time=1; 
				$total_over_time_min=$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][7]['ot_hr'];
				$extra_ot=$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][7]['ot_hr'];
			}
			else
			{
				$total_earning=$total_earning-($emp_salary_sheet[$row_emp[emp_code]][5]+$emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
				$b_total_earning_amount=$total_earning; // - ($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
				$is_with_over_time=0;
				$total_over_time_min=0;
				$emp_salary_sheet[$row_emp[emp_code]][5]=0;
				$emp_salary_sheet[$row_emp[emp_code]][6]=0;
				$emp_salary_sheet[$row_emp[emp_code]][7]=0;
				$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']=0;
				$extra_ot=0;
			}
			 
			// Check Disciplinary Actions for Deduction
			$late_amt = $emp_salary_sheet[$row_emp[emp_code]][23];
			$absent_amt = $emp_salary_sheet[$row_emp[emp_code]][24];
			$lwp_amt = $emp_salary_sheet[$row_emp[emp_code]][29];
			
			 
			$net_payable_amount=$total_earning-($total_deduction+($not_payable_days*$daily_gross_salary)); 
			$b_net_payable_amount=$b_total_earning_amount-($b_total_deduction_amount+($not_payable_days*$daily_gross_salary));   //$net_payable_amount-($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
			if($row_id=="") $row_id = return_next_id( "id", "hrm_salary_mst" ); else $row_id+=1;
			$sql = "INSERT INTO hrm_salary_mst (id,salary_periods,emp_code,employee_category,bank_gross,gross_salary,basic_salary,medical_allowance,house_rent,total_earning_amount,b_total_earning_amount,total_deduction_amount,is_with_over_time,b_over_time,e_over_time,total_over_time,over_time_rate,net_payable_amount,b_net_payable_amount,payable_days,total_abs_days,total_late_days,total_lwp_days,special_leave,earn_leave,faternity_leave,maternity_leave,sick_leave,casual_leave,government_holiday,festival_holiday,compensatory_total_holiday,total_weekly_holiday,not_payble_days,suspend_days, suspend_amount,daily_basic_salary,daily_gross_salary,total_working_days,total_calendar_days,total_leave_days,total_late_deduction_amount,total_absent_deduction_amount,total_lwp_deduction_amount,b_total_deduction_amount,b_net_payable_days,b_absent_days,b_late_days,b_government_holiday,b_festival_holiday,b_total_weekly_holiday,b_total_working_days,b_total_present_days,b_total_late_deduction_amount,b_total_absent_deduction_amount,b_attendance_bonus,company_id,location_id,division_id,department_id,section_id,subsection_id,emp_status,total_present_days)			
								 VALUES ('$row_id','". $cbo_salary_periods[0] ."', '$row_emp[emp_code]','$row_emp[category]','$row_emp[bank_gross]','".$emp_salary_sheet[$row_emp[emp_code]][0]."','".$emp_salary_sheet[$row_emp[emp_code]][1]."','".$emp_salary_sheet[$row_emp[emp_code]][3]."','".$emp_salary_sheet[$row_emp[emp_code]][2]."','$total_earning','$b_total_earning_amount','$total_deduction',
								 '$is_with_over_time','".$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']."','".$extra_ot."','$total_over_time_min','$ot_rate','$net_payable_amount','$b_net_payable_amount','$net_payable_days','$absent_days','$late_days',
								 '$lwp_leave_days','$spl_leave_days','$e_leave_days','$f_leave_days','$m_leave_days','$s_leave_days','$c_leave_days','$g_holiday_days','$f_holiday_days','$c_holiday_days','$weekday_days','$not_payable_days','$suspended_days',
								 '$suspension_deduct_amnt','$daily_basic_salary','$daily_gross_salary','$total_working_days','$calendar_days','$total_leave_days','$late_amt','$absent_amt','$lwp_amt','$b_total_deduction_amount','$gn_net_payable_days','$gn_absent_days',
								 '$gn_late_days','$gn_g_holiday_days','$gn_f_holiday_days','$gn_weekday_days','$gn_total_working_days','$gn_present_days','$gn_late_deduct_amnt','$gn_abs_deduct_amnt','$buyer_att_bonus_amount','$row_emp[company_id]',
								 '$row_emp[location_id]','$row_emp[division_id]','$row_emp[department_id]','$row_emp[section_id]','$row_emp[subsection_id]','$emp_condition','$present_days')";
								  
							 // echo $sql; die;
							 mysql_query( $sql ) or die (mysql_error());
				  $net_payable_amount=0; $total_earning=0; $total_deduction=0;	 $total_earning=0;$b_total_earning_amount=0;$total_deduction=0;$b_net_payable_amount=0;	$buyer_att_bonus_amount=0;			 
				//echo $total_earning."-".$total_deduction."--".$emp_salary_sheet[$row_emp[emp_code]][0]."\n";die;
			 
		} // End of Employee Loop
		$no_process_emp = implode(",",$no_process);  
  		echo $no_process_emp."*1";
		die;
	/*}
	else
	{
		$no_process_emp = implode(",",$no_process);  
  		echo $no_process_emp."*2";
		die;
	}*/
  

}

function get_total_payable_holidays_amount( $emp_code, $txt_from_date, $txt_to_date, $status, $mst_policy, $dtls_policy, $h_policy, $emp_salary_sheet, $tot_days)
{
	$sql_emp22="select * from hrm_attendance where emp_code='$emp_code' and attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' and status in($status) and sign_in_time<>'00:00:00' and sign_out_time<>'00:00:00' "; //and a.emp_code='0000038'  and a.emp_code='0000015' 
	 
	$exe_sql_emp22=mysql_query($sql_emp22);
	$day_no=0;
	while ($row_h22 = mysql_fetch_array($exe_sql_emp22))// EMp Master Qry starts 
	{ 
	 	$tot_min=datediff(n,$row_h22[sign_in_time],$row_h22[sign_out_time]);
		
		if ( $tot_min >=($mst_policy[$h_policy]['min_working_hour']*60))
		{
			$day_no=$day_no+1;
			if ($mst_policy[$h_policy]['pay_for']==0)  // pay for hour
			{
				$tot_hr=(int)$tot_min/60;
				for($j=0; $j<count($dtls_policy[$h_policy]); $j++)
				{
					if ($dtls_policy[$h_policy][$i]['parameter_type']==0) // Percentage
					{
						$amount+= ($emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['base_head']]/($tot_days*8))*$tot_hr ;
					}
					else if ($dtls_policy[$h_policy][$j]['parameter_type']==2)  // Fixed Amount
					{ 
						$amount+=  $emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['amount']]*($tot_hr) ;
					}
					 
				}
			}
			else //if ($mst_policy[$h_policy]['pay_for']==0)  // pay for Days
			{
				$tot_hr=(int)$tot_min/60;
				for($j=0; $j<count($dtls_policy[$h_policy]); $j++)
				{
					
					if ($dtls_policy[$h_policy][$j]['parameter_type']==0) // Percentage
					{
						$amount+= (($emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['base_head']]/($tot_days))*$dtls_policy[$h_policy][$j]['percentage_formula'])/100;
					}
					else if ($dtls_policy[$h_policy][$j]['parameter_type']==2)  // Fixed Amount
					{ 
						$amount+=  $emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['amount']];
					}
					 
				}
			}
		}
	} // while
	return $day_no."_".$amount;
}

function get_total_ot_hr( $ot_policy_slab, $txt_from_date, $txt_to_date,$overtime_policy,$salary_head, $emp_code, $comp_id, $var_hrm_chart )
{
	 for ($i=0; $i<count($ot_policy_slab[$overtime_policy][$salary_head]); $i++)
	 {
		$slot=$slot+$ot_policy_slab[$overtime_policy][$salary_head][$i]['overtime_slot']*60; 
		$rate=$ot_policy_slab[$overtime_policy][$salary_head][$i]['overtime_rate']; 
		
		$str_date=" attnd_date between '$txt_from_date' and '$txt_to_date'";
		$tot_ot=get_buyer_tot_emp_ot_hr($emp_code,$str_date,$var_hrm_chart[$comp_id][one_hour_ot_unit],$var_hrm_chart[$comp_id][allow_ot_fraction],$var_hrm_chart[$comp_id][ot_start_minute],$slot);
		$act_ot=$act_ot+($tot_ot-$act_ot);
	
	 }
	 
	 return $act_ot;
}


 
// Functions required for this processing
function daysdiff($date1,$date2) 
{ 
	$diff = (strtotime($date2) - strtotime($date1));
	
	$days = floor($diff / (1*60*60*24)+1);
	return $days; 		
}

	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

 



function change_date_for_process( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "-", $date_field );
		return $dates[1] . "/" . $dates[2] . "/" . $dates[0];
	}
	else return $date_field;
}
function change_date_for_process_sql( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "/", $date_field );
		return $dates[2] . "-" . $dates[0] . "-" . $dates[1];
	}
	else return $date_field;
}
function change_time_for_process( $time_field ) {
	if( $time_field != '' ) {
		$times = explode( "-", $time_field );
		return $times[0] . ":" . $times[1] . ":" . $times[2];
	}
	else return $time_field;
}
 
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>

