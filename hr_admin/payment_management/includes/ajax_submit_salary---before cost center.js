function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

function fnc_regular_process_salary()
{
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_salary_periods 	= escape(document.getElementById('cbo_salary_periods').value);
	var cbo_ot_rate_rounding 	= escape(document.getElementById('cbo_ot_rate_rounding').value);
	var txt_precision=escape(document.getElementById('txt_precision').value);
	// alert(txt_precision);
	$("#messagebox").removeClass().addClass('messagebox').text('Salary Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else
	{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/salary_processing.php?action=salary_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_salary_periods='+cbo_salary_periods+
					'&cbo_ot_rate_rounding='+cbo_ot_rate_rounding+
					'&txt_precision='+txt_precision
					);
		http.onreadystatechange = regular_process_salary_reply;
		http.send(null); 
	}
}

function regular_process_salary_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	 //alert(http.responseText);			
		if (response[1]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Salary Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Can Not Process Salary, Duplicate Process for this Periods.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==3)
		{
			
			alert('Can Not Process Salary,Have To Lock Previous Salary Period.');
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Can Not Process Salary,Have To Lock Previous Salary Period.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(3000);
				$(this).css('background-color', 'red');
			});
		}
		else if (response[1]==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Database is Busy, please try after some time.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				$(this).css('background-color', 'red');
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		
		else if (response[1]==9)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('process previous first').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				$(this).css('background-color', 'red');
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
						
	}
}	


function fnc_regular_process_production_salary()
{
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_salary_periods 	= escape(document.getElementById('cbo_salary_periods').value);
	var txt_emp_code        =document.getElementById('txt_emp_code').value;
	//alert(txt_emp_code);
	
	if($('#chk_reprocess').is(':checked'))
		var is_reprocess=1; //alert('yes');
	else 
		var is_reprocess=0;
	//alert(is_reprocess);	
 
	$("#messagebox").removeClass().addClass('messagebox').text('Salary Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else
	{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/production_salary_processing.php?action=production_salary_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_salary_periods='+cbo_salary_periods+
					'&is_reprocess='+is_reprocess+
					'&txt_emp_code='+txt_emp_code
					);
		http.onreadystatechange = regular_process_prod_salary_reply;
		http.send(null); 
	}
}

function regular_process_prod_salary_reply() 
{
	if(http.readyState == 4){ 
	//alert(http.responseText);	
	//return;		
	var response = (http.responseText).split('*');			
	//alert(response[0]);			
		if (response[1]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Salary Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Can Not Process Salary, Duplicate Process for this Periods.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}			
	}
}	




function fnc_regular_process_salary_pree()
{
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_salary_periods 	= escape(document.getElementById('cbo_salary_periods').value);
	//var txt_to_date 	= escape(document.getElementById('txt_to_date').value);
	
	// alert(cbo_salary_periods);
	$("#messagebox").removeClass().addClass('messagebox').text('Salary Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else
	{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/salary_pre_processing.php?action=salary_pre_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_salary_periods='+cbo_salary_periods
					);
		http.onreadystatechange = fnc_regular_process_salary_pree_reply;
		http.send(null); 
	}
}

function fnc_regular_process_salary_pree_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	//alert(response[0]);			
		if (response[1]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Salary Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Can Not Process Salary, Duplicate Process for this Periods.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}			
	}
}	




function fnc_regular_reprocess_salary()
{
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_salary_periods 	= escape(document.getElementById('cbo_salary_periods').value);
	var txt_emp_code        = escape(document.getElementById('txt_emp_code').value);
	var cbo_ot_rate_rounding 	= escape(document.getElementById('cbo_ot_rate_rounding').value);
	var txt_precision=escape(document.getElementById('txt_precision').value);
	//alert(txt_emp_code);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Salary Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if(txt_emp_code=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
	//alert(txt_emp_code);
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/salary_processing.php?action=salary_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_salary_periods='+cbo_salary_periods+
					'&txt_emp_code='+txt_emp_code+
					'&cbo_ot_rate_rounding='+cbo_ot_rate_rounding+
					'&txt_precision='+txt_precision);
		http.onreadystatechange = regular_reprocess_salary_reply;
		http.send(null); 
	}
}

function regular_reprocess_salary_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	//alert(response[0]);			
		if (response[1]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Salary Re Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		
		else if (response[1]==3)
		{
			
			alert('Can Not Process Salary,Have To Lock Previous Salary Period.');
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Can Not Process Salary,Have To Lock Previous Salary Period.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(3000);
				$(this).css('background-color', 'red');
			});
		}			
	}

}

function fnc_allowance_process_salary()
{
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var txt_from_date 	= escape(document.getElementById('txt_from_date').value);
	var txt_to_date 	= escape(document.getElementById('txt_to_date').value);
	var txt_emp_code= escape(document.getElementById('txt_emp_code').value);
	var cbo_ot_rate_rounding 	= escape(document.getElementById('cbo_ot_rate_rounding').value);
	var txt_precision=escape(document.getElementById('txt_precision').value);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Salary Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_from_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if(txt_to_date=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_to_date').focus();
			$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
	//alert(txt_emp_code);
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/allowance_processing.php?action=allowance_processing'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&txt_from_date='+txt_from_date+
					'&txt_to_date='+txt_to_date+
					'&txt_emp_code='+txt_emp_code+
					'&cbo_ot_rate_rounding='+cbo_ot_rate_rounding+
					'&txt_precision='+txt_precision);
		http.onreadystatechange = regular_process_allowance_reply;
		http.send(null); 
	}
}

function regular_process_allowance_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	//alert(response[0]);			
		if (response[1]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Allowance Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}		
	}

}


var timercount = 0;
var timestart  = null;
 
function showtimer() {
	if(timercount) {
		clearTimeout(timercount);
		clockID = 0;
	}
	if(!timestart){
		timestart = new Date();
	}
	var timeend = new Date();
	var timedifference = timeend.getTime() - timestart.getTime();
	timeend.setTime(timedifference);
	var minutes_passed = timeend.getMinutes();
	if(minutes_passed < 10){
		minutes_passed = "0" + minutes_passed;
	}
	var seconds_passed = timeend.getSeconds();
	if(seconds_passed < 10){
		seconds_passed = "0" + seconds_passed;
	}
	document.timeform.timetextarea.value = minutes_passed + ":" + seconds_passed;
	timercount = setTimeout("showtimer()", 1000);
}
 
function sw_start(){
	if(!timercount){
	timestart   = new Date();
	document.timeform.timetextarea.value = "00:00";
	document.timeform.laptime.value = "";
	timercount  = setTimeout("showtimer()", 1000);
	}
	else{
	var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var minutes_passed = timeend.getMinutes();
		if(minutes_passed < 10){
			minutes_passed = "0" + minutes_passed;
		}
		var seconds_passed = timeend.getSeconds();
		if(seconds_passed < 10){
			seconds_passed = "0" + seconds_passed;
		}
		var milliseconds_passed = timeend.getMilliseconds();
		if(milliseconds_passed < 10){
			milliseconds_passed = "00" + milliseconds_passed;
		}
		else if(milliseconds_passed < 100){
			milliseconds_passed = "0" + milliseconds_passed;
		}
		document.timeform.laptime.value = minutes_passed + ":" + seconds_passed + "." + milliseconds_passed;
	}
}

function Stop() {
	if(timercount) {
		clearTimeout(timercount);
		timercount  = 0;
		var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var minutes_passed = timeend.getMinutes();
		if(minutes_passed < 10){
			minutes_passed = "0" + minutes_passed;
		}
		var seconds_passed = timeend.getSeconds();
		if(seconds_passed < 10){
			seconds_passed = "0" + seconds_passed;
		}
		var milliseconds_passed = timeend.getMilliseconds();
		if(milliseconds_passed < 10){
			milliseconds_passed = "00" + milliseconds_passed;
		}
		else if(milliseconds_passed < 100){
			milliseconds_passed = "0" + milliseconds_passed;
		}
		document.timeform.timetextarea.value = minutes_passed + ":" + seconds_passed + "." + milliseconds_passed;
	}
	timestart = null;
}
 
function Reset() {
	timestart = null;
	document.timeform.timetextarea.value = "00:00";
	document.timeform.laptime.value = "";
}
 


//increment salary entry here
function fn_increment_entry()
{
		
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	
	var j=0;count=0;data='';effective_date='',issue_date='',arrear_data='',last_salary_processed_date='';reason='';
	for (i = 1; i <= document.getElementsByName('chk_all').length-1; i++)
			{
				if(document.getElementById('chk_all_'+i).checked)
				{
					count++;
					var emp_code = $('#attnd_id_'+i).val();
					 //alert (count);
					 if($('#effective_date_'+emp_code).val()=="")
						{
							$("#messagebox").fadeTo(200,0.1,function(){  
							$('#effective_date_'+emp_code).focus();
							$(this).html('Please Select Effective Date').addClass('messageboxerror').fadeTo(900,1);
							});
							return;
						}	
				
					if(count==1)
					{
						effective_date = $('#effective_date_'+emp_code).val();
						last_salary_processed_date= $('#last_salary_processed'+emp_code).val();
						
						data = $('#new_salary_break_'+emp_code).val();
						//arear
						arrear_data = $('#arrear_concate_'+emp_code).val();
						issue_date = $('#issue_date_'+emp_code).val();
						
						reason = $('#reason'+emp_code).val();
						
					}
					else
					{
						last_salary_processed_date += '*'+$('#last_salary_processed'+emp_code).val();	
						effective_date += '*'+$('#effective_date_'+emp_code).val();	
						data += '*'+$('#new_salary_break_'+emp_code).val();	
						//arear
						arrear_data += '***'+$('#arrear_concate_'+emp_code).val();
						issue_date += '*'+$('#issue_date_'+emp_code).val();
						//issue_date = $('#issue_date_'+emp_code).val();
						reason += '*'+$('#reason'+emp_code).val();
											
					}
				
			}				
								
		}
	//alert(data);return;
	if(count==0){ alert("At Least one Employee Have To Select."); return; }		
		nocache = Math.random();
		http.open('POST','includes/emp_increment_process.php?action=increment_process&data='+data+'&effective_date='+effective_date+'&arrear_data='+arrear_data+'&last_salary_processed_date='+last_salary_processed_date+'&issue_date='+issue_date+'&reason='+reason);
		http.onreadystatechange = response_fn_increment_entry;
		http.send(null); 	
}


function response_fn_increment_entry() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		//alert(response);		
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				
				$(this).html('Data Inserted Successfully.').addClass('messagebox_ok').fadeTo(1000,1);
				$(this).fadeOut(5000);			
				//showResult_search('','','increment_emp_search','div_list_view');
				clear_data();
			});
		}
		else if (response[0]==5)
		
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Entry!! Increment Already Exist.').addClass('messagebox_ok').fadeTo(900,1);

				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==6)
		
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Effective Date Must Be Greater Than Joining Date').addClass('messagebox_ok').fadeTo(900,1);

				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==7)
		
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Invalid Arrear Entry').addClass('messagebox_ok').fadeTo(900,1);

				//document.getElementById('save_up').value="";
			});
		}
		
	}

}



//increment salary approval here
function fn_increment_approved()
{
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
			
	var j=0;count=0;data='';effective_date='',arrear_data='',incr_master_id='',approve_status='';employees='';
	 
	for (i = 1; i <= document.getElementsByName('chk_all').length; i++)
			{	 		
				if( document.getElementById('chk_all_'+i).checked==true && $('#approve_status_'+i).val()*1>0 )
				{
					count++;
					//var split_result = ($('#attnd_id_'+i).val()).split('_');
					var emp_code = $('#attnd_id_'+i).val();
					 	
					if(count==1){
						employees = emp_code;					
						effective_date = $('#effective_date_'+i).val();
						data = $('#new_salary_break_'+i).val();
						//arear
						arrear_data = $('#arrear_concate_'+i).val();
						approve_status = $('#approve_status_'+i).val();
					}else{	
						employees +='*'+emp_code;						
						effective_date += '*'+$('#effective_date_'+i).val();	
						data += '*'+$('#new_salary_break_'+i).val();	
						//arear
						arrear_data += '***'+$('#arrear_concate_'+i).val();
						approve_status += '*'+$('#approve_status_'+i).val();					
					}
				
				}				
								
		}
	 
	if(count==0){ alert("At Least One Employee and Approve Status Have To Select."); return; }		
		nocache = Math.random();
		http.open('POST','includes/emp_increment_process.php?action=increment_approved&data='+data 
							+'&effective_date='+effective_date
							+'&arrear_data='+arrear_data
							+'&approve_status='+approve_status
							+'&employees='+employees						
							);
		http.onreadystatechange = response_fn_increment_approved;
		http.send(null); 	
			
}


function response_fn_increment_approved() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;
		//alert(response);				
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Inserted Successfully.').addClass('messagebox_ok').fadeTo(1000,1);
				$(this).fadeOut(5000);
				//clear_data();	
			});
		}
		
	}

}


//salary adjustment entry here
function fn_salary_adjustment_entry()
{
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
			
	var j=0;error='';count=0;amount='';
	
	var selected_empcode_salary_mstid 	= $('#selected_id_emp').val();	
	var cbo_month_selector 				= ($('#cbo_month_selector').val()).split('_');
	var cbo_sal_head 					= $('#cbo_sal_head').val();
	var cbo_is_adjust					= $('#cbo_is_adjust').val();
	
	if(document.getElementById('chk_salary_sheet').checked) chk_salary_sheet=1; else chk_salary_sheet=0;
	
	for (i = 1; i < document.getElementsByName('chk_all').length; i++)
		{					
			if(document.getElementById('chk_all_'+i).checked)
			{
				if($('#adjustment_amount'+i).val()==""){error=true;}
				count++;							
				if(count==1){						
					amount = $('#adjustment_amount'+i).val();						
				}else{					
					amount += '*'+$('#adjustment_amount'+i).val();
				}				
			}								
		}
	
	if(count==0)
	{ 
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$(this).html('At Least One Employee Need To Select For Save Data.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
		return false;
	}	
	else if(error==true)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$(this).html('Empty Amount is not Allow of selected Employee.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
		return false;
	}
	else if(cbo_sal_head==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$(this).html('Please Select The Salary Head.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
		return false;
	}
	nocache = Math.random();
	http.open('POST','includes/save_update_payment.php?action=salary_adjust&amount='+amount 
						+'&selected_empcode_salary_mstid='+selected_empcode_salary_mstid
						+'&cbo_month_selector='+cbo_month_selector[0]
						+'&cbo_sal_head='+cbo_sal_head
						+'&chk_salary_sheet='+chk_salary_sheet
						+'&cbo_is_adjust='+cbo_is_adjust											
				);
	http.onreadystatechange = response_salary_adjustment_entry; 
	http.send(null); 	
			
}


function response_salary_adjustment_entry() 
{
	if(http.readyState == 4){ 		
		var response = (http.responseText).split('###');				
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Inserted Successfully. '+ response[1]).addClass('messagebox_ok').fadeTo(1000,1);
				$(this).fadeOut(5000);	
			});
			
			//fn_check();
			$("#div_list_view").html('');
		}
		
	}

}



function fn_adjustment_record_delete(){
		
		var txt_demp_code=$('#txt_demp_code').val();
		var txt_adjusts_period=$('#txt_adjusts_period').val();
		var txt_adjusts_amount=$('#txt_adjusts_amount').val();
		
		if(txt_demp_code=='' ||  txt_adjusts_period=='' || txt_adjusts_amount==''){				
			$("#messagebox").fadeTo(200,0.1,function(){
				$(this).html('Please Select Employee.').addClass('messagebox_ok').fadeTo(1000,1);
				$(this).fadeOut(3000);
			});
			return false;
		}
		var param = 'txt_demp_code='+txt_demp_code+'&txt_adjusts_period='+txt_adjusts_period+'&txt_adjusts_amount='+txt_adjusts_amount;
		
		nocache = Math.random();
		http.open("POST","includes/save_update_payment.php?action=adjustment_delete&"+param);
		 
		http.onreadystatechange = response_adjustment_record_delete;
		http.send(null); 
	}


function response_adjustment_record_delete() 
{
	if(http.readyState == 4){ 		
		var response = http.responseText;				
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Deleted Successfully.').addClass('messagebox_ok').fadeTo(1000,1);
				$(this).fadeOut(5000);	
			});
			
			$('#txt_demp_code').val('');
			$('#txt_adjusts_period').val('');
			$('#txt_adjusts_amount').val('');
		}
		
	}

}




function fnc_regular_process_bonus()
{
	
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_festival_type 	= escape(document.getElementById('cbo_festival_type').value);
	var cbo_salary_periods 	= escape(document.getElementById('cbo_salary_periods').value);
	var txt_pay_date 	= escape(document.getElementById('txt_pay_date').value);
	var txt_join_date 	= escape(document.getElementById('txt_join_date').value);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Bonus Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_salary_periods').focus();
			$(this).html('Please Select Salary Periods').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_festival_type').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_festival_type').focus();
			$(this).html('Please Select Festival Type').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_join_date').val()=='')
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_join_date').focus();
			$(this).html('Please Select Bonus Calculation Closing Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/bonus_processing.php?action=bonus_processing'+
					'&cbo_festival_type='+cbo_festival_type+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_salary_periods='+cbo_salary_periods+
					'&txt_pay_date='+txt_pay_date+
					'&txt_join_date='+txt_join_date
					);
		http.onreadystatechange = regular_process_bonus_reply;
		http.send(null); 
	}
}

function regular_process_bonus_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	//alert(response[0]);			
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Bonus Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").html('').fadeIn(1000);
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Can Not Process Salary, Duplicate Process for this Periods.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").html('').fadeIn(1000);
			});
		}			
	}
}	




function fnc_regular_re_process_bonus()
{
	
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_festival_type 	= escape(document.getElementById('cbo_festival_type').value);
	var cbo_salary_periods 	= escape(document.getElementById('cbo_salary_periods').value);
	var txt_pay_date 	= escape(document.getElementById('txt_pay_date').value);
	var txt_join_date 	= escape(document.getElementById('txt_join_date').value);
	var txt_emp_code 	=escape(document.getElementById('txt_emp_code').value);;
	
	$("#messagebox").removeClass().addClass('messagebox').text('Bonus Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_salary_periods').focus();
			$(this).html('Please Select Salary Periods').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_festival_type').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_festival_type').focus();
			$(this).html('Please Select Festival Type').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_join_date').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_join_date').focus();
			$(this).html('Please Select Bonus Calculation Closing Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/bonus_processing.php?action=bonus_processing'+
					'&cbo_festival_type='+cbo_festival_type+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_salary_periods='+cbo_salary_periods+
					'&txt_pay_date='+txt_pay_date+
					'&txt_join_date='+txt_join_date+
					'&txt_emp_code='+txt_emp_code
					);
		http.onreadystatechange = regular_re_process_salary_reply;
		http.send(null); 
	}
}

function regular_re_process_salary_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	//alert(response[0]);			
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Bonus Re-Process Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").html('').fadeIn(1000);
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{
				$(this).html('Can Not Process Bonus, Duplicate Process for this Periods.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").html('').fadeIn(1000);
			});
		}			
	}
}	
