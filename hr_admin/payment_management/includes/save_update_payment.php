<?php
date_default_timezone_set('Asia/Dhaka');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

$user_name=$_SESSION['logic_erp']["user_name"];
$date_time= date("Y-m-d H:i:s");

//date_default_timezone_set('UTC');
include('../../../includes/common.php');
include('../../../includes/array_function.php');

extract ($_GET);
extract ($_POST);

if($action=="salary_adjust")
{ 
	//amount,selected_empcode_salary_mstid,cbo_month_selector,cbo_sal_head,chk_salary_sheet
	$amount = explode("*",$amount);
	$selected_emp_explode = explode(",",$selected_empcode_salary_mstid);
	
	for($i=0;$i<count($selected_emp_explode);$i++)
		{
			$not_save_emp='';
			$selected_empcode_salary_mstid = explode("_",$selected_emp_explode[$i]);//emp_code and hrm_salary_mst id
			$employee_code = $selected_empcode_salary_mstid[0];
			$salary_mst_id = $selected_empcode_salary_mstid[1];
			
 			//check salary already adjusted or not
			$chk = return_field_value("id","hrm_salary_adjustment","emp_code='$employee_code' and salary_periods='$cbo_month_selector' and salary_head='$cbo_sal_head' and is_deleted=0");
			if($chk==''){
				//insert salary adjustment here
				$id=return_next_id("id","hrm_salary_adjustment");
				$sql = "insert into hrm_salary_adjustment(id,emp_code,salary_periods,salary_head,amount,show_salary_sheet,inserted_by,insert_date,updated_by)
						values($id,'".$employee_code."','$cbo_month_selector','$cbo_sal_head','".$amount[$i]."','$chk_salary_sheet','$user_name','$date_time','$updated_by')";
				mysql_db_query($DB,$sql) or mysql_error("<br />".$sql);
			
			
				//update salary master and details table
				$isEarningOrDeductive = return_field_value("type","lib_payroll_head","id=$cbo_sal_head");
				if($isEarningOrDeductive==0 || $isEarningOrDeductive==3 || $isEarningOrDeductive==4){ //earning					
					$sql_e = mysql_query("select * from hrm_salary_mst where emp_code='$employee_code' and salary_periods='$cbo_month_selector'");// id=$salary_mst_id");
					$sql_e_result = mysql_fetch_array($sql_e);
					$salary_mst_id=$sql_e_result[id];
					$updatetotalEarningAmount = $sql_e_result[total_earning_amount]+$amount[$i];
					$updatetotalEarningAmount_buyer = $sql_e_result[b_total_earning_amount]+$amount[$i];
					$updatetotalNetAmount = $sql_e_result[net_payable_amount]+$amount[$i];
					$updatetotalNetAmount_buyer = $sql_e_result[b_net_payable_amount]+$amount[$i];
					$tblcolumn = "total_earning_amount=$updatetotalEarningAmount, b_total_earning_amount=$updatetotalEarningAmount_buyer,net_payable_amount=$updatetotalNetAmount, b_net_payable_amount=$updatetotalNetAmount_buyer";
				}
				else //deduction
				{					
					$sql_e = mysql_query("select * from hrm_salary_mst where emp_code='$employee_code' and salary_periods='$cbo_month_selector'");
					$sql_e_result = mysql_fetch_array($sql_e);
					$salary_mst_id=$sql_e_result[id];
					//$updatetotalDeductionAmount = $sql_e_result[total_earning_amount]-$amount[$i];
					//$updatetotalDeductionAmount_buyer = $sql_e_result[b_total_earning_amount]-$amount[$i];
					$updatetotalDeductionAmount = $sql_e_result[total_deduction_amount]+$amount[$i];
					$updatetotalDeductionAmount_buyer = $sql_e_result[b_total_deduction_amount]+$amount[$i];
					$updatetotalDeductionNetAmount = $sql_e_result[net_payable_amount]-$amount[$i];
					$updatetotalDeductionNetAmount_buyer = $sql_e_result[b_net_payable_amount]-$amount[$i];
					$tblcolumn = "total_deduction_amount=$updatetotalDeductionAmount, b_total_deduction_amount=$updatetotalDeductionAmount_buyer,net_payable_amount=$updatetotalDeductionNetAmount, b_net_payable_amount=$updatetotalDeductionNetAmount_buyer";
				
				}
				
				$update_mst_sql = "update hrm_salary_mst set $tblcolumn where id=$salary_mst_id";
				//echo $update_mst_sql;die;
				mysql_db_query($DB,$update_mst_sql) or mysql_error("<br />".$update_mst_sql);
				
				$headAmount = return_field_value("pay_amount","hrm_salary_mst mst, hrm_salary_dtls dtls","mst.id=dtls.salary_mst_id and mst.id=$salary_mst_id and salary_head_id=$cbo_sal_head");
				$updateHeadAmount = $headAmount+$amount[$i];
				$update_dtls_sql = "update hrm_salary_dtls set pay_amount=$updateHeadAmount where salary_mst_id=$salary_mst_id and salary_head_id=$cbo_sal_head";
 				mysql_db_query($DB,$update_dtls_sql) or mysql_error("<br />".$update_dtls_sql);		
				$rc = mysql_affected_rows();
				if($rc==0)
				{
					mysql_query( "insert into hrm_salary_dtls (salary_mst_id,emp_code,salary_head_id,pay_amount)values('".$salary_mst_id."','".$employee_code."','".$cbo_sal_head."','".$amount[$i]."')" );
				}
			}
			else
			{
				$not_save_emp .= " ".$employee_code;
				if(trim($not_save_emp)!=''){
					$mgs = 'Employee\'s Already Adjusted : '.$not_save_emp;
				}else $mgs='';
			}
		}
	
	echo "1###".$mgs;
	exit();
	
}




//salary adjustment record delete
if($action=="adjustment_delete")
	{			
		$salary_mst_id = return_field_value("id","hrm_salary_mst","salary_periods='$txt_adjusts_period' and emp_code='$txt_demp_code'");
		$salary_head = return_field_value("salary_head","hrm_salary_adjustment","salary_periods='$txt_adjusts_period' and emp_code='$txt_demp_code' and amount='$txt_adjusts_amount' and status_active=1 and is_deleted=0");
		$isEarningOrDeductive = return_field_value("type","lib_payroll_head","id=$salary_head");
		
		if($salary_head>0){
			
			mysql_query("UPDATE hrm_salary_adjustment SET updated_by='$user_name',update_date='$date_time',status_active=0,is_deleted=1 WHERE salary_periods='$txt_adjusts_period' and emp_code='$txt_demp_code' and amount='$txt_adjusts_amount' and status_active=1 and is_deleted=0");
			
			//salary master table update
			if($isEarningOrDeductive==0 || $isEarningOrDeductive==3 || $isEarningOrDeductive==4){ //earning
				$sql_e = mysql_query("select * from hrm_salary_mst where id=$salary_mst_id");
				$sql_e_result = mysql_fetch_array($sql_e);
 				$updatetotalEarningAmount = $sql_e_result[total_earning_amount]-$txt_adjusts_amount;
				$updatetotalEarningAmount_buyer = $sql_e_result[b_total_earning_amount]-$txt_adjusts_amount;
				$updatetotalNetAmount = $sql_e_result[net_payable_amount]-$txt_adjusts_amount;
				$updatetotalNetAmount_buyer = $sql_e_result[b_net_payable_amount]-$txt_adjusts_amount;
				$tblcolumn = "total_earning_amount=$updatetotalEarningAmount,b_total_earning_amount=$updatetotalEarningAmount_buyer,net_payable_amount=$updatetotalNetAmount,b_net_payable_amount=$updatetotalNetAmount_buyer";
			}
			else //deduction
			{
 				$sql_e = mysql_query("select * from hrm_salary_mst where id=$salary_mst_id");
				$sql_e_result = mysql_fetch_array($sql_e);
  				$updatetotalDeductionAmount = $sql_e_result[total_deduction_amount]-$txt_adjusts_amount;
				$updatetotalDeductionAmount_buyer = $sql_e_result[b_total_deduction_amount]-$txt_adjusts_amount;
				$updatetotalDeductionNetAmount = $sql_e_result[net_payable_amount]+$txt_adjusts_amount;
				$updatetotalDeductionNetAmount_buyer = $sql_e_result[b_net_payable_amount]+$txt_adjusts_amount;
				$tblcolumn = "total_deduction_amount=$updatetotalDeductionAmount, b_total_deduction_amount=$updatetotalDeductionAmount_buyer,net_payable_amount=$updatetotalDeductionNetAmount, b_net_payable_amount=$updatetotalDeductionNetAmount_buyer";
			
			}			
			$update_mst_sql = "UPDATE hrm_salary_mst SET $tblcolumn WHERE id=$salary_mst_id";
			mysql_db_query($DB,$update_mst_sql) or mysql_error("<br />".$update_mst_sql);
			
			//salary details table update
			$headAmount = return_field_value("pay_amount","hrm_salary_dtls","salary_mst_id=$salary_mst_id and salary_head_id=$salary_head");
			$updateHeadAmount = $headAmount-$txt_adjusts_amount;
			$update_dtls_sql = "update hrm_salary_dtls set pay_amount=$updateHeadAmount where salary_mst_id=$salary_mst_id and salary_head_id=$salary_head";
			mysql_db_query($DB,$update_dtls_sql) or mysql_error("<br />".$update_dtls_sql);			
		}	
		
		echo "1";
		exit();
	}

//fraction month salary process here
if($action=="fraction_month_salary"){
	//salary process here
	$result = mysql_query("select b.* from hrm_employee a,hrm_employee_salary b where a.status_active=1 and a.emp_code=b.emp_code");
	$salary_head_arr=array();
	$emp_code_arr=array();
	$e=0;
	while( $rows = mysql_fetch_array($result) )
	{		
		if( in_array($rows[emp_code],$emp_code_arr) ){} else $emp_code_arr[$e++] = $rows[emp_code];
		$salary_head_arr[$rows[emp_code]][$rows[payroll_head]] = $rows[amount];		
	}
	
	//fraction month salary mst table		
	$exp_periods = explode("_",$cbo_effective_periods);
	$start_effective_periods = $exp_periods[0];
	$exp_date = explode("-",$exp_periods[0]);
	$year = $exp_date[0];
	$month = $exp_date[1];
	
	$process_id = return_next_id( "process_id", "hrm_fraction_month_salary_mst" );
	for($i=1;$i<=$count;$i++)
	{
		$mst_id = return_next_id( "id", "hrm_fraction_month_salary_mst" );
		$e_salary_head= "salary_head_".$i;
		$e_type = "type_".$i;
		$e_amount = "amount_".$i;
		
		$sql_mst = "insert into hrm_fraction_month_salary_mst(id,process_id,effective_periods,salary_head,type,amount,inserted_by,insert_date)
				   values(".$mst_id.",".$process_id.",'".$start_effective_periods."','".$$e_salary_head."','".$$e_type."','".$$e_amount."','".$user_name."',".time().")";	   			
		mysql_query($sql_mst) or die($sql_mst ."<br />". mysql_error());
	}
	
	foreach($emp_code_arr as $key=>$val)
	{	
		$salary_amount=0;
		for($i=1;$i<=$count;$i++)
		{
			$e_salary_head= "salary_head_".$i;
			$e_type = "type_".$i;
			$e_amount = "amount_".$i;
			if($$e_type=='%'){
				$salary_amount += ( $salary_head_arr[$val][$$e_salary_head]*$$e_amount )/100;
			}
			else
			{
				$salary_amount += $salary_head_arr[$val][$$e_salary_head]+$$e_amount;
			}
		}
		
		$id = return_next_id( "id", "hrm_fraction_month_salary_dtls" );
		$process_sql = "insert into hrm_fraction_month_salary_dtls(id,effective_periods,emp_code,amount)
				 	 	values( $id,'".$start_effective_periods."','".$val."',$salary_amount)";
		mysql_query($process_sql) or die( $process_sql."<br />". mysql_error());
		
		//emp_code 	month 	year 	pay_date 	principal_amnt 	interest 	total_amnt_per_inst
		//hrm_loan_payback_scheduling_details table insert
		$loan_id = return_next_id( "id", "hrm_loan_payback_scheduling_details" );
		$loan_sql = "insert into hrm_loan_payback_scheduling_details(id,payback_mst_id,emp_code,month,year,pay_date,principal_amnt,interest,total_amnt_per_inst,inserted_by,insert_date)
				 	 	values( $loan_id,$process_id,'$val', $month, $year,'$start_effective_periods', $salary_amount, 0, 0,'".$user_name."',".time()." )";
		mysql_query($loan_sql) or die( $loan_sql."<br />". mysql_error());
	}
		echo "1";
		exit();
}

// piece_rate_production_entry
if($action=="piece_rate_production_entry")
{
	// uom_arr
	foreach($uom_arr as $key=>$value)
	{
		$uom_arr[$value]=$key;
	}
	$uom_id=$uom_arr[$uom];
	//echo $uom_id."su..re";die;

	$p_date=convert_to_mysql_date($prod_date);
	
	if($update_id=="")
	{
		$ids = return_next_id( "id", "hrm_piece_rate_production" );
		
		$sql="insert into hrm_piece_rate_production (id,
				company_id,
				emp_code,
				prod_date,
				prod_type,
				subsection_id,
				buyer_id,
				order_no,
				process_id,
				prod_qty,
				uom,
				rate,
				rate_original,
				present_rate_dzn,
				amount,
				subsidy_amount,
				subsidy_percentage,
				inserted_by,
				insert_date,
				updated_by
				) values(
					'$ids',
					'$company_id',
					'$emp_code',
					'$p_date',
					'$prod_type',
					'$subsection_id',
					'$buyer_id',
					'$order_no',
					'$process_id',
					'$prod_qty',
					'$uom',
					'$rate',
					'$rate_original',
					'$present_rate_dzn',
					'$amount',
					'$subsidy_amount',
					'$subsidy_percentage',
					'".mysql_real_escape_string($user_name)."',
					'$date_time',
					'$updated_by')";
		//echo $sql;die;
		// inserted_by 	insert_date 	updated_by 	update_date 	status_active 	is_deleted 	is_processed after process value is 1	is_locked			
		mysql_query($sql) or die( $sql."<br />". mysql_error());
		echo 1;
		exit();
	}
	else
	{
		$sql="update hrm_piece_rate_production set 
				company_id='$company_id',
				emp_code='$emp_code',
				prod_date='$p_date',
				prod_type='$prod_type',
				subsection_id='$subsection_id',
				buyer_id='$buyer_id',
				order_no='$order_no',
				process_id='$process_id',
				prod_qty='$prod_qty',
				uom='$uom',
				rate='$rate',
				rate_original='$rate_original',
				present_rate_dzn='$present_rate_dzn',
				amount='$amount',
				subsidy_amount='$subsidy_amount',
				subsidy_percentage='$subsidy_percentage',
				updated_by='".mysql_real_escape_string($user_name)."',
				update_date='$date_time'
				where id='$update_id'";
		//echo $sql;die;
		mysql_query($sql) or die( $sql."<br />".mysql_error());
		echo 2;
		exit();
	}
}

if($action=="piece_rate_production_entry_delete")
{
	//echo $update_id;
	$sql="delete from hrm_piece_rate_production where id='$update_id'";
	mysql_query($sql) or die( $sql."<br />".mysql_error());
	echo 3;
	exit();
}

function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata"; 
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}

?>