<?php

$sttime=time();
$stime=date("H:i:s",time());
session_start();
include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');
include('salary_functions.php');

extract( $_REQUEST );

// 1 for peak app system, 0 for others
$increment=0;

//month generated
if($type=="select_month_generate")
{		
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]."-".$row[ending_date]."</option>";
	}		
	exit();
}

//$txt_emp_code="0010021";
//$cbo_salary_periods="2013-12-01_2013-12-31";
if ($txt_emp_code!="") { $txt_emp_co=explode(",",$txt_emp_code); $txt_emp_code="'".implode("','",$txt_emp_co)."'"; }
//echo $txt_emp_code."<br>";
$cbo_salary_periods=explode("_",$cbo_salary_periods);

set_time_limit(0);
ignore_user_abort(1);

$user_only = $_SESSION['logic_erp']["user_name"];
$user_id_id=$_SESSION['logic_erp']["user_id"];

//mysql_query( "update database_status_process set status=1" );

$txt_from_date =  $cbo_salary_periods[0] ;
$txt_to_date =  $cbo_salary_periods[1] ;
$total_days_att=datediff("d",$txt_from_date,$txt_to_date);
//echo $total_days_att."ffffffff";
/* 
insert into hrm_employee_entitlement (emp_code,ot_entitled,status_active,is_deleted)  (
select emp_code,0,1,0 from hrm_employee where status_active=1 and emp_code not in (select emp_code from hrm_employee_entitlement ))
*/

if(trim($txt_emp_code)!="") $emp_code_search=" and emp_code in ( $txt_emp_code )";
else
{
	$sql = "SELECT emp_code from hrm_employee_entitlement where salary_type=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$emp_code_entitled = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$emp_code_entitled[] = $row[emp_code];
	}
	$tmp_emp_code="'".implode("','",$emp_code_entitled)."'";
	$emp_code_search=" and emp_code in ( $tmp_emp_code )";
}

// Employee OT Calculation
 	$sql=" SELECT emp_code,";   
	for($i=0;$i<$total_days_att; $i++)
	{
		$c_date=add_date($txt_from_date,$i);
		if($i!=0) $sql .=",";
		$sql .=" sum(CASE WHEN attnd_date ='$c_date' THEN total_over_time_min END) AS 'OT".$c_date."',  sum(CASE WHEN attnd_date ='$c_date' and is_regular_day=0 THEN total_over_time_min END) AS 'BOT".$c_date."'";
	}
	$sql .="from  hrm_attendance  where attnd_date between '".$txt_from_date."' and '".$txt_to_date."' $emp_code_search group by emp_code order by emp_code";	
	$exe_sql_attnd=mysql_db_query($DB, $sql);
		$employee_ot=array();
		$b_employee_ot=array();
		while($data=mysql_fetch_array($exe_sql_attnd))
		{
			for($i=0;$i<$total_days_att; $i++)
			{
				$c_date=add_date($txt_from_date,$i);
				$employee_ot[$data['emp_code']][$c_date]=$data['OT'.$c_date];
				$b_employee_ot[$data['emp_code']][$c_date]=$data['BOT'.$c_date];
			}
		} 
	
 // Employee Status Count
	$sql_attnd=" SELECT  emp_code,
			count(CASE WHEN status ='P' THEN emp_code END) AS 'present',
			count(CASE WHEN status ='P' and is_regular_day=0 THEN emp_code END) AS 'gn_present',
			count(CASE WHEN status ='A' THEN emp_code END) AS 'absent',
			count(CASE WHEN status ='A' and is_regular_day=0  THEN emp_code END) AS 'gn_absent',
			count(CASE WHEN status ='D' THEN emp_code END) AS 'late',
			count(CASE WHEN status ='D' and is_regular_day=0  THEN emp_code END) AS 'gn_late',
			count(CASE WHEN status ='GH' THEN emp_code END) AS 'g_holiday',
			count(CASE WHEN status ='FH' THEN emp_code END) AS 'f_holiday',
			count(CASE WHEN status ='CH' THEN emp_code END) AS 'c_holiday',
			count(CASE WHEN status ='W' THEN emp_code END) AS 'weekday',
			count(CASE WHEN status ='MR' THEN emp_code END) AS 'movement',
			count(CASE WHEN status ='CL' THEN emp_code END) AS 'cl',
			count(CASE WHEN status ='FL' THEN emp_code END) AS 'fl',
			count(CASE WHEN status ='EL' THEN emp_code END) AS 'el',
			count(CASE WHEN status ='SL' THEN emp_code END) AS 'sl',
			count(CASE WHEN status ='ML' THEN emp_code END) AS 'ml',
			count(CASE WHEN status ='SpL' THEN emp_code END) AS 'spl',
			count(CASE WHEN status ='LWP' THEN emp_code END) AS 'lwp',
			count(CASE WHEN status ='SP' THEN emp_code END) AS 'sp', 
			count(CASE WHEN is_regular_day=0 THEN emp_code END) AS 'off_day',
			count(CASE WHEN is_regular_day=0 and sign_in_time!='00:00:00' and sign_out_time!='00:00:00' and HOUR(TIMEDIFF(sign_in_time,sign_out_time))>7 THEN emp_code END) AS 'off_day_present',
			count(CASE WHEN is_regular_day=0 and status='D' THEN emp_code END) AS 'off_day_late',
			count(CASE WHEN early_out_min>0 THEN emp_code END) AS 'early_out_count',
			count(CASE WHEN early_out_min>0 and is_regular_day=1  THEN emp_code END) AS 'b_early_out_count'  
		from
			hrm_attendance 
		where 
			 
			attnd_date between '".$txt_from_date."' and '".$txt_to_date."' $emp_code_search group by emp_code";		
	//echo $sql_attnd; die;  and status!='D' and status='P'
	$exe_sql_attnd=mysql_db_query($DB, $sql_attnd);  //sum(CASE WHEN total_over_time_min <>0 THEN total_over_time_min END) AS 'total_over_time_min',
	$attendance_array=array();
	$current_emp_array=array();
	$i=0;
	while($data=mysql_fetch_array($exe_sql_attnd))
	{
		$attendance_array[$data['emp_code']]['present']=$data['present'];
		$attendance_array[$data['emp_code']]['gn_present']=$data['gn_present'];
		$attendance_array[$data['emp_code']]['absent']=$data['absent'];
		$attendance_array[$data['emp_code']]['gn_absent']=$data['gn_absent'];
		$attendance_array[$data['emp_code']]['late']=$data['late'];
		$attendance_array[$data['emp_code']]['gn_late']=$data['gn_late'];
		$attendance_array[$data['emp_code']]['g_holiday']=$data['g_holiday'];
		$attendance_array[$data['emp_code']]['f_holiday']=$data['f_holiday'];
		$attendance_array[$data['emp_code']]['c_holiday']=$data['c_holiday'];
		$attendance_array[$data['emp_code']]['weekday']=$data['weekday'];
		$attendance_array[$data['emp_code']]['movement']=$data['movement'];
		$attendance_array[$data['emp_code']]['cl']=$data['cl'];
		$attendance_array[$data['emp_code']]['fl']=$data['fl'];
		$attendance_array[$data['emp_code']]['el']=$data['el'];
		$attendance_array[$data['emp_code']]['sl']=$data['sl'];
		$attendance_array[$data['emp_code']]['ml']=$data['ml'];
		$attendance_array[$data['emp_code']]['spl']=$data['spl'];
		$attendance_array[$data['emp_code']]['lwp']=$data['lwp'];
		$attendance_array[$data['emp_code']]['sp']=$data['sp'];
		$attendance_array[$data['emp_code']]['off_day']=$data['off_day']; 
		$attendance_array[$data['emp_code']]['off_day_present']=$data['off_day_present']; 
		$attendance_array[$data['emp_code']]['off_day_late']=$data['off_day_late']; 
		$attendance_array[$data['emp_code']]['early_out_count']=$data['early_out_count'];
		$attendance_array[$data['emp_code']]['b_early_out_count']=$data['b_early_out_count'];
		$current_emp_array[$i]=$data['emp_code'];
		$i++; 
	}
 //print_r($attendance_array); die;
	$found_emp_code="'".implode("','",$current_emp_array)."'";
 
// OT POLICY Details
	/*$ot_policy = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$ot_policy[$row_shift[id]]['overtime_rate']=$row_shift[overtime_rate];
		$ot_policy[$row_shift[id]]['overtime_calculation_rule']=$row_shift[overtime_calculation_rule];
		$ot_policy[$row_shift[id]]['max_overtime']=$row_shift[max_overtime];
	}*/
	
// OT SLab POLICY Details
	$ot_policy_slab = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime_slab order by id asc"; 
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	$i=0;
	while($row_shift = mysql_fetch_assoc( $result_shift ) ) 
	{
		$i++;
		$ot_policy_slab[$row_shift[policy_id]][$i]=$row_shift;
		//$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]]['overtime_slot'][$i]=$row_shift[overtime_slot];
		//$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]]['overtime_multiplier'][$i]=$row_shift[overtime_multiplier];
		//$ot_policy_slab[$row_shift[policy_id]][$i]['overtime_head']['overtime_head']=$row_shift[overtime_head];
	}
	 
// Variable Settiings
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['not_pay_deduction'] = mysql_real_escape_string( $row['not_pay_deduction'] );
		$var_hrm_chart[$row['company_name']]['new_emp_card_charge'] = mysql_real_escape_string( $row['new_emp_card_charge'] );
		$var_hrm_chart[$row['company_name']]['ot_rate_multiplier'] = mysql_real_escape_string( $row['ot_rate_multiplier'] );
		$var_hrm_chart[$row['company_name']]['att_bonus_leave_type'] = mysql_real_escape_string( $row['att_bonus_leave_type'] ); 	 
		$treat_resign_on_next_month=mysql_real_escape_string( $row['treat_as_resign'] );
		$new_emp_abs_deduct=mysql_real_escape_string( $row['emp_abs_deduct'] );
	}
	
// Holiday Incentive Policy
	$sql = "SELECT * FROM lib_policy_holiday_incentive ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$var_holiday_mst = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$var_holiday_mst[$row['id']]['pay_for'] = mysql_real_escape_string( $row['pay_for'] );
		$var_holiday_mst[$row['id']]['min_working_hour'] = mysql_real_escape_string( $row['min_working_hour'] );
		$var_holiday_mst[$row['id']]['earning_head'] = mysql_real_escape_string( $row['earning_head'] );
	}
	
// Holiday Details POLICY Details
	$var_holiday_dtls = array();
	$sql_shift = "SELECT * FROM lib_policy_holiday_incentive_definition order by policy_id asc";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	$i=0;
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		if (in_array($row_shift[policy_id],$pol)) $i=$i+1; else $i=0; 
		$pol[]=$row_shift[policy_id];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['parameter_type']=$row_shift[parameter_type];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['percentage_formula']=$row_shift[percentage_formula];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['base_head']=$row_shift[base_head];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['amount']=$row_shift[amount];
	}
	
// Arrear Calculations
	$sql = "SELECT a.*,b.emp_code FROM hrm_arrear_earning_dtls a, hrm_arrear_earning_mst b WHERE b.id=a.mst_id and a.payment_month like '$cbo_salary_periods[0]' and b.is_approved=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$increment_arrear = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$increment_arrear[$row['emp_code']][$row['payment_month']] =  $row['amount'] ;
	}
	
// Loan Pay Array
 
	$sql = "select sum(a.total_amnt_per_inst) as total_amnt_per_inst,a.pay_date,a.emp_code, group_concat(a.id) as vid, b.loan_type from  hrm_loan_payback_scheduling_details a, hrm_loan_application b, hrm_loan_payback_scheduling_mst c  where a.pay_date  like '".date("Y-m", strtotime($txt_from_date))."%'  and is_paid=0 and b.id=c.loan_id and c.id=a.payback_mst_id  group by a.emp_code,b.loan_type";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$loan_amount_array = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$loan_amount_array[$row['emp_code']][$row['loan_type']] =  $row['total_amnt_per_inst'] ;
		if( $arr_loan_id=="") $arr_loan_id=$vid; else $arr_loan_id .=",".$vid; 
	}

// Increment Amount Processing 
	/*
	$emp_code_search=" and a.emp_code in ( $found_emp_code )";
	 $sql = "select a.emp_code,a.new_salary_grade,a.new_salary_rule,a.new_gross_salary,b.* from  hrm_increment_mst a,hrm_increment_dtls b where a.id=b.mst_id and a.is_approved=2 and a.effective_date <= '".$txt_to_date."'  and a.is_posted=0 and a.initial=0 $emp_code_search  order by id asc"; 
	//between '".$txt_from_date."' and '".$txt_to_date."'
 	//echo $sql; die;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$increment_amount_array = array(); 
	$chk_array = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		mysql_query("update hrm_employee_salary set amount=$row[per_head_increment] where emp_code='$row[emp_code]' and payroll_head='$row[payroll_head]'");
		if(!in_array($row['emp_code'],$chk_array))
		{
			 if ($row[new_salary_rule]!=0) $sql_qry =" salary_rule='$row[new_salary_rule]',";
			 if ($row[new_salary_grade]!=0) $sql_qry .=" salary_grade='$row[new_salary_grade]',";
			
			mysql_query("update hrm_employee set ".$sql_qry." gross_salary='$row[new_gross_salary]' where emp_code='$row[emp_code]'");
			$chk_array[$row['emp_code']]=$row['emp_code'];
			$sql_qry="";
		}
	}
	foreach($chk_array as $val)
	{
		mysql_query("update hrm_increment_mst set is_posted=1 where emp_code='$val' and initial=0 and is_approved=2 and effective_date <= '".$txt_to_date."'");
	}
 */
 
if( $increment==0 )
{
	$emp_code_search=" and a.emp_code in ( $found_emp_code )";
	 $sql = "select a.emp_code,a.new_salary_grade,a.new_salary_rule,a.new_gross_salary,b.* from  hrm_increment_mst a,hrm_increment_dtls b where a.id=b.mst_id and a.is_approved=2 and a.effective_date <= '".$txt_to_date."'  and a.is_posted=0 and a.initial=0 $emp_code_search  order by id asc"; 
	//between '".$txt_from_date."' and '".$txt_to_date."'
	//echo $sql; die;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$increment_amount_array = array(); 
	$chk_array = array(); 
	while( $row = mysql_fetch_assoc( $result ) ) { 	
		mysql_query("update hrm_employee_salary set amount=$row[per_head_increment] where emp_code='$row[emp_code]' and payroll_head='$row[payroll_head]'");
		if(!in_array($row['emp_code'],$chk_array))
		{
			 if ($row[new_salary_rule]!=0) $sql_qry =" salary_rule='$row[new_salary_rule]',";
			 if ($row[new_salary_grade]!=0) $sql_qry .=" salary_grade='$row[new_salary_grade]',";
			
			mysql_query("update hrm_employee set ".$sql_qry." gross_salary='$row[new_gross_salary]' where emp_code='$row[emp_code]'");
			$chk_array[$row['emp_code']]=$row['emp_code'];
			$sql_qry="";
		}
	}
	foreach($chk_array as $val)
	{
		mysql_query("update hrm_increment_mst set is_posted=1 where emp_code='$val' and initial=0 and is_approved=2 and effective_date like '$txt_from_date'");
	}
}
else
{
	$emp_code_search=" and a.emp_code in ( $found_emp_code )";
	$sql = "select a.emp_code,a.new_salary_grade,a.new_salary_rule,a.new_gross_salary,b.* from  hrm_increment_mst a,hrm_increment_dtls b where a.id=b.mst_id and a.is_approved=2 and a.effective_date <= '".$txt_to_date."' and a.is_posted=0 and a.initial=0 $emp_code_search order by id asc"; 
	//between '".$txt_from_date."' and '".$txt_to_date."'
	
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$increment_amount_array = array(); 
	$chk_array = array(); 
	$new_inc_amnt=array();
	while( $row = mysql_fetch_assoc( $result ) ) { 	
		$sql_qry="";
		mysql_query("delete from hrm_employee_salary where emp_code='$row[emp_code]'");
		if(!in_array($row['emp_code'],$chk_array))
		{
			 if ($row[new_salary_rule]!=0) $sql_qry =" salary_rule='$row[new_salary_rule]',";
			 if ($row[new_salary_grade]!=0) $sql_qry .=" salary_grade='$row[new_salary_grade]',";
			
			mysql_query("update hrm_employee set ".$sql_qry." gross_salary='$row[new_gross_salary]' where emp_code='$row[emp_code]'");
			$chk_array[$row['emp_code']]=$row['emp_code'];
			$sql_qry="";
			//$master_id="";
		}
		//$new_inc_amnt[$row[payroll_head]]=$row[payroll_head];
		 $namount=0;
			if(trim($row[type])=="%")
			{
				if($row[base_head]==20) $namount=round(($row[new_gross_salary]*$row[percentage_formula])/100);
			}
		 	// echo $row[per_head_increment]; die;
		if($master_id=="")$master_id = return_next_id( "id", "hrm_employee_salary" );  else $master_id+=1;
		if ($sql_mst!="") $sql_mst.=",";
				$sql_mst.="('$master_id','$row[emp_code]','$row[payroll_head]','$row[type]','$row[percentage_formula]','$row[base_head]','$namount') ";
	}
	$sql="insert into hrm_employee_salary (id, emp_code,payroll_head,type,percentage_formula,base_head,amount ) values ".$sql_mst;
	if ($sql_mst!="") mysql_query( $sql ) or die (mysql_error()); 	
	$sql_mst="";
 
	foreach($chk_array as $val)
	{
		mysql_query("update hrm_increment_mst set is_posted=1 where emp_code='$val' and initial=0 and is_approved=2 and effective_date <= '".$txt_to_date."'");
	}
}
// Attendance Bonus Calculation 
	$sql_data="select * from  lib_policy_attendance_bonus_definition order by id asc";
	$sql_data_exe=mysql_query($sql_data); 
	$attendance_bonus_pol=array();
	$polbon=array();
	$i=0;
	while($data=mysql_fetch_array($sql_data_exe))
	{
		if (in_array($data[policy_id],$polbon)) $i=$i+1; else $i=0; 
		$polbon[]=$data[policy_id];
		
		$attendance_bonus_pol[$data[policy_id]][$i]['total_leave_criteria']=$data[total_leave_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['cl_leave_criteria']=$data[cl_leave_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['sl_leave_criteria']=$data[sl_leave_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['lwp_leave_criteria']=$data[lwp_leave_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['spl_leave_criteria']=$data[spl_leave_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['el_leave_criteria']=$data[el_leave_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['ml_leave_criteria']=$data[ml_leave_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['late_criteria']=$data[late_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['absent_criteria']=$data[absent_criteria];
		$attendance_bonus_pol[$data[policy_id]][$i]['amount']=$data[amount];
	}
// late deduc Calculation 
	$sql_data="select * from  lib_policy_late_deduction_criteria  order by id asc";
	$sql_data_exe=mysql_query($sql_data); 
	$late_deduction_pol=array();
	$polbon=array();

	while($data=mysql_fetch_array($sql_data_exe))
	{
		if (in_array($data[policy_id],$polbon)) $i=$i+1; else $i=0; 
		$polbon[]=$data[policy_id];
		$late_deduction_pol[$data[policy_id]][$i]['late_slot']=$data[late_slot];
		$late_deduction_pol[$data[policy_id]][$i]['parameter_type']=$data[parameter_type];
		$late_deduction_pol[$data[policy_id]][$i]['percentage_formula']=$data[percentage_formula];
		$late_deduction_pol[$data[policy_id]][$i]['base_head']=$data[base_head];
		$late_deduction_pol[$data[policy_id]][$i]['amount']=$data[amount];
	}  
// Absent deduction Calculation 
	$sql_data="select * from  lib_policy_absent_deduction_definition order by id asc";
	$sql_data_exe=mysql_query($sql_data);
	$absent_deduction_pol=array();
	$polbon=array(); 
	while($data=mysql_fetch_array($sql_data_exe))
	{	 
		if (in_array($data[policy_id],$polbon)) $i=$i+1; else $i=0; 
		$polbon[]=$data[policy_id];
		$absent_deduction_pol[$data[policy_id]][$i]['parameter_type']=$data[parameter_type];
		$absent_deduction_pol[$data[policy_id]][$i]['percentage_formula']=$data[percentage_formula];
		$absent_deduction_pol[$data[policy_id]][$i]['base_head']=$data[base_head];
		$absent_deduction_pol[$data[policy_id]][$i]['amount']=$data[amount];
	}
// Leave LWp Calculation 	
	$sql_data="select * from  lib_policy_leave_definition where  leave_type='LWP' order by id asc";
	$sql_data_exe=mysql_query($sql_data);
	$lwp_deduction_pol=array();
	$polbon=array(); 
	while($data=mysql_fetch_array($sql_data_exe))
	{
		if (in_array($data[policy_id],$polbon)) $i=$i+1; else $i=0; 
		$polbon[]=$data[policy_id];
		$policy_def=explode('_',$data[deduction]);
		$lwp_deduction_pol[$data[policy_id]][$i]['formula']=$policy_def[0];
		$lwp_deduction_pol[$data[policy_id]][$i]['amount']=$policy_def[1];
		
	}
// Discpline Calculation 
	$sql_data="select a.*,b.emp_code from  hrm_disciplinary_info_dtls a, hrm_disciplinary_info_mst b where a.disc_mst_id=b.id and b.emp_code in ( $found_emp_code ) order by b.id desc";
	$sql_data_exe=mysql_query($sql_data);
	$suspension_deduction_pol=array();
	$polbon=array();
	while($data=mysql_fetch_array($sql_data_exe))
	{
		if (in_array($data[emp_code],$polbon)) $i=$i+1; else $i=0; 
		$polbon[]=$data[emp_code];
		$suspension_deduction_pol[$data[emp_code]][$i]['impact_salary_head']=$data[impact_salary_head];
		$suspension_deduction_pol[$data[emp_code]][$i]['impact_salary_head_perc']=$data[impact_salary_head_perc];
		 
	}
// Salary Breakdown Employee Wise
	$salary_breakdown = array();
	$sql_sal="select * from hrm_employee_salary  ";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_breakdown[$row_sal[emp_code]][$row_sal[payroll_head]]=$row_sal[amount];
	}
// All Salary Head for This Company	
	$salary_head_list = array();
	$salary_head_list_i= array();
	$salary_head_type= array();
	$sql_sal="select * from lib_payroll_head where is_applicable=1 and applicable_in_process=1 and status_active=1 and is_deleted=0 order by id"; 
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	$i=0;
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_head_list_head[$row_sal[payroll_head]]=$row_sal[id];
		  $salary_head_list_i[$i]=$row_sal[id];
		  $salary_head_type[$i]=$row_sal[type];
		  $i++;
	}
	
	//print_r( $salary_head_list_i);die;
 // Separated Employee
	$sql = "select separated_from,emp_code from  hrm_separation where separated_from >= '$txt_from_date' order by id asc";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$separated_employee_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$separated_employee_list[$row['emp_code']] =  $row['separated_from'];
	}	
 // Designation Wise OT Alllowance
	$sql = "select id,allowance_rate,allowance_treatment from   lib_designation where status_active=1 and is_deleted=0 and allowance_rate>0 order by id asc";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_allowance = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_allowance[$row[id]]['allowance_rate'] =  $row['allowance_rate'] ;
		$designation_allowance[$row[id]]['allowance_treatment'] =  $row['allowance_treatment'] ;
	}

// Salary Adjustment Entry Check
	$sqldd = "select a.id,a.adjustment_type,c.emp_code from  hrm_salary_adjust_mst a,hrm_salary_adjusted_employee c where a.id=c.salary_adjustment_mst_id and a.salary_period_id= '".$txt_from_date."' and a.status_active=1 and a.is_deleted=0";
	$resultdd = mysql_query( $sqldd ) or die( $sqldd . "<br />" . mysql_error() );
	$adjustment_employee = array();
	$adjustment_id=array();
	$adjustment_type=array();
	$i=0;
	while( $rowdd = mysql_fetch_assoc( $resultdd ) ) {
		$adjustment_employee[$rowdd['id']][] =  $rowdd['emp_code'] ; //[$rowdd['emp_code']][$rowdd['adjustment_type']]
		if(!in_array($rowdd['id'],$adjustment_id)) $adjustment_id[]=$rowdd['id'];
		$adjustment_type[$rowdd['id']]=$rowdd['adjustment_type'];
	}
	$adjust_id=implode(",",$adjustment_id);
	$sqldd = "SELECT * FROM hrm_salary_adjustment_criteria WHERE salary_adjustment_mst_id in ( $adjust_id ) order by payroll_head_id,salary_adjustment_mst_id";
	if ($adjust_id!="")
		$resultdd = mysql_query( $sqldd ) or die( $sqldd . "<br />" . mysql_error() );
	$salary_adjustments = array();
	$salary_adjustments_emp = array();
	$i=0;
	$sal_adjust_cnt=array();
	while( $rowdd = mysql_fetch_assoc( $resultdd ) ) {
		if (count($adjustment_employee[$rowdd['salary_adjustment_mst_id']])>0)
		{	
			foreach($adjustment_employee[$rowdd['salary_adjustment_mst_id']] as $key)
			{
			  if ( count($salary_adjustments[$key][$rowdd['payroll_head_id']] )!=0) $i=count($salary_adjustments[$key][$rowdd['payroll_head_id']]); else $i=0;
			  $sal_adjust_cnt[$key]['emp_count']++;
			  $sal_adjust_cnt[$key][$rowdd['payroll_head_id']]['head_count']++;
			  $salary_adjustments[$key][$rowdd['payroll_head_id']][$i]['criteria_id']=$rowdd['criteria_id'];
			  $salary_adjustments[$key][$rowdd['payroll_head_id']][$i]['adjustment_amount']=$rowdd['adjustment_amount'];
			  $salary_adjustments[$key][$rowdd['payroll_head_id']][$i]['formula_id']=$rowdd['formula_id'];
			  $salary_adjustments[$key][$rowdd['payroll_head_id']][$i]['formula_value']=$rowdd['formula_value'];
			  $salary_adjustments[$key][$rowdd['payroll_head_id']][$i]['base_head_id']=$rowdd['base_head_id'];
			  $salary_adjustments[$key][$rowdd['payroll_head_id']][$i]['payroll_head_id']=$rowdd['payroll_head_id'];
			  $salary_adjustments[$key][$rowdd['payroll_head_id']][$i]['mst_id']=$rowdd['salary_adjustment_mst_id'];
			  $i++;
			}
		}
	} 
//print_r( get_adjustment_amount( $salary_adjustments, "001420", 21, $emp_salary_sheet, 28 )); die;
 //print_r( $salary_adjustments); die;
 // Early out deduc Calculation 
	$sql_data="select * from  lib_policy_early_deduction_criteria  order by id asc";
	$sql_data_exe=mysql_query($sql_data); 
	$early_out_deduction_pol=array();
	$polbon=array();
	while($data=mysql_fetch_array($sql_data_exe))
	{
		if (in_array($data[policy_id],$polbon)) $i=$i+1; else $i=0; 
		$polbon[]=$data[policy_id];
		$early_out_deduction_pol[$data[policy_id]][$i]['early_slot']=$data[early_slot];
		$early_out_deduction_pol[$data[policy_id]][$i]['parameter_type']=$data[parameter_type];
		$early_out_deduction_pol[$data[policy_id]][$i]['percentage_formula']=$data[percentage_formula];
		$early_out_deduction_pol[$data[policy_id]][$i]['base_head']=$data[base_head];
		$early_out_deduction_pol[$data[policy_id]][$i]['amount']=$data[amount];
	}
	
 	/*$sql = "SELECT emp_code,shift_date,overtime_policy FROM hrm_duty_roster_process where shift_date between '$txt_from_date' and '$txt_to_date' ";
	$ot_policy_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$ot_policy_list[$row['emp_code']][$row['shift_date']] = mysql_real_escape_string($row['overtime_policy']);
	}*/
//Used Short  Terms 
//A=Absent, P=Present, D=Late/Delay, CL,ML,SL,EL...=All Leaves,SP=Suspended, H=Holiday, W=Weekend, R=Regular Day, MR=Movement Register

if ($action=="reconcile_salary_process")// Processing Started Here
{
	$i=0;
 	$tot_days=datediff("d", $txt_from_date, $txt_to_date);
	//echo $tot_days."ggggggg";
	if(trim($txt_emp_code)==""){ //new salary process
		$sql_emp="select * from hrm_employee where salary_rule<>0 and emp_code in ( $found_emp_code )   ";// and emp_code in ( $found_emp_code ) "; 
	}
	else //salary re process
	{
		mysql_query("delete a.*, b.* from  hrm_reconcile_salary_mst a,  hrm_reconcile_salary_dtls b where a.id=b.salary_mst_id and a.emp_code in ( $txt_emp_code ) and a.salary_periods = '".$txt_from_date."'");
		$sql_emp="select * from hrm_employee where emp_code in ( $found_emp_code ) and salary_rule<>0 ";
	}
// Duplicate Employee Check

	$sqldd = "select emp_code from  hrm_reconcile_salary_mst where salary_periods = '".$txt_from_date."'";
	$resultdd = mysql_query( $sqldd ) or die( $sqldd . "<br />" . mysql_error() );
	$duplicate_salary = array();
	$i=0;
	while( $rowdd = mysql_fetch_assoc( $resultdd ) ) {
		$duplicate_salary[$i] =  $rowdd['emp_code'] ;
		$i++;
	}

		$exe_sql_emp=mysql_db_query($DB, $sql_emp);
		$emp_salary_sheet=array();
		$emp_extra_details=array();
		$no_process=array();
		
		$n=0;
		$txt_from_date_exp = $txt_from_date;
		$txt_to_date_exp = $txt_to_date;
		while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
		{
			//echo "m".$m."=";
			//$m++;
			//$row_emp[joining_date]="2014-01-12";
			$emp_condition=0; // Regular Worker
			$old_mod="";
			$txt_from_date =  $txt_from_date_exp;
			$txt_to_date =  $txt_to_date_exp;
			if ($row_emp[status_active]==0) // In active Employee
			{
				$old_mod= $separated_employee_list[$row_emp[emp_code]];
				if (strtotime($row_emp[joining_date])>strtotime($txt_from_date) && strtotime($row_emp[joining_date])<=strtotime($txt_to_date)){
					$txt_from_date=$row_emp[joining_date];
				}
				if (strtotime($old_mod)>strtotime($txt_from_date) && strtotime($old_mod)<=strtotime($txt_to_date))
				{
					$emp_condition=1; // Resign Worker
					$txt_to_date=$old_mod;
				}
				else if(strtotime($old_mod)>strtotime($txt_to_date))
				{
					if( $treat_resign_on_next_month==1 && strtotime($old_mod)==strtotime(add_date($txt_to_date,1)))
						$emp_condition=1; // resign Worker
					else
						$emp_condition=0; // Regular Worker
				}
				else $emp_condition=3; // not  Worker
			}
			
			if ($row_emp[status_active]==1)
			{
				if (strtotime($row_emp[joining_date])>strtotime($txt_from_date) && strtotime($row_emp[joining_date])<=strtotime($txt_to_date))
				{
					$emp_condition=2; //New Worker
					$txt_from_date=$row_emp[joining_date];
				}
				else if (strtotime($row_emp[joining_date])>strtotime($txt_to_date) ) $emp_condition=3; // not Worker
			}
		$check_salary_presence=""; //return_field_value("emp_code","hrm_salary_mst"," emp_code='$row_emp[emp_code]' and salary_periods like '".date("Y-m",strtotime(convert_to_mysql_date($txt_from_date)))."%'");
	 	if (in_array($row_emp[emp_code],$duplicate_salary))  $check_salary_presence=1;
		//if ($row_emp[emp_code]=="00000162"){  echo $emp_condition."=".$old_mod."=".$txt_to_date;  die; }
		//	 echo $check_salary_presence; die;
		
		if ($emp_condition!=3 && $check_salary_presence=="") 
		{
			//echo "n-".$n."<br/> \n";
			//$n++;
			
			$total_ot_hr_nrd=0;
			$emp_salary_sheet[$row_emp[emp_code]][0]=$row_emp[gross_salary];
			$tmpr=($salary_breakdown[$row_emp[emp_code]][1]/208)*$var_hrm_chart[$row_emp['company_id']]['ot_rate_multiplier'];
			$hourly_ot_rate= number_formating( $tmpr, $txt_precision, $cbo_ot_rate_rounding );
			$minutly_ot_rate=$hourly_ot_rate/60;
			
			$daily_gross_salary=$emp_salary_sheet[$row_emp[emp_code]][0]/$tot_days; // from Var Setting
			 
				 $present_days=$attendance_array[$row_emp['emp_code']]['present'];
				 $absent_days=$attendance_array[$row_emp['emp_code']]['absent'];
				 $late_days=$attendance_array[$row_emp['emp_code']]['late'];
				 $g_holiday_days=$attendance_array[$row_emp['emp_code']]['g_holiday'];
				 $f_holiday_days=$attendance_array[$row_emp['emp_code']]['f_holiday'];
				 $c_holiday_days=$attendance_array[$row_emp['emp_code']]['c_holiday'];
				 $weekday_days=$attendance_array[$row_emp['emp_code']]['weekday'];
				 $movement_days=$attendance_array[$row_emp['emp_code']]['movement'];
				 $c_leave_days=$attendance_array[$row_emp['emp_code']]['cl'];
				 $s_leave_days=$attendance_array[$row_emp['emp_code']]['sl'];
				 $e_leave_days=$attendance_array[$row_emp['emp_code']]['el'];
				 $m_leave_days=$attendance_array[$row_emp['emp_code']]['ml'];
				 $f_leave_days=$attendance_array[$row_emp['emp_code']]['fl'];
				 $spl_leave_days=$attendance_array[$row_emp['emp_code']]['spl'];
				 $lwp_leave_days=$attendance_array[$row_emp['emp_code']]['lwp'];
				 $suspended_days=$attendance_array[$row_emp['emp_code']]['sp'];
				 
				 $total_cal_days=$present_days+$absent_days+$late_days+$g_holiday_days+$f_holiday_days+$c_holiday_days+$weekday_days+$movement_days+$c_leave_days+ $s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days+$lwp_leave_days+$f_leave_days+$suspended_days;
				 $total_leave_days=$c_leave_days+$s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days+ $lwp_leave_days+$f_leave_days; 
			
				//for buyer salary
				$gn_present_days=$present_days-$attendance_array[$row_emp['emp_code']]['gn_present'];
				$gn_absent_days=$absent_days-$attendance_array[$row_emp['emp_code']]['gn_absent'];
				$gn_late_days=$late_days-$attendance_array[$row_emp['emp_code']]['gn_late'];
				
				$total_off_day=$attendance_array[$row_emp['emp_code']]['off_day'];
				$gn_g_holiday_days=$g_holiday_days+$c_holiday_days;
				$gn_f_holiday_days=$f_holiday_days;
				//$gn_weekday_days=$total_off_day-($gn_g_holiday_days+$gn_f_holiday_days);
				$gn_weekday_days=$total_off_day-($g_holiday_days+$f_holiday_days+$c_holiday_days);
				 	 
			// print_r($attendance_array[$row_emp['emp_code']]); die;
			
			
			if ($emp_condition==2) // New
				$total_payable_days=datediff("d",$row_emp[joining_date],$txt_to_date);	//New Join 
			else if ($emp_condition==1) // Resign
				$total_payable_days=datediff("d",$txt_from_date,$old_mod);	// Resign
			else
				$total_payable_days=$tot_days;	//Regular
			
			
			//echo $not_payable_days; die;
			$calendar_days=$tot_days;
			if ( $emp_condition==1 )$not_payable_days=$calendar_days-$total_payable_days+1; else $not_payable_days=$calendar_days-$total_payable_days;
			
			$total_working_days=$calendar_days-($g_holiday_days+$f_holiday_days+$c_holiday_days+$weekday_days+$not_payable_days);
			$gn_total_working_days=$calendar_days-($gn_g_holiday_days+$gn_f_holiday_days+$gn_weekday_days+$not_payable_days);	//buyer
			$net_payable_days=$calendar_days-($absent_days+$lwp_leave_days+$not_payable_days);
			$gn_net_payable_days=$calendar_days-($gn_absent_days+$lwp_leave_days+$not_payable_days);	//buyer
			
			$salary_breakdown[$row_emp[emp_code]][20]=$emp_salary_sheet[$row_emp[emp_code]][0];
		
			// Start Traversing Slary Head and calcutae corresspndiong amount
			// print_r($salary_head_list_i);die;
			
			for($j=0; $j<count($salary_head_list_i);$j++)
			{
				
				switch ($salary_head_list_i[$j])
				{
					case 1:
						$emp_salary_sheet[$row_emp[emp_code]][1]= $salary_breakdown[$row_emp[emp_code]][1];  	 // Basic
						$daily_basic_salary=$salary_breakdown[$row_emp[emp_code]][1]/$tot_days; // from Var Setting
						$ot_rate=$hourly_ot_rate;
						
						break;
					case 2:
						$emp_salary_sheet[$row_emp[emp_code]][2]= $salary_breakdown[$row_emp[emp_code]][2];  // House Rent
						break;
					case 3:
					 	$emp_salary_sheet[$row_emp[emp_code]][3]= $salary_breakdown[$row_emp[emp_code]][3];  // Medical
						break;
					case 4:
						$emp_salary_sheet[$row_emp[emp_code]][4]= $salary_breakdown[$row_emp[emp_code]][4];  // Conveyence
						break;
					case 5:
						$old_hr=0;
						$nrd_old_hr=0;
						$old_slot=0;
						$ot_slot=0;  	
						$allowance_ot=0;
						$old_allowance_ot=0;
						/*$ot_policy_list[$row['emp_code']][$row['shift_date']]
						if( $row_emp['roster_policy_id']!=0 )
							$ot_id=$ot_policy_list[$row_emp['emp_code']][$row_emp['attnd_date']];
						else
							$ot_id=$row_emp['policy_overtime_id'];	*/
						
						foreach( $ot_policy_slab[$row_emp['overtime_policy']] as $key )
						{
							$u++;
							$ot_slot=$ot_slot+($key['overtime_slot']*60);//   $sql_data_rslt[overtime_slot]*60; 
							$ot_multiplier= $key['overtime_multiplier'];//$sql_data_rslt[overtime_multiplier];
							// echo $key['overtime_multiplier']."=".$hourly_ot_rate;
							$total_ot_hr=0;
							$total_ot_hr_nrd=0;
							$hr=0;
							$min=0;
							$hr_nrd=0;
							$min_nrd=0;
							$hor=0;
							$mint=0;
							$hor1=0;
							$mint1=0;
							if ($designation_allowance[$row_emp[designation_id]]['allowance_rate']<1)
							{
								
									if($var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction']!=3) 
									{
										for($i=0;$i<$total_days_att; $i++)
										{
											$c_date=add_date($txt_from_date,$i);
											$total_ot_hr +=get_buyer_ot_hr($employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
											if ($key['overtime_head']==5)
												$total_ot_hr_nrd +=get_buyer_ot_hr($b_employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
										}
									}
									else  // At Actual
									{
										for($i=0;$i<$total_days_att; $i++)
										{
											$c_date=add_date($txt_from_date,$i);
											$tmpp=get_buyer_ot_hr($employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
											$tmppd=explode(".",$tmpp);
											$hr+=$tmppd[0];
											$min+=$tmppd[1];
											
											if ($key['overtime_head']==5)
											{
												$tmpp_nrd=get_buyer_ot_hr($b_employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
												
												$tmppd_nrd=explode(".",$tmpp_nrd);
												$hr_nrd+=$tmppd_nrd[0];
												$min_nrd+=$tmppd_nrd[1];
											}
										}
										
										$total_ot_hr =($hr+ floor($min/60)).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
										$total_ot_hr_nrd=($hr_nrd + floor($min_nrd/60)).".".str_pad(($min_nrd%60),2,"0",STR_PAD_LEFT);
									}
								//echo $old_hr."dfd".$total_ot_hr; die;
								$splt_old=explode(".",$old_hr);
								$splt_total_ot=explode(".",$total_ot_hr);
								
								$total_ot_hr=(($splt_total_ot[0]*60)+$splt_total_ot[1])-(($splt_old[0]*60)+$splt_old[1]);//    $total_ot_hr-$old_hr;
								$old_hr= (($splt_total_ot[0]*60)+$splt_total_ot[1])+(($splt_old[0]*60)+$splt_old[1]); //$old_hr+$total_ot_hr;
								
								$total_ot_hr =(  floor($total_ot_hr/60)).".".str_pad(($total_ot_hr%60),2,"0",STR_PAD_LEFT);
								$old_hr=( floor($old_hr/60)).".".str_pad(($old_hr%60),2,"0",STR_PAD_LEFT);
								
								if ($key['overtime_head']==5)
								{
									$total_ot_hr_nrd=$total_ot_hr_nrd-$nrd_old_hr;
									$nrd_old_hr=$total_ot_hr_nrd+$nrd_old_hr;
								}
									
							}
							else
							{
								if($var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction']!=3) 
								{
									for($i=0;$i<$total_days_att; $i++)
									{
										$c_date=add_date($txt_from_date,$i);
										
										$total_ot_hr +=get_buyer_ot_hr($employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
										
										$total_ot_hr_nrd +=get_buyer_ot_hr($b_employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
										
									
									}
								}
								else  // At Actual
								{
									for($i=0;$i<$total_days_att; $i++)
									{
										$c_date=add_date($txt_from_date,$i);
										
										$tmpp=get_buyer_ot_hr($employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
										$tmppd=explode(".",$tmpp);
										$hr+=$tmppd[0];
										$min+=$tmppd[1];
									
										$tmpp_nrd=get_buyer_ot_hr($b_employee_ot[$row_emp['emp_code']][$c_date],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
										$tmppd_nrd=explode(".",$tmpp_nrd);
										$hr_nrd+=$tmppd_nrd[0];
										$min_nrd+=$tmppd_nrd[1];
									}
									$total_ot_hr =($hr+ floor($min/60)).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
									$total_ot_hr_nrd=($hr_nrd + floor($min_nrd/60)).".".str_pad(($min_nrd%60),2,"0",STR_PAD_LEFT);
								}
							
								if ($designation_allowance[$row_emp[designation_id]]['allowance_treatment']==1) // Holiday as Allowance
								{
									$total_ot_hr=$total_ot_hr-$old_hr;
									$old_hr= $old_hr+$total_ot_hr;
									
									$total_ot_hr_nrd=$total_ot_hr_nrd-$nrd_old_hr;
									$nrd_old_hr=$total_ot_hr_nrd+$nrd_old_hr;
									
									$emp_salary_sheet[$row_emp[emp_code]][41]=$nrd_old_hr*$designation_allowance[$row_emp[designation_id]]['allowance_rate'];
									$emp_extra_details[$row_emp[emp_code]][41]['ot_hr']=$nrd_old_hr;
								}
								else if ($designation_allowance[$row_emp[designation_id]]['allowance_treatment']==2)   // General Day as Allowance
								{
									//$allowance_ot=0;
									//$old_allowance_ot=0;
									$total_ot_hr=$total_ot_hr-$old_hr;
									$old_hr= $old_hr+$total_ot_hr;
									
									$total_ot_hr_nrd=$total_ot_hr_nrd-$nrd_old_hr;
									$nrd_old_hr=$total_ot_hr_nrd+$nrd_old_hr;
									
									$allowance_ot=$old_hr-$nrd_old_hr;
									$total_ot_hr=$total_ot_hr_nrd;
									$emp_salary_sheet[$row_emp[emp_code]][41]=$allowance_ot*$designation_allowance[$row_emp[designation_id]]['allowance_rate'];
									$emp_extra_details[$row_emp[emp_code]][41]['ot_hr']=$allowance_ot;
								}
								else
								{
									$total_ot_hr=$total_ot_hr-$old_hr;
									$old_hr= $old_hr+$total_ot_hr;
									
									$total_ot_hr_nrd=$total_ot_hr_nrd-$nrd_old_hr;
									$nrd_old_hr=$total_ot_hr_nrd+$nrd_old_hr;
									
									$allowance_ot=($old_hr);
									$total_ot_hr=0;//$total_ot_hr_nrd;
									$emp_salary_sheet[$row_emp[emp_code]][41]=$allowance_ot*$designation_allowance[$row_emp[designation_id]]['allowance_rate'];
									$emp_extra_details[$row_emp[emp_code]][41]['ot_hr']=$allowance_ot;
								}
							}
							
							if($key['overtime_head']==5)
							{
								
											//$minutly_ot_rate
								$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 5, $emp_salary_sheet, $tot_days ));
								
								if ($designation_allowance[$row_emp[designation_id]]['allowance_rate']<1)
								{
									$total_ot_hr +=$adjust[0];
									$tmppd=explode(".",$total_ot_hr);
									$hor=$tmppd[0];
									$mint=(strlen($tmppd[1]) == 1 ? $tmppd[1]."0" : $tmppd[1]) ;//$tmppd[1];
									
									$tmppd=explode(".",$total_ot_hr_nrd);
										$hor1=$tmppd[0];
										$mint1=(strlen($tmppd[1]) == 1 ? $tmppd[1]."0" : $tmppd[1]) ;//$tmppd[1];
										//$mint1=$tmppd[1];
										
									$total_ot_hr_nrd_amount = ($hor1*$hourly_ot_rate*$ot_multiplier)+($mint1*$minutly_ot_rate*$ot_multiplier);
									$emp_salary_sheet[$row_emp[emp_code]][5]= round($adjust[1]+$adjust[2]+$emp_salary_sheet[$row_emp[emp_code]][5]+((($hor*$hourly_ot_rate*$ot_multiplier)+($mint*$minutly_ot_rate*$ot_multiplier))-$total_ot_hr_nrd_amount));  
									$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+($total_ot_hr-$total_ot_hr_nrd);
									$emp_extra_details[$row_emp[emp_code]][5]['rate']=$hourly_ot_rate;
								}
								else
								{
									if ($designation_allowance[$row_emp[designation_id]]['allowance_treatment']==1)
									{
										$tmppd=explode(".",$total_ot_hr_nrd);
										$hor=$tmppd[0];
										$mint=$tmppd[1];
										
										$tmppd=explode(".",$total_ot_hr);
										$hor1=$tmppd[0];
										$mint1=$tmppd[1];
										
										$total_ot_hr_nrd_amount =($hor*$hourly_ot_rate*$ot_multiplier)+($mint*$minutly_ot_rate*$ot_multiplier);
										// $total_ot_hr_nrd*$hourly_ot_rate*$ot_multiplier;
										$emp_salary_sheet[$row_emp[emp_code]][5]= round($emp_salary_sheet[$row_emp[emp_code]][5]+((($hor1*$hourly_ot_rate*$ot_multiplier)+($mint1*$minutly_ot_rate*$ot_multiplier))-$total_ot_hr_nrd_amount));  
										$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+($total_ot_hr-$total_ot_hr_nrd);
										$emp_extra_details[$row_emp[emp_code]][5]['rate']=$hourly_ot_rate;
									}
									else if ($designation_allowance[$row_emp[designation_id]]['allowance_treatment']==2)
									{
										//$total_ot_hr_nrd_amount = $total_ot_hr_nrd*$hourly_ot_rate*$ot_multiplier;
										$tmppd=explode(".",$total_ot_hr_nrd);
										$hor=$tmppd[0];
										$mint=$tmppd[1];
										
										$tmppd=explode(".",$total_ot_hr);
										$hor1=$tmppd[0];
										$mint1=$tmppd[1];
										
										$emp_salary_sheet[$row_emp[emp_code]][5]= round($emp_salary_sheet[$row_emp[emp_code]][5]+((($hor1*$hourly_ot_rate*$ot_multiplier)+($mint1*$minutly_ot_rate*$ot_multiplier))));  
										$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+($total_ot_hr);
										$emp_extra_details[$row_emp[emp_code]][5]['rate']=$hourly_ot_rate;
									}
									else
									{
										$emp_salary_sheet[$row_emp[emp_code]][5]=0;// $emp_salary_sheet[$row_emp[emp_code]][5]+(($hourly_ot_rate*$ot_multiplier*$total_ot_hr));  
										$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']=0;// $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+($total_ot_hr);
										$emp_extra_details[$row_emp[emp_code]][5]['rate']=$hourly_ot_rate;
									}
								}
								
							}
							
							if($key['overtime_head']==6)
							{
								
								
								$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 6, $emp_salary_sheet, $tot_days ));
								if ($designation_allowance[$row_emp[designation_id]]['allowance_rate']<1)
								{
										$tmppd=explode(".",$total_ot_hr);
										$hor1=$tmppd[0];
										$mint1=$tmppd[1];
										
									$emp_salary_sheet[$row_emp[emp_code]][6]=round(($adjust[1]+$adjust[2])+($adjust[0]*$hourly_ot_rate*$ot_multiplier)+ $emp_salary_sheet[$row_emp[emp_code]][6]+(($hor1*$hourly_ot_rate*$ot_multiplier)+($mint1*$minutly_ot_rate*$ot_multiplier))+$total_ot_hr_nrd_amount);  
									$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$total_ot_hr+$nrd_old_hr+($adjust[0]); 
									$emp_extra_details[$row_emp[emp_code]][6]['rate']=$hourly_ot_rate;
									$total_ot_hr_nrd_amount=0;
									$total_ot_hr_nrd=0;
								}
								else
								{
									if ($designation_allowance[$row_emp[designation_id]]['allowance_treatment']==1)
									{
										$tmppd=explode(".",$total_ot_hr);
										$hor1=$tmppd[0];
										$mint1=$tmppd[1];
										
										$total_ot_hr_nrd_amount = $total_ot_hr_nrd*$hourly_ot_rate*$ot_multiplier;
										$emp_salary_sheet[$row_emp[emp_code]][6]= round($emp_salary_sheet[$row_emp[emp_code]][6]+(($hor1*$hourly_ot_rate*$ot_multiplier)+($mint1*$minutly_ot_rate*$ot_multiplier))-$total_ot_hr_nrd_amount);  
										$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$total_ot_hr-$total_ot_hr_nrd; 
										$emp_extra_details[$row_emp[emp_code]][6]['rate']=$hourly_ot_rate;
										$total_ot_hr_nrd_amount=0;
										$total_ot_hr_nrd=0;
									}
									else if ($designation_allowance[$row_emp[designation_id]]['allowance_treatment']==2)
									{
										$tmppd=explode(".",$total_ot_hr);
										$hor1=$tmppd[0];
										$mint1=$tmppd[1];
										
										$tmppd=explode(".",$total_ot_hr_nrd);
										$hor=$tmppd[0];
										$mint=$tmppd[1];
										
										$total_ot_hr_nrd_amount = ($hor*$hourly_ot_rate*$ot_multiplier)+($mint*$minutly_ot_rate*$ot_multiplier);
										//$total_ot_hr_nrd*$hourly_ot_rate*$ot_multiplier;
										$emp_salary_sheet[$row_emp[emp_code]][6]= round($emp_salary_sheet[$row_emp[emp_code]][6]+(($hor1*$hourly_ot_rate*$ot_multiplier)+($mint1*$minutly_ot_rate*$ot_multiplier)));//-$total_ot_hr_nrd_amount;  
										$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$total_ot_hr;//-$total_ot_hr_nrd; 
										$emp_extra_details[$row_emp[emp_code]][6]['rate']=$hourly_ot_rate;
										$total_ot_hr_nrd_amount=0;
										$total_ot_hr_nrd=0;
									}
									else
									{
										$emp_salary_sheet[$row_emp[emp_code]][6]=0;// $emp_salary_sheet[$row_emp[emp_code]][5]+(($hourly_ot_rate*$ot_multiplier*$total_ot_hr));  
										$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']=0;// $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+($total_ot_hr);
										$emp_extra_details[$row_emp[emp_code]][6]['rate']=$hourly_ot_rate;
									}
								}
							}
							if($key['overtime_head']==7)
							{
								$tmppd=explode(".",$total_ot_hr);
								$hor1=$tmppd[0];
								$mint1=$tmppd[1];
										
								$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 7, $emp_salary_sheet, $tot_days ));
								$emp_salary_sheet[$row_emp[emp_code]][7]= round(($adjust[1]+$adjust[2])+($adjust[0]*$hourly_ot_rate*$ot_multiplier)+$emp_salary_sheet[$row_emp[emp_code]][7]+(($hor1*$hourly_ot_rate*$ot_multiplier)+($mint1*$minutly_ot_rate*$ot_multiplier)));  
								$emp_extra_details[$row_emp[emp_code]][7]['ot_hr']= $adjust[0]+$emp_extra_details[$row_emp[emp_code]][7]['ot_hr']+$total_ot_hr; 
								$emp_extra_details[$row_emp[emp_code]][7]['rate']=$hourly_ot_rate;
								$total_ot_hr_nrd_amount=0;
								$total_ot_hr_nrd=0;
							}
						}
						//print_r($emp_extra_details);die;
					 //echo  $adjust."=".$emp_salary_sheet[$row_emp[emp_code]][41]."=".$emp_salary_sheet[$row_emp[emp_code]][5]; die;
						//echo  $adjust."=".$emp_salary_sheet[$row_emp[emp_code]][5]."=".$emp_salary_sheet[$row_emp[emp_code]][5]; die;
						
						break;
					case 8:  //Holiday Allowance
							$amount=0;
							
							$total_holidays=$attendance_array[$row_emp['emp_code']]['off_day_present'];
							 
							$i=0;
							$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 8, $emp_salary_sheet, $tot_days )); 
							 
							if ($var_holiday_mst[$row_emp[holiday_incentive_policy]]['pay_for']==1) /// Pay for Day
							{
								foreach($var_holiday_dtls[$row_emp[holiday_incentive_policy]] as $key)
								{
								
							
									if ($key['parameter_type']==0) // Percentage
									{
										$amount += (($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$total_holidays;  
										
										//((($emp_salary_sheet[$row_emp['emp_code']][$key[$j]['base_head']]*$key[$j]['amount'])/100)*$total_holidays);
									}
									else if ($key['parameter_type']==1)  // Formula Amount
									{ 
										//$amount +=$emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['amount']]*$total_holidays;
									}
									else if ($key['parameter_type']==2)  // Fixed Amount
									{ 
										$amount +=$emp_salary_sheet[$row_emp['emp_code']][$key['amount']]*$total_holidays;
									}
									$i++; 
								}
							}
							$emp_salary_sheet[$row_emp[emp_code]][8]= round($amount+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
							$emp_extra_details[$row_emp[emp_code]][8]['nof_holi']= $total_holidays;
								
						break;
					case 9:  //Tiffin Allowance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 9, $emp_salary_sheet, $tot_days )); 
						$emp_salary_sheet[$row_emp[emp_code]][9]= round($salary_breakdown[$row_emp[emp_code]][9]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
						break;
					case 10:  //Driver Allowance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 10, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][10]= round($salary_breakdown[$row_emp[emp_code]][10]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 11:  //Vehicle Allowance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 11, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][11]= round($salary_breakdown[$row_emp[emp_code]][11]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 12:   //Utility Allowance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 12, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][12]= round($salary_breakdown[$row_emp[emp_code]][12]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 13:  //Entertainment Allowance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 13, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][13]= round($salary_breakdown[$row_emp[emp_code]][13]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 14:   //Incentives
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 14, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][14]= round($salary_breakdown[$row_emp[emp_code]][14]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 15:  //Company PF Contribution
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 15, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][15]= round($salary_breakdown[$row_emp[emp_code]][15]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 16:  //Festival Bonus
						//$emp_salary_sheet[$row_emp[emp_code]][9]= $salary_breakdown[$row_emp[emp_code]][9];
						break;
					case 17:  //Arrear Earnings
						
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 17, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][17]=round($increment_arrear[$row_emp[emp_code]][$txt_from_date]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 18:  //Leave Fair Assistance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 18, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][18]= round($salary_breakdown[$row_emp[emp_code]][18]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 19:  //Other Earnings
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 19, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][19]= round($salary_breakdown[$row_emp[emp_code]][19]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 20:  // Gross Head
						//$emp_salary_sheet[$row_emp[emp_code]][17]=$increment_arrear[$row_emp[emp_code]][$txt_from_date];
						break;
					case 35:   //Attendance Bonus
							$att_bonus_amount=0;
							$buyer_att_bonus_amount=0; 
						if( $emp_condition==0)// Regular Worker
						{
							$i=0;
							if ($var_hrm_chart[$row_emp['company_id']]['att_bonus_leave_type']==1)  // Breakdown wise Leave
							{ 
								foreach($attendance_bonus_pol[$row_emp[attendance_bonus_policy]] as $key)
								{
									if($key['cl_leave_criteria']>= $c_leave_days && $key['sl_leave_criteria']>= $s_leave_days && $key['lwp_leave_criteria']>= $lwp_leave_days && $key['spl_leave_criteria']>= $spl_leave_days && $key['el_leave_criteria']>= $e_leave_days && $key['ml_leave_criteria']>= $m_leave_days && $key['late_criteria']>=$late_days  &&   $key['absent_criteria']>=$absent_days )
									{
										$att_bonus_amount=$key['amount']; break 1; 
									}
									$i++;
								}
								$i=0;
								//echo "ss".$att_bonus_amount; 
								$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 35, $emp_salary_sheet, $tot_days ));
								$emp_salary_sheet[$row_emp[emp_code]][35]=round($att_bonus_amount+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
								$att_bonus_amount=0;
								foreach($attendance_bonus_pol[$row_emp[attendance_bonus_policy]] as $key)
								{
									if( $key['cl_leave_criteria']>= $c_leave_days && $key['sl_leave_criteria']>= $s_leave_days && $key['lwp_leave_criteria']>= $lwp_leave_days && $key['spl_leave_criteria']>= $spl_leave_days && $key['el_leave_criteria']>= $e_leave_days && $key['ml_leave_criteria']>= $m_leave_days && $key['late_criteria']>=$gn_late_days  &&   $key['absent_criteria']>=$gn_absent_days )
									//if($key['leave_criteria']>= $total_leave_days && $key['late_criteria']>=$gn_late_days  && $key['absent_criteria']>=$gn_absent_days )
									{
										$buyer_att_bonus_amount=$key['amount']; break 1; 
									}
									$i++;
								}
							}
							else  // Calculate on Total Leave
							{
								foreach($attendance_bonus_pol[$row_emp[attendance_bonus_policy]] as $key)
								{
									if( $key['total_leave_criteria']>= $total_leave_days && $key['late_criteria']>=$late_days  &&   $key['absent_criteria']>=$absent_days )
									{
										$att_bonus_amount=$key['amount']; break 1; 
									}
									$i++;
								}
								$i=0;
								$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 35, $emp_salary_sheet, $tot_days ));
								$emp_salary_sheet[$row_emp[emp_code]][35]=round($att_bonus_amount+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
								$att_bonus_amount=0;
								foreach($attendance_bonus_pol[$row_emp[attendance_bonus_policy]] as $key)
								{
									if( $key['total_leave_criteria']>= $total_leave_days && $key['late_criteria']>=$gn_late_days  &&   $key['absent_criteria']>=$gn_absent_days )
									//if($key['leave_criteria']>= $total_leave_days && $key['late_criteria']>=$gn_late_days  && $key['absent_criteria']>=$gn_absent_days )
									{
										$buyer_att_bonus_amount=$key['amount']; break 1; 
									}
									$i++;
								}
							}
						}
						else
						{
							$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 35, $emp_salary_sheet, $tot_days ));
							$emp_salary_sheet[$row_emp[emp_code]][35]=round(($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
							//$emp_salary_sheet[$row_emp[emp_code]][35]=0; // $salary_breakdown[$row_emp[emp_code]][35];  
						 	$att_bonus_amount=0;
						}
					//	die;
						break;
					case 38:  //DA 	Dearness Allowance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 38, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][38]= round($salary_breakdown[$row_emp[emp_code]][38]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						 break;
					case 39:  //IR 	Interim Relief
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 39, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][39]= round($salary_breakdown[$row_emp[emp_code]][39]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						 break;
					case 40:  //Night Allowance
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 40, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][40]= round($salary_breakdown[$row_emp[emp_code]][40]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						 break;
					case 41:   // Extra OT as Allowance
						//echo  $adjust."=".$emp_salary_sheet[$row_emp[emp_code]][41]."=".$emp_salary_sheet[$row_emp[emp_code]][5]; die;
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 41, $emp_salary_sheet, $tot_days ));
					$emp_salary_sheet[$row_emp[emp_code]][41] = round($emp_salary_sheet[$row_emp[emp_code]][41]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						 break;
//========================================================// All Deduction Head Goes From Here
					case 21:   //Advance  
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 21, $emp_salary_sheet, $tot_days ));
						
						$loan_amount=$loan_amount_array[$row_emp['emp_code']][21]+$salary_breakdown[$row_emp[emp_code]][21];
						
						$emp_salary_sheet[$row_emp[emp_code]][21]= round($loan_amount+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
						
						//print_r( $emp_salary_sheet[$row_emp[emp_code]][21]); die;
					 	$loan_amount=0;
						break;
					case 22:  //Advance Income Tax
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 22, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][22]= round($salary_breakdown[$row_emp[emp_code]][22]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						//print_r( $emp_salary_sheet[$row_emp[emp_code]][22]); die;
						break;
					case 23:  // Late Present  Deduction
						$i=0;
						$late_deduct_amnt=0;
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 23, $emp_salary_sheet, $tot_days ));
						foreach( $late_deduction_pol[$row_emp[late_deduction_policy]] as $key )
						{
							if ($late_days>= $sql_data_rslt[late_slot])
							{
								$days_to_deduct= floor($late_days/$key['late_slot']); 
								if ( $key['parameter_type']==0) // Percentage
								{
									$late_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$days_to_deduct;  
									break 1;
								}
								else if ( $key['parameter_type']==1) // Fixed
								{
									//$late_deduct_amnt=$key['amount']*$days_to_deduct;
									//break;
								} 
								else if ( $key['parameter_type']==2) // Fixed
								{
									$late_deduct_amnt=$key['amount']*$days_to_deduct;
									break 1;
								} 
							}
							$i++;
						}
						$emp_salary_sheet[$row_emp[emp_code]][23]= round($late_deduct_amnt+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
						$i=0;
						$gn_late_deduct_amnt=0;
						foreach( $late_deduction_pol[$row_emp[late_deduction_policy]] as $key )
						{
							if ($gn_late_days>= $sql_data_rslt[late_slot])
							{
								$days_to_deduct= floor($gn_late_days/$key['late_slot']); 
								if ( $key['parameter_type']==0) // Percentage
								{
									$gn_late_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$days_to_deduct;  
									break 1;
								}
								else if ( $key['parameter_type']==1) // Fixed
								{
									//$gn_late_deduct_amnt=$key['amount']*$days_to_deduct;
									//break;
								} 
								else if ( $key['parameter_type']==2) // Fixed
								{
									$gn_late_deduct_amnt=$key['amount']*$days_to_deduct;
									break 1;
								} 
							}
							$i++;
						}
						break;
					case 24:  //Absent Deduction
						$i=0;
						// print_r(get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 24, $emp_salary_sheet, $tot_days )); die;
						$abs_deduct_amnt=0;
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 24, $emp_salary_sheet, $tot_days ));
						foreach( $absent_deduction_pol[$row_emp[absent_deduction_policy]] as $key )
						{
							//echo $key['parameter_type']."=".$absent_days."sss".$key[$i]['parameter_type']; print_r($absent_deduction_pol[$row_emp[absent_deduction_policy]]); die;
							$abs_deduct_amnt=0;
							if ($absent_days > 0)
							{ 
								if ( $key['parameter_type']==0 )
								{
									if( $new_emp_abs_deduct==1 && $emp_condition==2)
										$abs_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][20]/$tot_days))/100)*$absent_days;
									else
										$abs_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$absent_days;
									break 1;
								}
								else if ( $key['parameter_type']==1)
								{
									//$abs_deduct_amnt=$key['amount']*$absent_days;
									break 1;
								}
								else if ( $key['parameter_type']==2)
								{
									$abs_deduct_amnt=$key['amount']*$absent_days;
									break 1;
								}
							}
							$i++;
						}
						$emp_salary_sheet[$row_emp[emp_code]][24]= round($abs_deduct_amnt+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
						$i=0;
						//echo "gnabs".$gn_absent_days."="; 
						$gn_abs_deduct_amnt=0;
						foreach( $absent_deduction_pol[$row_emp[absent_deduction_policy]] as $key )
						{
							if ($gn_absent_days > 0)
							{ 
								if ( $key['parameter_type']==0)
								{
									if($new_emp_abs_deduct==1 && $emp_condition==2)
										$gn_abs_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][20]/$tot_days))/100)*$gn_absent_days;
									else
										$gn_abs_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$gn_absent_days;
										
									//$gn_abs_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$gn_absent_days;
									break 1;
								}
								else if ( $key['parameter_type']==1)
								{
									//$abs_deduct_amnt=$key['amount']*$absent_days;
									//break;
								}
								else if ( $key['parameter_type']==2)
								{
									$gn_abs_deduct_amnt=$key['amount']*$gn_absent_days;
									break 1;
								}
							}
							$i++;
						}
						break;
						
					case 25:  //Provident Fund Contribution
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 25, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][25]= round($salary_breakdown[$row_emp[emp_code]][25]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);  
						break;
					case 26:  //Transport Contribution
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 26, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][26]= round($salary_breakdown[$row_emp[emp_code]][26]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);  
						break;
					case 27: //Lunch Contribution
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 27, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][27]= round($salary_breakdown[$row_emp[emp_code]][27]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);  
						break;
					case 28: // Revenue Stamp
						if ($salary_breakdown[$row_emp[emp_code]][28]>0) $emp_salary_sheet[$row_emp[emp_code]][28]= $salary_breakdown[$row_emp[emp_code]][28]; 
						else $emp_salary_sheet[$row_emp[emp_code]][28]= round($var_hrm_chart[$row_emp['company_id']]['stamp_charge_deduction']);  
						break;
					case 29:  //Leave Without Pay LWP
						$i=0;
						$lwp_deduct_amnt=0;
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 29, $emp_salary_sheet, $tot_days ));
						foreach($lwp_deduction_pol[$row_emp[leave_policy]] as $key)
						{
							if ($lwp_leave_days > 0)
							{
								if($key['formula']==0) 
									$lwp_deduct_amnt +=($salary_breakdown[$row_emp[emp_code]][$key['amount']]/$tot_days)*$lwp_leave_days;
								else if ( $key['formula']==1) //50%
									$lwp_deduct_amnt +=(($policy_def[0]*($salary_breakdown[$row_emp[emp_code]][$key['amount']]/$tot_days))/100)*$lwp_leave_days;
								else if ( $key['formula']==2) // Fixed
									$lwp_deduct_amnt +=$key['amount']*$lwp_leave_days;
							}
							$i++;
						}
						$emp_salary_sheet[$row_emp[emp_code]][29]= round($lwp_deduct_amnt+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
					    $lwp_deduct_amnt=0;
						break;
					case 30: //Arrear Deduction
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 30, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][30]= round($salary_breakdown[$row_emp[emp_code]][30]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						break;
					case 31: //Other Deduction  
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 31, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][31]= round($salary_breakdown[$row_emp[emp_code]][31]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;
					case 32:	//Investment Allowance  
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 32, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][32]= round($salary_breakdown[$row_emp[emp_code]][32]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;
					case 33: 	//Taxable Income
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 33, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][33]= round($salary_breakdown[$row_emp[emp_code]][33]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;
					case 34: //Tax Rebate 
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 34, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][34]= round($salary_breakdown[$row_emp[emp_code]][34]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;
					case 36:  // Id Card Charge
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 36, $emp_salary_sheet, $tot_days ));
						if ($var_hrm_chart[$row_emp['company_id']]['new_emp_card_charge']>0)
						{
							if ($emp_condition==2)
								$emp_salary_sheet[$row_emp[emp_code]][36]= round($var_hrm_chart[$row_emp['company_id']]['new_emp_card_charge']+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
							else
								$emp_salary_sheet[$row_emp[emp_code]][36]= round($salary_breakdown[$row_emp[emp_code]][36]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);
						}
						else
							$emp_salary_sheet[$row_emp[emp_code]][36]= round($salary_breakdown[$row_emp[emp_code]][36]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]); 
							
						break;
					case 37:  //Punishment Amount
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 37, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][37]= round($salary_breakdown[$row_emp[emp_code]][37]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;	
					case 42:  //Punishment Amount
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 42, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][42]= round($salary_breakdown[$row_emp[emp_code]][42]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;
					case 43:  //Early Out Amount
						//print_r( $row_emp[early_deduction_policy] ); die;
						 
						/*$early_out_deduction_pol[$data[policy_id]][$i]['early_slot']=$data[early_slot];
		$early_out_deduction_pol[$data[policy_id]][$i]['parameter_type']=$data[parameter_type];
		$early_out_deduction_pol[$data[policy_id]][$i]['percentage_formula']=$data[percentage_formula];
		$early_out_deduction_pol[$data[policy_id]][$i]['base_head']=$data[base_head];
		$early_out_deduction_pol[$data[policy_id]][$i]['amount']=$data[amount];
		*/
						$i=0;
						$ead_deduct_amnt=0;
						$early_out_days=$attendance_array[$row_emp['emp_code']]['early_out_count'];
						
						foreach( $early_out_deduction_pol[$row_emp[early_deduction_policy]] as $key )
						{
							if ( $early_out_days>= $key['early_slot'])
							{
								$days_to_deduct= floor($early_out_days/$key['early_slot']); 
								if ( $key['parameter_type']==0) // Percentage
								{
									$ead_deduct_amnt=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$days_to_deduct;  
									break 1;
								}
								else if ( $key['parameter_type']==1) // Formula
								{
									//$ead_deduct_amnt=$key['amount']*$days_to_deduct;
									//break;
								} 
								else if ( $key['parameter_type']==2) // Fixed
								{
									$ead_deduct_amnt=$key['amount']*$days_to_deduct;
									break 1;
								} 
							}
							$i++;
						}
						
						$emp_salary_sheet[$row_emp[emp_code]][43]= round($ead_deduct_amnt); 
						// Buyer Early Out Amount
						$gn_early_out_amount=0;
						$bearly_out_days= $attendance_array[$row_emp['emp_code']]['b_early_out_count'];
						foreach( $early_out_deduction_pol[$row_emp[early_deduction_policy]] as $key )
						{
							if ($bearly_out_days>= $key['early_slot'])
							{
								$days_to_deduct= floor($bearly_out_days/$key['early_slot']); 
								if ( $key['parameter_type']==0) // Percentage
								{
									$gn_early_out_amount=(($key['percentage_formula']*($salary_breakdown[$row_emp[emp_code]][$key['base_head']]/$tot_days))/100)*$days_to_deduct;  
									break 1;
								}
								else if ( $key['parameter_type']==1) // Formula
								{
									//$gn_early_out_amount=$key['amount']*$days_to_deduct;
									//break;
								} 
								else if ( $key['parameter_type']==2) // Fixed
								{
									$gn_early_out_amount=$key['amount']*$days_to_deduct;
									break 1;
								} 
							}
							$i++;
						}
						
					    $gn_early_out_amount=0;
						break;
					case 44:  //PF LOAN Amount
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 44, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][44]= round($salary_breakdown[$row_emp[emp_code]][44]+$loan_amount_array[$row_emp['emp_code']][44]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;
					case 45:  //Dormitory Deduction Amount
						$adjust=explode("_",get_adjustment_amount( $salary_adjustments, $row_emp[emp_code], 45, $emp_salary_sheet, $tot_days ));
						$emp_salary_sheet[$row_emp[emp_code]][45]= round($salary_breakdown[$row_emp[emp_code]][45]+($adjust[0]*$hourly_ot_rate)+$adjust[1]+$adjust[2]);     
						break;
				} 
			} // End of For Loop
			// Calculate total 
			 
			$total_earning = $emp_salary_sheet[$row_emp[emp_code]][1]+$emp_salary_sheet[$row_emp[emp_code]][2]+$emp_salary_sheet[$row_emp[emp_code]][3]+$emp_salary_sheet[$row_emp[emp_code]][4]+$emp_salary_sheet[$row_emp[emp_code]][5]+$emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]+$emp_salary_sheet[$row_emp[emp_code]][8]+$emp_salary_sheet[$row_emp[emp_code]][9]+$emp_salary_sheet[$row_emp[emp_code]][10]+$emp_salary_sheet[$row_emp[emp_code]][11]+$emp_salary_sheet[$row_emp[emp_code]][12]+$emp_salary_sheet[$row_emp[emp_code]][13]+$emp_salary_sheet[$row_emp[emp_code]][14]+$emp_salary_sheet[$row_emp[emp_code]][15]+$emp_salary_sheet[$row_emp[emp_code]][16]+$emp_salary_sheet[$row_emp[emp_code]][17]+$emp_salary_sheet[$row_emp[emp_code]][18]+$emp_salary_sheet[$row_emp[emp_code]][19]+$emp_salary_sheet[$row_emp[emp_code]][35]+$emp_salary_sheet[$row_emp[emp_code]][38]+$emp_salary_sheet[$row_emp[emp_code]][39]+$emp_salary_sheet[$row_emp[emp_code]][40]+$emp_salary_sheet[$row_emp[emp_code]][41];
			
			$b_total_earning_amount=$total_earning+$buyer_att_bonus_amount - $emp_salary_sheet[$row_emp[emp_code]][35] - ($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
			 //echo $total_earning."--".$b_total_earning_amount."<br>";// die;
			$total_deduction =$emp_salary_sheet[$row_emp[emp_code]][21]+$emp_salary_sheet[$row_emp[emp_code]][22]+$emp_salary_sheet[$row_emp[emp_code]][23]+$emp_salary_sheet[$row_emp[emp_code]][24]+$emp_salary_sheet[$row_emp[emp_code]][25]+$emp_salary_sheet[$row_emp[emp_code]][26]+$emp_salary_sheet[$row_emp[emp_code]][27]+$emp_salary_sheet[$row_emp[emp_code]][28]+$emp_salary_sheet[$row_emp[emp_code]][29]+$emp_salary_sheet[$row_emp[emp_code]][30]+$emp_salary_sheet[$row_emp[emp_code]][31]+$emp_salary_sheet[$row_emp[emp_code]][32]+$emp_salary_sheet[$row_emp[emp_code]][33]+$emp_salary_sheet[$row_emp[emp_code]][34]+$emp_salary_sheet[$row_emp[emp_code]][42]+$emp_salary_sheet[$row_emp[emp_code]][37]+$emp_salary_sheet[$row_emp[emp_code]][36]+$emp_salary_sheet[$row_emp[emp_code]][43]+$emp_salary_sheet[$row_emp[emp_code]][44]+$emp_salary_sheet[$row_emp[emp_code]][45];
			
			$b_total_deduction_amount = $emp_salary_sheet[$row_emp[emp_code]][21]+$emp_salary_sheet[$row_emp[emp_code]][22]+$gn_late_deduct_amnt+$gn_abs_deduct_amnt+$emp_salary_sheet[$row_emp[emp_code]][25]+$emp_salary_sheet[$row_emp[emp_code]][26]+$emp_salary_sheet[$row_emp[emp_code]][27]+$emp_salary_sheet[$row_emp[emp_code]][28]+$emp_salary_sheet[$row_emp[emp_code]][29]+$emp_salary_sheet[$row_emp[emp_code]][30]+$emp_salary_sheet[$row_emp[emp_code]][31]+$emp_salary_sheet[$row_emp[emp_code]][32]+$emp_salary_sheet[$row_emp[emp_code]][33]+$emp_salary_sheet[$row_emp[emp_code]][34]+$gn_early_out_amount;
			
		 
			// Save Data to Table From Here
			 
			if( $row_emp[ot_entitled]==1 || $row_emp[staff_ot_entitled]==1) // Ot Entitled
			{
				$is_with_over_time=1; 
				
				$tmppd_nrd=explode(".",$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']);
				$hrs1=($tmppd_nrd[0]*60)+(strlen($tmppd_nrd[1]) == 1 ? $tmppd_nrd[1]."0" : $tmppd_nrd[1]) ;//$tmppd_nrd[1];
				//$mins1=$tmppd_nrd[1];
				
				$tmppd_nrd=explode(".",$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']);
				$hrs2=($tmppd_nrd[0]*60)+(strlen($tmppd_nrd[1]) == 1 ? $tmppd_nrd[1]."0" : $tmppd_nrd[1]);
				//$mins2=$tmppd_nrd[1];
				
				$tmppd_nrd=explode(".",$emp_extra_details[$row_emp[emp_code]][7]['ot_hr']);
				$hrs3=($tmppd_nrd[0]*60)+(strlen($tmppd_nrd[1]) == 1 ? $tmppd_nrd[1]."0" : $tmppd_nrd[1]);
				//$mins3=$tmppd_nrd[1];
				$tmin=($hrs1+$hrs2+$hrs3);
				 
				//$tmin=(strlen($tmin) == 1 ? $tmin."0" : $tmin) ;//$tmppd[1];
				 
				$total_ot_hr =($hr+ floor($min/60)).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
				
				$total_over_time_min= (floor($tmin/60)).".".str_pad(($tmin%60),2,"0",STR_PAD_LEFT);
				//echo $tmin."=".$total_over_time_min; die;
				  //$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][7]['ot_hr'];
				$extra_ot= $emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][7]['ot_hr'];
			}
			else
			{
				$total_earning=$total_earning-($emp_salary_sheet[$row_emp[emp_code]][5]+$emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
				$b_total_earning_amount=$total_earning; // - ($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
				$is_with_over_time=0;
				$total_over_time_min=0;
				$emp_salary_sheet[$row_emp[emp_code]][5]=0;
				$emp_salary_sheet[$row_emp[emp_code]][6]=0;
				$emp_salary_sheet[$row_emp[emp_code]][7]=0;
				$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']=0;
				$extra_ot=0;
			}
			//echo $total_ot_hr; die;
			// Check Disciplinary Actions for Deduction
			$suspension_deduct_amnt=0;
			if($suspended_days>0)
			{ 
				$i=0;
				$emp_condition=3;
				foreach($suspension_deduction_pol[$row_emp[emp_code]] as $key)
				{
					$suspension_deduct_amnt +=round((($key['impact_salary_head_perc']*($emp_salary_sheet[$row_emp[emp_code]][$key['impact_salary_head']]/$tot_days))/100)*$suspended_days);
					$i++;
				}
			}
			// Check Disciplinary Actions for Deduction
			$late_amt = $emp_salary_sheet[$row_emp[emp_code]][23];
			$absent_amt = $emp_salary_sheet[$row_emp[emp_code]][24];
			$lwp_amt = $emp_salary_sheet[$row_emp[emp_code]][29];
			$not_payable_deduction_amount=0;
			if ($var_hrm_chart[$row_emp['company_id']]['not_pay_deduction'] ==1)   // Deduct NP Days from Basic
			{ 
				$net_payable_amount=round($total_earning-($total_deduction+($not_payable_days*$daily_basic_salary))); 
				$b_net_payable_amount=round($b_total_earning_amount-($b_total_deduction_amount+($not_payable_days*$daily_basic_salary)));
				$not_payable_deduction_amount=round($not_payable_days*$daily_basic_salary);
			}
			else
			{
				$net_payable_amount=round($total_earning-($total_deduction+($not_payable_days*$daily_gross_salary))); 
				$b_net_payable_amount=round($b_total_earning_amount-($b_total_deduction_amount+($not_payable_days*$daily_gross_salary))); 
				$not_payable_deduction_amount=round($not_payable_days*$daily_gross_salary);
			}
			//$net_payable_amount-($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
			
			if($row_id=="") $row_id = return_next_id( "id", "hrm_reconcile_salary_mst" ); else $row_id+=1;
			
			if ($sql_mst!="") $sql_mst.=",";
			$sql_mst.="('$row_id','". $txt_from_date_exp ."', '$row_emp[emp_code]','$row_emp[category]','$row_emp[bank_gross]','".$emp_salary_sheet[$row_emp[emp_code]][0]."','".$emp_salary_sheet[$row_emp[emp_code]][1]."','".$emp_salary_sheet[$row_emp[emp_code]][3]."','".$emp_salary_sheet[$row_emp[emp_code]][2]."','$total_earning','$b_total_earning_amount','$total_deduction','$is_with_over_time','".$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']."','".$extra_ot."','$total_over_time_min','$ot_rate','$net_payable_amount','$b_net_payable_amount','$net_payable_days','$absent_days','$late_days','$lwp_leave_days','$spl_leave_days','$e_leave_days','$f_leave_days','$m_leave_days','$s_leave_days','$c_leave_days','$g_holiday_days','$f_holiday_days','$c_holiday_days','$weekday_days','$not_payable_days','$suspended_days','$suspension_deduct_amnt','$daily_basic_salary','$daily_gross_salary','$total_working_days','$calendar_days','$total_leave_days','$late_amt','$absent_amt','$lwp_amt','$b_total_deduction_amount','$gn_net_payable_days','$gn_absent_days','$gn_late_days','$gn_g_holiday_days','$gn_f_holiday_days','$gn_weekday_days','$gn_total_working_days','$gn_present_days','$gn_late_deduct_amnt','$gn_abs_deduct_amnt','$buyer_att_bonus_amount','$row_emp[company_id]','$row_emp[location_id]','$row_emp[division_id]','$row_emp[department_id]','$row_emp[section_id]','$row_emp[subsection_id]','$emp_condition','$present_days','".$emp_salary_sheet[$row_emp[emp_code]][41]."','".$emp_extra_details[$row_emp[emp_code]][41]['ot_hr']."','".$not_payable_deduction_amount."', '".$early_out_days."')";
			
				//print_r( $emp_salary_sheet[$row_emp[emp_code]][22]); die;
				
				for($k=0; $k<count($salary_head_list_i);$k++)
				{
					if($salary_head_type[$j]==0 || $salary_head_type[$j]==3 || $salary_head_type[$j]==4 ) $is_deductive=0; // Earning
					else  $is_deductive=1;
						
					if($id=="") $id = return_next_id( "id", "hrm_reconcile_salary_dtls" ); else $id+=1; //$id = return_next_id( "id", "hrm_salary_dtls" );
					if ($sql_dtls!="") $sql_dtls.=",";
					$sql_dtls.="('$id','$row_id','$row_emp[emp_code]','".$salary_head_list_i[$k]."','".$emp_salary_sheet[$row_emp[emp_code]][$salary_head_list_i[$k]]."','$is_deductive','$salary_head_type[$k]','$id')";
				} 
					//print_r($emp_salary_sheet[$row_emp[emp_code]][$salary_head_list_i[$k][22]]);die;
					
					
					$total_earning=0;$b_total_earning_amount=0;					
					$total_deduction=0;$is_with_over_time="";					
					$extra_ot=0;$total_over_time_min=0;					
					$ot_rate=0;$net_payable_amount=0;					
					$b_net_payable_amount=0;$suspension_deduct_amnt=0;					
					$late_amt=0;$absent_amt=0;$lwp_amt=0;				
					$b_total_deduction_amount=0;$gn_late_deduct_amnt=0;
					$gn_abs_deduct_amnt=0;$buyer_att_bonus_amount=0;$emp_condition=0;	
					$early_out_days=0;
					//echo $sql_dtls."=".$sql_mst; print_r($emp_salary_sheet); die;
				//echo $total_earning."-".$total_deduction."--".$emp_salary_sheet[$row_emp[emp_code]][0]."\n";die;
			}
			else
			{
				$no_process[]=$row_emp[emp_code]; 
			}
		} // End of Employee Loop
		
		$sql = "INSERT INTO hrm_reconcile_salary_mst (id,salary_periods,emp_code,employee_category,bank_gross,gross_salary,basic_salary,medical_allowance,house_rent,total_earning_amount,b_total_earning_amount,total_deduction_amount,is_with_over_time,b_over_time,e_over_time,total_over_time,over_time_rate,net_payable_amount,b_net_payable_amount,payable_days,total_abs_days,total_late_days,total_lwp_days,special_leave,earn_leave,faternity_leave,maternity_leave,sick_leave,casual_leave,government_holiday,festival_holiday,compensatory_total_holiday,total_weekly_holiday,not_payble_days,suspend_days, suspend_amount,daily_basic_salary,daily_gross_salary,total_working_days,total_calendar_days,total_leave_days,total_late_deduction_amount,total_absent_deduction_amount,total_lwp_deduction_amount,b_total_deduction_amount,b_net_payable_days,b_absent_days,b_late_days,b_government_holiday,b_festival_holiday,b_total_weekly_holiday,b_total_working_days,b_total_present_days,b_total_late_deduction_amount,b_total_absent_deduction_amount,b_attendance_bonus,company_id,location_id,division_id,department_id,section_id,subsection_id,emp_status,total_present_days,ot_allowance_amount,ot_allowance_hr,not_payable_deduction_amount,early_out_days) VALUES ".$sql_mst;
 
		 if ($sql_mst!="") mysql_query( $sql );	
		  
		 $sql_d="INSERT INTO hrm_reconcile_salary_dtls (id,salary_mst_id,emp_code,salary_head_id,pay_amount,is_deducted,head_type,pay_slip_id) values ".$sql_dtls; 					 		// echo $sql_d;die;
		 if ($sql_dtls!="")  mysql_query( $sql_d ); 
		 //echo  $sql."==".$sql_d; 
		 mysql_query("update hrm_loan_payback_scheduling_details set is_paid=1 where id in ( $arr_loan_id )");
		 
		 mysql_query( "update database_status_process set status=0" );
		 $no_process_emp = implode(",",$no_process);  
  		 echo "*1"; //die;
		 die;
		
	/*}
	
	foreach (array_keys($GLOBALS) as $k) unset($$k);
unset($k);
	
	else
	{
		$no_process_emp = implode(",",$no_process);  
  		echo $no_process_emp."*2";
		die;
	}*/
  

}



function get_adjustment_amount( $salary_adjustments, $emp_code, $head_id, $emp_salary_sheet, $days_in_month )
{
	
	//if ( $head_id==20 ) $head_id=0;
	global $adjustment_type;
	global $sal_adjust_cnt;
	//return $sal_adjust_cnt[$emp_code][21]['head_count'];
	//die;
	$adjustment_1=0;
	$adjustment_2=0;
	$adjustment_3=0;
	
	//if (count($salary_adjustments[$emp_code][$head_id])>0)
	if($sal_adjust_cnt[$emp_code][$head_id]['head_count']>0)
	{
		for ($adj=0; $adj<$sal_adjust_cnt[$emp_code][$head_id]['head_count']; $adj++)
		{
			//if ( $adjustment_type[$salary_adjustments[$emp_code][$head_id][$adj]['mst_id']]==1 )  // Earning
			//{
				if( $salary_adjustments[$emp_code][$head_id][$adj]['base_head_id']==20 ) $salary_adjustments[$emp_code][$head_id][$adj]['base_head_id']=0;
				if ( $salary_adjustments[$emp_code][$head_id][$adj]['criteria_id']==1 ) // Ot Hr
					$adjustment_1 +=$salary_adjustments[$emp_code][$head_id][$adj]['adjustment_amount'];
				
				else if ( $salary_adjustments[$emp_code][$head_id][$adj]['criteria_id']==2 ) // Daily cash
					$adjustment_2 += $salary_adjustments[$emp_code][$head_id][$adj]['adjustment_amount']*((($emp_salary_sheet[$emp_code][$salary_adjustments[$emp_code][$head_id][$adj]['base_head_id']]/$days_in_month)*$salary_adjustments[$emp_code][$head_id][$adj]['formula_value'])/100);
				
				else if ( $salary_adjustments[$emp_code][$head_id][$adj]['criteria_id']==3 ) // Cash
					$adjustment_3 +=$salary_adjustments[$emp_code][$head_id][$adj]['adjustment_amount'];
			//}
			/* else  // Deduction
			{
				if ( $salary_adjustments[$emp_code][$head_id][$adj]['criteria_id']==1 ) // Ot Hr
					$adjustment_4 +=$salary_adjustments[$emp_code][$head_id][$adj]['adjustment_amount'];
				
				else if ( $salary_adjustments[$emp_code][$head_id][$adj]['criteria_id']==2 ) // Daily cash
					$adjustment_5 += $salary_adjustments[$emp_code][$head_id][$adj]['adjustment_amount']*((($emp_salary_sheet[$emp_code][$salary_adjustments[$emp_code][$head_id][$adj]['base_head_id']]/$days_in_month)*$salary_adjustments[$emp_code][$head_id][$adj]['formula_value'])/100);
				
				else if ( $salary_adjustments[$emp_code][$head_id][$adj]['criteria_id']==3 ) // Cash
					$adjustment_6 +=$salary_adjustments[$emp_code][$head_id][$adj]['adjustment_amount'];
			}*/
			 
		}
		return $adjustment_1."_".$adjustment_2."_".$adjustment_3;
	}
	else return "0_0_0";
	
}


function get_total_payable_holidays_amount( $emp_code, $txt_from_date, $txt_to_date, $status, $mst_policy, $dtls_policy, $h_policy, $emp_salary_sheet, $tot_days)
{
	$sql_emp22="select * from hrm_attendance where emp_code='$emp_code' and attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' and status in($status) and sign_in_time<>'00:00:00' and sign_out_time<>'00:00:00' "; //and a.emp_code='0000038'  and a.emp_code='0000015' 
	 
	$exe_sql_emp22=mysql_query($sql_emp22);
	$day_no=0;
	while ($row_h22 = mysql_fetch_array($exe_sql_emp22))// EMp Master Qry starts 
	{ 
	 	$tot_min=datediff(n,$row_h22[sign_in_time],$row_h22[sign_out_time]);
		
		if ( $tot_min >=($mst_policy[$h_policy]['min_working_hour']*60))
		{
			$day_no=$day_no+1;
			if ($mst_policy[$h_policy]['pay_for']==0)  // pay for hour
			{
				$tot_hr=(int)$tot_min/60;
				for($j=0; $j<count($dtls_policy[$h_policy]); $j++)
				{
					if ($dtls_policy[$h_policy][$i]['parameter_type']==0) // Percentage
					{
						$amount+= ($emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['base_head']]/($tot_days*8))*$tot_hr ;
					}
					else if ($dtls_policy[$h_policy][$j]['parameter_type']==2)  // Fixed Amount
					{ 
						$amount+=  $emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['amount']]*($tot_hr) ;
					}
					 
				}
			}
			else //if ($mst_policy[$h_policy]['pay_for']==0)  // pay for Days
			{
				$tot_hr=(int)$tot_min/60;
				for($j=0; $j<count($dtls_policy[$h_policy]); $j++)
				{
					
					if ($dtls_policy[$h_policy][$j]['parameter_type']==0) // Percentage
					{
						$amount+= (($emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['base_head']]/($tot_days))*$dtls_policy[$h_policy][$j]['percentage_formula'])/100;
					}
					else if ($dtls_policy[$h_policy][$j]['parameter_type']==2)  // Fixed Amount
					{ 
						$amount+=  $emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['amount']];
					}
					 
				}
			}
		}
	} // while
	return $day_no."_".$amount;
}

function get_total_ot_hr( $ot_policy_slab, $txt_from_date, $txt_to_date,$overtime_policy,$salary_head, $emp_code, $comp_id, $var_hrm_chart )
{
	 for ($i=0; $i<count($ot_policy_slab[$overtime_policy][$salary_head]); $i++)
	 {
		$slot=$slot+$ot_policy_slab[$overtime_policy][$salary_head][$i]['overtime_slot']*60; 
		$rate=$ot_policy_slab[$overtime_policy][$salary_head][$i]['overtime_rate']; 
		
		$str_date=" attnd_date between '$txt_from_date' and '$txt_to_date'";
		$tot_ot=get_buyer_tot_emp_ot_hr($emp_code,$str_date,$var_hrm_chart[$comp_id][one_hour_ot_unit],$var_hrm_chart[$comp_id][allow_ot_fraction],$var_hrm_chart[$comp_id][ot_start_minute],$slot);
		$act_ot=$act_ot+($tot_ot-$act_ot);
	
	 }
	 
	 return $act_ot;
}


 
// Functions required for this processing
function daysdiff($date1,$date2) 
{ 
	$diff = (strtotime($date2) - strtotime($date1));
	
	$days = floor($diff / (1*60*60*24)+1);
	return $days; 		
}

	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

 



function change_date_for_process( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "-", $date_field );
		return $dates[1] . "/" . $dates[2] . "/" . $dates[0];
	}
	else return $date_field;
}
function change_date_for_process_sql( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "/", $date_field );
		return $dates[2] . "-" . $dates[0] . "-" . $dates[1];
	}
	else return $date_field;
}
function change_time_for_process( $time_field ) {
	if( $time_field != '' ) {
		$times = explode( "-", $time_field );
		return $times[0] . ":" . $times[1] . ":" . $times[2];
	}
	else return $time_field;
}
 
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}


function number_formating( $number, $precision=0, $round_type=1 ) 
{
	$num=explode(".",$number);
	$left=$num[0];
	$right=$num[1];
	if( strlen($right>$precision) ) $new_right=substr($right,0,$precision+1); else $new_right=$right;
	 
	if($round_type==1) return round ($number,$precision);
	if($precision==0) return round ($number,$precision);
	else if( $round_type==2 )
	{
		if( $right[$precision]>0 ) return $left.".".(( substr($right,0,$precision))+1); else return $left.".".(( substr($right,0,$precision)));
	}
	else if( $round_type==3 || $round_type==4 ) // down
	{
		if( $right[$precision]>0 ) return $left.".".(( substr($right,0,$precision))); else return $left.".".(( substr($right,0,$precision)));
	}
	 
}
?>


