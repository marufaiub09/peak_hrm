function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

function fnc_wages_process()
{
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var wages_for 	= escape(document.getElementById('wages_for').value);
	var txt_from_date 	= escape(document.getElementById('txt_from_date').value);
	var txt_to_date 	= escape(document.getElementById('txt_to_date').value);
	//var txt_precision=escape(document.getElementById('txt_precision').value);
	// alert(txt_precision);
	$("#messagebox").removeClass().addClass('messagebox').text('Wages Processing....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_from_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_to_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_to_date').focus();
			$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else
	{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/generate_weekly_wages_processing.php?action=wages_processing'+
					'&cbo_company_name='+cbo_company_name+
					'&wages_for='+wages_for+
					'&txt_from_date='+txt_from_date+
					'&txt_to_date='+txt_to_date
					);
		http.onreadystatechange = regular_process_wages_reply;
		http.send(null); //+'&txt_precision='+txt_precision
					
	}
}

function regular_process_wages_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('**');	
	//alert(response[0]);	
	//alert(response[0]+'==='+response[1]);.split('_')			
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Process found in given period.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Process locked in given period.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Please  process wages before  from date.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}	
		else if (response[0]==4)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Wages process successfully done .').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}	
		else if (response[0]==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Wages process Unsuccessful.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}	
		else if (response[0]==6)
		{
			$("#messagebox").removeClass().addClass('messagebox').text('').fadeOut(1000);
			$('#process_anim').html('<b>Mgs:-</b>Process not completed due to rate not found for the following process<br>'+response[1]);
			
			
			
			/*$.ajax({
				type: "POST",
				url: "includes/list_view.php?&process_id="+response[1],
				data: 'type=process_without_search',
				success: function(html) {				
				
				//save_sal_grade();
				}
				});*/
			
		}
		else if (response==7)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Database is Busy, please try after some time.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('process_anim').innerHTML='';
				$("#process_anim").html('').fadeIn(1000);
				$(this).css('background-color', 'red');
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
						
	}
}	


var timercount = 0;
var timestart  = null;
 
function showtimer() {
	if(timercount) {
		clearTimeout(timercount);
		clockID = 0;
	}
	if(!timestart){
		timestart = new Date();
	}
	var timeend = new Date();
	var timedifference = timeend.getTime() - timestart.getTime();
	timeend.setTime(timedifference);
	var minutes_passed = timeend.getMinutes();
	if(minutes_passed < 10){
		minutes_passed = "0" + minutes_passed;
	}
	var seconds_passed = timeend.getSeconds();
	if(seconds_passed < 10){
		seconds_passed = "0" + seconds_passed;
	}
	document.timeform.timetextarea.value = minutes_passed + ":" + seconds_passed;
	timercount = setTimeout("showtimer()", 1000);
}
 
function sw_start(){
	if(!timercount){
	timestart   = new Date();
	document.timeform.timetextarea.value = "00:00";
	document.timeform.laptime.value = "";
	timercount  = setTimeout("showtimer()", 1000);
	}
	else{
	var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var minutes_passed = timeend.getMinutes();
		if(minutes_passed < 10){
			minutes_passed = "0" + minutes_passed;
		}
		var seconds_passed = timeend.getSeconds();
		if(seconds_passed < 10){
			seconds_passed = "0" + seconds_passed;
		}
		var milliseconds_passed = timeend.getMilliseconds();
		if(milliseconds_passed < 10){
			milliseconds_passed = "00" + milliseconds_passed;
		}
		else if(milliseconds_passed < 100){
			milliseconds_passed = "0" + milliseconds_passed;
		}
		document.timeform.laptime.value = minutes_passed + ":" + seconds_passed + "." + milliseconds_passed;
	}
}

function Stop() {
	if(timercount) {
		clearTimeout(timercount);
		timercount  = 0;
		var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var minutes_passed = timeend.getMinutes();
		if(minutes_passed < 10){
			minutes_passed = "0" + minutes_passed;
		}
		var seconds_passed = timeend.getSeconds();
		if(seconds_passed < 10){
			seconds_passed = "0" + seconds_passed;
		}
		var milliseconds_passed = timeend.getMilliseconds();
		if(milliseconds_passed < 10){
			milliseconds_passed = "00" + milliseconds_passed;
		}
		else if(milliseconds_passed < 100){
			milliseconds_passed = "0" + milliseconds_passed;
		}
		document.timeform.timetextarea.value = minutes_passed + ":" + seconds_passed + "." + milliseconds_passed;
	}
	timestart = null;
}
 
function Reset() {
	timestart = null;
	document.timeform.timetextarea.value = "00:00";
	document.timeform.laptime.value = "";
}
