 
<?php

session_start();
include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');
include('salary_functions.php');

$e_date = time();
$user_only = $_SESSION['logic_erp']["user_name"];
$user_id_id=$_SESSION['logic_erp']["user_id"];

extract( $_GET );

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Division
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Weekly Holiday
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$weekend_lists[] = $row['weekend'];
	}
//Govt Holiday
	$sql = "SELECT * FROM lib_holiday_details WHERE is_deleted = 0 ORDER BY holiday_date ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday_lists = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday_lists[] = $row['holiday_date'];
	}

// Shift Details
	$shift_lists = array(); 
	$sql_shift = "SELECT * FROM lib_policy_shift";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$shift_lists[$row_shift[id]]['shift_start']=$row_shift[shift_start];
		$shift_lists[$row_shift[id]]['shift_end']=$row_shift[shift_end];
		$shift_lists[$row_shift[id]]['shift_type']=$row_shift[shift_type];
		$shift_lists[$row_shift[id]]['in_grace_minutes'] =add_time( $row_shift[shift_start], $row_shift[grace_minutes]); // Add In Grace Time with In Time
		$shift_lists[$row_shift[id]]['exit_buffer_minutes']=$row_shift[exit_buffer_minutes];
		
		$shift_lists[$row_shift[id]]['tiffin_start']=$row_shift[first_break_start];
		$shift_lists[$row_shift[id]]['tiffin_ends']=$row_shift[first_break_end];
		$shift_lists[$row_shift[id]]['dinner_start']=$row_shift[second_break_start];
		$shift_lists[$row_shift[id]]['dinner_ends']=$row_shift[second_break_end];
		
		$shift_lists[$row_shift[id]]['lunch_start']=$row_shift[lunch_time_start];
		$shift_lists[$row_shift[id]]['lunch_break_min']=datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]); 
		$shift_lists[$row_shift[id]]['total_working_min']=datediff(n,$row_shift[shift_start],$row_shift[shift_end])-datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]);
		$shift_lists[$row_shift[id]]['early_out_start']=$row_shift[early_out_start];
		$shift_lists[$row_shift[id]]['entry_restriction_start']=$row_shift[entry_restriction_start];
	
	}

// OT POLICY Details
	$ot_policy = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$ot_policy[$row_shift[id]]['overtime_rate']=$row_shift[overtime_rate];
		$ot_policy[$row_shift[id]]['overtime_calculation_rule']=$row_shift[overtime_calculation_rule];
		$ot_policy[$row_shift[id]]['max_overtime']=$row_shift[max_overtime];
	}
// OT SLab POLICY Details
	$ot_policy_slab = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime_slab order by policy_id,overtime_head asc";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	$i=0;
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		if (in_array($row_shift[policy_id],$pol)) $i=$i+1; else $i=0; 
		$pol[]=$row_shift[policy_id];
		$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]][$i]['overtime_slot']=$row_shift[overtime_slot];
		$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]][$i]['overtime_multiplier']=$row_shift[overtime_multiplier];
		//$ot_policy_slab[$row_shift[policy_id]][$i]['overtime_head']['overtime_head']=$row_shift[overtime_head];
	}
// Salary Breakdown Employee Wise
	$salary_breakdown = array();
	$sql_sal="select * from hrm_employee_salary";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_breakdown[$row_sal[emp_code]][$row_sal[payroll_head]]=$row_sal[amount];
	}
// All Salary Head for This Company	
	$salary_head_list = array();
	$salary_head_list_i= array();
	$salary_head_type= array();
	$sql_sal="select * from lib_payroll_head where is_applicable=1 and status_active=1 and is_deleted=0 order by id";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	$i=0;
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_head_list_head[$row_sal[payroll_head]]=$row_sal[id];
		  $salary_head_list_i[$i]=$row_sal[id];
		  $salary_head_type[$i]=$row_sal[type];
		  $i++;
	}

$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$var_hrm_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$var_hrm_chart[$row['company_name']] = array();
	$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
	$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
	$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
	$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
	$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
	$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
}

// Holiday Incentive Policy
$sql = "SELECT * FROM lib_policy_holiday_incentive ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$var_holiday_mst = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$var_holiday_mst[$row['id']]['pay_for'] = mysql_real_escape_string( $row['pay_for'] );
	$var_holiday_mst[$row['id']]['min_working_hour'] = mysql_real_escape_string( $row['min_working_hour'] );
	$var_holiday_mst[$row['id']]['earning_head'] = mysql_real_escape_string( $row['earning_head'] );
}

// Holiday Details POLICY Details
	$var_holiday_dtls = array();
	$sql_shift = "SELECT * FROM lib_policy_holiday_incentive_definition order by policy_id asc";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	$i=0;
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		if (in_array($row_shift[policy_id],$pol)) $i=$i+1; else $i=0; 
		$pol[]=$row_shift[policy_id];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['parameter_type']=$row_shift[parameter_type];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['percentage_formula']=$row_shift[percentage_formula];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['base_head']=$row_shift[base_head];
		$var_holiday_dtls[$row_shift[policy_id]][$i]['amount']=$row_shift[amount];
	}

// Arrear Calculations
$cbo_salary_periods = explode('_', $cbo_salary_periods ); 
 
$sql = "SELECT a.*,b.emp_code FROM hrm_arrear_earning_dtls a, hrm_arrear_earning_mst b WHERE b.id=a.mst_id and a.payment_month like '$cbo_salary_periods[0]' and b.is_approved=1";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$increment_arrear = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$increment_arrear[$row['emp_code']][$row['payment_month']] =  $row['amount'] ;
}

// Loan Pay Array
$sql = "select * from  hrm_loan_payback_scheduling_details where pay_date like 'cbo_salary_periods[0]' and is_paid=0 order by id asc";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$loan_amount_array = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$loan_amount_array[$row['emp_code']][$row['pay_date']] =  $row['total_amnt_per_inst'] ;
}

/*
// Increment Amount Processing
$sql = "select * from  hrm_increment_mst where is_approved=1 and effective_date like 'cbo_salary_periods[0]' and is_posted=0  order by id asc";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$increment_amount_array = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$increment_amount_array[$row['emp_code']]['id'] =  $row['id'];
	$increment_amount_array[$row['emp_code']]['new_gross_salary'] =  $row['new_gross_salary'];
	$increment_amount_array[$row['emp_code']]['increment_amount'] =  $row['increment_amount'];
}
*/

/*
// Late Deduction Plociy Details
$sql = "select * from  lib_policy_late_deduction_criteria order by id asc";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$late_deduction_policy_dtls = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$late_deduction_policy_dtls[$row['id']]['late_slot'] =  $row['late_slot'] ;
	$late_deduction_policy_dtls[$row['id']]['parameter_type'] =  $row['parameter_type'] ;
	$late_deduction_policy_dtls[$row['id']]['percentage_formula'] =  $row['percentage_formula'] ;
	$late_deduction_policy_dtls[$row['id']]['base_head'] =  $row['base_head'] ;
	$late_deduction_policy_dtls[$row['id']]['amount'] =  $row['amount'] ;  
}


 */
/*
Used Short  Terms 
A=Absent, P=Present, D=Late/Delay, CL,ML,SL,EL...=All Leaves,SP=Suspended, H=Holiday, W=Weekend, R=Regular Day, MR=Movement Register
*/
// Processing Started Here

if ($action=="salary_processing")
{
	set_time_limit(0);
	ignore_user_abort(1);
	
	//$cbo_salary_periods = explode('_', $cbo_salary_periods ); 
	$txt_from_date = convert_to_mysql_date( $cbo_salary_periods[0] );
	$txt_to_date = convert_to_mysql_date( $cbo_salary_periods[1] );
	 
	$i=0;
	
 	$tot_days=daysdiff($txt_from_date, $txt_to_date);
	
	if(trim($txt_emp_code)==""){ //new salary process
		$sql_emp="select * from hrm_employee where salary_rule<>0 order by emp_code desc"; 
	}
	else //salary re process
	{
		$sql_mst_id = "select id from hrm_salary_mst where emp_code in ($txt_emp_code) and salary_periods like '".date("Y-m",strtotime(convert_to_mysql_date($txt_from_date)))."%'";
		$sql_mst_id_result=mysql_db_query($DB,$sql_mst_id) or die(mysql_error()."<br />".$sql_mst_id);
		while($result_row=mysql_fetch_array($sql_mst_id_result)){
				$delete_mst_id=mysql_query("delete from hrm_salary_mst where id=$result_row[id]");
				$delete_dtls_id=mysql_query("delete from hrm_salary_dtls where salary_mst_id=$result_row[id]");
		}
		$sql_emp="select * from hrm_employee where emp_code in ($txt_emp_code) and salary_rule<>0 order by emp_code desc";
	} 
	
		$exe_sql_emp=mysql_db_query($DB, $sql_emp);
		$emp_salary_sheet=array();
		$emp_extra_details=array();
		$no_process=array();
		while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
		{ 
			$txt_from_date = convert_to_mysql_date( $cbo_salary_periods[0] );
			$txt_to_date = convert_to_mysql_date( $cbo_salary_periods[1] );
			$emp_condition=0; // Regular Worker
			if ($row_emp[status_active]==0) // In active Employee
			{
				$d="separated_from";
				$dd="hrm_separation";
				$ddd="emp_code='$row_emp[emp_code]' order by id desc";
				$old_mod= return_field_value($d,$dd,$ddd);
				
				if (strtotime($old_mod)>=strtotime($txt_from_date) && strtotime($old_mod)<strtotime($txt_to_date))
				{
					$emp_condition=1; // Resign Worker
					$txt_to_date=convert_to_mysql_date($old_mod);
				}
				else $emp_condition=3; // not  Worker
			}
			
			if ($row_emp[status_active]==1)
			{
				if (strtotime($row_emp[joining_date])>strtotime($txt_from_date) && strtotime($row_emp[joining_date])<=strtotime($txt_to_date))
				{
					$emp_condition=2; // New Worker
					$txt_from_date=convert_to_mysql_date($row_emp[joining_date]);
				}
				else if (strtotime($row_emp[joining_date])>strtotime($txt_to_date) ) $emp_condition=3; // not Worker
			}
		$check_salary_presence=return_field_value("emp_code","hrm_salary_mst"," emp_code='$row_emp[emp_code]' and salary_periods like '".date("Y-m",strtotime(convert_to_mysql_date($txt_from_date)))."%'");
	 	
		if ($emp_condition!=3 && $check_salary_presence=="") 
		{
			$total_ot_hr_nrd=0;
			 
			
		/*	if ($increment_amount_array[$row_emp['emp_code']]['id']!='')
			{
				//$salary_breakdown[$row_sal[emp_code]][$row_sal[payroll_head]]=$row_sal[amount];
			}
			*/
			$emp_salary_sheet[$row_emp[emp_code]][0]=$row_emp[gross_salary];
			
			$hourly_ot_rate=($salary_breakdown[$row_emp[emp_code]][1]/208)*2; // from Var Setting
			$daily_gross_salary=$emp_salary_sheet[$row_emp[emp_code]][0]/$tot_days; // from Var Setting
			
			$sql_attnd=" SELECT  count(CASE WHEN status ='P' THEN emp_code END) AS 'present',count(CASE WHEN status ='A' THEN emp_code END) AS 'absent',count(CASE WHEN status ='D' THEN emp_code END) AS 'late',count(CASE WHEN status ='GH' THEN emp_code END) AS 'g_holiday',count(CASE WHEN status ='FH' THEN emp_code END) AS 'f_holiday',count(CASE WHEN status ='CH' THEN emp_code END) AS 'c_holiday',count(CASE WHEN status ='W' THEN emp_code END) AS 'weekday',count(CASE WHEN status ='MR' THEN emp_code END) AS 'movement',count(CASE WHEN status ='CL' THEN emp_code END) AS 'cl',count(CASE WHEN status ='FL' THEN emp_code END) AS 'fl',count(CASE WHEN status ='EL' THEN emp_code END) AS 'el',count(CASE WHEN status ='SL' THEN emp_code END) AS 'sl',count(CASE WHEN status ='ML' THEN emp_code END) AS 'ml',count(CASE WHEN status ='SpL' THEN emp_code END) AS 'spl',count(CASE WHEN status ='LWP' THEN emp_code END) AS 'lwp',count(CASE WHEN status ='SP' THEN emp_code END) AS 'sp'  from hrm_attendance where emp_code='$row_emp[emp_code]' and attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' ";
			$exe_sql_attnd=mysql_db_query($DB, $sql_attnd);  //sum(CASE WHEN total_over_time_min <>0 THEN total_over_time_min END) AS 'total_over_time_min',
			if ($row_attnd = mysql_fetch_array($exe_sql_attnd))
			{ 
				 $present_days=$row_attnd[present];
				 $absent_days=$row_attnd[absent];
				 $late_days=$row_attnd[late];
				 $g_holiday_days=$row_attnd[g_holiday];
				 $f_holiday_days=$row_attnd[f_holiday];
				 $c_holiday_days=$row_attnd[c_holiday];
				 $weekday_days=$row_attnd[weekday];
				 $movement_days=$row_attnd[movement];
				 $c_leave_days=$row_attnd[cl];
				 $s_leave_days=$row_attnd[sl];
				 $e_leave_days=$row_attnd[el];
				 $m_leave_days=$row_attnd[ml];
				 $f_leave_days=$row_attnd[fl];
				 $spl_leave_days=$row_attnd[spl];
				 $lwp_leave_days=$row_attnd[lwp];
				 $suspended_days=$row_attnd[sp];
				 $total_cal_days=$present_days+$absent_days+$late_days+$g_holiday_days+$f_holiday_days+$c_holiday_days+$weekday_days+$movement_days+$c_leave_days+ $s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days+$lwp_leave_days+$f_leave_days+$suspended_days;
				 $total_leave_days=$c_leave_days+$s_leave_days+$e_leave_days+$m_leave_days+$spl_leave_days+ $lwp_leave_days+$f_leave_days; 
			}
			
			if ($emp_condition==2) // New
				$total_payable_days=daysdiff(convert_to_mysql_date($row_emp[joining_date]),$txt_to_date);	//New Join 
			else if ($emp_condition==1) // Resign
				$total_payable_days=daysdiff($txt_from_date,convert_to_mysql_date($old_mod));	// Resign
			else
				$total_payable_days=$tot_days;	//Regular
			
			$calendar_days=$tot_days;
			$not_payable_days=$calendar_days-$total_payable_days;
			$total_working_days=$calendar_days-($g_holiday_days+$f_holiday_days+$c_holiday_days+$weekday_days+$not_payable_days);
			$net_payable_days=$calendar_days-($absent_days+$lwp_leave_days+$not_payable_days);
			$salary_breakdown[$row_emp[emp_code]][20]=$emp_salary_sheet[$row_emp[emp_code]][0];
			
		
			// Start Traversing Slary Head and calcutae corresspndiong amont
			for($j=0; $j<count($salary_head_list_i);$j++)
			{
					
				if($salary_head_list_i[$j]==1)        // Basic Head
				{
					
					$emp_salary_sheet[$row_emp[emp_code]][1]= $salary_breakdown[$row_emp[emp_code]][1];  	 // Basic
					$daily_basic_salary=$salary_breakdown[$row_emp[emp_code]][1]/$tot_days; // from Var Setting
					$ot_rate=$emp_salary_sheet[$row_emp[emp_code]][1]/104;
					
				}
				else if($salary_head_list_i[$j]==2) $emp_salary_sheet[$row_emp[emp_code]][2]= $salary_breakdown[$row_emp[emp_code]][2];  // House Rent
				else if($salary_head_list_i[$j]==3) $emp_salary_sheet[$row_emp[emp_code]][3]= $salary_breakdown[$row_emp[emp_code]][3];  // Medical
				else if($salary_head_list_i[$j]==4) $emp_salary_sheet[$row_emp[emp_code]][4]= $salary_breakdown[$row_emp[emp_code]][4];  // Conveyence
				
				else if($salary_head_list_i[$j]==5 ) // Over Time
				{
					
					$sql_data="select * from  lib_policy_overtime_slab where policy_id=$row_emp[overtime_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$old_hr=0;
					$old_slot=0;
					$ot_slot=0;
					$u=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$u++;
						$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
						$ot_multiplier=$sql_data_rslt[overtime_multiplier];
						
						$total_ot_hr= get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."'",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
						$total_ot_hr=$total_ot_hr-$old_hr;
						$old_hr= $old_hr+$total_ot_hr; 
						
						if($sql_data_rslt[overtime_head]==5)
						{
							$total_ot_hr_nrd = get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' and is_regular_day=0",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
							$emp_salary_sheet[$row_emp[emp_code]][5]= $emp_salary_sheet[$row_emp[emp_code]][5]+($ot_rate*$ot_multiplier*$total_ot_hr);  
							$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+$total_ot_hr;
							$emp_extra_details[$row_emp[emp_code]][5]['rate']=$ot_rate;
						}
					}
					// echo $emp_extra_details[$row_emp[emp_code]][5]['ot_hr']."-".$row_emp[emp_code]; die;
					$ot_slot=0;
				}
				else if($salary_head_list_i[$j]==6 ) // Extra Over Time
				{
					$sql_data="select * from  lib_policy_overtime_slab where policy_id=$row_emp[overtime_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$old_hr=0;
					$old_slot=0;
					$u=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$u++;
						$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
						$ot_multiplier=$sql_data_rslt[overtime_multiplier];
						
						$total_ot_hr= get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."'",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
						$total_ot_hr=$total_ot_hr-$old_hr;
						$old_hr= $old_hr+$total_ot_hr; 
	 
						if($sql_data_rslt[overtime_head]==6)
						{
							$emp_salary_sheet[$row_emp[emp_code]][6]= $emp_salary_sheet[$row_emp[emp_code]][6]+($ot_rate*$ot_multiplier*$total_ot_hr);  
							$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$total_ot_hr+$total_ot_hr_nrd; 
							$emp_extra_details[$row_emp[emp_code]][6]['rate']=$ot_rate;
						}
					}
				}
				else if( $salary_head_list_i[$j]==7) // Further Extra Over Time
				{
					$sql_data="select * from  lib_policy_overtime_slab where policy_id=$row_emp[overtime_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$old_hr=0;
					$old_slot=0;
					$u=0;
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$u++;
						$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
						$ot_multiplier=$sql_data_rslt[overtime_multiplier];
						
						$total_ot_hr= get_buyer_tot_emp_ot_hr($row_emp[emp_code]," attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."'",$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'],$ot_slot);
						$total_ot_hr=$total_ot_hr-$old_hr;
						$old_hr= $old_hr+$total_ot_hr; 
						//echo $total_ot_hr."yy".$ot_slot."--";
						if($sql_data_rslt[overtime_head]==7)
						{
							$emp_salary_sheet[$row_emp[emp_code]][7]= $emp_salary_sheet[$row_emp[emp_code]][7]+($ot_rate*$ot_multiplier*$total_ot_hr);  
							$emp_extra_details[$row_emp[emp_code]][7]['ot_hr']= $emp_extra_details[$row_emp[emp_code]][7]['ot_hr']+$total_ot_hr; 
							$emp_extra_details[$row_emp[emp_code]][7]['rate']=$ot_rate;
						}
					} //echo $old_hr.".--".$emp_salary_sheet[$row_emp[emp_code]][7]."-amount-hr-".$emp_extra_details[$row_emp[emp_code]][7]['ot_hr']; die;
				}
				else if($salary_head_list_i[$j]==8) // Holiday Allowance
				{
					$holidays=explode('_', get_total_payable_holidays_amount( $row_emp[emp_code], $txt_from_date, $txt_to_date, "'H','W'", $var_holiday_mst, $var_holiday_dtls,  $row_emp[holiday_allowance_entitled], $emp_salary_sheet, $total_payable_days)) ; 
					
					$emp_salary_sheet[$row_emp[emp_code]][8]= $holidays[1]; //get_total_ot_hr($row_emp[emp_code],$txt_from_date, $txt_to_date,1);  
					$emp_extra_details[$row_emp[emp_code]][8]['nof_holi']= $holidays[0]; //get_total_ot_rate( $row_emp[emp_code],$overtime_policy[$row_emp[emp_code]] ); 
					
				}
				else if($salary_head_list_i[$j]==9) $emp_salary_sheet[$row_emp[emp_code]][9]= $salary_breakdown[$row_emp[emp_code]][9];   // Tiffin Allowance
				else if($salary_head_list_i[$j]==10) $emp_salary_sheet[$row_emp[emp_code]][10]= $salary_breakdown[$row_emp[emp_code]][10];   // Driver Allowance
				else if($salary_head_list_i[$j]==11) $emp_salary_sheet[$row_emp[emp_code]][11]= $salary_breakdown[$row_emp[emp_code]][11];   // Vehicle Allowance
				else if($salary_head_list_i[$j]==12) $emp_salary_sheet[$row_emp[emp_code]][12]= $salary_breakdown[$row_emp[emp_code]][12];   // Utility Allowance 
				
				else if($salary_head_list_i[$j]==13) $emp_salary_sheet[$row_emp[emp_code]][13]= $salary_breakdown[$row_emp[emp_code]][13];   // Entertainment Allowance  
				else if($salary_head_list_i[$j]==14) $emp_salary_sheet[$row_emp[emp_code]][14]= $salary_breakdown[$row_emp[emp_code]][14];   // Incentives Allowance 
				else if($salary_head_list_i[$j]==15)        // Company PF Contribution Allowance 
				{
					$emp_salary_sheet[$row_emp[emp_code]][15]= $salary_breakdown[$row_emp[emp_code]][15];   // PF Rules Added Here
				}
				else if($salary_head_list_i[$j]==16)	// Festival Bonus Allowance 
				{
					//$emp_salary_sheet[$row_emp[emp_code]][12]= $salary_breakdown[$row_emp[emp_code]][12];  
				}
				else if($salary_head_list_i[$j]==17)	// Arear Earnings from Increment Table
				{
					$emp_salary_sheet[$row_emp[emp_code]][17]=$increment_arrear[$row_emp[emp_code]][$cbo_salary_periods[0]];
				}
				else if($salary_head_list_i[$j]==18) $emp_salary_sheet[$row_emp[emp_code]][18]= $salary_breakdown[$row_emp[emp_code]][18];   // Leave Fair Assistance Allowance   
				 
				else if($salary_head_list_i[$j]==19) $emp_salary_sheet[$row_emp[emp_code]][19]= $salary_breakdown[$row_emp[emp_code]][19];   // Other Earnings Allowance 
				//else if($salary_head_list_i[$j]==20) $emp_salary_sheet[$row_emp[emp_code]][20]= $salary_breakdown[$row_emp[emp_code]][20];   // Gross Salary  Rules
				else if($salary_head_list_i[$j]==35)  // Attendance Bonus  Rules
				{
					if( $emp_condition==0)// Regular Worker
					{
						$sql_data="select * from  lib_policy_attendance_bonus_definition where policy_id=$row_emp[attendance_bonus_policy] order by id asc";
						$sql_data_exe=mysql_query($sql_data); 
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							if($sql_data_rslt[leave_criteria]>= $total_leave_days && $sql_data_rslt[late_criteria]>=$late_days  &&   $sql_data_rslt[absent_criteria]>=$absent_days )
							{
								$att_bonus_amount=$sql_data_rslt[amount]; break;
							}
						}  
						// echo $total_leave_days."-".$late_days."-".$absent_days."asdasd".$att_bonus_amount."<br>";
						 $emp_salary_sheet[$row_emp[emp_code]][35]=$att_bonus_amount; // $salary_breakdown[$row_emp[emp_code]][35];  
						 $att_bonus_amount=0;
					}
					else  // New and Resign Worker ---------------NO Bonus
					{
						 $emp_salary_sheet[$row_emp[emp_code]][35]=0; // $salary_breakdown[$row_emp[emp_code]][35];  
						 $att_bonus_amount=0;
					}
				}
				else if($salary_head_list_i[$j]==36) $emp_salary_sheet[$row_emp[emp_code]][36]= $salary_breakdown[$row_emp[emp_code]][36];   // Night Allowance  Rules
				
				// All Deduction Head Goes From Here
				else if($salary_head_list_i[$j]==21) // Advance or Salary Loan Deduction
				{
					if($salary_breakdown[$row_emp[emp_code]][21]<1)
					 	$loan_amount=$loan_amount_array[$row_emp[emp_code]][$cbo_salary_periods[0]];
					else
					 	$loan_amount=$salary_breakdown[$row_emp[emp_code]][21]; 
					 
					$emp_salary_sheet[$row_emp[emp_code]][21]= $loan_amount; 
					 $loan_amount=0;
					//echo   $emp_salary_sheet[$row_emp[emp_code]][21]; die;
				}
				else if($salary_head_list_i[$j]==22) // Advance Income Tax 
				{
					$emp_salary_sheet[$row_emp[emp_code]][22]= $salary_breakdown[$row_emp[emp_code]][22];   
				}
				else if($salary_head_list_i[$j]==23) // Late Deduction  
				{
					
					$sql_data="select * from  lib_policy_late_deduction_criteria where policy_id=$row_emp[late_deduction_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data); 
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						if ($late_days>= $sql_data_rslt[late_slot])
						{ 
							$days_to_deduct= floor($late_days/$sql_data_rslt[late_slot]); 
							if ( $sql_data_rslt[parameter_type]==0) // Percentage
							{
								$late_deduct_amnt=(($sql_data_rslt[percentage_formula]*($salary_breakdown[$row_emp[emp_code]][$sql_data_rslt[base_head]]/$tot_days))/100)*$days_to_deduct;  
								break;
							}
							else if ( $sql_data_rslt[parameter_type]==2) // Fixed
							{
								$late_deduct_amnt=$sql_data_rslt[amount]*$days_to_deduct;
								break;
							}
						}
					}  
					
					$emp_salary_sheet[$row_emp[emp_code]][23]= $late_deduct_amnt; 
					$late_deduct_amnt=0;
					//echo   $emp_salary_sheet[$row_emp[emp_code]][23]; die;
				}
				
				else if($salary_head_list_i[$j]==24) // Absent Deduction  
				{
					 
						
					$sql_data="select * from  lib_policy_absent_deduction_definition where policy_id=$row_emp[absent_deduction_policy] order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$abs_deduct_amnt=0; 
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						if ($absent_days > 0)
						{ 
							if ( $sql_data_rslt[parameter_type]==0)
							{
								$abs_deduct_amnt=(($sql_data_rslt[percentage_formula]*($salary_breakdown[$row_emp[emp_code]][$sql_data_rslt[base_head]]/$tot_days))/100)*$absent_days;
								break;
							}
							else if ( $sql_data_rslt[parameter_type]==2)
							{
								$abs_deduct_amnt=$sql_data_rslt[amount]*$absent_days;
								break;
							}
						}
						
					}  
					
					$emp_salary_sheet[$row_emp[emp_code]][24]= $abs_deduct_amnt; 
					$abs_deduct_amnt=0;
					//echo   $emp_salary_sheet[$row_emp[emp_code]][24]; die;
				}
				else if($salary_head_list_i[$j]==25) // Provident Fund Contribution
				{
					$emp_salary_sheet[$row_emp[emp_code]][25]= $salary_breakdown[$row_emp[emp_code]][25];   
				}
				else if($salary_head_list_i[$j]==26) // Transport Contribution
				{
					$emp_salary_sheet[$row_emp[emp_code]][26]= $salary_breakdown[$row_emp[emp_code]][26];   
				}
				else if($salary_head_list_i[$j]==27) // Lunch Contribution
				{
					$emp_salary_sheet[$row_emp[emp_code]][26]= $salary_breakdown[$row_emp[emp_code]][26];   
				}
				else if($salary_head_list_i[$j]==28) // Revenue Stamp
				{
					$emp_salary_sheet[$row_emp[emp_code]][28]= $salary_breakdown[$row_emp[emp_code]][28];   
				}
				else if($salary_head_list_i[$j]==29) // Leave Without Pay
				{
					$sql_data="select * from  lib_policy_leave_definition where policy_id=$row_emp[leave_policy] and leave_type='LWP' order by id asc";
					$sql_data_exe=mysql_query($sql_data);
					$lwp_deduct_amnt=0; 
					 
					while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
					{
						$policy_def=explode('_',$sql_data_rslt[deduction]);
						
						if ($lwp_leave_days > 0)
						{ 
							if ( $policy_def[0]==0) // 100%
							{
								$lwp_deduct_amnt=($salary_breakdown[$row_emp[emp_code]][$policy_def[1]]/$tot_days)*$lwp_leave_days;
								break;
							}
							else if ( $policy_def[0]==1) //50%
							{
								$lwp_deduct_amnt=(($policy_def[0]*($salary_breakdown[$row_emp[emp_code]][$policy_def[1]]/$tot_days))/100)*$lwp_leave_days;
								break;
							}
							else if ( $policy_def[0]==2) // Fixed
							{
								$lwp_deduct_amnt=$policy_def[1]*$lwp_leave_days;
								break;
							}
						}
					}  
					
					$emp_salary_sheet[$row_emp[emp_code]][29]= $lwp_deduct_amnt; 
					   $lwp_deduct_amnt=0;
				}
				else if($salary_head_list_i[$j]==30) // Arear Deduction
				{
					$emp_salary_sheet[$row_emp[emp_code]][30]= $salary_breakdown[$row_emp[emp_code]][30];   
				}
				else if($salary_head_list_i[$j]==31) // Others Deduction
				{
					 
					$emp_salary_sheet[$row_emp[emp_code]][31]= $salary_breakdown[$row_emp[emp_code]][31];   
				}
				else if($salary_head_list_i[$j]==32) // Investment Allowance
				{
					$emp_salary_sheet[$row_emp[emp_code]][32]= $salary_breakdown[$row_emp[emp_code]][32];   
				}
				else if($salary_head_list_i[$j]==33) // Taxable Income
				{
					$emp_salary_sheet[$row_emp[emp_code]][33]= $salary_breakdown[$row_emp[emp_code]][33];   
				}
				else if($salary_head_list_i[$j]==34) // Tax Rebate
				{
					$emp_salary_sheet[$row_emp[emp_code]][34]= $salary_breakdown[$row_emp[emp_code]][34];   
				}
				 
			} // End of For Loop
			// Calculate total 
			
			$total_earning =$emp_salary_sheet[$row_emp[emp_code]][1]+$emp_salary_sheet[$row_emp[emp_code]][2]+$emp_salary_sheet[$row_emp[emp_code]][3]+$emp_salary_sheet[$row_emp[emp_code]][4]+$emp_salary_sheet[$row_emp[emp_code]][5]+$emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]+$emp_salary_sheet[$row_emp[emp_code]][8]+$emp_salary_sheet[$row_emp[emp_code]][9]+$emp_salary_sheet[$row_emp[emp_code]][10]+$emp_salary_sheet[$row_emp[emp_code]][11]+$emp_salary_sheet[$row_emp[emp_code]][12]+$emp_salary_sheet[$row_emp[emp_code]][13]+$emp_salary_sheet[$row_emp[emp_code]][14]+$emp_salary_sheet[$row_emp[emp_code]][15]+$emp_salary_sheet[$row_emp[emp_code]][16]+$emp_salary_sheet[$row_emp[emp_code]][17]+$emp_salary_sheet[$row_emp[emp_code]][18]+$emp_salary_sheet[$row_emp[emp_code]][19]+$emp_salary_sheet[$row_emp[emp_code]][35]+$emp_salary_sheet[$row_emp[emp_code]][36];
			$b_total_earning_amount=$total_earning - ($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
			 //echo $total_earning."--".$b_total_earning_amount."<br>";// die;
			$total_deduction =$emp_salary_sheet[$row_emp[emp_code]][21]+$emp_salary_sheet[$row_emp[emp_code]][22]+$emp_salary_sheet[$row_emp[emp_code]][23]+$emp_salary_sheet[$row_emp[emp_code]][24]+$emp_salary_sheet[$row_emp[emp_code]][25]+$emp_salary_sheet[$row_emp[emp_code]][26]+$emp_salary_sheet[$row_emp[emp_code]][27]+$emp_salary_sheet[$row_emp[emp_code]][28]+$emp_salary_sheet[$row_emp[emp_code]][29]+$emp_salary_sheet[$row_emp[emp_code]][30]+$emp_salary_sheet[$row_emp[emp_code]][31]+$emp_salary_sheet[$row_emp[emp_code]][32]+$emp_salary_sheet[$row_emp[emp_code]][33]+$emp_salary_sheet[$row_emp[emp_code]][34];
			 
			
		 
			// Save Data to Table From Here
			 
			if( $row_emp[ot_entitled]==1) // Ot Entitled
			{
				$is_with_over_time=1; 
				$total_over_time_min=$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][7]['ot_hr'];
				$extra_ot=$emp_extra_details[$row_emp[emp_code]][6]['ot_hr']+$emp_extra_details[$row_emp[emp_code]][7]['ot_hr'];
			}
			else
			{
				$total_earning=$total_earning-($emp_salary_sheet[$row_emp[emp_code]][5]+$emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
				$b_total_earning_amount=$total_earning; // - ($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
				$is_with_over_time=0;
				$total_over_time_min=0;
				$emp_salary_sheet[$row_emp[emp_code]][5]=0;
				$emp_salary_sheet[$row_emp[emp_code]][6]=0;
				$emp_salary_sheet[$row_emp[emp_code]][7]=0;
				$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']=0;
				$extra_ot=0;
			}
			// Check Disciplinary Actions for Deduction
			if($suspended_days>0)
			{ 
				$emp_condition=3;
				$suspension_deduct_amnt=0;
				$sql_data="select a.* from  hrm_disciplinary_info_dtls a, hrm_disciplinary_info_mst b where a.disc_mst_id=b.id and b.emp_code='$row_emp[emp_code]' order by b.id desc";
				$sql_data_exe=mysql_query($sql_data);
				while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
				{
					$suspension_deduct_amnt=$suspension_deduct_amnt+(($sql_data_rslt[impact_salary_head_perc]*($emp_salary_sheet[$row_emp[emp_code]][$sql_data_rslt[impact_salary_head]]/$tot_days))/100)*$suspended_days;
				}
			}
			// Check Disciplinary Actions for Deduction
			$late_amt = $emp_salary_sheet[$row_emp[emp_code]][23];
			$absent_amt = $emp_salary_sheet[$row_emp[emp_code]][24];
			$lwp_amt = $emp_salary_sheet[$row_emp[emp_code]][29];
			
			 
			$net_payable_amount=$total_earning-($total_deduction+($not_payable_days*$daily_gross_salary)); 
			$b_net_payable_amount=$net_payable_amount-($emp_salary_sheet[$row_emp[emp_code]][6]+$emp_salary_sheet[$row_emp[emp_code]][7]);
			if($row_id=="") $row_id = return_next_id( "id", "hrm_salary_mst" ); else $row_id+=1;
			$sql = "INSERT INTO hrm_salary_mst (id,salary_periods,emp_code,employee_category,gross_salary,basic_salary,medical_allowance,house_rent,total_earning_amount,b_total_earning_amount,total_deduction_amount,is_with_over_time,b_over_time,e_over_time,total_over_time,over_time_rate,net_payable_amount,b_net_payable_amount,payable_days,total_abs_days,total_late_days,total_lwp_days,special_leave,earn_leave,faternity_leave,maternity_leave,sick_leave,casual_leave,government_holiday,festival_holiday,compensatory_total_holiday,total_weekly_holiday,not_payble_days,suspend_days, suspend_amount,daily_basic_salary,daily_gross_salary,total_working_days,total_calendar_days,total_leave_days,total_late_deduction_amount,total_absent_deduction_amount,total_lwp_deduction_amount,company_id,location_id,division_id,department_id,section_id,subsection_id,emp_status,total_present_days)			
								 VALUES ('$row_id', '".$cbo_salary_periods[0]."', '$row_emp[emp_code]','$row_emp[category]','".$emp_salary_sheet[$row_emp[emp_code]][0]."','".$emp_salary_sheet[$row_emp[emp_code]][1]."','".$emp_salary_sheet[$row_emp[emp_code]][3]."','".$emp_salary_sheet[$row_emp[emp_code]][2]."','$total_earning','$b_total_earning_amount','$total_deduction',
								 '$is_with_over_time','".$emp_extra_details[$row_emp[emp_code]][5]['ot_hr']."','".$extra_ot."','$total_over_time_min','$ot_rate','$net_payable_amount','$b_net_payable_amount','$net_payable_days','$absent_days','$late_days',
								 '$lwp_leave_days','$spl_leave_days','$e_leave_days','$f_leave_days','$m_leave_days','$s_leave_days','$c_leave_days','$g_holiday_days','$f_holiday_days','$c_holiday_days','$weekday_days','$not_payable_days','$suspended_days','$suspension_deduct_amnt','$daily_basic_salary','$daily_gross_salary','$total_working_days','$calendar_days','$total_leave_days','$late_amt','$absent_amt','$lwp_amt','$row_emp[company_id]','$row_emp[location_id]','$row_emp[division_id]','$row_emp[department_id]','$row_emp[section_id]','$row_emp[subsection_id]','$emp_condition','$present_days')";
								// echo $sql; die;
							 mysql_query( $sql ) or die (mysql_error());
				
				for($k=0; $k<count($salary_head_list_i);$k++)
				{
					if($salary_head_type[$j]==0 || $salary_head_type[$j]==3 || $salary_head_type[$j]==4 ) $is_deductive=0; // Earning
					else  $is_deductive=1;
						
					if($id=="") $id = return_next_id( "id", "hrm_salary_dtls" ); else $id+=1; //$id = return_next_id( "id", "hrm_salary_dtls" );
					$sql="INSERT INTO hrm_salary_dtls (id,salary_mst_id,emp_code,salary_head_id,pay_amount,is_deducted,head_type,pay_slip_id) values ('$id','$row_id','$row_emp[emp_code]','".$salary_head_list_i[$k]."','".$emp_salary_sheet[$row_emp[emp_code]][$salary_head_list_i[$k]]."','$is_deductive','$salary_head_type[$k]','$id')";
					mysql_query( $sql ) or die (mysql_error());  
				}
				 $net_payable_amount=0; $total_earning=0; $total_deduction=0;	 $total_earning=0;$b_total_earning_amount=0;$total_deduction=0;$b_net_payable_amount=0;				 
				//echo $total_earning."-".$total_deduction."--".$emp_salary_sheet[$row_emp[emp_code]][0]."\n";die;
			}
			else
			{
				$no_process[]=$row_emp[emp_code]; 
			}
		} // End of Employee Loop
		$no_process_emp = implode(",",$no_process);  
  		echo $no_process_emp."*1";
		die;
	/*}
	else
	{
		$no_process_emp = implode(",",$no_process);  
  		echo $no_process_emp."*2";
		die;
	}*/
  

}

function get_total_payable_holidays_amount( $emp_code, $txt_from_date, $txt_to_date, $status, $mst_policy, $dtls_policy, $h_policy, $emp_salary_sheet, $tot_days)
{
	$sql_emp22="select * from hrm_attendance where emp_code='$emp_code' and attnd_date between '".convert_to_mysql_date($txt_from_date)."' and '".convert_to_mysql_date($txt_to_date)."' and status in($status) and sign_in_time<>'00:00:00' and sign_out_time<>'00:00:00' "; //and a.emp_code='0000038'  and a.emp_code='0000015' 
	 
	$exe_sql_emp22=mysql_query($sql_emp22);
	$day_no=0;
	while ($row_h22 = mysql_fetch_array($exe_sql_emp22))// EMp Master Qry starts 
	{ 
	 	$tot_min=datediff(n,$row_h22[sign_in_time],$row_h22[sign_out_time]);
		
		if ( $tot_min >=($mst_policy[$h_policy]['min_working_hour']*60))
		{
			$day_no=$day_no+1;
			if ($mst_policy[$h_policy]['pay_for']==0)  // pay for hour
			{
				$tot_hr=(int)$tot_min/60;
				for($j=0; $j<count($dtls_policy[$h_policy]); $j++)
				{
					if ($dtls_policy[$h_policy][$i]['parameter_type']==0) // Percentage
					{
						$amount+= ($emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['base_head']]/($tot_days*8))*$tot_hr ;
					}
					else if ($dtls_policy[$h_policy][$j]['parameter_type']==2)  // Fixed Amount
					{ 
						$amount+=  $emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['amount']]*($tot_hr) ;
					}
					 
				}
			}
			else //if ($mst_policy[$h_policy]['pay_for']==0)  // pay for Days
			{
				$tot_hr=(int)$tot_min/60;
				for($j=0; $j<count($dtls_policy[$h_policy]); $j++)
				{
					
					if ($dtls_policy[$h_policy][$j]['parameter_type']==0) // Percentage
					{
						$amount+= (($emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['base_head']]/($tot_days))*$dtls_policy[$h_policy][$j]['percentage_formula'])/100;
					}
					else if ($dtls_policy[$h_policy][$j]['parameter_type']==2)  // Fixed Amount
					{ 
						$amount+=  $emp_salary_sheet[$emp_code][$dtls_policy[$h_policy][$j]['amount']];
					}
					 
				}
			}
		}
	} // while
	return $day_no."_".$amount;
}

function get_total_ot_hr( $ot_policy_slab, $txt_from_date, $txt_to_date,$overtime_policy,$salary_head, $emp_code, $comp_id, $var_hrm_chart )
{
	 for ($i=0; $i<count($ot_policy_slab[$overtime_policy][$salary_head]); $i++)
	 {
		$slot=$slot+$ot_policy_slab[$overtime_policy][$salary_head][$i]['overtime_slot']*60; 
		$rate=$ot_policy_slab[$overtime_policy][$salary_head][$i]['overtime_rate']; 
		
		$str_date=" attnd_date between '$txt_from_date' and '$txt_to_date'";
		$tot_ot=get_buyer_tot_emp_ot_hr($emp_code,$str_date,$var_hrm_chart[$comp_id][one_hour_ot_unit],$var_hrm_chart[$comp_id][allow_ot_fraction],$var_hrm_chart[$comp_id][ot_start_minute],$slot);
		$act_ot=$act_ot+($tot_ot-$act_ot);
	
	 }
	 
	 return $act_ot;
}


 
// Functions required for this processing
function daysdiff($date1,$date2) 
{ 
	$diff = (strtotime($date2) - strtotime($date1));
	
	$days = floor($diff / (1*60*60*24)+1);
	return $days; 		
}

	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

 



function change_date_for_process( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "-", $date_field );
		return $dates[1] . "/" . $dates[2] . "/" . $dates[0];
	}
	else return $date_field;
}
function change_date_for_process_sql( $date_field ) {
	if( $date_field != '' ) {
		$dates = explode( "/", $date_field );
		return $dates[2] . "-" . $dates[0] . "-" . $dates[1];
	}
	else return $date_field;
}
function change_time_for_process( $time_field ) {
	if( $time_field != '' ) {
		$times = explode( "-", $time_field );
		return $times[0] . ":" . $times[1] . ":" . $times[2];
	}
	else return $time_field;
}
 
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>

