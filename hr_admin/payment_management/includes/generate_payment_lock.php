<?php
/************************************
|	Completed  By : Md. Nuruzzaman
|	Dated         : 24.03.2014 
*************************************/

date_default_timezone_set('Asia/Dhaka');
session_start();
$user_name=$_SESSION['logic_erp']["user_name"];

include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');

extract( $_REQUEST );
	
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
}
	
// payment_lock
if( isset( $form ) && $form =="payment_lock" )
{ 
	//echo "su..re";
	$date_lock=convert_to_mysql_date($date);
	
	if ($category_id== '')	$category_id	= "and e.category in(0,1,2,3,4)";	else $category_id = "and e.category='$category_id'";
	if ($company_id==0) $company_id=""; else $company_id="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0 || $division_id=="null" ) $division_id=""; else $division_id="and a.division_id in($division_id)";
	if ($department_id==0 || $department_id=="null") $department_id=""; else  $department_id="and a.department_id in($department_id)";
	if ($section_id==0 || $section_id=="null") $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=="null") $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($designation_id==0 || $designation_id=="null") $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	
	$amount=""; 
	$field_name='';
	if( $bill_type==1 ) 
	{ 
		$amount=" and a.today_tiffin_allowance>0 and is_get_tiffin_allowance=1";
		$field_name="a.today_tiffin_allowance";
	}
	else if($bill_type==2)
	{
		$amount=" and a.today_night_allowance>0 and is_get_night_allowance=1";
		$field_name="a.today_night_allowance";
	}
	
	$sql="SELECT e.emp_code, e.id_card_no,CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS name,e.designation_id,a.emp_code,a.attnd_date, $field_name as amount 
		FROM hrm_attendance a,hrm_employee e 
		WHERE a.attnd_date='$date_lock' and 
		a.emp_code=e.emp_code 
		$category_id
		$company_id 
		$location_id
		$division_id 
		$department_id
		$section_id 
		$subsection_id  
		$designation_id 
		$amount
		order by a.emp_code";
	?>
	<table cellpadding="0" id="tbl_payment_lock" cellspacing="0" border="1" width="780" class="rpt_table"  rules="all"> 
		<thead>
        	<th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.frm_payment_lock.chk_list)" placeholder="Search" /></th> 
            <th width="40">SL</th>
            <th width="100">System Code</th>
            <th width="100">ID Card No</th>
            <th width="170">Name</th>
            <th width="150">Designation</th>
            <th width="100" >Date</th>
            <th width="100" >Amount</th>
		</thead>
        <tbody>
			<?
            $sl=1;
            $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
            while( $row = mysql_fetch_assoc( $result ) ) 
            {
            ?>
                <tr bgcolor="<? echo $bgcolor;?>">
                    <td width="20">
                        <input type="checkbox" name="chk_list" id="chk_list<? echo $sl; ?>" value="<?php echo $row['emp_code']; ?>" onclick="count_emp(document.frm_payment_lock.chk_list,<? echo $sl; ?>);" />
                    </td>
                    <td width="40" align="center">
						<? echo $sl;?>
                        <input type="hidden" id="hdn_date" name="hdn_date" value="<? echo $date_lock;?>" />
                        <input type="hidden" id="hdn_bill_type" name="hdn_bill_type"  value="<? echo $bill_type;?>" />
                    </td>
                    <td width="100" align="center"><? echo $row[emp_code]; ?></td>
                    <td width="100" align="center"><? echo $row[id_card_no]; ?></td>
                    <td width="170"><? echo $row[name];?></td>
                    <td width="150"><? echo $designation_chart[$row[designation_id]];?></td>
                    <td width="100" align="center"><? echo convert_to_mysql_date($row[attnd_date]);?></td>
                    <td width="100" align="right"><? echo $row[amount];?></td>
                </tr>
            <?
            $sl++; 
            }
            ?>
        </tbody>
    </table>
	<?php
    exit();
}

// finaly_payment_lock
if($action=="finaly_payment_lock")
{
	$emp_codes=substr($emp_code,0,-1);
	
	if($bill_type==1)
	{
		$sql="UPDATE hrm_attendance SET is_tiffin_locked=1 WHERE emp_code in ($emp_codes) and attnd_date='$date'";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo 1;
	}
	if($bill_type==2)
	{
		$sql="UPDATE hrm_attendance SET is_night_allowance_locked=1 WHERE emp_code in ($emp_codes) and attnd_date='$date'";		
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo 2;
	}
}
?>