<?php
session_start();
date_default_timezone_set('UTC');
include('../../../includes/common.php');
include('../../../includes/array_function.php');
/* Replace the data in these two lines with data for your db connection */
$type=$_GET["type"];

extract($_GET);
extract($_POST);

	//Section
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
	
	//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$designation_details[$row['id']] = $row['custom_designation'];
	}
	
//buyer
	$sql = "SELECT * FROM lib_buyer ORDER BY buyer_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$buyer_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$buyer_details[$row['id']] = mysql_real_escape_string( $row['buyer_name'] );		
	}
	
//process
	$sql = "SELECT * FROM hrm_process_mst ORDER BY process_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$process_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$process_details[$row['id']] = mysql_real_escape_string( $row['process_name'] );		
	}
	
// uom_arr
foreach($uom_arr as $key=>$value)
{
	$uom_arr[$key]=$value;
}
	
	
if(isset($_GET['getClientId']))
{  
	if ($type==1) // Employee for Disciplinary Info
	{
		//Designation array
			$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$designation_chart = array();
			while( $row = mysql_fetch_assoc( $result ) ) 
			{
				$designation_chart[$row['id']] = $row['custom_designation'];
			}
			
		//Company array
			$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$company_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$company_details[$row['id']] = $row['company_name'];
			}
		//Department array
			$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$department_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$department_details[$row['id']] = $row['department_name'];
			}
		//Diviion
			$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$division_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$division_details[$row['id']] = $row['division_name'];
			}
		//Location
			$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$location_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$location_details[$row['id']] =$row['location_name'];
			}
		//Section
		$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$section_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$section_details[$row['id']] = $row['section_name'];
			}
		//Sub Section
			$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$subsection_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$subsection_details[$row['id']] = $row['subsection_name'];
			}
		
		
	$res = mysql_query("SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
					FROM hrm_employee AS emp
					LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
					WHERE emp.emp_code = '".$_GET['getClientId']."' and emp.is_deleted=0 and emp.status_active=1 order by emp.emp_code")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
			echo "formObj.txt_designation.value = '".mysql_real_escape_string($designation_chart[$inf["designation_id"]])."';\n";    
			echo "formObj.txt_company.value = '".mysql_real_escape_string($company_details[$inf["company_id"]])."';\n"; 
			echo "formObj.txt_division.value = '".mysql_real_escape_string($division_details[$inf["division_id"]])."';\n"; 
			echo "formObj.txt_department.value = '".mysql_real_escape_string($department_details[$inf["department_id"]])."';\n"; 
			echo "formObj.txt_section.value = '".mysql_real_escape_string($section_details[$inf["section_id"]])."';\n"; 
		} 
	}
	
	if ($type==2) // Disc for Disciplinary Info
	{
		$res = mysql_query("SELECT * from hrm_disciplinary_info_mst
				WHERE emp_code = '".$_GET['getClientId']."' and withdrawn_date='0000-00-00' order by id desc")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_occurrence_date.value = '".$inf["occurrence_date"]."';\n";    
			echo "formObj.txt_action_date.value = '".$inf["action_date"]."';\n";    
			echo "formObj.txt_occurrence_details.value = '".mysql_real_escape_string($inf["occurrence_details"])."';\n";    
			echo "formObj.txt_investigating_members.value = '".mysql_real_escape_string($inf["investigating_members"])."';\n"; 
			echo "formObj.txt_investigation.value = '".mysql_real_escape_string($inf["investigation"])."';\n"; 
			echo "formObj.cbo_action_taken.value = '".mysql_real_escape_string($inf["action_taken"])."';\n"; 
			echo "formObj.txt_withdawn_date.value = '".$inf["withdrawn_date"]."';\n"; 
			echo "formObj.save_up_disc.value = '".$inf["id"]."';\n";  
			//echo "formObj.save_up_po.value = '".$inf["id"]."';\n"; 
		} 
		else{
			echo "formObj.txt_occurrence_date.value = '';\n";    
			echo "formObj.txt_action_date.value = '';\n";    
			echo "formObj.txt_occurrence_details.value = '';\n";    
			echo "formObj.txt_investigating_members.value = '';\n"; 
			echo "formObj.txt_investigation.value = '';\n"; 
			echo "formObj.cbo_action_taken.value = '';\n"; 
			echo "formObj.txt_withdawn_date.value = '';\n"; 
			echo "formObj.save_up_disc.value = '';\n";  
		}
	}
	
	if ($type==3) // Disc for Disciplinary Info
	{
		$res = mysql_query("SELECT * from hrm_movement_register WHERE emp_code = '".$_GET['getClientId']."' and position=1")  or die(mysql_error());
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_from_date.value = '".convert_to_mysql_date($inf["from_date"])."';\n";    
			echo "formObj.txt_to_date.value = '".convert_to_mysql_date($inf["to_date"])."';\n";    
			echo "formObj.txt_movement_purpose.value = '".mysql_real_escape_string($inf["movement_purpose"])."';\n";    
			echo "formObj.txt_destination.value = '".mysql_real_escape_string($inf["destination_place"])."';\n"; 
			echo "formObj.txt_start_time.value = '".$inf["start_time"]."';\n"; 
			echo "formObj.txt_return_time.value = '".$inf["end_time"]."';\n"; 
			echo "formObj.txt_adviced_by.value = '".mysql_real_escape_string($inf["adviced_by"])."';\n"; 
			echo "formObj.txt_entry_date.value = '".convert_to_mysql_date($inf["entry_date"])."';\n";  
			echo "formObj.save_up_move.value = '".$inf["row_id"]."';\n"; 
			echo "formObj.Save.value = 'Update';\n"; 
		}
	}
}	


if($type==6)
{
	/* $res = mysql_query("SELECT a.*, CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name FROM hrm_employee_promotion a, hrm_employee b WHERE a.id = '".$_GET['getClientId']."' ")  or die(mysql_error());*/
	$res = mysql_query("SELECT a.*, CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name FROM hrm_employee_promotion a, hrm_employee b WHERE a.id = '".$_GET['getClientId']."' and b.status_active=1")  or die(mysql_error());
	if($inf = mysql_fetch_array($res))
	{
		//echo "formObj.emp_code.value ='hello';\n";
		echo "formObj.emp_code.value = '".mysql_real_escape_string($inf["emp_code"])."';\n";     
		echo "formObj.txt_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
		echo "formObj.new_designation_level.value = '".mysql_real_escape_string($inf["new_designation_level"])."';\n"; 
		echo "populate_designations_new(".$inf["new_designation_level"].");\n";   
		echo "formObj.new_designation_id.value = '".mysql_real_escape_string($inf["new_designation"])."';\n";
		
		//echo "document.getElementById('new_designation_id').value = '".mysql_real_escape_string($inf["new_designation"])."';\n";
		//echo "formObj.increment_app.checked = '".mysql_real_escape_string($inf["increment_applicable"])."';\n";
		//echo "formObj.increment_id.value = '".mysql_real_escape_string($inf["increment_amount"])."';\n";
		echo "formObj.old_designation_level.value = '".mysql_real_escape_string($inf["last_designation_level"])."';\n";    
		echo "formObj.old_designation_id.value = '".mysql_real_escape_string($inf["last_designation"])."';\n";
		echo "formObj.cbo_year_selector.value = '".mysql_real_escape_string(date("Y",strtotime($inf["effective_date"])))."';\n";
		echo "formObj.cbo_month_selector.value = '".mysql_real_escape_string(date("m",strtotime($inf["effective_date"])))."';\n";
		echo "formObj.apr_score.value = '".mysql_real_escape_string($inf["apr_score"])."';\n";
		echo "formObj.save_up.value = '".$inf["id"]."';\n"; 
	}
}

/*
if($type==6)
{
	$res =mysql_query("SELECT a.*,b.increment_amount,CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name from hrm_employee emp,hrm_employee_promotion a,hrm_increment_mst b WHERE a.emp_code=emp.emp_code and a.mst_id=b.id and a.id = '".$_GET['getClientId']."' ")  or die(mysql_error());
	$res = mysql_query("SELECT a.*,b.increment_amount from hrm_employee_promotion a,hrm_increment_mst b WHERE a.mst_id=b.id and a.id = '".$_GET['getClientId']."' ")  or die(mysql_error());
	if($inf = mysql_fetch_array($res))
	{
		echo "formObj.employee_code.value = '".mysql_real_escape_string($inf["emp_code"])."';\n";
		echo "formObj.txt_name.value = '".mysql_real_escape_string($inf["name"])."';\n";
		echo "formObj.designation_id.value = '".mysql_real_escape_string($inf["last_designation"])."';\n"; 
		echo "formObj.designation_id_last.value = '".mysql_real_escape_string($inf["last_designation"])."';\n";     
		echo "formObj.designation_level.value = '".mysql_real_escape_string($inf["new_designation_level"])."';\n";    
		echo "formObj.designation_id_level.value = '".mysql_real_escape_string($inf["new_designation"])."';\n"; 
		echo "formObj.increment_app.checked = '".mysql_real_escape_string($inf["increment_applicable"])."';\n";
		echo "formObj.increment_id.value = '".mysql_real_escape_string($inf["increment_amount"])."';\n";
		echo "formObj.cbo_year_selector.value = '".mysql_real_escape_string(date("Y",strtotime($inf["effective_date"])))."';\n";
		echo "formObj.cbo_month_selector.value = '".mysql_real_escape_string(date("m",strtotime($inf["effective_date"])))."';\n";
		echo "formObj.save_up.value = '".$inf["mst_id"]."';\n"; 
	}
}
*/

if($type=='fraction_salary'){ 
	
	if($id=='') $id=''; else $id='and a.id='.$id;
	$sql = "SELECT b.*,a.effective_from_date,a.effective_to_date from fraction_month_salary_mst a,fraction_month_salary_dtls b
					WHERE a.status_active=1 and a.id=b.mst_id $id";
	//echo $sql;
	$sql_res = mysql_query($sql) or die( mysql_error() );
	
	$i=0;
	echo "reset_form();\n";
	while($result=mysql_fetch_array($sql_res)){
		echo "$('#effective_from_date').val('$result[effective_from_date]');\n";
		echo "$('#effective_to_date').val('$result[effective_to_date]');\n";
		
		$i++;
		echo "add_row();\n";
		echo "$('#base_head_".$i."').val('".$result["salary_head"]."');\n"; 
		echo "$('#type_".$i."').val('".$result["type"]."');\n"; 
		echo "$('#amount_".$i."').val('".$result["amount"]."');\n"; 		
	}
	exit();
}

// piece_rate_production_entry_set_value
if( $link =="piece_rate_production_entry_set_value" ) 
{
	//echo "su..re";
	$sql="SELECT e.emp_code,e.id_card_no,CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS name,e.company_id,e.designation_id,e.section_id,
		p.*,d.uom as org_uom,d.rate as org_rate 
		FROM hrm_employee e, hrm_piece_rate_production p, hrm_process_rate_dtls d 
		WHERE p.id= '$id' and p.emp_code=e.emp_code and p.status_active=1 and p.is_deleted = 0 and d.process_id=$process_id and d.buyer_id=$buyer_id";
		//and d.process_id=$process_id and d.buyer_id=$buyer_id
	//echo $sql; 
	$result=mysql_query($sql)or die(mysql_error());
	if($inf=mysql_fetch_array($result))
	{
		//echo $inf["name"];
		$exp_prod_date=explode("-",$inf[prod_date]);
		$p_date=$exp_prod_date[2]."-".$exp_prod_date[1]."-".$exp_prod_date[0];
		
		echo "$('#cbo_company_id').val('$inf[company_id]');\n";
		echo "$('#txt_prod_date').val('$p_date');\n";
		echo "$('#cbo_prod_type').val('$inf[prod_type]');\n";
		echo "$('#txt_id_card_no').val('$inf[id_card_no]');\n";
		echo "$('#txt_name').val('$inf[name]');\n";
		echo "$('#txt_designation').val('".$designation_details[$inf[designation_id]]."');\n";
		echo "$('#txt_section').val('".$section_details[$inf[section_id]]."');\n";
		echo "$('#cbo_subsection').val('".$inf[subsection_id]."');\n";
		echo "$('#txt_buyer').val('".$buyer_details[$inf[buyer_id]]."');\n";
		echo "$('#txt_order_no').val('$inf[order_no]');\n";
		echo "$('#txt_operation').val('".$process_details[$inf[process_id]]."');\n";
		echo "$('#txt_prod_qty').val('$inf[prod_qty]');\n";
		echo "$('#txt_uom').val('".$uom_arr[$inf[uom]]."');\n";
		echo "$('#txt_rate').val('$inf[rate]');\n";
		echo "$('#present_rate_dzn').val('$inf[present_rate_dzn]');\n";
		if($inf[org_uom]==1){ $su=$inf[org_rate]/12; }
		else{ $su=$inf[org_rate]; }
		echo "$('#txt_rate_original').val($su);\n";
		echo "$('#txt_amount').val('$inf[amount]');\n";
		echo "$('#txt_emp_code').val('$inf[emp_code]');\n";
		echo "$('#txt_buyer_id').val('$inf[buyer_id]');\n";
		echo "$('#txt_operation_id').val('$inf[process_id]');\n";
		//echo "formObj.txt_name.value = '".$inf[name]."';\n";    
		//echo "formObj.txt_name.value = '".mysql_real_escape_string($inf["name"])."';\n";
		echo "$('#original_uom').text('".$uom_arr[$inf[org_uom]]."');\n";
		echo "$('#original_rate').val('$inf[org_rate]');\n";	
	}
	exit();
}

/*
//get data update for 
if( $link == 'all_update' ) 
{
	$txt_system_id=$txt_system_id;
	$employee_info = array();
	
	$sql_emp= "SELECT adj.*,adjcriteria.*,adj_emp.*
		FROM hrm_salary_adjust_mst adj, hrm_salary_adjustment_criteria adjcriteria, hrm_salary_adjusted_employee  adj_emp
		WHERE  adj.id=adjcriteria.salary_adjustment_mst_id and adjcriteria.salary_adjustment_mst_id=adj_emp.salary_adjustment_mst_id 
		and adj.id like '$txt_system_id'  order by adj_emp.emp_code";
		//echo $sql_emp;die;
	$emp_result =  mysql_query( $sql_emp ) or die( $sql_emp . "<br />" . mysql_error() );
	while( $row = mysql_fetch_assoc( $emp_result ) ) 
	{
		$employee_info[] = $row;
	
		echo "$('#cbo_company_name').val('$row[company_id]');\n";
		echo "populate_select_month($row[salary_year_id]);\n";
		echo "$('#cbo_year_selector').val('$row[salary_year_id]');\n";
		echo "$('#cbo_salary_periods').val('$row[salary_period_id]');\n";
		echo "$('#txt_punish_date').val('$row[action_date]');\n";
		echo "$('#entry_moment').val('$row[entry_moment_id]');\n";
		echo "$('#adjustment_type').val('$row[adjustment_type]');\n";
		echo "$('#emp_code').val('$row[emp_code]');\n";
	
		foreach( $employee_info as $key=>$val ) 
		{
			$id = $key + 1;
			if( $id > 1 ) echo "add_rows(". $key .");\n"; echo "populate_value($row[adjustment_type]);\n";
			echo "$('#criteria_".$id."').val('". $employee_info[$key]['criteria_id'] ."');\n";
			echo "$('#adjustment_amt_".$id."').val('". $employee_info[$key]['adjustment_amount'] ."');\n";
			echo "$('#sal_adj_formula_".$id."').val('". $employee_info[$key]['formula_id'] ."');\n";
			echo "$('#formula_value_".$id."').val('". $employee_info[$key]['formula_value'] ."');\n";
			echo "$('#base_head_".$id."').val('". $employee_info[$key]['base_head_id'] ."');\n";
			echo "$('#cbo_search_by_2_id_1').val('". $employee_info[$key]['payroll_head_id'] ."');\n";
			echo "$('#adjustment_reason_".$id."').val('". $employee_info[$key]['reason'] ."');\n";
		}
	}
	exit();
}
*/

//month generated
if($type=="select_month_generate")
{		
	extract($_GET);
	extract($_POST);
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 and a.is_locked=0";
	//echo $sql;die;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."_".$row[year_id]."'>$value</option>";
	}		
	exit();
}

//new add
if($type=="select_company_generate")
{
	extract($_GET);
	extract($_POST);
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1  and id=$id  ORDER BY company_name ASC";
	//echo $sql;die;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$name=$row['company_name'];
		echo "<option value='".$row['id']."'>$name</option>";
	}		
	exit();
}

if($action=="process_lock")
{
	$status='';
	$cbo_month_selector=explode("_",$cbo_month_selector);
	$start_actual_date=$cbo_month_selector[0];
	//$year_first_month=return_field_value("actual_starting_date","lib_policy_year_periods","year_id='$cbo_month_selector[2]' order by id asc limit 0,1");
	
	//year_id='$cbo_month_selector[2]'
	//echo $year_first_month;die;
	
	if($year_first_month==$start_actual_date)
	{
		$status=true;
	}
	else
	{
	    $prev_month=add_month($cbo_month_selector[0],-1);
		$is_locked=return_field_value("id","lib_policy_year_periods","actual_starting_date='$prev_month' and is_locked=1");//year_id='$cbo_month_selector[2]' and 
		if($is_locked!="")
		{
			$status=true;	
		}
		else
		{
			$status=false;
		}
	}
	//echo $status;die;
	if($status==true)
	{
		$sql="UPDATE lib_policy_year_periods SET is_locked=1 WHERE  year_id='$cbo_year_selector' and actual_starting_date='$cbo_month_selector[0]' 
		and actual_ending_date='$cbo_month_selector[1]'";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "*1"; //die;
	}
	else
	{
		echo "*3*$prev_month"; //die;
	}
	die;
	
	/*
	$month=date("n",strtotime($cbo_month_selector[0]));
	$start_actual_date=$cbo_month_selector[0];
	echo $month;die;
	$result = mysql_query("select * from lib_policy_year_periods where year_id='$cbo_month_selector[2]'");
	$result=mysql_query( $sql );
	$row=mysql_fetch_array($result);
	
	if(strtotime($row[actual_starting_date]) < strtotime($cbo_month_selector[0])){
		 $previous_month="and actual_starting_date='$cbo_month_selector[0]";
		
	}
	else
	{
		
		$new_month= add_month($cbo_month_selector[0],-1);
		$previous_month="and actual_starting_date='$new_month'";
	}
	
	echo $sql="select * from lib_policy_year_periods where year_id='$cbo_month_selector[2]' $previous_month";die;
	$result=mysql_query( $sql );
	$row=mysql_fetch_array($result);
	
		if($row[is_locked]!=1){ echo "*3";die;}
	*/
	
	$sql="UPDATE lib_policy_year_periods SET is_locked=1 WHERE  year_id='$cbo_year_selector' and actual_starting_date='$cbo_month_selector[0]' 
	and actual_ending_date='$cbo_month_selector[1]'";
	mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	 echo "*1"; //die;
	 die;
}

function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}



//list_view
if($type=="list_view")
{		
	extract($_GET);
	//extract($_POST);
	
	
	 $sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	?>
	 <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:12px;" width="300" rules="all"  align="center">
         <thead>
         		<tr>
                	<th colspan="3">Salary Period Lock Status</th>
                </tr> 
            	
            <tr>
                
                <th width="120">Start Date</th>
                <th width="120">End Date</th>
                <th width="80">Status</th>
            </tr>
		</thead> 
        <?
		$r=0;
		while( $row = mysql_fetch_assoc( $result ) ) 
		{
			if ($row[is_locked]==1) $bgcolor="#FF0000"; else $bgcolor="#EEEEEE"; 
			?>
			<tr bgcolor="<? echo $bgcolor;?>"  height="20">
				<td width="120"><? echo $row[starting_date]; ?></td>
				<td width="120"><? echo $row[ending_date]; ?></td>
				<td width="80"><?  if($row[is_locked]==1) echo "Locked"; else echo ""; ?></td>
			</tr>
			<?
			$r++;
		}	
		?>	
	 </table>
     <?
	 exit();
}

function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
}
?> 