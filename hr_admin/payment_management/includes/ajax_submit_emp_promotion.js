// JavaScript Document
function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer"){
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	}else{
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();

function fn_promotion_entry(){
	var emp_code=document.getElementById('emp_code').value;
	var txt_name=document.getElementById('txt_name').value;
	var company_id=document.getElementById('company_id').value;
	//var designation_id=document.getElementById('designation_id').value;
	var division_id=document.getElementById('division_id').value;
	var section_id=document.getElementById('section_id').value;
	var subsection_id=document.getElementById('subsection_id').value;
	var old_designation_id=document.getElementById('old_designation_id').value;
	var old_designation_level=document.getElementById('old_designation_level').value;
	var new_designation_id=document.getElementById('new_designation_id').value;
	var new_designation_level=document.getElementById('new_designation_level').value;
	//var increment_app=document.getElementById('increment_app').checked ? 1: 0;
	//var new_salary_break=$('#new_salary_break').val();
 	//alert(new_designation_level);
	//var increment_id=document.getElementById('increment_id').value;
	var effective_year=document.getElementById('cbo_year_selector').value;
	var effective_month=document.getElementById('cbo_month_selector').value;
	var effective_date = escape(effective_year)+"-"+escape(effective_month)+"-01";
	var save_up = document.getElementById('save_up').value;
	
	var apr_score = document.getElementById('apr_score').value;
	
 $("#messagebox").removeClass().addClass('messagebox').text('').fadeIn(500);
		 if(effective_month == "0" )	{
			$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
				$('#effective_month').focus();
				$(this).html('Please Select  effective month').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(1000);
			});
		}		
		else if(new_designation_id == "0" ){
			$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
				$('#designation_id_level').focus();
				$(this).html('Please Select  New Designation').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(1000);
			});
		}
		else if(new_designation_level == "0" ){
			$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
				$('#new_designation_level').focus();
				$(this).html('Please Select  New Designation Level').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(1000);
			});
		}
		/*else if(increment_app ==false){
			$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
				$('#increment_app').focus();
				$(this).html('Please Select  The Increment Applicable Checkbox').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(1000);
			});	
		}*/
		/*else if(increment_id ==""){
			$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
				$('#increment_id').focus();
				$(this).html('Please Select  The Increment Entry').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(1000);
			});	
		}
		*/	
			

else{
 

 http.open('get','includes/save_update_promotion.php?action=promotion_info&isupdate='+save_up
			  +'&employee_code='+emp_code  
			  +'&txt_name='+txt_name
			  +'&company_id='+company_id
			  +'&division_id='+division_id
			  +'&section_id='+section_id
			  +'&subsection_id='+subsection_id
			  +'&new_designation_level='+new_designation_level
			  +'&new_designation_id='+new_designation_id
			  +'&old_designation_id='+old_designation_id
			  +'&old_designation_level='+old_designation_level
			  +'&effective_date='+effective_date
			  +'&apr_score='+apr_score
			);
	
	http.onreadystatechange=response_emp_promotion;
	http.send(null);
	
}
}

function response_emp_promotion() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response);

	 if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				showResult_multi('',response[1],'search_promotion_employee_code','emp_promotion_list_view');
				$(this).fadeOut(5000);
			});
			//$('#policy_button').css('visibility','visible');
		}
		
		
		else if (response[0]==3)
		
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);

				showResult_multi('',response[1],'search_promotion_employee_code','emp_promotion_list_view');
				
				//(response[1],'6','emp_promotion_list_view');
				document.getElementById('save_up').value="";
				$(this).fadeOut(5000);
			});
			
		}
		else if (response[0]==5)
		
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Entry!! Promotion Already Exist.').addClass('messagebox_ok').fadeTo(900,1);

				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==6)
		
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Effective Date Must Be Greater Than Joining Date').addClass('messagebox_ok').fadeTo(900,1);

				document.getElementById('save_up').value="";
			});
		}
	}
}



