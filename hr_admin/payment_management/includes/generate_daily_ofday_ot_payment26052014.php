<?php

/*#####################################

Completed  By : Sohel
Dated         :25/02/2014 

#####################################*/


date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');

extract( $_REQUEST );

$user_name=$_SESSION['logic_erp']["user_name"];
$e_date = time();

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}


$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	

	//company info
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);


if( $action == "daily_ofday_ot_payment" ) //Daily Off Day Ot Payment=======================================start========================================== 
{ 
	$loc_id=$location_id;
	//echo $offdate_selector;die;
	
	$cbo_salary_periods = explode('_', $cbo_month_selector );
	$from_date_from= $cbo_salary_periods[0];
	$to_date_to= $cbo_salary_periods[1];
	
	$search_off_day = $cbo_salary_periods[0];
	$search_date=explode("-",$search_off_day);
	$off_dates=$search_date[0]."-".$search_date[1]."-"."%%";
	//echo $off_dates;die;
	$cur_month = $months["'"+$search_date[1]+"'"];

	$rpt_company=$company_id;
	if ($cbo_company_id==0) $company_id=""; else $company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else	$department_id=" and  a.department_id in ($department_id)";
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($emp_code=="") $emp_code=""; else $emp_code="and b.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="b.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,b.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,b.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,b.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,b.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,b.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,b.emp_code,";}

	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(b.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(b.id_card_no), 5)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$r=1;
	ob_start();

	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$rpt_company' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		$ot_fraction = $res['allow_ot_fraction'];
		$ot_start_min = $res['ot_start_minute'];
		$one_hr_ot_unit = $res['one_hour_ot_unit'];
		$adjust_out_time=$res['adjust_out_time'];
		$applicable_above_salary=$res['applicable_above_salary'];
		$in_out_time_format=$res['in_out_time_format']; 
		$adjust_in_time = $res['adjust_in_time'];
		$var_hrm_chart=$res['ot_rate_multiplier'];
	}
	
	$salary_breakdown = array();
	$sql_sal="select * from hrm_employee_salary";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_breakdown[$row_sal[emp_code]][$row_sal[payroll_head]]=$row_sal[amount];
	}
	
	$greade_id_arr = array();
	$sql_sal="select * from lib_salary_grade where status_active='1' and is_deleted='0'";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$greade_id_arr[$row_sal[grade_name]]=$row_sal[id];
	}
	$ot_rate_arr = array();
	$sql_sal="select * from hrm_piece_rate_ot_mst where status_active='1' and is_deleted='0'";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($ot_rate = mysql_fetch_array($exe_sql_sal))
	{
		$ot_rate_arr[$ot_rate[grade_id]]=$ot_rate[rate];
	}
	
	
	$req_ot_arr = array();
	$sql_sal="select * from ot_requisition_dtl where status_active='1' and is_deleted='0'";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($ot_req = mysql_fetch_array($exe_sql_sal))
	{
		$req_ot_arr[$ot_req[emp_code]][$ot_req[ot_date]]=$ot_req[budgeted_ot];
	}
	
	$offdate_selector;
	$offdate = explode(',', $offdate_selector );
			
	?>
	<div style="1200px;" align="left">
	<table cellpadding="0" cellspacing="0" border="1" width="1200" class="rpt_table"  rules="all"> 
		<thead> 
			<tr height="30">
				<th colspan="13" align="center" bordercolor="#FFFFFF" style="font-size:18px;">
                <br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br />
                <?php  if($loc_id!=0){ ?> <font size="2pt"><? echo $location_details[$loc_id]."<br/>"; ?></font>  <?php } ?>
                <font size="2pt">Off Day OT Payment Sheet</font><br /><br />
                </th>
			</tr>
			  
			<tr height="40">
				<th width="30">SL</th>
				<th width="80">Emp code</th>
				<th width="80">ID No</th>
				<th width="150">Name</th>
				<th width="130">Designation</th>
				<th width="70" >Off Date</th>
				<th width="60" >Status</th>
                <th width="70" >Basic</th>
				<th width="60">Total OT</th>
				<th width="80">OT Rate</th>
				<th width="80">Amount</th>
				<th width="150">Signature</th>
				<th width="">Remarks</th> 
			</tr>           
		</thead>
	</table>
	</div>
    <?
	
	$small_print .=' <div style="1200px;" align="left">
	<table cellpadding="0" cellspacing="0" border="1" width="1200" class="rpt_table"  rules="all"> 
	<thead> 
	<tr height="30">
		<th colspan="13" align="center" bordercolor="#FFFFFF" style="font-size:18px;">
			<br /><font size="+1">'. $company_info_result["company_name"].'</font><br />
			<font size="2pt">'. $location_details[$loc_id]."<br/>".'</font>
			<font size="2pt">Off Day OT Payment Sheet</font><br /><br />
		</th>
	</tr>
	<tr height="40">
		<th width="30">SL</th>
		<th width="80">Emp code</th>
		<th width="80">ID No</th>
		<th width="150">Name</th>
		<th width="130">Designation</th>
		<th width="70" >Off Date</th>
		<th width="60" >Status</th>
		<th width="70" >Basic</th>
		<th width="60">Total OT</th>
		<th width="80">OT Rate</th>
		<th width="80">Amount</th>
		<th width="150">Signature</th>
		<th width="">Remarks</th> 
	</tr>           
	</thead>
	';
		
		?>
         <div style="1200px; overflow-y:scroll; max-height:300px;" align="left">
        <table cellpadding="0" cellspacing="0" border="1" width="1200" class="rpt_table"  rules="all" id="ofday_ot_payment"> 
        <?
	
	$sql="SELECT a.attnd_date,a.total_over_time_min,a.is_next_day,a.sign_in_time,a.sign_out_time,a.status,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.designation_id,CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name,b.emp_code,b.id_card_no,b.designation_level,b.salary_grade FROM hrm_attendance a,hrm_employee b
	WHERE a.emp_code=b.emp_code and 
		a.attnd_date in ($offdate_selector) and 
		a.status in ('GH','FH','CH','W') and 
		a.is_regular_day=0 and 
		b.ot_entitled=1 and 
		a.sign_in_time!='00:00:00' and a.sign_out_time!='00:00:00' and
		b.status_active=1
		$cbo_company_id 
		$location_id
		$division_id 
		$department_id
		$section_id 
		$subsection_id  
		$designation_id 
		$salary_based
		$id_card_no 
		$category
		$emp_code group by $dynamic_groupby $orderby";
		
		//echo $sql;
		//a.is_regular_day=0 and 
		//a.total_over_time_min!=0 and

	
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$off_day_ot_minute = array();
		$off_day_ot_rate = array();
		$total_amount="";
		
		$sl=0;
		while( $row = mysql_fetch_assoc( $result ) ) 
		{
			if ($cbo_salary_based==0) 
			{ 
				$ot_rate= round(($salary_breakdown[$row[emp_code]][1]/208)*$var_hrm_chart,2);
			}
			else 
			{
				if($row[salary_grade]=="" || $ot_rate_arr[$greade_id_arr[$row[salary_grade]]]==0 || $ot_rate_arr[$greade_id_arr[$row[salary_grade]]]=="")
				{
					$ot_rate= round(($salary_breakdown[$row[emp_code]][1]/208)*$var_hrm_chart,2);
				}
				else
				{
				 	$ot_rate= round($ot_rate_arr[$greade_id_arr[$row[salary_grade]]],2);
				}
			}
			$sl++;
			//start header print---------------------------------//
			if($sl==1)
			{
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
			}//end if condition of header print
			if($sl!=1)
			{
				$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				if(in_array($row[location_id],$location_arr) && $status_loc==1)
				{
					if(in_array($row[division_id],$division_arr) && $status_divis==1)
					{
						if(in_array($row[department_id],$department_arr) && $status_dept==1)
						{
							if(in_array($row[section_id],$section_arr) && $status_sec==1)
							{ 
								if(in_array($row[subsection_id],$subsection_arr) && $status_subsec==1)
								{}
								else if($status_subsec==1)
								{
								$subsection_arr[$row[subsection_id]]=$row[subsection_id];
								$new_subsec=1;
								}
							}
							else if($status_sec==1)
							{
								$section_arr[$row[section_id]]=$row[section_id];
								$subsection_arr=array();
								$subsection_arr[$row[subsection_id]]=$row[subsection_id];
								$new_sec=1;
								$new_subsec=1;
							}
						}
						else if($status_dept==1)
						{
							$department_arr[$row[department_id]]=$row[department_id];
							$section_arr=array();
							$subsection_arr=array();
							$section_arr[$row[section_id]]=$row[section_id];
							$subsection_arr[$row[subsection_id]]=$row[subsection_id];
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}
					}//division
					else if($status_divis==1)
					{
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//division else
				}//location
				else if($status_loc==1)
				{
					$location_arr[$row[location_id]]=$row[location_id];
					$division_arr=array();
					$department_arr=array();
					$section_arr=array();
					$subsection_arr=array();
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
					$new_loc=1;
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//location else
			}
			//header print here 
			$c_part_1="";
			if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
			{
				if($status_loc==1)
				{					
					$c_part_1 .= "  Location :  ".$location_details[$row[location_id]]."  ,";
				}				
				if($status_divis==1)
				{
					$c_part_1 .= "  Division :  ".$division_details[$row[division_id]]."  ,";
				}
				if($status_dept==1)
				{
					$c_part_1 .= "  Department :  ".$department_details[$row[department_id]]."  ,";
				}
				if($status_sec==1)
				{
					$c_part_1 .= "  Section :  ".$section_details[$row[section_id]]."  ,";
				}
				if($status_subsec==1)
				{
					$c_part_1 .= "  Sub Section :  ".$subsection_details[$row[subsection_id]]."  ,";
				}
				if($c_part_1!='')
				{
					//$i=0;
					$sl=1;
					?>
					<tr bgcolor="#CCCCCC">
						<td colspan="13"><b><?php echo substr($c_part_1,0,-1); ?></b></td>
					</tr>
					<?php  
					$small_print .='<tr bgcolor="#CCCCCC">
					<td colspan="13"><b>'. substr($c_part_1,0,-1).'</b></td>
					</tr>';    
				}
			}
			
			 if ($r%2==0) $bgcolor="#EEEEEE"; else $bgcolor="#FFFFFF"; 

			 
			 if($row[is_next_day]==0)
			 {
				 $asper_ot=datediff(n,$row[sign_in_time],$row[sign_out_time]); 
				 //$asper_ot=abs((int)($asper_ot));
			 }
			 else
			 {
				$mtime='23:59:59';
				$fast_ot=datediff(n,$row[sign_in_time],$mtime);
				$second_ot=datediff(n,'00:00:00',$row[sign_out_time]);
				$second_ot=($second_ot+1); 
				//$asper_ot=abs((int)($fast_ot+$second_ot)); 
				$asper_ot=($fast_ot+$second_ot);

			 }
			//echo $asper_ot;die;
			 
			$ot_hour="";$ot_amount="";
			// echo $req_ot_arr[$row[emp_code]][$row[attnd_date]]."abc";
			 if($req_ot_arr[$row[emp_code]][$row[attnd_date]]!=0 || $req_ot_arr[$row[emp_code]][$row[attnd_date]]!='')
			 {
				 if($asper_ot>480 && $asper_ot<1140)
				 {
					$total_ot=($asper_ot-60); 
				 }
				 else if($asper_ot>=1140)
				 {
					$total_ot=($asper_ot-120); 
				 }
				 else
				 {
					$total_ot=$asper_ot; 
				 }
				 
				 if($req_ot_arr[$row[emp_code]][$row[attnd_date]]<$total_ot)
				 {
					 $ho_ot=$req_ot_arr[$row[emp_code]][$row[attnd_date]];
				 }
				 else
				 {
					  $ho_ot=$total_ot;
				 }
				 $rate=($ot_rate/60);
				 $ot_amount=($ho_ot*$rate);
				 
				 $ot_hour= sprintf("%d . %02d ", abs((int)($ho_ot/60)), abs((int)($ho_ot%60)));
			 }
		 
		  //get_buyer_ot_hr($row[total_over_time_min],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
		  
		
		  ?>
		<tr bgcolor="<? echo $bgcolor;?>" height="50">
			<td width="30"><? echo $r;?> </td>
			<td width="80"><? echo $row[emp_code]; ?><input type="hidden" id="empcode_<? echo $r;?>" name="empcode_<? echo $r;?>" value="<? echo $row[emp_code]; ?>"/></td>
			<td width="80"><p><? echo $row[id_card_no]; ?></p></td>
			<td width="150"><? echo $row[name];?></td>
			<td width="130"><? echo $designation_chart[$row[designation_id]];?></td>
			<td width="70"><? echo $row[attnd_date];?><input type="hidden" id="attendate_<? echo $r;?>" name="attendate_<? echo $r;?>" value="<? echo $row[attnd_date];?>"/></td>
			<td width="60" align="center"><? echo $row[status];?> </td>
			<td width="70" align="center"><? echo round($salary_breakdown[$row[emp_code]][1]);?> </td>
			<td width="60" align="center"><? echo check_zero_value($ot_hour,$row[status],0); ?><input type="hidden" id="othour_<? echo $r;?>" name="othour_<? echo $r;?>" value="<? echo $ot_hour;?>"/></td>
			<td width="80" align="center"><? echo $ot_rate; ?><input type="hidden" id="otrate_<? echo $r;?>" name="otrate_<? echo $r;?>" value="<? echo $ot_rate; ?>" /></td>
			<td width="80" align="center">
			<?
			//echo round($ot_amount);
			echo $amount=round($ot_amount);
			
			 ?>
            <input type="hidden" id="otamount_<? echo $r;?>" name="otamount_<? echo $r;?>" value="<? echo $amount; $total_amount+=$amount; ?>" /></td>
			<td width="150">&nbsp;</td>
			<td width="">&nbsp;</td>
		</tr>
           
            <?
			
			 $small_print .=' <tr bgcolor="'.$bgcolor.'" height="40">
                <td width="30">'. $r.'</td>
                <td width="80">'. $row[emp_code].'</td>
                <td width="80">'. $row[id_card_no].'</td>
                <td width="150">'. $row[name].'</td>
                <td width="130">'. $designation_chart[$row[designation_id]].'</td>
                <td width="70">'. $row[attnd_date].'</td>
                <td width="60" align="center">'. $row[status].'</td>
				<td width="70" align="center">'.round($salary_breakdown[$row[emp_code]][1]).' </td>
                <td width="60" align="center">'. $ot_hour.'</td>
                <td width="80" align="center">'. $ot_rate.'</td>
                <td width="80" align="center">'. $amount.'</td>
                <td width="150">&nbsp;</td>
                <td width="">&nbsp;</td>
            </tr>';
			
			$r++; 
	}
		
?>	
        <tr>
            <td  colspan="10">&nbsp;<strong>Grand Total</strong> (In Words: ) <? echo number_to_words_final($total_amount); ?></td>
            <td  align='center'> <? echo number_format($total_amount);?> </td>
            <td colspan="2">&nbsp;</td>
        </tr>
    <tfoot>
        <tr>
            <td colspan="13">
            <? echo signeture_table_fashion(8,1,"1200px"); ?>
            </td>
        </tr>
    </tfoot>  
</table>
    </div>		 
<?	
   	 $small_print .='<tr>
            <td  colspan="10">&nbsp;<strong>Grand Total</strong> (In Words: ) '. number_to_words_final($total_amount).'</td>
            <td  align="center">'. number_format($total_amount).' </td>
            <td colspan="2">&nbsp;</td>
        </tr>';
		
		/*$small_print .='<tfoot>
			<tr>
				<td colspan="13">'. signeture_table_fashion(8,1,"1210px").'</td>
			</tr>
		</tfoot> ';*/
        
        
	if ($width=="") $width="100%"; 
		$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id='8' and company_id=1");
		$signeture_list = explode('#',$signeture_allow_or_no);
		if($signeture_allow_or_no!= "")	
		{
			$small_print .= '<tfoot><tr><td colspan="13"><table width="1200px">
			<tr height="90" style="font-size:14px; font-family:Arial; font-weight:bold">
			';
			foreach($signeture_list as $key=>$value)
			{
			//echo '<td align="center" valign="bottom" style="text-decoration:overline">'.$value.'</td> ';
			$small_print .='<td align="center" valign="bottom" style="border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;border-bottom-style:hidden;">'.$value.'</td> ';
			}
			echo '</tr></table>';
			}
			else
			{
			$small_print .='<table width="1200px">
			<tr height="90" style="font-size:18px; font-family:Arial; font-weight:bold">
			';
			
			$small_print .='</tr></table></td></tr></tfoot>';
		}		
	

     $small_print .='</table></div> ';
	//$small_print .='signeture_table_fashion(8,1,"1210px")';
	//echo signeture_table(8,$rpt_company,"1700px") ; 

	$html = ob_get_contents();
	ob_clean();		
	
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name"."####".$small_print;
	exit();
}



if($type=="save_update_offday_ot_payment")
{
	$id_field_name = "id";
	$table_name = "hrm_offday_ot_payment";
	
	mysql_query("SET CHARACTER SET utf8");
	mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
	
	$exp_emp_code=explode("*",$emp_code);
	$exp_atten_date=explode("*",$atten_date);
	$exp_ot_hour=explode("*",$ot_hour);
	$exp_ot_rate=explode("*",$ot_rate);
	$exp_ot_amount=explode("*",$ot_amount);
	
	for($i=0;$i<count($exp_emp_code);$i++)
	{
		$sql="SELECT id,emp_code,offday_date,is_locked FROM hrm_offday_ot_payment WHERE emp_code='$exp_emp_code[$i]' and offday_date= '$exp_atten_date[$i]'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if(mysql_num_rows($result)>0)
		{
			/*while( $row = mysql_fetch_assoc( $result ) ) 
			{
				if($row[is_locked]==1)
				{
				echo "2"."**".$exp_atten_date[$i];die;
				}
				else
				{*/
					$sql = "UPDATE hrm_offday_ot_payment
					SET
					emp_code='$exp_emp_code[$i]',
					offday_date='$exp_atten_date[$i]',
					ot_hour='$exp_ot_hour[$i]',
					ot_rate='$exp_ot_rate[$i]',
					ot_amount='$exp_ot_amount[$i]',
					updated_by='$user_name',
					update_date='$e_date'
					WHERE emp_code='$exp_emp_code[$i]' and offday_date='$exp_atten_date[$i]'";
					mysql_query( $sql ) or die (mysql_error());
				//}
			//}
		}
		else
		{
			$id= return_next_id($id_field_name,$table_name);
			$sql2 = "INSERT INTO hrm_offday_ot_payment(
			id,
			emp_code,
			offday_date,
			ot_hour,
			ot_rate,
			ot_amount,
			inserted_by,
			insert_date
			) VALUES(
			'$id',
			'$exp_emp_code[$i]',
			'$exp_atten_date[$i]',
			'$exp_ot_hour[$i]',
			'$exp_ot_rate[$i]',
			'$exp_ot_amount[$i]',
			'$user_name',
			'$e_date'
			)";
			mysql_query( $sql2 ) or die (mysql_error());
		}
	}
	$sql_history=encrypt($sql, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo 1;
	exit();
	
	
	//echo $emp_code."ggg";die;
}





//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}

/*
if($type=="offday_date_generate")
{		
	//2012-03-01_2012-03-31
	$exp_date=explode("_",$id);
	$from_date=$exp_date[0];
	$to_date=$exp_date[1];
	$sql = "SELECT attnd_date FROM hrm_attendance WHERE attnd_date between '$from_date' and '$to_date' and status in('GH','FH','CH','W') group by attnd_date ORDER BY attnd_date ASC  ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$value = $row["attnd_date"];
		echo "<option value='".$row[attnd_date]."'>$value</option>";
	}		
	exit();
}*/


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}



//new for fashion
function signeture_table_fashion($report_id,$company,$width)
{
	if ($width=="") $width="100%"; 
	$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company");
	$signeture_list = explode('#',$signeture_allow_or_no);
	if($signeture_allow_or_no!= "")	
	{
	 echo '<table width="'.$width.'">
		<tr height="90" style="font-size:18px; font-family:Arial; font-weight:bold">
		 ';
		foreach($signeture_list as $key=>$value)
		{
			//echo '<td align="center" valign="bottom" style="text-decoration:overline">'.$value.'</td> ';
			echo '<td align="center" valign="bottom" style="border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;border-bottom-style:hidden;">'.$value.'</td> ';
		}
		echo '</tr></table>';
	}
	else
	{
		echo '<table width="'.$width.'">
		<tr height="90" style="font-size:18px; font-family:Arial; font-weight:bold">
		 ';
		 
		echo '</tr></table>';
	}
}


?>