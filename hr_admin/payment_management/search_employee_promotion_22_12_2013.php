<?php 
session_start();

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


	$search_string=$_GET["m_company"];
	$m_buyer=$_GET["m_buyer"];

include('../../includes/common.php');
include('../../includes/array_function.php');

extract($_REQUEST);
$periods=explode("_",$cbo_month_selector);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="includes/functions.js"></script>
<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>

<link href="../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../../includes/tablefilter.js"></script>

<script>

//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


function search_populate(str)
{
	 //alert(str);
	if(str==0)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Employee Code";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==1)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Employee Name";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==2)
	{
		var designation = '<option value="0">--- Select ---</option>';
		<?php
		$designation_sql= mysql_db_query($DB, "select * from lib_designation where is_deleted=0  and status_active=1 order by level ");
		while ($row=mysql_fetch_array($designation_sql))
		{
			echo "designation += '<option value=\"$row[id]\">".mysql_real_escape_string($row[custom_designation])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Designation";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ designation +'</select>'; 
	}	
	else if(str==3)
	{
		var division = '<option value="0">--- Select ---</option>';
		<?php
		$division_sql= mysql_db_query($DB, "select * from lib_division where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($division_sql))
		{
			echo "division += '<option value=\"$row[id]\">".mysql_real_escape_string($row[division_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Division";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ division +'</select>'; 
	}	
	else if(str==4)
	{
		var department = '<option value="0">--- Select ---</option>';
		<?php
		$department_sql= mysql_db_query($DB, "select * from lib_department where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($department_sql))
		{
			echo "department += '<option value=\"$row[id]\">".mysql_real_escape_string($row[department_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Department";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ department +'</select>'; 
	}	
   else if(str==5)
	{
		var section = '<option value="0">--- Select ---</option>';
		<?php
		$section_sql= mysql_db_query($DB, "select * from lib_section where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($section_sql))
		{
			echo "section += '<option value=\"$row[id]\">".mysql_real_escape_string($row[section_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Section";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ section +'</select>'; 
	}
	else if(str==6)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Employee ID";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==7)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Punch Card No";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" />';
	}
	
	else if(str==8)
	{
		var designation_level = '<option value="0">--- Select ---</option>';
		<?php
		$designation_sql= mysql_db_query($DB, "select * from lib_designation where is_deleted=0  and status_active=1 order by level ");
		while ($row=mysql_fetch_array($designation_sql))
		{
			echo "designation_level += '<option value=\"$row[id]\">".mysql_real_escape_string($row[system_designation])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Designation Level";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:200px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ designation_level +'</select>'; 
	}	
}

function fn_list_showResult_search()
	{ 
		var company=document.getElementById('cbo_company_name').value;
		var cbo_category_name=document.getElementById('cbo_category_name').value;
		var search_by=document.getElementById('cbo_search_by').value;
		var param=company+"_"+cbo_category_name+"_"+search_by;
		var search_field=document.getElementById('txt_search_common').value;
		//alert(param);
		showResult_search_new(search_field,param,'search_emp_promotion','show_result_div');
	}
/*function showResult_search2(search_field,param,type,div)
{
	alert(type);
}*/
/*function showResult_search2(str,str2,type,div)
{
alert(type);
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  	xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  	if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
		
    	document.getElementById(div).innerHTML=xmlhttp.responseText;   
    }
  }
  
xmlhttp.open("GET","includes/list_view.php?search_string="+str+"&search_string2="+str2+"&type="+type,true);
xmlhttp.send();

}*/

function js_set_value(param)
{	
	var split_param=param.split("_");
	document.getElementById('hidden_emp_code').value=split_param[0];
	document.getElementById('hidden_name').value=split_param[1];
	document.getElementById('hidden_division').value=split_param[2];
	document.getElementById('hidden_section').value=split_param[3];
	document.getElementById('hidden_subsection').value=split_param[4];
	document.getElementById('hidden_department').value=split_param[5];
	document.getElementById('hidden_designation').value=split_param[6];
	document.getElementById('hidden_company').value=split_param[7];
	document.getElementById('hidden_joining_date').value=split_param[8];
	document.getElementById('hidden_designation_level').value=split_param[9];
	//alert(split_param[9]);
	parent.emailwindow.hide();
}
</script>

</head>

<body>
<div align="center">
<form name="search_order_frm"  id="search_order_frm" autocomplete="off" >
	<fieldset style="width:930px">
	<table class="rpt_table" width="750" cellspacing="2" cellpadding="0" border="0">
		<thead>
		  <th>Company&nbsp; </th>
          <th>Category</th>
          <th>Search By</th>
		  <th id="search_by_td_up"> Employee Code</th>
          <th><input type="reset" id="reset" value="Reset" class="formbutton" style="width:100px" /></th>		          
		</thead>
        <tr class="general">
          <td width="150" align="center">
              <select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:170px">
                <? if($company_cond=="")
                    { 
                ?>
                <option value="0">-- Select --</option>
                <?  }
                        $sql= mysql_db_query($DB, "select * from lib_company where status_active=1 and is_deleted=0 $company_cond order by company_name");
                        while ($selectResult = mysql_fetch_array($sql))
                            {
                        ?>
                <option value="<?php echo $selectResult["id"]; ?>"><?php echo $selectResult["company_name"]; ?></option>
                <?
                            }
                        ?>
              </select>
          </td>        
          <td width="150" align="center">
              <select name="cbo_category_name" id="cbo_category_name" class="combo_boxes" style="width:170px">
                <option value="0">-- Select --</option>
                <?
                            foreach($employee_category as $key=>$val)
                            {
                        ?>
                <option value="<? echo $key; ?>" ><? echo $val; ?></option>
                <?
                            }
                        ?>
              </select>
          </td>
           <td width="434">				
			<select name="cbo_search_by" id="cbo_search_by" class="combo_boxes" style="width:150px" onchange="search_populate(this.value)">
               
                <option value="0">Employee Code</option>
                <option value="1">Employee Name</option>
                <option value="2">Designation</option>
                <option value="3">Division</option>
                <option value="4">Department</option>
                <option value="5">Section</option>
                <option value="6">Employee ID</option>
                <option value="7">Punch Card</option>
                <option value="8">Designation Level</option>
              </select>					
		  </td>
          
          <td width="150" align="center" id="search_field_td_up"><input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" /></td>
          <td width="150" align="center"><input type="button" name="show_button" id="show_button" class="formbutton" value="Show" onclick="javascript:fn_list_showResult_search()" style="width:100px;"  /></td>
           <input type="hidden" name="hidden_emp_code" id="hidden_emp_code" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_name" id="hidden_name" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_division" id="hidden_division" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_section" id="hidden_section" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_subsection" id="hidden_subsection" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_department" id="hidden_department" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_designation" id="hidden_designation" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_company" id="hidden_company" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_joining_date" id="hidden_joining_date" value="" /><!-- Hidden field -->
           <input type="hidden" name="hidden_designation_level" id="hidden_designation_level" value="" /><!-- Hidden field -->
		</tr>
   </table>   
    <br />
    <div id="show_result_div" ></div>  
	</fieldset>
	</form>    
</div>
</body>

<?
						
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>