<?php
	/***************************************
	|	developed By:  Md. Nuruzzaman
	|	Date of Development : 15.02.2014
	***************************************/
	
	session_start();
	
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	extract($_GET);
	
	if ($_SESSION['logic_erp']["data_level_secured"]==1) 
	{
		if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
		if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
	}
	else{ $buyer_name="";$company_name="";}
	
	//echo $company_cond."xcz";
	//---------------------------------------------------------------------------------------------------------
	include('../../includes/common.php');
	include('../../includes/array_function.php');
	
	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string( $row['company_name'] );
	}
	
	//designation_chart
	$sql = "SELECT * FROM lib_designation WHERE  status_active=1 and is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
			$designation_details[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );
	}
	
	//section_details
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
			$section_details[$row['id']] = mysql_real_escape_string( $row['section_name'] );
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="includes/functions.js" type="text/javascript"></script>
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" type="text/css"  rel="stylesheet" media="screen">
        <script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
       	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
         
        <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script src="../../includes/update_status.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../js/modal.js"></script>
        <script type="text/javascript" src="includes/ajax_submit_emp_promotion.js"></script>
        <script type="text/javascript" src="includes/functions.js"></script>
        
		<script type="text/javascript">
			$(document).ready(function(){
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
				//populate_data();
			});
			
			// autocomplete of process/operation
			<?
			$sql= mysql_db_query($DB,"select * from  hrm_process_mst where is_deleted=0 and status_active=1 group by process_name");
			while ($row=mysql_fetch_array($sql)) {
				$process_name_dtls.= '"'.mysql_real_escape_string($row['process_name']).'",';
				$process_name_array[$row['process_name']]['id']=$row['id'];
				$process_name_array[$row['process_name']]['process_name']=$row['process_name'];
			}
			$process_name_arr=json_encode($process_name_array);
			echo "var valid_process_name = ". $process_name_arr . ";\n";
			
			$sql= mysql_db_query($DB,"SELECT * FROM hrm_process_rate_dtls ");
			while ($row=mysql_fetch_array($sql)) {
				$process_name_arrayss[$row['process_id']][$row['buyer_id']]['uom']=$row['uom'];
				$process_name_arrayss[$row['process_id']][$row['buyer_id']]['rate']=$row['rate'];
			}
			$process_name_arrss=json_encode($process_name_arrayss);
			echo "var valid_process_rate = ". $process_name_arrss . ";\n";
			
			//variable_settings_piece_rate
			$sql= mysql_db_query($DB,"SELECT subsidy_for,company_name FROM variable_settings_piece_rate ");
			while ($row=mysql_fetch_array($sql)) {
				$sub=explode("**",$row['subsidy_for']);
				for($i=0; $i<count($sub); $i++)
				{
					$subval=explode("_",$sub[$i]);
					$subsidy_array[$row['company_name']][$subval[0]]=$subval[1];
				}
			}
			$subsidy_arrayss=json_encode($subsidy_array);
			echo "var subsidy_array = ". $subsidy_arrayss . ";\n";
				
			?>
			
			$(function() {
				var process_name = [<? echo substr($process_name_dtls, 0, -1); ?>];
				$("#txt_operation").autocomplete({
					source: process_name 
				});
			});
			
			function subsidy_values( type )
			{
				if($('#cbo_company_id').val()==0)
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_company_id').focus();
						$(this).html('Please select company').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(type==0)
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_prod_type').focus();
						$(this).html('Please select prod. type').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				//alert(subsidy_array[$('#cbo_company_id').val()][type]);
				document.getElementById('txt_subsidy_percentage').value=subsidy_array[$('#cbo_company_id').val()][type];
				change_product_type();
			}
			// autocomplete of buyer
			<?
			$sql= mysql_db_query($DB,"select id,buyer_name from  lib_buyer where is_deleted=0 and status_active=1 order by buyer_name");
			//$buyer_id="";
			while ($row=mysql_fetch_array($sql)){
				//$buyer_id.=$row['id'];
				$buyer_name_dtls.= '"'.mysql_real_escape_string($row['buyer_name']).'",';
				$buyer_name_array[$row['buyer_name']]['id']=$row['id'];
				$buyer_name_array[$row['buyer_name']]['buyer_name']=$row['buyer_name'];
			}
			$buyer_name_arr= json_encode($buyer_name_array);
			echo "var valid_buyer_name = ". $buyer_name_arr . ";\n";
			?>
			$(function() {
				var buyer_name = [<? echo substr($buyer_name_dtls, 0, -1); ?>];
				$("#txt_buyer").autocomplete({
					source: buyer_name 
				});
			});
			
			// autocomplete of id_card_no
			<?
			$sql= mysql_db_query($DB,"select emp_code,id_card_no,CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name,company_id,designation_id,section_id from  hrm_employee where is_deleted=0 and status_active=1 order by id_card_no ASC");
			while ($row=mysql_fetch_array($sql)) {
				$id_card_no_dtls.= '"'.mysql_real_escape_string($row['id_card_no']).'",';
				$emp_code_array[$row['id_card_no']]['id_card']=$row['id_card_no'];
				$emp_code_array[$row['id_card_no']]['emp_code']=$row['emp_code'];
				$emp_code_array[$row['id_card_no']]['name']=$row['name'];
				$emp_code_array[$row['id_card_no']]['designation_id']=$designation_details[$row['designation_id']];
				$emp_code_array[$row['id_card_no']]['section_id']=$section_details[$row['section_id']];
			}
			$emp_code_arr= json_encode($emp_code_array);
			echo "var emp_valid_id_card = ". $emp_code_arr . ";\n";
			?>
			$(function() {
				var id_card_no = [<? echo substr($id_card_no_dtls, 0, -1); ?>];
				$("#txt_id_card_no").autocomplete({
					source: id_card_no 
				});
			});
			
			// ID Card No
			function key_id_card_no(e)
			{
				if ((e.which || e.keyCode) == 9) 
				{
					// tab key was pressed, do stuff
					e.preventDefault();
					var company_id=$('#cbo_company_id').val();
					var prod_date=$('#txt_prod_date').val();
					var prod_type=$('#cbo_prod_type').val();
					var id_card_no=$('#txt_id_card_no').val();
					
					if( $('#txt_id_card_no').val()=="")
					{
						alert("ID Card No Not Found");
						$('#txt_id_card_no').focus();
						return false;
					}
					else if(company_id==0)
					{
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#cbo_company_id').focus();
							$(this).html('Please select company').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
					else if(prod_date=="")
					{
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#txt_prod_date').focus();
							$(this).html('Please enter prod. date').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
					else if(prod_type==0)
					{
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#cbo_prod_type').focus();
							$(this).html('Please select prod. type').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
					else
					{
						if( !emp_valid_id_card[id_card_no] )
						{
							alert("ID Card No Not Found");
							$('#txt_id_card_no').focus();
							$('#txt_id_card_no').val('');
							$('#txt_emp_code').val('');
							$('#txt_name').val('');
							$('#txt_designation').val('');
							$('#txt_section').val('');
							return false;
						}
						else
						{
							//alert(emp_valid_id_card[id_card_no]['emp_code']);
							$('#txt_name').val(emp_valid_id_card[id_card_no]['name']);
							$('#txt_designation').val(emp_valid_id_card[id_card_no]['designation_id']);
							$('#txt_section').val(emp_valid_id_card[id_card_no]['section_id']);
							$('#txt_emp_code').val(emp_valid_id_card[id_card_no]['emp_code']);
							$('#txt_buyer').focus();
						}
					}
				}
			}
			
			// buyer
			function key_buyer(e)
			{
				if ((e.which || e.keyCode) == 9) 
				{
				  // tab key was pressed, do stuff
				 	e.preventDefault();
				  	var id_card_no=$('#txt_id_card_no').val();
					var buyer_name=$('#txt_buyer').val();
					if(id_card_no=='')
					{
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#txt_id_card_no').focus();
							$(this).html('Please enter ID Card No').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						$('#txt_buyer').val('');
					}
					else
					{
						if( !valid_buyer_name[buyer_name] )
						{
							alert("Buyer Not Found");
							$('#txt_buyer').val('');
							$('#txt_buyer_id').val('');
							$('#txt_buyer').focus();
							return false;
						}
						else
						{
							$('#txt_buyer').val(valid_buyer_name[buyer_name]['buyer_name']);
							$('#txt_buyer_id').val(valid_buyer_name[buyer_name]['id']);
							$('#txt_order_no').focus();
						}
					}
				}				
			}
			
			// process/operation
			function key_process(e)
			{
				if ((e.which || e.keyCode) == 9) 
				{
					// tab key was pressed, do stuff
					e.preventDefault();
					var buyer_name=$('#txt_buyer').val();
					var process_name=$('#txt_operation').val();
					if(buyer_name=='')
					{
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#txt_buyer').focus();
							$(this).html('Please enter buyer name').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						$('#txt_operation').val('');
					}
					else
					{ 
						if( !valid_process_name[process_name] )
						{
							alert("Process Not Found");
							$('#txt_operation').val('');
							$('#txt_operation_id').val('');
							$('#txt_operation').focus();
							return false;
						}
						else
						{ //valid_process_rate
						
							$('#txt_operation').val(valid_process_name[process_name]['process_name']);
							$('#txt_operation_id').val(valid_process_name[process_name]['id']);
							$('#txt_prod_qty').focus();
							
							var buyer_id=$('#txt_buyer_id').val();
							var process_id=$('#txt_operation_id').val();
							if( valid_process_rate[process_id][buyer_id] )
							{
								if( valid_process_rate[process_id][buyer_id]['uom']==1)//dzn
								{
									document.getElementById('original_uom').innerHTML="Dzn";
									document.getElementById('original_rate').innerHTML=valid_process_rate[process_id][buyer_id]['rate'];
									$('#txt_rate').val( ( valid_process_rate[process_id][buyer_id]['rate']/12 ).toFixed(4) );
									$('#txt_rate_original').val( ( valid_process_rate[process_id][buyer_id]['rate']/12 ).toFixed(4) );
								}
								else
								{
									document.getElementById('original_uom').innerHTML="Pcs";
									document.getElementById('original_rate').innerHTML=valid_process_rate[process_id][buyer_id]['rate'];
									$('#txt_rate').val(valid_process_rate[process_id][buyer_id]['rate']);
									$('#txt_rate_original').val(valid_process_rate[process_id][buyer_id]['rate']);
								}
							}
						}
					}
				}
			}
			
			// prod_qty
			function key_prod_qty(e)
			{
				if ((e.which || e.keyCode) == 9) 
				{
				  // tab key was pressed, do stuff
				  	e.preventDefault();
					var process_name=$('#txt_operation').val();
					if(process_name=='')
					{
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#txt_operation').focus();
							$(this).html('Please enter process name').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						$('#txt_prod_qty').val('');
						$('#txt_operation').focus();
					}
					else
					{
						var amount=($('#txt_rate').val()*$('#txt_prod_qty').val()).toFixed(4);
						$('#txt_amount').val(amount);
						$('#btn_save').focus();
					}
				}
			}
			
			// ajax object create
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
			
			// save
			function fnc_piece_rate_production_entry_save()
			{
				//alert("su..re");
				var company_id		= $('#cbo_company_id').val();
				var prod_date		= $('#txt_prod_date').val();
				var prod_type		= $('#cbo_prod_type').val();
				var id_card_no		= $('#txt_id_card_no').val();
				var buyer_name		= $('#txt_buyer').val();
				var order_no		= $('#txt_order_no').val();
				var process_name	= $('#txt_operation').val();
				var prod_qty		= $('#txt_prod_qty').val();
				//var uom				= $('#txt_uom').val();
				var uom				= 2;
				var rate			= $('#txt_rate').val();
				var rate_original	= $('#txt_rate_original').val();
				var amount			= $('#txt_amount').val();
				var update_id		= $('#update_id').val();
				var emp_code		= $('#txt_emp_code').val();
				var buyer_id		= $('#txt_buyer_id').val();
				var process_id		= $('#txt_operation_id').val();
				
				var subsidy_percentage=$('#txt_subsidy_percentage').val();
				var subsidy_amount=(amount*subsidy_percentage)/100;
				
				if(company_id==0)
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_company_id').focus();
						$(this).html('Please select company').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(prod_date=="")
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_prod_date').focus();
						$(this).html('Please enter prod. date').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(prod_type==0)
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_prod_type').focus();
						$(this).html('Please select prod. type').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(id_card_no=='')
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_id_card_no').focus();
						$(this).html('Please enter ID Card No').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(buyer_name=='')
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_buyer').focus();
						$(this).html('Please enter buyer name').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(order_no=='')
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_order_no').focus();
						$(this).html('Please enter order no').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(process_name=='')
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_operation').focus();
						$(this).html('Please enter process name').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if(prod_qty=='')
				{
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_prod_qty').focus();
						$(this).html('Please enter prod qty').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else
				{
					http.open('GET','includes/save_update_payment.php?action=piece_rate_production_entry&company_id='+company_id+
								'&prod_date='+ prod_date+
								'&prod_type='+ prod_type+
								'&id_card_no='+ id_card_no+
								'&buyer_name='+ buyer_name +
								'&order_no='+ order_no+
								'&process_name='+ process_name+
								'&prod_qty='+prod_qty+
								'&uom='+uom+
								'&rate='+ rate+
								'&rate_original='+rate_original+
								'&amount='+amount+
								'&update_id='+update_id+
								'&emp_code='+ emp_code+
								'&buyer_id='+buyer_id+
								'&process_id='+process_id+
								'&subsidy_percentage='+subsidy_percentage+
								'&subsidy_amount='+subsidy_amount);
								
					http.onreadystatechange = response_piece_rate_production_entry;
					http.send(null);
				}
			}
			function response_piece_rate_production_entry() 
			{
				if(http.readyState == 4) 
				{   
					//alert(http.responseText)
					var response = http.responseText;
					if( response == 1 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Inserted successfully.').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						populate_data();
						frm_refresh(0);
					}
					else if( response == 2 ) {
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						populate_data();
						frm_refresh(0);
					}
				}
			}
			
			// delete
			function fnc_piece_rate_production_entry_delete()
			{
				//alert("su..re");
				prompt("Are You Sure Want To Delete");
				var update_id = $('#update_id').val();
				http.open('GET','includes/save_update_payment.php?action=piece_rate_production_entry_delete&update_id='+update_id);
				http.onreadystatechange = response_piece_rate_production_entry_delete;
				http.send(null);
			}
			function response_piece_rate_production_entry_delete() 
			{
				if(http.readyState == 4) 
				{   
					//alert(http.responseText)
					var response = http.responseText;
					if( response == 3 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Deleted successfully.').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						populate_data();
						frm_refresh(0);
					}
				}
			}
	
			// frm_reset
			function frm_refresh( frm )
			{
				//alert("su..re");
				if( frm==1 )
				{
					$('#cbo_company_id').focus();
					$('#cbo_company_id').val(0);
					$('#txt_prod_date').val('');
					$('#cbo_prod_type').val(0);
					$('#txt_order_no').val('');
					populate_data();
				}
				else if(frm==2)
				{
					$('#txt_order_no').val('');
				}
				else $('#txt_id_card_no').focus();
				
				$('#txt_id_card_no').val('');
				$('#txt_name').val('');
				$('#txt_designation').val('');
				$('#txt_section').val('');
				$('#txt_buyer').val('');
				//$('#txt_order_no').val('');
				$('#txt_operation').val('');
				$('#txt_prod_qty').val('');
				//$('#txt_uom').val('');
				$('#txt_rate').val('');
				$('#txt_rate_original').val('');
				$('#txt_amount').val('');
				$('#update_id').val('');
				$('#txt_emp_code').val('');
				$('#txt_buyer_id').val('');
				$('#txt_operation_id').val('');
				//$('#original_uom').text('');
				//$('#original_rate').text('');
				document.getElementById('original_uom').innerHTML="&nbsp;";
				document.getElementById('original_rate').innerHTML="&nbsp;";
			}
			
			function change_product_type()
			{
				frm_refresh(2);
				populate_data();
			}
			
			// list view populate
			function populate_data() 
			{
				var company_id	= $('#cbo_company_id').val();
				var prod_date	= $('#txt_prod_date').val();
				var prod_type	= $('#cbo_prod_type').val();
				
				http.open('GET','includes/list_view.php?type=piece_rate_production_entry_print&company_id='+ company_id + '&prod_date='+ prod_date +'&prod_type='+prod_type);
				http.onreadystatechange = response_piece_rate_production_entry_print;
				http.send(null);			
				function response_piece_rate_production_entry_print()
				{
					if(http.readyState == 4) 
					{   
						//alert(http.responseText)
						var response=http.responseText.split('####');
						$('#data_panel2').html( response[0] );
					}
				}
				
				$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
				$.ajax({
					type: "POST",
					url: "includes/list_view.php",
					data: 'type=frm_piece_rate_production_entry&company_id='+ company_id + '&prod_date='+ prod_date +'&prod_type='+prod_type,
					success: function(data) {
						$('#data_panel').html( data );
						$('#tbl_piece_rate_production_entry').dataTable({
							"bRetrieve": true,
							"bDestroy": true,
							"bJQueryUI": true,
							"bPaginate": false,
							 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
							//"aaSorting": [[ 0, "asc" ]],*/
							"oLanguage": { "sSearch": "Search all columns:" },
							"sScrollX": "100%",
							"sScrollY": "200px",
							"sScrollXInner": "100%",
							"bScrollCollapse": true
						});
					}
				});
			}
			
			// print
			/*function fnc_piece_rate_production_entry_print(company_id,prod_date,prod_type)
			{
				//alert("su..re");
				http.open('GET','includes/list_view.php?type=piece_rate_production_entry_print&company_id='+ company_id + '&prod_date='+ prod_date +'&prod_type='+prod_type);
				http.onreadystatechange = response_piece_rate_production_entry_print;
				http.send(null);			
			}
			function response_piece_rate_production_entry_print()
			{
				if(http.readyState == 4) 
				{   
					//alert(http.responseText)
					var response=http.responseText.split('####');
					$('#data_panel2').html( response[0] );
				}
			}*/
			
			function new_window_short()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel2').innerHTML);
				d.close();
			}
			// js_set_value
			function js_set_value(id,buyer_id,process_id)
			{
				//alert(id);
				var update_id=$('#update_id').val(id);
				$.ajax({
					type: "POST",
					url: "includes/get_data_update.php",
					data: 'link=piece_rate_production_entry_set_value&id='+id+'&buyer_id='+ buyer_id+'&process_id='+ process_id,
					success: function(data) {
						eval(data);
					}
				});
			}
			
        </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body>
            <div align="center" style="width:1300px">
            	<div style="height:40px;" align="center">
                    <div class="form_caption">Piece Rate Production Entry</div>
                    <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
                </div>
                <div align="center">
                <form name="frm_piece_rate_production_entry" id="frm_piece_rate_production_entry" autocomplete="off">
                <fieldset style="width:1200px;"><legend>Piece Rate Production</legend>
                	<table width="460" border="0" cellpadding="1" cellspacing="1" class="rpt_table" align="center">
                    	<thead>
                        	<th width="160">Company</th>
                            <th width="150">Production Date</th>
                            <th width="150">Production Type</th>
                        </thead>
                        <tbody>
                        	<td>
                            	<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:160px;">
                                	<option value="0">--- select ---</option>
                                    <?php
									foreach($company_details as $key=>$value)
									{
									?>
										<option value="<?php echo $key;?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value;?></option>		
									<?php
                                    }
									?>
                                </select>
                            </td>
                            <td>
                            	<input type="text" name="txt_prod_date" id="txt_prod_date" class="datepicker" style="width:150px;" />
                            </td>
                            <td>
                            	<select name="cbo_prod_type" id="cbo_prod_type" class="combo_boxes" onchange="subsidy_values(this.value);" style="width:150px;">
                                    <?php
									foreach($sewing_process_rate_arr as $key=>$value)
									{
									?>
										<option value="<?php echo $key;?>"><?php echo $value;?></option>		
									<?php
                                    }
									?>
                                </select>
                            </td>
                        </tbody>
                    </table>
                    <div style="height:10px;"></div>
                	<table width="1140" border="0" cellpadding="1" cellspacing="1" class="rpt_table" align="center">
                    	<thead style="height:35px;">
                        	<tr style="height:8px;">
                                <th width="100" rowspan="4">ID Card</th>
                                <th width="150" rowspan="4">Name</th>
                                <th width="120" rowspan="4">Designation</th>
                                <th width="120" rowspan="4">Section</th>
                                <th width="120" rowspan="4">Buyer</th>
                                <th width="100" rowspan="4">Order No</th>
                                <th width="120" rowspan="4">Process/Operation</th>
                                <th width="70" rowspan="4">Prod. Qty</th>
                                <th width="70">UOM</th>
                                <th width="70">Rate</th>
                                <th width="100" rowspan="4">Amount</th>
                        	</tr>
                            <tr style="height:8px;"> 
                            	<th width="70" colspan="2">Original</th>
                            </tr> 
                            <tr style="height:8px;">
                            	<th width="70" id="original_uom">&nbsp;</th>
                                <th width="70" id="original_rate">&nbsp;</th> 
                            </tr>
                            <tr style="height:8px;">
                            	<th width="70" colspan="2">Converted</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                	<input type="text" name="txt_id_card_no" id="txt_id_card_no" style="width:100px;"  onkeypress="key_id_card_no(event);" />
                                    <input type="hidden" name="txt_emp_code" id="txt_emp_code" style="width:100px;" />
                                </td>
                                <td><input type="text" name="txt_name" id="txt_name" style="width:150px;" /></td>
                                <td><input type="text" name="txt_designation" id="txt_designation" style="width:120px;" /></td>
                                <td><input type="text" name="txt_section" id="txt_section" style="width:120px;" /></td>
                                <td>
                                	<input type="text" name="txt_buyer" id="txt_buyer" style="width:120px;"  onkeypress="key_buyer(event);" />
                                    <input type="hidden" name="txt_buyer_id" id="txt_buyer_id" style="width:120px;" />
                                </td>
                                <td><input type="text" name="txt_order_no" id="txt_order_no" style="width:100px;" /></td>
                                <td>
                                	<input type="text" name="txt_operation" id="txt_operation" style="width:120px;"  onkeypress="key_process(event);" />
                                    <input type="hidden" name="txt_operation_id" id="txt_operation_id" style="width:120px;" />
                                </td>
                                <td><input type="text" name="txt_prod_qty" id="txt_prod_qty" style="width:70px;"  onkeypress="key_prod_qty(event);" /></td>
                                <td><input type="text" name="txt_uom" id="txt_uom" readonly="readonly" value="Pcs" style="width:70px; text-align:center;" /></td>
                                <td>
                                    <input type="text" name="txt_rate" id="txt_rate" style="width:70px;" />
                                    <input type="hidden" name="txt_rate_original" id="txt_rate_original" readonly="readonly" style="width:70px;" />
                                </td>
                                <td>
                                	<input type="text" name="txt_amount" id="txt_amount" readonly="readonly" style="width:100px;" />
                                    <input type="hidden" name="txt_subsidy_percentage" id="txt_subsidy_percentage" readonly="readonly" style=" width:100px;" />
                                </td>
                            </tr>
                    	</tbody>
                    </table>
                    <div class="button_container" style="margin-top:10px;">
                    <input type="button" name="btn_save" id="btn_save" value="Save" class="formbutton"   style="width:100px;" onclick="javascript:fnc_piece_rate_production_entry_save();"/>&nbsp;
                    <input type="button" name="refresh" id="refresh" value="Refresh" class="formbutton" onclick="frm_refresh(1);" style="width:100px;"/>&nbsp;
                    <input type="button" name="delete" id="delete" value="Delete" class="formbutton"   onclick="javascript:fnc_piece_rate_production_entry_delete();"   style="width:100px;" />
                    <input type="button" onclick="new_window_short()" value="Print" name="Print" class="formbutton" style="width:100px"/>
                    <input type="hidden" id="update_id" name="update_id" />
                    </div>
                </fieldset> 
                </form> 
                </div>
                <fieldset style="width:1260px;" >
                	<div id="data_panel3"></div>
                    <div id="data_panel2" class="demo_jui" align="center" style="width:1150px; margin-top:10px; display:none;"></div>
                    <div id="data_panel" class="demo_jui" align="center" style="width:1150px; margin-top:10px;"></div>
                </fieldset>
            </div>
    </body>
</html>