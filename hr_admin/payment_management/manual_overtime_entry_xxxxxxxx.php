<html>
<head>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="includes/ajax_submit.js"></script>
	<script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../js/tooltip.js"></script>
	<script type="text/javascript" src="../includes/functions.js"></script>
	<script type="text/javascript" src="autocomplete/lib/prototype/prototype.js"></script>
	<script type="text/javascript" src="autocomplete/lib/scriptaculous/scriptaculous.js"></script> 
	<script type="text/javascript" src="autocomplete/src/autocomplete.js"></script>
 	<script type="text/javascript">
	</script>
	<link rel="stylesheet" type="text/css" media="all" href="../jsDatePick_ltr.min.css" />
	<script type="text/javascript" src="../jsDatePick.min.1.3.js"></script>
	<script type="text/javascript">
	 	function jsdate(fld){
			new JsDatePick({
							useMode:2,
							target:fld,
							dateFormat:"%d-%m-%Y"
							});
							};
	//	alert(document.getElementById('lastname').value)
	</script>
</head>

<?php
date_default_timezone_set('UTC');
include('../includes/common.php');
?>
<body onLoad="document.onkeydown = checkKeycode;">	
	<form name="manual_overtime_entry.php" action="" method="post">	
	<div align="center" style="width:900px">
		<label class="form_header"> Manual Overtime Entry </label>
	</div>
	<div align="center" style="width:900px">
		<fieldset style="width:800px">
			<legend>Manual Overtime Entry </legend>
			<table width="100%" cellspacing="0">
				<tr bgcolor="#CCCCCC">
					<td height="15" width="180" valign="top" align="center">Company Name </td>
					<td width="150" valign="top" align="center">Section </td>
					<td width="150" valign="top" align="center">Department </td>
					<td width="150" valign="top" align="center">Designation </td>
					<td width="100" valign="top" align="center">From </td>
					<td width="100" valign="top" align="center">To </td>
					
					<td>
						<input type="submit" value="   Populate   " name="save" id="save" class="formbutton" />
					</td>
				</tr>
				<tr>
					<td>
						<select name="company_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="section_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="department_name" id="select" style="width:170px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<select name="designation" id="select" style="width:130px" class="combo_boxes">
							<option value="0">--i--</option>
							<option value="0">--ii--</option>
						</select>
					</td>
					<td>
						<input name="from_date" id="from_date" class="text_boxes" style="width:130px"/>
					</td>
					<td>
						<input name="to_date" id="to_date" class="text_boxes" style="width:130px"/>
					</td>
				</tr>
				<tr>	
					<td colspan="7" height="20">
					<hr>
					</td>
				</tr>
			</table>
			
			<table>
				<tr>
					<td width="200" align="center">Shift Name</td>
					<td width="200" align="center">OT hour Per Shift </td>
				</tr>
				<tr>
					
					<td width="200" align="center">
						<select name="shift_name" id="select" style="width:130px" class="combo_boxes">
							<option value="0">All</option>
							<option value="0">Shift 1</option>
							<option value="0">Shift 2</option>
							<option value="0">Shift 3</option>
						</select>
					</td>
					<td width="200" align="center"><input name="ot_hour_oer_shift" id="ot_hour_oer_shift" class="text_boxes" style="width:130px"/></td>
				</tr>
			</table>
		</fieldset>
	</div>
	
	<div align="center" style="width:950px">
		<fieldset style="width:900px">
			
		</fieldset>
	</div>
</form>
</body>
</html>




