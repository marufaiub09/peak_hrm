<? 
/*########################
Completed by: Sohel
Dated: 25/02/2014
#########################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../includes/common.php');
include('../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
	
	
$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}
	
	/*$sql_res = "SELECT id,attnd_date FROM hrm_attendance WHERE status in('GH','FH','CH','W') group by attnd_date ORDER BY attnd_date ASC";
	$result = mysql_query( $sql_res ) or die( $sql_res . "<br />" . mysql_error() );
	
	$offdate_date = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$offdate_date[$row['attnd_date']] = mysql_real_escape_string( $row['attnd_date'] );		
	}
	*/
/*
if($type=="offday_date_generate")
{		
	$exp_date=explode("_",$id);
	$from_date=$exp_date[0];
	$to_date=$exp_date[1];
	$sql = "SELECT attnd_date FROM hrm_attendance WHERE attnd_date between '$from_date' and '$to_date' and status in('GH','FH','CH','W') group by attnd_date ORDER BY attnd_date ASC  ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$offdate_date = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$offdate_date[$row['attnd_date']] = mysql_real_escape_string( $row['attnd_date'] );		
	}
	
	//print_r($offdaydate_details);
	exit();
}

print_r($offdate_date);
	*/
	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
    
   
	<script src="../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
	<script src="../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
    <link href="../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    
    <!--<link href="../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> -->
   <!-- <script type="text/javascript" src="../../includes/tablefilter.js"></script>-->
    
	
	<script type="text/javascript" charset="utf-8">
	
	var save_perm = <? echo $permission[0]; ?>;
	var edit_perm = <? echo $permission[1]; ?>;
	var delete_perm = <? echo $permission[2]; ?>;
	var approve_perm = <? echo $permission[3]; ?>;

	var location_details = division_details = department_details = section_details =emp_code_details=policy_details=id_details= new Array();
	
	
	$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
		//header: false, 
		selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
	
	});

		function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/generate_daily_ofday_ot_payment.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_month_selector').html( html )
				}
			});
		}
		
		function populate_offday_date( selected_date ) {
			$.ajax({
				type: "GET",
				url: "includes/generate_daily_ofday_ot_payment.php?",
				data: 'type=offday_date_generate&id=' + selected_date,
				success: function( html ) {
					$('#offdate_selector').html( html )
				}
			
			});
			
			/*$(document).ready(function() {
			$("select").multiselect({
			selectedText: "# of # selected"
			});
			var hidValue = $("#offdate_selector").val();
			alert(hidValue);
			var selectedOptions = hidValue.split(",");
			for(var i in selectedOptions) {
			var optionVal = selectedOptions[i];
			$("select").find("option[value="+optionVal+"]").prop("selected", "selected");
			}
			$("select").multiselect('refresh');
			});*/
			
			
			/*$(function() {
				$('#fruits').change(function() {
				console.log($(this).val());
				}); 
			});*/
		}
		
	function openmypage_employee_info()
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}
			
	function openmypage_employee_info_id_card()
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information' , 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
			$('#id_card').val(thee_id.value);
		}
	}
		
		
	function openmypage_offday_date(page_link,title)
	{
		var cbo_month_selector		= $('#cbo_month_selector').val();
		if(cbo_month_selector==0)
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_month_selector').focus();
				$(this).html('Please Select Month').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else
		{	
			var data=document.getElementById('cbo_month_selector').value;
		   page_link='../ofday_ot_payment_popup.php?action=ot_payment_popup&data='+data
					
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=350px,height=350px,center=1,resize=0,scrolling=0','../')			
			emailwindow.onclose=function()
			{
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#offdate_selector').val(thee_id.value);
				var date_split = thee_id.value.split(",");
				var tot_row=date_split.length;
				var day_day='';
				for(var i=0; i<=tot_row; i++)
				{
					//alert (date_split[i].replace(/\'/g, ""));
					var day=date_split[i].replace(/\'/g, "").split("-");
					 //day_day +=day[2]+",";
					 if(day_day==''){day_day +=day[2];}else {day_day +=","+day[2];}
					 $('#offdate').val(day_day);
				}
				
				
			}
		}
	}			
//ajax submit starts here

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();





function fnc_daily_offday_ot_payment(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#offdate_selector').val() == 0) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#offdate_selector').focus();
			$(this).html('Please select offday Date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#company_id').val() == "" || $('#company_id').val() == 0) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_salary_based').val() == "") {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_salary_based').focus();
			$(this).html('Please select Salary Based.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
		/*$('#data_panel').html('<img src="../../resources/images/loading.gif" />');*/
		
							data = '&cbo_year_selector=' + $('#cbo_year_selector').val() 
							 + '&cbo_month_selector=' + $('#cbo_month_selector').val()
							 + '&offdate_selector=' + $('#offdate_selector').val() 
							 + '&cbo_emp_category=' + $('#cbo_emp_category').val()
							 +'&company_id=' + $('#company_id').val() 
							 + '&location_id=' + $('#location_id').val() 
							 + '&division_id=' + $('#division_id').val() 
							 + '&department_id=' + $('#department_id').val()
							 + '&section_id=' + $('#section_id').val() 
							 + '&subsection_id=' + $('#subsection_id').val() 
							 + '&designation_id=' + $('#designation_id').val()
							 + '&cbo_salary_based=' + $('#cbo_salary_based').val()
							 + '&group_by_id=' + $('#group_by_id').val()
							 + '&order_by_id=' + $('#order_by_id').val()
							 + '&id_card=' + $('#id_card').val()
							 + '&emp_code=' + $('#emp_code').val();
		
		//alert(data);
		nocache = Math.random();
		http.open( 'GET', 'includes/generate_daily_ofday_ot_payment.php?action=daily_ofday_ot_payment' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_daily_offday_ot_payment;
		http.send(null);
	}
	
}

	function response_fnc_daily_offday_ot_payment() {	
		if(http.readyState == 4) {		
		var response =http.responseText.split('####');
		//alert(response[1]);		
		/*$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );*/
		$('#data_panel2').html( response[0] );
		$('#data_panel3').html( response[2] );
		hide_search_panel();
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}		


function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel2').innerHTML);
	d.close();
}

function new_window_short()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel3').innerHTML);
	d.close();
}


function fnc_save_update_offday_ot(save_perm,edit_perm,delete_perm,approve_perm)
 {
	var tot_row=$('#ofday_ot_payment tr').length;
	var emp_code=""; var atten_date=""; var ot_hour=""; var ot_rate=""; var ot_amount="";
	for(var i=1; i<=tot_row; i++)
	{
		if(i>1)
		{
		 emp_code +="*";
		 atten_date +="*";
		 ot_hour +="*";
		 ot_rate +="*";
		 ot_amount +="*";
		}
		emp_code += $("#empcode_"+i).val(); 
		atten_date += $("#attendate_"+i).val(); 
		ot_hour += $("#othour_"+i).val(); 
		ot_rate += $("#otrate_"+i).val(); 
		ot_amount += $("#otamount_"+i).val(); 
	}
	//alert (ot_amount);
	 // return;
	
	
	  //$("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
	  $('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	  
	  if (edit_perm==2 || save_perm==2){
				$("#messagebox").removeClass().addClass('messagebox').text('You do not have save & update permission').fadeIn(1000);
	  }
		var data = 'type=save_update_offday_ot_payment&emp_code='+emp_code+'&atten_date='+atten_date+'&ot_hour='+ot_hour+'&ot_rate='+ot_rate+'&ot_amount='+ot_amount;
		//alert (data);return;
		
		http.open('POST','includes/generate_daily_ofday_ot_payment.php',true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", data.length);
		http.setRequestHeader("Connection", "close");
		http.onreadystatechange = response_fnc_save_update_offday_ot;
		http.send(data);

  }
 
//Manual Attendace Response ( Update )
function response_fnc_save_update_offday_ot() 
{
	if(http.readyState == 4)
	 {
	   //alert (http.responseText);
		//var response = http.responseText;
		var response =http.responseText.split('**');
		if( response[0] == 1 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Off Day Ot Payment inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
		}
		else if( response[0] == 2 ) 
		{
			//alert (response[1]);
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('OT Already Paid For'+response[1]).addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
		}

	}
}



</script>

<style type="text/css"> 
	td {overflow:hidden; }
	@media print {thead {display: table-header-group;}}
	@media print {tfoot {display: table-footer-group;}}
	.verticalText 
	{               
		writing-mode: tb-rl;
		filter: flipv fliph;
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
	}
	
	.datahighlight {
        background-color: #ffdc87 !important;
	}
</style>
 </head>
 <body style="font-family:verdana; font-size:11px;">
 <div align="left" style="width:100%;">
	<div align="center"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Daily Off Day OT Payment</h3>
   
	<form id="service_report_form" action="javascript:fnc_daily_offday_ot_payment()" autocomplete="off" method="POST"> 
    <div id="content_search_panel" > 
		<fieldset id="filter_panel">
			<table align="center" width="1120px" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <th width="140px"><strong>Category</strong></th>
					<th width="140px"><strong>Company</strong></th>
					<th width="140px"><strong>Location</strong></th>
                    <th width="140px"><strong>Division</strong></th>
					<th width="140px"><strong>Department</strong></th>
                    <th width="140px"><strong>Section</strong></th>
                    <th width="140px"><strong>SubSection</strong></th>
                    <th width="140px"><strong>Designation</strong></th>
				</thead>
                <tr class="general">					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td id="company">
                         <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td id="location">
						<select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
					</td>
                  <td id="division">
						<select name="division_id" id="division_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                          <option value="0">-- Select --</option>
                            <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                    </td>					
                    <td id="department">
                        <select name="department_id" id="department_id" class="combo_boxes" style="width:140px"  multiple="multiple">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select></td>
					
					 <td id="section">
						<select name="section_id" id="section_id" class="combo_boxes" style="width:140px"  multiple="multiple">
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                     <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px"  multiple="multiple">
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                </tr>
          </table>
          <div style="height:8px;"></div>
          <table align="center" width="1000" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
					<th><strong>Year</strong></th>
                	<th><strong>Month</strong></th>
                    <th><strong>Off Day Date</strong></th>
                    <th><strong>Salary Based</strong></th>
                   <th><strong>Group By</strong></th>
               		<th><strong>Order By</strong></th>
                    <th><strong>ID Card No</strong></th>	
                    <th><strong>System Code</strong></th>
                    <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>
				</thead>
                <tr class="general">
                	<td>
                	<select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" > 					  
                 		 <option value="0">-- Select --</option>
                        <?php foreach( $policy_year AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                	</select>
                </td>                    
                <td>
                    <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes" >	<!--onchange="populate_offday_date(this.value)"	-->					
                        <option value="0">-- Select --</option>
                    </select>
                </td> 
                <td>
                    <!--<select name="offdate_selector" id="offdate_selector" class="combo_boxes" style="width:100px" >	
                    	<option value="0">-- Select --</option>			
                    </select>-->
                     <input type="text" name="offdate" id="offdate" class="text_boxes" style="width:100px" ondblclick="openmypage_offday_date('','Off Day Information'); return false" placeholder="Double Click For Search" autocomplete="off" readonly="readonly" />
                      <input type="hidden" name="offdate_selector" id="offdate_selector" class="text_boxes" style="width:100px"  />
                </td> 	          
                    <td>
                    	<select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:100px" >							
                           <option value="">-- Select --</option>
                            <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <!--$salary_based_arr=array(0=>"Monthly Salary Based",1=>"Piece Rate Based");-->
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                    </td>
                    <td align="center">
                        <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                    </td>				
                	<td align="center">
                   		 <input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:100px" />&nbsp;
                    </td>
                </tr>
			</table>
		</fieldset>
    </div>
       <!-- <fieldset>
            <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
        </fieldset>-->
        
        <fieldset>        
             <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div> 
                                 
            <div align="center">
            <input type="button" value="Save" name="save" id="save" class="formbutton" style="width:100px" onclick="javascript:fnc_save_update_offday_ot(save_perm,edit_perm,delete_perm,approve_perm)" />
            <input type="button" onclick="new_window_short()" value="Print Ot Sheet" name="Print" class="formbutton" style="width:100px"/>
            </div>                     			
        </fieldset>
         <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden; display:none;">&nbsp;</div>
    </form>
</div>	
</body>
</html>