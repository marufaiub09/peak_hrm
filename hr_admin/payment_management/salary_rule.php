
<?php
include('../../includes/common.php');
 
	$sql = "SELECT * FROM lib_policy_salary_breakdown WHERE id = ".$_REQUEST['policy_id']." ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_breakdown_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$salary_breakdown_policy[$row['id']] = $row;
		
		$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		$salary_breakdown_policy[$row['id']]['definition'] = array();
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
		}
	}	
	
	$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$payroll_heads = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$payroll_heads[$row['id']] = $row;
	}	
	
if( $_REQUEST['policy_id'] ) {
	foreach( $salary_breakdown_policy[$_REQUEST['policy_id']]['definition'] AS $counter => $head ) {
?>
<tr>
	<td>
		<select name="payroll_head[]" id="payroll_head_<?php echo ($counter + 1); ?>" class="combo_boxes">
			<option value="0"></option>
			<?php foreach( $payroll_heads AS $payroll_id => $payroll ) { if( !in_array( $payroll['type'], array( 'Festival Earning', 'LFA Earning', 'Taxation' ) ) ) { ?>
			<option value="<?php echo $payroll['id']; ?>" <?php if( $payroll['id'] == $head['payroll_head'] ) echo "selected"; ?>><?php echo $payroll['custom_head']; ?></option>
			<?php } } ?>
		</select>
	</td>
	<td>
		<select name="type[]" id="type_<?php echo ($counter + 1); ?>" class="combo_boxes" disabled>
			<option value="%" <?php if( $head['type'] == '%' ) echo "selected"; ?>>%</option>
			<option value="Fixed" <?php if( $head['type'] == 'Fixed' ) echo "selected"; ?>>Fixed</option>
			<option value="Formula" <?php if( $head['type'] == 'Formula' ) echo "selected"; ?>>Formula</option>
		</select>
	</td>
	<td><input type="text" name="percentage_formula[]" id="percentage_formula_<?php echo ($counter + 1); ?>" value="<?php echo $head['percentage_formula']; ?>" class="text_boxes numbers" disabled /></td>
	<td>
		<select name="base_head[]" id="base_head_<?php echo ($counter + 1); ?>" class="combo_boxes" disabled>
			<option value="0"></option>
			<?php foreach( $payroll_heads AS $payroll_id => $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
			<option value="<?php echo $payroll['id']; ?>" <?php if( $head['type'] == '%' && $head['base_head'] == $payroll['id'] ) echo "selected"; ?>><?php echo $payroll['custom_head']; ?></option>
			<?php } } ?>
		</select>
	</td>
	<td><input type="text" name="amount[]" id="amount_<?php echo($counter + 1); ?>" value="<?php if( $head['type'] == 'Fixed' ) echo "0.00"; else echo $head['amount']; ?>" onblur="amount_blur( <?php echo ($counter + 1); ?> );" class="text_boxes numbers" disabled />
    <input type="text" name="amount_org[]" id="amount_org_<?php echo($counter + 1); ?>" value="<?php if( $head['type'] == 'Fixed' ) echo "0.00"; else echo $head['amount']; ?>" onblur="amount_blur( <?php echo ($counter + 1); ?> );" class="text_boxes numbers" disabled />
    </td>
</tr>
<?php
    }
}
?>