<?php
/***************************************
|	developed By:  Md. Ekram Hossain
|	Date of Development : 10/10/2012
***************************************/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else{ $buyer_name="";$company_name="";}

//echo $company_cond."xcz";
//---------------------------------------------------------------------------------------------------------
include('../../includes/common.php');

//designation_chart_new
$sql = "SELECT * FROM lib_designation WHERE  status_active=1 and is_deleted = 0 ORDER BY level,trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart_new = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart_new[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart_new[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE  status_active=1 and is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="includes/functions.js" type="text/javascript"></script>
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" type="text/css"  rel="stylesheet" media="screen">
        <script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../js/modal.js"></script>
        <script type="text/javascript" src="includes/ajax_submit_emp_promotion.js"></script>
        <script type="text/javascript" src="includes/functions.js"></script>
        
		<script type="text/javascript">
        var designation_chart = new Array();
        <?php
        foreach( $designation_chart AS $designation_id_level => $designation ) {
			echo "designation_chart[$designation_id_level] = new Array();\n";
			foreach( $designation AS $key => $value ) {
			if( $key == 'id' || $key == 'level' ) echo "designation_chart[$designation_id_level]['$key'] = $value;\n";
			else echo "designation_chart[$designation_id_level]['$key'] = '$value';\n";
			}
        }
        ?>
        //new add on change	
        var designation_chart_new = new Array();
        <?php
        foreach( $designation_chart_new AS $designation_id_level => $designation ) {
        echo "designation_chart_new[$designation_id_level] = new Array();\n";
        foreach( $designation AS $key => $value ) {
			if( $key == 'id' || $key == 'level' ) echo "designation_chart_new[$designation_id_level]['$key'] = $value;\n";
			else echo "designation_chart_new[$designation_id_level]['$key'] = '$value';\n";
			}
        }
        ?>	 
        /*
			$(document).ready(function() {
			//populate_designations(0,'level');
			//populate_designations(0,'desig');
			populate_designations_new();
			});
		*/
        function openmypage_employee_promotion_info_id_card(page_link,title)
        {			
			//alert("ok.....");
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=990px,height=370px,center=1,resize=0,scrolling=no','../')			
			emailwindow.onclose=function()
			{
				var hidden_emp_code = this.contentDoc.getElementById("hidden_emp_code");
				//alert(hidden_emp_code.value);
				var ab=(hidden_emp_code.value).split('_');
				$('#emp_code').val(ab[0]);
				$('#txt_name').val(ab[1]);
				$('#division_id').val(ab[2]);
				$('#section_id').val(ab[3]);
				$('#subsection_id').val(ab[4]);
				$('#old_designation_id').val(ab[6]);
				$('#company_id').val(ab[7]);
				$('#joining_date').val(ab[8]);
				$('#old_designation_level').val(ab[9]);
				$('#id_card').val(ab[10]);
				
				var hidden_emp_code=ab[0];
				//alert(ab[0]);
				showResult_multi('',hidden_emp_code,'search_promotion_employee_code','emp_promotion_list_view');
			}
        }	
        
        function openmypage_employee_promotion_info(page_link,title)
        {			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=990px,height=370px,center=1,resize=0,scrolling=no','../')			
			emailwindow.onclose=function()
			{
				var hidden_emp_code = this.contentDoc.getElementById("hidden_emp_code");
				//alert(hidden_emp_code.value);
				var ab=(hidden_emp_code.value).split('_');
				$('#emp_code').val(ab[0]);
				$('#txt_name').val(ab[1]);
				$('#division_id').val(ab[2]);
				$('#section_id').val(ab[3]);
				$('#subsection_id').val(ab[4]);
				$('#old_designation_id').val(ab[6]);
				$('#company_id').val(ab[7]);
				$('#joining_date').val(ab[8]);
				$('#old_designation_level').val(ab[9]);
				$('#id_card').val(ab[10]);
				
				var hidden_emp_code=ab[0];
				//alert(ab[0]);
				
				/*
				var hidden_company = this.contentDoc.getElementById("hidden_company").value;
				var hidden_name = this.contentDoc.getElementById("hidden_name").value;
				var hidden_division = this.contentDoc.getElementById("hidden_division").value;
				var hidden_section = this.contentDoc.getElementById("hidden_section").value;
				var hidden_subsection = this.contentDoc.getElementById("hidden_subsection").value;
				var hidden_department = this.contentDoc.getElementById("hidden_department").value;
				var hidden_designation = this.contentDoc.getElementById("hidden_designation").value;
				var hidden_designation = this.contentDoc.getElementById("hidden_designation").value;
				var hidden_joining_date = this.contentDoc.getElementById("hidden_joining_date").value;
				var hidden_designation_level = this.contentDoc.getElementById("hidden_designation_level").value;
				//alert(hidden_designation_level);
				$('#employee_code').val(hidden_emp_code);
				$('#company_id').val(hidden_company);
				$('#txt_name').val(hidden_name);
				$('#division_id').val(hidden_division);
				$('#section_id').val(hidden_section);
				$('#subsection_id').val(hidden_subsection);
				$('#department_id').val(hidden_department);
				$('#old_designation_id').val(hidden_designation);
				//$('#designation_id_last').val(hidden_designation);
				$('#joining_date').val(hidden_joining_date);
				$('#old_designation_level').val(hidden_designation_level);
				*/
				
				showResult_multi('',hidden_emp_code,'search_promotion_employee_code','emp_promotion_list_view');
			}
        }	
        
        function reset_field()
        {
        document.getElementById('emp_promotion_list_view').innerHTML="";
        //document.getElementById('data_panel2').innerHTML="";
        }
        
        function populate_designations_new( designation_level ) {
			var options = '<option value="0">-- Select --</option>';
			for( var i = 0; i < designation_chart_new.length; i++ ) {
			if( designation_chart_new[i] != undefined && designation_chart_new[i]['level'] == designation_level && designation_chart_new[i]['status_active'] == 1 ) {
			options += '<option value="' + designation_chart_new[i]['id'] + '">' + designation_chart_new[i]['custom_designation'] + '</option>';
			}
        }
        $('#new_designation_id').html( options );
        //$('#designation_id').sort_select_box();
        }
        </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body>
        <form name="promotion_entry" id="promotion_entry" autocomplete="off">
            <div align="center" style="width:950px">
            	<div style="height:40px;">
                    <div class="form_caption">Employee Promotion Entry</div>
                    <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
                </div>
                <fieldset><legend>Employee Promotion</legend>
                    <table width="900" border="0" cellpadding="1" cellspacing="1">
                        <tbody>
                            <tr>
                                <td><strong>System Code</strong></td>
                                <td width="180px">
                                    <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:170px" autocomplete="off" ondblclick="openmypage_employee_promotion_info('search_employee_promotion.php','Employee Information'); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                                </td>
                                <td><strong>ID Card No</strong></td>
                                <td width="180px">
                                    <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:170px" ondblclick="openmypage_employee_promotion_info_id_card('search_employee_promotion_by_id.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" />
                                </td>
                                <td>&nbsp;</td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><strong>Employee Name</strong></td>
                                <td><input type="text" name="txt_name" id="txt_name" class="text_boxes" style="width:170px;" readonly="readonly" /> </td> 
                                <td><strong>Company Name</strong></td>
                                <td>
                                    <select name="company_id" id="company_id" class="combo_boxes" style="width:180px;" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach($company_details AS $company){if($company['status_active']==1){?>
                                        <option value="<?php echo $company['id'];?>" <? if(count($company_details)==1)echo"selected"; ?>><?php echo $company['company_name'];?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                <td><strong>Designation</strong></td>
                                <td width="180px">
                                    <select name="old_designation_id" id="old_designation_id" class="combo_boxes" style="width:180px" disabled="disabled" >
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Designation Level</strong></td>
                                <td>
                                    <select name="old_designation_level" id="old_designation_level" class="combo_boxes" style="width:180px" disabled="disabled" >
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $designation_chart AS $designation_level ) { if( $designation_level['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $designation_level['id']; ?>"><?php echo $designation_level['system_designation']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                <td><strong>Division</strong></td>
                                <td>
                                    <select name="division_id" id="division_id" class="combo_boxes" style="width:180px" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) {?>
                                        <option value="<?php echo $division['id']; ?>" <? if (count($division_details)==1) echo "selected";?>><?php echo $division['division_name']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                <td><strong>Section</strong></td>
                                <td>
                                    <select name="section_id" id="section_id" class="combo_boxes" style="width:180px" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $section['id']; ?>" <? if(count($section_details)==1) echo "selected";?>><?php echo $section['section_name']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Subsection</strong></td>
                                <td>
                                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:180px" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                <td><strong>Joining Date</strong></td>
                                <td>
                                    <input type="text" name="joining_date" id="joining_date" class="text_boxes" style="width:170px;" readonly="readonly" disabled="disabled"/>
                                </td>
                                <td>&nbsp;</td><td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset> 
                <fieldset >
                    <table width="900"  class="rpt_table" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                            <th width="150px"><strong>New Designation Level</strong></th>
                            <th width="150px"><strong>New Designation</strong></th>
                            <th width="150px"><strong>Effective Year</strong></th>
                            <th width="150px"><strong>Effective Month</strong></th>
                            <th width="150px"><strong>APR Score</strong></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select name="new_designation_level" id="new_designation_level" class="combo_boxes"  onchange="populate_designations_new(this.value)" style="width:180px;">
                                    <option value="0">-- Select --</option>
                                    <?php
                                    $sql = "SELECT DISTINCT(system_designation), level FROM lib_designation where  status_active=1 and is_deleted=0 ORDER BY level";
                                    $result = mysql_query( $sql );
                                    while( $row = mysql_fetch_array( $result ) ) {
                                    ?>
                                    <option value="<?php echo $row['level']; ?>"><?php echo $row['level'] . ". " . $row['system_designation']; ?></option>
                                    <?php } ?>
                                    </select>
                                </td> 
                                <td> 
                                    <select name="new_designation_id" id="new_designation_id" class="combo_boxes"  style="width:180px;">
                                        <!--onchange="populate_designations(this.value,'level')" this line is vise versa for desig lavel-->
                                        <option value="0">-- Select --</option>                    
                                    </select>                        
                                </td> 
                                <td> 
                                    <select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" style="width:180px">
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        $c_year=date("Y");
                                        $s_year=$c_year-5;
                                        $e_year=$c_year+5;
                                        
                                        for ($year = $s_year; $year <= $e_year; $year++) { ?>
                                        <option value=<?php echo $year;
                                        if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option><?php } ?>
                                    </select>                        
                                </td> 
                                <td> 
                                    <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes"  style="width:180px" >
                                      <option value="0">-- Select --</option>
                                      <option value="01">January</option>
                                      <option value="02">February</option>
                                      <option value="03">March</option>
                                      <option value="04">April</option>
                                      <option value="05">May</option>
                                      <option value="06">June</option>
                                      <option value="07">July</option>
                                      <option value="08">August</option>
                                      <option value="09">September</option>
                                      <option value="10">October</option>
                                      <option value="11">November</option>
                                      <option value="12">December</option>
                                  </select>
                                </td>
                                 <td> 
                                   <input type="text" name="apr_score" id="apr_score" class="text_boxes" value="" onkeypress="return numbersonly(this,event)" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="button_container" style="margin-top:20px;">
                        <input type="button" name="rpo_search" id="rpo_search" value="Save" class="formbutton"   style="width:150px"  onclick="javascript:fn_promotion_entry()";/>&nbsp;
                        <input type="reset" name="rpo_reset" id="rpo_reset" value="Reset" class="formbutton"   style="width:150px"/>&nbsp;
                        <input type="button" name="policy_button" id="policy_button" value="Policy Tagging" class="formbutton"   style="width:150px; visibility:hidden" />
                        
                        <input type="hidden" name="hidden_emp_code" id="hidden_emp_code" value="" /><!-- Hidden field -->
                        <input type="hidden" name="save_up" id="save_up">
                    </div>
                </fieldset>
                <fieldset>
                    <div id="emp_promotion_list_view" style="margin-top:5px; width:900px;" align="center"></div>
                    <!-- <div id="data_panel2" align="left" style="margin-top:10px;"></div>-->
                </fieldset>
            </div>
        </form>
    </body>
</html>