<?php
/****************************
	name :Ekram
	date :2014-05-11

****************************/

include('../../includes/common.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}


$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}


if ( check_database_process_status()==1 )
{
	echo server_busy_process(); die;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_salary.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	<script>
	
function countUp() {

 month = 'Jan';      // change the month to the one you want in the same format as shown
 date = '01';        // change the date to the one you want in the same format as shown
 year = '2002';      // change the year to the one you want in the same format as shown

 theDate = month + ' ' + date + ' ' + year;

   now = new Date();
   setdate = new Date(theDate);

   timer = (now - setdate) / 1000 / 60 / 60 / 24;
   timer = Math.round(timer);

document.getElementById("secs").innerHTML=timer;

  }






function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();


function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_month_selector').html( html )
				}
			});
		}



function fnc_regular_period_lock()
{
	var cbo_location_name 	= escape(document.getElementById('cbo_location_name').value);
	var cbo_company_name 	= escape(document.getElementById('cbo_company_name').value);
	var cbo_year_selector 	= escape(document.getElementById('cbo_year_selector').value);
	var cbo_month_selector 	= escape(document.getElementById('cbo_month_selector').value);
	
	// alert(cbo_year_selector+'=='+cbo_month_selector);return;
	$("#messagebox").removeClass().addClass('messagebox').text('Process Locking....').fadeIn(1000);

	if($('#cbo_company_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_salary_periods').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else
	{	
		$("#process_anim").html('<img src="../../images/loading2.gif" />').fadeIn(1000);
		nocache = Math.random();
		http.open('GET','includes/get_data_update.php?action=process_lock'+
					'&cbo_location_name='+cbo_location_name+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_year_selector='+cbo_year_selector+
					'&cbo_month_selector='+cbo_month_selector
					);
		http.onreadystatechange = regular_process_lock_reply;
		http.send(null); 
	}
}

function regular_process_lock_reply() 
{
	if(http.readyState == 4){ 		
	var response = (http.responseText).split('*');			
	//alert(response);	return;		
		if (response[1]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Period Locked Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
				
			});
		}
		else if (response[1]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Period Locked Un-Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
		else if (response[1]==3)
		{
			
			alert('Please Lock The Previous Period First.');
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Please Lock The Previous Period First.').addClass('messagebox_ok').fadeTo(900,1);
				$("#process_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
				$(this).css('background-color', 'red');
			});
		}				
	}
}	



function generate_list_view(selected_id){
	$.ajax({
				type: "GET",
				url: "includes/get_data_update.php?",
				data: 'type=list_view&id=' + selected_id,
				success: function( html ) {
					$('#data_panel').html( html )
					
				}
			});
		}

</script>

</head>

<body style="font-family:verdana; font-size:11px;">
<div  style="width:850px;">

<div style="width:500px;float:left">
    <div align="center" style="font-size:18px; width:500px; position:relative; height:40px; margin:5px 0;">Salary Period Lock</div>	
	<form id="holiday_form" action="javascript:fnc_regular_period_lock();" method="POST">
		<fieldset style="width:500px;">
			<legend>Lock Period</legend>
			<table width="500" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td>  Company Name</td>
					<td>
						<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Company Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 order by company_name");
								$numrows=mysql_num_rows($company_sql);
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value="<? echo $r_company["id"];?>" <? if($numrows==1)echo "selected"; ?> ><? echo "$r_company[company_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
			  <tr style="display:none">
					<td >  Location</td>
					<td>
						<select name="cbo_location_name" id="cbo_location_name" class="combo_boxes" style="width:250px ">
							<option value="0">--- Select Location Name ---</option>
							<?
								$company_sql= mysql_db_query($DB, "select location_name,id from lib_location where is_deleted=0  and status_active=1 order by location_name");
								while ($r_company=mysql_fetch_array($company_sql))
								{
								?>
								<option value=<? echo $r_company["id"];
								if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[location_name]" ?> </option>
								<?
								}
							?>
					  </select>	
					</td>
				</tr>
				<tr>
					<td>Select Year and Month</td>
                    <td>
                	<select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value),generate_list_view(this.value)" style="width:100px;"> 					  
                 		 <option value="0">-- Select --</option>
                        <?php foreach( $policy_year AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                	</select>
                
                    <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes" style="width:145px;">							
                        <option value="0">-- Select --</option>
                    </select>
                </td> 
				</tr>
                
				<tr>
					<td colspan="2" height="20"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" name="data_process" id="data_process" value="Start Salary Period Lock" style="width:270px; height:45px" class="formbutton" /> <input type="hidden" onclick="countUp()" /></td>
					
				</tr>
				<tr style="visibility:hidden">
					<td colspan="2" height="20" align="center" id="mins">
                    	<div style="float:left">
                        <div id="mins" style="float:left">cz</div>
                        <div id="secs" style="float:left; margin-left:10px"> zxc</div></div>
                    </td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<div align="center" id="messagebox"></div>
						<div id="process_anim"></div>
					</td>
				</tr>
			</table>
		</fieldset>
		
	</form>
  </div>
  <div align="center" style="font-size:18px; width:300px; position:relative; height:40px; margin:5px 0;"></div>	
    <div id="data_panel"  class="demo_jui" style="margin-top:10px; width:300px; float:right"></div>
</div>

</body>
</html>