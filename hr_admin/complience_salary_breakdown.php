<?php
/***************************************
|	developed By:  Md. Ekram Hossain
|	Date of Development : 03/06/2014
***************************************/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else{ $buyer_name="";$company_name="";}


//---------------------------------------------------------------------------------------------------------
include('../includes/common.php');
include('../includes/array_function.php');

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}	
$sql = "SELECT * FROM lib_designation WHERE  status_active=1 and is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown where is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = $row;
	
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0  ORDER BY custom_head ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="includes/functions.js" type="text/javascript"></script>
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" type="text/css"  rel="stylesheet" media="screen">
        <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
          <script type="text/javascript" src="payment_management/includes/ajax_submit_emp_promotion.js"></script>
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        <script type="text/javascript" src="includes/functions.js"></script>
        
	
    <script type="text/javascript">
    var head_counter = 0, payroll_heads = new Array(), payroll_options = '<option value="0"></option>';
	<?php 
	foreach( $payroll_heads AS $payroll_id => $payroll ) {
		if( $payroll['type'] != 3 || $payroll['type'] != 4 || $payroll['type'] != 2 ) {
			echo "payroll_options += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';\n";
			
			echo "payroll_heads[$payroll_id] = new Array();\n";
			foreach( $payroll AS $key => $value ) {
				if( $key == 'id' ) echo "payroll_heads[$payroll_id]['$key'] = $value;\n";
				else echo "payroll_heads[$payroll_id]['$key'] = '".$value."';\n";
			}
		}
	}
	?>

	 
	
	
	function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();
	
	
	$(document).ready(function() {
		$( "#formula_editor" ).hide();
		
	});
	
	function add_salary_breakdown() {
		//alert('asd');
		var head_counter=$('#salary tbody tr').length
		head_counter++;
		$('#salary tbody').append(
			'<tr>'
				+ '<td>'
					+ '<select name="payroll_head[]" id="payroll_head_' + head_counter + '" class="combo_boxes">'
						+ payroll_options
					+ '</select>'
				+ '</td>'
				+ '<td>'
					+ '<select name="type[]" id="type_' + head_counter + '" onchange="change_type( ' + head_counter + ' )" class="combo_boxes">'
						+ '<option value="%">%</option>'
						+ '<option value="Fixed">Fixed</option>'
						+ '<option value="Formula">Formula</option>'
					+ '</select>'
				+ '</td>'
				+ '<td><input type="text" name="percentage_formula[]" id="percentage_formula_' + head_counter + '" onfocus="add_formula( ' + head_counter + ' );" onblur="calculate_amount( ' + head_counter + ' );" value="" class="text_boxes numbers" /></td>'
				+ '<td>'
					+ '<select name="base_head[]" id="base_head_' + head_counter + '" onchange="calculate_amount( ' + head_counter + ' );" class="combo_boxes">'
						+ payroll_options
					+ '</select>'
				+ '</td>'
				+ '<td><input type="text" name="amount[]" id="amount_' + head_counter + '" value="0" onkeypress="return numbersonly(this,event)" onblur="calculate_total();" class="text_boxes numbers" disabled /></td>'
			+ '</tr>'
		);
		change_type( head_counter );
	}
	
	function salary_breakdown_policy_change( policy_id ) {
		if( policy_id != 0 ) {
			$.ajax({
				type: "POST",
				url: "employee_info_tabs/salary_rule.php?policy_id=" + policy_id,
				success: function( html ) {
					$('#salary tbody').html( html );
					head_counter = $('#salary tbody tr').length;
				}
			});
		}
		else {
			$('#salary tbody input[type="text"]').each(function() { $(this).removeAttr( 'disabled' ); });
			$('#salary tbody select').each(function() { $(this).removeAttr( 'disabled' ); });
		}
	}
	
	//old
	/*function calculate_total() {
		var earning = 0;
		var deduction = 0;
		//alert(payroll_heads.length);
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			for( var j = 1; j <= payroll_heads.length; j++ ) {
				if( $('#payroll_head_' + i).val() != 0 && $('#payroll_head_' + i).val() == payroll_heads[j]['id'] ) {
					if( payroll_heads[j]['type'] == 0 ) earning += Math.round( $('#amount_' + i).val() );
					else deduction += Math.round( $('#amount_' + i).val() );
					break;
				}
			}
		}
		//alert(earning - deduction);return;
		$('#total_salary').html( Math.floor( earning - deduction ) );
	}*/
	
	//new(10-06-2014)
	function calculate_total() {
		var earning = 0;
		var deduction = 0;
		
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			for( var j = 1; j <= payroll_heads.length; j++ ) {
				if( $('#payroll_head_' + i).val() != 0 && $('#payroll_head_' + i).val() == payroll_heads[j]['id'] ) {
					if( payroll_heads[j]['type'] == 0 ) earning += parseFloat( $('#amount_' + i).val() );
					else deduction += parseFloat( $('#amount_' + i).val() );
					break;
				}
			}
		}
		var grs=$('#gross_salary').val();
		//var rana=(earning);
		var different_val=Math.abs(earning - grs);
		//alert(different_val);
		var tot=((earning - deduction)+different_val);
		//alert(earning+'==='+deduction);return;
		//alert(Math.floor( earning - deduction ));return;
		//alert( earning - deduction );
		//$('#total_salary').html( Math.floor( earning - deduction ) );
		$('#total_salary').html(Math.floor(tot));
		
	}
	
	//old
	/*function calculate_all() {
 		if( $('#gross_salary').val() > 0 ) {
			for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
				if( $('#payroll_head_' + i).val() == 0 ) {
					$('#payroll_head_' + i).parent().parent().remove();
					head_counter--;
					for( var j = i; j <= $('#salary tbody tr').length; j++ ) {
						//alert('hi');//need to update the next ids if the deleted id is not the last
					}
				}
				else if( $('#type_' + i).val() == 'Fixed' && $('#amount_' + i).val() < 1 ) {
					alert('Please provide the fixed values first.');
					return false;
				}
				else if( $('#type_' + i).val() == 'Fixed' ) {
					$('#amount_' + i).val( parseFloat( $('#amount_' + i).val() ).toFixed( 2 ) );
				}
			} 
			for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
				if( $('#type_' + i).val() == 'Formula' ) {
					var formula = $('#percentage_formula_' + i).val();
					var formula2 = '';
					var abbreviation = '';
					for( var j = 0; j < formula.length; j++ ) {
						if( formula.charCodeAt( j ) >= 40 && formula.charCodeAt( j ) <= 57 && formula.charCodeAt( j ) != 44 ) {
							if( abbreviation != '' ) {
								if( abbreviation == 'GS' ) {
									formula2 += $('#gross_salary').val();
								}
								else {
									for( var k = 0; k < payroll_heads.length; k++ ) {
										if( payroll_heads[k] != undefined && payroll_heads[k]['abbreviation'] == abbreviation ) {
											for( var l = 1; l <= $('#salary tbody tr').length; l++ ) {
												if( $('#payroll_head_' + l).val() == payroll_heads[k]['id'] ) {
													if( parseInt( $('#amount_' + l).val() ) < 1 ) {
														alert( 'Please define ' + payroll_heads[k]['custom_head'] + ' first.' );
														return false;
													}
													else formula2 += parseInt( $('#amount_' + l).val() );
													break;
												}
											}
											break;
										}
									}
								}
								abbreviation = '';
							}
							formula2 += formula[j];
						}
						else abbreviation += formula[j];
					} 
					jQuery.globalEval( 'var formula_value = ' + formula2 );
					$('#amount_' + i).val( parseFloat(formula_value.toFixed( 2 ) ));
				}
				else if( $('#type_' + i).val() == '%' ) {
					if( $('#base_head_' + i).val() == 20 ) {
						var percentage_value = $('#gross_salary').val() * $('#percentage_formula_' + i).val() / 100;
						//alert(percentage_value);return;
						$('#amount_' + i).val( parseFloat( percentage_value ).toFixed( 2 ) );
						
					}
					else {
						for( var j = 1; j <= $('#salary tbody tr').length; j++ ) {
							if( $('#payroll_head_' + j).val() == $('#base_head_' + i).val() ) {
								if( parseInt( $('#amount_' + j).val() ) < 1 ) {
									alert( 'Undefined.' );
									return false;
								}
								else {
									var percentage_value = $('#amount_' + j).val() * $('#percentage_formula_' + i).val() / 100;
									$('#amount_' + i).val( parseFloat( percentage_value ).toFixed( 2 ) );
								}
								break;
							}
						}
					}
				}
			}
		}
		else alert( 'To calculate salary please provide gross salary' );
		
		calculate_total();
	}*/
	
	//new(10-06-2014)
	function calculate_all() {
 		if( $('#gross_salary').val() > 0 ) {
			for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
				if( $('#payroll_head_' + i).val() == 0 ) {
					$('#payroll_head_' + i).parent().parent().remove();
					head_counter--;
					for( var j = i; j <= $('#salary tbody tr').length; j++ ) {
						//alert('hi');//need to update the next ids if the deleted id is not the last
					}
				}
				else if( $('#type_' + i).val() == 'Fixed' && $('#amount_' + i).val() < 1 ) {
					alert('Please provide the fixed values first.');
					return false;
				}
				else if( $('#type_' + i).val() == 'Fixed' ) {
					$('#amount_' + i).val( parseFloat( $('#amount_' + i).val() ).toFixed( 2 ) );
				}
			} 
			for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
				if( $('#type_' + i).val() == 'Formula' ) {
					var formula = $('#percentage_formula_' + i).val();
					
					var formula2 = '';
					var abbreviation = '';
					for( var j = 0; j < formula.length; j++ ) {
						if( formula.charCodeAt( j ) >= 40 && formula.charCodeAt( j ) <= 57 && formula.charCodeAt( j ) != 44 ) {
							if( abbreviation != '' ) {
								if( abbreviation == 'GS' ) {
									formula2 += $('#gross_salary').val();
								}
								else {
									
									for( var k = 0; k < payroll_heads.length; k++ ) {
										if( payroll_heads[k] != undefined && payroll_heads[k]['abbreviation'] == abbreviation ) {
											for( var l = 1; l <= $('#salary tbody tr').length; l++ ) {
												if( $('#payroll_head_' + l).val() == payroll_heads[k]['id'] ) {
													if( parseFloat( $('#amount_' + l).val() ) < 1 ) {
														alert( 'Please define ' + payroll_heads[k]['custom_head'] + ' first.' );
														return false;
													}
													else formula2 += parseFloat( $('#amount_' + l).val() );
													break;
												}
											}
											break;
										}
									}
								}
								abbreviation = '';
							}
							formula2 += formula[j];
						}
						else abbreviation += formula[j];
					} 
					jQuery.globalEval( 'var formula_value = ' + formula2 );
					//alert( parseFloat(formula_value.toFixed( 2 )));break;
					$('#amount_' + i).val( parseFloat(formula_value.toFixed( 2 ) ));
				}
				else if( $('#type_' + i).val() == '%' ) {
					if( $('#base_head_' + i).val() == 20 ) {
						var percentage_value = $('#gross_salary').val() * $('#percentage_formula_' + i).val() / 100;
						//alert(percentage_value);return;
						$('#amount_' + i).val( parseFloat( percentage_value ).toFixed( 2 ) );
						
					}
					else {
						for( var j = 1; j <= $('#salary tbody tr').length; j++ ) {
							if( $('#payroll_head_' + j).val() == $('#base_head_' + i).val() ) {
								if( parseFloat( $('#amount_' + j).val() ) < 1 ) {
									alert( 'Undefined.' );
									return false;
								}
								else {
									var percentage_value = $('#amount_' + j).val() * $('#percentage_formula_' + i).val() / 100;
									$('#amount_' + i).val( parseFloat( percentage_value ).toFixed( 2 ) );
								}
								break;
							}
						}
					}
				}
			}
		}
		else alert( 'To calculate salary please provide gross salary' );
		
		calculate_total();
	}
	
		
	function change_type( counter ) {
		switch( $("#type_" + counter).val() ) {
			case '%':
				$('#percentage_container').html('Percentage');
				$("#percentage_formula_" + counter).removeAttr( 'disabled' );
				$("#base_head_" + counter).removeAttr( 'disabled' );
				$("#amount_" + counter).attr( 'disabled', 'disabled' );
				break;
			case 'Fixed':
				$("#percentage_formula_" + counter).attr( 'disabled', 'disabled' );
				$("#base_head_" + counter).val( '0' );
				$("#base_head_" + counter).attr( 'disabled', 'disabled' );
				$("#percentage_formula_" + counter).val( '' );
				$("#amount_" + counter).removeAttr( 'disabled' );
				break;
			case 'Formula':
				$('#percentage_container').html('Formula');
				$("#percentage_formula_" + counter).removeAttr( 'disabled' );
				$("#percentage_formula_" + counter).val( '' );
				$("#base_head_" + counter).val( '0' );
				$("#base_head_" + counter).attr( 'disabled', 'disabled' );
				$("#amount_" + counter).attr( 'disabled', 'disabled' );
				break;
		}
	}
	function calculate_amount( counter ) {
		var relatee = $('#base_head_' + counter).val();
		var add_value = 0;
		
		for( var i = 1; i <= head_counter; i++ ) {
			if( $('#payroll_head_' + i).val() == relatee ) {
				add_value = $('#amount_' + i).val() * $('#percentage_formula_' + counter).val() / 100;
				$('#amount_' + counter).val( parseFloat(add_value.toFixed( 2 )) );
				break;
			}
		}
		calculate_total();
	}
	function add_formula( counter ) {
		if( $("#type_" + counter).val() == 'Formula' ) {
			$('input[name="formula"]').val( $("#percentage_formula_" + counter).val() );
			$( "#formula_editor" ).dialog({
				modal: true,
				width: 348,
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
						$("#percentage_formula_" + counter).val( $('input[name="formula"]').val() )
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				}
			});
		}
	}
	
	function write_formula( btn_name ) {
		if( btn_name != 'backspace' ) $('input[name="formula"]').val( $('input[name="formula"]').val() + $('input[name="' + btn_name + '"]').attr('fval') );
		else {
			var text = $('input[name="formula"]').val();
			var backSpace = text.substr( 0, text.length - 1 );
			$('input[name="formula"]').val( backSpace );
		}
	}
	
	/*function reset_salary_form() {
		$('#salary tbody').html( '' );
		document.getElementById('emp_salary_form').reset();
	}*/
	
		//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

//alert(user_priv_tab[4]['edit_prv']);	

function show_emp_salary() {
			var designation_id=$("#designation_id").val();
			var emp_code=$("#emp_code").val();
			var gross_salary=$("#gross_salary").val();
			
 			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php?&designation_id="+designation_id+'&emp_code=' + emp_code+'&gross_salary=' + gross_salary,
				data: 'link=compliance_salary',
				success: function(data) {				
				eval(data);
				//save_sal_grade();
				}
				});
			 
			$.ajax({
				type: "POST",
				url: "includes/get_data_update.php?&emp_code="+emp_code,
				data: 'link=compliance_salary_breakdown',
				success: function(data) {				
				eval(data);
				head_counter=$("#hidden_head_count").val();				
				for( var i = 1; i <= head_counter; i++ ) change_type( i );
					calculate_total();
					
					/*if(joining_date_gross_salary_condition==0)
						{
							if(is_data_level_secured==1)
							{ 
								if($("#gross_salary").val()!='')
								{
									$('#gross_salary').attr("disabled","disabled")
								}
							}
						}*/					
				}
				});
				
				
		}


    function openmypage_employee_promotion_info(page_link,title)
        {			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=990px,height=370px,center=1,resize=0,scrolling=no','')			
			emailwindow.onclose=function()
			{
				var hidden_emp_code = this.contentDoc.getElementById("hidden_emp_code");
				//alert(hidden_emp_code.value);return;
				
				var ab=(hidden_emp_code.value).split('_');
				//alert(ab[11]);return;
				$('#emp_code').val(ab[0]);
				$('#txt_name').val(ab[1]);
				$('#division_id').val(ab[2]);
				$('#section_id').val(ab[3]);
				$('#subsection_id').val(ab[4]);
				$('#department_id').val(ab[5]);
				$('#designation_id').val(ab[6]);
				$('#company_id').val(ab[7]);
				$('#joining_date').val(ab[8]);
				$('#old_designation_level').val(ab[9]);
				$('#id_card').val(ab[10]);
				$('#location_id').val(ab[11]);
				var hidden_emp_code=ab[0];
				show_emp_salary();
				//showResult_multi('',hidden_emp_code,'search_promotion_employee_code','emp_promotion_list_view');
			}
        }	
        
  
  
  
      
 
 //Employee Salary of Employee Information ( Save / Update )
function fnc_emp_compliance_salary(save_perm,edit_perm,delete_perm,approve_perm){
	
	
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	
	calculate_all();
	if( save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if(edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if( $('#salary_grade').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#salary_grade').focus();
			$(this).html('Please Enter salary_grade').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	var gross = Math.floor( $('#gross_salary').val() );
	//alert(gross);return;

	var amount = 0;
	for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
		for( var j = 1; j <= payroll_heads.length; j++ ) {
			if( payroll_heads[j] != undefined && payroll_heads[j]['id'] == $('#payroll_head_' + i).val() && payroll_heads[j]['salary_head'] == 1 ) {
				amount += parseFloat( $('#amount_' + i).val() );
				//amount += Math.floor( $('#amount_' + i).val() );
				//amount += Math.round( $('#amount_' + i).val() );old
				break;
			}
		}
	}
	//var diff = gross - amount;old
	//new(10-06-2014)
	var diff =Math.floor(Math.abs(gross - amount));
	//alert(diff);return;
	if( diff != 0 && ( diff < - 0.03 || diff > 0.03 ) ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#gross_salary').focus();
			$(this).html('Gross salary (' + gross + ') and total salary (' + amount + ') do not match (' + diff + ').').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( diff != 0 ) {
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			if( $('#payroll_head_' + i).val() == 1 ) {
				$('#amount_' + i).val( parseFloat( $('#amount_' + i).val() ) + diff );
				break;
			}
		}
	}
	
	if( error == false ) {
		field_array[0] = new Array( 'emp_code', $('#emp_code').val() );
		field_array[1] = new Array( 'salary_grade', $('#salary_grade').val() );
		field_array[2] = new Array( 'salary_rule', $('#salary_rule').val() );
		field_array[3] = new Array( 'gross_salary', $('#gross_salary').val() );
		
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			field_array[i + 3] = new Array( $('#payroll_head_' + i).val(), $('#type_' + i).val(), $('#percentage_formula_' + i).val(), $('#base_head_' + i).val(), $('#amount_' + i).val() );
		}
		
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			if( i > 3 ) {
				data += '&head' + ( i - 3 ) + '=';
				for( var j = 0; j < field_array[i].length; j++ ) {
					if( j > 0 ) data += '_';
					data += field_array[i][j];
				}
			}
			else data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		data += '&head_counter=' + $('#salary tbody tr').length;
		//alert(data);return;
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_compliance_salary' + data+ '&nocache=' + nocache );
		http.onreadystatechange = response_emp_compliance_salary;
		http.send(null);
	}
}
//Employee Address Response of Employee Information ( Save / Update )
function response_emp_compliance_salary() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');
		
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			
			
		}
	}
}     
   
   
   
   
   
   </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body>
        <form name="" id="" autocomplete="off">
            <div align="center" style="width:1200px">
            	<div style="height:40px;">
                    <div class="form_caption">Compliance Salary Breakdown</div>
                    <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
                </div>
                <fieldset style="width:1050px;">
                	<legend align="left">Employee info</legend>
                        <table width="280px" border="0" cellpadding="0" cellspacing="2">
                            <tr>
                                <td width="100px"><strong>System Code</strong></td>
                                <td width="180px">
                                 <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:170px" autocomplete="off" ondblclick="openmypage_employee_promotion_info('payment_management/search_employee_promotion.php','Employee Information'); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" /></td>
                            
                            </tr>
                        </table>
                    <table width="1050" border="0" cellpadding="1" cellspacing="1">
                        <tbody>
                            <tr>
                                <td  width="100px"><strong>ID Card No</strong></td>
                                <td width="120px"><input type="text" name="id_card" id="id_card" class="text_boxes" style="width:120px" disabled="disabled" /></td>
                                <td><strong>Employee Name</strong></td>
                                <td><input type="text" name="txt_name" id="txt_name" class="text_boxes" style="width:170px;" disabled="disabled"  /> </td>
                                <td><strong>Designation</strong></td>
                                <td width="180px">
                                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:180px" disabled="disabled" >
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                <td><strong>Joining Date</strong></td>
                                <td>
                                    <input type="text" name="joining_date" id="joining_date" class="text_boxes" style="width:130px;" readonly="readonly" disabled="disabled"/>
                                </td>
                               
                            </tr>
                            <tr>
                                 
                                <td><strong>Company Name</strong></td>
                                <td>
                                    <select name="company_id" id="company_id" class="combo_boxes" style="width:130px;" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach($company_details AS $company){if($company['status_active']==1){?>
                                        <option value="<?php echo $company['id'];?>" <? if(count($company_details)==1)echo"selected"; ?>><?php echo $company['company_name'];?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                 <td><strong>Location Name</strong></td>
                                <td>
                                    <select name="location_id" id="location_id" class="combo_boxes" style="width:180px;" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach($location_details AS $location){if($location['status_active']==1){?>
                                        <option value="<?php echo $location['id'];?>" <? if(count($location_details)==1)echo"selected"; ?>><?php echo $location['location_name'];?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                <td><strong>Division</strong></td>
                                <td>
                                    <select name="division_id" id="division_id" class="combo_boxes" style="width:180px" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) {?>
                                        <option value="<?php echo $division['id']; ?>" <? if (count($division_details)==1) echo "selected";?>><?php echo $division['division_name']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                <td><strong>Department</strong></td>
                                <td>
                                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) {?>
                                        <option value="<?php echo $department['id']; ?>" <? if (count($department_details)==1) echo "selected";?>><?php echo $department['department_name']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                            </tr>
                            <tr>
                                
                                <td><strong>Section</strong></td>
                                <td>
                                    <select name="section_id" id="section_id" class="combo_boxes" style="width:130px" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $section['id']; ?>" <? if(count($section_details)==1) echo "selected";?>><?php echo $section['section_name']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                            
                          
                                <td><strong>Subsection</strong></td>
                                <td>
                                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:180px" disabled="disabled">
                                        <option value="0">--Select All--</option>
                                        <?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
                                        <?php } } ?>
                                    </select>                        
                                </td>
                                
                                <td>&nbsp;</td><td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset> 
                <fieldset style="width:800px;"><legend align="left">Salary info</legend>
                    <table border="0" cellpadding="0" cellspacing="2" width="750px" align="center">
                        <tr>
                            <td width="100">Salary Grade:</td>
                            <td width="150"><input type="text" name="salary_grade" id="salary_grade" class="text_boxes" value="" style="width:100px"  disabled="disabled"/></td>
                            <td width="100">Policy:</td>
                            <td width="150">
                            <select name="salary_rule" id="salary_rule" class="combo_boxes" onchange="salary_breakdown_policy_change( this.value )" style="width:170px" disabled="disabled" >
                            <option value="0">---Select-----</option>
                            <?php foreach( $salary_breakdown_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
                            <option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
                            <?php } } ?>
                            </select>
                            </td>
                            <td width="100">Gross Salary:</td>
                            <td width="150"><input type="text" name="gross_salary" id="gross_salary" class="text_boxes numbers" value="" style="width:150px"  onkeypress="return numbersonly(this,event)" /></td>
                        
                        </tr>
                    </table>
	<table id="salary" border="0" cellpadding="0" cellspacing="1" style="margin-top:20px;" width="850px" align="center">
		<thead>
			<tr>
				<th class="header">Payroll Head</th>
				<th class="header">Type</th>
				<th class="header" id="percentage_container">Percentage</th>
				<th class="header">Base Head</th>
				<th class="header">Amount</th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<td colspan="3" style="padding-top:10px;"><input type="button" id="Add" class="formbutton" value="Add" onclick="add_salary_breakdown()" style="width:100px;"/></td>
				<td align="center" style="padding-top:10px;"><input type="button" id="calculate" class="formbutton" value="Calculate" onclick="calculate_all()" /></td>
				<td align="justify" style="padding-top:10px;">Net Amount: <span id="total_salary" style="float:right;">0.00</span></td>
			</tr>
			<tr>
				<td colspan="5" align= "center" style="padding-top:10px;">
                	<input type="hidden" name="hidden_head_count" value="" />&nbsp;&nbsp;
					<input type="button" id="save_addres_sl" name="save" class="formbutton" value="Save"  style="width:130px;" onclick="fnc_emp_compliance_salary()"/>&nbsp;&nbsp;
					<input type="reset" name="cancel" id="cancel" class="formbutton" value="Refresh" onclick="reset_salary_form()" style="width:130px;"/>&nbsp;&nbsp;
                    
				</td>
              
			</tr>
		</tfoot>
	</table>
<!--</form>-->
<div id="formula_editor" title="Formula Editor">
	<table width="100%">
		<tr>
			<td><input type="text" name="formula" value="" style="padding:5px; width:308px; margin-bottom:4px;" readonly /></td>
		</tr>
		<tr>
			<td>
				<input type="button" name="brace_start" value="(" fval="(" class="operator" onclick="write_formula( 'brace_start' );" />
				<input type="button" name="brace_end" value=")" fval=")" class="operator" onclick="write_formula( 'brace_end' );" />
				<input type="button" name="plus" value="+" fval="+" class="operator" onclick="write_formula( 'plus' );" />
				<input type="button" name="minus" value="-" fval="-" class="operator" onclick="write_formula( 'minus' );" />
				<input type="button" name="multiply" value="*" fval="*" class="operator" onclick="write_formula( 'multiply' );" />
				<input type="button" name="divide" value="/" fval="/" class="operator" onclick="write_formula( 'divide' );" />
				<?php for( $i = 0; $i <= 9; $i++ ) { ?>
				<input type="button" name="number_<?php echo $i; ?>" value="<?php echo $i; ?>" fval="<?php echo $i; ?>" class="operator" onclick="write_formula( 'number_<?php echo $i; ?>' );" />
				<?php } ?>
				<input type="button" name="point" value="." fval="." class="operator" onclick="write_formula( 'point' );" />
				<input type="button" name="backspace" value="&larr;" class="operator" onclick="write_formula( 'backspace' );" />
			</td>
		</tr>
		<tr>
			<td>
				<?php foreach( $payroll_heads AS $payroll_id => $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
				<input type="button" name="<?php echo $payroll['custom_head']; ?>" value="<?php echo $payroll['custom_head'] . " (" . $payroll['abbreviation'] . ")"; ?>" fval="<?php echo $payroll['abbreviation']; ?>" style="width:158px; padding:2px 0;" onclick="write_formula( '<?php echo $payroll['custom_head']; ?>' );" />
				<?php } } ?>
			</td>
		</tr>
	</table>
  </div>
         </fieldset>
        </form>
    </body>
</html>
	
