﻿<?php

session_start();
date_default_timezone_set('UTC');

include('../../includes/common.php');
include('../../includes/array_function.php');
/* Replace the data in these two lines with data for your db connection */
extract($_GET);
extract($_POST);

//$marital_status=array(0=>"Single",1=>"Married",2=>"Separated",3=>"Widow");

		//payroll_heads
			$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$payroll_heads = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$payroll_heads[$row['id']] = array();
				foreach( $row AS $key => $value ) {
					$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
				}
			}
		//Designation array
			$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$designation_chart = array();
			$designation_chart_local = array();
			while( $row = mysql_fetch_array( $result ) ) 
			{
				$designation_chart[$row['id']] = $row['custom_designation'];
				$designation_chart_local[$row['id']] = $row['custom_designation_local'];
			}
			
			//print_r($designation_chart_local);die;
		//Company array
			$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$company_details = array();
			while( $row = mysql_fetch_array( $result ) ) {
				$company_details[$row['id']] = $row['company_name'];
			}
		//Department array
			$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$department_details = array();
			while( $row = mysql_fetch_array( $result ) ) {
				$department_details[$row['id']] = $row['department_name'];
			}
		//Diviion
			$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$division_details = array();
			while( $row = mysql_fetch_array( $result ) ) {
				$division_details[$row['id']] = $row['division_name'];
			}
		//Location
			$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$location_details = array();
			while( $row = mysql_fetch_array( $result ) ) {
				$location_details[$row['id']] =$row['location_name'];
			}
		//Section
		$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$section_details = array();
			while( $row = mysql_fetch_array( $result ) ) {
				$section_details[$row['id']] = $row['section_name'];
			}
		//Sub Section
			$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$subsection_details = array();
			while( $row = mysql_fetch_array( $result ) ) {
				$subsection_details[$row['id']] = $row['subsection_name'];
			}
		// skill
		$sql="SELECT * FROM lib_skill WHERE is_deleted = 0 ORDER BY skill_name ASC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$skill_details = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
		$skill_details[$row['id']] = mysql_real_escape_string( $row['skill_name'] );
		}
		







if(isset($_GET['getClientId']))
{  

$employee_info = array();	// do not delete 	
	
if ($type==1) // Employee for Disciplinary Info
	{
		
	$res = mysql_query("SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE emp_code = '".$_GET['getClientId']."' and is_deleted=0 and status_active=1 order by emp_code")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{			
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
			echo "formObj.txt_designation.value = '".mysql_real_escape_string($designation_chart[$inf["designation_id"]])."';\n";    
			echo "formObj.txt_company.value = '".mysql_real_escape_string($company_details[$inf["company_id"]])."';\n"; 
			echo "formObj.txt_division.value = '".mysql_real_escape_string($division_details[$inf["division_id"]])."';\n"; 
			echo "formObj.txt_department.value = '".mysql_real_escape_string($department_details[$inf["department_id"]])."';\n"; 
			echo "formObj.txt_section.value = '".$section_details[$inf["section_id"]]."';\n"; 
			//echo "formObj.txt_sub_section.value = '".$section_details[$inf["subsection_id"]]."';\n";  
			//echo "formObj.save_up_po.value = '".$inf["id"]."';\n"; 
		} 
		
	}
	
	if ($type==2) // Disc for Disciplinary Info
	{
		$res = mysql_query("SELECT * from hrm_disciplinary_info_mst
					WHERE emp_code = '".$_GET['getClientId']."' and withdrawn_date='0000-00-00' order by id desc")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_occurrence_date.value = '".$inf["occurrence_date"]."';\n";    
			echo "formObj.txt_action_date.value = '".$inf["action_date"]."';\n";    
			echo "formObj.txt_occurrence_details.value = '".mysql_real_escape_string($inf["occurrence_details"])."';\n";    
			echo "formObj.txt_investigating_members.value = '".mysql_real_escape_string($inf["investigating_members"])."';\n"; 
			echo "formObj.txt_investigation.value = '".mysql_real_escape_string($inf["investigation"])."';\n"; 
			echo "formObj.cbo_action_taken.value = '".mysql_real_escape_string($inf["action_taken"])."';\n"; 
			echo "formObj.txt_withdawn_date.value = '".mysql_real_escape_string($inf["withdrawn_date"])."';\n"; 
			echo "formObj.save_up_disc.value = '".$inf["id"]."';\n"; 
			echo "formObj.txt_comm_form_date.value = '".$inf["commitee_form_date"]."';\n"; 
			echo "formObj.txt_report_date.value = '".$inf["reporting_date"]."';\n"; 

		} 
		else{
			echo "formObj.txt_occurrence_date.value = '';\n";    
			echo "formObj.txt_action_date.value = '';\n";    
			echo "formObj.txt_occurrence_details.value = '';\n";    
			echo "formObj.txt_investigating_members.value = '';\n"; 
			echo "formObj.txt_investigation.value = '';\n"; 
			echo "formObj.cbo_action_taken.value = '';\n"; 
			echo "formObj.txt_withdawn_date.value = '';\n"; 
			echo "formObj.save_up_disc.value = '';\n"; 
			echo "formObj.txt_comm_form_date.value = '';\n"; 
			echo "formObj.txt_report_date.value = '';\n";  
		}
		
	}
}	

if($type=="branch_name")
{
	$id=$_GET['getClientId'];
 	$sql = "SELECT * FROM lib_bank WHERE is_deleted = 0 and status_active=1 and id=$id ORDER BY bank_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );	
	while( $row = mysql_fetch_array( $result ) ) {
		echo $row["branch_name"];
	}
	exit();
}

if($type=="calculate_qnty")
{
 $sql1="select sum(issue_item_quantity),sum(amount) from hrm_resource_issue_item_details where emp_code='$getClientId' and issue_item_name='$item_name' and status_active=1 and is_deleted=0";
	$result1=mysql_query($sql1);
	$row1=mysql_fetch_array($result1);
	
	 echo "formObj.qnty.value = '".mysql_real_escape_string($row1[0])."';\n"; 
	 $issue_rate=$row1[1]/$row1[0];  //sum(sum(amount)/sum(issue_item_quantity))
	 echo "formObj.hi_rate.value = '".$issue_rate."';\n";  
	 echo "formObj.hi_amount.value = '".mysql_real_escape_string($row1[1])."';\n";
	
	 $sql2="select sum(return_item_quantity),sum(balance_amount) from hrm_resource_return_item_detail where emp_code='$getClientId' and return_item_name ='$item_name' and status_active=1 and is_deleted=0";
	$result2=mysql_query($sql2);
	$row2=mysql_fetch_array($result2);
	
	echo "formObj.re_qnty.value = '".mysql_real_escape_string($row2[0])."';\n";
	echo "formObj.bl_amount.value = '".mysql_real_escape_string($row2[1])."';\n";
	exit();
}

if($link=="references")
{
	$sql = "SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name,designation_id FROM hrm_employee WHERE emp_code = '$emp_code'";
	$result=mysql_query($sql);
	$row=mysql_fetch_array($result);
	echo "$('#emp_ref_name').val('".mysql_real_escape_string($row["name"])."');\n";
	echo "$('#emp_ref_name').attr('title','".$designation_chart[$row["designation_id"]] ."');\n";	
}

if($link=="functional_superior")
{
	$sql = "SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name,designation_id  FROM hrm_employee WHERE emp_code = '$emp_code'";
	$result=mysql_query($sql);
	$row=mysql_fetch_array($result);
	echo "$('#functional_superior_name').val('".mysql_real_escape_string($row["name"])."');\n";
	echo "$('#functional_superior_name').attr('title','".$designation_chart[$row["designation_id"]] ."');\n";
	
	//$('#element').attr( 'title', 'foo' );
}

if($link=="basic")
{
	//echo $emp_code."===".$id_card_no;die;
	if($id_card_no!="")
	{
		$sql = "SELECT * FROM hrm_employee WHERE id_card_no = '$id_card_no'";
		
		$sql_bank_total = "SELECT a.*,b.id_card_no,b.gross_salary FROM hrm_employee_salary_bank a,hrm_employee b 
		WHERE b.id_card_no = '$id_card_no' and b.emp_code=a.emp_code and a.status_active=1";
	}
	else
	{ 
		$sql = "SELECT * FROM hrm_employee WHERE emp_code = '$emp_code'";
		
		$sql_bank_total = "SELECT a.*,b.id_card_no,b.gross_salary FROM hrm_employee_salary_bank a,hrm_employee b 
			WHERE b.emp_code = '$emp_code' and b.emp_code=a.emp_code and a.status_active=1";
	}
	
	$result_bank_total =  mysql_query( $sql_bank_total ) or die( $sql_bank_total . "<br />" . mysql_error() );
	while( $row_bank_total = mysql_fetch_assoc( $result_bank_total ) ) 
	{
		$db_total_bank+=$row_bank_total['salary_amount'];
	}
	echo "$('#bank_total_last').val('".$db_total_bank."');\n";
	
	mysql_query("SET CHARACTER SET utf8");
	mysql_query("SET SESSION collation_connection ='utf8_general_ci'");
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
	while( $row = mysql_fetch_array( $result ) ) 
	{
		echo "$('#emp_code').val('".mysql_real_escape_string($row["emp_code"])."');\n";	
		$_SESSION['emp_code'] = $row["emp_code"];
		echo "$('#first_name').val('".mysql_real_escape_string($row["first_name"])."');\n";	
		echo "$('#middle_name').val('".mysql_real_escape_string($row["middle_name"])."');\n";	
		echo "$('#last_name').val('".mysql_real_escape_string($row["last_name"])."');\n";	
		echo "$('#full_name_bangla').val('".mysql_real_escape_string(htmlentities($row["full_name_bangla"],ENT_COMPAT,"UTF-8"))."');\n";	
		echo "$('#id_card_no').val('".mysql_real_escape_string($row["id_card_no"])."');\n";	
		echo "$('#punch_card_no').val('".mysql_real_escape_string($row["punch_card_no"])."');\n";	
		echo "$('#dob').val('".mysql_real_escape_string(convert_to_mysql_date($row["dob"]))."');\n";	
		$days_age =datediff("yyyy",$row["dob"],date("d-m-Y"));
		echo "$('#hidden_age').val('".$days_age."');\n";
		echo "$('#father_name').val('".mysql_real_escape_string($row["father_name"])."');\n";	
		echo "$('#father_name_bangla').val('".mysql_real_escape_string(htmlentities($row["father_name_bangla"],ENT_COMPAT,"UTF-8"))."');\n";	
		echo "$('#mother_name').val('".mysql_real_escape_string($row["mother_name"])."');\n";	
		echo "$('#mother_name_bangla').val('".mysql_real_escape_string(htmlentities($row["mother_name_bangla"],ENT_COMPAT,"UTF-8"))."');\n";	
		echo "$('#sex').val('".mysql_real_escape_string($row["sex"])."');\n";
		echo "$('#birth_place').val('".mysql_real_escape_string($row["birth_place"])."');\n";	
		echo "$('#religion').val('".mysql_real_escape_string($row["religion"])."');\n";	
		echo "$('#blood_group').val('".$row["blood_group"]."');\n";	
		echo "$('#marital_status').val('".mysql_real_escape_string($row["marital_status"])."');\n";	
		echo "$('#nationality').val('".mysql_real_escape_string($row["nationality"])."');\n";	
		echo "$('#national_id').val('".mysql_real_escape_string($row["national_id"])."');\n";
		echo "$('#passport_no').val('".mysql_real_escape_string($row["passport_no"])."');\n";
		echo"populate_designations( $row[designation_level] );\n";
		echo "$('#designation_id').val('".mysql_real_escape_string($row["designation_id"])."');\n";
		echo "$('#designation_level').val('".mysql_real_escape_string($row["designation_level"])."');\n";
		echo "$('#joining_date').val('".mysql_real_escape_string(convert_to_mysql_date($row["joining_date"]))."');\n";
		echo "$('#confirmation_date').val('".mysql_real_escape_string(convert_to_mysql_date($row["confirmation_date"]))."');\n";
		echo "$('#service_benifit_from').val('".mysql_real_escape_string($row["service_benifit_from"])."');\n";
		echo "$('#category').val('".mysql_real_escape_string($row["category"])."');\n";
		echo "$('#functional_superior').val('".mysql_real_escape_string($row["functional_superior"])."');\n";
		echo "$('#functional_superior_name').val('".mysql_real_escape_string(return_field_value("CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name","hrm_employee","emp_code='$row[functional_superior]'"))."');\n";
		echo "$('#functional_superior_name').attr('title','".$designation_chart[mysql_real_escape_string(return_field_value("designation_id","hrm_employee","emp_code='$row[functional_superior]'"))]."');\n";
		echo "$('#admin_superior').val('".mysql_real_escape_string($row["admin_superior"])."');\n";
		echo "$('#salary_grade').val('".mysql_real_escape_string($row["salary_grade"])."');\n";
		echo "$('#salary_rule').val('".mysql_real_escape_string($row["salary_rule"])."');\n";
		echo "$('#gross_salary').val('".mysql_real_escape_string($row["gross_salary"])."');\n";
		echo "$('#remark').val('".mysql_real_escape_string($row["remark"])."');\n";
		echo "$('#status_attendance').val('".mysql_real_escape_string($row["status_attendance"])."');\n";
		echo "$('#status_salary').val('".mysql_real_escape_string($row["status_salary"])."');\n";
		echo "$('#company_id').val('".mysql_real_escape_string($row["company_id"])."');\n";
		echo "$('#location_id').val('".mysql_real_escape_string($row["location_id"])."');\n";
		echo "$('#division_id').val('".mysql_real_escape_string($row["division_id"])."');\n";
		echo "$('#department_id').val('".mysql_real_escape_string($row["department_id"])."');\n";
		echo "$('#section_id').val('".mysql_real_escape_string($row["section_id"])."');\n";
		echo "$('#subsection_id').val('".mysql_real_escape_string($row["subsection_id"])."');\n";
		echo "$('#skill_rank').val('".mysql_real_escape_string($row["skill_rank"])."');\n";
		//return_field_value("CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name","hrm_employee","emp_code='$row[emp_ref_code]'");
		echo "$('#emp_ref_name').val('".mysql_real_escape_string(return_field_value("CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name","hrm_employee","emp_code='$row[emp_ref_code]'"))."');\n";
		echo "$('#emp_ref_name').attr('title','".$designation_chart[mysql_real_escape_string(return_field_value("designation_id","hrm_employee","emp_code='$row[emp_ref_code]'"))]."');\n";
		echo "$('#emp_ref_code').val('".mysql_real_escape_string($row["emp_ref_code"])."');\n";	
		echo"show_hide_trs($row[category]);\n";
		echo "$('#Contract_start_date').val('".mysql_real_escape_string(convert_to_mysql_date($row["Contract_start_date"]))."');\n";
		echo "$('#Contract_end_date').val('".mysql_real_escape_string(convert_to_mysql_date($row["Contract_end_date"]))."');\n";
		//echo "$('#emp_status_active').val('".mysql_real_escape_string($row["status_active"])."');\n";
		if($row["status_active"]==1){$emp_status_active="Active Employee";} 
		else 
		{
			$separated_from=return_field_value("separated_from","hrm_separation","emp_code='$_SESSION[emp_code]' ORDER BY id DESC LIMIT 1");
			$separation_type_id=return_field_value("separation_type","hrm_separation","emp_code='$_SESSION[emp_code]' ORDER BY id DESC LIMIT 1");
			$emp_status_active="Inactive Employee"." (".convert_to_mysql_date($separated_from).")(".$separation_type[$separation_type_id].")";
		}
		echo "$('#emp_status_active').html('".$emp_status_active."');\n";		
		echo "$('#txt_status_salary').val('".mysql_real_escape_string($row["status_salary"])."');\n";
		//echo "$('#total_bank_salary').val('');\n";
		//echo "$('#gross_sal').val('');\n";
	}
		
	//echo "$('#save_up_address').val('".mysql_real_escape_string($_SESSION['emp_code'])."');\n";
	$date_time= date("Y-m-d"); 
	$pre_salary_periods=add_month($date_time,-1);
	$emp_code=$_SESSION['emp_code'];
	$sql_sal="SELECT emp_code FROM `hrm_salary_mst` WHERE emp_code = '$emp_code'";  // and `salary_periods` LIKE '$pre_salary_periods'
	$result_sal =  mysql_query( $sql_sal ) or die( $sql_sal . "<br />" . mysql_error() );
	if( mysql_num_rows($result_sal)>0)
	{
		
		echo "$('#designation_id').attr('disabled', 'true');\n";
		echo "$('#designation_level').attr('disabled', 'true');\n";
		
		//echo "$('#company_id').attr('disabled', 'true');\n";
		//echo "$('#location_id').attr('disabled', 'true');\n";
		//echo "$('#division_id').attr('disabled', 'true');\n";
		//echo "$('#department_id').attr('disabled', 'true');\n";
		//echo "$('#section_id').attr('disabled', 'true');\n";
		//echo "$('#subsection_id').attr('disabled', 'true');\n";
	}
	else
	{
		echo "$('#designation_id').removeAttr('disabled');\n";
		echo "$('#designation_level').removeAttr('disabled');\n";
		
		//echo "$('#company_id').removeAttr('disabled');\n";
		//echo "$('#location_id').removeAttr('disabled');\n";
		//echo "$('#division_id').removeAttr('disabled');\n";
		//echo "$('#department_id').removeAttr('disabled');\n";
		//echo "$('#section_id').removeAttr('disabled');\n";
		//echo "$('#subsection_id').removeAttr('disabled');\n";
		
	}
	exit();
}

if( $link == "photo" ) 
{
	$emp_code=$_SESSION['emp_code'];
	$sql = "SELECT location,identifier FROM resource_photo WHERE identifier = '$emp_code' AND type = 0  order by id DESC";
	//echo $sql;die;
	/*	
	if($id_card_no!=""){
	$sql= "select a.*,b.id_card_no,b.emp_code  from  resource_photo a, hrm_employee b where a.identifier=b.emp_code and  b.id_card_no='$id_card_no'";
	}else{
	$sql= "SELECT location FROM resource_photo WHERE identifier = '$emp_code' AND type = 0  order by id DESC";	
	}
	//echo $sql;die;	   
	*/
	mysql_query("SET CHARACTER SET utf8");
	mysql_query("SET SESSION collation_connection ='utf8_general_ci'");;
	
	//$_SESSION['emp_code'] = $emp_code;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	if( mysql_num_rows( $result ) > 0 ) {
		
		$row =  mysql_fetch_array( $result );
		$link_param="$row[location]";
		echo "<li class=\"success\"><img width=\"140px\" height=\"150px\" src=\"../$link_param\" onclick=\"fn_remove_picture('$link_param')\"><br /></li>\n";
	}
	else echo "\n";
		//echo "$('#save_up_photo').val('".mysql_real_escape_string($row["identifier"])."');\n";	
	exit();
}
	
if( $link == 'address' ) 
{
	$emp_code=$_SESSION['emp_code'];
	$sql = "SELECT * FROM hrm_employee_address WHERE emp_code = '$emp_code'";
	//echo $sql;die;
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
	
	while( $row = mysql_fetch_array( $result ) ) 
	{	
		if( $row['address_type'] == 0 ) $type = "present";
		if( $row['address_type'] == 1 ) $type = "permanent";
		if( $row['address_type'] == 2 ) $type = "alternate";
		
		echo "$('#save_up_address').val('".mysql_real_escape_string($row['emp_code'])."');\n";	
		//echo "$('#".$type."_house_no').val('".mysql_real_escape_string($row["subsection_id"])."');\n";			
		echo "$('#".$type."_house_no').val('".mysql_real_escape_string($row["house_no"])."');\n";
		echo "$('#".$type."_house_no_bangla').val('".mysql_real_escape_string($row["house_no_bangla"])."');\n";
		echo "$('#".$type."_road_no').val('".mysql_real_escape_string($row["road_no"])."');\n";
		echo "$('#".$type."_road_no_bangla').val('".mysql_real_escape_string($row["road_no_bangla"])."');\n";
		echo "$('#".$type."_division_id').val('".mysql_real_escape_string($row["division_id"])."');\n";
		echo "$('#".$type."_division_id_bangla').val('".mysql_real_escape_string($row["division_id_bangla"])."');\n";
		echo "$('#".$type."_district_id').val('".mysql_real_escape_string($row["district_id"])."');\n";
		echo "$('#".$type."_district_id_bangla').val('".mysql_real_escape_string($row["district_id_bangla"])."');\n";
		echo "$('#".$type."_thana').val('".mysql_real_escape_string($row["thana"])."');\n";
		echo "$('#".$type."_thana_bangla').val('".mysql_real_escape_string($row["thana_bangla"])."');\n";
		echo "$('#".$type."_village').val('".mysql_real_escape_string($row["village"])."');\n";
		echo "$('#".$type."_village_bangla').val('".mysql_real_escape_string($row["village_bangla"])."');\n";
		echo "$('#".$type."_post_code').val('".mysql_real_escape_string($row["post_code"])."');\n";
		echo "$('#".$type."_post_code_bangla').val('".mysql_real_escape_string($row["post_code_bangla"])."');\n";
		echo "$('#".$type."_phone_no').val('".mysql_real_escape_string($row["phone_no"])."');\n";
		echo "$('#".$type."_mobile_no').val('".mysql_real_escape_string($row["mobile_no"])."');\n";
		echo "$('#".$type."_email').val('".mysql_real_escape_string($row["email"])."');\n";
		echo "$('#co').val('".mysql_real_escape_string($row["co"])."');\n";
		echo "$('#co_bangla').val('".mysql_real_escape_string($row["co_bangla"])."');\n";
		//$employee_info[] =  $row;
	}
	exit();
}

if( $link == 'salary' ) 
{
	$emp_code=$_SESSION['emp_code'];
	//echo $emp_code;
	$sql = "SELECT emp_code,salary_grade,salary_rule,gross_salary,bank_gross,cash_disbursement FROM hrm_employee WHERE emp_code = '$emp_code' ORDER BY id ASC";
	$row =  mysql_fetch_array( mysql_query( $sql ) ) or die( $sql . "<br />" . mysql_error() );	
	if($row['salary_grade']>0){	
	echo "$('#salary_grade').val(" . $row['salary_grade'] . ");\n";
	}else{
	$row['salary_grade']=return_field_value("salary_grade","lib_designation","id='$designation_id'");
	//return_field_value("salary_grade","lib_designation","id='$designation_id'");
	echo "$('#salary_grade').val(" . $row['salary_grade'] . ");\n";
	}
	echo "$('#salary_rule').val(" . $row['salary_rule'] . ");\n";
	echo "$('#gross_salary').val(" . $row['gross_salary'] . ");\n";
	//echo "$('#bank_salary').val(" . $row['bank_gross'] . ");\n";
	echo "$('#cash_disbursement').val(" . $row['cash_disbursement'] . ");\n";
	//echo "salary_breakdown_policy_change(" . $row['salary_rule'] . ")\n";	
	//echo "$('#save_up_salary').val('" . $row[emp_code] . "');\n";
	if( mysql_real_escape_string($row['salary_rule'])!=0 || mysql_real_escape_string($row['gross_salary'])!=NULL )
	{
		echo "$('#save_up_salary').val('".mysql_real_escape_string($row["emp_code"])."');\n";	
	}
	//echo "calculate_all()\n";	
	
	$date_time= date("Y-m-d"); 
	$pre_salary_periods=add_month($date_time,-1);
	$sql_sal="SELECT emp_code FROM `hrm_salary_mst` WHERE emp_code = '$emp_code'";  // and `salary_periods` LIKE '$pre_salary_periods'
	$result_sal =  mysql_query( $sql_sal ) or die( $sql_sal . "<br />" . mysql_error() );
	if( mysql_num_rows($result_sal)>0)
	{
		//echo "$('#salary_rule').attr('disabled', 'true');\n";
		//echo "$('#gross_salary').attr('disabled', 'true');\n";
	}
	else
	{
		//echo "$('#salary_rule').removeAttr('disabled');\n";
		//echo "$('#gross_salary').removeAttr('disabled');\n";
	}
	
	exit();	
}

if( $link == 'salary_breakdown' ) 
{
	$emp_code=$_SESSION['emp_code'];
	$sql = "SELECT * FROM hrm_employee_salary WHERE emp_code = '$emp_code' ORDER BY id ASC"; 	
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$append_data = '';
	$i = 0;
	if( mysql_num_rows( $result ) > 0 ) 
	{
		while( $row = mysql_fetch_array( $result ) ) 
		{
			$i++;
			$append_data .= "<tr>
				<td>
					<select name=\"payroll_head[]\" id=\"payroll_head_$i\" class=\"combo_boxes\">
						<option value=\"0\"></option>";
						foreach( $payroll_heads AS $payroll ) {
						$append_data .= "<option value=\"$payroll[id]\""; if( $payroll['id'] == $row['payroll_head'] ) $append_data .= " selected"; $append_data .= ">$payroll[custom_head]</option>";
						}
					$append_data .=	"</select>
				</td>
				<td>
					<select name=\"type[]\" id=\"type_$i\" onchange=\"change_type( $i )\" class=\"combo_boxes\">
						<option value=\"%\""; if( $row['type'] == "%" ) $append_data .= " selected"; $append_data .= ">%</option>
						<option value=\"Fixed\""; if( $row['type'] == "Fixed" ) $append_data .= " selected"; $append_data .= ">Fixed</option>
						<option value=\"Formula\""; if( $row['type'] == "Formula" ) $append_data .= " selected"; $append_data .= ">Formula</option>
					</select>
				</td>
				<td>
					<input type=\"text\" name=\"percentage_formula[]\" id=\"percentage_formula_$i\" onfocus=\"add_formula( $i );\" onblur=\"calculate_amount( $i );\" value=\"$row[percentage_formula]\" class=\"text_boxes numbers\" />
				</td>
				<td>
					<select name=\"base_head[]\" id=\"base_head_$i\" onchange=\"calculate_amount( $i );\" class=\"combo_boxes\">
						<option value=\"0\"></option>";
						foreach( $payroll_heads AS $payroll ) {
						$append_data .= "<option value=\"$payroll[id]\""; if( $payroll['id'] == $row['base_head'] ) $append_data .= " selected"; $append_data .= ">$payroll[custom_head]</option>";
						}
					$append_data .=	"</select>
				</td>
				<td><input type=\"text\" name=\"amount[]\" id=\"amount_$i\" value=\"$row[amount]\" onblur=\"calculate_total();\" class=\"text_boxes numbers\" disabled /></td>
				<td><input type=\"hidden\" name=\"save_up_amount[]\" id=\"saveupamount_$i\" value=\"$row[emp_code]\" /></td>
			</tr>";
		}
	}
	/*
	else {
		$i = 1;
		$append_data .= "<tr>
							<td>
								<select name=\"payroll_head[]\" id=\"payroll_head_$i\" class=\"combo_boxes\">";
									foreach( $payroll_heads AS $payroll ) {
										$append_data .=	"<option value=\"$payroll[id]\">$payroll[custom_head]</option>";
									}
		$append_data .=	"		</select>
							</td>
							<td>
								<select name=\"type[]\" id=\"type_$i\" onchange=\"change_type( $i )\" class=\"combo_boxes\">
									<option value=\"%\">%</option>
									<option value=\"Fixed\">Fixed</option>
									<option value=\"Formula\">Formula</option>
								</select>
							</td>
							<td><input type=\"text\" name=\"percentage_formula[]\" id=\"percentage_formula_$i\" onfocus=\"add_formula( $i );\" onblur=\"calculate_amount( $i );\" value=\"\" class=\"text_boxes numbers\" /></td>
							<td>
								<select name=\"base_head[]\" id=\"base_head_$i\" onchange=\"calculate_amount( $i );\" class=\"combo_boxes\">";
									foreach( $payroll_heads AS $payroll ) {
										$append_data .=	"<option value=\"$payroll[id]\">$payroll[custom_head]</option>";
									}
		$append_data .=	"		</select>
							</td>
							<td><input type=\"text\" name=\"amount[]\" id=\"amount_$i\" value=\"0\" onblur=\"calculate_total();\" class=\"text_boxes numbers\" disabled /></td>
						</tr>";
	}
	*/
	echo "$('#salary tbody').html( '" . mysql_real_escape_string( $append_data ) . "' );\n";
	echo "$('#hidden_head_count').val(".$i.");\n";
	//echo "calculate_all()\n";	
	exit();
}

if( $link == 'salary_bank' ) 
{
	$emp_code=$_SESSION['emp_code'];
	$sql = "SELECT * FROM hrm_employee_salary_bank WHERE emp_code = '$emp_code'";		
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	while( $row = mysql_fetch_array( $result ) ) {			
	//$employee_info[] =  $row;		
	echo "$('#bank_id').val(". $row['lib_bank_id']." );\n";
	echo "$('#branch_name').val('". $row['branch_name']."' );\n";
	echo "$('#account_no').val('". $row['account_no']."' );\n";
	echo "$('#tin_no').val('". $row['tin_no']."' );\n";
	echo "$('#save_up_bank_sal').val('". $row['emp_code']."' );\n";
	//echo "$('#save_up_bank_sal').val( '" . mysql_real_escape_string( $row['emp_code'] ) . "' );\n";
	}
	exit();
}

if( $link == 'entitlement' ) 
{
	$emp_code=$_SESSION['emp_code'];		
	$sql = "SELECT * FROM hrm_employee_entitlement WHERE emp_code = '$emp_code'";
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	if( mysql_num_rows( $result ) > 0 ) 
	{
		$employee_info['entitlement'] =  mysql_fetch_assoc( $result );
		list( $year, $month, $day ) = explode( "-", $employee_info['entitlement']['pf_effective_date'] );
		$employee_info['entitlement']['pf_effective_date'] = $day . "-" . $month . "-" . $year;
		
		list( $year, $month, $day ) = explode( "-", $employee_info['entitlement']['gi_effective_date'] );
		$employee_info['entitlement']['gi_effective_date'] = $day . "-" . $month . "-" . $year;
	}
	
	$emp_sql = "SELECT * FROM hrm_employee WHERE emp_code = '$emp_code'";
	$emp_result =  mysql_query( $emp_sql ) or die( $emp_sql . "<br />" . mysql_error() );
	if($emp_rows = mysql_fetch_array( $emp_result ))
	{
		$employee_info['entitlement']['ot_entitled']=$emp_rows['ot_entitled'];
		$employee_info['entitlement']['staff_ot_entitled']=$emp_rows['staff_ot_entitled'];
		$employee_info['entitlement']['holiday_allowance_entitled']=$emp_rows['holiday_allowance_entitled'];
		$employee_info['entitlement']['pf_entitled']=$emp_rows['pf_entitled'];
		$employee_info['entitlement']['gi_entitled']=$emp_rows['gi_entitled'];
		
		if($emp_rows['ot_entitled']!=0 || $emp_rows['staff_ot_entitled']!=0 || $emp_rows['pf_entitled']!=0 || $emp_rows['gi_entitled']!=0)
		{
			echo "$('#save_up_entitlement_emp').val( '" . mysql_real_escape_string( $emp_rows['emp_code'] ) . "' );\n";
		}
	}
		
	$sql2 = "SELECT * FROM hrm_employee_nominee WHERE emp_code = '$emp_code'";
	$result2 =  mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	while( $row = mysql_fetch_assoc( $result2 ) ) 
	{
		$employee_info['nominee'][] = $row;
		echo "$('#save_up_nominee').val( '" . mysql_real_escape_string( $row['emp_code'] ) . "' );\n";
	}
	 
	echo "$('#save_up_entitlement_table').val( '" . mysql_real_escape_string( $employee_info['entitlement']['emp_code'] ) . "' );\n";
	//echo $employee_info['entitlement']['ot_entitled'];die;
	if( $employee_info['entitlement']['ot_entitled'] == 1 ) echo "$('#ot_entitled').attr( 'checked', true );\n";
	if( $employee_info['entitlement']['staff_ot_entitled'] == 1 ) echo "$('#staff_ot_entitled').attr( 'checked', true );\n";
	if( $employee_info['entitlement']['holiday_allowance_entitled'] == 1 ) echo "$('#holiday_allowance_entitled').attr( 'checked', true );\n";
		echo "$('input:radio[name=\"salary_type\"]').filter('[value=".$employee_info['entitlement']['salary_type']."]').attr( 'checked', true );\n";
					
	if( $employee_info['entitlement']['pf_entitled'] == 1 ) {
		echo "$('#pf_entitled').attr( 'checked', true );\n";
		echo "$('#pf input').removeAttr( 'disabled' );\n";
		if( $employee_info['entitlement']['pf_effective_date'] != '00-00-0000' ) echo "$('#pf_effective_date').val('".$employee_info['entitlement']['pf_effective_date']."');\n"; 
		$id=0;
		foreach( $employee_info['nominee'] as $key=>$val ) {
			if( $employee_info['nominee'][$key]['type'] == 0 ) {
				$id = $id + 1; 
				if( $id > 1 ){ echo "add_pf_nominee( $id-1 );\n"; }
				echo "$('#pf_nominee_name_".$id."').val('". $employee_info['nominee'][$key]['name'] ."');\n";
				echo "$('#pf_nominee_relation_".$id."').val('". $employee_info['nominee'][$key]['relation'] ."');\n";
				echo "$('#pf_nominee_ratio_".$id."').val('". $employee_info['nominee'][$key]['ratio'] ."');\n";
				echo "$('#pf_nominee_address_".$id."').val('". $employee_info['nominee'][$key]['address'] ."');\n";
			}
		}
	} 
	if( $employee_info['entitlement']['gi_entitled'] == 1 ) 
	{
		echo "$('#gi_entitled').attr( 'checked', true );\n";
		echo "$('#gi input').removeAttr( 'disabled' );\n";
		if( $employee_info['entitlement']['gi_effective_date'] != '00-00-0000' ) echo "$('#gi_effective_date').val('". $employee_info['entitlement']['gi_effective_date']."');\n";
		$id=0;
		foreach( $employee_info['nominee'] as $key=>$val ) 
		{
			if( $employee_info['nominee'][$key]['type'] == 1 ) 
			{ 
				$id = $id + 1; 
				if( $id > 1 ){ echo "add_gi_nominee( $id-1 );\n"; }
				echo "$('#gi_nominee_name_".$id."').val('". $employee_info['nominee'][$key]['name'] ."');\n";
				echo "$('#gi_nominee_relation_".$id."').val('". $employee_info['nominee'][$key]['relation'] ."');\n";
				echo "$('#gi_nominee_ratio_".$id."').val('". $employee_info['nominee'][$key]['ratio'] ."');\n";
				echo "$('#gi_nominee_address_".$id."').val('". $employee_info['nominee'][$key]['address'] ."');\n";
			}
		}
	}		
	exit();
}

if( $link == 'experience' ) 
{
	//echo "su..re";die;
	$emp_code=$_SESSION['emp_code'];
	$employee_info = array();
	$sql = "SELECT * FROM hrm_employee_experience WHERE emp_code = '$emp_code' and status_active=1 and is_deleted=0 ORDER BY id ASC";
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	//echo $result;
	if( mysql_num_rows( $result ) > 0 ) 
	{
		while( $row = mysql_fetch_array( $result ) ) 
		{
			list( $year, $month, $day ) = explode( "-", $row['joining_date'] );
			$row['joining_date'] = $day . "-" . $month . "-" . $year;
			
			list( $year, $month, $day ) = explode( "-", $row['resigning_date'] );
			$row['resigning_date'] = $day . "-" . $month . "-" . $year;
			
			$row['gross_salary'] = ( $row['gross_salary'] == '0.00' ) ? NULL : $row['gross_salary'];
			
			$employee_info[] = $row;
			echo "$('#save_up_experience').val( '" . mysql_real_escape_string( $row['emp_code'] ) . "' );\n";
		}
	}
	else $employee_info = "";		
	
	foreach( $employee_info as $key=>$val ) 
	{
		$id = $key + 1;
		if( $id > 1 ) echo "add_experience( $key );\n";						
		echo "$('#organization_name_".$id."').val( '".mysql_real_escape_string($employee_info[$key]['organization_name'])."' );\n";
		echo "$('#designation_".$id."').val('". mysql_real_escape_string($employee_info[$key]['designation'])."');\n";
		echo "$('#joining_date_".$id."').val('". mysql_real_escape_string($employee_info[$key]['joining_date'])."');\n";
		echo "$('#resigning_date_".$id."').val('". mysql_real_escape_string($employee_info[$key]['resigning_date'])."');\n";
		echo "$('#service_length_".$id."').val('". $employee_info[$key]['sevice_length']."');\n";
		echo "$('#gross_salary_".$id."').val('". $employee_info[$key]['gross_salary']."');\n";				
	}					
	//echo "reset_experience_form()\n";
	exit();
}

if( $link == 'education' ) 
{
	$emp_code=$_SESSION['emp_code'];
	$employee_info = array();
	$sql = "SELECT * FROM hrm_employee_education WHERE emp_code = '$emp_code' ORDER BY id ASC";
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	if( mysql_num_rows( $result ) > 0 ) {
		while( $row = mysql_fetch_array( $result ) ) {
			$employee_info[] = $row;
			//echo "$('#save_up_education').val('".$row['emp_code']."');\n";
			echo "$('#save_up_education').val( '" . mysql_real_escape_string( $row['emp_code'] ) . "' );\n";
		}
	}
	else $employee_info = "";		
	foreach( $employee_info as $key=>$val ) {
			$id = $key + 1;
			if( $id > 1 ) echo "add_education(".$key." );\n";
			echo "$('#exam_name_".$id."').val('".$employee_info[$key]['exam_name']."');\n";
			echo "$('#board_".$id."').val('".$employee_info[$key]['board']."');\n";
			echo "$('#institution_".$id."').val('".$employee_info[$key]['institution']."');\n";
			echo "$('#discipline_".$id."').val('".$employee_info[$key]['discipline']."');\n";
			echo "$('#major_subject_".$id."').val('".$employee_info[$key]['major_subject']."');\n";
			echo "$('#passing_year_".$id."').val('".$employee_info[$key]['passing_year']."');\n";
			echo "$('#result_".$id."').val('".$employee_info[$key]['result']."');\n";
			echo "$('#edu_nature_".$id."').val('".$employee_info[$key]['education_nature']."');\n";
		}		
	exit();
}
	
// Skill
if( $link == 'skill' ) 
{
	$emp_code=$_SESSION['emp_code'];
	$employee_info = array();
	$sql = "SELECT * FROM hrm_employee_skill WHERE emp_code = '$emp_code' ORDER BY id ASC";
	$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	if( mysql_num_rows( $result ) > 0 ) 
	{
		while( $row = mysql_fetch_array( $result ) ) 
		{
			list( $year, $month, $day ) = explode( "-", $row['eff_date'] );
			$row['eff_date'] = $day . "-" . $month . "-" . $year;
			$employee_info[] = $row;
			
			echo "$('#save_up_skill').val( '" . mysql_real_escape_string( $row['emp_code'] ) . "' );\n";
		}
	}
	else $employee_info = "";		
	foreach( $employee_info as $key=>$val ) 
	{
		$id = $key + 1;
		if( $id > 1 ) echo "add_skill(".$key." );\n";
		echo "$('#skill_name_".$id."').val('".$skill_details[$employee_info[$key]['skill_name']]."');\n";
		echo "$('#skill_id_".$id."').val('".$employee_info[$key]['skill_name']."');\n";
		echo "$('#current_assign_".$id."').val('".$employee_info[$key]['current_assign']."');\n";
		echo "$('#eff_date_".$id."').val('".mysql_real_escape_string($employee_info[$key]['eff_date'])."');\n";
	}
	//echo "reset_skill_form();\n";		
	exit();
}

// Family	
if( $link == 'family' ) {
		$emp_code=$_SESSION['emp_code'];
		$sql = "SELECT * FROM hrm_employee_family WHERE emp_code = '$emp_code' ORDER BY id ASC";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			while( $row = mysql_fetch_array( $result ) ) {
				list( $year, $month, $day ) = explode( "-", $row['dob'] );
				$row['dob'] = $day . "-" . $month . "-" . $year;
				
				$employee_info[] = $row;
				
				echo "$('#save_up_familyinfo').val( '" . mysql_real_escape_string( $row['emp_code'] ) . "' );\n";
			}
		}
		else $employee_info = "";
		
		foreach( $employee_info as $key=>$val ) {
				$id = $key + 1;
				if( $id > 1 ) echo "add_family(". $key .");\n";
				echo "$('#fname_".$id."').val('". $employee_info[$key]['name'] ."');\n";
				echo "$('#frelation_".$id."').val('". $employee_info[$key]['relation'] ."');\n";
				if( $employee_info[$key]['dob'] != '00-00-0000' ) {
					echo "$('#fdob_".$id."').val('". $employee_info[$key]['dob'] ."');\n";
					echo "calculate_age('". $id ."');\n";	
					//echo "$('#fage_".$id."').val( calculate_age('". $employee_info[$key]['dob'] ."') );\n";
				}
				echo "$('#foccupation_".$id."').val('". $employee_info[$key]['occupation'] ."');\n";
				echo "$('#fcontact_no_".$id."').val('". $employee_info[$key]['contact_no'] ."');\n";
				
			}	
			//echo "reset_family_form();\n";	
		exit();
	}
	

if( $link == 'policy_taging' ) {
		
		$emp_code=$_SESSION['emp_code'];
		$sql = "SELECT * FROM hrm_employee WHERE emp_code = '$emp_code' ORDER BY id ASC";
		//echo $sql;die;
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			while( $employee_info = mysql_fetch_array( $result ) ) {			 	 	 	 	 	 	 	 	 			
					
					echo "reset_form();\n";
					
					if($employee_info['overtime_policy']!=0 || $employee_info['holiday_incentive_policy']!=0 || $employee_info['duty_roster_policy']!=0 || $employee_info['leave_policy']!=0 || $employee_info['maternity_leave_policy']!=0 || $employee_info['attendance_bonus_policy']!=0 || $employee_info['late_deduction_policy']!=0 || $employee_info['absent_deduction_policy']!=0 || $employee_info['bonus_policy']!=0 || $employee_info['tax_policy']!=0 || $employee_info['shift_policy']!=0 || $employee_info['tiffin_policy']!=0 || $employee_info['allowance_policy']!=0 || $employee_info['early_deduction_policy']!=0 )
					{
					echo "$('#save_up_policytaging').val( '" . mysql_real_escape_string( $employee_info['emp_code'] ) . "' );\n";
					}
					
					
					echo "$('#overtime_policy').val('". $employee_info['overtime_policy'] ."');\n";
					echo "$('#holiday_incentive_policy').val('". $employee_info['holiday_incentive_policy'] ."');\n";
					echo "$('#duty_roster_policy').val('". $employee_info['duty_roster_policy'] ."');\n";
					echo "$('#leave_policy').val('". $employee_info['leave_policy'] ."');\n";
					if( $employee_info['sex']==0){
						echo "$('#maternity_leave_policy').attr('disabled', 'true');\n";
					}else{
					echo "$('#maternity_leave_policy').val('". $employee_info['maternity_leave_policy'] ."');\n";
					}
					echo "$('#attendance_bonus_policy').val('". $employee_info['attendance_bonus_policy'] ."');\n";
					echo "$('#late_deduction_policy').val('". $employee_info['late_deduction_policy'] ."');\n";
					echo "$('#absent_deduction_policy').val('". $employee_info['absent_deduction_policy'] ."');\n";
					echo "$('#bonus_policy').val('". $employee_info['bonus_policy'] ."');\n";
					echo "$('#tax_policy').val('". $employee_info['tax_policy'] ."');\n";
					echo "$('#shift_policy').val('". $employee_info['shift_policy'] ."');\n";
					echo "$('#tiffin_policy').val('". $employee_info['tiffin_policy'] ."');\n";
					echo "$('#allowance_policy').val('". $employee_info['allowance_policy'] ."');\n";
					echo "$('#early_deduction_policy').val('". $employee_info['early_deduction_policy'] ."');\n";
					 		 	 	 	 	 	 					
					if( $employee_info['overtime_policy'] > 0 ) echo "$('#overtime').attr('checked', 'true');\n"; 					
					if( $employee_info['holiday_incentive_policy'] > 0 ) echo "$('#holiday_incentive').attr('checked', 'true');\n";						
					if( $employee_info['duty_roster_policy'] > 0 ) echo "$('#duty_roster').attr('checked', 'true');\n"; 					
					if( $employee_info['leave_policy'] > 0 ) echo "$('#leave').attr('checked', 'true');\n"; 						
					if( $employee_info['maternity_leave_policy'] > 0 )  echo "$('#maternity_leave').attr('checked', 'true');\n"; 						
					if( $employee_info['attendance_bonus_policy'] > 0 ) echo "$('#attendance_bonus').attr('checked', 'true');\n";						
					if( $employee_info['absent_deduction_policy'] > 0 ) echo "$('#absent_deduction').attr('checked', 'true');\n";						
					if( $employee_info['late_deduction_policy'] > 0 ) echo "$('#late_deduction').attr('checked', 'true');\n";							
					if( $employee_info['bonus_policy'] > 0 ) echo "$('#bonus').attr('checked', 'true');\n"; 						
					if( $employee_info['tax_policy'] > 0 ) echo "$('#tax').attr('checked', 'true');\n";								
					if( $employee_info['shift_policy'] > 0 ) echo "$('#shift').attr('checked', 'true');\n";
					if( $employee_info['sex']==0) echo "$('#maternity_leave').attr('disabled', 'true');\n";
					if( $employee_info['tiffin_policy'] > 0 ) echo "$('#tiffin').attr('checked', 'true');\n";
					if( $employee_info['allowance_policy'] > 0 ) echo "$('#allowance').attr('checked', 'true');\n"; 
					if( $employee_info['early_deduction_policy'] > 0 ) echo "$('#early_deduction').attr('checked', 'true');\n"; 	
								
				}					
		}
		
		exit();	
	}


if( $link == 'weekend' ) {
	
		$emp_code=$_SESSION['emp_code'];
		$sql = "SELECT * FROM hrm_weekend WHERE emp_code = '$emp_code' ORDER BY id ASC";
		
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$j=0;$weekend='';$weekend_id='';
		if( mysql_num_rows( $result ) > 0 ) {
			while( $employee_info = mysql_fetch_array( $result ) ) {
					
					if($j==0){
						$weekend = $employee_info[weekend];
						$weekend_id = $employee_info[weekend];
					}else{
						$weekend .= "*".$employee_info[weekend];
						$weekend_id .= ",".$employee_info[weekend];
					}
					$j++;
					
					echo "$('#save_up_weekend').val( '" . mysql_real_escape_string( $employee_info['emp_code'] ) . "' );\n";	
				}	
				
			echo "$('#weekend').val('". $weekend ."');\n";	
			echo "$('#weekend_id').val('". $weekend_id ."');\n";
			
						
		}
		
		exit();	
	}
	
if( $link == 'submit_docs' ) {
//echo $sql = "SELECT a.id as ids,a.document_name,b.mst_id,b.designation_id FROM  lib_hrm_documents a,lib_docs_designation b WHERE b.designation_id ='$designation_id' and a.id=b.mst_id ORDER BY a.id ASC";die;
	?>	
		
    <select name="document_name" id="document_name" class="combo_boxes" style="width:140px">
        <option value="0">-- Select --</option>
        <?
        $sql= mysql_db_query($DB, "SELECT a.id as ids,a.document_name,a.document_type,b.mst_id,b.designation_id FROM  lib_hrm_documents a,lib_docs_designation b WHERE b.designation_id ='$designation_id' and a.id=b.mst_id and a.document_type=2 ORDER BY a.document_name ASC");
        $numrows=mysql_num_rows($sql);
        while ($selectResult = mysql_fetch_array($sql))
        {
        ?>
            <option value="<?php echo $selectResult["ids"]; ?>"><?php echo $selectResult["document_name"]; ?></option>
        <?
        }
        ?>
    </select>
<?  
exit();                   	 	
}

// submit_docs_office
if( $link == 'submit_docs_office' ) 
{
	//echo "hello";
	?>	
    <select name="document_id" id="document_id" class="combo_boxes" style="width:140px">
        <option value="0">-- Select --</option>
        <?
        $sql= mysql_db_query($DB, "SELECT a.id as ids,a.document_name,a.document_type,b.mst_id,b.designation_id FROM  lib_hrm_documents a,lib_docs_designation b WHERE b.designation_id ='$designation_id' and a.id=b.mst_id and a.document_type=1 ORDER BY a.document_name ASC");
        $numrows=mysql_num_rows($sql);
        while ($selectResult = mysql_fetch_array($sql))
        {
        ?>
            <option value="<?php echo $selectResult["ids"]; ?>"><?php echo $selectResult["document_name"]; ?></option>
        <?
        }
        ?>
    </select>
	<?  
exit();                   	 	
}



if( $link == 'del_bank' ) {
	//echo $del_id;die;
	//$sql = "DELETE FROM hrm_employee_salary_bank WHERE id = '$del_id'";
	//$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		//exit();
		//if($del_id==0 || $del_id==""){
		//}
	$emp_code=$_SESSION['emp_code'];
	$sql = "UPDATE hrm_employee_salary_bank
			SET
				status_active='0',
				is_deleted='1',
				is_locked='1'
			WHERE id=$del_id";
			//echo $sql;die;  and emp_code=$emp_code
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );	
		
		
	}



//new for compliance
if( $link == 'compliance_salary' ) {
	
		 $emp_code=$emp_code;
		
		 $sql = "SELECT salary_grade,salary_rule,gross_salary,buyer_gross_salary FROM hrm_employee WHERE emp_code = '$emp_code' ORDER BY id ASC";
		$row =  mysql_fetch_array( mysql_query( $sql ) ) or die( $sql . "<br />" . mysql_error() );	
		if($row['salary_grade']>0){	
		echo "$('#salary_grade').val(" . $row['salary_grade'] . ");\n";
		}else{
		$row['salary_grade']=return_field_value("salary_grade","lib_designation","id='$designation_id'");
		//return_field_value("salary_grade","lib_designation","id='$designation_id'");
		echo "$('#salary_grade').val(" . $row['salary_grade'] . ");\n";
		}
		echo "$('#salary_rule').val(" . $row['salary_rule'] . ");\n";
		if($row['buyer_gross_salary']>0){
			echo "$('#gross_salary').val(" . $row['buyer_gross_salary'] . ");\n";	
		}else{
			echo "$('#gross_salary').val(" . $row['gross_salary'] . ");\n";
		}
		
		
		
		exit();	
	}



if( $link == 'compliance_salary_breakdown' ) {
	
		$emp_code=$emp_code;
		$sql = "SELECT * FROM hrm_employee_salary WHERE emp_code = '$emp_code' ORDER BY id ASC"; 	
		$result_actual =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$append_data = '';
		
		//new
		$sql = "SELECT * FROM hrm_compliance_salary_breakdown WHERE emp_code = '$emp_code' ORDER BY id ASC"; 	
		$result_compliance =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if(mysql_num_rows( $result_compliance )> 0 ){
			 $result=$result_compliance;
		}else{
			 $result=$result_actual;
		}
		
		$i = 0;
		if( mysql_num_rows( $result ) > 0 ) {
			
			while( $row = mysql_fetch_array( $result ) ) {
				$i++;
				$append_data .= "<tr>
									<td>
										<select name=\"payroll_head[]\" id=\"payroll_head_$i\" class=\"combo_boxes\">
											<option value=\"0\"></option>";
											foreach( $payroll_heads AS $payroll ) {
												$append_data .= "<option value=\"$payroll[id]\""; if( $payroll['id'] == $row['payroll_head'] ) $append_data .= " selected"; $append_data .= ">$payroll[custom_head]</option>";
											}
				$append_data .=	"		</select>
									</td>
									<td>
										<select name=\"type[]\" id=\"type_$i\" onchange=\"change_type( $i )\" class=\"combo_boxes\">
											<option value=\"%\""; if( $row['type'] == "%" ) $append_data .= " selected"; $append_data .= ">%</option>
											<option value=\"Fixed\""; if( $row['type'] == "Fixed" ) $append_data .= " selected"; $append_data .= ">Fixed</option>
											<option value=\"Formula\""; if( $row['type'] == "Formula" ) $append_data .= " selected"; $append_data .= ">Formula</option>
										</select>
									</td>
									<td><input type=\"text\" name=\"percentage_formula[]\" id=\"percentage_formula_$i\" onfocus=\"add_formula( $i );\" onblur=\"calculate_amount( $i );\" value=\"$row[percentage_formula]\" class=\"text_boxes numbers\" /></td>
									<td>
										<select name=\"base_head[]\" id=\"base_head_$i\" onchange=\"calculate_amount( $i );\" class=\"combo_boxes\">
											<option value=\"0\"></option>";
											foreach( $payroll_heads AS $payroll ) {
												$append_data .= "<option value=\"$payroll[id]\""; if( $payroll['id'] == $row['base_head'] ) $append_data .= " selected"; $append_data .= ">$payroll[custom_head]</option>";
											}
				$append_data .=	"		</select>
									</td>
									<td><input type=\"text\" name=\"amount[]\" id=\"amount_$i\" value=\"$row[amount]\" onblur=\"calculate_total();\" class=\"text_boxes numbers\" disabled /></td>
								</tr>";
								
			}
		}
		/*else {
			$i = 1;
			$append_data .= "<tr>
								<td>
									<select name=\"payroll_head[]\" id=\"payroll_head_$i\" class=\"combo_boxes\">";
										foreach( $payroll_heads AS $payroll ) {
											$append_data .=	"<option value=\"$payroll[id]\">$payroll[custom_head]</option>";
										}
			$append_data .=	"		</select>
								</td>
								<td>
									<select name=\"type[]\" id=\"type_$i\" onchange=\"change_type( $i )\" class=\"combo_boxes\">
										<option value=\"%\">%</option>
										<option value=\"Fixed\">Fixed</option>
										<option value=\"Formula\">Formula</option>
									</select>
								</td>
								<td><input type=\"text\" name=\"percentage_formula[]\" id=\"percentage_formula_$i\" onfocus=\"add_formula( $i );\" onblur=\"calculate_amount( $i );\" value=\"\" class=\"text_boxes numbers\" /></td>
								<td>
									<select name=\"base_head[]\" id=\"base_head_$i\" onchange=\"calculate_amount( $i );\" class=\"combo_boxes\">";
										foreach( $payroll_heads AS $payroll ) {
											$append_data .=	"<option value=\"$payroll[id]\">$payroll[custom_head]</option>";
										}
			$append_data .=	"		</select>
								</td>
								<td><input type=\"text\" name=\"amount[]\" id=\"amount_$i\" value=\"0\" onblur=\"calculate_total();\" class=\"text_boxes numbers\" disabled /></td>
							</tr>";
		}*/
		
		echo "$('#salary tbody').html( '" . mysql_real_escape_string( $append_data ) . "' );\n";
		echo "$('#hidden_head_count').val(".$i.");\n";
		//echo "calculate_all()\n";	
		exit();
	}



/*tiffin policy wise shift generate
if($link=="policy_shift_list")
{	
		//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and id='$id' ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
	?>	
    <select name="shift_policy" id="shift_policy" class="combo_boxes" onchange="openmypage_fixed_roster(this.value,'fixed_roster');" > 
        <option value="0">-- Select --</option>
        <?php foreach( $shift_policy AS $policy ) { if( $policy['status_active'] == 1 ) { ?>
        <option value="<?php echo $policy['id']; ?>"><?php echo $policy['shift_name']; ?></option>
        <?php } } ?>
    </select>
    <input type="text" id="txt_date" style="display:none" class="datepicker" name="txt_date" placeholder="Enter Date" size="8" >
      
<?  
exit(); 
}*/

//new get data by ekram-------------//

if ($type==3){														
		
		
		mysql_query( "SET CHARACTER SET utf8" );
		mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
				
		$res = mysql_query("SELECT * from  hrm_employee_docs  where id='".$_GET['getClientId']."'") or die(mysql_error());
		if($inf = mysql_fetch_array($res)){
			echo "formObj.document_name.value = '".mysql_real_escape_string($inf["document_name"])."';\n"; 
			echo "formObj.submitted_copy.value = '".mysql_real_escape_string($inf["submitted_copy"])."';\n";
			echo "formObj.Sub_date.value = '".convert_to_mysql_date(mysql_real_escape_string($inf["submission_date"]))."';\n";
			echo "formObj.save_up.value = '".$inf["id"]."';\n";
			//echo "$('#submitted_copy').val('". $inf['submitted_copy'] ."');\n"; 	emp_code
			
			echo "$('#save_up_document_submit').val( '" . mysql_real_escape_string( $inf['emp_code'] ) . "' );\n";												
			} 
	}




if($type=="discipline_get_data_info")
{
	
	$emp_code = $_GET['getClientId'];
	$id = $_GET['id'];
	
	$sql= "select * from hrm_disciplinary_info_mst where id='$id' and  status_active=1 and is_deleted=0 limit 1";
	$query_result=mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	if($row=mysql_fetch_array($query_result))
	{	 	 	 	 	 	 	 	 	
		echo"$('#txt_occurrence_date').val('$row[occurrence_date]')".";\n";
		echo"$('#txt_action_date').val('$row[action_date]')".";\n";
		echo"$('#txt_occurrence_details').val('$row[occurrence_details]')".";\n";
		echo"$('#txt_investigating_members').val('$row[investigating_members]')".";\n";
		echo"$('#txt_investigation').val('$row[investigation]')".";\n";
		echo"$('#txt_comm_form_date').val('$row[commitee_form_date]')".";\n";
		echo"$('#txt_report_date').val('$row[reporting_date]')".";\n";
		echo"$('#txt_withdawn_date').val('$row[withdrawn_date]')".";\n";
		echo"$('#cbo_action_taken').val('$row[action_taken]')".";\n";
		echo"$('#txt_emp_code_search').val('$row[emp_code]')".";\n";
		echo"$('#save_up').val('$id')".";\n";
		echo"$('#save').val('Update')".";\n";
		
		$sql="select * from hrm_disciplinary_info_dtls where disc_mst_id=$row[id]";
		$query_result=mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$numrows=mysql_num_rows($query_result);
	
			if($row["action_taken"]==1 && $numrows!=0)
				{					
					echo"document.getElementById('td_salary_container').style.visibility='visible';"."\n";					
					if($numrows==1)
							{
								$result=mysql_fetch_array($query_result);
								echo"$('#cbo_impact_salary_head1').val('$result[impact_salary_head]')".";\n";
								echo"$('#txt_impact_salary_head_perc1').val('$result[impact_salary_head_perc]')".";\n";
							}
						else if($numrows>1)
							{								
								$i=1;
								while($result=mysql_fetch_array($query_result))
									{									
										echo "add_sal_head_row(6,".$i.");\n";
										echo"$('#cbo_impact_salary_head".$i."').val('".$result["impact_salary_head"]."')".";\n";
										echo"$('#txt_impact_salary_head_perc".$i."').val('".$result["impact_salary_head_perc"]."')".";\n";
										$i=$i+1;										
									}
									
							}					
					
				}
			else
				{
					echo"document.getElementById('td_salary_container').style.visibility='hidden';"."\n";
				}
				
	}

	exit();
}

if($type=="appointment_letter")
{
	
	/*//salary details 
	$salary_breakdown = array();
	$sql_sal="select * from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date'";
	//echo $sql_sal;die;
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}*/
	
	
	
	?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>	
</head>
<?
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);
	//print_r($emp_code_explode);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++){
		
			mysql_query( "SET CHARACTER SET utf8" );
			mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
		
			$sql="select * from hrm_employee where emp_code='".$emp_code_explode[$i]."'";			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
		while($row=mysql_fetch_array($result)){
			$designation=return_field_value("custom_designation_local","lib_designation","id=$row[designation_id]");
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$house_rent = return_field_value("amount","hrm_employee_salary","payroll_head=2 and emp_code=$row[emp_code]");
			$medical_allowance = return_field_value("amount","hrm_employee_salary","payroll_head=3 and emp_code=$row[emp_code]");
			//$others = return_field_value("amount","hrm_employee_salary","payroll_head=19 and emp_code=$row[emp_code]");
			$others=0;
			$vehicle_allowance = return_field_value("amount","hrm_employee_salary","payroll_head=11 and emp_code=$row[emp_code]");
			//$vehicle_allowance = return_field_value("amount","hrm_employee_salary","payroll_head=4 and emp_code=$row[emp_code]");
			$entertainment_allowance = return_field_value("amount","hrm_employee_salary","payroll_head=13 and emp_code=$row[emp_code]");
			$gross=$basic_salary+$house_rent+$medical_allowance+$vehicle_allowance+$entertainment_allowance+$others;
			$overtime=round(($basic_salary/26)/8,3);
			$overtime=$overtime*2;
	//ob_start()
	?>
	<div align="center" style="width:800">
    	<style type="text/css" media="print">
       	 p{ page-break-after: always;}
    	</style>
    <? ob_start() ?>
	  <table width="700" bgcolor="#FFFFFF" style="font-size:9px">
		<tr>
		  <td width="293" rowspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../resources/images/com_logo.png" width="146" height="66" /> </td>
		  <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+2"><b><u>নিয়োগ পত্র</u></b></font><br />
		<b>APPOINTMENT LETTER</b></td>
		</tr>
		<tr>
			<td width="395" align="left">&nbsp;</td>
		</tr>
		<tr>
			<td width="293"><b>Unit: <? echo $company_details[$row["company_id"]]; ?></b></td>
			<td width="395">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">	
				<table width="100%" style="font-size:9px">
				  <tr>
					<td width="14%">কার্ড নং</td>
					<td width="16%">:<? echo $row["id_card_no"]; ?></td>
					<td width="12%">নামঃ</td>
					<td width="27%" style="font-family:'Kalpurush ANSI'">:<? echo $row["full_name_bangla"]; ?></td>
					<td width="6%">পদবী</td>
					<td width="25%">:<? echo $designation_chart[$row['designation_id']]; ?></td>
				  </tr>
				  <tr>
					<td>গ্রেডে  (শ্রমিক/কর্মচারী)</td>
					<td>:<? echo $row["salary_grade"]; ?></td>
					<td style="font-family:'Kalpurush ANSI'">যোগদানের  তারিখঃ</td>
					<td style="font-family:'Kalpurush ANSI'">:<? echo convert_to_mysql_date($row["joining_date"]); ?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				</table>
			</td>
			
		</tr>		
		<tr>
		  <td><b>চাকুরীর শর্ত ও নিয়মাবলী </b></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td><b>১। বেতন ও ভাতাঃ</b></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<table width="100%" style="font-size:9px">
				<tr>
					<td width="43%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) মূল বেতন (Monthly Basic Pay) </td>
					<td width="34%" style="font-family:'Kalpurush ANSI'">টাকাঃ..................<? echo $basic_salary; ?>...........................</td> 
					<td width="23%" align="center" style=" border:1px solid;font-family:'Kalpurush ANSI'"> প্রতি ঘন্টায় ওভারটাইম</td>          
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ) বাড়ী ভাড়া (House Rent-40% of Basic pay)</td>
					<td style="font-family:'Kalpurush ANSI'">টাকাঃ..................<? echo $house_rent; ?>...........................</td>
				   <td rowspan="3" style="border:1px solid;font-family:'Kalpurush ANSI'"> টাকাঃ....<? echo $overtime; ?>.......</td>  
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) চিকিৎসা ভাতা (Medical Allowance)</td>
					<td style="font-family:'Kalpurush ANSI'">টাকাঃ..................<? echo $medical_allowance; ?>...........................</td>  
			  </tr>
              <tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;চ)যাতায়াত ভাতা(Vehicle Allowance)</td>
					<td style="font-family:'Kalpurush ANSI'">টাকাঃ..................<? echo $vehicle_allowance; ?>...........................</td>  
			  </tr>
              <tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ছ)খাদ্য ভাতা(Entertainment Allowance)</td>
					<td style="font-family:'Kalpurush ANSI'">টাকাঃ..................<? echo $entertainment_allowance; ?>...........................</td>  
			  </tr>
              <tr>
              		<td colspan="2"><hr /></td>
					
              </tr>
              <tr>
              		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; মোট বেতন (Monthly Gross Pay)</td>
					<td style="font-family:'Kalpurush ANSI'">টাকাঃ..................<? echo $gross; ?>......................./=</td>
              </tr>
			</table>
			</td>
		</tr>
		<tr>
		  <td colspan="2"></td>
		  </tr>
		<!--
        <tr>
		  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;মোট বেতন (Monthly Gross Pay)</td>
		  <td>টাকাঃ..................<!? echo $gross; ?>......................./=</td>          
		</tr>
        -->
		<tr>
		  <td colspan="2"><b>২। কর্মঘন্টা ও ওভারটাইমঃ </b></td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) দৈনিক কর্মঘন্টা				: ০৮ ঘন্টা। 		খ) বিরতি : 	প্রতিকর্ম  দিবস/শিফটে ১ ঘন্টা। </td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ) দৈনিক ওভারটাইমঃ শ্রমিকের  সম্মতি  ক্রমে সর্বোচ্চ ০২  ঘন্টা বা সরকারী নিয়ম মোতাবেক। দৈনিক ০৮ ঘন্টার বেশী কাজ ও.টি. হিসাবে  গণ্য  করা হয়। </td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ) ওভারটাইম হিসাবঃ মূল বেতনের দ্বিগুন হারে হিসাব করা হয়। হিসাব মূল বেতন ২০৮×২×মূল ওভারটাইম ঘন্টা।</td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) বেতন প্রদানের সময়ঃ প্রত্যেক মাসের ১ম সপ্তাহে বেতন ও ওভারটাইম একত্রে প্রদান করা হয়।</td>
		  </tr>
		<tr>
		  <td colspan="2"><b>৩। সাধারণ ছুটিঃ</b></td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) সাপ্তাহিক ছুটিঃ সপ্তাহে ০১(এক) দিন সাধারনত  শুক্রবার। </td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ) উৎসবজনিত ছুটিঃ বছরে ১১(এগারো) দিন (পূর্ণ বেতনে)।</td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ) নৈমিত্তিক ছুটিঃ বছরে ১০(দশ) দিন (পূর্ণ বেতনে)।</td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) চিকিৎসা ছুটিঃ বছরে ১৪(চৌদ্দ) দিন।</td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঙ) অর্জিত ছুটিঃ প্রতি ১৮ কর্মদিনের জন্য ১(এক) দিন (পূর্ণ বেতনে) কমপক্ষে ০১ বছর চাকুরী পূর্ণ করলে। সর্বোচ্চ ৪০ দিন পর্যন্ত জমা করা যাবে।</td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;চ) মাতৃত্বকালীন ছুটিঃ ১৬ সপ্তাহ বা ১১২(একশত বারো) দিন (পূর্ণ বেতনে)।মাতৃত্বকালীন আইন, ২০০৬ মোতাবেক।</td>
		  </tr>
		<tr>
		  <td colspan="2"><b>৪। অন্যান্য সুবিধাঃ</b></td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) প্রাথমিক চিকিৎসা সুবিধা প্রদান করা হয়।</td>
		  </tr>
		<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ) অন্যান্য ধর্মালম্বীদের জন্য তাদের নিজ প্রধান ধর্মীয় উৎসবে পর্যন্ত ছুটি প্রদান করা হয়।</td>
		  </tr>
		<tr>
		  <td colspan="2"><b>৫। শর্তাবলীঃ </b></td>
		  </tr>
		<tr>
		  <td colspan="2" align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) আপনাকে ০৩(তিন) মাসের প্রবেশনকালীন সময়ের  জন্য নিয়োগ করা হল। কৃতিত্বের সাথে প্রবেশনকালীন সময় সমাপ্তির পর চাকরী স্থায়ীকরণ  পত্র প্রদান করা হবে এবং আপনি নিয়মিত শ্রমিক/কর্মচারী হিসেবে বিবেচিত হবে। প্রবেশন  পিরিয়ডে কোম্পানী যে কোন সময় কোন প্রকার নোটিশ ছাড়াই আপনার চাকুরীর অবসান করতে  পারেন বা আপনিও চাকুরী থেকে ইস্তফা নিতে পারেন। সেক্ষেত্রে আপনার হাজিরা অনুযায়ী  সকল পাওনা পরিশোধ করা হবে। </td>
		</tr>
		<tr>
		  <td colspan="2" align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ) চাকুরী স্থায়ী হবার পর স্বেচ্ছায় চাকুরী ছেড়ে দিতে চাইলে ৬০(ষাট) দিনের লিখিত নোটিশ বা নোটিশের পরিবর্তে বেতন সমর্পন করতে হবে। অন্যদিকে কর্তৃপক্ষ আপনার চাকুরীর অবসান করতে চাইলে ১২০(একশত বিশ) দিনের লিখিত নোটিশ অথবা নোটিশের পরিবর্তে মজুরী প্রদান করবেন।</td>
		</tr>
		<tr>
		  <td colspan="2" align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ) আপনার চাকুরী কোম্পানী কর্তৃক জারীকৃত বিধি-বিধান ও বাংলাদেশ প্রচলিত শ্রম আইন দ্বারা পরিচালিত হবে। কোম্পানীর যাবতীয় নিয়ম কানুন পরিবর্তনযোগ্য এবং আপনি পরিবর্তিত নিয়ম-কানুন সর্বদা মেনে চলবে বাধ্য থাকবেন। আপনি যদি কখনও কোন রুপ অসদাচরণের অপরাধে দোষী প্রমানিত হন, তবে কর্তৃপক্ষ আইন মোতাবেক আপনাকে চাকুরীচ্যুতিসহ যে কোন শাস্তি দিতে পারবেন।</td>
		  </tr>
		<tr>
		  <td colspan="2" align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) কর্তৃপক্ষ প্রয়োজনবোধে আপনাকে এই প্রতিষ্ঠানের যে কোন বিভাগে বা এই গ্রুপের যে কোন কারখানায়/অফিসে বদলি করতে পারবেন।</td>
		  </tr>
		<tr>
		  <td colspan="2" align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঙ) অত্র প্রতিষ্ঠানের কর্মরত থাকাকালীন সময় অন্য কোথাও প্রত্যেক্ষ বা পরোক্ষভাবে কোন চাকুরী গ্রহন করতে পারবেন না। আপনার চাকুরীর পরি-সমাপ্তি ঘটলে আপনি এই কোম্পানির সমস্ত কাগজপত্র, দলিলাদি অথবা কোন বস্ত আপনার হেফাজতে থাকলে, সেই সকল দ্রব্যাদি আপনি ফেরত দিবেন এবং কোম্পানির ব্যবসা সংক্রান্ত কোন কাগজপত্রের নকল অথবা অংশ বিশেষ আপনার নিকট রাখতে পারবেন না।</td>
		  </tr>
		
        <tr>
		  <td colspan="2" align="right">&nbsp;</td>
		</tr>
        
		<tr>
		  <td colspan="2" align="right"><p>------------------------------------<br />
			নিয়োগকারী(Employer)&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
		  </tr>
		<tr>
		  <td colspan="2">প্রত্যেয়নঃ </td>
		  </tr>
		<tr>
		  <td colspan="2" align="justify" style="font-family:'Kalpurush ANSI'"><p style="font-family:'Kalpurush ANSI'">আমি ......<? echo $row["full_name_bangla"]; ?>.......এই নিয়োগ পত্রের  শর্তাবলী পাঠ করেছি/আমাকে পাঠ করে শুনানো হয়েছে। এতে বর্ণিত শর্তাদি সম্পূর্ণরুপে  অবগত হয়ে, আমি স্বেচ্ছায় ও স্বজ্ঞানে উপরোক্ত শর্তসমূহ মেনে নিয়ে, এই নিয়োগপত্রে  স্বাক্ষর করে অনুলিপি গ্রহন করছি। এবং এসরোটেক্স গ্রুপে....<? echo convert_to_mysql_date($row["joining_date"]); ?>.....তারিখ হতে কাজে  যোগদান করছি। </p></td>
		  </tr>		
		<tr>
		  <td style="font-family:'Kalpurush ANSI'">তারিখ...<? echo convert_to_mysql_date($row["joining_date"]); ?>...</td>
		  <td align="right"><p>------------------------------------<br />প্রার্থীর স্বাক্ষর&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
		</tr>		
		</table>
    <p></p>     
</div><br />
        <?
		}		
		
	}		
		$html_data = ob_get_contents();
		ob_get_clean();
		
		//previous file delete code-----------------------------//
		foreach (glob("../"."*.doc") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
				
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,$html_data);			
		*/
			
			
		echo "$html_data####$filename";
		
		exit();			
		
}
?>
<?
if($type=="appointment_letter_assistant_manager")
{
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++){
			$sql="select *,concat(first_name,' ',middle_name,' ',last_name) as name from hrm_employee where emp_code='".$emp_code_explode[$i]."'";			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			
		while($row=mysql_fetch_array($result)){
			$thana=return_field_value("thana","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$village=return_field_value("village","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$post_code=return_field_value("post_code","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$district_id_bangla=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			
	?>
	<div align="center" style="width:800">
    	<style type="text/css" media="print">
       	 p{ page-break-after: always;}
    	</style>
    <? ob_start() ?>
	  <table width="700" bgcolor="#FFFFFF" style="font-size:13px;">
		<tr>
		  <!--<td width="293" rowspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../../resources/images/asro_logo.png" width="146" height="66" /> </td>-->
		  <td align="center" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+2" style="font-family:'Kalpurush ANSI'"><b><u>নিয়োগ পত্র </u></b></font><br />
		<b>APPOINTMENT LETTER</b></td>
		</tr>
		<tr>
			<td width="395" align="left">&nbsp;</td>
		</tr>
		<tr>
			<td width="293"><b>Ref:</b></td>
          
			<td width="395" align="right">Date: &nbsp; <?php echo convert_to_mysql_date(date("Y-m-d")); ?>&nbsp;</td>
		</tr>
       
		<tr>
			<td colspan="2">	
				<table width="100%" style="font-size:9px">
				  <tr>
					<td>Card No.</td>
					<td><? echo $row["id_card_no"]; ?></td>
					<td>Name:</td>
					<td><? echo $row["name"]; ?></td>
					<td>Post:</td>
					<td><?php echo $designation_chart[$row[designation_level]]; ?></td>
				  </tr>
				  <tr>
					<td>Grade:</td>
					<td><? echo $row["salary_grade"]; ?></td>
					<td>Date Of Joining:</td>
					<td><? echo convert_to_mysql_date($row["joining_date"]); ?></td>
					<td>Village:</td>
					<td><?php echo $village; ?></td>
                  </tr>
                  <tr>
                    <td>Post Office:</td>
					<td><? echo $post_code; ?></td>
					<td>Police Station:</td>
					<td><?php echo $thana; ?></td>
                    
					<td>District:</td>
					<td><?php echo $district_id_bangla;?></td>
				 </tr>
                  
				</table>
		
			
		
                <tr>
                <td><h4>Re: Letter Of Apponment</h4> </td>
                </tr>		
                <tr>
                  <td colspan="6">This has reference to your apponment and subsequent interview and recent dicisions with us and your willingness to join the Company
                      we are pleased to inform you that you have been selected for apponment as  <?php echo $designation_chart[$row[designation_level]]; ?> (<?php echo $department_details[$row[department_id]];?>) with effective from <?php echo convert_to_mysql_date($row[confirmation_date]);?>
                      with the following terms and conditions.</td>
                </tr>
                <tr>
                  <td><b>Terms and Conditions:</b></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                     <td colspan="6">1. you will report to the General Manager (<?php echo $department_details[$row[department_id]];?>)</td>
                </tr>
                <tr>
              		<td colspan="6">2.You will be employeed by the Company ffor service transfarable to any where in bangladesh in other sister concern but currently 
             		 based at the <b><?php echo $company_details[$row[company_id]];?><b></td>
              	</tr>
              	 <tr>
              		 <td colspan="6">3.Yopur Job Responsibilities will attatch with the apponment letter</td>
               	</tr>
                 <tr>
               		<td colspan="6">4.you will be entitled to receive salary of Tk &nbsp;<b><?php echo $row[gross_salary];?></b>(<?php echo $row[gross_salary];?> Thousand Taka )Only Per month</td>
               	</tr>
               	<tr>
               		<td colspan="6">5.your appoinment will be confirmed on Satisfactory Complition of probationary 
              		 period of six(06) months.During the probation period your services may be terminited at any time without assaigning any reason what so ever.</td>
               </tr>
                <tr>
       	        	<td colspan="6">6.During your employement with us  you will devoted the whole of your working hours entirely to the business of the organization and shall put forward 							                     your bestendeavors to uphold and promotion the interest,welfare,image and reputation of <b><?php echo $company_details[$row[company_id]];?><b></td>
               </tr>
               <tr>
               		<td colspan="6">7.you will not during the continuance of your employeement with the company be engaged in any trade,business or occupation,other than the business of the
               		company,nor without the written consent of the company,be directly or indirectly inserted in any other Company or business.</td>
               </tr>
               <tr>
               		<td colspan="6">8.After confirmation, your employeement may be terminited by yourself or by the company upon giving  two (02) months notice to the other in writing.</td>
               </tr>
				 <tr>
               		<td colspan="6">9.You will upon terminited of your employeement ,deliver to the company all property of th ecompany which may be in your possession including 	                    materials,memorandum,notes,photograph,records,plates,sketches,plans or other documents made by you or otherwise.</td>
               </tr>
				<tr>
                	<td colspan="6">10.After the termination of your employeement with the company,you will not for a period of two years for such termination,without the consent in                      writing of the company can be directly or indirectly engagaed in any business relationship with any buyer or customer of <b><?php echo $company_details[$row[company_id]];?><b></td>
                </tr>
                <tr>
                		<td colspan="6">11.Other terms and conditions of service will be in accordance with company policies & regulations which may be altered by the company from time to 	                       time.</td>
                </tr>
                <tr>
                		<td colspan="6">12.you will put forward your best efforts to uphosd and incalculate the cover values cherished by <b><?php echo $company_details[$row[company_id]];?><b>.</td>
                </tr>
                <tr>
                		<td colspan="6">Please confirm your agreement with the contents of this letter and returning each page of the duplicate ci\opy of this letter and report to J M                       Fabrics Ltd,Administration office,where you will be assaigned to your reporting Authority.</td><br />
                <tr>
                		<td colspan="6">                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                     <td colspan="6">Looking forward to welcoming you to our organization and wishing you every success.</td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
               <tr>
                  <td colspan="6">Yours Sincerly</td>
               </tr> 
               <tr>
                <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr> 
                
               <tr>
                <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>  
                <tr>
                <td colspan="6">_ _ _ _ _ _ _ _ _ _ </td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                <td colspan="6">I accept the Terms and conitions as set out above.</td>
                </tr>
                
                <tr>
                <td colspan="6">Name:<?php echo $row["name"]; ?></td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                <td colspan="6">Signature:</td>
                </tr>
                <tr>
                <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                <td colspan="6">Date: &nbsp;<?php echo convert_to_mysql_date(date("Y-m-d")); ?> &nbsp;</td>
                </tr>
 </table>
 <p></p>      
</div><br />
        <?
		}		
		
	}		
		$html_data = ob_get_contents();
		ob_get_clean();
		
		//previous file delete code-----------------------------//
		foreach (glob("../"."*.doc") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
				
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,$html_data);			
		*/
			
			
		echo "$html_data####$filename";
		
		exit();			
		
}

if($type=="appointment_letter_assistant_worker")
{
	
	mysql_query("SET CHARACTER SET utf8");
	mysql_query("SET SESSION collation_connection ='utf8_general_ci'");
	
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++){
			$sql="select *,concat(first_name,' ',middle_name,' ',last_name) as name from hrm_employee where emp_code='".$emp_code_explode[$i]."'";			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			
		while($row=mysql_fetch_array($result)){
			$thana=return_field_value("thana","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$village=return_field_value("village","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$post_code=return_field_value("post_code","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$district_id_bangla=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$house_rent = return_field_value("amount","hrm_employee_salary","payroll_head=2 and emp_code=$row[emp_code]");
			$medical_allowance = return_field_value("amount","hrm_employee_salary","payroll_head=3 and emp_code=$row[emp_code]");
			$others = return_field_value("amount","hrm_employee_salary","payroll_head=4 and emp_code=$row[emp_code]");
			$gross=$basic_salary+$house_rent+$medical_allowance+$others;
			$overtime=round(($basic_salary/26)/8,3);
	?>
    <? ob_start() ?>
<div align="center" style="width:800">
	 <style type="text/css" media="print">
       	 p{ page-break-after: always;}
    	</style>
<table width="700" bgcolor="#FFFFFF" style="font-size:11px;">
		<tr> 
			<td align="right"  colspan="2">&nbsp;<b><?php echo $company_details[$row[company_id]];?><b>&nbsp;<br /><b><?php echo $company_details[$row[plot_no]];?><b> , <b><?php echo $company_details[$row[level_no]];?><b> , <b><?php echo $company_details[$row[road_no]];?><b> , <b><?php echo $company_details[$row[block_no]];?><b><br /><b><?php echo $company_details[$row[province]];?><b> ।</td>
	  </tr>
		 <tr> 
         	<td align="center" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+2" style="font-family:'Kalpurush ANSI'"><b><u>নিয়োগ পত্র</u></b></font><br />
			<b>APPOINTMENT LETTER</b></td>
		</tr>
		<tr>
			<td width="395" align="left">&nbsp;</td>
		</tr>
		<tr>
				<td>সুত্রঃ জে,এম,এফ,এল/মা স- /নি প  </td><td align="right" style="font-family:'Kalpurush ANSI'">তারিখঃ &nbsp; <?php echo convert_to_mysql_date(date("Y-m-d"));?>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>বিষয়ঃ</b></td> 
		</tr>
        <tr>
              <td colspan="2">জনাব/জনাবা&nbsp;<font  style="font-family:'Kalpurush ANSI'"><b><? echo $row["full_name_bangla"]; ?></b></font>&nbsp;আই,ডি নং <? echo $row["id_card_no"]; ?>&nbsp;আপনার&nbsp;<font  style="font-family:'Kalpurush ANSI'"><b><?php echo convert_to_mysql_date(date("Y-m-d"));?></b></font>&nbsp;ইং তারিখের আবেদনপএ এবং তৎুপররতী সাক্ষাৎকারের প্রেক্ষিতে&nbsp;<font  style="font-family:'Kalpurush ANSI'"><b><?php echo convert_to_mysql_date($row[joining_date]);?></b></font>&nbsp;তারিখ হইতে আপনাকে নিন্মোক্ত পদে &nbsp;<b><?php echo $company_details[$row[company_id]];?><b>&nbsp;<?php echo $row[section_id];?>&nbsp;সেকশনে&nbsp;গ্রেড&nbsp;<? echo $row["salary_grade"];?>&nbsp;নিন্মোক্ত শর্ত সাপেক্ষে নিয়োগ প্রদান করা হইল । </td>
              
              
		</tr>
        <tr>
<td colspan="2"><b>শর্তাবলী :</b></td>
</tr>
<tr>
<td colspan="2">০১.যোগদান পরবতী ৩ (তিন)মাস আবেক্ষাকালীন সময় হিসাবে বিবেচিত হইবে।আবেক্ষাকালীন সময় অতিবাহীত হওয়ার পর আপনার নিয়োগকৃ্ত পদে স্হায়ী  কর্মী   হিসাবে বিবেচিত হইবেন।
</td>
</tr>
<tr>
<td colspan="2">০২.আপনার মজুরি/বেতন কাঠামো নিম্নরুপ :-</td>
</tr>
<tr>
	<td colspan="2" style="font-family:'Kalpurush ANSI">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) মূল  বেতন টাকাঃ..................<? echo $basic_salary; ?>.........................../=</td>
</tr>
<tr>
	<td colspan="2" style="font-family:'Kalpurush ANSI">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ) বাড়ী ভাড়া টাকাঃ..................<? echo $house_rent; ?>.........................../=(বাড়ী ভাড়া মূল বেতনের ৪০ %)</td>
	  
</tr>
<tr>
	<td colspan="2" style="font-family:'Kalpurush ANSI">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ) চিকিৎসাভাতা টাকাঃ..................<? echo $medical_allowance; ?>.........................../=(চিকিৎসা ভাতা)</td>
	  
</tr>
<tr>
	<td colspan="2" style="font-family:'Kalpurush ANSI">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) অন্যান্য ভাতা টাকাঃ..................<? echo $others; ?>.........................../=(অন্যান্য ভাতা )</td>
	
</tr>

 <tr>
       <td colspan="2" style="font-family:'Kalpurush ANSI">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঙ) মোট বেতন টাকাঃ..................<? echo $gross; ?>......................./=(মোট বেতন)</td>
</tr>
<tr>
		<td colspan="2">০৩)মাসিক মজুরি/বেতন পরবর্তী মাসের সাত(৭) তারিখের মধ্যে যেকোন  কর্মদিবসের মধ্যে প্রদান করা হইবে।</td>
</tr>
<tr>
		<td colspan="2">০৪)ওভারটাইম নিন্মের পদ্ধতি অনুযায়ী প্রদান করা হইবে:-</td>
</tr>
<tr>
		<td colspan="2">০১)(মুল মজুরি ২ মোট অতিরিক্ত ঘণ্টা )/২০৮</td>
</tr>
<tr>
		<td colspan="2">০২)ওভারটাইম প্রতিদিন কাজের ৮(আট) ঘণ্টা বেশি এবং সাপ্তাহিক ৪৮(আটচল্লিশ)ঘনটার অতিরিক্ত কাজ করার সময়কে বুঝইবে।</td>
</tr>
<tr>
		<td colspan="2">০৩)মজুরি এবং ওভারটাইম টাকা একসাথে প্রদান করা হইবে।</td>
</tr>
<tr>
		<td colspan="2">০৫)আপনাকে একটি পরিচয় পত্র প্রদান করা হইবে এবং আপনাকে সর্বদা  পরিচয়পত্র পরিধান করিতে হইবে।</td>
</tr>
<tr>
		<td colspan="2" align="justify">০৬)চাকুরি হইতে ইস্তেফা দিতে চাইলে স্হায়ী শ্রমিকের ক্ষেএে ৬০(ষাট) দিন এবং অস্হায়ী শ্রমিকের ক্ষেএে ৩০(এিশ) দিনের অগ্রিম নোটিশ প্রদান করিতে হইবে।বিনা নোটিশে চাকরি হইতে ইস্তেফা দিতে চাইলে সে ক্ষেএে প্রদেয় নোটিশের পরিবর্তে  নোটিশ মেয়াদের জন্য মজুরির সমপরিমান অর্থ  কর্তৃপক্ষকে প্রদান করিতে হইবে।</td>
</tr>
<tr>
		<td colspan="2">০৭)কর্তৃপক্ষ কোন স্হায়ী কর্মীর  চাকুরির অবসান করিতে চাইলে অগ্রিম ১২০(একশত বিশ) দিনের লিখিত নোটিশ  অথবা নোটিশের পরিবর্তে  ১২০(একশত বিশ) দিনের মুল মজুরি প্রদান করিবেন।
</td>
</tr>
<tr>
    <td colspan="2"><b>ছুটি নিন্মোক্ত নিয়মে প্রদানযোগ্যঃ</b></td>
    </tr>
<tr>
		 <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) নৈমিত্তিক ছুটিঃ বছরে ১০(দশ) দিন (পূর্ণ বেতনে)। এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ)অসুস্হতাজনিত ছুটি: বছরে ১৪(চৌদদ) দিন (পূর্ণ বেতনে)। এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ) উৎসবজনিত ছুটিঃ বছরে ১১(এগারো) দিন (পূর্ণ বেতনে)।এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) মাতৃত্বকালীন ছুটিঃ ১৬ সপ্তাহ বা ১১২(একশত বারো) দিন (পূর্ণ বেতনে)।
	      তবে  যাদের চাকুরির বয়স ৬(ছয়) মাস  পূর্ণ  হইয়াছে তাহাদের  ক্ষেএে প্রযোজ্য।</td>
</tr>
<tr>
		<td colspan="2">০৮)অর্জিত  ছুটি :একটানা ১২(বার)মাস চাকুরি পূর্ণ  হইলে, পূর্ববছরের  প্রতি ১৮(আঠার)  কর্মদিবসের জন্য ০১(এক) দিন হিসাবে ছুটি পরবর্তী ১২(বার)মাসে ভোগ করা যাইবে।তবে এই ছুটি একসাথে চল্লিশ দিনের বেশি জমা রাখা  যাইবে না।</td>
</tr>
<tr>
		<td colspan="2">০৯)অবেক্ষাকালীন সময়ে আপনার বিরুদ্ধে আনিত যে কোন অনিয়ম/অভিযোগ প্রমান সাপেক্ষে কর্তৃপক্ষ আপনাকে চাকুরি হইতে অব্যাহতি(অবসান সুবিধা ছাড়া) দেওয়ার  ক্ষমতা রাখিবেন</td>
</tr>
<tr>
 		<td colspan="2">১০)প্রতি কর্মদিবসের  মধ্যহ্ন বিরতির হিসাবে ১(এক) ঘণ্টা নামাজ,খাওয়া ও বিশ্রাম জন্য প্রদান করা হইবে।</td>
</tr>
<tr>
		<td colspan="2">১১)কর্তৃপক্ষ প্রয়োজনবোধে আপনাকে এই প্রতিষ্ঠানের যে কোন বিভাগে বা এই গ্রুপের যে কোন কারখানায়/অফিসে বদলি করতে পারবেন।</td>
</tr>


<tr>
<td colspan="2">উপরে বর্ণিত শর্তাদি  যদি আপনার নিকট গ্রহনযোগ্য হয়,তাহা হইলে এই পত্রের অনুলিপিতে  আপনার সম্মতিসূচক স্বাক্ষর প্রদান করার জন্য অনুরোধ করা হইল।</td>
</tr>
<tr>

		<td  colspan="6"><br />অনুমোদনকারীকর্তৃপক্ষ<br />-----------------------------<br />
		  নামঃ<br />
          পদবিঃ
          <div align="right">উপরোক্ত বিষয়গুলো বুঝিয়া স্ব-জ্ঞানে,নিজে প্রনোদিত  স্বাক্ষর<br /></div>
		</td>        
		
 </tr>

<tr>
 		<td colspan="2"><div align="right">-------------------------------------------<br />স্বাক্ষর(প্রাথী/প্রাথীনী)</div></td>
</tr>

</table>
<p></p>
</div><br />
        <?
		}		
		
	}		
		$html_data = ob_get_contents();
		//$html_data=mysql_real_escape_string($html_datas);
		ob_get_clean();
		
		//previous file delete code-----------------------------//
		foreach (glob("../"."*.doc") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
				
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,$html_data);			
		*/
			
			
		echo "$html_data####$filename";
		
		exit();			
		//appoinment letter for worker end-
}

// start application_letter_for_all
if($type=="application_letter_for_all")
{
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	$emp_code = $_GET['getClientId'];
	
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++)
	{
		$sql="select *,concat(first_name,' ',middle_name,' ',last_name) as name from hrm_employee where emp_code='".$emp_code_explode[$i]."'";			
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		while($row=mysql_fetch_array($result))
		{
			$village_bangla=return_field_value("village_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$thana_bangla=return_field_value("thana_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$district_id_bangla=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$post_code_bangla=return_field_value("post_code_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$village_bangla_par=return_field_value("village_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$thana_bangla_par=return_field_value("thana_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$district_id_bangla_par=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$post_code_bangla_par=return_field_value("post_code_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$mobile_number=return_field_value("mobile_no","hrm_employee_address","emp_code=$row[emp_code]");
			$exam_name=return_field_value("exam_name","hrm_employee_education","emp_code=$row[emp_code] order by passing_year desc");
			$emp_reference_name=return_field_value("reference_name ","hrm_employee_reference","emp_code=$row[emp_code]");
			$emp_reference_desig=return_field_value("lib_designation_id","hrm_employee_reference","emp_code=$row[emp_code]");
			$emp_nominee=return_field_value("name","hrm_employee_nominee","emp_code=$row[emp_code]");
			$emp_nominee_relation=return_field_value("relation","hrm_employee_nominee","emp_code=$row[emp_code]");
			$emp_nominee_address=return_field_value("address","hrm_employee_nominee","emp_code=$row[emp_code]");

			$sql_family="select count(id) as total from hrm_employee_family where emp_code=$row[emp_code] and relation in(3,4) and status_active=1 and is_deleted=0";
			//echo $sql_family;die;
			$result_family=mysql_query($sql_family);
			$row_family=mysql_fetch_array($result_family);
			
			$sql_reference="select concat(first_name,' ',middle_name,' ',last_name) as name,designation_id from hrm_employee where emp_code='".$row['emp_ref_code']."'";			
			$result_reference = mysql_query( $sql_reference ) or die( $sql_reference . "<br />" . mysql_error() );
			$row_reference=mysql_fetch_array($result_reference);
			
			$sql_photo="SELECT location FROM resource_photo WHERE identifier=$row[emp_code] and type=0 and is_deleted=0";
			//echo $sql_photo;die;
			$result_photo=mysql_query($sql_photo);
			$row_photo=mysql_fetch_array($result_photo);
			//echo $sql_photo;die;
			
			//$emp_org_exp=return_field_value("organization_name ","hrm_employee_experience","emp_code=$row[emp_code]");
			$marital_status=return_field_value("marital_status","hrm_employee","emp_code=$row[emp_code]");
			if($marital_status==0){
			$status='অবিবাহিত';
			}
			if($marital_status==1){
			$status='বিবাহিত';
			}
			if($marital_status==2){
			$status='তালাকপ্রাপ্ত';
			}
			if($marital_status==3){
			$status='বিধবা';
			}
	
			$dob=$row[dob];
			$dob_split=explode("-",$dob);
			$dob_1=$dob_split[0];	
			
			$joining_date=$row[joining_date];
			$joining_date_split=explode("-",$joining_date);
			$joining_date_1=$joining_date_split[0];
			$age=(($joining_date_1*1)-($dob_1*1));
			?>
            <div style="width:900px;">
				<style type="text/css" media="print">
					p{ page-break-after: always;}
                </style>
            <?php ob_start() ?>
            <!--
            <table border="1" width="880px">
            	<tr>
                	<td>&nbsp;</td>
                </tr>
            </table>
            -->
            <table align="left"  width="100%" style="font-size:15px;" >
                <tr><td align="center" colspan="2">চাকুরীর আবেদন পত্র </td></tr>
                <tr> 
                    <td>&nbsp;</td>
                    <!--<td rowspan="6" align="center"><img src='../../../resources/photo/HRM_000123_0_19307.jpg' width="130px" height="150px"/></td>-->
                    <td rowspan="6" align="center"><img src='../../../<?php echo $row_photo[location];?>' width="130px" height="150px"/></td>
                </tr>
                <tr> 
                    <td colspan="2">তারিখঃ ........../........../..........</td>
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                    <td colspan="2">বরাবর<br />ব্যবস্হাপনা পরিচালক <br /><?php echo $company_details[$row[company_id]];?>.<!--জে এম ফেব্রিকস লিঃ ইউনিট<br/>দক্ষিন নয়াপাড়া,৬নং ডগরী,ভাওয়াল ,মির্জাপুর,<br/>গাজীপুর ।</td>-->
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
					<?php
						if($designation_chart_local[$row['designation_id']]=='')
						{ 
							?>
							<td colspan="2">বিষয়ঃ..........<? echo $designation_chart[$row['designation_id']];?>........... পদে চাকুরীর জন্য আবেদন ।</td>
							<?
						}
						else
						{ 
							?>
							<td colspan="2">বিষয়ঃ..........<? echo $designation_chart_local[$row['designation_id']];?>........... পদে চাকুরীর জন্য আবেদন ।</td>
							<?php 
						} 
					?>
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">জনাব ,<br />জনাব সবিনয়  নিবেদন এই যে ,আমি বিশ্বস্ত সুত্রে জানিতে পারিলাম যে ,আপনার স্বনামধন্য রপ্তানিমুখী পোশাক শিল্প প্রতিষ্ঠানে বর্ণিত পদে কিছু সংখ্যক লোক নিয়োগ করা হবে । আমি উক্ত পদে একজন প্রার্থী হিসাবে আমার পূর্ণ জীবন বৃত্তান্ত ও কাজের অভিজ্ঞতা সহ সংশ্লিষ্ট বিষয়াদি আপনার  বিবেচনাথে  পেশ করিলাম।</td>
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <!--</table>
                <table align="left"  width="70%" style="font-size:15px;">-->
                <tr>
                    <td colspan="2">নাম :&nbsp;&nbsp;<?php if($row["full_name_bangla"]==''){echo  $row["name"];}else{ echo  "<font style='font-family:Kalpurush ANSI'>".$row["full_name_bangla"]."</font>";}?></td>
                </tr>
                <tr>
                    <td>পিতা/স্বামীর নামঃ&nbsp;&nbsp;<? if($row["father_name_bangla"]==''){echo $row["father_name"];}else{ echo  "<font style='font-family:Kalpurush ANSI'>".$row["father_name_bangla"]."</font>";} ?></td>
                    <td>মাতার নামঃ&nbsp;&nbsp;<? if($row["mother_name_bangla"]==''){echo $row["mother_name"];}else{ echo  "<font style='font-family:Kalpurush ANSI'>".$row["mother_name_bangla"]."</font>";} ?></td>
                </tr>
                <tr>
                    <td colspan="2">বর্তমান  ঠিকানাঃ </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ্রামঃ&nbsp;&nbsp;<? echo $village_bangla; ?></td>
                    <td>ডাকঘরঃ&nbsp;&nbsp;<? echo $post_code_bangla; ?>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;থানাঃ&nbsp;&nbsp;<? echo $thana_bangla; ?></td>
                    <td>জেলাঃ&nbsp;&nbsp;<? echo $district_id_bangla; ?></td>
                </tr>
                <tr>
                    <td colspan="2">স্হায়ী ঠিকানাঃ</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ্রামঃ&nbsp;&nbsp;<?php echo $village_bangla ?></td>
                    <td>ডাকঘরঃ&nbsp;&nbsp;<? echo $post_code_bangla; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;থানাঃ&nbsp;&nbsp;<? echo $thana_bangla_par; ?></td>
                    <td>জেলাঃ&nbsp;&nbsp;<? echo $district_id_bangla; ?></td>
                </tr>
                <tr>
                    <td colspan="2">মোবাইল নং:&nbsp;&nbsp;<? echo "<font style='font-family:Kalpurush ANSI'>".$mobile_number."</font>" ?></td>
                </tr>
                <tr>
                    <td>জন্ম তারিখঃ&nbsp;&nbsp;<? echo "<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($row[dob])."</font>" ?></td>
                    <td>বয়সঃ&nbsp;&nbsp;<? echo "<font style='font-family:Kalpurush ANSI'>".$age."</font>" ?>&nbsp;বছর </td>
                </tr>
                <tr>
                    <td>জাতীয়তাঃ&nbsp;&nbsp;<? echo $row[nationality]; ?></td>
                    <td>ধর্ম:&nbsp;&nbsp;<? echo $row[religion];?></td>
                </tr>
                <tr>
                    <td colspan="2">বৈবাহিক অবস্থাঃ&nbsp;&nbsp;<? echo $status; ?></td>
                </tr>
                <tr>
                    <td colspan="2">জীবিত সন্তানের সংখ্যা ( মহিলাদের ক্ষেত্রে প্রযোজ্য ): &nbsp;&nbsp;<? echo $row_family['total']; ?></td>
                </tr>
                <tr>
                    <td colspan="">জীবন বীমা নমিনীর নামঃ&nbsp;&nbsp;<? echo $emp_nominee; ?></td>
                    <td colspan="">সম্পর্কঃ&nbsp;&nbsp;<? echo $emp_nominee_relation; ?></td>
                </tr>
                <tr>
                    <td colspan="2">জীবন বীমা নমিনীর ঠিকানাঃ&nbsp;&nbsp;<? echo $emp_nominee_address; ?></td>
                </tr>
                <tr>
                    <td colspan="2">পূর্ব   অভিজ্ঞতা :</td>
                </tr>
            </table>
            <div align="left">
                <table align="left" width="100%" cellpadding="0" cellspacing="0" border="1">
                    <?php 
                        $tab="<tr align='center'>
                            <td width='50'>নং</td>
                            <td>কারখানার নাম</td>
                            <td>কারখানার অবস্থান</td>
                            <td>পদবী</td>
                            <td>চাকুরীর সময়কাল</td>
                            <td>বেতন</td>
                        </tr>"; 
                    ?>
                    <!--
                    <tr align="center">
                    <td width="50">নং</td>
                    <td>কারখানার নাম </td>
                    <td>কারখানার অবস্থান </td>
                    <td>পদবী </td>
                    <td>চাকুরীর সময়কাল </td>
                    <td>বেতন </td>
                    </tr>
                    -->
                    <?php
                    $c=1;
                    $sql_ex="select * from hrm_employee_experience where emp_code='$row[emp_code]' and status_active=1 and is_deleted=0";
                    $res=mysql_query($sql_ex);
                    if(mysql_num_rows($res) > 0)
                    {
                        echo $tab; 
                        while($row_ex=mysql_fetch_array($res))
                        {
                            ?>
                            <tr>
                            <td width="50"><? echo $c; ?></td>
                            <td width="200"><? echo $row_ex[organization_name]; ?></td>
                            <td width="200">&nbsp;</td>
                            <td width="200"><? echo $row_ex[designation]; ?></td>
                            <td width="200"><? echo $row_ex[sevice_length]; ?></td>
                            <td align="right"><? echo $row_ex[gross_salary]; ?></td>
                            </tr>
                            <?php	
                            $c++;
                        }
                    }
                    ?>
                </table>
            </div>
            <div align="left"> 
                <table align="left" width="100%" border="0">
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="">শিক্ষাগত যোগ্যতাঃ&nbsp;&nbsp;<? echo $exam_name; ?></td>
                    </tr>
                    <tr>
                        <td>রেফারেন্স মাধ্যমঃ&nbsp;&nbsp;নামঃ&nbsp;&nbsp;<? echo $row_reference['name'];?>&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;পদবীঃ&nbsp;&nbsp;<? echo $designation_chart[$row_reference['designation_id']]; ?></td>
                    </tr>
                    <tr> 
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr> 
                        <td colspan="2">
                        প্রদত্ত জীবন বৃত্তান্ত বিবেচনা করে আমাকে উক্ত পদে নিয়োগ দেয়ার জন্য মহোদয়ের নিকট অনুরোধ জানাচ্ছি এবং নিয়োগ পেলে প্রতিষ্ঠানের সকল নিয়ম কানুনের প্রতি শ্রদ্ধাশীল ও অনুগত থাকার অঙ্গীকার করছি । 
                        </td>
                    </tr>
                    <tr> 
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">নিবেদক/নিবেদিকা</td>
                    </tr>
                    <tr height="80">
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-decoration:overline;">আবেদনকারীর স্বাক্ষর ও তারিখ</td>
                        <td style="text-decoration:overline;">সত্যায়নকারীর স্বাক্ষর</td>
                    </tr>
                </table>
            </div>
            <p></p>
            </div><br />
        	<?php
		}
	}		
	$html_data = ob_get_contents();
	ob_get_clean();
	
	//previous file delete code-----------------------------//
	foreach (glob("../"."*.doc") as $filename) 
	{
		//if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
			
	/*
	$name=time();
	$filename=$name.".doc";
	$create_new_doc = fopen("../".$filename, 'w');	
	$is_created = fwrite($create_new_doc,$html_data);			
	*/
		
	echo "$html_data####$filename";
	exit();			
	//appoinment letter for all end-
}



//new
// start candidate application letter
if($type=="candidate_app_letter")
{
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++)
	{
		$sql="select *,concat(first_name,' ',middle_name,' ',last_name) as name from eval_general_info where id='".$emp_code_explode[$i]."'";			
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		while($row=mysql_fetch_array($result))
		{
			$village_bangla=return_field_value("village_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$thana_bangla=return_field_value("thana_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$district_id_bangla=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$post_code_bangla=return_field_value("post_code_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=0");
			$village_bangla_par=return_field_value("village_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$thana_bangla_par=return_field_value("thana_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$district_id_bangla_par=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$post_code_bangla_par=return_field_value("post_code_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$mobile_number=return_field_value("mobile_no","hrm_employee_address","emp_code=$row[emp_code]");
			$exam_name=return_field_value("exam_name","hrm_employee_education","emp_code=$row[emp_code] order by passing_year desc");
			$emp_reference_name=return_field_value("reference_name ","hrm_employee_reference","emp_code=$row[emp_code]");
			$emp_reference_desig=return_field_value("lib_designation_id","hrm_employee_reference","emp_code=$row[emp_code]");
			$emp_nominee=return_field_value("name","hrm_employee_nominee","emp_code=$row[emp_code]");
			$emp_nominee_relation=return_field_value("relation","hrm_employee_nominee","emp_code=$row[emp_code]");
			$emp_nominee_address=return_field_value("address","hrm_employee_nominee","emp_code=$row[emp_code]");

			$sql_family="select count(id) as total from hrm_employee_family where emp_code=$row[emp_code] and relation in(3,4) and status_active=1 and is_deleted=0";
			//echo $sql_family;die;
			$result_family=mysql_query($sql_family);
			$row_family=mysql_fetch_array($result_family);
			
			$sql_reference="select concat(first_name,' ',middle_name,' ',last_name) as name,designation_id from hrm_employee where emp_code='".$row['emp_ref_code']."'";			
			$result_reference = mysql_query( $sql_reference ) or die( $sql_reference . "<br />" . mysql_error() );
			$row_reference=mysql_fetch_array($result_reference);
			
			$sql_photo="SELECT location FROM resource_photo WHERE identifier=$row[emp_code] and type=0 and is_deleted=0";
			//echo $sql_photo;die;
			$result_photo=mysql_query($sql_photo);
			$row_photo=mysql_fetch_array($result_photo);
			//echo $sql_photo;die;
			
			//$emp_org_exp=return_field_value("organization_name ","hrm_employee_experience","emp_code=$row[emp_code]");
			$marital_status=return_field_value("marital_status","hrm_employee","emp_code=$row[emp_code]");
			if($marital_status==0){
			$status='অবিবাহিত';
			}
			if($marital_status==1){
			$status='বিবাহিত';
			}
			if($marital_status==2){
			$status='তালাকপ্রাপ্ত';
			}
			if($marital_status==3){
			$status='বিধবা';
			}
	
			$dob=$row[dob];
			$dob_split=explode("-",$dob);
			$dob_1=$dob_split[0];	
			
			$joining_date=$row[joining_date];
			$joining_date_split=explode("-",$joining_date);
			$joining_date_1=$joining_date_split[0];
			$age=(($joining_date_1*1)-($dob_1*1));
			?>
            <div style="width:900px;">
				<style type="text/css" media="print">
					p{ page-break-after: always;}
                </style>
            <?php ob_start() ?>
            <!--
            <table border="1" width="880px">
            	<tr>
                	<td>&nbsp;</td>
                </tr>
            </table>
            -->
            <table align="left"  width="100%" style="font-size:15px;" >
                <tr><td align="center" colspan="2">চাকুরীর আবেদন পত্র </td></tr>
                <tr> 
                    <td>&nbsp;</td>
                    <!--<td rowspan="6" align="center"><img src='../../../resources/photo/HRM_000123_0_19307.jpg' width="130px" height="150px"/></td>-->
                    <td rowspan="6" align="center"><img src='../../../<?php echo $row_photo[location];?>' width="130px" height="150px"/></td>
                </tr>
                <tr> 
                    <td colspan="2">তারিখঃ ........../........../..........</td>
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                    <td colspan="2">বরাবর<br />ব্যবস্হাপনা পরিচালক /মহাব্যবস্হাপক/কারখানা ব্যবস্হাপক<br /><?php echo $company_details[$row[company_id]];?>.<!--জে এম ফেব্রিকস লিঃ ইউনিট<br/>দক্ষিন নয়াপাড়া,৬নং ডগরী,ভাওয়াল ,মির্জাপুর,<br/>গাজীপুর ।</td>-->
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
					<?php
						if($designation_chart_local[$row['designation_id']]=='')
						{ 
							?>
							<td colspan="2">বিষয়ঃ..........<? echo $designation_chart[$row['designation_id']];?>........... পদে চাকুরীর জন্য আবেদন ।</td>
							<?
						}
						else
						{ 
							?>
							<td colspan="2">বিষয়ঃ..........<? echo $designation_chart_local[$row['designation_id']];?>........... পদে চাকুরীর জন্য আবেদন ।</td>
							<?php 
						} 
					?>
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">জনাব ,<br />সবিনয়  নিবেদন এই যে ,আমি আপনার প্রতিষ্ঠানের উপরেউল্লেখিত পদে নিয়োগ পাওয়ার জন্য আবেদন করছি এবং এই লক্ষ্যে নিচে আমার জীবন বৃওান্ত প্রদান করছি । </td>
                </tr>
                <tr> 
                    <td colspan="2">&nbsp;</td>
                </tr>
                <!--</table>
                <table align="left"  width="70%" style="font-size:15px;">-->
                <tr>
                    <td colspan="2">১ । নাম :&nbsp;&nbsp;<?php if($row["name_bangla"]==''){echo  $row["name"];}else{ echo  "<font style='font-family:Kalpurush ANSI'>".$row["name_bangla"]."</font>";}?></td>
                </tr>
                <tr>
                    <td>২ । পিতা/স্বামীর নামঃ&nbsp;&nbsp;<? if($row["father_name_bangla"]==''){echo $row["father_name"];}else{ echo  "<font style='font-family:Kalpurush ANSI'>".$row["father_name_bangla"]."</font>";} ?></td>
                    <td>৩ । মাতার নামঃ&nbsp;&nbsp;<? if($row["mother_name_bangla"]==''){echo $row["mother_name"];}else{ echo  "<font style='font-family:Kalpurush ANSI'>".$row["mother_name_bangla"]."</font>";} ?></td>
                </tr>
                <tr>
                    <td colspan="2">৪ । বর্তমান  ঠিকানাঃ </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ্রামঃ&nbsp;&nbsp;<? echo $village_bangla; ?></td>
                    <td>ডাকঘরঃ&nbsp;&nbsp;<? echo $post_code_bangla; ?>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;থানাঃ&nbsp;&nbsp;<? echo $thana_bangla; ?></td>
                    <td>জেলাঃ&nbsp;&nbsp;<? echo $district_id_bangla; ?></td>
                </tr>
                <tr>
                    <td colspan="2">৫ । স্হায়ী ঠিকানাঃ</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ্রামঃ&nbsp;&nbsp;<?php echo $village_bangla ?></td>
                    <td>ডাকঘরঃ&nbsp;&nbsp;<? echo $post_code_bangla; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;থানাঃ&nbsp;&nbsp;<? echo $thana_bangla_par; ?></td>
                    <td>জেলাঃ&nbsp;&nbsp;<? echo $district_id_bangla; ?></td>
                </tr>
                <tr>
                    <td colspan="2">৬ । প্রার্থীর মোবাইল নং:&nbsp;&nbsp;<? echo "<font style='font-family:Kalpurush ANSI'>".$mobile_number."</font>" ?></td>
                </tr>
                <tr>
                    <td>৭ । জন্ম তারিখ/বয়সঃ&nbsp;&nbsp;<? echo "<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($row[dob])."</font>" ?></td>
                    <td>বয়সঃ&nbsp;&nbsp;<? echo "<font style='font-family:Kalpurush ANSI'>".$age."</font>" ?>&nbsp;বছর </td>
                </tr>
                <tr>
                    <td>৮ । জাতীয়তাঃ&nbsp;&nbsp;<? echo $row[nationality]; ?></td>
                    <td>৯ । ধর্ম:&nbsp;&nbsp;<? echo $row[religion];?></td>
                </tr>
                <tr>
                        <td colspan="2">১০ । শিক্ষাগত যোগ্যতাঃ&nbsp;&nbsp;<? echo $exam_name; ?></td>
                  </tr>
                <tr>
                    <td colspan="2">১১ । বৈবাহিক অবস্থাঃ&nbsp;&nbsp;<? echo $status; ?></td>
                </tr>
                <tr>
                    <td colspan="2">১২ । জীবিত সন্তানের সংখ্যা ( মহিলাদের ক্ষেত্রে প্রযোজ্য ): &nbsp;&nbsp;<? echo $row_family['total']; ?></td>
                </tr>
                <tr>
                    <td colspan="">নমিনীর নামঃ&nbsp;&nbsp;<? echo $emp_nominee; ?></td>
                    <td colspan="">সম্পর্কঃ&nbsp;&nbsp;<? echo $emp_nominee_relation; ?></td>
                </tr>
                <tr>
                    <td colspan="2">নমিনীর ঠিকানাঃ&nbsp;&nbsp;<? echo $emp_nominee_address; ?></td>
                </tr>
                <tr>
                    <td colspan="2">পূর্ব   অভিজ্ঞতা :</td>
                </tr>
            </table>
            <div align="left">
                <table align="left" width="100%" cellpadding="0" cellspacing="0" border="1">
                    <?php 
                        $tab="<tr align='center'>
                            <td width='50'>নং</td>
                            <td>কারখানার নাম</td>
                            <td>কারখানার অবস্থান</td>
                            <td>পদবী</td>
                            <td>চাকুরীর সময়কাল</td>
                            <td>বেতন</td>
                        </tr>"; 
                    ?>
                    <!--
                    <tr align="center">
                    <td width="50">নং</td>
                    <td>কারখানার নাম </td>
                    <td>কারখানার অবস্থান </td>
                    <td>পদবী </td>
                    <td>চাকুরীর সময়কাল </td>
                    <td>বেতন </td>
                    </tr>
                    -->
                    <?php
                    $c=1;
                    $sql_ex="select * from hrm_employee_experience where emp_code='$row[emp_code]' and status_active=1 and is_deleted=0";
                    $res=mysql_query($sql_ex);
                    if(mysql_num_rows($res) > 0)
                    {
                        echo $tab; 
                        while($row_ex=mysql_fetch_array($res))
                        {
                            ?>
                            <tr>
                            <td width="50"><? echo $c; ?></td>
                            <td width="200"><? echo $row_ex[organization_name]; ?></td>
                            <td width="200">&nbsp;</td>
                            <td width="200"><? echo $row_ex[designation]; ?></td>
                            <td width="200"><? echo $row_ex[sevice_length]; ?></td>
                            <td align="right"><? echo $row_ex[gross_salary]; ?></td>
                            </tr>
                            <?php	
                            $c++;
                        }
                    }
                    ?>
                </table>
            </div>
            <div align="left"> 
                <table align="left" width="100%" border="0">
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="">শিক্ষাগত যোগ্যতাঃ&nbsp;&nbsp;<? echo $exam_name; ?></td>
                    </tr>
                    <tr>
                        <td>রেফারেন্স মাধ্যমঃ&nbsp;&nbsp;নামঃ&nbsp;&nbsp;<? echo $row_reference['name'];?>&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;পদবীঃ&nbsp;&nbsp;<? echo $designation_chart[$row_reference['designation_id']]; ?></td>
                    </tr>
                    <tr> 
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr> 
                        <td colspan="2">
                        প্রদত্ত জীবন বৃত্তান্ত বিবেচনা করে আমাকে উক্ত পদে নিয়োগ দেয়ার জন্য মহোদয়ের নিকট অনুরোধ জানাচ্ছি এবং নিয়োগ পেলে প্রতিষ্ঠানের সকল নিয়ম কানুনের প্রতি শ্রদ্ধাশীল ও অনুগত থাকার অঙ্গীকার করছি । 
                        </td>
                    </tr>
                    <tr> 
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">নিবেদক/নিবেদিকা</td>
                    </tr>
                    <tr height="80">
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-decoration:overline;">আবেদনকারীর স্বাক্ষর ও তারিখ</td>
                        <td style="text-decoration:overline;">সত্যায়নকারীর স্বাক্ষর</td>
                    </tr>
                </table>
            </div>
            <p></p>
            </div><br />
        	<?php
		}
	}		
	$html_data = ob_get_contents();
	ob_get_clean();
	
	//previous file delete code-----------------------------//
	foreach (glob("../"."*.doc") as $filename) 
	{
		//if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
			
	/*
	$name=time();
	$filename=$name.".doc";
	$create_new_doc = fopen("../".$filename, 'w');	
	$is_created = fwrite($create_new_doc,$html_data);			
	*/
		
	echo "$html_data####$filename";
	exit();			
	//appoinment letter for all end-
}



if($type=="appointment_letter_assistant_officer")
{ //apponment leeter for assistant officer start
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++){
			$sql="select *,concat(first_name,' ',middle_name,' ',last_name) as name from hrm_employee where emp_code='".$emp_code_explode[$i]."'";			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			
		while($row=mysql_fetch_array($result)){
			$thana=return_field_value("thana","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$village=return_field_value("village","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$post_code=return_field_value("post_code","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$district_id_bangla=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			
	?>
<div align="center" style="width:800">
	<style type="text/css" media="print">
       	 p{ page-break-after: always;}
    	</style>
    <? ob_start() ?>
	  <table width="700" bgcolor="#FFFFFF" style="font-size:15px;">
     <tr>
    
     
			 <td colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+2" style="font-family:'Kalpurush ANSI'"><b><u>নিয়োগ পত্র</u></b></font><br />
			<b>APPOINTMENT LETTER</b></td>
        </tr>
        <tr>
        	<td colspan="4" align="right">Date:&nbsp;<?php echo convert_to_mysql_date(date("Y-m-d")); ?>&nbsp;</td>
            </tr>
		<tr>
		  <td>Name </td>
		  <td>:</td>
          <td width="575"><?php echo $row["name"];?></td>
		</tr>
		<tr>
			<td>Village</td>
            <td>:</td>
            <td><?php echo $village;?></td>
		</tr>
		<tr>
			<td>Post Office</td>
            <td>:</td>
            <td><?php echo $post_code;?></td>
		</tr>
        <tr>
			<td>Police Station</td>
            <td>:</td>
            <td><?php echo $thana;?></td>
		</tr>
        <tr>
			<td>District</td>
            <td>:</td>
            <td><?php echo $district_id_bangla;?></td>
		</tr>
        <tr>
           <td>        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="3"><b>SUBJECT:&nbsp;&nbsp;&nbsp;LETTER OF APPOINMENT</b></td>
       	</tr>
        <tr>
        	<td>Dear,</td>
        </tr>
        <tr>
        		<td colspan="4" align="justify">with reference to your application dated <?php echo convert_to_mysql_date(date("Y-m-d"));?> and the subsequent Interview with us,the management has considered employeeing you in the position of &nbsp;"<?php echo $designation_chart[$row['id']];?>"&nbsp;(Intimate) at <b><?php echo $company_details[$row[company_id]];?><b></td>
        </tr>
        <tr>
       	   		<td colspan="4"><b>You will join on/before <?php echo convert_to_mysql_date($row[joining_date]);?> under the following terms and conditions:</b></td>
        </tr>
		<tr>
       		 <td colspan="3">1.Your Consolidated salary will be TK &nbsp;<b><?php echo $row[gross_salary];?></b>&nbsp;(<?php echo $row[gross_salary];?> Thousand Only)&nbsp; per month.</td>
        </tr>
        <tr>
         	 <td colspan="4">2.The company may from time to time change your job or assaign you with different responsibilities without affecting your monthly consolidated salary and it will be your obligation to accept such change or assaignment any objection</td>
        </tr>
        
        <tr>
        	<td colspan="4">3.you will have to work according to the instructions of your superiors and shall perform your duties as per job description/direction to be given you from time to time.</td>
        </tr>
        <tr>
        	<td colspan="4">4.you will be probotion for a period of 6(six) month which may be extended.If your services are not found satisfactory during probationary period your services may be terminated without notice.</td>
        </tr> 
        <tr>
        	<td colspan="4">5.The  appoinment may be terminate by either party by giving two month's notice or pay in lieu there of after confirmation. 
        </tr>
        <tr>
        	<td colspan="4">6.All other terms of employeement not covered here in shall be according with the rules and regulations of the company in force from time to time.</td> 
        </tr>
        <tr>
        	<td colspan="3">7.If you breach any of the terms of this aggerement  or found guilty of misconduct including dishonest,absence without leave,disobedient to the lawfull orders or instruction etc,then you will be liable to disciplinary action as per relatiove law.</td>
        </tr>
        <tr>
        	<td colspan="4">8.Your position may be changed to any other unit of the company in the best interest of the management.</td>
        </tr>
        <tr>
        	<td colspan="4">If the offer is acceptable to you on the above terms and conditions please affix your signature in attention there of in the original copy of this letter.</td>
        </tr>
        <tr>
        <td colspan="4"></br><h5>Yours Faithfully</h5></td>
        </tr>
        
        <tr>
        <td colspan="3">_________________<br/>Manager-HR</td><td align="left">_________________<br/>Accept</td>
        
        </tr>
        
</table>
	<p></p>
</div><br />
        <?
		}		
		
	}		
		$html_data = ob_get_contents();
		ob_get_clean();
		
		//previous file delete code-----------------------------//
		foreach (glob("../"."*.doc") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
				
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,$html_data);			
		*/
			
			
		echo "$html_data####$filename";
		
		exit();			
		//apponment leeter for assistant officer end--
}
if($type=="appointment_letter_assistant_staff")
{ //<!--apponment letter for Staff-->
	
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );

	
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++){
			$sql="select *,concat(first_name,' ',middle_name,' ',last_name) as name from hrm_employee where emp_code='".$emp_code_explode[$i]."'";			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			
		while($row=mysql_fetch_array($result)){
			$thana=return_field_value("thana","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$village=return_field_value("village","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$post_code=return_field_value("post_code","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$district_id_bangla=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			
	?>
<div align="center" style="width:800">
	<style type="text/css" media="print">
       	 p{ page-break-after: always;}
    	</style>
 <? ob_start() ?>

<table width="700" bgcolor="#FFFFFF" style="font-size:12px;">
		<tr align="right"> 
			<td colspan="2">জে এম ফেব্রিকস লিঃ ইউনিট <br />দক্ষিন নয়াপাড়া,৬নং ডগরী ,ভাওয়াল,মির্জাপুর <br />গাজীপুর ।</td>
        </tr><br />
	<tr>
		  <!--<td width="293" rowspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../resources/images/asro_logo.png" width="146" height="66" /> </td>-->
		  <td align="center" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+2" style="font-family:'Kalpurush ANSI'"><b><u>নিয়োগ পত্র</u></b></font><br />
		<b>APPOINTMENT LETTER</b></td>
	</tr>
    <tr>
			<td width="395" align="left">&nbsp;</td>
	</tr>
	<tr>
			<td>সুত্রঃ জে,এম,এফ,এল/মা স- /নি প</td>         								    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td align="right" style="font-family:'Kalpurush ANSI'">তারিখঃ &nbsp;<?php echo convert_to_mysql_date(date('Y-m-d'));?>&nbsp;</td>
	</tr>
	<tr>
			<td  colspan="2"><font size="+1"><b><u>বিষয়ঃ নিয়োগ পএ</u></b></font></td> 
	</tr>
    <tr>
    	<td colspan="2">জনাব/জনাবা<br /> &nbsp;<font style="font-family:'Kalpurush ANSI'"> <?php echo  $row["full_name_bangla"];?></font>&nbsp; আপনার &nbsp;<font style="font-family:'Kalpurush ANSI'"><?php echo convert_to_mysql_date(date("Y-m-d"));?></font>&nbsp; ইং তারিখের আবেদনপএ এবং তৎপরবর্তী  সাক্ষাৎকারের 
         প্রেক্ষিতে &nbsp; <font style="font-family:'Kalpurush ANSI'"><?php echo convert_to_mysql_date($row[confirmation_date]);?></font>&nbsp;ইং তারিখ হইতে আপনাকে <?php echo $designation_chart[$row['id']];?> পদে &nbsp;<b><?php echo $company_details[$row[company_id]];?><b> &nbsp;<?php echo $row[section_id];?>&nbsp;সেকশনে &nbsp;<? echo $row["salary_grade"];?>&nbsp;গ্রেডে নিম্নোক্ত শর্ত  সাপেক্ষে  নিয়োগ  প্রদান করা হইল । 
     </td>
                
	</tr>
    <tr>
		<td colspan="2">শর্তাবলী </td>
	</tr>
    <tr>
		<td colspan="2">০১.যোগদান পরবর্তী  ৩ (তিন)মাস আবেক্ষাকালীন সময় হিসাবে বিবেচিত হইবে।আবেক্ষাকালীন সময় অতিবাহীত হওয়ার পর আপনার নিয়োগকৃ্ত পদে স্হায়ী কর্মী  হিসাবে বিবেচিত হইবেন।</td>
	</tr>
    <tr>
    	<td colspan="2">০২.আপনার  সর্বমোট  বেতন <font style="font-family:'Kalpurush ANSI'"><?php echo $row["gross_salary"];?></font>...../টাকা(কথায়ঃ &nbsp; <font style="font-family:'Kalpurush ANSI'"><?php echo $row["gross_salary"];?></font>...... হাজার টাকা  )</td> 
    </tr>
    <tr>
		<td colspan="2">০৩)মাসিক মজুরি/বেতন পরবর্তী মাসের সাত(৭) তারিখের মধ্যে যেকোন  কমদিবসের মধ্যে প্রদান করা হইবে।</td>
	</tr>
    <tr>
		<td colspan="2">০৫)আপনাকে একটি পরিচয় পত্র প্রদান করা হইবে এবং আপনাকে সর্বদা  পরিচয়পত্র পরিধান করিতে হইবে।</td>
	</tr>
	<tr>
		<td colspan="2">০৬)চাকুরি হইতে ইস্তেফা দিতে চাইলে স্হায়ী শ্রমিকের ক্ষেএে ৬০(ষাট) দিন এবং অস্হায়ী শ্রমিকের ক্ষেএে
		৩০(এিশ) দিনের অগ্রিম নোটিশ প্রদান করিতে হইবে।বিনা নোটিশে চাকরি হইতে ইস্তেফা দিতে চাইলে সে ক্ষেএে প্রদেয় নোটিশের পরিবতে নোটিশ মেয়াদের জন্য মজুরির সমপরিমান অর্থ  কর্তৃপক্ষকে  প্রদান করিতে
		হইবে।
		</td>
	</tr>
    <tr>
		<td colspan="2">০৭)কর্তৃপক্ষ কোন স্হায়ী কমীর চাকুরির অবসান করিতে চাইলে অগ্রিম ১২০(একশত বিশ) দিনের লিখিত
		নোটিশ  অথবা নোটিশের পরিবরতে ১২০(একশত বিশ) দিনের মুল মজুরি প্রদান করিবেন।
		</td>
	</tr>
    <tr>
    <td colspan="2">০৮)ছুটি নিন্মোক্ত নিয়মে প্রদানযোগ্যঃ</td>
    </tr>
	<tr>
		 <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) নৈমিত্তিক ছুটিঃ বছরে ১০(দশ) দিন (পূর্ণ বেতনে)। এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ)অসুস্থতাজনিত ছুটি: বছরে ১৪(চৌদদ) দিন (পূর্ণ বেতনে)। এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ) উৎসবজনিত ছুটিঃ বছরে ১১(এগারো) দিন (পূর্ণ বেতনে)।এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) মাতৃত্বকালীন ছুটিঃ ১৬ সপ্তাহ বা ১১২(একশত বারো) দিন (পূর্ণ বেতনে)।
	      তবে  যাদের চাকুরির বয়স ৬(ছয়) মাস পূন্ হইয়াছে তাহাদের  ক্ষেএে প্রযোজ্য।</td>
</tr>
<tr>
		<td colspan="2">ঙ)অর্জিত  ছুটি :একটানা ১২(বার)মাস চাকুরি পূর্ণ  হইলে,পূর্ববছরের  প্রতি ১৮(আঠার) কর্মদিবসের  জন্য ০১(এক) দিন হিসাবে ছুটি পরবর্তী ১২(বার)মাসে ভোগ করা যাইবে।তবে এই ছুটি একসাথে চল্লিশ দিনের বেশি জমা রাখা যাইবে না।</td>
</tr>
<tr>
 		<td colspan="2">০৯)অবেক্ষাকালীন সময়ে আপনার বি্রুদ্ধে আনিত যে কোন অনিয়ম/অভিযোগ প্রমান সাপেক্ষে কতৃপক্ষ আপনাকে চাকুরি হইতে অব্যাহতি(অবসান সুবিধা ছাড়া) দেওায়ার ক্ষমতা রাখিবেন</td>
   	
</tr>
<tr>
	<td colspan="2">১০)কর্তৃপক্ষ প্রয়োজনবোধে আপনাকে এই প্রতিষ্ঠানের যে কোন বিভাগে বা এই গ্রুপের যে কোন কারখানায়/অফিসে বদলি করতে পারবেন।</td>
</tr>
<tr>
<td colspan="2">উপরে বর্ণিত  শর্তাদি  যদি আপনার নিকট গ্রহনযোগ্য হয়,তাহা হইলে এই পত্রের অনুলিপিতে আপনার সম্মতিসূচক স্বাক্ষর প্রদান করার জন্য অনুরোধ করা হইল।</td>
</tr>
<tr>
<td colspan="2">উপরে বর্ণিত  শর্তাদি যদি আপনার নিকট গ্রহনযোগ্য হয়,তাহা হইলে এই পত্রের অনুলিপিতে আপনার সম্মতিসূচক স্বাক্ষর প্রদান করার জন্য অনুরোধ করা হইল।</td>
</tr>
<tr>
		<td colspan="2">অনুমোদনকারীকর্তৃপক্ষ<div align="right">উপরোক্ত বিষয়গুলো বুঝিয়া স্ব-জ্ঞানে,নিজে প্রনোদিত  স্বাক্ষর</div></td>
 </tr>
<tr>
 		<td colspan="2">.................<br />ব্যবস্হাপক(মানবসম্পদ)<div align="right">......................<br />স্বাক্ষর(প্রাথী/প্রাথীনী)</div></td>
</tr>


</table>
	<p></p>
</div><br />
        <?
		}		
		
	}		
		$html_data = ob_get_contents();
		ob_get_clean();
		
		//previous file delete code-----------------------------//
		foreach (glob("../"."*.doc") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
				
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,$html_data);			
		*/
			
			
		echo "$html_data####$filename";
		
		exit();			
		//!--apponment letter for Staff end-->
}

if($type=="appointment_letter_assistant_peon")
{
	
	//<!--apponment letter for Peon Start-->
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );

	
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++){
			$sql="select *,concat(first_name,' ',middle_name,' ',last_name) as name from hrm_employee where emp_code='".$emp_code_explode[$i]."'";			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			
		while($row=mysql_fetch_array($result)){
			$thana=return_field_value("thana","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$village=return_field_value("village","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$post_code=return_field_value("post_code","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			$district_id_bangla=return_field_value("district_id_bangla","hrm_employee_address","emp_code=$row[emp_code] and address_type=1");
			
	?>
<div align="center" style="width:800">
	<style type="text/css" media="print">
       	 p{ page-break-after: always;}
    	</style>
 <? ob_start() ?>

<table width="700" bgcolor="#FFFFFF" style="font-size:12px;">
 		<tr> 
			<td colspan="2" align="right">জে এম ফেব্রিকস লিঃ ইউনিট <br />দক্ষিন নয়াপাড়া,৬নং ডগরী ,ভাওয়াল ,মির্জাপুর ,<br />গাজীপুর ।            

		</tr>
	<tr>
		  <td width="293" rowspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
		  <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+2" style="font-family:'Kalpurush ANSI'"><b><u>নিয়োগ পত্র</u></b></font><br />
		<b>APPOINTMENT LETTER</b></td>
	</tr>
    <tr>
			<td width="395" align="left">&nbsp;</td>
	</tr>
	<tr>
			<td>সুত্রঃ জে,এম,এফ,এল/মা স- /নি প</td><td align="right" style="font-family:'Kalpurush ANSI'">তারিখঃ &nbsp; <?php echo convert_to_mysql_date(date('Y-m-d'));?>&nbsp;</td>
	</tr>
	<tr>
			<td colspan="2"><b>বিসয়ঃ নিয়োগ পএ</b></td> 
	</tr>
    <tr>
              <td colspan="2">জনাব/জনাবা<br /> &nbsp;<font style="font-family:'Kalpurush ANSI'"> <?php echo  $row["full_name_bangla"];?></font>&nbsp; আপনার&nbsp;<font style="font-family:'Kalpurush ANSI'"><?php echo convert_to_mysql_date(date("Y-m-d"));?></font>&nbsp; ইং তারিখের আবেদনপএ এবং তৎপরবর্তী  সাক্ষাৎকারের প্রেক্ষিতে &nbsp; <font style="font-family:'Kalpurush ANSI'"><?php echo convert_to_mysql_date($row[confirmation_date]);?></font>&nbsp;ইং তারিখ হইতে আপনাকে <?php echo $designation_chart[$row['id']];?> পদে &nbsp;<b><?php echo $company_details[$row[company_id]];?><b> &nbsp;<?php echo $row[section_id];?>&nbsp; সেকশনে &nbsp;<? echo $row["salary_grade"];?>&nbsp; গ্রেডে নিম্নোক্ত শর্ত  সাপেক্ষে  নিয়োগ  প্রদান করা হইল ।</td>
                  
               
	</tr>
    <tr>
		<td colspan="2"><b>শর্তাবলী</b></td>
	</tr>
    <tr>
		<td colspan="2">০১.যোগদান পরবর্তী   ৩ (তিন)মাস আবেক্ষাকালীন সময় হিসাবে বিবেচিত হইবে।আবেক্ষাকালীন সময়
		অতিবাহীত হওয়ার পর আপনার নিয়োগকৃ্ত পদে স্হায়ী কমী হিসাবে বিবেচিত হইবেন।
		</td>
	</tr>
    <tr>
    	<td colspan="2">০২.আপনার  সর্বমোট  বেতন <font style="font-family:'Kalpurush ANSI'"><?php echo $row["gross_salary"];?></font>...../টাকা(কথায়ঃ &nbsp;<font style="font-family:'Kalpurush ANSI'"><?php echo $row["gross_salary"];?></font>...... হাজার টাকা  )</td> 
    </tr>
    <tr>
		<td colspan="2">০৩)মাসিক মজুরি/বেতন পরবর্তী মাসের সাত(৭) তারিখের মধ্যে যেকোন  কর্মদিবসের মধ্যে প্রদান করা হইবে।</td>
	</tr>
    <tr>
		<td colspan="2">০৫)আপনাকে একটি পরিচয় পত্র প্রদান করা হইবে এবং আপনাকে সবদা পরিচয়পত্র পরিধান করিতে হইবে।</td>
	</tr>
	<tr>
		<td colspan="2">০৬)চাকুরি হইতে ইস্তেফা দিতে চাইলে স্হায়ী শ্রমিকের ক্ষেএে ৬০(ষাট) দিন এবং অস্হায়ী শ্রমিকের ক্ষেএে
		৩০(এিশ) দিনের অগ্রিম নোটিশ প্রদান করিতে হইবে।বিনা নোটিশে চাকরি হইতে ইস্তেফা দিতে চাইলে সে ক্ষেএে প্রদেয় নোটিশের পরিবর্তে  নোটিশ মেয়াদের জন্য মজুরির সমপরিমান অর্থ কর্তৃপক্ষকে প্রদান করিতে
		হইবে।
		</td>
	</tr>
    <tr>
		<td colspan="2">০৭)কর্তৃপক্ষ কোন স্হায়ী কমীর চাকুরির অবসান করিতে চাইলে অগ্রিম ১২০(একশত বিশ) দিনের লিখিত
		নোটিশ  অথবা নোটিশের পরিবর্তে  ১২০(একশত বিশ) দিনের মুল মজুরি প্রদান করিবেন।
		</td>
	</tr>
    <tr>
    <td colspan="2">০৮)ছুটি নিন্মোক্ত নিয়মে প্রদানযোগ্যঃ</td>
    </tr>
	<tr>
		 <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ক) নৈমিত্তিক ছুটিঃ বছরে ১০(দশ) দিন (পূর্ণ বেতনে)। এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;খ)অসুথতাজনিত ছুটি: বছরে ১৪(চৌদদ) দিন (পূর্ণ বেতনে)। এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;গ) উৎসবজনিত ছুটিঃ বছরে ১১(এগারো) দিন (পূর্ণ বেতনে)।এই ছুটি জমা রেখে পরবর্তী বছরে ভোগ করা যাইবে না । </td>
</tr>
<tr>
		  <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ঘ) মাতৃত্বকালীন ছুটিঃ ১৬ সপ্তাহ বা ১১২(একশত বার ) দিন (পূর্ণ বেতনে)।
	      তবে  যাদের চাকুরির বয়স ৬(ছয়) মাস পূর্ণ  হইয়াছে তাহাদের  ক্ষেএে প্রযোজ্য।</td>
</tr>
<tr>
		<td colspan="2">ঙ)অজিত ছুটি :একটানা ১২(বার)মাস চাকুরি পূর্ণ হইলে, পূর্ববছরের প্রতি ১৮(আঠার) কর্মদিবসের  জন্য ০১(এক) দিন হিসাবে ছুটি পরবর্তী ১২(বার)মাসে ভোগ করা যাইবে।তবে এই ছুটি একসাথে চল্লিশ দিনের বেশি জমা রাখা যাইবেনা  না</td>
</tr>
<tr>
 		<td colspan="2">০৯)অবেক্ষাকালীন সময়ে আপনার বিরুদ্ধে আনিত যে কোন অনিয়ম/অভিযোগ প্রমান সাপেক্ষে কর্তৃপক্ষ আপনাকে চাকুরি হইতে অব্যাহতি(অবসান সুবিধা ছাড়া) দেওায়ার ক্ষমতা রাখিবেন</td>
   	
</tr>
<tr>
	<td colspan="2">১০)কর্তৃপক্ষ প্রয়োজনবোধে আপনাকে এই প্রতিষ্ঠানের যে কোন বিভাগে বা এই গ্রুপের যে কোন কারখানায়/অফিসে বদলি করতে পারবেন।</td>
</tr>
<tr>
<td colspan="2">উপরে বর্ণিত   শর্তাদি  যদি আপনার নিকট গ্রহনযোগ্য হয়,তাহা হইলে এই পত্রের অনুলিপিতে আপনার সম্মতিসূচক স্বাক্ষর প্রদান করার জন্য অনুরোধ করা হইল।</td>
</tr>

<tr>
		<td colspan=""><br /><b>অনুমোদনকারীকর্তৃপক্ষ</b></td><td align="right"><br /><b>উপরোক্ত বিষয়গুলো বুঝিয়া স্ব-জ্ঞানে,নিজে প্রনোদিত  স্বাক্ষর</b></td>
 </tr>
<tr>
 		<td colspan="2">নামঃ <div align="right">স্বাক্ষর(প্রাথী/প্রাথীনী)</div></td>
</tr>
<tr>
		<td colspan="2">পদবিঃ
</td>
</tr>

</table>
	<p></p>
</div><br />
        <?
		}		
		
	}		
		$html_data = ob_get_contents();
		ob_get_clean();
		
		//previous file delete code-----------------------------//
		foreach (glob("../"."*.doc") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
				
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,$html_data);			
		*/
			
			
		echo "$html_data####$filename";
		
		exit();			
		//<!--apponment letter for peon end-->
}
?>



<?

if($type=="employment")
{
	$emp_code = $_GET['getClientId'];
	$emp_code_explode=explode(",",$emp_code);	
	for($i=0;$i<=count($emp_code_explode)-1;$i++){
			$sql="select *  from hrm_employee  where emp_code='".$emp_code_explode[$i] ."' and status_active=1 and is_deleted=0";			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		while($row=mysql_fetch_array($result)){
			$designation=return_field_value("custom_designation","lib_designation","id=$row[designation_id]");
			//$section_id=return_field_value("section_id","hrm_employee_job","emp_code=$row[emp_code]");
			$section=return_field_value("section_name","lib_section","id=$row[section_id]");
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$house_rent = return_field_value("amount","hrm_employee_salary","payroll_head=2 and emp_code=$row[emp_code]");
			$medical_allowance = return_field_value("amount","hrm_employee_salary","payroll_head=3 and emp_code=$row[emp_code]");
			$others = return_field_value("amount","hrm_employee_salary","payroll_head=4 and emp_code=$row[emp_code]");
			$gross=$basic_salary+$house_rent+$medical_allowance+$others;
			$overtime=round(($basic_salary/26)/8,3);
	
			$dob=mysql_real_escape_string($row["dob"]);
			$dob_explode=explode("-",$dob);
			$day=$dob_explode[2];
			$month=$dob_explode[1];
			$year=$dob_explode[0];
	
			$marital_status=$row["marital_status"];
			if($marital_status==1){$status='বিবাহিত';}else{$status='অবিবাহিত';}
			$salary=return_field_value("sum(amount)","hrm_employee_salary","emp_code=$row[emp_code] and status_active=1 and is_deleted=0 group by emp_code");
			$photo=return_field_value("location","resource_photo","identifier=$row[emp_code] and type=1 and is_deleted=0");
			$education=return_field_value("exam_name","hrm_employee_education","emp_code=$row[emp_code] and status_active=1 and is_deleted=0 order by id ASC limit 1");
			ob_start();
	?>
	<div align="center" style="width:800">
    	<style type="text/css" media="print">
            p{ page-break-after: always;}
        	</style>
		<table width="700" height="auto"  bgcolor="#FFFFFF" style="font-size:10px">
		
		<tr>
		  <td width="371" align="left"><img src="../resources/images/asro_logo.png" width="146" height="66" /> </td>
		  <td width="256" align="center" ><font size="+2"><b><u>নিয়োগ</u></b></font><br />
			<b>EMPLOYMENT</b></td>
		  <td width="307" align="center" ><img src="../<? echo $photo; ?>" width="100" height="100" /></td>
		</tr>			
		<tr>
			<td width="371"><b>Unit: ASROTEX LTD.</b></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">	
				<table width="100%"  style="font-size:10px">
				  <tr>
					<td width="10%">নাম (Name)</td>
					<td width="18%">: <? echo mysql_real_escape_string($row["full_name_bangla"]); ?></td>
				  </tr>
				  <tr>
					<td width="9%">পিতার নাম (Father's Name)</td>
					<td width="22%">: <? echo mysql_real_escape_string($row["father_name_bangla"]); ?></td>
				  </tr>
				  <tr>
					<td width="3%">মাতার নাম (Mother's Name)</td>
					<td width="22%">: <? echo mysql_real_escape_string($row["mother_name_bangla"]); ?></td>
				  </tr>
				</table>
		  </td>
		  </tr>		
			<tr>
			<td colspan="3">
          <?	
			$pres_address_sql="select * from hrm_employee_address where address_type=0 and emp_code=$row[emp_code]";			
			$pres_address_result = mysql_query( $pres_address_sql ) or die( $pres_address_sql . "<br />" . mysql_error() );
			while($pres_row=mysql_fetch_array($pres_address_result))
			{	 
		  ?>	<table width="100%"  style="font-size:10px">
          		  <tr><td>
					<table width="100%"  style="font-size:10px">
						<tr>
							<td colspan="4">বর্তমান ঠিকানা (Present Address):</td>								
						</tr>							
						<tr>
								<td width="19%">বাড়ী নং</td>
								<td width="38%">:<? echo mysql_real_escape_string($pres_row["house_no_bangla"]); ?></td> 
								<td width="13%">রোড নং</td> 
								<td width="30%">:<? echo mysql_real_escape_string($pres_row["road_no_bangla"]); ?></td>
					  </tr>
							<tr>
								<td>মহল্লা/গ্রাম</td>
								<td>:<? echo mysql_real_escape_string($pres_row["village_bangla"]); ?></td> 
								<td>ডাকঘর</td> 
								<td>:<? echo mysql_real_escape_string($pres_row["post_code_bangla"]); ?></td>
							</tr>
							<tr>
								<td>থানা</td>
								<td>:.<? echo mysql_real_escape_string($pres_row["thana_bangla"]); ?></td> 
								<td>জেলা</td> 
								<td>:<? echo mysql_real_escape_string($pres_row["district_id_bangla"]); ?></td>
							</tr>
					</table>
                    </td>
                    <td>
			<?	
				$pres_mobile_no=mysql_real_escape_string($pres_row["mobile_no"]);
				$pres_phone_no=mysql_real_escape_string($pres_row["phone_no"]);
			}
			?>	
			
			<?
			$perm_address_sql="select * from hrm_employee_address where address_type=1 and emp_code=$row[emp_code]";
			$perm_address_result = mysql_query( $perm_address_sql ) or die( $perm_address_sql . "<br />" . mysql_error() );
			while($perm_row=mysql_fetch_array($perm_address_result))
			{
			?>	
			<table width="100%"  style="font-size:10px">
							<tr>
								<td colspan="4">স্থায়ী ঠিকানা (Permanent Address):</td>								
							</tr>							
							<tr>
								<td width="19%">বাড়ী নং</td>
								<td width="36%">:<? echo  mysql_real_escape_string($perm_row["house_no_bangla"]); ?></td> 
								<td width="13%">রোড নং</td> 
								<td width="32%">:<? echo mysql_real_escape_string($perm_row["road_no_bangla"]); ?></td>
							</tr>
							<tr>
								<td>মহল্লা/গ্রাম</td>
								<td>:<? echo mysql_real_escape_string($perm_row["village_bangla"]); ?></td> 
								<td>ডাকঘর</td> 
								<td>:<? echo mysql_real_escape_string($perm_row["post_code_bangla"]); ?></td>
							</tr>
							<tr>
								<td>থানা</td>
								<td>:<? echo mysql_real_escape_string($perm_row["thana_bangla"]); ?></td> 
								<td>জেলা</td> 
								<td>:<? echo mysql_real_escape_string($perm_row["district_id_bangla"]); ?></td>
							</tr>
			  </table>
              </td>
              </tr>
              </table>
			<?			
				$perm_mobile_no=mysql_real_escape_string($perm_row["mobile_no"]);
				$perm_phone_no=mysql_real_escape_string($perm_row["phone_no"]);
			}
			?>
  			  </td>
	  	  </tr>
				  	<tr>					
						<td colspan="3">
						<table width="100%"  style="font-size:10px">														
							<tr>
								<td>জরুরী যোগাযোগ নং(Emergency Contact)</td>
								<td style="font-family:'Kalpurush ANSI'">: ফোন......<? echo $pres_phone_no; ?>......</td> 
								<td style="font-family:'Kalpurush ANSI'">মোবাইল:......<? echo $pres_mobile_no; ?>......</td> 
								<td></td>
							</tr>
							<tr>
								<td style="font-family:'Kalpurush ANSI'">জন্ম তারিখ/বয়স (Date Of Birth)</td>
								<td colspan="3" style="font-family:'Kalpurush ANSI'">:......<? echo $day; ?>...... 
								/......<? echo $month; ?>......
								/......<? echo $year; ?>.....</td>
							</tr>
							<tr>
								<td>বৈবাহিক অবস্থা (Marital Status)</td>
								<td>:........<? echo $status; ?>...........................</td> 
								<td>শিক্ষাগত যোগ্যতা (Educational Qualificaton)</td> 
								<td>:........<? echo $education; ?>.......</td>
							</tr>
						</table>					
					</td>
				  </tr>
				  <tr>	
					<td>পূর্ব অভিজ্ঞতা (Previous Experience):</td>
					<td colspan="2"></td>
				  </tr>
				<tr>					
					<td colspan="3">
						<table cellpadding="0" cellspacing="1" border="1" class="display" width="100%"  style="font-size:10px">												
							<tr align="center">
								<td></td>
								<td>কোম্পানির নাম (Name of Factory)</td> 
								<td>পদবী (Post)</td> 
								<td>বেতন (Salary)</td>
								<td>হইতে (From)</td>
								<td>পর্যন্ত (To)</td>
							</tr>
					<?
					$experience_sql="select * from hrm_employee_experience where emp_code=$row[emp_code] and status_active=1 and is_deleted=0";		
					$experience_result = mysql_query( $experience_sql ) or die( $experience_sql . "<br />" . mysql_error() );
					$count=0;$check=0;
					while($experience_row=mysql_fetch_array($experience_result))
					{	
						$count++;
					?>
								<tr>								
								<td><? echo $count; ?></td>
								<td><? echo $experience_row["organization_name"]; ?></td>
								<td><? echo $experience_row["designation"]; ?></td>
								<td style="font-family:'Kalpurush ANSI'"><? echo $experience_row["gross_salary"]; ?></td>
								<td style="font-family:'Kalpurush ANSI'"><? echo $experience_row["joining_date"]; ?></td>
								<td style="font-family:'Kalpurush ANSI'"><? echo $experience_row["resigning_date"]; ?></td>
							</tr>
					<?						
					}
					for($count=$count+1;$count<=5;$count++)
					{						
					?>		
							<tr>																
								<td><? echo $count; ?> </td> 	
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>					
					<?
                    }
					?>
				</table>					
					</td>
		  </tr> 				
		
		<tr>
		  <td colspan="3">আমি এই মর্মে ঘোষনা করিতেছি যে, আমার জানামতে উপরোক্ত তথ্যাবলী নির্ভূল এবং সত্য। মিথ্যা প্রমানিত হলে কর্তৃপক্ষ আমার বিরুদ্ধে যে কোন আইনানুগ ব্যবস্থা গ্রহন করতে পারবেন। আমার ঠিকানা পরিবর্তিত হলে ০৩(তিন) দিনের মধ্যে পারসোনেল বিভাগে জানাতে বাধ্য থাকব।</td>		  
		</tr>
		<tr>
		  <td>তারিখ.............</td>
		   <td colspan="3" align="right"><p>------------------------------------<br />
			আবেদনকারী (Applicant)&nbsp;&nbsp;&nbsp;&nbsp;</p></td>		  
		</tr>		
		
		<tr>
		  <td colspan="3" align="center">প্রার্থীর যোগ্যতা ও বয়স যাচাই করা হয়েছে। নিয়োগদানের জন্য সুপারিশ করা হইল। <br />(Applicant's competency and age has been verified. Recommended for employment)</td>		  
		</tr>
		<tr>		  
		   <td align="left"><p>------------------------------------<br />
			Departmental Head&nbsp;&nbsp;&nbsp;&nbsp;</p></td>		
			<td colspan="2" align="right"><p>------------------------------------<br />
			HR Department&nbsp;&nbsp;&nbsp;&nbsp;</p></td>		
		</tr>
		<tr>					
			<td colspan="3">
				<table width="100%" style=" border:1px solid;font-size:10px" cellpadding="0" cellspacing="0" >														
					<tr>								
						<td colspan="6" align="center" style=" border:1px solid;"> OFFICIAL USE ONLY </td>
					</tr>
					<tr>
						<td>Card No</td>
						<td>:....<? echo mysql_real_escape_string($row["id_card_no"]); ?>.......</td>
						<td>Designation</td>
						<td>:....<? echo $designation; ?>......</td>
						<td>Section</td>
						<td>:....<? echo $section; ?>......</td>								
					</tr>
					<tr>
						<td>Join Date</td>
						<td>:....<? echo $dob; ?>......</td>
						<td>Salary TK</td>
						<td colspan="2">:....<? echo $salary; ?>......(Taka)</td>
						<td></td>													
					</tr>
					<tr>
						<td>Remarks</td>
						<td colspan="5">:....<? echo mysql_real_escape_string($row["remark"]); ?>.....</td>																
					</tr>
					<tr>
						<td>Reference</td>
						<td colspan="5">:....<? echo mysql_real_escape_string($row["id_card_no"]); ?>......</td>																
					</tr>
					<tr>								
						<td colspan="6" >&nbsp;</td>
					</tr>
					<tr>
					  	<td colspan="3" align="left">Signature...................................</td>
					 	<td colspan="3" align="right">Authorised Signature...................................</td>								  
					</tr>
				</table>					
			</td>
		  </tr>		  	
    </table>
    <p></p>
</div><br />
     <?
		}		
				
	}	
			$html_data = ob_get_contents();
			ob_get_clean();
		
		//previous file delete code-----------------------------//
		foreach (glob("../"."*.doc") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
		//---------end------------//
		
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,ob_get_contents());*/			
		echo "$html_data####$filename";
		exit();			
}

//===================================================sohel===============================================================================
	
if($type=="manpower_update")
{
	$id=$_GET['getClientId'];
	//echo $id;die;
	   $res = mysql_query("SELECT * FROM budgeted_manpower_setup WHERE id = '$id' and is_deleted=0 and status_active=1")  or die(mysql_error());
	
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.cbo_company_id.value = '".mysql_real_escape_string($inf["company_id"])."';\n";    
			echo "formObj.cbo_location_id.value = '".mysql_real_escape_string($inf["location_id"])."';\n";    
			echo "formObj.cbo_division_id.value = '".mysql_real_escape_string($inf["division_id"])."';\n"; 
			echo "formObj.cbo_department_id.value = '".mysql_real_escape_string($inf["department_id"])."';\n"; 
			echo "formObj.cbo_section_id.value = '".mysql_real_escape_string($inf["section_id"])."';\n"; 
			echo "formObj.cbo_subsection_id.value = '".mysql_real_escape_string($inf["subsection_id"])."';\n"; 
			echo "formObj.cbo_emp_category.value = '".mysql_real_escape_string($inf["emp_catg_id"])."';\n"; 
			echo "formObj.cbo_desig_level.value = '".mysql_real_escape_string($inf["desig_level_id"])."';\n"; 
			echo "formObj.cbo_designation_id.value = '".mysql_real_escape_string($inf["designation_id"])."';\n"; 
			echo "formObj.txt_no_post.value = '".mysql_real_escape_string($inf["no_of_post"])."';\n"; 
			echo "formObj.save_update.value = '".mysql_real_escape_string($inf["id"])."';\n"; 
			//echo "formObj.approval_date.value = '".convert_to_mysql_date($inf["approval_date"])."';\n"; 
			//echo "formObj.save_up.value = '".$inf["id"]."';\n"; 
			//echo "$('#txt_emp_code').attr('disabled','true')".";\n";

		} 
}
//====================================================end sohel===================================================================================

// Expatriate Employee Information
if($type=="get_data_update_exp")
{ 
	$res = mysql_query("SELECT * FROM hrm_expatriate_employee_info WHERE emp_code = '$emp_code' and is_deleted=0 and status_active=1")  or die(mysql_error());
	if($inf = mysql_fetch_array($res))
	{
		$pass_issue_date=convert_to_mysql_date($inf["pp_issue_date"]);
		$pass_expire_date=convert_to_mysql_date($inf["pp_expiry_date"]);
		$pass_renew=convert_to_mysql_date($inf["pp_renew_date"]);
		
		$visa_issue_date=convert_to_mysql_date($inf["visa_issue_date"]);
		$visa_expire_date=convert_to_mysql_date($inf["visa_expiry_date"]);
		$visa_renew=convert_to_mysql_date($inf["visa_renew_date"]);
		
		$permit_issue_date=convert_to_mysql_date($inf["wp_issue_date"]);
		$permit_expire_date=convert_to_mysql_date($inf["wp_expiry_date"]);
		$permit_renew=convert_to_mysql_date($inf["wp_renew_date"]);
		
		$contact_start=convert_to_mysql_date($inf["cont_start_date"]);
		$contact_end=convert_to_mysql_date($inf["cont_end_date"]);
		$contact_renew=convert_to_mysql_date($inf["cont_renew_date"]);
		
		echo "document.getElementById('up_id').value = '".mysql_real_escape_string($inf["id"])."';\n";
		echo "document.getElementById('pass_no').value = '".mysql_real_escape_string($inf["pp_number"])."';\n";
		echo "document.getElementById('pass_issue_date').value = '".mysql_real_escape_string($pass_issue_date)."';\n";
		echo "document.getElementById('pass_expire_date').value = '".mysql_real_escape_string($pass_expire_date)."';\n";
		echo "document.getElementById('pass_renew').value = '".mysql_real_escape_string($pass_renew)."';\n";
		
		echo "document.getElementById('visa_no').value = '".mysql_real_escape_string($inf["visa_number"])."';\n";
		echo "document.getElementById('visa_issue_date').value = '".mysql_real_escape_string($visa_issue_date)."';\n";
		echo "document.getElementById('visa_expire_date').value = '".mysql_real_escape_string($visa_expire_date)."';\n";
		echo "document.getElementById('visa_renew').value = '".mysql_real_escape_string($visa_renew)."';\n";
		
		echo "document.getElementById('permit_no').value = '".mysql_real_escape_string($inf["wp_number"])."';\n";
		echo "document.getElementById('permit_issue_date').value = '".mysql_real_escape_string($permit_issue_date)."';\n";
		echo "document.getElementById('permit_expire_date').value = '".mysql_real_escape_string($permit_expire_date)."';\n";
		echo "document.getElementById('permit_renew').value = '".mysql_real_escape_string($permit_renew)."';\n";
		
		echo "document.getElementById('contact_no').value = '".mysql_real_escape_string($inf["cont_number"])."';\n";
		echo "document.getElementById('contact_start').value = '".mysql_real_escape_string($contact_start)."';\n";
		echo "document.getElementById('contact_end').value = '".mysql_real_escape_string($contact_end)."';\n";
		echo "document.getElementById('contact_renew').value = '".mysql_real_escape_string($contact_renew)."';\n";
		
		echo "document.getElementById('currency').value = '".mysql_real_escape_string($inf["salary_currency"])."';\n";
		echo "document.getElementById('local_currency').value = '".mysql_real_escape_string($inf["local_currency_payment"])."';\n";
		echo "document.getElementById('exchange_rate').value = '".mysql_real_escape_string($inf["notional_exchange_rate"])."';\n";
		echo "document.getElementById('remark').value = '".mysql_real_escape_string($inf["remark"])."';\n";
	} 
}


if($type=="fixed_roster")
{
	$sqls = mysql_query("SELECT * FROM lib_policy_shift WHERE id = '$search_string' and is_deleted=0 and status_active=1")  or die(mysql_error());
	if($inf = mysql_fetch_array($sqls))
		{
			$shift_type=$inf["shift_type"];
		}
		
	$emps = mysql_query("SELECT id,shift_policy FROM hrm_employee WHERE emp_code='$search_string2' and is_deleted=0")  or die(mysql_error());
	if($inf_emps = mysql_fetch_array($emps))
		{
			$emp_shift_id=$inf_emps["shift_policy"];
		}
		
		
	$sqlr = mysql_query("SELECT * FROM lib_policy_shift WHERE id = '$emp_shift_id' and is_deleted=0 and status_active=1")  or die(mysql_error());
	if($infr = mysql_fetch_array($sqlr))
		{
			$x_shift_type=$infr["shift_type"];
		}
		if($x_shift_type==$shift_type )//|| $x_shift_type<$shift_type
		{
			echo "1_$emp_shift_id";
			//echo "Change OT Police if Shift Change".$x_shift_type.'_'.$shift_type; 
		}
		else if($x_shift_type>$shift_type)
		{
			echo "2_$emp_shift_id";
			//echo "Delete Roster Process".$x_shift_type.'_'.$shift_type; 
		}
		else if($x_shift_type<$shift_type && $x_shift_type==$shift_type)
		{
			echo "3_$emp_shift_id";
			//echo "Delete Roster Process".$x_shift_type.'_'.$shift_type; 
		}
		
	//echo $x_shift_type.'_'.$shift_type;
	
	exit();	
}

function return_field_value($fdata,$tdata,$cdata)
{
$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}


function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}



//list_view
if($type=="list_view")
{		
	extract($_GET);
	//extract($_POST);
	
	
	 $year=date("Y",strtotime(($period)));
	 $sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.actual_starting_date like '$year-%%'  and b.type=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	?>
	 <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:12px;" width="300" rules="all"  align="center">
         <thead>
         		<tr>
                	<th colspan="3">Salary Period Lock Status</th>
                </tr> 
            	
            <tr>
                
                <th width="120">Start Date</th>
                <th width="120">End Date</th>
                <th width="80">Status</th>
            </tr>
		</thead> 
        <?
		$r=0;
	while( $row = mysql_fetch_assoc( $result ) ) {
    
	if ($row[is_locked]==1) $bgcolor="#FF0000"; else $bgcolor="#EEEEEE"; 
				
	
	?>
        <tr bgcolor="<? echo $bgcolor;?>"  height="20">
        
            <td width="120"><? echo $row[starting_date]; ?></td>
            <td width="120"><? echo $row[ending_date]; ?></td>
            <td width="80"><?  if($row[is_locked]==1) echo "Locked"; else echo ""; ?></td>
        
        </tr>
				<?
				$r++;
			
	}	
	?>	
	 </table>
     <?
	 exit();
}


?>