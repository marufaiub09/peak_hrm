var globar_var_save_clicked=0;
//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

//Employee Basic of Employee Information ( Save / Update )
function fnc_emp_basic(save_perm,edit_perm,delete_perm,approve_perm) {
		
	var txt_punch_card=$('#punch_card_no').val();
	//var emp_ref=$('#emp_ref').val();
	//var txt_Contract_start_date=$('#Contract_start_date').val();
	//var txt_Contract_end_date=$('#Contract_end_date').val();
	 
	var hidden_age=$('#hidden_age').val();
	if(hidden_age<18){
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#year').focus();		
			$(this).html('Age Should Not be Less Than 18').addClass('messageboxerror').fadeTo(900,1);
		});
		return;
	}
	
	save_up=$('#emp_code').val();
	var fields = new Array( 'emp_code','first_name','middle_name','last_name','full_name_bangla','id_card_no','punch_card_no','dob',
							'father_name','father_name_bangla','mother_name','mother_name_bangla','birth_place','religion','blood_group',
							'marital_status','sex','nationality','national_id','passport_no','designation_id','designation_level','joining_date',
							'confirmation_date','category','functional_superior','admin_superior','remark','status_attendance','status_salary','company_id','location_id',
							'division_id','department_id','section_id','subsection_id','skill_rank','Contract_start_date','Contract_end_date','emp_ref_code');
	
	for( var i = 0; i < fields.length; i++ ) {
		field_array[i]	= new Array( fields[i], $('#' + fields[i]).val() );
	}
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	
	if( save_up=="" && save_perm==2){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up!="" && edit_perm==2){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if( $('#first_name').val()==''){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#first_name').focus();
		$(this).html('Please input the employee name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if( $('#dob').val()==''){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#dob').focus();
		$(this).html('Please Select The Date of Birth.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if( $('#joining_date').val()==''){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#joining_date').focus();
		$(this).html('Please Select Joining Date.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if(  $('#designation_id').val()==0){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#designation_id').focus();
		$(this).html('Please Select Designation name').addClass('messageboxerror').fadeTo(900,1);
		});			
	}
	else if(  $('#company_id').val()==0){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#company_id').focus();
		$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if(  $('#department_id').val()==0){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#department_id').focus();
		$(this).html('Please Select department name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else if( error == false ) {
		
		//check save event-----------//		
		if(globar_var_save_clicked==0)
			{
				globar_var_save_clicked=1;
			}
		else
			return;
		
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		//alert (data);		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_basic' + data +'&txt_punch_card='+txt_punch_card+'&nocache=' + nocache );
		
		http.onreadystatechange = response_emp_basic;		
		http.send(null);
	}
}
//Employee Basic Response of Employee Information ( Save / Update )
function response_emp_basic() {
	if(http.readyState == 4) {
		//alert(http.responseText);
		var response = http.responseText.split('_');	
		//alert(response[0]);return;
		
		 if( response[0] == 3 ) {
			if( response[1] == 0 ) var field = 'National ID';
			if( response[1] == 1 ) var field = 'Passport No';
			if( response[1] == 2 ) var field = 'Punch Card No';
			if( response[1] == 3 ) var field = 'ID Card No';
			
			alert('Duplicate ' + field + '. Please use another one.');
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate ' + field + '. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
			$('#emp_code').val( response[1] );
			$('#id_card_no').val( response[2] );
			$('#new').css( "visibility", "visible" );
			
			enable_tabs();
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
			$('#id_card_no').val( response[1] );
		}
	
	
	else if( response[0] == 11 ) 
		{ 
			//alert ("Manpower over the Budget ");
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Manpower Exceed the Budgeted Employee.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			}); 
		}
		else if( response[0] == 111 ) 
		{ 
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Variable Settings Must For ID Card.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			}); 
		}
		//alert ($('#status_salary').val());
			if(joining_date_gross_salary_condition==0)
			{
			if(is_data_level_secured==1){ $('#joining_date').attr("disabled","disabled")}
			}
			
			if(joining_date_gross_salary_condition==0)
			{
				if(is_data_level_secured==1)
				{ 
					if($("#gross_salary").val()!='')
					{
						$('#gross_salary').attr("disabled","disabled")
					}
				}
			}
				
			if( is_data_level_secured==1 && $('#status_salary').val()==0)
			{
				$('#employee_info').tabs('disable', 3);
				$('#employee_info').tabs('disable', 4);
				
				$('#status_attendance').attr("disabled","disabled");
				$('#status_salary').attr("disabled","disabled");
			}
	
	
	}
	//global variable
	globar_var_save_clicked=0;	
}
//Employee Address of Employee Information ( Save / Update )
function fnc_emp_address(save_perm,edit_perm,delete_perm,approve_perm) {
	
	var save_up=$('#save_up_address').val();
	//alert (edit_perm);return;
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	
	if( save_up=="" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up!="" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{
	
	
	var fields = new Array( 'village','village_bangla','house_no','house_no_bangla','road_no','road_no_bangla','post_code','post_code_bangla','thana','thana_bangla','district_id','district_id_bangla','division_id','division_id_bangla','phone_no','mobile_no','email','co','co_bangla' );
	
	
	
	
	for( var i = 0; i < 3; i++) {
		if( i == 0  ) { var fields_counter = 17; var type = 'present'; }
		else if( i == 1  ) { var fields_counter = 17; var type = 'permanent'; }
		else { var fields_counter = 19; var type = 'alternate'; }
		
		for( var j = 0; j < fields_counter; j++ ) {
			field_array[j + i * 17] = new Array( type + '_' + fields[j], $('#' + type + '_' + fields[j]).val() );
		}
	}
	field_array[53] = new Array( 'emp_code', $('#emp_code').val() );
	
	
	nocache = Math.random();
	var data = '';
	for( var i = 0; i < field_array.length; i++ ) {
		data += '&' + field_array[i][0] + '=' + field_array[i][1];
	}
	if(document.getElementById('same').checked)
		{
			var same=1;
		}
	else
		{
			var same=0;
		}
	data +='&same='+same;
	//alert(data);
	http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_address' + data + '&nocache=' + nocache );
	http.onreadystatechange = response_emp_address;
	http.send(null);
	}
}
//Employee Address Response of Employee Information ( Save / Update )
function response_emp_address() {
	if(http.readyState == 4) {
		
		//alert (http.responseText);
		var response = http.responseText.split('_');	
		
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Salary of Employee Information ( Save / Update )
function fnc_emp_salary(save_perm,edit_perm,delete_perm,approve_perm){
	
	var save_up=$("#save_up_salary").val();
	var save_up_break='';
	for( var i = 1; i <= $('#salary tbody tr').length; i++ ) 
	{
		save_up_break += $('#saveupamount_' + i).val();
	}
	//alert(save_up_break);return;
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	//salary_breakdown_policy_change($('#salary_rule').val());if( save_up=="" && save_perm==2){	
	calculate_all();
	if( save_up=="" && save_up_break=="" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up!="" && save_up_break!="" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	/*
	else if( $('#salary_grade').val() == '' || $('#salary_grade').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#salary_grade').focus();
			$(this).html('Please Enter salary_grade').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	*/
	/*
	else if(  $('#salary_rule').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#salary_grade').focus();
			$(this).html('Please Enter salary_grade').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	*/
	
	var gross = parseFloat( $('#gross_salary').val() );
	//alert(gross);

	var amount = 0; 
	for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
		for( var j = 1; j <= payroll_heads.length; j++ ) {
			if( payroll_heads[j] != undefined && payroll_heads[j]['id'] == $('#payroll_head_' + i).val() && payroll_heads[j]['salary_head'] == 1 ) {
				//amount += parseFloat( $('#amount_' + i).val() );
				//amount += Math.floor( $('#amount_' + i).val() );
				amount += parseFloat( $('#amount_' + i).val() );
				//amount += Math.floor( $('#amount_' + i).val() );
				//amount += Math.round( $('#amount_' + i).val() );
				break;
			}
		}
	}
	//alert(gross);return;
	var diff =Math.floor(Math.abs(gross - amount));
	//alert(diff);return;
	if( diff != 0 && ( diff < - 0.03 || diff > 0.03 ) ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#gross_salary').focus();
			$(this).html('Gross salary (' + gross + ') and total salary (' + amount + ') do not match (' + diff + ').').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( diff != 0 ) {
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			if( $('#payroll_head_' + i).val() == 1 ) {
				$('#amount_' + i).val( parseFloat( $('#amount_' + i).val() ) + diff );
				break;
			}
		}
	}
	
	if( error == false ) {
		field_array[0] = new Array( 'emp_code', $('#emp_code').val() );
		field_array[1] = new Array( 'salary_grade', $('#salary_grade').val() );
		field_array[2] = new Array( 'salary_rule', $('#salary_rule').val() );
		field_array[3] = new Array( 'gross_salary', $('#gross_salary').val() );
		
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			field_array[i + 3] = new Array( $('#payroll_head_' + i).val(), $('#type_' + i).val(), $('#percentage_formula_' + i).val(), $('#base_head_' + i).val(), $('#amount_' + i).val() );
		}
		
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			if( i > 3 ) {
				data += '&head' + ( i - 3 ) + '=';
				for( var j = 0; j < field_array[i].length; j++ ) {
					if( j > 0 ) data += '_';
					data += field_array[i][j];
				}
			}
			else data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		data += '&head_counter=' + $('#salary tbody tr').length;
		//var bank_salary = $('#bank_salary').val();
		var cash_disbursement = $('#cash_disbursement').val();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_salary' + data +'&cash_disbursement=' +cash_disbursement+ '&nocache=' + nocache );
		http.onreadystatechange = response_emp_salary;
		http.send(null);
	}
}
//Employee Address Response of Employee Information ( Save / Update )
function response_emp_salary() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');
		
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
			
			if(joining_date_gross_salary_condition==0)
			{
			if(is_data_level_secured==1){ $('#joining_date').attr("disabled","disabled")}
			}
			
			if(joining_date_gross_salary_condition==0)
				{
					if(is_data_level_secured==1)
					{ 
						if($("#gross_salary").val()!='')
						{
							$('#gross_salary').attr("disabled","disabled")
						}
					}
				}
		}
	}
}
//Bank Salary of Employee Information ( Save / Update )
function fnc_emp_salary_bank(save_perm,edit_perm,delete_perm,approve_perm) 
{
	
	var save_up=$("#save_up_bank_sal").val();
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	//if( save_up=="" && save_perm==2){
	if( save_up=="" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up!="" &&  edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if( $('#bank_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#bank_id').focus();
			$(this).html('Please Select a Salary Bank.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#branch_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#branch_name').focus();
			$(this).html('Please Enter a Branch Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#account_no').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#account_no').focus();
			$(this).html('Please Enter an Account No.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#salary_amount').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#salary_amount').focus();
			$(this).html('Please Enter Salary Amount.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if ( (($('#total_bank_salary').val()*1) + ($('#salary_amount').val()*1)) > ($('#gross_sal').val()*1 ) )
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#salary_amount').focus();
			$(this).html('Bank Salary Never Exceed Gross Salary.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		field_array[0] = new Array( 'emp_code', $('#emp_code').val() );
		field_array[1] = new Array( 'lib_bank_id', $('#bank_id').val() );
		field_array[2] = new Array( 'branch_name', $('#branch_name').val() );
		field_array[3] = new Array( 'account_no', $('#account_no').val() );
		field_array[4] = new Array( 'salary_amount', $('#salary_amount').val());
		field_array[5] = new Array( 'show_tax', $('#show_tax').val() );
		field_array[6] = new Array( 'sequence', $('#sequence').val());
		field_array[7] = new Array( 'update_id', $('#update_id').val() );
		//alert($('#update_id').val());
		
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		
		
	//var sal_amt=$('#salary_amount').val();	
	//var total_bank_salary=parseInt($('#total_bank_salary').val()*1);
	
	//var tot_bank_pev_curent=parseInt($('#salary_amount').val()*1)+parseInt($('#total_bank_salary').val()*1);
	
	//alert(totas);
	//return;
		
		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_salary_bank' + data+'&nocache=' + nocache );
		http.onreadystatechange = response_emp_salary_bank;
		http.send(null);
	}
}
//Response of Bank Salary of Employee Information ( Save / Update )
function response_emp_salary_bank() {
	if(http.readyState == 4) {
		
		var response = http.responseText.split('_');
		//alert(response[3])	
		show_emp_salary_bank( 0 )
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
				calculate_list_total( 1 );
				//show_emp_salary_bank();
				emp_salary_bank_load();
			});
			//showResult_multi($('#emp_code').val(),'','update_list_view','tbl_list_view');
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
				calculate_list_total( 1 );
				//show_emp_salary_bank();
				emp_salary_bank_load();
				//reset_form('emp_salary_bank_form');
				//document.getElementById('emp_salary_bank_form').reset();
			});
			//showResult_multi($('#emp_code').val(),'','update_list_view','tbl_list_view');
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Bank Account Number.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
				calculate_list_total( 1 );
				show_emp_salary_bank();
				//testing();
				//reset_form('emp_salary_bank_form');
				//document.getElementById('emp_salary_bank_form').reset();
			});
			//showResult_multi($('#emp_code').val(),'','update_list_view','tbl_list_view');
		}
	}
}

//Employee Entitlement of Employee Information ( Save / Update )
function fnc_emp_entitlement(save_perm,edit_perm,delete_perm,approve_perm) {
	
	var save_up=$("#save_up_entitlement_emp").val();
	//var save_up_entit=$("#save_up_entitlement_table").val();
	//var save_up_nominee=$("#save_up_nominee").val();
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	field_array = new Array();
	var error = false;
	
	if( save_up == "" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up != "" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	
	field_array['emp_code'] = $('#emp_code').val();
	field_array['ot_entitled'] = $('#ot_entitled').is(':checked') ? 1 : 0;
	field_array['staff_ot_entitled'] = $('#staff_ot_entitled').is(':checked') ? 1 : 0;
	field_array['holiday_allowance_entitled'] = $('#holiday_allowance_entitled').is(':checked') ? 1 : 0;
	
	if( $('input[name="salary_type"]:checked').val() == undefined ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('input[name="salary_type"]:first').focus();
			$(this).html('Please Select Salary Type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else field_array['salary_type'] = $('input[name="salary_type"]:checked').val();
	
	if( $('#pf_entitled').is(':checked') ) {
		field_array['pf_entitled'] = 1;
		
		if( $('#pf_effective_date').val() == '' ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#pf_effective_date').focus();
				$(this).html('Please Provide Provident Fund Effective Date.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else field_array['pf_effective_date'] = $('#pf_effective_date').val();
		
		$('#pf tbody tr').each(function() {
			if( $(this).find('input[name="pf_nominee_name[]"]').val() == '' && $(this).find('input[name="pf_nominee_relation[]"]').val() == '' && $(this).find('input[name="pf_nominee_ratio[]"]').val() == '' ) {
				if( this != this.parentNode.firstChild ) $(this).remove();
			}
		});
		var correct_pf_nominee_counter = 0;
		field_array['pf_nominee'] = new Array();
		$('#pf tbody tr').each(function() {
			if( $(this).find('input[name="pf_nominee_name[]"]').val() == '' || $(this).find('input[name="pf_nominee_relation[]"]').val() == '' || $(this).find('input[name="pf_nominee_ratio[]"]').val() == '' ) {
				error = true;
				$(this).find('input[name="pf_nominee_name[]"]').focus();
				$('#messagebox').html('Please Provide All Nominee Information.').addClass('messageboxerror');
			}
			else {
				field_array['pf_nominee'][correct_pf_nominee_counter] = new Array();
				field_array['pf_nominee'][correct_pf_nominee_counter]['pf_nominee_name']=$(this).find('input[name="pf_nominee_name[]"]').val();
				field_array['pf_nominee'][correct_pf_nominee_counter]['pf_nominee_relation']=$(this).find('input[name="pf_nominee_relation[]"]').val();
				field_array['pf_nominee'][correct_pf_nominee_counter]['pf_nominee_ratio']=$(this).find('input[name="pf_nominee_ratio[]"]').val();
				field_array['pf_nominee'][correct_pf_nominee_counter]['pf_nominee_address']=$(this).find('input[name="pf_nominee_address[]"]').val();
  				correct_pf_nominee_counter++;
			}
		});
	}
	 
	if( $('#gi_entitled').is(':checked') ) {
		field_array['gi_entitled'] = 1;
		
		if( $('#gi_effective_date').val() == '' ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#gi_effective_date').focus();
				$(this).html('Please Provide Group Insurance Effective Date.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else field_array['gi_effective_date'] = $('#gi_effective_date').val();
		
		$('#gi tbody tr').each(function() {
			if( $(this).find('input[name="gi_nominee_name[]"]').val() == '' && $(this).find('input[name="gi_nominee_relation[]"]').val() == '' && $(this).find('input[name="gi_nominee_ratio[]"]').val() == '' ) {
				if( this != this.parentNode.firstChild ) $(this).remove();
			}
		});
		var correct_gi_nominee_counter = 0;
		field_array['gi_nominee'] = new Array();
		$('#gi tbody tr').each(function() {
			if( $(this).find('input[name="gi_nominee_name[]"]').val() == '' || $(this).find('input[name="gi_nominee_relation[]"]').val() == '' || $(this).find('input[name="gi_nominee_ratio[]"]').val() == '' ) {
				error = true;
				$(this).find('input[name="gi_nominee_name[]"]').focus();
				$('#messagebox').html('Please Provide All Nominee Information.').addClass('messageboxerror');
			}
			else {
				field_array['gi_nominee'][correct_gi_nominee_counter] = new Array();
				field_array['gi_nominee'][correct_gi_nominee_counter]['gi_nominee_name']=$(this).find('input[name="gi_nominee_name[]"]').val();
				field_array['gi_nominee'][correct_gi_nominee_counter]['gi_nominee_relation']=$(this).find('input[name="gi_nominee_relation[]"]').val();
				field_array['gi_nominee'][correct_gi_nominee_counter]['gi_nominee_ratio']=$(this).find('input[name="gi_nominee_ratio[]"]').val();
				field_array['gi_nominee'][correct_gi_nominee_counter]['gi_nominee_address']=$(this).find('input[name="gi_nominee_address[]"]').val();
  				correct_gi_nominee_counter++;
			}
		});
	}
	
	if( error == false ) {
		nocache = Math.random();
		var data = '';
		for( var key in field_array ) {
			if( key != 'pf_nominee' && key != 'gi_nominee' ) data += '&' + key + '=' + field_array[key];
		}
		for( var key in field_array['pf_nominee'] ) {
 			data += '&pf_nominee_name' + key + '=' + field_array['pf_nominee'][key]['pf_nominee_name'];
			data += '&pf_nominee_relation' + key + '=' + field_array['pf_nominee'][key]['pf_nominee_relation'];
			data += '&pf_nominee_ratio' + key + '=' + field_array['pf_nominee'][key]['pf_nominee_ratio'];
			data += '&pf_nominee_address' + key + '=' + field_array['pf_nominee'][key]['pf_nominee_address'];
 		}
		for( var key in field_array['gi_nominee'] ) {
 			data += '&gi_nominee_name' + key + '=' + field_array['gi_nominee'][key]['gi_nominee_name'];
			data += '&gi_nominee_relation' + key + '=' + field_array['gi_nominee'][key]['gi_nominee_relation'];
			data += '&gi_nominee_ratio' + key + '=' + field_array['gi_nominee'][key]['gi_nominee_ratio'];
			data += '&gi_nominee_address' + key + '=' + field_array['gi_nominee'][key]['gi_nominee_address'];
 		}
		/*
		if( $('#pf_entitled').is(':checked') ) {
			data += '&pf_nominee_counter=' + $('#pf tbody tr').length;
			var i = 0;
			$('#pf tbody tr').each(function() {
				data += '&pf_nominee_' + i + '=' + $(this).find('input[name="pf_nominee_name[]"]').val()
												+ '_' + $(this).find('input[name="pf_nominee_relation[]"]').val()
												+ '_' + $(this).find('input[name="pf_nominee_ratio[]"]').val();
			});
		}
		if( $('#gi_entitled').is(':checked') ) {
			data += '&gi_nominee_counter=' + $('#gi tbody tr').length;
			var i = 0;
			$('#gi tbody tr').each(function() {
				data += '&gi_nominee_' + i + '=' + $(this).find('input[name="gi_nominee_name[]"]').val()
												+ '_' + $(this).find('input[name="gi_nominee_relation[]"]').val()
												+ '_' + $(this).find('input[name="gi_nominee_ratio[]"]').val();
			});
		}*/
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_entitlement' + data + '&pf_nominee_counter='+correct_pf_nominee_counter+'&gi_nominee_counter='+correct_gi_nominee_counter +'&nocache=' + nocache );
		http.onreadystatechange = response_emp_entitlement;
		http.send(null);
	}
}
//Employee Entitlement Response of Employee Information ( Save / Update )
function response_emp_entitlement() {
	if(http.readyState == 4) {
		//alert (http.responseText);return;
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Experience of Employee Information ( Save / Update )
function fnc_emp_experience(save_perm,edit_perm,delete_perm,approve_perm) {
	
	var save_up=$("#save_up_experience").val();
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = true, counter = 0, data = '&emp_code=' + $('#emp_code').val();
	
	if( save_up == "" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up != "" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else
	{
	$('#tbl_experience tbody tr').each(function() {
		if( $(this).find('input[name="organization_name[]"]').val() != '' && $(this).find('input[name="designation[]"]').val() != ''
			&& $(this).find('input[name="joining_date[]"]').val() != '' && $(this).find('input[name="resigning_date[]"]').val() != '' ) {
			error = false;
			data += '&' + 'organization_name_' + counter + '=' + $(this).find('input[name="organization_name[]"]').val()
					+ '&' + 'designation_' + counter + '=' + $(this).find('input[name="designation[]"]').val()
					+ '&' + 'joining_date_' + counter + '=' + $(this).find('input[name="joining_date[]"]').val()
					+ '&' + 'resigning_date_' + counter + '=' + $(this).find('input[name="resigning_date[]"]').val()
					+ '&' + 'service_length_' + counter + '=' + $(this).find('input[name="service_length[]"]').val()
					+ '&' + 'gross_salary_' + counter + '=' + $(this).find('input[name="gross_salary[]"]').val();
			counter++;
		}
	});
	data += '&counter=' + counter;
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_experience' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_experience;
		http.send(null);
	}
	else {
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#tbl_experience tbody input:first').focus();
				$(this).html('Please Enter Atleast one correct experience with first four mandatory fields.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
}
}
//Employee Experience Response of Employee Information ( Save / Update )
function response_emp_experience() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Resigning date date cannot be greater then joining date.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Education of Employee Information ( Save / Update )
function fnc_emp_education(save_perm,edit_perm,delete_perm,approve_perm) {
	
	var save_up=$("#save_up_education").val();
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = true, counter = 0, data = '&emp_code=' + $('#emp_code').val();
	
	if( save_up=="" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up !="" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{
	
	
	
	$('#tbl_education tbody tr').each(function() {
		if( $(this).find('input[name="exam_name[]"]').val() != '' && $(this).find('input[name="result[]"]').val() != '' && $(this).find('select[name="edu_nature[]"]').val() != '' && $(this).find('select[name="edu_nature[]"]').val() != '') {
			
			error = false;
			data += '&' + 'exam_name_' + counter + '=' + $(this).find('input[name="exam_name[]"]').val()
					+ '&' + 'board_' + counter + '=' + $(this).find('input[name="board[]"]').val()
					+ '&' + 'institution_' + counter + '=' + $(this).find('input[name="institution[]"]').val()
					+ '&' + 'discipline_' + counter + '=' + $(this).find('input[name="discipline[]"]').val()
					+ '&' + 'major_subject_' + counter + '=' + $(this).find('input[name="major_subject[]"]').val()
					+ '&' + 'passing_year_' + counter + '=' + $(this).find('input[name="passing_year[]"]').val()
					+ '&' + 'result_' + counter + '=' + $(this).find('input[name="result[]"]').val()
					+ '&' + 'edu_nature_' + counter + '=' + $(this).find('select[name="edu_nature[]"]').val();
			counter++;
		}
	});
	data += '&counter=' + counter;

	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_education' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_education;
		http.send(null);
	}
	else {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#tbl_experience tbody input:first').focus();
			$(this).html('Please enter atleast one correct education info with first and last mandatory fields.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
}
}
//Employee Education Response of Employee Information ( Save / Update )
function response_emp_education() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Family of Employee Information ( Save / Update )
function fnc_emp_family(save_perm,edit_perm,delete_perm,approve_perm) {
	
	var save_up=$("#save_up_familyinfo").val();
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = true, counter = 0, data = '&emp_code=' + $('#emp_code').val();
	
	if( save_up == "" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up != "" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{
	
	$('#tbl_family tbody tr').each(function() {
		if( $(this).find('input[name="fname[]"]').val() != '' && $(this).find('select[name="frelation[]"]').val() != '' ) {
			error = false;
			data += '&' + 'name_' + counter + '=' + $(this).find('input[name="fname[]"]').val()
					+ '&' + 'relation_' + counter + '=' + $(this).find('select[name="frelation[]"]').val()
					+ '&' + 'dob_' + counter + '=' + $(this).find('input[name="fdob[]"]').val()
					+ '&' + 'occupation_' + counter + '=' + $(this).find('input[name="foccupation[]"]').val()
					+ '&' + 'contact_no_' + counter + '=' + $(this).find('input[name="fcontact_no[]"]').val();
			counter++;
		}
	});
	data += '&counter=' + counter;
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_family' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_family;
		http.send(null);
	}
	else {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#tbl_family tbody input:first').focus();
			$(this).html('Please enter atleast one correct family info with first three mandatory fields.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
}
}
//Employee Family Response of Employee Information ( Save / Update )
function response_emp_family() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
	}
}

//Employee Skill of Employee Information ( Save / Update )
function fnc_emp_skill(save_perm,edit_perm,delete_perm,approve_perm) {
	//alert("su..re");
	
	var save_up=$("#save_up_skill").val();
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = true, counter = 0, data = '&emp_code=' + $('#emp_code').val();
	
	if( save_up == "" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up != "" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{
		var rowCount = $('#tbl_skill tbody tr').length;
		//alert(rowCount);
		for (var i=1; i<=rowCount; i++)
		{
			if($('#skill_id_'+i).val()!="" )
			{
				for(var j=1; j<=rowCount; j++)
				{
					if($('#skill_id_'+i).val()==$('#skill_id_'+(i+j)).val())
					{
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#skill_id_'+(i+j)).focus();
							$(this).html('You are entered duplicate value').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
						return;
					}
				}
			}
		}
					
	$('#tbl_skill tbody tr').each(function() {
		if( $(this).find('input[name="skill_id[]"]').val() != 0 ) {
			error = false;
			data += '&' + 'name_' + counter + '=' + $(this).find('input[name="skill_id[]"]').val()
					+ '&' + 'assign_' + counter + '=' + $(this).find('select[name="current_assign[]"]').val()
					+ '&' + 'effdate_' + counter + '=' + $(this).find('input[name="eff_date[]"]').val();
			counter++;
		}
	});
	data += '&counter=' + counter;
	
	//alert(data);
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_skill' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_skill;
		http.send(null);
	}
	else {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#tbl_skill tbody select:first').focus();
			$(this).html('Atleast one skill required.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
}
}
//Employee Skill Response of Employee Information ( Save / Update )
function response_emp_skill() {
	if(http.readyState == 4) {
		//alert(http.responseText);
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
	}
}

//Policy Tagging Save Update form employee information
function fnc_policy_tag_save(save_perm,edit_perm,delete_perm,approve_perm) {
	
	var save_up=$("#save_up_policytaging").val();
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false; var policy_counter = 0; var data = ''; var employees = ''; var policy = ''; var rdate = ''; var roster_val = '';
	
	if( save_up == "" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up != "" &&  edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	if( error == false ) {
		$('input[name="policy[]"]:checked').each(function() {
			policy_counter++;
			if( $('select[name="' + $(this).val() + '_policy"]').val() == 0 ) {
				error = true;
				alert( 'Please specify ' + $(this).val() + ' policy.' );
			}
			else policy += $(this).val() + '_' + $('select[name="' + $(this).val() + '_policy"]').val() + '|';
		});
	}
	
	//alert(policy);
	if(!document.getElementById('duty_roster').checked)
	{
		roster_val=0;
	}
	
	if( error == false ) {
		if( policy_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).html('Please select at lease one policy for policy tagging.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
	}
	
	if( error == false ) {
		employees = $('#emp_code').val();
		rdate = $('#txt_date').val();
		policy = policy.substr( 0, policy.length - 1 );
		data = '&employees=' + employees + '&policy=' + policy + '&rdate=' + rdate + '&roster_val=' + roster_val;	
		
		//alert(employees);return;
		
		nocache = Math.random();		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=policy_tagging' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_policy_tag_save;
		http.send(null);
	}
}
//Policy Tagging Response ( Save / Update )
function response_fnc_policy_tag_save() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' policy/(s) has been tagged successfully with ' + response[2] + ' employee/(s).' ).addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Leave policy has not been updated for the following employees: ' + response[1] + '. Reason: ' + response[2] ).addClass('messagebox_ok').fadeTo(900,1);
			});
		}
		//reset_form();
	}
}


//Weekend Save Update form employee information
function fnc_weekend(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	var weekend = $('#weekend').val();
	var weekend_id = $('#weekend_id').val();
	var save_up=$("#save_up_weekend").val();
	
	if( save_up == "" && save_perm==2){	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up != "" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	if( $('#weekend_id').val() == '' ) {
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#weekend').focus();
			$(this).html('Please select at least one weekend').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	
	
	if( error == false ) {
		employees = $('#emp_code').val();		
		data = '&employees=' + employees+'&weekends=' + weekend_id ;		
		nocache = Math.random();		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=weekend' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_weekend;
		http.send(null);
	}
}


//Weekend Response ( Save / Update )
function response_fnc_weekend() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Weekend Defined Successfully Done.' ).addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
			});
		}		
		//reset_form();
	}
}



//new add by Ekram Document Submission

function save_update_submitted_docs(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	
	var emp_code = $('#emp_code').val();
	var document_name = $('#document_name').val();
	var submitted_copy = $('#submitted_copy').val();
	var Sub_date = $('#Sub_date').val();
	var designation_id = $('#designation_id').val();
	var save_up =$('#save_up').val();
	
	var save_up_document =$('#save_up_document_submit').val();
	
	if( save_up_document =="" && save_perm==2)
	{	
		error = true;					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if( save_up_document != "" && edit_perm==2){		
		error = true;				
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	//alert(Sub_date);
	if( $('#document_name').val() == 0 ||  $('#submitted_copy').val() == '' ||  $('#Sub_date').val() == '') {
		error = true;
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#weekend').focus();
			$(this).html('Please select All Fields').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=save_needed_documents&isupdate='+save_up
		 +'&emp_code='+emp_code
		 +'&document_name='+document_name
		 +'&submitted_copy='+submitted_copy
		 +'&Sub_date='+Sub_date
		 +'&designation_id='+designation_id);
		 
		 
		 http.onreadystatechange=response_needed_documents;
		http.send(null);
		 
	}
}

//Document Submission Response ( Save / Update )
function response_needed_documents() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(http.responseText);
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				showResult_multi(designation_id,emp_code,'list_of_essential_docs','docs_list');
				$(this).fadeOut(5000);
				reset_form('document_submit_form');
			});
			
		}		
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				showResult_multi(designation_id,emp_code,'list_of_essential_docs','docs_list');
				document.getElementById('save_up').value="";
				$(this).fadeOut(5000);
				document.getElementById('document_submit_form').reset();
			});
		}
	}
}

//Employee Job Separation ( Save / Update )
function fnc_job_separation(save_perm,edit_perm,delete_perm,approve_perm,cnt) {
var r=confirm('You are going to separate  '+cnt+' Employees')
if (!r==true)
  {
  return;
  }
else
  {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, counter = 0, data = '&chk_values=';
	
	if(save_perm==2){
		error = true;		
		$(this).html('You Do Not Have Save Permission.').addClass('messageboxerror').fadeTo(900,1);
	}
	
	else if( $('#separated_from').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$('#separated_from').focus();
			$(this).html('Please enter a date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#separation_type').val() == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$('#separation_type').focus();
			$(this).html('Please select separation type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#separation_type').val() == 1  && $('#separation_period').val() == 0 ) 
	{
		$('#messagebox').fadeTo( 200, 0.1, function() {
				error = true;
				$('#separation_period').focus();
				$(this).html('Please select separation Period.').addClass('messageboxerror').fadeTo(900,1);
			});
	}
	
	else {
		var employee_counter = 0;
		$('#tbl_separation tbody input[type="checkbox"]:checked').each(function() {
				data += $(this).val() + '_';
				employee_counter++;
			});
			data = data.substr( 0, data.length - 1 );
			//alert(data);		
			if( employee_counter == 0 ) {
				error = true;
				$("#messagebox").removeClass().addClass('messageboxerror').text('Please select at least one employee.').fadeIn(1000);							
			}
			else data += '&separated_from=' + $('#separated_from').val() + '&separation_type=' + $('#separation_type').val() + '&cause_of_separation=' + $('#cause_of_separation').val() + '&separation_period=' + $('#separation_period').val();
		//alert(data);	
		if( error == false ){
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_hr_admin.php?action=job_separation' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_job_separation;
			http.send(null);
		}
	}
  }
}
//Employee Job Separation Response ( Save / Update )
function response_job_separation() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');
			
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' employee/(s) has been separated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				
				save_activities_history('','hrm','job_separation','save','../');
				
			});
			populate_data();
		}
		
		else if (response[0]==4)
		{
			
			alert('The Period You Selected Is Locked......');
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('The Period You Selected Is Locked.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(3000);
			});
		}
		else if (response[0]==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Database is Busy, please try after some time.').addClass('messagebox_ok').fadeTo(900,1);
			});
		}
		
	}
}
//Employee Job Reactivation ( Save / Update )
function fnc_job_reactivation(save_perm,edit_perm,delete_perm,approve_perm,cnt) {
	var r=confirm('You are going to re-activate '+cnt+' Employees')
if (!r==true)
  {
  return;
  }
else
  {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, counter = 0, data = '&chk_values=';
	
	if(save_perm==2){
		error = true;		
		$(this).html('You Do Not Have Save Permission.').addClass('messageboxerror').fadeTo(900,1);
	}
	
	else if( $('#rejoin_date').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$('#rejoin_date').focus();
			$(this).html('Please enter a date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	else{		
			
			var employee_counter = 0;		
			$('#tbl_reactivation tbody input[type="checkbox"]:checked').each(function() {
				data += $(this).val() + '_';
				employee_counter++;
			});
			data = data.substr( 0, data.length - 1 );
			
			if( employee_counter == 0 ) {
				error = true;	
				$("#messagebox").removeClass().addClass('messageboxerror').text('Please select at least one employee.').fadeIn(1000);
			}
			else data += '&rejoin_date=' + $('#rejoin_date').val() + '&service_benefit_from=' + $('#service_benefit_from').val();
		
		if(error ==false){			
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_hr_admin.php?action=job_reactivation' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_job_reactivation;
			http.send(null);
		}
	}
  }
}
//Employee Job Reactivation Response ( Save / Update )
function response_job_reactivation() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' employee/(s) has been reactivated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				save_activities_history('','hrm','job_reactivation','save','../');
			});
		populate_data();	
		}
		
		else if (response[0]==4)
		{
			
			alert('The Period You Selected Is Locked......');
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('The Period You Selected Is Locked.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if (response[0]==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Database is Busy, please try after some time.').addClass('messagebox_ok').fadeTo(900,1);
			});
		}
		
	}
}


//Policy Tagging ( Save / Update )
function fnc_policy_tagging(save_perm,edit_perm,delete_perm,approve_perm) {
	//alert ('sohel');
	//$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if(document.policy_tagging_form.chk_yes.checked==true)
	{
		var chk_policy_yes=1;
	}
	else
	{
		var chk_policy_yes=0;
	}
	if(document.policy_tagging_form.chk_no.checked==true)
	{
		var chk_policy_no=1;
	}
	else
	{
		var chk_policy_no=0;
	}
		//alert (chk_policy_yes);	return;
			
	$("#messagebox").html('<img src="../images/loading.gif" />').fadeIn(1000);
	var error = false; var employee_counter = 0; var policy_counter = 0; var data = ''; var employees = ''; var policy = '';
	
	if( save_perm==2){	// no save permision
		error = true;
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	if( edit_perm==2){	// no edit permision
		error = true;
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	if($('#Check_ctr').attr('checked'))
		{
			//alert ('1');
			employees = '&cbo_emp_category=' + $('#cbo_emp_category').val()
				+ '&cbo_company_id=' + $('#cbo_company_id').val() 
				+ '&location_id=' + $('#location_id').val()
				+' &division_id=' + $('#division_id').val() 
				+ '&department_id=' + $('#department_id').val()				
				+ '&section_id=' + $('#section_id').val() 
				+ '&designation_id=' + $('#designation_id').val()
				+ '&subsection_id=' + $('#subsection_id').val()
				+ '&policy_id=' + $('#policy_id').val()
				+'&chk_policy_yes='+chk_policy_yes
				+'&chk_policy_no='+chk_policy_no;
		}
		else
		{
			$('#tbl_policy_tagging tbody input[type="checkbox"]:checked').each(function() {
				employees += $(this).val() + '_';
				employee_counter++;
			});	
		}
	if( employees == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$(this).html('Please select at lease one employee for policy tagging.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	/*if( employee_counter > 100 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$(this).html('Please select maximum 100 employees for policy tagging.').addClass('messageboxerror').fadeTo(900,1);
		});
	}*/
	if( error == false ) {
		$('input[name="policy[]"]:checked').each(function() {
			policy_counter++;
			if( $('select[name="' + $(this).val() + '_policy"]').val() == 0 ) {
				error = true;
				alert( 'Please specify ' + $(this).val() + ' policy.' );
			}
			else policy += $(this).val() + '_' + $('select[name="' + $(this).val() + '_policy"]').val() + '|';
		});
	}
	if( error == false ) {
		if( policy_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).html('Please select at lease one policy for policy tagging.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
	}
	
	if( error == false ) {
		nocache = Math.random();
		employees = employees.substr( 0, employees.length - 1 );
		policy = policy.substr( 0, policy.length - 1 );
		var data = 'employees=' + employees+ '&policy=' + policy + '&employee_counter=' + employee_counter + '&action=policy_tagging&nocache=' + nocache;		
		//alert(data);return;
		http.open('POST','includes/save_update_hr_admin.php',true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", data.length);
		http.setRequestHeader("Connection", "close");
		http.onreadystatechange = response_policy_tagging;
		http.send(data);
	}
}
//Policy Tagging Response ( Save / Update )
function response_policy_tagging() {
	if(http.readyState == 4)
	 {
		var response = http.responseText.split('_');	
		//alert(http.responseText);
		//alert($('#policy_tagging').html());
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' policy/(s) has been tagged successfully with ' + response[2] + ' employee/(s).' ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				save_activities_history('','hrm','policy_tagging','save/update','../');
			});
			//populate_data();
		}
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Leave policy has not been updated for the following employees: ' + response[1] + '. Reason: ' + response[2] ).addClass('messagebox_ok').fadeTo(900,1);
			});
			//populate_data();
		}
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Reason: ' + response[2]+'&nbsp;'+response[4]  ).addClass('messagebox_ok').fadeTo(900,1);
			});
			//populate_data();
		}
		populate_data();		
	}
}
	
	
//----------------------------------------Disciplinary Info ( Save / Update )---------------------------------------------------//
function fnc_disciplinary_information_save(save_perm,edit_perm,delete_perm,approve_perm) {
	var i,j=0;
	var emp_code=new Array();	
	//for save emp_code(multiple) assign here 
	for (i = 1; i < document.getElementsByName('chk_all').length; i++)
		{	
			var td_id=$('#'+i).val();
			var tr_checked_id=i+'_'+td_id;			
			if(document.getElementById(i).checked)
				{					
					emp_code[j++] = td_id;					
				}
		}	
	//for update emp_code assign here
	if(document.getElementById('save').value=='Update')
		{
			var emp_code=escape(document.getElementById('txt_emp_code_search').value);
		}	
		
	var txt_occurrence_date 	= escape(document.getElementById('txt_occurrence_date').value);
	var txt_action_date 	= escape(document.getElementById('txt_action_date').value);
	var txt_occurrence_details 	= escape(document.getElementById('txt_occurrence_details').value);
	var txt_investigating_members 	= escape(document.getElementById('txt_investigating_members').value);
	var txt_investigation 	= escape(document.getElementById('txt_investigation').value);
	var cbo_action_taken 	= escape(document.getElementById('cbo_action_taken').value);
	var txt_withdawn_date 	= escape(document.getElementById('txt_withdawn_date').value);	
	var txt_comm_form_date 	= escape(document.getElementById('txt_comm_form_date').value);
	var txt_report_date 	= escape(document.getElementById('txt_report_date').value);
	
	var save_up 	= escape(document.getElementById('save_up').value);
	var cbo_impact_salary_head="";
	var txt_impact_salary_head_perc="";
	if( cbo_action_taken == 1 ) 
		{
			var tbl_row_count=document.getElementById('td_sal_head').rows.length;		
			for( s=1; s<tbl_row_count; s++) 
				{
					var cbo_impact_salary_head_data	= escape(document.getElementById('cbo_impact_salary_head'+s).value);
					var txt_impact_salary_head_perc_data = escape(document.getElementById('txt_impact_salary_head_perc'+s).value);
					cbo_impact_salary_head+=cbo_impact_salary_head_data + ",";
					txt_impact_salary_head_perc+=txt_impact_salary_head_perc_data + ",";
				}
		}
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
		
	if( save_up=="" && save_perm==2){	// no save permision					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox			 
			$(this).html('You Do Not Have Insert Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
		}
	else if( save_up!="" && edit_perm==2){	// no edit permision							
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox				 
			$(this).html('You Do Not Have Update Permission.').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Information').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_occurrence_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_occurrence_date').focus();
			$(this).html('Please Select Occurance Date').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if(emp_code.length==""){						
		$("#messagebox").fadeTo(200,0.1,function(){			
			$(this).html('Please Select Employees').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		if (cbo_action_taken==1)
			{
				http.open('get','includes/save_update_hr_admin.php?action=disciplinary_information&isupdate='+save_up+
						'&txt_emp_code='+emp_code+					
						'&txt_occurrence_date='+txt_occurrence_date+
						'&txt_action_date='+txt_action_date+
						'&txt_occurrence_details='+txt_occurrence_details+
						'&txt_investigating_members='+txt_investigating_members+
						'&txt_investigation='+txt_investigation+
						'&cbo_action_taken='+cbo_action_taken+
						'&txt_withdawn_date='+txt_withdawn_date+
						'&cbo_impact_salary_head='+cbo_impact_salary_head+
						'&txt_comm_form_date='+txt_comm_form_date+
						'&txt_report_date='+txt_report_date+
						'&txt_impact_salary_head_perc='+txt_impact_salary_head_perc);
						
			}
		else  
			{
			http.open('get','includes/save_update_hr_admin.php?action=disciplinary_information&isupdate='+save_up+
						'&txt_emp_code='+emp_code+					
						'&txt_occurrence_date='+txt_occurrence_date+
						'&txt_action_date='+txt_action_date+
						'&txt_occurrence_details='+txt_occurrence_details+
						'&txt_investigating_members='+txt_investigating_members+
						'&txt_investigation='+txt_investigation+
						'&cbo_action_taken='+cbo_action_taken+
						'&txt_comm_form_date='+txt_comm_form_date+
						'&txt_report_date='+txt_report_date+
						'&txt_withdawn_date='+txt_withdawn_date);
			}
		http.onreadystatechange = disciplinary_information_reply;
		http.send(null); 
	}
}
//Disciplinary Info Response( Save / Update )
function disciplinary_information_reply() {
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		//alert(response);
		if (response==2) {
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','employee information','save','../');
				$(this).fadeOut(5000);
				document.getElementById('save_up').value="";
				document.getElementById('disciplinary_information').reset();
			});
		}
		else if (response==3) {
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','hrm','disciplinary information','save','../');
				$(this).fadeOut(5000);
				document.getElementById('save').value="Save";
				document.getElementById('disciplinary_information').reset();
				document.getElementById('save_up').value="";
			});
		}
		
	}
}
//-------------------------------------END Disciplinary Action-----------------------------------------//
  
  
  //Resource Allocation issue item----------------

 function fnc_resource_allocation(save_perm,edit_perm,delete_perm,approve_perm)
 {
	var txt_emp_code=escape(document.getElementById('txt_emp_code').value);
	var txt_issue_date= escape(document.getElementById('txt_issue_date').value);
	var resource_category= escape(document.getElementById('resource_category').value);
	var txt_item= escape(document.getElementById('txt_item').value);
	var txt_rate= escape(document.getElementById('txt_rate').value);
	var txt_qnty_issued= escape(document.getElementById('txt_qnty_issued').value);
	var txt_amount= escape(document.getElementById('txt_amount').value);
	var save_up= escape(document.getElementById('save_up').value);
	
	//alert(resource_category);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}	
	else if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}else if($('#txt_issue_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_issue_date').focus();
			$(this).html('Please Insert Issue Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_item').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_item').focus();
			$(this).html('Please Insert Issue Item Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	/*else if($('#txt_rate').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_rate').focus();
			$(this).html('Please insert Issue Item Rate').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}*/
	else if($('#txt_qnty_issued').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_qnty_issued').focus();
			$(this).html('Please insert Issue Item Quantity Rate').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else
	{
		nocache = Math.random();
		http.open('GET','resource_allocation/save_resource_allocation.php?action=issue_item&isupdate='+save_up+
				    
					'&txt_emp_code='+txt_emp_code+
				    '&txt_issue_date='+txt_issue_date+
					'&resource_category='+resource_category+
					'&txt_item='+txt_item+
					'&txt_rate='+txt_rate+
					'&txt_qnty_issued='+txt_qnty_issued+
					'&txt_amount='+txt_amount+
					'&nocache='+nocache);
		http.onreadystatechange = fnc_resource_allocation_reply_info;
		http.send(null); 
	 }
 }
function fnc_resource_allocation_reply_info() {
	if(http.readyState == 4) {
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Issue Item has been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","resource_allocation_issue","insert","../");
			});
			showResult_search(document.getElementById("txt_emp_code").value,"resource_issue_item","list_view");

				document.getElementById('resource_category').value=0;
				document.getElementById('txt_issue_date').value="";
				document.getElementById('txt_item').value="";
				document.getElementById('txt_rate').value="";
				document.getElementById('txt_qnty_issued').value="";
				document.getElementById('txt_amount').value="";
				//document.getElementById('txt_item').focus();
		}
		if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Issue Item has been updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","resource_allocation_issue","update","../");

			});
			
			showResult_search(document.getElementById("txt_emp_code").value,"resource_issue_item","list_view");
			
				document.getElementById('resource_category').value=0;
				document.getElementById('txt_issue_date').value="";
				document.getElementById('txt_item').value="";
				document.getElementById('txt_rate').value="";
				document.getElementById('txt_qnty_issued').value="";
				document.getElementById('txt_amount').value="";
				document.getElementById('save_up').value="";
				//document.getElementById('txt_item').focus();
		}
		
		if( response == 5 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('In Same Date Item duplication not allowed.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","resource_allocation_issue","update","../");

			});
			
				
		}
		//reset_form();
	}
}

//Resource Allocation Return item
function fnc_resource_return_item(save_perm,edit_perm,delete_perm,approve_perm)
{
	var txt_emp_code=escape(document.getElementById('txt_emp_code').value);
	var txt_returned_date= escape(document.getElementById('txt_returned_date').value);
	var return_resource_category= escape(document.getElementById('return_resource_category').value);
	var txt_item_return= escape(document.getElementById('txt_item_return').value);
	var txt_quty_return= escape(document.getElementById('txt_quty_return').value);
	var txt_balance_qnty= escape(document.getElementById('txt_balance_qnty').value);
	var txt_balance_amount= escape(document.getElementById('txt_balance_amount').value);
	var save_up= escape(document.getElementById('save_up0').value);
	
	//alert(txt_emp_code+txt_returned_date+txt_item_return+txt_quty_return+txt_balance_qnty+txt_balance_amount+"hidden"+save_up);
	//alert(return_resource_category);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}else if($('#txt_returned_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_returned_date').focus();
			$(this).html('Please Insert Reurned Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_item_return').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_item_return').focus();
			$(this).html('Please Select Return Item Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_quty_return').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_quty_return').focus();
			$(this).html('Please insert Item Quantity').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else
	{
		nocache = Math.random();
		http.open('GET','resource_allocation/save_resource_allocation.php?action=return_item&isupdate='+save_up+
				    
					'&txt_emp_code='+txt_emp_code+
				    '&txt_returned_date='+txt_returned_date+
					'&return_resource_category='+return_resource_category+
					'&txt_item_return='+txt_item_return+
					'&txt_quty_return='+txt_quty_return+
					'&txt_balance_qnty='+txt_balance_qnty+
					'&txt_balance_amount='+txt_balance_amount+
					'&nocache='+nocache);
		http.onreadystatechange = fnc_resource_return_allocation_reply_info;
		http.send(null); 
	}
}

function fnc_resource_return_allocation_reply_info() {
	if(http.readyState == 4) {
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Return Item has been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","resource_allocation_return","insert","../");
			});
			
			showResult_search(document.getElementById("txt_emp_code").value,"return_item","list_view1");
			
				document.getElementById('txt_returned_date').value="";
				document.getElementById('txt_item_return').value="";
				document.getElementById('txt_quty_return').value="";
				document.getElementById('txt_balance_qnty').value="";
				document.getElementById('txt_balance_amount').value="";
				//document.getElementById('txt_returned_date').focus();
		}
		if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Return Item has been updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","resource_allocation_return","update","../");



			});
			//setTimeout('showResult_search(document.getElementById("txt_emp_code").value,"return_item","list_view1")',150);
		showResult_search(document.getElementById("txt_emp_code").value,"return_item","list_view1");
			
				document.getElementById('txt_returned_date').value="";
				document.getElementById('txt_item_return').value="";
				document.getElementById('txt_quty_return').value="";
				document.getElementById('txt_balance_qnty').value="";
				document.getElementById('txt_balance_amount').value="";
				document.getElementById('save_up0').value="";
				//document.getElementById('txt_returned_date').focus();
		}
		
		//reset_form();
	}
}

//Resource Allocation money recovery
function fnc_resource_money_recovery(save_perm,edit_perm,delete_perm,approve_perm)
{
	var txt_emp_code=escape(document.getElementById('txt_emp_code').value);
	var recovered_date= escape(document.getElementById('recovered_date').value);
	var txt_net_recovered= escape(document.getElementById('txt_net_recovered').value);
	var cbo_salary_head= escape(document.getElementById('cbo_salary_head').value);
	var cbo_salary_period= escape(document.getElementById('cbo_salary_period').value);
	var save_up1= escape(document.getElementById('save_up1').value);
	
	//alert(recovered_date+txt_net_recovered+cbo_salary_head+cbo_salary_period+save_up1);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up1=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up1!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#recovered_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#recovered_date').focus();
			$(this).html('Please Insert Recovered Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_net_recovered').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_net_recovered').focus();
			$(this).html('Please Insert Recovered Money').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	
	else if($('#cbo_salary_head').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_salary_head').focus();
			$(this).html('Please select Salary Head').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}else if($('#cbo_salary_head').val()!=0 && $('#cbo_salary_period').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_salary_period').focus();
			$(this).html('Please select Salary Period').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else
	{
		nocache = Math.random();
		http.open('GET','resource_allocation/save_resource_allocation.php?action=money_recovery&isupdate='+save_up1+
				    
					'&txt_emp_code='+txt_emp_code+
					'&recovered_date='+recovered_date+
				    '&txt_net_recovered='+txt_net_recovered+
					'&cbo_salary_head='+cbo_salary_head+
					'&cbo_salary_period='+cbo_salary_period+
					'&nocache='+nocache);
		http.onreadystatechange = fnc_resource_money_rcovery_reply_info;
		http.send(null); 
 }	
	
}
	
function fnc_resource_money_rcovery_reply_info() {
	if(http.readyState == 4) {
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Recovered Money has been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","resource_allocation_money","insert","../");
			});
			setTimeout('showResult_search(document.getElementById("txt_emp_code").value,"resource_money_recovery","list_view2")',150);
				document.getElementById('recovered_date').value="";
				document.getElementById('txt_net_recovered').value="";
				document.getElementById('cbo_salary_head').value="";
				document.getElementById('cbo_salary_period').value="";
				document.getElementById('recovered_date').focus();
		}
		if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Recovered Money has been updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","resource_allocation_money","update","../");
			});
			setTimeout('showResult_search(document.getElementById("txt_emp_code").value,"resource_money_recovery","list_view2")',150);
				document.getElementById('recovered_date').value="";
				document.getElementById('txt_net_recovered').value="";
				document.getElementById('cbo_salary_head').value="";
				document.getElementById('cbo_salary_period').value="";
				document.getElementById('save_up1').value="";
				document.getElementById('recovered_date').focus();
		}
		
		//reset_form();
	}
}
// Transport Detail Information
function fnc_transport(save_perm,edit_perm,delete_perm,approve_perm)
{
	//alert(save_perm);
	var cbo_transport_type=escape(document.getElementById('cbo_transport_type').value);
	var txt_vehicle_no= escape(document.getElementById('txt_vehicle_no').value);
	var txt_route= escape(document.getElementById('txt_route').value);
	var txt_stoppage= escape(document.getElementById('txt_stoppage').value);
	var txt_capacity= escape(document.getElementById('txt_capacity').value);
	var txt_booked=escape(document.getElementById('txt_booked').value);
	var txt_remainning= escape(document.getElementById('txt_remainning').value);
	var txt_driver_name= escape(document.getElementById('txt_driver_name').value);
	var txt_license_no= escape(document.getElementById('txt_license_no').value);
	var license_expired_date= escape(document.getElementById('license_expired_date').value);
	var txt_mobile_no= escape(document.getElementById('txt_mobile_no').value);
	var save_up= escape(document.getElementById('save_up').value);
	var emp_code=escape(document.getElementById('emp_code').value);//hidden field
	
	//alert(save_up);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#cbo_transport_type').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_transport_type').focus();
			$(this).html('Please Select transport type').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_vehicle_no').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_vehicle_no').focus();
			$(this).html('Please Insert Vehicle No.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	
	else if($('#txt_route').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_route').focus();
			$(this).html('Please Insert Route Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select At List One Employee').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#txt_capacity').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_capacity').focus();
			$(this).html('Please Insert Vehicle Capacity').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	/*else if($('#txt_driver_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_driver_name').focus();
			$(this).html('Please Insert Driver\'s Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}*/
	/*else if($('#txt_license_no').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_license_no').focus();
			$(this).html('Please Insert Driving License No.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}*/
	/*else if($('#license_expired_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#license_expired_date').focus();
			$(this).html('Please Insert Expired Licencse Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}*/
	else
	{
		nocache = Math.random();
		http.open('GET','resource_allocation/save_resource_allocation.php?action=transport&isupdate='+save_up+
				    
					'&cbo_transport_type='+cbo_transport_type+
				    '&txt_vehicle_no='+txt_vehicle_no+
					'&txt_route='+txt_route+
					'&txt_stoppage='+txt_stoppage+
					'&txt_capacity='+txt_capacity+
					'&txt_booked='+txt_booked+
				    '&txt_remainning='+txt_remainning+
					'&txt_driver_name='+txt_driver_name+
					'&txt_license_no='+txt_license_no+
					'&license_expired_date='+license_expired_date+
					'&txt_mobile_no='+txt_mobile_no+
					'&emp_code='+emp_code+
					'&nocache='+nocache);
		http.onreadystatechange = fnc_transport_reply_info;
		http.send(null); 
 	}	
	
}

function fnc_transport_reply_info() {
	if(http.readyState == 4) {
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() 
			{
				$(this).html('Transport Detail have been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","transport_users","insert","../");
			});
			
			//showResult_search_transport(document.getElementById("cbo_transport_type").value,document.getElementById("txt_vehicle_no").value,"transport_detail","transport_div");]
			//check_count();
			//document.getElementById('checkbox').checked=false;
			
			document.getElementById('check_all').checked=false;
			document.getElementById('cbo_transport_type').value=0;
			document.getElementById('txt_vehicle_no').value="";
			document.getElementById('txt_route').value="";
			document.getElementById('txt_stoppage').value="";
			document.getElementById('txt_capacity').value="";
			document.getElementById('txt_booked').value="";
			document.getElementById('txt_remainning').value="";
			document.getElementById('txt_driver_name').value="";
			document.getElementById('txt_license_no').value="";
			document.getElementById('license_expired_date').value="";
			document.getElementById('txt_mobile_no').value="";
			document.getElementById('save_up').value="";
			document.getElementById('emp_code').value="";
			document.getElementById('emp_code_data').value=" ";	
		}
		if( response == 2 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() 
			{
				$(this).html('Transport Detail have been updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","transport_users","update","../");
			});
			//showResult_search_transport(document.getElementById("cbo_transport_type").value,document.getElementById("txt_vehicle_no").value,"transport_detail","transport_div");
			//document.getElementById('checkbox').checked=false;
			//check_count();
			document.getElementById('check_all').checked=false;
			document.getElementById('cbo_transport_type').value=0;
			document.getElementById('txt_vehicle_no').value="";
			document.getElementById('txt_route').value="";
			document.getElementById('txt_stoppage').value="";
			document.getElementById('txt_capacity').value="";
			document.getElementById('txt_booked').value="";
			document.getElementById('txt_remainning').value="";
			document.getElementById('txt_driver_name').value="";
			document.getElementById('txt_license_no').value="";
			document.getElementById('license_expired_date').value="";
			document.getElementById('txt_mobile_no').value="";
			document.getElementById('save_up').value="";
			document.getElementById('emp_code').value="";
			document.getElementById('emp_code_data').value=" ";	
		}
		//reset_form();
	}
}



 //--------------------------------Manual attendence search-----------------//
	function fn_search_manual_attendence(){	
	
		//var temp_id=0;
		//var temp_name=0;
		 selected_id_attnd.length = 0;  selected_id.length = 0; 
		// var selected_id = new Array ;
       // var selected_id_attnd = new Array ;
		
		var emp_category	= $('#cbo_emp_category').val();
		var company_id		= $('#cbo_company_id').val();
		var location_id		= $('#cbo_location_id').val();
		var division_id		= $('#cbo_division_id').val();
		var department_id	= $('#cbo_department_id').val();
		var section_id		= $('#cbo_section_id').val();
		var subsection_id	= $('#cbo_subsection_id').val();
		var designation_id	= $('#cbo_designation_id').val();
		
			
		var from_date		= $('#txt_from_date').val();
		var to_date			= $('#txt_to_date').val();
		var attn_policy		= $('#cbo_attn_policy').val();
		var attn_status		= $('#cbo_attn_status').val();
		var emp_status		= $('#cbo_emp_status').val();
		var id_card			= $('#txt_id_card').val();
		var code_no			= $('#txt_emp_code').val();
		
		//alert(id_card); 
		$('#selected_id_i').val(''); 
		$('#selected_id_emp').val(''); //initialize
		//selected_id = new Array ; //initialize
		//selected_id_attnd = new Array ; //initialize
		
		//$("#messagebox").removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
		$("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
		
		if(cbo_company_id==0)
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cbo_company_id').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if(txt_from_date=='')
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_from_date').focus();
				$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if( txt_to_date=='')
		{ 
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_to_date').focus();
				$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}	
		else
		{
			var data='type=search_list_manual_attendence&emp_category='+emp_category+
						'&company_id='+company_id+
						'&location_id='+location_id+
						'&division_id='+division_id+
						'&department_id='+department_id+
						'&section_id='+section_id+
						'&subsection_id='+subsection_id+
						'&designation_id='+designation_id+
						'&from_date='+from_date+
						'&to_date='+to_date+
						'&attn_policy='+attn_policy+
						'&attn_status='+attn_status+
						'&emp_status='+emp_status+
						'&id_card='+id_card+
						'&code_no='+code_no;
			//alert(data);
			
			http.open('POST','includes/list_view.php',false);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", data.length);
			http.setRequestHeader("Connection", "close");
			http.onreadystatechange = show_search_list_manual_attendence;
			http.send(data);
		}
	}
	function show_search_list_manual_attendence()
	{
		if(http.readyState == 4) {
			var response = http.responseText;
			//alert(response);
			document.getElementById('manual_attendence_list_container').innerHTML=http.responseText;
			
			$("#messagebox").removeClass().addClass('messagebox').text('Generated Successfuly....').fadeIn(3000);
			
          
	         var tableFilters = {
				  col_0: "none",
				 //col_18: "none",
				 //col_17: "none",
				 //col_19: "none",
				 //col_20: "none",
				 //col_21: "none",
				 //col_22: "none",
				
				// btn_reset: true[this line adds a reset button]
				
				
					/*col_operation: {
					id: [],
				   col: [],
				   operation: [],
				   write_method: []
					}*/
				 }
			
 	        setFilterGrid("table_body",-1,tableFilters);
			hide_search_panel();
			$("#messagebox").fadeOut(5000);
					
		}
		else
		{
			$("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
		}
	}
	
		
	//manual attendance updated here
	function fn_update_attn_manual(save_perm,edit_perm,delete_perm,approve_perm)
  	{
	  //validating message	
	  //$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	  $("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
	  if (edit_perm==2 || save_perm==2){
				$("#messagebox").removeClass().addClass('messagebox').text('You do not have save & update permission').fadeIn(1000);
	  }
	  
	  var row_id  = ($('#selected_id_i').val()).split(',');
	  var attn_id = ($('#selected_id_emp').val()).split(',');
	  var counts = row_id.length;
	  //alert(counts);
	  if($('#selected_id_i').val()==''){
		  	$("#messagebox").removeClass('messagebox').addClass('messagebox').text('Validating.....Atleast 1 employee have to select!!!!').fadeIn(1000);
	  }
	 
	  var employee_code; var present_date; var in_time; var out_time; var status; var delay; var is_next; var ot_min; var early; var remarks;var take_tiffin;var take_dinner;var is_questionable;var is_manualy_update; var req_ot_org;		
		
	  fn_add_values_in_html_on_save();
	
	  for(i=0;i<counts;i++){
		  
		if(i==0){
			employee_code 	=$('#B'+row_id[i]+1).html();
			present_date	=$('#F'+row_id[i]+5).html(); //date field in html		
			//in_time		=$('#G'+row_id[i]+6).html();
			in_time			=$('#in_time_hours_'+row_id[i]).val()+':'+$('#in_time_minuties_'+row_id[i]).val()+':'+$('#in_time_seconds_'+row_id[i]).val();
			//alert(in_time);
			//out_time		=$('#H'+row_id[i]+7).html();
			out_time		=$('#out_time_hours_'+row_id[i]).val()+':'+$('#out_time_minuties_'+row_id[i]).val()+':'+$('#out_time_seconds_'+row_id[i]).val();
			status			=$('#I'+row_id[i]+8).html();
			delay			=$('#J'+row_id[i]+9).html();
			is_next			=$('#K'+row_id[i]+10).html();
			ot_min			=$('#L'+row_id[i]+11).html();
			early			=$('#M'+row_id[i]+12).html();
			//remarks		=$('#Q'+row_id[i]+16).html();
			take_tiffin		=$('#Q'+row_id[i]+17).html();
			take_dinner		=$('#X'+row_id[i]+18).html();
			is_questionable	=$('#Z'+row_id[i]+21).html();
			is_manualy_update=$('#Z'+row_id[i]+20).html();
			req_ot_org=$('#AZ'+row_id[i]+22).html();
		  }
		else
		{
			//alert(i);
			employee_code 	+="__"+$('#B'+row_id[i]+1).html();
			present_date	+="__"+$('#F'+row_id[i]+5).html(); //date field in html		
			//in_time		+="__"+$('#G'+row_id[i]+6).html();
			in_time			+="__"+$('#in_time_hours_'+row_id[i]).val()+':'+$('#in_time_minuties_'+row_id[i]).val()+':'+$('#in_time_seconds_'+row_id[i]).val();
			//alert(in_time);
			//out_time		+="__"+$('#H'+row_id[i]+7).html();
			out_time		+="__"+$('#out_time_hours_'+row_id[i]).val()+':'+$('#out_time_minuties_'+row_id[i]).val()+':'+$('#out_time_seconds_'+row_id[i]).val();
			status			+="__"+$('#I'+row_id[i]+8).html();
			delay			+="__"+$('#J'+row_id[i]+9).html();
			is_next			+="__"+$('#K'+row_id[i]+10).html();
			ot_min			+="__"+$('#L'+row_id[i]+11).html();
			early			+="__"+$('#M'+row_id[i]+12).html();
			//remarks			+="__"+$('#Q'+row_id[i]+16).html();
			take_tiffin		+="__"+$('#X'+row_id[i]+17).html();
			take_dinner		+="__"+$('#Y'+row_id[i]+18).html();
			is_questionable	+="__"+$('#Z'+row_id[i]+21).html();	
			req_ot_org+="__"+$('#AZ'+row_id[i]+22).html();		
		}
	}
		
		var data = 'type=update_attn_table&employee_code='+employee_code+'&present_date='+present_date+'&in_time='+in_time+'&out_time='+out_time+'&status='+status+'&delay='+delay+'&is_next='+is_next+'&ot_min='+ot_min+'&early='+early+'&remarks='+remarks+'&take_tiffin='+take_tiffin+'&take_dinner='+take_dinner+'&is_questionable='+is_questionable+'&counts='+counts+'&req_ot_org='+req_ot_org;
		
		//alert(data);
		http.open('POST','includes/manual_attendance_data.php',true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", data.length);
		http.setRequestHeader("Connection", "close");
		http.onreadystatechange = response_fn_update_attn_manual;
		http.send(data);

  }
 

//Manual Attendace Response ( Update )
function response_fn_update_attn_manual() {
	if(http.readyState == 4) {
		var response = http.responseText.split('###');
		//alert(response[0]);	
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(2000);
				selected_id.length = 0;
		 		selected_id_attnd.length=0;
				  $('#selected_id_i').val('');
				 $('#selected_id_emp').val('');
				 
				 save_activities_history('','hrm','manual_attendace','update','../');
				//below line uncheck checkboxes
				$("#table_body").find('tr').each(function()
				{
					$(this).find('input[name=chk_all]').attr('checked', false);
				});
				
				
				document.getElementById('show_count_list').innerHTML="";
				
			});			
		}
		else {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Manual Attendace Problem Occured.' ).addClass('messagebox_ok').fadeTo(900,1);
			});
		}		
	}
}

//--------------------------------------end manual  attendance--------------------------------------//



	
    
//===============================OT Requsition start=================================================	
	//manual attendance updated here
function fn_update_ot_manual(save_perm,edit_perm,delete_perm,approve_perm)
  	{
		if(!document.getElementById('emp_code_dtl'))
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				error = true;
				$(this).html('Click Search Button and Show OT Requisition List View.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
			});
			return;
		}
		
	var cbo_company_id=escape(document.getElementById('cbo_company_id').value);
	var location_id= escape(document.getElementById('location_id').value);
	var department_id= $('#department_id').val();//escape(document.getElementById('department_id').value);
	var section_id=$('#section_id').val();// escape(document.getElementById('section_id').value);
	var subsection_id= $('#subsection_id').val();//escape(document.getElementById('subsection_id').value);
	var designation_id=$('#designation_id').val();// escape(document.getElementById('designation_id').value);
	
	var shift_id= escape(document.getElementById('shift_id').value);
	var txt_from_date= escape(document.getElementById('txt_from_date').value);
	var txt_to_date= escape(document.getElementById('txt_from_date').value);
	//var txt_to_date= escape(document.getElementById('txt_to_date').value);
	var cbo_emp_category=escape(document.getElementById('cbo_emp_category').value);
	var cbo_emp_status=escape(document.getElementById('cbo_emp_status').value);
	var txt_out_time_hour=escape(document.getElementById('txt_out_time_hour').value);
	var txt_out_time_minute=escape(document.getElementById('txt_out_time_minute').value);
	var emp_code_dtl=document.getElementById('emp_code_dtl').value; 
	var confarm_save_update=document.getElementById('confarm_save_update').value;
	
	//alert (confarm_save);
	
	  $("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
	  
	  if (edit_perm==2 || save_perm==2){
				$("#messagebox").removeClass().addClass('messagebox').text('You do not have save & update permission').fadeIn(1000);
				return;
	  }
	  var row_id  = ($('#selected_id_i').val()).split(',');
	  var attn_id = ($('#selected_id_emp').val()).split(',');
	  var counts = row_id.length;
	  var id_card=$('#id_card').val();
	  var emp_code=$('#emp_code').val();
	  var check_all=0;
	  if(emp_code!="" || id_card!="") check_all=1;
	  
	 // alert(attn_id);
	 /* if($('#selected_id_i').val()==''){
		  	$("#messagebox").removeClass('messagebox').addClass('messagebox').text('Validating.....Atleast 1 employee have to select!!!!').fadeIn(1000);
	  return;
	  }*/
	  
	 if( $('#chk_all').attr('checked') && check_all==0 )
	 {
		var data = 'type=save_update_requisition&cbo_company_id='+cbo_company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&shift_id='+shift_id+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date+'&cbo_emp_category='+cbo_emp_category+'&cbo_emp_status='+cbo_emp_status+'&txt_out_time_hour='+txt_out_time_hour+'&txt_out_time_minute='+txt_out_time_minute+'&emp_code_dtl=all_emp'+'&confarm_save_update='+confarm_save_update;
		//alert (data);
	 }
	 else
	 {
		if($('#emp_code_dtl').val()=='')
		{
			$("#messagebox").removeClass('messagebox').addClass('messagebox').text('Validating.....Atleast 1 employee have to select!!!!').fadeIn(1000);
			return;
		}
		 var data = 'type=save_update_requisition&cbo_company_id='+cbo_company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&shift_id='+shift_id+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date+'&cbo_emp_category='+cbo_emp_category+'&cbo_emp_status='+cbo_emp_status+'&txt_out_time_hour='+txt_out_time_hour+'&txt_out_time_minute='+txt_out_time_minute+'&emp_code_dtl='+emp_code_dtl+'&confarm_save_update='+confarm_save_update;
		//alert (data);return;
	 }
		
		http.open('POST','includes/ot_requisition.php',true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", data.length);
		http.setRequestHeader("Connection", "close");
		http.onreadystatechange = response_fn_update_attn_manual_ot;
		http.send(data);
  }
 
//Manual Attendace Response ( Update )
function response_fn_update_attn_manual_ot() {
	if(http.readyState == 4)
	 {
		//alert (http.responseText);
		var response = http.responseText;
		
		if( response == 1 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('OT Requisition have been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
		}
		else if( response == 2 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('This OT Update successfully .').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
		}
		else if( response == 5 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Database Busy, Please Wate.........').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
			
			return;
		}
		else if (response==4)
		{
			
			alert("The Period You Selected is Locked......");
			
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('The Period You Selected Is Locked.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).css('background-color', 'red');
				$(this).fadeOut(5000);
			});
			
			return;
		}
		
		
		//fn_process_attendance();
	}
}

//=================================================================OT Requsition end=================================================	

function fn_process_attendance()
{
	var txt_from_date= escape(document.getElementById('txt_from_date').value);
	var txt_to_date= escape(document.getElementById('txt_to_date').value);
	var txt_emp_code=document.getElementById('emp_code_dtl').value;
	//alert (txt_emp_code);
	 if( $('#chk_all').attr('checked') )
	 {
		 txt_emp_code="";
	 }
	var data = 'action=data_processing&txt_emp_code='+txt_emp_code+'&txt_from_date='+txt_from_date+'&txt_to_date='+txt_to_date+'&from_requisition=from_requisition';
	
	http.open('POST','attendance_leave_management/data_management/includes/data_processing.php',true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.setRequestHeader("Content-length", data.length);
	http.setRequestHeader("Connection", "close");
	http.onreadystatechange = response_tmp_process;
	http.send(data);
}

function response_tmp_process()
{
	if(http.readyState == 4)
	 {
		var response = http.responseText;
		if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Overtime Processed Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//document.getElementById('process_anim').innerHTML='';
				//$("#process_anim").html('').fadeIn(1000);
				//$("#download_anim").removeClass().addClass('messagebox').html('').fadeIn(1000);
			});
		}
	 }
}
//--------------------------------------end manual  attendance--------------------------------------//






//Confirmation Letter data send-----------------------//
 function fnc_confirmation_letter(str,type){
		
		ajax.requestFile = "hrm_bangla_report_data.php?search_string="+str+"&type="+type;	// Specifying which file to get
		ajax.onCompletion = show_fnc_confirmation_letter;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_confirmation_letter()
{
	 //alert(ajax.response);	
	var response=(ajax.response).split('####');
	//$('#data_panel2').html( '<a href="' + response[1] + '"> Print </a>' + response[0] );
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';	 
	document.getElementById('confirm_div').innerHTML=response[0];
}
//-------------------------END search----------------------------//


//Leave Record data send-----------------------//
 function fnc_leave_record(str,type){
		
		ajax.requestFile = "hrm_bangla_report_data.php?search_string="+str+"&type="+type;	// Specifying which file to get
		ajax.onCompletion = show_fnc_leave_record;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_leave_record()
{
	//alert(ajax.response);	
	//var response=(ajax.response).split('####');
	var response=ajax.response;
	//$('#data_panel2').html( '<a href="' + response[1] + '"> Print </a>' + response[0] );
	
	//document.getElementById('data_panel1').innerHTML='<a href="' + response[1] + '"> Print </a>';
	document.getElementById('confirm_div').innerHTML=response;
}
//-------------------------END search----------------------------//

//Appointment Letter data send-----------------------//
function fnc_appointment_letter(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var emp_code=$('#txt_emp_code').val();				
		ajax.requestFile = 'includes/get_data_update.php?type=appointment_letter&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_fnc_appointment_letter;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_appointment_letter()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}

//Appointment Letter data send-----------------------//

//Appointment Letter for assistant manager data send-----------------------//
function fnc_appointment_letter_for_manager(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var emp_code=$('#txt_emp_code').val();				
		ajax.requestFile = '../../includes/get_data_update.php?type=appointment_letter_assistant_manager&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_fnc_appointment_letter_assistant_manager;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_appointment_letter_assistant_manager()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}

//Appointment Letter for assistant manager data send end-----------------------//



//Appointment Letter for assistant worker data send-----------------------//
function fnc_appointment_letter_for_worker(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var emp_code=$('#txt_emp_code').val();				
		ajax.requestFile = '../../includes/get_data_update.php?type=appointment_letter_assistant_worker&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_fnc_appointment_letter_for_worker;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_appointment_letter_for_worker()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}

//Appointment Letter for assistant  worker data send end-----------------------//


//Apponment Letter for assistant officer---------------------------------------//
function fnc_appointment_letter_for_assistant_officer(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var emp_code=$('#txt_emp_code').val();				
		ajax.requestFile = '../../includes/get_data_update.php?type=appointment_letter_assistant_officer&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_fnc_appointment_letter_for_assistant_officer;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_appointment_letter_for_assistant_officer()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}

//Apponment Letter for assistant officer end---------------------------------------//

//Apponment Letter for staff---------------------------------------//

function fnc_appointment_letter_for_staff(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var emp_code=$('#txt_emp_code').val();				
		ajax.requestFile = '../../includes/get_data_update.php?type=appointment_letter_assistant_staff&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_appointment_letter_assistant_staff;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_appointment_letter_assistant_staff()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}

//Apponment Letter for staff end---------------------------------------//


//Apponment Letter for Peon start---------------------------------------//

function fnc_appointment_letter_for_peon(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var emp_code=$('#txt_emp_code').val();				
		ajax.requestFile = '../../includes/get_data_update.php?type=appointment_letter_assistant_peon&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_appointment_letter_assistant_peon;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_appointment_letter_assistant_peon()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}


//Apponment Letter for Peon end---------------------------------------//

//Apponment Letter for all start---------------------------------------//

function fnc_appointment_letter_for_all(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var emp_code=$('#txt_emp_code').val();				
		ajax.requestFile = '../../includes/get_data_update.php?type=application_letter_for_all&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_fnc_appointment_letter_for_all;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_appointment_letter_for_all()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}


//Apponment Letter for all end---------------------------------------//





//Party Ledger data send-----------------------//
function fnc_party_ledger(){		
		$("#messagebox").removeClass().addClass('messagebox').text('Generatting....').fadeIn(1000);
		var data=$('#cbo_company').val()+"_"+$('#cbo_party').val()+"_"+$('#start_date').val()+"_"+$('#end_date').val()+"_"+$('#bill_type').val();
		var emp_code=$('#cbo_company').val();				
		ajax.requestFile = 'reports/sub_generate_report.php?type=party_ledger&data='+data;	// Specifying which file to get
		ajax.onCompletion = show_fnc_party_ledger;	// Specify function that will be executed after file has been found
		ajax.runAJAX();		
}

function show_fnc_party_ledger()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Successfully....').fadeIn(1000);
	var response=(ajax.response).split('####');		
	document.getElementById('data_panel1').innerHTML='<a href="'+response[1]+'" > Print </a>';
	document.getElementById('data_panel2').innerHTML=response[0];	
}
//Party Ledger data send-----------------------//

function fn_search_result_employment()
{
	var emp_code=$('#txt_emp_code').val();
	if(emp_code=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Search field is blank.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else 
	{		
		ajax.requestFile = 'includes/get_data_update.php?type=employment&getClientId='+emp_code;	// Specifying which file to get
		ajax.onCompletion = show_fn_search_result_employment;	// Specify function that will be executed after file has been found
		ajax.runAJAX();
		
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$(this).html('Report generated successfully.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}


function show_fn_search_result_employment()
{
	//alert(ajax.response);
	var response=(ajax.response).split('####');
	//$('#data_panel2').html( '<a href="' + response[1] + '"> Print </a>' + response[0] );
	
	document.getElementById('data_panel1').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
	document.getElementById('data_panel2').innerHTML=response[0];
}

// Loan Application sohel
function fnc_loan_application(save_perm,edit_perm,delete_perm,approve_perm)
 {
	
	var txt_emp_code=escape(document.getElementById('txt_emp_code').value);
	var txt_basic_salary= escape(document.getElementById('txt_basic_salary').value);
	var txt_gross_salary= escape(document.getElementById('txt_gross_salary').value);
	var joining_date= escape(document.getElementById('joining_date').value);
	var loan_date= escape(document.getElementById('loan_date').value);
	var txt_loan_amount= escape(document.getElementById('txt_loan_amount').value);
	var payment_date=escape(document.getElementById('payment_date').value);
	var txt_loan_type= escape(document.getElementById('txt_loan_type').value);
	var txt_loan_purpose= escape(document.getElementById('txt_loan_purpose').value);
	var combo_approved_status= escape(document.getElementById('combo_approved_status').value);
	var hid_txt_approved_by= escape(document.getElementById('hid_txt_approved_by').value);
	var hid_txt_recommended_by= escape(document.getElementById('hid_txt_recommended_by').value);
	var approved_amount= escape(document.getElementById('approved_amount').value);
	var approval_date= escape(document.getElementById('approval_date').value);
	var save_up= escape(document.getElementById('save_up').value);
	
	//alert(approval_date);return;
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if (approve_perm==2) // no approved 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have approved permission.').fadeIn(1000);
	}
	else if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#loan_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#loan_date').focus();
			$(this).html('Please Select Loan Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#txt_loan_amount').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_loan_amount').focus();
			$(this).html('Please Insert Loan Amount').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#payment_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#payment_date').focus();
			$(this).html('Please Select Payment Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#txt_loan_type').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_loan_type').focus();
			$(this).html('Please Select Loan Type').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#txt_loan_purpose').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_loan_purpose').focus();
			$(this).html('Please Select Loan Purpose').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else
	{
		nocache = Math.random();
		http.open('GET','includes/save_loan_application.php?action=loan_application&isupdate='+save_up+
				    
					'&txt_emp_code='+txt_emp_code+
				    '&txt_basic_salary='+txt_basic_salary+
					'&txt_gross_salary='+txt_gross_salary+
					'&joining_date='+joining_date+
					'&loan_date='+loan_date+
					'&txt_loan_amount='+txt_loan_amount+
					'&payment_date='+payment_date+
					'&txt_loan_type='+txt_loan_type+
					'&txt_loan_purpose='+txt_loan_purpose+
					'&combo_approved_status='+combo_approved_status+
					'&hid_txt_approved_by='+hid_txt_approved_by+
					'&hid_txt_recommended_by='+hid_txt_recommended_by+
					'&approved_amount='+approved_amount+
					'&approval_date='+approval_date+
					'&nocache='+nocache);
		http.onreadystatechange = fnc_loan_application_reply_info;
		http.send(null); 
	 }
 }
function fnc_loan_application_reply_info() {
	if(http.readyState == 4) {
		var response = http.responseText;	
		//alert(response);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 );
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","loan_entry","insert","../../");
				
				showResult_search_loan(document.getElementById("txt_emp_code").value,"loan_app","loan_application_div");
				document.getElementById('save_up').value="";
				document.getElementById('hid_txt_approved_by').value="";
				document.getElementById('hid_txt_recommended_by').value="";
				document.getElementById('loan_date').value="";
				document.getElementById('txt_loan_amount').value="";
				document.getElementById('payment_date').value="";
				document.getElementById('txt_loan_type').value=0;
				document.getElementById('txt_loan_purpose').value=0;
				document.getElementById('combo_approved_status').value=1;
				document.getElementById('txt_approved_by').value="";
				document.getElementById('txt_recommended_by').value="";
				document.getElementById('approved_amount').value="";
				document.getElementById('approval_date').value="";
				show_hide_approval(1);
			});
			
		}
		
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 );
				$(this).fadeOut(5000);
				save_activities_history('',"hrm","loan_entry","update","../../");
				
				showResult_search_loan(document.getElementById("txt_emp_code").value,"loan_app","loan_application_div");
				document.getElementById('save_up').value="";
				document.getElementById('hid_txt_approved_by').value="";
				document.getElementById('hid_txt_recommended_by').value="";
				document.getElementById('loan_date').value="";
				document.getElementById('txt_loan_amount').value="";
				document.getElementById('payment_date').value="";
				document.getElementById('txt_loan_type').value=0;
				document.getElementById('txt_loan_purpose').value=0;
				document.getElementById('combo_approved_status').value=1;
				document.getElementById('txt_approved_by').value="";
				document.getElementById('txt_recommended_by').value="";
				document.getElementById('approved_amount').value="";
				document.getElementById('approval_date').value="";
				document.getElementById('txt_emp_code').disabled=false;
				show_hide_approval(1);

			});
		
		}
		
		//reset_form();
	}
}
// Payback Scheduling
function fnc_payback_scheduling(save_perm,edit_perm,delete_perm,approve_perm)
 {
	var txt_emp_code=escape(document.getElementById('txt_emp_code').value);
	var txt_approved_amount=escape(document.getElementById('txt_approved_amount').value);
	var combo_interest_type= escape(document.getElementById('combo_interest_type').value);
	var txt_interest_rate= escape(document.getElementById('txt_interest_rate').value);
	var text_total_interest= escape(document.getElementById('text_total_interest').value);
	var txt_total_amount= escape(document.getElementById('txt_total_amount').value);
	var combo_adjustment_source= escape(document.getElementById('combo_adjustment_source').value);
	var txt_starting_month=escape(document.getElementById('txt_starting_month').value);
	var txt_total_installment= escape(document.getElementById('txt_total_installment').value);
	var combo_adjustment_advice= escape(document.getElementById('combo_adjustment_advice').value);
	var txt_amount_per_inst= escape(document.getElementById('txt_amount_per_inst').value);
	var txt_install_no_for_inst= escape(document.getElementById('txt_install_no_for_inst').value);
	var save_up= escape(document.getElementById('save_up').value);
	
	//alert(combo_adjustment_advice);return;
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Code').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else if($('#combo_interest_type').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#combo_interest_type').focus();
			$(this).html('Please Select Interest Type').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#txt_total_installment').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_total_installment').focus();
			$(this).html('Please Insert Total Installment').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#combo_adjustment_source').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#combo_adjustment_source').focus();
			$(this).html('Please Select Adjustment Source').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#txt_starting_month').val()=="" || $('#txt_starting_month').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_starting_month').focus();
			$(this).html('Please Select Starting Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	/*else if($('#combo_adjustment_advice').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#combo_adjustment_advice').focus();
			$(this).html('Please Select Adjustment Advice').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}*/
	else if(($('#combo_adjustment_advice').val()==2 || $('#combo_adjustment_advice').val()==3) && $('#txt_install_no_for_inst').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_install_no_for_inst').focus();
			$(this).html('Please Insert Number of Installment for Interest or Principal').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else
	{
		nocache = Math.random();
		http.open('GET','includes/save_loan_application.php?action=payback_scheduling&isupdate='+save_up+
				    
					'&txt_emp_code='+txt_emp_code+
					'&txt_approved_amount='+txt_approved_amount+
				    '&combo_interest_type='+combo_interest_type+
					'&txt_interest_rate='+txt_interest_rate+
					'&text_total_interest='+text_total_interest+
					'&txt_total_amount='+txt_total_amount+
					'&combo_adjustment_source='+combo_adjustment_source+
					'&txt_starting_month='+txt_starting_month+
					'&txt_total_installment='+txt_total_installment+
					'&combo_adjustment_advice='+combo_adjustment_advice+
					'&txt_amount_per_inst='+txt_amount_per_inst+
					'&txt_install_no_for_inst='+txt_install_no_for_inst+
					'&nocache='+nocache);
		http.onreadystatechange = fnc_payback_scheduling_reply_info;
		http.send(null); 
	 }
 }
function fnc_payback_scheduling_reply_info() {
	if(http.readyState == 4) {
		//alert (http.responseText);return;
		var response = http.responseText;	
		//alert(response);return;
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo( 900, 1 );
				save_activities_history('',"hrm","payback_scheduling","insert","../../");
				
				showResult_search_loan(document.getElementById("txt_emp_code").value,"payback_scheduling","payback_scheduling_div");
				document.getElementById('save_up').value="";
				document.getElementById('combo_interest_type').value=3;
				document.getElementById('txt_interest_rate').value="";
				document.getElementById('text_total_interest').value="";
				document.getElementById('txt_total_amount').value="";
				document.getElementById('txt_total_installment').value="";
				document.getElementById('combo_adjustment_source').value=0;
				document.getElementById('txt_starting_month').value="";
				document.getElementById('combo_adjustment_advice').value=0;
				document.getElementById('txt_amount_per_inst').value="";
				document.getElementById('txt_install_no_for_inst').value="";
				show_hide_interest(0);
			});
			
		}
		
		else if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo( 900, 1 );
				save_activities_history('',"hrm","payback_scheduling","update","../../");
				
				showResult_search_loan(document.getElementById("txt_emp_code").value,"payback_scheduling","payback_scheduling_div");
				document.getElementById('save_up').value="";
				document.getElementById('combo_interest_type').value=3;
				document.getElementById('txt_interest_rate').value="";
				document.getElementById('text_total_interest').value="";
				document.getElementById('txt_total_amount').value="";
				document.getElementById('txt_total_installment').value="";
				document.getElementById('combo_adjustment_source').value=0;
				document.getElementById('txt_starting_month').value="";
				document.getElementById('combo_adjustment_advice').value=0;
				document.getElementById('txt_amount_per_inst').value="";
				document.getElementById('txt_install_no_for_inst').value="";
				document.getElementById('txt_emp_code').disabled=false;
				show_hide_interest(0);

			});
		
		}
		
		//reset_form();
	}
}
// Early Adjustment Information
function fnc_early_adjustment(save_perm,edit_perm,delete_perm,approve_perm)
{
	if($('#combo_adjust_mode').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#combo_adjust_mode').focus();
			$(this).html('Please Select Adjustment Mode').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else
	{
		var save_paid="";
		var tot_paid="";
		var txt_emp_code= escape(document.getElementById('txt_emp_code').value);
		var combo_adjust_mode= escape(document.getElementById('combo_adjust_mode').value);
		var txt_cash= escape(document.getElementById('txt_cash').value);
		var txt_gross_salary= escape(document.getElementById('txt_gross_salary').value);
		var txt_loan_balance= escape(document.getElementById('txt_loan_balance').value);
		var save_up= escape(document.getElementById('save_up').value);
		var tot_row=document.getElementById('tot_row').value;
		//alert (tot_row);
		if(combo_adjust_mode==3)
		{
			/*for(i=1;i<=tot_row;i++)
			{
				if(document.getElementById('adjusted'+i).value=="")
				{
					save_paid=2;
				}
				else
				{
					save_paid=1;	
				}
				tot_paid += ',' + save_paid;
				tot_paid += ',' + document.getElementById('tot_id'+i).value+"_";
			}*/
			
			var cash_val=document.getElementById('txt_cash').value;
			var paid_val="";
			for(i=1;i<=tot_row;i++)
			{
				//tot_paid += ',' + save_paid;
				if(document.getElementById('adjusted'+i).value!="")
				{
					//if(i!=1) paid_val +=",";	
					var per_cash=document.getElementById('adjusted'+i).value;
					paid_val = document.getElementById('adjusted'+i).value+"_"+document.getElementById('pay_date'+i).value+"_"+document.getElementById('tot_id'+i).value;
				}
				
				if(i!=1) tot_paid +="_";	
				tot_paid += document.getElementById('tot_id'+i).value;
			}
			
			var total_amount=(cash_val*1+per_cash*1);
			if(txt_loan_balance>total_amount || txt_loan_balance<total_amount)
			{
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
					$('#combo_adjust_mode').focus();
					$(this).html('Please Type Exact Amount').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
					return;
				});	
			}
			//alert (paid_val);
		}
		else if(combo_adjust_mode==2)
		{
			//save_paid=2;
			var paid_val=document.getElementById('txt_cash').value;	
			for(i=1;i<=tot_row;i++)
			{
				//tot_paid += ',' + save_paid;
				//tot_paid += ',' + document.getElementById('tot_id'+i).value+"_";
				if(i!=1) tot_paid +="_";	
				tot_paid += document.getElementById('tot_id'+i).value;
			}	
		}
		else if(combo_adjust_mode==1) 
		{
			//save_paid=1;
			//alert (tot_row);
			var paid_val="";
			for(i=1;i<=tot_row;i++)
			{
				//tot_paid += ',' + save_paid;
				if(document.getElementById('adjusted'+i).value!="")
				{
					//if(i!=1) paid_val +=",";	
					paid_val = document.getElementById('adjusted'+i).value+"_"+document.getElementById('pay_date'+i).value+"_"+document.getElementById('tot_id'+i).value;
				}
				
				if(i!=1) tot_paid +="_";	
				tot_paid += document.getElementById('tot_id'+i).value;
			}	
			//alert (paid_val);
		}
		else if(combo_adjust_mode==4)
		{
		
			var cash_val=document.getElementById('txt_cash').value;
			var mst_id=document.getElementById('mst_id').value;
			var payment_date=document.getElementById('payment_date').value;
			var txt_total_installment=document.getElementById('txt_total_installment').value;
			
			var paid_val=cash_val+'_'+mst_id+'_'+payment_date+'_'+txt_total_installment;
			//alert (mst_id);
			for(i=1;i<=tot_row;i++)
			{
				if(i!=1) tot_paid +="_";	
				tot_paid += document.getElementById('tot_id'+i).value;
			}
			//alert(tot_paid);
			
			if(tot_paid==""){						
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
					$('#combo_adjust_mode').focus();
					$(this).html('Please Select Adjustment Mode and Adjustment Re-Schedule Again').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});		
			}
		}
		else
		{
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#combo_adjust_mode').focus();
				$(this).html('Please Select Adjustment Mode').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});			
		}
		//alert(tot_paid);
		//return;
		$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
		if (save_up=='' && save_perm==2) // no save
		{
			$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
		}
		else if (save_up!='' && edit_perm==2) // no edit 
		{
			$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
		}
		else if($('#txt_emp_code').val()==""){						
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_emp_code').focus();
				$(this).html('Please Select Employee').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});		
		}
		
		else if(($('#combo_adjust_mode').val()==1) && (txt_loan_balance*1 > txt_gross_salary*1)){						
			$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#combo_adjust_mode').focus();
				$(this).html('Please Select Different Adjustment Mode').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});		
		}
		else
		{
			nocache = Math.random();
			http.open('GET','includes/save_loan_application.php?action=early_adjustment&isupdate='+save_up+
						
						'&txt_emp_code='+txt_emp_code+
						'&combo_adjust_mode='+combo_adjust_mode+
						'&tot_paid_id='+tot_paid+
						'&paid_val='+paid_val+
						'&tot_row='+tot_row+
						'&nocache='+nocache);
			http.onreadystatechange = fnc_early_adjustment_reply_info;
			http.send(null); 
		}	
	}
}

function fnc_early_adjustment_reply_info() {
	if(http.readyState == 4) {
		var response = http.responseText;	
		//alert(response);return;
		if( response == 1 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data has been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 );
				save_activities_history('',"hrm","early_adjustment","insert","../../");
				$(this).fadeOut(10000);
			});
		}
		if( response == 2 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data have been updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 );
				save_activities_history('',"hrm","early_adjustment","update","../../");
				$(this).fadeOut(10000);

			});
		}
		//reset_form();
	}
}



// JavaScript Document

//=====================================fnc_manpower_setup start ===========================================

function fnc_manpower_setup(save_perm,edit_perm,delete_perm,is_delet)
{
	
	var cbo_company_id=escape(document.getElementById('cbo_company_id').value);
	var cbo_location_id= escape(document.getElementById('cbo_location_id').value);
	var cbo_division_id= escape(document.getElementById('cbo_division_id').value);
	//alert (cbo_division_id);
	var cbo_department_id= escape(document.getElementById('cbo_department_id').value);
	var cbo_section_id= escape(document.getElementById('cbo_section_id').value);
	var cbo_subsection_id= escape(document.getElementById('cbo_subsection_id').value);
	var cbo_emp_category=escape(document.getElementById('cbo_emp_category').value);
	//alert (cbo_emp_category);
	var cbo_desig_level= escape(document.getElementById('cbo_desig_level').value);
	var cbo_designation_id= escape(document.getElementById('cbo_designation_id').value);
	var txt_no_post= escape(document.getElementById('txt_no_post').value);
	var save_update= escape(document.getElementById('save_update').value);
	var emp_code=escape(document.getElementById('emp_code').value);//hidden field
	
	
	if($('#cbo_company_id').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	/*else if($('#cbo_location_id').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Location Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else if($('#cbo_division_id').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Division Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else if($('#cbo_department_id').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Department Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}*/
	else if($('#cbo_emp_category').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_emp_category').focus();
			$(this).html('Please Select Emp Category Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else if($('#cbo_desig_level').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_desig_level').focus();
			$(this).html('Please Select Desig Level Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else if($('#cbo_designation_id').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_designation_id').focus();
			$(this).html('Please Select Designation Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else if($('#txt_no_post').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_no_post').focus();
			$(this).html('Please Select No of Post').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	
	else
	{		
		nocache = Math.random();
		http.open('GET','includes/save_manpower_setup.php?action=manpower_setup&isupdate='+save_update+
				    
					'&cbo_company_id='+cbo_company_id+
					'&cbo_location_id='+cbo_location_id+
				    '&cbo_division_id='+cbo_division_id+
					'&cbo_department_id='+cbo_department_id+
					'&cbo_section_id='+cbo_section_id+
					'&cbo_subsection_id='+cbo_subsection_id+
					'&cbo_emp_category='+cbo_emp_category+
				    '&cbo_desig_level='+cbo_desig_level+
					'&cbo_designation_id='+cbo_designation_id+
					'&txt_no_post='+txt_no_post+
					'&save_update='+save_update+
					'&is_delet='+is_delet+
					'&emp_code='+emp_code);
		http.onreadystatechange = fnc_manpower_setup_reply_info;
		http.send(null); 
 	}	
	
}

function fnc_manpower_setup_reply_info()
{
if(http.readyState == 4)
 {
		//alert (http.responseText);
		var response = http.responseText;
			
		if( response == 1 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Manpower Setup have been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
		showResult_search_transport(document.getElementById("cbo_company_id").value,document.getElementById("cbo_emp_category").value,"manpower_setup_detail","manpower_setup_div");
		document.getElementById('cbo_company_id').value=0;
		document.getElementById('cbo_location_id').value=0;
		document.getElementById('cbo_division_id').value=0;
		document.getElementById('cbo_department_id').value=0;
		document.getElementById('cbo_section_id').value=0;
		document.getElementById('cbo_subsection_id').value=0;
		document.getElementById('cbo_emp_category').value=0;
		document.getElementById('cbo_desig_level').value=0;
		document.getElementById('cbo_designation_id').value=0;
		document.getElementById('txt_no_post').value="";
		document.getElementById('save_update').value="";
		}
		else if( response == 2 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Manpower Setup have been updated successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
		showResult_search_transport(document.getElementById("cbo_company_id").value,document.getElementById("cbo_emp_category").value,"manpower_setup_detail","manpower_setup_div");
		document.getElementById('cbo_company_id').value=0;
		document.getElementById('cbo_location_id').value=0;
		document.getElementById('cbo_division_id').value=0;
		document.getElementById('cbo_department_id').value=0;
		document.getElementById('cbo_section_id').value=0;
		document.getElementById('cbo_subsection_id').value=0;
		document.getElementById('cbo_emp_category').value=0;
		document.getElementById('cbo_desig_level').value=0;
		document.getElementById('cbo_designation_id').value=0;
		document.getElementById('txt_no_post').value="";
		document.getElementById('save_update').value="";
		}
		else if( response == 3 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Manpower Setup have been delete successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//save_activities_history('',"hrm","transport_users","insert","../");
			});
		showResult_search_transport(document.getElementById("cbo_company_id").value,document.getElementById("cbo_emp_category").value,"manpower_setup_detail","manpower_setup_div");
		document.getElementById('cbo_company_id').value=0;
		document.getElementById('cbo_location_id').value=0;
		document.getElementById('cbo_division_id').value=0;
		document.getElementById('cbo_department_id').value=0;
		document.getElementById('cbo_section_id').value=0;
		document.getElementById('cbo_subsection_id').value=0;
		document.getElementById('cbo_emp_category').value=0;
		document.getElementById('cbo_desig_level').value=0;
		document.getElementById('cbo_designation_id').value=0;
		document.getElementById('txt_no_post').value="";
		document.getElementById('save_update').value="";
		}
		//reset_form();
	}
}
//=====================================fnc_manpower_setup end ===========================================

/***********************************************************
|	START Expatriate Employee Information 					
|	onClick SAVE button following function will be execute  
|	********************************************************/
	
function fun_expatriate_employee_save(save_perm,edit_perm,delete_perm,approve_perm)
{
	//alert("I am from SAVE Button Click");
	var up_id=escape(document.getElementById('up_id').value);
	var emp_code=escape(document.getElementById('emp_code').value);
	var pass_no=escape(document.getElementById('pass_no').value);
	var pass_issue_date=escape(document.getElementById('pass_issue_date').value);	
	var pass_expire_date=(document.getElementById('pass_expire_date').value);	
	var pass_renew=escape(document.getElementById('pass_renew').value);
	var visa_no=escape(document.getElementById('visa_no').value);
	var visa_issue_date=escape(document.getElementById('visa_issue_date').value);	
	var visa_expire_date=escape(document.getElementById('visa_expire_date').value);	
	var visa_renew=escape(document.getElementById('visa_renew').value);	
	var permit_no=escape(document.getElementById('permit_no').value);
	var permit_issue_date=escape(document.getElementById('permit_issue_date').value);
	var permit_expire_date=escape(document.getElementById('permit_expire_date').value);	
	var permit_renew=escape(document.getElementById('permit_renew').value);	
	var contact_no=escape(document.getElementById('contact_no').value);	
	var contact_start=escape(document.getElementById('contact_start').value);
	var contact_end=escape(document.getElementById('contact_end').value);
	var contact_renew=escape(document.getElementById('contact_renew').value);	
	var currency=escape(document.getElementById('currency').value);	
	var local_currency=escape(document.getElementById('local_currency').value);	
	var exchange_rate=escape(document.getElementById('exchange_rate').value);
	var remark=document.getElementById('remark').value;
	//alert(pass_no);
	
	//$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if($('#emp_code').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#emp_code').focus();
			$(this).html('Browse employee code').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#pass_no').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#pass_no').focus();
			$(this).html('Enter passport number').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
/*	
else if($('#pass_issue_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#pass_issue_date').focus();
			$(this).html('Enter passport issue date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#pass_expire_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#pass_expire_date').focus();
			$(this).html('Enter passport expiry date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#pass_renew').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#pass_renew').focus();
			$(this).html('Enter passport renew date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#visa_no').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#visa_no').focus();
			$(this).html('Enter visa number').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#visa_issue_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#visa_issue_date').focus();
			$(this).html('Enter visa issue date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#visa_expire_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#visa_expire_date').focus();
			$(this).html('Enter visa expiry date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#visa_renew').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#visa_renew').focus();
			$(this).html('Enter visa renew date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#permit_no').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#permit_no').focus();
			$(this).html('Enter work permit number').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#permit_issue_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#permit_issue_date').focus();
			$(this).html('Enterwork permit issue date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#permit_expire_date').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#permit_expire_date').focus();
			$(this).html('Enter work permit expiry date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#permit_renew').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#permit_renew').focus();
			$(this).html('Enter work permit renew date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#contact_no').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#contact_no').focus();
			$(this).html('Enter contact number').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#contact_start').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#contact_start').focus();
			$(this).html('Enter contact start date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#contact_end').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#contact_end').focus();
			$(this).html('Enter contact end date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#contact_renew').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#contact_renew').focus();
			$(this).html('Enter contact renew date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#currency').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#currency').focus();
			$(this).html('Select currency').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#local_currency').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#local_currency').focus();
			$(this).html('Enter local currency').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#exchange_rate').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#exchange_rate').focus();
			$(this).html('Enter exchange rate').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#remark').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#remark').focus();
			$(this).html('Enter remark').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
*/	
	else
	{
		http.open('GET','includes/expatriate_employee.php?action=save_update_expatriate&emp_code='+emp_code+
		'&up_id='+up_id+
		'&pass_no='+pass_no+
		'&pass_issue_date='+pass_issue_date+
		'&pass_expire_date='+pass_expire_date+
		'&pass_renew='+pass_renew+
		'&visa_no='+visa_no+
		'&visa_issue_date='+visa_issue_date+
		'&visa_expire_date='+visa_expire_date+
		'&visa_renew='+visa_renew+
		'&permit_no='+permit_no+
		'&permit_issue_date='+permit_issue_date+
		'&permit_expire_date='+permit_expire_date+
		'&permit_renew='+permit_renew+
		'&contact_no='+contact_no+
		'&contact_start='+contact_start+
		'&contact_end='+contact_end+
		'&contact_renew='+contact_renew+
		'&currency='+currency+
		'&local_currency='+local_currency+
		'&exchange_rate='+exchange_rate+
		'&remark='+remark);
			
		http.onreadystatechange = response_fnc_update_expatriate_manual;
		http.send(null); 
}
}
//Expatriate Employee information's INSERT/UPDATE Response
function response_fnc_update_expatriate_manual() 
{
	if(http.readyState == 4)
	{
		//alert (http.responseText);
		var response = http.responseText;
		if( response == 1 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() 
			{
				$(this).html('Expatriate employee information have been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//document.getElementById('expatriate_employee_info').reset();
			});
		}
		else if( response == 2 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() 
			{
				$(this).html('Expatriate employee information Update successfully .').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
				//document.getElementById('expatriate_employee_info').reset();
			});
		}
		else if( response == 3 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() 
			{
				$(this).html('Fill up all field .').addClass('messagebox_ok').fadeTo( 900, 1 )
				$(this).fadeOut(5000);
			});
		}
	}
}
// END of Expatriate Employee Information

// taining name
function func_training_record(save_perm,edit_perm,delete_perm,approve_perm) {
	var employee_counter=0;
	var data='';
	$('#tbl_training_list_view tbody input[type="checkbox"]:checked').each(function() {
	employee_counter++;
	data += $(this).val() + '|';
	});		
	//alert(data);
	var training_id	=$('#training_id').val();
	var training_date	=$('#training_date').val();
	var training_place	=$('#training_place').val();
	var provided_by		=$('#provided_by').val();
	var trainer_name	=$('#trainer_name').val();
	var duration		=$('#duration').val();
	var cost			=$('#cost').val();
	var is_update		= $('#is_update').val();
	var remark        =$('#txt_remarks').val();
	//alert(is_update);
	
	//alert(training_name+", "+training_date+", "+training_place+", "+provided_by+', '+trainer_name+", "+duration+", "+cost+", "+is_update);
	//$('#messagebox').removeClass().addClass('messageboxerror').text('Validating....').fadeIn(1000);
	if (is_update=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
/*	
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete/Inactive permission.').fadeIn(1000);		
	}
*/	
	else if( training_id ==0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#training_id').focus();
			$(this).html('Please Select Training Name ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( training_date == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#training_date').focus();
			$(this).html('Please Enter Training Date ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( training_place ==0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#training_place').focus();
			$(this).html('Please Select Training Place ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( provided_by == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#provided_by').focus();
			$(this).html('Please Enter Provided By ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( trainer_name ==0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#trainer_name').focus();
			$(this).html('Please Enter Trainer Name ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( duration == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#duration').focus();
			$(this).html('Please Enter Duration ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
		else if( cost == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cost').focus();
			$(this).html('Please Enter Cost ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else {
		//alert(data);
		//nocache = Math.random();		
		http.open('GET','includes/save_update_hr_admin.php?action=training_record&training_id='+training_id+
					'&training_date='+ training_date+
					'&training_place='+ training_place+
					'&provided_by='+ provided_by+
					'&trainer_name='+ trainer_name +
					'&duration='+ duration+
					'&cost='+ cost+
					'&remarks='+remark+
					'&is_update='+ is_update+
					'&data='+data);
		http.onreadystatechange = response_training_record;
		http.send(null);
	}
}
function response_training_record() 
{
	if(http.readyState == 4) 
	{   //alert(http.responseText)
		var response = http.responseText.split("_");
		//alert(response[0]+' '+response[1]);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted successfully.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				//populate_data();
				showResult_multi(response[1],'','training_list_view','tbl_list_view');
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','designation_details','save','../../');
				$(this).fadeOut(5000);
				//populate_data();
				showResult_multi(response[1],'','training_list_view','tbl_list_view');
				//reset_form();
			});
		}
	}
}
