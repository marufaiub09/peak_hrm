<?
/*******************************************************
|	Department		: HRM
|	Mene Name		: Expatriate Employee Information
|	Developed by	: Md. Nuruzzaman
|
|	****************************************************/
	
	session_start();
	include('../../includes/common.php');
	//include('../../includes/common_functions.php');
	
	$user_name=$_SESSION['logic_erp']["user_name"];
	$date_time= date("Y-m-d H:i:s");
	
	extract ($_GET);
	extract ($_POST);
	$type = $_REQUEST['type'];
		
	if($action=="save_update_expatriate")
	{
		// Convert date Y-M-D formate
		$pass_issue_date=convert_to_mysql_date($pass_issue_date);
		$pass_expire_date=convert_to_mysql_date($pass_expire_date);
		$pass_renew=convert_to_mysql_date($pass_renew);
		$visa_issue_date=convert_to_mysql_date($visa_issue_date);
		$visa_expire_date=convert_to_mysql_date($visa_expire_date);
		$visa_renew=convert_to_mysql_date($visa_renew);
		$permit_issue_date=convert_to_mysql_date($permit_issue_date);
		$permit_expire_date=convert_to_mysql_date($permit_expire_date);
		$permit_renew=convert_to_mysql_date($permit_renew);
		$contact_start=convert_to_mysql_date($contact_start);
		$contact_end=convert_to_mysql_date($contact_end);
		$contact_renew=convert_to_mysql_date($contact_renew);
		
		if($up_id=="")
		{
		  $sql = "INSERT INTO hrm_expatriate_employee_info(
					id,
					emp_code,
					pp_number,
					pp_issue_date,
					pp_expiry_date,
					pp_renew_date,
					visa_number,
					visa_issue_date,
					visa_expiry_date,
					visa_renew_date,
					wp_number,
					wp_issue_date,
					wp_expiry_date,
					wp_renew_date,
					cont_number,
					cont_start_date,
					cont_end_date,
					cont_renew_date,
					salary_currency,
					local_currency_payment,
					notional_exchange_rate,
					remark,
					inserted_by, 
					insert_date, 
					updated_by
				) VALUES(
					'',
					'$emp_code',
					'$pass_no',
					'$pass_issue_date',
					'$pass_expire_date',
					'$pass_renew',
					'$visa_no',
					'$visa_issue_date',
					'$visa_expire_date',
					'$visa_renew',
					'$permit_no',
					'$permit_issue_date',
					'$permit_expire_date',
					'$permit_renew',
					'$contact_no',
					'$contact_start',
					'$contact_end',
					'$contact_renew',
					'$currency',
					'$local_currency',
					'$exchange_rate',
					'$remark',
					'$user_name',
					'$date_time',
					'$updated_by'
					)";
					
					mysql_query( $sql ) or die (mysql_error());
					$sql_history=encrypt($sql, "logic_erp_2011_2012");
					$_SESSION['sql_history']=$sql_history;
					echo 1;
					exit();
			}
		if($up_id!="")
		{
			$sql="UPDATE hrm_expatriate_employee_info SET 			
				emp_code='$emp_code',
				pp_number='$pass_no',
				pp_issue_date='$pass_issue_date',
				pp_expiry_date='$pass_expire_date',
				pp_renew_date='$pass_renew',
				visa_number='$visa_no',
				visa_issue_date='$visa_issue_date',
				visa_expiry_date='$permit_expire_date',
				visa_renew_date='$visa_renew',
				wp_number='$permit_no',
				wp_issue_date='$permit_issue_date',
				wp_expiry_date='$permit_expire_date',
				wp_renew_date='$permit_renew',
				cont_number='$contact_no',
				cont_start_date='$contact_start',
				cont_end_date='$contact_end',
				cont_renew_date='$contact_renew',
				salary_currency='$currency',
				local_currency_payment='$local_currency',
				notional_exchange_rate='$exchange_rate',
				remark='$remark',
				updated_by='$user_name',
				update_date='$date_time' 
				WHERE  id='$up_id' and is_deleted=0 and status_active=1";
				
				mysql_query( $sql ) or die (mysql_error());
				$sql_history=encrypt($sql, "logic_erp_2011_2012");
				$_SESSION['sql_history']=$sql_history;
				echo 2;
				exit();
		}
	}
?>