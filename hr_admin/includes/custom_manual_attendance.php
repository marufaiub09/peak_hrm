<?php
date_default_timezone_set('UTC');
session_start();

include('../../includes/common.php');
//include('../../includes/common_functions.php');

$e_date = time();
$user_only = $_SESSION["user_name"];

$user_name=$_SESSION['logic_erp']["user_name"];
$date_time= date("Y-m-d H:i:s");

extract ($_REQUEST);
 
$type = $_REQUEST['type'];

//emp_basic
	$sql = "SELECT * FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}
// Shift Details
	$shift_lists = array();
	$sql_shift = "SELECT * FROM lib_policy_shift";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$shift_lists[$row_shift[id]]['shift_name']=$row_shift[shift_name];
		$shift_lists[$row_shift[id]]['shift_start']=$row_shift[shift_start];
		$shift_lists[$row_shift[id]]['shift_end']=$row_shift[shift_end];
		$shift_lists[$row_shift[id]]['shift_type']=$row_shift[shift_type];
		$shift_lists[$row_shift[id]]['grace_minutes']=$row_shift[grace_minutes];		
		$shift_lists[$row_shift[id]]['in_grace_minutes']=add_time($row_shift[shift_start], $row_shift[grace_minutes]); // Add In Grace Time with In Time
		$shift_lists[$row_shift[id]]['grace_minutes']=$row_shift[grace_minutes];
		$shift_lists[$row_shift[id]]['cross_date']=$row_shift[cross_date];
		
		$shift_lists[$row_shift[id]]['exit_buffer_minutes']=$row_shift[exit_buffer_minutes];		
		$shift_lists[$row_shift[id]]['tiffin_start']=$row_shift[first_break_start];
		$shift_lists[$row_shift[id]]['tiffin_ends']=$row_shift[first_break_end];
		if($row_shift[first_break_start]!="00:00:00")
			$shift_lists[$row_shift[id]]['tiffin_break_min']=datediff(n,$row_shift[first_break_start],$row_shift[first_break_end]); 
		else
			$shift_lists[$row_shift[id]]['tiffin_break_min']=0;
		$shift_lists[$row_shift[id]]['dinner_start']=$row_shift[second_break_start];
		$shift_lists[$row_shift[id]]['dinner_ends']=$row_shift[second_break_end];
		if($row_shift[second_break_start]!="00:00:00")
			$shift_lists[$row_shift[id]]['dinner_break_min']=datediff(n,$row_shift[second_break_start],$row_shift[second_break_end]);
		else
			$shift_lists[$row_shift[id]]['dinner_break_min']=0;
			
		$shift_lists[$row_shift[id]]['lunch_start']=$row_shift[lunch_time_start];
		$shift_lists[$row_shift[id]]['lunch_end']=$row_shift[lunch_time_end];
		$shift_lists[$row_shift[id]]['lunch_break_min']=datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]); 
		$shift_lists[$row_shift[id]]['total_working_min']=datediff(n,$row_shift[shift_start],$row_shift[shift_end])-datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]);
		$shift_lists[$row_shift[id]]['early_out_start']=$row_shift[early_out_start];
		$shift_lists[$row_shift[id]]['entry_restriction_start']=$row_shift[entry_restriction_start];
	
	}
	
	
// OT POLICY Details
	$ot_policy_arr = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$ot_policy_arr[$row_shift[id]]['overtime_rate']=$row_shift[overtime_rate];
		$ot_policy_arr[$row_shift[id]]['overtime_calculation_rule']=$row_shift[overtime_calculation_rule];
		$ot_policy_arr[$row_shift[id]]['max_overtime']=$row_shift[max_overtime];
	}	

 //print_r($ot_policy_arr);die;

/// Duty Roster and general shift and ot policy arrray
	$roster_ot_policy_array=array();
	 
	$txt_from_date_prev_tmp = convert_to_mysql_date( $txt_from_date );
	$txt_to_date_next_tmp = convert_to_mysql_date( $txt_to_date );
	$tot_days_tmp=datediff("d",$txt_from_date_prev_tmp, $txt_to_date_next_tmp);
	
	$sql = "SELECT a.current_shift,a.overtime_policy,a.shift_date,a.emp_code FROM hrm_duty_roster_process a, hrm_employee b where b.duty_roster_policy!=0 and a.emp_code=b.emp_code and a.shift_date between '".  $txt_from_date_prev_tmp ."' and '". $txt_to_date_next_tmp ."'";
	 
	$sql_exe = mysql_query( $sql );
	while( $sql_rslt = mysql_fetch_array( $sql_exe ) )
	{
		$roster_ot_policy_array[$sql_rslt[emp_code]][$sql_rslt[shift_date]]['ot']= $sql_rslt[overtime_policy];
		$roster_ot_policy_array[$sql_rslt[emp_code]][$sql_rslt[shift_date]]['shift']= $sql_rslt[current_shift];
	}
	
	//print_r($roster_ot_policy_array);die;
	
	$sql = "SELECT a.shift_policy,a.overtime_policy,a.emp_code FROM hrm_employee a where a.duty_roster_policy=0 and a.status_active=1 and a.is_deleted=0";
	$sql_exe = mysql_query( $sql );
	while($sql_rslt = mysql_fetch_array( $sql_exe ))
	{
		for ($i=0; $i<$tot_days_tmp; $i++)
		{
			//$tmp_crnt_date = add_date( $txt_from_date_prev_tmp ,$i);
			$cd = strtotime($txt_from_date_prev_tmp);
  			$tmp_crnt_date = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$i,date('Y',$cd)));
			
			$roster_ot_policy_array[$sql_rslt['emp_code']][$tmp_crnt_date]['ot']= $sql_rslt['overtime_policy'];
			$roster_ot_policy_array[$sql_rslt['emp_code']][$tmp_crnt_date]['shift']= $sql_rslt['shift_policy'];
		}
	}	
$sql = "SELECT company_name,adjust_in_time,adjust_out_time,first_ot_limit,one_hour_ot_unit,allow_ot_fraction,ot_start_minute,dinner_ot_treatment,last_punch_time_with_ot,dinner_ot_treatment_time,reqn_based_ot,device_fixed,outtime_reqn,is_xcess_controlled,ignor_questionable_ot  FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		 	 	 	
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['dinner_ot_treatment'] = mysql_real_escape_string( $row['dinner_ot_treatment'] );
		$var_hrm_chart[$row['company_name']]['dinner_ot_treatment_time'] = mysql_real_escape_string( $row['dinner_ot_treatment_time'] );
		$var_hrm_chart[$row['company_name']]['last_punch_time_with_ot'] = mysql_real_escape_string( $row['last_punch_time_with_ot'] );
		$var_hrm_chart[$row['company_name']]['reqn_based_ot'] = mysql_real_escape_string( $row['reqn_based_ot'] );
		$var_hrm_chart[$row['company_name']]['device_fixed'] = mysql_real_escape_string( $row['device_fixed'] );
		$var_hrm_chart[$row['company_name']]['outtime_reqn'] = mysql_real_escape_string( $row['outtime_reqn'] );
		$var_hrm_chart[$row['company_name']]['is_xcess_controlled'] = mysql_real_escape_string( $row['is_xcess_controlled'] );
		$var_hrm_chart[$row['company_name']]['ignor_questionable_ot'] = mysql_real_escape_string( $row['ignor_questionable_ot'] );
	}
 $sql = "SELECT ot_date,sum(budgeted_ot) as budgeted_ot,emp_code FROM ot_requisition_dtl WHERE ot_date ='".convert_to_mysql_date( $txt_attn_date )."' group by emp_code,ot_date";
	
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$ot_requisition_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$ot_requisition_list[$row['emp_code']][$row['ot_date']] = $row['budgeted_ot'];
	}

 
//attendance table update
if($action=="custom_attn_processing")
	{	
		/*
		$emp_category 	=$emp_category;
		$company_id		=$company_id;
		$location_id	=$location_id;
		$division_id	=$division_id;
		$department_id	=$department_id;
		$section_id		=$section_id;
		$subsection_id	=$subsection_id;
		$join_status	=$join_status;
		$txt_date 		=$txt_date;
		$txt_attn_date	=$txt_attn_date;
		*/
		
		if( $company_id!=0 ) $emp_cond .=" and a.company_id='$company_id' ";
		if( $location_id!=0 ) $emp_cond .=" and a.location_id='$location_id' ";
		if( $division_id!=0 ) $emp_cond .=" and a.division_id in ( $division_id ) ";
		if( $department_id!=0 ) $emp_cond .=" and a.department_id in ( $department_id) ";
		if( $section_id!=0 ) $emp_cond .=" and a.section_id in ( $section_id) ";
		if( $subsection_id!=0 ) $emp_cond .=" and a.subsection_id in ( $subsection_id) ";
		if( $emp_category!=0 ) $emp_cond .=" and b.category in ( $emp_category) ";
	
	
	if( $join_status==1 ){
	 	$sql="SELECT a.*,b.ot_entitled,b.duty_roster_policy FROM hrm_attendance a, hrm_employee b WHERE  a.emp_code=b.emp_code and b.joining_date < '".convert_to_mysql_date($txt_date)."' and attnd_date = '".convert_to_mysql_date($txt_attn_date)."' $emp_cond";
	}
	 if( $join_status==2 ){
		$sql="SELECT a.*,b.ot_entitled,b.duty_roster_policy FROM hrm_attendance a, hrm_employee b WHERE  a.emp_code=b.emp_code and b.joining_date > '".convert_to_mysql_date($txt_date)."' and attnd_date = '".convert_to_mysql_date($txt_attn_date)."' $emp_cond";
	 }
	 if( $join_status==0){
	$sql="SELECT a.*,b.ot_entitled,b.duty_roster_policy FROM hrm_attendance a, hrm_employee b WHERE  a.emp_code=b.emp_code and  attnd_date = '".convert_to_mysql_date($txt_attn_date)."' $emp_cond";
	 }
	 
	//echo $sql;die;
	
	$sql_exe=mysql_query($sql);
	 
	while($row=mysql_fetch_array($sql_exe)) 	
	{
		if($row[duty_roster_policy]!=0)
		{
			$shift_policy=$roster_ot_policy_array[$row[emp_code]][$row['attnd_date']]['shift']; //$shift_ot_policy_arr[0];
			$ot_policy =$roster_ot_policy_array[$row[emp_code]][$row['attnd_date']]['ot']; //$shift_ot_policy_arr[1];
		}
		else
		{
			$shift_policy=$row[policy_shift_id]; //$shift_ot_policy_arr[0];
			$ot_policy =$row[policy_overtime_id]; //$shift_ot_policy_arr[1];
		}
			
		/*if( $row[is_regular_day]==1 )
		{
			 echo "3";
			 die;
		}*/
		
		 $ot_min_st = get_overtime( $row[ot_entitled], $ot_policy, $row[sign_in_time],$row[sign_out_time],1,$row[is_next_day],$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id, $row[emp_code], $row[attnd_date]);	
		
		//$ot_min_st = get_overtime($ot_entitled,$ot_policy,$new_in_time,$new_out_time,1,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id,$emp_code_tmp,$attnd_date);	
		//get_overtime($ot_entitled,$ot_policy,$new_in_time,$new_out_time,$is_regular_day,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id, $emp_code_tmp, $attnd_date);	
		
		$late_mins= get_delay_min( $row[is_regular_day], $row[sign_in_time], $shift_policy, $shift_lists );
		if(	$ot_min_st>0) $ot_min=abs($ot_min_st);else $ot_min=0;
		if(	$ot_min_st<0) $early_out=abs($ot_min_st);else $early_out=0;
		
		if ( $row[sign_in_time]=="00:00:00" && $row[sign_out_time]=="00:00:00") { $status="A"; $early_out=0; }
		if ( $row[sign_in_time]!="00:00:00" || $row[sign_out_time]!="00:00:00") $status="P";
		if ( $row[sign_in_time]=="00:00:00" && $row[sign_out_time]!="00:00:00") $is_questionale=1;
		else if ( $row[sign_in_time]!="00:00:00" && $row[sign_out_time]=="00:00:00") $is_questionale=1;
		else $is_questionale=0;
		
		if ( $late_mins>0 ) $status="D";
		$id_arr[]=$row[id]; //str_replace("'",'',$$updateidyarncost);
		
		$data_values[$row[id]]=explode(",",("'".$status."',".$ot_min.",".$late_mins.",".$early_out.",".$is_questionale.",0"));
		
	}
		$update_column="status*total_over_time_min*late_time_min*early_out_min*is_questionable*is_manually_updated";
		
		if( $company_id!=0 ) $emp_cond_cost_centre .="company_id=$company_id"."|";
		if( $location_id!=0 ) $emp_cond_cost_centre .="location_id=$location_id"."|";
		if( $division_id!=0 ) $emp_cond_cost_centre .="division_id in ( $division_id ) "."|";
		if( $department_id!=0 ) $emp_cond_cost_centre .="department_id in ( $department_id) "."|";
		if( $section_id!=0 ) $emp_cond_cost_centre .="section_id in ( $section_id) "."|";
		if( $subsection_id!=0 ) $emp_cond_cost_centre .="subsection_id in ( $subsection_id) "."|";
		if( $emp_category!=0 ) $emp_cond_cost_centre .="category=$emp_category"."|";
		
		
		/*echo "insert into manual_attendance_information (emp_code,date,inserted_by,inserted_date,updated_by,updated_date,cost_centers)
		 values  
		('$emp_cond_cost_centre','".convert_to_mysql_date($txt_attn_date)."','".$user_name."','".$date_time."','cost_centre_wise_manual_attendance','','$emp_cond_cost_centre')";die;*/
		
		//echo  bulk_update_sql_statement( "hrm_attendance","id", $update_column, $data_values, $id_arr ) ;die; 
		if( mysql_query( bulk_update_sql_statement( "hrm_attendance","id", $update_column, $data_values, $id_arr ) )){
			mysql_query("insert into manual_attendance_information (emp_code,date,inserted_by,inserted_date,updated_by,updated_date,cost_centers)
		 					values  
						('','".convert_to_mysql_date($txt_attn_date)."','".$user_name."',
						'".$date_time."','cost_centre_wise_manual_attendance','','$emp_cond_cost_centre')");
			echo "2";
		}
		else
		{
			echo "1";
		}
	 
	//echo "1"."ghgfhfg";	
	exit();

}


function bulk_update_sql_statement( $table, $id_column, $update_column, $data_values, $id_count )
{
	$field_array=explode("*",$update_column);
	//$id_count=explode("*",$id_count);
	//$data_values=explode("*",$data_values);
	//print_r($data_values);die;
	$sql_up.= "UPDATE $table SET ";
	
	 for ($len=0; $len<count($field_array); $len++)
	 {
		 $sql_up.=" ".$field_array[$len]." = CASE $id_column ";
		 for ($id=0; $id<count($id_count); $id++)
		 {
			 if (trim($data_values[$id_count[$id]][$len])=="") $sql_up.=" when ".$id_count[$id]." then  '".$data_values[$id_count[$id]][$len]."'" ;
			 else $sql_up.=" when ".$id_count[$id]." then  ".$data_values[$id_count[$id]][$len]."" ;
		 }
		 if ($len!=(count($field_array)-1)) $sql_up.=" END, "; else $sql_up.=" END ";
	 }
	 $sql_up.=" where id in (".implode(",",$id_count).")";
	 return $sql_up;     
} 

function get_overtime($ot_entitle,$ot_policy,$gen_in_time,$gen_out_time,$is_regular_day,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id,$emp_code,$attnd_date)
{
	global $var_hrm_chart;
	$get_dinner=0;
	$max_day_close_time="23:59:59";
	$p_time="00:00:00";
	$ignor_lunch_deduction=0;
	$ignor_dinner_deduction=0;
	if ( $shift_lists[$shift_policy]['dinner_start']<=$max_day_close_time and $shift_lists[$shift_policy]['dinner_start']>$shift_lists[$shift_policy]['shift_start'])
		$is_dinner_next_day=0;
	else
		$is_dinner_next_day=1; 
		 $is_questionable=0;
	if( $gen_in_time=="" || $gen_in_time=="00:00:00") $is_questionable=1;
	if( $gen_out_time=="" || $gen_out_time=="00:00:00") $is_questionable=1;
	
	$r_dinner_start=$shift_lists[$shift_policy]['dinner_start'];
	$r_dinner_ends=$shift_lists[$shift_policy]['dinner_ends'];
	
	if( $shift_lists[$shift_policy]['lunch_start']=="00:00:00" ) $ignor_lunch_deduction=1;
	if( $shift_lists[$shift_policy]['dinner_start']=="00:00:00" ) $ignor_dinner_deduction=1;
	$m_overtime_calculation_rule=  $ot_policy_arr[$ot_policy]['overtime_calculation_rule'];
	if ($m_overtime_calculation_rule=="") $m_overtime_calculation_rule=1;
	$get_dinners=0;
	$add_next_day_ot=0;
	$is_prev_day_log=0;
	if($is_next_day==1 && $shift_lists[$shift_policy]['cross_date']==0) $is_prev_day_log=1;
	
	if($is_next_day==0 && $shift_lists[$shift_policy]['cross_date']==1)
	{
		if( $gen_out_time > $shift_lists[$shift_policy]['shift_start'] && $gen_out_time < "23:59:59" && $shift_lists[$shift_policy]['shift_end']>"00:00:00" )
			return $gen_ot_time.','.$get_tiffin.','.$get_dinner.','.$is_questionable.','.$actual_ot_min;
		else if( $gen_out_time < $shift_lists[$shift_policy]['shift_end'] && $gen_out_time > "00:00:00" )
			return $gen_ot_time.','.$get_tiffin.','.$get_dinner.','.$is_questionable.','.$actual_ot_min;
	}
	
	if ($ot_entitle==1)
	{
		if ($is_questionable==0) // Regular Present 
		{
			if ( $is_regular_day==1) // Regular working Day
			{
				
				if ( $m_overtime_calculation_rule==0)  // OT Rules and Regulations
				{
					if ($is_next_day==0)
					{
						if($is_prev_day_log==0) // Current Date
						{
							
							if($ignor_lunch_deduction==0)
								$gen_ot_time=(datediff(n, $gen_in_time,$gen_out_time)-($shift_lists[$shift_policy]['total_working_min']-$shift_lists[$shift_policy]['lunch_break_min'])); 
							else
								$gen_ot_time=(datediff(n, $gen_in_time,$gen_out_time)-($shift_lists[$shift_policy]['total_working_min'])); 
						}
						else   // Date Crossover OT
						{
							$timeDiffout=datediff(n,$gen_in_time,$max_day_close_time);
							$p_time="00:00:00";
							$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
							if($ignor_lunch_deduction==0)
								$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']+$shift_lists[$shift_policy]['lunch_break_min']);
							else
								$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']);
						}
					}
					else
					{
						$timeDiffout=datediff(n,$gen_in_time,$max_day_close_time);
						$p_time="00:00:00";
						$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
						if($ignor_lunch_deduction==0)
							$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']+$shift_lists[$shift_policy]['lunch_break_min']);
						else
							$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']);
						 
					}
					 
				} 
				else 
				{
					
					if ($is_next_day==0)
					{
						
						if( $is_prev_day_log==0 ) // Current Date
							$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
						 else
							$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
							
						if($gen_ot_time<0) $gen_ot_time=0;	
					}
					else
					{
						
						if( $is_prev_day_log==0 ) // Current Date
							$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
						 else
						 {
							 $timeDiffout=datediff(n,$shift_lists[$shift_policy]['shift_end'],$max_day_close_time);
							 $p_time="00:00:00";
							 $gen_ot_time=$timeDiffout+datediff(n,$p_time,$gen_out_time);
							$add_next_day_ot=1;
							$get_dinners=1;
							//$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
						 }
					} 
				}																							
			}
			else // Not Regular Day -- Holiday
			{
				if( $shift_lists[$shift_policy]['shift_start']>$gen_in_time ) 
					$off_day_ot_in=$shift_lists[$shift_policy]['shift_start'];
				else
					$off_day_ot_in=$gen_in_time;
				
				if ( $m_overtime_calculation_rule==0)  // OT Rules and Regulations
				{
					if($is_prev_day_log==0) // Current Date
					{
						if($ignor_lunch_deduction==0)
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time)-$shift_lists[$shift_policy]['lunch_break_min'];
						else
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time);
					}
					else
					{
						$timeDiffout=datediff(n,$off_day_ot_in,$max_day_close_time);
						$p_time="00:00:00";
						$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
						$add_next_day_ot=1;
						if($ignor_lunch_deduction==0) $gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['lunch_break_min']);
						else
							$gen_ot_time=$timeDiffout;
							$get_dinners=1;
					} 
				}
				else //OT Rules 2nd
				{  
				
					//echo $is_prev_day_log."sumon".$m_overtime_calculation_rule."=";
					if( $is_prev_day_log==0 && $shift_lists[$shift_policy]['cross_date']==0) // Current Date
					{
						
						if ($gen_out_time<=$shift_lists[$shift_policy]['lunch_start'])
						{
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time);
						}
						else if ($gen_out_time>=$shift_lists[$shift_policy]['lunch_start'] && $gen_out_time<=$shift_lists[$shift_policy]['lunch_end'])
						{
							$gen_ot_time=datediff(n, $off_day_ot_in,$shift_lists[$shift_policy]['lunch_start']);
						}
						else if ($gen_in_time>=$shift_lists[$shift_policy]['lunch_start'] && $gen_in_time<=$shift_lists[$shift_policy]['lunch_end'])
						{
							$gen_ot_time=datediff(n, $shift_lists[$shift_policy]['lunch_end'],$gen_out_time);
						}
						else if ($gen_in_time>=$shift_lists[$shift_policy]['lunch_end'])
						{
							 
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time);
						}
						else
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time)-$shift_lists[$shift_policy]['lunch_break_min'];
						
						
					}
					elseif( $is_prev_day_log==0 && $shift_lists[$shift_policy]['cross_date']==1 ) // Current Date
					{
						
						$timeDiffout=datediff(n,$off_day_ot_in,$max_day_close_time);
						$p_time="00:00:00";
						$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
						$add_next_day_ot=1;
						$get_dinners=1;
						if($ignor_lunch_deduction==0) $gen_ot_time=$timeDiffout-$shift_lists[$shift_policy]['lunch_break_min'];
						else $gen_ot_time=$timeDiffout;
					} 
					else
					{
						$timeDiffout=datediff(n,$off_day_ot_in,$max_day_close_time);
						$p_time="00:00:00";
						$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
						$add_next_day_ot=1;
						$get_dinners=1;
						if( $ignor_lunch_deduction==0 ) $gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['lunch_break_min']);
						else $gen_ot_time=$timeDiffout;
					}
				}
				//$gen_ot_time=$gen_ot_time;	
			}
		}
		// Questionable else here
	}
	else  // Not OT Entitled
	{
		 $gen_ot_time=0;
	}
	
	if ( $add_next_day_ot==1 ) $gen_ot_time=$gen_ot_time+1;
	$add_next_day_ot=0;
	
	if ( $shift_lists[$shift_policy]['dinner_ends']<=$max_day_close_time and $shift_lists[$shift_policy]['dinner_ends']>$shift_lists[$shift_policy]['shift_start']) $is_dinner_next_day=0;
	else $is_dinner_next_day=1; 
	
	if (  $gen_ot_time!=0 && $ignor_dinner_deduction==0 )
	{
		
		if ($var_hrm_chart[$company_id]['dinner_ot_treatment']!=0 && $get_dinners==1)

		{
			if ($var_hrm_chart[$company_id]['dinner_ot_treatment']==1) // Add OT
			{/* 
				if( $is_dinner_next_day==0 && $is_prev_day_log==1 )
				{
					$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
				}
				else if( $is_dinner_next_day==0 && $is_prev_day_log==0 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==1 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==0 )
				{
					$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
				}
			 */}
			 else //minus ot
			 {
				if( $is_dinner_next_day==0 && $is_prev_day_log==1 )
				{
					$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					$get_dinner=1;
				}
				else if( $is_dinner_next_day==0 && $is_prev_day_log==0 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
						$get_dinner=1;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
						$get_dinner=1;
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==1 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
						$get_dinner=1;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
						$get_dinner=1;
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==0 )
				{
					$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					$get_dinner=1;
				}
			 }
		}
	}
	  global $ot_requisition_list;
//return "ss".$ot_requisition_list[$emp_code][$attnd_date];

	if( $is_prev_day_log==1 ) $b_ot_amnt=$ot_requisition_list[$emp_code][$attnd_date]; 
	else $b_ot_amnt=$ot_requisition_list[$emp_code][$attnd_date]; 
	$actual_ot_min=$gen_ot_time;
	if( $gen_ot_time > 0 )
	{
		if( $var_hrm_chart[$company_id]['reqn_based_ot']==1 )
		{
			if( $b_ot_amnt < $gen_ot_time) //$budgeted_ot >0 and  
			{
				//if( $var_hrm_chart[$company_id]['outtime_reqn']==1)
					//$gen_out_time=generate_new_out_time( $gen_out_time, $shift_lists[$shift_policy]['shift_end'], $b_ot_amnt );
				$gen_ot_time=$b_ot_amnt;
				
			}//$device_fixed=$var_hrm_chart[$r_emps_data['company_id']]['device_fixed'];
		}
	}

//return "ss".$gen_ot_time;


	if($var_hrm_chart[$company_id]['ignor_questionable_ot'] ==1)
	{		
		if( $is_questionable==1 ) $gen_ot_time=0;
	} 
	$get_tiffin=0;
	// return "sss".$gen_ot_time;
	if( $shift_lists[$shift_policy]['tiffin_start']<$gen_out_time ) $get_tiffin=1;
	return $gen_ot_time.','.$get_tiffin.','.$get_dinner.','.$is_questionable.','.$actual_ot_min;
	//return "nulll".$get_tiffin;

}

function get_overtime2222( $ot_entitle, $ot_policy, $in_time, $out_time, $is_regular_day, $is_next_day, $shift_policy, $shift_lists,$ot_policy_arr,$new_sts,$company_id)
{	
	$max_day_close_time="23:59:59";
	if( $out_time=="00:00:00" ) // Regular Present )
		return 0;
	if( $in_time=="00:00:00" ) $in_time=""; // Regular Present ) 
	if ( $ot_entitle==1)
	{
		if ( $in_time!="" and $out_time!="" ) // Regular Present 
		{	
			if( $is_next_day==0 ) // Current Date
			{
				$gen_ot_time=datediff(n, $shift_lists[$shift_policy]['shift_end'], $out_time); 
				//return $gen_ot_time; 	
				 
			}
			else   // Date Crossover OT
			{						
				$timeDiffout=datediff(n,$shift_lists[$shift_policy]['shift_end'],$max_day_close_time);
				$p_time="00:00:00";
				$timeDiffout=$timeDiffout+datediff(n,$p_time,$out_time);
				//echo $timeDiffout."###".datediff(n,$p_time,$out_time);die;
				$gen_ot_time=$timeDiffout; //+$shift_lists[$r_emps_data[shift_policy]]['lunch_break_min']
				//$get_dinners=1;
				//return $gen_ot_time;  
			}
		}
		$ignor_dinner_deduction=0;
		if( $shift_lists[$shift_policy]['dinner_start']=="00:00:00" ) $ignor_dinner_deduction=1;
		if ( $shift_lists[$shift_policy]['dinner_ends']<=$max_day_close_time and $shift_lists[$shift_policy]['dinner_ends']>$shift_lists[$shift_policy]['shift_start']) $is_dinner_next_day=0;
		else $is_dinner_next_day=1; 
		
		
	}
	else return "0";
}

function get_delay_min( $is_regular_day, $in_time, $shift_policy, $shift_lists ,$status)
{	
	 
	//$new_graced_time=add_time( $in_time,$shift_lists[$shift_policy]['in_grace_minutes']);
	if($in_time!="00:00:00")
	{
		$late_min=datediff(n, $shift_lists[$shift_policy]['in_grace_minutes'], $in_time);
		if( $late_min > 0 ){ $late_min = $late_min+$shift_lists[$shift_policy]['grace_minutes']; return $late_min; }
		
	}
	
}

function get_early_min( $is_next_day,$is_regular_day, $out_time, $shift_policy, $shift_lists)
{	
	//if ( $is_next_day==0 )
		if($out_time!="00:00:00") $early_min=datediff(n,$out_time, $shift_lists[$shift_policy]['shift_end']);   
	/*else if ( $shift_lists[$row_shift[shift_name]]['cross_date']==0 && $is_next_day==1 )
		$early_min=0;
	else if ( $shift_lists[$row_shift[shift_name]]['cross_date']==1 )
		$early_min=datediff(n,$out_time, $shift_lists[$shift_policy]['shift_end']);   
	*/
	if($early_min>0) 
		return $early_min;
	else
		return "0";	
}


function add_time($event_time,$event_length)
{
	
	$timestamp = strtotime("$event_time");
	$etime = strtotime("+$event_length minutes", $timestamp);
	$etime=date('H:i:s', $etime);
	return $etime;
	 
}

function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}




function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
function return_shift_id( $shift, $date) {
	
		$sql = "SELECT shift_policy,overtime_policy FROM  lib_overtime_with_shift where shift_policy='$shift'";
		$sql_exe = mysql_query( $sql );
		$sql_rslt = mysql_fetch_array( $sql_exe );
		$current_shift = $sql_rslt[0]."###".$sql_rslt[1];
	return $current_shift;
}
?>