
<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');
include('../../includes/common_functions.php');
include('../../includes/array_function.php');

$user_only = $_SESSION["user_name"];

$user_name = $_SESSION["user_name"];
$date_time= date("Y-m-d H:i:s");
extract ($_REQUEST);



//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
	
	
if($action=="ot_adjustment")
{
	
	?>
<div style="99%;" align="left" id="ot_adjustment_list_container">
		<table cellspacing="0" cellpadding="0" width="1500" class="rpt_table" >
			<thead>				
                <th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.frm_ot_adjustment.chk_list)" placeholder="Search" /></th>
                <th width="80" >Code</th>
				<!--<th width="80">Card No</th>-->
                <th width="80">ID Card</th>
                <th width="150">Employee Name</th>
                <th width="100">Designation</th>
                <th width="80">Date</th>
                <th width="80">In Time</th>
                <th width="80">Out Time</th>                
                <th width="100">OT Min</th>
                <th width="100">Adjustment Mins</th>               
                <th width="100">Adjustment Type</th> 
                <th width="100">Net OT Min</th>
			</thead>
		</table>
</div>        
<div style="100%; overflow-y:scroll; max-height:180px;" id="ot_adjustment_list_container">

	<table cellspacing="0" cellpadding="0" width="1500" class="rpt_table" id="tbl_ot_adjustment" >
<?php	
	 
	$i=0;
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and emp.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0 || $division_id=="") $division_id=""; else $division_id="and emp.division_id in ($division_id)";
	if ($department_id==0 || $department_id=="") $department_id=""; else $department_id="and emp.department_id in ($department_id)";
	if ($section_id==0 || $section_id=="") $section_id=""; else $section_id="and emp.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=="") $subsection_id=""; else $subsection_id="and emp.subsection_id in ($subsection_id)";
	if ($designation_id==0 || $designation_id=="") $designation_id=""; else $designation_id="and emp.designation_id in ($designation_id)";	
	
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and emp.id_card_no in ('".implode("','",explode(",",$id_card))."')";	
	
	
	$sql="select CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.punch_card_no,emp.id_card_no, attd.* 
		  from hrm_employee emp, hrm_attendance attd 
		  WHERE 
			attd.status in ('P','D') and
			attd.sign_out_time<>'00:00:00' and
			emp.ot_entitled=1 and
			emp.emp_code=attd.emp_code and 
			emp.shift_policy=$cbo_shift_name and
			attd.attnd_date = '".convert_to_mysql_date($txt_date)."'
			$cbo_company_id
			$location_id
			$division_id
			$department_id
			$section_id
			$subsection_id
			$designation_id
			$category
			$id_card_no
			$emp_code
		  	order by attnd_date,emp_code";
	
	//echo $sql;
	$result = mysql_db_query( $DB, $sql ) or die( $sql . "<br />" . mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$i++;
	if ($i%2==0)  
		$bgcolor="#E9F3FF";
	else
		$bgcolor="#FFFFFF";
	
	if($cbo_adjustment_type==1) $net_ot = $row['total_over_time_min']+$txt_adjustment_mins;
	else $net_ot = $row['total_over_time_min']-$txt_adjustment_mins;
	
	 $designation = $designation_chart[$row[designation_id]];	 
	?>
		<tr style="text-decoration:none" align="center" bgcolor="<?php echo "$bgcolor"; ?>"> 
					<td id="<? echo "A".$i; ?>" width="20"><input type="checkbox" name="chk_list" id="chk_list" onclick="count_emp(document.frm_ot_adjustment.chk_list)" value="<? echo $i;?>" /></td>
					<td id="<? echo "B".$i; ?>" width="80"><?php echo $row['emp_code']; ?></td>
					<?php /*?><td id="<? echo "C".$i; ?>" width="80"><?php if( $row['punch_card_no'] != '' ) 	echo $row['punch_card_no']; ?></td> <?php */?> 
                    <td id="" width="80"><?php if( $row['id_card_no'] != '' ) 	echo $row['id_card_no']; ?></td> <!-- later include this field -->                 
					<td id="<? echo "D".$i; ?>" width="150"  title="<? echo $row['name']; ?>"><?php if( $row['name'] != '' ){ if(strlen($row['name'])>20) echo substr($row['name'],0,15)."..."; else	echo $row['name']; } ?>  </td>	
					<td id="<? echo "E".$i; ?>" width="100" title="<? echo $designation; ?>"><?php if( $row['designation_id'] != '0' ){ if(strlen($designation)>14) echo substr($designation,0,10)."..."; else	echo $designation; } ?></td> 
                    <td id="<? echo "F".$i; ?>" width="80"><?php if( $row['attnd_date'] != '0000-00-00' )	echo convert_to_mysql_date($row['attnd_date']); ?></td>                   				 	
					<td id="<? echo "G".$i; ?>" width="80"><?php echo $row['sign_in_time']; ?></td>	
                    <td id="<? echo "H".$i; ?>" width="80"><?php echo $row['sign_out_time']; ?></td>                                   				
					
                    <td id="<? echo "L".$i; ?>" width="100"><?php echo $row['total_over_time_min']; ?></td>                   
                    <td id="<? echo "M".$i; ?>" width="100"><input type="text" id="txt_adjst_min_html<? echo $i; ?>" name="txt_adjst_min_html<? echo $i; ?>" class="text_boxes" value="<?php echo $txt_adjustment_mins; ?>" style="width:80px;" onkeyup="fn_net_ot_cal(this.value,<? echo $row['total_over_time_min'];?>,<? echo $cbo_adjustment_type; ?>,'O<? echo $i;?>')" /></td>                   
                    <td id="<? echo "N".$i; ?>" width="100"><?php echo $value_status[$cbo_adjustment_type]; ?></td> 
                    <td id="<? echo "O".$i; ?>" width="100"><?php if($net_ot>0) echo $net_ot; else echo "0"; ?></td>                   
                   
				</tr>
				<?php
				
				}
				?>                
		</table>      
</div>	
<? 
	 
	exit();
}


if($action=="ot_adjustment_save")
{
							
	$explode_emp 		= 	explode("_",$employee_code);
	$explode_ot_min 	= 	explode("_",$ot_min);
	$explode_adj_min 	= 	explode("_",$adj_min);
	$explode_net_ot 	= 	explode("_",$net_ot_min);				
	$salary_p		 	= 	explode("-",$txt_date);
	$salary_periods 	= 	$salary_p[2]."-".$salary_p[1]."-01";
	$txt_date	=	convert_to_mysql_date($txt_date);
	
	for($i=0;$i<count($explode_emp);$i++)
	{
		 
		$sql_qr = mysql_query("select * from hrm_ot_adjustment where emp_code='".$explode_emp[$i]."' and overtime_date='$txt_date'");
		$numrows = mysql_num_rows($sql_qr);
		if($numrows>0)
		{
			mysql_query("delete from hrm_ot_adjustment where emp_code='".$explode_emp[$i]."' and overtime_date='$txt_date'");
		}
		
		$id= return_next_id('id','hrm_ot_adjustment');
		$sql = "INSERT INTO hrm_ot_adjustment (
				id,
				emp_code,
				salary_periods,
				overtime_date,
				shift_name,
				adjustment_type,
				adjustment_mins,
				net_ot_min,
				remarks,
				inserted_by,
				insert_date,
				updated_by				
				)
				VALUES (
				$id, 
				'".$explode_emp[$i]."', 
				'$salary_periods', 
				'$txt_date', 
				'$cbo_shift_name', 
				'$cbo_adjustment_type', 
				'".$explode_adj_min[$i]."', 
				'".$explode_net_ot[$i]."', 
				'$txt_remarks', 
				'$user_name', 
				'$date_time',
				'$updated_by'				
				)";
				
		mysql_query($sql) or die(mysql_error());		
		
		$out_time = return_field_value("sign_out_time","hrm_attendance","emp_code='".$explode_emp[$i]."' and attnd_date='$txt_date'");
		$total_over_time_min = return_field_value("total_over_time_min","hrm_attendance","emp_code='".$explode_emp[$i]."' and attnd_date='$txt_date'");
		if($cbo_adjustment_type==1) //increase
		{
			$get_out_time = add_time($out_time,$explode_adj_min[$i]);
		}
		else // decrease
		{
			if($explode_adj_min[$i]>$total_over_time_min)
			{				
				$get_out_time = add_time($out_time,-$total_over_time_min);
			}
			else 
				$get_out_time = add_time($out_time,-$explode_adj_min[$i]);
		}
		
		$update_sql = "update hrm_attendance set sign_out_time='$get_out_time', total_over_time_min='".$explode_net_ot[$i]."' where emp_code='".$explode_emp[$i]."' and attnd_date='$txt_date'";
		mysql_query($update_sql) or die(mysql_error());
				
	}
	
	echo "1";
	exit();
}
	
function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}


?>