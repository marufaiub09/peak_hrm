//Do not change 
function createObject() {
var request_type;
var browser = navigator.appName;
if(browser == "Microsoft Internet Explorer"){
request_type = new ActiveXObject("Microsoft.XMLHTTP");
}else{
request_type = new XMLHttpRequest();
}
return request_type;
}

var http = createObject();


//--------------------------Variable_Settings_Commercial Start-----------------------------------------------------------------------
function fnc_commercial_variable_settings(){
	
	var txt_mst_id						= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_com			= escape(document.getElementById('cbo_company_name_com').value);
	var cbo_variable_list_commercial	= escape(document.getElementById('cbo_variable_list_commercial').value);
	if (cbo_variable_list_commercial==5)
	{
		var capacity_in_value	= escape(document.getElementById('capacity_in_value').innerHTML);
		var txt_capacity_value	= escape(document.getElementById('txt_capacity_value').value);
		var currency			= escape( $('#currency').text() );
		var cbo_currency_id		= escape(document.getElementById('cbo_currency_id').value);
	}
	else if(cbo_variable_list_commercial==6)
	{
		var max_btb_limit		= escape( $('#max_btb_limit').text() );
		var txt_max_btb_limit	= escape(document.getElementById('txt_max_btb_limit').value);
	}
	else
	{
		var max_pc_limit		= escape( $('#max_pc_limit').text() );
		var txt_max_pc_limit	= escape(document.getElementById('txt_max_pc_limit').value);
	}
	var save_up_com 					= escape(document.getElementById('save_up_comm').value);
	alert(save_up_com);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_com').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_com').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		if (cbo_variable_list_commercial==5)
			{
				http.open('get','save_update_common_variable_settings.php?action=commercial_variable_settings&isupdate='+save_up_com+
				'&cbo_company_name_com='+cbo_company_name_com+
				'&cbo_variable_list_commercial='+cbo_variable_list_commercial+
				'&capacity_in_value='+capacity_in_value+
				'&txt_capacity_value='+txt_capacity_value+
				'&currency='+currency+
				'&cbo_currency_id='+cbo_currency_id+
				'&nocache ='+nocache);
				//alert(save_up_com);
			}
		else if(cbo_variable_list_commercial==6)
			{
				http.open('get','save_update_common_variable_settings.php?action=commercial_variable_settings&isupdate='+save_up_com+
					'&cbo_company_name_com='+cbo_company_name_com+
					'&cbo_variable_list_commercial='+cbo_variable_list_commercial+
					'&max_btb_limit='+max_btb_limit+
					'&txt_max_btb_limit='+txt_max_btb_limit+
					'&nocache ='+nocache);
					//alert(isupdate_com);
			}
		else
		{
				http.open('get','save_update_common_variable_settings.php?action=commercial_variable_settings&isupdate='+save_up_com+
				'&cbo_company_name_com='+cbo_company_name_com+
				'&cbo_variable_list_commercial='+cbo_variable_list_commercial+
				'&max_pc_limit='+max_pc_limit+
				'&txt_max_pc_limit='+txt_max_pc_limit+
				'&nocache ='+nocache);
				//alert(isupdate_com);
		}
		http.onreadystatechange = variable_settings_comReply_info;
		http.send(null); 
	}
}

function variable_settings_comReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		//alert(response);
		cbo_variable_list_commercial=document.getElementById('cbo_variable_list_commercial').value;
		save_up_com=document.getElementById('save_up_comm').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Work Order ID, Please use New  Work Order ID.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_commercial==5)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('capacity_in_value').value="";
					document.getElementById('txt_capacity_value').value="";
					document.getElementById('currency').value="";
					document.getElementById('cbo_currency_id').value="";
				}
				else if(cbo_variable_list_commercial==6)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_btb_limit').value="";
					document.getElementById('txt_max_btb_limit').value="";
				}
				else
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_pc_limit').value="";
					document.getElementById('txt_max_pc_limit').value="";
				}
			});
		}
		else(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_commercial==5)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('capacity_in_value').value="";
					document.getElementById('txt_capacity_value').value="";
					document.getElementById('currency').value="";
					document.getElementById('cbo_currency_id').value="";
				}
				else if(cbo_variable_list_commercial==6)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_btb_limit').value="";
					document.getElementById('txt_max_btb_limit').value="";
				}
				else
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_pc_limit').value="";
					document.getElementById('txt_max_pc_limit').value="";
				}
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Commercial End-----------------------------------------------------------------------


//--------------------------Variable_Settings_Order Tracking Start-----------------------------------------------------------------------
function fnc_order_tracking_variable_settings(){
	
	var txt_mst_id				= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_wo		= escape(document.getElementById('cbo_company_name_wo').value);
	var cbo_variable_list_wo	= escape(document.getElementById('cbo_variable_list_wo').value);
	if (cbo_variable_list_wo==12)
	{
		var sales_year_started	= escape(document.getElementById('sales_year_started').innerHTML);
		var cbo_sales_year_started_date	= escape(document.getElementById('cbo_sales_year_started_date').value);
	}
	var save_up_wo 					= escape(document.getElementById('save_up_woo').value);
	alert(save_up_wo);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_wo').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_wo').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		if (cbo_variable_list_wo==12)
			{
				http.open('get','save_update_common_variable_settings.php?action=order_tracking_variable_settings&isupdate='+save_up_wo+
				'&cbo_company_name_wo='+cbo_company_name_wo+
				'&cbo_variable_list_wo='+cbo_variable_list_wo+
				'&sales_year_started='+sales_year_started+
				'&cbo_sales_year_started_date='+cbo_sales_year_started_date+
				'&nocache ='+nocache);
				//alert(save_up_com);
			}
		http.onreadystatechange = variable_settings_woReply_info;
		http.send(null); 
	}
}

function variable_settings_woReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		alert(response);
		cbo_variable_list_wo=document.getElementById('cbo_variable_list_wo').value;
		save_up_wo=document.getElementById('save_up_woo').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Work Order ID, Please use New  Work Order ID.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_wo==12)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value="";
					document.getElementById('sales_year_started').value="";
					document.getElementById('cbo_sales_year_started_date').value="";
				}
			});
		}
		else(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_wo==12)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value="";
					document.getElementById('sales_year_started').value="";
					document.getElementById('cbo_sales_year_started_date').value="";
				}
				
				
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Order Tracking End-----------------------------------------------------------------------

//--------------------------Variable_Settings_Production Start-----------------------------------------------------------------------
function fnc_production_variable_settings(){
	
	var txt_mst_id						= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_production			= escape(document.getElementById('cbo_company_name_production').value);
	var cbo_variable_list_production	= escape(document.getElementById('cbo_variable_list_production').value);
	if (cbo_variable_list_production==1)
	{
		var cutting_update				= escape(document.getElementById('cutting_update').innerHTML);
		var cbo_cutting_update			= escape(document.getElementById('cbo_cutting_update').value);
		var printing_emb_production		= escape(document.getElementById('printing_emb_production').innerHTML);
		var cbo_printing_emb_production	= escape(document.getElementById('cbo_printing_emb_production').value);
		var sewing_production			= escape(document.getElementById('sewing_production').innerHTML);
		var cbo_sewing_production		= escape(document.getElementById('cbo_sewing_production').value);
		var iron_update					= escape(document.getElementById('iron_update').innerHTML);
		var cbo_iron_update				= escape(document.getElementById('cbo_iron_update').value);
		var finishing_update			= escape(document.getElementById('finishing_update').innerHTML);
		var cbo_finishing_update		= escape(document.getElementById('cbo_finishing_update').value);
	}
	else if(cbo_variable_list_production==2)
	{
		var order_quantity			= escape(document.getElementById('order_quantity').innerHTML);
		var txt_order_qty_1			= escape(document.getElementById('txt_order_qty_1').value);
		var txt_order_qty_2			= escape(document.getElementById('txt_order_qty_2').value);
		var txt_order_qty_3			= escape(document.getElementById('txt_order_qty_3').value);
		
		var excess_percent			= escape(document.getElementById('excess_percent').innerHTML);
		var txt_excess_percent_1	= escape(document.getElementById('txt_excess_percent_1').value);
		var txt_excess_percent_2	= escape(document.getElementById('txt_excess_percent_2').value);
		var txt_excess_percent_3	= escape(document.getElementById('txt_excess_percent_3').value);
	}
	else if(cbo_variable_list_production==3)
	{
		var fabric_roll_level		= escape(document.getElementById('fabric_roll_level').innerHTML);
		var cbo_fabric_roll_level	= escape(document.getElementById('cbo_fabric_roll_level').value);
	}
	else if(cbo_variable_list_production==4)
	{
		var fabric_machine_level		= escape(document.getElementById('fabric_machine_level').innerHTML);
		var cbo_fabric_machine_level	= escape(document.getElementById('cbo_fabric_machine_level').value);
	}
	else
	{
		var batch_maintained		= escape(document.getElementById('batch_maintained').innerHTML);
		var cbo_batch_maintained	= escape(document.getElementById('cbo_batch_maintained').value);
	}
	var save_up_pro 					= escape(document.getElementById('save_up_proo').value);
	alert(save_up_pro);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_production').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_production').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		if (cbo_variable_list_production==1)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
				'&cbo_company_name_production='+cbo_company_name_production+
				'&cbo_variable_list_production='+cbo_variable_list_production+
				'&cutting_update='+cutting_update+
				'&cbo_cutting_update='+cbo_cutting_update+
				'&printing_emb_production='+printing_emb_production+
				'&cbo_printing_emb_production='+cbo_printing_emb_production+
				'&sewing_production='+sewing_production+
				'&cbo_sewing_production='+cbo_sewing_production+
				'&iron_update='+iron_update+
				'&cbo_iron_update='+cbo_iron_update+
				'&finishing_update='+finishing_update+
				'&cbo_finishing_update='+cbo_finishing_update+
				'&nocache ='+nocache);
				//alert(save_up_pro);
			}
		else if(cbo_variable_list_production==2)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
					'&cbo_company_name_production='+cbo_company_name_production+
					'&cbo_variable_list_production='+cbo_variable_list_production+
					'&order_quantity='+order_quantity+
					'&txt_order_qty_1='+txt_order_qty_1+
					'&txt_order_qty_2='+txt_order_qty_2+
					'&txt_order_qty_3='+txt_order_qty_3+
					'&excess_percent='+excess_percent+
					'&txt_excess_percent_1='+txt_excess_percent_1+
					'&txt_excess_percent_2='+txt_excess_percent_2+
					'&txt_excess_percent_3='+txt_excess_percent_3+
					'&nocache ='+nocache);
					//alert(save_up_pro);
			}
		else if(cbo_variable_list_production==3)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
					'&cbo_company_name_production='+cbo_company_name_production+
					'&cbo_variable_list_production='+cbo_variable_list_production+
					'&fabric_roll_level='+fabric_roll_level+
					'&cbo_fabric_roll_level='+cbo_fabric_roll_level+
					'&nocache ='+nocache);
					//alert(save_up_pro);
			}
		else if(cbo_variable_list_production==4)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
					'&cbo_company_name_production='+cbo_company_name_production+
					'&cbo_variable_list_production='+cbo_variable_list_production+
					'&fabric_machine_level='+fabric_machine_level+
					'&cbo_fabric_machine_level='+cbo_fabric_machine_level+
					'&nocache ='+nocache);
					//alert(save_up_pro);
			}
		else
		{
			http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
				'&cbo_company_name_production='+cbo_company_name_production+
				'&cbo_variable_list_production='+cbo_variable_list_production+
				'&batch_maintained='+batch_maintained+
				'&cbo_batch_maintained='+cbo_batch_maintained+
				'&nocache ='+nocache);
				//alert(save_up_pro);
		}
		http.onreadystatechange = variable_settings_proReply_info;
		http.send(null); 
	}
}

function variable_settings_proReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		alert(response);
		cbo_variable_list_production=document.getElementById('cbo_variable_list_production').value;
		save_up_pro=document.getElementById('save_up_proo').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Work Order ID, Please use New  Work Order ID.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_production==1)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('cutting_update').value="";
					document.getElementById('cbo_cutting_update').value="";
					document.getElementById('printing_emb_production').value="";
					document.getElementById('cbo_printing_emb_production').value="";
					document.getElementById('sewing_production').value="";
					document.getElementById('cbo_sewing_production').value="";
					document.getElementById('iron_update').value="";
					document.getElementById('cbo_iron_update').value="";
					document.getElementById('finishing_update').value="";
					document.getElementById('cbo_finishing_update').value="";
				}
				else if(cbo_variable_list_production==2)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('order_quantity').value="";
					document.getElementById('txt_order_qty_1').value="";
					document.getElementById('txt_order_qty_2').value="";
					document.getElementById('txt_order_qty_3').value="";
					document.getElementById('excess_percent').value="";
					document.getElementById('txt_excess_percent_1').value="";
					document.getElementById('txt_excess_percent_2').value="";
					document.getElementById('txt_excess_percent_3').value="";
				}
				else if(cbo_variable_list_production==3)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_roll_level').value="";
					document.getElementById('cbo_fabric_roll_level').value="";
				}
				else if(cbo_variable_list_production==4)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_machine_level').value="";
					document.getElementById('cbo_fabric_machine_level').value="";
				}
				else
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('batch_maintained').value="";
					document.getElementById('cbo_batch_maintained').value="";
				}
			});
		}
		else(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_production==1)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('cutting_update').value="";
					document.getElementById('cbo_cutting_update').value="";
					document.getElementById('printing_emb_production').value="";
					document.getElementById('cbo_printing_emb_production').value="";
					document.getElementById('sewing_production').value="";
					document.getElementById('cbo_sewing_production').value="";
					document.getElementById('iron_update').value="";
					document.getElementById('cbo_iron_update').value="";
					document.getElementById('finishing_update').value="";
					document.getElementById('cbo_finishing_update').value="";
				}
				else if(cbo_variable_list_production==2)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('order_quantity').value="";
					document.getElementById('txt_order_qty_1').value="";
					document.getElementById('txt_order_qty_2').value="";
					document.getElementById('txt_order_qty_3').value="";
					document.getElementById('excess_percent').value="";
					document.getElementById('txt_excess_percent_1').value="";
					document.getElementById('txt_excess_percent_2').value="";
					document.getElementById('txt_excess_percent_3').value="";
				}
				else if(cbo_variable_list_production==3)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_roll_level').value="";
					document.getElementById('cbo_fabric_roll_level').value="";
				}
				else if(cbo_variable_list_production==4)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_machine_level').value="";
					document.getElementById('cbo_fabric_machine_level').value="";
				}
				else
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('batch_maintained').value="";
					document.getElementById('cbo_batch_maintained').value="";
				}
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Production End-----------------------------------------------------------------------

//--------------------------Variable_Settings_Inventory Start-----------------------------------------------------------------------

function fnc_inventory_variable_settings(){
	
	var txt_mst_id						= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_inventory		= escape(document.getElementById('cbo_company_name_inventory').value);
	var cbo_variable_list_inventory		= escape(document.getElementById('cbo_variable_list_inventory').value);
	if (cbo_variable_list_inventory==8)
	{
		var category		= escape(document.getElementById('category').innerHTML);
		var cbo_category	= escape(document.getElementById('cbo_category').value);
		var item_group		= escape(document.getElementById('item_group').innerHTML);
		var cbo_item_group	= escape(document.getElementById('cbo_item_group').value);
		var source			= escape(document.getElementById('source').innerHTML);
		var cbo_source		= escape(document.getElementById('cbo_source').value);
		var standard		= escape(document.getElementById('standard').innerHTML);
		var txt_standard	= escape(document.getElementById('txt_standard').value);
	}
	else if(cbo_variable_list_inventory==9)
	{
		var hide_opening_stock_flag			= escape(document.getElementById('hide_opening_stock_flag').innerHTML);
		var cbo_hide_opening_stock_flag		= escape(document.getElementById('cbo_hide_opening_stock_flag').value);
	}
	else if(cbo_variable_list_inventory==10)
	{
		var item_rate_optional		= escape(document.getElementById('item_rate_optional').innerHTML);
		var cbo_item_rate_optional	= escape(document.getElementById('cbo_item_rate_optional').value);
	}
	else
	{
		var item_qc			= escape(document.getElementById('item_qc').innerHTML);
		var cbo_item_qc		= escape(document.getElementById('cbo_item_qc').value);
	}
	var save_up_inven 					= escape(document.getElementById('save_up_invenn').value);
	alert(save_up_inven);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_inventory').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_inventory').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		if (cbo_variable_list_inventory==8)
			{
				http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings&isupdate='+save_up_inven+
				'&cbo_company_name_inventory='+cbo_company_name_inventory+
				'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
				'&category='+category+
				'&cbo_category='+cbo_category+
				'&item_group='+item_group+
				'&cbo_item_group='+cbo_item_group+
				'&source='+source+
				'&cbo_source='+cbo_source+
				'&standard='+standard+
				'&txt_standard='+txt_standard+
				'&nocache ='+nocache);
				//alert(save_up_inven);
			}
		else if(cbo_variable_list_inventory==9)
			{
				http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings&isupdate='+save_up_inven+
					'&cbo_company_name_inventory='+cbo_company_name_inventory+
					'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
					'&hide_opening_stock_flag='+hide_opening_stock_flag+
					'&cbo_hide_opening_stock_flag='+cbo_hide_opening_stock_flag+
					'&nocache ='+nocache);
					//alert(save_up_inven);
			}
		else if(cbo_variable_list_inventory==10)
			{
				http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings&isupdate='+save_up_inven+
					'&cbo_company_name_inventory='+cbo_company_name_inventory+
					'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
					'&item_rate_optional='+item_rate_optional+
					'&cbo_item_rate_optional='+cbo_item_rate_optional+
					'&nocache ='+nocache);
					//alert(save_up_inven);
			}
		else
		{
			http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings&isupdate='+save_up_inven+
				'&cbo_company_name_inventory='+cbo_company_name_inventory+
				'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
				'&item_qc='+item_qc+
				'&cbo_item_qc='+cbo_item_qc+
				'&nocache ='+nocache);
				//alert(save_up_inven);
		}
		http.onreadystatechange = variable_settings_inventoryReply_info;
		http.send(null); 
	}
}

function variable_settings_inventoryReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		alert(response);
		cbo_variable_list_inventory=document.getElementById('cbo_variable_list_inventory').value;
		save_up_inven=document.getElementById('save_up_invenn').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Work Order ID, Please use New  Work Order ID.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_inventory==8)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('category').value="";
					document.getElementById('cbo_category').value="";
					document.getElementById('item_group').value="";
					document.getElementById('cbo_item_group').value="";
					document.getElementById('source').value="";
					document.getElementById('cbo_source').value="";
					document.getElementById('standard').value="";
					document.getElementById('txt_standard').value="";
				}
				else if(cbo_variable_list_inventory==9)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('hide_opening_stock_flag').value="";
					document.getElementById('cbo_hide_opening_stock_flag').value="";
				}
				else if(cbo_variable_list_inventory==10)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_rate_optional').value="";
					document.getElementById('cbo_item_rate_optional').value="";
				}
				else
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_qc').value="";
					document.getElementById('cbo_item_qc').value="";
				}
			});
		}
		else(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_inventory==8)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('category').value="";
					document.getElementById('cbo_category').value="";
					document.getElementById('item_group').value="";
					document.getElementById('cbo_item_group').value="";
					document.getElementById('source').value="";
					document.getElementById('cbo_source').value="";
					document.getElementById('standard').value="";
					document.getElementById('txt_standard').value="";
				}
				else if(cbo_variable_list_inventory==9)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('hide_opening_stock_flag').value="";
					document.getElementById('cbo_hide_opening_stock_flag').value="";
				}
				else if(cbo_variable_list_inventory==10)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_rate_optional').value="";
					document.getElementById('cbo_item_rate_optional').value="";
				}
				else
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_qc').value="";
					document.getElementById('cbo_item_qc').value="";
				}
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Inventory End-----------------------------------------------------------------------

