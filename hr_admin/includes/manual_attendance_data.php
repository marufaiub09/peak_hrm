<?php
date_default_timezone_set("Asia/Dhaka");
session_start();

include('../../includes/common.php');
//include('../../includes/common_functions.php');

$e_date = time();
//$user_only = $_SESSION["user_name"];

$user_name = $_SESSION['logic_erp']['user_name'];
$date_time= date("Y-m-d H:i:s");

extract ($_REQUEST);

//emp_basic
	$sql = "SELECT * FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}
// Shift Details
	$shift_lists = array();
	$sql_shift = "SELECT * FROM lib_policy_shift";
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	while( $row_shift = mysql_fetch_array( $result_shift ) ) 
	{
		$shift_lists[$row_shift[id]]['shift_name']=$row_shift[shift_name];
		$shift_lists[$row_shift[id]]['shift_start']=$row_shift[shift_start];
		$shift_lists[$row_shift[id]]['shift_end']=$row_shift[shift_end];
		$shift_lists[$row_shift[id]]['shift_type']=$row_shift[shift_type];
		$shift_lists[$row_shift[id]]['grace_minutes']=$row_shift[grace_minutes];		
		$shift_lists[$row_shift[id]]['in_grace_minutes']=add_time($row_shift[shift_start], $row_shift[grace_minutes]); // Add In Grace Time with In Time
		$shift_lists[$row_shift[id]]['grace_minutes']=$row_shift[grace_minutes];
		$shift_lists[$row_shift[id]]['cross_date']=$row_shift[cross_date];
		
		$shift_lists[$row_shift[id]]['exit_buffer_minutes']=$row_shift[exit_buffer_minutes];		
		$shift_lists[$row_shift[id]]['tiffin_start']=$row_shift[first_break_start];
		$shift_lists[$row_shift[id]]['tiffin_ends']=$row_shift[first_break_end];
		if($row_shift[first_break_start]!="00:00:00")
			$shift_lists[$row_shift[id]]['tiffin_break_min']=datediff(n,$row_shift[first_break_start],$row_shift[first_break_end]); 
		else
			$shift_lists[$row_shift[id]]['tiffin_break_min']=0;
		$shift_lists[$row_shift[id]]['dinner_start']=$row_shift[second_break_start];
		$shift_lists[$row_shift[id]]['dinner_ends']=$row_shift[second_break_end];
		if($row_shift[second_break_start]!="00:00:00")
			$shift_lists[$row_shift[id]]['dinner_break_min']=datediff(n,$row_shift[second_break_start],$row_shift[second_break_end]);
		else
			$shift_lists[$row_shift[id]]['dinner_break_min']=0;
			
		$shift_lists[$row_shift[id]]['lunch_start']=$row_shift[lunch_time_start];
		$shift_lists[$row_shift[id]]['lunch_end']=$row_shift[lunch_time_end];
		$shift_lists[$row_shift[id]]['lunch_break_min']=datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]); 
		$shift_lists[$row_shift[id]]['total_working_min']=datediff(n,$row_shift[shift_start],$row_shift[shift_end])-datediff(n,$row_shift[lunch_time_start],$row_shift[lunch_time_end]);
		$shift_lists[$row_shift[id]]['early_out_start']=$row_shift[early_out_start];
		$shift_lists[$row_shift[id]]['entry_restriction_start']=$row_shift[entry_restriction_start];
	
	}
// Variable Settings
	$sql = "SELECT company_name,adjust_in_time,adjust_out_time,first_ot_limit,one_hour_ot_unit,allow_ot_fraction,ot_start_minute,dinner_ot_treatment,last_punch_time_with_ot,dinner_ot_treatment_time,reqn_based_ot,device_fixed,outtime_reqn,is_xcess_controlled,ignor_questionable_ot  FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		 	 	 	
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['dinner_ot_treatment'] = mysql_real_escape_string( $row['dinner_ot_treatment'] );
		$var_hrm_chart[$row['company_name']]['dinner_ot_treatment_time'] = mysql_real_escape_string( $row['dinner_ot_treatment_time'] );
		$var_hrm_chart[$row['company_name']]['last_punch_time_with_ot'] = mysql_real_escape_string( $row['last_punch_time_with_ot'] );
		$var_hrm_chart[$row['company_name']]['reqn_based_ot'] = mysql_real_escape_string( $row['reqn_based_ot'] );
		$var_hrm_chart[$row['company_name']]['device_fixed'] = mysql_real_escape_string( $row['device_fixed'] );
		$var_hrm_chart[$row['company_name']]['outtime_reqn'] = mysql_real_escape_string( $row['outtime_reqn'] );
		$var_hrm_chart[$row['company_name']]['is_xcess_controlled'] = mysql_real_escape_string( $row['is_xcess_controlled'] );
		$var_hrm_chart[$row['company_name']]['ignor_questionable_ot'] = mysql_real_escape_string( $row['ignor_questionable_ot'] );
	}
	
//Employee OT Requisition
	$sql = "SELECT ot_date,sum(budgeted_ot) as budgeted_ot,emp_code FROM ot_requisition_dtl WHERE ot_date between '".convert_to_mysql_date( $txt_from_date )."' and '".convert_to_mysql_date( $txt_to_date )."' group by emp_code,ot_date";
	
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$ot_requisition_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$ot_requisition_list[$row['emp_code']][$row['ot_date']] = $row['budgeted_ot'];
	}

if($type=="manual_attendance_data_set")
{
	
	 
	
 	$data=explode("__",$data);
	
	
	
	$tbl_row_id=explode(",",$data[0]);
	$tbl_attnd_id=explode(",",$data[1]);
	
	if( $data[9]==1 )
	{
		$new_intime=explode("*",$data[3]);
		$new_outtime=explode("*",$data[4]);
	}
	 
	$sql1="select * from hrm_attendance where id in ( ".$data[1]." )";	
	$result1=mysql_query($sql1);
	while($row1=mysql_fetch_array($result1))
	{ 
		$attnd_array[$row1['id']] = array();
		foreach( $row1 AS $key => $value ) {
			$attnd_array[$row1['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	 
	for($t=0;$t<count($tbl_row_id);$t++)
	{ 
	 	//echo $tbl_attnd_id[$t]; die;
		if( $data[9]==1)
			$status_tmp=check_daily_attendance_status($attnd_array[$tbl_attnd_id[$t]],$data[2],$new_intime[$t],$new_outtime[$t],$data[6],$data[26], $emp_basic, $shift_lists,$ot_policy_arr,$roster_ot_policy_array ); 
		else
			$status_tmp=check_daily_attendance_status($attnd_array[$tbl_attnd_id[$t]],$data[2],$data[3],$data[4],$data[6],$data[26], $emp_basic, $shift_lists,$ot_policy_arr,$roster_ot_policy_array ); 
		
		//( $id, $new_sts, $new_in_time, $new_out_time, $is_next_day, $new_shift_policy, $emp_basic, $shift_lists, $ot_policy_arr, $roster_ot_policy_array)
 //print_r( $status_tmp ); die;
		$status=explode(',',$status_tmp);
		  
		if( $data[6]!=1 ) $status[7]=$shift_lists[$status[6]]['cross_date']; else  $status[7]=$data[6];
		
		 
		$shift_time=explode("_", $status[11]);
		
		$in_time=explode(":",$status[1]);
		$out_time=explode(":",$status[2]);
		
		echo "$('#"."G"."$tbl_row_id[$t]"."6').html(\"<input type='text' name='in_time_hours_".$tbl_row_id[$t]."' id='in_time_hours_".$tbl_row_id[$t]."' value='".$in_time[0]."'  readonly='readonly' onKeyPress='return numbersonly(this, event);' onKeyUp='fnc_move_cursor(this.value,'in_time_hours_".$tbl_row_id[$t]."','in_time_minuties_".$tbl_row_id[$t]."',2,23);' style='width:15px;'/>:<input type='text' name='in_time_minuties_".$tbl_row_id[$t]."' id='in_time_minuties_".$tbl_row_id[$t]."' value='".$in_time[1]."' readonly='readonly' onKeyPress='return numbersonly(this, event);' onKeyUp='fnc_move_cursor(this.value,'in_time_minuties_".$tbl_row_id[$t]."','in_time_seconds_".$tbl_row_id[$t]."',2,59)' style='width:15px;'/>:<input type='text' name='in_time_seconds_".$tbl_row_id[$t]."' id='in_time_seconds_".$tbl_row_id[$t]."' value='".$in_time[2]."' readonly='readonly' onKeyPress='return numbersonly(this, event);' onKeyUp='fnc_move_cursor(this.value,'in_time_seconds_".$tbl_row_id[$t]."','in_time_hours_".$tbl_row_id[$t]."',2,59)' style='width:15px;'/>\");\n";  // In Time
		
		echo "$('#"."H"."$tbl_row_id[$t]"."7').html(\"<input type='text' name='out_time_hours_".$tbl_row_id[$t]."' id='out_time_hours_".$tbl_row_id[$t]."' value='".$out_time[0]."' readonly='readonly' onKeyPress='return numbersonly(this, event);' onKeyUp='fnc_move_cursor(this.value,'out_time_hours_".$tbl_row_id[$t]."','out_time_minuties_".$tbl_row_id[$t]."',2,23);' style='width:15px;'/>:<input type='text' name='out_time_minuties_".$tbl_row_id[$t]."' id='out_time_minuties_".$tbl_row_id[$t]."' value='".$out_time[1]."' readonly='readonly' onKeyPress='return numbersonly(this, event);' onKeyUp='fnc_move_cursor(this.value,'out_time_minuties_".$tbl_row_id[$t]."','out_time_seconds_".$tbl_row_id[$t]."',2,59)' style='width:15px;'/>:<input type='text' name='out_time_seconds_".$tbl_row_id[$t]."' id='out_time_seconds_".$tbl_row_id[$t]."' value='".$out_time[2]."' readonly='readonly' onKeyPress='return numbersonly(this, event);' onKeyUp='fnc_move_cursor(this.value,'out_time_seconds_".$tbl_row_id[$t]."','out_time_hours_".$tbl_row_id[$t]."',2,59)' style='width:15px;'/>\");\n";  // Out Time	
		
		//$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start']."_".$shift_lists[$shift_policy]['shift_end'];	
		
		
		echo "$('#"."I"."$tbl_row_id[$t]"."8').html('".$status[0]."');\n";		//Status
		echo "$('#"."J"."$tbl_row_id[$t]"."9').html('".$status[4]."');\n";  // Late
		echo "$('#"."L"."$tbl_row_id[$t]"."11').html('".$status[3]."');\n";  // OT
		echo "$('#"."M"."$tbl_row_id[$t]"."12').html('".$status[5]."');\n";   // Early
		echo "$('#"."N"."$tbl_row_id[$t]"."13').html('".$shift_lists[$status[6]]['shift_name']."');\n";   // shift name
		echo "$('#"."O"."$tbl_row_id[$t]"."14').html('".$shift_time[0]."');\n";//shift in
		echo "$('#"."P"."$tbl_row_id[$t]"."15').html('".$shift_time[1]."');\n"; //shift out  
		//".$shift_lists[$shift_policy]['shift_start']."_".$shift_lists[$shift_policy]['shift_end']
		if($status[7]==0) $status[7]='No'; else $status[7]='Yes'; 
		echo "$('#"."K"."$tbl_row_id[$t]"."10').html('".$status[7]."');\n";  // is next day
		 //echo "$('#"."Q"."$tbl_row_id[$t]"."16').html('".$status[13]."');\n";   // remarks
		
  		echo "$('#"."X"."$tbl_row_id[$t]"."17').html('".$status[8]."');\n";   // get tiffin
		echo "$('#"."Y"."$tbl_row_id[$t]"."18').html('".$status[9]."');\n";   // get dinner
		$status[10]=0;
		if (( trim($status[1])=="00:00:00" || trim($status[1])=="") && (trim($status[2])!="00:00:00" && trim($status[2])!="" )) $status[10]=1;
		else if (( trim($status[1])!="00:00:00" && trim($status[1])!="") && (trim($status[2])=="00:00:00" || trim($status[2])=="" )) $status[10]=1;
		//else if (trim($status[1])=="00:00:00" && trim($status[2])=="00:00:00") $status[10]=0;
		//else if (trim($status[1])=="00:00:00" && trim($status[2])=="00:00:00") $status[10]=0;
		if (trim($status[0])=="A") $status[10]=0;
		//if ($status[2]=="00:00:00" || $status[2]=="")   $status[10]=1;
		echo "$('#"."Z"."$tbl_row_id[$t]"."21').html('".$status[10]."');\n";   // is questionable
		echo "$('#"."AZ"."$tbl_row_id[$t]"."22').html('".$status[13]."');\n";   // is questionable
		
		
	}
	exit();
}

function check_daily_attendance_status ( $row1, $new_sts, $new_in_time, $new_out_time, $is_next_day, $new_shift_policy, $emp_basic, $shift_lists, $ot_policy_arr, $roster_ot_policy_array)
{
//-------------------------------------------------------------------------------------------------//
	 
			$shift_policy=$row1[policy_shift_id];//$roster_ot_policy_array[$row1[emp_code]][$row1['attnd_date']]['shift']; //$shift_ot_policy_arr[0];
			$ot_policy =$row1[policy_overtime_id ];//$roster_ot_policy_array[$row1[emp_code]][$row1['attnd_date']]['ot']; //$shift_ot_policy_arr[1];
			$shift_lists[$shift_policy]['shift_start'] = $row1['r_sign_in_time'];  	 
        	$shift_lists[$shift_policy]['shift_end'] = $row1['r_sign_out_time'];
			$cross_date_policy=$shift_lists[$shift_policy]['cross_date'];
			
			$shift_lists[$shift_policy]['in_grace_minutes']=add_time($shift_lists[$shift_policy]['shift_start'], $shift_lists[$shift_policy]['grace_minutes']); 
			
	 	$emp_code_tmp=$row1['emp_code'];
		$shift_policy_name = $shift_lists[$shift_policy]['shift_name'];
		$ot_entitled = $emp_basic[$row1['emp_code']]['ot_entitled'];
		//return $row1['is_regular_day']."sdasd";
		$attnd_date =$row1['attnd_date'];
		$is_regular_day = $row1['is_regular_day'];
		$company_id=$row1['company_id'];
		
		if ($is_next_day==0 && $new_out_time==""){ $is_next_day=0; }  //$row1['is_next_day'];
		 
		if ($new_in_time=="" || $new_in_time=="00:00:00") $new_in_time=$row1[sign_in_time]; else $new_in_time=$new_in_time;
		if ($new_out_time=="" || $new_out_time=="00:00:00") $new_out_time=$row1[sign_out_time]; else $new_out_time=$new_out_time;
			
		
		 // Return formaT---------------- $status=$status+intime+outtime+ot+late+earlyout
		if ($new_sts=="A")   // Set Absent 
		{
			if ($row1[status]=="P" || $row1[status]=="D" || $row1[status]=="MR" || $row1[status]=="CL" || $row1[status]=="SL" || $row1[status]=="LWP" || $row1[status]=="SpL" || $row1[status]=="EL" || $row1[status]=="ML") $status="A"; else $status=$row1[status];
			 
			 $status=$status.",00:00:00,00:00:00,0,0,0,".$shift_policy.",".$is_next_day.",0,0,".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'];
			 return $status; die;
		}		
		
		else if ($new_sts=="PL")  // set Present with Late
		{	
		
			$ot_min_st = get_overtime($ot_entitled,$ot_policy,$new_in_time,$new_out_time,$is_regular_day,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id, $emp_code_tmp, $attnd_date);	
		 
			$st_explode=explode(',',$ot_min_st);
			
			if( $st_explode[0] < 0 )$ot_mins=0; else $ot_mins=$st_explode[0];  
			$get_tiffin=$st_explode[1];
			$get_dinner=$st_explode[2];
			$questionable=$st_explode[3];
			$actual_ot_min=$st_explode[4];
			
			$late_mins = get_delay_min( $is_regular_day, $new_in_time, $shift_policy, $shift_lists );
			$early_out = get_early_min( $is_next_day,$is_regular_day, $new_out_time, $shift_policy, $shift_lists ); 
			if(	$early_out<0) $early_out=0;	
		 
			if ($row1[status]=="P" || $row1[status]=="D" || $row1[status]=="MR" || $row1[status]=="CL" || $row1[status]=="SL" || $row1[status]=="LWP" || $row1[status]=="SpL" || $row1[status]=="EL" || $row1[status]=="ML" || $row1[status]=="A") $status="P"; else $status=$row1[status];
			if ($late_mins>0)  
			{
				$status="D";
				$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'].",".$actual_ot_min;			
			}
			else
			{
				$late_mins=0;
				$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'].",".$actual_ot_min;
			}			
			
			return $status; 
		}
		
		else if ($new_sts=="P")  // set Present with no Late
		{
			$ot_min_st = get_overtime($ot_entitled,$ot_policy,$new_in_time,$new_out_time,$is_regular_day,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id,$emp_code_tmp,$attnd_date);	
		 //echo $ot_min_st;die;
			$st_explode=explode(',',$ot_min_st);
			if( $st_explode[0] < 0 )$ot_mins=0; else $ot_mins=$st_explode[0];
			$get_tiffin=$st_explode[1];
			$get_dinner=$st_explode[2];
			$questionable=$st_explode[3];			
			$actual_ot_min=$st_explode[4];
			
			$early_out = get_early_min( $is_next_day,$is_regular_day, $new_out_time, $shift_policy, $shift_lists );
			if(	$early_out<0) $early_out=0;			
			$late_mins= 0;
			
			if ($row1[status]=="P" || $row1[status]=="D" || $row1[status]=="MR" || $row1[status]=="CL" || $row1[status]=="SL" || $row1[status]=="LWP" || $row1[status]=="SpL" || $row1[status]=="EL" || $row1[status]=="ML" || $row1[status]=="A") $status="P"; else $status=$row1[status];
			 
			$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'].",".$actual_ot_min;			
 			return $status; die;
		}
		else if ($new_sts=="W")  // set Weekly Holiday
		{
			$is_regular_day=0;
			$ot_min_st = get_overtime($ot_entitled,$ot_policy,$new_in_time,$new_out_time,$is_regular_day,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id,$emp_code_tmp,$attnd_date);	
			$st_explode=explode(',',$ot_min_st);
			if( $st_explode[0] < 0 )$ot_mins=0; else $ot_mins=$st_explode[0];
			$get_tiffin=$st_explode[1];
			$get_dinner=$st_explode[2];
			$questionable=$st_explode[3];
			$actual_ot_min=$st_explode[4];
			
			$late_mins = 0;
			$early_out = 0;
						
			if ($row1[status]=="P" || $row1[status]=="D" || $row1[status]=="MR" || $row1[status]=="CL" || $row1[status]=="SL" || $row1[status]=="LWP" || $row1[status]=="SpL" || $row1[status]=="EL" || $row1[status]=="ML" || $row1[status]=="A") $status="W"; else $status=$row1[status];
			 
			$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'].",".$actual_ot_min;	
			return $status; die;
		}
		else if ($new_sts=="CH")  // set Holiday
		{
			$is_regular_day=0;
			$ot_min_st = get_overtime($ot_entitled,$ot_policy,$new_in_time,$new_out_time,$is_regular_day,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id,$emp_code_tmp,$attnd_date);	
			$st_explode=explode(',',$ot_min_st);
			if( $st_explode[0] < 0 )$ot_mins=0; else $ot_mins=$st_explode[0];
			$get_tiffin=$st_explode[1];
			$get_dinner=$st_explode[2];
			$questionable=$st_explode[3];
			$actual_ot_min=$st_explode[4];
			
			$late_mins = 0;
			$early_out = 0;
			 
 			if ($row1[status]=="P" || $row1[status]=="D" || $row1[status]=="MR" || $row1[status]=="CL" || $row1[status]=="SL" || $row1[status]=="LWP" || $row1[status]=="SpL" || $row1[status]=="EL" || $row1[status]=="ML" || $row1[status]=="A") $status="CH"; else $status=$row1[status];
			 
			$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'].",".$actual_ot_min;	
			return $status;
		}
		else if ($new_sts=="GD")  // set General Duty
		{ 
			$ot_min_st = get_overtime($ot_entitled,$ot_policy,$new_in_time,$new_out_time,1,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id,$emp_code_tmp,$attnd_date);	
			$st_explode=explode(',',$ot_min_st);
			//echo $ot_min_st; die;
			$ot_min=$st_explode[0];
			$get_tiffin=$st_explode[1];
			$get_dinner=$st_explode[2];
			$questionable=$st_explode[3];
			$actual_ot_min=$st_explode[4];
			
			if(	$ot_min>0) $ot_mins=abs($ot_min);else $ot_mins=0;
			if(	$ot_min<0) $early_out=abs($ot_min);else $early_out=0;
			
			$late_mins= get_delay_min( $is_regular_day, $new_in_time, $shift_policy, $shift_lists ,$new_sts);
			
			if ($new_in_time=="00:00:00" && $new_out_time=="00:00:00") { $status="A"; $early_out=0; }
			if ($new_in_time!="00:00:00" || $new_out_time!="00:00:00") $status="P"; 
			if ($late_mins>0) $status="D";
			
			//if ($row1[status]=="P" || $row1[status]=="D" || $row1[status]=="MR" || $row1[status]=="CL" || $row1[status]=="SL" || $row1[status]=="LWP" || $row1[status]=="SpL" || $row1[status]=="EL" || $row1[status]=="ML" || $row1[status]=="A") $status="P"; else $status=$row1[status];
			if ($late_mins>0)  
				{
					$status="D";
					$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'].",".$actual_ot_min;			
				}
			else
				{
					$late_mins=0;
					$status=$status.",".$new_in_time.",".$new_out_time.",".$ot_mins.",".$late_mins.",".$early_out.",".$shift_policy.",".$is_next_day.",".$get_tiffin.",".$get_dinner.",".$questionable.",".$shift_lists[$shift_policy]['shift_start'].",".$shift_lists[$shift_policy]['shift_end'].",".$actual_ot_min;
				} 
			
			return $status; 
		}
	 
	  
}


//attendance table update
if($type=="update_attn_table")
	{	
		$employee_code 	=explode("__",$employee_code);
		$present_date	=explode("__",$present_date);
		$in_time		=explode("__",$in_time);
		$out_time		=explode("__",$out_time);
		$status			=explode("__",$status);
		$delay			=explode("__",$delay);
		$is_next		=explode("__",$is_next);
		$ot_min			=explode("__",$ot_min);
		$early 			=explode("__",$early);
		//$remarks		=explode("__",$remarks);
		$take_tiffin	=explode("__",$take_tiffin);	
		$take_dinner	=explode("__",$take_dinner);	
		$is_questionable=explode("__",$is_questionable);
		$req_ot_org=explode("__",$req_ot_org);		
		 
		$sa=0;$un=0;
		for($i=0;$i<$counts;$i++)
		{			
			$chk_sql="delete from hrm_movement_register where emp_code='$employee_code[$i]' and move_current_date='".convert_to_mysql_date($present_date[$i])."'";
			mysql_db_query( $DB, $chk_sql ) or die( $chk_sql . "<br />" . mysql_error() );
			
			
			 
			if($is_next[$i]=="Yes") $next_day=1; else $next_day=0; 	
			
			$attdatas=return_field_value("concat(is_regular_day,'_',r_sign_in_time,'_',r_sign_out_time,'_',policy_shift_id)","hrm_attendance","emp_code='$employee_code[$i]' and attnd_date='".convert_to_mysql_date($present_date[$i])."'"); 
			$attdatas=explode("_",$attdatas);
			$is_regular=$attdatas[0];
			
			$shift_poli=$attdatas[3];//$emp_basic[$employee_code[$i]]['shift_policy'];
			
			if($in_time[$i]!="00:00:00" )
			{
				if( $str!="" ) $str .=","; 
				
				$str .="( '".$emp_basic[$employee_code[$i]]['punch_card_no']."','".convert_to_mysql_date($present_date[$i])."','".$in_time[$i]."','".$emp_basic[$employee_code[$i]]['company_id']."','".$emp_basic[$employee_code[$i]]['location_id']."','".$shift_poli."','".$shift_lists[$shift_poli]['shift_type']."','".$next_day."','".$attdatas[1]."','".$attdatas[2]."','".$shift_lists[$shift_poli]['grace_minutes']."','".$shift_lists[$shift_poli]['exit_buffer_minutes']."','".$shift_lists[$shift_poli]['entry_restriction_start']."',1,1 )";
			}
			
			if($next_day==1)
				$ddate=add_date(convert_to_mysql_date($present_date[$i]),1);
			else 
				$ddate=convert_to_mysql_date($present_date[$i]);
			//echo $ddate; die;
			if($out_time[$i]!="00:00:00")
			{
				if( $str!="" ) $str .=","; 
				$str .="( '".$emp_basic[$employee_code[$i]]['punch_card_no']."','".$ddate."','".$out_time[$i]."','".$emp_basic[$employee_code[$i]]['company_id']."','".$emp_basic[$employee_code[$i]]['location_id']."','".$shift_poli."','".$shift_lists[$shift_poli]['shift_type']."','".$next_day."','".$attdatas[1]."','".$attdatas[2]."','".$shift_lists[$shift_poli]['grace_minutes']."','".$shift_lists[$shift_poli]['exit_buffer_minutes']."','".$shift_lists[$shift_poli]['entry_restriction_start']."',2,1 )";
			}
			if( trim($status[$i])=='A' )
			{
				if( $att_date=="" ) $att_date="'".convert_to_mysql_date($present_date[$i])."'"; else $att_date .=",'".convert_to_mysql_date($present_date[$i])."'";
				if( $rf_code=="" ) $rf_code="'".$emp_basic[$employee_code[$i]]['punch_card_no']."'"; else $rf_code .=",'".$emp_basic[$employee_code[$i]]['punch_card_no']."'";
			}
			
			if($take_tiffin[$i]=="null") $take_tiffin[$i]=0; 
			if($take_dinner[$i]=="null") $take_dinner[$i]=0;
			if( $status[$i]=="CH" || $status[$i]=="W" ) $is_regular=0;
			$attn_update_sql = "update hrm_attendance 
								set 
								sign_in_time='$in_time[$i]',
								sign_out_time='$out_time[$i]',
								is_next_day='$next_day', 
								status='$status[$i]', 
								late_time_min='$delay[$i]', 
								early_out_min='$early[$i]',								
 								total_over_time_min='$ot_min[$i]', 
								req_ot_org='$req_ot_org[$i]', 
								is_regular_day='$is_regular',
								is_get_tiffin_allowance='$take_tiffin[$i]',	
								is_get_night_allowance='$take_dinner[$i]',
								is_questionable='$is_questionable[$i]',
								is_manually_updated='1'								
								where emp_code='$employee_code[$i]' and attnd_date='".convert_to_mysql_date($present_date[$i])."' ";//remarks='$remarks[$i]',
			
			$result = mysql_query( $attn_update_sql ) or die( $attn_update_sql . "<br />" . mysql_error() );
			$rows=mysql_fetch_array($result);
			
			
			
			$affected = mysql_affected_rows();
			if( $affected > 0 ){
					$sa .=$employee_code[$i].',';
				}
			else
				{
					$un .=$employee_code[$i].',';
				}
			
		}
			
			
			for($j=0;$j<$counts;$j++){
				$emp_manu_his="insert into manual_attendance_information (emp_code,date,inserted_by,inserted_date,updated_by,updated_date,cost_centers) values  
							('$employee_code[$j]','".convert_to_mysql_date($present_date[$j])."','".$user_name."','".$date_time."','manual_attendance_entry','','')";
				$result_history = mysql_query( $emp_manu_his ) or die( $emp_manu_his . "<br />" . mysql_error() );
			}
			
			
			$strfld="insert into hrm_raw_data_attnd_backup (cid,dtime,ctime,company_id,location_id,shift_policy_id,shift_type,is_next_day,shift_start,shift_end,graced_minutes,exit_buffer_minutes,entry_restriction_start,punch_type,is_manual) values ".$str;
			//echo $strfld;die; 
			if( $str!="" ) mysql_query( $strfld );
			
			$sql_del="delete from hrm_raw_data_attnd_backup where cid in (".$rf_code.") and dtime in ( ".$att_date." ) ";
			//if( $rf_code!="" )  mysql_query( $sql_del );
						
			//echo $attn_update_sql;die;	
			echo "1###Total Saved : $sa and Total Unsaved : $un";
			exit();
	}




 
function get_overtime($ot_entitle,$ot_policy,$gen_in_time,$gen_out_time,$is_regular_day,$is_next_day,$shift_policy,$shift_lists,$ot_policy_arr,$new_sts,$company_id,$emp_code,$attnd_date)
{
	
	global $var_hrm_chart;
	$get_dinner=0;
	$max_day_close_time="23:59:59";
	$p_time="00:00:00";
	$ignor_lunch_deduction=0;
	$ignor_dinner_deduction=0;
	if ( $shift_lists[$shift_policy]['dinner_start']<=$max_day_close_time and $shift_lists[$shift_policy]['dinner_start']>$shift_lists[$shift_policy]['shift_start'])
		$is_dinner_next_day=0;
	else
		$is_dinner_next_day=1; 
		 $is_questionable=0;
	if( $gen_in_time=="" || $gen_in_time=="00:00:00") $is_questionable=1;
	if( $gen_out_time=="" || $gen_out_time=="00:00:00") $is_questionable=1;
	
	$r_dinner_start=$shift_lists[$shift_policy]['dinner_start'];
	$r_dinner_ends=$shift_lists[$shift_policy]['dinner_ends'];
	
	if( $shift_lists[$shift_policy]['lunch_start']=="00:00:00" ) $ignor_lunch_deduction=1;
	if( $shift_lists[$shift_policy]['dinner_start']=="00:00:00" ) $ignor_dinner_deduction=1;
	$m_overtime_calculation_rule=  $ot_policy_arr[$ot_policy]['overtime_calculation_rule'];
	if ($m_overtime_calculation_rule=="") $m_overtime_calculation_rule=1;
	$get_dinners=0;
	//$get_tiffin
	
	$add_next_day_ot=0;
	$is_prev_day_log=0;
	if($is_next_day==1 && $shift_lists[$shift_policy]['cross_date']==0) $is_prev_day_log=1;
	
	if($is_next_day==0 && $shift_lists[$shift_policy]['cross_date']==1 && $is_regular_day==1)
	{
		if( $gen_out_time > $shift_lists[$shift_policy]['shift_start'] && $gen_out_time < "23:59:59" && $shift_lists[$shift_policy]['shift_end']>"00:00:00" )
			return $gen_ot_time.','.$get_tiffin.','.$get_dinner.','.$is_questionable.','.$actual_ot_min;
		else if( $gen_out_time < $shift_lists[$shift_policy]['shift_end'] && $gen_out_time > "00:00:00" )
			return $gen_ot_time.','.$get_tiffin.','.$get_dinner.','.$is_questionable.','.$actual_ot_min;
	}

	if ($ot_entitle==1)
	{
		if ($is_questionable==0) // Regular Present 
		{
			if ( $is_regular_day==1) // Regular working Day
			{
				if ( $m_overtime_calculation_rule==0)  // OT Rules and Regulations
				{
					if ($is_next_day==0)
					{
						if($is_prev_day_log==0) // Current Date
						{
							
							if($ignor_lunch_deduction==0)
								$gen_ot_time=(datediff(n, $gen_in_time,$gen_out_time)-($shift_lists[$shift_policy]['total_working_min']-$shift_lists[$shift_policy]['lunch_break_min'])); 
							else
								$gen_ot_time=(datediff(n, $gen_in_time,$gen_out_time)-($shift_lists[$shift_policy]['total_working_min'])); 
						}
						else   // Date Crossover OT
						{
							$timeDiffout=datediff(n,$gen_in_time,$max_day_close_time);
							$p_time="00:00:00";
							$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
							if($ignor_lunch_deduction==0)
								$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']+$shift_lists[$shift_policy]['lunch_break_min']);
							else
								$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']);
						}
					}
					else
					{
						$timeDiffout=datediff(n,$gen_in_time,$max_day_close_time);
						$p_time="00:00:00";
						$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
						if($ignor_lunch_deduction==0)
							$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']+$shift_lists[$shift_policy]['lunch_break_min']);
						else
							$gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['total_working_min']);
						 
					}
					 
				} 
				else 
				{
					
					if ($is_next_day==0)
					{
						
						if( $is_prev_day_log==0 ) // Current Date
							$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
						 else
							$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
							
						if($gen_ot_time<0) $gen_ot_time=0;	
					}
					else
					{
						
						if( $is_prev_day_log==0 ) // Current Date
							$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
						 else
						 {
							 $timeDiffout=datediff(n,$shift_lists[$shift_policy]['shift_end'],$max_day_close_time);
							 $p_time="00:00:00";
							 $gen_ot_time=$timeDiffout+datediff(n,$p_time,$gen_out_time);
							$add_next_day_ot=1;
							$get_dinners=1;
							//$gen_ot_time=datediff(n,$shift_lists[$shift_policy]['shift_end'],$gen_out_time);//$gen_ot_time;
						 }
					} 
				}																							
			}
			else // Not Regular Day -- Holiday
			{
				if( $shift_lists[$shift_policy]['shift_start']>$gen_in_time ) 
					$off_day_ot_in=$shift_lists[$shift_policy]['shift_start'];
				else
					$off_day_ot_in=$gen_in_time;
				
				if ( $m_overtime_calculation_rule==0)  // OT Rules and Regulations
				{
					
					if($is_prev_day_log==0) // Current Date
					{
						if($ignor_lunch_deduction==0)
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time)-$shift_lists[$shift_policy]['lunch_break_min'];
						else
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time);
							
					}
					else
					{
						$timeDiffout=datediff(n,$off_day_ot_in,$max_day_close_time);
						$p_time="00:00:00";
						$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
						$add_next_day_ot=1;
						if($ignor_lunch_deduction==0) $gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['lunch_break_min']);
						else
							$gen_ot_time=$timeDiffout;
							$get_dinners=1;
					} 
				}
				else //OT Rules 2nd
				{  
					
					
					if( $is_prev_day_log==0 && $shift_lists[$shift_policy]['cross_date']==0) // Current Date
					{
						
						if ($gen_out_time<=$shift_lists[$shift_policy]['lunch_start'])
						{
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time);
						}
						else if ($gen_out_time>=$shift_lists[$shift_policy]['lunch_start'] && $gen_out_time<=$shift_lists[$shift_policy]['lunch_end'])
						{
							$gen_ot_time=datediff(n, $off_day_ot_in,$shift_lists[$shift_policy]['lunch_start']);
						}
						else if ($gen_in_time>=$shift_lists[$shift_policy]['lunch_start'] && $gen_in_time<=$shift_lists[$shift_policy]['lunch_end'])
						{
							$gen_ot_time=datediff(n, $shift_lists[$shift_policy]['lunch_end'],$gen_out_time);
						}
						else if ($gen_in_time>=$shift_lists[$shift_policy]['lunch_end'])
						{
							 
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time);
						}
						else
							$gen_ot_time=datediff(n, $off_day_ot_in,$gen_out_time)-$shift_lists[$shift_policy]['lunch_break_min'];
						
						
					}
					elseif( $is_prev_day_log==0 && $shift_lists[$shift_policy]['cross_date']==1 ) // Current Date
					{
						// echo $gen_out_time."sumon".$max_day_close_time."=";die;
						$p_time="00:00:00";
						if($gen_out_time<=$max_day_close_time && $gen_out_time>$shift_lists[$shift_policy]['shift_start'])
						{
							//$timeDiffout=datediff(n,$off_day_ot_in,$max_day_close_time);
							//$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
							$timeDiffout=datediff(n,$off_day_ot_in,$gen_out_time);
							$add_next_day_ot=1;
							$get_dinners=1;
							if($ignor_lunch_deduction==0) $gen_ot_time=$timeDiffout-$shift_lists[$shift_policy]['lunch_break_min'];
							else $gen_ot_time=$timeDiffout;
						}
						else
						{
							$timeDiffout=datediff(n,$off_day_ot_in,$max_day_close_time);
							$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
							//$timeDiffout=datediff(n,$off_day_ot_in,$gen_out_time);
							$add_next_day_ot=1;
							$get_dinners=1;
							if($ignor_lunch_deduction==0) $gen_ot_time=$timeDiffout-$shift_lists[$shift_policy]['lunch_break_min'];
							else $gen_ot_time=$timeDiffout;
						}
					} 
					else
					{
						$timeDiffout=datediff(n,$off_day_ot_in,$max_day_close_time);
						$p_time="00:00:00";
						$timeDiffout=$timeDiffout+datediff(n,$p_time,$gen_out_time);
						$add_next_day_ot=1;
						$get_dinners=1;
						if( $ignor_lunch_deduction==0 ) $gen_ot_time=$timeDiffout-($shift_lists[$shift_policy]['lunch_break_min']);
						else $gen_ot_time=$timeDiffout;
					}
				}
				//$gen_ot_time=$gen_ot_time;	
			}
		}
		// Questionable else here
	}
	else  // Not OT Entitled
	{
		 $gen_ot_time=0;
	}
	
	if ( $add_next_day_ot==1 ) $gen_ot_time=$gen_ot_time+1;
	$add_next_day_ot=0;
	
	
	
	if ( $shift_lists[$shift_policy]['dinner_ends']<=$max_day_close_time and $shift_lists[$shift_policy]['dinner_ends']>$shift_lists[$shift_policy]['shift_start']) $is_dinner_next_day=0;
	else $is_dinner_next_day=1; 
	
	if( $get_dinners==0 && $is_next_day==0 && $is_prev_day_log==0 && $is_dinner_next_day==0 )				 
	{
		if( $shift_lists[$shift_policy]['dinner_ends']<=$gen_out_time ) $get_dinners=1;
		else  if( $shift_lists[$shift_policy]['dinner_start']<=$gen_out_time ) $get_dinners=1;
	}
	
	if (  $gen_ot_time!=0 && $ignor_dinner_deduction==0 )
	{
		
		if ($var_hrm_chart[$company_id]['dinner_ot_treatment']!=0 && $get_dinners==1)
		{
			if ($var_hrm_chart[$company_id]['dinner_ot_treatment']==1) // Add OT
			{/* 
				if( $is_dinner_next_day==0 && $is_prev_day_log==1 )
				{
					$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
				}
				else if( $is_dinner_next_day==0 && $is_prev_day_log==0 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==1 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==0 )
				{
					$gen_ot_time=$gen_ot_time +$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
				}
			 */}
			 else //minus ot
			 {
				if( $is_dinner_next_day==0 && $is_prev_day_log==1 )
				{
					$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					$get_dinner=1;
				}
				else if( $is_dinner_next_day==0 && $is_prev_day_log==0 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
						$get_dinner=1;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
						$get_dinner=1;
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==1 )
				{
					if ($gen_out_time>$r_dinner_start && $gen_out_time<$r_dinner_ends)
					{	
						$dinn=datediff(n,$r_dinner_start,$gen_out_time);
						$gen_ot_time=$gen_ot_time-$dinn;
						$get_dinner=1;
					}
					else if($gen_out_time>=$r_dinner_ends)
					{
						$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
						$get_dinner=1;
					}
				}
				else if( $is_dinner_next_day==1 && $is_prev_day_log==0 )
				{
					$gen_ot_time=$gen_ot_time-$var_hrm_chart[$company_id]['dinner_ot_treatment_time'];
					$get_dinner=1;
				}
			 }
		}
	}
	  global $ot_requisition_list;
//return "ss".$ot_requisition_list[$emp_code][$attnd_date];

	if( $is_prev_day_log==1 ) $b_ot_amnt=$ot_requisition_list[$emp_code][$attnd_date]; 
	else $b_ot_amnt=$ot_requisition_list[$emp_code][$attnd_date]; 
	$actual_ot_min=$gen_ot_time;
	if( $gen_ot_time > 0 )
	{
		if( $var_hrm_chart[$company_id]['reqn_based_ot']==1 )
		{
			if( $b_ot_amnt < $gen_ot_time) //$budgeted_ot >0 and  
			{
				//if( $var_hrm_chart[$company_id]['outtime_reqn']==1)
					//$gen_out_time=generate_new_out_time( $gen_out_time, $shift_lists[$shift_policy]['shift_end'], $b_ot_amnt );
				$gen_ot_time=$b_ot_amnt;
				
			}//$device_fixed=$var_hrm_chart[$r_emps_data['company_id']]['device_fixed'];
		}
	}
//return "ss".$gen_ot_time;
	if($var_hrm_chart[$company_id]['ignor_questionable_ot'] ==1)
	{		
		if( $is_questionable==1 ) $gen_ot_time=0;
	} 
	$get_tiffin=0;
	// return "sss".$gen_ot_time;
	if( $shift_lists[$shift_policy]['tiffin_ends']<$gen_out_time && $is_next_day==0) $get_tiffin=1;
	else if( $shift_lists[$shift_policy]['tiffin_ends']>$gen_out_time && $is_prev_day_log==1 ) $get_tiffin=1;
	if($get_tiffin==1 && $gen_ot_time>0)  $gen_ot_time=$gen_ot_time-$shift_lists[$shift_policy]['tiffin_break_min'];
	
	return $gen_ot_time.','.$get_tiffin.','.$get_dinner.','.$is_questionable.','.$actual_ot_min;
	//return "nulll".$get_tiffin;

}

function get_delay_min( $is_regular_day, $in_time, $shift_policy, $shift_lists ,$status)
{	
	if($is_regular_day==1)
	{
		//$new_graced_time=add_time( $in_time,$shift_lists[$shift_policy]['in_grace_minutes']);
		$late_min=datediff(n, $shift_lists[$shift_policy]['in_grace_minutes'],$in_time);
		if( $late_min>0 ) $late_min = $late_min+$shift_lists[$shift_policy]['grace_minutes'];
	}
	else
	{
		if ($status=="GD")
		{
			//$new_graced_time=add_time( $in_time,$shift_lists[$shift_policy]['grace_minutes']);
			$late_min=datediff(n, $shift_lists[$shift_policy]['in_grace_minutes'],$in_time);
			if( $late_min>0 ) $late_min = $late_min+$shift_lists[$shift_policy]['grace_minutes'];
		}
		else
			$late_min=0;
	}
	return $late_min;
}

function get_early_min( $is_next_day,$is_regular_day, $out_time, $shift_policy, $shift_lists ,$status)
{	
	if($is_regular_day==1 && $is_next_day==0)
	{
		if($shift_lists[$shift_policy]['shift_end']!='00:00:00')
			$early_min=datediff(n,$out_time,$shift_lists[$shift_policy]['shift_end']);   
		else
			$early_min=0;	
	}
	else if($is_regular_day==1 && $is_next_day==1)
	{
		$early_min=0;   
	}
	else
	{
		if ($status=="GD" && $is_next_day==0)
		{
			$early_min=datediff(n,$out_time,$shift_lists[$shift_policy]['shift_end']);   
		}
		else if($is_regular_day==1 && $is_next_day==1)
		{
			$early_min=0;   
		}
		else
			$early_min=0;
	}	
	return $early_min;
}


function add_time($event_time,$event_length)
{
	
	$timestamp = strtotime("$event_time");
	$etime = strtotime("+$event_length minutes", $timestamp);
	$etime=date('H:i:s', $etime);
	return $etime;
	 
}

function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}



 
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
function return_shift_id( $shift, $date) {
	
		$sql = "SELECT shift_policy,overtime_policy FROM  lib_overtime_with_shift where shift_policy='$shift'";
		$sql_exe = mysql_query( $sql );
		$sql_rslt = mysql_fetch_array( $sql_exe );
		$current_shift = $sql_rslt[0]."###".$sql_rslt[1];
	return $current_shift;
}
?>