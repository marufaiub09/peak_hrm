
<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');
//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
$search_string=$_GET["search_string"];
$search_string2=$_GET["search_string2"];
$type=$_GET["type"];

?>

<script>$(".rpt_table tr:even").css("background", "#E9F3FF");$(".rpt_table tr:odd").css("background", "#FFFFFF");</script>

<?

// Employee information for Resource Allocation
if($type=="emp_resource_allocation")
{
	
?>

<div>
    <div style="width:930px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="35" align="left"><strong>SL</strong></th>
                <th width="80" align="left"><strong>Emp. Code</strong></th>
                <th width="85" align="left"><strong>Punch Card</strong></th>
                <th width="130" align="left"><strong>Emp. Name</strong></th>
                <th width="130" align="left"><strong>Designation</strong></th>
                <th width="120" align="left"><strong>Company Name</strong></th>
                <th width="110" align="left"><strong>Division</strong></th>
                <th width="115" align="left"><strong>Department</strong></th>
                <th align="left"><strong>Section</strong></th><input type="hidden" name="id_field" id="id_field" />
            </thead>
		</table>
	</div>	
	<div style="width:930px; overflow-y:scroll; min-height:50px; max-height:230px;" id="resource_allocation_list_view" align="left">
    	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
<?php
$i=1;
if ($search_string2==0){
		
		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE emp_code like '%$search_string' and is_deleted=0 and status_active=1 order by emp_code";
}
elseif ($search_string2==1){

		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE concat(first_name,' ',middle_name,' ',last_name) like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code";
}
elseif ($search_string2==2){
		$sql= "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.custom_designation
		from hrm_employee a, lib_designation d
		where a.designation_id=d.id and d.custom_designation like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}
elseif ($search_string2==3){
		$sql= "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.division_name
		from hrm_employee a, lib_division d
		where a.division_id=d.id and d.division_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}
elseif ($search_string2==4){
		
		$sql= "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.department_name
		from hrm_employee a, lib_department d
		where a.department_id=d.id and d.department_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}
elseif ($search_string2==5){

		$sql= "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.section_name
		from hrm_employee a, lib_section d
		where a.section_id=d.id and d.section_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code";
}

elseif ($search_string2==6){
		
		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE id_card_no like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code";
} 	

elseif ($search_string2==7){
		
		$sql="SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE punch_card_no like '%$search_string%' and is_deleted=0 and status_active=1 order by emp_code";
}
//echo $sql;
$results=mysql_db_query($DB, $sql) or die(mysql_error());
while ($selectResult = mysql_fetch_array($results))
			{
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";	
?>
				 <tr bgcolor="<?php echo "$bgcolor"; ?>" style="text-decoration:none; cursor:pointer" onclick="js_set_value(<?php echo $i; ?>); parent.emailwindow.hide();"> 
					<td width="35"><?php echo "$i"; ?></td>	
					<td width="80"><?php echo "$selectResult[emp_code]"; ?> <input type="hidden" name="txt_emp_code<? echo $i; ?>" id="txt_emp_code<? echo $i; ?>" value="<?php echo "$selectResult[emp_code]"; ?>" /></td><input type="hidden" name="txt_emp_name<? echo $i; ?>" id="txt_emp_name<? echo $i; ?>" value="<?php echo "$selectResult[name]"; ?>" />	
					<td width="85"><? echo $selectResult[punch_card_no]; ?></td>
                    <td width="130"><?php echo $selectResult[name]; //"<a href='##' style='text-decoration:none' onclick='js_set_value($i); parent.emailwindow.hide();'>$selectResult[name]</a>"; ?> </td>	
					<td width="130"><? echo $designation_chart[$selectResult[designation_id]]; ?></td>
					<td width="120"><?php echo $company_details[$selectResult[company_id]]; ?></td>	
					<td width="110"><?php echo $division_details[$selectResult[division_id]]; //"$selectResult[division_name]"; ?></td>	
					<td width="115"><?php echo $department_details[$selectResult[department_id]]; ?></td>	
					<td><?php echo $section_details[$selectResult[section_id]]; ?></td>	
				</tr>
				<?php
				$i++;
				}
				?>
		</table>
    </div>
</div>
			
<?php 	
}
// Employee Information for Payback Sheduling
if($type=="emp_payback_sheduling")
{
	
?>
	<table width="100%" cellpadding="0" cellspacing="1">
			<tr bgcolor="#8FAA8C" align="">
				<td width="80">SL</td>
				<td>Emp. Code</td>
				<td>Emp. Name</td>
				<td>Designation</td>
				<td>Company Name</td>
				<td>Division</td>
				<td>Department</td>
				<td>Section</td>
				<td><input type="hidden" name="id_field" id="id_field" /></td>
			</tr>
	
<?php
$i=1;
if ($search_string2==0){
	if ($search_string!=""){
		/*$sql= mysql_db_query($DB, "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
					FROM hrm_employee AS emp
					LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
					WHERE emp.emp_code like '%$search_string' and emp.is_deleted=0 and emp.status_active=1 order by emp.emp_code");*/
		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
					WHERE emp_code like '%$search_string' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
	}
}
elseif ($search_string2==1){

	if ($search_string!=""){
/*		$sql= mysql_db_query($DB, "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
					FROM hrm_employee AS emp
					LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
					WHERE concat(emp.first_name,' ',emp.middle_name,' ',emp.last_name) like '%$search_string%' and emp.is_deleted=0 and emp.status_active=1 order by emp.emp_code");
*/		$sql= mysql_db_query($DB, "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee 
					WHERE concat(first_name,' ',middle_name,' ',last_name) like '%$search_string%' and is_deleted=0 and status_active=1 and emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by emp_code");
	}
}
elseif ($search_string2==2){
	if ($search_string!=""){
		/*$sql= mysql_db_query($DB, "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.custom_designation,f.*
		from hrm_employee a, lib_designation d, hrm_employee_job f
		where a.emp_code=f.emp_code and a.designation_id=d.id and d.custom_designation like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");*/
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.custom_designation
		from hrm_employee a, lib_designation d
		where a.designation_id=d.id and d.custom_designation like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
	}
}
elseif ($search_string2==3){
	if ($search_string!=""){
/*		$sql= mysql_db_query($DB, "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.division_name,f.*
		from hrm_employee a, lib_division d, hrm_employee_job f
		where a.emp_code=f.emp_code and f.division_id=d.id and d.division_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");
*/		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.division_name
		from hrm_employee a, lib_division d
		where a.division_id=d.id and d.division_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
	}
}
elseif ($search_string2==4){
	if ($search_string!=""){
		/*$sql= mysql_db_query($DB, "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.department_name,f.*
		from hrm_employee a, lib_department d, hrm_employee_job f
		where a.emp_code=f.emp_code and f.department_id=d.id and d.department_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");*/
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.department_name
		from hrm_employee a, lib_department d
		where a.department_id=d.id and d.department_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");
	}
}
elseif ($search_string2==5){
	if ($search_string!=""){
/*		$sql= mysql_db_query($DB, "select concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.section_name,f.*
		from hrm_employee a, lib_section d, hrm_employee_job f
		where a.emp_code=f.emp_code and f.section_id=d.id and d.section_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 order by a.emp_code");
*/
		$sql= mysql_db_query($DB, "select a.*, concat(a.first_name,' ',a.middle_name,' ',a.last_name) as name,a.designation_id,d.section_name
		from hrm_employee a, lib_section d
		where a.section_id=d.id and d.section_name like '%$search_string%' and a.is_deleted=0 and a.status_active=1 and a.emp_code in(select emp_code from hrm_loan_application where status_active=1 and is_deleted=0 and approved_status=3) order by a.emp_code");

	}
}

while ($selectResult = mysql_fetch_array($sql))
			{
	if ($i%2==0)  
		$bgcolor="#E9F3FF";
	else
		$bgcolor="#FFFFFF";	
?>
				 <tr style="text-decoration:none" align="" bgcolor="<?php echo "$bgcolor"; ?>"> 
					<td width="20" align="center"><?php echo "$i"; ?></td>	
					<td width="115" align="center"><?php echo "$selectResult[emp_code]"; ?> <input type="hidden" name="txt_emp_code<? echo $i; ?>" id="txt_emp_code<? echo $i; ?>" value="<?php echo "$selectResult[emp_code]"; ?>" /></td><input type="hidden" name="txt_emp_name<? echo $i; ?>" id="txt_emp_name<? echo $i; ?>" value="<?php echo "$selectResult[name]"; ?>" />	
					<td width="115"><?php echo "<a href='##' style='text-decoration:none' onclick='js_set_value($i); parent.emailwindow.hide();'>$selectResult[name]</a>"; ?> </td>	
					<td width="150"><? echo $designation_chart[$selectResult[designation_id]];// $selectResult[system_designation]; ?>
					</td>
					
					<td width="165"><?php 
									echo $company_details[$selectResult[company_id]]; //$selectResult[company_name];
									 ?></td>	
					<td width="80"><?php echo $division_details[$selectResult[division_id]]; //"$selectResult[division_name]"; ?></td>	
					<td width="80"><?php echo $department_details[$selectResult[department_id]];//"$selectResult[department_name]"; ?></td>	
					<td width="80"><?php echo $section_details[$selectResult[section_id]];// "$selectResult[section_name]"; ?></td>	
                    <td></td>
				</tr>
				<?php
				$i++;
				}
				?>
		</table>
<?php 	
}
function return_field_value($fdata,$tdata,$cdata)
{
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}
?>

