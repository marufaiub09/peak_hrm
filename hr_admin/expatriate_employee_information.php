<?php
/********************************************************
|														
|	Department Name	:	HRM								
|	Manu Name		:	Expatriate Employee Information	
|	Designed By		:	Md. Nuruzzaman					
|	Date			:	4 September, 2013				
|														
********************************************************/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
include('../includes/array_function.php');
$ConsumptionTabIndex=-1;
extract($_POST);
if(!$TabIndexNo) { $TabIndexNo=0; }

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Expatriate Employee Information</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen">
        
        <link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/popup_window.js"></script>
        <script type="text/javascript" src="../js/modal.js"></script>
        
        <script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
    
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="includes/functions.js" type="text/javascript"></script>
        
        <script>
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;

			$(document).ready(function() {
				$('.datepicker').datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
			
				$('#criteria1').change(function() {
					if( $(this).val() == 0 )		var opt = populate_cost_center2( 'company', 0 );
					else if( $(this).val() == 1 )	var opt = populate_cost_center2( 'location', 0 );
					else if( $(this).val() == 2 )	var opt = populate_cost_center2( 'division', 0 );
					else if( $(this).val() == 3 )	var opt = populate_cost_center2( 'department', 0 );
					else if( $(this).val() == 4 )	var opt = populate_cost_center2( 'section', 0 );
					else if( $(this).val() == 5 )	var opt = populate_cost_center2( 'designation', 0 );
				});
			});
			//After double click on the employee code fild "expatriate_emp_info" will be execute here
			function expatriate_emp_info(page_link,title)
			{			
				//alert("I am from DOUBLE Click");
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0',' ')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected_emp");
					
					var result= thee_loc.value.split('_' );
					document.getElementById('emp_code').value=result[0];
					document.getElementById('emp_name').value=result[1];
					document.getElementById('emp_cat').value=result[2];
					document.getElementById('designation_level').value=result[3];
					document.getElementById('designation').value=result[4];
					document.getElementById('join_date').value=result[5];
					document.getElementById('comp_name').value=result[6];
					document.getElementById('lication').value=result[7];
					document.getElementById('division').value=result[8];
					document.getElementById('department').value=result[9];
					document.getElementById('section').value=result[10];
					document.getElementById('sub_section').value=result[11];
					document.getElementById('father_name').value=result[12];
					document.getElementById('mother_name').value=result[13];
					
					if(result[14]==0) document.getElementById('sex').value="Male";	
					else document.getElementById('sex').value="Female";	
					 
					document.getElementById('nationality').value=result[15];
					document.getElementById('national_id').value=result[16];
					
					if(result[17]==0)document.getElementById('marital_status').value="Single";	
					else if(result[17]==1) document.getElementById('marital_status').value="Married";	
					else if(result[17]==2) document.getElementById('marital_status').value="Separated";	
					else if(result[17]==3) document.getElementById('marital_status').value="Widow";
					
					get_data_update_expatriate( result[0] );
					//document.getElementById('emp_code').value
				}
			}
			function get_data_update_expatriate( emp_codess )
			{
				ajax.requestFile = 'includes/get_data_update.php?emp_code='+emp_codess+'&type=get_data_update_exp';	// Specifying which file to get
				ajax.onCompletion = showEmpData_expatriate;	// Specify function that will be executed after file has been found
				ajax.runAJAX();	
			}
			function showEmpData_expatriate()
			{
				 eval(ajax.response);
			}
			
			//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

        </script>
    </head>
    <body>
        <div align="center">    
         <div id="examples" align="center" style="width:100%;font-size:11px; margin-left:-12px; margin-top:-20px;">
                <form name="expatriate_employee_info" id="expatriate_employee_info" method="post" style="margin-top:25px" action="javascript:fun_expatriate_employee_save(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete="off">
                    <fieldset>
                        <legend>Expatriate Employee Information</legend>
                        <!-- Start Form -->
                        <table cellpadding="0" cellspacing="1" width="900px">
                            <tr>
                                <input type="hidden" name="up_id" id="up_id" />
                                <td>Employee Code</td>
                                <td> 
                                <input type="text" name="emp_code" id="emp_code" class="text_boxes" ondblclick="expatriate_emp_info('includes/ot_popup/expatriate_emp_popup.php','Expatriate Employee Information'); return false" placeholder="Double click For browse" readonly /></td>
                                <td>Employee Name</td>
                                <td><input type="text" name="emp_name" id="emp_name" class="text_boxes" placeholder="" readonly /></td>
                                <td>Employee Category</td>
                                <td><input type="text" name="emp_cat" id="emp_cat" class="text_boxes" placeholder="" readonly /></td>
                            </tr>                        
                            <tr>
                                <td>Designation Level</td>
                                <td><input type="text" name="designation_level" id="designation_level" class="text_boxes" placeholder="" readonly /></td>
                                <td>Designation</td>
                                <td><input type="text" name="designation" id="designation" class="text_boxes" placeholder="" readonly /></td>
                                <td>Joining Date</td>
                                <td><input type="text" name="join_date" id="join_date" class="text_boxes" placeholder="" readonly /></td>
                            </tr>
                            <tr>
                                <td>Company Name</td>
                                <td><input type="text" name="comp_name" id="comp_name" class="text_boxes" placeholder="" readonly /></td>
                                <td>Location</td>
                                <td><input type="text" name="lication" id="lication" class="text_boxes" placeholder="" readonly /></td>
                                <td>Division</td>
                                <td><input type="text" name="division" id="division" class="text_boxes" placeholder="" readonly /></td>
                            </tr>
                            <tr>
                                <td>Department</td>
                                <td><input type="text" name="department" id="department" class="text_boxes" placeholder="" readonly /></td>
                                <td> Section</td>
                                <td><input type="text" name="section" id="section" class="text_boxes" placeholder="" readonly /></td>
                                <td>Subsection</td>
                                <td><input type="text" name="sub_section" id="sub_section" class="text_boxes" placeholder="" readonly /></td>
                            </tr>
                            <tr>
                                <td>Father's Name</td>
                                <td><input type="text" name="father_name" id="father_name" class="text_boxes" placeholder="" readonly /></td>
                                <td>Mother's Name</td>
                                <td><input type="text" name="mother_name" id="mother_name" class="text_boxes" placeholder="" readonly /></td>
                                <td>Sex</td>
                                <td><input type="text" name="sex" id="sex" class="text_boxes" placeholder="" readonly /></td>
                            </tr>
                            <tr>
                                <td>Nationality</td>
                                <td><input type="text" name="nationality" id="nationality" class="text_boxes" placeholder="" readonly /></td>
                                <td>National ID</td>
                                <td><input type="text" name="national_id" id="national_id" class="text_boxes" placeholder="" readonly /></td>
                                <td> Marital Status</td>
                                <td><input type="text" name="marital_status" id="marital_status" class="text_boxes" placeholder="" readonly /></td>
                            </tr>                            
                            <tr>
                            	<td colspan="6" height="22">
                                	<div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div>
                                </td>
                            </tr>
                            <tr>
                            	<td colspan="6">
                                    <table>
                                        <tr>
                                            <td>
                                                <fieldset style="width: 240px;">
                                                    <legend>Passport Information</legend>
                                                    <table align="left" width="240px">
                                                        <tr>
                                                        	<td>Number</td>
                                                            <td><input type="text" name="pass_no" id="pass_no" class="text_boxes" placeholder="" style="width:100px;" onkeypress="return numbersonly(this,event)"/></td>
                                                        </tr>
                                                        <tr>
                                                        	<td> Issue Date</td>
                                                            <td><input type="text" name="pass_issue_date" id="pass_issue_date" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                        <tr>
                                                        	<td>Expiry Date</td>
                                                            <td><input type="text" name="pass_expire_date" id="pass_expire_date" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Renew Process Start</td>
                                                            <td><input type="text" name="pass_renew" id="pass_renew" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                            <td>
                                                <fieldset style="width: 240px;">
                                                    <legend>Visa Information</legend>
                                                    <table align="left" width="240px">
                                                        <tr>
                                                            <td>Number</td>
                                                            <td><input type="text" name="visa_no" id="visa_no" class="text_boxes" placeholder="" style="width:100px;" onkeypress="return numbersonly(this,event)"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Issue Date</td>
                                                            <td><input type="text" name="visa_issue_date" id="visa_issue_date" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Expiry Date</td>
                                                            <td><input type="text" name="visa_expire_date" id="visa_expire_date" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                            <tr>
                                                            <td>Renew Process Start</td>
                                                            <td><input type="text" name="visa_renew" id="visa_renew" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                            <td>
                                                <fieldset style="width: 240px;">
                                                    <legend>Work Permit Information</legend>
                                                    <table align="left" width="240px">
                                                        <tr>
                                                            <td>Number</td>
                                                            <td><input type="text" name="permit_no" id="permit_no" class="text_boxes" placeholder="" style="width:100px;" onkeypress="return numbersonly(this,event)" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Issue Date</td>
                                                            <td><input type="text" name="permit_issue_date" id="permit_issue_date" class="datepicker" placeholder="" readonly style="width:100px;" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Expiry Date</td>
                                                            <td><input type="text" name="permit_expire_date" id="permit_expire_date" class="datepicker" placeholder="" readonly  style="width:100px;"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Renew Process Start</td>
                                                            <td><input type="text" name="permit_renew" id="permit_renew" class="datepicker" placeholder="" readonly style="width:100px;" /></td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                            <td>
                                                <fieldset style="width: 240px;">
                                                    <legend>Contract Information</legend>
                                                    <table align="left" width="240px">
                                                        <tr>
                                                            <td>Number</td>
                                                            <td><input type="text" name="contact_no" id="contact_no" class="text_boxes" placeholder="" style="width:100px;" onkeypress="return numbersonly(this,event)" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Start Date</td>
                                                            <td><input type="text" name="contact_start" id="contact_start" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>End Date</td>
                                                            <td><input type="text" name="contact_end" id="contact_end" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Renew Process Start</td>
                                                            <td><input type="text" name="contact_renew" id="contact_renew" class="datepicker" placeholder="" readonly style="width:100px;"/></td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                           </tr>
                           <tr>
                           	<td colspan="6" height="5"></td>
                           </tr>
                           <tr>
                           	<td colspan="6">
                            	<table>
                                	<tr>
                                    	<td>
                                            <fieldset style="width: 350px;">
                                                <legend>Other's Information</legend>
                                                <table align="left" style="width: 350px;">
                                                    <tr>
                                                        <td>Salary Currency</td>
                                                        <td><select name="currency" id="currency" class="combo_boxes" style="width:112px;">
                                                                <option value="">---select---</option>
                                                                <?php //currency_details
                                                                $sql = "SELECT currency_name FROM lib_list_currency WHERE  status_active=1 and is_deleted = 0 ORDER BY currency_name ASC";
                                                                $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
                                                                while( $row = mysql_fetch_assoc( $result ) ) {
                                                                ?>		
                                                                <option value="<?php echo $row['currency_name']; ?>"><?php echo $row['currency_name']; ?></option>
                                                                <?php
                                                                }
                                                                ?>                                                                      
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    	<td>Local Currency Payment</td>
                                                        <td><input type="text" name="local_currency" id="local_currency" class="text_boxes" placeholder="" style="width:100px;" onkeypress="return numbersonly(this,event)" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    	<td>National Exchange Rate</td>
                                                        <td><input type="text" name="exchange_rate" id="exchange_rate" class="text_boxes" placeholder=""  style="width:100px;" onkeypress="return numbersonly(this,event)" />
                                                        </td>
                                                    </tr>
                                                </table>
                                        	</fieldset>
                                        </td>
                                        <td>
                                            <fieldset>
                                            	<legend>Remark</legend>
                                                <table align="left">
                                                    <tr><td><textarea name="remark" id="remark" class="text_area" style="width:645px; height:70px;"></textarea></td></tr>
                                                </table>
                                        	</fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                           </tr>
                    	</table>
                        <table>
                        	<tr><td colspan="6" height="10"></td></tr>
                            <tr>
                                <td colspan="4" align="center" class="button_container" width="100%">
                                    <input type="submit" value=" Save" name="save" id="save" class="formbutton" style="width:100px"/>&nbsp;
                                    <!--<input type="submit" value=" Update" name="update" id="update" class="formbutton" style="width:100px"/>&nbsp;
                                    <input type="submit" value="Delete" name="delete" id="delete" class="formbutton" style="width:100px"/>&nbsp;-->
                                    <input type="reset" value="Cancel"  name="add" id="close" class="formbutton" onclick="hidden_field_reset()" style="width:100px"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>