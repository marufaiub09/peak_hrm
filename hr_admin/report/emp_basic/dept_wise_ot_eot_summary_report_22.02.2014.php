<?php 
/***********************************
| Developed by	: Md. Nuruzzaman 
| Date			: 22.02.2014
***********************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
		
	$sql="SELECT * FROM lib_skill WHERE is_deleted = 0 ORDER BY skill_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$skill_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$skill_details[$row['id']] = mysql_real_escape_string( $row['skill_name'] );
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}
//$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        
        <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    	<script type="text/javascript" src="../../../includes/tablefilter.js"></script>
        
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				// datepicker
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
				
				// multiselect
				$("#cbo_status,#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id,#shift_id").multiselect({
					//header: false, 
					selectedText: "# of # selected",
				});
			});
			
			//ajx submit starts here
			function createObject() 
			{
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) 
				{
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} 
				else 
				{
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
			
			// Months dropdown
			function populate_select_month( selected_id ) {
				//alert (selected_id);
				$.ajax({
					type: "GET",
					url: "includes/generate_dept_wise_ot_eot_summary_report.php",
					data: 'type=select_month_generate&id='+ selected_id,
					success: function( html ) {
						$('#cbo_month').html( html )
					}
				});
			}
			
			//fnc_dept_wise_ot_eot_summary_report_show
			function fnc_dept_wise_ot_eot_summary_report_show()
			{
				//alert("su..re");
				$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
				var data = '';
				var category_id		= $('#cbo_category_id').val();
				var company_id		= $('#cbo_company_id').val();
				var location_id		= $('#cbo_location_id').val();
				var division_id		= $('#cbo_division_id').val();
				var department_id	= $('#cbo_department_id').val();
				var section_id		= $('#cbo_section_id').val();
				var subsection_id	= $('#cbo_subsection_id').val();
				var designation_id	= $('#cbo_designation_id').val();
				var year			= $('#cbo_year').val();
				var month			= $('#cbo_month').val();
				var from_date		= $('#txt_from_date').val();
				var to_date			= $('#txt_to_date').val();
				var salary_based	= $('#cbo_salary_based').val();
				var	emp_type		= $('#cbo_emp_type').val();
				var group_by_id		= $('#cbo_group_by_id').val();
				var order_by_id		= $('#cbo_order_by_id').val();
					
				var data =	'&category_id='+category_id+
							'&company_id='+company_id+
							'&location_id='+location_id+
							'&division_id='+division_id+
							'&department_id='+department_id+
							'&section_id='+section_id+
							'&subsection_id='+subsection_id+
							'&designation_id='+designation_id+
							'&year='+year+
							'&month='+month+
							'&from_date='+from_date+
							'&to_date='+to_date+
							'&salary_based='+salary_based+
							'&emp_type='+emp_type+
							'&group_by_id='+group_by_id+
							'&order_by_id='+order_by_id;
							
														
					nocache = Math.random();
					http.open( 'GET', 'includes/generate_dept_wise_ot_eot_summary_report.php?action=generate_dept_wise_ot_eot_summary_report' + data + '&nocache=' + nocache );
					http.onreadystatechange = response_generate_dept_wise_ot_eot_summary_report;
					http.send(null);
			}
			
			//Punch Report Response	
			function response_generate_dept_wise_ot_eot_summary_report() 
			{	
				if(http.readyState == 4) {
					var response=http.responseText.split('####');
					//alert(response);		
					//$('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
					$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').append( '<input type="button" onclick="new_window_short()" value="HTML Short" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel2').html( response[0] );
					$('#data_panel3').html( response[2] );
					
					hide_search_panel();
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
			}	
			
			function new_window()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel2').innerHTML);
				d.close();
			}
			
			function new_window_short()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel3').innerHTML);
				d.close();
			}
		</script>
		<style type="text/css"> 
			td {overflow:hidden; }
			@media print {thead {display: table-header-group;}}
			@media print {tfoot {display: table-footer-group;}}
			.verticalText 
			{               
				writing-mode: tb-rl;
				filter: flipv fliph;
				-webkit-transform: rotate(270deg);
				-moz-transform: rotate(270deg);
			}
		</style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:1220px;" align="center"></div></div> 
        <h3 align="left" style="top:1px;width:1220px;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Department Wise Daily OT/EOT Summary Report</h3>
        <div id="content_search_panel" style="width:1120px;"> 
            <form id="frm_emp_nominee_info" name="frm_emp_nominee_info" action="javascript:fnc_dept_wise_daily_ot_eot_summary_report_show()" autocomplete="off" method="POST">
                <fieldset id="filter_panel" style="width:1120px" >
                    <table class="rpt_table"  width="1120" cellspacing="1" align="center">
                        <thead>
                            <th width="140">Category</th>
                            <th width="140">Company Name</th>				    
                            <th width="140">Location</th>
                            <th width="140">Division</th>				    
                            <th width="140">Department</th>
                            <th width="140">Section</th>				    
                            <th width="140">Subsection</th>
                            <th width="140">Designation</th>				    
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:140px">
                                        <option value="">All Category</option>
                                        <?
                                        foreach($employee_category as $key=>$val)
                                        {
                                        ?>
                                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                        <?
                                        }
                                        ?> 
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px" >
                                        <? if($company_cond=="")
                                        { 
                                        ?>
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        } 
                                        foreach( $company_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:140px" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $location_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $division_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $department_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $section_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="subsection">
                                    <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $subsection_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="designation">
                                    <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $designation_chart AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="height:10px;"></div>
                    <table class="rpt_table"  width="1050" cellspacing="1" align="center">
                        <thead>
                            <th width="100">Year</th>
                            <th width="100">Month</th>
                            <th width="140" colspan="2">Date</th>				    
                            <th width="140">Salary Based</th>
                            <th width="140">Emp. Type</th>
                            <th width="140">Group By</th>				    
                            <th width="140">Order By</th>
                            <th width="70"><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:70px" /></th>				    
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select name="cbo_year" id="cbo_year" class="combo_boxes" onchange="populate_select_month(this.value)" style="width:100px" > 					  
                                        <option value="0">-- Select --</option>
                                        <?php 
										foreach( $policy_year AS $key=>$value )
										{ 
										?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
										} 
										?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_month" id="cbo_month" class="combo_boxes" style="width:100px">							
                                        <option value="0">-- Select --</option>
                                    </select>
                                </td> 
                                <td>
                                    <input type="text" name="txt_from_date" id="txt_from_date" value="" class="datepicker" size="10" style="width:70px" />
                               </td>
                                <td>
                                     <input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" size="10" style="width:70px" />
                                </td> 
                                <td>
                                    <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:140px" >							
                                       <option value="">-- Select --</option>
                                        <?php 
										foreach( $salary_based_arr AS $key=>$value )
										{ 
										?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
										} 
										?>
                                     </select>
                                </td>
                                <td>
                                    <select name="cbo_emp_type" id="cbo_emp_type" class="combo_boxes" style="width:140px" >                    	
                                    <option value=""> All Employee </option>
                                    <option value="0">Regular Employee</option>
                                    <option value="1">Separated Employee</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_group_by_id" id="cbo_group_by_id" class="combo_boxes" style="width:140px" >							
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        foreach( $groupby_arr AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_order_by_id" id="cbo_order_by_id" class="combo_boxes" style="width:140px" >							
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        foreach( $orderby_arr AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td bgcolor="#A6CAF0"><input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:70px" /></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            </form>
        </div>
        <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel2" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden">&nbsp;</div>
    </body>
</html>
