<?php
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond;
//--------------------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<script src="../../../resources/jquery-1.6.1.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	function populate() {
			
			if(document.getElementById('company_id').value==0)
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#company_id').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			else if(document.getElementById('cost_center').value==0)
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cost_center').focus();
			$(this).html('Please Select Cost Center').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			
			else if(document.getElementById('to_date').value=="")
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#to_date').focus();
			$(this).html('Please Select Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
		else
		{
		$('#messagebox').removeClass().addClass('messagebox').text('Report Generating....').fadeIn(1000);
		var div="data_panel";
		document.getElementById(div).innerHTML = "";
		
		var data=document.getElementById('company_id').value+"_"+document.getElementById('cost_center').value+"_"+document.getElementById('to_date').value;
		//alert(data);
		if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
		else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
		
		xmlhttp.onreadystatechange = function() {
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				//document.getElementById(div).innerHTML = xmlhttp.responseText;
				var data_split=(xmlhttp.responseText).split('####');
				var link_data=data_split[1];			
				document.getElementById('confirm_div').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
				document.getElementById(div).innerHTML=data_split[0];
				
				hide_search_panel();
				
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).html('Report has been generated succesfully.....').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
		}
		xmlhttp.open( "GET", "includes/generate_report.php?data=" + data +"&type=leavers_report", true );
		xmlhttp.send();
		}
	}
	
	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.height="auto";
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel').innerHTML+'</body</html>');
		d.close();
		document.getElementById('scroll_body').style.overflow="scroll";
		document.getElementById('scroll_body').style.height="280px";
	}	
	
	function reset_field()
	{
		document.getElementById('data_panel').innerHTML="";
		document.getElementById('confirm_div').innerHTML="";
	}
	</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:100%;">
	<div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>	
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Separated Employee Summary</h3>
    <div id="content_search_panel" > 
    <form>
    <fieldset>
    		<table align="center" cellspacing="0" cellpadding="0" width="700px" class="rpt_table" >
    			<thead>
					<th align="center"><strong>Select Company</strong></th>
                    <th align="center"><strong>Select Cost Center</strong></th>
                   <!-- <th align="center"><strong>From Date</strong></th>-->
                    <th align="center"><strong>Select Date</strong></th>
                    <th><input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();" style="width:100px"/></th>
                 </thead>
                <tr class="general">
                    <td id="company">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:150px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
					<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
					<?php } } ?>
							
						</select>
					</td>
                    <td><select name="cost_center" id="cost_center" class="combo_boxes" style="width:150px">
                              <option value="0">---Select---</option>
                              <option value="1">by Location</option>
                              <option value="2">by Division</option>
                              <option value="3">by Department</option>
                              <option value="4">by Section</option>
                              <option value="5">by Subsection</option>
                              <option value="6">by Designation</option>
                  		</select></td>
                   
                    <td><input type="text" name="to_date" id="to_date" value="" class="text_boxes" style="width:140px"/>
							 <script type="text/javascript">
                                $( "#to_date" ).datepicker({
                                dateFormat: 'dd-mm-yy',
                                changeMonth: true,
                                changeYear: true
                                });
                            </script>
                    </td>
                    <td><input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="populate();" style="width:100px" /></td>
                </tr>
			</table>
	</fieldset>
    </form> 
    </div>   
</div>
<fieldset>
    <div id="confirm_div" style="margin-top:5px;" align="center"></div>
	<div id="data_panel" align="left" style="margin-top:10px;"></div>
</fieldset>
</body>
</html>