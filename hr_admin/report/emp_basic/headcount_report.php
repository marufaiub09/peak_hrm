<?php
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond;
//--------------------------------------------------------------------------------------------------------------------

include("../../../includes/common.php");
include("../../../includes/array_function.php");
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script> 
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
	
	<script type="text/javascript">
	
	function toggle( x, origColor ) {
		//alert(x+origColor);
			var newColor = 'yellow';
			if ( document.getElementById(x).style ) {
				document.getElementById(x).style.backgroundColor = ( newColor == document.getElementById(x).style.backgroundColor )? origColor : newColor;
			}
		}
	
	
	function reset_field()
	{
		document.getElementById('data_panel').innerHTML="";
		document.getElementById('data_panel2').innerHTML="";
	}
	
	</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:100%;">
	
		<div align="center" id="messagebox"></div>
	</div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Head Count Reporting of Employees</h3>
    <div id="content_search_panel" > 
   <form>
    <fieldset>
		
        	<table cellspacing="0" cellpadding="0" width="700" class="rpt_table" align="center" >
				<thead>
                	<th align="center"><strong>Select Company</strong></th>
                    <th align="center"><strong>Select Cost Center</strong></th>
                    <th align="center"><strong>Employee Category</strong></th>
                    <th align="center"><strong>Select Date</strong></th>
                    <th><input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();" style="width:100px"/></th>
				</thead>
                <tr class="general">
                    <td id="company">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:150px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
					<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
					<?php } } ?>
							
						</select>
					</td>
                    <td>
                        <select name="cost_center" id="cost_center" class="combo_boxes" style="width:150px">
                              <option value="0">---Select---</option>
                              <option value="1">by Location</option>
                              <option value="2">by Division</option>
                              <option value="3">by Department</option>
                              <option value="4">by Section</option>
                              <option value="5">by Subsection</option>
                              <option value="6">by Designation</option>
                         </select>
                    </td>
                    <td>
                        <select name="emp_category" id="emp_category" class="combo_boxes" style="width:150px">
                            <option value="">All Category</option>
							<? foreach($employee_category as $key=>$val) { ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option> <? } ?> 
                        </select>
                    </td>
                    <td><input type="text" name="to_date" id="to_date" value="" class="text_boxes" style="width:140px"/>
                   								 <script type="text/javascript">
                                                    $( "#to_date" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               	</script>
                    </td>
                    <td><input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="head_count_populate();" style="width:100px" /></td>
                </tr>
			</table>
	</fieldset>
   </form>
   </div>
</div>
<fieldset>
    <div id="data_panel" style="margin-top:5px;" align="center"></div>
	<div id="data_panel2" align="left" style="margin-top:10px;"></div>
</fieldset>
</body>
</html>