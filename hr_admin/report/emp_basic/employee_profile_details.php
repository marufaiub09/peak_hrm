<?php
/*************************************
|
|	Developed By	: Md. Nuruzzaman
|	Date			: 03.06.2014
|
**************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//---------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
include("../../../includes/array_function.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        <script type="text/javascript">
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
		
            function show_employee_profile()
            {
                //alert("su..re");
                if($('#emp_code').val()=="" && $('#id_card').val()=="")
                {
                    //alert("su..re");
                    if($('#id_card').val()=="")
                    {
                        $('#messagebox').fadeTo( 200, 0.1, function() {
                            $('#id_card').focus();
                            $(this).html('Please enter employee id card.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                    }
                    else
                    {
                        $('#messagebox').fadeTo( 200, 0.1, function() {
                            $('#emp_code').focus();
                            $(this).html('Please enter employee code.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);
                        });
                    }
                }
				else 
				{
					$('#messagebox').removeClass().addClass('messagebox').text('Report generating....').fadeIn(1000);
					nocache = Math.random();
					http.open('GET','includes/generate_employee_profile_details.php?action=action_employee_profile_details'+'&id_card='+$('#id_card').val()+'&emp_code='+$('#emp_code').val()+ '&nocache=' + nocache);
					http.onreadystatechange = response_employee_profile_details;
					http.send(null);
				}	
            }
				
    		function response_employee_profile_details()
			{
				if(http.readyState == 4)
				{ 		
					var response = http.responseText.split('####');
					<!--$('#data_panel').html( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );-->
					$('#data_panel2').html( response[0] );
					//$("tr:even").css("background-color", "#000000");
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Profile generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
			}
			
			function print_report()
			{
				$('#accordion_h1').hide();
				$('#content_search_panel').hide();
				$('#print').hide();
				window.print();
				$('#accordion_h1').show();
				$('#content_search_panel').show();
				$('#print').show();
			}
			
			function new_window()
			{
				$('#accordion_h1').hide();
				$('#content_search_panel').hide();
				$('#print').hide();
				
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel2').innerHTML);
				d.close();
				
				$('#accordion_h1').show();
				$('#content_search_panel').show();
				$('#print').show();
			}
			
            function openmypage_employee_info()
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php','Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')		
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#emp_code').val(thee_id.value);
                }
            }
                
            function openmypage_employee_info_id_card()
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php','Employee Information',  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }
                
            function reset_field()
            {
                document.getElementById('data_panel').innerHTML="";
                document.getElementById('data_panel2').innerHTML="";
            }
            
            //Numeric Value allow field script
            function numbersonly(myfield, e, dec)
            {
                var key;
                var keychar;
            
                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);
            
                // control keys
                if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
                return true;
                
                // numbers
                else if ((("0123456789.,-").indexOf(keychar) > -1)) return true;
                else return false;
            }
        </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%;">
            <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:430px;" align="center"></div></div>
            <h3 align="left" style="top:1px;width:430px;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee Profile</h3>
            <div id="content_search_panel" > 
                <form>
                    <fieldset style="width:400px;">
                        <table align="center" cellspacing="0" cellpadding="0" width="380" class="rpt_table" >
                            <thead>	
                                <th align="center" width="140px"><strong>ID Card No</strong></th>
                                <th align="center" width="140px"><strong>System Code</strong></th>
                                <th align="center"><input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();"  style="width:100px"/></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:140px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>
                                    <td align="center">
                                        <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:140px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                                    </td>                
                                    <td align="center">
                                        <input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="show_employee_profile();"  style="width:100px"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                  </fieldset>
                </form>
            </div>
            <div id="data_panel" style="margin-top:5px;" align="center"></div>
            <div id="data_panel2" align="center" style="margin-top:10px;"></div>
        </div>
    </body>
</html>