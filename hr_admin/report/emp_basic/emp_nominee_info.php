<?php 
/***********************************
| Developed by	: Md. Nuruzzaman 
| Date			: 19.02.2014
***********************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
		
	$sql="SELECT * FROM lib_skill WHERE is_deleted = 0 ORDER BY skill_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$skill_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$skill_details[$row['id']] = mysql_real_escape_string( $row['skill_name'] );
	}

//$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
         <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
     <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    	<script type="text/javascript" src="../../../includes/tablefilter.js"></script>
        
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$("#cbo_status,#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id,#shift_id").multiselect({
					//header: false, 
					selectedText: "# of # selected",
				});
				$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
			});
			
			$(document).ready(function() {
				$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
				});
			});
			
			//ajx submit starts here
			function createObject() 
			{
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) 
				{
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} 
				else 
				{
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
			
			function fnc_emp_nominee_info_show()
			{
				//alert("su..re");
				$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
				var data = '';
				var category_id		= $('#cbo_category_id').val();
				var company_id		= $('#cbo_company_id').val();
				var location_id		= $('#cbo_location_id').val();
				var division_id		= $('#cbo_division_id').val();
				var department_id	= $('#cbo_department_id').val();
				var section_id		= $('#cbo_section_id').val();
				var subsection_id	= $('#cbo_subsection_id').val();
				var designation_id	= $('#cbo_designation_id').val();
				var group_by_id		= $('#cbo_group_by_id').val();
				var order_by_id		= $('#cbo_order_by_id').val();
				var id_card			= $('#txt_id_card_no').val();
				var emp_code		= $('#txt_emp_code').val();
				var txt_fromdate	= $('#txt_fromdate').val();
				var txt_todate		= $('#txt_todate').val();
				var nominee_entitled= $('#nominee_entitled').val();
					
				var data =	'&category_id='+category_id+
							'&company_id='+company_id+
							'&location_id='+location_id+
							'&division_id='+division_id+
							'&department_id='+department_id+
							'&section_id='+section_id+
							'&subsection_id='+subsection_id+
							'&designation_id='+designation_id+
							'&nominee_entitled='+nominee_entitled+
							'&group_by_id='+group_by_id+
							'&order_by_id='+order_by_id+
							'&id_card='+id_card+
							'&txt_fromdate='+txt_fromdate+
							'&txt_todate='+txt_todate+
							'&emp_code='+emp_code;
														
					nocache = Math.random();
					http.open( 'GET', 'includes/generate_emp_nominee_info_report.php?action=generate_emp_nominee_info_report' + data + '&nocache=' + nocache );
					http.onreadystatechange = response_generate_emp_nominee_info_report;
					http.send(null);
			}
			
			//Punch Report Response	
			function response_generate_emp_nominee_info_report() 
			{	
				if(http.readyState == 4) {
					var response=http.responseText.split('####');
					//alert(response);		
					//$('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
					$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').append( '<input type="button" onclick="new_window_short()" value="HTML Short" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel2').html( response[0] );
					$('#data_panel3').html( response[2] );
					
					hide_search_panel();
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Employee Nominee Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
			}	
			
			function new_window()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel2').innerHTML);
				d.close();
			}
			
			function new_window_short()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel3').innerHTML);
				d.close();
			}
			
			 // Employee search by emp_code
            function openmypage_employee_info()
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_category_id').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#txt_emp_code').val(thee_id.value);
                }
            }
			
			 // Employee search by id_card_no
            function openmypage_employee_info_id_card()
            {			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#cbo_category_id').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#txt_id_card_no').val(thee_id.value);
                }
            }
		</script>
		<style type="text/css"> 
			td {overflow:hidden; }
			@media print {thead {display: table-header-group;}}
			@media print {tfoot {display: table-footer-group;}}
			.verticalText 
			{               
				writing-mode: tb-rl;
				filter: flipv fliph;
				-webkit-transform: rotate(270deg);
				-moz-transform: rotate(270deg);
			}
		</style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div style="width:100%;" align="center">
            <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>        <h3 align="left" style="top:1px;width:100%;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee Nominee Information</h3>
            <div id="content_search_panel"> 
                <form id="frm_emp_nominee_info" name="frm_emp_nominee_info" action="javascript:fnc_emp_nominee_info_show()" autocomplete="off" method="POST">
                    <fieldset>
                        <table class="rpt_table"  width="1120" cellspacing="1" align="center">
                            <thead>
                                <th width="140">Category</th>
                                <th width="140">Company Name</th>				    
                                <th width="140">Location</th>
                                <th width="140">Division</th>				    
                                <th width="140">Department</th>
                                <th width="140">Section</th>				    
                                <th width="140">Subsection</th>
                                <th width="140">Designation</th>				    
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px" >
                                            <? if($company_cond=="")
                                            { 
                                            ?>
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:140px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <?php foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <?php foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <?php foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <?php foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="designation">
                                        <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                            <?php foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="height:10px;"></div>
                        <table class="rpt_table"  width="1000" cellspacing="1" align="center">
                            <thead>
                                <th width="200">Joining Date</th>
                                <th width="120">Nominee Entitled</th>
                                <th width="120">Group By</th>
                                <th width="120">Order By</th>
                                <th width="140">ID Card No</th>				    
                                <th width="140">System Code</th>
                                <th width="70"><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:70px" /></th>				    
                            </thead>
                            <tbody>
                                <tr>
                                     <td>
                                           <input type="text" name="txt_fromdate" id="txt_fromdate" value="" class="datepicker" style="width:80px" readonly="readonly" /> To
                                           <input type="text" name="txt_todate" id="txt_todate" value="" class="datepicker" style="width:80px" readonly="readonly" />
                                    </td>
                                    <td align="center">
                                        <select name="nominee_entitled" id="nominee_entitled" class="combo_boxes" style="width:120px" >                    	
                                            <option value="0">-- Select --</option>
                                            <option value="1">Entitled</option>
                                            <option value="2">Not Entitled</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_group_by_id" id="cbo_group_by_id" class="combo_boxes" style="width:120px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $groupby_arr AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_order_by_id" id="cbo_order_by_id" class="combo_boxes" style="width:120px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $orderby_arr AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="txt_id_card_no" id="txt_id_card_no" class="text_boxes" style="width:140px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>
                                    <td align="center">
                                        <input type="text" id="txt_emp_code" name="txt_emp_code" class="text_boxes" style="width:140px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                                    </td>
                                    <td bgcolor="#A6CAF0"><input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:70px" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
            <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="data_panel2" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden">&nbsp;</div>
        </div>
    </body>
</html>
