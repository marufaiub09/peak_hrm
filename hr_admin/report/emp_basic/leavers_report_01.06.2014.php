<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond;
//--------------------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
include("../../../includes/array_function.php");

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) 
	{
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<script src="../../../resources/jquery-1.6.1.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$( "#txt_date" ).datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		//ajax object create
		function createObject() 
		{
			var request_type;
			var browser = navigator.appName;
			if( browser == "Microsoft Internet Explorer" ) 
			{
				request_type = new ActiveXObject("Microsoft.XMLHTTP");
			} 
			else 
			{
				request_type = new XMLHttpRequest();
			}
			return request_type;
		}
		
		var http = createObject();
		var field_array = new Array();
		
		// fnc_separated_emp_summary
		function fnc_separated_emp_summary()
		{
			if($('#cbo_company_id').val()==0)
			{
				$("#messagebox").fadeTo(200,0.1,function(){
				$('#cbo_company_id').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				});		
			}
			else if($('#txt_date').val()=="")
			{
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#txt_date').focus();
				$(this).html('Please Select Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				});		
			}
			else
			{
				//alert("su..re");	
				$('#messagebox').removeClass().addClass('messagebox').text('Report Generating....').fadeIn(1000);
				var data=	'&category_id='+$('#cbo_category_id').val() +
							'&company_id='+$('#cbo_company_id').val() +
							'&location_id='+$('#cbo_location_id').val() +
							'&division_id='+$('#cbo_division_id').val() +
							'&department_id='+$('#cbo_department_id').val() +
							'&section_id='+$('#cbo_section_id').val() +
							'&subsection_id='+$('#cbo_subsection_id').val() +
							'&designation_id='+$('#cbo_designation_id').val() +
							'&date='+$('#txt_date').val();
				
				nocache = Math.random();
				http.open( 'GET', 'includes/generate_leavers_report.php?action=action_separated_emp_summary' + data + '&nocache=' + nocache );
				http.onreadystatechange = response_separated_emp_summary;
				http.send(null);
			}
		}
		
		function response_separated_emp_summary()
		{
			//alert("su..re");	
		}
	
	function populate() {
			
			if(document.getElementById('company_id').value==0)
			{
				$("#messagebox").fadeTo(200,0.1,function(){
				$('#company_id').focus();
				$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				});		
			}
			else if(document.getElementById('cost_center').value==0)
			{
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#cost_center').focus();
				$(this).html('Please Select Cost Center').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				});		
			}
			
			else if(document.getElementById('to_date').value=="")
			{
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
				$('#to_date').focus();
				$(this).html('Please Select Date').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				});		
			}
		else
		{
		$('#messagebox').removeClass().addClass('messagebox').text('Report Generating....').fadeIn(1000);
		var div="data_panel";
		document.getElementById(div).innerHTML = "";
		
		var data=document.getElementById('company_id').value+"_"+document.getElementById('cost_center').value+"_"+document.getElementById('to_date').value;
		//alert(data);
		if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
		else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
		
		xmlhttp.onreadystatechange = function() {
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				//document.getElementById(div).innerHTML = xmlhttp.responseText;
				var data_split=(xmlhttp.responseText).split('####');
				var link_data=data_split[1];			
				document.getElementById('confirm_div').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
				document.getElementById(div).innerHTML=data_split[0];
				
				hide_search_panel();
				
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).html('Report has been generated succesfully.....').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
		}
		xmlhttp.open( "GET", "includes/generate_report.php?data=" + data +"&type=leavers_report", true );
		xmlhttp.send();
		}
	}
	
	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.height="auto";
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel').innerHTML+'</body</html>');
		d.close();
		document.getElementById('scroll_body').style.overflow="scroll";
		document.getElementById('scroll_body').style.height="280px";
	}	
	
	function reset_field()
	{
		document.getElementById('data_panel').innerHTML="";
		document.getElementById('confirm_div').innerHTML="";
	}
	</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%;">
            <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>	
            <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')">
                -Separated Employee Summary
            </h3>
            <div id="content_search_panel" > 
                <form id="frm_separated_emp_summary" name="frm_separated_emp_summary" action="javascript:fnc_separated_emp_summary()">
                    <fieldset>
                        <table align="center" cellspacing="0" cellpadding="0" width="1120px" class="rpt_table" >
                            <thead>
                                <th width="140px" align="center"><strong>Category</strong></th>
                                <th width="140px" align="center"><strong>Company</strong></th>
                                <th width="140px" align="center"><strong>Location</strong></th>
                                <th width="140px" align="center"><strong>Division</strong></th>
                                <th width="140px" align="center"><strong>Department</strong></th>
                                <th width="140px" align="center"><strong>Section</strong></th>
                                <th width="140px" align="center"><strong>Sub Section</strong></th>
                                <th width="140px" align="center"><strong>Designation</strong></th>
                            </thead>
                            <tbody>
                                <tr class="general">
                                    <td align="center">
                                        <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td align="center" id="company">
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
											<? 
                                            if($company_cond=="")
                                            { 
                                            ?>
                                            <option value="0">-- select --</option>
                                            <?php 
											} 
											foreach( $company_details AS $company ) 
											{ 
												if( $company['status_active'] == 1 ) 
												{ 
												?>
												<option value="<?php echo $company['id']; ?>" <? if(count($company_details)==1)echo"selected"; ?>><?php echo $company['company_name']; ?></option>
												<?php 
												} 
											} 
											?>
                                        </select>
                                    </td>
                                    <td align="center" id="location">
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- select --</option>
                                            <?php 
                                            foreach( $location_details AS $location ) 
                                            { 
                                                if( $location['status_active'] == 1 ) 
                                                { 
                                                ?>
                                                <option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
                                                <?php 
                                                } 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td align="center" id="division">
                                        <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- select --</option>
                                            <?php 
											foreach( $division_details AS $division ) 
											{ 
												if( $division['status_active'] == 1 ) 
												{ 
												?>
												<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
												<?php 
												} 
											} 
											?>
                                        </select>
                                    </td>
                                    <td align="center" id="department">
                                        <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- select --</option>
                                            <?php 
											foreach( $department_details AS $department ) 
											{ 
												if( $department['status_active'] == 1 ) 
												{ 
												?>
												<option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
												<?php 
												} 
											} 
											?>
                                        </select>
                                    </td>
                                    <td align="center" id="section">
                                        <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- select --</option>
                                            <?php 
											foreach( $section_details AS $section ) 
											{ 
												if( $section['status_active'] == 1 ) 
												{ 
												?>
												<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
												<?php 
												} 
											}
											?>
                                        </select>
                                    </td>
                                    <td align="center" id="subsection">
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- select --</option>
                                            <?php 
											foreach( $subsection_details AS $subsection ) 
											{ 
												if( $subsection['status_active'] == 1 ) 
												{ 
												?>
												<option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
												<?php 
												} 
											} 
											?>
                                        </select>
                                    </td>
                                    <td align="center" id="designation">
                                        <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- select --</option>
                                            <?php 
											foreach( $designation_chart AS $designation ) 
											{ 
												if( $designation['status_active'] == 1 ) 
												{ 
												?>
												<option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
												<?php 
												} 
											} 
											?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="height:8px;"></div>
                        <table align="center" cellpadding="0" cellspacing="0" width="200px" class="rpt_table">
                        	<thead>
                            	<th width="100px">Date</th>
                                <th><input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();" style="width:100px"/></th>
                            </thead>
                            <tbody>
                            	<tr>
                                	<td><input type="text" name="txt_date" id="txt_date" value="" class="text_boxes" style="width:100px"/></td>
                                    <td><input type="submit" name="btn_search" id="btn_search" value="Show" class="formbutton" style="width:100px" /></td>	
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form> 
            </div>   
        </div>
        <fieldset>
        <div id="confirm_div" style="margin-top:5px;" align="center"></div>
        <div id="data_panel" align="left" style="margin-top:10px;"></div>
        </fieldset>
    </body>
</html>