<?php 
/*************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 08.12.2013
**************************************/ 
session_start();
include('../../../includes/common.php');
include('../../../includes/array_function.php');
extract($_REQUEST);

if ($_SESSION['logic_erp']["data_level_secured"]==1){
if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else{
$buyer_name="";
$company_name="";
}
$search_string=$_GET["m_company"];
$m_buyer=$_GET["m_buyer"];

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
    <script type="text/javascript" src="includes/ajax_submit_hrm.js"></script>
    <script type="text/javascript" src="../../../resources/jquery-1.6.2.js"></script>
    <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
    <script type="text/javascript" src="../../../resources/multiselect/multi_select.js"></script>
    <script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
    <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
            function func_set_training_record(str){
                $('#data').val(str);
                parent.emailwindow.hide();
            }
    </script>
	</head>
    <body><?php 
		if($action=="skill_details")
		{ 
		$exp_code=explode(",",$code);
		foreach($exp_code as $exp_codes){$code_no.="'".$exp_codes."',";}
		$emp_code=substr($code_no,0,-1);
		?>
            <div align="center">
                <form name="search_order_frm"  id="search_order_frm" autocomplete="off">
                    <fieldset style="width:930px">
                        <table class="rpt_table" width="900" cellspacing="2" cellpadding="0" border="0">
                            <thead>
                                <th>Sl</th>
                                <th>ID Card No</th>
                                <th>System Code</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Effective Date</th>
                                <th>Joinning Date</th><input type="hidden" name="data" id="data"/>
                            </thead>
                            <tbody><?php 	
                                /*$sql="SELECT emp_code,id_card_no,CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name,department_id,joining_date 
									FROM hrm_employee 
									WHERE emp_code in ($emp_code)";*/
								$sql="SELECT e.emp_code,e.id_card_no,CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS  name,e.department_id,e.joining_date,s.eff_date 
									FROM hrm_employee e,hrm_employee_skill s 
									WHERE e.emp_code in ($emp_code) and 
									e.emp_code=s.emp_code
									group by e.emp_code";
                                
								//echo $sql;die;
								$result=mysql_query($sql) or die(mysql_error());
                                $sl=1;
                                while($row=mysql_fetch_assoc($result))
								{
                                    if ($sl%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; ?>
									
                                    <tr bgcolor="<?php echo "$bgcolor"; ?>" style="cursor:pointer;">
                                        <td><?php echo $sl;?></td>
                                        <td><?php echo $row['id_card_no'];?></td>
                                        <td><?php echo $row['emp_code'];?></td>
                                        <td><?php echo $row['name'];?></td>
                                        <td><?php echo $department_details[$row['department_id']];?></td>
                                        <td><?php echo convert_to_mysql_date($row['eff_date']);?></td>
                                        <td><?php echo convert_to_mysql_date($row['joining_date']);?></td>
                                    </tr><?php $sl++;}?>
                             </tbody>
                        </table>
                        <div style="border:none" id="td_show_result"></div>
                        <table width="930">
                            <tfoot>
                                <th align="center" height="30" valign="bottom">
                                    <div style="width:100%"> 
                                    <div style="width:50%; float:left" align="left">
                                        <input type="hidden" readonly="readonly" name="txt_selected_id" id="txt_selected_id" /> 
                                        <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" />
                                        <input type="hidden" readonly="readonly" style="width:250px" class="text_boxes" name="txt_selected" id="txt_selected" />
                                    </div>
                                </th>
                            </tfoot>
                        </table>
                    </fieldset>
                </form>    
            </div><?php 
		}?>
    </body>
</html>