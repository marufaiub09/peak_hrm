<?php
 
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

 
//--------------------------------------------------------------------------------------------------------------------

include("../../../includes/common.php");
include('../../../includes/array_function.php');
	
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 order by trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
	
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
	$(document).ready(function(e) {
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
		//header: false, 
		selectedText: "# of # selected",
		});
	}); 
</script>    
<script type="text/javascript">
	
	function reset_field()
	{
		document.getElementById('data_panel').innerHTML="";
		document.getElementById('data_panel2').innerHTML="";
		document.getElementById('data_panel3').innerHTML="";
	}
	
	
$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		
function fnc_newly_join_summary()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#txt_from_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_from_date').focus();
			$(this).html('Please select From Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_to_date').val() == "" )
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_to_date').focus();
			$(this).html('Please select To Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( error == false ) {
		$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			data = '&txt_from_date=' + $('#txt_from_date').val() 
					+ '&txt_to_date=' + $('#txt_to_date').val()
					+'&cbo_emp_category=' + $('#cbo_emp_category').val() 
					+'&cbo_company_id=' + $('#cbo_company_id').val()
					+ '&location_id=' + $('#location_id').val()
					+ '&division_id=' + $('#division_id').val()
					+ '&department_id=' + $('#department_id').val() 
					+'&section_id=' + $('#section_id').val() 
					+'&subsection_id=' + $('#subsection_id').val()
					+ '&group_by_id=' + $('#group_by_id').val()
					+ '&order_by_id=' + $('#order_by_id').val();
		//alert(data);
		if (window.XMLHttpRequest)
		  {
			http=new XMLHttpRequest();
		  }
		else
		  {
			 http=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		nocache = Math.random();
		http.open( 'GET', 'includes/generate_designation_wise_newly_join_summary.php?action=newly_join_summary' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_wise_newly_join_summary;
		http.send(null);
	}
}

function response_wise_newly_join_summary()
{	
	if(http.readyState == 4)
	 {
		var response = http.responseText.split('####');
		 //alert(response[0]);		
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		 $("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}

function new_window()
{
var w = window.open("Surprise", "#");
var d = w.document.open();
d.write(document.getElementById('data_panel2').innerHTML);
d.close();
}
	
</script>
    
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:950px">
	<div class="form_caption">
        Designation Wise Newly Join Summary
     </div>
		<div align="center" id="messagebox"></div>
	</div>
  <form>
    <fieldset id="filter_panel" style="width:1400px">
		<legend>Newly Join Summary</legend>
        	 <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" align="center">
				<thead>
                	<tr> 
                    	<th align="center"><strong>Date</strong></th>
                        <th align="center"><strong>Category</strong></th>
                        <th align="center"><strong>Company</strong></th>
                        <th align="center"><strong>Location</strong></th>
                        <th align="center"><strong>Division</strong></th>
                        <th align="center"><strong>Department</strong></th>
                        <th align="center"><strong>Section</strong></th>
                        <th align="center"><strong>Sub Section</strong></th>
                        <th align="center"><strong>Group By</strong></th>
                        <th align="center"><strong>Order By</strong></th>
				</thead>

                <tr class="general">
                	 <td align="center">
						 <input type="text" name="txt_from_date" id="txt_from_date" value="" class="datepicker" style="width:60px" readonly="readonly" /> To
                    	 <input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" style="width:60px" readonly="readonly" />
 					</td> 
                    <td>
                         <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:120px">
                           <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
                    </td>
                    <td>
                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:130px">
                            <? if($company_cond=="")
                            { 
                            ?>
                            <option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
                            <option value="<?php echo $company['id']; ?>" <? if(count($company_details)==1)echo"selected"; ?> ><?php echo $company['company_name']; ?></option>
                            <?php } } ?>
                        </select>
                    </td>
                    <td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:130px">
                            <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
                            <option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
                            <?php } } ?>
                        </select>
                    </td>
                    <td align="center" id="division">
                    <select name="division_id" id="division_id" class="combo_boxes" style="width:110px" multiple="multiple">
                        <option value="0">-- All --</option>
                        <?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
                    <option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    <td align="center" id="department">
                    <select name="department_id" id="department_id" class="combo_boxes" style="width:110px" multiple="multiple">
                        <option value="0">-- All --</option>
                        <?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
                    <option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    
                    <td align="center" id="section">
                    <select name="section_id" id="section_id" class="combo_boxes" style="width:110px" multiple="multiple">
                    <option value="0">-- All --</option>
                        <?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
                    <option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:110px"  multiple="multiple">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>   
                    <td colspan="11" align="center">
                    	<input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="fnc_newly_join_summary();"  style="width:150px"/>
                        <input type="reset" name="rpo_search" id="rpo_search" value="Reset" class="formbutton" onClick="reset_field();" style="width:150px" />
                    </td>
                </tr>
			</table>
	</fieldset>
 </form>
</div>
	<div id="data_panel" align="center" style="margin-top:10px;">&nbsp;</div>
    <div id="data_panel2" align="left" style="margin-top:10px;">&nbsp;</div>
    <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden">&nbsp;</div>
</body>
</html>