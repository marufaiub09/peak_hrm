<?php
/**************************************
|	Developed by	: Md. Nuruzzaman
|	Date			: 06.01.2014
***************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond;
//-------------------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
include('../../../includes/array_function.php');
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content="Logic Payroll Software" />
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        
        <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script> 
        
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
         <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
     <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
                //header: false, 
                selectedText: "# of # selected",
                });
				
				$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
            });
        
			//Numeric Value allow field script
			function numbersonly(myfield, e, dec)
			{
				var key;
				var keychar;
			
				if (window.event)
					key = window.event.keyCode;
				else if (e)
					key = e.which;
				else
					return true;
				keychar = String.fromCharCode(key);
			
				// control keys
				if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
				return true;
				
				// numbers
				else if ((("0123456789.,-").indexOf(keychar) > -1))
					return true;
				else
					return false;
			}	
				
			function openmypage_employee_info()
			{			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id");				
					$('#emp_code').val(thee_id.value);
				}
			}
			function openmypage_employee_info_id_card()
			{			
				//'../../search_employee_multiple_by_id.php','Employee Information'
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
					$('#id_card').val(thee_id.value);
				}
			}	
			function reset_field()
			{
				document.getElementById('data_panel').innerHTML="";
				document.getElementById('data_panel2').innerHTML="";
			}
        </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="left" style="width:100%">
            <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>    
            <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee Wise Weekend</h3>
            <div id="content_search_panel" > 
                <form autocomplete="off">
                    <fieldset>
                        <table align="center" cellspacing="0" cellpadding="0" width="1120px" class="rpt_table" >
                            <thead>
                                <th align="center"><strong>Category</strong></th>
                                <th align="center"><strong>Company</strong></th>
                                <th align="center"><strong>Location</strong></th>
                                <th align="center"><strong>Division</strong></th>
                                <th align="center"><strong>Department</strong></th>
                                <th align="center"><strong>Section</strong></th>
                                <th align="center"><strong>Sub Section</strong></th>
                                <th align="center"><strong>Designation</strong></th>
                            </thead>
                            <tbody>
                                <tr class="general">
                                    <td>
                                        <select name="category" id="category" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <? foreach($employee_category as $key=>$val) {?>
                                            <option value="<? echo $key; ?>"><? echo $val; ?></option><? }?> 
                                        </select>
                                    </td>
                                    <td>
                                        <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                                            <? if($company_cond==""){ ?>
                                            <option value="0">-- Select --</option>
                                            <?php } foreach( $company_details AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                                          <option value="0">-- Select --</option>
                                            <?php foreach( $location_details AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="division_id" id="division_id" style="width:140px" multiple="multiple" >
                                            <?php foreach( $division_details AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>        
                                    <td>
                                        <select name="department_id" id="department_id" style="width:140px" multiple="multiple" >
                                            <?php foreach( $department_details AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="section_id" id="section_id" style="width:140px" multiple="multiple" >
                                            <?php foreach( $section_details AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="subsection_id" id="subsection_id" style="width:140px" multiple="multiple" >
                                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                    <td id="designation">
                                        <select name="designation_id" id="designation_id" style="width:140px;" multiple="multiple" >
                                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                </tr>
                          </tbody>
                          </table>
                          <div style="height:8px;"></div>
                           <table align="center" cellspacing="0" cellpadding="0" width="600" class="rpt_table" >
                            <thead>
                                <th align="center"><strong>Group By</strong></th>
                                <th align="center"><strong>Order By</strong></th>
                                <th align="center"><strong>ID Card No</strong></th>
                                <th align="center"><strong>System Code</strong></th>
                                <th align="center"><input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();"  style="width:100px"/></th>
                            </thead>
                            <tbody> 
                            <tr class="general">         
                                    <td>
                                        <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>
                                    <td>
                                        <input type="text" name="emp_code" id="emp_code" value="" class="text_boxes" size="40" style="width:140px"  onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" />
                                    </td>  
                                    <td align="center">
                                        <input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="weekend_emp_populate();"  style="width:100px"/>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
            <fieldset>
                <div id="data_panel" style="margin-top:5px;" align="center"></div>
                <div id="data_panel2" align="left" style="margin-top:10px;"></div>
            </fieldset>
        </div>
    </body>
</html>