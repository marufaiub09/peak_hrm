<?php
/*************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 08.12.2013
**************************************/ 
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//---------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
include('../../../includes/array_function.php');
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_training WHERE status=1 and is_deleted = 0 and status_active=1 ORDER BY training_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$training_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$training_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$training_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
$months_training = array('01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        <script type="text/javascript" src="includes/ajax_submit_hr_admin.js"></script>
        <script type="text/javascript" src="../../../resources/jquery-1.6.2.js"></script>
        <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
        <script type="text/javascript" src="../../../resources/multiselect/multi_select.js"></script>
        
        <link type="text/css" href="../../../css/style_common.css" rel="stylesheet" media="screen" />
        <link type="text/css" href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" media="screen" />
        <link type="text/css" href="../../../css/popup_window.css" rel="stylesheet" />
        <link type="text/css" href="../../../includes/filtergrid.css" rel="stylesheet" /> 
        <link type="text/css" href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" />
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
     <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
		<script type="text/javascript" charset="utf-8">
            $(document).ready(function(e) {
              $("#division_id,#department_id,#section_id,#subsection_id,#designation_id,#training_id").multiselect({
                selectedText: "# of # selected",
                });
				$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
             });
			  
			$(document).ready(function() {
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
				year_change();
			});
			
			function openmypage_employee_info()
			{			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id");				
					$('#emp_code').val(thee_id.value);
				}
			}	
			function openmypage_employee_info_id_card()
			{			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
					$('#id_card').val(thee_id.value);
				}
			}
				
			function reset_field_training()
			{
				document.getElementById('data_panel').innerHTML="";
				document.getElementById('data_panel2').innerHTML="";
			}
			
			//ajax submit starts here
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			var http = createObject();
			var field_array = new Array();
			
			function training_record_populate()
			{
				var error = false, data = '';
				if( $('#company_id').val() == 0 ) {
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#company_id').focus();
						$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				if( error == false ) {
					//alert($('#training_id').val());
					$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
					data = 	'&month_selector	=' + $('#month_selector').val()+ 
							'&year_selector		=' + $('#year_selector').val()+ 
							'&from_date			=' + $('#from_date').val()+ 
							'&to_date			=' + $('#to_date').val()+ 
							'&category			=' + $('#category').val()+ 
							'&company_id		=' + $('#company_id').val()+
							'&location_id		=' + $('#location_id').val()+
							'&division_id		=' + $('#division_id').val()+ 
							'&department_id		=' + $('#department_id').val()+ 
							'&section_id		=' + $('#section_id').val()+ 
							'&subsection_id		=' + $('#subsection_id').val()+
							'&designation_id	=' + $('#designation_id').val()+
							'&cbo_salary_based	=' + $('#cbo_salary_based').val()+ 
							'&group_by_id		=' +$('#group_by_id').val()+ 
							'&order_by_id		=' +$('#order_by_id').val()+
							'&id_card			=' + $('#id_card').val()+
							'&emp_code			=' + $('#emp_code').val()+
							'&training_id		=' + $('#training_id').val();
							
					nocache = Math.random();
					http.open( 'GET', 'includes/generate_training_record_report.php?type=training_record_report' + data + '&nocache=' + nocache );
					http.onreadystatechange = response_training_record_report;
					http.send(null); 
				}
			}
			
			function response_training_record_report() {	
				if(http.readyState == 4) {
					var response = http.responseText.split('####');
					 //alert(response[0]);		
					$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
					$('#data_panel').append( '<input type="button" onclick="new_window_short()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel2').html( response[0] );
					var tableFilters ={col_0: "none",}
					setFilterGrid("table_body",-1,tableFilters);
					hide_search_panel();
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				//document.getElementById('scroll_body_topheader').innerHTML=document.getElementById('to_summery_hidden').innerHTML;
				//document.getElementById('to_summery_hidden').innerHTML="";
			}
			
			//Numeric Value allow field script
			function numbersonly(myfield, e, dec)
			{
				var key;
				var keychar;
				if (window.event)
					key = window.event.keyCode;
				else if (e) key = e.which;
				else return true; keychar = String.fromCharCode(key);
				// control keys
				if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
				return true;
				// numbers
				else if ((("0123456789.,-").indexOf(keychar) > -1)) return true;
				else return false;
			}
			
			function show_training_record(str)
			{
				//alert(str);
				//page_link='requires/sub_contract_order_entry_controller.php?action=job_popup&data='+data
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','search_training_record_report.php?action=training_record_report&str='+str,'Compliance Training Record Report','width=1050px,height=400px,center=1,resize=0,scrolling=0','../../')
				emailwindow.onclose=function()
				{
					//alert(str);
					//var thee_loc = this.contentDoc.getElementById("txt_selected");
					//var thee_id = this.contentDoc.getElementById("txt_selected_id");				
					//$('#emp_code').val(thee_id.value);
				}
			}
			
			function show_training_record_by_trnm(tid,emp)
			{
				//alert(tid+'='+emp);
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','search_training_record_report.php?action=training_record_report_by_trnm&tid='+tid+'&emp='+emp,'Compliance Training Record Report','width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')
				emailwindow.onclose=function()
				{
					//alert(str);
					//var thee_loc = this.contentDoc.getElementById("txt_selected");
					//var thee_id = this.contentDoc.getElementById("txt_selected_id");				
					//$('#emp_code').val(thee_id.value);
				}
			}
			
			function month_change(str)
			{
				//alert(str);
				var year=$('#year_selector').val();
				var fdate='01'+'-'+str+'-'+year;
				$('#from_date').val(fdate);
				
				var ld=32 - new Date(year, str-1, 32).getDate();
				//alert(ld);
				var ldate=ld+'-'+str+'-'+year;
				$('#to_date').val(ldate);
			}
			
			function year_change()
			{
				var year=$('#year_selector').val();
				$('#month_selector').val(0);
				var fdate='01'+'-'+'01'+'-'+year;
				$('#from_date').val(fdate);
				
				var ldate='31'+'-'+'12'+'-'+year;
				$('#to_date').val(ldate);
			}
		</script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%">
            <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
            <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Compliance Training Record Report</h3>
            <div id="content_search_panel" > 
            <form autocomplete="off" id="traning_record">
                <fieldset>
                    <table align="center" cellspacing="0" cellpadding="0" width="1120px" class="rpt_table" >
                        <thead>
                            <th align="center"><strong>Category</strong></th>
                            <th align="center"><strong>Company</strong></th>
                            <th align="center"><strong>Location</strong></th>
                            <th align="center"><strong>Division</strong></th>
                            <th align="center"><strong>Department</strong></th>
                            <th align="center"><strong>Section</strong></th>
                            <th align="center"><strong>Sub Section</strong></th>
                            <th align="center"><strong>Designation</strong></th>
                        </thead>
                        <tbody>
                            <tr class="general">
                                <td align="center">
                                    <select name="category" id="category" class="combo_boxes" style="width:140px">
                                        <option value="">All Category</option>
                                        <? foreach($employee_category as $key=>$val){?>
                                        <option value="<? echo $key; ?>"><? echo $val; ?></option><? }?> 
                                    </select>
                                </td>
                                <td align="center" id="company">
                                    <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                                    <? if($company_cond==""){ ?>
                                        <option value="0">-- All --</option>
                                        <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $company['id'];?>"<? if(count($company_details)==1)echo"selected";?>><?php echo $company['company_name'];?></option><?php }}?>
                                    </select>
                                </td>
                                <td align="center" id="location">
                                    <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                                        <option value="0">-- All --</option>
                                        <?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option><?php } } ?>
                                    </select>
                                </td>
                                <td align="center" id="division">
                                    <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option><?php } } ?>
                                    </select>
                                </td>
                                <td align="center" id="department">
                                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option><?php } } ?>
                                    </select>
                                </td>
                                <td align="center" id="section">
                                    <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option><?php } } ?>
                                    </select>
                                </td>
                                <td align="center" id="subsection">
                                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option><?php } } ?>
                                    </select>
                                </td>
                                <td align="center" id="designation">
                                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option><?php } } ?>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="height:8px;"></div>
                    <table align="center" cellspacing="0" cellpadding="0" width="900" class="rpt_table" >
                        <thead>
                            <th align="center"><strong>Name of Training</strong></th>
                            <th align="center"><strong>Year</strong></th>
                            <th align="center"><strong>Month</strong></th>
                            <th align="center" colspan="2"><strong>Date</strong></th>
                            <th align="center"><strong>Salary Based</strong></th>
                            <th align="center"><strong>Group By</strong></th>
                            <th align="center"><strong>Order By</strong></th>
                            <th align="center"><strong>ID Card No</strong></th>
                            <th align="center"><strong>System Code</strong></th>
                        </thead>
                        <tbody>
                            <tr>
                                 <td align="center" id="division">
                                    <select name="training_id" id="training_id" class="combo_boxes" style="width:110px" multiple="multiple">
                                        <option value="0">-- All --</option>
                                        <?php foreach( $training_details AS $training ) { if( $training['status_active'] == 1 ) { ?>
                                        <option value="<?php echo $training['id']; ?>"><?php echo $training['training_name']; ?></option><?php } } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="year_selector" id="year_selector" class="combo_boxes" onchange="year_change();" style="width:80px" > 
                                        <option value="0">-- Select --</option>
                                        <?php $c_year=date("Y");$s_year=$c_year-25;$e_year=$c_year+10;
                                        for ($year = $s_year; $year <= $e_year; $year++) { ?>
                                        <option value=<?php echo $year;if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option><?php }?>
                                    </select>
                                </td>
                                <td>
                                    <select name="month_selector" id="month_selector" class="combo_boxes" onchange="month_change(this.value);" style="width:80px">
                                        <option value="0">--select--</option>
                                        <?php foreach($months_training as $id=>$value){ ?>
                                        <option value="<?php echo $id;?>"><?php echo $value;?></option> <?php } ?>  
                                    </select>
                                </td>
                                <td align="center">
                                    <input type="text" name="from_date" id="from_date" value="" class="datepicker" style="width:70px" readonly="readonly" placeholder="from" />
                                </td>
                                <td>
                                    <input type="text" name="to_date" id="to_date" value="" class="datepicker" style="width:70px" readonly="readonly" placeholder="to" />
                                </td> 
                                <td>
                                    <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:100px" >							
                                       <option value="">-- Select --</option>
                                        <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                     </select>
                                </td>
                                <td>
                                    <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $groupby_arr AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $orderby_arr AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                </td>
                                <td align="center">
                                    <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                    <div style="padding-top:10px;">
                        <input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="training_record_populate();"  style="width:100px"/>
                        <input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field_training();"  style="width:100px"/>
                    </div>
                </fieldset>
            </form>
        </div>
        <fieldset>
            <div id="data_panel" style="margin-top:5px;" align="center"></div>
            <div id="data_panel2" align="left" style="margin-top:10px;"></div>
        </fieldset>
        </div>
    </body>
</html>