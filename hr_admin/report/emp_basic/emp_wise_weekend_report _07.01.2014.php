<?php
/**************************************
|	Developed by	: Md. Nuruzzaman
|	Date			: 06.01.2014
***************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond;
//--------------------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
include('../../../includes/array_function.php');
/*//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY level ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}*/


$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script> 
    
    
   
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
    
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
	 
      <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
    
     <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
	$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
	});
 });
	
//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}	
	
function openmypage_employee_info(page_link,title)
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}
function openmypage_employee_info_id_card(page_link,title)
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
			$('#id_card').val(thee_id.value);
		}
	}	
	function reset_field()
	{
		document.getElementById('data_panel').innerHTML="";
		document.getElementById('data_panel2').innerHTML="";
	}
	

function append_report_checkbox(tid,is_resize)
{
	var i=0;
	$("#"+tid+" thead th").each(function() {
		$(this).addClass('res'+i)
		$(this).prepend('<input type="checkbox" id="'+tid+"_"+i+'" name="rept_check_box[]" class="rpt_check" onclick="report_check_box_click(this.id)" value="1" checked="checked" />');
		i++
    });
	
	$("#scroll_body table tr").each(function() {
		j=0;
		$(this).find("td").each(function(){
			$(this).addClass('res'+j)
			j++;	
		});
    });
	$("#report_table_footer tr").each(function() {
		j=0;
		$(this).find("th").each(function(){
			$(this).addClass('res'+j)
			j++;	
		});
    });
	 if (is_resize==1)
	 {
		 table_column_resize(tid);
	 }
}

function report_check_box_click(id)
{
	if ($('#'+id).is(":checked")==false)
	{
		$('#'+id).removeAttr('checked');
		$('#'+id).removeClass("rpt_check");
	}
	else
	{
		$('#'+id).attr('checked','checked');
		$('#'+id).addClass('rpt_check');
	}
}

function table_column_resize(tble_id)
{
	  
	 $(function(){
        var pressed = false;
        var start = undefined;
        var startX, startWidth;
		var classid;
        $("#"+tble_id+" th").mousedown(function(e) {

            start = $(this);
            classid=$(start).attr('class');
            pressed = true;
            startX = e.pageX;
            startWidth = $(this).width();
            startX1 = e.pageX;
        });

        $(document).mousemove(function(e) {
            if(pressed) {
                $('.'+classid).width(startWidth+(e.pageX-startX));
            }
        });

        $(document).mouseup(function() {
            if(pressed) {
                pressed = false;
            }
        });
    });
}

function report_convert_button(url)
{
	return '<input onclick="print_priview_html( \'data_panel2\', \'scroll_body\',\'table_header_1\',\'report_table_footer\', 1, \'0\',\''+url+'\' )" type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/>&nbsp;&nbsp;<input type="button" onclick="print_priview_html( \'data_panel2\', \'scroll_body\',\'table_header_1\',\'report_table_footer\', 3, \'0\',\''+url+'\' )" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>';
}



function print_priview_html( report_div, scroll_div, header_table, footer_table, report_type, link_pos, rel_path,extra_func )  //type: 1=xls, 2=pdf, 3=html
{
	var filter=0;
	 
	if (report_type==3)
	{
		var mxheght= document.getElementById(scroll_div).style.maxHeight;
		var total_width=0;
		var top_wd=$("#"+header_table).width();
		var botom_wd=$("#"+scroll_div+ " table").width();
		var top_wd_new=0;
		var botom_wd_new=0;
			var i=0;
			$("#"+header_table+" thead th").each(function() {
				var wd=($(this).width());
				if (!$('#'+header_table+"_"+i).hasClass('rpt_check'))
				{
				  total_width=(total_width*1)+(wd*1);
					$("#"+header_table).find("tr").each(function(){
						$(this).find("th:eq("+i+")").hide();
					   
					});
					$("#"+scroll_div+" table").find("tr").each(function(){
						$(this).find("td:eq("+i+")").hide();
					});
					$("#"+footer_table).find("tfoot tr").each(function(){
						$(this).find("th:eq("+i+")").hide();
					   
					});
					
				}
				i++;
			});
			top_wd_new=top_wd-total_width;
			botom_wd_new=botom_wd-total_width;
			
			 if ($("#"+scroll_div +" table tr:first").attr('class')=='fltrow') 
			 {
				filter=1;
				$("#"+scroll_div +" table tr:first").hide();	 
			 }
			 $("#"+header_table).width(top_wd_new);
			 $("#"+scroll_div+ " table").width(botom_wd_new);
			  
			 document.getElementById(scroll_div).style.overflow="auto";
			 document.getElementById(scroll_div).style.maxHeight="none";
			  
			var html = document.getElementById(report_div).innerHTML;
			 
			 /*var tto1=($(html).find('a').replaceWith(function() {
				return this.innerHTML;
			}).end().html());
			alert(tto1);*/
			 var tto=($(html).find('input').replaceWith(function() {
				return this.innerHTML;
			}).end().html());  			
			 alert(tto);
			var w = window.open("Surprise", "#");
			var d = w.document.open();
			var btn='<input type="button" onclick="javascript:window.print()" value="  Print  " name="Print" class="formbutton" style="width:100px"/><br><br>';
			 
			d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
			'<html><head><link rel="stylesheet" href="'+rel_path+'css/style_common.css" type="text/css" media="print" /><style type="text/css">p{word-break:break-all;word-wrap: break-word;width:100%;}</style><title></title></head><body><div id="report_on_popup">'+btn+tto+'</div></body</html>');
			d.close();
			 
			document.getElementById(scroll_div).style.overflowY="scroll";
	 		document.getElementById(scroll_div).style.maxHeight=mxheght;
			if ( filter==1)
			{
				$("#"+scroll_div +" table tr:first").show();
			}
			i=0;
			$("#"+header_table+" thead th").each(function() {
				if (!$('#th_'+i).hasClass('check'))
				{
				  	$("#"+header_table).find("tr").each(function(){
						$(this).find("th:eq("+i+")").show(); 
					});
					$("#"+scroll_div+" table").find("tr").each(function(){
						$(this).find("td:eq("+i+")").show();
					});
					
					$("#"+footer_table).find("tfoot tr").each(function(){
						$(this).find("th:eq("+i+")").show();
					   
					});
				}
				i++;
			});
			 $("#"+header_table).width(top_wd);
			 $("#"+scroll_div+ " table").width(botom_wd);
		 
	}
	else if ( report_type==1  || report_type==2)
	{
		var original_html = document.getElementById(report_div).innerHTML;
		var mxheght= document.getElementById(scroll_div).style.maxHeight;
		var total_width=0;
		var top_wd=$("#"+header_table).width();
		var botom_wd=$("#"+scroll_div+ " table").width();
		var top_wd_new=0;
		var botom_wd_new=0;
			var k=0;
			var idd;
			$("#"+header_table+" thead th").each(function() {
				var wd=($(this).width());
				
				if (!$('#'+header_table+"_"+k).hasClass('rpt_check'))
				{
					 //alert(k+"-"+$('#'+header_table+"_"+k).hasClass('rpt_check'))
				  	total_width=(total_width*1)+(wd*1);
					$("#"+header_table).find("tr").each(function(){
						$(this).find("th:eq("+k+")").addClass('out_of_report') ;
					});
					$("#"+scroll_div+" table").find("tr").each(function(){
						$(this).find("td:eq("+k+")").addClass('out_of_report') ;
					});
					$("#"+footer_table).find("tfoot tr").each(function(){
						$(this).find("th:eq("+k+")").addClass('out_of_report');
					});
				}
				k++;
			});
			$(".out_of_report").remove();
			$(".rpt_check").remove();
			  
			top_wd_new=top_wd-total_width;
			botom_wd_new=botom_wd-total_width;
			
			 if ($("#"+scroll_div +" table tr:first").attr('class')=='fltrow') 
			 {
				filter=1;
				$("#"+scroll_div +" table tr:first").remove();	 
			 }
			 
			 $("#"+header_table).width(top_wd_new);
			 $("#"+scroll_div+ " table").width(botom_wd_new);
			  
			 document.getElementById(scroll_div).style.overflow="auto";
			 document.getElementById(scroll_div).style.maxHeight="none";
			  
			var html = document.getElementById(report_div).innerHTML;
			 var tto=($(html).find('a').replaceWith(function() {
				return this.innerHTML;
			}).end().html());
			  alert(tto);
			 
			 $.post(rel_path+"includes/common_functions_for_js.php",
			  { path: rel_path, action: "generate_report_file", htm_doc: tto },
			  function(data){
				window.open(rel_path+data, "#");
			  }
			);
			 document.getElementById(report_div).innerHTML="" ;
			 document.getElementById(report_div).innerHTML=original_html ;
			  if ($("#"+scroll_div +" table tr:first").attr('class')=='fltrow') 
			 {
				$("#"+scroll_div +" table tr:first").remove();	 
			 }
			 if (!tableFilters) var tableFilters="";
			 setFilterGrid('table_body',-1,tableFilters);
			   
	}
	 
}	
/*function weekend_emp_populate()
{
	alert("jhhh");
}*/
	</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:1100px">
    <div>
        <div class="form_caption">Employee Wise Weekend</div>
		<div align="center" id="messagebox"></div>
	</div>
    <form autocomplete="off">
		<fieldset id="filter_panel" style="width:100%"><legend>Search Panel</legend>
			<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
				<thead>
                	<th align="center"><strong>Category</strong></th>
					<th align="center"><strong>Company</strong></th>
					<th align="center"><strong>Location</strong></th>
                    <th align="center"><strong>Division</strong></th>
					<th align="center"><strong>Department</strong></th>
                    <th align="center"><strong>Section</strong></th>
                    <th align="center"><strong>Sub Section</strong></th>
					<th align="center"><strong>Designation</strong></th>
                    <th align="center"><strong>Salary Based</strong></th>
                    <th align="center"><strong>Group By</strong></th>
                    <th align="center"><strong>Order By</strong></th>
                    <th align="center"><strong>ID Card No</strong></th>
                    <th align="center"><strong>System Code</strong></th>
				</thead>
                <tr class="general">
                	<td>
                        <select name="category" id="category" class="combo_boxes" style="width:110px">
                            <option value="">All Category</option>
                            <? foreach($employee_category as $key=>$val) {?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option><? }?> 
                    	</select>
					</td>
                    <td>
                    <select name="company_id" id="company_id" class="combo_boxes" style="width:120px">
                        <? if($company_cond==""){ ?>
                        <option value="0">-- Select --</option>
                        <?php } foreach( $company_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option><?php } ?>
                    </select>
                </td>
                <td>
                    <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                      <option value="0">-- Select --</option>
                        <?php foreach( $location_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                  </select>
                </td>
                <td>
                	<select name="division_id" id="division_id" style="width:110px" multiple="multiple" >
                             <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                </td>        
                <td>
                    <select name="department_id" id="department_id" style="width:120px" multiple="multiple" >
                         <?php foreach( $department_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="section_id" id="section_id" style="width:120px" multiple="multiple" >
                         <?php foreach( $section_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td id="subsection">
                    <select name="subsection_id" id="subsection_id" style="width:120px" multiple="multiple" >
                         <?php foreach( $subsection_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                    </td>
                    <td id="designation">
                     <select name="designation_id" id="designation_id" style="width:120px;" multiple="multiple" >
						 <?php foreach( $designation_chart AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                    </td> 
                    <td>
                    	<select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:100px" >							
                           <option value="">-- Select --</option>
                            <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card('../../search_employee_multiple_by_id.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" />
                    </td>
                    <td><input type="text" name="emp_code" id="emp_code" value="" class="text_boxes" size="40" style="width:140px"  onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info('../../search_employee_multiple.php','Employee Information'); return false" placeholder="Double Click For Search" /></td>  
                </tr>
                <tr>
                    <td colspan="12" align="center">
                    	<input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="weekend_emp_populate();"  style="width:150px"/>
                        <input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();"  style="width:150px"/>
                    </td>
                </tr>
			</table>
		</fieldset>
      </form>
    <fieldset style="width:100%;">
        <div id="data_panel" style="margin-top:5px;" align="center"></div>
        <div id="data_panel2" align="left" style="margin-top:10px;"></div>
    </fieldset>
</div>
</body>
</html>

<?php 
// generate_report
//weekend_emp_report
if($type=="weekend_emp_report")
	{
		//echo $emp_code."emp";die;
		$exp_id_card=explode(",",$id_card);
		foreach($exp_id_card as $card){$card_no.="'".$card."',";}
		$id_card_no=substr($card_no,0,-1);
		//echo $id_card_no;die;
		if ($category==0) $category=""; else $category="and e.category='$category'";
		if ($company_id=='') $company_id =""; else $company_id ="and e.company_id='$company_id'";
		if ($location_id==0) $location_id=""; else $location_id="and e.location_id='$location_id'";
		if ($division_id==0) $division_id=""; else $division_id="and e.division_id in($division_id)";
		if ($department_id==0) $department_id=""; else $department_id="and e.department_id in($department_id)";
		if ($section_id==0) $section_id=""; else $section_id="and e.section_id in($section_id)";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and e.subsection_id in($subsection_id)";
		if ($designation_id==0) $designation_id=""; else $designation_id="and e.designation_id in($designation_id)";
		if ($emp_code=="") $emp_code=""; else $emp_code="and w.emp_code in ($emp_code)";
		if ($id_card=="") $id_card_no=""; else $id_card_no="and e.id_card_no in ($id_card_no)";
		if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and e.salary_type_entitled='$cbo_salary_based'";
		

	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
 		
	/*
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(trim(id_card_no) as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(trim(id_card_no),5)";}*/
	//echo $orderby;die;					
	
	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(trim(id_card_no) as SIGNED)";}			 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." right(trim(id_card_no),5)";}	 
	//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
	
$dynamic_groupby = substr(e.'.'.$groupby, 0, -1);//this line is used to remove last comma and dynamic group by	

//$table_width=count($salary_head_mst_id)*90;
 //echo $table_width;die; 
 //echo $table_width+975; 
		$sql = "SELECT w.emp_code,w.weekend,e.emp_code,e.id_card_no,e.designation_id,e.joining_date, CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS  name
				FROM hrm_weekend w left join hrm_employee e on w.emp_code=e.emp_code
				WHERE 
				e.is_deleted=0 and 
				e.status_active=1 
				$category
				$company_id 
				$location_id 
				$division_id 
				$department_id 
				$section_id 
				$subsection_id
				$designation_id	
				$salary_based
				$id_card_no
				$emp_code
				group by $dynamic_groupby $orderby";			
				//order by CAST(id_card_no AS SIGNED) ASC";
				//echo $sql;
		$result=mysql_query($sql);
		$excl_html="";
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{ 
		$excl_html='<div style="width:1200px; max-height:280px" align="left">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1200px" class="rpt_table" id="table_header_1" >
            <thead>
                <th width="35" align="center"><strong>SL</strong></th>
                <th width="80" align="center"><strong>ID Card No</strong></th> 
                <th width="80" align="center"><strong>System Code</strong></th>
                <th width="130" align="center"><strong>Emp Name</strong></th>
                <th width="100" align="center"><strong>DOJ</strong></th>
                <th width="120" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Weekend</strong></th>
				<th width="120" align="center"><strong>Remarks</strong></th>
	   		</thead>
       </table>
</div> 
    <div style=" width:1200px; overflow-y:scroll; max-height:280px" id="scroll_body" align="left">
   <table cellspacing="0" cellpadding="0" width="width:1200px;" border="1" rules="all" class="rpt_table"  id="">';
		?>
		
    <div style=" width:1120px; max-height:280px" align="left">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1100px" class="rpt_table" id="table_header_1" >
            <thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="60" align="center"><strong>ID Card No</strong></th>
                <th width="60" align="center"><strong>System Code</strong></th>
                <th width="110" align="center"><strong>Emp Name</strong></th>
                <th width="90" align="center"><strong>Designation</strong></th>
                <th width="70" align="center"><strong>DOJ</strong></th>
                <th width="70" align="center"><strong>Weekend</strong></th>
                <th width="90" align="center"><strong>Remarks</strong></th>
	   		</thead>
       </table>
</div> 
    <div style="width:1120px; overflow-y:scroll; max-height:280px" id="scroll_body" align="left">
   <table cellspacing="0" cellpadding="0" width="1100px" border="1" rules="all" class="rpt_table"  id="">			 
	  <?php                  
		$i=1; $gross==0;
		$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
			
while($emp=mysql_fetch_array($result))
		{
			$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;$not_sub_tot=0;
				//if(in_array($row[company_id],$company_arr))
				//{
					if(in_array($emp[location_id],$location_arr))
					{
							if(in_array($emp[division_id],$division_arr))
							{
									if(in_array($emp[department_id],$department_arr))
									{
										if(in_array($emp[section_id],$section_arr))
										{
											if(in_array($emp[subsection_id],$subsection_arr))
											{}
											else
											{
												$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
												$new_subsec=1;
											}
										}
										else
										{
											$section_arr[$emp[section_id]]=$emp[section_id];
											$subsection_arr=array();
											$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else
									{
										$department_arr[$emp[department_id]]=$emp[department_id];
										$section_arr=array();
										$subsection_arr=array();
										$section_arr[$emp[section_id]]=$emp[section_id];
										$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else
							{
									$division_arr[$emp[division_id]]=$emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$emp[department_id]]=$emp[department_id];
									$section_arr[$emp[section_id]]=$emp[section_id];
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else
					{
						$location_arr[$emp[location_id]]=$emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
			/*	
				}//company
			else
			{
				$company_arr[$row[company_id]]=$row[company_id];
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
				$new_com=1;
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//company else
			*/	 
 				//header print here 
				if($new_com==1 && $status_com==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Company : <? echo $company_details[$emp[company_id]]; ?></th></tr><?
				}
				if($new_loc==1 && $status_loc==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$emp[location_id]]; ?></th></tr><?
				}				
				if($new_divis==1 && $status_divis==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$emp[division_id]]; ?></th></tr><?
				}
 				if($new_dept==1 && $status_dept==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Department : <? echo $department_details[$emp[department_id]]; ?></th></tr><?
					
				}
				if($new_sec==1 && $status_sec==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Section : <? echo $section_details[$emp[section_id]]; ?></th></tr><?
				}
				if($new_subsec==1 && $status_subsec==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Sub Section : <? echo $subsection_details[$emp[subsection_id]]; ?></th></tr><?
				}
				
				ob_start();
				if($new_com==1 && $status_com==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Company : <? echo $company_details[$emp[company_id]]; ?></th></tr><?
				}
				if($new_loc==1 && $status_loc==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$emp[location_id]]; ?></th></tr><?
				}				
				if($new_divis==1 && $status_divis==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$emp[division_id]]; ?></th></tr><?
				}
 				if($new_dept==1 && $status_dept==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Department : <? echo $department_details[$emp[department_id]]; ?></th></tr><?
					
				}
				if($new_sec==1 && $status_sec==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Section : <? echo $section_details[$emp[section_id]]; ?></th></tr><?
				}
				if($new_subsec==1 && $status_subsec==1)
				{
					?><tr><th colspan="<? echo count($salary_head_mst_id)+12; ?>" align="left" bgcolor="#CCCCCC">Sub Section : <? echo $subsection_details[$emp[subsection_id]]; ?></th></tr><?
				}
			$tmp_htm=ob_get_contents();
			ob_clean();
			$excl_html .=$tmp_htm;
			$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0 order by id desc limit 1");
			$image="";
			if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
			$row4=return_field_value("increment_amount","hrm_increment_mst","emp_code='$emp[emp_code]' and status_active=1 and is_deleted=0  order by id desc limit 0,1");
			/*$sql= "SELECT
				sum(CASE WHEN payroll_head=1 THEN amount END) AS 'basic_salary',
				sum(CASE WHEN payroll_head=2 THEN amount END) AS 'house_rent',
				sum(CASE WHEN payroll_head=3 THEN amount END) AS 'medical_allowance',
				sum(CASE WHEN payroll_head=4 THEN amount END) AS 'conveyance_allowance',
				sum(CASE WHEN payroll_head=19 THEN amount END) AS 'others_allowance'
				from hrm_employee_salary where emp_code='$emp[emp_code]' group by emp_code";
			$selectResult=mysql_query($sql);
			$row=mysql_fetch_array($selectResult);
			
			$gross=$row['basic_salary']+$row['house_rent']+$row['medical_allowance']+$row['conveyance_allowance']+$row['others_allowance'];
			$gross=number_format($gross,2,'.','');*/
			
			/*if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else*/
			if ($i%2==0) $bgcolor="#EEEEEE"; 
			else $bgcolor="#FFFFFF";
			
			$excl_html .='<tr bgcolor="'.$bgcolor.'">
				<td width="35" align="left"><div style="word-wrap:break-word; width:35px">'.$i.'</div></td>
                <td width="80" align="left"><div style="word-wrap:break-word; width:80px">'.$emp['id_card_no'].'</div></td>
				<td width="80" align="left"><div style="word-wrap:break-word; width:80px">'.$emp['emp_code'].'</div></td>
				<td width="130" align="left"><div style="word-wrap:break-word; width:130px">'.$emp['name'].'</div></td>
				<td width="120" align="left"><div style="word-wrap:break-word; width:120px">'.$designation_chart[$emp['designation_id']].'</div></td>
                <td width="100" align="center"><div style="word-wrap:break-word; width:100px">'.convert_to_mysql_date($emp['joining_date']).'</div></td>
                <td width="80"><div style="word-wrap:break-word; width:80px">'.$emp['weekend'].'</td>
				<td width="120"><div style="word-wrap:break-word; width:120px"></td>';
	?>
  <!-- <table cellspacing="0" cellpadding="0" width="<? //echo $table_width+1150; ?>" border="1" rules="all" class="rpt_table"  id="table_body">-->	         
            <tr bgcolor="<? echo $bgcolor; ?>">
				<td width="30" align="left"><div style="word-wrap:break-word; width:30px"><?php echo $i;?></div></td>
                <td width="60" align="left"><div style="word-wrap:break-word; width:60px"><?php echo $emp['id_card_no'];?></div></td>
				<td width="60" align="left"><div style="word-wrap:break-word; width:60px"><?php echo $emp['emp_code'] ; ?></div></td>
				<td width="110" align="left"><div style="word-wrap:break-word; width:110px"><?php echo $emp['name'] ; ?></div></td>
                <td width="90" align="left"><div style="word-wrap:break-word; width:90px"><?php echo $designation_chart[$emp['designation_id']]; ?></div></td>                
                <td width="70" align="center"><div style="word-wrap:break-word; width:70px"><? echo convert_to_mysql_date($emp['joining_date']) ; ?></div></td>
                <td width="70"><div style="word-wrap:break-word; width:70px"><?php echo $emp['weekend']; ?></td>				
				<td width="90" align="left"><div style="word-wrap:break-word; width:90px">&nbsp;</div></td>
			</tr> 
			<?php 
			$i++;
		}
} // end else
$excl_html .=' </table>
    
</div>';
			?>
       </table>
</div>
		<?php
			/*foreach (glob(""."*.pdf") as $filename) {			
                @unlink($filename);
			}
			echo "$html####$filename";
			exit();
	} //end Salary Breakdown if condition*/
	//for report temp file delete 
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$excl_html);
	
	echo "$html"."####"."$name";		
	exit();
}

?>