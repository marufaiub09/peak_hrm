<?php
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond."xcz";
//---------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
include("../../../includes/array_function.php");

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY level ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script> 
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
   
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
	
    <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
    
	<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" /> 
 	<script type="text/javascript">
	$(document).ready(function(e) {
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
		//header: false, 
		selectedText: "# of # selected",
		});
			
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
		});
	}); 
		
function openmypage_employee_info()
{			
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
	emailwindow.onclose=function()
	{
		var thee_loc = this.contentDoc.getElementById("txt_selected");
		var thee_id = this.contentDoc.getElementById("txt_selected_id");				
		$('#emp_code').val(thee_id.value);
	}
}	
function openmypage_employee_info_id_card()
{			
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#category').val()+'&company_id='+$('#company_id').val(), 'Employee Information',  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
	emailwindow.onclose=function()
	{
		var thee_loc = this.contentDoc.getElementById("txt_selected");
		var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
		$('#id_card').val(thee_id.value);
	}
}	
	function reset_field()
	{
		document.getElementById('data_panel').innerHTML="";
		document.getElementById('data_panel2').innerHTML="";
	}
	
	//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>

</head>

<body style="font-family:verdana; font-size:11px;">
<div align="left" style="width:100%;">
    <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee Profile</h3>
    <div id="content_search_panel" > 
    <form autocomplete="off">
		<fieldset id="filter_panel" >
			<div style="padding:10px;">
			<table align="center" cellspacing="0" cellpadding="0" width="1120px" class="rpt_table" >
				<thead>
                	<th align="center"><strong>Category</strong></th>
					<th align="center"><strong>Company</strong></th>
					<th align="center"><strong>Location</strong></th>
                    <th align="center"><strong>Division</strong></th>
					<th align="center"><strong>Department</strong></th>
                    <th align="center"><strong>Section</strong></th>
                    <th align="center"><strong>Sub Section</strong></th>
					<th align="center"><strong>Designation</strong></th>
				</thead>
                <tr class="general">
                	 <td align="center">
                        <select name="category" id="category" class="combo_boxes" style="width:140px">
                            <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                        </select>
					</td>
                
                    <td align="center" id="company">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- All --</option>
                            <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
							<option value="<?php echo $company['id']; ?>" <? if(count($company_details)==1)echo"selected"; ?>><?php echo $company['company_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                     <td align="center" id="location">
						<select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
							<option value="0">-- All --</option>
							<?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
							<option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                    <td align="center" id="division">
						<select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple" >
							<?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
						<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
					<?php } } ?>
						</select>
                    </td>
                   <td align="center" id="department">
						<select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple" >
							<?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
						<option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                     <td align="center" id="section">
						<select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
							<?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
						<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
					 <td align="center" id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple" >
							<?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
						<option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                     <td align="center" id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple"  >
							<?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
						<option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
					<?php } } ?>
						</select>
					</td>
            </tr>
            </table>
            <div style="height:8px;"></div>
            <table align="center" cellspacing="0" cellpadding="0" width="700" class="rpt_table" >
                <thead>
                    <th align="center"><strong>Salary Based</strong></th>
                    <th align="center"><strong>Employee Type</strong></th>
                    <th align="center"><strong>ID Card No</strong></th>
                    <th align="center"><strong>System Code</strong></th>
                    <th align="center"><input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();"  style="width:100px"/></th>
                </thead>
                <tr class="general">
                    <td>
                    	<select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:120px" >							
                           <option value="">-- Select --</option>
                            <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                     <td align="center">
                	<select name="cbo_salary_sheet" id="cbo_salary_sheet" class="combo_boxes" style="width:120px" >                    	
                        <option value=""> All Employee </option>
                        <option value="0">Regular Employee</option>
                        <option value="1">Separated Employee</option>
                    </select>
                </td>
                <td>
                    <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:150px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                </td>
                <td align="center">
                    <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:150px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                </td>                
                    <td align="center">
                    	<input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="employee_profile_populate();"  style="width:100px"/>
                    </td>
                </tr>
			</table>
            </div>
      </fieldset>
	</form>
    </div>
    <fieldset style="width:100%;">
        <div id="data_panel" style="margin-top:5px;" align="center"></div>
        <div id="data_panel2" align="center" style="margin-top:10px;"></div>
    </fieldset>
</div>
</body>
</html>