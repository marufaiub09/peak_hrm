<?php 
/***********************************
| Developed by	: Md. Nuruzzaman 
| Date			: 23.01.2014
***********************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
		
	$sql="SELECT * FROM lib_skill WHERE is_deleted = 0 ORDER BY skill_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$skill_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$skill_details[$row['id']] = mysql_real_escape_string( $row['skill_name'] );
	}

//$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        
        <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    	<script type="text/javascript" src="../../../includes/tablefilter.js"></script>
        
        <script type="text/javascript" charset="utf-8">
			function generate_skill_type_report()
			{
				//alert("su..re");
				$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
				var error = false;
				div="report_container";
				data_panel="data_panel";
				if( $('#company_id').val() == "0" ) {
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#company_id').focus();
						$(this).html('Please Select The Company.').addClass('messageboxerror').fadeTo(900,1);
					});
				}
				if( error == false )
				{
				var data  = '&category='+$('#category').val() +
							'&company_id='+$('#company_id').val() +
							'&location_id='+$('#location_id').val() +
							'&division_id='+$('#division_id').val() +
							'&department_id='+$('#department_id').val() +
							'&section_id='+$('#section_id').val() +
							'&subsection_id='+$('#subsection_id').val() +
							'&designation_id='+$('#designation_id').val()+
							'&salary_based='+$('#salary_based').val()+ 
							'&skill_id='+$('#skill_id').val();
				
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{			
						var data_split=(xmlhttp.responseText).split('####');
						var link_data=data_split[1];			
						$('#data_panel').html( '<br>Convert To : <a href="includes/tmp_report_file/' + data_split[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
						$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );		
						
						document.getElementById(div).innerHTML=data_split[0];
						
						setFilterGrid("table_body",-1);

						hide_search_panel();
						$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
						{ 
							 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
						});
					}
				}  	
				xmlhttp.open("GET","includes/generate_skill_set_report.php?type=skill_set_report"+data,true);//generate_report_payment
				xmlhttp.send();
				}
			}
			function new_window()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('report_container').innerHTML);
				d.close();
			}
			
			function show_skill_details(code)
			{
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','search_skill_details.php?action=skill_details&code='+code,'Skill details','width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')
				emailwindow.onclose=function()
				{
					//alert(str);
					//var thee_loc = this.contentDoc.getElementById("txt_selected");
					//var thee_id = this.contentDoc.getElementById("txt_selected_id");				
					//$('#emp_code').val(thee_id.value);
				}
			}
		</script>
		<style type="text/css"> 
			td {overflow:hidden; }
			@media print {thead {display: table-header-group;}}
			@media print {tfoot {display: table-footer-group;}}
			.verticalText 
			{               
				writing-mode: tb-rl;
				filter: flipv fliph;
				-webkit-transform: rotate(270deg);
				-moz-transform: rotate(270deg);
			}
		</style>
    </head>
    <body>
        <div style="width:1300px;" align="center">
            <div align="center"><div align="center" id="messagebox"></div></div>
            <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Skill Set Report</h3>
            <div id="content_search_panel" > 
                <form>
                    <fieldset>
                        <table width="700" cellpadding="0" cellspacing="0" class="rpt_table">        	
                            <thead>
                                <th><strong>Category</strong></th>              
                                <th><strong>Company</strong></th>  
                                <th><strong>Location</strong></th>
                                <th><strong>Division</strong></th>
                                <th><strong>Department</strong></th>
                                <th><strong>Section</strong></th>
                                <th><strong>SubSection</strong></th>
                                <th><strong>Designation</strong></th>
                                <th><strong>Salary Based</strong></th>
                                <th><strong>Skill Type</strong></th>	
                            </thead>
                            <tbody>
                                <td>
                                    <select name="category" id="category" class="combo_boxes">
                                        <option value="">All Category</option>
                                        <? foreach($employee_category as $key=>$val) { ?>
                                        <option value="<? echo $key; ?>"><? echo $val; ?></option> <? } ?> 
                                    </select>
                                </td>                                 
                                <td>
                                    <select name="company_id" id="company_id" class="combo_boxes" style="width:120px">
                                        <? if($company_cond==""){ ?>
                                        <option value="0">-- Select --</option>
                                        <?php } foreach( $company_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>" <? if(count($company_details)==1){ echo "selected";} ?>><?php echo $value; ?></option><?php } ?>
                                    </select>
                                </td> 
                                <td>
                                    <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $location_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                  </select>
                                </td>
                                <td>
                                    <select name="division_id" id="division_id" class="combo_boxes" style="width:120px">
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $division_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option> <?php } ?>
                                  </select>
                              </td> 
                                <td>
                                    <select name="department_id" id="department_id" class="combo_boxes" style="width:120px">
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $department_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="section_id" id="section_id" class="combo_boxes" style="width:120px">
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $section_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                    </select>
                                </td>
                                <td id="subsection">
                                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px">
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                    </select>
                                </td>
                                <td id="designation">
                                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="salary_based" id="salary_based" class="combo_boxes" style="width:100px" >							
                                       <option value="">-- Select --</option>
                                        <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                     </select>
                                </td>
                                <td>
                                    <select name="skill_id" id="skill_id" class="combo_boxes" style="width:100px" >							
                                       <option value="">-- Select --</option>
                                       <?php foreach( $skill_details AS $key=>$value ){ ?>
                                       <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                                     </select>
                                </td>
                                </tr>
                                <tr height="10px"></tr>
                                <tr>
                                	<td colspan="10" align="center">
                                    	<input type="button" name="search" id="search" value="Show" class="formbutton" style="width:100px" onclick="generate_skill_type_report()"/>
                                    	<input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
            <table>
                <tr>
                <td style="border:none" colspan="10" height="25" id="data_panel" align="center"></td>               
                </tr>   
                <tr align="center">            	
                <td style="border:none" colspan="10" id="report_container"></td>
                </tr>  
            </table>
        </div>
    </body>
</html>
