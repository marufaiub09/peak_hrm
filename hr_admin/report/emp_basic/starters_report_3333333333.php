<?php
 
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

 
//--------------------------------------------------------------------------------------------------------------------

include("../../../includes/common.php");
include('../../../includes/array_function.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}
	
	//location_details
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$location_details[$row['id']] = mysql_real_escape_string( $row['location_name'] );
	}
	
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}

	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
    
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
	<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function() {
			$( "#to_date" ).datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
				//header: false, 
				selectedText: "# of # selected",
			});
			
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
			});
        });
    
        function reset_field()
        {
             document.getElementById('data_panel').innerHTML="";
             document.getElementById('data_panel2').innerHTML="";
        }
	</script>
</head>
<body style="font-family:verdana; font-size:11px;">
    <div align="center" style="width:100%;">
        <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
        <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')">
        	- Starters Report Summary
        </h3>
        <div id="content_search_panel" > 
        <form>
            <fieldset>
                     <table cellspacing="0" cellpadding="0" width="600" class="rpt_table" align="center">
                        <thead>
                            <th width="140px" align="center"><strong>Category</strong></th>
                            <th width="140px" align="center"><strong>Company</strong></th>
                            <th width="140px" align="center"><strong>Location</strong></th>
                            <th width="140px" align="center"><strong>Division</strong></th>
                            <th width="140px" align="center"><strong>Department</strong></th>
                            <th width="140px" align="center"><strong>Section</strong></th>
                            <th width="140px" align="center"><strong>Sub Section</strong></th>
                            <th width="140px" align="center"><strong>Designation</strong></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                     <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                                        <option value="">All Category</option>
                                        <?
                                        foreach($employee_category as $key=>$val)
                                        {
                                        ?>
                                        <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                        <?
                                        }
                                        ?> 
                                     </select>
                                </td>
                                <td>
                                    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
                                        <? if($company_cond=="")
                                           { 
                                            ?>
                                        <option value="0">-- Select --</option>
                                        <?php } foreach( $company_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:140px">
                                        <option value="0">-- select --</option>
                                        <?php 
                                        foreach( $location_details AS $key=>$value) 
                                        { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <!--<option value="0">-- Select --</option>-->
                                        <?php foreach( $division_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                
                                <td>
                                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <!--<option value="0">-- Select --</option>-->
                                        <?php foreach( $department_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                               <td>
                                    <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <!--<option value="0">-- Select --</option>-->
                                        <?php foreach( $section_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                               <td>
                                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <!--<option value="0">-- Select --</option>-->
                                        <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                       <!--<option value="0">-- Select --</option>-->
                                        <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </td> 
                            </tr>
                        </tbody>
                    </table>
                    <div style="height:8px;"></div>
                    <table cellspacing="0" cellpadding="0" width="620" class="rpt_table" align="center">
                        <thead>
                            <th align="center" width="140px"><strong>Salary Based</strong></th>
                            <th align="center" width="140px"><strong>Group By</strong></th>
                            <th align="center" width="140px"><strong>Order By</strong></th>
                            <th align="center" width="100px"><strong>Date</strong></th>
                            <th><input type="reset" name="rpo_search" id="rpo_search" value="Reset" class="formbutton" onClick="reset_field();" style="width:100px" /></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:140px" >							
                                    <option value="">-- Select --</option>
                                    <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                 </select>
                                </td> 
                                <td>
                                    <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:140px" >							
                                       <option value="0">-- Select --</option>
                                        <?php foreach( $groupby_arr AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                     </select>
                                </td>
                                <td>
                                    <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:140px" >							
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $orderby_arr AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td><input type="text" name="to_date" id="to_date" value="" class="text_boxes" style="width:100px"/></td>                    
                                <td><input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="search_starter_summary();" style="width:100px" /></td>
                            </tr>
                        </tbody>
                    </table>
            </fieldset>  	
        </form>
        </div>
        
        
    </div>
    <fieldset>
        <div id="data_panel" style="margin-top:5px;" align="center"></div>
        <div id="data_panel2" align="center" style="margin-top:10px;"></div>
    </fieldset>
</body>
</html>