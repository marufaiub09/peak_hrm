<?php
 
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
//--------------------------------------------------------------------------------------------------------------------

include("../../../includes/common.php");
include('../../../includes/array_function.php');
	
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 order by trim(custom_designation) ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
	
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
    <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  	<link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" /> 
  
    <script type="text/javascript" charset="utf-8">
		$(document).ready(function(e) {
			$( ".datepicker" ).datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
			
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
			//header: false, 
			selectedText: "# of # selected",
			});
			
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
			});
		}); 

		function reset_field()
		{
			document.getElementById('data_panel').innerHTML="";
			document.getElementById('data_panel2').innerHTML="";
			document.getElementById('data_panel3').innerHTML="";
		}
    </script>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:100%;">
	<div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
	<h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Starters Report of Employees</h3>
    <div id="content_search_panel" > 
  <form>
    <fieldset>
        	 <table cellspacing="0" cellpadding="0" width="1120px" class="rpt_table" align="center">
				<thead>
                	<tr>                	  
                        <th align="center"><strong>Category</strong></th>
                        <th align="center"><strong>Company</strong></th>
                        <th align="center"><strong>Location</strong></th>
                        <th align="center"><strong>Division</strong></th>
                        <th align="center"><strong>Department</strong></th>
                        <th align="center"><strong>Section</strong></th>
                        <th align="center"><strong>Designation</strong></th>
                        <th align="center"><strong>Salary Based</strong></th>
                    </tr>
				</thead>

                <tr class="general">
                    <td>
                         <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                            <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
                    </td>
                    <td>
                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
                            <? if($company_cond=="")
                            { 
                            ?>
                            <option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
                            <option value="<?php echo $company['id']; ?>" <? if(count($company_details)==1)echo"selected"; ?> ><?php echo $company['company_name']; ?></option>
                            <?php } } ?>
                        </select>
                    </td>
                    <td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                            <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
                            <option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
                            <?php } } ?>
                        </select>
                    </td>
                    <td align="center" id="division">
                    <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
                    <option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    <td align="center" id="department">
                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
                    <option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    
                    <td align="center" id="section">
                    <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
                    <option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    
                    <td align="center" id="designation">
                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
                    <option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
                    <?php } } ?>
                    </select>
                    </td>
                    <td>
                    	<select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:140px" >							
                           <option value="">-- Select --</option>
                            <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                </tr>
            </table>
            <div style="height:8px;"></div>
            <table cellspacing="0" cellpadding="0" width="700" class="rpt_table" align="center">
				<thead>
                    <th align="center" width="140px" colspan="2"><strong>Date</strong></th>
                    <th align="center" width="140px"><strong>Employee Type</strong></th>
                    <th align="center" width="140px"><strong>Group By</strong></th>
                    <th align="center" width="140px"><strong>Order By</strong></th>
                    <th width="100px"><input type="reset" name="rpo_search" id="rpo_search" value="Reset" class="formbutton" onClick="reset_field();" style="width:100px" /></th>
				</thead>
                <tr class="general">
                    <td><input type="text" name="from_date" id="from_date" value="" class="datepicker" placeholder="From" style="width:70px"/></td>
                    <td><input type="text" name="to_date" id="to_date" value="" class="datepicker" placeholder="To" style="width:70px"/></td>                    
                    <td>
                        <select name="cbo_employee_type" id="cbo_employee_type" class="combo_boxes" style="width:140px" >                    	
                            <option value=""> All Employee </option>
                            <option value="1">Regular Employee</option>
                            <option value="0">Separated Employee</option>
                        </select>
                    </td>
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:140px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:140px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                    <td><input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="search_starter_employees_dtls();" style="width:100px" /></td>
                </tr>
			</table>
	</fieldset>
 </form>
 </div>
</div>
	<div id="data_panel" align="center" style="margin-top:10px;">&nbsp;</div>
    <div id="data_panel2" align="left" style="margin-top:10px;">&nbsp;</div>
    <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden">&nbsp;</div>
</body>
</html>