<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include("../../../../includes/common.php");
include('../../../../includes/array_function.php');

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )){
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ){
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
	$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
	$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}
	extract($_GET);
	$active_data=explode("_", $data);
	//print_r($active_data);
	function my_old($dob){
		$now = date('d-m-Y');
		$dob = explode('-', $dob);
		$now = explode('-', $now);
		$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
		if($now[0] < $dob[0]){
			$now[0] += $mnt[$now[1]-1];
			$now[1]--;
		}
		if($now[1] < $dob[1]){
			$now[1] += 12;
			$now[2]--;
		}
		if($now[2] < $dob[2]) return false;
		return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
	}
	?>
	<style type="text/css">
        @page { size 8.5in 11in; margin: 0.5in; }
        div.page { page-break-after: always; background-color:#FFF;}
    </style>
<?php
if($type=="starters_report_detail")
{		 
	//ob_start();
	$from_date=convert_to_mysql_date($from_date);
	$to_date=convert_to_mysql_date($to_date);
	
	if ($cbo_emp_category=='') $category ="and category in ($employee_category_index)"; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else	$division_id=" and division_id in($division_id)";	
	if ($department_id==0) $department_id=""; else	$department_id=" and department_id in($department_id)";	
	if ($section_id==0) $section_id=""; else $section_id="and section_id in($section_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in($designation_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	if ($employee_type=="") $employee_type=""; else $employee_type="and status_active='$employee_type'";
	
	//new add group by order by
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
	
	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(id_card_no), 5)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$subsection_total_gross_amount=0;
	$section_total_gross_amount=0;	
	$department_total_gross_amount=0;
	$division_total_gross_amount=0;	
	$location_total_gross_amount=0;				
	$grand_total_gross_amount=0;
	
	
		$company_info_sql="select com.id, company_name, email, website, plot_no, level_no, road_no,	block_no,zip_code,city, cnt.country_name from lib_company com,lib_list_country cnt where com.country_id =cnt.id";
	
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$row=mysql_fetch_array($result);
		?>
		
<div align="left">
		
        	<div align="center">
                <span style="font-size:18px; font-weight:bold"><? echo $row["company_name"]; ?></span><br />
                <b>Recruitment Panel From <? echo convert_to_mysql_date($from_date); ?> To <? echo convert_to_mysql_date($to_date); ?></b> 
			</div>
        
            <table id="scroll_body" cellspacing="0" cellpadding="0" class="rpt_table" width="2000" border="1" rules="all"> 
                <thead>                            
                    <th width="35" align="center"><strong>SL</strong></th>
                    <th width="80" align="center"><strong>ID</strong></th>
                    <th width="80" align="center"><strong>Photo</strong></th>
                    <th width="90" align="center"><strong>Emp Code</strong></th>
                    <th width="150" align="center"><strong>Name</strong></th>
                    <th width="150" align="center"><strong>Company</strong></th>
                    <th width="120" align="center"><strong>Designation</strong></th>
                    <th width="75" align="center"><strong>DOB</strong></th>
                    <th width="75" align="center"><strong>DOJ</strong></th>
                    <th width="75" align="center"><strong>Grade</strong></th>
                    <th width="75" align="center"><strong>Gross</strong></th>
                    <th width="150" align="center"><strong>Address</strong></th>
                    <th width="70" align="center"><strong>Sex</strong></th>
                    <th width="70" align="center"><strong>Religion</strong></th>
                    <th width="90" align="center"><strong>Age</strong></th>
                    <th width="120" align="center"><strong>Division</strong></th>
                    <th width="120" align="center"><strong>Department</strong></th>
                    <th width="100" align="center"><strong>Section</strong></th>
                    <th width="100" align="center"><strong>Sub Section</strong></th>
                    <th width="130" align="center"><strong>Route</strong></th>
                    <th width="100"><strong>Vehicle No.</strong></th>
                </thead>
                
		<?php  $small_print .='<div align="center" style="width:900px">
            <span style="font-size:18px; font-weight:bold">'.$row["company_name"].'</span><br />
            <b>Recruitment Panel From '.$from_date.' To '.$to_date.'</b></div> <br />
            <div align="left"><b>Section : '.$section_details[$emp[section_id]].'</b></div>';
            $small_print .='<table id="table_body" cellspacing="0" cellpadding="0" class="rpt_table" width="800" border="1" rules="all">
            <thead>                            
                <th width="40" align="center"><strong>SL</strong></th>
                <th width="160" align="center"><strong>Name</strong></th>
                <th width="100" align="center"><strong>Emp ID</strong></th>
                <th width="100" align="center"><strong>Emp Code</strong></th>
                <th width="120" align="center"><strong>Designation</strong></th>
                <th width="90" align="center"><strong>DOJ</strong></th>
                <th width="60" align="center"><strong>Grade</strong></th>
                <th width="90" align="center"><strong>Gross</strong></th>
                <th width="" align="center"><strong>Remarks</strong></th>
            </thead>';
		
	
	$sl=0;
	$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
	FROM hrm_employee 
	WHERE 
	joining_date between '$from_date' and '$to_date'
	$category
	$cbo_company_id
	$location_id
	$division_id
	$department_id
	$section_id
	$designation_id
	$salary_based
	$employee_type
	group by $dynamic_groupby $orderby";
	//is_deleted=0 and status_active=1 cut from query(reason when emp seperate the emp is not found on the str report ismail requirment 27-08-2013)
	//echo $sql;die;
	$result=mysql_query($sql);
		$j=1;
		while($emp=mysql_fetch_assoc($result))
		{
			$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=0 and status_active=1 and is_deleted=0";
			$result1=mysql_query($sql1);
			$row=mysql_fetch_assoc($result1);
			if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
			if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
			if($row['village']=="") $village=""; else $village=$row['village'].", ";
			if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
			if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
			$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
			
			$sql2="SELECT * FROM `hrm_transport_mst` WHERE `vehicle_no` in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
			$result2=mysql_query($sql2);
			$row2=mysql_fetch_assoc($result2);
			
			//$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
			$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0 order by id desc limit 1");
			$image="";
			if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
			
			
				$sl++;
				if($sl==1)
				{
					$location_arr[$emp[location_id]]=$emp[location_id];
					$division_arr[$emp[division_id]]=$emp[division_id];
					$department_arr[$emp[department_id]]=$emp[department_id];
					$section_arr[$emp[section_id]]=$emp[section_id];
					$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
				}//end if condition of header print
				//end header print
				if($sl!=1)
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
					if(in_array($emp[location_id],$location_arr) && $status_loc==1)
					{
						if(in_array($emp[division_id],$division_arr) && $status_divis==1)
						{
							if(in_array($emp[department_id],$department_arr) && $status_dept==1)
							{
								if(in_array($emp[section_id],$section_arr) && $status_sec==1)
								{ 
									if(in_array($emp[subsection_id],$subsection_arr) && $status_subsec==1)
									{}
									else if($status_subsec==1)
									{
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_subsec=1;
									}
								}
								else if($status_sec==1)
								{
									$section_arr[$emp[section_id]]=$emp[section_id];
									$subsection_arr=array();
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_sec=1;
									$new_subsec=1;
								}
							}
							else if($status_dept==1)
							{
								$department_arr[$emp[department_id]]=$emp[department_id];
								$section_arr=array();
								$subsection_arr=array();
								$section_arr[$emp[section_id]]=$emp[section_id];
								$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
								$new_dept=1;
								$new_sec=1;
								$new_subsec=1;
							}
						}//division
						else if($status_divis==1)
						{
							$division_arr[$emp[division_id]]=$emp[division_id];
							$department_arr=array();
							$section_arr=array();
							$subsection_arr=array();
							$department_arr[$emp[department_id]]=$emp[department_id];
							$section_arr[$emp[section_id]]=$emp[section_id];
							$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
							$new_divis=1;
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$emp[location_id]]=$emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
				}
				
				//header print here 
				$c_part_1="";
				if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
				{
					if($status_loc==1)
					{					
						$c_part_1 .= "  Location :  ".$location_details[$emp[location_id]]."  ,";
					}				
					if($status_divis==1)
					{
						$c_part_1 .= "  Division :  ".$division_details[$emp[division_id]]."  ,";
					}
					if($status_dept==1)
					{
						$c_part_1 .= "  Department :  ".$department_details[$emp[department_id]]."  ,";
					}
					if($status_sec==1)
					{
						$c_part_1 .= "  Section :  ".$section_details[$emp[section_id]]."  ,";
					}
					if($status_subsec==1)
					{
						$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp[subsection_id]]."  ,";
					}
					
					if($c_part_1!='')
					{
						$i=0;$sl=1;
						?><tr><td colspan="21"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
					}	
				}?>	
           
   <?
			//new section 
			if ($j%2==0) {$bgcolor="#E9F3FF";} 
			else {$bgcolor="#FFFFFF";}
			?>
                <?
                $small_print .='<tr>
					<td width="40" align="center" style="font-size:13px;">'.$j.'</td>
					<td width="160" style="font-size:13px;">'.$emp[name].'</td>
					<td width="110" style="font-size:13px;">&nbsp;'.$emp[id_card_no].'</td>
					<td width="110" style="font-size:13px;">&nbsp;'.$emp[emp_code].'</td>
					<td width="120" style="font-size:13px;">&nbsp;'.$designation_chart[$emp[designation_id]].'</td>
					<td width="90" align="center" style="font-size:13px;">'.convert_to_mysql_date($emp[joining_date]).'</td>
					<td width="45" align="right" style="font-size:13px;">'.$emp[salary_grade].'</td>
					<td width="70" align="right" style="font-size:13px;">'.$emp[gross_salary].'</td>
					<td width="">&nbsp;</td>
				</tr>';?>
                
                
                <tr bgcolor="<? echo $bgcolor; ?>">
                    <td width="35" align="center"><?php echo $j;?></td>
                    <td width="80">&nbsp;<?php echo $emp['id_card_no'];?></td>
                    <td width="80" valign="bottom" align="center"><?php echo $image; ?></td>
                    <td width="90">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
                    <td width="150"><?php echo $emp['name'] ; ?></td>
                    <td width="150"><?php echo $company_details[$emp['company_id']]; ?></td>
                    <td width="120">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
                    <td width="75" align="center"><?php echo convert_to_mysql_date($emp['dob']) ; ?></td>
                    <td width="75" align="center"><?php echo convert_to_mysql_date($emp['joining_date']) ; ?></td>
                    <td width="75" align="center"><?php echo $emp['salary_grade'] ; ?></td>
                    <td width="75" align="center"><?php echo $emp['gross_salary'] ; ?></td>
                    <td width="150"><?php echo $address ; ?></td>
                    <td width="70" align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
                    <td width="70" align="center"><?php echo $emp['religion'] ; ?></td>
                    <td width="90"><?php $birth_date=convert_to_mysql_date($emp['dob']); $age = my_old($birth_date);
                                    printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);?>
                    </td>
                    <td width="120">&nbsp;<?php echo $division_details[$emp['division_id']]; ?></td>
                    <td width="120">&nbsp;<?php echo $department_details[$emp['department_id']]; ?></td>
                    <td width="100">&nbsp;<?php echo $section_details[$emp['section_id']]; ?></td>
                    <td width="100">&nbsp;<?php echo $subsection_details[$emp['subsection_id']]; ?></td> 
                    <td width="130">&nbsp;<?php echo $row2['route']; ?></td>
                    <td width="100">&nbsp;<?php echo $row2['vehicle_no']; ?></td>			
                </tr><?php 
                $j++;
			}// end while loop
		?>			
			</table><?php
			$small_print .='</table>'; 	
					
			$html=ob_get_contents();		
			ob_clean();
			//for report temp file delete 
			foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
			}		
			//html to xls convert
			$name=time();
			$name="$name".".xls";	
			$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
			$is_created = fwrite($create_new_excel,$html);
			echo "$html"."####"."$name"."####".$small_print;		
			exit();
		
}// end if condition starters report detail


function add_month($orgDate,$mon)
{
	$cd = strtotime($orgDate);
	$retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	return $retDAY;
}

	
function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}
?>