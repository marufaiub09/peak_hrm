<?php
/***********************************
| Developed by	: Md. Nuruzzaman 
| Date			: 20.02.2014
***********************************/
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	// company
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	// location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}
	
	// division
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}
	
	// department
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	// section
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	// subsection
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	// designation
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_details[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}

	$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	
	// company details
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	//District
	$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}
	
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['not_pay_deduction'] = mysql_real_escape_string( $row['not_pay_deduction'] );
		$var_hrm_chart[$row['company_name']]['new_emp_card_charge'] = mysql_real_escape_string( $row['new_emp_card_charge'] );
		$var_hrm_chart[$row['company_name']]['ot_rate_multiplier'] = mysql_real_escape_string( $row['ot_rate_multiplier'] );
		$var_hrm_chart[$row['company_name']]['att_bonus_leave_type'] = mysql_real_escape_string( $row['att_bonus_leave_type'] ); 	 	 	
	}
	
//month generated
if($type=="select_month_generate")
{		
	//echo $id;
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}

//generate_division_dropdown
if($type=="generate_division_dropdown")
{		
	//echo 'su..re';
	$sql= "SELECT * FROM lib_division where location_id='$location_id' and status_active=1 and is_deleted=0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	$division_dtls=array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		//$division_dtls[$row['id']]=$row["division_name"];
		echo "<option value='".$row['id']."'>".$row["division_name"]."</option>";
	}		
	exit();
}

// generate_dept_wise_ot_eot_summary_report	
if( $action == "generate_dept_wise_ot_eot_summary_report" ) 
{ 
	$divs_id=$division_id;
	$start_date=convert_to_mysql_date($from_date);
	$end_date=convert_to_mysql_date($to_date);
	$get_days=datediff(d,$start_date,$end_date);
	$all_date=createDateRangeArray($start_date,$end_date);
	
	if ($category_id	== '')	$category_id	= "and e.category in(0,1,2,3,4)";	else $category_id = "and e.category='$category_id'";
	if ($company_id		== 0)	$company_id		= "";	else	$company_id	= "and a.company_id='$company_id'";
	if ($location_id	== 0) 	$location_id	= ""; 	else 	$location_id= "and a.location_id='$location_id'";
	if ($division_id	== 0 || $division_id	== '')	$division_id	= "";	else	$division_id	= "and a.division_id in($division_id)";
	if ($department_id  == 0 || $department_id	== '')	$department_id	= "";	else	$department_id	= "and a.department_id in($department_id)";
	if ($section_id		== 0 || $section_id		== '') 	$section_id		= ""; 	else 	$section_id		= "and a.section_id in($section_id)";
	if ($subsection_id	== 0 || $subsection_id	== '')	$subsection_id	= "";	else	$subsection_id	= "and a.subsection_id in($subsection_id)";
	if ($designation_id == 0 || $designation_id	== '')	$designation_id	= "";	else	$designation_id	= "and a.designation_id in($designation_id)";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="a.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,a.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,a.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,a.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.emp_code,";}
	
	//new order by
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(e.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(a.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(e.id_card_no), 5)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	ob_start();
	?>
	<table cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
    <?php
		$i=0;
		$sl=0;
		$r=0;
		
		$sql_dept="SELECT * FROM lib_department WHERE division_id in ( $divs_id ) and status_active=1 and is_deleted=0 order by department_name ASC";
		$result_dept=mysql_query($sql_dept) or die($sql_dept . "<br />" .mysql_error());
		$all_dept_array=array();
		
		while( $row_dept=mysql_fetch_assoc($result_dept) )
		{
			$dept_arr[]=$row_dept['id'];
			$divcount[$row_dept['division_id']]++;
			$all_dept_array[$row_dept['division_id']][$row_dept['id']]=$row_dept['department_name'];
		}
		/*
		print_r($data_array_ot);die;
		
		print_r($sql); die;
		$sql=" SELECT emp_code,";  
		$i=0;
		foreach($all_dept_array as $divid=>$key)
		{
			/// echo "d-".$div."<br>";
			foreach($key as $deptid=>$deptname)
			{
				if($i!=0) $sql .=",";
				$sql .=" sum(CASE WHEN division_id ='$divid' and department_id='$deptid' THEN total_over_time_min END) AS 'OT".$divid."_".$deptid."'";
				$i++;
			}
			 
		}
		
		$sql=" SELECT emp_code,";  
		$i=0;
		foreach($all_dept_array as $divid=>$key)
		{
			/// echo "d-".$div."<br>";
			foreach($key as $deptid=>$deptname)
			{
				if($i!=0) $sql .=",";
				$sql .=" sum(CASE WHEN division_id ='$divid' and department_id='$deptid' THEN total_over_time_min END) AS 'OT".$divid."_".$deptid."'";
				$i++;
			}
			 
		}
		$sql .=" from  hrm_attendance  where attnd_date between '$start_date' and '$end_date'  group by emp_code order by emp_code";	
		echo $sql; 
		die;
		 
		for($i=0;$i<$total_days_att; $i++)
		{
			$c_date=add_date($txt_from_date,$i);
			if($i!=0) $sql .=",";
			$sql .=" sum(CASE WHEN attnd_date ='$c_date' THEN total_over_time_min END) AS 'OT".$c_date."',  sum(CASE WHEN attnd_date ='$c_date' and is_regular_day=0 THEN total_over_time_min END) AS 'BOT".$c_date."'";
		}
		$sql .="from  hrm_attendance  where attnd_date between '$start_date' and '$end_date'  group by emp_code order by emp_code";	
		$exe_sql_attnd=mysql_db_query($DB, $sql);
		$employee_ot=array();
		$b_employee_ot=array();
		while($data=mysql_fetch_array($exe_sql_attnd))
		{
			for($i=0;$i<$total_days_att; $i++)
			{
				$c_date=add_date($txt_from_date,$i);
				$employee_ot[$data['emp_code']][$c_date]=$data['OT'.$c_date];
				$b_employee_ot[$data['emp_code']][$c_date]=$data['BOT'.$c_date];
			}
		} 
		*/
		
		$sql="SELECT e.category,e.emp_code,a.emp_code,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.attnd_date,a.total_over_time_min 
		FROM hrm_attendance a,hrm_employee e 
		WHERE 
		a.attnd_date between '$start_date' and '$end_date' and
		a.emp_code=e.emp_code 
		and a.department_id in (".implode(",",$dept_arr).") and a.division_id in ($divs_id) and a.total_over_time_min>0";
		$result_att=mysql_query($sql) or die($sql . "<br />" .mysql_error());
		while( $row=mysql_fetch_assoc($result_att) )
		{
			$tmpp=get_buyer_ot_hr($row[total_over_time_min],$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],1440);
			 
			if( $tmpp >= 2)
			{ 
				$data_array_ot[$row['division_id']][$row['department_id']][$row['attnd_date']]["both"] +=2;
				$data_array_ot[$row['division_id']][$row['department_id']][$row['attnd_date']]["botm"] +=0;
			}
			else
			{
				$tmppd_nrd=explode(".",$tmpp);
				$data_array_ot[$row['division_id']][$row['department_id']][$row['attnd_date']]["both"] +=$tmppd_nrd[0];
				$data_array_ot[$row['division_id']][$row['department_id']][$row['attnd_date']]["botm"] +=$tmppd_nrd[1];
			}
			if( $tmpp >2 )
			{
				$tmppd=$tmpp-2.0;
				$tmppd_nrd=explode(".",$tmppd);
				
				$data_array_ot[$row['division_id']][$row['department_id']][$row['attnd_date']]["eoth"] +=$tmppd_nrd[0];
				$data_array_ot[$row['division_id']][$row['department_id']][$row['attnd_date']]["eotm"] +=$tmppd_nrd[1];
			}			
		}
		?>
        <thead> 
            <tr height="65">
                <th colspan="<?php echo (count($dept_arr)*2)+(count($divcount)*2)+1?>" align="center" bordercolor="#FFFFFF" style="vertical-align:top;">
                    <br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br /><br />
                    Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, 
                    Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                </th>
            </tr>
            <tr>
                <th rowspan="3"  width="80px">Date</th>
                <?php
                foreach($all_dept_array as $divids=>$divdd)
                {
                ?>
                <th colspan="<?php echo (($divcount[$divids]*2)+2); ?>"><?php echo $division_details[$divids]; ?></th>
                <?php 
                }
                ?>
            </tr>
            <tr>
                <?php 
                foreach( $all_dept_array as $divids=>$divdd )
                { 
                    foreach( $divdd as $depids=>$depdd )
                    {
                	?>
                    <th style="font-size:12px;" colspan="2" width="140px"> <?php echo $depdd; ?></th>
               		<?php 
                    }
                    ?>
                    <th style="font-size:12px;" colspan="2" width="140px"> Total</th>
                    <?php
                } 
                ?>
            </tr>
            <tr>
                <?php 
                foreach( $all_dept_array as $divids=>$divdd )
                { 
                    foreach( $divdd as $depids=>$depdd )
                    {
                	?>
                    <th style="font-size:12px;" width="70" > <?php echo "OT"; ?></th>
                    <th style="font-size:12px;" width="70"> <?php echo "EOT"; ?></th>
               		<?php 
                    }
                    ?>
                    <th style="font-size:12px;" width="70" > <?php echo "OT"; ?></th>
                    <th style="font-size:12px;" width="70"> <?php echo "EOT"; ?></th>
                    <?
                } 
                ?>
            </tr>
        </thead>
        <tbody>
		<?
        for($days=0; $days<$get_days; $days++ )
        {
			if ($days%2==0) $bgcolor="#E9F3FF";
			else $bgcolor="#FFFFFF"; 
        ?>
        <tr bgcolor="<? echo $bgcolor;?>">
            <td width="80px" align="center"><?php echo change_date_format($all_date[$days]);?></td>
                <?php 
                foreach( $all_dept_array as $divids=>$divdd )
                { 
                    foreach( $divdd as $depids=>$depdd )
                    {
                        $bot = ($data_array_ot[$divids][$depids][$all_date[$days]]["both"]+floor($data_array_ot[$divids][$depids][$all_date[$days]]["botm"]/60)).".".str_pad(($data_array_ot[$divids][$depids][$all_date[$days]]["botm"]%60),2,"0",STR_PAD_LEFT);
                        $eot = ($data_array_ot[$divids][$depids][$all_date[$days]]["eoth"]+ floor($data_array_ot[$divids][$depids][$all_date[$days]]["eotm"]/60)).".".str_pad(($data_array_ot[$divids][$depids][$all_date[$days]]["eotm"]%60),2,"0",STR_PAD_LEFT);
                        
                        $bot_tmp=explode(".",$bot);
                        $bot_h+=$bot_tmp[0];
                        $bot_m+=$bot_tmp[1];
                        
                        $eot_tmp=explode(".",$eot);
                        $eot_h+=$eot_tmp[0];
                        $eot_m+=$eot_tmp[1];
						
						$sum[$divids][$depids]['oth']+=$bot_tmp[0];
						$sum[$divids][$depids]['otm']+=$bot_tmp[1];
						
						$sum[$divids][$depids]['eoth']+=$eot_tmp[0];
						$sum[$divids][$depids]['eotm']+=$eot_tmp[1];
                        ?> 
                        <td align="right"><?php echo $bot; ?></td>
                        <td align="right"><?php echo $eot; ?></td>
                        <?php 
                    }
                    ?>
                    <td align="right"><?php echo ($bot_h+floor($bot_m/60)).".".str_pad(($bot_m%60),2,"0",STR_PAD_LEFT); ?></td>
                    <td align="right"><?php echo ($eot_h+floor($eot_m/60)).".".str_pad(($eot_m%60),2,"0",STR_PAD_LEFT); ?></td>
                    <?php
					$g_total_ot=($bot_h+floor($bot_m/60)).".".str_pad(($bot_m%60),2,"0",STR_PAD_LEFT);
					$g_total_eot=($eot_h+floor($eot_m/60)).".".str_pad(($eot_m%60),2,"0",STR_PAD_LEFT);
					
					$gbot_tmp=explode(".",$g_total_ot);
					$gbot_h+=$gbot_tmp[0];
					$gbot_m+=$gbot_tmp[1];
					
					$geot_tmp=explode(".",$g_total_eot);
					$geot_h+=$geot_tmp[0];
					$geot_m+=$geot_tmp[1];
					
                    $bot_tmp="";
                    $bot_h=0;
                    $bot_m=0;
                    
                    $eot_tmp="";
                    $eot_h=0;
                    $eot_m=0;
			} 
			?>
            </tr>
		<?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr bgcolor="#E9F3FF">
            <td width="80px" align="center" style="font-size:12px;"><b>Total</b></td>
			<?php 
            foreach( $all_dept_array as $divids=>$divdd )
            { 
                foreach( $divdd as $depids=>$depdd )
                {	
                    ?> 
                    <td style="font-size:12px;" align="right"><b><?php echo ($sum[$divids][$depids]['oth']+floor($sum[$divids][$depids]['otm']/60)).".".str_pad(($sum[$divids][$depids]['otm']%60),2,"0",STR_PAD_LEFT); ?></b></td>
                    <td style="font-size:12px;" align="right"><b><?php echo ($sum[$divids][$depids]['eoth']+floor($sum[$divids][$depids]['eotm']/60)).".".str_pad(($sum[$divids][$depids]['eotm']%60),2,"0",STR_PAD_LEFT); ?></b></td>
                    <?php 
                }
                ?>
                <td style="font-size:12px;" align="right"><b><?php echo ($gbot_h+floor($gbot_m/60)).".".str_pad(($gbot_m%60),2,"0",STR_PAD_LEFT); ?></b></td>
                <td style="font-size:12px;" align="right"><b><?php echo ($geot_h+floor($geot_m/60)).".".str_pad(($geot_m%60),2,"0",STR_PAD_LEFT); ?></b></td>
                <?php
            } 
            ?>
        </tr>
    </tfoot>
</table>
<?
//previous file delete code-----------------------------//
$html = ob_get_contents();
	ob_clean();		
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name"."####".$small_print;
	exit();
}

function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}
		
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>