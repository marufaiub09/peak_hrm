<?php
/***********************************
| Developed by	: Md. Nuruzzaman 
| Date			: 23.01.2014
***********************************/
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}

	$sql="SELECT * FROM lib_skill WHERE is_deleted = 0 ORDER BY skill_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$skill_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$skill_details[$row['id']] = mysql_real_escape_string( $row['skill_name'] );
	}
	
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
// Skill Set Report
if( $type == "skill_set_report" ) {
 	//echo $category;die;	
	if ($category=='') $category =""; else $category ="and e.category='$category'";
	if ($company_id==0) $company_id=""; else $company_id="and e.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and e.location_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and e.division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and e.department_id in ($department_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and e.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and e.subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and e.designation_id in ($designation_id)";
	if ($salary_based=="") $salary_based=""; else $salary_based="and e.salary_type_entitled='$salary_based'";	
	if ($skill_id=="") $skill_id=""; else $skill_id="and s.skill_name in ($skill_id)";
	
 	ob_start();?>
    			
	<div align="center" style="width:600px; font-size:16px"><b><? echo $company_details[$company_id]; ?></b></div>
        <table border="0" style="margin-top:5px; margin-left:20px;" width="98%" height="auto" align="center">
            <tr>
                <td colspan="18" align="center" bordercolor="#FFFFFF"><br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br />
                Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                </td>
            </tr>
        </table> 
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="550" rules="all">
        <thead> 	
            <tr>
                <th width="50"><b>Sl No</b></th>
                <th width="130"><b>Job Type</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="80"><b>No of Emp.</b></th>
                <th width="150"><b>Remarks</b></th>
            </tr>
        </thead>
    </table>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="550" rules="all" id="table_body" >
        <tbody><?	
			$sql="SELECT count(s.skill_name) as noemp, s.skill_name,group_concat(s.emp_code) as emp_code,e.designation_id
				FROM hrm_employee_skill s, hrm_employee e 
				WHERE s.status_active=1 and 
				s.current_assign=1 and
				s.emp_code=e.emp_code  
				$category 
				$company_id 
				$location_id 
				$division_id 
				$department_id 
				$section_id 
				$subsection_id
				$designation_id 
				$salary_based 
				$skill_id 
				group by e.designation_id,s.skill_name";
			//echo $sql;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$i=0;
			$sl=0;
			$aa="";
			while ($row = mysql_fetch_array($result))// EMp Master Qry starts 
			{ 
				//$aa.=$row['emp_code'].",";
				$sl++;
				$i++;
				if ($i%2==0) $bgcolor="#E9F3FF";
				else $bgcolor="#FFFFFF"; ?>
                 	
                <tr bgcolor="<? echo $bgcolor;?>" >
                	<td width="50"><? echo $i;?></td>
					<td width="130"><? echo $skill_details[$row['skill_name']];?></td>
					<td width="120"><? echo $designation_chart[$row['designation_id']];?></td>
					<td width="80" align="center"><a href="#" onclick="show_skill_details('<? echo $row['emp_code'];?>');"><? echo $row['noemp'];?></a></td>						
					<td width="150">&nbsp;</td>
				</tr><?
			}//while loop end here ?>
        </tbody>
    </table><?	
	//previous file delete code-----------------------------//
	$html = ob_get_contents();
		ob_clean();		
		foreach (glob("tmp_report_file/"."*.xls") as $filename) 
		{			
		   @unlink($filename);
		}
		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);
		
		echo "$html"."####"."$name"."####".$small_print;
		exit();
	}
	
// Employee Wise Skill Assign Report
if( $type == "emp_wise_skill_assign_report" ) {
 	//echo $category;die;	
	if ($category=='') $category =""; else $category ="and e.category='$category'";
	if ($company_id==0) $company_id=""; else $company_id="and e.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and e.location_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and e.division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and e.department_id in ($department_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and e.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and e.subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and e.designation_id in ($designation_id)";
	if ($salary_based=="") $salary_based=""; else $salary_based="and e.salary_type_entitled='$salary_based'";	
	if ($skill_id=="") $skill_id=""; else $skill_id="and s.skill_name in ($skill_id)";

 	ob_start();?>
    			
	<div align="center" style="width:800px; font-size:16px"><b><? echo $company_details[$company_id]; ?></b></div>
    
    <table border="0" style="margin-top:5px; margin-left:20px;" width="98%" height="auto" align="center">
         <tr>
            <td colspan="18" align="center" bordercolor="#FFFFFF"><br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br />
            Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
            </td>
           </tr>
    </table> 
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="750" rules="all">
        <thead> 	
            <tr>
                <th width="150"><b>Employee Name</b></th>
                <th width="130"><b>Assign Skill</b></th>
                <th width="130"><b>Unassign Skill</b></th>
                <th width="150"><b>Remarks</b></th>
            </tr>
        </thead>
    </table>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="750" rules="all" id="table_body" >
        <tbody>
		<?	
			$sql="SELECT concat(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS name, 
				group_concat(case when current_assign=1 then s.skill_name end) as assign, 
				group_concat(case when current_assign!=1 then s.skill_name end) as unassign 
				FROM hrm_employee_skill s, hrm_employee e 
				WHERE s.status_active=1 and  
				s.emp_code=e.emp_code 
				$category 
				$company_id 
				$location_id 
				$division_id 
				$department_id 
				$section_id 
				$subsection_id
				$designation_id 
				$salary_based 
				$skill_id 
				group by e.emp_code";
			//echo $sql;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$i=0;
			$sl=0;
			$aa="";
			while ($row = mysql_fetch_array($result))// EMp Master Qry starts 
			{ 
				//$aa.=$row['emp_code'].",";
				$assign=explode(",",$row['assign']);
				$unassign=explode(",",$row['unassign']);
				if(count($assign)>count($unassign)) $num_row=count($assign); else $num_row=count($unassign);
				$sl++;
				$i++;
				for($s=0;$s<$num_row;$s++)
				{
					if ($i%2==0) $bgcolor="#E9F3FF";
					else $bgcolor="#FFFFFF"; 
					
					$assign_skill_id=$assign[$s];
					$unassign_skill_id=$unassign[$s];
				?>
                <tr bgcolor="<? echo $bgcolor;?>" >
                    <?
                    if($s==0)
                    {
                    ?>
                        <td width="150" rowspan="<? echo $num_row; ?>" valign="middle"><? echo $row['name'];?></td>
                    <?
                    }
                    ?>   
                    <td width="130"><? echo $skill_details[$assign_skill_id];?></td>
                    <td width="130"><? echo $skill_details[$unassign_skill_id];?></td>
                    <td width="150">&nbsp;</td>
                </tr>
				<?
				}
			}//while loop end here ?>
        </tbody>
    </table>
	<?	
	//previous file delete code-----------------------------//
	$html = ob_get_contents();
		ob_clean();		
		foreach (glob("tmp_report_file/"."*.xls") as $filename) 
		{			
		   @unlink($filename);
		}
		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);
		
		echo "$html"."####"."$name"."####".$small_print;
		exit();
	}
?>