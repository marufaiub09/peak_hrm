<?php
/*************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 08.12.2013
**************************************/ 
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");
include("../../../../includes/array_function.php");
include("../../../../includes/common_functions.php");
extract($_GET);

//Designation array
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result )){
$designation_chart[$row['id']] = $row['custom_designation'];
}
//Company array
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$company_details[$row['id']] = $row['company_name'];
}
//Department array
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$department_details[$row['id']] = $row['department_name'];
}
//Diviion
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$division_details[$row['id']] = $row['division_name'];
}
//Location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$location_details[$row['id']] =$row['location_name'];
}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$section_details[$row['id']] = $row['section_name'];
}
//Sub Section
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$subsection_details[$row['id']] = $row['subsection_name'];
}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$lib_list_division = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$lib_list_division[$row['id']] = $row['division_name'];
}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$lib_list_district = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$lib_list_district[$row['id']] = $row['district_name'];
}
//	Training Name
$sql="SELECT * FROM  lib_training";
$result=mysql_query($sql);
$training_details=array();
while($row=mysql_fetch_assoc($result)){$training_details[$row['id']]=$row['short_name'];}
?>

<style>
a{ color:#0000FF;}
a:hover{ color:#900;}
</style>

<?php
if($type=="training_record_report")
{ 
	
	$fdate=convert_to_mysql_date($from_date);
	$tdate=convert_to_mysql_date($to_date);
	
	
	if ($category=='') $category=" and a.category in (0,1,2,3,4) "; else $category=" and a.category='$category'";
	if ($company_id=='') $company_id =""; else $company_id ="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else $department_id="and a.department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id  in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id  in ($designation_id) ";
	//if ($id_card=="") $id_card_no=""; else $id_card_no="and a.id_card_no in ($id_card_no)";
	//if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and a.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and a.salary_type_entitled='$cbo_salary_based'";
	
	//new add group by order by
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==!0)
	{
		if($group_by_id==1){$status_com=1;$groupby="a.company_id,a.emp_code,";}
		else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,a.emp_code,";}
		else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,a.emp_code,";}
		else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.emp_code,";}
		else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.emp_code,";}
		else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.emp_code,";}
		
		//if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(emp.id_card_no as SIGNED)";}	 
		else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(a.designation_level as SIGNED)";}	 
		else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(a.emp_code as SIGNED)";}	 
		else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(a.id_card_no), 5)as SIGNED)";}	 
		//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
		
		$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
		$location_arr=array();
		$division_arr=array();
		$department_arr=array();
		$section_arr=array();
		$subsection_arr=array();
		$sl=0;
	
		/*$sql="SELECT m.id,group_concat(distinct(m.training_id)),d.emp_code,a.id_card_no,CONCAT(a.first_name, ' ',a.middle_name, ' ',a.last_name) AS name,a.designation_id,a.joining_date,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id 
		FROM hrm_training_mst m, hrm_training_dtls d, hrm_employee a 
		WHERE m.training_id in($training_id) and m.id=d.id_mst and a.emp_code=d.emp_code $category $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $emp_code
		group by $dynamic_groupby $orderby";*/
	
		$sql="SELECT m.id,group_concat(distinct(m.training_id)),d.emp_code,a.id_card_no,CONCAT(a.first_name, ' ',a.middle_name, ' ',a.last_name) AS name,a.designation_id,a.joining_date,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id 
		FROM hrm_training_mst m, hrm_training_dtls d, hrm_employee a 
		WHERE m.training_id in($training_id) and m.id=d.id_mst and a.emp_code=d.emp_code $category $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $salary_based $id_card_no $emp_code and m.training_date BETWEEN '$fdate' and '$tdate'
		group by $dynamic_groupby $orderby";
		
		//echo $sql;die;
		$result=mysql_query($sql) or die(mysql_error());
		$new_section=array();	
		if(mysql_num_rows($result)==0)
		{
		echo "<p align='center' style='font-size:16px' style='color:#000'><b>No Data Found.</b></p>";
		}
		else
		{ 
			$j=1;
			while($emp=mysql_fetch_assoc($result))
			{
				if (in_array($emp['section_id'], $new_section))
				{
					$i++;
				}
				else
				{	
					$new_section[$emp['section_id']]=$emp['section_id'];
					if($j!=1) echo "</table><br />";$small_print .='</table><br />';
				?>
				<div align="left"><?php
					$sl++;
					if($sl==1)
					{
						$location_arr[$emp[location_id]]=$emp[location_id];
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
					}//end if condition of header print
					//end header print
					if($sl!=1)
					{
						$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
						if(in_array($emp[location_id],$location_arr) && $status_loc==1)
						{
							if(in_array($emp[division_id],$division_arr) && $status_divis==1)
							{
								if(in_array($emp[department_id],$department_arr) && $status_dept==1)
								{
									if(in_array($emp[section_id],$section_arr) && $status_sec==1)
									{ 
										if(in_array($emp[subsection_id],$subsection_arr) && $status_subsec==1)
										{}
										else if($status_subsec==1)
										{
										$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
										$new_subsec=1;
										}
									}
									else if($status_sec==1)
									{
										$section_arr[$emp[section_id]]=$emp[section_id];
										$subsection_arr=array();
										$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
										$new_sec=1;
										$new_subsec=1;
									}
								}
								else if($status_dept==1)
								{
									$department_arr[$emp[department_id]]=$emp[department_id];
									$section_arr=array();
									$subsection_arr=array();
									$section_arr[$emp[section_id]]=$emp[section_id];
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
								}
							}//division
							else if($status_divis==1)
							{
								$division_arr[$emp[division_id]]=$emp[division_id];
								$department_arr=array();
								$section_arr=array();
								$subsection_arr=array();
								$department_arr[$emp[department_id]]=$emp[department_id];
								$section_arr[$emp[section_id]]=$emp[section_id];
								$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
								$new_divis=1;
								$new_dept=1;
								$new_sec=1;
								$new_subsec=1;
							}//division else
						}//location
						else if($status_loc==1)
						{
							$location_arr[$emp[location_id]]=$emp[location_id];
							$division_arr=array();
							$department_arr=array();
							$section_arr=array();
							$subsection_arr=array();
							$division_arr[$emp[division_id]]=$emp[division_id];
							$department_arr[$emp[department_id]]=$emp[department_id];
							$section_arr[$emp[section_id]]=$emp[section_id];
							$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
							$new_loc=1;
							$new_divis=1;
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}//location else
					}
					//header print here 
					$c_part_1="";
					if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
					{
						if($status_loc==1)
						{					
							$c_part_1 .= "  Location :  ".$location_details[$emp[location_id]]."  ,";
						}				
						if($status_divis==1)
						{
							$c_part_1 .= "  Division :  ".$division_details[$emp[division_id]]."  ,";
						}
						if($status_dept==1)
						{
							$c_part_1 .= "  Department :  ".$department_details[$emp[department_id]]."  ,";
						}
						if($status_sec==1)
						{
							$c_part_1 .= "  Section :  ".$section_details[$emp[section_id]]."  ,";
						}
						if($status_subsec==1)
						{
							$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp[subsection_id]]."  ,";
						}
						if($c_part_1!='')
						{
							//$i=0;
							$sl=1;?>
							<? echo '<b>'.substr($c_part_1,0,-1).'</b>' ?><?php
						}
					}?>	
				</div>
				<table id="scroll_body" cellspacing="0" cellpadding="0" class="rpt_table">
				   
					<thead>                            
						<th width="50">Sl</th>
						<th width="150">Emp ID</th>
						<th width="180">Emp Name</th>
						<th width="120">Designation</th>
						<th width="120">DOJ</th>
						<th width="180">Name of Training</th>
						<th width="120">Division</th>
						<th width="120">Department</th>
						<th width="120">Section</th>
						<th width="120">Subsection</th>
					</thead><?
				}
				//new section 
				?>
					<tr bgcolor="<? echo $bgcolor; ?>">
					<td width="50"><?php echo $j; ?></td>
					<td width="150"><?php echo $emp['id_card_no']; ?></td>
					<td width="180"><?php echo '<a href="##" onclick="show_training_record('."'".$emp['emp_code']."'".')" style="cursor:pointer">'.$emp['name'].'</a>' ?></td>
					<td width="120"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
					<td width="120"><?php echo convert_to_mysql_date($emp['joining_date']); ?></td>
					<td width="180"><?php
						$trn_nm=explode(",",$emp['group_concat(distinct(m.training_id))']);
						foreach($trn_nm as $value){
						echo '<a href="##" onclick="show_training_record_by_trnm('.$value.",'".$emp['emp_code']."'".')" style="cursor:pointer">'.$training_details[$value].'</a>'." &nbsp; ";
						}?>
					</td>
					<td width="120"><?php echo $division_details[$emp['division_id']]; ?></td>
					<td width="120"><?php echo $department_details[$emp['department_id']]; ?></td>
					<td width="120"><?php echo $section_details[$emp['section_id']]; ?></td>
					<td width="120"><?php echo $subsection_details[$emp['subsection_id']]; ?></td>
					</tr><?php $j++;
				}// end while loop
				$small_print .='</table>'; ?>			
				</table><?php
		   }
				
    	}
		else
		{
		
		
	
		
		$location_arr=array();
		$division_arr=array();
		$department_arr=array();
		$section_arr=array();
		$subsection_arr=array();
		$sl=0;
	
	
		$sql="SELECT m.id,group_concat(distinct(m.training_id)),d.emp_code,a.id_card_no,CONCAT(a.first_name, ' ',a.middle_name, ' ',a.last_name) AS name,a.designation_id,a.joining_date,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id 
		FROM hrm_training_mst m, hrm_training_dtls d, hrm_employee a 
		WHERE m.training_id in($training_id) and m.id=d.id_mst and a.emp_code=d.emp_code $category $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $salary_based $id_card_no $emp_code and m.training_date BETWEEN '$fdate' and '$tdate' group by emp_code 
		";
		
		//echo $sql;die;
		$result=mysql_query($sql) or die(mysql_error());
		$new_section=array();	
		if(mysql_num_rows($result)==0)
		{
		echo "<p align='center' style='font-size:16px' style='color:#000'><b>No Data Found.</b></p>";
		}
		else
		{ 
			
			
				
				?>
			
				<table id="scroll_body" cellspacing="0" cellpadding="0" class="rpt_table">
				   
					<thead>                            
						<th width="50">Sl</th>
						<th width="150">Emp ID</th>
						<th width="180">Emp Name</th>
						<th width="120">Designation</th>
						<th width="120">DOJ</th>
						<th width="180">Name of Training</th>
						<th width="120">Division</th>
						<th width="120">Department</th>
						<th width="120">Section</th>
						<th width="120">Subsection</th>
					</thead><?
				$j=1;
			while($emp=mysql_fetch_assoc($result))
			{
				//new section 
				?>
					<tr bgcolor="<? echo $bgcolor; ?>">
					<td width="50"><?php echo $j; ?></td>
					<td width="150"><?php echo $emp['id_card_no']; ?></td>
					<td width="180"><?php echo '<a href="##" onclick="show_training_record('."'".$emp['emp_code']."'".')" style="cursor:pointer">'.$emp['name'].'</a>' ?></td>
					<td width="120"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
					<td width="120"><?php echo convert_to_mysql_date($emp['joining_date']); ?></td>
					<td width="180"><?php
						$trn_nm=explode(",",$emp['group_concat(distinct(m.training_id))']);
						foreach($trn_nm as $value){
						echo '<a href="##" onclick="show_training_record_by_trnm('.$value.",'".$emp['emp_code']."'".')" style="cursor:pointer">'.$training_details[$value].'</a>'." &nbsp; ";
						}?>
					</td>
					<td width="120"><?php echo $division_details[$emp['division_id']]; ?></td>
					<td width="120"><?php echo $department_details[$emp['department_id']]; ?></td>
					<td width="120"><?php echo $section_details[$emp['section_id']]; ?></td>
					<td width="120"><?php echo $subsection_details[$emp['subsection_id']]; ?></td>
					</tr><?php $j++;
				}// end while loop
				$small_print .='</table>'; ?>			
				</table><?php
		   }
				
    		
			
		}
		$html=ob_get_contents();		
				ob_clean();
				//for report temp file delete 
				foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
				@unlink($filename);
				}		
				//html to xls convert
				$name=time();
				$name="$name".".xls";	
				$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
				$is_created = fwrite($create_new_excel,$html);
				echo "$html"."####"."$name"."####".$small_print;		
				exit();
}// end if condition
?>