<?php

/*######################################

	Completed By
	Name : Sohel
	Date : 17-07-2014
		
######################################*/

session_start();

header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract ( $_REQUEST );

if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}



date_default_timezone_set('UTC');
include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');
/* Replace the data in these two lines with data for your db connection */
$type=$_GET["type"];

extract($_GET);
extract($_POST);

	if($type=="select_month_generate")
	{		
		extract($_GET);
		extract($_POST);
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1";//and b.is_locked=0
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}
	
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	
	$sql = "SELECT * FROM  lib_bank WHERE status_active=1 and is_deleted = 0  ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$bank_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$bank_name[$row['id']]['bank_name'] = mysql_real_escape_string( $row['bank_name'] );
		$bank_name[$row['id']]['branch_name'] = mysql_real_escape_string( $row['branch_name'] );
		$des=explode(",",mysql_real_escape_string( $row['contact_person'] ));
		$bank_name[$row['id']]['contact_person'] =$des[0]; // mysql_real_escape_string( $row['contact_person'] );
		$bank_name[$row['id']]['desig'] =$des[1];
		$bank_name[$row['id']]['address'] = mysql_real_escape_string( $row['address'] );	 
	}	


	
	$sql = "SELECT * FROM  hrm_employee_salary_bank  WHERE status_active=1 and is_deleted = 0  ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$account_number = array();
	$account_bank = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$account_number[$row['emp_code']]= mysql_real_escape_string( $row['account_no'] );
	}	
	

//print_r($account_bank);
 $sql = "SELECT * FROM   lib_bank_account  WHERE status_active=1 and is_deleted = 0  ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$account_number_company = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$account_number_company[$row['account_id']][$row['company_name']]= mysql_real_escape_string( $row['account_no'] );
				
	}

if( $action == "emp_bank_salary_report" ) //=========================================================
{	

	$rpt_company=$company_id;
	$loc_id=$location_id;

	$cbo_salary_periods=explode("_",$cbo_salary_periods);
	$cur_date=date("d-m-Y");
	$cbo_company=$company_id;
	$cbo_emp_bank_name=$cbo_emp_bank;
	$emp_bank_id=explode(",",$cbo_emp_bank);
	//if ($cbo_salary_periods=='') $cbo_salary_periods =""; else $cbo_salary_periods ="and a.salary_periods='$cbo_salary_periods[0]'";
	if ($cbo_emp_bank==0 || $cbo_emp_bank=='') $cbo_emp_bank=""; else $cbo_emp_bank="and b.lib_bank_id='$cbo_emp_bank'";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}
	if ($location_id==0 || $location_id=='') $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else	$department_id=" and  a.department_id in ($department_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="emp.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="emp.company_id,emp.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="emp.company_id,emp.location_id,emp.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(emp.id_card_no,5)";}	 
	
	$dynamic_groupby = " group by ".substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
 	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
 
	$subsection_total_amount=0;
	$section_total_amount=0;	
	$department_total_amount=0;
	$division_total_amount=0;	
	$location_total_amount=0;				
	$grand_total_amount=0;	
	//$grand_total_amount_show=0;
 
	$sl=0;
	$r=1;
	
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	if($security_id==0)
		$str_hide_salary="";
	else if( $security_id==1 )
	 	$str_hide_salary=" and c.status_salary=0";
	else if( $security_id==2 )
		$str_hide_salary= " and c.status_salary=1";	
		
 ob_start();
 
if( $cbo_emp_bank_name==1 )//SCB========================================
{
	
	 $sql = "SELECT a.salary_periods,a.emp_code as emp_code,a.id as mst_id,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,
        a.subsection_id,a.net_payable_amount,a.total_absent_deduction_amount,a.not_payable_deduction_amount,a.gross_salary,b.*,CONCAT(c.first_name, ' ',c.middle_name, ' ', c.last_name) AS name,c.emp_code,c.id_card_no,c.designation_id,c.status_salary
        FROM hrm_fraction_salary_mst a,hrm_employee_salary_bank b,hrm_employee c 
        WHERE 
        a.salary_periods like '$cbo_salary_periods[0]' and b.status_active=1 
		and a.emp_code=b.emp_code and b.emp_code=c.emp_code 
        and a.emp_status in ( 0,2 )
		$company_id 
        $location_id 
        $division_id 
		$department_id 
        $section_id 
        $subsection_id 
        $cbo_emp_bank
        $str_hide_salary group by a.emp_code order by c.designation_level";
		
        //echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$data_array=array();
		while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
		{ 
			$sl++; 
			
			$fraction_table_amount=round($row_emp['net_payable_amount']);
			$bank_table_amount=$row_emp['salary_amount'];
			
			if(  $bank_table_amount <= $fraction_table_amount )
			{
				$cash_amount=$fraction_table_amount-$bank_table_amount;
				$bank_amount=$bank_table_amount;
			}
			else
			{
				$bank_amount=$fraction_table_amount;
				$cash_amount=0;
			}
			
			if ($bank_amount > 0)
			{
				$data_array[$row_emp[emp_code]]['name']=$row_emp[name];
				$data_array[$row_emp[emp_code]]['id_card_no']=$row_emp[id_card_no];
				$data_array[$row_emp[emp_code]]['designation_id']=$row_emp[designation_id];
				$data_array[$row_emp[emp_code]]['account_no']=$row_emp[account_no];
				$data_array[$row_emp[emp_code]]['bank_ammount']=$bank_amount;
				$data_array[$row_emp[emp_code]]['month']="Salary for the month of " .$months[(int)date("m",strtotime($cbo_salary_periods[0]))]."' ".date("Y",strtotime($cbo_salary_periods[0]));
				$grand_total_amount+=$bank_amount;
			}
		}//while loop end here 

?>
<div style="width:700px;margin-top:15px;" class="page" align="center">
	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>
    <table border="0" style="margin-top:10px; margin-left:20px;"  height="auto" align="center" width="99%"  >
        <tr>
            <td align="left" style="text-align:left; font-size:15px">
           	 	Date: <? echo $cur_date; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="text-align:left; font-size:15px">
                To<br />
                <? 
                if($bank_name[$cbo_emp_bank_name]['desig']!="") 
                echo $bank_name[$cbo_emp_bank_name]['contact_person'].",<br />".$bank_name[$cbo_emp_bank_name]['desig'].",<br />".$bank_name[$cbo_emp_bank_name]['bank_name'];
                else echo $bank_name[$cbo_emp_bank_name]['contact_person'].",<br />".$bank_name[$cbo_emp_bank_name]['bank_name'];
                ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="text-align:left; font-size:14px">Branch Name # <? echo $bank_name[$cbo_emp_bank_name]['branch_name']; ?>
                <br /> Address # <? echo $bank_name[$cbo_emp_bank_name]['address']; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="text-align:left; font-size:20px" height="5"></td>
        </tr>
        <tr>
            <td align="left" style="text-align:left; font-size:15px" height="35" valign="middle" >
                <b><font size="3"><b>Subject:</b></font>&nbsp;<span style="text-decoration:underline;">Request to disburse salary for the month of  <? echo $months[(int)date("m",strtotime($cbo_salary_periods[0]))]."' ".date("Y",strtotime($cbo_salary_periods[0]));?>. </span></b>
            </td>
        </tr>
        <tr>
            <td align="left" style="text-align:left; font-size:12px">Dear Sir/Madam,</td>
        </tr>
        <tr>
            <td align="left" style="text-align:left; font-size:12px" height="50" valign="top">
            With due respect we are requesting you to please disburse the net payable salary <b>Tk.<span id="show_total"><? echo $grand_total_amount;?></span> in words:( Taka <span id="show_total_inwords"><? echo number_to_words_final($grand_total_amount); ?></span> only)</b> to the all account holders as per salary sheet from our Company Account<b> <? echo $account_number_company[$cbo_emp_bank_name][$cbo_company]; ?></b>.	
            
            </td>
        </tr>
         <tr>
				<td align="left" style="text-align:left; font-size:20px" height="5"></td>
			</tr>
            <tr>
				<td align="left" style="text-align:left; font-size:12px">Thanking you,</td>
			</tr>
            <tr>
				<td align="left" style="text-align:left; font-size:20px" height="5"></td>
			</tr>
            <tr>
				<td align="left" style="text-align:left; font-size:12px">Singnetory of Company account.</td>
			</tr>
            <tr>
				<td align="left" style="text-align:left; font-size:20px" height="30"></td>
			</tr>
            <tr>
				<td align="left" style="text-align:left; font-size:12px">...................................</td>
			</tr>
             <tr>
				<td align="left" style="text-align:left; font-size:14px"><b>(With signature & Seal)</b></td>
			</tr>
            <tr>
				<td align="left" style="text-align:left; font-size:20px" height="5"></td>
			</tr>
    </table> 
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="700" rules="all"  align="center">
        <thead> 	
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="90"><b>ID No</b></th>
                <th width="140"><b>Name</b></th>
                <th width="150"><b>Designation</b></th>
                <th width="50"><b>Net payable(tk.)</b></th>
                <th width="100"><b>A/C No</b></th>
            </tr>
        </thead>
        <tbody> 
		<? 
		
		foreach ($data_array as $row_emp)// EMp Master Qry starts 
		{ 
			if ($r%2==0) $bgcolor="#EEEEEE"; else $bgcolor="#FFFFFF";  
			?>
			<tr bgcolor="<? echo $bgcolor;?>">
                <td width="30" align="center"><? echo $r;?> </td>
                <td width="90" align="center"><? echo $row_emp['id_card_no'];?> </td>
                <td width="140"><? echo $row_emp['name'];?></td>
                <td width="150"><? echo $designation_chart[$row_emp['designation_id']];?></td>
                <td width="50"  align="right"> <? echo $row_emp['bank_ammount']; ?> </td>
                <td width="100" align="right"> &nbsp;<? echo $row_emp['account_no']; ?> &nbsp;</td>						
            </tr>
            <?
			$r++;
			//$grand_total_amount+=$bank_amount;
		}
		
	
		
		?>
		<tr  bgcolor="#CCCCCC" style="font-weight:bold">
			<td  colspan="4">&nbsp;<strong>Grand Total</strong>(In Words: ) <? echo number_to_words_final($grand_total_amount); ?></td>
			<td  align='right'> <? echo number_format($grand_total_amount);?> </td>
			<td  align='right'>&nbsp;</td>
		</tr>
    </tbody>
</table>
</div>
<p></p>
<script type="text/javascript">
	function grand_total()
	{
		var grand_total=document.getElementById('total_amount_show').value;
		//alert(grand_total);
		document.getElementById('show_total_new').innerHTML='Account Debit Advice by Tk.'+grand_total;
	}
</script>
<?	
 }
 
if($cbo_emp_bank_name==2) //Dutch Bangla Bank Limited 
{
//echo "sohel";
?>
<script type="text/javascript">
function grand_total_show()
{
	var grand_total=document.getElementById('total_amount').value;
	document.getElementById('show_total').innerHTML='Account Debit Advice by Tk.'+grand_total;
}
</script>
 <table border="0" style="margin-top:10px; margin-left:20px;"  height="auto" align="center" width="700"  >
    <tr>
    	<td align="left">Date: <? echo $cur_date; ?></td>
    </tr>
    <tr>
        <td align="left" style="text-align:left; font-size:15px">
            To<br />
            <? 
            if($bank_name[$cbo_emp_bank_name]['desig']!="") 
            echo $bank_name[$cbo_emp_bank_name]['contact_person'].",<br />".$bank_name[$cbo_emp_bank_name]['desig'].",<br />".$bank_name[$cbo_emp_bank_name]['bank_name'];
            else echo $bank_name[$cbo_emp_bank_name]['contact_person'].",<br />".$bank_name[$cbo_emp_bank_name]['bank_name'];
            ?>
        </td>
    </tr>
    <tr>
        <td align="left" style="text-align:left; font-size:10px">Branch Name # <? echo $bank_name[$cbo_emp_bank_name]['branch_name']; ?>
        	<br /> Address # <? echo $bank_name[$cbo_emp_bank_name]['address']; ?>
        </td>
    </tr>
    <tr>
    	<td align="left" style="text-align:left; font-size:20px" height="5"></td>
    </tr>
    <tr>
        <td align="left" style="text-align:left; font-size:15px" height="35" valign="middle" ><font size="3"><b>Subject:</b></font>&nbsp;Account debit advice, by TK. <span id="show_total"></span>
        </td>
    </tr>
    <tr>
    	<td align="left" style="text-align:left; font-size:12px">Dear Sir,</td>
    </tr>
    <tr>
        <td align="left" style="text-align:left; font-size:12px" height="50" valign="top">You are authorized to debit our Account No 
        <? echo $account_number_company[$cbo_company]; // echo "==".(int)date("m",strtotime($cbo_salary_periods[0]))."==" ?>
        as Salary for the Month of <? echo $months[(int)date("m",strtotime($cbo_salary_periods[0]))]."' ".date("Y",strtotime($cbo_salary_periods[0]));?> as per below statement	
        </td>
    </tr>
</table> 
<table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="700" rules="all"  align="left">
    <thead> 	
        <tr>
            <th width="30"><b>SL</b></th>
            <th width="50"><b>ID No</b></th>
            <th width="140"><b> Name</b></th>
            <th width="150"><b>Designation</b></th>
            <th width="90"><b>Section</b></th>
             <th width="80"><b>Amount(tk.)</b></th>
            <th width="90"><b> A/C No</b></th>
           
        </tr>
    </thead>
    <tbody> 
	<?  
	$sql = "SELECT a.salary_periods,a.emp_code as emp_code,a.id as mst_id,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,
		a.subsection_id,a.net_payable_amount,a.total_absent_deduction_amount,a.not_payable_deduction_amount,a.gross_salary,a.daily_gross_salary,
		b.*,CONCAT(c.first_name, ' ',c.middle_name, ' ', c.last_name) AS name,c.emp_code,c.id_card_no,c.designation_id,c.status_salary,c.cash_disbursement 
		FROM 
		hrm_fraction_salary_mst a,hrm_employee_salary_bank b,hrm_employee c  
		WHERE 
		a.salary_periods like '$cbo_salary_periods[0]' and 
		a.emp_code=b.emp_code and b.emp_code=c.emp_code
		and b.status_active=1 
		and a.emp_status in (0,2)
		$company_id
		$location_id
		$division_id
		$department_id 
		$section_id 
		$subsection_id 
		$cbo_emp_bank
		$str_hide_salary group by a.emp_code order by c.id_card_no";
	
	//echo $sql;
	
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
	{ 
		$sl++; 
		$np=$row_emp[net_payable_amount];		
		//echo $np;die;
		$total_bank_amount=0;
		$bnk_count=0;
		$bnid=0;
		foreach( $bank_amount_arr[$row_emp['emp_code']] as $key=>$val)
		{
			$bnid=$key;
			$bnk_count++;
			$total_bank_amount+=$val;
			$emp_bank_amount[$key]=$val;
		}
		for ($i=0; $i<count( $emp_bank_id ); $i++)
		{ 
			if( $bnk_count<2) 
			{ 
				if($emp_bank_id[$i]==2 )
				{  
					$bank_amount=$np; 
					//$grand_total_amount+=$np; 
					break 1;  
				}
			}
			else
			{
			  if( $emp_bank_id[$i]==2 )
			  {
				if($np>$emp_bank_amount[1])
				{
					$bank_amount=$emp_bank_amount[2];
					//$grand_total_amount+=$emp_bank_amount[2];
				}
				else
					$bank_amount="";
			  } 
			}	 
		}
		if ($bank_amount > 0)
		{ 
			if ($r%2==0) $bgcolor="#EEEEEE"; else $bgcolor="#FFFFFF";  
			?>
			<tr bgcolor="<? echo $bgcolor;?>">
				<td height="30" align="center"><? echo $r;?> </td>
				<td width="50"><? echo $row_emp['id_card_no']; ?></td>
				<td width="140"><? echo $row_emp['name'];?></td>
				<td width="150"><? echo $designation_chart[$row_emp['designation_id']];?></td>
				<td width="90"><? echo $section_details[$row_emp['section_id']];?></td>
				<td width="80" align="right">
				<? 
				/*$attn_amount=$row_emp[gross_salary]-($row_emp[total_absent_deduction_amount]+$row_emp[not_payable_deduction_amount]);
				
				//$sql_attnd_bonus="SELECT pay_amount from hrm_salary_dtls where salary_mst_id=$row_emp[mst_id] and head_type=0 and salary_head_id=8";
				//$exe_sql_attnd_bonus=mysql_db_query($DB, $sql_attnd_bonus);
				//$row_earn = mysql_fetch_array($exe_sql_attnd_bonus);
				
				$ext_pay_amt=round(($ext_over_time[$row_emp['emp_code']]*$row_emp['daily_gross_salary']),0);
				if($ext_pay_amt>0)
				{
					$ext_pay_amt=$ext_pay_amt;
				}
				else
				{
					$ext_pay_amt=0;
				}
							
				//echo $ext_pay_amt;die;	
				if($salary_dtls_info[$row_emp['mst_id']][19]>0)
				{
					$salary_dtls_info[$row_emp['mst_id']][19]=$salary_dtls_info[$row_emp['mst_id']][19];
				}
				else
				{
					$salary_dtls_info[$row_emp['mst_id']][19]=0;
				}
				//echo $salary_dtls_info[$row_emp['mst_id']][19];
				
				$gross_payable=round($attn_amount+$ext_pay_amt+$salary_dtls_info[$row_emp['mst_id']][19]);
				$sql_attnd =" SELECT  ";
				for ($i=0; $i<count($deduction_head_id); $i++)
				{
					if ($i!=count($deduction_head_id)-1)
					{
						$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN round(pay_amount) END) AS '"."sal".$deduction_head_id[$i]."', ";
					}
					else
					{
						$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN round(pay_amount) END) AS '"."sal".$deduction_head_id[$i]."' ";
					}
				}
				
				$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[mst_id] and head_type=1 order by salary_head_id";
				
				// echo $sql_attnd;die;
				$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
				$row_deduction = mysql_fetch_array($exe_sql_emp1);
						
				$total_deduction=($row_deduction['sal21']+$row_deduction['sal28']+$row_deduction['sal31']+$row_deduction['sal23']+$row_deduction['sal22']+$row_deduction['sal37']);
				$np=($gross_payable-$total_deduction);*/
				echo $bank_amount;
				?>
				</td>
				<td width="90" align="right"><? echo $account_number[$row_emp['emp_code']];?></td>						
			</tr>
			<?
			$r++;
			$grand_total_amount+=$bank_amount;
		}
		
}//while loop end here
?>
    <tr  bgcolor="#CCCCCC" style="font-weight:bold">
        <td  colspan="5">&nbsp;<strong>Grand Total</strong> (In Words: ) <? echo number_to_words_final($grand_total_amount); ?></td>
        <td  align='right'> <? echo number_format($grand_total_amount);?>
        <input type="hidden" id="total_amount" value=" <? echo number_format($grand_total_amount);?>"/>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr style="font-weight:bold; width:900">
    	<td  colspan="7" height="70" width="250"><br /><br /><br /><br />&nbsp;Authorized Signature<br /><br />Name:<br /><br />Designation:</td>
    </tr> 
    
 </tbody> 
</table>
</div>
<p></p>
<?	
$grand_total_amount_show=$grand_total_amount;	
}

	//previous file delete code-----------------------------//
	$html = ob_get_contents();
	ob_clean();		
	
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
	@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
	}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

function signeture_table_fk($report_id,$company,$width,$location)
{
	if ($width=="") $width="100%"; 
	$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company and location_id=$location");
	$signeture_list = explode('#',$signeture_allow_or_no);
	if($signeture_allow_or_no!= "")	
	{
	 echo '<table width="'.$width.'">
		<tr height="130" style="font-size:14px; font-family:Arial; font-weight:bold">
		 ';
		foreach($signeture_list as $key=>$value)
		{
			echo '<td align="center" valign="bottom" style="border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;border-bottom-style:hidden;">'.$value.'</td> ';
		}
		echo '</tr></table>';
	}
	else
	{
		echo '<table width="'.$width.'">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 ';
		 
		echo '</tr></table>';
	}
}
?>
