<?php
session_start();

// Sohel

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");
include('../../../../includes/array_function.php');

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
		
//	designation level
$sql = "SELECT DISTINCT level,system_designation FROM lib_designation WHERE is_deleted = 0 ORDER BY system_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_level = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_level[$row['level']] = mysql_real_escape_string( $row['system_designation'] );		
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}

extract($_GET);

if($type=="budgeted_manpower_report")
{
		if ($category=='') $emp_category=""; else $emp_category="and category='$category'";
		if ($category=='') $category=""; else $category="and emp_catg_id='$category'";
		if ($company_id=='') $company_id =""; else $company_id ="and company_id='$company_id'";
		if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
		if ($division_id==0) $division_id=""; else $division_id="and division_id in ($division_id)";
		if ($department_id==0) $department_id=""; else $department_id="and department_id in ($department_id)";
		if ($section_id==0) $section_id=""; else $section_id="and section_id  in ($section_id)";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";
		if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id  in ($designation_id) ";
	
		//if ($emp_code=="") $emp_code=""; else $emp_code="and  emp_code in ('".implode("','",explode(",",$emp_code))."')";
		//if ($id_card=="") $id_card_no=""; else $id_card_no="and  id_card_no in ('".implode("','",explode(",",$id_card))."')";
		
		//new add group by order by
		$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
		
		if($group_by_id==0){$groupby="emp_code,";}
		else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
		else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
		else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,emp_code,";}
		else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
		else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
		else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
		
		//echo $order_by_id; die;	
		if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(id_card_no as SIGNED)";}	 
		else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(designation_level as SIGNED)";}	 
		else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(emp_code as SIGNED)";}	 
		else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(id_card_no), 5)as SIGNED)";}	 
		//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
		
		$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
		$location_arr=array();
		$division_arr=array();
		$department_arr=array();
		$section_arr=array();
		$subsection_arr=array();
		$sl=0;
		
	$sql_emp = "SELECT id as count_id,emp_code,company_id,location_id,division_id,department_id,section_id,subsection_id,category,designation_level,designation_id FROM  hrm_employee WHERE status_active=1 
	$emp_category
	$company_id 
	$location_id 
	$division_id 
	$department_id 
	$section_id 
	$subsection_id
	$designation_id
	order by company_id,location_id,division_id,department_id,section_id,subsection_id,category,designation_level,designation_id
	";
	//echo $sql_emp;
	//group by $dynamic_groupby $orderby
	$result_emp=mysql_query($sql_emp);
	$emp_count_arr=array();
	//echo mysql_num_rows($result_emp); die;
	while ($emp = mysql_fetch_array($result_emp))
	{
		$emp_count_arr[$emp['company_id']][$emp['location_id']][$emp['division_id']][$emp['department_id']][$emp['section_id']][$emp['subsection_id']][$emp['category']][$emp['designation_level']][$emp['designation_id']] +=1;
	}
	//print_r( $emp_count_arr[1][1][1][133][1124][0]  );
	
	 $sql = "SELECT * FROM budgeted_manpower_setup WHERE is_deleted=0 and status_active=1 
				$category
				$company_id 
				$location_id 
				$division_id 
				$department_id 
				$section_id 
				$subsection_id
				$designation_id
				";
				//group by $dynamic_groupby $orderby
				$result=mysql_query($sql);
				ob_start();
				
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{?>
       
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
            <thead>
                <th>SL No.</th>
                <th>Compnay Name</th>
                <th>Location</th>
                <th>Division</th>
                <th>Department</th>
                <th>Section</th>
                <th>Subsection</th>
                <th>Employee Catg.</th>
                <th>Designation Level</th>
                <th>Designation</th>
                <th>Current Employee</th>
                <th>No. of Post </th>
                <th>Short/Surplus</th>
	   		</thead>
        <?                 
			$i=1;
			while($emp=mysql_fetch_assoc($result))
			{
				
		 /*   $sl++;
	        //header print here 
			
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$emp[section_id]]=$emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$emp[department_id]]=$emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$emp[section_id]]=$emp[section_id];
										$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$emp[division_id]]=$emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$emp[department_id]]=$emp[department_id];
									$section_arr[$emp[section_id]]=$emp[section_id];
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$emp[location_id]]=$emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
		}
		
		$c_part_1="";
		if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp[subsection_id]]."  ,";
			}
	

			if($c_part_1!='')
			{
				//$i=0;
				$sl=1;
					?><tr>
               			 <td colspan="22"><b><? echo substr($c_part_1,0,-1); ?></b></td>
                	 </tr>
               </table>       

				<?
			}		
	  ?>                 
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="table_body" >	 
	
	  <?		 
 		
	  }//end if*/
		
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
                <tr bgcolor="<? echo $bgcolor; ?>">
                    <td align="center"><? echo $i; ?></td>
                    <td align="center"><? echo $company_details[$emp['company_id']]; ?></td>
                    <td align="center"><? echo $location_details[$emp['location_id']]; ?></td>
                    <td align="center"><? echo $division_details[$emp['division_id']];?></td>
                    <td align="center"><? echo $department_details[$emp['department_id']]; ?></td>
                    <td align="center"><? echo $section_details[$emp['section_id']]; ?></td>
                    <td align="center"><? echo $subsection_details[$emp['subsection_id']]; ?></td>
                    <td align="center"><? echo $employee_category[$emp['emp_catg_id']]; ?></td>
                    <td align="center"><? echo $designation_level[$emp['desig_level_id']]; ?></td>
                    <td align="center"><? echo $designation_chart[$emp['designation_id']]; ?></td>
                    <td align="center">
					<?
						echo $emp_count_arr[$emp['company_id']][$emp['location_id']][$emp['division_id']][$emp['department_id']][$emp['section_id']][$emp['subsection_id']][$emp['emp_catg_id']][$emp['desig_level_id']][$emp['designation_id']]; 
					?>
                    </td>
                    <td align="center"><? echo $emp['no_of_post']; ?></td>
                    <td align="center">
					<? 
						echo $short_surplus=($emp_count_arr[$emp['company_id']][$emp['location_id']][$emp['division_id']][$emp['department_id']][$emp['section_id']][$emp['subsection_id']][$emp['emp_catg_id']][$emp['desig_level_id']][$emp['designation_id']]-$emp['no_of_post']);
					?>
                    </td>
                </tr>
				<?
				$i++; 
			}
		?>
        </table>
       
        <?
		}
}

?>