<?php
/***********************************
| Developed by	: Md. Nuruzzaman 
| Date			: 20.02.2014
***********************************/
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	// company
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	// location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}
	
	// division
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}
	
	// department
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	// section
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	// subsection
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	// designation
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_details[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}

	$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	
	// company details
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	//District
	$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}
	
	
//month generated
if($type=="select_month_generate")
{		
	//echo $id;
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}

//generate_division_dropdown
if($type=="generate_division_dropdown")
{		
	//echo 'su..re';
	$sql= "SELECT * FROM lib_division where location_id='$location_id' and status_active=1 and is_deleted=0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	$division_dtls=array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		//$division_dtls[$row['id']]=$row["division_name"];
		echo "<option value='".$row['id']."'>".$row["division_name"]."</option>";
	}		
	exit();
}


// generate_dept_wise_ot_eot_summary_report	
if( $action == "generate_dept_wise_ot_eot_summary_report" ) 
{ 
	$start_date=convert_to_mysql_date($from_date);
	$end_date=convert_to_mysql_date($to_date);
	$get_days=datediff(d,$start_date,$end_date);
	$all_date=createDateRangeArray($start_date,$end_date);
	//print_r( $all_date);die;
	
	if ($category_id	== '')	$category_id	= "and a.category in(0,1,2,3,4)";	else $category_id = "and a.category='$emp_category'";
	if ($company_id		== 0)	$company_id		= "";	else	$company_id	= "and a.company_id='$company_id'";
	if ($location_id	== 0) 	$location_id	= ""; 	else 	$location_id= "and a.location_id='$location_id'";
	if ($division_id	== 0 || $division_id	== '')	$division_id	= "";	else	$division_id	= "and a.division_id in($division_id)";
	if ($department_id  == 0 || $department_id	== '')	$department_id	= "";	else	$department_id	= "and a.department_id in($department_id)";
	if ($section_id		== 0 || $section_id		== '') 	$section_id		= ""; 	else 	$section_id		= "and a.section_id in($section_id)";
	if ($subsection_id	== 0 || $subsection_id	== '')	$subsection_id	= "";	else	$subsection_id	= "and a.subsection_id in($subsection_id)";
	if ($designation_id == 0 || $designation_id	== '')	$designation_id	= "";	else	$designation_id	= "and a.designation_id in($designation_id)";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="a.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,a.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,a.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,a.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.emp_code,";}
	
	//new order by
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(a.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(a.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(b.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(a.id_card_no), 5)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	ob_start();
	?>
	<table width="1300" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
    <?php
		$i=0;
		$sl=0;
		$r=0;
		
		$sql_dept="SELECT * FROM lib_department WHERE division_id=2 and status_active=1 and is_deleted=0 order by department_name ASC";
		$result_dept=mysql_query($sql_dept) or die($sql_dept . "<br />" .mysql_error());
		$all_dept=array();
		while($row_dept=mysql_fetch_assoc($result_dept))
		{
			$all_dept[$row_dept['id']]=$row_dept['department_name'];
		}
		array_push($all_dept,"Total");
		
		//===========
		$sql="SELECT * FROM hrm_attendance WHERE attnd_date between '$start_date' and '$end_date' group by emp_code";
		//echo $sql; die;
		//================
		/*$sql = "SELECT CONCAT(a.first_name, ' ', a.middle_name, ' ', a.last_name) AS name, a.id_card_no, a.emp_code,
			a.category,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.designation_id,
			a.joining_date,a.father_name,a.mother_name,a.dob,b.emp_code,
			group_concat(case when b.status_active=1 and b.is_deleted=0 and b.type=1 then b.name end) as n_name,
			group_concat(case when b.status_active=1 and b.is_deleted=0 and b.type=1 then b.relation end) as n_relation,
			group_concat(case when b.status_active=1 and b.is_deleted=0 and b.type=1 then b.ratio end) as n_ratio 
			FROM 
			hrm_employee a,hrm_employee_nominee b 
			WHERE 
			a.status_active=1 and 
			a.is_deleted=0 and
			a.emp_code=b.emp_code 	 
			$category_id
			$company_id
			$location_id  
			$division_id 
			$department_id 
			$section_id 
			$subsection_id 
			$designation_id
			$id_card_no
			$emp_code
			group by $dynamic_groupby $orderby";*/
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		//echo mysql_num_rows($result);die;
		while ($row = mysql_fetch_array($result))// EMp Master Qry starts 
		{ 
			$i++;
			$sl++;
			$r++;
			
			// employee present address
			/*$sql1="select * from hrm_employee_address where emp_code='$row[emp_code]' and address_type=0 and status_active=1 and is_deleted=0";
			$result1=mysql_query($sql1);
			$row_add=mysql_fetch_assoc($result1);
			if($row_add['house_no']=="") $house_no=""; else $house_no="House#".$row_add['house_no'].", ";
			if($row_add['road_no']=="") $road_no=""; else $road_no="Road#".$row_add['road_no'].", ";
			if($row_add['village']=="") $village=""; else $village=$row_add['village'].", ";
			if($row_add['post_code']=="") $post_code=""; else $post_code=$row_add['post_code'].", ";
			if($row_add['thana']=="") $thana=""; else $thana=$row_add['thana'].", ";
			if($row_add['mobile_no']=="") $mobile_num=""; else $mobile_num=$row_add['mobile_no'].", ";
			$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row_add['district_id']];*/	
					
			if($sl==1)//start header print
			{
				//echo "oks";die;
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];?>
				<thead> 
					<tr height="65">
						<th colspan="13" align="center" bordercolor="#FFFFFF" style="vertical-align:top;">
                            <br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br /><br />
                            Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, 
                            Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                        </th>
					</tr>
                    <tr>
                    	<th rowspan="3">Date</th>
                        <th colspan="<?php echo count($all_dept)*2;?>"><?php echo $division_details[2];?></th>
                    </tr>
                    <tr>
						<?php 
						foreach( $all_dept AS $key=>$value )
						{ 
						?>
                        <th style="font-size:12px;" colspan="2"> <?php echo $value; ?></th>
                        <?php 
						} 
						?>
                    </tr>
                    <tr>
						<?php 
						foreach( $all_dept AS $key=>$value )
						{ 
						?>
                        <th style="font-size:12px;">OT</th>
                        <th style="font-size:12px;">EOT</th>
                        <?php 
						} 
						?>
                    </tr>
					<?php 
					foreach( $all_date AS $value )
					{ 
					?>
					<tr>
                        <td><?php echo convert_to_mysql_date($value);?></td>
                        <?php 
                        foreach( $all_dept AS $key=>$value )
                        { 
                        ?>
                        <td>120</td>
                        <td>150</td>
                        <?php 
                        } 
                        ?>
					</tr>
					<?php 
					} 
                    ?>                    
				</thead>
				<tbody>
			<? 
			}//end header print
			if($sl!=1)
			{
				//echo $sl."ssss";die;
				$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				if(in_array($row[location_id],$location_arr) && $status_loc==1)
				{
					if(in_array($row[division_id],$division_arr) && $status_divis==1)
					{
						if(in_array($row[department_id],$department_arr) && $status_dept==1)
						{
							if(in_array($row[section_id],$section_arr) && $status_sec==1)
							{ 
								if(in_array($row[subsection_id],$subsection_arr) && $status_subsec==1)
								{}
								else if($status_subsec==1)
								{
								$subsection_arr[$row[subsection_id]]=$row[subsection_id];
								$new_subsec=1;
								}
							}
							else if($status_sec==1)
							{
								$section_arr[$row[section_id]]=$row[section_id];
								$subsection_arr=array();
								$subsection_arr[$row[subsection_id]]=$row[subsection_id];
								$new_sec=1;
								$new_subsec=1;
							}
						}
						else if($status_dept==1)
						{
							$department_arr[$row[department_id]]=$row[department_id];
							$section_arr=array();
							$subsection_arr=array();
							$section_arr[$row[section_id]]=$row[section_id];
							$subsection_arr[$row[subsection_id]]=$row[subsection_id];
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}
					}//division
					else if($status_divis==1)
					{
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//division else
				}//location
				else if($status_loc==1)
				{
					$location_arr[$row[location_id]]=$row[location_id];
					$division_arr=array();
					$department_arr=array();
					$section_arr=array();
					$subsection_arr=array();
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
					$new_loc=1;
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//location else
			 }//end if($sl!=1)
			//header print here 
			$c_part_1="";
			if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
			{
				if($status_loc==1)
				{					
					$c_part_1 .= "  Location :  ".$location_details[$row[location_id]]."  ,";
				}				
				if($status_divis==1)
				{
					$c_part_1 .= "  Division :  ".$division_details[$row[division_id]]."  ,";
				}
				if($status_dept==1)
				{
					$c_part_1 .= "  Department :  ".$department_details[$row[department_id]]."  ,";
				}
				if($status_sec==1)
				{
					$c_part_1 .= "  Section :  ".$section_details[$row[section_id]]."  ,";
				}
				if($status_subsec==1)
				{
					$c_part_1 .= "  Sub Section :  ".$subsection_details[$row[subsection_id]]."  ,";
				}
				if($c_part_1!='')
				{
					$i=1;
					$sl=1;
				?>
					<tr><td colspan="13" ><b><? echo substr($c_part_1,0,-1); ?></b></td></tr>
				
				<?php
				}		
			}
			
			if ($r%2==0) $bgcolor="#E9F3FF";
			else $bgcolor="#FFFFFF"; 
			?>
            <tr bgcolor="<? echo $bgcolor;?>" >
            
            </tr>
            <?
		}// end while
	?>
        </tbody>
    </table>
    <?

//previous file delete code-----------------------------//
$html = ob_get_contents();
	ob_clean();		
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name"."####".$small_print;
	exit();
}


function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}
		
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

?>