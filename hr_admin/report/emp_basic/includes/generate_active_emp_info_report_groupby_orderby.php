<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");

$separation_type=array(1=>"Resignation",2=>"Retirement",3=>"Disability",4=>"Death",5=>"Terminated",6=>"Un-noticed");
$sex=array(0=>"Male",1=>"Female");
$marital_status=array(0=>"Single",1=>"Married",2=>"Separated",3=>"Widow");
$blood_group=array(0=>"-- Select --",1=>"A+",2=>"A-",3=>"B+",4=>"B-",5=>"AB+",6=>"AB-",7=>"O+",8=>"O-");
$emp_relation=array(0=>"-- Select --",1=>"Husband",2=>"Wife",3=>"Son",4=>"Daughter");
//$ot_entitled=array(0=>"No",1=>"Yes");
//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}
	
extract($_GET);

$active_data=explode("_", $data);
//print_r($active_data);
//print $search_string;

function my_old($dob){
	$now = date('d-m-Y');
	$dob = explode('-', $dob);
	$now = explode('-', $now);
	$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
	if($now[0] < $dob[0]){
		$now[0] += $mnt[$now[1]-1];
		$now[1]--;
	}
	if($now[1] < $dob[1]){
		$now[1] += 12;
		$now[2]--;
	}
	if($now[2] < $dob[2]) return false;
	return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
}

?>
<style type="text/css">
    @page { size 8.5in 11in; margin: 0.5in; }
    div.page { page-break-after: always; background-color:#FFF;}
</style>
<?

if($type=="active_emp_list")
	{
			
		if ($category=='') $category=""; else $category="and category='$category'";
		if ($company_id=='') $company_id =""; else $company_id ="and company_id='$company_id'";
		if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
		if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
		if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";
		if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
		if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
		//if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ($emp_code)";
		if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
		if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
		
			 if ($txt_date!="")
			 {
				$txt_date=date("Y-m-d",strtotime(convert_to_mysql_date($txt_date)));
				$str_join_date=" and dob like '".$txt_date."'";
			 }
			 		
$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(id_card_no, 5)";}
	
	
	
	ob_start();
	
	//end	
		
	
		
		// echo $str_join_date;
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
				FROM hrm_employee 
				WHERE 
				is_deleted=0 and 
				status_active=1 
				$category
				$company_id 
				$location_id 
				$division_id 
				$department_id 
				$section_id 
				$subsection_id
				$designation_id	
				$emp_code
				$str_join_date
				$orderby"; 		
				//order by CAST('id_card_no' AS SIGNED) ASC";
		 //echo $sql; die;
		$result=mysql_query($sql);
		ob_start();
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{?>

    <div style="width:2150px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
            <thead>
                <th width="35" align="center"><strong>SL</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Emp Photo</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="130" align="center"><strong>Emp Name</strong></th>
                <th width="140" align="center"><strong>Company</strong></th>
                <th width="120" align="center"><strong>Designation</strong></th>
                <th width="75" align="center"><strong>DOB</strong></th>
                <th width="75" align="center"><strong>DOJ</strong></th>
                <th width="200" align="center"><strong>Address</strong></th>
                <th width="60" align="center"><strong>Sex</strong></th>
                <th width="70" align="center"><strong>Religion</strong></th>
                <th width="130" align="center"><strong>Age</strong></th>
                <th width="160" align="center"><strong>Division</strong></th>
                <th width="130" align="center"><strong>Department</strong></th>
                <th width="100" align="center"><strong>Section</strong></th>
				<th width="100" align="center"><strong>Sub Section</strong></th>
                <th width="100"><strong>OT Entitled</strong></th>
                <th width="130" align="center"><strong>Route</strong></th>
                <th><strong>Vehicle No.</strong></th>
                
	   		</thead>
       </table>
    </div>
    <div style="width:2150px;  height:280px" id="scroll_body" align="left">
        <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="table_body" >	 
        <?php                  
		$i=1;
		
		while($emp=mysql_fetch_assoc($result))
		{
			
		//new
		$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
			if(in_array($emp[location_id],$location_arr))
			{
				if(in_array($emp[division_id],$division_arr))
				{
					if(in_array($emp[department_id],$department_arr))
					{
						if(in_array($emp[section_id],$section_arr))
						{
							if(in_array($emp[subsection_id],$subsection_arr))
							{}
							else
							{
								$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
								$new_subsec=1;
							}
						}
						else
						{
							$section_arr[$emp[section_id]]=$emp[section_id];
							$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
							$new_sec=1;
							$new_subsec=1;
						}
					}
					else
					{
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}
				}//division
				else
				{
					$division_arr[$emp[division_id]]=$emp[division_id];
					$department_arr[$emp[department_id]]=$emp[department_id];
					$section_arr[$emp[section_id]]=$emp[section_id];
					$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//division else
			}//location
			else
			{
				$location_arr[$emp[location_id]]=$emp[location_id];
				$division_arr[$emp[division_id]]=$emp[division_id];
				$department_arr[$emp[department_id]]=$emp[department_id];
				$section_arr[$emp[section_id]]=$emp[section_id];
				$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//location else
			//header print here 
			if($new_com==1 && $status_com==1)
			{					
				?><tr><td colspan="20" align="left" bgcolor="#CCCCCC"><strong>Company : <? echo $company_details[$emp[company_id]][company_name]; ?></strong></td></tr><?
			}
			if($new_loc==1 && $status_loc==1)
			{					
				?><tr><td colspan="20" align="left" bgcolor="#CCCCCC"><strong>Location : <? echo $location_details[$emp[location_id]][location_name]; ?></strong></td></tr><?
			}				
			if($new_divis==1 && $status_divis==1)
			{
				?><tr><td colspan="20" align="left" bgcolor="#CCCCCC"><strong>Division : <? echo $division_details[$emp[division_id]][division_name]; ?></strong></td></tr><?
			}
			if($new_dept==1 && $status_dept==1)
			{
				?><tr><td colspan="20" align="left" bgcolor="#CCCCCC"><strong>Department : <? echo $department_details[$emp[department_id]][department_name]; ?></strong></td></tr><?
			}
			if($new_sec==1 && $status_sec==1)
			{
				?><tr><td colspan="20" align="left" bgcolor="#CCCCCC"><strong>Section : <? echo $section_details[$emp[section_id]][section_name]; ?></strong></td></tr><?
			}
			if($new_subsec==1 && $status_subsec==1)
			{
				?><tr><td colspan="20" align="left" bgcolor="#CCCCCC"><strong>Sub Section : <? echo $subsection_details[$emp[subsection_id]][subsection_name]; ?></strong></td></tr><?
			}
		//end	
			
	$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=0 and status_active=1 and is_deleted=0";
			$result1=mysql_query($sql1);
			$row=mysql_fetch_assoc($result1);
			if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
			if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
			if($row['village']=="") $village=""; else $village=$row['village'].", ";
			if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
			if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
			$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
			
			$sql2="SELECT * FROM `hrm_transport_mst` WHERE `vehicle_no` in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
			$result2=mysql_query($sql2);
			$row2=mysql_fetch_assoc($result2);
			
			$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
			$image="";
			if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
	
			if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
	?>
			<tr bgcolor="<? echo $bgcolor; ?>">
				<td width="35" align="center"><?php echo $i;?></td>
                <td width="80">&nbsp;<?php echo $emp['id_card_no'];?></td>
                <td width="80" valign="bottom" align="center"><?php echo $image; ?></td>
				<td width="80">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
				<td width="130"><?php echo $emp['name'] ; ?></td>
                <td width="140"><?php echo $company_details[$emp['company_id']]; ?></td>
				<td width="120">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
				<td width="75" align="center"><?php echo convert_to_mysql_date($emp['dob']) ; ?></td>
				<td width="75" align="center"><?php echo convert_to_mysql_date($emp['joining_date']) ; ?></td>
				<td width="200"><?php echo $address ; ?></td>
				<td width="60" align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
				<td width="70" align="center"><?php echo $emp['religion'] ; ?></td>
				<td width="130"><?php
					$birth_date=convert_to_mysql_date($emp['dob']);
					$age = my_old($birth_date);
					printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);
					?></td>
               <td width="160">&nbsp;<?php echo $division_details[$emp['division_id']]; ?></td>
               <td width="130">&nbsp;<?php echo $department_details[$emp['department_id']]; ?></td>
               <td width="100">&nbsp;<?php echo $section_details[$emp['section_id']]; ?></td>
               <td width="100">&nbsp;<?php echo $subsection_details[$emp['subsection_id']]; ?></td>
               <td width="100"><?php  if($emp['ot_entitled']==1){echo "yes";}?></td>
			   <td width="130">&nbsp;<?php echo $row2['route']; ?></td>
               <td>&nbsp;<?php echo $row2['vehicle_no']; ?></td>
			</tr>
			<?php 
			$i++;}
			
			?>
        </table>
    </div>

<?php
			
	}  
	   $html = ob_get_contents();
		ob_clean();	
		
		foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
				@unlink($filename);
		}		
		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,trim($html));	
		
		echo "$html"."####"."$name";		
		exit();
			
} //end Active Employee List if condition

function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}	
	
function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}

?>
     
	