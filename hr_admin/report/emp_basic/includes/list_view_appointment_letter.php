<?php
/********************************
| Developed by	: Ekram Hossain
********************************/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

include("../../../../includes/common.php");
extract($_GET);
extract($_POST);
// Calculate service length
function my_old($doj,$doc){
	//$now = date('d-m-Y');
	$jdate = explode('-', $doj);
	$cdate = explode('-', $doc);
	$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (($cdate[0]%400 == 0) or ($cdate[0]%4==0 and $cdate[0]%100!=0)) $mnt[2]=29;
	if($cdate[2] < $jdate[2]){
		$cdate[2] += $mnt[$cdate[1]-1];
		$cdate[1]--;
	}
	if($cdate[1] < $jdate[1]){
		$cdate[1] += 12;
		$cdate[2]--;
	}
	if($cdate[0] < $jdate[0]) return false;
	return  array('year' => $cdate[0] - $jdate[0], 'mnt' => $cdate[1] - $jdate[1], 'day' => $cdate[2] - $jdate[2]);
}

$sql = "SELECT * FROM hrm_employee_salary WHERE is_deleted = 0 and status_active=1 ORDER BY emp_code ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_info = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_info[$row['emp_code']][$row['payroll_head']] = $row['amount']; //this line explain that condition in which under you want ot geet value.the $row['emp_code'] express that which employee and [$row['payroll_head'] explains under which payroll_head condition and the =$row['amount'] explain the which value to get
}


//section_details
$sql = "SELECT * FROM lib_section";
//echo $sql;die;
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']]= mysql_real_escape_string($row['section_name']);
	}


 //employee address
$sql = "SELECT * FROM hrm_employee_address WHERE is_deleted = 0 and status_active=1 ORDER BY emp_code ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
//echo $sql;die;
$address_info = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$address_info[$row['emp_code']][$row['address_type']]['house_no'] =$row['house_no'];
	$address_info[$row['emp_code']][$row['address_type']]['road_no'] =$row['road_no'];
	$address_info[$row['emp_code']][$row['address_type']]['thana'] =$row['thana'];
	$address_info[$row['emp_code']][$row['address_type']]['village'] =$row['village'];
	$address_info[$row['emp_code']][$row['address_type']]['post_code'] =$row['post_code'];
	$address_info[$row['emp_code']][$row['address_type']]['phone_no'] =$row['phone_no'];
	$address_info[$row['emp_code']][$row['address_type']]['mobile_no'] =$row['mobile_no'];
	$address_info[$row['emp_code']][$row['address_type']]['email'] =$row['email'];
	$address_info[$row['emp_code']][$row['address_type']] ['district_id']=$row['district_id'];
	$address_info[$row['emp_code']][$row['address_type']]['house_no_bangla'] =$row['house_no_bangla'];
	$address_info[$row['emp_code']][$row['address_type']]['road_no_bangla'] =$row['road_no_bangla'];
	$address_info[$row['emp_code']][$row['address_type']]['thana_bangla'] =$row['thana_bangla'];
	$address_info[$row['emp_code']][$row['address_type']]['village_bangla'] =$row['village_bangla'];
	$address_info[$row['emp_code']][$row['address_type']]['post_code_bangla'] =$row['post_code_bangla'];
	$address_info[$row['emp_code']][$row['address_type']] ['district_id_bangla']=$row['district_id_bangla'];
	$address_info[$row['emp_code']][$row['address_type']] ['co_bangla']=$row['co_bangla'];
}
//print_r($address_info); 

//district_details
$sql = "SELECT * FROM lib_list_district WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
//echo $sql;die;
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$district_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$district_details[$row['id']]= mysql_real_escape_string($row['district_name']);
	}

//experience details
$sql = "SELECT * FROM  hrm_employee_experience WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$experience_details = array();
$experience_com = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$experience_details[$row['emp_code']]= mysql_real_escape_string($row['sevice_length']);
	$experience_com[$row['emp_code']]= mysql_real_escape_string($row['organization_name']);
	}




//nominee details
$sql = "SELECT * FROM  hrm_employee_nominee WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$nominee_details = array();
$nominee_relation = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$nominee_details[$row['emp_code']]= mysql_real_escape_string($row['name']);
	$nominee_relation[$row['emp_code']]= mysql_real_escape_string($row['relation']);
	}
	
//Education level
$sql = "SELECT * FROM  hrm_employee_education WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$education_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$education_details[$row['emp_code']]= mysql_real_escape_string($row['exam_name']);
	}


//company logo
$sql = "SELECT * FROM  lib_company  ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_name = array();
$company_logo=array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_name[$row['id']]=$row['company_name'];
	$company_logo[$row['id']]=$row['logo_location'];
	
	}

//print_r($company_logo);die;

//employee photo
$sql = "SELECT * FROM  resource_photo  ORDER BY id ASC";

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$employee_photo = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$employee_photo[$row['identifier']]=$row['location'];
	}


//print_r($employee_photo);

//Designation_details
$sql = "SELECT * FROM lib_designation  ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_details = array();
$designation_details_l = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	//$designation_details[$row['id']]= mysql_real_escape_string( $custom_designation );
	$designation_details[$row['id']]=mysql_real_escape_string($row['custom_designation']);
	$designation_details_l[$row['id']]=mysql_real_escape_string($row['custom_designation_local']);
	
	}


//division_details
$sql = "SELECT * FROM lib_division";

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']]= mysql_real_escape_string($row['division_name']);
	}

//department_details
$sql = "SELECT * FROM lib_department";

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']]= mysql_real_escape_string($row['department_name']);
	}


//subsection_details
$sql = "SELECT * FROM lib_subsection";
//echo $sql;die;
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']]= mysql_real_escape_string($row['subsection_name']);
	}
	
//print_r($subsection_details); 


//increment_details_old
$sql = "SELECT * FROM hrm_increment_mst";

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$increment_details = array();
$increment_details_issue_date= array();
$increment_details_old_grs=array();
$increment_details_old_base=array();
$increment_details_old_house=array();
$increment_details_old_medi= array();
$increment_details_new_gross=array();
$increment_mst_id=array();
while( $row = mysql_fetch_array( $result ) ) {
	$increment_details[$row['emp_code']]= mysql_real_escape_string($row['effective_date']);
	$increment_details_old_grs[$row['emp_code']]= mysql_real_escape_string($row['old_gross_salary']);
	$increment_details_old_base[$row['emp_code']]= mysql_real_escape_string($row['old_basic_salary']);
	$increment_details_old_house[$row['emp_code']]= mysql_real_escape_string($row['old_house_rent']);
	$increment_details_old_medi[$row['emp_code']]= mysql_real_escape_string($row['old_medical_allowance']);
	$increment_details_new_gross[$row['emp_code']]= mysql_real_escape_string($row['new_gross_salary']);
	$increment_mst_id[$row['emp_code']]= mysql_real_escape_string($row['id']);
	$increment_details_issue_date[$row['emp_code']]= mysql_real_escape_string($row['issue_date']);
	}

/*if($increment_mst_id!="")//this condition is added for if the increment is not done then other letter is print
{*/

if ($increment_mst_id=="") $increment_mst_id=""; else $increment_mst_id="and b.mst_id in ('".implode("','",explode(",",$increment_mst_id))."')";

//$increment_mst_id_val=implode(",",$increment_mst_id);

$sql = "SELECT a.emp_code, b.payroll_head, b.amount FROM hrm_increment_mst a,hrm_increment_dtls b where  a.is_approved=2 and a.id=b.mst_id and b.payroll_head in(1,2,3)  $increment_mst_id";
//echo $sql;die;

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$increment_details_new_basic=array();
$increment_details_new_house=array();
$increment_details_new_medical=array();
while( $row = mysql_fetch_array( $result ) ) 
{
	$increment_details_new_basic[$row['emp_code']][$row['payroll_head']] = $row['amount'];
	$increment_details_new_house[$row['emp_code']][$row['payroll_head']] = $row['amount'];
	$increment_details_new_medical[$row['emp_code']][$row['payroll_head']] = $row['amount'];
}

//}
//print_r($increment_details_new_basic);die;


//this lines remove

$sql = "SELECT emp_code,salary_periods,gross_salary FROM  hrm_salary_mst where salary_periods='2013-11-01'";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$grs_arr=array();
while( $row = mysql_fetch_array( $result ) ) {
	
	$grs_arr[$row['emp_code']]=($row['gross_salary']);
	//$grs_arr[$row['emp_code']][$row['salary_periods']] = $row['gross_salary'];
}
//print_r($grs_arr);die;

$sql = "SELECT emp_code,salary_periods,gross_salary FROM  hrm_salary_mst where salary_periods='2013-12-01'";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$grs_arr_des=array();
while( $row = mysql_fetch_array( $result ) ) {
	
	$grs_arr_des[$row['emp_code']]=($row['gross_salary']);
	//$grs_arr[$row['emp_code']][$row['salary_periods']] = $row['gross_salary'];
}



$sql ="SELECT * FROM  hrm_separation";
//$sql ="SELECT max(separated_from) as separated_from,emp_code FROM  hrm_separation";
//echo $sql;die;
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$sepa_details=array();
while( $row = mysql_fetch_array( $result ) ) {
	
	//$seperate_emp[$row['emp_code']]=$row['separated_from'];
	$sepa_details[$row['emp_code']]= mysql_real_escape_string($row['separated_from']);
}

//print_r($seperate_emp);

//new employee promotion

$sql = "SELECT * from hrm_employee_promotion order by id asc";
//echo $sql;die;

$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$emp_promotion_details=array();
while( $row = mysql_fetch_array( $result ) ) 
{
	$emp_promotion_details[$row['emp_code']]['new_designation'] = $row['new_designation'];
	$emp_promotion_details[$row['emp_code']]['last_designation'] = $row['last_designation'];
	$emp_promotion_details[$row['emp_code']]['effective_date'] = $row['effective_date'];
	
	
}

//print_r($emp_promotion_details);die;

if($type==1)
{
	
	$data=explode("_",trim($search_string));
	$emp_code_explode=explode(",",$data[3]);
	
	for($i=0;$i<count($emp_code_explode);$i++)
	{
		//echo $emp_code_explode[$i];
	?>

	<div  align="left">
   	   <!-- <style type="text/css" media="print">
       		div{ page-break-after: always;}
    	</style>-->
	<?php
		ob_start();
		
		mysql_query( "SET CHARACTER SET utf8" );
		mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );

//leave_policy
   $sql_leave_bl="select * from hrm_leave_balance where emp_code='$emp_code_explode[$i]' and status_active=1 and is_deleted=0";
   //echo  $sql_leave_bl;die;
   $result_leave_bl=mysql_query($sql_leave_bl);
		  
		  $leave_lm_cl=0;$leave_la_cl=0;$leave_bl_cl=0;
		  $leave_lm_sl=0;$leave_la_sl=0;$leave_bl_sl=0;
		  $leave_lm_lwp=0;$leave_la_lwp=0;$leave_bl_lwp=0;
		  $leave_lm_rl=0;$leave_la_rl=0;$leave_bl_rl=0;
		  $leave_lm_spl=0;$leave_la_spl=0;$leave_bl_spl=0;
		  $leave_lm_el=0;$leave_la_el=0;$leave_bl_el=0;
		  $leave_lm_edl=0;$leave_la_edl=0;$leave_bl_edl=0;
		  while($row_leave_bl=mysql_fetch_assoc($result_leave_bl))
		  {
		  	if($row_leave_bl[leave_type]=="CL") {
			  $leave_lm_cl=$row_leave_bl[leave_limit];
			  $leave_la_cl=$row_leave_bl[leave_availed];
			  $leave_bl_cl=$row_leave_bl[balance];
			  }
			 // echo $leave_lm_cl."-". $leave_la_cl."-".$leave_bl_cl;
		  	if($row_leave_bl[leave_type]=="SL") {
			  $leave_lm_sl=$row_leave_bl[leave_limit];
			  $leave_la_sl=$row_leave_bl[leave_availed];
			  $leave_bl_sl=$row_leave_bl[balance];
			  }
		  	if($row_leave_bl[leave_type]=="LWP") {
			  $leave_lm_lwp=$row_leave_bl[leave_limit];
			  $leave_la_lwp=$row_leave_bl[leave_availed];
			  $leave_bl_lwp=$row_leave_bl[balance];
			  }
			   if($row_leave_bl[leave_type]=="RL") {
			  $leave_lm_rl=$row_leave_bl[leave_limit];
			  $leave_la_rl=$row_leave_bl[leave_availed];
			  $leave_bl_rl=$row_leave_bl[balance];
			  }
		  	if($row_leave_bl[leave_type]=="SpL") {
			  $leave_lm_spl=$row_leave_bl[leave_limit];
			  $leave_la_spl=$row_leave_bl[leave_availed];
			  $leave_bl_spl=$row_leave_bl[balance];
			  }
			if($row_leave_bl[leave_type]=="EL") {
			  $leave_lm_el=$row_leave_bl[leave_limit];
			  $leave_la_el=$row_leave_bl[leave_availed];
			  $leave_bl_el=$row_leave_bl[balance];
			  }
			 if($row_leave_bl[leave_type]=="EdL") {
			  $leave_lm_edl=$row_leave_bl[leave_limit];
			  $leave_la_edl=$row_leave_bl[leave_availed];
			  $leave_bl_edl=$row_leave_bl[balance];
			  }
		  }
		
		
		//$sql_emp_sql = "SELECT * FROM  hrm_employee   where  id='$data[3]' order by emp_code";
		$sql_emp_sql = "SELECT * FROM  hrm_employee   where  emp_code='$emp_code_explode[$i]' order by id";
	
 	 	//echo $sql_emp_sql;die;	
		$sql_emp=mysql_query($sql_emp_sql);
		$emp = mysql_fetch_array($sql_emp);
		$sql_emp_sql2 = "SELECT * from hrm_dynamic_letter where  company_name=$data[0] and report_name=$data[1] and report_template=$data[2] order by id ASC";
		
		$sql_emp2=mysql_query($sql_emp_sql2);
		while($selectResult = mysql_fetch_array($sql_emp2))
		{ 
			mysql_query( "SET CHARACTER SET utf8" );
			mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
			$dob=$emp[dob];
			$dob_split=explode("-",$dob);
			$dob_1=$dob_split[0];	
			
			$joining_date=$emp[joining_date];
			$joining_date_split=explode("-",$joining_date);
			$joining_date_1=$joining_date_split[0];
			$age=(($joining_date_1*1)-($dob_1*1));
			
			$print_number=$selectResult[print_number]+1; //number of page print generate
	
			$name=" ".$emp[first_name]." ".$emp[middle_name]." ".$emp[last_name]." ";
		
			$raw_data=str_replace("EMPNAMEEMPNAME",$name,$selectResult[letter_body_text]); //replace EMPNAMEEMPNAME  with employee name
			$raw_data=str_replace("RFCODERFCODE"," ".$emp[punch_card_no]." ",$raw_data); //replace RFCODERFCODE  with employee punch card number
			$raw_data=str_replace("JOINDATEJOINDATE"," ".convert_to_mysql_date($emp[joining_date])." ",$raw_data); //replace JOINDATEJOINDATE  with employee joining date
			$raw_data=str_replace("SECTIONSECTION"," ".$section_details[$emp['section_id']]." ",$raw_data);   //REPLACE SECTIONSECTION  WITH EMPLOYEE SECTION
			$raw_data=str_replace("SALARYGRSALARYGR"," ".$emp[salary_grade]." ",$raw_data); //REPLACE SALARYGRSALARYGR  WITH EMPLOYEE SALARY GRADE
			$raw_data=str_replace("GRSALGRSAL"," ".round($emp[gross_salary])." ",$raw_data); //REPLACE  GRSALGRSAL  with employee gross salary
			$raw_data=str_replace("DESIGDESIG"," ".$designation_details[$emp['designation_id']]." ",$raw_data); //REPLACE DESIGDESIG  WITH EMPLOYEE DESIGNATION
			$raw_data=str_replace("EMPCODEEMPCODE"," ".$emp[emp_code]." ",$raw_data); //REPLACE EMPCODEEMPCODE WITH EMPLOYEE emp_code
			$raw_data=str_replace("BASICSALBASICSAL"," ".round($salary_info[$emp['emp_code']][1])." ",$raw_data); //REPLACE BASICSALBASICSAL  WITH EMPLOYEE basic salary
			$raw_data=str_replace("HRENTHRENT"," ".round($salary_info[$emp['emp_code']][2])." ",$raw_data); //REPLACE HRENTHRENT  WITH EMPLOYEE HOUSE RENT
			$raw_data=str_replace("MALLOWMALLOW"," ".round($salary_info[$emp['emp_code']][3])." ",$raw_data); //REPLACE MALLOWMALLOW WITH EMPLOYEE MEDICAL  Allowance 
			$raw_data=str_replace("OTHALLOWOTHALLOW"," ".round($salary_info[$emp['emp_code']][4])." ",$raw_data); //REPLACE OTHALLOWOTHALLOW WITH EMPLOYEE OTHER  Allowance 
			$raw_data=str_replace("BLNAMEBLNAME"," ".$emp[full_name_bangla]." ",$raw_data); //replace BLNAMEBLNAME  with employee NAME IN BANGLA
			$raw_data=str_replace("CRDATECRDATE"," ".convert_to_mysql_date(date("Y-m-d"))." ",$raw_data); //replace CRDATECRDATE with CURRENT DATE
			$raw_data=str_replace("CONDATECONDATE"," ".convert_to_mysql_date($emp[confirmation_date])." ",$raw_data); //replace CONDATECONDATE  with employee confirmation date
			$raw_data=str_replace("DTBIRTHDTBIRTH"," ".convert_to_mysql_date($emp[dob])." ",$raw_data);   //REPLACE DTBIRTHDTBIRTH WITH EMPLOYEE DATE OF BIRTH
			$raw_data=str_replace("EMPAGEEMPAGE"," ".$age." "."Years",$raw_data);   //REPLACE EMPAGEEMPAGE WITH EMPLOYEE PRESENT AGE
			$raw_data=str_replace("NATIONNATION"," ".$emp[nationality]." ",$raw_data);   //REPLACE NATIONNATION WITH EMPLOYEE NATIONALITY
			$raw_data=str_replace("RELEGIORELEGIO"," ".$emp[religion]." ",$raw_data);   //REPLACE RELEGIORELEGIO  WITH EMPLOYEE RELEGION
			$raw_data=str_replace("BLGROUPBLDROUP"," ".$emp[blood_group]." ",$raw_data);   //REPLACE BLGROUPBLDROUP  WITH EMPLOYEE BLOOOD GROUP
			
			$raw_data=str_replace("EDULEVELEDULEVEL"," ".$education_details[$emp['emp_code']]." ",$raw_data);   //REPLACE EDULEVELEDULEVEL WITH EMPLOYEE EDUCATION LEVEL
			$raw_data=str_replace("PTEMPPTEMP"," "."<img src='../../../".$employee_photo[$emp[emp_code]]."' height='120' width='100' />"." ",$raw_data);   //REPLACE PTEMPPTEMP WITH EMPLOYEE PHOTO
			$raw_data=str_replace("CMLOGCMLOG"," "."<img src='../../../".$company_logo[$emp[company_id]]."' height='60' width='60' />"." ",$raw_data);   //REPLACE CMLOGCMLOG WITH company LOGO
			$perm_address="House No: ".$address_info[$emp['emp_code']][1]['house_no'].", Road No: ".$address_info[$emp['emp_code']][1]['road_no'].",Village :".							                        $address_info[$emp['emp_code']][1]['village'].",Thana:".$address_info[$emp['emp_code']][1]['thana'].",Post Code:".$address_info[$emp[                              'emp_code']][1]['post_code'].",District:".$district_details[$address_info[$emp['emp_code']][1]['district_id']];
			$raw_data=str_replace("PRADDPRADD"," ".$perm_address." ",$raw_data);   //REPLACE PRADDPRADD WITH EMPLOYEE permanent ADDRESS
			
			$present_add="House No: ".$address_info[$emp['emp_code']][0]['house_no'].", Road No: ".$address_info[$emp['emp_code']][0]['road_no'].",Village :".							                        $address_info[$emp['emp_code']][0]['village'].",Thana:".$address_info[$emp['emp_code']][0]['thana'].",Post Code:".$address_info[$emp[                              'emp_code']][0]['post_code'].",District:".$district_details[$address_info[$emp['emp_code']][1]['district_id']];
			$raw_data=str_replace("RSADDRSADD"," ".$present_add." ",$raw_data);   //REPLACE RSADDRSADD WITH EMPLOYEE  present ADDRESS
			$raw_data=str_replace("EXPEEXPE"," ".$experience_details[$emp['emp_code']]." ",$raw_data); //replaces EXPEEXPE  employee experience 
			
			//echo $raw_data; 
			$raw_data=str_replace("GRSALINWORD"," ".number_to_words_taka($emp[gross_salary])." ",$raw_data); //replaces GRSALINWORD  employee gross 	                   salary in WORD
					
			 $raw_data=str_replace("JOINDATEBANGLA"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($emp[joining_date])."</font>"." ",$raw_data);
																								//replace JOINDATEBANGLA  with employee joining date IN BANGLA
			
			$raw_data=str_replace("CRDATEBANGLA"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date(date("Y-m-d"))."</font>"." ",$raw_data); //replace           CRDATEBANGLA with CURRENT DATE	IN BANGLA	
			
			
			$raw_data=str_replace("BANGLACONFIRM"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($emp[confirmation_date])."</font>"." ",$raw_data); 		     													 //replace BANGLACONFIRM  with employee confirmation date	in bangla	
				
			$raw_data=str_replace("DTBIRTHBANGLA"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($emp[dob])."</font>"." ",$raw_data);         	       																	//REPLACE DTBIRTHBANGLA WITH EMPLOYEE DATE OF BIRTH in bangla
			
			
			$raw_data=str_replace("BANGLABASICSAL"," "."<font style='font-family:Kalpurush ANSI'>".round($salary_info[$emp['emp_code']][1])."</font>"." ",$raw_data); 					                                                                    //REPLACE      BANGLABASICSAL  WITH EMPLOYEE basic salary in bangla
			
			$raw_data=str_replace("GROSSSALBANGLAWORD"," "."<font style='font-family:Kalpurush ANSI'>".number_to_words_bengali($emp[gross_salary], "টাকা", "পয়সা")."</font>"." ",$raw_data);
			
			$raw_data=str_replace("GRSALBANGNUM"," "."<font style='font-family:Kalpurush ANSI'>".round($emp[gross_salary])."</font>"." ",$raw_data);   		                                                          //REPLACE  GRSALBANGNUM    with employee gross salary IN BANGLA number
			$raw_data=str_replace("GROSSSALENGWORD"," ".number_to_words_final($emp[gross_salary])." ",$raw_data);//REPLACE  GROSSSALENGWORD  with employee gross salary IN ENGLISH  WORD
			
			$raw_data=str_replace("BANGLAHSRENT"," "."<font style='font-family:Kalpurush ANSI'>".round($salary_info[$emp['emp_code']][2])."</font>"." ",$raw_data);	                                                         //REPLACE 	 BANGLAHSRENT  WITH EMPLOYEE HOUSE RENT IN BANGLA
			
			$raw_data=str_replace("MEDIALLOWBAN"," "."<font style='font-family:Kalpurush ANSI'>".round($salary_info[$emp['emp_code']][3])."</font>"." ",$raw_data);                                                                                 //REPLACE    MEDIALLOWBAN WITH EMPLOYEE MEDICAL  Allowance IN bangla
			
			$raw_data=str_replace("OTHRALLOWBAN"," "."<font style='font-family:Kalpurush ANSI'>".round($salary_info[$emp['emp_code']][4])."</font>"." ",$raw_data);  	                                                      //REPLACE    OTHRALLOWBAN  WITH EMPLOYEE OTHER  Allowance IN bangla
		
		
			$raw_data=str_replace("PRINTNUM"," ".$print_number." ",$raw_data); //replace PRINTNUM with   print number  of every print
			//$raw_data=str_replace("PRINTNUMBANGLA"," "."<font style='font-family:Kalpurush ANSI'>".$print_number."</font>"." ",$raw_data); //replace PRINTNUMBANGLA with   print number  of every print bangla
			$raw_data=str_replace("BANGLAPRI"," "."<font style='font-family:Kalpurush ANSI'>".$print_number."</font>"." ",$raw_data);//replace //replace PRINTNUMBANGLA with   print number  of every print bangla with   print number  of every print bangla
			
			$raw_data=str_replace("EMPAGEBANGLA"," "."<font style='font-family:Kalpurush ANSI'>".$age."</font>"." "."বছর",$raw_data);   //REPLACE EMPAGEBANGLA WITH EMPLOYEE PRESENT AGE IN BANGLA
			
			$raw_data=str_replace("IDCARDNOIDCARDNO"," ".$emp[id_card_no]." ",$raw_data); //replace IDCARDNOIDCARDNO  with employee  id card number
			$raw_data=str_replace("CMPNAMECMPNAME"," ".$company_name[$emp[company_id]]." ",$raw_data); //replace CMPNAMECMPNAME  with employee  company name
			
			$perm_addresses="বাড়ি নং : ".$address_info[$emp['emp_code']][1]['house_no_bangla'].", রোড নং : ".$address_info[$emp['emp_code']][1]['road_no_bangla'].",গ্রাম :".							                        $address_info[$emp['emp_code']][1]['village_bangla'].",থানা:".$address_info[$emp['emp_code']][1]['thana_bangla'].", পোস্ট কোড :".$address_info[$emp[                              'emp_code']][1]['post_code_bangla'].", জেলা :".$address_info[$emp['emp_code']][1]['district_id_bangla'];
			$raw_data=str_replace("PERMANENTPERMANENT"," ".$perm_addresses." ",$raw_data); //replace permanent in bangla
			
			$present_address="বাড়ি নং : ".$address_info[$emp['emp_code']][0]['house_no_bangla'].", রোড নং : ".$address_info[$emp['emp_code']][0]['road_no_bangla'].",গ্রাম :".							                        $address_info[$emp['emp_code']][0]['village_bangla'].",থানা: ".$address_info[$emp['emp_code']][0]['thana_bangla'].",পোস্ট কোড :".$address_info[$emp[                              'emp_code']][0]['post_code_bangla'].",জেলা :".$address_info[$emp['emp_code']][0]['district_id_bangla'];
			$raw_data=str_replace("PRESENTPRESENT"," ".$present_address." ",$raw_data); //replace present address in bangla
			
			$raw_data=str_replace("FATHERNAMEBANGLA"," ".$emp[father_name_bangla]." ",$raw_data);   //REPLACE FATHERNAMEBANGLA  WITH EMPLOYEE father name bangla
			$raw_data=str_replace("MOTHERNAMEBANGLA"," ".$emp[mother_name_bangla]." ",$raw_data);   //REPLACE MOTHERNAMEBANGLA  WITH EMPLOYEE mother name bangla
			$raw_data=str_replace("DESIGBANGLA"," ".$designation_details_l[$emp['designation_id']]." ",$raw_data); //REPLACE DESIGDESIGBANGLA  WITH EMPLOYEE DESIGNATION in bangla
			
			//$raw_data=str_replace("DIVIBANDIVIBAN"," "."<font style='font-family:Kalpurush ANSI'>".$division_details[$emp['division_id']]."</font>"." ",$raw_data);
											//Replaces DIVIBANDIVIBAN with employee division
			
			$raw_data=str_replace("DIVENGDIVENG"," ".$division_details[$emp['division_id']]." ",$raw_data);
											//Replaces DIVENGDIVENG with employee division in english
			
			$raw_data=str_replace("DEPTENGDEPTENG"," ".$department_details[$emp['department_id']]." ",$raw_data);
											//Replaces DEPTENGDEPTENG with employee department in  engli
		
		//$raw_data=str_replace("DIVIBANDIVIBAN"," "."<font style='font-family:Kalpurush ANSI'>".$division_details[$emp['division_id']]."</font>"." ",$raw_data);
											//Replaces DIVIBANDIVIBAN with employee division 
				
		 $raw_data=str_replace("SUBSECSUBSEC"," ".$subsection_details[$emp['subsection_id']]." ",$raw_data);
											//Replaces SUBSECSUBSEC with employee subsection or line
		
		 $raw_data=str_replace("INCREDATEDATE"," ".convert_to_mysql_date($increment_details[$emp['emp_code']])." ",$raw_data);
											//Replaces INCREDATEDATE with employee effective date
		
		$raw_data=str_replace("INCREBANGDATEBANGDATE"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($increment_details[$emp['emp_code']])."</font>"." ",$raw_data);         	       			//REPLACE INCREBANGDATEBANGDATE WITH EMPLOYEE increment effective in bangla	
			
		 
		
		 $raw_data=str_replace("OLDBASEOLDBASE"," ".round($increment_details_old_base[$emp['emp_code']])." ",$raw_data);	
								//Replaces OLDBASEOLDBASE with employee old basic salary
								
		$raw_data=str_replace("OLDBASEBANGOLDBASEBANG"," "."<font style='font-family:Kalpurush ANSI'>".round($increment_details_old_base[$emp['emp_code']])."</font>"." ",$raw_data); 						
							//Replaces OLDBASEBANGOLDBASEBANG with employee old basic salary
		 
		 $raw_data=str_replace("OLDHOUSEHOUSE"," ".round($increment_details_old_house[$emp['emp_code']])." ",$raw_data);	
								//Replaces OLDHOUSEHOUSE with employee old house rent
								
								
		$raw_data=str_replace("OLDHOUSEBANGOLDHOUSEBANG"," "."<font style='font-family:Kalpurush ANSI'>".round($increment_details_old_house[$emp['emp_code']])."</font>"." ",$raw_data); 						
							//Replaces OLDHOUSEBANGOLDHOUSEBANG with employee old house rent IN BANGLA
			
		 $raw_data=str_replace("OLDMEDIOLDMEDI"," ".round($increment_details_old_medi[$emp['emp_code']])." ",$raw_data);	
								//Replaces OLDMEDIOLDMEDI with employee old  medical allowance
								
		 $raw_data=str_replace("OLDMEDIBANGOLDMEDIBANG"," "."<font style='font-family:Kalpurush ANSI'>".round($increment_details_old_medi[$emp['emp_code']])."</font>"." ",$raw_data); 	
								//Replaces OLDMEDIBANGOLDMEDIBANG with employee old  medical allowance in bangla
			
		
		 $raw_data=str_replace("OLDTOTSALOLDTOTSAL"," ".round($increment_details_old_grs[$emp['emp_code']])." ",$raw_data);	
					//Replaces OLDTOTSALOLDTOTSAL with employee total old  salary 	
		
		$raw_data=str_replace("OLDTOTBANGOLDTOTBANG"," "."<font style='font-family:Kalpurush ANSI'>".round($increment_details_old_grs[$emp['emp_code']])."</font>"." ",$raw_data);
				//Replaces OLDTOTSALOLDTOTSAL with employee total old  salary IN BANGLA 	
		
		//new breakdown start 
		$raw_data=str_replace("NEWBASENEWBASE"," ".round(($increment_details_new_basic[$emp['emp_code']][1]+$increment_details_old_base[$emp['emp_code']]))." ",$raw_data);
		//+$salary_info[$emp['emp_code']][1] cut after $increment_details_new_basic[$emp['emp_code']][1] change on 2013-08-27 for ismail 
					//Replaces NEWBASENEWBASE with employee new basic
		
		$raw_data=str_replace("NEWBASEBANGLANEWBASEBANGLA"," "."<font style='font-family:Kalpurush ANSI'>".round(($increment_details_new_basic[$emp['emp_code']][1]+$increment_details_old_base[$emp['emp_code']]))."</font>"." ",$raw_data);
		//+$salary_info[$emp['emp_code']][1] cut after $increment_details_new_basic[$emp['emp_code']][1]  
				//Replaces NEWBASEBANGLANEWBASEBANGLA with employee new basic in bangla
				
		$raw_data=str_replace("NEWHOUSEHOUSE"," ".round(($increment_details_new_house[$emp['emp_code']][2]+$increment_details_old_house[$emp['emp_code']]))." ",$raw_data);
		//+$salary_info[$emp['emp_code']][2] cut after$increment_details_new_house[$emp['emp_code']][2]
				//Replaces NEWHOUSEHOUSE with employee new house
				
		$raw_data=str_replace("NEWHOUSEBANGLABANGLANEWHOUSE"," "."<font style='font-family:Kalpurush ANSI'>".round(($increment_details_new_house[$emp['emp_code']][2]+$increment_details_old_house[$emp['emp_code']]))."</font>"." ",$raw_data);
		//+$salary_info[$emp['emp_code']][2] cut after$increment_details_new_house[$emp['emp_code']][2] 
				//Replaces NEWHOUSEBANGLABANGLANEWHOUSE with employee new house in bangla
		
		$raw_data=str_replace("NEWMDCAMDLCAL"," ".round($increment_details_new_medical[$emp['emp_code']][3])." ",$raw_data);
				//+$salary_info[$emp['emp_code']][3] cut after $increment_details_new_medical[$emp['emp_code']][3]
				//Replaces NEWMDCAMDLCAL with employee new medical
				
		$raw_data=str_replace("NEWMDCALBANGNEWMDCALBANG"," "."<font style='font-family:Kalpurush ANSI'>".round(($increment_details_new_medical[$emp['emp_code']][3]+$salary_info[$emp['emp_code']][3]))."</font>"." ",$raw_data);		
			//Replaces NEWMDCALBANGNEWMDCALBANG with employee new medical in bangla
			
			$raw_data=str_replace("NEWTOTSALNEWTOTSAL"," ".round($increment_details_new_gross[$emp['emp_code']])." ",$raw_data);	
			//Replaces NEWTOTSALNEWTOTSAL with employee total new  salary 
					
			$raw_data=str_replace("NEWTOTBANGNEWTOTBANG"," "."<font style='font-family:Kalpurush ANSI'>".round($increment_details_new_gross[$emp['emp_code']])."</font>"." ",$raw_data);//Replaces NEWTOTBANGNEWTOTBANG with employee total NEW  salary IN BANGLA 
		
							
			 $raw_data=str_replace("ISSUDATEISSUDATE"," ".convert_to_mysql_date($increment_details_issue_date[$emp['emp_code']])." ",$raw_data);	
			 //Replaces ISSUDATEISSUDATE with employee increment letter issue date english	
	
			$raw_data=str_replace("ISSUBANGISSUEBANG"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($increment_details_issue_date[$emp['emp_code']])."</font>"." ",$raw_data); 
			//Replaces ISSUBANGISSUEBANG with employee increment letter issue date	 bangla
			
			
			 $raw_data=str_replace("INSLASTIDINCLASTID"," ".($increment_mst_id[$emp['emp_code']])." ",$raw_data);	
			 //Replaces INSLASTIDINCLASTID with employee increment REF NUMBER
			 
			 
			 $raw_data=str_replace("PREDTPREDT"," ".substr((convert_to_mysql_date($increment_details[$emp['emp_code']])),3,9)." ",$raw_data);	
			 //Replaces PREDTPREDT with employee increment REF DATE NUMBER		

			 $raw_data=str_replace("FROMDATERANGE"," ".$data[4]." ",$raw_data);	
			 //Replaces FROMDATERANGE with employee leave from date
			 
			 $raw_data=str_replace("TODATERANGE"," ".$data[5]." ",$raw_data);	
			 //Replaces TODATERANGE with employee leave to date
			
			 $date_range=datediff( 'd',$data[4],$data[5]);
			 $raw_data=str_replace("DAYCALCULATION"," ".$date_range." ",$raw_data);	
			 //Replaces DAYCALCULATION with employee fromdate to todate day calculation
			 
			 $raw_data=str_replace("LEAVELIMITCL"," ".$leave_lm_cl." ",$raw_data);	
			 //Replaces LEAVELIMITCL with employee CL leave limit
			 
			 $raw_data=str_replace("LEAVELIMITSL"," ".$leave_lm_sl." ",$raw_data);	
			 //Replaces LEAVELIMITSL with employee SL leave limit
			 
			 $raw_data=str_replace("LEAVELIMITSPL"," ".$leave_lm_spl." ",$raw_data);	
			 //Replaces LEAVELIMITSPL with employee SPL leave limit

			 $raw_data=str_replace("LEAVEENJOYEDCL"," ".$leave_la_cl." ",$raw_data);	
			 //Replaces LEAVEENJOYEDCL with employee CL leave enjoyed
			 
			 $raw_data=str_replace("LEAVEENJOYEDSL"," ".$leave_la_sl." ",$raw_data);	
			 //Replaces LEAVEENJOYEDSL with employee SL leave enjoyed
			 
			 $raw_data=str_replace("LEAVEENJOYEDSPL"," ".$leave_la_spl." ",$raw_data);	
			 //Replaces LEAVEENJOYEDSPL with employee SPL leave enjoyed
			 
			 $raw_data=str_replace("LEAVEBALANCECL"," ".$leave_bl_cl." ",$raw_data);	
			 //Replaces LEAVEBALANCECL with employee CL leave balance
			 
			 $raw_data=str_replace("LEAVEBALANCESL"," ".$leave_bl_sl." ",$raw_data);	
			 //Replaces LEAVEBALANCESL with employee SL leave balance
			 
			 $raw_data=str_replace("LEAVEBALANCESPL"," ".$leave_bl_spl." ",$raw_data);	
			 //Replaces LEAVEBALANCESPL with employee SPL leave balance
			 
			$duration=my_old($emp[joining_date],date("Y-m-d"));
			$job_duration=$duration[year]." Years, ".$duration[mnt]." Months, ".$duration[day]." Days";
			$raw_data=str_replace("SERVICELENGTH"," ".$job_duration." ",$raw_data);	
			//Replaces SERVICELENGTH with employee service length
			
			
			$raw_data=str_replace("TRNALWTRANALW"," "."<font style='font-family:Kalpurush ANSI'>".round($salary_info[$emp['emp_code']][11])."</font>"." ",$raw_data); 					                             //REPLACE TRNALWTRANALW  WITH EMPLOYEE transport allowance in bangla
			
			$raw_data=str_replace("FODALWFODALW"," "."<font style='font-family:Kalpurush ANSI'>".round($salary_info[$emp['emp_code']][13])."</font>"." ",$raw_data); 					                             //REPLACE FODALWFODALW  WITH EMPLOYEE food allowance in bangla
			
		$raw_data=str_replace("GRSPREVGRSPREV"," "."<font style='font-family:Kalpurush ANSI'>".round($grs_arr[$emp['emp_code']])."</font>"." ",$raw_data); 					                             //REPLACE GRSPREVGRSPREV  WITH EMPLOYEE november gross in bangla
			
			
		$raw_data=str_replace("GRSDECGRSDEC"," "."<font style='font-family:Kalpurush ANSI'>".round($grs_arr_des[$emp['emp_code']])."</font>"." ",$raw_data); 					                             //REPLACE GRSDECGRSDEC  WITH EMPLOYEE DECEMBER gross in bangla	
			 
			
			$raw_data=str_replace("SEPOVDTSEPOVDT"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date(add_date($sepa_details[$emp[emp_code]],11))."</font>"." ",$raw_data);
			//REPLACE SEPOVDTSEPOVDT  WITH EMPLOYEE first notice date  in bangla
			
			$raw_data=str_replace("SEPOVDT2NDSEPOVDT2ND"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date(add_date($sepa_details[$emp[emp_code]],22))."</font>"." ",$raw_data);
			//REPLACE SEPOVDT2NDSEPOVDT2ND  WITH EMPLOYEE second notice date  in bangla
			
			$raw_data=str_replace("SEPOVDT3RDSEPOVDT3RD"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date(add_date($sepa_details[$emp[emp_code]],29))."</font>"." ",$raw_data);
			//REPLACE SEPOVDT2NDSEPOVDT2ND  WITH EMPLOYEE third notice date  in bangla
			
			
			$raw_data=str_replace("SEPDATESEPDATE"," "."<font style='font-family:Kalpurush ANSI'>".convert_to_mysql_date($sepa_details[$emp[emp_code]])."</font>"." ",$raw_data);
			
			
$raw_data=str_replace("HOBANGHOBANG"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][1]['house_no_bangla']."</font>"." ",$raw_data);
	
	//REPLACE HOBANGHOBANG  WITH EMPLOYEE permanent house no bangla
$raw_data=str_replace("ROBANGROBANG"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][1]['road_no_bangla']."</font>"." ",$raw_data);
	
	//REPLACE ROBANGROBANG  WITH EMPLOYEE permanent road no bangla
$raw_data=str_replace("VILBANGVILBANG"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][1]['village_bangla']."</font>"." ",$raw_data);

	//REPLACE VILBANGVILBANG  WITH EMPLOYEE permanent village  bangla
$raw_data=str_replace("THNBANGTHNBANG"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][1]['thana_bangla']."</font>"." ",$raw_data);

	//REPLACE THNBANGTHNBANG  WITH EMPLOYEE permanent thana  bangla
$raw_data=str_replace("POCODEPOCODE"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][1]['post_code_bangla']."</font>"." ",$raw_data);

	//REPLACE POCODEPOCODE  WITH EMPLOYEE permanent post office no bangla
$raw_data=str_replace("DISTBANGDISTBANG"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][1]['district_id_bangla']."</font>"." ",$raw_data);
	//REPLACE DISTBANGDISTBANG  WITH EMPLOYEE permanent district no bangla
			
	$raw_data=str_replace("COBANGCOBANG"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][1]['co_bangla']."</font>"." ",$raw_data);
	//REPLACE COBANGCOBANG  WITH EMPLOYEE permanent care of  bangla	
	
	
	$raw_data=str_replace("PREHONOPREHONO"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][0]['house_no_bangla']."</font>"." ",$raw_data);
	
	//REPLACE PREHONOPREHONO  WITH EMPLOYEE present house no bangla
	
$raw_data=str_replace("PREROADPREROAD"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][0]['road_no_bangla']."</font>"." ",$raw_data);
	
	//REPLACE PREROADPREROAD  WITH EMPLOYEE present road no bangla
$raw_data=str_replace("PREVILPREVIL"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][0]['village_bangla']."</font>"." ",$raw_data);

	//REPLACE PREVILPREVIL  WITH EMPLOYEE present village  bangla
$raw_data=str_replace("PRETHPRETH"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][0]['thana_bangla']."</font>"." ",$raw_data);

	//REPLACE PRETHPRETH  WITH EMPLOYEE present thana  bangla
$raw_data=str_replace("PREPOPREPO"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][0]['post_code_bangla']."</font>"." ",$raw_data);

	//REPLACE PREPOPREPO  WITH EMPLOYEE present post office no bangla
$raw_data=str_replace("PREDISTPREDIST"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][0]['district_id_bangla']."</font>"." ",$raw_data);
	//REPLACE PREDISTPREDIST  WITH EMPLOYEE present district no bangla
			
	$raw_data=str_replace("PRECOPRECO"," "."<font style='font-family:Kalpurush ANSI'>".$address_info[$emp['emp_code']][0]['co_bangla']."</font>"." ",$raw_data);
	//REPLACE PRECOPRECO  WITH EMPLOYEE present care of  bangla		
	
	$raw_data=str_replace("NIDNID"," ".$emp[national_id]." ",$raw_data);   //REPLACE NIDNID  WITH EMPLOYEE national id in english
	
	$raw_data=str_replace("NIDBANNIDBAN"," "."<font style='font-family:Kalpurush ANSI'>".$emp[national_id]."</font>"." ",$raw_data);
	//REPLACE NIDBANNIDBAN  WITH EMPLOYEE national id bangla	
	
	$raw_data=str_replace("CNVALCNVAL"," ".round($salary_info[$emp['emp_code']][4])." ",$raw_data); 
	//REPLACE CNVALCNVAL  WITH EMPLOYEE convience in eng
	$raw_data=str_replace("CNVALBANCNVALBAN"," "."<font style='font-family:Kalpurush ANSI'>".round($salary_info[$emp['emp_code']][4])."</font>"." ",$raw_data);
	//REPLACE CNVALBANCNVALBAN  WITH EMPLOYEE convience in bangla
	
	$raw_data=str_replace("NOMNAMNOMNAM"," ".$nominee_details[$emp['emp_code']]." ",$raw_data); 
	//REPLACE NOMNAMNOMNAM  WITH EMPLOYEE NOMINEE name	
	
	$raw_data=str_replace("NOMRELNOMREL"," ".$nominee_relation[$emp['emp_code']]." ",$raw_data); 
	//REPLACE NOMRELNOMREL  WITH EMPLOYEE NOMINEE RELATION	
	
	$raw_data=str_replace("EMXPORGEMXPORG"," ".$experience_com[$emp['emp_code']]." ",$raw_data); 
	//REPLACE EMXPORGEMXPORG  WITH EMPLOYEE last experience	
	
	$raw_data=str_replace("PRODESIGPRODESIG"," ".$designation_details[$emp_promotion_details[$emp['emp_code']]['new_designation']]." ",$raw_data); 
	//REPLACE PRODESIGPRODESIG  WITH EMPLOYEE promoted designation	
	
	$raw_data=str_replace("CURDESIGCURDESIG"," ".$designation_details[$emp_promotion_details[$emp['emp_code']]['last_designation']]." ",$raw_data); 
	//REPLACE CURDESIGCURDESIG  WITH EMPLOYEE current designation
	
	$raw_data=str_replace("PROEFFDTPROEFFDT"," ".convert_to_mysql_date($emp_promotion_details[$emp['emp_code']]['effective_date'])." ",$raw_data); 
	//REPLACE PROEFFDTPROEFFDT  WITH EMPLOYEE PROMOTION effective date
			
	
	$raw_data=str_replace("FNAMEFNAME"," ".$emp[father_name]." ",$raw_data);
	$raw_data=str_replace("MNAMEMNAME"," ".$emp[mother_name]." ",$raw_data);			
			
			
			mysql_query("update hrm_dynamic_letter set print_number=$print_number where id=$selectResult[id]");
			
		}
		echo $raw_data;
?>
<p></p>		
 </div>	
 <?php	
	}
		$html_data = ob_get_contents();
		ob_get_clean();
		
		//previous file delete code-----------------------------//
		//foreach (glob("../"."*.doc") as $filename) {
		//if( @filemtime($filename) < (time()-$seconds_old) )
		   // @unlink($filename);
		//}
			
		/*$name=time();
		$filename=$name.".doc";
		$create_new_doc = fopen("../".$filename, 'w');	
		$is_created = fwrite($create_new_doc,$html_data);			
		*/
		
		
		//echo "$html_data####$filename";
		echo $html_data;
		exit();	 
}



function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
}



function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>
