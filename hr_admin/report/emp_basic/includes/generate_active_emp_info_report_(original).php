<?php
	session_start();
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	include("../../../../includes/common.php");
	include('../../../../includes/array_function.php');
	extract($_GET);
	//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
	//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
	//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
	//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
	//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
	//Section
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
	//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
	//Division 
	$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
	
	//print_r($lib_list_division);die;	
	//District
	$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}
	
	// my_old
	function my_old($dob)
	{
		$now = date('d-m-Y');
		$dob = explode('-', $dob);
		$now = explode('-', $now);
		$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
		if($now[0] < $dob[0]){
			$now[0] += $mnt[$now[1]-1];
			$now[1]--;
		}
		if($now[1] < $dob[1]){
			$now[1] += 12;
			$now[2]--;
		}
		if($now[2] < $dob[2]) return false;
		return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
	}
?>
<style type="text/css">
    @page { size 8.5in 11in; margin: 0.5in; }
    div.page { page-break-after: always; background-color:#FFF;}
</style>
<?
if($type=="active_emp_list")
{
	 $company_name=return_field_value("company_name","lib_company","id='$company_id' and status_active=1 and is_deleted=0");
	 $location_name=return_field_value("location_name","lib_location","id='$location_id' and status_active=1 and is_deleted=0");	   
	
	if ($category=='') $category="and category in ($employee_category_index)"; else $category="and category='$category'";
	if ($company_id=='') $company_id =""; else $company_id ="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else $department_id="and department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and section_id  in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id  in ($designation_id) ";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	//if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ($id_card_no)";
	//if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ($emp_code)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	
	if($id_card_condition==1)
	{
		if ($id_card_from!="" && $id_card_to!="") 
		 {
			$def=($id_card_to-$id_card_from);
			 for($i=0;$i<=$def; $i++)
			 {
				 if($i!=0)$id_card_no_tot .=",";
				 $id_card_no_tot .=$id_card_from+$i; 
			}
		}
		if ($id_card_no_tot=="") $id_card_no=""; else $id_card_no="and id_card_no in ( $id_card_no_tot )";
	}
	else
	{
		if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
	}
	//echo $id_card_no_tot;
	//$cdate="2013-08-07";//date("Y-m-d",time());
	$cdate=date("Y-m-d",time());
	//$tt=date("Y-m-d",time());
	//echo "ss".$tt; die;
	if($duration_ends==0 || $duration_ends=="")  $duration_ends=$txt_service_length_start+10;
	if(trim($txt_service_length_start)!="") $service_length=" AND DATEDIFF( '$cdate',joining_date) between $txt_service_length_start and $duration_ends "; else $service_length="";
	
	if ($txt_date!="")
	{
		$txt_date=date("Y-m-d",strtotime(convert_to_mysql_date($txt_date)));
		$str_join_date=" and dob like '".$txt_date."'";
	}
	
	$txt_joining_date_from=convert_to_mysql_date($txt_joining_date_from);
	$txt_joining_date_to=convert_to_mysql_date($txt_joining_date_to);
	
	if ($txt_joining_date_from=='' || $txt_joining_date_to=='') $joining_date_con =""; else $joining_date_con =" and joining_date between '$txt_joining_date_from' and '$txt_joining_date_to'";
	
	//new add group by order by
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
	
	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(id_card_no), 5)as SIGNED)";}	 
	//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$subsection_total_gross_amount=0;
	$section_total_gross_amount=0;	
	$department_total_gross_amount=0;
	$division_total_gross_amount=0;	
	$location_total_gross_amount=0;				
	$grand_total_gross_amount=0;
	$sl=0;
	
	$sql = "SELECT * FROM hrm_increment_mst WHERE is_posted=0 and is_approved=2 and status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$increment_table_gross_salary_arr = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$increment_table_gross_salary_arr[$row['emp_code']] = $row['new_gross_salary'];
	}
	//end	 group by order by	 
	// echo $str_join_date;
	$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name,DATEDIFF( '$cdate',joining_date) as jobdur
		FROM hrm_employee 
		WHERE 
		is_deleted=0 and 
		status_active=1 
		$category
		$company_id 
		$location_id 
		$division_id 
		$department_id 
		$section_id 
		$subsection_id
		$designation_id
		$salary_based
		$id_card_no	
		$emp_code
		$service_length
		$str_join_date
		$joining_date_con		
		group by $dynamic_groupby $orderby";
		//echo $sql; 
		
		$result=mysql_query($sql);
		ob_start();	
		if(mysql_num_rows($result)==0)
		{
			echo "<div align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></div>";
		}
		else
		{?>
		<div style="font-size:14px; font-style:italic; color:#FF0000; margin-left:800px;">N.B:Gross Salary after last approved Increment.</div>
		<div style="width:2770px;" align="left">
        
			<table cellspacing="0" cellpadding="0" width="2770px" class="rpt_table" border="1" rules="all">
				<thead>
					<th width="40" align="center"><strong>SL</strong></th>
					<th width="120" align="center"><strong>Emp ID</strong></th>
					<th width="80" align="center"><strong>Emp Photo</strong></th>
					<th width="80" align="center"><strong>Emp Code</strong></th>
					<th width="130" align="center"><strong>Emp Name</strong></th>
					<th width="140" align="center"><strong>Company</strong></th>
					<th width="120" align="center"><strong>Designation</strong></th>
                    <th width="75" align="center"><strong>Salary Grade</strong></th>
					<th width="90" align="center"><strong>DOB</strong></th>
					<th width="90" align="center"><strong>DOJ</strong></th>
					<th width="75" align="center"><strong>Serv. Len</strong></th>
                    <th width="75" align="center"><strong>Basic Salary</strong></th>
					<th width="75" align="center"><strong>Gross Salary</strong></th>
					<th width="200" align="center"><strong>Address</strong></th>
					<th width="130" align="center"><strong>Division</strong></th>
					<th width="60" align="center"><strong>Sex</strong></th>
					<th width="70" align="center"><strong>Religion</strong></th>
					<th width="130" align="center"><strong>Age</strong></th>
					<th width="70" align="center"><strong>Blood Group</strong></th>
					<th width="160" align="center"><strong>Division</strong></th>
					<th width="130" align="center"><strong>Department</strong></th>
					<th width="100" align="center"><strong>Section</strong></th>
					<th width="100" align="center"><strong>Sub Section</strong></th>
					<th width="100" align="center"><strong>OT Entitled</strong></th>
					<th width="130" align="center"><strong>Route</strong></th>
					<th width="100" align="center"><strong>Vehicle No.</strong></th>
					<th width="100" align="center"><strong>Mobile No</strong></th>
				</thead>
		   </table>
		   <?php
		  
			$small_print .='<table cellspacing="0" cellpadding="0" width="1055px" class="rpt_table" border="1" rules="all">';
			$small_print .='<td colspan="11" align="center" style="border:hidden"><font size="+1">'. $company_name.'</font><br />'.$location_name.'<br /><font size="+0"> Active Employee List</font><br /></td></table>';
			
		    $small_print .='
			<table cellspacing="0" cellpadding="0" width="1055px" class="rpt_table" border="1" rules="all">
				<thead>
					<th width="40" align="center"><strong>SL</strong></th>
					<th width="100" align="center"><strong>Emp ID</strong></th>
					<th width="100" align="center"><strong>Emp Code</strong></th>
					<th width="150" align="center"><strong>Emp Name</strong></th>
					<th width="150" align="center"><strong>Designation</strong></th>
					<th width="75" align="center"><strong>Salary Grade</strong></th>
					<th width="100" align="center"><strong>Section</strong></th>
					<th width="100" align="center"><strong>Sub Section</strong></th>
					<th width="90" align="center"><strong>DOJ</strong></th>
					<th width="75" align="center"><strong>Basic Salary</strong></th>
					<th width="75" align="center"><strong>Gross Salary</strong></th>
				</thead>';
		   
		   
		   $excel_preview .='
			<table cellspacing="0" cellpadding="0" width="2620px" class="rpt_table" border="1" rules="all">
				<thead>
					<th width="40" align="center"><strong>SL</strong></th>
					<th width="120" align="center"><strong>Emp ID</strong></th>
					<th width="80" align="center"><strong>Emp Code</strong></th>
					<th width="130" align="center"><strong>Emp Name</strong></th>
					<th width="140" align="center"><strong>Company</strong></th>
					<th width="120" align="center"><strong>Designation</strong></th>
					<th width="90" align="center"><strong>DOB</strong></th>
					<th width="90" align="center"><strong>DOJ</strong></th>
					<th width="75" align="center"><strong>Serv. Len</strong></th>
					<th width="75" align="center"><strong>Gross Salary</strong></th>
					<th width="200" align="center"><strong>Address</strong></th>
					<th width="130" align="center"><strong>Division</strong></th>
					<th width="60" align="center"><strong>Sex</strong></th>
					<th width="70" align="center"><strong>Religion</strong></th>
					<th width="130" align="center"><strong>Age</strong></th>
					<th width="70" align="center"><strong>Blood Group</strong></th>
					<th width="160" align="center"><strong>Division</strong></th>
					<th width="130" align="center"><strong>Department</strong></th>
					<th width="100" align="center"><strong>Section</strong></th>
					<th width="100" align="center"><strong>Sub Section</strong></th>
					<th width="100" align="center"><strong>OT Entitled</strong></th>
					<th width="130" align="center"><strong>Route</strong></th>
					<th width="100" align="center"><strong>Vehicle No.</strong></th>
					<th width="100" align="center"><strong>Mobile No</strong></th>
				</thead>
		   </table>';
		   ?>
		</div>
		<div style="width:2770px;" id="scroll_body" align="left">
			<table cellspacing="0" cellpadding="0" width="2770px" class="rpt_table" border="1" rules="all">
			<?php
			//$small_print.='<table cellspacing="0" cellpadding="0" width="1055px" class="rpt_table" id="table_body" border="1">';
			$excel_preview.='<table cellspacing="0" cellpadding="0" width="2620px" class="rpt_table" id="table_body" border="1">';
			$i=1;
			$im=0;
			$emp_code_img_arr= array();
			$emp_id_img_arr= array();
				
			while($emp=mysql_fetch_assoc($result))
			{
				$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=0 and status_active=1 and is_deleted=0";
				$result1=mysql_query($sql1);
				$row=mysql_fetch_assoc($result1);
				if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
				if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
				if($row['village']=="") $village=""; else $village=$row['village'].", ";
				if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
				if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
				if($row['mobile_no']=="") $mobile_num=""; else $mobile_num=$row['mobile_no'].", ";
				$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
				//echo $lib_list_district[$row['district_id']];die;
				$sql2="SELECT * FROM  hrm_transport_mst  WHERE  vehicle_no  in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
				$result2=mysql_query($sql2);
				$row2=mysql_fetch_assoc($result2);
				
				$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0 order by id desc limit 1");
				$image="";
				if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
				
				/*$image_pi="";
				if($row3=="") $image_pi=""; else $image_pi="<img src='../../../$row3' height='182' width='144' />";*/
				//new add
				//print_r($location_arr[$row_emp[location_id]]);
				$sl++;
				//start header print---------------------------------//
				if($sl==1)
				{
					$location_arr[$emp[location_id]]=$emp[location_id];
					$division_arr[$emp[division_id]]=$emp[division_id];
					$department_arr[$emp[department_id]]=$emp[department_id];
					$section_arr[$emp[section_id]]=$emp[section_id];
					$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
				}//end if condition of header print
		
				
				if($sl!=1)//---------------------------------------------------------------------------------------------------
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
					if(in_array($emp[location_id],$location_arr) && $status_loc==1)
					{
						if(in_array($emp[division_id],$division_arr) && $status_divis==1)
						{
						if(in_array($emp[department_id],$department_arr) && $status_dept==1)
						{
						if(in_array($emp[section_id],$section_arr) && $status_sec==1)
						{ 
						if(in_array($emp[subsection_id],$subsection_arr) && $status_subsec==1)
						{}
						else if($status_subsec==1)
						{
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_subsec=1;
						}
						}
						else if($status_sec==1)
						{
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr=array();
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_sec=1;
						$new_subsec=1;
						}
						}
						else if($status_dept==1)
						{
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr=array();
						$subsection_arr=array();
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
						}
						}//division
						else if($status_divis==1)
						{
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
						}//division else
					}//location
					else if($status_loc==1)
					{
					$location_arr[$emp[location_id]]=$emp[location_id];
					$division_arr=array();
					$department_arr=array();
					$section_arr=array();
					$subsection_arr=array();
					$division_arr[$emp[division_id]]=$emp[division_id];
					$department_arr[$emp[department_id]]=$emp[department_id];
					$section_arr[$emp[section_id]]=$emp[section_id];
					$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
					$new_loc=1;
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
					}//location else
				}
				
				if($new_subsec==1 && $status_subsec==1 && $sl!=1)
				{
				?>
				<tr>
					<td width='' colspan="11"><b> Subsection Total</b></td>
                     <td width='' align='right' > <? echo number_format($subsection_total_basic_amount); ?></td>
					<td width='' align='right' > <? echo number_format($subsection_total_gross_amount); ?></td>
					<td colspan="14">&nbsp;</td>
				</tr>
				<?
				$small_print .='<tr>
                <td width="" colspan="9"><b> Subsection Total</b></td>
				<td width="" align="right" >'.number_format($subsection_total_basic_amount).'</td>
                <td width="" align="right" >'.number_format($subsection_total_gross_amount).'</td>
            	</tr>';
				
				$subsection_total_gross_amount=0;
				$subsection_total_basic_amount=0;
				}
				
				if($new_sec==1 && $status_sec==1 && $sl!=1)
				{
					?>
					<tr>
						<td width='' colspan="11"><b>SectionTotal</b></td>
                        <td width='' align='right' > <? echo number_format($section_total_basic_amount); ?></td>
						<td width='' align='right' > <? echo number_format($section_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Section Total</b></td>
					<td width="" align="right" >'.number_format($section_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($section_total_gross_amount).'</td>
					</tr>';
					$section_total_gross_amount=0;
					$section_total_basic_amount=0;
				}
				
				if($new_dept==1 && $status_dept==1 && $sl!=1)
				{
					?>
					<tr>
						<td colspan="11">Department Total</td>
                         <td width='' align='right' > <? echo number_format($department_total_basic_amount); ?></td>
						<td width='' align='right' > <? echo number_format($department_total_gross_amount); ?></td>
						<td  colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Department Total</b></td>
					<td width="" align="right" >'.number_format($department_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($department_total_gross_amount).'</td>
					</tr>';
					$department_total_gross_amount=0;
					$department_total_basic_amount=0;
				}
				
				if($new_divis==1 && $status_divis==1 && $sl!=1)
				{
				?>
					<tr>
						<td  colspan="11" >Division Total</td>
                        <td width='' align='right' > <? echo number_format($division_total_basic_amount); ?></td>
						<td width='' align='right' > <? echo number_format($division_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Division Total</b></td>
					<td width="" align="right" >'.number_format($division_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($division_total_gross_amount).'</td>
					</tr>';
					$division_total_gross_amount=0;	
					$division_total_basic_amount=0;	
				}
				
				if($new_loc==1 && $status_loc==1 && $sl!=1)
				{
					?>
					<tr>
						<td colspan="11">Location Total</td>
                        <td width='' align='right'> <? echo number_format($location_total_basic_amount); ?></td>
						<td width='' align='right'> <? echo number_format($location_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Location Total</b></td>
					<td width="" align="right" >'.number_format($location_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($location_total_gross_amount).'</td>
					</tr>';
					$location_total_gross_amount=0;
					$location_total_basic_amount=0;
				}
		
				//header print here 
				$c_part_1="";
				if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
				{
					
					if($status_loc==1)
					{					
						$c_part_1 .= "  Location :  ".$location_details[$emp[location_id]]."  ,";
					}				
					if($status_divis==1)
					{
						$c_part_1 .= "  Division :  ".$division_details[$emp[division_id]]."  ,";
					}
					if($status_dept==1)
					{
						$c_part_1 .= "  Department :  ".$department_details[$emp[department_id]]."  ,";
					}
					if($status_sec==1)
					{
						$c_part_1 .= "  Section :  ".$section_details[$emp[section_id]]."  ,";
					}
					if($status_subsec==1)
					{
						$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp[subsection_id]]."  ,";
					}
	
					if($c_part_1!='')
					{
						//$i=0;
						$sl=1;
						?>
						<tr>
							<td colspan="23"><b><? echo substr($c_part_1,0,-1); ?></b></td>
						</tr>
					  </table>  
						<?
						$small_print .='<tr><td colspan="23"><b>'.substr($c_part_1,0,-1).'</b></td></tr> ';
					}		
					?>                 
					<table cellspacing="0" cellpadding="0" width="2770px" class="rpt_table" id="table_body" border="1" rules="all" >
					<?
					
					 //$small_print .='<table cellspacing="0" cellpadding="0" width="1055px" class="rpt_table" border="1" rules="all">';
				}
				
				
				if( $image!="") 
				{
					$im++;
					$emp_code_img_arr[]=$emp['emp_code'];
					$emp_id_img_arr[]=$emp['id_card_no'];
				}
					
				/*
				for( $k = 0; $k <= count( $emp_code_img_arr ); $k++ ) 
				{
				$image_print .= "<td>";
				$image_print .="<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">";								
				$image_print .= "<tr><td align=\"center\">" . $image_pi . "</td></tr>";
				$image_print .= "<tr><td align=\"center\">Card No: " . $emp['id_card_no'] . "</td></tr>
				</table>";
				$image_print .=$im;
				$image_print .= "</td>";
				
				if( $k == $im && $k % 3 == 1 ) 
				{
				$html .= "</tr>"; 
				$k = 0;
				}
				
				if( $k == count( $emp_code_img_arr ) && $k % 3 == 1 ) $image_print .= "<td>&nbsp;</td>";
				elseif( $k == count( $emp_code_img_arr ) && $k % 3 == 2 ) $image_print .= "<td>&nbsp;</td>";
				
				if( $k % 9 == 0 && $k < count( $emp_code_img_arr ) ) {
				$image_print .= "</tr></table>";
				
				$image_print = "<table width=\"90%\" border=\"1\" cellspacing=\"1\"><tr>";
				//$html = "<table width=\"90%\" border=\"1\" cellspacing=\"1\" height=\"60%\"><tr>";
				}
				elseif( $k % 3 == 0 && $k < count( $emp_code_img_arr ) ) $image_print .= "</tr><tr>";
				
				}
				*/
				//$image_print .= "</tr>";
				/*	
				if( $im == $im && $im % 3 == 1 ) $image_print .= "</td>";
				elseif( $im == $im && $im % 3 == 2 ) $image_print .= "</td>";
				
				if( $i == count( $employees ) && $i % 3 == 1 ) $html .= "<td colspan=\"2\">&nbsp;</td>";
				elseif( $i == count( $employees ) && $i % 3 == 2 ) $html .= "<td>&nbsp;</td>";
				
				if( $im % 9 == 0 && $im < $im ) {
				$image_print .= "</tr></table>";
				
				$image_print = "<table width=\"90%\" border=\"1\" cellspacing=\"1\"><tr>";
				}
				elseif( $im % 3 == 0 && $im < $im ) $image_print .= "</tr><tr>";
				*/
		
				if ($i%2==0) $bgcolor="#E9F3FF";
				else $bgcolor="#FFFFFF";
				?>
				<tr bgcolor="<? echo $bgcolor; ?>">
					<td width="40" align="center"><?php echo $i;?></td>
					<td width="120" align="center"><?php echo $emp['id_card_no'];?></td>
					<td width="80" valign="bottom" align="center"><?php echo $image; ?></td>
					<td width="80" align="center"><?php echo $emp['emp_code'] ; ?></td>
					<td width="130"><?php echo $emp['name'] ; ?></td>
					<td width="140"><?php echo $company_details[$emp['company_id']]; ?></td>
					<td width="120"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
                    <td width="75" align="center"><?php echo $emp['salary_grade'] ; ?></td>
					<td width="90" align="center"><?php echo convert_to_mysql_date($emp['dob']) ; ?></td>
					<td width="90" align="center"><?php echo convert_to_mysql_date($emp['joining_date']) ; ?></td>
					<td width="75" align="center"><?php echo $emp['jobdur'] ; ?></td>
                    <? $basic_salary=return_field_value("amount","hrm_employee_salary","emp_code='$emp[emp_code]' and payroll_head=1 and status_active=1 and is_deleted=0");?>
                    <td width="75" align="center"><?php echo $basic_salary; ?></td>
					<td width="75" align="center">
					 <?php 
					 if($increment_table_gross_salary_arr[$emp['emp_code']]=="" || $increment_table_gross_salary_arr[$emp['emp_code']]==0)
					 {
						echo $gross=$emp['gross_salary'] ; 
					 }
					 else
					 {
						echo $gross=$increment_table_gross_salary_arr[$emp['emp_code']]; 
					 }
					 ?>
					</td>
                    
					<td width="200"><?php echo $address ; ?></td>
					<? $row_div_perma=return_field_value("division_id","hrm_employee_address","emp_code='$emp[emp_code]' and address_type=1 and status_active=1 and is_deleted=0");?>
					<td width="130" align="center"><?php echo $lib_list_division[$row_div_perma]; ?></td>
					<td width="60" align="center"><?php echo $sex_arr[$emp['sex']];?></td>
					<td width="70" align="center"><?php echo $emp['religion'] ; ?></td>
					<td width="130">
					<?php $birth_date=convert_to_mysql_date($emp['dob']); $age = my_old($birth_date); printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);?>
					</td>
					<td width="70" align="center"><?php echo $blood_group[$emp['blood_group']]; ?></td>
					<td width="160" align="center"><?php echo $division_details[$emp['division_id']]; ?></td>
					<td width="130" align="center"><?php echo $department_details[$emp['department_id']]; ?></td>
					<td width="100" align="center"><?php echo $section_details[$emp['section_id']]; ?></td>
					<td width="100" align="center"><?php echo $subsection_details[$emp['subsection_id']]; ?></td>
					<td width="100" align="center"><?php  if($emp['ot_entitled']==1){echo "yes";}?></td>
					<td width="130" align="center"><?php echo $row2['route']; ?></td>
					<td  width="100" align="center"><?php echo $row2['vehicle_no']; ?></td>
					<td  width="100" align="center"><?php echo $mobile_num; ?></td>
				</tr>
				<?php
				$small_print.='<tr bgcolor="'.$bgcolor.'">
					<td width="40" align="center">'.$i.'</td>
					<td width="100" align="center">'.$emp[id_card_no].'</td>
					<td width="100" align="center">'.$emp[emp_code].'</td>
					<td width="150"><p>'.$emp[name].'</p></td>
					<td width="150"><p>'.$designation_chart[$emp[designation_id]].'</p></td>
					<td width="75" align="center">'.$emp['salary_grade'].'</td>
					<td width="100" align="center">'.$section_details[$emp[section_id]].'</td>
					<td width="100" align="center">'.$subsection_details[$emp[subsection_id]].'</td>
					<td width="90" align="center">'.convert_to_mysql_date($emp[joining_date]).'</td>
					<td width="75" align="center">'.$basic_salary.'</td>
					<td width="75" align="center">'.$gross.'</td>
				</tr>'; 
				
				
				$excel_preview.='<tr bgcolor="'.$bgcolor.'">
					<td width="40" align="center">'.$i.'</td>
					<td width="120" align="center">'.$emp[id_card_no].'</td>
					<td width="80" align="center">'.$emp[emp_code].'</td>
					<td width="130">'.$emp[name].'</td>
					<td width="140">'.$company_details[$emp[company_id]].'</td>
					<td width="120">'.$designation_chart[$emp[designation_id]].'</td>
					<td width="90" align="center">'.convert_to_mysql_date($emp[dob]).'</td>
					<td width="90" align="center">'.convert_to_mysql_date($emp[joining_date]).'</td>
					<td width="75" align="center">'.$emp[jobdur].'</td>
					<td width="75" align="center">';
					if($increment_table_gross_salary_arr[$emp[emp_code]]=="" || $increment_table_gross_salary_arr[$emp['emp_code']]==0)
					{
						$gs=$emp[gross_salary]; 
					}
					else
					{
						$gs=$increment_table_gross_salary_arr[$emp[emp_code]]; 
					}
					$excel_preview.=$gs.'</td>
					<td width="200">'.$address.'</td>';
					$row_div_perma=return_field_value("division_id","hrm_employee_address","emp_code='$emp[emp_code]' and address_type=1 and status_active=1 and is_deleted=0");
					$excel_preview.='<td width="130">'.$lib_list_division[$row_div_perma].'</td>
					<td width="60" align="center">'.$sex_arr[$emp[sex]].'</td>
					<td width="70" align="center">'.$emp[religion].'</td>
					<td width="130">';$birth_date=convert_to_mysql_date($emp[dob]); $age =my_old($birth_date);
					$excel_preview.=$age[year]." years ".$age[mnt]." months ".$age[day]."days".'</td>
					<td width="70" align="center">'.$blood_group[$emp[blood_group]].'</td>
					<td width="160" align="center">'.$division_details[$emp[division_id]].'</td>
					<td width="130" align="center">'.$department_details[$emp[department_id]].'</td>
					<td width="100" align="center">'.$section_details[$emp[section_id]].'</td>
					<td width="100" align="center">'.$subsection_details[$emp[subsection_id]].'</td>';
					$excel_preview.='<td width="100" align="center">'; if($emp[ot_entitled]==1){ $ot_en="yes";}
					$excel_preview.=$ot_en.'</td>
					<td width="130" align="center">'.$row2[route].'</td>
					<td  width="100" align="center">'.$row2[vehicle_no].'</td>
					<td  width="100" align="center">'.$mobile_num.'</td>
				</tr>'; 
				$ct++;
				//totals of gross
				$subsection_total_gross_amount +=round($emp['gross_salary']);
				$section_total_gross_amount +=round($emp['gross_salary']);
				$department_total_gross_amount +=round($emp['gross_salary']);
				$division_total_gross_amount +=round($emp['gross_salary']);
				$location_total_gross_amount +=round($emp['gross_salary']);
				$grand_total_gross_amount +=round($emp['gross_salary']);
				
				$subsection_total_basic_amount +=round($basic_salary);
				$section_total_basic_amount +=round($basic_salary);
				$department_total_basic_amount +=round($basic_salary);
				$division_total_basic_amount +=round($basic_salary);
				$location_total_basic_amount +=round($basic_salary);
				$grand_total_basic_amount +=round($basic_salary);
				
			$i++;
			} //end while==========================================================================
			
			$excel_preview.='</table>';
			
			$img_emp_id= implode(",",$emp_code_img_arr);
			//print_r($emp_code_img_arr);
			//echo $img_emp_id;
			$image_print = "<table width=\"100%\" border=\"1\" cellspacing=\"1\"><tr>";
			
			for( $k = 0; $k <= count( $emp_code_img_arr ); $k++ ) 
			{
				$row4=return_field_value("location","resource_photo","identifier ='" .$emp_code_img_arr[$k-1]. "' and type=0 and is_deleted=0 order by id desc limit 1");
				$image_pin="";
				if($row4=="") $image_pin=""; else $image_pin="<img src='../../../$row4' height='175' width='144' />";
		
					$image_print .= "<td>";
					//$image_print .= $k;
					//width 192 height 240
					$image_print .="<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">";								
					$image_print .= "<tr><td align=\"center\">" . $image_pin . "</td></tr>";
					$image_print .= "<tr><td align=\"center\">Card No: " . $emp_id_img_arr[$k-1] . "</td></tr>
					</table>";
					$image_print .= "</td>";
				
					if( $k == count( $emp_code_img_arr ) && $k % 4 == 1 ) $image_print .= "<td>&nbsp;</td>";
					else if( $k == count( $emp_code_img_arr ) && $k % 4 == 2 ) $image_print .= "<td>&nbsp;</td>";
					
					if( $k % count( $emp_code_img_arr ) == 0 && $k < count( $emp_code_img_arr ) ) {
					$image_print .= "</tr></table>";
					
					$image_print = "<table width=\"70%\" border=\"1\" cellspacing=\"20\"><tr>";
					}
					else if( $k % 4 == 0 && $k < count( $emp_code_img_arr ) ) $image_print .= "</tr><tr>";
				}
			
				$image_print .= "</tr></table>";
				
				if($status_subsec==1)
				{
					?>
					<tr>
						<td width='' colspan="11"><b> Subsection Total</b></td>
                        <td width='' align='right' > <? echo number_format($subsection_total_basic_amount); ?></td>
						<td width='' align='right' > <? echo number_format($subsection_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Subsection Total</b></td>
					<td width="" align="right" >'.number_format($subsection_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($subsection_total_gross_amount).'</td>
					</tr>';
					$subsection_total_gross_amount=0;
					$subsection_total_basic_amount=0;
				}
		
				if($status_sec==1)
				{
					?>
					<tr>
						<td width='' colspan="11"><b>Section Total</b></td>
                        <td width='' align='right' > <? echo number_format($section_total_basic_amount); ?></td>
						<td width='' align='right' > <? echo number_format($section_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Section Total</b></td>
					<td width="" align="right" >'.number_format($section_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($section_total_gross_amount).'</td>
					</tr>';
					$section_total_gross_amount=0;
					$section_total_basic_amount=0;
				}
		
				if($status_dept==1 )
				{
					?>
					<tr style="font-weight:bold">
						<td colspan="11">Department Total</td>
                         <td width='' align='right' > <? echo number_format($department_total_basic_amount); ?></td>
						<td width='' align='right' > <? echo number_format($department_total_gross_amount); ?></td>
						<td  colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Department Total</b></td>
					<td width="" align="right" >'.number_format($department_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($department_total_gross_amount).'</td>
					</tr>';
					$department_total_gross_amount=0;
					$department_total_basic_amount=0;
				}
		 
				if($status_divis==1 )
				{
					?>
					<tr style="font-weight:bold">
						<td  colspan="11" >Division Total</td>
                         <td width='' align='right' > <? echo number_format($division_total_basic_amount); ?></td>
						<td width='' align='right' > <? echo number_format($division_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Division Total</b></td>
					<td width="" align="right" >'.number_format($division_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($division_total_gross_amount).'</td>
					</tr>';
					$division_total_gross_amount=0;
					$division_total_basic_amount=0;	
				}
		 
				if($status_loc==1 )
				{
					?>
					<tr style="font-weight:bold">
						<td colspan="11">Location Total</td>
                         <td width='' align='right'> <? echo number_format($location_total_basic_amount); ?></td>
						<td width='' align='right'> <? echo number_format($location_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr>
					<?
					$small_print .='<tr>
					<td width="" colspan="9"><b> Location Total</b></td>
					<td width="" align="right" >'.number_format($location_total_basic_amount).'</td>
					<td width="" align="right" >'.number_format($location_total_gross_amount).'</td>
					</tr>';
					$location_total_gross_amount=0;	
					$location_total_basic_amount=0;
				}
				?>
					<tr  bgcolor="#CCCCCC" style="font-weight:bold">
						<td  colspan="11">&nbsp;Grand Total</td>
                        <td width='' align='right'> <? echo number_format($grand_total_basic_amount); ?></td>
						<td width='' align='right'> <? echo number_format($grand_total_gross_amount); ?></td>
						<td colspan="14">&nbsp;</td>
					</tr> 		
				</table>
                <?
                $small_print.='<tr  bgcolor="#CCCCCC" style="font-weight:bold">
						<td  colspan="9">&nbsp;Grand Total</td>
						<td width="" align="right"> '. number_format($grand_total_basic_amount).'</td>
						<td width="" align="right"> '. number_format($grand_total_gross_amount).'</td>
					</tr> 		
					</table>';
				?>
			</div>
			<?php
		} //end if condition
		 
		$html = ob_get_contents();
		ob_clean();
		
		foreach (glob( "tmp_report_file/"."*_short.xls") as $filename) 
		{			
			@unlink($filename);
		}
		//html to xls convert
		$name=time();
		$filename_short="tmp_report_file/".$name."_short.xls";
		$create_new_doc_short = fopen($filename_short, 'w');	
		$is_created_short = fwrite($create_new_doc_short,$excel_preview);
		$filename_short="$name"."_short.xls";
		echo "$html"."####"."$name"."####".$image_print."####".$filename_short."####".$small_print;
		exit();
		
		/*
		foreach (glob("../includes/tmp_report_file/$user_name*.xls") as $filename) 
		{
			if( @filemtime($filename) < (time()-$seconds_old) )
			@unlink($filename);
		}
		//---------end------------//
		$name=time();
		$filename="../includes/tmp_report_file/".$user_name."_".$name.".xls";
		$filename_short="../includes/tmp_report_file/".$user_name."_".$name."short.xls";
		$create_new_doc = fopen($filename, 'w');
		$create_new_doc_short = fopen($filename_short, 'w');	
		$is_created = fwrite($create_new_doc,$html);
		$is_created_short = fwrite($create_new_doc_short,$html_short);
		$filename="includes/tmp_report_file/".$user_name."_".$name.".xls";
		$filename_short="includes/tmp_report_file/".$user_name."_".$name."short.xls";
		echo "$total_data####$filename####$filename_short";
		exit();
		*/	
		
} //end Active Employee List if condition


function add_month($orgDate,$mon)
{
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
  return $retDAY;
}	
	
function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}
?>
     
	