<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");
include('../../../../includes/array_function.php');

$separation_type=array(1=>"Resignation",2=>"Retirement",3=>"Disability",4=>"Death",5=>"Terminated",6=>"Un-noticed");
$sex=array(0=>"Male",1=>"Female");
$marital_status=array(0=>"Single",1=>"Married",2=>"Separated",3=>"Widow");
$blood_group=array(0=>"-- Select --",1=>"A+",2=>"A-",3=>"B+",4=>"B-",5=>"AB+",6=>"AB-",7=>"O+",8=>"O-");
$emp_relation=array(0=>"-- Select --",1=>"Husband",2=>"Wife",3=>"Son",4=>"Daughter");
//$ot_entitled=array(0=>"No",1=>"Yes");
//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}

$sql = "select * from lib_payroll_head where is_applicable=1 and salary_head=1 and status_active=1 and is_deleted=0 and id!=20 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$salary_head_mst = array();
	$salary_head_mst_short = array();
	$salary_head_mst_id = array();

	$i=0;
	while( $row = mysql_fetch_assoc( $result ) ) {	
		$i++;	
			$salary_head_mst[$i] = mysql_real_escape_string( $row['custom_head'] );	
			$salary_head_mst_id[$i] = mysql_real_escape_string( $row['id'] );	
			//$salary_head_mst_short[$i]=	mysql_real_escape_string( $row['abbreviation'] );	
	}
//print_r($salary_head_mst_id);die;


	$sql = "SELECT a.*,b.* FROM hrm_increment_mst a,hrm_increment_dtls b WHERE a.id=b.mst_id and a.is_posted=0 and a.is_approved=2 and a.status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$increment_table_salary_brackdawn_arr = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$increment_table_salary_brackdawn_arr[$row['emp_code']][$row['payroll_head']] = $row['per_head_increment'];
	}



extract($_REQUEST);
$active_data=explode("_", $data);
//print_r($active_data);
//print $search_string;

function my_old($dob){
	$now = date('d-m-Y');
	$dob = explode('-', $dob);
	$now = explode('-', $now);
	$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
	if($now[0] < $dob[0]){
		$now[0] += $mnt[$now[1]-1];
		$now[1]--;
	}
	if($now[1] < $dob[1]){
		$now[1] += 12;
		$now[2]--;
	}
	if($now[2] < $dob[2]) return false;
	return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
}

// start leavers report if condition		
if($action=="action_separated_emp_summary")
{
	//echo "ca==".$category_id."co==".$company_id."l==".$location_id."d==".$division_id."dept==".$department_id."sec==".$section_id."sub==".$subsection_id."des==".$designation_id."dt==".$date." =su..re";die;
	//$active_data=explode("_", $data);
	//print_r($active_data);
	$to_date=convert_to_mysql_date($date);
	$month_prev=add_month($date,-11);
	$start_mon=date("M",strtotime($month_prev));
	$end_mon=date("M",strtotime($to_date));
	
	if ($category_id=='') $category="and category in ($employee_category_index)"; else $category="and category='$category_id'";
	if ($company_id=='') $company_id =""; else $company_id ="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	?>
    <div>
        <div style="width:945px;" align="left">
            <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
                <thead>
                    <th width='190' align='center'>leavers Head Count</th>
                    <?
                    for($i=0;$i<=11;$i++)
                    {
                    echo "<th width='60' align='center'>".date("M",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."</th>";
                    }
                    ?>
                    <th></th>
                </thead>
            </table>
        </div>
        <div style="width:945px; overflow:scroll; height:280px" id="scroll_body" align="left">
            <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >	 
                <?
                $bar="";
                $total=0;
                $str_cond="";
                $str_cond_all="";
                
                if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_all="location_id"; $bar = &$location_details;}
                if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_all="division_id"; $bar = &$division_details;}
                if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_all="department_id"; $bar = &$department_details;} 
                if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_all="section_id"; $bar = &$section_details;}
                if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_all="subsection_id"; $bar = &$subsection_details;} 
                if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_all="designation_id"; $bar = &$designation_chart;}
                
                $sql="select distinct($str_cond_all) from hrm_employee where company_id='$active_data[0]' and status_active=1 and is_deleted=0 $str_cond order by $str_cond_all desc";	
                
                $result=mysql_query($sql);
                $co=1;
                while($row=mysql_fetch_array($result))
                {
                echo"<tr>";
                echo"<td width='190'>".$bar[$row[0]]."</td>";
                for($cn=0;$cn<=11;$cn++)
                {
                $str_date=date("Y-m"."%%",strtotime(add_month($month_prev,$cn)));
                //echo $str_date;
                $sql1="select count(*) as data from hrm_separation a,hrm_employee b where a.emp_code=b.emp_code and b.company_id='$active_data[0]' and a.separated_from like '$str_date' and b.$str_cond_all='$row[0]' and b.status_active=0 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0";
                $result1=mysql_query($sql1);
                $row_data=mysql_fetch_assoc($result1);
                if($row_data['data']=="") $row_data['data']=0; else $row_data['data']=$row_data['data']; 
                echo "<td width='60' align='right'>".$row_data['data']."</td>";
                $month_total[$cn]=$month_total[$cn]+$row_data['data'];
                }
                
                echo"<td></td></tr>";
                $co++;}
                echo "<tfoot>
                <th width='190' align='right'>Total=</th>";
                for($j=0;$j<=11;$j++)
                {
                echo "<th width='60' align='right'>$month_total[$j]</th>";
                }
                ?>
                <th></th>
                </tfoot>
            </table>
		</div>
	</div>
<?	

	$html = ob_get_contents();
	ob_clean();
	
	foreach (glob(""."*.pdf") as $filename) {			
		@unlink($filename);
	}

	echo "$html####$filename";
	exit();
}// end leavers report if condition
	

function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}	
	
function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}
?>