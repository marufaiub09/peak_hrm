<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );
 
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	//$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 order by trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}
	

if( $action == "newly_join_summary" ) 
{
	//$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	//$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $company_id=""; else $company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else $department_id="and department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	
	if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(id_card_no), 5)as SIGNED)";}
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="emp_code";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,adivision_id,emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
	
$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by	

	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	ob_start();	
	?>
    <div align="center" style="width:1300px; font-size:16px"><b><? echo $company_details[$cbo_company_id]; ?></b></div>
    <table width="100%" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all" align="left">
	<?
	$sl=0;
		$sql = "SELECT *,COUNT(id) AS tot_per FROM hrm_employee where joining_date between '$txt_from_date' and '$txt_to_date' $category $company_id $location_id  $division_id $department_id  $section_id $subsection_id $salary_based  group by designation_id,section_id $orderby";
		echo $sql;//die;and section_id<>0 
		$result_des = mysql_query( $sql )or die( $sql . "<br />" . mysql_error() );
if(mysql_num_rows($result_des)==0)
{
echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";	
}
else
{
		$designation = array();
		$section = array();
		$sub_sec= array();
		$deper_total= array();
		$div_total= array();
		while( $row_des = mysql_fetch_assoc($result_des) ) 
		{
			$designation[$row_des['designation_id']] = mysql_real_escape_string($row_des['designation_id']);
			$section[$row_des['section_id']][$row_des['designation_id']] = mysql_real_escape_string($row_des['tot_per']);
			$sub_sec[$row_des['section_id']][$row_des['designation_id']] = mysql_real_escape_string($row_des['tot_per']);
			$deper_total[$row_des['section_id']][$row_des['designation_id']] = mysql_real_escape_string($row_des['tot_per']);
			$div_total[$row_des['section_id']][$row_des['designation_id']] = mysql_real_escape_string($row_des['tot_per']);
		}
		//print_r($section);die;
		?>
		<thead> 	
            <th width="20"><b>SL</b></th>
            <th width="100" align="center"><strong>SECTION</strong></th>
            <?
            foreach($designation as $key)
            {
				?>
				<th width="100" align="center"><strong><? echo $designation_chart[$key]; // $designation_chart?></strong></th>
				<?
            }
            ?>
		</thead>  
		<?
		$sql_sec = "SELECT *,COUNT(designation_id) AS tot_per FROM hrm_employee where joining_date between '$txt_from_date' and '$txt_to_date' $category $company_id $location_id  $division_id $department_id  $section_id $subsection_id group by section_id $orderby";
		//echo $sql_sec;die;
		$total=array(); $sum_total=array();
		
		$total_subsec=array();
		$total_depertment=array();
		$total_divi=array();
		
		$result = mysql_query( $sql_sec ) or die( $sql_sec . "<br />" . mysql_error() );
		while( $row = mysql_fetch_assoc($result) ) 
		{
			$sl++;
//====================================================================================================================================================================	
			if($sl==1)
			{
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
			}
			if($sl!=1)
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
					if(in_array($row[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row[subsection_id]]=$row[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row[section_id]]=$row[section_id];
											$subsection_arr=array();
											$subsection_arr[$row[subsection_id]]=$row[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row[department_id]]=$row[department_id];
										$section_arr=array();
										$subsection_arr=array();
										$section_arr[$row[section_id]]=$row[section_id];
										$subsection_arr[$row[subsection_id]]=$row[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row[division_id]]=$row[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row[department_id]]=$row[department_id];
									$section_arr[$row[section_id]]=$row[section_id];
									$subsection_arr[$row[subsection_id]]=$row[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row[location_id]]=$row[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
				
				}
			if($new_subsec==1 && $status_subsec==1 && $sl!=1)
			{
			?>
            <tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="2" width=''><b> Subsection Total</b></td>
                    <?
                    foreach($designation as $key) 
                    {
                    ?>
                    <td width="100" align="center"><strong><? echo $total_subsec[$key]; ?></strong></td>
                    <?
					$total_subsec[$key]=0;
                    }
                    ?>
             </tr>
			<?
			}
			if($new_sec==1 && $status_sec==1 && $sl!=1)
			{
				?>
                <tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="2" width=''><b>SectionTotal</b></td>
                    <?
                    foreach($designation as $key) 
                    {
                    ?>
                    <td width="100" align="center"><strong><? echo $total[$key]; ?></strong></td>
                    <?
					$total[$key]=0;
                    }
                    ?>
                </tr>
				<?
			}
			if($new_dept==1 && $status_dept==1 && $sl!=1)
			{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="2" align="center">Department Total</td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $total_depertment[$key]; ?></strong></td>
                <?
				$total_depertment[$key]=0;
                }
                ?>
			</tr>
			<?
			}
			if($new_divis==1 && $status_divis==1 && $sl!=1)
			{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="2">Division Total</td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $total_divi[$key]; ?></strong></td>
                <?
				$total_divi[$key]=0;
                }
                ?>
			</tr>
			<?
			}
			if($new_loc==1 && $status_loc==1 && $sl!=1)
			{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="2">Location Total</td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $total[$key]; ?></strong></td>
                <?
				//$total[$key]=0;
                }
                ?>
			</tr>
			<?
			}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row[subsection_id]]."  ,";
			}
			if($c_part_1!='')
			{
				//$i=0;$sl=1;
				?><tr style="font-weight:bold; font-size:14px"><td colspan="20"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}
//==========================================================================================================================================================================
			?>	
			<tr>
			<td width="20"><b><? echo $sl; ?></b></td>
			<td width="100" align="center"><strong><? echo $section_details[$row[section_id]]; //$section_details ?></strong></td>
			<?
			//print_r($designation);
			foreach($designation as $key=>$value) 
			{
				$total[$key]+=$section[$row[section_id]][$key];
				$sum_total[$key]+=$section[$row[section_id]][$key];
				?>
				<td width="100" align="center"><strong><? echo $section[$row[section_id]][$key]; ?></strong></td>
				<?
				$total_subsec[$key]+=$sub_sec[$row[section_id]][$value];
				$total_depertment[$key]+=$deper_total[$row[section_id]][$value];
				$total_divi[$key]+=$div_total[$row[section_id]][$value];
			}
			?>
			</tr> 
			<? 
		} //while loop end here================================
		
		if($status_subsec==1)
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td  colspan="2"><b>Subsection Total</b></td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $sum_total[$key]; ?></strong></td>
                <?
                }
                ?>			
			</tr>
			<?
		}
		if($status_sec==1)
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="2">Section Total</td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $sum_total[$key]; ?></strong></td>
                <?
                }
                ?>					
			</tr>
			<?
		} 
		if($status_dept==1 )
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="2">Department Total</td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $sum_total[$key]; ?></strong></td>
                <?
                }
                ?>					
			</tr>
			<?
			
		}
		if($status_divis==1 )
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td  colspan="2">Division Total</td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $sum_total[$key]; ?></strong></td>
                <?
                }
                ?>							
			</tr>
			<?
		}
		if($status_loc==1 )
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td  colspan="2">Location Total</td>
                <?
                foreach($designation as $key) 
                {
                ?>
                <td width="100" align="center"><strong><? echo $sum_total[$key]; ?></strong></td>
                <?
                }
                ?>		
			</tr>
			<?
		}
		?>
        <tr>
			<td colspan="2" align="center"><b>GRAND TOTAL</b></td>
			<?
			foreach($designation as $key) 
			{
				?>
				<td width="100" align="center"><strong><? echo $sum_total[$key]; //$total[$key]; ?></strong></td>
				<?
			}
			?>
			</tr> 
    </table>
	<?
}

	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>