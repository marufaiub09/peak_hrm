<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../includes/common.php");

$separation_type=array(1=>"Resignation",2=>"Retirement",3=>"Disability",4=>"Death",5=>"Terminated",6=>"Un-noticed");
$sex=array(0=>"Male",1=>"Female");
$marital_status=array(0=>"Single",1=>"Married",2=>"Separated",3=>"Widow");
$blood_group=array(0=>"-- Select --",1=>"A+",2=>"A-",3=>"B+",4=>"B-",5=>"AB+",6=>"AB-",7=>"O+",8=>"O-");
$emp_relation=array(0=>"-- Select --",1=>"Husband",2=>"Wife",3=>"Son",4=>"Daughter");

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}
	
extract($_GET);

$active_data=explode("_", $data);
//print_r($active_data);
//print $search_string;

function my_old($dob){
	$now = date('d-m-Y');
	$dob = explode('-', $dob);
	$now = explode('-', $now);
	$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
	if($now[0] < $dob[0]){
		$now[0] += $mnt[$now[1]-1];
		$now[1]--;
	}
	if($now[1] < $dob[1]){
		$now[1] += 12;
		$now[2]--;
	}
	if($now[2] < $dob[2]) return false;
	return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
}

?>
<style type="text/css">
    @page { size 8.5in 11in; margin: 0.5in; }
    div.page { page-break-after: always; background-color:#FFF;}
</style>
<?php
	
	//Active Employee List
if($type=="active_emp_list")
	{
			
		if ($category==0) $category=""; else $category="and category='$category'";
		if ($company_id=='') $company_id =""; else $company_id ="and company_id='$company_id'";
		if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
		if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
		if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";
		if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
		if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
		if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ($emp_code)";
						
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
				FROM hrm_employee 
				WHERE 
				is_deleted=0 and 
				status_active=1 
				$category
				$company_id 
				$location_id 
				$division_id 
				$department_id 
				$section_id 
				$subsection_id
				$designation_id	
				$emp_code			
				order by CAST(id_card_no AS SIGNED) ASC";
		
		$result=mysql_query($sql);
		ob_start();
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{?>

    <div style="width:2050px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="35" align="center"><strong>SL</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Emp Photo</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="130" align="center"><strong>Emp Name</strong></th>
                <th width="140" align="center"><strong>Company</strong></th>
                <th width="120" align="center"><strong>Designation</strong></th>
                <th width="75" align="center"><strong>DOB</strong></th>
                <th width="75" align="center"><strong>DOJ</strong></th>
                <th width="200" align="center"><strong>Address</strong></th>
                <th width="60" align="center"><strong>Sex</strong></th>
                <th width="70" align="center"><strong>Religion</strong></th>
                <th width="130" align="center"><strong>Age</strong></th>
                <th width="160" align="center"><strong>Division</strong></th>
                <th width="130" align="center"><strong>Department</strong></th>
                <th width="100" align="center"><strong>Section</strong></th>
                <th width="100" align="center"><strong>Sub Section</strong></th>
                <th width="130" align="center"><strong>Route</strong></th>
                <th><strong>Vehicle No.</strong></th>
	   		</thead>
       </table>
    </div>
    <div style="width:2050px; overflow-y:scroll; height:280px" id="scroll_body" align="left">
        <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >	 
        <?php                  
		$i=1;
		while($emp=mysql_fetch_assoc($result))
		{
			$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=0 and status_active=1 and is_deleted=0";
			$result1=mysql_query($sql1);
			$row=mysql_fetch_assoc($result1);
			if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
			if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
			if($row['village']=="") $village=""; else $village=$row['village'].", ";
			if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
			if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
			$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
			
			$sql2="SELECT * FROM `hrm_transport_mst` WHERE `vehicle_no` in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
			$result2=mysql_query($sql2);
			$row2=mysql_fetch_assoc($result2);
			
			$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
			$image="";
			if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
	?>
			<tr>
				<td width="35" align="center"><?php echo $i;?></td>
                <td width="80">&nbsp;<?php echo $emp['id_card_no'];?></td>
                <td width="80" valign="bottom" align="center"><?php echo $image; ?></td>
				<td width="80">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
				<td width="130"><?php echo $emp['name'] ; ?></td>
                <td width="140"><?php echo $company_details[$emp['company_id']]; ?></td>
				<td width="120">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
				<td width="75" align="center"><?php echo convert_to_mysql_date($emp['dob']) ; ?></td>
				<td width="75" align="center"><?php echo convert_to_mysql_date($emp['joining_date']) ; ?></td>
				<td width="200"><?php echo $address ; ?></td>
				<td width="60" align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
				<td width="70" align="center"><?php echo $emp['religion'] ; ?></td>
				<td width="130"><?php
					$birth_date=convert_to_mysql_date($emp['dob']);
					$age = my_old($birth_date);
					printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);
					?></td>
               <td width="160">&nbsp;<?php echo $division_details[$emp['division_id']]; ?></td>
               <td width="130">&nbsp;<?php echo $department_details[$emp['department_id']]; ?></td>
               <td width="100">&nbsp;<?php echo $section_details[$emp['section_id']]; ?></td>
               <td width="100">&nbsp;<?php echo $subsection_details[$emp['subsection_id']]; ?></td> 
               <td width="130">&nbsp;<?php echo $row2['route']; ?></td>
               <td>&nbsp;<?php echo $row2['vehicle_no']; ?></td> 
			</tr>
			<?php $i++;}
			
			?>
        </table>
    </div>

<?php
			
	}  
	   $html = ob_get_contents();
		ob_clean();	
		
		foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
				@unlink($filename);
		}		
		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,trim($html));	
		
		echo "$html"."####"."$name";		
		exit();
			
} //end Active Employee List if condition
		
		
			
// Start Salary Breakdown
if($type=="salary_breakdown_report")
	{
		
		if($active_data[0]=="") $category="%%"; else $category=$active_data[0];
		if($active_data[1]==0) $company_name="%%"; else $company_name=$active_data[1];
		if($active_data[2]==0) $location_name="%%"; else $location_name=$active_data[2];
		if($active_data[3]==0) $division_name="%%"; else $division_name=$active_data[3];
		if($active_data[4]==0) $department_name="%%"; else $department_name=$active_data[4];
		if($active_data[5]==0) $section_name="%%"; else $section_name=$active_data[5];
		if($active_data[6]==0) $designation_name="%%"; else $designation_name=$active_data[6];
		if($active_data[7]==0) $subsection_name="%%"; else $subsection_name=$active_data[7];
		if($search_string=="") $search_string="%%"; else $search_string="%".trim($search_string)."%";
		if($emp_id=="") $search_emp_id="%%"; else $search_emp_id="%".trim($emp_id)."%";
		
		$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name FROM hrm_employee WHERE emp_code like '$search_string' and id_card_no like '$search_emp_id' and category like '$category' and designation_id like '$designation_name' and company_id like '$company_name' and location_id like '$location_name' and division_id like '$division_name' and department_id like '$department_name' and section_id like '$section_name' and subsection_id like '$subsection_name' and is_deleted=0 and status_active=1 order by id_card_no ASC";
		$result=mysql_query($sql);
		ob_start();
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{?>
<div>
    <div style="width:1500px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="35" align="center"><strong>SL</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Emp Photo</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="130" align="center"><strong>Emp Name</strong></th>
                <th width="80" align="center"><strong>DOJ</strong></th>
                <th width="140" align="center"><strong>Company</strong></th>
                <th width="120" align="center"><strong>Designation</strong></th>
                <th width="130" align="center"><strong>Department</strong></th>
                <th width="80" align="center"><strong>Salary Grade</strong></th>
                <th width="80" align="center"><strong>Basic Salary</strong></th>
                <th width="80" align="center"><strong>House Rent</strong></th>
                <th width="80" align="center"><strong>Medical Allowance</strong></th>
                <th width="80" align="center"><strong>Conveyance Allowance</strong></th>
                <th width="80" align="center"><strong>Others Allowance</strong></th>
                <th align="center"><strong>Gross Salary</strong></th>
	   		</thead>
       </table>
    </div>   
    <div style="width:1500px; overflow:scroll; height:280px" id="scroll_body" align="left">
   		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >	 
	  <?php                  
		$i=1; $gross==0;
		while($emp=mysql_fetch_assoc($result))
		{
			
			$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
			$image="";
			if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
			
			$sql= "SELECT
				sum(CASE WHEN payroll_head=1 THEN amount END) AS 'basic_salary',
				sum(CASE WHEN payroll_head=2 THEN amount END) AS 'house_rent',
				sum(CASE WHEN payroll_head=3 THEN amount END) AS 'medical_allowance',
				sum(CASE WHEN payroll_head=4 THEN amount END) AS 'conveyance_allowance',
				sum(CASE WHEN payroll_head=19 THEN amount END) AS 'others_allowance'
				from hrm_employee_salary where emp_code='$emp[emp_code]' group by emp_code";
			$selectResult=mysql_query($sql);
			$row=mysql_fetch_array($selectResult);
			
			$gross=$row['basic_salary']+$row['house_rent']+$row['medical_allowance']+$row['conveyance_allowance']+$row['others_allowance'];
			$gross=number_format($gross,2,'.','');
	?>
			<tr>
				<td width="35" align="left"><?php echo $i;?></td>
                <td width="80" align="left">&nbsp;<?php echo $emp['id_card_no'];?></td>
                <td width="80" valign="bottom" align="center"><?php echo $image; ?></td>
				<td width="80" align="left">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
				<td width="130" align="left">&nbsp;<?php echo $emp['name'] ; ?></td>
                <td width="80" align="center"><?php echo convert_to_mysql_date($emp['joining_date']) ; ?></td>
                <td width="140"><?php echo $company_details[$emp['company_id']]; ?></td>
				<td width="120" align="left">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
				<td width="130" align="left">&nbsp;<?php echo $department_details[$emp['department_id']]; ?></td>
                <td width="80" align="center"><?php echo $emp['salary_grade'] ; ?></td>
                <td width="80" align="right"><?php if($row['basic_salary']=="") echo number_format(0,2); else echo $row['basic_salary'] ; ?></td>
                <td width="80" align="right"><?php if($row['house_rent']=="") echo number_format(0,2); else echo $row['house_rent'] ; ?></td>
                <td width="80" align="right"><?php if($row['medical_allowance']=="") echo number_format(0,2); echo $row['medical_allowance'] ; ?></td>
                <td width="80" align="right"><?php if($row['conveyance_allowance']=="") echo number_format(0,2); echo $row['conveyance_allowance'] ; ?></td>
                <td width="80" align="right"><?php if($row['others_allowance']=="") echo number_format(0,2); echo $row['others_allowance'] ; ?></td>
                <td align="right"><?php echo $gross ; ?></td>
			</tr> 
			<?php $i++;}
						} // end else
			?>
       </table>
    </div>
</div>
		<?php
			$html = ob_get_contents();
		    ob_clean();
		   
			foreach (glob(""."*.pdf") as $filename) {			
                @unlink($filename);
			}
			
			echo "$html####$filename";
			exit();
	} //end Salary Breakdown if condition
			
//separated Employee List start if condition
if($type=="separated_emp_list")
			{
				if($active_data[0]==0) $category="%%"; else $category=$active_data[0];
				if($active_data[1]==0) $company_name="%%"; else $company_name=$active_data[1];
				if($active_data[2]==0) $location_name="%%"; else $location_name=$active_data[2];
				if($active_data[3]==0) $division_name="%%"; else $division_name=$active_data[3];
				if($active_data[4]==0) $department_name="%%"; else $department_name=$active_data[4];
				if($active_data[5]==0) $section_name="%%"; else $section_name=$active_data[5];
				if($active_data[6]==0) $designation_name="%%"; else $designation_name=$active_data[6];
				if($active_data[7]=="") $from_date=""; else $from_date=convert_to_mysql_date($active_data[7]);
				if($active_data[8]=="") $to_date=""; else $to_date=convert_to_mysql_date($active_data[8]);
				if($from_date!="" && $to_date=="")
				{
					$to_date=$from_date;	
				}
				if($from_date=="") $str_date=""; else $str_date=" and separated_from between '$from_date' and '$to_date' ";
				if($search_string=="") $search_string="%%"; else $search_string="%".trim($search_string)."%";
				
					
				$query="SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
				FROM hrm_employee WHERE emp_code like '$search_string' and designation_id like '$designation_name' and company_id like '$company_name' and location_id like '$location_name' and division_id like '$division_name' and department_id like '$department_name' and section_id like '$section_name' and is_deleted=0 and status_active=0 and emp_code in(select emp_code from hrm_separation where status_active=1 and is_deleted=0 and separation_type like '$category' $str_date) order by designation_level ASC";
				$result=mysql_query($query);
					
				if(mysql_num_rows($result)==0)
					{
						echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
					}
					else
					{?>
<div>
    <div style="width:1500px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="35" align="center"><strong>SL</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="130" align="center"><strong>Emp Name</strong></th>
                <th width="80" align="center"><strong>Card No.</strong></th>
                <th width="140" align="center"><strong>Company</strong></th>
                <th width="120" align="center"><strong>Designation</strong></th>
                <th width="130" align="center"><strong>Department</strong></th>
                <th width="75" align="center"><strong>DOJ</strong></th>
                <th width="75" align="center"><strong>DOC</strong></th>
                <th width="90" align="center"><strong>Gross Salary</strong></th>
                <th width="60" align="center"><strong>Sex</strong></th>
                <th width="70" align="center"><strong>Religion</strong></th>
                <th width="75" align="center"><strong>DOB</strong></th>
                <th width="75" align="center"><strong>DOS</strong></th>
                <th width="110" align="center"><strong>Sep. Type</strong></th>
                <th align="center"><strong>Cause of Sep.</strong></th>
	   		</thead>
       </table>
    </div>
    <div style="width:1500px; overflow:scroll; height:280px" id="scroll_body" align="left">
        <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >	
		
	  <?php  
	  	$i=1;
		while($emp=mysql_fetch_assoc($result))
		{
			
			$sql1="select * from hrm_separation where emp_code='$emp[emp_code]' and separation_type like '$category' and status_active=1 and is_deleted=0";
			$result1=mysql_query($sql1);
			$row=mysql_fetch_assoc($result1);
		?>
			<tr>
				<td width="35"><?php echo $i;?></td>
				<td width="80">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
				<td width="130"><?php echo $emp['name'] ; ?></td>
                <td width="80">&nbsp;<?php echo $emp['id_card_no'] ; ?></td>
                <td width="140"><?php echo $company_details[$emp['company_id']]; ?></td>
				<td width="120">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
                <td width="130">&nbsp;<?php echo $department_details[$emp['department_id']]; ?></td>
                <td width="75" align="center"><?php echo convert_to_mysql_date($emp['joining_date']) ; ?></td>
                <td width="75" align="center"><?php echo convert_to_mysql_date($emp['confirmation_date']); ?></td>
                <td width="90"><?php echo $emp['gross_salary'] ; ?></td>
                <td width="60"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
				<td width="70"><?php echo $emp['religion'] ; ?></td>
				<td width="75" align="center"><?php echo convert_to_mysql_date($emp['dob']); ?></td>
				<td width="75" align="center"><?php echo convert_to_mysql_date($row['separated_from']); ?></td>
                <td width="110">&nbsp;<?php echo $separation_type[$row['separation_type']]; ?></td>
                <td>&nbsp;<?php echo $row['cause_of_separation']; ?></td>
			</tr>
			<?php $i++;}
						} // end else
			?>
        </table>
    </div>
</div>
    <?php  
			$html = ob_get_contents();
		    ob_clean();
		   
			foreach (glob(""."*.pdf") as $filename) {			
                @unlink($filename);
			}
			
			echo "$html####$filename";
			exit();
		              
}// end separated Employee List  if condition 
			
// Headcount of Employees start if condition
if($type=="headcount_of_employees")
{
		$active_data=explode("_", $data);
		//print_r($active_data);
		
		$to_date=convert_to_mysql_date($active_data[2]);
		 
		$month_prev=add_month($active_data[2],-11);
		 
		$start_mon=date("M",strtotime($month_prev));
		$end_mon=date("M",strtotime($to_date));
		  
	?>	
<div>
    <div style="width:955px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
        	<thead>
            	<th width='190' align='center'>Head Count</th>
		
       		 <? 
				for($i=0;$i<=11;$i++)
				{
					
					echo "<th width='60' align='center'>".date("M",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."</th>";
				}
			?>
            	<th></th>
			</thead>
         </table>
      </div>
    <div style="width:955px; overflow:scroll; height:280px" id="scroll_body" align="left">
        <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >	 

        	<?
				$bar="";
				$total=0;
				$str_cond="";
				$str_cond_all="";
				
				if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_all="location_id"; $bar = &$location_details;}
				if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_all="division_id"; $bar = &$division_details;}
				if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_all="department_id"; $bar = &$department_details;} 
				if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_all="section_id"; $bar = &$section_details;}
				if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_all="subsection_id"; $bar = &$subsection_details;} 
				if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_all="designation_id"; $bar = &$designation_chart;}
					$sql="select distinct($str_cond_all) from hrm_employee where company_id='$active_data[0]' and status_active=1 and is_deleted=0 $str_cond order by $str_cond_all desc";	
					$result=mysql_query($sql);
					$co=1;
					while($row=mysql_fetch_array($result))
					{
						/*if ($co%2==0)  
							$tr_bgcolor="#E9F3FF";
						else
							$tr_bgcolor="#FFFFFF";
							
						echo"<tr id='$co' onclick='toggle($co,\"$tr_bgcolor\")'>";*/
						echo"<tr id='$co'>";
							echo"<td width='190'>".$bar[$row[0]]."</td>";
							for($cn=0;$cn<=11;$cn++)
							{
								$cbo_year_selector=date("Y"."%%",strtotime(add_month($month_prev,$cn)));
								$cbo_month_selector=date("m"."%%",strtotime(add_month($month_prev,$cn)));
								$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
								$str_date=date("Y-m",strtotime(add_month($month_prev,$cn)));
								$str_last=$str_date."-".$get_days;
								//echo $str_last;
								// $sql1="select count(*) as data from hrm_employee a,hrm_employee_job b  where a.emp_code=b.emp_code and b.company_id='$active_data[0]' and a.joining_date<='$str_last' and $str_cond_search='$row[0]' and a.emp_code not in (select emp_code from hrm_separation where separated_from<='$str_last') and a.status_active=1 and a.is_deleted=0 $str_cond2";
								$sql1="select count(*) as data from hrm_employee where company_id='$active_data[0]' and joining_date<='$str_last' and $str_cond_all='$row[0]' and emp_code not in (select emp_code from hrm_separation where separated_from<='$str_last') and status_active=1 and is_deleted=0 $str_cond";
								$result1=mysql_query($sql1);
								$row_data=mysql_fetch_assoc($result1);
								if($row_data['data']=="") $row_data['data']=0; else $row_data['data']=$row_data['data']; 
								echo "<td width='60' align='right'>".$row_data['data']."</td>";
								$month_total[$cn]=$month_total[$cn]+$row_data['data'];
							}
						
						echo"<td></td></tr>";
					$co++;}
					
				echo "<tfoot>
						<th width='190' align='right'>Total:</th>";
						for($j=0;$j<=11;$j++)
						{
						echo "<th width='60' align='right'>$month_total[$j]</th>";
						}
			?>
            	<th></th>
            </tfoot>
		</table>
    </div>
</div>
	<?	
	$html = ob_get_contents();
    ob_clean();
   
	foreach (glob(""."*.pdf") as $filename) {			
		@unlink($filename);
	}
	
	echo "$html####$filename";
	exit();
			
}// end head count type if condition
			
	// start leavers report if condition		
if($type=="leavers_report")
{
	$active_data=explode("_", $data);
	//print_r($active_data);
	
	$to_date=convert_to_mysql_date($active_data[2]);
	 
	$month_prev=add_month($active_data[2],-11);
	 
	$start_mon=date("M",strtotime($month_prev));
	$end_mon=date("M",strtotime($to_date));
?>
<div>
    <div style="width:945px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
        	<thead>
            	<th width='190' align='center'>leavers Head Count</th>
	<?
			 
			for($i=0;$i<=11;$i++)
			{
				
				echo "<th width='60' align='center'>".date("M",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."</th>";
			}
		?>
 				<th></th>
			</thead>
         </table>
      </div>
    <div style="width:945px; overflow:scroll; height:280px" id="scroll_body" align="left">
        <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >	 
		<?
			$bar="";
			$total=0;
			$str_cond="";
			$str_cond_all="";

			if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_all="location_id"; $bar = &$location_details;}
			if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_all="division_id"; $bar = &$division_details;}
			if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_all="department_id"; $bar = &$department_details;} 
			if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_all="section_id"; $bar = &$section_details;}
			if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_all="subsection_id"; $bar = &$subsection_details;} 
			if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_all="designation_id"; $bar = &$designation_chart;}
			
			   $sql="select distinct($str_cond_all) from hrm_employee where company_id='$active_data[0]' and status_active=1 and is_deleted=0 $str_cond order by $str_cond_all desc";	

				$result=mysql_query($sql);
				$co=1;
				while($row=mysql_fetch_array($result))
				{
						
					echo"<tr>";
						echo"<td width='190'>".$bar[$row[0]]."</td>";
						for($cn=0;$cn<=11;$cn++)
						{
							$str_date=date("Y-m"."%%",strtotime(add_month($month_prev,$cn)));
							//echo $str_date;
							$sql1="select count(*) as data from hrm_separation a,hrm_employee b where a.emp_code=b.emp_code and b.company_id='$active_data[0]' and a.separated_from like '$str_date' and b.$str_cond_all='$row[0]' and b.status_active=0 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0";
							$result1=mysql_query($sql1);
							$row_data=mysql_fetch_assoc($result1);
							if($row_data['data']=="") $row_data['data']=0; else $row_data['data']=$row_data['data']; 
							echo "<td width='60' align='right'>".$row_data['data']."</td>";
							$month_total[$cn]=$month_total[$cn]+$row_data['data'];
						}
					
					echo"<td></td></tr>";
				$co++;}
			echo "<tfoot>
					<th width='190' align='right'>Total=</th>";
					for($j=0;$j<=11;$j++)
					{
					echo "<th width='60' align='right'>$month_total[$j]</th>";
					}
			?>
            	<th></th>
            </tfoot>
		</table>
    </div>
</div>
<?	

	$html = ob_get_contents();
	ob_clean();
	
	foreach (glob(""."*.pdf") as $filename) {			
		@unlink($filename);
	}

	echo "$html####$filename";
	exit();
}// end leavers report if condition
	



 
if($type=="starters_report_summary")
{
				//$active_data=explode("_", $data);				
				$to_date=convert_to_mysql_date($to_date);				 
				$month_prev=add_month($to_date,-11);				 
				$start_mon=date("M",strtotime($month_prev));
				$end_mon=date("M",strtotime($to_date));
	
	?>	
   
    <div style="width:983px;">	
        <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
        	<thead>
            	<th width='190'>Starters Head Count</th>
					<?	
						for($i=0;$i<=11;$i++)
						{
							echo "<th width='60' align='center'>".date("M",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."</th>";
						}
					?>                    
				</thead>
				<tbody>	
               		 <?
						$bar="";
						$str_cond="";
						$str_cond_all="";
						$total=0;
						$new_department = array();
						
						if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
						if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
						if ($department_id==0) $department_id=""; else	$department_id=" and department_id='$department_id'";
						if ($division_id==0) $division_id=""; else	$division_id=" and division_id='$division_id'";	
						if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
						if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
						
						$sql = "select * 
								from hrm_employee 
								where 
								status_active=1 and 
								is_deleted=0 
								$category
								$cbo_company_id
								$department_id
								$division_id
								$section_id
								$designation_id
								group by department_id,section_id desc";	
						//echo $sql;die;
						$result=mysql_query($sql);
						$co=1;
						while($row=mysql_fetch_array($result))
						{
								
							if (in_array($row['department_id'], $new_department))
							{}						
							else
							{	
								$new_department[$row['department_id']]=$row['department_id'];
								if($co!=1){
									echo "<tr><th width='190'>Sub Total</th>";
									foreach($sub_month_total as $key=>$val)
									{
										echo "<th width='60' align='right'>$val</th>";
									}
									echo "</tr>";
								}
								echo "<tr><td colspan=13><b>Department Name : ".$department_details[$row[department_id]]."</b></td></tr>";
								$sub_month_total="";
							}
							
							if ($co%2==0) $bgcolor="#EFEFEF"; 
							else $bgcolor="#FFFFFF";
							
							echo"<tr bgcolor='$bgcolor'>";
								echo "<td width='190'>".$section_details[$row[section_id]]."</td>";
								for($cn=0;$cn<=11;$cn++)
								{
									
									$str_date=date("Y-m"."%%",strtotime(add_month($month_prev,$cn)));
									//echo $str_date;
									
								    $sql1="select count(*) as data 
										   from hrm_employee 
										   where 
										   company_id=$row[company_id] and 
										   department_id=$row[department_id] and
										   section_id=$row[section_id] and
										   joining_date like '$str_date' and 
										   status_active=1 and is_deleted=0"; 
								   
									$result1=mysql_query($sql1);
									$row_data=mysql_fetch_assoc($result1);
									if($row_data['data']=="") $row_data['data']=0; else $row_data['data']=$row_data['data']; 
									echo "<td width='60' align='right'>".$row_data['data']."</td>";
									$sub_month_total[$cn] += $row_data['data'];
									$month_total[$cn] += $row_data['data'];
								}
							echo"</tr>";
							$co++;
						
						}
							
						
				echo "<tfoot><tr>
						<th width='190'>Sub Total</th>";
						foreach($sub_month_total as $key=>$val)
						{
							echo "<th width='60' align='right'>$val</th>";
						}
				echo "</tr>";
			    echo "<tr><th width='190'>Total</th>";
				
						foreach($month_total as $key=>$val)
						{
							echo "<th width='60' align='right'>$val</th>";
						}
					?>            	
           	</tr></tfoot>
		</table>

	<?				
		
		$html=ob_get_contents();		
		ob_clean();
		
		
		//for report temp file delete 
		foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
				@unlink($filename);
		}		
		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);
		
		echo "$html"."####"."$name";		
		exit();
			
		
}// end if condition starters report





// start if condition starters report detail
if($type=="starters_report_detail")
{		 
		
		ob_start();
		$from_date=convert_to_mysql_date($from_date);
		$to_date=convert_to_mysql_date($to_date);
		 		
		if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
		if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
		if ($department_id==0) $department_id=""; else	$department_id=" and department_id='$department_id'";	
		if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
		if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
		
		
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
				FROM hrm_employee 
				WHERE 
				joining_date between '$from_date' and '$to_date' and 
				is_deleted=0 and 
				status_active=1 
				$category
				$cbo_company_id
				$department_id
				$section_id
				$designation_id
				order by section_id,joining_date ASC"; 
		//echo $sql;
		$result=mysql_query($sql);
		$new_section=array();	
			//echo $sql;die;
			if(mysql_num_rows($result)==0)
			{
					echo "<p align='center' style='font-size:16px' style='color:#000'><b>No Data Found.</b></p>";
			}
			else
			{
				 ?>
                 
                 
                        
				 <?php  
						$j=1;
						while($emp=mysql_fetch_assoc($result))
						{
							
								$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=0 and status_active=1 and is_deleted=0";
								$result1=mysql_query($sql1);
								$row=mysql_fetch_assoc($result1);
								if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
								if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
								if($row['village']=="") $village=""; else $village=$row['village'].", ";
								if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
								if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
								$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
								
								$sql2="SELECT * FROM `hrm_transport_mst` WHERE `vehicle_no` in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
								$result2=mysql_query($sql2);
								$row2=mysql_fetch_assoc($result2);
								
								$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
								$image="";
								if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
								
								if (in_array($emp['section_id'], $new_section))
								{
									$i++;
								}
								else
								{	
									$new_section[$emp['section_id']]=$emp['section_id'];
									
								
								if($j!=1) echo "</table><br />";$small_print .='</table><br />';
						?>
                        	
                                <div align="center">
                                    <span style="font-size:18px; font-weight:bold"><? echo $company_details[$emp[company_id]]; ?></span><br />
                                    <b>Recruitment Panel From <? echo convert_to_mysql_date($from_date); ?> To <? echo convert_to_mysql_date($to_date); ?></b> 
                                </div>
                                <div align="left"><b>Section : <? echo $section_details[$emp[section_id]];?></b></div>
                         
						 <?  $small_print .='<div align="center" style="width:900px">
												<span style="font-size:18px; font-weight:bold">'.$company_details[$emp[company_id]].'</span><br />
												<b>Recruitment Panel From '.$from_date.' To '.$to_date.'</b></div> <br />
												<div align="left"><b>Section : '.$section_details[$emp[section_id]].'</b></div>';
											 
						 
						 	 $small_print .='<table id="table_body" cellspacing="0" cellpadding="0" class="rpt_table">
											<thead>                            
												<th width="50" align="center"><strong>SL</strong></th>
												<th width="150" align="center"><strong>Name</strong></th>
												<th width="80" align="center"><strong>Emp ID</strong></th>
												<th width="90" align="center"><strong>Emp Code</strong></th>
												<th width="120" align="center"><strong>Designation</strong></th>
												<th width="80" align="center"><strong>DOJ</strong></th>
												<th width="90" align="center"><strong>Grade</strong></th>
												<th width="110" align="center"><strong>Gross</strong></th>
												<th width="" align="center"><strong>Remarks</strong></th>
											</thead>';
						 
						 ?>      
                               
                                <table id="table_body" cellspacing="0" cellpadding="0" class="rpt_table" >
                                    <thead>                            
                                        <th width="35" align="center"><strong>SL</strong></th>
                                        <th width="80" align="center"><strong>ID</strong></th>
                                        <th width="80" align="center"><strong>Photo</strong></th>
                                        <th width="90" align="center"><strong>Emp Code</strong></th>
                                        <th width="130" align="center"><strong>Name</strong></th>
                                        <th width="140" align="center"><strong>Company</strong></th>
                                        <th width="120" align="center"><strong>Designation</strong></th>
                                        <th width="75" align="center"><strong>DOB</strong></th>
                                        <th width="75" align="center"><strong>DOJ</strong></th>
                                        <th width="75" align="center"><strong>Grade</strong></th>
                                        <th width="75" align="center"><strong>Gross</strong></th>
                                        <th width="200" align="center"><strong>Address</strong></th>
                                        <th width="60" align="center"><strong>Sex</strong></th>
                                        <th width="70" align="center"><strong>Religion</strong></th>
                                        <th width="130" align="center"><strong>Age</strong></th>
                                        <th width="160" align="center"><strong>Division</strong></th>
                                        <th width="130" align="center"><strong>Department</strong></th>
                                        <th width="100" align="center"><strong>Section</strong></th>
                                        <th width="100" align="center"><strong>Sub Section</strong></th>
                                        <th width="130" align="center"><strong>Route</strong></th>
                                        <th width="100"><strong>Vehicle No.</strong></th>
                                    </thead>
										
						<?
									
								}//new section 
						
						
								
								$small_print .='<tr>
													<td width="35" align="center">'.$j.'</td>
													<td width="130">'.$emp[name].'</td>
													<td width="80">&nbsp;'.$emp[id_card_no].'</td>
													<td width="90">&nbsp;'.$emp[emp_code].'</td>
													<td width="120">&nbsp;'.$designation_chart[$emp[designation_id]].'</td>
													<td width="75" align="center">'.convert_to_mysql_date($emp[joining_date]).'</td>
												    <td width="90" align="right">'.$emp[salary_grade].'</td>
													<td width="110" align="right">'.$emp[gross_salary].'</td>
												    <td width="130">&nbsp;</td>
												</tr>';
								
						?>
							
                                <tr>
                                    <td width="35" align="center"><?php echo $j;?></td>
                                    <td width="80">&nbsp;<?php echo $emp['id_card_no'];?></td>
                                    <td width="80" valign="bottom" align="center"><?php echo $image; ?></td>
                                    <td width="90">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
                                    <td width="130"><?php echo $emp['name'] ; ?></td>
                                    <td width="140"><?php echo $company_details[$emp['company_id']]; ?></td>
                                    <td width="120">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
                                    <td width="75" align="center"><?php echo convert_to_mysql_date($emp['dob']) ; ?></td>
                                    <td width="75" align="center"><?php echo convert_to_mysql_date($emp['joining_date']) ; ?></td>
                                    <td width="75" align="center"><?php echo $emp['salary_grade'] ; ?></td>
                                    <td width="75" align="center"><?php echo $emp['gross_salary'] ; ?></td>
                                    <td width="200"><?php echo $address ; ?></td>
                                    <td width="60" align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
                                    <td width="70" align="center"><?php echo $emp['religion'] ; ?></td>
                                    <td width="130"><?php
                                        $birth_date=convert_to_mysql_date($emp['dob']);
                                        $age = my_old($birth_date);
                                        printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);
                                        ?></td>
                                   <td width="160">&nbsp;<?php echo $division_details[$emp['division_id']]; ?></td>
                                   <td width="130">&nbsp;<?php echo $department_details[$emp['department_id']]; ?></td>
                                   <td width="100">&nbsp;<?php echo $section_details[$emp['section_id']]; ?></td>
                                   <td width="100">&nbsp;<?php echo $subsection_details[$emp['subsection_id']]; ?></td> 
                                   <td width="130">&nbsp;<?php echo $row2['route']; ?></td>
                                   <td width="100">&nbsp;<?php echo $row2['vehicle_no']; ?></td> 
								</tr>
						   
							<?php 
								$j++;
								
							}// end while loop
										
			$small_print .='</table>';
				
			?>			
			
            </table>
	<?		
		
		$html=ob_get_contents();		
		ob_clean();
		
		
		//for report temp file delete 
		foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
				@unlink($filename);
		}		
		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);
		
		echo "$html"."####"."$name"."####".$small_print;		
		exit();
			
		}
		
		
		
}// end if condition starters report detail



if($type=="emp_profile")// Start emp Profile if
{
		if ($category==0) $category=""; else $category="and category='$category'";
		if ($company_id=='') $company_id =""; else $company_id ="and company_id='$company_id'";
		if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
		if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
		if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";
		if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
		if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
		if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ($emp_code)";
		
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
				FROM hrm_employee 
				WHERE 
				is_deleted=0 and 
				status_active=1 
				$category
				$company_id 
				$location_id 
				$division_id 
				$department_id 
				$section_id 
				$subsection_id
				$designation_id	
				$emp_code			
				order by CAST(id_card_no AS SIGNED) ASC";
		$result=mysql_query($sql);
		
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{
			$i=0;				
		while($emp=mysql_fetch_assoc($result))
		{
		
			$row=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
			$image="";
			if($row=="") $image=""; else $image="<img src='../../../$row' height='160' width='160' />";
?>
<div style="width:900px;" class="page">
	 <div align="center" style="font-size:16px; margin-top:15px"><b>Employee Profile</b></div>
     <div style="float: right; clear: left; width:300px; margin-top:30px"><table border="1"><tr height="160px"><td width="160px"><?php echo $image; ?></td></tr></table></div>
     <div style="width:800px;margin-left:30px;">
	<table  width="500px" cellpadding="2" cellspacing="1" border="0">
        <tr>
            <td width="200">Employee Code</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[emp_code]; ?></td>
       </tr>
       <tr>
            <td width="200">Name</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[name]; ?></td>
       </tr>
       <tr>
            <td width="200">Father's Name</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[father_name]; ?></td>
       </tr>
       <tr>
            <td width="200">Mother's Name</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[mother_name]; ?></td>
       </tr>
       <?php
		$sql5="select * from hrm_employee_address where emp_code='$emp[emp_code]' and (address_type=0 || address_type=1) and status_active=1 and is_deleted=0";
		 $result5=mysql_query($sql5);
		 if(mysql_num_rows($result5)==0)
		 {?>
	   <tr>
            <td valign="top"  width="200">Present Address </td>
            <td valign="top" width="20">:</td>
            <td width="200">&nbsp;</td>
       </tr>
	   <tr>
            <td valign="top"  width="200">Permanent Address </td>
            <td valign="top" width="20">:</td>
            <td width="200">&nbsp;</td>
       </tr> 
	<?php }
	else
	{
		while($row5=mysql_fetch_assoc($result5))
		{
			if($row5['house_no']=="") $house_no=""; else $house_no="House#".$row5['house_no'].", ";
			if($row5['road_no']=="") $road_no=""; else $road_no="Road#".$row5['road_no'].", <br />";
			if($row5['village']=="") $village=""; else $village=$row5['village'].", <br />";
			if($row5['post_code']=="") $post_code=""; else $post_code=$row5['post_code'].", ";
			if($row5['thana']=="") $thana=""; else $thana=$row5['thana'].", ";
			
			 if($row5['address_type']==0)
			 {
			 ?>
	   <tr>
            <td valign="top"  width="200">Present Address </td>
            <td valign="top" width="20">:</td>
            <td width="200"><?php echo $house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row5['district_id']]; ?>
            </td>
       </tr>
	   <?php }
			 else if($row5['address_type']==1)
			 {?>
	   <tr>
            <td valign="top"  width="200">Permanent Address </td>
            <td valign="top" width="20">:</td>
            <td width="200"><?php echo $house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row5['district_id']]; ?>
            </td>
       </tr> 
			<?php }
		}}?>
        <tr>
            <td width="200">Date of Birth</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['dob']; ?></td>
       </tr>
        <tr>
            <td width="200">Marital Status</td>
            <td width="20">:</td>
            <td width="200"><?php echo $marital_status[$emp['marital_status']]; ?></td>
       </tr>
        <tr>
            <td width="200">Nationality</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['nationality']; ?></td>
       </tr>
        <tr>
            <td width="200">Religion</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['religion']; ?></td>
       </tr>
       <tr>
            <td width="200">Education</td>
            <td width="20">:</td>
            <td width="200">&nbsp;</td>
       </tr>
   </table>
   <br />
   <table class="display" border="1"  width="80%">
        <tr>
            <td align="center">Exam Name</td>
            <td align="center">Institute Name</td>
            <td align="center">Passing Year</td>
            <td align="center">Board/University</td>
            <td align="center">Group</td>
            <td align="center">Division/CGPA</td>
        </tr>
        <?php 
			$sql2="select * from hrm_employee_education where emp_code='$emp[emp_code]' and is_deleted=0 order by passing_year ASC";
			$result2=mysql_query($sql2);
			while($row2=mysql_fetch_assoc($result2))
			{
		?>
        <tr>
            <td><?php echo $row2['exam_name']; ?></td>
            <td><?php echo $row2['institution']; ?></td>
            <td align="right"><?php echo $row2['passing_year']; ?></td>
            <td align="center"><?php echo $row2['board']; ?></td>
            <td align="center"><?php echo $row2['discipline']; ?></td>
            <td align="center"><?php echo $row2['result']; ?></td>
        </tr><?php } ?>
    </table>
    <br />
    <table width="500px" border="0">
    	<tr>
        	<td width="200">Employment History</td>
            <td width="20">:</td>
            <td width="200">&nbsp;</td>
        </tr>
    </table>
     <table class="display" border="1" width="600px">
        <tr>
            <td align="center">Company Name</td>
            <td align="center">Designation</td>
            <td align="center">Start Date</td>
            <td align="center">End Date</td>
        </tr>
         <?php 
			$sql3="select * from hrm_employee_experience where emp_code='$emp[emp_code]' and is_deleted=0";
			$result3=mysql_query($sql3);
			while($row3=mysql_fetch_assoc($result3))
			{
		?>
        <tr>
            <td><?php echo $row3['organization_name']; ?></td>
            <td><?php echo $row3['designation']; ?></td>
            <td align="center"><?php echo $row3['joining_date']; ?></td>
            <td align="center"><?php echo $row3['resigning_date']; ?></td>
        </tr><?php }?>
    </table>
    <br />
   <table  width="500px" cellpadding="2" cellspacing="1" border="0">
        <tr>
            <td width="200">Blood Group</td>
            <td width="20">:</td>
            <td width="200"><?php echo $blood_group[$emp['blood_group']]; ?></td>
       </tr>
       <tr>
            <td width="200">Department</td>
            <td width="20">:</td>
            <td width="200"><?php echo $department_details[$emp['department_id']]; ?></td>
       </tr>
       <tr>
            <td width="200">Designation</td>
            <td width="20">:</td>
            <td width="200"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
       </tr>
       <tr>
            <td width="200">Joining Date</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['joining_date']; ?></td>
       </tr>
       <tr>
            <td width="200">Confirmation Date</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['confirmation_date']; ?></td>
       </tr>
       <?php 
	   		if($emp[sex]==0) $str_con=" and relation=2"; else $str_con=" and relation=1";
			
	   		$sql8="select * from hrm_employee_family where emp_code='$emp[emp_code]' and status_active=1 and is_deleted=0 $str_con";
			$result8=mysql_query($sql8);
			$row8=mysql_fetch_assoc($result8);
		?>
            <tr>
                <td width="200">Spouse Name</td>
                <td width="20">:</td>
                <td width="200"><?php echo $row8[name]; ?></td>
           </tr>
           <tr>
                <td width="200">Spouse Occupation</td>
                <td width="20">:</td>
                <td width="200"><?php echo $row8[occupation]; ?></td>
           </tr>
            <tr>
                <td width="200">Number Of Children</td>
                <td width="20">:</td>
                <td width="200"><?php $num_child=return_field_value("count(*)","hrm_employee_family","emp_code ='$emp[emp_code]' and (relation=3 || relation=4) and status_active=1 and is_deleted=0"); echo $num_child; ?></td>
           </tr>
       <?php 
	   		$sql7="select * from hrm_employee_address where emp_code='$emp[emp_code]' and status_active=1 and is_deleted=0";
			$result7=mysql_query($sql7);
			$row7=mysql_fetch_assoc($result7);
		?>
        <tr>
            <td width="200">Phone No.</td>
            <td width="20">:</td>
            <td width="200"><?php echo $row7['phone_no']; ?></td>
       </tr>
        <tr>
            <td width="200">Cell No.</td>
            <td width="20">:</td>
            <td width="200"><?php echo $row7['mobile_no']; ?></td>
       </tr>
       <tr>
            <td width="200">Email</td>
            <td width="20">:</td>
            <td width="200"><?php echo $row7['email']; ?></td>
       </tr>
       
       </table>
     </div>
    <div style="float: right; clear: left; width:400px; margin-top:-50px"><?php if($emp[status_active]==1) echo "<b>Status: Active</b>"; else echo "<b>Status: Inactive</b>";  ?></div>
</div>
	<?php $i++;}
	}// end else 
	        $html = ob_get_contents();
		   ob_clean();
		   
			foreach (glob(""."*.pdf") as $filename) {			
                @unlink($filename);
			}
			
			echo "$html####$filename";
			exit();
                         
}// end emp Profile if


function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}	
	
function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}

?>
     
	
