<?php
/*######################################
	Developed By
	Name : Md.Ekram Hossain
	Date : 27-11-2012
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1){
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
//echo $company_cond."xcz";
//---------------------------------------------------------------------------------------------------------
include("../../../includes/common.php");
include("../../../includes/array_function.php");

//Company details
$sql="SELECT * FROM  hrm_employee where is_deleted=0 and status_active=1  $company_cond ORDER by emp_code ASC";
$result=mysql_query($sql) or die($sql."<br/>".mysql_error());
$company_details=array();
while($row=mysql_fetch_assoc($result)){
	$company_details[$row['id']]=mysql_real_escape_string($row['gross_salary']);
}
//print_r($company_details);
//Company details
$sql="SELECT * FROM  lib_company where is_deleted=0 and status_active=1  $company_cond ORDER by company_name ASC";
$result=mysql_query($sql) or die($sql."<br/>".mysql_error());
$company_details=array();
while($row=mysql_fetch_assoc($result)){
	$company_details[$row['id']]=mysql_real_escape_string($row['company_name']);
}
//print_r($company_details);
//Location_details
$sql="SELECT * FROM lib_location WHERE is_deleted = 0 ORDER by location_name";
$result=mysql_query($sql) or die($sql."<br/>".mysql_error());
$location_details=array();
while($row=mysql_fetch_assoc($result)){
	$location_details[$row['id']]=mysql_real_escape_string($row['location_name']);	
}
//print_r($location_details);
//Division details
$sql="SELECT * FROM lib_division WHERE is_deleted=0  ORDER by division_name ASC";
$result=mysql_query($sql) or die($sql ."<br/>" .mysql_error());
$division_details=array();
while($row=mysql_fetch_assoc($result)){
	$division_details[$row['id']]=mysql_real_escape_string($row['division_name']);
}
//Department details
$sql="SELECT * FROM lib_department WHERE is_deleted=0  ORDER by department_name ASC";
$result=mysql_query($sql) or die($sql ."<br/>" .mysql_error());
$department_details=array();
while($row=mysql_fetch_assoc($result)){
	$department_details[$row['id']]=mysql_real_escape_string($row['department_name']);
}
//Designation details
$sql="SELECT * FROM lib_designation WHERE is_deleted=0  ORDER by custom_designation ASC";
$result=mysql_query($sql) or die($sql ."<br/>" .mysql_error());
$designation_details=array();
while($row=mysql_fetch_assoc($result)){
	$designation_details[$row['id']]=mysql_real_escape_string($row['custom_designation']);
}
//Section  details
$sql="SELECT * FROM lib_section WHERE is_deleted=0  ORDER by section_name ASC";
$result=mysql_query($sql) or die($sql ."<br/>" .mysql_error());
$section_details=array();
while($row=mysql_fetch_assoc($result)){
	$section_details[$row['id']]=mysql_real_escape_string($row['section_name']);
}
//Subsection details
$sql="SELECT * FROM lib_subsection WHERE is_deleted=0  ORDER by subsection_name ASC";
$result=mysql_query($sql) or die($sql ."<br/>" .mysql_error());
$subsection_details=array();
while($row=mysql_fetch_assoc($result)){
	$subsection_details[$row['id']]=mysql_real_escape_string($row['subsection_name']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%"> 
            <div class="form_caption" align="center">Manpower Budget Report</div>
            <fieldset><legend>Search Panel</legend>
                <form name="manpower_budget" id="manpower_budget" class="">
                  <table width="1120px" class="rpt_table" align="center" cellspacing="0" cellpadding="0">
                    <thead>
                        <th width="140px">Category</th>
                        <th width="140px">Company</th>
                        <th width="140px">Location</th>
                        <th width="140px">Division</th>
                        <th width="140px">Department</th>
                        <th width="140px">Designation</th>
                        <th width="140px">Section</th>
                        <th width="140px">Subsection</th>
                     </thead>
                     <tbody>   
                        <tr class="general">
                            <td>
                                <select name="category_id" id="category_id" class="combo_boxes" style="width:140px;">
                                    <option value="">All Category</option>
                                    <? foreach($employee_category as $key=>$val){?>
                                    <option value="<? echo $key; ?>"><? echo $val; ?></option><? }?> 
                                </select>
                            </td>
                            <td>
                                <select name="company_id" id="company_id" class="combo_boxes" style="width:140px;">
                                    <? if($company_cond==""){?>
                                    <option value="0">---Select---</option><?php } 
                                    foreach($company_details AS $key=>$value){?>
                                    <option value=<?php echo $key ;?>><?php echo $value ;?></option><?php } ?>
                                </select>
                            </td>
                            <td>
                                <select name="location_id" id="location_id" class="combo_boxes" style="width:140px;">
                                    <option value="0">---Select---</option>
                                    <?php foreach($location_details as $key=>$value){?>
                                    <option value=<? echo "$key" ;?>><? echo "$value" ;?></option><?php }?> 		
                                </select>
                            </td>
                            <td>
                                <select name="division_id" id="division_id" class="combo_boxes" style="width:140px;">
                                    <option value="0">---Select---</option>
                                    <?php foreach($division_details as $key=>$value){?>
                                    <option value=<?php echo "$key" ;?>><?php  echo "$value" ;?></option><?php } ?>
                                </select>
                            </td>
                            <td>
                                <select name="department_id" id="department_id" class="combo_boxes" style="width:140px;">
                                    <option value="0">---Select---</option>
                                    <?php foreach ($department_details as $key=>$value){?>
                                    <option value=<?php echo $key; ?>><?php echo $value;?></option><?php } ?>
                                </select>
                            </td>
                            <td>
                                <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px;">
                                    <option value="0">---Select---</option>
                                    <?php foreach($designation_details as $key=>$value){?>
                                    <option value=<?php echo "$key" ;?>><?php echo "$value" ;?></option><?php } ?>
                                </select>
                            </td>
                            <td>
                                <select name="section_id" id="section_id" class="combo_boxes" style="width:140px;">
                                    <option value="0">---Select---</option>
                                    <?php  foreach($section_details as $key=>$value){?>
                                    <option value=<?php echo $key ;?>><?php echo $value;?></option><?php }?>
                                </select>
                            </td>
                            <td>
                                <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px;">
                                    <option value="0">---Select---</option>
                                    <?php  foreach($subsection_details as $key=>$value){?>
                                    <option value=<?php echo $key ;?>><?php echo $value;?></option><?php }?>
                                </select>
                            </td>
                        </tr>  
                        <!--
                        <tr>
                            <td colspan="12">
                                <div class="button_container" align="center">
                                    <input type="button" name="show" id="show" class="formbutton"  value="Show"  style="width:100px;"/>
                                    <input type="reset" name="reset" value="Reset"  class="formbutton" style="width:100px;"/>
                                </div>
                            </td>
                        </tr>
                        -->
                    </tbody>
                 </table>
                 <div style="padding-top:10px;" align="center"> 
                    <input type="button" name="show" id="show" class="formbutton"  value="Show"  style="width:100px;"/>
                    <input type="reset" name="reset" value="Reset"  class="formbutton" style="width:100px;"/>
                 </div>  
                </form>
			</fieldset>
            <fieldset>
                <div id="list_view_manpower" style="margin-top:5px;" align="center"></div>
                <div id="data_panel" style="margin-top:5px;" align="center"></div>
            </fieldset> 
		</div>
    </body>
</html>