<? 
// Sohel
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');
 
 
 $sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_name ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
    
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
	 <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
  <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />


	<script>
	
			
function generate_night_allowance_bill_report()
{
	var error = false;
	div="report_container";
	data_panel="data_panel";
	
	if( $('#txt_date').val()*1 == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_date').focus();
			$(this).html('Please select The Date.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( $('#company_id').val()*1 == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please select The Company.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	else if( $('#report_type').val()== 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#report_type').focus();
			$(this).html('Please select The report type.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( error == false )
	{
		$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
		$('#report_container').html('<img src="../../../resources/images/loading.gif" />');
		var data = '&txt_date=' + $('#txt_date').val() 
					+ '&txt_to_date=' + $('#txt_to_date').val()
					+ '&cbo_emp_category=' + $('#cbo_emp_category').val()
					+ '&company_id=' + $('#company_id').val() 
					+ '&location_id=' + $('#location_id').val() 
					+ '&division_id=' + $('#division_id').val() 
					+ '&department_id=' + $('#department_id').val()
					+ '&section_id=' + $('#section_id').val() 
					+ '&subsection_id=' + $('#subsection_id').val() 
					+ '&designation_id=' + $('#designation_id').val()
 					+ '&group_by_id=' + $('#group_by_id').val()
					+ '&order_by_id=' + $('#order_by_id').val()
					+ '&bill_amount=' + $('#bill_amount').val()
					+ '&report_type=' + $('#report_type').val()
					+ '&emp_code='+$('#emp_code').val();

		
		 //alert(data);bill_amount
		if (window.XMLHttpRequest)
			{
				xmlhttp=new XMLHttpRequest();
			}
		else
			{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{			
				var data_split=(xmlhttp.responseText).split('####');
				var link_data=data_split[1];			
				
				$('#data_panel').html( '<br><b>Convert To </b><a href="includes/tmp_report_file/' + data_split[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
				$('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );		
				
				document.getElementById(div).innerHTML=data_split[0];
				 hide_search_panel();
				$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
				{ 
					 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
					 $(this).fadeOut(5000);
				});
			}
		}
		xmlhttp.open("GET","includes/generate_night_allowance_bill_report.php?action=night_allowance_bill_sheet"+"&data="+data,true);
		xmlhttp.send();
	}
}

function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('report_container').innerHTML);
	d.close();

}


function openmypage_employee_info()
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#emp_code').val(thee_id.value);
			}
		}


$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
 });
 
 
 $(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		})
 
 
 //Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


</script>
    
   <style type="text/css"> 
   		td {overflow:hidden; }   
		@media print {
	   		thead {display: table-header-group;}
		}
	 	@media print {
	   		tfoot {display: table-footer-group;}
		}
		@media print{
				html>body tbody.table_body {
				max-height:305px;
				display: block;
				overflow:auto;
				}
		}
		.verticalText 
		{               
			writing-mode: tb-rl;
			filter: flipv fliph;
			-webkit-transform: rotate(270deg);
			-moz-transform: rotate(270deg);
		}
		.ui-multiselect.myClass
		{
			width:150px;
		}
	</style>
    
 <style type="text/css"> 
	td {overflow:hidden; }
	@media print {thead {display: table-header-group;}}
	@media print {tfoot {display: table-footer-group;}}
	
	
</style>

</head>
<body>
<div align="center" style="width:100%;">
	<!--<div align="center"><div align="center" id="messagebox"></div></div>-->
    <div align="center" style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Allowance Bill Sheet</h3>
    <div id="content_search_panel" > 
<form>
	<fieldset>
    	<table align="center" width="1120px" cellpadding="0" cellspacing="0" border="0" class="rpt_table">
            <thead>
                <th>Category</th>
                <th>Company</th>
                <th>Location</th>
                <th>Division</th>
                <th>Department</th>
                <th>Section</th>
                <th>SubSection</th>
                <th>Designation</th>   
            </thead>
            <tr class="general">
                <td>
                     <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                         <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                     </select>
                </td>
                <td>
                    <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                        <? if($company_cond=="")
                           { 
                            ?>
                        <option value="0">-- Select --</option>
                        <?php } foreach( $company_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                       <!-- <option value="0">-- Select --</option>-->
                        <?php foreach( $location_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                  </select>
                </td>
                <td>
                	<select name="division_id" id="division_id" style="width:140px" multiple="multiple" >
                             <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                </td>        
                <td>
                    <select name="department_id" id="department_id" style="width:140px" multiple="multiple">
                         <?php foreach( $department_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="section_id" id="section_id" style="width:140px" multiple="multiple" >
                         <?php foreach( $section_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="subsection_id" id="subsection_id" style="width:140px" multiple="multiple" >
                         <?php foreach( $subsection_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                     <select name="designation_id" id="designation_id" style="width:140px;" multiple="multiple" >
						 <?php foreach( $designation_chart AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>  
           </tr>
     </table>
     <div style="height:8px;"></div>
      <table align="center" width="760" cellpadding="0" cellspacing="0" border="0" class="rpt_table">
            <thead>
                <th width="140px;" colspan="2">Date</th>
                <th width="140px;">Report Type</th>
                <th width="140px;">Group By</th>
                <th width="140px;">Order By</th>
                <th width="140px;">Employee Code</th> 
                <th width="100px;">Bill Amount</th> 
                <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>         
            </thead>
            <tr class="general"> 
             <td>
             	<input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="10" style="width:70px;" />
             </td>
             <td>
                <input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" size="10" style="width:70px;" />
             </td>
                <td>
                    <select name="report_type" id="report_type" class="combo_boxes" style="width:140px" >							
                        <option value="0">-- Select --</option>
                        <option value="1">Night Allowance Bill</option>
                        <option value="2">Holiday Allowance Bill</option>
                     </select>
                </td>        
                 <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:140px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:140px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                <td><input type="text" name="emp_code" id="emp_code" value="" class="text_boxes" size="40" style="width:140px"  onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" />
                </td>
                 <td>
                 	<input type="text" name="bill_amount" id="bill_amount" value="" class="text_boxes" size="40" style="width:100px"  onkeypress="return numbersonly(this,event)" />
                </td>               
            		<td  id="search_salary" align="center">
                    <input type="button" name="search" id="search" value="Show" class="formbutton" style="width:100px" onclick="generate_night_allowance_bill_report()"/>            	  					
                  </td>               
            	</tr>
          </table>
    </fieldset>
</form>
</div>

<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
<div id="report_container" align="center" class="demo_jui2" style="margin-top:10px;"></div>

</div>
</body>
</html>
