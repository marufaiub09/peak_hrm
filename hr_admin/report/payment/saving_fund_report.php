<? 
//Completed by: Sohel
//Date :29-09-2013
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');
 
 
 $sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_name ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
    
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
  <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />

	<script>
		/*
		function populate_select_month( selected_id ) {
				$.ajax({
					type: "GET",
					url: "includes/generate_report_payment.php?",
					data: 'type=select_month_generate&id=' + selected_id,
					success: function( html ) {
						$('#cbo_month_selector').html( html )
					}
				});
			}
					
		function daysInMonth(month,year) 
		{
			return new Date(year, month, 0).getDate();
		}
		*/
	
		$(document).ready(function() {
					$(".datepicker").datepicker({
						dateFormat: 'dd-mm-yy',
						changeMonth: true,
						changeYear: true
					});
				});
			
		function generate_saving_fund_report()
		{
			var error = false;
			div="report_container";
			data_panel="data_panel";
			
			if( $('#txt_from_date').val()== '' ) {
				error = true;
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#txt_from_date').focus();
					$(this).html('Please select The Date.').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
			else if( $('#txt_to_date').val()== '' ) {
				error = true;
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#txt_to_date').focus();
					$(this).html('Please select The Date.').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
			else if( $('#company_id').val()*1 == 0 ) {
				error = true;
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#company_id').focus();
					$(this).html('Please select The Company.').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
			else if( error == false )
			{
				$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
				$('#report_container').html('<img src="../../../resources/images/loading.gif" />');
				var data = '&txt_from_date=' + $('#txt_from_date').val() 
							+ '&txt_to_date=' + $('#txt_to_date').val() 
							+ '&cbo_emp_category=' + $('#cbo_emp_category').val()
							+ '&company_id=' + $('#company_id').val() 
							+ '&location_id=' + $('#location_id').val() 
							+ '&division_id=' + $('#division_id').val() 
							+ '&department_id=' + $('#department_id').val()
							+ '&section_id=' + $('#section_id').val() 
							+ '&subsection_id=' + $('#subsection_id').val() 
							+ '&designation_id=' + $('#designation_id').val()
							+ '&cbo_salary_sheet='+$('#cbo_salary_sheet').val()
							+ '&group_by_id=' + $('#group_by_id').val()
							+ '&order_by_id=' + $('#order_by_id').val()
							+ '&emp_code='+$('#emp_code').val();
		
				
				 //alert(data);
				if (window.XMLHttpRequest)
					{
						xmlhttp=new XMLHttpRequest();
					}
				else
					{
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{			
						var data_split=(xmlhttp.responseText).split('####');
						var link_data=data_split[1];			
						
						$('#data_panel').html( '<br><b>Convert To </b><a href="includes/tmp_report_file/' + data_split[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
						$('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );		
						
						document.getElementById(div).innerHTML=data_split[0];
						 hide_search_panel();
						$("#messagebox").fadeTo(200,0.1,function() 
						{ 
							 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
						});
					}
				}
				xmlhttp.open("GET","includes/generate_saving_fund_report.php?action=saving_fund_report_sheet"+"&data="+data,true);
				xmlhttp.send();
			}
		}
		
		function new_window()
		{
			var w = window.open("Surprise", "#");
			var d = w.document.open();
			d.write(document.getElementById('report_container').innerHTML);
			d.close();
		}
		
		function openmypage_employee_info()
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#emp_code').val(thee_id.value);
			}
		}
		
		$(document).ready(function(e) 
		{
			  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
				//header: false, 
				selectedText: "# of # selected",
				
			});
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});	
			
		 });
		
		//Numeric Value allow field script
		function numbersonly(myfield, e, dec)
		{
			var key;
			var keychar;
		
			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);
		
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			
			// numbers
			else if ((("0123456789.,-").indexOf(keychar) > -1))
				return true;
			else
				return false;
		} 
</script>
    
	<style type="text/css"> 
        td {overflow:hidden; }   
        @media print {
            thead {display: table-header-group;}
        }
        @media print {
            tfoot {display: table-footer-group;}
        }
        @media print{
                html>body tbody.table_body {
                max-height:305px;
                display: block;
                overflow:auto;
                }
        }
        .verticalText 
        {               
            writing-mode: tb-rl;
            filter: flipv fliph;
            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
        }
        .ui-multiselect.myClass
        {
            width:150px;
        }
        td {overflow:hidden; }
        @media print {thead {display: table-header-group;}}
        @media print {tfoot {display: table-footer-group;}}
    </style>
</head>
<body>
<div align="left" style="width:100%;">
    <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Saving Fund Report</h3>
    <div id="content_search_panel" > 
<form>
	<fieldset>
    	<table width="1120px" cellpadding="0" cellspacing="0" border="0" class="rpt_table" align="center">
            <thead>
                <th><strong>Category</strong></th>
                <th><strong>Company</strong></th>
                <th><strong>Location</strong></th>
                <th><strong>Division</strong></th>
                <th><strong>Department</strong></th>
                <th><strong>Section</strong></th>
                <th><strong>SubSection</strong></th>
                <th><strong>Designation</strong></th>   
            </thead>
            <tr class="general">
                <td>
                     <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                        <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                     </select>
                </td>
                <td>
                    <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                        <? if($company_cond=="")
                           { 
                            ?>
                        <option value="0">-- Select --</option>
                        <?php } foreach( $company_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                      <option value="0">-- Select --</option>
                        <?php foreach( $location_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                  </select>
                </td>
                <td>
                	<select name="division_id" id="division_id" style="width:140px" multiple="multiple" >
                             <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                </td>        
                <td>
                    <select name="department_id" id="department_id" style="width:140px" multiple="multiple">
                         <?php foreach( $department_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="section_id" id="section_id" style="width:140px" multiple="multiple" >
                         <?php foreach( $section_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td id="subsection">
                    <select name="subsection_id" id="subsection_id" style="width:140px" multiple="multiple" >
                         <?php foreach( $subsection_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td id="designation">
                     <select name="designation_id" id="designation_id" style="width:140px;" multiple="multiple" >
						 <?php foreach( $designation_chart AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td> 
            </tr>
    </table>
    <div style="height:8px;"></div>
    <table align="center" width="800" cellpadding="0" cellspacing="0" border="0" class="rpt_table">
            <thead>
             	<th width="140px" colspan="2"><strong>Date</strong></th>
                <th><strong>Employee Type</strong></th> 
                <th><strong>Group By</strong></th>
                <th><strong>Order By</strong></th>
                <th><strong>Employee Code</strong></th>
                <th> <input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>  
            </thead>
            <tr class="general">
            	<td align="center">
						 <input type="text" name="txt_from_date" id="txt_from_date" value="" class="datepicker" style="width:70px" readonly="readonly" placeholder="From" />
                </td>
                <td>
                    	 <input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" style="width:70px" readonly="readonly" placeholder="To" />
 				</td> 
                <td>
                	<select name="cbo_salary_sheet" id="cbo_salary_sheet" class="combo_boxes" style="width:140px" >                    	
                        <option value=""> All Employee </option>
                        <option value="0">Regular Employee</option>
                        <option value="1">Separated All</option>
                    </select>
                </td>
                 <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:140px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:140px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                <td><input type="text" name="emp_code" id="emp_code" value="" class="text_boxes" size="40" style="width:140px"  onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" />
                </td> 
                <td  id="search_salary" align="center">
                	<input type="button" name="search" id="search" value="Show" class="formbutton" style="width:100px" onclick="generate_saving_fund_report()"/>            	  
                </td>               
            	</tr>
          </table><br />
        <!--  <div style="width:300px; background-color:#FFF; padding: 0px 0px 10px;" id="data_panel" align="center"></div>
          <div id="report_container"></div>
       -->
    	
    </fieldset>
</form>
</div>

<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>

<div id="report_container" align="left" class="demo_jui2" style="margin-top:10px;"></div>

</div>
</body>
</html>
