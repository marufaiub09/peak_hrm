<?php 
	/***************************************
	|	Developed By	: Md. Nuruzzaman
	|	Date			: 25.11.2014
	*****************************************/
	session_start();
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	include('../../../includes/common.php');
	include('../../../includes/array_function.php');
	include('../../../includes/common_functions.php');
	extract($_REQUEST);
	extract($_GET);
	
	// 1=limited access and 2=self user
	if ($_SESSION['logic_erp']["data_level_secured"]==1 || $_SESSION['logic_erp']["data_level_secured"]==2) 
	{
		// company_id
		if( $_SESSION['logic_erp']['company_id'] == 0 || $_SESSION['logic_erp']['company_id']=="")
		{
			$user_company_id="";
			$loc_company_id="";
		}
		else 
		{
			$user_company_id="id in(".$_SESSION['logic_erp']['company_id'].") and";
			$loc_company_id="company_id in(".$_SESSION['logic_erp']['company_id'].") and";
		}
		
		// location_id
		if( $_SESSION['logic_erp']['location_id'] == 0 || $_SESSION['logic_erp']['location_id']=="")
		{
			$user_location_id="";
			$div_location_id="";
		}
		else
		{
			$user_location_id="id in(".$_SESSION['logic_erp']['location_id'].") and";
			$div_location_id="location_id in(".$_SESSION['logic_erp']['location_id'].") and";
		}
		
		// division_id
		if( $_SESSION['logic_erp']['division_id'] == 0 || $_SESSION['logic_erp']['division_id']=="")
		{
			$user_division_id="";
			$dpt_division_id="";
		}
		else
		{
			$user_division_id="id in(".$_SESSION['logic_erp']['division_id'].") and";
			$dpt_division_id="division_id in(".$_SESSION['logic_erp']['division_id'].") and";
		}
		
		// department_id
		if( $_SESSION['logic_erp']['department_id'] == 0 || $_SESSION['logic_erp']['department_id']=="")
		{
			$user_department_id="";
			$sec_department_id="";
		}
		else
		{
			$user_department_id="id in(".$_SESSION['logic_erp']['department_id'].") and";
			$sec_department_id="department_id in(".$_SESSION['logic_erp']['department_id'].") and";
		}
		
		// section_id
		if( $_SESSION['logic_erp']['section_id'] == 0 || $_SESSION['logic_erp']['section_id']=="")
		{
			$user_section_id="";
			$subsec_section_id="";
		}
		else
		{
			$user_section_id="id in(".$_SESSION['logic_erp']['section_id'].") and";
			$subsec_section_id="section_id in(".$_SESSION['logic_erp']['section_id'].") and";
		}
		
		// subsection_id
		if( $_SESSION['logic_erp']['subsection_id'] == 0 || $_SESSION['logic_erp']['subsection_id']=="")
		{
			$user_subsection_id="";
		}
		else
		{
			$user_subsection_id="id in(".$_SESSION['logic_erp']['subsection_id'].") and";
		}
	}
	
	// all access
	else if ($_SESSION['logic_erp']["data_level_secured"]==0) 
	{
		$user_company_id="";
		$user_location_id="";
		$user_division_id="";
		$user_department_id="";
		$user_section_id="";
		$user_subsection_id="";
	}

	//company_details
	$sql = "SELECT id,company_name FROM lib_company WHERE $user_company_id status_active=1 and is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$company_details[$row['id']] = mysql_real_escape_string( $row['company_name'] );
	}
	
	//location_details
	$sql = "SELECT id, location_name FROM lib_location WHERE $user_location_id status_active=1 and is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$location_details[$row['id']] = mysql_real_escape_string( $row['location_name'] );
	}
	
	//division_details
	$sql = "SELECT id, division_name FROM lib_division WHERE $user_division_id status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$division_details[$row['id']] = mysql_real_escape_string( $row['division_name'] );
	}
	
	//department_details
	$sql = "SELECT id, department_name FROM lib_department WHERE $user_department_id status_active=1 and is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$department_details[$row['id']] = mysql_real_escape_string( $row['department_name'] );
	}
	
	//section_details
	$sql = "SELECT id, section_name FROM lib_section WHERE $user_section_id status_active=1 and is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$section_details[$row['id']] = mysql_real_escape_string( $row['section_name'] );
	}
	
	//subsection_details
	$sql = "SELECT id, subsection_name FROM lib_subsection WHERE $user_subsection_id status_active=1 and is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );
	}
	
	//selft user
	if ($_SESSION['logic_erp']["data_level_secured"]==2) 
	{
		$sql = "SELECT id_card_no,category,designation_id FROM hrm_employee WHERE emp_code='".$_SESSION['logic_erp']['priv_emp_code']."' and status_active=1 and is_deleted = 0";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$seft_info = mysql_fetch_array( $result );
	}
	//$seft_info['category']='';
	
//--------------------------------------------------------------------------------------------------------------------
	
	// lib_designation
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    
        <script type="text/javascript">
            $(document).ready(function(e) {
                $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
                    selectedText: "# of # selected",
                });
                
                $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({ });
                
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
                
                var usrType='<?php echo $_SESSION['logic_erp']["data_level_secured"];?>'
                //alert(usrType);
                user_data_level_secured(usrType);
            });
		
			// user_data_level_secured
			function user_data_level_secured(usrType)
			{
				//alert("su..re");
				if(usrType==2)
				{
					$('#cbo_emp_category').attr("disabled","disabled");
					$('#company_id').attr("disabled","disabled");
					$('#location_id').attr("disabled","disabled");
					$("#division_id").multiselect("disable");
					$('#department_id').multiselect("disable");
					$('#section_id').multiselect("disable");
					$('#subsection_id').multiselect("disable");
					$('#designation_id').multiselect("disable");
					$('#id_card').attr("disabled","disabled");
					$('#emp_code').attr("disabled","disabled");
				}
			}


	//onchange="populate_cost_center( 'location', this.value );"
	function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/pay_slip_report_peak.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_month_selector').html( html )
				}
			});
		}
				
function generate_salary_report()
{
	var error = false;
	div="report_container";
	data_panel="data_panel";
	if( $('#cbo_month_selector').val()*1 == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select The month.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	if( error == false )
	{
		$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
		var data = '&txt_date=' + $('#txt_date').val() 
				+ '&cbo_year_selector=' + $('#cbo_year_selector').val() 
				+ '&cbo_month_selector=' + $('#cbo_month_selector').val() 
				+ '&cbo_emp_category=' + $('#cbo_emp_category').val()
				+ '&company_id=' + $('#company_id').val() 
				+ '&location_id=' + $('#location_id').val() 
				+ '&division_id=' + $('#division_id').val() 
				+ '&department_id=' + $('#department_id').val()
				+ '&section_id=' + $('#section_id').val() 
				+ '&subsection_id=' + $('#subsection_id').val() 
				+ '&designation_id=' + $('#designation_id').val()
				+ '&cbo_salary_sheet='+$('#cbo_salary_sheet').val()
				+ '&payment_method='+$('#payment_method').val()
				+ '&group_by_id='+$('#group_by_id').val()
				+ '&order_by_id='+$('#order_by_id').val()
				+ '&id_card='+$('#id_card').val() 
				+ '&emp_code='+$('#emp_code').val();
				
				//alert (data);
	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{			
			var data_split=(xmlhttp.responseText).split('####');
			var link_data=data_split[1];			
		 	
			$('#data_panel').html('&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
			
			$('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="print_report()" value="Print" name="Print" class="formbutton" style="width:100px"/>' );
			
			document.getElementById(div).innerHTML=data_split[0];
			hide_search_panel();
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
		}
  	}
	//xmlhttp.open("GET","includes/pay_slip_report_reach_appearls.php?type=pay_slip"+"&data="+data,true);
	//xmlhttp.open("GET","includes/pay_slip_report_head_off.php?types=pay_slip_ho"+"&data="+data,true); 
	xmlhttp.open("GET","includes/pay_slip_report_peak.php?types=5"+"&data="+data,true); 
	xmlhttp.send();
	}
}


function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('report_container').innerHTML);
	d.close();

}


function print_report()
{
	$('#data_panel').hide();
	$('#messagebox').hide();
	$('#accordion_h1').hide();
	
  	window.print(document.getElementById('report_container').innerHTML);
	
	$('#data_panel').show();
	$('#messagebox').show();
	$('#accordion_h1').show();
}


	function openmypage_employee_info()
	{			
		//alert($('#company_id').val());
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_user_access_emp_by_code.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}	
	
	
function openmypage_employee_info_id_card()
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_user_access_emp_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#id_card').val(thee_id.value);
			}
		}	


//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


</script>
<style type="text/css"> 
	.page_break{page-break-before:always}
	
	p{page-break-before:always}
	
	td { overflow:hidden; }
  
	@media print {thead {display: table-header-group;}}
	@media print {tfoot {display: table-footer-group;}}
	
	.verticalText 
	{               
		writing-mode: tb-rl;
		filter: flipv fliph;
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
	}
 </style>
</head>


<body>
<div align="left" style="width:100%;">
    <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> - Pay Slip</h3>
    <div id="content_search_panel" > 
<form>
	<fieldset>
    	<div align="center" style="height:30px;"><strong>Pay Slip Date</strong> <input type="text" id="txt_date" class="datepicker" /> </div>
    	<table align="center" width="1120px" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th><strong>Category</strong></th>
                <th><strong>Company</strong></th>
                <th><strong>Location</strong></th>
                <th><strong>Division</strong></th>
                <th><strong>Department</strong></th>
                <th><strong>Section</strong></th>
                <th><strong>SubSection</strong></th>
                <th><strong>Designation</strong></th> 
             </thead>
            <tr class="general">
                <td>
                    <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                        <option value="">All Category</option>
                        <?php
                        foreach($employee_category as $key=>$val)
                        {
                        ?>
                        <option value="<? echo $key; ?>"<?php if($seft_info['category']!="" && $key==$seft_info['category']){?> selected=<?php echo "selected";}?>><?php echo $val; ?></option>
                        <?php
                        }
                        ?> 
                    </select>
                </td>
                <td>
                    <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                        <option value="0">-- Select --</option>
                        <?php
                        foreach( $company_details AS $key=>$value )
                        { 
                        ?>
                        <option value="<?php echo $key; ?>"<?php if(count($company_details)==1){?> selected=<?php echo "selected";}?>><?php echo $value; ?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </td>
                <td>
                    <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                        <option value="0">-- Select --</option>
                        <?php 
                        foreach( $location_details AS $key=>$value )
                        { 
                        ?>
                        <option value="<?php echo $key; ?>"<?php if(count($location_details)==1){?> selected=<?php echo "selected";}?>><?php echo $value; ?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </td>
                <td>
                    <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php 
                        foreach( $division_details AS $key=>$value )
                        { 
                        ?>
                        <option value="<?php echo $key; ?>"<?php if(count($division_details)==1){?> selected=<?php echo "selected";}?>><?php echo $value; ?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </td>
                <td>
                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php 
                        foreach( $department_details AS $key=>$value )
                        { 
                        ?>
                        <option value="<?php echo $key; ?>"<?php if(count($department_details)==1){?> selected=<?php echo "selected";}?>><?php echo $value; ?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </td>
                <td>
                    <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php 
                        foreach( $section_details AS $key=>$value )
                        { 
                        ?>
                        <option value="<?php echo $key; ?>"<?php if(count($section_details)==1){?> selected=<?php echo "selected";}?>><?php echo $value; ?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </td>
                <td>
                    <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php 
                        foreach( $subsection_details AS $key=>$value )
                        { 
                        ?>
                        <option value="<?php echo $key; ?>"<?php if(count($subsection_details)==1){?> selected=<?php echo "selected";}?>><?php echo $value; ?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </td>    
                <td id="designation">
                    <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px;" multiple="multiple" >
                        <!--<option value="0">-- Select --</option>-->
                        <?php foreach( $designation_chart AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
       </table>
       <div style="height:8px;"></div>
       <table align="center" width="800" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th><strong>Year</strong></th>
                <th><strong>Month</strong></th>
                <th><strong>Payment Mode</strong></th>
                <th><strong>Employee Type</strong></th>
                <th><strong>Group By</strong></th>
                <th><strong>Order By</strong></th>
                <th><strong>ID Card No</strong></th>
                <th><strong>System Code</strong></th>
                <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>   
             </thead>
            <tr class="general">         
                <td>
                    <select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" style="width:120px;" >
                    <option value="0">-- Select --</option>
                    <?php foreach( $policy_year AS $key=>$value ){ ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php } ?>
                    </select>
                </td>                    
                <td>
                    <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes" style="width:120px;" >							
                    <option value="0">-- Select --</option>
                    </select>
                </td> 
                <td>
                    <select name="payment_method" id="payment_method" class="combo_boxes" style="width:120px"> <!--onchange="populate_bank_drop_down(this.value)"-->
                        <option value="0">---Select---</option>
                        <option value="1"> Cash Payment </option>
                        <option value="2"> Bank Payment </option>
                        <!--<option value="3"> Both </option>
						<option value="4"> Non Bank Payment </option>-->
                    </select>
                </td>
                <td>
                    <select name="cbo_salary_sheet" id="cbo_salary_sheet" class="combo_boxes" style="width:120px;" >                    	
                    <option value=""> All Employee </option>
                    <option value="0">Regular Employee All</option>
                    <option value="10">Regular Employee Old</option>
                    <option value="2">New Employee</option>
                    <option value="1">Separated All</option>
                    <option value="4">Separated Resigned </option>
                    <option value="5">Separated Retirement</option>
                    <option value="6">Separated Disability</option>
                    <option value="7">Separated Death</option>
                    <option value="8">Separated Terminated</option>
                    <option value="9">Separated Un-noticed</option>
                    </select>
                </td> 
                <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>
                        
					<?php 
                    // self access
                    if ($_SESSION['logic_erp']["data_level_secured"]==2) 
                    {
                        
                        ?>
                        <td>
                            <input type="text" name="id_card" id="id_card" class="text_boxes" value="<?php echo $seft_info['id_card_no'];?>" readonly="readonly" style="width:100px;" />
                        </td>
                        <td align="center">
                            <input type="text" id="emp_code" name="emp_code" class="text_boxes" value="<?php echo $_SESSION['logic_erp']['emp_code'];?>" readonly="readonly" style="width:100px;" />
                        </td> 
                        <?php
                    }
                    else 
                    {
                        ?>
                        <td>
                            <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px;" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                        </td>
                        <td align="center">
                            <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:100px;" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                        </td>
                        <?php 
                    }
                    ?>
            		<td align="center">
                	<input type="button" name="search" id="search" value="Show" class="formbutton" style="width:100px" onclick="generate_salary_report()"/>
           	    	</td>               
            </tr> 
      </table>
    </fieldset>
</form>
</div>
		<fieldset>
		<div id="data_panel" align="center"></div>
        <div id="report_container" align="center" style="background-color:#FFFFFF"></div>
        </fieldset>

</div>
</body>
</html>

