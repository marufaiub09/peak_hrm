<?
/****************************************

Completed by: Md.Ekram Hossain

Date :29-09-2013
*****************************************/


session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

?>
<?php

include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');
 
 
 $sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}

?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
    <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    
 <script type="text/javascript" charset="utf-8">
	
	$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id").multiselect({
  	   	selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({ });
	
 });   
    
 
	
	function populate_select_month( sal_year ) {
			$.ajax({
				type: "GET",
				url: "includes/generate_salary_top_sheet_report_actual_peak.php?",
				data: 'type=select_month_generate&id=' + sal_year,
				success: function( html ) {
					$('#cbo_month_selector').html( html )
				}
			});
		}
				
   	
		
function generate_top_sheet_report()
{
	$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
	var error = false;
	div="report_container";
	data_panel="data_panel";
	if( $('#cbo_month_selector').val()*1 == "0" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please Select The month.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#company_id').val()*1 == "0" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please Select The Company.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#type_id').val()*1 == "0" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#type_id').focus();
			$(this).html('Please Select Type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false )
	{ 
	var data  = '&cbo_year_selector=' + $('#cbo_year_selector').val() + 
				'&cbo_month_selector=' + $('#cbo_month_selector').val() +
				'&cbo_emp_category=' + $('#cbo_emp_category').val() +
				'&company_id=' + $('#company_id').val() +
				'&location_id=' + $('#location_id').val() +
				'&division_id=' + $('#division_id').val() +
				'&department_id=' + $('#department_id').val() +
				'&section_id=' + $('#section_id').val() +
				'&subsection_id=' + $('#subsection_id').val()+
				'&group_by_id=' + $('#group_by_id').val() +
				'&cbo_salary_sheet='+ $('#cbo_salary_sheet').val();
	//alert(data);return;
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
 	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{			
			var data_split=(xmlhttp.responseText).split('####');
			var link_data=data_split[1];			
		 	$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + data_split[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
			$('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );		
			
			document.getElementById(div).innerHTML=data_split[0];
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				 $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
			});
			 
		}
  	}  	
	//xmlhttp.open("GET","includes/generate_salary_top_sheet_buyer_aman.php?types=wages_salary_sheet_section"+data,true);//generate_report_payment
	xmlhttp.open("GET","includes/generate_salary_top_sheet_report_actual_peak.php?action=top_sheet"+data,true);//generate_report_payment
	xmlhttp.send();
	}
}


function fn_color(table_id){
	$(document).ready(function(){
			$('#'+table_id+' tbody tr').hover(function(){ 
				$(this).children().css("background-color","#ffdc87");//.addClass('datahighlight');
			},function(){ 
				$(this).children().css("background-color","");//removeClass('datahighlight');
			});
	});
}

function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('report_container').innerHTML);
	d.close();

}

</script>
      
<style type="text/css"> 
	td {overflow:hidden; }
	@media print {thead {display: table-header-group;}}
	@media print {tfoot {display: table-footer-group;}}
	.verticalText 
	{               
		writing-mode: tb-rl;
		filter: flipv fliph;
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
	}
	
	.datahighlight {
        background-color: #ffdc87 !important;
	}
</style>
 

</head>



<body>
<div align="left" style="width:100%;">

	<div align="center">
		<div align="center" id="messagebox"></div>
	</div>
    
   <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Salary Top Sheet</h3>
  <div id="content_search_panel" > 
<form>
	<fieldset>
    	<table align="center" width="1050" cellpadding="0" cellspacing="0" border="0" class="rpt_table">        	
            <thead>
                <th><strong>Category</strong></th>              
                <th><strong>Company</strong></th> 
                <th><strong>Location</strong></th>  
                <th><strong>Division</strong></th> 
                <th><strong>Department</strong></th>
                <th><strong>Section</strong></th>
                <th><strong>Sub Section</strong></th>
            </thead>
            <tr class="general">
                <td>
                     <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes">
                         <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                     </select>
                </td>                                 
                <td>
                    <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                        <? if($company_cond=="")
                           { 
                            ?>
                        <option value="0">-- Select --</option>
                        <?php } foreach( $company_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>" <? if(count($company_details)==1){ echo "selected";} ?>><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td> 
                <td>
                    <select name="location_id" id="location_id" class="combo_boxes" style="width:150px" >
                        <option value="0">-- Select --</option>
                        <?php foreach( $location_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td> 
                <td>
                    <select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                        <?php foreach( $division_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td> 
                <td>
                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php foreach( $department_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <?php foreach( $section_details AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
               </tr>
               <tr><td colspan="8">&nbsp;</td></tr>
               </table>
               
               <table align="center" width="600" cellpadding="0" cellspacing="0" border="0" class="rpt_table">
                <thead>
                <th><strong>Year</strong></th>
                <th><strong>Month</strong></th>  
                <th><strong>Type</strong></th>
                <th><strong>Top Sheet</strong></th> 
                <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" />   </th> 
                    
                </thead>
                <tr class="general"> 
                 <td>
                	<select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" style="width:120px" > 					  
                 		 <option value="0">-- Select --</option>
                        <?php foreach( $policy_year AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                	</select>
                </td>                    
                <td>
                    <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes" style="width:120px" >							
                        <option value="0">-- Select --</option>
                    </select>
                </td>
                <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:120px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                 <td>
                	<select name="cbo_salary_sheet" id="cbo_salary_sheet" class="combo_boxes" style="width:100px" >                    	
                        <option value=""> All Employee </option>
                        <option value="0"> Regular Employee</option>
                        <option value="1"> Separated Employee</option>
                        <option value="2"> New Employee</option>
                    </select>
                </td>
            	<td>
                <input type="button" name="search" id="search" value="Show" class="formbutton" style="width:120px" onclick="generate_top_sheet_report()"/>
                &nbsp;
                </td>	
            </tr>
           
        </table>
    	 
    </fieldset>
</form>
</div>
		
        <fieldset>
            <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="report_container"></div>
        </fieldset>
</div>
</body>
</html>
