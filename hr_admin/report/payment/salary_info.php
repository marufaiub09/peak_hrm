<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include("../../../includes/common.php");
include('../../../includes/array_function.php');
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_name ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY level ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<script src="../../../resources/jquery-1.6.1.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
     <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
  <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    
	<script type="text/javascript">
	
		
$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
 });
	
	function populate() {
	
		$('#messagebox').removeClass().addClass('messagebox').text('Report Generating....').fadeIn(1000);
		var div="data_panel2";
		document.getElementById(div).innerHTML = "";
		var data=document.getElementById('category').value+"_"+document.getElementById('company_id').value+"_"+document.getElementById('location_id').value+"_"+document.getElementById('division_id').value+"_"+document.getElementById('department_id').value+"_"+document.getElementById('section_id').value+"_"+document.getElementById('designation_id').value+"_"+document.getElementById('subsection_id').value;
		//alert(data);
		if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
		else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
		
		xmlhttp.onreadystatechange = function() {
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				//document.getElementById(div).innerHTML = xmlhttp.responseText;
				var data_split=xmlhttp.responseText.split('####');
				var link_data=data_split[1];
				document.getElementById('data_panel').innerHTML='<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
				document.getElementById(div).innerHTML=data_split[0];
				hide_search_panel();
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).html('Report has been generated succesfully.....').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
		}
		xmlhttp.open( "GET", "includes/generate_report_payment.php?data=" + data +"&type=salary_info"+"&search_string="+document.getElementById('search_string').value, true );
		xmlhttp.send();
	}

	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.height="auto";
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel2').innerHTML+'</body</html>');
		d.close();
		document.getElementById('scroll_body').style.overflow="scroll";
		document.getElementById('scroll_body').style.height="280px";
	}
	
	//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}	

	</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>
<body style="font-family:verdana; font-size:11px;">
<div align="left" style="width:100%">
    <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Present Salary Information</h3>
    <div id="content_search_panel" > 
    <form>
		<fieldset id="filter_panel" >
			<table align="center" width="1120px" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <th><strong>Category</strong></th>
					<th><strong>Company</strong></th>
					<th><strong>Location</strong></th>
                    <th><strong>Division</strong></th>
					<th><strong>Department</strong></th>
                    <th><strong>Section</strong></th>
                    <th><strong>SubSection</strong></th>
                    <th><strong>Designation</strong></th> 
				</thead>
                <tr class="general">
                	 <td>
					<select name="category" id="category" class="combo_boxes" style="width:140px">
                     <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
				</select>
					</td>
                
                    <td id="company">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
					<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
					<?php } } ?>
							
						</select>
					</td>
                    
                     <td id="location">
						<select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
							<option value="0">-- Select --</option>
							<?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
					<option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                    <td id="division">
						<select name="division_id" id="division_id" class="combo_boxes" style="width:140px" multiple="multiple">
							<!--<option value="0">-- Select --</option>-->
							<?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
					<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
					<?php } } ?>
						</select>
                    </td>
                   <td id="department">
						<select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        	<!--<option value="0">-- Select --</option>-->
							<?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
					<option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                     <td id="section">
						<select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple">
                       <!--<option value="0">-- Select --</option>-->
							<?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
					<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
					 <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple">
                        <!--<option value="0">-- Select --</option>-->
							<?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
					<option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                     <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple" >
							<!--<option value="0">-- Select --</option>-->
							<?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
					<option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
					<?php } } ?>
						</select>
					</td>
                </tr>
           </table>
           <div style="height:8px;"></div>
           <table align="center" width="240" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <th><strong>Employee Code</strong></th>
					<th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>	
				</thead>
                <tr class="general">         
                    <td>
                    	<input type="text" id="search_string" name="search_string" class="text_boxes" style="width:140px" onkeypress="return numbersonly(this,event)" />
                    </td>
                    <td>
                    	<input type="button" name="rpo_search" id="rpo_search" value="Show" class="formbutton" onClick="populate();"  style="width:100px"/>
                    </td>
                </tr>
			</table>
		</fieldset>
     </form>
     </div>
</div>
<fieldset>
	<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
    <div id="data_panel2" align="left" class="demo_jui" style="margin-top:10px;"></div>
</fieldset>
</body>
</html>