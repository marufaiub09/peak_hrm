<?php
session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');
extract ( $_REQUEST );

//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}
	
 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
	$sql = "SELECT * FROM variable_settings_report WHERE is_deleted = 0 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$variable_settings_report = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$variable_settings_report[$row['company_id']][$row['module_id']] = mysql_real_escape_string($row['report_list']);
	}
 
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$allow_rate=array();
	$designation_chart = array();
	while( $row = mysql_fetch_array( $result ) ) {		
			$designation_chart[$row['id']] =  $row['custom_designation'];
			$designation_chart_rate[$row['id']]['rate'] = ( $row['allowance_rate'] );
			$allow_rate[$row['id']]	= $row['allowance_rate'] ;
	}
	 
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY weekend ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$weekend_chart[$row['emp_code']] = mysql_real_escape_string( $row['weekend'] );		
	}

	$sql = "select * from lib_payroll_head where  salary_head=1 and status_active=1 and is_deleted=0 and id!=20 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_head_mst = array();
	$salary_head_mst_short = array();
	$salary_head_mst_id = array();
	$i=0;
	while( $row = mysql_fetch_assoc( $result ) ) {	
		$i++;	
			$salary_head_mst[$i] = mysql_real_escape_string( $row['custom_head'] );	
			$salary_head_mst_id[$i] = mysql_real_escape_string( $row['id'] );	
			$salary_head_mst_short[$i]=	mysql_real_escape_string( $row['abbreviation'] );	
	}
	
	//salary head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and salary_head=1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_head = array();
	$salary_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$salary_head[$row['id']] = mysql_real_escape_string( $row['custom_head'] );	
			$salary_head_id[] = mysql_real_escape_string( $row['id'] );		
	}
	
	//earning head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4) and id not in (6,7)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head = array();
	$earning_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id[] = mysql_real_escape_string( $row['id'] );	
	}

//print_r($earning_head);die;	
	
	//earning head Original Salary Sheet 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head_original = array();
	$earning_head_id_original = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head_original[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id_original[] = mysql_real_escape_string( $row['id'] );	
	}
	//deduction head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (1,2) and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$deduction_head = array();
	$deduction_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$deduction_head[] = mysql_real_escape_string( $row['custom_head'] );
			 $deduction_head_id[] = mysql_real_escape_string( $row['id'] );		
	}
//print_r($deduction_head_id);die;
$bangArray = array(	
					'Friday'=>'শুক্রবার', 
					'Saturday'=>'শনিবার', 
					'Sunday'=>'রবিবার', 
					'Monday'=>'সোমবার', 
					'Tuesday'=>'মঙ্গলবার', 
					'Wednesday'=>'বুধবার', 
					'Thursday'=>'বৃহস্পতিবার' 
				   );
	
$bangMonthArray = array('January'=>'জানুয়ারি', 'February'=>'ফেব্রুয়ারি', 'March'=>'মার্চ', 'April'=>'এপ্রিল','May'=>'মে','June'=>'জুন', 'July'=>'জুলাই','August'=>'আগস্ট', 'September'=>'সেপ্টেম্বর', 'October'=>'অক্টোবর', 'November'=>'নভেম্বর', 'December'=>'ডিসেম্বর');



 	
	if($type=="wages_salary_sheet"){ //check which salary format selected
	
		$sql="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and salary_sheet_format!='0' order by id DESC "; //pay_slip_format
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$row = mysql_fetch_array( $result );
		$types=$row[salary_sheet_format];
	}

//wages_salary_sheet1
if($types=="wages_salary_sheet1") 	// jm salary sheet automated
{	
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];	
	//find GH FH CH, H
	$table_width=(count($salary_head)+count($earning_head)+count($deduction_head))*50;
?>

<style>
	.verticalText 
	{               
		writing-mode: tb-rl;
		 filter: flipv fliph;
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
		-o-transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		transform: rotate(270deg);
		width: 3em;		 
	}
</style>

<?php
	$rpt_company=$company_id;
	$company_info_sql="select * from lib_company where id='$company_id'";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10) { $emp_status="and a.emp_status in ( 0 )";} //$txt_from_date = $previous_month;
	//else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	$search_header_caption="";
	//print_r($location_details); 
	if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
	if ($location_id==0 || $location_id=='') $location_id=""; else { $search_header_caption .=" Location: ".$location_details[trim($location_id)].";";  $location_id="and a.location_id='$location_id'"; }
	//echo $search_header_caption;
	if ($division_id==0 || $division_id=='') $division_id=""; 
	else {
		$tmp="";
		$division_s=explode(",",$division_details);
		foreach($division_s as $key)
		{
			if($tmp=="")$tmp=$division_details[$key]; else $tmp.=",".$division_details[$key];
		} 
		$search_header_caption .=" Division: ".$tmp.";";
		$division_id="and a.division_id in ($division_id)"; 
	
	}
	
	if ($department_id==0 || $department_id=='') $department_id=""; 
	else	{
		$tmp="";
			$division_s=explode(",",$department_id);
			foreach($division_s as $key)
			{
				if($tmp=="")$tmp=$department_details[$key]; else $tmp.=",".$department_details[$key];
			} 
			$search_header_caption .=" Department: ".$tmp.";";
			 
			$department_id=" and  a.department_id in ($department_id)";    
		}
	
	if ($section_id==0 || $section_id=='') $section_id=""; 
	else { 
			$tmp="";
			$division_s=explode(",",$section_id);
			foreach($division_s as $key)
			{
				if($tmp=="")$tmp=$section_details[$key]; else $tmp.=",".$section_details[$key];
			} 
			$search_header_caption .=" Section: ".$tmp.";";
		
		 
		$section_id="and a.section_id in ($section_id)"; 
	}
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; 
	else {  
			$tmp="";
			$division_s=explode(",",$subsection_id);
			foreach($division_s as $key)
			{
				if($tmp=="")$tmp=$subsection_details[$key]; else $tmp.=",".$subsection_details[$key];
			} 
			$search_header_caption .=" Sub Section: ".$tmp.";";
		
		
		$subsection_id="and a.subsection_id in ($subsection_id)";
	}	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and b.designation_id in ($designation_id)";
	
	if ($payment_method==0) $payment_met =""; 
	else if ($payment_method==1) $payment_met =" and b.bank_gross<1 ";// "and a.emp_code not in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')
	else if ($payment_method==2) $payment_met =" and b.bank_gross>0 ";//"and a.emp_code in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	//echo $payment_method."=ss".$payment_met; die;
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}	
	//if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(b.id_card_no,5)";}	 

	$sql = "SELECT b.emp_code,sum(a.pay_amount) as pay_amount FROM hrm_salary_dtls a, hrm_salary_mst b WHERE b.id=a.salary_mst_id and b.salary_periods='$txt_from_date' and salary_head_id in (5,6,7) group by a.emp_code";
 	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
 	$processed_ot_amount = array();
 	while( $row = mysql_fetch_assoc( $result ) ) {		
			$processed_ot_amount[$row['emp_code']] += $row['pay_amount'];
 	}
	if ( $groupby!="" ) { $search_header_caption =""; }
 	ob_start();
 	?>
	<table width="1700" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
	<?php
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$section_total_gross=0;$section_total_salary=0;$section_total_net_payable=0;$section_total_not_payable=0;$section_total_deduction=0;$section_total_bank=0;$section_total_cash=0;	
	$section_total_earning_head_amt = array();$section_total_deduction_head_amt = array();

	$department_total_gross=0;$department_total_salary=0;$department_total_net_payable=0;$department_total_not_payable=0;$department_total_deduction=0;$department_total_bank=0;$department_total_cash=0;	
	$department_total_earning_head_amt = array();$department_total_deduction_head_amt = array();

	$division_total_gross=0;$division_total_salary=0;$division_total_net_payable=0;$division_total_not_payable=0;$division_total_deduction=0;$division_total_bank=0;$division_total_cash=0;	
	$division_total_earning_head_amt = array();$division_total_deduction_head_amt = array();

	$location_total_gross=0;$location_total_salary=0;$location_total_net_payable=0;$location_total_not_payable=0;$location_total_deduction=0;$location_total_bank=0;$location_total_cash=0;	
	$location_total_earning_head_amt = array();$location_total_deduction_head_amt = array();
	
	$grand_total_gross = 0;	$grand_total_salary = 0;$grand_total_net_payable = 0;$grand_total_not_payable = 0;$grand_total_deduction = 0;$grand_total_bank=0;$grand_total_cash=0;
	$grand_total_earning_head_amt = array();$grand_total_deduction_head_amt = array();
	
 	$sl=0;
	$ct=0;
	
 	$sql_emp  ="select a.*,b.first_name,b.middle_name,b.last_name,b.id_card_no,b.punch_card_no,b.dob,b.designation_id,b.designation_level,b.joining_date,b.category,b.salary_grade,b.ot_entitled,b.staff_ot_entitled
			   from 
			   hrm_salary_mst a, hrm_employee b
			   where
			   a.emp_code=b.emp_code and
			   a.salary_periods='$txt_from_date'  
			   $emp_code $category $company_id $location_id $division_id $section_id $subsection_id 
			   $designation_id $department_id $payment_met $emp_status 
			   $orderby";
			   
//echo $sql_emp;die;
	
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{ 
		//for staff OT here..... do not change
		//net_payable_amount  total_earning_amount
		// echo $row_emp[total_over_time]; die;
		if( $row_emp[staff_ot_entitled]==1 )
		{			
			$staff_ot_tmp=0;
			$staff_ot_tmp = $processed_ot_amount[$row_emp['emp_code']]; //$row_emp[total_over_time]*$row_emp[over_time_rate]; 
			$processed_ot_amount[$row_emp['emp_code']]=0;
			$row_emp[total_earning_amount]=$row_emp[total_earning_amount] - $staff_ot_tmp;
			$row_emp[net_payable_amount]=$row_emp[net_payable_amount] - $staff_ot_tmp;
			$row_emp[total_over_time]=0;
			$row_emp[over_time_rate]=0;			
		}
		$sl++;
 		
		   //start header print---------------------------------//
		   if($sl==1)
		   {
				$location_arr[$row_emp[location_id]]=$row_emp[location_id];
				$division_arr[$row_emp[division_id]]=$row_emp[division_id];
				$department_arr[$row_emp[department_id]]=$row_emp[department_id];
				$section_arr[$row_emp[section_id]]=$row_emp[section_id];
				$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];

			   ?> 
				<thead>    
					<tr style="text-align:center; font-weight:bold">    	 
						<th colspan="<? echo count($earning_head)+count($deduction_head)+21; ?> ">
							<div style="font-size:13px"><?	echo $company_info_result["company_name"];?></div>
							<div style="font-size:8px">Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
								Salary Sheet For The Month Of <? echo $cur_month; ?>-<? echo $cur_year; ?> 
						  </div>
						</th>
					</tr>				   
					<tr style="text-align:center; font-weight:bold" height="20">
                        <th width="40" rowspan="3">SL</th><th width="60" >ID</th>
                        <th colspan="2">Emp Name</th>
                        
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">Joining</div></th>
                        
                        <th width="50" rowspan="3">
                        <? 
						
						$k=0;
						foreach($salary_head_mst_short as $key) { 
							$k++;
									if(count($salary_head_mst_short)==$k)  echo $key; else  echo $key."<br /><hr />"; 
								}
						?>
                        </th>
                        <th width="50" rowspan="3">Gross Wages</th>
                        <th width="50" rowspan="2">DOM</th>
                        <th width="50">CL</th>
                        <th width="50">GH</th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">Present Days</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">Pay Days</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">Absent Days</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">N.Pay Days</div></th>
                        <th colspan="<? echo count($earning_head); ?>">Others Earning</th>		
                        <th width="50" rowspan="3">TOTAL SALARY</th>
                        <th colspan="<? echo count($deduction_head); ?>">DEDUCTION</th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">Total<br />Deduct</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">NP Amount</div></th>
                        <th width="70" rowspan="3">Net payable </th>
                        <th width="70" rowspan="3">In Bank</th>
                        <th width="70" rowspan="3">In Cash</th>
                        <th width="150" rowspan="3"> Signature</th>
                    </tr>
                    <tr style="text-align:center; font-weight:bold">
                        <th width="60" rowspan="2">Emp Code</th>
                        <th width="170" rowspan="2">Designation</th>
                        <th width="50" rowspan="2" valign="center"><div class="verticalText">Grade</div></th>    	
                        <th width="50">SL</th>
                        <th width="50">WH</th>
                        <?
                         //echo count($earning_head);
                            for ($i=0; $i<count($earning_head); $i++)
                            {
                                if($earning_head_id[$i]==5)
                                {
                                    
                        ?>	
                                <th width="50" rowspan="2"><? echo $earning_head[$i]."<br><hr>"."HRS <br> Rate <br> Amount";  ?></th>
                        <?
                                }
								else if ($earning_head_id[$i]==41) // overtime Allowance
                                {
						?>	
                                <th width="50" rowspan="2"><? echo $earning_head[$i]."<br><hr>"."HRS <br> Rate <br> Amount";  ?></th>
                        <?
								}
                                else
                                {
                                    ?>
                                    <th width="50" rowspan="2" valign="center"><div class="verticalText"><? echo $earning_head[$i];  ?></div></th>
                                    <?
                                }
                            }?>   
                        
                        <?
                            for ($i=0; $i<count($deduction_head); $i++)
                            {
                        ?>	
                        <th width="50" rowspan="2" valign="center"><div class="verticalText" ><? echo $deduction_head[$i]; ?></div></th>
                        <?
                            }?>
                         
                    </tr>
                    <tr style="text-align:center; font-weight:bold">    	
                        
                        <th width="50">WD</th>
                        <th width="50">EL</th>
                        <th width="50">FH</th>    	
				  	</tr>                  
				</thead>
               <? if( $search_header_caption!="") echo '<tr style="font-weight:bold">
					<td colspan="20" align="left">'.$search_header_caption.'</td>
				</tr>';
				 
		   }//end if condition of header print
		   //end header print
		
 		//---------------------------------------------------------------------------------------------------	
		//---------------------------------------------------------------------------------------------------
		
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row_emp[section_id]]=$row_emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row_emp[department_id]]=$row_emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row_emp[section_id]]=$row_emp[section_id];
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row_emp[division_id]]=$row_emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row_emp[department_id]]=$row_emp[department_id];
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row_emp[location_id]]=$row_emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row_emp[division_id]]=$row_emp[division_id];
						$department_arr[$row_emp[department_id]]=$row_emp[department_id];
						$section_arr[$row_emp[section_id]]=$row_emp[section_id];
						$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		}
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Total</td>
					<td align="center"><? echo number_format($subsection_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($subsection_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($subsection_total_salary); ?></td> 
					<? foreach($subsection_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($subsection_total_deduction); ?></td>
					<td align="center" ><? echo number_format($subsection_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($subsection_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($subsection_total_bank); ?></td>
					<td align="center" ><? echo number_format($subsection_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$subsection_total_gross=0;$subsection_total_salary=0;$subsection_total_deduction=0;$subsection_total_not_payable=0;$subsection_total_net_payable=0;$subsection_total_bank=0;$subsection_total_cash=0;	
			$subsection_total_earning_head_amt = array();$subsection_total_deduction_head_amt = array();

		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Section Total</td>
					<td align="center"><? echo number_format($section_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($section_total_earning_head_amt as $key=>$val ){ ?><td align="center"><? echo number_format($val); ?></td><? } ?>
					<td align="center"><? echo number_format($section_total_salary); ?></td> 
					<? foreach($section_total_deduction_head_amt as $key=>$val ){ ?><td align="center"><? echo number_format($val); ?></td><? } ?>  
					<td align="center" ><? echo number_format($section_total_deduction); ?></td>
					<td align="center" ><? echo number_format($section_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($section_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($section_total_bank); ?></td>
					<td align="center" ><? echo number_format($section_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$section_total_gross=0;$section_total_salary=0;$section_total_deduction=0;$section_total_not_payable=0;$section_total_net_payable=0;$section_total_bank=0;$section_total_cash=0;	
			$section_total_earning_head_amt = array();$section_total_deduction_head_amt = array();
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Department Total</td>
					<td align="center"><? echo number_format($department_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($department_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($department_total_salary); ?></td> 
					<? foreach($department_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($department_total_deduction); ?></td>
					<td align="center" ><? echo number_format($department_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($department_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($department_total_bank); ?></td>
					<td align="center" ><? echo number_format($department_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$department_total_gross=0;$department_total_salary=0;$department_total_deduction=0;$department_total_not_payable=0;$department_total_net_payable=0;$department_total_bank=0;$department_total_cash=0;	
			$department_total_earning_head_amt = array();$department_total_deduction_head_amt = array();
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Division Total</td>
					<td align="center"><? echo number_format($division_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($division_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($division_total_salary); ?></td> 
					<? foreach($division_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($division_total_deduction); ?></td>
					<td align="center" ><? echo number_format($division_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($division_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($division_total_bank); ?></td>
					<td align="center" ><? echo number_format($division_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$division_total_gross=0;$division_total_salary=0;$division_total_deduction=0;$division_total_not_payable=0;$division_total_net_payable=0;$division_total_bank=0;$division_total_cash=0;	
			$division_total_earning_head_amt = array();$division_total_deduction_head_amt = array();
 			
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Location Total</td>
					<td align="center"><? echo number_format($location_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($location_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($location_total_salary); ?></td> 
					<? foreach($location_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($location_total_deduction); ?></td>
					<td align="center" ><? echo number_format($location_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($location_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($location_total_bank); ?></td>
					<td align="center" ><? echo number_format($location_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$location_total_gross=0;$location_total_salary=0;$location_total_deduction=0;$location_total_not_payable=0;$location_total_net_payable=0;$location_total_bank=0;$location_total_cash=0;	
			$location_total_earning_head_amt = array();$location_total_deduction_head_amt = array();
 			
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
			}
		

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr><td colspan="26"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}
		
		$ct++;
 			
		if ($ct%2==0) $bgcolor="#EFEFEF"; 
		else $bgcolor="#FFFFFF";
		
    ?>
    <tbody>	    	
    	<tr height="102" bgcolor="<? echo $bgcolor; ?>" >
        	<td align="center" valign="middle"><? echo $sl; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[id_card_no]; ?> <br><hr style="width:80%; border:1px dotted"> <? echo $row_emp[emp_code]; ?></td>
          
            <td align="center" valign="middle"><? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name].'<br><hr style="width:80%; border:1px dotted">'.$designation_chart[$row_emp[designation_id]]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[salary_grade]; ?></td>
            <td align="center" valign="middle"><div class="verticalText"><? echo $row_emp[joining_date]; ?></div></td>
            
            <td align="center" valign="middle"> 
			 <? 
			 $sql_attnd =" SELECT  ";
			// print_r($salary_head_mst_id);
			for ($i=1; $i<=count($salary_head_mst_id); $i++)
			{
				if ($i!=count($salary_head_mst_id))
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$salary_head_mst_id[$i]."' THEN round(pay_amount) END) AS 'salary_".$salary_head_mst_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$salary_head_mst_id[$i]."' THEN round(pay_amount) END) AS 'salary_".$salary_head_mst_id[$i]."'";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] order by salary_head_id";
			$sql_exe=mysql_query($sql_attnd);
			if($row_data = mysql_fetch_array($sql_exe))
		 //echo $sql_attnd;die; 
			{
				$k=0;
				foreach($salary_head_mst_id as $key) { 
					$k++;
							if(count($salary_head_mst_id)==$k)  echo $row_data['salary_'.$key]; else  echo $row_data['salary_'.$key]."<br />"; 
						}
			}
			 /*foreach($salary_head_mst as $key) { 
							echo $key."<br /><hr />";
						}
						 
			 echo number_format($row_emp[basic_salary])."<br>".number_format($row_emp[house_rent])."<br>".number_format($row_emp[medical_allowance])."<br>".number_format($row_emp[conveyence]); */ 	 ?></td>
            <td align="center" valign="middle"><? echo number_format($row_emp[gross_salary]); ?></td>
            <td align="center" valign="middle"><? echo $row_emp[total_calendar_days]."<br>".$row_emp[total_working_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[casual_leave]."<br>".$row_emp[sick_leave]."<br>".$row_emp[earn_leave]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[government_holiday]+$row_emp[compensatory_total_holiday]."<br>".$row_emp[total_weekly_holiday]."<br>".$row_emp[festival_holiday]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[total_present_days]+$row_emp[total_late_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[payable_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[total_abs_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[not_payble_days]; ?></td>
            <?
			
			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($earning_head_id); $i++)
			{
				if ($i!=count($earning_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN round(pay_amount) END) AS '".$earning_head_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN round(pay_amount) END) AS '".$earning_head_id[$i]."'";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=0 order by salary_head_id";
			//echo $sql_attnd;die;  
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				for ($i=0; $i<count($earning_head_id); $i++)
				{
					if( $earning_head_id[$i]==5  )
					{
						$ot_amnt =$processed_ot_amount[$row_emp['emp_code']];// $row_emp[total_over_time]*$row_emp[over_time_rate];
						//round($row_earn[$earning_head_id[$i]]); // $row_emp[total_over_time]*$row_emp[over_time_rate]; 	
						//$tot_ott+=round($row_earn[$earning_head_id[$i]]);
						//section department division total
						$subsection_total_earning_head_amt[$i] += round($ot_amnt);
						$section_total_earning_head_amt[$i] += round($ot_amnt);
						$department_total_earning_head_amt[$i] += round($ot_amnt);
						$division_total_earning_head_amt[$i] += round($ot_amnt);
						$location_total_earning_head_amt[$i] += round($ot_amnt);
   						
						if($row_emp[total_over_time]==0){ $row_emp[total_over_time]=0; $row_emp[over_time_rate]=0; $ot_amnt=0; }
			?>
            			<td align="center" valign="middle"><? echo $row_emp[total_over_time]."<br>".$row_emp[over_time_rate]."<br>".round($ot_amnt); ?></td>
            
            <? //$row_earn[$earning_head_id[$i]]
					}
					else if( $earning_head_id[$i]==41  )
					{ 
						?>
                        <td align="center" valign="middle"><? echo $row_emp[ot_allowance_hr]."<br>".($allow_rate[$row_emp['designation_id']])."<br>".round($row_emp[ot_allowance_amount]); ?></td>
                        <?
						$subsection_total_earning_head_amt[$i] += round($row_emp[ot_allowance_amount]);
						$section_total_earning_head_amt[$i] += round($row_emp[ot_allowance_amount]);
						$department_total_earning_head_amt[$i] += round($row_emp[ot_allowance_amount]);
						$division_total_earning_head_amt[$i] += round($row_emp[ot_allowance_amount]);
						$location_total_earning_head_amt[$i] += round($row_emp[ot_allowance_amount]);
						
					}
					else
					{
						//section department division total
						$subsection_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						$section_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						$department_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						$division_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						$location_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						
 			?>
            			<td align="center" valign="middle"><? echo number_format(round($row_earn[$earning_head_id[$i]])); ?></td>
           
            <? 		}
				}
			}
			?>
             <td align="center" valign="middle"><? echo number_format(round($row_emp[total_earning_amount])); ?></td>
            <?
			
			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($deduction_head_id); $i++)
			{
				if ($i!=count($deduction_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN round(pay_amount) END) AS '".$deduction_head_id[$i]."', ";
					
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN round(pay_amount) END) AS '".$deduction_head_id[$i]."' ";
					//echo $sql_attnd;die;
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=1 order by salary_head_id";
			 
			//echo $sql_attnd;die;
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				for ($i=0; $i<count($deduction_head_id); $i++)
				{
					 //section department division total
					 $subsection_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 $section_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 $department_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 $division_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 $location_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					// print_r($deduction_head_id);die;
			?>
            
            <td align="center" valign="middle"><? echo number_format(round($row_earn[$deduction_head_id[$i]])); ?></td>
           
            <?  
				}
			}
			?>
            
             <td align="center" valign="middle"><? echo number_format(round($row_emp[total_deduction_amount])); ?></td>
             <td align="center" valign="middle"><? echo number_format(round($row_emp[not_payable_deduction_amount])); //number_format(round($row_emp[not_payble_days]*$row_emp[daily_gross_salary])) ?></td>
             <td align="center" valign="middle"><? echo number_format(round($row_emp[net_payable_amount])); ?></td>
             <? $cash = $row_emp[net_payable_amount]-$row_emp[bank_gross]; ?>
             <td align="center" valign="middle"><? if($cash>0){echo number_format(round($row_emp[bank_gross]));}else{$cash=0;$row_emp[bank_gross]=$row_emp[net_payable_amount];echo number_format(round($row_emp[bank_gross]));} ?></td>
             <td align="center" valign="middle"><? echo number_format(round($cash)); ?></td>
             <td></td>
        </tr>
        <?
		//$pb++;
		//if($pb==7) { echo  '<hr class="pb" />'; $pb=0; } 
				$ct++;
				//sub section 
				$subsection_total_gross += round($row_emp[gross_salary]);
				$subsection_total_salary += round($row_emp[total_earning_amount]);				
				$subsection_total_not_payable += round($row_emp[not_payable_deduction_amount]);
				$subsection_total_deduction += round($row_emp[total_deduction_amount]);
				$subsection_total_net_payable += round($row_emp[net_payable_amount]);
				$subsection_total_bank += round($row_emp[bank_gross]);
				$subsection_total_cash += round($cash);
				
 				//section 
				$section_total_gross += round($row_emp[gross_salary]);
				$section_total_salary += round($row_emp[total_earning_amount]);				
				$section_total_not_payable += round($row_emp[not_payable_deduction_amount]);
				$section_total_deduction += round($row_emp[total_deduction_amount]);
				$section_total_net_payable += round($row_emp[net_payable_amount]);
				$section_total_bank += round($row_emp[bank_gross]);
				$section_total_cash += round($cash);
				
				//department 
				$department_total_gross += round($row_emp[gross_salary]);
				$department_total_salary += round($row_emp[total_earning_amount]);				
				$department_total_not_payable += round($row_emp[not_payable_deduction_amount]);
				$department_total_deduction += round($row_emp[total_deduction_amount]);
				$department_total_net_payable += round($row_emp[net_payable_amount]);
				$department_total_bank += round($row_emp[bank_gross]);
				$department_total_cash += round($cash);
				
				//division 
				$division_total_gross += round($row_emp[gross_salary]);
				$division_total_salary += round($row_emp[total_earning_amount]);				
				$division_total_not_payable += round($row_emp[not_payable_deduction_amount]);
				$division_total_deduction += round($row_emp[total_deduction_amount]);
				$division_total_net_payable += round($row_emp[net_payable_amount]);
				$division_total_bank += round($row_emp[bank_gross]);
				$division_total_cash += round($cash);
				
				//division 
				$location_total_gross += round($row_emp[gross_salary]);
				$location_total_salary += round($row_emp[total_earning_amount]);				
				$location_total_not_payable += round($row_emp[not_payable_deduction_amount]);
				$location_total_deduction += round($row_emp[total_deduction_amount]);
				$location_total_net_payable += round($row_emp[net_payable_amount]);
				$location_total_bank += round($row_emp[bank_gross]);
				$location_total_cash += round($cash);
				
				//grand total
				$grand_total_gross 			+= round($row_emp[gross_salary]);
				$grand_total_salary 		+= round($row_emp[total_earning_amount]);		
				$grand_total_net_payable 	+= round($row_emp[net_payable_amount]);
				$grand_total_not_payable 	+= round($row_emp[not_payable_deduction_amount]);
				$grand_total_deduction 		+= round($row_emp[total_deduction_amount]);
				$grand_total_bank 			+= round($row_emp[bank_gross]);
				$grand_total_cash 			+= round($cash);
				
	}//while loop end here 
	
	?>
    
	</tbody>   
    <?
    	if($status_subsec==1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Total</td>
					<td align="center"><? echo number_format($subsection_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($subsection_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($subsection_total_salary); ?></td> 
					<? foreach($subsection_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($subsection_total_deduction); ?></td>
					<td align="center" ><? echo number_format($subsection_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($subsection_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($subsection_total_bank); ?></td>
					<td align="center" ><? echo number_format($subsection_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$subsection_total_gross=0;$subsection_total_salary=0;$subsection_total_deduction=0;$subsection_total_not_payable=0;$subsection_total_net_payable=0;$subsection_total_bank=0;$subsection_total_cash=0;	
			$subsection_total_earning_head_amt = array();$subsection_total_deduction_head_amt = array();

		}
		
		if($status_sec==1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Section Total</td>
					<td align="center"><? echo number_format($section_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($section_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($section_total_salary); ?></td> 
					<? foreach($section_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($section_total_deduction); ?></td>
					<td align="center" ><? echo number_format($section_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($section_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($section_total_bank); ?></td>
					<td align="center" ><? echo number_format($section_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$section_total_gross=0;$section_total_salary=0;$section_total_deduction=0;$section_total_not_payable=0;$section_total_net_payable=0;$section_total_bank=0;$section_total_cash=0;	
			$section_total_earning_head_amt = array();$section_total_deduction_head_amt = array();
						
		}
		
		if($status_dept==1 )
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Department Total</td>
					<td align="center"><? echo number_format($department_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($department_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($department_total_salary); ?></td> 
					<? foreach($department_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($department_total_deduction); ?></td>
					<td align="center" ><? echo number_format($department_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($department_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($department_total_bank); ?></td>
					<td align="center" ><? echo number_format($department_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$department_total_gross=0;$department_total_salary=0;$department_total_deduction=0;$department_total_not_payable=0;$department_total_net_payable=0;$department_total_bank=0;$department_total_cash=0;	
			$department_total_earning_head_amt = array();$department_total_deduction_head_amt = array();
		}
		 
		if($status_divis==1 )
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Division Total</td>
					<td align="center"><? echo number_format($division_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($division_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($division_total_salary); ?></td> 
					<? foreach($division_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($division_total_deduction); ?></td>
					<td align="center" ><? echo number_format($division_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($division_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($division_total_bank); ?></td>
					<td align="center" ><? echo number_format($division_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$division_total_gross=0;$division_total_salary=0;$division_total_deduction=0;$division_total_not_payable=0;$division_total_net_payable=0;$division_total_bank=0;$division_total_cash=0;	
			$division_total_earning_head_amt = array();$division_total_deduction_head_amt = array();
 			
		}
		 
		if($status_loc==1 )
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Location Total</td>
					<td align="center"><? echo number_format($location_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($location_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($location_total_salary); ?></td> 
					<? foreach($location_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($location_total_deduction); ?></td>
					<td align="center" ><? echo number_format($location_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($location_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($location_total_bank); ?></td>
					<td align="center" ><? echo number_format($location_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$location_total_gross=0;$location_total_salary=0;$location_total_deduction=0;$location_total_not_payable=0;$location_total_net_payable=0;$location_total_bank=0;$location_total_cash=0;	
			$location_total_earning_head_amt = array();$location_total_deduction_head_amt = array();
 			
		}
    ?>
</table>
<? 
	echo signeture_table_peak(8,$rpt_company,"1700px") ; 
	
	$html = ob_get_contents();
	ob_clean();	
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) 
		{			
            @unlink($filename);
		}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
 	}
 ?>




<?
 
if($types=="wages_salary_sheet2") //for head office formate-1
{	
	
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];	
	$sql_sal="select * from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date'";
	//echo $sql_sal;die;
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}
?>	
	 <style>
		
	.verticalText 
		{               
			writing-mode: tb-rl;
			filter: flipv fliph;
			-webkit-transform: rotate(270deg);
			-moz-transform: rotate(270deg);
		}
		
	</style>
	
<?
	
	
	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10)
	{
		$emp_status="and a.emp_status in ( 0 )";
		//$txt_from_date = $previous_month;
		
	}
	//else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and  a.designation_id='$designation_id'";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id='$department_id'";
	if ($payment_method==0) $payment_met =""; else if ($payment_method==1) $payment_met ="and a.emp_code not in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	else if ($payment_method==2) $payment_met ="and a.emp_code in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	
	if ($company_id==0) 
	{
		$company_id="";			
	}
	else
	{
		$company_id="and com.id='$company_id'";				
	}
		
	$company_info_sql="select com.id, company_name, email, website, plot_no, level_no, road_no,	block_no,zip_code,city, cnt.country_name from lib_company com,lib_list_country cnt where com.country_id =cnt.id $company_id";
	
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	ob_start();
	while( $company_info_result = mysql_fetch_array( $result ) ) 
	{	 	 
	 	 
?>

<table width="1300" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
	<thead>    
     <tr style="text-align:center; font-weight:bold"  >    	 
    	<th colspan="26">
			<div style="border:0px; font-size:15px"><?	echo $company_info_result["company_name"];?></div>
            <div style="border:0px; font-size:11px">
            	Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"]; echo $company_info_result["country_name"];?><br />
            	Salary Sheet For The Month Of <? echo $cur_month; ?>-<? echo $cur_year; ?>          	
        	</div>
        </th>
    </tr>   
    <tr style="text-align:center; font-size:11px;">
   		<th>&nbsp;</th>
        <th colspan="4">Employee Information</th>
        <th colspan="5">Attendence Information</th>
    	<th colspan="6">Salary/Allowance</th>
        <th colspan="8">Deduction</th>
        <th colspan="2">&nbsp;</th>    	
    </tr>   
    <tr style="text-align:center; font-size:11px; font-family:Arial, Helvetica, sans-serif" height="60px">    	
    	<th width="30">SL No</th>
        
      <th width="50">Emp Code</th>
        <th width="100">Name</th>
        <th width="80">Designation</th>
        <th width="20"><div class="verticalText">DOJ</div></th>
    	
        <th width="20"><div class="verticalText">Calendar Days</div></th>
        <th width="20"><div class="verticalText">Leave</div></th>
        <th width="20"><div class="verticalText">Absent</div></th>
        <th width="20"><div class="verticalText">Late</div></th>
        <th width="20"><div class="verticalText">Payable</div></th>
        
        <th width="50"><div class="verticalText">Basic</div></th>
        <th width="50"><div class="verticalText">House<br />Rent</div></th>
        <th width="50"><div class="verticalText">Medical</div></th>
        <th width="40"><div class="verticalText">Other<br />Allowance</div></th>
        <th width="40"><div class="verticalText">Arrear<br />Earnings</div></th>
        <th width="70"><div class="verticalText">Gross<br />Salary</div></th>
    	
        <th width="40"><div class="verticalText">Advance</div></th>
        <th width="40"><div class="verticalText">Tax</div></th>
        <th width="40"><div class="verticalText">Late</div></th>
        <th width="40"><div class="verticalText">Absent</div></th>
        <th width="20"><div class="verticalText">Stamp</div></th>
        <th width="30"><div class="verticalText">Others</div></th>
        <th width="30"><div class="verticalText">Arrear Deduction</div></th>
        <th width="50"><div class="verticalText">Total Deduction</div></th>
    	
        <th width="70">Net payable </th>
        <th width="100"> Signature</th>
    </tr>
    
    </thead>
    <tbody>
	<?
		$i=0;$total_basic=0;$total_house_rent=0;$total_medical=0;$total_other_allowance=0;$total_gross_salary=0;$total_arrear_ern=0;$total_advance=0;$total_tax=0;$total_late=0;$total_absent=0;$total_others=0;$total_arrear_deduction=0;$sum_total_deduction=0;$total_net_payable=0;$total_stamp_reve=0;
		$total_others_allow_ern=0;
		$sql_sal="select a.*,CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,emp.joining_date,emp.emp_code 
				  from hrm_employee emp,hrm_salary_mst a 
				  where 
				  emp.emp_code=a.emp_code and 
				  a.total_calendar_days<>0 and 
				  a.salary_periods like '$txt_from_date' and 
				  a.company_id=$company_info_result[id] 
				  $location_id 
				  $section_id 
				  $subsection_id 
				  $department_id 
				  $designation_id 
				  $category 
				  $payment_met 
				  $emp_status 
				  order by emp.designation_level ASC"; 
		
		//echo $sql_sal; die;
		$exe_sql_sal=mysql_db_query($DB, $sql_sal);
		while ($row_sal = mysql_fetch_array($exe_sql_sal))
		{
			
			$tax = (int)$salary_dtls_arr[$row_sal[emp_code]][22];
			$late_deduction = (int)$salary_dtls_arr[$row_sal[emp_code]][23];
			$absent_deduction = (int)$salary_dtls_arr[$row_sal[emp_code]][24];
			$others_allow_ern = (int)$salary_dtls_arr[$row_sal[emp_code]][19];
			$stamp_reve = (int)$salary_dtls_arr[$row_sal[emp_code]][28];
			$arrear_ern = (int)$salary_dtls_arr[$row_sal[emp_code]][17];
			$advaced_deduction = (int)$salary_dtls_arr[$row_sal[emp_code]][21];
			$arrear_deduction = (int)$salary_dtls_arr[$row_sal[emp_code]][30];
			$others_deduction = (int)$salary_dtls_arr[$row_sal[emp_code]][31];
			//In total sumuary based on company
			$total_basic +=$row_sal[basic_salary];
			$total_house_rent +=$row_sal[house_rent];
			$total_medical +=$row_sal[medical_allowance];
			$total_other_allowance +=$row_sal[conveyence];
			$total_gross_salary +=$row_sal[gross_salary];
			$total_arrear_ern += $arrear_ern;
			$total_advance +=$advaced_deduction;
			$total_tax +=$tax;
			$total_late +=$late_deduction;
			$total_absent +=$absent_deduction;
			$total_stamp_reve +=$stamp_reve;
			$total_others +=$others_deduction;
			$total_arrear_deduction +=$arrear_deduction;
			$sum_total_deduction +=$row_sal[total_deduction_amount];
			$total_net_payable +=$row_sal[net_payable_amount];
						
			$i++;
			if ($i%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
			?>
    <tr bgcolor="<? echo $bgcolor; ?>" height="90"  style="font-size:11px; font-family:Arial, Helvetica, sans-serif">
    	<td>
        	<? echo $i; ?>
        </td>
        <td>
        	<? echo $row_sal[emp_code]; ?>
        </td>
   		<td>
        	<? echo $row_sal[name]; ?>
        </td>
        <td>
        	<? $newtext = wordwrap($designation_chart[$row_sal[designation_id]], 15, "\n", true); echo $newtext; ?>
        </div></td>
        <td>
        	<div class="verticalText"><? echo convert_to_mysql_date($row_sal[joining_date]); ?></div>
        </td>
        <td align="right">
        	<? echo $row_sal[total_calendar_days]; ?>        	
        </td>
        <td align="right">
        	<? echo $row_sal[total_leave_days]; ?>        	
        </td>
        <td align="right">
        	<? echo $row_sal[total_abs_days]; ?>
        </td>
        <td align="right">
        	<? echo $row_sal[total_late_days]; ?>
        </td>
        <td align="right">
        	<? echo $row_sal[payable_days]; ?>
        </td>
        <td align="right">
        	<? echo $row_sal[basic_salary]; ?>
        </td>
        <td align="right">
        	<? echo $row_sal[house_rent]; ?>
        </td>
        <td align="right">
        	<? echo $row_sal[medical_allowance]; ?>
        </td>
        <td align="right">
        	<? echo $others_allow_ern; ?>
        </td>
         <td align="right">
        	<? echo $arrear_ern; ?>
        </td>
  		<td align="right">
        	<? echo $row_sal[gross_salary]; ?>
        </td>
       
            <td align="right" style="background-color:#FFF8DC">
        		<? echo $advaced_deduction;  ?>
        	</td>
            
            <td align="right" style="background-color:#FFF8DC">
        		<?  echo $tax; ?>
        	</td>
            
            <td align="right" style="background-color:#FFF8DC">
        		<?   echo $late_deduction;   ?>
        	</td>
             <td align="right" style="background-color:#FFF8DC">
        		<?  echo $absent_deduction;  ?>
        	</td>
            <td align="right" style="background-color:#FFF8DC">
        		<?  echo $stamp_reve;   ?>
        	</td>
            <td align="right" style="background-color:#FFF8DC">
        		<?  echo $others_deduction;  ?>
        	</td>
            
        <td align="right" style="background-color:#FFF8DC"><?  echo $arrear_deduction; ?>        	
      	</td>    
       
  <td align="right" style="background-color:#FFF8DC">
        	<? echo $row_sal[total_deduction_amount]; ?>
        </td>
        <td align="right">
        	<? echo $row_sal[net_payable_amount]; ?>
        </td>
        <td>
        	 
        </td>        
    </tr>
            
            <?
		}
		?>
	<tr bgcolor="#FFFFFF" style="font-size:12px; font-weight:bold">
    	<td colspan="10">
        	Total
        </td>
        <td align="right">
        	<? echo $total_basic; ?>
        </td>
        <td align="right">
        	<? echo $total_house_rent; ?>
        </td>
        <td align="right">
        	<? echo $total_medical; ?>
        </td>
        <td align="right">
        	<? echo $total_other_allowance; ?>
        </td>
        <td align="right">
        	<? echo $total_arrear_ern; ?>
        </td>
        <td align="right">
        	<? echo $total_gross_salary; ?>
        </td>
         <td align="right"><? echo $total_advance; ?>      	
        </td>
        <td align="right">
        	<? echo $total_tax; ?>
        </td>
        <td align="right">
        	<? echo $total_late; ?>
        </td>
        <td align="right">
        	<? echo $total_absent; ?>
        </td>
        <td align="right">
        	<? echo $total_stamp_reve; ?>
        </td>
        <td align="right">
        	<? echo $total_others; ?>
        </td>
        <td align="right">
        	<? echo $total_arrear_deduction; ?>
        </td>
       <td align="right">
        	<? echo $sum_total_deduction; ?>
        </td>
        <td align="right">
        	<? echo $total_net_payable; ?>
        </td>
        <td align="right">&nbsp;
        	
        </td>
    </tr>
		</tbody>
        
        <tfoot>
        	<tr  height="150" style="font-size:11px; font-family:Arial, Helvetica, sans-serif">
        		<td colspan="26" align="center" valign="bottom"><div style="float:left; width:190px"><strong style="text-decoration:overline">Executive (HR)</strong></div><div style="float:left; width:190px"><strong style="text-decoration:overline">Internal Audit Section</strong></div><div style="float:left; width:190px"><strong style="text-decoration:overline">Executive (Acc and Fin)</strong></div><div style="float:left; width:190px"><strong style="text-decoration:overline">Manager(Acc and Fin)</strong></div><div style="float:left;"><strong style="text-decoration:overline">AGM (Admin, HR & Compliance)</strong></div><div style="float:left; width:190px"><strong style="text-decoration:overline">ED (Acc,Fin and Comm)</strong></div><div style="float:left; width:150px"><strong style="text-decoration:overline">Finance Advisor</strong></div></td>
              </tr>
        </tfoot>

</table>	
<?
	}
	

	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();	

	
}

if($types=="wages_salary_sheet_ho2") //for head office formate-2 automated
{	
	$table_width=(count($salary_head)+count($earning_head)+count($deduction_head))*40;
	
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];	
	$sql_sal="select * from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date' and d.salary_head_id in (select id from lib_payroll_head where is_applicable=1 and salary_head=1 and status_active=1 and is_deleted=0 order by id) ";
	//echo $sql_sal;die;
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}
	
?>
	
<style>
		.verticalText 
		{
			writing-mode: tb-rl;
			filter: flipv fliph;
			-webkit-transform: rotate(270deg);
			-moz-transform: rotate(270deg);
		}
		#rpt_table tbody{ color:#000000; font:Arial, Helvetica, sans-serif; font-size:12px; border:1px solid #000;}
	</style>
	
<?	
	
	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10)
	{
		$emp_status="and a.emp_status in ( 0 )";
		//$txt_from_date = $previous_month;
		
	}
	//else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and  b.designation_id='$designation_id'";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id='$department_id'";
	if ($payment_method==0) $payment_met =""; else if ($payment_method==1) $payment_met ="and a.emp_code not in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	else if ($payment_method==2) $payment_met ="and a.emp_code in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";	
?>
	
<table id="rpt_table" width="<? echo $table_width+1000; ?>" cellpadding="0" cellspacing="0" rules="all" class="rpt_table">

<?	
    $new_dept=array();
	$total_print_check=0;
	$sql_emp = "select a.*,b.*, a.gross_salary as e_gross_salary 
				from hrm_salary_mst a, hrm_employee b 
				where 
				a.emp_code=b.emp_code and 
				a.salary_periods='$txt_from_date' 
				$emp_code $location_id $section_id $subsection_id $designation_id $department_id $company_id $category $payment_met $emp_status order by b.company_id,b.designation_id";
	//echo $sql_emp;die;
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{ 
		$sl++;
		if (in_array($row_emp['company_id'], $new_dept))
			{
				$i++;
				//total sumuary, based on company
				$total_earning_amount_in += $row_emp[total_earning_amount];
				$sum_total_deduction +=$row_emp[total_deduction_amount];
				$total_net_payable +=$row_emp[net_payable_amount];
				
			}
			else
			{	
				
				$sl=1;		 
	 	 		$company_info_sql="select com.id, company_name, email, website, plot_no, level_no, road_no,	block_no,zip_code,city, cnt.country_name from lib_company com,lib_list_country cnt where com.country_id =cnt.id and com.id='$row_emp[company_id]'";
				$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
				$company_info_result = mysql_fetch_array($result);
				

     //sub total as per company
     if($total_print_check!=0){ ?>  
        <tr style="font-weight:bold">
            	<td colspan="10" align="center"><b>Total</b></td>
                <? foreach($total_salary_per_head as $key=>$val){ ?>
                <td align="right"><? echo $val; ?></td>	
				<? } ?>
				<? foreach($total_earning_per_head as $key=>$val){ ?>
                <td align="right"><? echo $val; ?></td>	
				<? } ?>
                <td align="right"><? echo $total_earning_amount_in; ?></td>
				<? foreach($total_deduction_per_head as $key=>$val){ ?>
                <td align="right"><? echo $val; ?></td>	
				<? } ?>
                <td align="right"><? echo $sum_total_deduction; ?></td>
                <td align="right"><? echo $total_net_payable; ?></td>
                <td></td>
  </tr>
    <? 
		} 
		
		//total sumuary, based on company variable initialize				
		$total_earning_amount_in = $row_emp[total_earning_amount];
		$sum_total_deduction =$row_emp[total_deduction_amount];
		$total_net_payable =$row_emp[net_payable_amount];
	?>
    
    <thead> 
    <tr style="text-align:center; font-weight:bold"  >    	 
    	<th colspan="<? echo count($salary_head)+count($earning_head)+count($deduction_head)+15 ; ?>">
			<div style="border:0px; font-size:15px"><?	echo $company_info_result["company_name"];?></div>
            <div style="border:0px; font-size:11px">
            	Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"]; echo $company_info_result["country_name"];?><br />
            	Salary Sheet For The Month Of <? echo $cur_month; ?>-<? echo $cur_year; ?>          	
        	</div>
        </th>
    </tr>     
    <tr style="text-align:center; font-weight:bold;font-size:12px">
   		<th>&nbsp;</th>
        <th colspan="4">Employee Information</th>
        <th colspan="5">Attendence Information</th>
        <th colspan="<? echo count($salary_head); ?>" >Salary</th>
    	<th colspan="<? echo count($earning_head)+1; ?>" >Earning</th>
        <th colspan="<? echo count($deduction_head)+1; ?>" >Deduction</th>
        <th colspan="2">&nbsp;</th>    	
    </tr>    
    <tr style="text-align:center; font-weight:bold; font-size:12px">
    	<th width="30" rowspan="2">SL No</th><th width="50" rowspan="2">Emp Code</th><th width="100" rowspan="2">Name</th><th width="80" rowspan="2">Designation</th>
    	<th width="30" rowspan="2"><div class="verticalText">DOJ</div></th><th colspan="5">Days</th>
        <? foreach( $salary_head as $key=>$val) { ?><th width="40" rowspan="2"><div class="verticalText"><? echo $val; ?></div></th> <? } ?>
		<? foreach( $earning_head as $key=>$val) { ?><th width="40" rowspan="2"><div class="verticalText"><? echo $val; ?></div></th> <? } ?><th width="50" rowspan="2">Total Earning</th>
    	<? foreach( $deduction_head as $key=>$val) { ?><th width="40" rowspan="2"><div class="verticalText"><? echo $val; ?></div></th> <? } ?><th width="50" rowspan="2">Total Deduction</th><th width="70" rowspan="2">Net payable </th><th width="150" rowspan="2"> Signature</th>
    </tr>
    <tr style="text-align:center; font-weight:bold;font-size:12px" height="80">
      <th width="30"><span class="verticalText">WO</span></th>
      <th width="30"><span class="verticalText">Leave</span></th>
      <th width="30"><span class="verticalText">Absent</span></th>
      <th width="30"><div class="verticalText">Late</div></th>
      <th width="30"><span class="verticalText">Pay</span></th>
    </tr>
    </thead>
		<?
                    $new_dept[$i]=$row_emp[company_id];
                    $i++;
					$total_print_check++;
                }
				
				if($sl%2==0) $bgcolor='#E9F3FF';
				else $bgcolor='#FFFFFF';	
        ?>
    
    	<tbody>	
        	<tr height="96" bgcolor="<? echo $bgcolor; ?>">       
                <td align="center" valign="middle"><? echo $sl; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[emp_code]; ?></td>              
                <td align="center" valign="middle"><? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name]; ?></td>
                <td align="center" valign="middle"><? echo $designation_chart[$row_emp[designation_id]]; ?></td>
                <td align="center" valign="middle"><div class="verticalText"><? echo $row_emp[joining_date]; ?></div></td>
                <td align="center" valign="middle"><? echo $row_emp[total_calendar_days]; ?></td>
				<td align="center" valign="middle"><? echo $row_emp[total_leave_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[total_abs_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[total_late_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[payable_days]; ?></td>
                
				<? foreach( $salary_head as $key=>$val) { 
                	if($key==20){ 
						$total_salary_per_head[$key] +=$row_emp[e_gross_salary];;		
				?>
						<td align="center" valign="middle"><? echo $row_emp[e_gross_salary]; ?></td>
                <?        
					}else{
						$total_salary_per_head[$key] +=$salary_dtls_arr[$row_emp[emp_code]][$key];
				?>	
                	<td align="center" valign="middle"><? if($salary_dtls_arr[$row_emp[emp_code]][$key]=='') echo "0"; else echo $salary_dtls_arr[$row_emp[emp_code]][$key]; ?></td>
                <? } } ?>
                              
               
            <?
			
			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($earning_head_id); $i++)
			{
				if ($i!=count($earning_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."' ";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=0 order by salary_head_id";
			//echo $sql_attnd;die;  
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				for ($i=0; $i<count($earning_head_id); $i++)
				{
				
					if( $earning_head_id[$i]==5 )
					{
						$ot_amnt=return_field_value("sum(pay_amount)","hrm_salary_dtls","emp_code='$row_emp[emp_code]' and salary_mst_id=$row_emp[0] and salary_head_id in (5,6,7)");
						$total_earning_per_head[$i] +=$ot_amnt;
			?>
            			<td align="center" valign="middle"><? echo $row_emp[total_over_time]."<br>".$row_emp[over_time_rate]."<br>".$ot_amnt; ?></td>
            
            <? //$row_earn[$earning_head_id[$i]]
					}
					else
					{
						$total_earning_per_head[$i] +=$row_earn[$deduction_head_id[$i]];
			?>
            			<td align="center" valign="middle"><? echo $row_earn[$earning_head_id[$i]]; ?></td>
           
            <? 		}
				}
			}
				
			?>
             <td align="center" valign="middle"><? echo $row_emp[total_earning_amount]; ?></td>
            <?
			
			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($deduction_head_id); $i++)
			{
				if ($i!=count($deduction_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."' ";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=1 order by salary_head_id";
			 
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				for ($i=0; $i<count($deduction_head_id); $i++)
				{
					 $total_deduction_per_head[$i] +=$row_earn[$deduction_head_id[$i]];
			?>
            <td align="center" valign="middle"><? echo $row_earn[$deduction_head_id[$i]]; ?></td>
           
            <?  
				}
			}
			?>
                <td align="center" valign="middle"><? echo $row_emp[total_deduction_amount]; ?></td> 
                <td align="center" valign="middle"><? echo $row_emp[net_payable_amount]; ?></td>
                <td align="center" valign="middle">&nbsp;</td>
            </tr>    
        </tbody>
        <?
			}
		?>
        <tbody>
        	<tr style="font-weight:bold">
            	<td colspan="10" align="center"><b>Total</b></td>
                <? foreach($total_salary_per_head as $key=>$val){ ?>
                <td align="right"><? echo $val; ?></td>	
				<? } ?>
				<? foreach($total_earning_per_head as $key=>$val){ ?>
                <td align="right"><? echo $val; ?></td>	
				<? } ?>
                <td align="right"><? echo $total_earning_amount_in; ?></td>
				<? foreach($total_deduction_per_head as $key=>$val){ ?>
                <td align="right"><? echo $val; ?></td>	
				<? } ?>
                <td align="right"><? echo $sum_total_deduction; ?></td>
                <td align="right"><? echo $total_net_payable; ?></td>
                <td></td>
            </tr>
        </tbody>
        <tfoot>
        	<tr height="150" style="font-size:12px; font-family:Arial, Helvetica, sans-serif;">
        		<td colspan="30" align="center" valign="bottom" ><div style="float:left; width:200px"><strong style="text-decoration:overline">Executive (HR)</strong></div><div style="float:left; width:200px"><strong style="text-decoration:overline">Internal Audit Section</strong></div><div style="float:left; width:200px"><strong style="text-decoration:overline">Executive (Acc and Fin)</strong></div><div style="float:left; width:200px"><strong style="text-decoration:overline">Manager(Acc and Fin)</strong></div><div style="float:left;"><strong style="text-decoration:overline">AGM (Admin, HR & Compliance)</strong></div><div style="float:left; width:200px"><strong style="text-decoration:overline">ED (Acc,Fin and Comm)</strong></div><div style="float:left; width:200px"><strong style="text-decoration:overline">Finance Advisor</strong></div></td>
          </tr>
        </tfoot>
</table>

<? 

	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();	



}
	
	
if($types=="wages_salary_sheet4") // asrotext ltd formate-final automated
{
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];
	$rpt_company=$company_id;
	//salary details 
	$salary_breakdown = array();
	$sql_sal="select d.* from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date'";
	//echo $sql_sal;die;
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}

	
	
	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10)
	{
		$emp_status="and a.emp_status in ( 0 )";
		//$txt_from_date = $previous_month;
		
	}
	//else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and  b.designation_id='$designation_id'";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id='$department_id'";
	if ($payment_method==0) $payment_met =""; else if ($payment_method==1) $payment_met ="and a.emp_code not in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	else if ($payment_method==2) $payment_met ="and a.emp_code in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	if ($company_id==0){ $company="";}else{$company="and a.company_id='$company_id'";}
	if ($emp_code==""){ $emp_code="";}else{$emp_code="and a.emp_code in ($emp_code)";}
	
	
	ob_start();
	
				$company_info_sql="select * from lib_company where id='$company_id'";
				//echo $company_info_sql;die;
				$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
				$company_info_result = mysql_fetch_array($result);
	
	
	?>
    
	<style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style>
    
	<table width="1600" cellpadding="0" cellspacing="0" style="font-size:11px; font-family:'Arial Black', Gadget, sans-serif;" class="rpt_table"  rules="all" ><!--rules="all"-->
		       
        
	<?
	$sl=0;
	$new_section=array();
	$new_department=array();
		
	
	$sql_emp = "select a.*, a.gross_salary as sal_gross_salary,a.basic_salary as sal_basic_salary, a.house_rent as sal_house_rent,a.medical_allowance as sal_medical_allowance,b.* 
				from hrm_salary_mst a, hrm_employee b 
				where
				a.emp_code=b.emp_code and 
				a.salary_periods like '$txt_from_date' 
				$emp_code $location_id $section_id $subsection_id $category $payment_met $designation_id $department_id $company $emp_status 
				order by b.department_id,b.section_id,CAST(b.id_card_no as SIGNED)";
	
	//echo $sql_emp;die;
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);	
	while ($row_emp = mysql_fetch_array($exe_sql_emp)) // Master Qry starts 
	{
		$sl++;
		if($sl%2==0) $bgcolor='#FFFFFF';
		else $bgcolor='#EEEEEE';
		
		$total_holiday = $row_emp[government_holiday]+$row_emp[festival_holiday]+$row_emp[compensatory_total_holiday];
		
			
		if(in_array($row_emp[section_id],$new_section)) //sub total
		{
			
			
		}
		else{
				$new_section[$row_emp[section_id]] = $row_emp[section_id];
				if($sl!=1){
			?>
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Sub Total</td>
                    <? foreach($sec_sub_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $sec_sub_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $sec_sub_total_overtime_amt;?></td>
                    <? foreach($sec_sub_total_earning_arr as $key=>$val){
					?>
                    	<td><? echo $val; ?></td>
                    <? } ?>                   
                    <td><? echo $sec_sub_total_earning_total_amt; ?></td>
                    <? foreach($sec_sub_total_deduc_sal as $key=>$val){
					?>
                    	<td><? echo $val; ?></td>
                    <? } ?>                    
                     <td><? echo $sec_sub_total_deduc_amt; ?></td>
                     <? foreach($sec_sub_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>                     
                    <td><? echo $sec_sub_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>
			<?
				$sec_sub_total_sal = array();
				$sec_sub_total_salary_payable_amt = 0;
				$sec_sub_total_overtime_amt = 0;
				$sec_sub_total_earning_arr = array();
				$sec_sub_total_earning_total_amt = 0;
				$sec_sub_total_deduc_sal = array();
				$sec_sub_total_deduc_amt = 0;
				$sec_sub_total_payable_amt = 0;
				$sec_sub_total_late_absent_lwp = array();
			}
			
			
			if(in_array($row_emp[department_id],$new_department)) //grand total
		{
			
		}
		else{
				$new_department[$row_emp[department_id]] = $row_emp[department_id];
				if($sl!=1){
			?>
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Grand Total</td>
                    <? foreach($dept_grand_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $dept_grand_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $dept_grand_total_overtime_amt;?></td>
                    <? foreach($dept_grand_total_earning_arr as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $dept_grand_total_earning_total_amt; ?></td>
                    <? foreach($dept_grand_total_deduc_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                     <td><? echo $dept_grand_total_deduc_amt; ?></td>
                     <? foreach($dept_grand_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?> 
                    <td><? echo $dept_grand_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>
			<?
				$dept_grand_total_sal = array();
				$dept_grand_total_salary_payable_amt = 0;
				$dept_grand_total_overtime_amt = 0;
				$dept_grand_total_earning_arr = array();
				$dept_grand_total_earning_total_amt = 0;
				$dept_grand_total_deduc_sal = array();
				$dept_grand_total_deduc_amt = 0;
				$dept_grand_total_payable_amt = 0;
				$dept_grand_total_late_absent_lwp = array();
			}
		}
			
				
			?>
          

                <thead>  
                    <tr style="text-align:center; font-weight:bold; border:solid 0 #060;">    	 
                        <th colspan="<? echo count($salary_head)+count($earning_head_id)+count($deduction_head_id)+26; ?>" >
                            <div style="border:0px; font-size:17px"><?	echo $company_info_result["company_name"];?></div>
                            <div style="border:0px; font-size:12px">
                                Address: <? echo $company_info_result["plot_no"]; ?>, <? echo $company_info_result["level_no"]; ?>, <? echo $company_info_result["block_no"]; ?>, <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                                Salary Sheet For The Month Of <? echo $cur_month; ?>-<? echo $cur_year; ?>            	
                            </div>
                            <div align="left"><b>Department :</b> <? echo $department_details[$row_emp['department_id']]; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Section : </b> <? echo $section_details[$row_emp['section_id']]; ?></div>
                        </th>
                    </tr>
                    <tr>
                        <th rowspan="2" width="30">SL</th>
                        <th rowspan="2" width="130">Employee</th>
                        <th rowspan="2" width="70">Card No</th>
                        <th rowspan="2" width="70">Emp Code</th>
                        <th rowspan="2" width="100">Designation</th>
                        <th rowspan="2" width="50">DOJ</th>
                        <th rowspan="2" width="40">Grade</th>
                        <? foreach( $salary_head as $key=>$val) {
								if($key==20){
						?>
                                	<th rowspan="2" width="80"><? echo $val; ?></th>
                        <? 		} 
						}
						?>
                        <? foreach( $salary_head as $key=>$val) {
								if($key!=20){
						?>
                                	<th rowspan="2" width="80"><? echo $val; ?></th>
                        <? 		} 
						}
						?>
                        <th rowspan="2" width="40">DOM</th>
                        <th rowspan="2" width="40">Pre Days</th>
                        <th colspan="3" width="70">Leave</th>
                        <th width="40">WD</th>
                        <th rowspan="2" width="40">Abs Days</th>
                        <th rowspan="2" width="40">Pay Days</th>
                        <th rowspan="2" width="40">Salary Payment</th>
                        <th colspan="3" width="150">Overtime</th>
                        <?    
                        //echo count($earning_head);
                        for ($i=0; $i<count($earning_head); $i++)
                        {
                            if($earning_head_id[$i]!=5)
                            {
                        ?>	
                                <th width="50" rowspan="2"><div class="verticalText"><? echo $earning_head[$i];  ?></div></th>
                        <?
                            }
                            
                        }
                        ?>
                            <th width="50" rowspan="2">Total Earn</th>
                        <? 
                        for ($i=0; $i<count($deduction_head); $i++)
                        {
							if($deduction_head_id[$i]!=23 && $deduction_head_id[$i]!=24 && $deduction_head_id[$i]!=29)
							{
						?>				
                            	<th width="50" rowspan="2"><div class="verticalText"><? echo $deduction_head[$i];  ?></div></th>
                        <? 
							}
							else
							{
								$late_absent_lwp_head[$deduction_head_id[$i]]=$deduction_head[$i];
							}
						} 
						?>	
                        <th width="50" rowspan="2">Total Deduct</th> 
                        <?
                        foreach($late_absent_lwp_head as $key=>$val)
                        {
                        ?>
                            <th width="50" rowspan="2"><div class="verticalText"><? echo $val;  ?></div></th>
                        <?	
                        } 
						?>         
                        <th rowspan="2" width="80">Net Payable</th>
                        <th rowspan="2" width="150">Signature</th>
                    </tr>
                    <tr>
                      <th>CL</th>
                      <th>SL</th>
                      <th>EL</th>
                      <th>HD</th>
                      <th>OT Hour</th>
                      <th>OT Rate</th>
                      <th>OT Amt</th>
                    </tr>        
                </thead>
          
            
        <?    	
			
		}
		
	?>	
		
		<tbody>	    	
    		<tr height="94" bgcolor="<? echo $bgcolor; ?>" >
        		<td align="center" valign="middle"><? echo $sl; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[id_card_no]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[emp_code]; ?></td>
                <td align="center" valign="middle"><? echo $designation_chart[$row_emp[designation_id]]; ?></td>
                <td align="center" valign="middle"><div class="verticalText"><? echo $row_emp[joining_date]; ?></div></td>
                <td align="center" valign="middle"><? echo $row_emp[salary_grade]; ?></td>
                <? foreach( $salary_head as $key=>$val) { 
					if($key==20){
						 $sec_sub_total_sal[$key] += round($row_emp[sal_gross_salary]); 
						 $dept_grand_total_sal[$key] += round($row_emp[sal_gross_salary]); 
					?>
                         <td align="center" valign="middle"><? echo round($row_emp[sal_gross_salary]); ?></td>
                    <? 
						} 
					}
					
					foreach( $salary_head as $key=>$val) {
						if($key!=20){
						 $sec_sub_total_sal[$key] += round($salary_dtls_arr[$row_emp[emp_code]][$key]);
						 $dept_grand_total_sal[$key] += round($salary_dtls_arr[$row_emp[emp_code]][$key]);
					?>    
                         <td align="center" valign="middle"><? echo round($salary_dtls_arr[$row_emp[emp_code]][$key]); ?></td>
					<? 		
						} 
					} 
				?>
                
                
                <td align="center" valign="middle"><? echo $row_emp[total_calendar_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[payable_days]-$total_holiday-$row_emp[total_weekly_holiday]-$row_emp[total_leave_days];?></td>
                <td align="center" valign="middle"><? echo $row_emp[casual_leave]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[sick_leave]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[earn_leave]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[total_weekly_holiday]."<br />". $total_holiday; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[total_abs_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[payable_days]; ?></td>
                <? 
				$not_payable_amounts = round( $row_emp[not_payble_days]*$row_emp[daily_gross_salary] );
				//gross - absent - late - lwp - not payable 
				$late_absent_lwp_deduc_amt = round($salary_dtls_arr[$row_emp[emp_code]][23]+$salary_dtls_arr[$row_emp[emp_code]][24]+$salary_dtls_arr[$row_emp[emp_code]][29]);
				$gross_salary_on_pay_days = $row_emp[sal_gross_salary]- $late_absent_lwp_deduc_amt -$not_payable_amounts;
				$salary_payment = round($gross_salary_on_pay_days);
				$sec_sub_total_salary_payable_amt += $salary_payment; 
				$dept_grand_total_salary_payable_amt += $salary_payment; 
				?>
                <td align="center" valign="middle"><? echo $salary_payment; ?></td>
                
                <td align="center" valign="middle"><? echo $row_emp[total_over_time]; ?></td>
                <td align="center" valign="middle"><? if( $salary_dtls_arr[$row_emp[emp_code]][5]>0 ) echo $row_emp[over_time_rate]; else echo "0"; ?></td>
                <? $sec_sub_total_overtime_amt += round($salary_dtls_arr[$row_emp[emp_code]][5]+$salary_dtls_arr[$row_emp[emp_code]][6]+$salary_dtls_arr[$row_emp[emp_code]][7]); ?>
                <? $dept_grand_total_overtime_amt += round($salary_dtls_arr[$row_emp[emp_code]][5]+$salary_dtls_arr[$row_emp[emp_code]][6]+$salary_dtls_arr[$row_emp[emp_code]][7]);?>
                <td align="center" valign="middle"><? echo round($salary_dtls_arr[$row_emp[emp_code]][5]+$salary_dtls_arr[$row_emp[emp_code]][6]+$salary_dtls_arr[$row_emp[emp_code]][7]); ?></td>
                
                <?
			
					$sql_attnd =" SELECT  ";
					for ($i=0; $i<count($earning_head_id); $i++)
					{
						if ($i!=count($earning_head_id)-1)
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."', ";
						}
						else
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."' ";
						}
					}
					$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=0 order by salary_head_id";
					//echo $sql_attnd;die;  
					$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
					if($row_earn = mysql_fetch_array($exe_sql_emp1))
					{
						for ($i=0; $i<count($earning_head_id); $i++)
						{
						
							if( $earning_head_id[$i]!=5 )
							{
								$sec_sub_total_earning_arr[$i] += round($row_earn[$earning_head_id[$i]]);
								$dept_grand_total_earning_arr[$i] += round($row_earn[$earning_head_id[$i]]);
					?>
								<td align="center" valign="middle"><? if($row_earn[$earning_head_id[$i]]>0) echo round($row_earn[$earning_head_id[$i]]); else echo "0"; ?></td>
					
					<? //$row_earn[$earning_head_id[$i]]
							}							
						}
					}
						
						$total_eran = round( $row_emp[total_earning_amount]-($salary_dtls_arr[$row_emp[emp_code]][23]+$salary_dtls_arr[$row_emp[emp_code]][24]+$salary_dtls_arr[$row_emp[emp_code]][29])-($row_emp[not_payble_days]*$row_emp[daily_gross_salary]) );
						$sec_sub_total_earning_total_amt += $total_eran;
						$dept_grand_total_earning_total_amt += $total_eran;
					?>
					 	
                        <td align="center" valign="middle"><? echo $total_eran; ?></td>
					
					<?
					
					$sql_attnd =" SELECT  ";
					for ($i=0; $i<count($deduction_head_id); $i++)
					{
						
						if ($i!=count($deduction_head_id)-1)
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."', ";
						}
						else
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."' ";
						}
						
					}
					$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=1 order by salary_head_id";
					//echo $sql_attnd;die;
					$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
					if($row_earn = mysql_fetch_array($exe_sql_emp1))
					{
						
						for ($i=0; $i<count($deduction_head_id); $i++)
						{
							if($deduction_head_id[$i]!=23 && $deduction_head_id[$i]!=24 && $deduction_head_id[$i]!=29)
							{
							 	$sec_sub_total_deduc_sal[$i] += round($row_earn[$deduction_head_id[$i]]);
							 	$dept_grand_total_deduc_sal[$i] += round($row_earn[$deduction_head_id[$i]]);
					?>
								<td align="center" valign="middle"><? if($row_earn[$deduction_head_id[$i]]>0 ) echo round($row_earn[$deduction_head_id[$i]]); else echo "0"; ?></td>
					<?  
							}
							else
							{
								$late_absent_lwp_head_val[$deduction_head_id[$i]] =  round($row_earn[$deduction_head_id[$i]]);
							}
						}
					}
						
						$sec_sub_total_deduc_amt += round($row_emp[total_deduction_amount]-$late_absent_lwp_deduc_amt);
						$dept_grand_total_deduc_amt += round($row_emp[total_deduction_amount]-$late_absent_lwp_deduc_amt);
				?>
                <td align="center" valign="middle"><? echo round($row_emp[total_deduction_amount]-$late_absent_lwp_deduc_amt); ?></td>
               <?
				foreach($late_absent_lwp_head_val as $key=>$val)
				{
					$sec_sub_total_late_absent_lwp[$key] += $val;
					$dept_grand_total_late_absent_lwp[$key] += $val;
				?>
					<td align="center" valign="middle"><? echo round($val); ?></td>
				<?	
				}				
					$sec_sub_total_payable_amt += round($row_emp[net_payable_amount]); 
					$dept_grand_total_payable_amt += round($row_emp[net_payable_amount]); 
				?>
                <td align="center" valign="middle"><? echo $row_emp[net_payable_amount]; ?></td>
                <td align="center" valign="middle">&nbsp;</td>			
    		</tr>
        </tbody>    
    <?
	}// while loop end	   
		   ?>
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Sub Total</td>
                    <? foreach($sec_sub_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $sec_sub_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $sec_sub_total_overtime_amt;?></td>
                    <? foreach($sec_sub_total_earning_arr as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $sec_sub_total_earning_total_amt; ?></td>
                    <? foreach($sec_sub_total_deduc_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $sec_sub_total_deduc_amt; ?></td>
                    <? foreach($sec_sub_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?> 
                    <td><? echo $sec_sub_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>
  
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Grand Total</td>
                    <? foreach($dept_grand_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $dept_grand_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $dept_grand_total_overtime_amt;?></td>
                    <? foreach($dept_grand_total_earning_arr as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $dept_grand_total_earning_total_amt; ?></td>
                    <? foreach($dept_grand_total_deduc_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                     <td><? echo $dept_grand_total_deduc_amt; ?></td>
                    <? foreach($dept_grand_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?> 
                    <td><? echo $dept_grand_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>   
    
    			<tr height="130" valign="bottom">
                	<td colspan="<? echo count($salary_head)+count($earning_head_id)+count($deduction_head_id)+26; ?>" align="center">
                		<!--<div style="width:240px;float:left; text-decoration:overline">Prepared By</div>
                        <div style="width:240px;float:left; text-decoration:overline">Officer(P&C)</div>
                    	<div style="width:240px;float:left; text-decoration:overline">Manager(HR)</div>				
                    	<div style="width:240px;float:left; text-decoration:overline">Executive(Accounts)</div>						
                    	<div style="width:240px;float:left; text-decoration:overline">Asst. Manager Audit</div>				
                    	<div style="width:240px;float:left; text-decoration:overline">G.M (Operation)</div>			
                    	<div style="float:240px; text-decoration:overline">Director</div>-->
                    </td>    
                </tr>
	</table>
    

<?
echo signeture_table(8,$rpt_company,"1600px") ;

	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();	
	

}//end if condition types
	
	
if($types=="wages_salary_sheet3") // AJI LTD formate-final automated
{
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];
 
	//salary details 
	$salary_breakdown = array();
	$sql_sal="select d.* from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date'";
	//echo $sql_sal;die;
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}

	
	
	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10)
	{
		$emp_status="and a.emp_status in ( 0 )";
		//$txt_from_date = $previous_month;
		
	}
	//else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and  b.designation_id='$designation_id'";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id='$department_id'";
	if ($payment_method==0) $payment_met =""; else if ($payment_method==1) $payment_met ="and a.emp_code not in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	else if ($payment_method==2) $payment_met ="and a.emp_code in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	if ($company_id==0){ $company="";}else{$company="and a.company_id='$company_id'";}
	if ($emp_code==""){ $emp_code="";}else{$emp_code="and a.emp_code in ($emp_code)";}
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
 	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(b.id_card_no,5)";}	 
	
	$increment_info=array();
	$company_info_sql="select emp_code,old_gross_salary,increment_amount from  hrm_increment_mst where effective_date='$txt_from_date' and is_posted=1 and is_approved=2 and initial=0";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	while($inc = mysql_fetch_array($result))
	{
		$increment_info[$inc[emp_code]]['old']=$inc[old_gross_salary];
		$increment_info[$inc[emp_code]]['inc']=$inc[increment_amount];
	}
	//print_r($increment_info);
	ob_start();
	
				$company_info_sql="select * from lib_company where id='$company_id'";
				$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
				$company_info_result = mysql_fetch_array($result);
	
	
	?>
    
	<style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style>
    
	<table width="1675" cellpadding="0" cellspacing="0" style="font-size:12px; font-family:'Times New Roman', Times, serif" class="rpt_table"  rules="all" ><!--rules="all"-->
		       
        
	<?
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	//sub section 
	$subsection_total_gross =0;	$subsection_total_salary=0;$subsection_att_bonus_total=0;$subsection_total_over_time_total=0;$subsection_buy_total_over_time_amount=0;
	$subsection_act_total_over_time_amount=0;$subsection_total_deduction_amount=0;$subsection_net_payable_amount=0;$subsection_net_payable_amount_bank=0;
	$subsection_net_payable_amount_bank=0;$subsection_total_advaced_amount=0;
	
 	//section 
	$section_total_gross =0;$section_total_salary =0;$section_att_bonus_total =0;$section_total_over_time_total=0;$section_buy_total_over_time_amount=0;
	$section_act_total_over_time_amount=0;$section_total_deduction_amount=0;$section_net_payable_amount=0;$section_net_payable_amount_cash=0;
	$section_net_payable_amount_bank=0;$section_net_payable_amount_bank=0;$section_total_advaced_amount=0;

	//department 
	$department_total_gross =0;$department_total_salary =0;$department_att_bonus_total =0;$department_total_over_time_total=0;$department_buy_total_over_time_amount=0;
	$department_act_total_over_time_amount=0;$department_total_deduction_amount=0;$department_net_payable_amount=0;$department_net_payable_amount_cash=0;
	$department_net_payable_amount_bank=0;$department_net_payable_amount_bank=0;$department_total_advaced_amount=0;
	
	//division 
	$division_total_gross =0;$division_total_salary =0;	$division_att_bonus_total =0;$division_total_over_time_total=0;$division_buy_total_over_time_amount=0;
	$division_act_total_over_time_amount=0;$division_total_deduction_amount=0;$division_net_payable_amount=0;$division_net_payable_amount_cash=0;
	$division_net_payable_amount_bank=0;$division_net_payable_amount_bank=0;$division_total_advaced_amount=0;
	
	//location 
	$location_total_gross =0;$location_total_salary =0;	$location_att_bonus_total =0;$location_total_over_time_total=0;$location_buy_total_over_time_amount=0;
	$location_act_total_over_time_amount=0;	$location_total_deduction_amount=0;$location_net_payable_amount=0;$location_net_payable_amount_cash=0;
	$location_net_payable_amount_bank=0;$location_net_payable_amount_bank=0;$location_total_advaced_amount=0;
	
  	$sl=0;
	$ct=0;
		
	
	$sql_emp = "select a.*, a.id as mst_id, a.gross_salary as sal_gross_salary,a.basic_salary as sal_basic_salary, a.house_rent as sal_house_rent,a.medical_allowance as sal_medical_allowance,b.* 
				from hrm_salary_mst a, hrm_employee b 
				where
				a.emp_code=b.emp_code and 
				a.salary_periods like '$txt_from_date' 
				$emp_code $location_id $section_id $subsection_id $category $payment_met $designation_id $department_id $company $emp_status 
				$orderby";
	
	//echo $sql_emp;die;
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);	
	while ($row_emp = mysql_fetch_array($exe_sql_emp)) // Master Qry starts 
	{
		$sl++;				
		$total_holiday = $row_emp[government_holiday]+$row_emp[festival_holiday]+$row_emp[compensatory_total_holiday];
		
			
		//----------------------------------------------------------------------------------
		if($ct==0)
		{
			$location_arr[$row_emp[location_id]]=$row_emp[location_id];
			$division_arr[$row_emp[division_id]]=$row_emp[division_id];
			$department_arr[$row_emp[department_id]]=$row_emp[department_id];
			$section_arr[$row_emp[section_id]]=$row_emp[section_id];
			$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
		}
		
		
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row_emp[section_id]]=$row_emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row_emp[department_id]]=$row_emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row_emp[section_id]]=$row_emp[section_id];
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row_emp[division_id]]=$row_emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row_emp[department_id]]=$row_emp[department_id];
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row_emp[location_id]]=$row_emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row_emp[division_id]]=$row_emp[division_id];
						$department_arr[$row_emp[department_id]]=$row_emp[department_id];
						$section_arr[$row_emp[section_id]]=$row_emp[section_id];
						$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
		}
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?>
			<tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Total</td>
                    	<td><? echo $subsection_total_gross; ?></td>
                        <td></td>
                        <td><? echo $subsection_total_salary; ?></td>
                      	<td><? echo $subsection_att_bonus_total; ?></td>
                        <td><? echo $subsection_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $subsection_total_punishment_amnt; ?></td> 
                        <td><? echo $subsection_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $subsection_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $subsection_buy_total_over_time_amount; ?></td>
                        <td><? echo $subsection_act_total_over_time_amount; ?></td> 
                        <td><? echo $subsection_total_deduction_amount; ?></td> 
                        <td><? echo $subsection_net_payable_amount; ?></td> 
                        <td><? echo $subsection_net_payable_amount_bank; ?></td>
                        <td><? echo $subsection_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>
				
 			<?
				
			//sub section 
			$subsection_total_gross =0;	$subsection_total_salary=0;$subsection_att_bonus_total=0;$subsection_total_over_time_total=0;$subsection_buy_total_over_time_amount=0;
			$subsection_act_total_over_time_amount=0;$subsection_total_deduction_amount=0;$subsection_net_payable_amount=0;
			$subsection_net_payable_amount_bank=0;$subsection_net_payable_amount_cash=0;$subsection_total_advaced_amount=0;	
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?>
			 <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Section Total</td>
                    	<td><? echo $section_total_gross; ?></td>
                        <td></td>
                        <td><? echo $section_total_salary; ?></td>
                      	<td><? echo $section_att_bonus_total; ?></td>
                        <td><? echo $section_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $section_total_punishment_amnt; ?></td> 
                        <td><? echo $section_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $section_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $section_buy_total_over_time_amount; ?></td>
                        <td><? echo $section_act_total_over_time_amount; ?></td> 
                        <td><? echo $section_total_deduction_amount; ?></td> 
                        <td><? echo $section_net_payable_amount; ?></td> 
                        <td><? echo $section_net_payable_amount_bank; ?></td>
                        <td><? echo $section_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>
   			<?
				
				//section 
				$section_total_gross =0;$section_total_salary =0;$section_att_bonus_total =0;$section_total_over_time_total=0;$section_buy_total_over_time_amount=0;
				$section_act_total_over_time_amount=0;$section_total_deduction_amount=0;$section_net_payable_amount=0;$section_net_payable_amount_cash=0;
				$section_net_payable_amount_bank=0;$section_net_payable_amount_cash=0;$section_total_advaced_amount=0;			
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?>
			<tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Department Total</td>
                    	<td><? echo $department_total_gross; ?></td>
                        <td></td>
                        <td><? echo $department_total_salary; ?></td>
                      	<td><? echo $department_att_bonus_total; ?></td>
                        <td><? echo $department_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $department_total_punishment_amnt; ?></td> 
                        <td><? echo $department_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $department_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $department_buy_total_over_time_amount; ?></td>
                        <td><? echo $department_act_total_over_time_amount; ?></td> 
                        <td><? echo $department_total_deduction_amount; ?></td> 
                        <td><? echo $department_net_payable_amount; ?></td> 
                        <td><? echo $department_net_payable_amount_bank; ?></td>
                        <td><? echo $department_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr> 
             
            
 			<?
				
				//department 
				$department_total_gross =0;$department_total_salary =0;$department_att_bonus_total =0;$department_total_over_time_total=0;$department_buy_total_over_time_amount=0;
				$department_act_total_over_time_amount=0;$department_total_deduction_amount=0;$department_net_payable_amount=0;$department_net_payable_amount_cash=0;
				$department_net_payable_amount_bank=0;$department_net_payable_amount_cash=0;$department_total_advaced_amount=0;
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?>
			 <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Division Total</td>
                    	<td><? echo $division_total_gross; ?></td>
                        <td></td>
                        <td><? echo $division_total_salary; ?></td>
                      	<td><? echo $division_att_bonus_total; ?></td>
                        <td><? echo $division_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $division_total_punishment_amnt; ?></td> 
                        <td><? echo $division_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $division_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $division_buy_total_over_time_amount; ?></td>
                        <td><? echo $division_act_total_over_time_amount; ?></td> 
                        <td><? echo $division_total_deduction_amount; ?></td> 
                        <td><? echo $division_net_payable_amount; ?></td> 
                        <td><? echo $division_net_payable_amount_bank; ?></td>
                        <td><? echo $division_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr> 
            
 			<?
				
				
				//division 
				$division_total_gross =0;$division_total_salary =0;	$division_att_bonus_total =0;$division_total_over_time_total=0;$division_buy_total_over_time_amount=0;
				$division_act_total_over_time_amount=0;$division_total_deduction_amount=0;$division_net_payable_amount=0;$division_net_payable_amount_cash=0;
				$division_net_payable_amount_bank=0;$division_net_payable_amount_cash=0;$division_total_advaced_amount=0;		
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?>
           <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Location Total</td>
                    	<td><? echo $location_total_gross; ?></td>
                        <td></td>
                        <td><? echo $location_total_salary; ?></td>
                      	<td><? echo $location_att_bonus_total; ?></td>
                        <td><? echo $location_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $location_total_punishment_amnt; ?></td> 
                        <td><? echo $location_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $location_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $location_buy_total_over_time_amount; ?></td>
                        <td><? echo $location_act_total_over_time_amount; ?></td> 
                        <td><? echo $location_total_deduction_amount; ?></td> 
                        <td><? echo $location_net_payable_amount; ?></td> 
                        <td><? echo $location_net_payable_amount_bank; ?></td>
                        <td><? echo $location_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>
			<?
				
				//location 
				$location_total_gross =0;$location_total_salary =0;	$location_att_bonus_total =0;$location_total_over_time_total=0;$location_buy_total_over_time_amount=0;
				$location_act_total_over_time_amount=0;	$location_total_deduction_amount=0;$location_net_payable_amount=0;$location_net_payable_amount_cash=0;
				$location_net_payable_amount_bank=0;$location_net_payable_amount_cash=0;$location_total_advaced_amount=0;
							
 		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
			}
		

			if($c_part_1!='')
			{
				$sl=1;
				?><tr><td colspan="26"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}
		
		
		//----------------------------------------------------------------------------------
		// header print here-----------------------		
		if($sl==1 && $ct==0)		
		{				
		?>
        
          <thead>  
                    <tr style="text-align:center; font-weight:bold; border:solid 0 #060;">    	 
                        <th colspan="31" >
                        	<div style="border:0px; font-size:17px"><?	echo "AJI Group";?></div>
                            <div style="border:0px; font-size:14px"><?	echo $company_info_result["company_name"];?></div>
                            <div style="border:0px; font-size:12px; visibility:hidden">
                                Address: <? echo $company_info_result["plot_no"]; ?>, <? echo $company_info_result["level_no"]; ?>, <? echo $company_info_result["block_no"]; ?>, <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                            </div>
                            <div style="border:0px; font-size:12px;">
                                Salary Sheet For The Month Of <? echo $cur_month; ?>-<? echo $cur_year; ?>            	
                            </div>
                         </th>
                    </tr>
                    <tr style="text-align:center; font-weight:bold" height="100">
                        <th width="30">SL</th>
                        <th width="130">Employee</th>
                        <th width="50">AJI#</th>
                         
                        <th width="100">Designation</th>
                        <th width="40"><div class="verticalText">Previous Salary</div></th>
                        <th width="40"><div class="verticalText">Increment Salary</div></th>
                        <th width="50">Salary</th>
                        <th width="25"><div class="verticalText">Attnd. Days</div></th>
                        <th width="40"><div class="verticalText">Earned Salary</div></th>
                        <th width="40"><div class="verticalText">Attnd. Bonus</div></th>
                        <th width="25"><div class="verticalText">Card Pay</div></th>
                        <th width="40"><div class="verticalText">Late Days</div></th>
                        <th width="40"><div class="verticalText">Absent Days</div></th>
                        <th width="40"><div class="verticalText">Leave Days</div></th>
                        <th width="20"><div class="verticalText">Punish Days</div></th>
                        <th width="40"><div class="verticalText">Punish Amount</div></th>
                        <th width="40"><div class="verticalText">Adv. Amount</div></th>
                        <th width="20"><div class="verticalText">Over Time</div></th>
                        <th width="40"><div class="verticalText">Extra OT</div></th>
                        <th width="20"><div class="verticalText">Total OT</div></th>
                        <th width="40"><div class="verticalText">OT Rate</div></th>
                        <th width="40"><div class="verticalText">OT Amount</div></th>
                        <th width="40"><div class="verticalText">Extra OT Amount</div></th>
                        <th width="50"><div class="verticalText">Deduction</div></th>
                        <th width="60"><div class="verticalText">Net Payment</div></th>
                        <th width="60">Bank</th>
                        <th width="60">Cash</th>
                        <th width="100">Details</th>
                        <th width="100">Signature</th>
                        <th><div class="verticalText">Remarks</div></th>
                     </tr>      
                </thead>
        	<?    	
			
		}
		  
			// Details
			$string="";
			$att_bonus="";
			$sql_attnd =" select sum(CASE WHEN salary_head_id =28 THEN pay_amount END) AS stamp, sum(CASE WHEN salary_head_id =29 THEN pay_amount END) AS lwp_amnt, sum(CASE WHEN salary_head_id =31 THEN pay_amount END) AS others, sum(CASE WHEN salary_head_id =23 THEN pay_amount END) AS late, sum(CASE WHEN salary_head_id =24 THEN pay_amount END) AS absent , sum(CASE WHEN salary_head_id =35 THEN pay_amount END) AS att_bonus,sum(CASE WHEN salary_head_id =36 THEN pay_amount END) AS cp_charge,sum(CASE WHEN salary_head_id =37 THEN pay_amount END) AS punishment_amnt from hrm_salary_dtls where salary_mst_id=$row_emp[mst_id] order by salary_head_id";
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				if ($row_earn[absent]>0) $string .="Deduct absent amount ".round($row_earn[absent]).", ";   
				if ($row_earn[late]>0) $string .="Deduct late amount ".round($row_earn[late]).", ";
				if ($row_earn[lwp_amnt]>0) $string .="Deduct lwp amount ".round($row_earn[lwp_amnt]).", ";
				if ($row_earn[others]>0) $string .="Deduct others amount ".round($row_earn[others]).", ";
				if ($row_earn[cp_charge]>0) $string .="Deduct card amount ".round($row_earn[cp_charge]).", ";
				if ($row_earn[punishment_amnt]>0) $string .="Deduct punishment amount ".round($row_earn[punishment_amnt]).", ";				
				
				if ($string!="") $string .=" and "; 
				if ($row_earn[stamp]>0) $string .="Deduct stamp charge ".round($row_earn[stamp]).". ";
				$att_bonus=round($row_earn[att_bonus]);   
				$cp_charge=round($row_earn[cp_charge]);
				$punishment_amnt=round($row_earn[punishment_amnt]);
				$absent_amnt=$row_earn[absent];
				//$row_emp[sal_gross_salary]=$row_emp[sal_gross_salary]-$row_earn[absent];
				 
			}
			
			$ct++;
			if ($ct%2==0) $bgcolor="#EFEFEF"; 
			else $bgcolor="#FFFFFF";
			
			
			
			$row_emp[net_payable_amount]=$row_emp[net_payable_amount]-round($row_emp[e_over_time]*$row_emp[over_time_rate]);
			
			if ($row_emp[net_payable_amount]>$row_emp[bank_gross])  $row_emp[bank_gross]=round($row_emp[bank_gross]); else $row_emp[bank_gross]=round($row_emp[net_payable_amount]);
			if ($row_emp[net_payable_amount]>$row_emp[bank_gross]) $cash_payable= round($row_emp[net_payable_amount]-$row_emp[bank_gross]); else $cash_payable=0;  // Cash
			
			
			//payment days
			$pay_days = $row_emp[total_present_days]+$row_emp[total_weekly_holiday]+$total_holiday+$row_emp[total_late_days];
			//earned salary
			$earned_salary = round($row_emp[sal_gross_salary]-(round($row_emp[not_payble_days]*$row_emp[daily_gross_salary])+$absent_amnt));
			//total deduction amount
			$deduct_amount = round($row_emp[total_deduction_amount]-$absent_amnt);
	?>	
		
		<tbody>	    	
    		<tr height="94" style="font-size:13px;" bgcolor="<? echo $bgcolor; ?>" >
        		<td align="center" valign="middle"><? echo $sl; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[id_card_no]; ?></td>
                 
                <td align="center" valign="middle"><? echo $designation_chart[$row_emp[designation_id]]; ?></td>
                <td align="right" valign="middle"><? echo $increment_info[$row_emp[emp_code]]['old']; ?></td>
                <td align="right" valign="middle"><? echo $increment_info[$row_emp[emp_code]]['inc']; ?></td>
                <td align="right" valign="middle"><? echo round($row_emp[sal_gross_salary]); ?></td>
				<td align="center" valign="middle"><? echo $pay_days; ?></td>
				<td align="right" valign="middle"><? echo $earned_salary; ?></td> 
                <td align="right" valign="middle"><? echo round($att_bonus); ?></td> 
                <td align="right" valign="middle"><? echo $cp_charge; //Card Amount ?></td>  
                <td align="center" valign="middle"><? echo round($row_emp[total_late_days]); ?></td> 
                <td align="center" valign="middle"><? echo round($row_emp[total_abs_days]); ?></td>
                <td align="center" valign="middle"><? echo round($row_emp[casual_leave]+$row_emp[sick_leave]+$row_emp[earn_leave]+$row_emp[maternity_leave]+$row_emp[faternity_leave]+$row_emp[special_leave]); ?></td>
                <td align="right" valign="middle"><? echo ""; // Punshment days ?></td>
                <td align="right" valign="middle"><? echo $punishment_amnt; // Punshment Amount ?></td>
                <td align="right" valign="middle"><? echo round($row_emp[advance_deduction]); // Ad---  Unknown ?></td> 
                <td align="center" valign="middle"><? echo $row_emp[b_over_time]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[e_over_time]; // EOT ?></td>
                <td align="center" valign="middle"><? echo $row_emp[total_over_time]; ?></td>
                <td align="center" valign="middle"><? if($row_emp[total_over_time]==0)echo"0";else echo $row_emp[over_time_rate]; ?></td>
                <td align="right" valign="middle"><? echo round($row_emp[b_over_time]*$row_emp[over_time_rate]); ?></td>
                <td align="right" valign="middle"><? echo round($row_emp[e_over_time]*$row_emp[over_time_rate]); ?></td>
                
                <td align="right" valign="middle"><? echo $deduct_amount; ?></td>
                <td align="right" valign="middle"><? echo round($row_emp[net_payable_amount]); ?></td>
                <td align="right" valign="middle"><? echo $row_emp[bank_gross]; // bank ?></td>
                <td align="right" valign="middle"><? echo $cash_payable; // Cash ?></td>
                <td align="center" valign="middle" style="font-size:9px">
					<?
						echo $string;  
					?></td>
                <td align="center" valign="middle"><? echo "";// Sign ?></td>
                <td align="center" valign="middle"><? if ($row_emp[emp_status]==1) echo "New"; else if ($row_emp[emp_status]==2) echo "Resign"; else echo ""; // Rem ?></td>
    		</tr>
        </tbody>    
    <?
	
	
				$ct++;
				//sub section 
				$subsection_total_gross += round($row_emp[sal_gross_salary]);
				$subsection_total_salary += round($earned_salary);				
				$subsection_att_bonus_total += round($att_bonus);
				$subsection_cp_charge_amount += round($cp_charge);
				$subsection_total_punishment_amnt+=round($row_emp[punishment_amnt]); 
				$subsection_total_advaced_amount+=round($row_emp[advance_deduction]);
 				$subsection_total_over_time_total+=round($row_emp[total_over_time]);
				$subsection_buy_total_over_time_amount+=round($row_emp[b_over_time]*$row_emp[over_time_rate]);
				$subsection_act_total_over_time_amount+=round($row_emp[e_over_time]*$row_emp[over_time_rate]);
				$subsection_total_deduction_amount+=round($deduct_amount);
				$subsection_net_payable_amount+=round($row_emp[net_payable_amount]);
				$subsection_net_payable_amount_bank+=round($row_emp[bank_gross]); //bank
				$subsection_net_payable_amount_cash+=round($cash_payable); //cash
				
 				//section 
				$section_total_gross += round($row_emp[sal_gross_salary]);
				$section_total_salary += round($earned_salary);				
				$section_att_bonus_total += round($att_bonus);
				$section_cp_charge_amount += round($cp_charge);
				$section_total_punishment_amnt+=round($row_emp[punishment_amnt]);
				$section_total_advaced_amount+=round($row_emp[advance_deduction]);
 				$section_total_over_time_total+=round($row_emp[total_over_time]);
				$section_buy_total_over_time_amount+=round($row_emp[b_over_time]*$row_emp[over_time_rate]);
				$section_act_total_over_time_amount+=round($row_emp[e_over_time]*$row_emp[over_time_rate]);
				$section_total_deduction_amount+=round($deduct_amount);
				$section_net_payable_amount+=round($row_emp[net_payable_amount]);
				$section_net_payable_amount_bank+=round($row_emp[bank_gross]); //bank
				$section_net_payable_amount_cash+=round($cash_payable); //cash
				
				//department 
				$department_total_gross += round($row_emp[sal_gross_salary]);
				$department_total_salary += round($earned_salary);				
				$department_att_bonus_total += round($att_bonus);
				$department_cp_charge_amount += round($cp_charge);
				$department_total_punishment_amnt+=round($row_emp[punishment_amnt]);
				$department_total_advaced_amount+=round($row_emp[advance_deduction]);
 				$department_total_over_time_total+=round($row_emp[total_over_time]);
				$department_buy_total_over_time_amount+=round($row_emp[b_over_time]*$row_emp[over_time_rate]);
				$department_act_total_over_time_amount+=round($row_emp[e_over_time]*$row_emp[over_time_rate]);
				$department_total_deduction_amount+=round($deduct_amount);
				$department_net_payable_amount+=round($row_emp[net_payable_amount]);
				$department_net_payable_amount_bank+=round($row_emp[bank_gross]); //bank
				$department_net_payable_amount_cash+=round($cash_payable); //cash
				
				//division 
				$division_total_gross += round($row_emp[sal_gross_salary]);
				$division_total_salary += round($earned_salary);				
				$division_att_bonus_total += round($att_bonus);
				$division_cp_charge_amount += round($cp_charge);
				$division_total_punishment_amnt+=round($row_emp[punishment_amnt]);
				$division_total_advaced_amount+=round($row_emp[advance_deduction]);
 				$division_total_over_time_total+=round($row_emp[total_over_time]);
				$division_buy_total_over_time_amount+=round($row_emp[b_over_time]*$row_emp[over_time_rate]);
				$division_act_total_over_time_amount+=round($row_emp[e_over_time]*$row_emp[over_time_rate]);
				$division_total_deduction_amount+=round($deduct_amount);
				$division_net_payable_amount+=round($row_emp[net_payable_amount]);
				$division_net_payable_amount_bank+=round($row_emp[bank_gross]); //bank
				$division_net_payable_amount_cash+=round($cash_payable); //cash
				
				//location 
				$location_total_gross += round($row_emp[sal_gross_salary]);
				$location_total_salary += round($earned_salary);				
				$location_att_bonus_total += round($att_bonus);
				$location_cp_charge_amount += round($cp_charge);
				$location_total_punishment_amnt+=round($row_emp[punishment_amnt]);
				$location_total_advaced_amount+=round($row_emp[advance_deduction]);
 				$location_total_over_time_total+=round($row_emp[total_over_time]);
				$location_buy_total_over_time_amount+=round($row_emp[b_over_time]*$row_emp[over_time_rate]);
				$location_act_total_over_time_amount+=round($row_emp[e_over_time]*$row_emp[over_time_rate]);
				$location_total_deduction_amount+=round($deduct_amount);
				$location_net_payable_amount+=round($row_emp[net_payable_amount]);
				$location_net_payable_amount_bank+=round($row_emp[bank_gross]); //bank
				$location_net_payable_amount_cash+=round($cash_payable); //cash
				
				//grand total
				$grand_total_gross += round($row_emp[sal_gross_salary]);
				$grand_total_salary += round($earned_salary);				
				$grand_att_bonus_total += round($att_bonus);
				$grand_cp_charge_amount += round($cp_charge);
				$grand_total_punishment_amnt+=round($row_emp[punishment_amnt]);
				$grand_total_advaced_amount+=round($row_emp[advance_deduction]);
 				$grand_total_over_time_total+=round($row_emp[total_over_time]);
 				$grand_buy_total_over_time_amount+=round($row_emp[b_over_time]*$row_emp[over_time_rate]);
				$grand_act_total_over_time_amount+=round($row_emp[e_over_time]*$row_emp[over_time_rate]);
				$grand_total_deduction_amount+=round($deduct_amount);
				$grand_net_payable_amount+=round($row_emp[net_payable_amount]);
				$grand_net_payable_amount_bank+=round($row_emp[bank_gross]); //bank
				$grand_net_payable_amount_cash+=round($cash_payable); //cash
				
				
 		}// while loop end	   
	 ?>
		
     		
        <?  if($status_subsec==1){	?>
                <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Total</td>
                    	<td><? echo $subsection_total_gross; ?></td>
                        <td></td>
                        <td><? echo $subsection_total_salary; ?></td>
                      	<td><? echo $subsection_att_bonus_total; ?></td>
                        <td><? echo $subsection_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $subsection_total_punishment_amnt; ?></td> 
                        <td><? echo $subsection_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $subsection_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $subsection_buy_total_over_time_amount; ?></td>
                        <td><? echo $subsection_act_total_over_time_amount; ?></td> 
                        <td><? echo $subsection_total_deduction_amount; ?></td> 
                        <td><? echo $subsection_net_payable_amount; ?></td> 
                        <td><? echo $subsection_net_payable_amount_bank; ?></td>
                        <td><? echo $subsection_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>
                
         <? } ?>
       
        <?  if($status_sec==1){	?>
 				<tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Section Total</td>
                    	<td><? echo $section_total_gross; ?></td>
                        <td></td>
                        <td><? echo $section_total_salary; ?></td>
                      	<td><? echo $section_att_bonus_total; ?></td>
                        <td><? echo $section_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $section_total_punishment_amnt; ?></td> 
                        <td><? echo $section_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $section_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $section_buy_total_over_time_amount; ?></td>
                        <td><? echo $section_act_total_over_time_amount; ?></td> 
                        <td><? echo $section_total_deduction_amount; ?></td> 
                        <td><? echo $section_net_payable_amount; ?></td> 
                        <td><? echo $section_net_payable_amount_bank; ?></td>
                        <td><? echo $section_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>
         <? } ?>
                
        <?  if($status_dept==1){	?>
               <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Department Total</td>
                    	<td><? echo $department_total_gross; ?></td>
                        <td></td>
                        <td><? echo $department_total_salary; ?></td>
                      	<td><? echo $department_att_bonus_total; ?></td>
                        <td><? echo $department_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $department_total_punishment_amnt; ?></td> 
                        <td><? echo $department_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $department_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $department_buy_total_over_time_amount; ?></td>
                        <td><? echo $department_act_total_over_time_amount; ?></td> 
                        <td><? echo $department_total_deduction_amount; ?></td> 
                        <td><? echo $department_net_payable_amount; ?></td> 
                        <td><? echo $department_net_payable_amount_bank; ?></td>
                        <td><? echo $department_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>  
         <? } ?>
                
        <?  if($status_divis==1){	?>
               <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Division Total</td>
                    	<td><? echo $division_total_gross; ?></td>
                        <td></td>
                        <td><? echo $division_total_salary; ?></td>
                      	<td><? echo $division_att_bonus_total; ?></td>
                        <td><? echo $division_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $division_total_punishment_amnt; ?></td> 
                        <td><? echo $division_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $division_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $division_buy_total_over_time_amount; ?></td>
                        <td><? echo $division_act_total_over_time_amount; ?></td> 
                        <td><? echo $division_total_deduction_amount; ?></td> 
                        <td><? echo $division_net_payable_amount; ?></td> 
                        <td><? echo $division_net_payable_amount_bank; ?></td>
                        <td><? echo $division_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>  
         <? } ?>
                  
        <?  if($status_loc==1){	?>
                <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Location Total</td>
                    	<td><? echo $location_total_gross; ?></td>
                        <td></td>
                        <td><? echo $location_total_salary; ?></td>
                      	<td><? echo $location_att_bonus_total; ?></td>
                        <td><? echo $location_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $location_total_punishment_amnt; ?></td> 
                        <td><? echo $location_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $location_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $location_buy_total_over_time_amount; ?></td>
                        <td><? echo $location_act_total_over_time_amount; ?></td> 
                        <td><? echo $location_total_deduction_amount; ?></td> 
                        <td><? echo $location_net_payable_amount; ?></td> 
                        <td><? echo $location_net_payable_amount_bank; ?></td>
                        <td><? echo $location_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>  
         <? } ?>
                
                 <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold">
                		<td colspan="6">&nbsp;Company Total</td>
                    	<td><? echo $grand_total_gross; ?></td>
                        <td></td>
                        <td><? echo $grand_total_salary; ?></td>
                      	<td><? echo $grand_att_bonus_total; ?></td>
                        <td><? echo $grand_cp_charge_amount; ?></td>
                        <td colspan="4"></td> 
                        <td><? echo $grand_total_punishment_amnt; ?></td> 
                        <td><? echo $grand_total_advaced_amount; ?></td>                      
                         <td colspan="2"></td>
                        <td><? echo $grand_total_over_time_total; ?></td>
                        <td></td>
                        <td><? echo $grand_buy_total_over_time_amount; ?></td>
                        <td><? echo $grand_act_total_over_time_amount; ?></td> 
                        <td><? echo $grand_total_deduction_amount; ?></td> 
                        <td><? echo $grand_net_payable_amount; ?></td> 
                        <td><? echo $grand_net_payable_amount_bank; ?></td>
                        <td><? echo $grand_net_payable_amount_cash; ?></td>
                        <td colspan="3"> </td> 
            	</tr>  
          
    			<tr height="130" valign="bottom">
                	<td colspan="31" align="center">
                		<div style="width:240px;float:left; text-decoration:overline">Prepared By</div>
                        <div style="width:240px;float:left; text-decoration:overline">Officer(P&C)</div>
                    	<div style="width:240px;float:left; text-decoration:overline">Manager(HR)</div>				
                    	<div style="width:240px;float:left; text-decoration:overline">Executive(Accounts)</div>						
                    	<div style="width:240px;float:left; text-decoration:overline">Asst. Manager Audit</div>				
                    	<div style="width:240px;float:left; text-decoration:overline">G.M (Operation)</div>			
                    	<div style="float:240px; text-decoration:overline">Director</div>
                    </td>    
                </tr>
	</table>
    

<?


	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();	
	

}//end if condition types


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
}

function signeture_table_peak($report_id,$company,$width)
{
	if ($width=="") $width="100%"; 
	$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company");
	$signeture_list = explode('#',$signeture_allow_or_no);
	if($signeture_allow_or_no!= "")	
	{
	 echo '<table width="'.$width.'">
		<tr height="130" style="font-size:14px; font-family:Arial; font-weight:bold">
		 ';
		foreach($signeture_list as $key=>$value)
		{
			echo '<td align="center" valign="bottom" style="border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;border-bottom-style:hidden;">'.$value.'</td> ';
		}
		echo '</tr></table>';
	}
	else
	{
		echo '<table width="'.$width.'">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 ';
		 
		echo '</tr></table>';
	}
}

?>