<?
/******************************
Completed by:Sohel
Date:19-09-2015
******************************/

session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM  lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}	
	
	//designation details
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}	
	
	//level
	$sql = "SELECT * FROM lib_designation";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart_level = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart_level[$row['level']] = mysql_real_escape_string($row['system_designation']);
	}	
	
	//deduction head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (1,2) and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$deduction_head = array();
	$deduction_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{		
		$deduction_head[] = mysql_real_escape_string( $row['custom_head'] );
		$deduction_head_id[] = mysql_real_escape_string( $row['id'] );		
	}
	//print_r($deduction_head_id);die;
	
	
	// Variable Settiings
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['not_pay_deduction'] = mysql_real_escape_string( $row['not_pay_deduction'] );
		$var_hrm_chart[$row['company_name']]['new_emp_card_charge'] = mysql_real_escape_string( $row['new_emp_card_charge'] );
		$var_hrm_chart[$row['company_name']]['ot_rate_multiplier'] = mysql_real_escape_string( $row['ot_rate_multiplier'] );
		$var_hrm_chart[$row['company_name']]['att_bonus_leave_type'] = mysql_real_escape_string( $row['att_bonus_leave_type'] ); 	 	 	
	}
	
	extract($_REQUEST);
	
	if($type=="select_month_generate")//month generated
	{		
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}

if($action=="top_sheet")
{
	ob_start();
	
	if($group_by_id==0){$orderby_fild="a.department_id"; $header_field="Department"; $orderby_bu="department_id";}
	else if($group_by_id==1){$orderby_fild="a.company_id"; $header_field="Company"; $orderby_bu="company_id";}
	else if($group_by_id==2){$orderby_fild="a.location_id"; $header_field="Location"; $orderby_bu="location_id";}
	else if($group_by_id==3){$orderby_fild="a.division_id"; $header_field="Division"; $orderby_bu="division_id";}
	else if($group_by_id==4){$orderby_fild="a.department_id"; $header_field="Department"; $orderby_bu="department_id";}
	else if($group_by_id==5){$orderby_fild="a.section_id"; $header_field="Section"; $orderby_bu="section_id";}
	else if($group_by_id==6){$orderby_fild="a.subsection_id"; $header_field="Subsection"; $orderby_bu="subsection_id";} 
	else {$group_by_id="a.department_id"; $header_field="Department"; $orderby_bu="department_id";}
	//echo $orderby_bu."===";
	$company_name=$company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
	if ($company_id==0) $company=''; else $company=" and a.company_id='$company_id'";
	if ($location_id==0) $location=""; else $location="and a.location_id='$location_id'";
	if ($division_id==0) $division=""; else $division="and a.division_id  in($division_id)";
	if ($department_id==0) $department=""; else	$department="and a.department_id in($department_id)";
	if ($section_id==0 ) $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ($cbo_salary_sheet=='') {$emp_status="";} else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} else $emp_status="and a.emp_status='$cbo_salary_sheet'";


	$starting_date = explode('_', $cbo_month_selector ); 
	$salary_date = $starting_date[0];
	$exp_month=explode("-",$salary_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	//echo $cur_month;die;
	$i=0;	
	$salary_date_previous=add_date($salary_date,-1);
	$exp_month_previous=explode("-",$salary_date_previous);
	$previous_month = $months["'"+$exp_month_previous[1]+"'"];
	$pre_sal_date=$exp_month_previous[0]."-".$exp_month_previous[1]."-"."01";
	
	//echo $group_by_id;die;
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="$orderby_fild,";}
	else if($group_by_id==1){$status_com=1;$groupby="$orderby_fild,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,$orderby_fild,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,$orderby_fild,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,$orderby_fild,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,$orderby_fild,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,$orderby_fild,";}
	
	$dynamic_groupby = substr($groupby, 0, -1);

	$sl=0;
	$i=0;
	
	$sql="select emp_code,over_time_rate,salary_periods,total_over_time,e_over_time from hrm_salary_mst where salary_periods in ('$salary_date','$pre_sal_date') order by emp_code";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$ot_rate_emp_wise = array();
	$total_overt_times=array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$ot_rate_emp_wise[$row['emp_code']][$row['salary_periods']]=$row['over_time_rate'];
		$total_overt_times[$row['emp_code']][$row['salary_periods']]=$row['e_over_time'];
	}	

	$grandtotal_c_employee=0;$grandtotal_g_salary=0;$late_abs_np_deduct_amount=0;$grand_total_earning_amount=0;$grandtotal_att_bonus_amount=0;$grandtotal_b_buyer_ot=0;$grandtotal_b_extra_ot=0;$grandtotal_over_time_amount=0;$grandtotal_salary_amount=0;$grandtotal_advance_amount=0;$grandtotal_stamp_amount=0;$grandtotal_others_deduction_amount=0;$grandtotal_saving_fund=0;$grandtotal_total_deduction_amount=0;$grandtotal_net_payable_amount=0;
	
	$grandtotal_pre_c_employee=0;$grandtotal_pre_g_salary=0;$pre_late_abs_np_deduct_amount=0;$grand_total_pre_earning_amount=0;$grand_total_pre_att_bonus_amount=0;$grandtotal_pre_b_buyer_ot=0; $grandtotal_pre_b_extra_ot=0; $grandtotal_pre_total_overtime_amount=0; $grandtotal_pre_salary_amount=0; $grandtotal_pre_advance_amount=0;$grandtotal_pre_stamp=0; $grandtotal_pre_others_deduction_amount=0; $grandtotal_pre_saving_fund_amount=0; $grandtotal_pre_total_deduction_amount=0; $grandtotal_pre_net_payable_amount=0;

?>
    <table width="1210" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
		<thead>
            	<?
                $company_info_sql="select id,company_name,email,website, plot_no, level_no, road_no,block_no,zip_code,city from lib_company where id='$company_name'";
				$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
				$company_info_result = mysql_fetch_array($result);
				?>
            <tr style="text-align:center; font-weight:bold">    	 
                <th colspan="18">
                <div style="font-size:13px"><?	echo $company_info_result["company_name"];?></div>
                <div style="font-size:10px">Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                Salary Top Sheet For The Month Of <? echo $cur_month; ?> <? echo $exp_month[0]; ?>
                </div>
                </th>
            </tr>
            <tr>
                <th width="30" rowspan="2">SL</th>
                <th width="160" colspan="2" rowspan="2"><? echo $header_field; ?></th>
                <th width="60" rowspan="2">Manpower</th>
                <th width="80" rowspan="2">Gross Salary</th>
                <th width="80" rowspan="2">Late Present <br /> ABS Amount <br /> N P Amount</th>
                <th width="80" rowspan="2">Attendance Salary</th>
                <th width="60" rowspan="2">Attendance Bonus</th>
                <th width="60" rowspan="2">OT Amount</th>
                <th width="60" rowspan="2">Extra OT</th>
                <th width="80" rowspan="2">Total OT Amount</th>
                <th width="80" rowspan="2">Total Salary</th>
                <th width="240" colspan="4">Deduction</th>
                <th width="60" rowspan="2">Total Deduction</th>
                <th width="80" rowspan="2">NET PAYABLE AMOUNT</th>
            </tr>
             <tr style="text-align:center; font-weight:bold">
                <th width="60">Adv. Salary</th>
                <th width="60">Stamp</th>
                <th width="60">Other Deduction</th>
                <th width="60">Saving Fund</th>
            </tr>	
        </thead>
        <tbody>
<? 

	$sql = "SET @@group_concat_max_len = 9999999";
	$r = @mysql_query ($sql);
	
	$sql_salary ="select group_concat(CAST(a.id as CHAR)) as mst_id,a.company_id,a.location_id,a.division_id,
	a.department_id,a.section_id,a.subsection_id, a.designation_id,a.employee_category,b.category,b.designation_level,a.emp_status, b.designation_level, 
		count(a.id) as count_employee,
		sum(CASE WHEN a.salary_periods='$salary_date' THEN 1 else 0 END ) as c_employee,
		sum(CASE WHEN a.salary_periods='$pre_sal_date' THEN 1 else 0 END ) as pre_c_employee,
		SUM(CASE WHEN a.salary_periods='$salary_date' THEN a.gross_salary else 0 END ) as g_salary,
		SUM(CASE WHEN a.salary_periods='$pre_sal_date' THEN a.gross_salary else 0 END ) as pre_g_salary,
		SUM(CASE WHEN a.salary_periods='$salary_date' THEN a.not_payable_deduction_amount else 0 END ) as not_payable_deduction_amount,
		SUM(CASE WHEN a.salary_periods='$salary_date' THEN a.b_total_late_deduction_amount else 0 END ) as total_late_deduction_amount,
		SUM(CASE WHEN a.salary_periods='$salary_date' THEN a.total_lwp_deduction_amount else 0 END ) as total_lwp_deduction_amount,
		SUM(CASE WHEN a.salary_periods='$pre_sal_date' THEN a.not_payable_deduction_amount else 0 END ) as pre_not_payable_deduction_amount,
		SUM(CASE WHEN a.salary_periods='$pre_sal_date' THEN a.b_total_late_deduction_amount else 0 END ) as pre_total_late_deduction_amount,
		SUM(CASE WHEN a.salary_periods='$pre_sal_date' THEN a.total_lwp_deduction_amount else 0 END ) as pre_total_lwp_deduction_amount,
		SUM(CASE WHEN a.salary_periods='$salary_date' THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as b_buyer_ot,
		SUM(CASE WHEN a.salary_periods='$pre_sal_date' THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as pre_b_buyer_ot,
		SUM(CASE WHEN a.salary_periods='$salary_date' THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as b_extra_ot,
		SUM(CASE WHEN a.salary_periods='$pre_sal_date' THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as pre_b_extra_ot,
		SUM(CASE WHEN a.salary_periods='$salary_date' THEN a.net_payable_amount else 0 END ) as net_payable_amount,
		SUM(CASE WHEN a.salary_periods='$pre_sal_date' THEN a.net_payable_amount else 0 END ) as pre_net_payable_amount
		from hrm_salary_mst a, hrm_employee b where a.emp_code=b.emp_code and a.salary_periods in ('$salary_date','$pre_sal_date') 
		$emp_status $category $company $location $division $department $section_id $subsection_id
		group by a.employee_category,$dynamic_groupby having c_employee>0 order by a.employee_category,$dynamic_groupby";	
	//die;
	$sql_salary_result = mysql_query($sql_salary) or die(mysql_error());   
	$c=0;
	$result_row_arr=array();
	//$deduction=0;
	while($row_cat = mysql_fetch_array($sql_salary_result))
	{
		$c++;
		//echo $row_cat[mst_id];
		$dtls_sql = "select  group_concat(a.emp_code) as mst_emp_code,a.employee_category,
			SUM(CASE WHEN dtls.salary_head_id=35 and a.salary_periods='$salary_date' THEN (dtls.pay_amount) else 0 END ) as att_bonus_amount,
			SUM(CASE WHEN dtls.salary_head_id=35 and a.salary_periods='$pre_sal_date' THEN (dtls.pay_amount) else 0 END ) as pre_att_bonus_amount,
			SUM(CASE WHEN dtls.salary_head_id=28 and a.salary_periods='$salary_date' THEN (dtls.pay_amount) else 0 END ) as stamp,
			SUM(CASE WHEN dtls.salary_head_id=28 and a.salary_periods='$pre_sal_date' THEN (dtls.pay_amount) else 0 END ) as pre_stamp,
			SUM(CASE WHEN dtls.salary_head_id=21 and a.salary_periods='$salary_date' THEN (dtls.pay_amount) else 0 END ) as advance_amount,
			SUM(CASE WHEN dtls.salary_head_id=21 and a.salary_periods='$pre_sal_date' THEN (dtls.pay_amount) else 0 END ) as pre_advance_amount,
			SUM(CASE WHEN dtls.salary_head_id=25 and a.salary_periods='$salary_date' THEN (dtls.pay_amount) else 0 END ) as saving_fund,
			SUM(CASE WHEN dtls.salary_head_id=25 and a.salary_periods='$pre_sal_date' THEN (dtls.pay_amount) else 0 END ) as pre_saving_fund,
			SUM(CASE WHEN dtls.salary_head_id=31 and a.salary_periods='$salary_date' THEN (dtls.pay_amount) else 0 END ) as others_deduction_amount,
			SUM(CASE WHEN dtls.salary_head_id=31 and a.salary_periods='$pre_sal_date' THEN (dtls.pay_amount) else 0 END ) as pre_others_deduction_amount,
			SUM(CASE WHEN dtls.salary_head_id=24 and a.salary_periods='$salary_date' THEN (dtls.pay_amount) else 0 END ) as total_absent_deduction_amount,
			SUM(CASE WHEN dtls.salary_head_id=24 and a.salary_periods='$pre_sal_date' THEN (dtls.pay_amount) else 0 END ) as pre_total_absent_deduction_amount,
			SUM(CASE WHEN dtls.salary_head_id=45 and a.salary_periods='$salary_date' THEN (dtls.pay_amount) else 0 END ) as dormitory_deduction
		from 
		hrm_salary_mst a, hrm_salary_dtls dtls 
		where 
		a.id=dtls.salary_mst_id and a.salary_periods in ('$salary_date','$pre_sal_date') and a.id in ($row_cat[mst_id])  group by a.employee_category,$dynamic_groupby order by a.employee_category,$dynamic_groupby";
		$dtls_sql_result = mysql_query($dtls_sql) or die(mysql_error());   
		$result_dtls = mysql_fetch_array($dtls_sql_result);
		
		$mst_emp_code=array_unique(explode(",",$result_dtls[mst_emp_code]));
		$total_eot_amt=0;
		$pre_total_eot_amt=0;
		foreach($mst_emp_code as $emp_code)
		{
			$splt_total_ot=explode(".",$total_overt_times[$emp_code][$salary_date]);
			$pre_splt_total_ot=explode(".",$total_overt_times[$emp_code][$pre_sal_date]);
			
			$total_ot_min=(($splt_total_ot[0]*60)+$splt_total_ot[1]);
			$ot_rate=$ot_rate_emp_wise[$emp_code][$salary_date];
			$ot_rate_mnt=$ot_rate/60;
			
			$pre_total_ot_min=(($pre_splt_total_ot[0]*60)+$pre_splt_total_ot[1]);
			$pre_ot_rate=$ot_rate_emp_wise[$emp_code][$pre_sal_date];
			$pre_ot_rate_mnt=$pre_ot_rate/60;
			
			$ot_amt=($splt_total_ot[0]*$ot_rate)+($splt_total_ot[1]*$ot_rate_mnt);
			$pre_ot_amt=($pre_splt_total_ot[0]*$pre_ot_rate)+($pre_splt_total_ot[1]*$pre_ot_rate_mnt);
			
			$total_eot_amt+=$ot_amt;
			$pre_total_eot_amt+=$pre_ot_amt;
		}
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][c_employee]+= $row_cat[c_employee];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_c_employee]+= $row_cat[pre_c_employee];
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][g_salary]+= $row_cat[g_salary];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_g_salary]+= $row_cat[pre_g_salary];
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][not_payable_deduction_amount]+= $row_cat[not_payable_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][total_late_deduction_amount]+= $row_cat[total_late_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][total_absent_deduction_amount]+= $result_dtls[total_absent_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][total_lwp_deduction_amount]+= $row_cat[total_lwp_deduction_amount];
		
		$deduction=($row_cat[not_payable_deduction_amount]+$row_cat[total_late_deduction_amount]+$result_dtls[total_absent_deduction_amount]+$row_cat[total_lwp_deduction_amount]);
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][total_earning_amount]+= ($row_cat[g_salary]-$deduction);
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_not_payable_deduction_amount]+= $row_cat[pre_not_payable_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_total_late_deduction_amount]+= $row_cat[pre_total_late_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_total_absent_deduction_amount]+= $result_dtls[pre_total_absent_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_total_lwp_deduction_amount]+= $row_cat[pre_total_lwp_deduction_amount];
		$pre_deduction+=($row_cat[pre_not_payable_deduction_amount]+$row_cat[pre_total_late_deduction_amount]+$result_dtls[pre_total_absent_deduction_amount]+$row_cat[pre_total_lwp_deduction_amount]);
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_total_earning_amount]+= ($row_cat[pre_g_salary]-$pre_deduction);
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][att_bonus_amount]+= $result_dtls[att_bonus_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_att_bonus_amount]+= $result_dtls[pre_att_bonus_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][b_buyer_ot]+= $row_cat[b_buyer_ot];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_b_buyer_ot]+= $row_cat[pre_b_buyer_ot];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][b_extra_ot]+= $total_eot_amt; //$row_cat[b_extra_ot];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_b_extra_ot]+= $pre_total_eot_amt; //$row_cat[pre_b_extra_ot];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][total_overtime_amount]+= ($row_cat[b_buyer_ot]+$total_eot_amt);
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_total_overtime_amount]+= ($row_cat[pre_b_buyer_ot]+$pre_total_eot_amt);
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][total_salary_amount]+= ($row_cat[g_salary]+$result_dtls[att_bonus_amount]+$row_cat[b_buyer_ot]+$row_cat[b_extra_ot])-$deduction;
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_total_salary_amount]+= ($row_cat[pre_g_salary]+$result_dtls[pre_att_bonus_amount]+$row_cat[pre_b_buyer_ot]+$row_cat[pre_b_extra_ot])-$pre_deduction;
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][advance_amount]+= $result_dtls[advance_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_advance_amount]+= $result_dtls[pre_advance_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][stamp]+= $result_dtls[stamp];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_stamp]+= $result_dtls[pre_stamp];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][others_deduction_amount]+= $result_dtls[others_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_others_deduction_amount]+= $result_dtls[pre_others_deduction_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][saving_fund]+= $result_dtls[saving_fund];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_saving_fund]+= $result_dtls[pre_saving_fund];
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][total_deduction_amount]= ($result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][advance_amount]+$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][stamp]+$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][others_deduction_amount]+$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][saving_fund]);
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_total_deduction_amount]= ($result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_advance_amount]+$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_stamp]+$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_others_deduction_amount]+$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_saving_fund]);
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][net_payable_amount]+= $row_cat[net_payable_amount];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][pre_net_payable_amount] += $row_cat[pre_net_payable_amount];
		
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][location_id] = $row_cat[location_id];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][division_id] = $row_cat[division_id];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][department_id] = $row_cat[department_id];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][section_id] = $row_cat[section_id];
		$result_row_arr[$row_cat[employee_category]][$row_cat[$orderby_bu]][subsection_id] = $row_cat[subsection_id];
		
	}
	//print_r($result_row_arr);
	
	$kki=1;
	$row_span=array();
	foreach($result_row_arr as $key=>$dept_id)
	{
		foreach($dept_id as $k=>$id)
		{
			$row_span[$key]+=1;
			$kki++;
		}
	}
	//var_dump($row_span); 
	$sl=0;
	foreach($result_row_arr as $key=>$val)
	{	
		$n=0;
		foreach($val as $kk=>$value)
		{
			$sl++;
			
			if($group_by_id==0){ $data_field=$department_details[$kk]; $data_field_bu=$result_row[department_id];}
			else if($group_by_id==1){$data_field=$company_details[$kk]; $data_field_bu=$result_row[company_id];}
			else if($group_by_id==2){$data_field=$location_details[$kk]; $data_field_bu=$result_row[location_id];}
			else if($group_by_id==3){$data_field=$division_details[$kk]; $data_field_bu=$result_row[division_id];} 
			else if($group_by_id==4){$data_field=$department_details[$kk]; $data_field_bu=$result_row[department_id];} 
			else if($group_by_id==5){$data_field=$section_details[$kk]; $data_field_bu=$result_row[section_id];}
			else if($group_by_id==6){$data_field=$subsection_details[$kk]; $data_field_bu=$result_row[subsection_id];}
			else{ $data_field=$department_details[$kk]; $data_field_bu=$result_row[department_id];}
			
			?>
			<tr align="right" bgcolor="<? echo $bgcolor; ?>">
				<td width="" align="center" valign="middle"><? echo $sl;  ?></td>
				<?
				if($n==0)
				{ 
					?>
					<td width="" valign="middle" align="center" rowspan="<? echo $row_span[$key]; ?>"><? echo $employee_category[$key];//."==".$key; ?></td>
					<? 
				} 
				?>
				<td width="" align="center" valign="middle" ><? echo $data_field;//."==".$kk; ?></td>
				<td width=""><? echo number_format($value[c_employee]); ?></td>
				<td width=""><? echo number_format($value[g_salary]); ?></td>
				<td align="center" width="" ><? echo number_format($value[total_late_deduction_amount])."<br>". number_format($value[total_absent_deduction_amount])."<br>".number_format($value[not_payable_deduction_amount]); ?></td>
				<td width=""><? echo number_format($value[total_earning_amount]); ?> </td>
                <td width=""><? echo number_format($value[att_bonus_amount]); ?> </td>
                <td width=""><? echo number_format($value[b_buyer_ot]); ?> </td>
				<td width=""><? echo number_format($value[b_extra_ot]); ?> </td>
				<td width=""><? echo number_format($value[total_overtime_amount]); ?></td>
				<td width=""><? echo number_format($value[total_salary_amount]); ?></td>
				<td width=""><? echo number_format($value[advance_amount]); ?></td>
				<td width=""><? echo number_format($value[stamp]); ?></td>
				<td width=""><? echo number_format($value[others_deduction_amount]); ?></td>
				<td width=""><? echo number_format($value[saving_fund]); ?></td>
				<td width=""><? echo number_format($value[total_deduction_amount]); ?></td>
				<td width=""><? echo number_format($value[net_payable_amount]);?></td>
			</tr>
			<?
		 	$n++;
		 
			$grandtotal_c_employee+=$value[c_employee];
			$grandtotal_g_salary+=$value[g_salary];
			$late_abs_np_deduct_amount+=($value[total_late_deduction_amount]+$value[total_absent_deduction_amount]+$value[not_payable_deduction_amount]);
			$grand_total_earning_amount+=$value[total_earning_amount];
			$grandtotal_att_bonus_amount+=$value[att_bonus_amount];
			$grandtotal_b_buyer_ot+=$value[b_buyer_ot];
			$grandtotal_b_extra_ot+=$value[b_extra_ot];
			$grandtotal_over_time_amount+=$value[total_overtime_amount];
			$grandtotal_salary_amount+=$value[total_salary_amount];
			$grandtotal_advance_amount+=$value[advance_amount];
			$grandtotal_stamp_amount+=$value[stamp];
			$grandtotal_others_deduction_amount+=$value[others_deduction_amount];
			$grandtotal_saving_fund+=$value[saving_fund];
			$grandtotal_total_deduction_amount+=$value[total_deduction_amount];
			$grandtotal_net_payable_amount+=$value[net_payable_amount];
			
			$grandtotal_pre_c_employee+=$value[pre_c_employee];
			$grandtotal_pre_g_salary+=$value[pre_g_salary];
			$pre_late_abs_np_deduct_amount+=($value[pre_total_late_deduction_amount]+$value[pre_total_absent_deduction_amount]+$value[pre_not_payable_deduction_amount]);
			$grand_total_pre_earning_amount+=$value[pre_total_earning_amount];
			$grand_total_pre_att_bonus_amount+=$value[pre_att_bonus_amount];
			$grandtotal_pre_b_buyer_ot+=$value[pre_b_buyer_ot];
			$grandtotal_pre_b_extra_ot+=$value[pre_b_extra_ot];
			$grandtotal_pre_total_overtime_amount+=$value[pre_total_overtime_amount];
			$grandtotal_pre_salary_amount+=$value[pre_total_salary_amount];
			$grandtotal_pre_advance_amount+=$value[pre_advance_amount];
			$grandtotal_pre_stamp+=$value[pre_stamp];
			$grandtotal_pre_others_deduction_amount+=$value[pre_others_deduction_amount];
			$grandtotal_pre_saving_fund_amount+=$value[pre_saving_fund];
			$grandtotal_pre_total_deduction_amount+=$value[pre_total_deduction_amount];
			$grandtotal_pre_net_payable_amount+=$value[pre_net_payable_amount];
		}
	}
	
	$diff_c_employee=($grandtotal_c_employee-$grandtotal_pre_c_employee);
	$diff_g_salary=($grandtotal_g_salary-$grandtotal_pre_g_salary);
	$diff_late_abs_np_deduct_amount=($late_abs_np_deduct_amount-$pre_late_abs_np_deduct_amount);
	$diff_total_earning_amount=($grand_total_earning_amount-$grand_total_pre_earning_amount);
	$diff_att_bonus_amount=($grandtotal_att_bonus_amount-$grand_total_pre_att_bonus_amount);
	$diff_b_buyer_ot=($grandtotal_b_buyer_ot-$grandtotal_pre_b_buyer_ot);
	$diff_b_extra_ot=($grandtotal_b_extra_ot-$grandtotal_pre_b_extra_ot);
	$diff_over_time_amount=($grandtotal_over_time_amount-$grandtotal_pre_total_overtime_amount);
	$diff_salary_amount=($grandtotal_salary_amount-$grandtotal_pre_salary_amount);
	$diff_advance_amount=($grandtotal_advance_amount-$grandtotal_pre_advance_amount);
	$diff_stamp_amount=($grandtotal_stamp_amount-$grandtotal_pre_stamp);
	$diff_others_deduction_amount=($grandtotal_others_deduction_amount-$grandtotal_pre_others_deduction_amount);
	$diff_saving_fund=($grandtotal_saving_fund-$grandtotal_pre_saving_fund_amount);
	$diff_total_deduction_amount=($grandtotal_total_deduction_amount-$grandtotal_pre_total_deduction_amount);
	$diff_net_payable_amount=($grandtotal_net_payable_amount-$grandtotal_pre_net_payable_amount);
	
	?>
    	</tbody>
        <tr style="background-color:#FFF">
            <td colspan="3" align="center">Grand Total <? echo $cur_month." ".$exp_month[0];?></td>
            <td width="" align="right"><? echo number_format($grandtotal_c_employee); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_g_salary); ?></td>
            <td width="" align="right"><? echo number_format($late_abs_np_deduct_amount); ?></td>
            <td width="" align="right"><? echo number_format($grand_total_earning_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_att_bonus_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_b_buyer_ot); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_b_extra_ot); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_over_time_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_salary_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_advance_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_stamp_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_others_deduction_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_saving_fund); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_total_deduction_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_net_payable_amount); ?></td>
        </tr>
         <tr style="background-color:#FFF">
            <td colspan="18" align="center">&nbsp;</td>
        </tr>
        <tr style="background-color:#FFF">
            <td colspan="3" align="center">Grand Total <? echo $previous_month." ".$exp_month_previous[0]; ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_c_employee); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_g_salary); ?></td>
            <td width="" align="right"><? echo number_format($pre_late_abs_np_deduct_amount); ?></td>
            <td width="" align="right"><? echo number_format($grand_total_pre_earning_amount); ?></td>
            <td width="" align="right"><? echo number_format($grand_total_pre_att_bonus_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_b_buyer_ot); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_b_extra_ot); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_total_overtime_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_salary_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_advance_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_stamp); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_others_deduction_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_saving_fund_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_total_deduction_amount); ?></td>
            <td width="" align="right"><? echo number_format($grandtotal_pre_net_payable_amount); ?></td>
        </tr>
        <tr style="background-color:#FFF">
            <td colspan="3" align="center">Difference</td>
            <td width="" align="right"><? echo number_format($diff_c_employee); ?></td>
            <td width="" align="right"><? echo number_format($diff_g_salary); ?></td>
            <td width="" align="right"><? echo number_format($diff_late_abs_np_deduct_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_total_earning_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_att_bonus_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_b_buyer_ot); ?></td>
            <td width="" align="right"><? echo number_format($diff_b_extra_ot); ?></td>
            <td width="" align="right"><? echo number_format($diff_over_time_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_salary_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_advance_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_stamp_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_others_deduction_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_saving_fund); ?></td>
            <td width="" align="right"><? echo number_format($diff_total_deduction_amount); ?></td>
            <td width="" align="right"><? echo number_format($diff_net_payable_amount); ?></td>
        </tr>
    </table>
    <?
	echo signeture_table_peak(10,$company_name,"1210px");
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();

}//end if condition of action


function signeture_table_peak($report_id,$company,$width,$location)
{
	if ($width=="") $width="100%"; 
	$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company");
	$signeture_list = explode('#',$signeture_allow_or_no);
	if($signeture_allow_or_no!= "")	
	{
	 echo '<table width="'.$width.'">
		<tr height="100" style="font-size:11px; font-family:Arial; font-weight:bold">';
		foreach($signeture_list as $key=>$value)
		{
			//echo '<td align="center" valign="bottom" style="text-decoration:overline">'.$value.'</td> ';
			echo '<td align="center" valign="bottom" style="text-decoration:overline; border-top-style:hidden; border-right-style:hidden; border-left-style:hidden; border-bottom-style:hidden;">'.$value.'</td>';
		}
		echo '</tr></table>';
	}
	else
	{
		echo '<table width="'.$width.'">
		<tr height="100" style="font-size:11px; font-family:Arial; font-weight:bold">';
		 
		echo '</tr></table>';
	}
}



function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}    


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die; 
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}


function add_month($orgDate,$mon){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
  return $retDAY;
}

?>    
