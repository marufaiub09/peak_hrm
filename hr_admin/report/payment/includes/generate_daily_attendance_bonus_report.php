<?php
/*************************************
|	Developed by	: Md. Nuruzzamam
|	date			: 24.12.2013
**************************************/
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}


	$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	
	//company info
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	

// daily_attendance_bonus_report
if($action=="daily_attendance_bonus_report") 
{
	
	//echo $id_card_no;die;
	
	$from_date=convert_to_mysql_date($txt_from_date);
	$to_date=convert_to_mysql_date($txt_to_date);
	$get_days=datediff(d,$from_date,$to_date);
	$all_date=createDateRangeArray($from_date,$to_date);
	//print_r($all_date);
	
	if ($category_id=='') $category_id="and e.category in (0,1,2,3,4)"; else $category_id="and e.category='$category_id'";
	if ($company_id==0) $company_id=""; else $company_id="and e.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and e.location_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and e.division_id in ($division_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and e.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and e.subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and e.designation_id in ($designation_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and  e.department_id in ($department_id)";
	//if ($emp_code==0) $emp_code=""; else $emp_code="and e.emp_code in ($emp_code)";
	//if ($id_card=='') $id_card_no=""; else $id_card_no="and e.id_card_no in ($id_card_no)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and e.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and e.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="e.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="e.company_id,e.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="e.company_id,e.location_id,e.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="e.company_id,e.location_id,e.division_id,e.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,e.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,e.subsection_id,e.emp_code,";}

	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(e.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(e.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(e.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(e.id_card_no), 5)as SIGNED)";}	 
	//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by

	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	ob_start();
	
	?>
    <table cellpadding="0" cellspacing="0" border="1" align="center"  class="rpt_table"  rules="all"><?	
	$sql="SELECT a.emp_code,e.emp_code,e.id_card_no,e.category,e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,e.subsection_id,e.designation_id,CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS name ";
	$j=0;
	foreach( $all_date AS $value )
	{
		$j++;
		$sql.= ",SUM(CASE WHEN a.attnd_date='$value' and a.status='P' and a.is_questionable=0 and ( a.sign_out_time>='16:00:00' or  a.sign_out_time<='08:00:00') THEN 50 else 0 END) AS 'status$j'";
		//$sql.= ",SUM(CASE WHEN a.attnd_date='$value' and a.status='P' and a.is_questionable=0 and a.early_out_min=0 THEN 50 else 0 END) AS 'status$j'";
	}
	$sql.=" FROM hrm_attendance a,hrm_employee e 
	WHERE e.emp_code=a.emp_code AND
	e.salary_type_entitled=1 
	$category_id
	$company_id
	$location_id  
	$division_id 
	$department_id 
	$section_id 
	$subsection_id 
	$designation_id
	$id_card_no 
	$emp_code AND 
	a.attnd_date BETWEEN '$from_date' AND '$to_date'
	group by $dynamic_groupby $orderby";
	//echo $sql;die;
	
	$subsec_total_payable_tk=0;$sec_total_payable_tk=0;$dep_total_payable_tk=0;$divis_total_payable_tk=0;$loc_total_payable_tk=0;
	$i=1;
	$sl=0;
	$result=mysql_query($sql) or die(mysql_error());
	while($row=mysql_fetch_assoc($result))
	{
		$sl++; 
		$total_ot=0;
		
		if($sl==1)//start header print
		{
			//echo "oks";die;
			$location_arr[$row[location_id]]=$row[location_id];
			$division_arr[$row[division_id]]=$row[division_id];
			$department_arr[$row[department_id]]=$row[department_id];
			$section_arr[$row[section_id]]=$row[section_id];
			$subsection_arr[$row[subsection_id]]=$row[subsection_id];?>
			<thead> 
				<tr height="65">
					<th colspan="<? echo $get_days+7; ?>" align="center" bordercolor="#FFFFFF" style="vertical-align:top;"><br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br /><br />
					Regular Attendance Bonus on  <? echo $txt_from_date; ?> To <? echo $txt_to_date; ?></th>
				</tr>
				<tr>
					<th width="50">SL</th>
                    <th width="80">ID Card No</th>
					<!--<th width="80">System code</th>-->
					<th width="150">Name</th>
					<th width="100">Designation</th>
					<?php foreach( $all_date AS $value ){ ?>
					<th width="50" style="font-size:9px;"> <?php echo substr($value,8,2); ?></th>
					<?php } ?>                    
					<th width="70">Net Payable</th>
					<th width="100">Signature</th>
					<th width="80">Remarks</th>            
				</tr>   
			</thead>
			<tbody><? 
		}//end header print
		if($sl!=1)
		{
			//echo $sl."ssss";die;
			$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
			if(in_array($row[location_id],$location_arr) && $status_loc==1)
			{
				if(in_array($row[division_id],$division_arr) && $status_divis==1)
				{
					if(in_array($row[department_id],$department_arr) && $status_dept==1)
					{
						if(in_array($row[section_id],$section_arr) && $status_sec==1)
						{ 
							if(in_array($row[subsection_id],$subsection_arr) && $status_subsec==1)
							{}
							else if($status_subsec==1)
							{
							$subsection_arr[$row[subsection_id]]=$row[subsection_id];
							$new_subsec=1;
							}
						}
						else if($status_sec==1)
						{
							$section_arr[$row[section_id]]=$row[section_id];
							$subsection_arr=array();
							$subsection_arr[$row[subsection_id]]=$row[subsection_id];
							$new_sec=1;
							$new_subsec=1;
						}
					}
					else if($status_dept==1)
					{
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr=array();
						$subsection_arr=array();
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}
				}//division
				else if($status_divis==1)
				{
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr=array();
					$section_arr=array();
					$subsection_arr=array();
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//division else
			}//location
			else if($status_loc==1)
			{
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr=array();
				$department_arr=array();
				$section_arr=array();
				$subsection_arr=array();
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//location else
		 }//end if($sl!=1)
		 
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left"><b> Subsection Total</b></td>
                    <td align="center"><?php echo $subsec_total_payable_tk;  ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <? $subsec_total_payable_tk=0;
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left"><b>SectionTotal</b></td>
                    <td align="center">&nbsp;<?php echo $sec_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <? $sec_total_payable_tk=0;
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left">Department Total</td>
                    <td align="center">&nbsp;<?php echo $dep_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <? $dep_total_payable_tk=0;
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left" >Division Total</td>
                    <td align="center">&nbsp;<?php echo $divis_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                    <? $divis_total_payable_tk=0;
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left">Location Total</td>
                    <td align="center">&nbsp;<?php echo $loc_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
					<? $loc_total_payable_tk=0;
		}
	 
	//header print here 
	$c_part_1="";
	if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
	{
		if($status_loc==1)
		{					
			$c_part_1 .= "  Location :  ".$location_details[$row[location_id]]."  ,";
		}				
		if($status_divis==1)
		{
			$c_part_1 .= "  Division :  ".$division_details[$row[division_id]]."  ,";
		}
		if($status_dept==1)
		{
			$c_part_1 .= "  Department :  ".$department_details[$row[department_id]]."  ,";
		}
		if($status_sec==1)
		{
			$c_part_1 .= "  Section :  ".$section_details[$row[section_id]]."  ,";
		}
		if($status_subsec==1)
		{
			$c_part_1 .= "  Sub Section :  ".$subsection_details[$row[subsection_id]]."  ,";
		}
		if($c_part_1!='')
		{
			$i=0;$sl=1;?>
            <tr><td colspan="<? echo $get_days+11; ?>" ><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
		}		
	}
	
	if ($i%2==0) $bgcolor="#EEEEEE";  else $bgcolor="#FFFFFF";
	
	//if ($ot_amount_id==0){ $ot_amount_id="";} else $ot_amount_id =">";
	//if($row[status1]){
	?>
        <tr bgcolor="<? echo $bgcolor;?>" height="30">
            <td width="50" align="center" valign="middle"><? echo $sl;?> </td>
            <td width="80" valign="middle"><? echo $row[id_card_no]; ?></td>
            <!--<td width="80" valign="middle"><? // echo $row[emp_code]; ?></td>-->
            <td width="150" valign="middle"><? echo $row[name];?></td>
            <td width="100" valign="middle"><? echo $designation_chart[$row['designation_id']];?></td>
            <?php 
            $z=0;
            $net_pay=0; 
            foreach( $all_date AS $key=>$value )
            {
                $z++;
                //$net_pay=$net_pay+$row["status$z"];
                $net_pay+=$row["status$z"];?>
            <td width="50" valign="middle" align="center"><? echo $row["status".$z];?></td><? } ?>
            <td width="70" valign="middle" align="center"><? echo $net_pay; ?></td>
            <td width="100" valign="middle">&nbsp;</td>
            <td width="80" valign="middle">&nbsp;</td>
        </tr><?  
		$grang_total_payable_tk+=$net_pay;
		$subsec_total_payable_tk+=$net_pay;
		$sec_total_payable_tk+=$net_pay;
		$dep_total_payable_tk+=$net_pay;
		$divis_total_payable_tk+=$net_pay;
		$loc_total_payable_tk+=$net_pay;
		$i++;
}

	//}
	// while loop
	//echo $grang_total_payable_tk."= ".$subsec_total_payable_tk."= ".$sec_total_payable_tk."= ".$dep_total_payable_tk."= ".$divis_total_payable_tk."= ".$loc_total_payable_tk;die;
		
		if($status_subsec==1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left"><b> Subsection Total</b></td>
                    <td align="center"><?php echo $subsec_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td><?
		}
		if($status_sec==1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left"><b>SectionTotal</b></td>
                    <td align="center">&nbsp;<?php echo $sec_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td><?
		}
		
		if($status_dept==1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left">Department Total</td>
                    <td align="center">&nbsp;<?php echo $dep_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td><?
		}
		 
		if($status_divis==1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left" >Division Total</td>
                    <td align="center">&nbsp;<?php echo $divis_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td><?
		}
		 
		if($status_loc==1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="<? echo $get_days+4; ?>" align="left">Location Total</td>
                    <td align="center">&nbsp;<?php echo $loc_total_payable_tk; ?></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td><?
		}
    		?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
				<td colspan="<? echo $get_days+4; ?>"><strong>Greand Total</strong></td>
                <td align="center">&nbsp;<?php echo $grang_total_payable_tk; ?></td>
                <td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
            </tr>
 		</tbody>
        <tfoot>
        	<tr height="150" style="font-size:11px; font-family:Arial, Helvetica, sans-serif">
                <td colspan="<? echo $get_days+7; ?>" align="center" valign="bottom">                
                	<div style="float:left; width:140px"><strong style="text-decoration:overline">H/R OFFICER</strong></div>
                    <div style="float:left; width:150px"><strong style="text-decoration:overline">PREPARED BY (PAYROLL)</strong></div>
                    <div style="float:left; width:140px"><strong style="text-decoration:overline">CHECKED BY</strong></div>
                    <div style="float:left; width:140px"><strong style="text-decoration:overline">HRM</strong></div>
                    <div style="float:left; width:140px"><strong style="text-decoration:overline">MANAGER A/C</strong></div>
                    <div style="float:left; width:140px"><strong style="text-decoration:overline">MANAGER AUDIT</strong></div>
                    <div style="float:left; width:150px"><strong style="text-decoration:overline">DGM (ACCOUNTS AND FINANCE)</strong></div>
                    <div style="float:left; width:140px"><strong style="text-decoration:overline">MANAGING DIRECTOR</strong></div>
                </td>
            </tr>        
        </tfoot>
    </table><?
//previous file delete code-----------------------------//
	$html = ob_get_contents();
	ob_clean();		
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}
/*
function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}*/
/*		
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
*/
/*
function createDateRangeArray($strDateFrom,$strDateTo)
	{
		// takes two dates formatted as YYYY-MM-DD and creates an
		// inclusive array of the dates between the from and to dates.
	
		// could test validity of dates here but I'm already doing
		// that in the main script
	
		$aryRange=array();
	
		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
	
		if ($iDateTo>=$iDateFrom)
		{
			array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo)
			{
				$iDateFrom+=86400; // add 24 hours
				array_push($aryRange,date('Y-m-d',$iDateFrom));
			}
		}
		return $aryRange;
	}*/
	
	
?>