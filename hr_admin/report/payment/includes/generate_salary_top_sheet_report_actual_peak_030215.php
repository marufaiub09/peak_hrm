<?php
/***************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 02.02.2015
*****************************************/

	session_start();
	header('Content-type: text/html; charset=utf-8');
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	
	include('../../../../includes/common.php');
	include('../../../../includes/array_function.php');
	include('../../../../includes/common_functions.php');
	extract($_REQUEST);

 	//lib_company
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) { $company_details[$row['id']] = mysql_real_escape_string($row['company_name']); }

	//lib_location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) { $location_details[$row['id']] = mysql_real_escape_string($row['location_name']); }

	//lib_division
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) { $division_details[$row['id']] = mysql_real_escape_string($row['division_name']); }

	//lib_department
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) { $department_details[$row['id']] = mysql_real_escape_string($row['department_name']); }

	//lib_section
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) { $section_details[$row['id']] = mysql_real_escape_string($row['section_name']); }
	
	//lib_subsection
	$sql = "SELECT * FROM  lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) { $subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']); }	

	//top_sheet
	if($action=="top_sheet")//check which top sheet format  
	{
		$type_id=$group_by_id; 
		$sqls="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and salary_top_sheet_format !='0' and company_name='$company_id' order by id DESC ";  
		$result = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
		$row = mysql_fetch_array( $result );
		
		if($type_id==1) 	$types=$row[salary_top_sheet_format]; //department wise
		elseif($type_id==2) $types=$row[salary_top_sheet_format]; 
		elseif($type_id==3) $types="wages_salary_sheet_division";
		elseif($type_id==4) $types="wages_salary_sheet_department";
		elseif($type_id==5) $types="wages_salary_sheet_section";
		elseif($type_id==6) $types="wages_salary_sheet_subsection";  
		else $types="wages_salary_sheet_comparative";
		//echo $types;die;
		
		$sql = "SET @@group_concat_max_len = 9999999";
		$r = @mysql_query ($sql);
	
		//if($row[salary_top_sheet_format]=="salary_top_sheet_format2"){
			
		if($types=="wages_salary_sheet_department") //  department wise
		{
			//echo $types;die;
			ob_start();
			$company_name=$company_id;
			if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
			if ($company_id==0) $company=''; else $company=" and a.company_id='$company_id'";
			if ($department_id==0) $department=""; else	$department=" and a.department_id in($department_id)";
			if ($division_id==0) $division=""; else $division="and  a.division_id  in($division_id)";
			if ($section_id==0 ) $section_id=""; else $section_id="and a.section_id in ($section_id)";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
			if ($location_id==0) $location=""; else $location="and  a.location_id='$location_id'";
			if ($cbo_salary_sheet==''){$emp_status="";} else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} else $emp_status="and a.emp_status='$cbo_salary_sheet'";
			
			$starting_date = explode('_', $cbo_month_selector ); 
			$salary_date = $starting_date[0];
			$exp_month=explode("-",$salary_date);
			$cur_month = $months["'"+$exp_month[1]+"'"];
			$i=0;	
			
			$company_info_sql="select id,company_name,email,website, plot_no, level_no, road_no,block_no,zip_code,city from lib_company where id='$company_name'";
			$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
			$company_info_result = mysql_fetch_array($result);
			?>
	
            <div style="border:0px; font-size:18px;" align="center"><?	echo $company_info_result["company_name"];?></div>
            <div style="border:0px; font-size:13px" align="center">
                Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"]; echo "Bangladesh";?><br />
                Salary Top Sheet For The Month Of <? echo $cur_month; ?> <? echo $exp_month[0]; ?><br />
            </div>
			<?
	
			$sql = "SET @@group_concat_max_len = 9999999";
			$r = @mysql_query ($sql);
						 
			$location_arr=array();
			$division_arr=array();
			$department_arr=array();
			?>                
            <table id="topsheet_tbl" width="1500" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;" class="rpt_table" rules="all">
				<thead>  		
                    <tr>
                        <th width="50">SL</th>
                        <th width="180">Emp Category</th>
                        <th width="50">Manpower</th>
                        <th width="120">G. Salary</th>
                        <th width="100">Total OT Amt</th>
                        <th width="100">Tiffin Allowance</th>
                        <th width="100">Attn Bonus</th>
                        <th width="100">T. Salary</th>
                        <th width="80">ADV. Sal</th>
                        <th width="80">Late Present</th>
                        <th width="80">ABS Amt</th>
                        <th width="80">Stamp</th>
                        <th width="80">LWP</th>
                        <th width="90">Others Deduc</th>
                        <th width="90">Savings Fund</th>
                        <th width="120">T. Deduc</th>
                        <th width="120">Net Pay</th>
                        <th width="">Remarks</th>
                    </tr>   
				</thead>
              	<tbody>
				<?
				$subtotal_loc_c_employee=0;$subtotal_loc_t_salary=0;$subtotal_loc_adv_salary=0;$subtotal_loc_abs_amnt=0;$subtotal_loc_np_amnt=0;
				$subtotal_loc_other_deduc=0;$subtotal_loc_stamp=0;$subtotal_loc_tax=0;$subtotal_loc_net_pay=0;$subtotal_loc_withheld=0;
				$subtotal_loc_final_pay=0;$subtotal_loc_in_bank=0;$subtotal_loc_in_cash=0;$subtotal_loc_trans_alw=0;$subtotal_loc_tiff_alw=0;
				$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;	   
				
				$subtotal_dep_c_employee=0;$subtotal_dep_t_salary=0;$subtotal_dep_adv_salary=0;$subtotal_dep_abs_amnt=0;$subtotal_dep_np_amnt=0;
				$subtotal_dep_other_deduc=0;$subtotal_dep_stamp=0;$subtotal_dep_tax=0;$subtotal_dep_net_pay=0;$subtotal_dep_withheld=0;
				$subtotal_dep_final_pay=0;$subtotal_dep_in_bank=0;$subtotal_dep_in_cash=0;$subtotal_dep_trans_alw=0;$subtotal_dep_tiff_alw=0;
				$subtotal_dep_lwp_deduction_amount=0;$subtotal_dep_pf_amount=0;
                        
				$subtotal_div_c_employee=0;$subtotal_div_t_salary=0;$subtotal_div_adv_salary=0;$subtotal_div_abs_amnt=0;$subtotal_div_np_amnt=0;
				$subtotal_div_other_deduc=0;$subtotal_div_stamp=0;$subtotal_div_tax=0;$subtotal_div_net_pay=0;$subtotal_div_withheld=0;
				$subtotal_div_final_pay=0;$subtotal_div_in_bank=0;$subtotal_div_in_cash=0;$subtotal_div_trans_alw=0;$subtotal_div_tiff_alw=0;
				$subtotal_div_lwp_deduction_amount=0;$subtotal_div_pf_amount=0;
                        
				$grandtotal_c_employee=0;$grandtotal_t_salary=0;$grandtotal_adv_salary=0;$grandtotal_abs_amnt=0;$grandtotal_np_amnt=0;
				$grandtotal_other_deduc=0;$grandtotal_stamp=0;$grandtotal_tax=0;$grandtotal_net_pay=0;$grandtotal_withheld=0;
				$grandtotal_final_pay=0;$grandtotal_in_bank=0;$grandtotal_in_cash=0;$grandtotal_trans_alw=0;$grandtotal_tiff_alw=0;$grandtotal_late_salary=0;
				$grandtotal_lwp_deduction_amount=0;$grandtotal_div_pf_amount=0;
                        
				$start=0; 
				$sql_salary = " select group_concat(CAST(a.id as CHAR)) as mst_id, a.company_id, a.location_id, a.division_id, a.department_id, a.section_id, a.subsection_id, a.employee_category, a.emp_status,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN 1 else 0 END ) as c_employee,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN a.gross_salary else 0 END ) as g_salary,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.total_over_time*a.over_time_rate) else 0 END ) as actual_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_earning_amount else 0 END ) as total_earning_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN not_payble_days else 0 END ) as not_payble_days,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN daily_basic_salary else 0 END ) as daily_basic_salary,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_deduction_amount else 0 END ) as total_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN net_payable_amount else 0 END ) as net_payable_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.not_payable_deduction_amount) else 0 END ) as total_not_payable_amt,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.bank_gross) else 0 END ) as emp_bank_gross
					from 
					hrm_salary_mst a, hrm_employee b
					where a.emp_code=b.emp_code and a.salary_periods = '$salary_date' 
					$emp_status $category $company $location $division $department $section_id $subsection_id
					group by a.company_id,a.location_id,a.division_id,a.department_id,a.employee_category
					having c_employee>0";
                                            
					// echo "<br/>".$sql_salary;die; 
					$sql_salary_result = mysql_query($sql_salary) or die(mysql_error()); 
					//$numrows=mysql_num_rows($sql_salary_result);
                    while($result_row = mysql_fetch_array($sql_salary_result))
					{
						/*(29-09-2013)$sql = "SELECT a.emp_code,sum(a.pay_amount) as pay_amount FROM hrm_salary_dtls a WHERE  a.salary_mst_id in ( $result_row[mst_id] ) and salary_head_id in (5,6,7) group by a.head_type";
						//echo $sql;die;
						$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
						
						//$processed_ot_amount = array();
						while( $row = mysql_fetch_assoc( $result ) ) {		
								$processed_ot_amount = ( $row['pay_amount'] );
									
						}*/
						
						//for staff OT code here....do not change					
						$result_row[b_ot] = $result_row[b_ot]- $result_row[staff_buyer_ot];
						$result_row[e_ot] = $result_row[e_ot]- $result_row[staff_extra_ot];
						$result_row[total_earning_amount]=$result_row[total_earning_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						$result_row[net_payable_amount]=$result_row[net_payable_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						 
					//---------------------------------------------------------------------------------------------------	
					//---------------------------------------------------------------------------------------------------
					
					$new_loc=0;$new_divis=0;$new_dept=0;
					if(in_array($result_row[location_id],$location_arr))
					{
						if(in_array($result_row[division_id],$division_arr))
						{
							if(in_array($result_row[department_id],$department_arr))
							{
							}
							else
							{
								$department_arr[$result_row[department_id]]=$result_row[department_id];
								$new_dept=1;
							}
						}//division
						else
						{
							$division_arr[$result_row[division_id]]=$result_row[division_id];
							$department_arr = array();
							$department_arr[$result_row[department_id]]=$result_row[department_id];
							$new_divis=1;
							$new_dept=1;
						}//division else
					}//location
					else
					{
						$location_arr[$result_row[location_id]]=$result_row[location_id];
						$division_arr = array();
						$department_arr = array();
						$division_arr[$result_row[division_id]]=$result_row[division_id];
						$department_arr[$result_row[department_id]]=$result_row[department_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
					}//location else
					
					if($new_dept==1 && $start!=0)
					{
						?> 
						<tr style="font-weight:bold; background-color:#FFF">
							<th colspan="2" align="center">Department Total</th>
							<th width="" align="right"><? echo number_format($subtotal_dep_c_employee); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_g_salary); ?></th>
							<!--<th width="" align="right"><? //echo number_format($subtotal_dep_ot_amnt+$subtotal_dep_extra_ot_amnt); ?></th>-->
							<th width="" align="right"><? echo number_format($subtotal_dep_ot_amnt); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_tiff_alw); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_attn_bonus); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_t_salary); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_adv_salary); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_late_salary); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_abs_amnt); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_stamp); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_lwp_deduction_amount); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_other_deduc); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_pf_amount); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_total_deduc); ?></th>
							<th width="" align="right"><? echo number_format($subtotal_dep_net_pay); ?></th>
							<th width="" align="right"></th>
                        </tr> 
                        <?
						$subtotal_dep_c_employee=0; $subtotal_dep_g_salary=0; $subtotal_dep_ot_amnt=0; $subtotal_dep_extra_ot_amnt=0; $subtotal_dep_attn_bonus=0; 
						$subtotal_dep_arrear_ern=0; $subtotal_dep_t_salary=0; $subtotal_dep_adv_salary=0; $subtotal_dep_abs_amnt=0; $subtotal_dep_np_amnt=0;
						$subtotal_dep_other_deduc=0;  $subtotal_dep_stamp=0; $subtotal_dep_tax=0; $subtotal_dep_total_deduc=0; $subtotal_dep_net_pay=0;
						$subtotal_dep_withheld=0; $subtotal_dep_final_pay=0; $subtotal_dep_in_bank=0; $subtotal_dep_in_cash=0; $subtotal_dep_trans_alw=0;
						$subtotal_dep_tiff_alw=0;$subtotal_dep_late_salary=0;$subtotal_dep_lwp_deduction_amount=0;$subtotal_dep_pf_amount=0;	       
						//$department_arr = array();
					} 
					
					if($new_divis==1 && $start!=0)
					{
						?> 
                        <tr style="font-weight:bold; background-color:#FFF">
                            <th colspan="2" align="center">Division Total</th>
                            <th width="" align="right"><? echo number_format($subtotal_div_c_employee); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_g_salary); ?></th>
                           <!-- <th width="" align="right"><? //echo number_format($subtotal_div_ot_amnt+$subtotal_div_extra_ot_amnt); ?></th>-->
                            <th width="" align="right"><? echo number_format($subtotal_div_ot_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_tiff_alw); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_attn_bonus); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_t_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_adv_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_late_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_abs_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_stamp); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_lwp_deduction_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_other_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_pf_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_total_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_div_net_pay); ?></th>
                            <th width="" align="right"></th>
						</tr> 
						<?
						$subtotal_div_c_employee=0; $subtotal_div_g_salary=0; $subtotal_div_ot_amnt=0; $subtotal_div_extra_ot_amnt=0; $subtotal_div_attn_bonus=0; 
						$subtotal_div_arrear_ern=0; $subtotal_div_t_salary=0; $subtotal_div_adv_salary=0; $subtotal_div_abs_amnt=0; $subtotal_div_np_amnt=0;
						$subtotal_div_other_deduc=0;  $subtotal_div_stamp=0; $subtotal_div_tax=0; $subtotal_div_total_deduc=0; $subtotal_div_net_pay=0;
						$subtotal_div_withheld=0; $subtotal_div_final_pay=0; $subtotal_div_in_bank=0; $subtotal_div_in_cash=0; $subtotal_div_trans_alw=0;
						$subtotal_div_tiff_alw=0;$subtotal_div_late_salary=0;$subtotal_div_lwp_deduction_amount=0;$subtotal_div_pf_amount=0;
						//$department_arr = array();
					}
					
					if($new_loc==1 && $start!=0)
					{					
						?> 
                        <tr style="font-weight:bold; background-color:#FFF">
                            <th colspan="2" align="center">Location Total</th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
                            <!--<th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
                            <th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
                            <th width="" align="right"></th>
						</tr> 
						<?
						$subtotal_loc_c_employee=0; $subtotal_loc_g_salary=0; $subtotal_loc_ot_amnt=0; $subtotal_loc_extra_ot_amnt=0; $subtotal_loc_attn_bonus=0; 
						$subtotal_loc_arrear_ern=0; $subtotal_loc_t_salary=0; $subtotal_loc_adv_salary=0; $subtotal_loc_abs_amnt=0; $subtotal_loc_np_amnt=0;
						$subtotal_loc_other_deduc=0;  $subtotal_loc_stamp=0; $subtotal_loc_tax=0; $subtotal_loc_total_deduc=0; $subtotal_loc_net_pay=0;
						$subtotal_loc_withheld=0; $subtotal_loc_final_pay=0; $subtotal_loc_in_bank=0; $subtotal_loc_in_cash=0; $subtotal_loc_trans_alw=0;
						$subtotal_loc_tiff_alw=0;$subtotal_loc_late_salary=0;$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;
						//$division_arr = array();
						//$department_arr = array();
					}	
					
					//header print here 
					if($new_loc==1)
					{					
					?><tr><th colspan="22" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$result_row[location_id]]; ?></th></tr><?
					}				
					if($new_divis==1)
					{
					?><tr><th colspan="22" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$result_row[division_id]]; ?></th></tr><?
					}
					
					if($new_dept==1)
					{
					?><tr><th colspan="22" align="left" bgcolor="#CCCCCC">Department : <? echo $department_details[$result_row[department_id]]; ?></th></tr><?
					}
					
					//---------------------------------------------------------------------------------------------------
					//---------------------------------------------------------------------------------------------------
					
					$i++;
					if ($i%2==0)$bgcolor="#EFEFEF";  
					else $bgcolor="#FFFFFF";
							
					if($result_row[employee_category]==0)$status='Top Mgt.';
					else if($result_row[employee_category]==1)$status='Mid Mgt.';
					else if($result_row[employee_category]==2)$status='Non Mgt.';
						
					$dtls_sql = "select  group_concat(mst.emp_code) as mst_emp_code,mst.employee_category,
							SUM(CASE WHEN dtls.salary_head_id=25 THEN dtls.pay_amount else 0 END ) as c_total_pf_amount,
							SUM(CASE WHEN dtls.salary_head_id=23 THEN dtls.pay_amount else 0 END ) as c_total_late_deduction_amount,
							SUM(CASE WHEN dtls.salary_head_id=29 THEN dtls.pay_amount else 0 END ) as c_total_lwp_deduction_amount,
							SUM(CASE WHEN dtls.salary_head_id=24 THEN dtls.pay_amount else 0 END ) as c_absent,
							SUM(CASE WHEN dtls.salary_head_id=35 THEN dtls.pay_amount else 0 END ) as c_attn_bonus,
							SUM(CASE WHEN dtls.salary_head_id=17 THEN dtls.pay_amount else 0 END ) as c_arrear_earn,
							SUM(CASE WHEN dtls.salary_head_id=21 THEN dtls.pay_amount else 0 END ) as c_advancedSalary,
							SUM(CASE WHEN dtls.salary_head_id=31 THEN dtls.pay_amount else 0 END ) as c_otherDeduc,
							SUM(CASE WHEN dtls.salary_head_id=28 THEN dtls.pay_amount else 0 END ) as c_stamp,
							SUM(CASE WHEN dtls.salary_head_id=22 THEN dtls.pay_amount else 0 END ) as c_tax,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status=1 THEN (mst.net_payable_amount) else 0 END ) as witheldAmt,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status<>1 THEN (mst.net_payable_amount) else 0 END ) as finalPayAmt
							from 
							hrm_salary_mst mst, hrm_salary_dtls dtls 
							where 
							mst.id=dtls.salary_mst_id and mst.id in ($result_row[mst_id]) group by mst.company_id, mst.location_id, mst.division_id, mst.department_id, mst.employee_category";	
							/*mst.salary_periods = '$salary_date' and 
							mst.company_id=$result_row[company_id] and
							mst.location_id=$result_row[location_id] and
							mst.division_id=$result_row[division_id] and	
							mst.department_id=$result_row[department_id] and  				 
							mst.employee_category=$result_row[employee_category]*/
					//echo  $dtls_sql."\n";die;
					$dtls_sql_result = mysql_query($dtls_sql) or die(mysql_error());
					$result_dtls = mysql_fetch_array($dtls_sql_result);
					 
					//do not delete---- used for staff ot
					$result_dtls[witheldAmt]=round($result_dtls[witheldAmt]-$result_row[staff_ot_for_witheld]);
					$result_dtls[finalPayAmt]=round($result_dtls[finalPayAmt]-$result_row[staff_ot_for_final_pay]);
					?>
					<tr align="right" bgcolor="<? echo $bgcolor; ?>" onMouseOver="fn_color('topsheet_tbl')">
						<td width=""><? echo $i; ?></td>
						<td width="" align="left"><? echo $status; ?></td>
						<td width=""><? echo number_format($result_row[c_employee]); ?></td>
						<td width=""><? echo number_format($result_row[g_salary]); ?></td>
						<td width=""><? echo $result_row[actual_ot]; ?></td>
						<?
						$tiffinAllow=return_field_value("sum(dtls.amount)","hrm_salary_mst mst, hrm_employee_salary dtls","mst.emp_code=dtls.emp_code and mst.salary_periods like '%$salary_date%' and mst.emp_code in ($result_dtls[mst_emp_code]) and dtls.payroll_head=9 and mst.emp_status !=1 ");
						?>
						<td width=""><? echo number_format($tiffinAllow); ?></td>
						<td width=""><? $attn_bonus=$result_dtls[c_attn_bonus]; if(strlen($attn_bonus)>0)echo number_format($attn_bonus); else echo "0"; ?></td>
						<td width=""><? echo number_format($result_row[total_earning_amount]); ?></td>
						<td width=""><? $advSalary=$result_dtls[c_advancedSalary]; if(strlen($advSalary)>0)echo number_format($advSalary); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_late_deduction_amount])); ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_absent])); ?></td>
						<td width=""><? $stamp=$result_dtls[c_stamp]; if(strlen($stamp)>0)echo number_format($stamp); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_lwp_deduction_amount])); ?></td>
						<td width=""><? $otherDeduction=$result_dtls[c_otherDeduc]; if(strlen($otherDeduction)>0)
						echo number_format($otherDeduction); else echo "0"; ?>
						</td>
						<td width=""><? echo number_format(round($result_dtls[c_total_pf_amount])); ?></td>
						<td width=""><? echo number_format($result_row[total_deduction_amount]);?></td>
						<td width=""><? echo number_format($result_row[net_payable_amount]);?></td>
						<td width=""></td>
					</tr>   
					<?	
					//sub department total calculation
					$subtotal_dep_c_employee += $result_row[c_employee];
					$subtotal_dep_g_salary += $result_row[g_salary];
					$subtotal_dep_ot_amnt += $result_row[actual_ot];
					$subtotal_dep_extra_ot_amnt += $result_row[e_ot];
					$subtotal_dep_trans_alw += $tiffinAllow;
					$subtotal_dep_attn_bonus += $attn_bonus;
					$subtotal_dep_trans_alw += $transportAllw;
					$subtotal_dep_arrear_ern += $arrearErning;
					$subtotal_dep_t_salary += $result_row[total_earning_amount];
					$subtotal_dep_adv_salary += $advSalary;
					$subtotal_dep_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_dep_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_dep_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_dep_other_deduc += $otherDeduction;
					$subtotal_dep_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_dep_stamp += $stamp;
					$subtotal_dep_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_dep_tax += $tax;
					$subtotal_dep_total_deduc += $result_row[total_deduction_amount];
					$subtotal_dep_net_pay += $result_row[net_payable_amount];
					$subtotal_dep_withheld += $witheld;
					$subtotal_dep_final_pay += $final_pay;
					$subtotal_dep_in_bank += $result_row[emp_bank_gross];
					$subtotal_dep_in_cash += $cash_sal;
						
					//sub division total calculation
					$subtotal_div_c_employee += $result_row[c_employee];
					$subtotal_div_g_salary += $result_row[g_salary];
					$subtotal_div_ot_amnt += $result_row[actual_ot];
					$subtotal_div_extra_ot_amnt += $result_row[e_ot];
					$subtotal_div_tiff_alw += $tiffinAllow;
					$subtotal_div_attn_bonus += $attn_bonus;
					$subtotal_div_trans_alw += $transportAllw;
					$subtotal_div_arrear_ern += $arrearErning;
					$subtotal_div_t_salary += $result_row[total_earning_amount];
					$subtotal_div_adv_salary += $advSalary;
					$subtotal_div_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_div_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_div_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_div_other_deduc += $otherDeduction;
					$subtotal_div_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_div_stamp += $stamp;
					$subtotal_div_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_div_tax += $tax;
					$subtotal_div_total_deduc += $result_row[total_deduction_amount];
					$subtotal_div_net_pay += $result_row[net_payable_amount];
					$subtotal_div_withheld += $witheld;
					$subtotal_div_final_pay += $final_pay;
					$subtotal_div_in_bank += $result_row[emp_bank_gross];
					$subtotal_div_in_cash += $cash_sal;
					
					//sub location total calculation
					$subtotal_loc_c_employee += $result_row[c_employee];
					$subtotal_loc_g_salary += $result_row[g_salary];
					$subtotal_loc_ot_amnt += $result_row[actual_ot];
					$subtotal_loc_extra_ot_amnt += $result_row[e_ot];
					$subtotal_loc_tiff_alw += $tiffinAllow;
					$subtotal_loc_attn_bonus += $attn_bonus;
					$subtotal_loc_trans_alw += $transportAllw;
					$subtotal_loc_arrear_ern += $arrearErning;
					$subtotal_loc_t_salary += $result_row[total_earning_amount];
					$subtotal_loc_adv_salary += $advSalary;
					$subtotal_loc_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_loc_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_loc_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_loc_other_deduc += $otherDeduction;
					$subtotal_loc_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_loc_stamp += $stamp;
					$subtotal_loc_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_loc_tax += $tax;
					$subtotal_loc_total_deduc += $result_row[total_deduction_amount];
					$subtotal_loc_net_pay += $result_row[net_payable_amount];
					$subtotal_loc_withheld += $witheld;
					$subtotal_loc_final_pay += $final_pay;
					$subtotal_loc_in_bank += $result_row[emp_bank_gross];
					$subtotal_loc_in_cash += $cash_sal;
					
					//grand calculation
					$grandtotal_c_employee += $result_row[c_employee];
					$grandtotal_g_salary += $result_row[g_salary];
					$grandtotal_ot_amnt += $result_row[actual_ot];
					$grandtotal_extra_ot_amnt += $result_row[e_ot];
					$grandtotal_tiff_alw += $tiffinAllow;
					$grandtotal_attn_bonus += $attn_bonus;
					$grandtotal_trans_alw += $transportAllw;
					$grandtotal_arrear_ern += $arrearErning;
					$grandtotal_t_salary += $result_row[total_earning_amount];
					$grandtotal_adv_salary += $advSalary;
					$grandtotal_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$grandtotal_abs_amnt += round($result_dtls[c_absent]);
					$grandtotal_np_amnt += $result_row[total_not_payable_amt];
					$grandtotal_other_deduc += $otherDeduction;
					$grandtotal_pf_amount += ($result_dtls[c_total_pf_amount]);
					$grandtotal_stamp += $stamp;
					$grandtotal_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$grandtotal_tax += $tax;
					$grandtotal_total_deduc += $result_row[total_deduction_amount];
					$grandtotal_net_pay += $result_row[net_payable_amount];
					$grandtotal_withheld += $witheld;
					$grandtotal_final_pay += $final_pay;
					$grandtotal_in_bank += $result_row[emp_bank_gross];
					$grandtotal_in_cash += $cash_sal;
					$start++;
				}
				?>	
                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Division total</th>
                    <th width="" align="right"><? echo number_format($subtotal_div_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_g_salary); ?></th>
                    <!--<th width="" align="right"><? //echo number_format($subtotal_div_ot_amnt+$subtotal_div_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($subtotal_div_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_div_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
                            
                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Location total</th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
                    <!--<th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th> 
                    <th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
                                
                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Company total</th>
                    <th width="" align="right"><? echo number_format($grandtotal_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_g_salary); ?></th>
                    <!--<th width="" align="right"><? //echo number_format($grandtotal_ot_amnt+$grandtotal_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($grandtotal_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
            </tbody>       
        </table><br />
			<?	
            echo signeture_table(10,$company_name,"1500px") ;
            $html = ob_get_contents();
            ob_clean();	
            foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) { @unlink($filename); }
                
            //html to xls convert
            $name=time();
            $name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
            $create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
            $is_created = fwrite($create_new_excel,$html);	
            
            echo "$html"."####"."$name";		
            exit();	
		}//end wages_salary_sheet_department
					
		if($types=="wages_salary_sheet_comparative")
		{
			ob_start();
			$sql = "SET @@group_concat_max_len = 9999999";
			$r = @mysql_query ($sql);
		 
			$company_name=$company_id;
			if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
			if ($company_id==0) $company=''; else $company=" and a.company_id='$company_id'";
			if ($department_id==0) $department=""; else	$department=" and a.department_id in($department_id)";
			if ($division_id==0) $division=""; else $division="and  a.division_id  in($division_id)";
			if ($section_id==0 ) $section_id=""; else $section_id="and a.section_id in ($section_id)";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
			if ($location_id==0) $location=""; else $location="and  a.location_id='$location_id'";
			if ($cbo_salary_sheet=='') {$emp_status="";} else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} else $emp_status="and a.emp_status='$cbo_salary_sheet'";
			
			$starting_date = explode('_', $cbo_month_selector ); 
			$salary_date = $starting_date[0];
			$exp_month=explode("-",$salary_date);
			$cur_month = $months["'"+$exp_month[1]+"'"];
			$newdate = strtotime ( '-1 month' , strtotime ( $salary_date ) ) ;
			$before_month_date = date ( 'Y-m-d' , $newdate );
			$b_exp_month=explode("-",$before_month_date);
			$before_month = $months["'"+$b_exp_month[1]+"'"];
	
			$i=0;	
			$company_info_sql="select id,company_name,email,website, plot_no, level_no, road_no,block_no,zip_code,city from lib_company where id='$company_name'";
			$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
			$company_info_result = mysql_fetch_array($result);
			?>
			<div style="border:0px; font-size:18px; width:1200px" align="center"><?	echo $company_info_result["company_name"];?></div>
			<div style="border:0px; font-size:13px; width:1200px" align="center">
                Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"]; echo "Bangladesh";?><br />
                Comparative Salary Statement - <? echo $cur_month; ?> <? echo $exp_month[0]; ?><br />
			</div>
			<?
            $location_arr=array();
            $division_arr=array();
            $department_arr=array();
            ?>                
            <table id="topsheet_tbl" width="1400" cellpadding="0" cellspacing="0" border="1" style="font-size:12px;" class="rpt_table" rules="all">
                <thead>  		
                    <tr>
                        <th width="">&nbsp;</th>
                        <th width="">&nbsp;</th>
                        <th colspan="10"><? echo $before_month; ?></th>
                        <th colspan="10"><? echo $cur_month; ?></th>
                        <th colspan="10">Variance (<? echo $cur_month."-".$before_month; ?>)</th>
                    </tr>	
                    <tr>
                        <th width="50">SL</th>
                        <th width="180">Emp Category</th>
                        <th width="50">Manpower</th>
                        <th width="100">Total OT Amt</th>
                        <th width="100">Tiffin Allowance</th>
                        <th width="100">Attn Bonus</th>
                        <th width="100">T. Salary</th>
                        <th width="80">Late Present</th>
                        <th width="80">ABS Amt</th>
                        <th width="80">LWP</th>
                        <th width="120">T. Deduc</th>
                        <th width="120">Net Pay</th>
                        <th width="50">Manpower</th>
                        <th width="100">Total OT Amt</th>
                        <th width="100">Tiffin Allowance</th>
                        <th width="100">Attn Bonus</th>
                        <th width="100">T. Salary</th>
                        <th width="80">Late Present</th>
                        <th width="80">ABS Amt</th>
                        <th width="80">LWP</th>
                        <th width="120">T. Deduc</th>
                        <th width="120">Net Pay</th>
                    
                        <!-- new comparative header-->
                        <th width="50">Manpower</th>
                        <th width="100">Total OT Amt</th>
                        <th width="100">Tiffin Allowance</th>
                        <th width="100">Attn Bonus</th>
                        <th width="100">T. Salary</th>
                        <th width="80">Late Present</th>
                        <th width="80">ABS Amt</th>
                        <th width="80">LWP</th>
                        <th width="120">T. Deduc</th>
                        <th width="120">Net Pay</th>
                    </tr>   
                </thead>
                <tbody>
				<?
				$subtotal_loc_b_employee=0; $subtotal_loc_b_ot_amnt=0; $subtotal_loc_b_extra_ot_amnt=0;
				$subtotal_loc_b_attn_bonus=0;$subtotal_loc_b_total_salary=0;$subtotal_loc_b_tiff_allow=0;
				$subtotal_loc_b_late_present=0;$subtotal_loc_b_abs_amt=0;$subtotal_loc_b_lwp_amt=0;
				$subtotal_loc_b_total_deduction_amt=0;$subtotal_loc_b_net_pay=0;
				$subtotal_loc_c_employee=0; $subtotal_loc_c_ot_amnt=0; $subtotal_loc_c_extra_ot_amnt=0; 
				$subtotal_loc_c_attn_bonus=0;$subtotal_loc_c_total_salary=0;
				$subtotal_loc_c_late_present=0;$subtotal_loc_c_abs_amt=0;$subtotal_loc_c_lwp_amt=0;
				$subtotal_loc_c_total_deduction_amt=0;$subtotal_loc_c_net_pay=0;$subtotal_loc_c_tiff_allow=0;
	
				$subtotal_div_b_employee=0; $subtotal_div_b_ot_amnt=0; $subtotal_div_b_extra_ot_amnt=0;
				$subtotal_div_b_attn_bonus=0;$subtotal_div_b_total_salary=0;$subtotal_div_b_tiff_allow=0;
				$subtotal_div_b_late_present=0;$subtotal_div_b_abs_amt=0;$subtotal_div_b_lwp_amt=0;
				$subtotal_div_b_total_deduction_amt=0;$subtotal_div_b_net_pay=0;
				$subtotal_div_c_employee=0; $subtotal_div_c_ot_amnt=0; $subtotal_div_c_extra_ot_amnt=0; 
				$subtotal_div_c_attn_bonus=0;$subtotal_div_c_total_salary=0;
				$subtotal_div_c_late_present=0;$subtotal_div_c_abs_amt=0;$subtotal_div_c_lwp_amt=0;
				$subtotal_div_c_total_deduction_amt=0;$subtotal_div_c_net_pay=0;$subtotal_div_c_tiff_allow=0;
	
				$subtotal_b_employee=0; $subtotal_b_ot_amnt=0; $subtotal_b_extra_ot_amnt=0;
				$subtotal_b_attn_bonus=0;$subtotal_b_total_salary=0;$subtotal_b_tiff_allow=0;
				$subtotal_b_late_present=0;$subtotal_b_abs_amt=0;$subtotal_b_lwp_amt=0;
				$subtotal_b_total_deduction_amt=0;$subtotal_b_net_pay=0;
				$subtotal_c_employee=0; $subtotal_c_ot_amnt=0; $subtotal_c_extra_ot_amnt=0; 
				$subtotal_c_attn_bonus=0;$subtotal_c_total_salary=0;
				$subtotal_c_late_present=0;$subtotal_c_abs_amt=0;$subtotal_c_lwp_amt=0;
				$subtotal_c_total_deduction_amt=0;$subtotal_c_net_pay=0;$subtotal_c_tiff_allow=0;
					
				$grandtotal_b_employee=0; $grandtotal_b_ot_amnt=0; $grandtotal_b_extra_ot_amnt=0;
				$grandtotal_b_attn_bonus=0;$grandtotal_b_total_salary=0;$grandtotal_b_tiff_allow=0;
				$grandtotal_b_late_present=0;$grandtotal_b_abs_amt=0;$grandtotal_b_lwp_amt=0;
				$grandtotal_b_total_deduction_amt=0;$grandtotal_b_net_pay=0;
				$grandtotal_c_employee=0; $grandtotal_c_ot_amnt=0; $grandtotal_c_extra_ot_amnt=0; 
				$grandtotal_c_attn_bonus=0;$grandtotal_c_total_salary=0;
				$grandtotal_c_late_present=0;$grandtotal_c_abs_amt=0;$grandtotal_c_lwp_amt=0;
				$grandtotal_c_total_deduction_amt=0;$grandtotal_c_net_pay=0;$grandtotal_c_tiff_allow=0;

				$start=0; 
				$sql_salary = " SELECT group_concat(a.id) as mst_id,a.company_id, a.location_id,a.division_id, a.department_id,a.section_id,a.subsection_id,a.employee_category,
					SUM(CASE WHEN a.salary_periods like '%$salary_date%' THEN 1 else 0 END ) as c_employee,
					SUM(CASE WHEN a.salary_periods like '%$before_month_date%' THEN 1 else 0 END ) as b_employee,
					
					SUM(CASE WHEN a.salary_periods like '%$salary_date%' THEN round(a.total_over_time*a.over_time_rate) else 0 END ) as c_actual_ot,
					  	
					SUM(CASE WHEN a.salary_periods like '%$salary_date%' THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as c_buyer_ot,
					SUM(CASE WHEN a.salary_periods like '%$salary_date%' THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as c_extra_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_earning_amount else 0 END ) as c_total_earning_amount,
					SUM(CASE WHEN salary_periods like '%$before_month_date%' THEN total_earning_amount else 0 END ) as b_total_earning_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as c_total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$before_month_date%' THEN total_absent_deduction_amount else 0 END ) as b_total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_lwp_deduction_amount else 0 END ) as c_total_lwp_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$before_month_date%' THEN total_lwp_deduction_amount else 0 END ) as b_total_lwp_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_deduction_amount else 0 END ) as c_total_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$before_month_date%' THEN total_deduction_amount else 0 END ) as b_total_deduction_amount,
					
					SUM(CASE WHEN a.salary_periods like '%$before_month_date%' THEN round(a.total_over_time*a.over_time_rate) else 0 END ) as b_actual_ot,
					
					SUM(CASE WHEN a.salary_periods like '%$before_month_date%' THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as b_buyer_ot,
					SUM(CASE WHEN a.salary_periods like '%$before_month_date%' THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as b_extra_ot,
					SUM(CASE WHEN a.salary_periods like '%$salary_date%' THEN round(a.net_payable_amount) else 0 END ) as c_net_payable_amount,								
					SUM(CASE WHEN a.salary_periods like '%$before_month_date%' THEN round(a.net_payable_amount) else 0 END ) as b_net_payable_amount,
					SUM(CASE WHEN a.salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as c_staff_buyer_ot,
					SUM(CASE WHEN a.salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as c_staff_extra_ot,
																		
					SUM(CASE WHEN a.salary_periods like '%$before_month_date%' and b.staff_ot_entitled=1 THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as b_staff_buyer_ot,
					SUM(CASE WHEN a.salary_periods like '%$before_month_date%' and b.staff_ot_entitled=1 THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as b_staff_extra_ot,
					
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status=1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as c_staff_ot_for_witheld,
					SUM(CASE WHEN salary_periods like '%$before_month_date%' and b.staff_ot_entitled=1 and a.emp_status=1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as b_staff_ot_for_witheld,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status<>1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as c_staff_ot_for_final_pay,	
					SUM(CASE WHEN salary_periods like '%$before_month_date%' and b.staff_ot_entitled=1 and a.emp_status<>1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as b_staff_ot_for_final_pay
					
					FROM								
					hrm_salary_mst a, hrm_employee b
					WHERE a.emp_code=b.emp_code $emp_status $category $company $location $division  $department $section_id $subsection_id
					group by a.company_id,a.location_id,a.division_id,a.employee_category	
					HAVING c_employee>0						
					";								
						
					//echo "<br/>".$sql_salary;die;
					$sql_salary_result = mysql_query($sql_salary) or die(mysql_error());
					//$numrows=mysql_num_rows($sql_salary_result);
					while($result_row = mysql_fetch_array($sql_salary_result))
					{
						//for staff OT code here....do not change
						//$result_row[c_buyer_ot] = $result_row[c_buyer_ot]- $result_row[c_staff_buyer_ot];
						//$result_row[c_extra_ot] = $result_row[c_extra_ot]- $result_row[c_staff_extra_ot];
						//$result_row[b_buyer_ot] = $result_row[b_buyer_ot]- $result_row[b_staff_buyer_ot];
						//$result_row[b_extra_ot] = $result_row[b_extra_ot]- $result_row[b_staff_extra_ot];
						//for staff OT code here....do not change					
						//$result_row[c_total_earning_amount]=$result_row[c_total_earning_amount]-($result_row[c_staff_buyer_ot]+$result_row[c_staff_extra_ot]);
						//$result_row[b_total_earning_amount]=$result_row[b_total_earning_amount]-($result_row[b_staff_buyer_ot]+$result_row[b_staff_extra_ot]);
						//$result_row[c_net_payable_amount]=$result_row[c_net_payable_amount]-($result_row[c_staff_buyer_ot]+$result_row[c_staff_extra_ot]);
						//$result_row[b_net_payable_amount]=$result_row[b_net_payable_amount]-($result_row[b_staff_buyer_ot]+$result_row[b_staff_extra_ot]);
						
						//---------------------------------------------------------------------------------------------------	
						//---------------------------------------------------------------------------------------------------
						$new_loc=0;$new_divis=0; 
						if( in_array($result_row[location_id],$location_arr) )
						{
							if( !in_array($result_row[division_id],$division_arr) )
							{
								$division_arr[$result_row[division_id]]=$result_row[division_id];
								$new_divis=1;
							}//division end if
						}
						else
						{
							$location_arr[$result_row[location_id]]=$result_row[location_id];
							$division_arr=array();
							$division_arr[$result_row[division_id]]=$result_row[division_id];
							$new_loc=1;
							$new_divis=1;
						}
						 
						if($new_divis==1 && $i!=0)
						{
							?> 
                            <tr style="font-weight:bold; background-color:#FFF"> 
                                <th colspan="2" align="center">Total</th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_employee); ?></th>
                                <!--<th width="" align="right"><? //echo number_format($subtotal_div_b_ot_amnt+$subtotal_div_b_extra_ot_amnt); ?></th>-->
                                <th width="" align="right"><? echo number_format($subtotal_div_b_ot_amnt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_tiff_allow); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_attn_bonus); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_total_salary); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_late_present); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_abs_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_lwp_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_total_deduction_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_b_net_pay); ?></th>
                                    
                                <th width="" align="right"><? echo number_format($subtotal_div_c_employee); ?></th>
                                <!-- <th width="" align="right"><? //echo number_format($subtotal_div_c_ot_amnt+$subtotal_div_c_extra_ot_amnt); ?></th>-->
                                <th width="" align="right"><? echo number_format($subtotal_div_c_ot_amnt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_tiff_allow); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_attn_bonus); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_total_salary); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_late_present); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_abs_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_lwp_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_total_deduction_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_div_c_net_pay); ?></th>
                                    
                                <!--new comparative tot div wise-->
                                <th width="" align="right"><? echo number_format((($subtotal_div_c_employee)*1)-(($subtotal_div_b_employee)*1)); ?></th>
                                <!--<th width="" align="right"><? //echo 
                                //number_format(((($subtotal_div_c_ot_amnt+$subtotal_div_c_extra_ot_amnt)*1)-($subtotal_div_b_ot_amnt+$subtotal_div_b_extra_ot_amnt)*1)); ?>
                                </th>-->
                                <th width="" align="right"><? echo 
                                number_format(((($subtotal_div_c_ot_amnt)*1)-($subtotal_div_b_ot_amnt)*1)); ?>
                                </th>
                                <th width="" align="right"><? echo number_format(((($subtotal_div_c_tiff_allow)*1)-($subtotal_div_b_tiff_allow)*1)); ?></th>
                                <th width="" align="right"><? echo number_format(((($subtotal_div_c_attn_bonus)*1)-($subtotal_div_b_attn_bonus)*1)); ?></th>
                                <th width="" align="right"><? echo number_format(((($subtotal_div_c_total_salary)*1)-($subtotal_div_b_total_salary)*1)); ?></th>
                                <th width="" align="right"><? echo number_format(((($subtotal_div_c_late_present)*1)-($subtotal_div_b_late_present)*1)); ?></th>
                                <th width="" align="right"><? echo number_format(((($subtotal_div_c_abs_amt)*1)-($subtotal_div_b_abs_amt)*1)); ?></th>
                                <th width="" align="right"><? echo number_format(((($subtotal_div_c_lwp_amt)*1)-($subtotal_div_b_lwp_amt)*1)); ?></th>
                                <th width="" align="right"><? echo number_format(((($subtotal_div_c_total_deduction_amt)*1)-($subtotal_div_b_total_deduction_amt)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((((($subtotal_div_c_net_pay)*1)-$subtotal_div_b_net_pay)*1)); ?></th>
							</tr> 
							<?
							$subtotal_div_b_employee=0; $subtotal_div_b_ot_amnt=0; $subtotal_div_b_extra_ot_amnt=0;
							$subtotal_div_b_attn_bonus=0;$subtotal_div_b_total_salary=0;$subtotal_div_b_tiff_allow=0;
							$subtotal_div_b_late_present=0;$subtotal_div_b_abs_amt=0;$subtotal_div_b_lwp_amt=0;
							$subtotal_div_b_total_deduction_amt=0;$subtotal_div_b_net_pay=0;
							$subtotal_div_c_employee=0; $subtotal_div_c_ot_amnt=0; $subtotal_div_c_extra_ot_amnt=0; 
							$subtotal_div_c_attn_bonus=0;$subtotal_div_c_total_salary=0;
							$subtotal_div_c_late_present=0;$subtotal_div_c_abs_amt=0;$subtotal_div_c_lwp_amt=0;
							$subtotal_div_c_total_deduction_amt=0;$subtotal_div_c_net_pay=0;$subtotal_div_c_tiff_allow=0;
						}
							
						if($new_loc==1 && $i!=0)
						{
							?> 
                            <tr style="font-weight:bold; background-color:#FFF"> 
                                <th colspan="2" align="center">Location Total</th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_employee); ?></th>
                                <!-- <th width="" align="right"><? //echo number_format($subtotal_loc_b_ot_amnt+$subtotal_loc_b_extra_ot_amnt); ?></th>-->
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_ot_amnt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_tiff_allow); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_attn_bonus); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_total_salary); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_late_present); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_abs_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_lwp_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_total_deduction_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_b_net_pay); ?></th>
                                    
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
                                <!--<th width="" align="right"><? //echo number_format($subtotal_loc_c_ot_amnt+$subtotal_loc_c_extra_ot_amnt); ?></th>-->
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_ot_amnt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_tiff_allow); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_attn_bonus); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_total_salary); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_late_present); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_abs_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_lwp_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_total_deduction_amt); ?></th>
                                <th width="" align="right"><? echo number_format($subtotal_loc_c_net_pay); ?></th>
                                    
                                <!--new comparative tot loc wise-->
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_employee)*1)-(($subtotal_loc_c_employee)*1)); ?></th>
                                <!--<th width="" align="right"><? //echo 
                                //number_format((($subtotal_loc_b_ot_amnt+$subtotal_loc_b_extra_ot_amnt)*1)-(($subtotal_loc_c_ot_amnt+$subtotal_loc_c_extra_ot_amnt)*1)); ?>
                                </th>-->
                                <th width="" align="right"><? echo 
                                number_format((($subtotal_loc_b_ot_amnt)*1)-(($subtotal_loc_c_ot_amnt)*1)); ?>
                                </th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_tiff_allow)*1)-(($subtotal_loc_c_tiff_allow)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_attn_bonus)*1)-(($subtotal_loc_c_attn_bonus)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_total_salary)*1)-(($subtotal_loc_c_total_salary)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_late_present)*1)-(($subtotal_loc_c_late_present)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_abs_amt)*1)-(($subtotal_loc_c_abs_amt)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_lwp_amt)*1)-(($subtotal_loc_c_lwp_amt)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_total_deduction_amt)*1)-(($subtotal_loc_c_total_deduction_amt)*1)); ?></th>
                                <th width="" align="right"><? echo number_format((($subtotal_loc_b_net_pay)*1)-(($subtotal_loc_c_net_pay)*1)); ?></th>
							</tr> 
							<?
							$subtotal_loc_b_employee=0; $subtotal_loc_b_ot_amnt=0; $subtotal_loc_b_extra_ot_amnt=0;
							$subtotal_loc_b_attn_bonus=0;$subtotal_loc_b_total_salary=0;$subtotal_loc_b_tiff_allow=0;
							$subtotal_loc_b_late_present=0;$subtotal_loc_b_abs_amt=0;$subtotal_loc_b_lwp_amt=0;
							$subtotal_loc_b_total_deduction_amt=0;$subtotal_loc_b_net_pay=0;
							$subtotal_loc_c_employee=0; $subtotal_loc_c_ot_amnt=0; $subtotal_loc_c_extra_ot_amnt=0; 
							$subtotal_loc_c_attn_bonus=0;$subtotal_loc_c_total_salary=0;
							$subtotal_loc_c_late_present=0;$subtotal_loc_c_abs_amt=0;$subtotal_loc_c_lwp_amt=0;
							$subtotal_loc_c_total_deduction_amt=0;$subtotal_loc_c_net_pay=0;$subtotal_loc_c_tiff_allow=0;
						}
							
						//header print here 
						if($new_loc==1)
						{
							?><tr><th colspan="32" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$result_row[location_id]]; ?></th></tr><?
						}
						if($new_divis==1)
						{
							?><tr><th colspan="32" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$result_row[division_id]]; ?></th></tr><?
						}
						//---------------------------------------------------------------------------------------------------
						//---------------------------------------------------------------------------------------------------

						$i++;
						if ($i%2==0) $bgcolor="#EFEFEF";
						else $bgcolor="#FFFFFF";	
						if($result_row[employee_category]==0)$status='Top Mgt.';
						else if($result_row[employee_category]==1)$status='Mid Mgt.';
						else if($result_row[employee_category]==2)$status='Non Mgt.';
						
						 
						$dtls_sql = "select mst.employee_category, group_concat(mst.emp_code) as mst_emp_code,
							SUM(CASE WHEN dtls.salary_head_id=35 and mst.salary_periods like '%$salary_date%' THEN dtls.pay_amount else 0 END ) as c_attn_bonus,
							SUM(CASE WHEN dtls.salary_head_id=35 and mst.salary_periods like '%$before_month_date%' THEN dtls.pay_amount else 0 END ) as b_attn_bonus,
							SUM(CASE WHEN dtls.salary_head_id=23 and mst.salary_periods like '%$salary_date%' THEN dtls.pay_amount else 0 END ) as c_late_present,
							SUM(CASE WHEN dtls.salary_head_id=23 and mst.salary_periods like '%$salary_date%' THEN dtls.pay_amount else 0 END ) as b_late_present,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status=1 THEN (mst.net_payable_amount) else 0 END ) as c_witheldAmt,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status=1 THEN (mst.net_payable_amount) else 0 END ) as b_witheldAmt,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status<>1 THEN (mst.net_payable_amount) else 0 END ) as c_finalPayAmt,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status<>1 THEN (mst.net_payable_amount) else 0 END ) as b_finalPayAmt
							from 
							hrm_salary_mst mst, hrm_salary_dtls dtls 
							where 
							mst.id=dtls.salary_mst_id and mst.id in ($result_row[mst_id]) group by mst.company_id,mst.location_id,mst.division_id,mst.employee_category"; 
							/*mst.id=dtls.salary_mst_id and							
							mst.company_id=$result_row[company_id] and
							mst.location_id=$result_row[location_id] and
							mst.division_id=$result_row[division_id] and	
							mst.employee_category=$result_row[employee_category]";*/
							//echo  $dtls_sql."\n";die;
						$dtls_sql_result = mysql_query($dtls_sql) or die(mysql_error());
						$result_dtls = mysql_fetch_array($dtls_sql_result);
					
						//do not delete---- used for staff ot
						$result_dtls[c_witheldAmt]=round($result_dtls[c_witheldAmt]-$result_row[c_staff_ot_for_witheld]);
						$result_dtls[b_witheldAmt]=round($result_dtls[b_witheldAmt]-$result_row[b_staff_ot_for_witheld]);
						$result_dtls[c_finalPayAmt]=round($result_dtls[c_finalPayAmt]-$result_row[c_staff_ot_for_final_pay]);
						$result_dtls[b_finalPayAmt]=round($result_dtls[b_finalPayAmt]-$result_row[b_staff_ot_for_final_pay]);
					
						$c_tiff_alloe=return_field_value("sum(dtls.amount)","hrm_salary_mst mst, hrm_employee_salary dtls","mst.emp_code=dtls.emp_code and mst.salary_periods like '%$salary_date%' and mst.emp_code in ($result_dtls[mst_emp_code]) and dtls.payroll_head=9 and mst.emp_status !=1 and mst.employee_category=$result_dtls[employee_category] and mst.company_id=$result_row[company_id] and mst.location_id=$result_row[location_id] and mst.division_id=$result_row[division_id]");
						$b_tiff_allow=return_field_value("sum(dtls.amount)","hrm_salary_mst mst, hrm_employee_salary dtls","mst.emp_code=dtls.emp_code and mst.salary_periods like '%$before_month_date%' and mst.emp_code in ($result_dtls[mst_emp_code]) and dtls.payroll_head=9 and mst.emp_status !=1 and mst.employee_category=$result_dtls[employee_category] and mst.company_id=$result_row[company_id] and mst.location_id=$result_row[location_id] and mst.division_id=$result_row[division_id]");

						//$c_salary_amt = $result_row[c_net_payable_amount]-$result_row[c_buyer_ot]-$result_row[c_extra_ot]-$result_dtls[c_attn_bonus];
						//$b_salary_amt = $result_row[b_net_payable_amount]-$result_row[b_buyer_ot]-$result_row[b_extra_ot]-$result_dtls[b_attn_bonus];
						
						$c_salary_amt = $result_row[c_net_payable_amount];
						$b_salary_amt = $result_row[b_net_payable_amount];
						?>
                        <tr align="right" bgcolor="<? echo $bgcolor; ?>" onMouseOver="fn_color('topsheet_tbl')"  > <!-- onmouseover="fn_color('topsheet_tbl')" -->
                            <td width=""><? echo $i; ?></td>
                            <td width="" align="left"><? echo $status; ?></td>
                            <td width=""><? echo number_format($result_row[c_employee]); ?></td>
                            <!--<td width=""><? //echo number_format($result_row[c_buyer_ot]+$result_row[c_extra_ot]); ?></td>-->
                            <td width=""><? echo number_format($result_row[c_actual_ot]); ?></td>
                            <td width=""><? echo number_format($b_tiff_allow); ?></td>
                            <td width=""><? echo number_format($result_dtls[c_attn_bonus]); ?></td>
                            <td width=""><? echo number_format($result_row[c_total_earning_amount]);?></td>
                            <td width=""><? echo number_format($result_row[c_late_present]);?></td>
                            <td width=""><? echo number_format($result_row[c_total_absent_deduction_amount]);?></td>
                            <td width=""><? echo number_format($result_row[c_total_lwp_deduction_amount]);?></td>
                            <td width=""><? echo number_format($result_row[c_total_deduction_amount]);?></td>
                            <td width=""><? echo number_format($result_row[c_net_payable_amount]);?></td>
                            
                            <td width=""><? echo number_format($result_row[b_employee]); ?></td>
                            <!--<td width=""><? //echo number_format($result_row[b_buyer_ot]+$result_row[b_extra_ot]); ?></td>-->
                            <td width=""><? echo number_format($result_row[b_buyer_ot]); ?></td>
                            <td width=""><? echo number_format($b_tiff_allow); ?></td>
                            <td width=""><? echo number_format($result_dtls[b_attn_bonus]); ?></td>
                            <td width=""><? echo number_format($result_row[b_total_earning_amount]);?></td>
                            <td width=""><? echo number_format($result_row[b_late_present]);?></td>
                            <td width=""><? echo number_format($result_row[b_total_absent_deduction_amount]);?></td>
                            <td width=""><? echo number_format($result_row[b_total_lwp_deduction_amount]);?></td>
                            <td width=""><? echo number_format($result_row[b_total_deduction_amount]);?></td>
                            <td width=""><? echo number_format($result_row[b_net_payable_amount]);?></td>
    
                            <!--new comparative man-->
                            <td width=""><? echo number_format((($result_row[c_employee])*1))-(($result_row[b_employee])*1); ?></td>
                            <!--<td width=""><? //echo number_format((($result_row[c_buyer_ot]+$result_row[c_extra_ot])*1))-((($result_row[b_buyer_ot]+$result_row[b_extra_ot])*1)); ?></td>-->
                            <td width=""><? echo number_format((($result_row[c_actual_ot])*1))-((($result_row[b_actual_ot])*1)); ?></td>
                            <td width=""><? echo number_format((($c_tiff_allow)*1)-(($b_tiff_allow)*1)); ?></td>
                            <td width=""><? echo number_format((($result_dtls[c_attn_bonus])*1)-(($result_dtls[b_attn_bonus])*1)); ?></td>
                            <td width=""><? echo number_format((($result_row[c_total_earning_amount])*1)-(($result_row[b_total_earning_amount])*1)); ?></td>
                            <td width=""><? echo number_format((($result_row[c_late_present])*1)-(($result_row[b_late_present])*1)); ?></td>
                            <td width=""><? echo number_format((($result_row[c_total_absent_deduction_amount])*1)-(($result_row[b_total_absent_deduction_amount])*1)); ?></td>
                            <td width=""><? echo number_format((($result_row[c_total_lwp_deduction_amount])*1)-(($result_row[b_total_lwp_deduction_amount])*1)); ?></td>
                            <td width=""><? echo number_format((($result_row[c_total_deduction_amount])*1)-(($result_row[b_total_deduction_amount])*1)); ?></td>
                            <td width=""><? echo number_format((($result_row[c_net_payable_amount])*1)-(($result_row[b_net_payable_amount])*1)); ?></td>
                      	</tr>   
						<?	
						//sub division total calculation
						$subtotal_div_b_employee+=$result_row[b_employee]; 
						/*$subtotal_div_b_ot_amnt+=$result_row[b_buyer_ot]+$result_row[b_extra_ot];*/
						$subtotal_div_b_ot_amnt+=$result_row[b_actual_ot]; 
						$subtotal_div_b_attn_bonus+=$result_dtls[b_attn_bonus];
						$subtotal_div_b_total_salary+=$result_row[b_total_earning_amount];
						$subtotal_div_b_tiff_allow+=$b_tiff_allow;
						$subtotal_div_b_late_present+=$result_row[b_late_present];
						$subtotal_div_b_abs_amt+=$result_row[b_total_absent_deduction_amount];
						$subtotal_div_b_lwp_amt+=$result_row[b_total_lwp_deduction_amount];
						$subtotal_div_b_total_deduction_amt+=$result_row[b_total_deduction_amount];
						$subtotal_div_b_net_pay=$result_row[b_net_payable_amount];
						$subtotal_div_c_employee+=$result_row[c_employee];
						/*$subtotal_div_c_ot_amnt+=$result_row[c_buyer_ot]+$result_row[c_extra_ot];*/
						$subtotal_div_c_ot_amnt+=$result_row[c_actual_ot];
						$subtotal_div_c_tiff_allow+=$c_tiff_allow;
						$subtotal_div_c_attn_bonus+=$result_dtls[c_attn_bonus];
						$subtotal_div_c_total_salary+=$result_row[c_total_earning_amount];
						$subtotal_div_c_late_present+=$result_row[c_late_present];
						$subtotal_div_c_abs_amt+=$result_row[c_total_absent_deduction_amount];
						$subtotal_div_c_lwp_amt+=$result_row[c_total_lwp_deduction_amount];
						$subtotal_div_c_total_deduction_amt+=$result_row[c_total_deduction_amount];
						$subtotal_div_c_net_pay+=$result_row[c_net_payable_amount];

						//sub locision total calculation
						$subtotal_loc_b_employee+=$result_row[b_employee]; 
						/*$subtotal_loc_b_ot_amnt+=$result_row[b_buyer_ot]+$result_row[b_extra_ot];*/ 
						$subtotal_loc_b_ot_amnt+=$result_row[b_actual_ot];
						$subtotal_loc_b_attn_bonus+=$result_dtls[b_attn_bonus];
						$subtotal_loc_b_total_salary+=$result_row[b_total_earning_amount];
						$subtotal_loc_b_tiff_allow+=$b_tiff_allow;
						$subtotal_loc_b_late_present+=$result_row[b_late_present];
						$subtotal_loc_b_abs_amt+=$result_row[b_total_absent_deduction_amount];
						$subtotal_loc_b_lwp_amt+=$result_row[b_total_lwp_deduction_amount];
						$subtotal_loc_b_total_deduction_amt+=$result_row[b_total_deduction_amount];
						$subtotal_loc_b_net_pay=$result_row[b_net_payable_amount];
						$subtotal_loc_c_employee+=$result_row[c_employee];
						/*$subtotal_loc_c_ot_amnt+=$result_row[c_buyer_ot]+$result_row[c_extra_ot];*/
						$subtotal_loc_c_ot_amnt+=$result_row[c_actual_ot];
						$subtotal_loc_c_tiff_allow+=$c_tiff_allow;
						$subtotal_loc_c_attn_bonus+=$result_dtls[c_attn_bonus];
						$subtotal_loc_c_total_salary+=$result_row[c_total_earning_amount];
						$subtotal_loc_c_late_present+=$result_row[c_late_present];
						$subtotal_loc_c_abs_amt+=$result_row[c_total_absent_deduction_amount];
						$subtotal_loc_c_lwp_amt+=$result_row[c_total_lwp_deduction_amount];
						$subtotal_loc_c_total_deduction_amt+=$result_row[c_total_deduction_amount];
						$subtotal_loc_c_net_pay+=$result_row[c_net_payable_amount];
						
						//grand calculation
						$grandtotal_b_employee+=$result_row[b_employee]; 
						/*$grandtotal_b_ot_amnt+=$result_row[b_buyer_ot]+$result_row[b_extra_ot]; */
						$grandtotal_b_ot_amnt+=$result_row[b_actual_ot]; 
						$grandtotal_b_attn_bonus+=$result_dtls[b_attn_bonus];
						$grandtotal_b_total_salary+=$result_row[b_total_earning_amount];
						$grandtotal_b_tiff_allow+=$b_tiff_allow;
						$grandtotal_b_late_present+=$result_row[b_late_present];
						$grandtotal_b_abs_amt+=$result_row[b_total_absent_deduction_amount];
						$grandtotal_b_lwp_amt+=$result_row[b_total_lwp_deduction_amount];
						$grandtotal_b_total_deduction_amt+=$result_row[b_total_deduction_amount];
						$grandtotal_b_net_pay=$result_row[b_net_payable_amount];
						$grandtotal_c_employee+=$result_row[c_employee];
						/*$grandtotal_c_ot_amnt+=$result_row[c_buyer_ot]+$result_row[c_extra_ot];*/
						$grandtotal_c_ot_amnt+=$result_row[c_actual_ot];
						$grandtotal_c_tiff_allow+=$c_tiff_allow;
						$grandtotal_c_attn_bonus+=$result_dtls[c_attn_bonus];
						$grandtotal_c_total_salary+=$result_row[c_total_earning_amount];
						$grandtotal_c_late_present+=$result_row[c_late_present];
						$grandtotal_c_abs_amt+=$result_row[c_total_absent_deduction_amount];
						$grandtotal_c_lwp_amt+=$result_row[c_total_lwp_deduction_amount];
						$grandtotal_c_total_deduction_amt+=$result_row[c_total_deduction_amount];
						$grandtotal_c_net_pay+=$result_row[c_net_payable_amount];
						$start++;
					}
					?>	
					<tr style="font-weight:bold; background-color:#FFF">
                        <th colspan="2" align="center">Total</th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_employee); ?></th>
                        <!-- <th width="" align="right"><? //echo number_format($subtotal_div_c_ot_amnt+$subtotal_div_c_extra_ot_amnt); ?></th>-->
                        <th width="" align="right"><? echo number_format($subtotal_div_c_ot_amnt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_tiff_allow); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_attn_bonus); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_total_salary); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_late_present); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_abs_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_lwp_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_total_deduction_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_c_net_pay); ?></th>
                        
                        <th width="" align="right"><? echo number_format($subtotal_div_b_employee); ?></th>
                        <!-- <th width="" align="right"><? //echo number_format($subtotal_div_b_ot_amnt+$subtotal_div_b_extra_ot_amnt); ?></th>-->
                        <th width="" align="right"><? echo number_format($subtotal_div_b_ot_amnt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_tiff_allow); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_attn_bonus); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_total_salary); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_late_present); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_abs_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_lwp_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_total_deduction_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_div_b_net_pay); ?></th>
                        <!--new comparative tot div wise-->
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_employee)*1)-(($subtotal_div_b_employee)*1)); ?></th>
                        <!--<th width="" align="right"><? //echo 
                        //number_format((($subtotal_div_c_ot_amnt+$subtotal_div_c_extra_ot_amnt)*1)-(($subtotal_div_b_ot_amnt+$subtotal_div_b_extra_ot_amnt)*1)); ?>
                        </th>-->
                        <th width="" align="right"><? echo 
                        number_format((($subtotal_div_c_ot_amnt)*1)-(($subtotal_div_b_ot_amnt)*1)); ?>
                        </th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_tiff_allow)*1)-(($subtotal_div_b_tiff_allow)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_attn_bonus)*1)-(($subtotal_div_b_attn_bonus)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_total_salary)*1)-(($subtotal_div_b_total_salary)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_late_present)*1)-(($subtotal_div_b_late_present)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_abs_amt)*1)-(($subtotal_div_b_abs_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_lwp_amt)*1)-(($subtotal_div_b_lwp_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_total_deduction_amt)*1)-(($subtotal_div_b_total_deduction_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_div_c_net_pay)*1)-(($subtotal_div_b_net_pay)*1)); ?></th>
					</tr>
                    <tr style="font-weight:bold; background-color:#FFF">
                        <th colspan="2" align="center">Location Total</th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
                        <!--<th width="" align="right"><? //echo number_format($subtotal_loc_c_ot_amnt+$subtotal_loc_c_extra_ot_amnt); ?></th>-->
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_ot_amnt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_tiff_allow); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_attn_bonus); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_total_salary); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_late_present); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_abs_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_lwp_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_total_deduction_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_c_net_pay); ?></th>

                        <th width="" align="right"><? echo number_format($subtotal_loc_b_employee); ?></th>
                        <!-- <th width="" align="right"><? //echo number_format($subtotal_loc_b_ot_amnt+$subtotal_loc_b_extra_ot_amnt); ?></th>-->
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_ot_amnt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_tiff_allow); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_attn_bonus); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_total_salary); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_late_present); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_abs_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_lwp_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_total_deduction_amt); ?></th>
                        <th width="" align="right"><? echo number_format($subtotal_loc_b_net_pay); ?></th>
                        <!--new comparative tot loc wise-->
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_employee)*1)-(($subtotal_loc_b_employee)*1)); ?></th>
                        <!--<th width="" align="right"><? //echo 
                        //number_format((($subtotal_loc_c_ot_amnt+$subtotal_loc_c_extra_ot_amnt)*1)-(($subtotal_loc_b_ot_amnt+$subtotal_loc_b_extra_ot_amnt)*1)); ?>
                        </th>-->
                        <th width="" align="right"><? echo 
                        number_format((($subtotal_loc_c_ot_amnt)*1)-(($subtotal_loc_b_ot_amnt)*1)); ?>
                        </th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_tiff_allow)*1)-(($subtotal_loc_b_tiff_allow)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_attn_bonus)*1)-(($subtotal_loc_b_attn_bonus)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_total_salary)*1)-(($subtotal_loc_b_total_salary)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_late_present)*1)-(($subtotal_loc_b_late_present)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_abs_amt)*1)-(($subtotal_loc_b_abs_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_lwp_amt)*1)-(($subtotal_loc_b_lwp_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_total_deduction_amt)*1)-(($subtotal_loc_b_total_deduction_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($subtotal_loc_c_net_pay)*1)-(($subtotal_loc_b_net_pay)*1)); ?></th>
                    </tr>
                    <tr style="font-weight:bold; background-color:#FFF">
                        <th colspan="2" align="center">Grand Total</th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_employee); ?></th>
                        <!-- <th width="" align="right"><? //echo number_format($grandtotal_c_ot_amnt+$grandtotal_c_extra_ot_amnt); ?></th>-->
                        <th width="" align="right"><? echo number_format($grandtotal_c_ot_amnt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_tiff_allow); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_attn_bonus); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_total_salary); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_late_present); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_abs_amt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_lwp_amt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_total_deduction_amt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_c_net_pay); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_employee); ?></th>
                        <!-- <th width="" align="right"><? //echo number_format($grandtotal_b_ot_amnt+$grandtotal_b_extra_ot_amnt); ?></th>-->
                        <th width="" align="right"><? echo number_format($grandtotal_b_ot_amnt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_tiff_allow); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_attn_bonus); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_total_salary); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_late_present); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_abs_amt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_lwp_amt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_total_deduction_amt); ?></th>
                        <th width="" align="right"><? echo number_format($grandtotal_b_net_pay); ?></th>
						<!--new comparative tot loc wise-->
                        <th width="" align="right"><? echo number_format((($grandtotal_b_employee)*1)-(($grandtotal_c_employee)*1)); ?></th>
                        <!--<th width="" align="right"><? //echo 
                        //number_format((($grandtotal_b_ot_amnt+$grandtotal_b_extra_ot_amnt)*1)-(($grandtotal_c_ot_amnt+$grandtotal_c_extra_ot_amnt)*1)); ?>
                        </th>-->
                        <th width="" align="right"><? echo 
                        number_format((($grandtotal_b_ot_amnt)*1)-(($grandtotal_c_ot_amnt)*1)); ?>
                        </th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_tiff_allow)*1)-(($grandtotal_c_tiff_allow)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_attn_bonus)*1)-(($grandtotal_c_attn_bonus)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_total_salary)*1)-(($grandtotal_c_total_salary)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_late_present)*1)-(($grandtotal_c_late_present)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_abs_amt)*1)-(($grandtotal_c_abs_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_lwp_amt)*1)-(($grandtotal_c_lwp_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_total_deduction_amt)*1)-(($grandtotal_c_total_deduction_amt)*1)); ?></th>
                        <th width="" align="right"><? echo number_format((($grandtotal_b_net_pay)*1)-(($grandtotal_c_net_pay)*1)); ?></th>
					</tr>
                </tbody>       
            </table><br />
            <div align="center" style="width:1200px">
                <table style="float:left;margin-left:300px" width="200" class="rpt_table" border="1" rules="all">
                    <thead>
                        <tr><th colspan="2">Manpower</th></tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="120"><? echo $before_month; ?> <? echo $b_exp_month[0]; ?></td>
                            <td width="80"><? echo number_format($grandtotal_b_employee); ?></td>
                        </tr>
                        <tr>
                            <td><? echo $cur_month; ?> <? echo $exp_month[0]; ?></td>
                            <td><? echo number_format($grandtotal_c_employee); ?></td>
                        </tr>
                        <tr>
            
                            <td>Changed By</td>
                            <td><? $diff = $grandtotal_c_employee-$grandtotal_b_employee; echo number_format($diff); ?></td>
                        </tr>
                        <tr>
                            <td>%</td>
                            <td><? $per_change =($diff/$grandtotal_b_employee)*100; echo number_format($per_change,2); ?></td>
                        </tr>
                    </tbody>
                </table>
                <table style="float:left; margin-left:50px" width="200" class="rpt_table" border="1" rules="all">
                    <thead>
                        <tr> <th colspan="2">Salary and Other Earnings</th> </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="120"><? echo $before_month; ?> <? echo $b_exp_month[0]; ?></td>
                            <td width="80"><? echo number_format($grandtotal_b_net_pay); ?></td>
                        </tr>
                        <tr>
                            <td><? echo $cur_month; ?> <? echo $exp_month[0]; ?></td>
                            <td><? echo number_format($grandtotal_c_net_pay); ?></td>
                        </tr>
                        <tr>
                            <td>Changed By</td>
                            <td><? $diff = $grandtotal_c_net_pay-$grandtotal_b_net_pay; echo number_format($diff); ?></td>
                        </tr>
                        <tr>
                            <td>%</td>
                            <td><? $per_change =($diff/$grandtotal_b_net_pay)*100; echo number_format($per_change,2); ?></td>
                        </tr>
                    </tbody>
                </table>
                <div style="clear:both"></div>
            </div>    
			<?	
            echo signeture_table(10,$company_name,"1500px") ;
            $html = ob_get_contents();
            ob_clean();	
            foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) { @unlink($filename); }		
                
            //html to xls convert
            $name=time();
            $name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
            $create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
            $is_created = fwrite($create_new_excel,$html);	
            
            echo "$html"."####"."$name";		
            exit();	
        }
					
		if($types=="wages_salary_sheet_division")
		{
			ob_start();
			$company_name=$company_id;
			if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
			if ($company_id==0) $company=''; else $company=" and a.company_id='$company_id'";
			if ($department_id==0) $department=""; else	$department=" and a.department_id in($department_id)";
			if ($division_id==0) $division=""; else $division="and  a.division_id  in($division_id)";
			if ($section_id==0 ) $section_id=""; else $section_id="and a.section_id in ($section_id)";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
			if ($location_id==0) $location=""; else $location="and  a.location_id='$location_id'";
			if ($cbo_salary_sheet=='') {$emp_status="";} else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} else $emp_status="and a.emp_status='$cbo_salary_sheet'";
			
			$starting_date = explode('_', $cbo_month_selector ); 
			$salary_date = $starting_date[0];
			$exp_month=explode("-",$salary_date);
			$cur_month = $months["'"+$exp_month[1]+"'"];
			$i=0;	
			
			$company_info_sql="select id,company_name,email,website, plot_no, level_no, road_no,block_no,zip_code,city from lib_company where id='$company_name'";
			$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
			$company_info_result = mysql_fetch_array($result);
			
			?>

			<div style="border:0px; font-size:18px;" align="center"><?	echo $company_info_result["company_name"];?></div>
			<div style="border:0px; font-size:13px" align="center">
			Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"]; echo "Bangladesh";?><br />
			Salary Top Sheet For The Month Of <? echo $cur_month; ?> <? echo $exp_month[0]; ?><br />
			</div>

			<?
            $location_arr=array();
            $division_arr=array();
            $department_arr=array();
            ?>                
            <table id="topsheet_tbl" width="1500" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;" class="rpt_table" rules="all">
                <thead>  		
                    <tr>
                        <th width="50">SL</th>
                        <th width="180">Emp Category</th>
                        <th width="50">Manpower</th>
                        <th width="120">G. Salary</th>
                        <th width="100">Total OT Amt</th>
                        <th width="100">Tiffin Allowance</th>
                        <th width="100">Attn Bonus</th>
                        <th width="100">T. Salary</th>
                        <th width="80">ADV. Sal</th>
                        <th width="80">Late Present</th>
                        <th width="80">ABS Amt</th>
                        <th width="80">Stamp</th>
                        <th width="80">LWP</th>
                        <th width="90">Others Deduc</th>
                        <th width="90">Savings Fund</th>
                        <th width="120">T. Deduc</th>
                        <th width="120">Net Pay</th>
                        <th width="">Remarks</th>
                    </tr>   
                </thead>
                <tbody>
                <?
				$subtotal_loc_c_employee=0;$subtotal_loc_t_salary=0;$subtotal_loc_adv_salary=0;$subtotal_loc_abs_amnt=0;$subtotal_loc_np_amnt=0;
				$subtotal_loc_other_deduc=0;$subtotal_loc_stamp=0;$subtotal_loc_tax=0;$subtotal_loc_net_pay=0;$subtotal_loc_withheld=0;
				$subtotal_loc_final_pay=0;$subtotal_loc_in_bank=0;$subtotal_loc_in_cash=0;$subtotal_loc_trans_alw=0;$subtotal_loc_tiff_alw=0;
				$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;	 
				
				$subtotal_div_c_employee=0;$subtotal_div_t_salary=0;$subtotal_div_adv_salary=0;$subtotal_div_abs_amnt=0;$subtotal_div_np_amnt=0;
				$subtotal_div_other_deduc=0;$subtotal_div_stamp=0;$subtotal_div_tax=0;$subtotal_div_net_pay=0;$subtotal_div_withheld=0;
				$subtotal_div_final_pay=0;$subtotal_div_in_bank=0;$subtotal_div_in_cash=0;$subtotal_div_trans_alw=0;$subtotal_div_tiff_alw=0;
				$subtotal_div_lwp_deduction_amount=0;$subtotal_div_pf_amount=0;
				
				$grandtotal_c_employee=0;$grandtotal_t_salary=0;$grandtotal_adv_salary=0;$grandtotal_abs_amnt=0;$grandtotal_np_amnt=0;
				$grandtotal_other_deduc=0;$grandtotal_stamp=0;$grandtotal_tax=0;$grandtotal_net_pay=0;$grandtotal_withheld=0;
				$grandtotal_final_pay=0;$grandtotal_in_bank=0;$grandtotal_in_cash=0;$grandtotal_trans_alw=0;$grandtotal_tiff_alw=0;$grandtotal_late_salary=0;
				$grandtotal_lwp_deduction_amount=0;$grandtotal_div_pf_amount=0;
				
				//echo "Sss";
				$start=0; 
				$sql_salary = "select group_concat(a.id) as mst_id, a.company_id, a.location_id, a.division_id, a.department_id, a.section_id, a.subsection_id, a.employee_category, a.emp_status,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN 1 else 0 END ) as c_employee,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN a.gross_salary else 0 END ) as g_salary,
					
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.total_over_time*a.over_time_rate) else 0 END ) as actual_ot,
					
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as b_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as e_ot, 
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_earning_amount else 0 END ) as total_earning_amount,								
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN not_payble_days else 0 END ) as not_payble_days,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN daily_basic_salary else 0 END ) as daily_basic_salary,								
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(total_deduction_amount) else 0 END ) as total_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN net_payable_amount else 0 END ) as net_payable_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.not_payable_deduction_amount) else 0 END ) as total_not_payable_amt,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.bank_gross) else 0 END ) as emp_bank_gross,								
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as staff_buyer_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as staff_extra_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status=1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as staff_ot_for_witheld,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status<>1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as staff_ot_for_final_pay
					from 
					hrm_salary_mst a, hrm_employee b
					where a.emp_code=b.emp_code and a.salary_periods = '$salary_date' 
					$emp_status $category $company $location $division $department $section_id $subsection_id
					group by a.company_id,a.location_id,a.division_id,a.employee_category
					having c_employee>0";
					//$department
					//SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					//SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					//
					//echo "<br/>".$sql_salary;die; 
					$sql_salary_result = mysql_query($sql_salary) or die(mysql_error());
					//$numrows=mysql_num_rows($sql_salary_result);
					while($result_row = mysql_fetch_array($sql_salary_result))
					{
					
						//for staff OT code here....do not change
						//$result_row[b_ot] = $result_row[b_ot]- $result_row[staff_buyer_ot];
						//$result_row[e_ot] = $result_row[e_ot]- $result_row[staff_extra_ot];
						//$result_row[total_earning_amount]=$result_row[total_earning_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						//$result_row[net_payable_amount]=$result_row[net_payable_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						
						//---------------------------------------------------------------------------------------------------	
						//---------------------------------------------------------------------------------------------------
					
						$new_loc=0;$new_divis=0;$new_dept=0;
						if(in_array($result_row[location_id],$location_arr))
						{
								if(in_array($result_row[division_id],$division_arr))
								{
										/*if(in_array($result_row[department_id],$department_arr))
										{
										}
										else
										{
											$department_arr[$result_row[department_id]]=$result_row[department_id];
											$new_dept=1;
										}*/
								}//division
								else
								{
										$division_arr[$result_row[division_id]]=$result_row[division_id];
										$department_arr = array();
										$department_arr[$result_row[department_id]]=$result_row[department_id];
										$new_divis=1;
										$new_dept=1;
								}//division else
						}//location
						else
						{
							$location_arr[$result_row[location_id]]=$result_row[location_id];
							$division_arr = array();
							$department_arr = array();
							$division_arr[$result_row[division_id]]=$result_row[division_id];
							$department_arr[$result_row[department_id]]=$result_row[department_id];
							$new_loc=1;
							$new_divis=1;
							$new_dept=1;
						}//location else
					
					if($new_divis==1 && $start!=0)
					{
						 
						?><tr style="font-weight:bold; background-color:#FFF">
								<th colspan="2" align="center">Division Total</th>
								<th width="" align="right"><? echo number_format($subtotal_div_c_employee); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_g_salary); ?></th>
							   <!-- <th width="" align="right"><? //echo number_format($subtotal_div_ot_amnt+$subtotal_div_extra_ot_amnt); ?></th>-->
								<th width="" align="right"><? echo number_format($subtotal_div_ot_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_tiff_alw); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_attn_bonus); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_t_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_adv_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_late_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_abs_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_stamp); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_lwp_deduction_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_other_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_pf_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_total_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_net_pay); ?></th>
								<th width="" align="right"></th>
						
						</tr><?
						
						$subtotal_div_c_employee=0; $subtotal_div_g_salary=0; $subtotal_div_ot_amnt=0; $subtotal_div_extra_ot_amnt=0; $subtotal_div_attn_bonus=0; 
						$subtotal_div_arrear_ern=0; $subtotal_div_t_salary=0; $subtotal_div_adv_salary=0; $subtotal_div_abs_amnt=0; $subtotal_div_np_amnt=0;
						$subtotal_div_other_deduc=0;  $subtotal_div_stamp=0; $subtotal_div_tax=0; $subtotal_div_total_deduc=0; $subtotal_div_net_pay=0;
						$subtotal_div_withheld=0; $subtotal_div_final_pay=0; $subtotal_div_in_bank=0; $subtotal_div_in_cash=0; $subtotal_div_trans_alw=0;
						$subtotal_div_tiff_alw=0;$subtotal_div_late_salary=0;$subtotal_div_lwp_deduction_amount=0;$subtotal_div_pf_amount=0;
						//$department_arr = array();
						
					}
					
					
					if($new_loc==1 && $start!=0)
					{					
						
						?><tr style="font-weight:bold; background-color:#FFF">
								<th colspan="2" align="center">Location Total</th>
								<th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
							   <!-- <th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
								<th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
								<th width="" align="right"></th>
						
						</tr><?
					
						
						$subtotal_loc_c_employee=0; $subtotal_loc_g_salary=0; $subtotal_loc_ot_amnt=0; $subtotal_loc_extra_ot_amnt=0; $subtotal_loc_attn_bonus=0; 
						$subtotal_loc_arrear_ern=0; $subtotal_loc_t_salary=0; $subtotal_loc_adv_salary=0; $subtotal_loc_abs_amnt=0; $subtotal_loc_np_amnt=0;
						$subtotal_loc_other_deduc=0;  $subtotal_loc_stamp=0; $subtotal_loc_tax=0; $subtotal_loc_total_deduc=0; $subtotal_loc_net_pay=0;
						$subtotal_loc_withheld=0; $subtotal_loc_final_pay=0; $subtotal_loc_in_bank=0; $subtotal_loc_in_cash=0; $subtotal_loc_trans_alw=0;
						 $subtotal_loc_tiff_alw=0;$subtotal_loc_late_salary=0;$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;
							
						//$division_arr = array();
						//$department_arr = array();
					}	
					
					
					//header print here 
					if($new_loc==1)
					{					
						?><tr><th colspan="23" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$result_row[location_id]]; ?></th></tr><?
					}				
					if($new_divis==1)
					{
						?><tr><th colspan="23" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$result_row[division_id]]; ?></th></tr><?
					}
					?>
					<?
					
					//---------------------------------------------------------------------------------------------------
					//---------------------------------------------------------------------------------------------------
					
					
						$i++;
						
						if ($i%2==0)$bgcolor="#EFEFEF";  
						else $bgcolor="#FFFFFF";
								
						if($result_row[employee_category]==0)$status='Top Mgt.';
						else if($result_row[employee_category]==1)$status='Mid Mgt.';
						else if($result_row[employee_category]==2)$status='Non Mgt.';
						
						 
						$dtls_sql = "select  group_concat(mst.emp_code) as mst_emp_code,mst.employee_category,
								SUM(CASE WHEN dtls.salary_head_id=25 THEN dtls.pay_amount else 0 END ) as c_total_pf_amount,
								SUM(CASE WHEN dtls.salary_head_id=23 THEN dtls.pay_amount else 0 END ) as c_total_late_deduction_amount,
								SUM(CASE WHEN dtls.salary_head_id=29 THEN dtls.pay_amount else 0 END ) as c_total_lwp_deduction_amount,
								SUM(CASE WHEN dtls.salary_head_id=24 THEN dtls.pay_amount else 0 END ) as c_absent,
								SUM(CASE WHEN dtls.salary_head_id=35 THEN dtls.pay_amount else 0 END ) as c_attn_bonus,
								SUM(CASE WHEN dtls.salary_head_id=17 THEN dtls.pay_amount else 0 END ) as c_arrear_earn,
								SUM(CASE WHEN dtls.salary_head_id=21 THEN dtls.pay_amount else 0 END ) as c_advancedSalary,
								SUM(CASE WHEN dtls.salary_head_id=31 THEN dtls.pay_amount else 0 END ) as c_otherDeduc,
								SUM(CASE WHEN dtls.salary_head_id=28 THEN dtls.pay_amount else 0 END ) as c_stamp,
								SUM(CASE WHEN dtls.salary_head_id=22 THEN dtls.pay_amount else 0 END ) as c_tax,
								SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status=1 THEN (mst.net_payable_amount) else 0 END ) as witheldAmt,
								SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status<>1 THEN (mst.net_payable_amount) else 0 END ) as finalPayAmt
								from 
								hrm_salary_mst mst, hrm_salary_dtls dtls 
								where 
								mst.id=dtls.salary_mst_id and mst.id in ($result_row[mst_id]) group by mst.company_id,mst.location_id,mst.division_id,mst.employee_category";	
								/*mst.salary_periods = '$salary_date' and 
								mst.company_id=$result_row[company_id] and
								mst.location_id=$result_row[location_id] and
								mst.division_id=$result_row[division_id] and	
								mst.department_id=$result_row[department_id] and  				 
								mst.employee_category=$result_row[employee_category]*/
					 //echo  $dtls_sql."\n";die;
					$dtls_sql_result = mysql_query($dtls_sql) or die(mysql_error());
					$result_dtls = mysql_fetch_array($dtls_sql_result);
					
					//do not delete---- used for staff ot
					$result_dtls[witheldAmt]=round($result_dtls[witheldAmt]-$result_row[staff_ot_for_witheld]);
					$result_dtls[finalPayAmt]=round($result_dtls[finalPayAmt]-$result_row[staff_ot_for_final_pay]);
					
				?>
				   
				  <tr align="right" bgcolor="<? echo $bgcolor; ?>" onMouseOver="fn_color('topsheet_tbl')">
						<td width=""><? echo $i; ?></td>
						<td width="" align="left"><? echo $status; ?></td>
						<td width=""><? echo number_format($result_row[c_employee]); ?></td>
						<td width=""><? echo number_format($result_row[g_salary]); ?></td>
						<td width="">&nbsp;</td>
						<?
						$tiffinAllow=return_field_value("sum(dtls.amount)","hrm_salary_mst mst, hrm_employee_salary dtls","mst.emp_code=dtls.emp_code and mst.salary_periods like '%$salary_date%' and mst.emp_code in ($result_dtls[mst_emp_code]) and dtls.payroll_head=9 and mst.emp_status !=1 ");
						?>
						<td width=""><? echo number_format($tiffinAllow); ?></td>
						<td width=""><? $attn_bonus=$result_dtls[c_attn_bonus]; if(strlen($attn_bonus)>0)echo number_format($attn_bonus); else echo "0"; ?></td>
						<td width=""><? echo number_format($result_row[total_earning_amount]); ?></td>
						<td width=""><? $advSalary=$result_dtls[c_advancedSalary]; if(strlen($advSalary)>0)echo number_format($advSalary); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_late_deduction_amount])); ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_absent])); ?></td>
						<td width=""><? $stamp=$result_dtls[c_stamp]; if(strlen($stamp)>0)echo number_format($stamp); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_lwp_deduction_amount])); ?></td>
						<td width=""><? $otherDeduction=$result_dtls[c_otherDeduc]; if(strlen($otherDeduction)>0)
						echo number_format($otherDeduction); else echo "0"; ?>
						</td>
						<td width=""><? echo number_format(round($result_dtls[c_total_pf_amount])); ?></td>
						<td width=""><? echo number_format($result_row[total_deduction_amount]);?></td>
						<td width=""><? echo number_format($result_row[net_payable_amount]);?></td>
						<td width=""></td>
					</tr>  
				  
				<?	
								
					//sub division total calculation
					$subtotal_div_c_employee += $result_row[c_employee];
					$subtotal_div_g_salary += $result_row[g_salary];
					//$subtotal_div_ot_amnt += $result_row[b_ot];
					$subtotal_div_ot_amnt += $result_row[actual_ot];
					$subtotal_div_extra_ot_amnt += $result_row[e_ot];
					$subtotal_div_tiff_alw += $tiffinAllow;
					$subtotal_div_attn_bonus += $attn_bonus;
					$subtotal_div_trans_alw += $transportAllw;
					$subtotal_div_arrear_ern += $arrearErning;
					$subtotal_div_t_salary += $result_row[total_earning_amount];
					$subtotal_div_adv_salary += $advSalary;
					$subtotal_div_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_div_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_div_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_div_other_deduc += $otherDeduction;
					$subtotal_div_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_div_stamp += $stamp;
					$subtotal_div_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_div_tax += $tax;
					$subtotal_div_total_deduc += $result_row[total_deduction_amount];
					$subtotal_div_net_pay += $result_row[net_payable_amount];
					$subtotal_div_withheld += $witheld;
					$subtotal_div_final_pay += $final_pay;
					$subtotal_div_in_bank += $result_row[emp_bank_gross];
					$subtotal_div_in_cash += $cash_sal;
					
					//sub location total calculation
					$subtotal_loc_c_employee += $result_row[c_employee];
					$subtotal_loc_g_salary += $result_row[g_salary];
					//$subtotal_loc_ot_amnt += $result_row[b_ot];
					$subtotal_loc_ot_amnt += $result_row[actual_ot];
					$subtotal_loc_extra_ot_amnt += $result_row[e_ot];
					$subtotal_loc_tiff_alw += $tiffinAllow;
					$subtotal_loc_attn_bonus += $attn_bonus;
					$subtotal_loc_trans_alw += $transportAllw;
					$subtotal_loc_arrear_ern += $arrearErning;
					$subtotal_loc_t_salary += $result_row[total_earning_amount];
					$subtotal_loc_adv_salary += $advSalary;
					$subtotal_loc_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_loc_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_loc_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_loc_other_deduc += $otherDeduction;
					$subtotal_loc_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_loc_stamp += $stamp;
					$subtotal_loc_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_loc_tax += $tax;
					$subtotal_loc_total_deduc += $result_row[total_deduction_amount];
					$subtotal_loc_net_pay += $result_row[net_payable_amount];
					$subtotal_loc_withheld += $witheld;
					$subtotal_loc_final_pay += $final_pay;
					$subtotal_loc_in_bank += $result_row[emp_bank_gross];
					$subtotal_loc_in_cash += $cash_sal;
					
					//grand calculation
					$grandtotal_c_employee += $result_row[c_employee];
					$grandtotal_g_salary += $result_row[g_salary];
					//$grandtotal_ot_amnt += $result_row[b_ot];
					$grandtotal_ot_amnt += $result_row[actual_ot];
					$grandtotal_extra_ot_amnt += $result_row[e_ot];
					$grandtotal_tiff_alw += $tiffinAllow;
					$grandtotal_attn_bonus += $attn_bonus;
					$grandtotal_trans_alw += $transportAllw;
					$grandtotal_arrear_ern += $arrearErning;
					$grandtotal_t_salary += $result_row[total_earning_amount];
					$grandtotal_adv_salary += $advSalary;
					$grandtotal_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$grandtotal_abs_amnt += round($result_dtls[c_absent]);
					$grandtotal_np_amnt += $result_row[total_not_payable_amt];
					$grandtotal_other_deduc += $otherDeduction;
					$grandtotal_pf_amount += ($result_dtls[c_total_pf_amount]);
					$grandtotal_stamp += $stamp;
					$grandtotal_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$grandtotal_tax += $tax;
					$grandtotal_total_deduc += $result_row[total_deduction_amount];
					$grandtotal_net_pay += $result_row[net_payable_amount];
					$grandtotal_withheld += $witheld;
					$grandtotal_final_pay += $final_pay;
					$grandtotal_in_bank += $result_row[emp_bank_gross];
					$grandtotal_in_cash += $cash_sal;
					
					$start++;
				}
				?>	
					
				   
					
					<tr style="font-weight:bold; background-color:#FFF">
						<th colspan="2" align="center">Division Total</th>
								<th width="" align="right"><? echo number_format($subtotal_div_c_employee); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_g_salary); ?></th>
								<!--<th width="" align="right"><? //echo number_format($subtotal_div_ot_amnt+$subtotal_div_extra_ot_amnt); ?></th>-->
								<th width="" align="right"><? echo number_format($subtotal_div_ot_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_tiff_alw); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_attn_bonus); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_t_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_adv_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_late_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_abs_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_stamp); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_lwp_deduction_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_other_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_pf_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_total_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_div_net_pay); ?></th>
								<th width="" align="right"></th>
					</tr>
					
					 <tr style="font-weight:bold; background-color:#FFF">
								<th colspan="2" align="center">Location Total</th>
								<th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
							   <!-- <th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
								<th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
								<th width="" align="right"></th>
					</tr>
					
					<tr style="font-weight:bold; background-color:#FFF">
						<th colspan="2" align="center">Company total</th>
								<th width="" align="right"><? echo number_format($grandtotal_c_employee); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_g_salary); ?></th>
							   <!-- <th width="" align="right"><? //echo number_format($grandtotal_ot_amnt+$grandtotal_extra_ot_amnt); ?></th>-->
								<th width="" align="right"><? echo number_format($grandtotal_ot_amnt); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_tiff_alw); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_attn_bonus); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_t_salary); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_adv_salary); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_late_salary); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_abs_amnt); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_stamp); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_lwp_deduction_amount); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_other_deduc); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_pf_amount); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_total_deduc); ?></th>
								<th width="" align="right"><? echo number_format($grandtotal_net_pay); ?></th>
								<th width="" align="right"></th>
					</tr>
				   
	  
	  </tbody>       
	</table><br />
		  
		
	<?	
		echo signeture_table(10,$company_name,"1500px") ;
	
		
		
		$html = ob_get_contents();
		ob_clean();	
		
		foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) {			
				@unlink($filename);
		}		
		
		//html to xls convert
		$name=time();
		$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);	
		
		echo "$html"."####"."$name";		
		exit();	
		
					}
					
	if($types=="wages_salary_sheet_section"){
				
			ob_start();
			$company_name=$company_id;
			if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
			if ($company_id==0) $company=''; else $company=" and a.company_id='$company_id'";
			if ($department_id==0) $department=""; else	$department=" and a.department_id in($department_id)";
			if ($division_id==0) $division=""; else $division="and  a.division_id  in($division_id)";
			if ($section_id==0 ) $section_id=""; else $section_id="and a.section_id in ($section_id)";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
			if ($location_id==0) $location=""; else $location="and  a.location_id='$location_id'";
			if ($cbo_salary_sheet=='') {$emp_status="";}
			 else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";}
			 else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1";} 
			 else $emp_status="and a.emp_status='$cbo_salary_sheet'";
			
			//echo $category;die;
			$starting_date = explode('_', $cbo_month_selector ); 
			$salary_date = $starting_date[0];
			$exp_month=explode("-",$salary_date);
			$cur_month = $months["'"+$exp_month[1]+"'"];
			$i=0;	
			
					$company_info_sql="select id,company_name,email,website, plot_no, level_no, road_no,block_no,zip_code,city from lib_company where id='$company_name'";
					$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
					$company_info_result = mysql_fetch_array($result);
				
	?>
	
				<div style="border:0px; font-size:18px;" align="center"><?	echo $company_info_result["company_name"];?></div>
				<div style="border:0px; font-size:13px" align="center">
					Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"]; echo "Bangladesh";?><br />
					Salary Top Sheet For The Month Of <? echo $cur_month; ?> <? echo $exp_month[0]; ?><br />
				</div>
	
	<?
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	?>                
	<table id="topsheet_tbl" width="1500" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;" class="rpt_table" rules="all">
	  <thead>  		
			<tr>
						<th width="50">SL</th>
						<th width="180">Emp Category</th>
						<th width="50">Manpower</th>
						<th width="120">G. Salary</th>
						<th width="100">Total OT Amt</th>
						<th width="100">Tiffin Allowance</th>
						<th width="100">Attn Bonus</th>
						<th width="100">T. Salary</th>
						<th width="80">ADV. Sal</th>
						<th width="80">Late Present</th>
						<th width="80">ABS Amt</th>
						<th width="80">Stamp</th>
						<th width="80">LWP</th>
						<th width="90">Others Deduc</th>
						<th width="90">Savings Fund</th>
						<th width="120">T. Deduc</th>
						<th width="120">Net Pay</th>
						<th width="">Remarks</th>
				</tr>    
	  </thead>
	  <tbody>
			 
			 <?
				
				$subtotal_loc_c_employee=0;$subtotal_loc_t_salary=0;$subtotal_loc_adv_salary=0;$subtotal_loc_abs_amnt=0;$subtotal_loc_np_amnt=0;
				$subtotal_loc_other_deduc=0;$subtotal_loc_stamp=0;$subtotal_loc_tax=0;$subtotal_loc_net_pay=0;$subtotal_loc_withheld=0;
				$subtotal_loc_final_pay=0;$subtotal_loc_in_bank=0;$subtotal_loc_in_cash=0;$subtotal_loc_trans_alw=0;$subtotal_loc_tiff_alw=0;
				$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;$subtotal_loc_g_salary=0;	 
				
				
				$subtotal_sec_c_employee=0;$subtotal_sec_t_salary=0;$subtotal_sec_adv_salary=0;$subtotal_sec_abs_amnt=0;$subtotal_sec_np_amnt=0;
				$subtotal_sec_other_deduc=0;$subtotal_sec_stamp=0;$subtotal_sec_tax=0;$subtotal_sec_net_pay=0;$subtotal_sec_withheld=0;
				$subtotal_sec_final_pay=0;$subtotal_sec_in_bank=0;$subtotal_sec_in_cash=0;$subtotal_sec_trans_alw=0;$subtotal_sec_tiff_alw=0;
				$subtotal_sec_lwp_deduction_amount=0;$subtotal_sec_pf_amount=0;$subtotal_sec_g_salary=0;
				
					
				$grandtotal_c_employee=0;$grandtotal_t_salary=0;$grandtotal_adv_salary=0;$grandtotal_abs_amnt=0;$grandtotal_np_amnt=0;
				$grandtotal_other_deduc=0;$grandtotal_stamp=0;$grandtotal_tax=0;$grandtotal_net_pay=0;$grandtotal_withheld=0;
				$grandtotal_final_pay=0;$grandtotal_in_bank=0;$grandtotal_in_cash=0;$grandtotal_trans_alw=0;$grandtotal_tiff_alw=0;$grandtotal_late_salary=0;
				$grandtotal_lwp_deduction_amount=0;$grandtotal_div_pf_amount=0;
				
				
						
				$start=0; 
				$sql_salary = " select group_concat(a.id) as mst_id, a.company_id, a.location_id, a.division_id, a.department_id, a.section_id, a.subsection_id, a.employee_category, a.emp_status,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN 1 else 0 END ) as c_employee,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN a.gross_salary else 0 END ) as g_salary,
					
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.total_over_time*a.over_time_rate) else 0 END ) as actual_ot,
					
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as b_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as e_ot, 
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_earning_amount else 0 END ) as total_earning_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN not_payble_days else 0 END ) as not_payble_days,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN daily_basic_salary else 0 END ) as daily_basic_salary,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_deduction_amount else 0 END ) as total_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN net_payable_amount else 0 END ) as net_payable_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.not_payable_deduction_amount) else 0 END ) as total_not_payable_amt,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.bank_gross) else 0 END ) as emp_bank_gross,								
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as staff_buyer_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as staff_extra_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status=1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as staff_ot_for_witheld,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status<>1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as staff_ot_for_final_pay
					from 
					hrm_salary_mst a, hrm_employee b
					where a.emp_code=b.emp_code and a.salary_periods = '$salary_date' 
					$emp_status $category $company $location $division $department $section_id $subsection_id
					group by a.company_id,a.location_id,a.section_id,a.employee_category 
					having c_employee>0";
					//$department
									
					//echo "<br/>".$sql_salary;die; 
					$sql_salary_result = mysql_query($sql_salary) or die(mysql_error());
					//$numrows=mysql_num_rows($sql_salary_result);
					while($result_row = mysql_fetch_array($sql_salary_result))
					{
					
						//for staff OT code here....do not change					
						//$result_row[b_ot] = $result_row[b_ot]- $result_row[staff_buyer_ot];
						//$result_row[e_ot] = $result_row[e_ot]- $result_row[staff_extra_ot];
						//$result_row[total_earning_amount]=$result_row[total_earning_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						//$result_row[net_payable_amount]=$result_row[net_payable_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						
					//---------------------------------------------------------------------------------------------------	
					
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;
					if(in_array($result_row[location_id],$location_arr))
					{
						if(in_array($result_row[section_id],$section_arr))
						{
						}
						else
						{
							$section_arr[$result_row[section_id]]=$result_row[section_id];
							$new_sec=1;
						}
							
					}//location
					else
					{
						$location_arr[$result_row[location_id]]=$result_row[location_id];
						$division_arr = array();
						$department_arr = array();
						$division_arr[$result_row[division_id]]=$result_row[division_id];
						$department_arr[$result_row[department_id]]=$result_row[department_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
					}//location else
					
					if($new_sec==1 && $start!=0)
					{
						?>
                        <tr style="font-weight:bold; background-color:#FFF">
							<th colspan="2" align="center">Section Total</th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_c_employee); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_g_salary); ?></th>
                            <!--<th width="" align="right"><? //echo number_format($subtotal_sec_ot_amnt+$subtotal_sec_extra_ot_amnt); ?></th>-->
                            <th width="" align="right"><? echo number_format($subtotal_sec_ot_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_tiff_alw); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_attn_bonus); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_t_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_adv_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_late_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_abs_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_stamp); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_lwp_deduction_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_other_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_pf_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_total_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_sec_net_pay); ?></th>
                            <th width="" align="right"></th>
						</tr>
						<?
						$subtotal_sec_c_employee=0;$subtotal_sec_t_salary=0;$subtotal_sec_adv_salary=0;$subtotal_sec_abs_amnt=0;$subtotal_sec_np_amnt=0;
						$subtotal_sec_other_deduc=0;$subtotal_sec_stamp=0;$subtotal_sec_tax=0;$subtotal_sec_net_pay=0;$subtotal_sec_withheld=0;
						$subtotal_sec_final_pay=0;$subtotal_sec_in_bank=0;$subtotal_sec_in_cash=0;$subtotal_sec_trans_alw=0;$subtotal_sec_tiff_alw=0;
						$subtotal_sec_lwp_deduction_amount=0;$subtotal_sec_pf_amount=0;$subtotal_sec_g_salary=0;
						//$department_arr = array();
					}
					
					if($new_loc==1 && $start!=0)
					{					
						?> 
                        <tr style="font-weight:bold; background-color:#FFF">
                            <th colspan="2" align="center">Location Total</th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
                            <!-- <th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
                            <th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
                            <th width="" align="right"></th>
						</tr>
						<?
						$subtotal_loc_c_employee=0; $subtotal_loc_g_salary=0; $subtotal_loc_ot_amnt=0; $subtotal_loc_extra_ot_amnt=0; $subtotal_loc_attn_bonus=0; 
						$subtotal_loc_arrear_ern=0; $subtotal_loc_t_salary=0; $subtotal_loc_adv_salary=0; $subtotal_loc_abs_amnt=0; $subtotal_loc_np_amnt=0;
						$subtotal_loc_other_deduc=0;  $subtotal_loc_stamp=0; $subtotal_loc_tax=0; $subtotal_loc_total_deduc=0; $subtotal_loc_net_pay=0;
						$subtotal_loc_withheld=0; $subtotal_loc_final_pay=0; $subtotal_loc_in_bank=0; $subtotal_loc_in_cash=0; $subtotal_loc_trans_alw=0;
						 $subtotal_loc_tiff_alw=0;$subtotal_loc_late_salary=0;$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;
						//$division_arr = array();
						//$department_arr = array();
					}	
					
					//header print here 
					if($new_loc==1)
					{					
						?><tr><th colspan="23" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$result_row[location_id]]; ?></th></tr><?
					}				
					if($new_sec==1)
					{
						?><tr><th colspan="23" align="left" bgcolor="#CCCCCC">Section : <? echo $section_details[$result_row[section_id]]; ?></th></tr><?
					}
					
					//---------------------------------------------------------------------------------------------------
					//---------------------------------------------------------------------------------------------------

					$i++;
					if ($i%2==0)$bgcolor="#EFEFEF";  
					else $bgcolor="#FFFFFF";
							
					if($result_row[employee_category]==0)$status='Top Mgt.';
					else if($result_row[employee_category]==1)$status='Mid Mgt.';
					else if($result_row[employee_category]==2)$status='Non Mgt.';
					
					
					$dtls_sql = "select  group_concat(mst.emp_code) as mst_emp_code,mst.employee_category,
							SUM(CASE WHEN dtls.salary_head_id=25 THEN dtls.pay_amount else 0 END ) as c_total_pf_amount,
							SUM(CASE WHEN dtls.salary_head_id=23 THEN dtls.pay_amount else 0 END ) as c_total_late_deduction_amount,
							SUM(CASE WHEN dtls.salary_head_id=29 THEN dtls.pay_amount else 0 END ) as c_total_lwp_deduction_amount,
							SUM(CASE WHEN dtls.salary_head_id=24 THEN dtls.pay_amount else 0 END ) as c_absent,
							SUM(CASE WHEN dtls.salary_head_id=35 THEN dtls.pay_amount else 0 END ) as c_attn_bonus,
							SUM(CASE WHEN dtls.salary_head_id=17 THEN dtls.pay_amount else 0 END ) as c_arrear_earn,
							SUM(CASE WHEN dtls.salary_head_id=21 THEN dtls.pay_amount else 0 END ) as c_advancedSalary,
							SUM(CASE WHEN dtls.salary_head_id=31 THEN dtls.pay_amount else 0 END ) as c_otherDeduc,
							SUM(CASE WHEN dtls.salary_head_id=28 THEN dtls.pay_amount else 0 END ) as c_stamp,
							SUM(CASE WHEN dtls.salary_head_id=22 THEN dtls.pay_amount else 0 END ) as c_tax,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status=1 THEN (mst.net_payable_amount) else 0 END ) as witheldAmt,
							SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status<>1 THEN (mst.net_payable_amount) else 0 END ) as finalPayAmt
							from 
							hrm_salary_mst mst, hrm_salary_dtls dtls 
							where 
							mst.id=dtls.salary_mst_id and mst.id in ($result_row[mst_id]) group by mst.company_id,mst.location_id,mst.section_id,mst.employee_category ";	
							/*mst.salary_periods = '$salary_date' and 
							mst.company_id=$result_row[company_id] and
							mst.location_id=$result_row[location_id] and
							mst.division_id=$result_row[division_id] and	
							mst.department_id=$result_row[department_id] and  				 
							mst.employee_category=$result_row[employee_category]*/
					//echo  $dtls_sql."\n";die;
					$dtls_sql_result = mysql_query($dtls_sql) or die(mysql_error());
					$result_dtls = mysql_fetch_array($dtls_sql_result);
					
					//do not delete---- used for staff ot
					$result_dtls[witheldAmt]=round($result_dtls[witheldAmt]-$result_row[staff_ot_for_witheld]);
					$result_dtls[finalPayAmt]=round($result_dtls[finalPayAmt]-$result_row[staff_ot_for_final_pay]);
					
				?>
				   
				   <tr align="right" bgcolor="<? echo $bgcolor; ?>" onMouseOver="fn_color('topsheet_tbl')"  > <!-- onmouseover="fn_color('topsheet_tbl')" -->
						<td width=""><? echo $i; ?></td>
						<td width="" align="left"><? echo $status; ?></td>
						<td width=""><? echo number_format($result_row[c_employee]); ?></td>
						<td width=""><? echo number_format($result_row[g_salary]); ?></td>
						<td width="">&nbsp;</td>
						<?
						$tiffinAllow=return_field_value("sum(dtls.amount)","hrm_salary_mst mst, hrm_employee_salary dtls","mst.emp_code=dtls.emp_code and mst.salary_periods like '%$salary_date%' and mst.emp_code in ($result_dtls[mst_emp_code]) and dtls.payroll_head=9 and mst.emp_status !=1 ");
						?>
						<td width=""><? echo number_format($tiffinAllow); ?></td>
						<td width=""><? $attn_bonus=$result_dtls[c_attn_bonus]; if(strlen($attn_bonus)>0)echo number_format($attn_bonus); else echo "0"; ?></td>
						<td width=""><? echo number_format($result_row[total_earning_amount]); ?></td>
						<td width=""><? $advSalary=$result_dtls[c_advancedSalary]; if(strlen($advSalary)>0)echo number_format($advSalary); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_late_deduction_amount])); ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_absent])); ?></td>
						<td width=""><? $stamp=$result_dtls[c_stamp]; if(strlen($stamp)>0)echo number_format($stamp); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_lwp_deduction_amount])); ?></td>
						<td width=""><? $otherDeduction=$result_dtls[c_otherDeduc]; if(strlen($otherDeduction)>0)
						echo number_format($otherDeduction); else echo "0"; ?>
						</td>
						<td width=""><? echo number_format(round($result_dtls[c_total_pf_amount])); ?></td>
						<td width=""><? echo number_format($result_row[total_deduction_amount]);?></td>
						<td width=""><? echo number_format($result_row[net_payable_amount]);?></td>
						<td width=""></td>
				  </tr>   
				  
				<?	
								
					//sub section total calculation
					$subtotal_sec_c_employee += $result_row[c_employee];
					$subtotal_sec_g_salary += $result_row[g_salary];
					//$subtotal_sec_ot_amnt += $result_row[b_ot];
					$subtotal_sec_ot_amnt += $result_row[actual_ot];
					$subtotal_sec_extra_ot_amnt += $result_row[e_ot];
					$subtotal_sec_tiff_alw += $tiffinAllow;
					$subtotal_sec_attn_bonus += $attn_bonus;
					$subtotal_sec_trans_alw += $transportAllw;
					$subtotal_sec_arrear_ern += $arrearErning;
					$subtotal_sec_t_salary += $result_row[total_earning_amount];
					$subtotal_sec_adv_salary += $advSalary;
					$subtotal_sec_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_sec_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_sec_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_sec_other_deduc += $otherDeduction;
					$subtotal_sec_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_sec_stamp += $stamp;
					$subtotal_sec_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_sec_tax += $tax;
					$subtotal_sec_total_deduc += $result_row[total_deduction_amount];
					$subtotal_sec_net_pay += $result_row[net_payable_amount];
					$subtotal_sec_withheld += $witheld;
					$subtotal_sec_final_pay += $final_pay;
					$subtotal_sec_in_bank += $result_row[emp_bank_gross];
					$subtotal_sec_in_cash += $cash_sal;
					
					//sub location total calculation
					$subtotal_loc_c_employee += $result_row[c_employee];
					$subtotal_loc_g_salary += $result_row[g_salary];
					//$subtotal_loc_ot_amnt += $result_row[b_ot];
					$subtotal_loc_ot_amnt += $result_row[actual_ot];
					$subtotal_loc_extra_ot_amnt += $result_row[e_ot];
					$subtotal_loc_tiff_alw += $tiffinAllow;
					$subtotal_loc_attn_bonus += $attn_bonus;
					$subtotal_loc_trans_alw += $transportAllw;
					$subtotal_loc_arrear_ern += $arrearErning;
					$subtotal_loc_t_salary += $result_row[total_earning_amount];
					$subtotal_loc_adv_salary += $advSalary;
					$subtotal_loc_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_loc_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_loc_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_loc_other_deduc += $otherDeduction;
					$subtotal_loc_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_loc_stamp += $stamp;
					$subtotal_loc_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_loc_tax += $tax;
					$subtotal_loc_total_deduc += $result_row[total_deduction_amount];
					$subtotal_loc_net_pay += $result_row[net_payable_amount];
					$subtotal_loc_withheld += $witheld;
					$subtotal_loc_final_pay += $final_pay;
					$subtotal_loc_in_bank += $result_row[emp_bank_gross];
					$subtotal_loc_in_cash += $cash_sal;
					
					//grand calculation
					$grandtotal_c_employee += $result_row[c_employee];
					$grandtotal_g_salary += $result_row[g_salary];
					//$grandtotal_ot_amnt += $result_row[b_ot];
					$grandtotal_ot_amnt += $result_row[actual_ot];
					$grandtotal_extra_ot_amnt += $result_row[e_ot];
					$grandtotal_tiff_alw += $tiffinAllow;
					$grandtotal_attn_bonus += $attn_bonus;
					$grandtotal_trans_alw += $transportAllw;
					$grandtotal_arrear_ern += $arrearErning;
					$grandtotal_t_salary += $result_row[total_earning_amount];
					$grandtotal_adv_salary += $advSalary;
					$grandtotal_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$grandtotal_abs_amnt += round($result_dtls[c_absent]);
					$grandtotal_np_amnt += $result_row[total_not_payable_amt];
					$grandtotal_other_deduc += $otherDeduction;
					$grandtotal_pf_amount += ($result_dtls[c_total_pf_amount]);
					$grandtotal_stamp += $stamp;
					$grandtotal_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$grandtotal_tax += $tax;
					$grandtotal_total_deduc += $result_row[total_deduction_amount];
					$grandtotal_net_pay += $result_row[net_payable_amount];
					$grandtotal_withheld += $witheld;
					$grandtotal_final_pay += $final_pay;
					$grandtotal_in_bank += $result_row[emp_bank_gross];
					$grandtotal_in_cash += $cash_sal;
					$start++;
				}
				?>	

                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Section total</th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_g_salary); ?></th>
                    <!-- <th width="" align="right"><? //echo number_format($subtotal_sec_ot_amnt+$subtotal_sec_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($subtotal_sec_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_sec_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Location Total</th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
                    <!-- <th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Company total</th>
                    <th width="" align="right"><? echo number_format($grandtotal_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_g_salary); ?></th>
                    <!-- <th width="" align="right"><? //echo number_format($grandtotal_ot_amnt+$grandtotal_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($grandtotal_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
            </tbody>       
		</table><br />
		<?	
        echo signeture_table(10,$company_name,"1500px") ;
		$html = ob_get_contents();
		ob_clean();	
		foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) { @unlink($filename); }		
		
		//html to xls convert
		$name=time();
		$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);	
		
		echo "$html"."####"."$name";		
		exit();	
	}
	
	if($types=="wages_salary_sheet_subsection")
	{
		ob_start();
		$company_name=$company_id;
		if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
		if ($company_id==0) $company=''; else $company=" and a.company_id='$company_id'";
		if ($department_id==0) $department=""; else	$department=" and a.department_id in($department_id)";
		if ($division_id==0) $division=""; else $division="and  a.division_id  in($division_id)";
		if ($section_id==0 ) $section_id=""; else $section_id="and a.section_id in ($section_id)";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
		if ($location_id==0) $location=""; else $location="and  a.location_id='$location_id'";
		if ($cbo_salary_sheet=='') {$emp_status="";} else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} else $emp_status="and a.emp_status='$cbo_salary_sheet'";
		
		$starting_date = explode('_', $cbo_month_selector ); 
		$salary_date = $starting_date[0];
		$exp_month=explode("-",$salary_date);
		$cur_month = $months["'"+$exp_month[1]+"'"];
		$i=0;	
			
		$company_info_sql="select id,company_name,email,website, plot_no, level_no, road_no,block_no,zip_code,city from lib_company where id='$company_name'";
		$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
		$company_info_result = mysql_fetch_array($result);
		?>
	
        <div style="border:0px; font-size:18px;" align="center"><?	echo $company_info_result["company_name"];?></div>
        <div style="border:0px; font-size:13px" align="center">
            Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"]; echo "Bangladesh";?><br />
            Salary Top Sheet For The Month Of <? echo $cur_month; ?> <? echo $exp_month[0]; ?><br />
        </div>
		<?
        $location_arr=array();
        $division_arr=array();
        $department_arr=array();
        $section_arr=array();
        $subsection_arr=array();
		?>                
            <table id="topsheet_tbl" width="1500" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;" class="rpt_table" rules="all">
                <thead>  		
                    <th width="50">SL</th>
                    <th width="180">Emp Category</th>
                    <th width="50">Manpower</th>
                    <th width="120">G. Salary</th>
                    <th width="100">Total OT Amt</th>
                    <th width="100">Tiffin Allowance</th>
                    <th width="100">Attn Bonus</th>
                    <th width="100">T. Salary</th>
                    <th width="80">ADV. Sal</th>
                    <th width="80">Late Present</th>
                    <th width="80">ABS Amt</th>
                    <th width="80">Stamp</th>
                    <th width="80">LWP</th>
                    <th width="90">Others Deduc</th>
                    <th width="90">Savings Fund</th>
                    <th width="120">T. Deduc</th>
                    <th width="120">Net Pay</th>
                    <th width="">Remarks</th>
                </thead>
                <tbody>
				<?
				
				$subtotal_loc_c_employee=0;$subtotal_loc_t_salary=0;$subtotal_loc_adv_salary=0;$subtotal_loc_abs_amnt=0;$subtotal_loc_np_amnt=0;
				$subtotal_loc_other_deduc=0;$subtotal_loc_stamp=0;$subtotal_loc_tax=0;$subtotal_loc_net_pay=0;$subtotal_loc_withheld=0;
				$subtotal_loc_final_pay=0;$subtotal_loc_in_bank=0;$subtotal_loc_in_cash=0;$subtotal_loc_trans_alw=0;$subtotal_loc_tiff_alw=0;
				$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;	 
				
				$subtotal_subsec_c_employee=0;$subtotal_subsec_t_salary=0;$subtotal_subsec_adv_salary=0;$subtotal_subsec_abs_amnt=0;$subtotal_subsec_np_amnt=0;
				$subtotal_subsec_other_deduc=0;$subtotal_subsec_stamp=0;$subtotal_subsec_tax=0;$subtotal_subsec_net_pay=0;$subtotal_subsec_withheld=0;
				$subtotal_subsec_final_pay=0;$subtotal_subsec_in_bank=0;$subtotal_subsec_in_cash=0;$subtotal_subsec_trans_alw=0;$subtotal_subsec_tiff_alw=0;
				$subtotal_subsec_lwp_deduction_amount=0;$subtotal_subsec_pf_amount=0;$subtotal_subsec_g_salary=0;
				
				$grandtotal_c_employee=0;$grandtotal_t_salary=0;$grandtotal_adv_salary=0;$grandtotal_abs_amnt=0;$grandtotal_np_amnt=0;
				$grandtotal_other_deduc=0;$grandtotal_stamp=0;$grandtotal_tax=0;$grandtotal_net_pay=0;$grandtotal_withheld=0;
				$grandtotal_final_pay=0;$grandtotal_in_bank=0;$grandtotal_in_cash=0;$grandtotal_trans_alw=0;$grandtotal_tiff_alw=0;$grandtotal_late_salary=0;
				$grandtotal_lwp_deduction_amount=0;$grandtotal_div_pf_amount=0;
				
				/*$subtotal_subsec_c_employee=0;$subtotal_subsec_t_salary=0;$subtotal_subsec_adv_salary=0;$subtotal_subsec_abs_amnt=0;$subtotal_subsec_np_amnt=0;
				$subtotal_subsec_other_deduc=0;$subtotal_subsec_stamp=0;$subtotal_subsec_tax=0;$subtotal_subsec_net_pay=0;$subtotal_subsec_withheld=0;
				$subtotal_subsec_final_pay=0;$subtotal_subsec_in_bank=0;$subtotal_subsec_in_cash=0;$subtotal_subsec_trans_alw=0;*/

				$start=0; 
				$sql_salary = " select group_concat(a.id) as mst_id, a.company_id, a.location_id, a.division_id, a.department_id, a.section_id, a.subsection_id, a.employee_category, a.emp_status,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN 1 else 0 END ) as c_employee,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN a.gross_salary else 0 END ) as g_salary,
					
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.total_over_time*a.over_time_rate) else 0 END ) as actual_ot,
					
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as b_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as e_ot, 
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_earning_amount else 0 END ) as total_earning_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN not_payble_days else 0 END ) as not_payble_days,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN daily_basic_salary else 0 END ) as daily_basic_salary,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_deduction_amount else 0 END ) as total_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN net_payable_amount else 0 END ) as net_payable_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN total_absent_deduction_amount else 0 END ) as total_absent_deduction_amount,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.not_payable_deduction_amount) else 0 END ) as total_not_payable_amt,
					SUM(CASE WHEN salary_periods like '%$salary_date%' THEN round(a.bank_gross) else 0 END ) as emp_bank_gross,								
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.b_over_time*a.over_time_rate) else 0 END ) as staff_buyer_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 THEN round(a.e_over_time*a.over_time_rate) else 0 END ) as staff_extra_ot,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status=1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as staff_ot_for_witheld,
					SUM(CASE WHEN salary_periods like '%$salary_date%' and b.staff_ot_entitled=1 and a.emp_status<>1 THEN (a.total_over_time*a.over_time_rate) else 0 END ) as staff_ot_for_final_pay
					from 
					hrm_salary_mst a, hrm_employee b
					where a.emp_code=b.emp_code and a.salary_periods = '$salary_date' 
					$emp_status $category $company $location $division $department $section_id $subsection_id
					group by a.company_id,a.location_id,a.subsection_id,a.employee_category
					having c_employee>0";
					//$department
									
					//echo "<br/>".$sql_salary;die; 
					$sql_salary_result = mysql_query($sql_salary) or die(mysql_error());
					//$numrows=mysql_num_rows($sql_salary_result);
					while($result_row = mysql_fetch_array($sql_salary_result))
					{
						//for staff OT code here....do not change					
						//$result_row[b_ot] = $result_row[b_ot]- $result_row[staff_buyer_ot];
						//$result_row[e_ot] = $result_row[e_ot]- $result_row[staff_extra_ot];
						//$result_row[total_earning_amount]=$result_row[total_earning_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						//$result_row[net_payable_amount]=$result_row[net_payable_amount]-($result_row[staff_buyer_ot]+$result_row[staff_extra_ot]);
						
					//---------------------------------------------------------------------------------------------------	
					
					$new_loc=0;$new_divis=0;$new_dept=0;$new_subsec=0;
					if(in_array($result_row[location_id],$location_arr))
					{
						if(in_array($result_row[subsection_id],$subsection_arr))
						{
						}
						else
						{
							$subsection_arr[$result_row[subsection_id]]=$result_row[subsection_id];

							$new_subsec=1;
						}
							
					}//location
					else
					{
						$location_arr[$result_row[location_id]]=$result_row[location_id];
						$division_arr = array();
						$department_arr = array();
						$division_arr[$result_row[division_id]]=$result_row[division_id];
						$department_arr[$result_row[department_id]]=$result_row[department_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_subsec=1;
					}//location else
					
					if($new_subsec==1 && $start!=0)
					{
						?>
                        <tr style="font-weight:bold; background-color:#FFF">
							<th colspan="2" align="center">Subsection Total</th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_c_employee); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_g_salary); ?></th>
                            <!-- <th width="" align="right"><? //echo number_format($subtotal_subsec_ot_amnt+$subtotal_subsec_extra_ot_amnt); ?></th>-->
                            <th width="" align="right"><? echo number_format($subtotal_subsec_ot_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_tiff_alw); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_attn_bonus); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_t_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_adv_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_late_salary); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_abs_amnt); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_stamp); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_lwp_deduction_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_other_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_pf_amount); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_total_deduc); ?></th>
                            <th width="" align="right"><? echo number_format($subtotal_subsec_net_pay); ?></th>
                            <th width="" align="right"></th>
						</tr>
						<?
						$subtotal_subsec_c_employee=0;$subtotal_subsec_t_salary=0;$subtotal_subsec_adv_salary=0;$subtotal_subsec_abs_amnt=0;
						$subtotal_subsec_np_amnt=0;
						$subtotal_subsec_other_deduc=0;$subtotal_subsec_stamp=0;$subtotal_subsec_tax=0;$subtotal_subsec_net_pay=0;$subtotal_subsec_withheld=0;
						$subtotal_subsec_final_pay=0;$subtotal_subsec_in_bank=0;$subtotal_subsec_in_cash=0;$subtotal_subsec_trans_alw=0;$subtotal_subsec_tiff_alw=0;
						$subtotal_subsec_lwp_deduction_amount=0;$subtotal_subsec_pf_amount=0;$subtotal_subsec_g_salary=0;
						
						//$department_arr = array();
					}
					
					if($new_loc==1 && $start!=0)
					{					
						
						?><tr style="font-weight:bold; background-color:#FFF">
							<th colspan="2" align="center">Location Total</th>
								<th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
							   <!-- <th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
								<th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
								<th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
								<th width="" align="right"></th>
						
						</tr><?
					
						
						$subtotal_loc_c_employee=0; $subtotal_loc_g_salary=0; $subtotal_loc_ot_amnt=0; $subtotal_loc_extra_ot_amnt=0; $subtotal_loc_attn_bonus=0; 
						$subtotal_loc_arrear_ern=0; $subtotal_loc_t_salary=0; $subtotal_loc_adv_salary=0; $subtotal_loc_abs_amnt=0; $subtotal_loc_np_amnt=0;
						$subtotal_loc_other_deduc=0;  $subtotal_loc_stamp=0; $subtotal_loc_tax=0; $subtotal_loc_total_deduc=0; $subtotal_loc_net_pay=0;
						$subtotal_loc_withheld=0; $subtotal_loc_final_pay=0; $subtotal_loc_in_bank=0; $subtotal_loc_in_cash=0; $subtotal_loc_trans_alw=0;
						 $subtotal_loc_tiff_alw=0;$subtotal_loc_late_salary=0;$subtotal_loc_lwp_deduction_amount=0;$subtotal_loc_pf_amount=0;
					}	
					
					
					//header print here 
					if($new_loc==1)
					{					
						?><tr><th colspan="23" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$result_row[location_id]]; ?></th></tr><?
					}				
					if($new_subsec==1)
					{
						?><tr><th colspan="23" align="left" bgcolor="#CCCCCC">SubSection : <? echo $subsection_details[$result_row[subsection_id]]; ?></th></tr><?
					}
					?>
					<?
					
					//---------------------------------------------------------------------------------------------------
					//---------------------------------------------------------------------------------------------------
					
					
						$i++;
						
						if ($i%2==0)$bgcolor="#EFEFEF";  
						else $bgcolor="#FFFFFF";
								
						if($result_row[employee_category]==0)$status='Top Mgt.';
						else if($result_row[employee_category]==1)$status='Mid Mgt.';
						else if($result_row[employee_category]==2)$status='Non Mgt.';
						
						
						$dtls_sql = "select  group_concat(mst.emp_code) as mst_emp_code,mst.employee_category,
								SUM(CASE WHEN dtls.salary_head_id=25 THEN dtls.pay_amount else 0 END ) as c_total_pf_amount,
								SUM(CASE WHEN dtls.salary_head_id=23 THEN dtls.pay_amount else 0 END ) as c_total_late_deduction_amount,
								SUM(CASE WHEN dtls.salary_head_id=29 THEN dtls.pay_amount else 0 END ) as c_total_lwp_deduction_amount,
								SUM(CASE WHEN dtls.salary_head_id=24 THEN dtls.pay_amount else 0 END ) as c_absent,
								SUM(CASE WHEN dtls.salary_head_id=35 THEN dtls.pay_amount else 0 END ) as c_attn_bonus,
								SUM(CASE WHEN dtls.salary_head_id=17 THEN dtls.pay_amount else 0 END ) as c_arrear_earn,
								SUM(CASE WHEN dtls.salary_head_id=21 THEN dtls.pay_amount else 0 END ) as c_advancedSalary,
								SUM(CASE WHEN dtls.salary_head_id=31 THEN dtls.pay_amount else 0 END ) as c_otherDeduc,
								SUM(CASE WHEN dtls.salary_head_id=28 THEN dtls.pay_amount else 0 END ) as c_stamp,
								SUM(CASE WHEN dtls.salary_head_id=22 THEN dtls.pay_amount else 0 END ) as c_tax,
								SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status=1 THEN (mst.net_payable_amount) else 0 END ) as witheldAmt,
								SUM(CASE WHEN dtls.salary_head_id=1 and mst.emp_status<>1 THEN (mst.net_payable_amount) else 0 END ) as finalPayAmt
								from 
								hrm_salary_mst mst, hrm_salary_dtls dtls 
								where 
								mst.id=dtls.salary_mst_id and mst.id in ($result_row[mst_id]) group by mst.company_id,mst.location_id,mst.subsection_id,mst.employee_category";	
								/*mst.salary_periods = '$salary_date' and 
								mst.company_id=$result_row[company_id] and
								mst.location_id=$result_row[location_id] and
								mst.division_id=$result_row[division_id] and	
								mst.department_id=$result_row[department_id] and  				 
								mst.employee_category=$result_row[employee_category]*/
					//echo  $dtls_sql."\n";die;
								
					//echo  $dtls_sql."\n";die;
					$dtls_sql_result = mysql_query($dtls_sql) or die(mysql_error());
					$result_dtls = mysql_fetch_array($dtls_sql_result);
					
					//do not delete---- used for staff ot
					$result_dtls[witheldAmt]=round($result_dtls[witheldAmt]-$result_row[staff_ot_for_witheld]);
					$result_dtls[finalPayAmt]=round($result_dtls[finalPayAmt]-$result_row[staff_ot_for_final_pay]);
					
				?>
				   
				   <tr align="right" bgcolor="<? echo $bgcolor; ?>" onMouseOver="fn_color('topsheet_tbl')"  > <!-- onmouseover="fn_color('topsheet_tbl')" -->
						<td width=""><? echo $i; ?></td>
						<td width="" align="left"><? echo $status; ?></td>
						<td width=""><? echo number_format($result_row[c_employee]); ?></td>
						<td width=""><? echo number_format($result_row[g_salary]); ?></td>
						<td width="">&nbsp;</td>
						<?
						$tiffinAllow=return_field_value("sum(dtls.amount)","hrm_salary_mst mst, hrm_employee_salary dtls","mst.emp_code=dtls.emp_code and mst.salary_periods like '%$salary_date%' and mst.emp_code in ($result_dtls[mst_emp_code]) and dtls.payroll_head=9 and mst.emp_status !=1 ");
						?>
						<td width=""><? echo number_format($tiffinAllow); ?></td>
						<td width=""><? $attn_bonus=$result_dtls[c_attn_bonus]; if(strlen($attn_bonus)>0)echo number_format($attn_bonus); else echo "0"; ?></td>
						<td width=""><? echo number_format($result_row[total_earning_amount]); ?></td>
						<td width=""><? $advSalary=$result_dtls[c_advancedSalary]; if(strlen($advSalary)>0)echo number_format($advSalary); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_late_deduction_amount])); ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_absent])); ?></td>
						<td width=""><? $stamp=$result_dtls[c_stamp]; if(strlen($stamp)>0)echo number_format($stamp); else echo "0"; ?></td>
						<td width=""><? echo number_format(round($result_dtls[c_total_lwp_deduction_amount])); ?></td>
						<td width=""><? $otherDeduction=$result_dtls[c_otherDeduc]; if(strlen($otherDeduction)>0)
						echo number_format($otherDeduction); else echo "0"; ?>
						</td>
						<td width=""><? echo number_format(round($result_dtls[c_total_pf_amount])); ?></td>
						<td width=""><? echo number_format($result_row[total_deduction_amount]);?></td>
						<td width=""><? echo number_format($result_row[net_payable_amount]);?></td>
						<td width=""></td>
				  </tr>   
				  
				<?	
								
					//subsection total calculation
					$subtotal_subsec_c_employee += $result_row[c_employee];
					$subtotal_subsec_g_salary += $result_row[g_salary];
					//$subtotal_subsec_ot_amnt += $result_row[b_ot];
					$subtotal_subsec_ot_amnt += $result_row[actual_ot];
					$subtotal_subsec_extra_ot_amnt += $result_row[e_ot];
					$subtotal_subsec_tiff_alw += $tiffinAllow;
					$subtotal_subsec_attn_bonus += $attn_bonus;
					$subtotal_subsec_trans_alw += $transportAllw;
					$subtotal_subsec_arrear_ern += $arrearErning;
					$subtotal_subsec_t_salary += $result_row[total_earning_amount];
					$subtotal_subsec_adv_salary += $advSalary;
					$subtotal_subsec_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_subsec_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_subsec_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_subsec_other_deduc += $otherDeduction;
					$subtotal_subsec_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_subsec_stamp += $stamp;
					$subtotal_subsec_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_subsec_tax += $tax;
					$subtotal_subsec_total_deduc += $result_row[total_deduction_amount];
					$subtotal_subsec_net_pay += $result_row[net_payable_amount];
					$subtotal_subsec_withheld += $witheld;
					$subtotal_subsec_final_pay += $final_pay;
					$subtotal_subsec_in_bank += $result_row[emp_bank_gross];
					$subtotal_subsec_in_cash += $cash_sal;
					
					//sub location total calculation
					$subtotal_loc_c_employee += $result_row[c_employee];
					$subtotal_loc_g_salary += $result_row[g_salary];
					//$subtotal_loc_ot_amnt += $result_row[b_ot];
					$subtotal_loc_ot_amnt += $result_row[actual_ot];
					$subtotal_loc_extra_ot_amnt += $result_row[e_ot];
					$subtotal_loc_tiff_alw += $tiffinAllow;
					$subtotal_loc_attn_bonus += $attn_bonus;
					$subtotal_loc_trans_alw += $transportAllw;
					$subtotal_loc_arrear_ern += $arrearErning;
					$subtotal_loc_t_salary += $result_row[total_earning_amount];
					$subtotal_loc_adv_salary += $advSalary;
					$subtotal_loc_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$subtotal_loc_abs_amnt += round($result_dtls[c_absent]);
					$subtotal_loc_np_amnt += $result_row[total_not_payable_amt];
					$subtotal_loc_other_deduc += $otherDeduction;
					$subtotal_loc_pf_amount += ($result_dtls[c_total_pf_amount]);
					$subtotal_loc_stamp += $stamp;
					$subtotal_loc_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$subtotal_loc_tax += $tax;
					$subtotal_loc_total_deduc += $result_row[total_deduction_amount];
					$subtotal_loc_net_pay += $result_row[net_payable_amount];
					$subtotal_loc_withheld += $witheld;
					$subtotal_loc_final_pay += $final_pay;
					$subtotal_loc_in_bank += $result_row[emp_bank_gross];
					$subtotal_loc_in_cash += $cash_sal;
					
					//grand calculation
					$grandtotal_c_employee += $result_row[c_employee];
					$grandtotal_g_salary += $result_row[g_salary];
					//$grandtotal_ot_amnt += $result_row[b_ot];
					$grandtotal_ot_amnt += $result_row[actual_ot];
					$grandtotal_extra_ot_amnt += $result_row[e_ot];
					$grandtotal_tiff_alw += $tiffinAllow;
					$grandtotal_attn_bonus += $attn_bonus;
					$grandtotal_trans_alw += $transportAllw;
					$grandtotal_arrear_ern += $arrearErning;
					$grandtotal_t_salary += $result_row[total_earning_amount];
					$grandtotal_adv_salary += $advSalary;
					$grandtotal_late_salary += ($result_dtls[c_total_late_deduction_amount]);
					$grandtotal_abs_amnt += round($result_dtls[c_absent]);
					$grandtotal_np_amnt += $result_row[total_not_payable_amt];
					$grandtotal_other_deduc += $otherDeduction;
					$grandtotal_pf_amount += ($result_dtls[c_total_pf_amount]);
					$grandtotal_stamp += $stamp;
					$grandtotal_lwp_deduction_amount += $result_dtls[c_total_lwp_deduction_amount];
					$grandtotal_tax += $tax;
					$grandtotal_total_deduc += $result_row[total_deduction_amount];
					$grandtotal_net_pay += $result_row[net_payable_amount];
					$grandtotal_withheld += $witheld;
					$grandtotal_final_pay += $final_pay;
					$grandtotal_in_bank += $result_row[emp_bank_gross];
					$grandtotal_in_cash += $cash_sal;
					
					$start++;
				}
				?>	
                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Subsection total</th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_g_salary); ?></th>
                    <!-- <th width="" align="right"><? //echo number_format($subtotal_subsec_ot_amnt+$subtotal_subsec_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($subtotal_subsec_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_subsec_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
                <tr style="font-weight:bold; background-color:#FFF">
                	<th colspan="2" align="center">Location Total</th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_g_salary); ?></th>
                    <!--  <th width="" align="right"><? //echo number_format($subtotal_loc_ot_amnt+$subtotal_loc_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($subtotal_loc_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($subtotal_loc_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
                <tr style="font-weight:bold; background-color:#FFF">
                    <th colspan="2" align="center">Company total</th>
                    <th width="" align="right"><? echo number_format($grandtotal_c_employee); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_g_salary); ?></th>
                    <!-- <th width="" align="right"><? //echo number_format($grandtotal_ot_amnt+$grandtotal_extra_ot_amnt); ?></th>-->
                    <th width="" align="right"><? echo number_format($grandtotal_ot_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_tiff_alw); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_attn_bonus); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_t_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_adv_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_late_salary); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_abs_amnt); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_stamp); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_lwp_deduction_amount); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_other_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_pf_amount); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_total_deduc); ?></th>
                    <th width="" align="right"><? echo number_format($grandtotal_net_pay); ?></th>
                    <th width="" align="right"></th>
                </tr>
            </tbody>       
        </table><br />
		<?	
		echo signeture_table(10,$company_name,"1500px") ;
		$html = ob_get_contents();
		ob_clean();	
		foreach (glob( "tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) { @unlink($filename); }		
		
		//html to xls convert
		$name=time();
		$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);	
		
		echo "$html"."####"."$name";		
		exit();	
	}
}

	//month generated
	if($type=="select_month_generate")
	{		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}

	function return_field_value($fdata,$tdata,$cdata)
	{
		$sql_data="select $fdata from  $tdata where $cdata";
		//echo $sql_data;die; 
		$sql_data_exe=mysql_query($sql_data);
		$sql_data_rslt=mysql_fetch_array($sql_data_exe);
		$m_data  = $sql_data_rslt[0];
		
		return $m_data ;
	}
?>