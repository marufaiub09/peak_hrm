<?php
session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}
	
	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$company_detail[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}	

	$bangArray = array(	
 						'Friday'=>'শুক্রবার', 
						'Saturday'=>'শনিবার', 
						'Sunday'=>'রবিবার', 
						'Monday'=>'সোমবার', 
						'Tuesday'=>'মঙ্গলবার', 
						'Wednesday'=>'বুধবার', 
						'Thursday'=>'বৃহস্পতিবার' 
 					   );
	
	$bangMonthArray = array('January'=>'জানুয়ারি', 'February'=>'ফেব্রুয়ারি', 'March'=>'মার্চ', 'April'=>'এপ্রিল','May'=>'মে','June'=>'জুন', 'July'=>'জুলাই','August'=>'আগস্ট', 'September'=>'সেপ্টেম্বর', 'October'=>'অক্টোবর', 'November'=>'নভেম্বর', 'December'=>'ডিসেম্বর');

	
	extract($_REQUEST);
	if($type=="pay_slip")//check which top sheet format  
	{ 
		$sql="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and pay_slip_format!='0' order by id DESC ";  
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$row = mysql_fetch_array( $result );
		$types=$row[pay_slip_format]; 
	}
?>
<style media="print">
	p{page-break-before:always}
</style>
<?php
if($types=="5") //pay Sleep format peak 
{
  	ob_start();
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];	
	  
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and  a.designation_id in ($designation_id)";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id in ($department_id)";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}	
	//if ($emp_code==""){ $emp_code="";}else{$emp_code="and a.emp_code in ($emp_code)";}	
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	//salary details 
	$salary_dtls_arr = array();
	$sql_sal="select d.* from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date'";
	//echo $sql_sal;die;
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}
	//print_r($salary_dtls_arr);die;
	
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$sql_emp="select a.*, a.gross_salary as sal_gross_salary,a.basic_salary as sal_basic_salary, a.house_rent as sal_house_rent,a.medical_allowance as sal_medical_allowance,b.emp_code,b.first_name,b.middle_name,b.last_name,b.id_card_no,b.punch_card_no,b.joining_date,b.salary_grade 
			  from hrm_salary_mst a, hrm_employee b 
			  where 
			  a.salary_periods like '$txt_from_date' and
			  a.emp_code=b.emp_code
			  $emp_code	 
			  $location_id 
			  $division_id
			  $section_id 
			  $subsection_id 
			  $category 
			  $payment_met 
			  $designation_id 
			  $department_id 
			  $company_id 
			 order by cast(right(b.id_card_no,5)as SIGNED)";
	 //echo $sql_emp;die;

	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	$numrows=mysql_num_rows($exe_sql_emp); 
	$i=0;
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMP Master Qry starts 
	{ 
		if($row_emp[staff_ot_entitled]==1 && $row_emp[ot_entitled]==1)
		{ 
			$e_ot_amt =$row_emp[total_over_time]*$row_emp[over_time_rate];
			// $e_ot_amt=0;
			$row_emp[over_time_rate]=0;
			$row_emp[total_over_time]=0;
			$row_emp[net_payable_amount]=$row_emp[net_payable_amount]-round($e_ot_amt);//
			$row_emp[total_earning_amount]=$row_emp[total_earning_amount]-round($e_ot_amt);//
			$e_ot_amt=0;
		}
		else
		{
			$e_ot_amt =$row_emp[total_over_time]*$row_emp[over_time_rate];
			$row_emp[net_payable_amount]=$row_emp[net_payable_amount];//-round($e_ot_amt)
			$row_emp[total_earning_amount]=$row_emp[total_earning_amount];//-round($e_ot_amt)
		}
		$earn = $e_ot_amt+$salary_dtls_arr[$row_emp[emp_code]][35]+$salary_dtls_arr[$row_emp[emp_code]][17];
		$before_salary_info = $row_emp[net_payable_amount]+$row_emp[total_deduction_amount]-$salary_dtls_arr[$row_emp[emp_code]][24]-$earn;
	
		$d="increment_amount";
		$dd="hrm_increment_mst";
		$ddd="emp_code=$row_emp[emp_code] and effective_date like '$txt_from_date' and is_approved='1'";
		$inc_amt=return_field_value($d,$dd,$ddd);
		//echo $inc_amt;die;
		
		//($i%2==0)?$flt="right":$flt="left";
?>	

<div style="width:450px; float:<?php echo ($i%2==0)?$flt='right':$flt='left';?>; height:800px;">  
    <table width="450px" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table" style="font-size:8px">
        <tr>
          <td align="center" colspan="2"><span  align="centre" style="font-size:19px; font-weight:bold">Peak Apparels Ltd.</span></td>
         </tr>
         <tr>       
           <td align="center"><span align="left" style="font-size:14px; font-weight:bold;font-family:'Kalpurush ANSI'">পে-স্লিপ <? echo $bangMonthArray[$cur_month]; ?>-<? echo $cur_year; ?></span></td>                        
       </tr>
    </table>
    <table id="tble_pay_slip" class="rpt_table" width="450px" cellpadding="0" cellspacing="0" border="1" style="font-size:15px;font-weight:normal;">
         <tr>
            <td width="50%">নাম</td> 
            <td width="50%" align="left" style="font-size:18px;font-weight:bold;">&nbsp;<? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name];?></td>
            <td width="50%">কার্ড নং</td> 
            <td width="50%" align="left" style="font-size:18px;font-weight:bold;">&nbsp;<? echo $row_emp[id_card_no];?></td>
        </tr>
        <tr>
            <td width="50%">পদবী </td> 
            <td width="50%" align="left" style="font-size:14px;font-weight:bold;">&nbsp;<? if(strlen($designation_chart[$row_emp[designation_id]])>30){echo substr($designation_chart[$row_emp[designation_id]],0,30)."..";} else echo $designation_chart[$row_emp[designation_id]]; ?></td>
            <td width="50%">সেকশন </td> 
            <td width="50%" align="left" style="font-size:14px;font-weight:bold;">&nbsp;<? echo $section_details[$row_emp[section_id]];?></td>                    
        </tr>
        <tr>
            <td width="50%">লাইন নং </td> 
            <td width="50%" align="left" style="font-size:14px;font-weight:bold;">&nbsp;<? echo $subsection_details[$row_emp[subsection_id]];?></td>                     
        </tr>
        <tr>
          <td width="50%" colspan="2">মুল বেতন </td> 
          <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo round($row_emp[sal_basic_salary]);?></td> 
        </tr>
        <tr>
          <td width="50%" colspan="2"> বাড়ি ভাড়া ভাতা </td> 
          <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $row_emp[house_rent]; ?></td>                    
        </tr>
         <tr>
            <td width="50%" colspan="2">খাদ্য ভাতা</td>
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo  $salary_dtls_arr[$row_emp[emp_code]][13]; ?></td>          
         </tr> 
         <tr>
            <td width="50%" colspan="2">যাতায়াত ভাতা </td>
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo  $salary_dtls_arr[$row_emp[emp_code]][11]; ?></td>          
         </tr> 
        <tr>
          <td width="50%" colspan="2">চিকিৎসা ভাতা </td> 
          <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $row_emp[medical_allowance]; ?></td> 
        </tr>
        <tr>
          <td width="50%" colspan="2">মোট বেতন </td> 
          <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:18px;font-weight:bold;" colspan="2">
          &nbsp;<? echo round($row_emp[sal_basic_salary]+$row_emp[house_rent]+$row_emp[medical_allowance]+$salary_dtls_arr[$row_emp[emp_code]][13]+$salary_dtls_arr[$row_emp[emp_code]][11]);?></td>
       </tr>
        <tr>
        <tr>
            <td width="50%" colspan="2">কার্য দিবস </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $row_emp[total_working_days];?></td> 
        </tr>
        <tr>
            <td width="50%" colspan="2">উপস্থিত দিন </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo round($row_emp[total_present_days]+$row_emp[total_late_days]);?></td>                    
        </tr>
        <tr>
            <td width="50%" colspan="2">অনুউপস্তিত দিন </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $row_emp[total_abs_days];?></td> 
        </tr>
        <tr>
            <td width="50%" colspan="2">উপস্থিত বেতন </td>
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo  round(($row_emp[payable_days])*($row_emp[daily_gross_salary]));//round((($row_emp[payable_days]+$row_emp[total_abs_days])*($row_emp[daily_gross_salary]))-($row_emp[total_absent_deduction_amount])); ?></td> 
        </tr>
            <td width="50%" colspan="2">প্রতি ঘণ্টা ওভার টাইম</td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $row_emp[over_time_rate];?></td> 
        </tr>
        <tr>
            <td width="50%" colspan="2">মোট ওভার টাইম ঘণ্টা</td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $row_emp[total_over_time];?></td>
        </tr>
         <tr>
         <tr>
            <td width="50%" colspan="2">মোট ওভার টাইম টাকা</td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo round($e_ot_amt);?></td>                    
        </tr>
        <tr>
            <td width="50%" colspan="2">হাজিরা  বোনাস </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo  $salary_dtls_arr[$row_emp[emp_code]][35];//$row_emp[b_attendance_bonus]; ?></td> </tr>
        <tr>
          <td width="50%" colspan="2">বর্ধিত বেতন </td> 
          <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $inc_amt;?></td>                    
        </tr>
        <tr>
            <td width="50%" colspan="2">টিফিন এ্যালাউন্স</td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $salary_dtls_arr[$row_emp[emp_code]][9];?></td>
        </tr>
        <tr>
            <td width="50%" colspan="2">সর্বমোট বেতন </td>
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:18px;font-weight:bold;" colspan="2">
            &nbsp;<?php echo $total_sal=round($row_emp[total_earning_amount]);//-($row_emp[total_absent_deduction_amount]+$row_emp[not_payable_deduction_amount]));?>
            </td>
        </tr>
        <tr>
            <td width="50%" colspan="2">অনুপস্থিত দিনের জন্য কর্তন </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo round($row_emp[total_absent_deduction_amount]); ?></td>                    
        </tr>
        <tr>
            <td width="50%" colspan="2">অগ্রীম কর্তন </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $salary_dtls_arr[$row_emp[emp_code]][21];?></td>
        </tr> 
        <tr>
            <td width="50%" colspan="2">অন্যান্য কর্তন </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $salary_dtls_arr[$row_emp[emp_code]][31]; ?></td>
        </tr>  
        <tr>
            <td width="50%" colspan="2">স্ট্যাম্প কর্তন </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $salary_dtls_arr[$row_emp[emp_code]][28];?></td>
        </tr>
        <tr>
            <td width="50%" colspan="2">সেভিং ফান্ড কর্তন </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo $salary_dtls_arr[$row_emp[emp_code]][25];?></td>
        </tr>
        <tr>
            <td width="50%" colspan="2">মোট কর্তন  </td> 
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:17px;font-weight:bold;" colspan="2">&nbsp;<? echo  $total_deduct=$salary_dtls_arr[$row_emp[emp_code]][28]+$salary_dtls_arr[$row_emp[emp_code]][21]+$salary_dtls_arr[$row_emp[emp_code]][31]+$salary_dtls_arr[$row_emp[emp_code]][25]+$row_emp[total_absent_deduction_amount];?></td>
        </tr>
        <tr>
            <td width="50%"  colspan="2">মোট প্রদেয় বেতন </td>
            <td width="50%" align="right" style="font-family:'SugondhaMJ';font-size:18px;font-weight:bold;" colspan="2">&nbsp;<? echo round($total_sal-($total_deduct+$row_emp[not_payable_deduction_amount]));?></td>
        </tr>
    </table>
    <table width="450px" border="1" cellpadding="0" cellspacing="0"  class="rpt_table" style="font-size:14px; margin-top:5px">
        <tr>
          <td width="50%" height="35" valign="middle" >হিসাব বিভাগ</td>
          <td width="50%" align="left" style="font-family:'SugondhaMJ';" valign="middle"  colspan="2" >গ্রহীতা</td>                  
        </tr>
    </table><br /><br />
</div>
<!--<div style="clear:both"></div>-->   
<!--<p></p>-->
<?php
	$i++;
	if($i==4)
	{
		echo "<p>&nbsp;</p>";
		echo "<div style='clear:both'></div>"; 
		$i=0;
	}

}//end while
	
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) { @unlink($filename); }		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();

}
//Pay Sleep for Peak appearls End

//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}


function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	
	return $m_data ;
}

?>
