<?
session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');
extract ( $_REQUEST );

//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}
	
 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
	$sql = "SELECT * FROM variable_settings_report WHERE is_deleted = 0 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$variable_settings_report = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$variable_settings_report[$row['company_id']][$row['module_id']] = mysql_real_escape_string($row['report_list']);
	}
 
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$allow_rate=array();
	$designation_chart = array();
	while( $row = mysql_fetch_array( $result ) ) {		
			$designation_chart[$row['id']] =  $row['custom_designation'];
			$designation_chart_rate[$row['id']]['custom_designation_local'] = ( $row['custom_designation_local'] );
			$allow_rate[$row['id']]	= $row['allowance_rate'] ;
	}
	 
	
// Variable Settiings
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['not_pay_deduction'] = mysql_real_escape_string( $row['not_pay_deduction'] );
		$var_hrm_chart[$row['company_name']]['new_emp_card_charge'] = mysql_real_escape_string( $row['new_emp_card_charge'] );
		$var_hrm_chart[$row['company_name']]['ot_rate_multiplier'] = mysql_real_escape_string( $row['ot_rate_multiplier'] );
		$var_hrm_chart[$row['company_name']]['att_bonus_leave_type'] = mysql_real_escape_string( $row['att_bonus_leave_type'] ); 	 	 	
	}




$bangArray = array(	
					'Friday'=>'শুক্রবার', 
					'Saturday'=>'শনিবার', 
					'Sunday'=>'রবিবার', 
					'Monday'=>'সোমবার', 
					'Tuesday'=>'মঙ্গলবার', 
					'Wednesday'=>'বুধবার', 
					'Thursday'=>'বৃহস্পতিবার' 
				   );
	
$bangMonthArray = array('January'=>'জানুয়ারি', 'February'=>'ফেব্রুয়ারি', 'March'=>'মার্চ', 'April'=>'এপ্রিল','May'=>'মে','June'=>'জুন', 'July'=>'জুলাই','August'=>'আগস্ট', 'September'=>'সেপ্টেম্বর', 'October'=>'অক্টোবর', 'November'=>'নভেম্বর', 'December'=>'ডিসেম্বর');


//All bank name
	$sql = "select * from  lib_bank where status_active=1 and is_deleted=0";
	//echo $sql;die;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$bank_id_arr = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$bank_id_arr[$row['id']] = mysql_real_escape_string( $row['bank_name'] );		
		   
	}
//print_r($bank_id_arr);die;

//get employee wise bank mat
$sql = "select * from  hrm_employee_salary_bank where status_active=1 and is_deleted=0  order by sequence";
//echo $sql;die;
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$bank_amount_arr=array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$bank_amount_arr[$row['emp_code']][$row['lib_bank_id']]=$row['salary_amount'];
}


 

$sql = "select * from  hrm_maternity_leave  order by emp_code";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$maternity_arr=array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$maternity_arr[$row['emp_code']]['leave_start_date']=$row['leave_start_date'];
	$maternity_arr[$row['emp_code']]['leave_end_date']=$row['leave_end_date'];
	$maternity_arr[$row['emp_code']]['est_delivery_date']=$row['est_delivery_date'];
	
}



function my_old($dob)
	{
		$now = date('d-m-Y');
		$dob = explode('-', $dob);
		$now = explode('-', $now);
		$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
		if($now[0] < $dob[0])
		{
			$now[0] += $mnt[$now[1]-1];
			$now[1]--;
		}
		if($now[1] < $dob[1])
		{
			$now[1] += 12;
			$now[2]--;
		}
		if($now[2] < $dob[2]) return false;
		return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
	}



?>


<style type="text/css" media="print">
	
         p{ page-break-after: always;}
		 
    </style>
    
<?

if($type=="wages_salary_sheet") 	// jm salary sheet automated
{	
	
	
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];
	$previous_month=add_month($txt_from_date,-1);
	
	$one_month_before=add_month($txt_from_date,-1);	
	$two_month_before=add_month($txt_from_date,-2);
	$three_month_before=add_month($txt_from_date,-3);
	
	$one_month_before_bangla=explode("-",$one_month_before);
	$two_month_before_bangla=explode("-",$two_month_before);
	$three_month_before_bangla=explode("-",$three_month_before);
	
	$bangla_month_first= $bangMonthArray[$months["'"+$three_month_before_bangla[1]+"'"]];
	$bangla_month_second= $bangMonthArray[$months["'"+$two_month_before_bangla[1]+"'"]];
	$bangla_month_third= $bangMonthArray[$months["'"+$one_month_before_bangla[1]+"'"]];
	
	
	//echo $bangla_month_first."==".$bangla_month_second."==".$bangla_month_third;die;
	 	 
	
	$sql_salary = "select * from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods ='$one_month_before'";
	$result = mysql_query( $sql_salary ) or die( $sql_salary . "<br />" . mysql_error() );
	while( $row = mysql_fetch_array( $result ) ) {
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_calendar_days']=$row['total_calendar_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_working_days']=$row['total_working_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_leave_days']=$row['total_leave_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_weekly_holiday']=$row['total_weekly_holiday'];
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['government_holiday']=$row['government_holiday'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['festival_holiday']=$row['festival_holiday'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['casual_leave']=$row['casual_leave'];
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_abs_days']=$row['total_abs_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['gross_salary']=$row['gross_salary'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['b_over_time']=$row['b_over_time'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['over_time_rate']=$row['over_time_rate'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_absent_deduction_amount']=$row['total_absent_deduction_amount'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['net_payable_amount']=$row['net_payable_amount'];
	 
		$test[$row['salary_periods']][$row['emp_code']][$row['salary_head_id']]=$row['pay_amount'];
	
	}
	
	$sql_salary = "select * from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods ='$two_month_before'";
	$result = mysql_query( $sql_salary ) or die( $sql_salary . "<br />" . mysql_error() );
	while( $row = mysql_fetch_array( $result ) ) {
		
		 
		 $salary_arr[$row['salary_periods']][$row['emp_code']]['total_calendar_days']=$row['total_calendar_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_working_days']=$row['total_working_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_leave_days']=$row['total_leave_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_weekly_holiday']=$row['total_weekly_holiday'];
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['government_holiday']=$row['government_holiday'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['festival_holiday']=$row['festival_holiday'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['casual_leave']=$row['casual_leave'];
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_abs_days']=$row['total_abs_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['gross_salary']=$row['gross_salary'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['b_over_time']=$row['b_over_time'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['over_time_rate']=$row['over_time_rate'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_absent_deduction_amount']=$row['total_absent_deduction_amount'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['net_payable_amount']=$row['net_payable_amount'];
		 
		 
		 $test[$row['salary_periods']][$row['emp_code']][$row['salary_head_id']]=$row['pay_amount'];
	
	}
	$sql_salary = "select * from hrm_salary_mst m, hrm_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods ='$three_month_before'";
	$result = mysql_query( $sql_salary ) or die( $sql_salary . "<br />" . mysql_error() );
	while( $row = mysql_fetch_array( $result ) ) {
		
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_calendar_days']=$row['total_calendar_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_working_days']=$row['total_working_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_leave_days']=$row['total_leave_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_weekly_holiday']=$row['total_weekly_holiday'];
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['government_holiday']=$row['government_holiday'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['festival_holiday']=$row['festival_holiday'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['casual_leave']=$row['casual_leave'];
		
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_abs_days']=$row['total_abs_days'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['gross_salary']=$row['gross_salary'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['b_over_time']=$row['b_over_time'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['over_time_rate']=$row['over_time_rate'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['total_absent_deduction_amount']=$row['total_absent_deduction_amount'];
		$salary_arr[$row['salary_periods']][$row['emp_code']]['net_payable_amount']=$row['net_payable_amount'];
		
		$test[$row['salary_periods']][$row['emp_code']][$row['salary_head_id']]=$row['pay_amount']; 
	 
	}
	
	//print_r($salary_arr);die;				
	//echo $sql_salary;die;				

	$rpt_company=$company_id;
	$company_info_sql="select * from lib_company where id='$company_id'";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10)
	{
		$emp_status="and a.emp_status in ( 0 )";
		//$txt_from_date = $previous_month;
		
	}
		
	
	
	$search_header_caption="";
	//print_r($location_details); 
	if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
	if ($location_id==0 || $location_id=='') $location_id=""; else { $search_header_caption .=" Location: ".$location_details[trim($location_id)].";";  $location_id="and a.location_id='$location_id'"; }
	//echo $search_header_caption;
	if ($division_id==0 || $division_id=='') $division_id=""; 
	else {
		$tmp="";
		$division_s=explode(",",$division_details);
		foreach($division_s as $key)
		{
			if($tmp=="")$tmp=$division_details[$key]; else $tmp.=",".$division_details[$key];
		} 
		$search_header_caption .=" Division: ".$tmp.";";
		$division_id="and a.division_id in ($division_id)"; 
	
	}
	
	if ($department_id==0 || $department_id=='') $department_id=""; 
	else	{
		$tmp="";
			$division_s=explode(",",$department_id);
			foreach($division_s as $key)
			{
				if($tmp=="")$tmp=$department_details[$key]; else $tmp.=",".$department_details[$key];
			} 
			$search_header_caption .=" Department: ".$tmp.";";
			 
			$department_id=" and  a.department_id in ($department_id)";    
		}
	
	if ($section_id==0 || $section_id=='') $section_id=""; 
	else { 
			$tmp="";
			$division_s=explode(",",$section_id);
			foreach($division_s as $key)
			{
				if($tmp=="")$tmp=$section_details[$key]; else $tmp.=",".$section_details[$key];
			} 
			$search_header_caption .=" Section: ".$tmp.";";
		
		 
		$section_id="and a.section_id in ($section_id)"; 
	}
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; 
	else {  
			$tmp="";
			$division_s=explode(",",$subsection_id);
			foreach($division_s as $key)
			{
				if($tmp=="")$tmp=$subsection_details[$key]; else $tmp.=",".$subsection_details[$key];
			} 
			$search_header_caption .=" Sub Section: ".$tmp.";";
		
		
		$subsection_id="and a.subsection_id in ($subsection_id)";
	}	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and b.designation_id in ($designation_id)";
	
	if ($payment_method==0) $payment_met =""; 
	else if ($payment_method==1) $payment_met =" and b.bank_gross<1 ";
	else if ($payment_method==2) $payment_met =" and b.bank_gross>0 ";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}	
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	$emp_bank_id=explode(",",$emp_bank_name);
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
		
		if($group_by_id==0){$groupby="a.emp_code,";}
		else if($group_by_id==1){$status_com=1;$groupby="a.company_id,a.emp_code,";}
		else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,a.emp_code,";}
		else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,a.emp_code,";}
		else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.emp_code,";}
		else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.emp_code,";}
		else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.emp_code,";}

		//echo $order_by_id; die;	
		if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(b.id_card_no as SIGNED)";}	 
		else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(b.designation_level as SIGNED)";}	 
		else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(a.emp_code as SIGNED)";}	 
		else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(b.id_card_no), 5)as SIGNED)";}	 
	
		$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by  

	if ( $groupby!="" ) { $search_header_caption =""; }
	

	
	
	ob_start();
 	
	
	
 	$sl=0;
	
		$sql_emp  ="select a.emp_code,a.salary_periods,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.designation_id,
		a.*,CONCAT(b.first_name,' ',b.middle_name,' ',b.last_name) as name,b.id_card_no,b.punch_card_no,b.dob,
		b.designation_level,b.joining_date,b.category,b.salary_grade,b.ot_entitled,b.staff_ot_entitled
		from 
		hrm_salary_mst a,hrm_employee b
		where
		a.emp_code=b.emp_code and
		a.salary_periods='$txt_from_date'
		$emp_code $id_card_no $bank_id $category $company_id $location_id $division_id $section_id $subsection_id 
		$designation_id $department_id $payment_met $emp_status group by $dynamic_groupby
		$orderby";
		
		//echo $sql_emp;die;
		
	
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{ 
		
		
		$sl++;
		  
			  
			   ?> 
               
        
        
               
<div align="center" style="width:1200px;">
	
    
    <table width="1200"  cellpadding="0" cellspacing="0" border="1"  class="rpt_table" rules="all">
            
            
            <tr style="text-align:center; font-weight:bold; font-size:24px" height="20" bgcolor="#CCCCCC">
            	<th colspan="18"><font size="3">ফকির এ্যাপারেলস্ লিমিটেড </font><br /><font size="2">এ-১২৭-১৩১,১৩৫-১৩৮,১৪২-১৪৫,বি ৫০১-৫০৩ বিসিক হোসিয়ারি শিল্প নগরী </font><br />
                <font size="2">মাতৃকল্যাণ সুবিধার পরিমান নিরুপন পত্র (শুধুমাত্র মহিলা শ্রমিকের জন্য )<? echo " ".$bangMonthArray[$cur_month]; ?>-<? echo " ".$cur_year; ?><? echo $rpt_cap; ?></font>
                </th>
            </tr>
            
            <tr>
              <td align="left" colspan="6">কারখানার নাম : <font size="3">ফকির এ্যাপারেলস্ লিমিটেড</font></td>
               <td align="left" colspan="6">আবেদনের তারিখ (দিন/মাস/বছর) :</td>
               <td align="left" colspan="6">সংযুক্তকরন : ২য় কিস্তি নেওয়ার ক্ষেত্রে যাহা প্রযোজ্য ।</td>
			</tr>
            
            
            <tr>
      <td align="left" colspan="6">ঠিকানা : বিসিক হোসিয়ারি শিল্প নগরী,শাসনগাঁও,ফাতুল্লা,নারায়ানগঞ্জ । </td>
       <td align="left" colspan="6">নিয়োগের তারিখ (দিন/মাস/বছর) :<font style='font-family:Kalpurush ANSI'><? echo convert_to_mysql_date($row_emp[joining_date]);?></font></td>
       <td align="left" colspan="6">১.ডাক্তার কর্তৃক প্রত্যয়ন পত্র ।</td>
			</tr>
            
            
            <tr>
              <td align="left" colspan="6">আবেদনকারীর নাম :&nbsp;<? echo $row_emp[name]; ?></td>
               <td align="left" colspan="6">চাকুরী কাল (দিন/মাস/বছর):&nbsp;<font style='font-family:Kalpurush ANSI'><?php
                            $joining_date=convert_to_mysql_date($row_emp['joining_date']);
                            $service_length = my_old($joining_date);
                            //printf("%d বছর, %d months, %d days\n", $service_length[year], $service_length[mnt], $service_length[day]);
							echo $service_length[day]."দিন,".$service_length[mnt]."মাস,".$service_length[year]."বছর";
                        ?></font></td>
               <td align="left" colspan="6">২.সন্তানের জন্ম সনদের কপি ।</td>
			</tr>
            
            
            <tr>
              <td align="left" colspan="6">কার্ড নং  :<? echo $row_emp[id_card_no]; ?><span style="float:right;vertical-align:middle;font-family:Kalpurush ANSI">ইডিডি তাং (দিন/মাস/বছর):<? echo convert_to_mysql_date($maternity_arr[$row_emp[emp_code]][est_delivery_date]); ?></span></td>
               <td align="left" colspan="6" style="font-family:Kalpurush ANSI">মাতৃত্বকালীন ছুটির কাল:&nbsp;<? echo convert_to_mysql_date($maternity_arr[$row_emp[emp_code]][leave_start_date]); ?>&nbsp;ইং হইতে&nbsp;<? echo convert_to_mysql_date($maternity_arr[$row_emp[emp_code]][leave_end_date]); ?>&nbsp;ইং</td>
               <td align="left" colspan="6">৩.ছুটি শেষে যোগদানের হাজিরা কার্ড ।</td>
			</tr>
            
            
            <tr>
              <td align="left" colspan="6">পদবী : <? echo $designation_chart_rate[$row_emp[designation_id]]['custom_designation_local']; ?></td>
               <td align="left" colspan="6">পরিশোধের তারিখ : ইং </td>
               <td align="left" colspan="6">৪. মনোনয়ন দান সার্টিফিকেট (২য় কিস্তি )</td>
			</tr>
            
            
            
            <tr>
              <td align="left" colspan="6">সেকশন : <? echo $section_details[$row_emp[section_id]];  ?></td>
               <td align="left" colspan="6">মন্তব্য :</td>
               <td align="left" colspan="6"> ৫.নিয়োগ পত্রের কপি ।</td>
			</tr>
            
             <tr>
              <td align="left" colspan="18" height="10">&nbsp;</td>
			</tr>
       
                <tr style="text-align:center; font-weight:bold" height="20">
                    <th rowspan="2">নোটিশ প্রদানের পূর্বে মজুরী প্রাপ্ত ০৩ মাস </th>
                    <th rowspan="2">নোটিশ প্রদানের পূর্বে ০৩ মাসের মোট দিন  </th>
                    <th rowspan="2">প্রকৃত কাজের দিন  </th>
                    <th rowspan="2">ছুটির দিন  </th>
                    <th rowspan="2">অনুমোদিত অনুপস্তিতির  দিন</th>
                    <th rowspan="2">মূল মজুরী</th>
                    <th rowspan="2">বাড়ি ভাড়া </th>
                    <th rowspan="2">চিকিৎসা ভাতা +যাতায়াত ভাতা +খাদ্য  ভাতা </th>
                    <th rowspan="2">মোট মজুরী</th>
                    <th colspan="2">অতিরিক্ত ভাতা </th>
                    <th  rowspan="2">হাজিরা  বোনাস </th>
                    <th  rowspan="2">উৎসব বোনাস </th>
                    <th  rowspan="2">সর্বমোট প্রাপ্য মজুরী </th>
                    <th rowspan="2">অনুপস্তিতি জনিত কর্তন  </th>
                    <th rowspan="2">অন্যান্য কর্তন  </th>
                    <th rowspan="2">চূড়ান্ত হিসাবে প্রাপ্য (টাকা)</th>
                    <th rowspan="2">গ্রহিতার স্বাক্ষর </th>
                </tr>
                <tr>
                    <th>ঘণ্টা </th>
                    <th>টাকা </th>
                </tr>
                <tr>
                    <td><? echo $bangla_month_first; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$three_month_before][$row_emp['emp_code']]['total_calendar_days']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$three_month_before][$row_emp['emp_code']]['total_working_days']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$three_month_before][$row_emp['emp_code']]['government_holiday']+$salary_arr[$three_month_before][$row_emp['emp_code']]['festival_holiday']+$salary_arr[$three_month_before][$row_emp['emp_code']]['casual_leave']+$salary_arr[$three_month_before][$row_emp['emp_code']]['total_weekly_holiday']); ?></td>
                   <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$three_month_before][$row_emp['emp_code']]['total_abs_days']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$three_month_before][$row_emp['emp_code']][1]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$three_month_before][$row_emp['emp_code']][2]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($test[$three_month_before][$row_emp['emp_code']][3]+$test[$three_month_before][$row_emp['emp_code']][11]+$test[$three_month_before][$row_emp['emp_code']][13]); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$three_month_before][$row_emp['emp_code']]['gross_salary']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$three_month_before][$row_emp['emp_code']]['b_over_time']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$three_month_before][$row_emp['emp_code']]['over_time_rate']*$salary_arr[$three_month_before][$row_emp['emp_code']]['b_over_time']); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$three_month_before][$row_emp['emp_code']][35]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$three_month_before][$row_emp['emp_code']][16]; ?></td>
                     <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$three_month_before][$row_emp['emp_code']]['gross_salary']+($salary_arr[$three_month_before][$row_emp['emp_code']]['over_time_rate']*$salary_arr[$three_month_before][$row_emp['emp_code']]['b_over_time'])+$test[$three_month_before][$row_emp['emp_code']][35]); ?></td>
                 <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$three_month_before][$row_emp['emp_code']]['total_absent_deduction_amount']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$three_month_before][$row_emp['emp_code']][31]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$three_month_before][$row_emp['emp_code']]['net_payable_amount']; ?></td>
                    
                </tr>
                <tr>
                    <td><? echo $bangla_month_second; ?></td>
                    <td  align="center" style="font-family:Kalpurush ANSI"><?  echo $salary_arr[$two_month_before][$row_emp['emp_code']]['total_calendar_days'];?></td>
                    <td  align="center" style="font-family:Kalpurush ANSI"><?  echo $salary_arr[$two_month_before][$row_emp['emp_code']]['total_working_days'];?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$two_month_before][$row_emp['emp_code']]['government_holiday']+$salary_arr[$two_month_before][$row_emp['emp_code']]['festival_holiday']+$salary_arr[$two_month_before][$row_emp['emp_code']]['casual_leave']+$salary_arr[$two_month_before][$row_emp['emp_code']]['total_weekly_holiday']); ?></td>
                    <td  align="center" style="font-family:Kalpurush ANSI"><?  echo $salary_arr[$two_month_before][$row_emp['emp_code']]['total_abs_days'];?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$two_month_before][$row_emp['emp_code']][1]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$two_month_before][$row_emp['emp_code']][2]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($test[$two_month_before][$row_emp['emp_code']][3]+$test[$two_month_before][$row_emp['emp_code']][11]+$test[$two_month_before][$row_emp['emp_code']][13]); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$two_month_before][$row_emp['emp_code']]['gross_salary']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$two_month_before][$row_emp['emp_code']]['b_over_time']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$two_month_before][$row_emp['emp_code']]['over_time_rate']*$salary_arr[$two_month_before][$row_emp['emp_code']]['b_over_time']); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$two_month_before][$row_emp['emp_code']][35]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$two_month_before][$row_emp['emp_code']][16]; ?></td>
                   <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$two_month_before][$row_emp['emp_code']]['gross_salary']+($salary_arr[$two_month_before][$row_emp['emp_code']]['over_time_rate']*$salary_arr[$two_month_before][$row_emp['emp_code']]['b_over_time'])+$test[$two_month_before][$row_emp['emp_code']][35]); ?></td>
                   <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$two_month_before][$row_emp['emp_code']]['total_absent_deduction_amount']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$two_month_before][$row_emp['emp_code']][31]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$two_month_before][$row_emp['emp_code']]['net_payable_amount']; ?></td>
                   
                </tr>
                    <tr>
                    <td><? echo $bangla_month_third; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$one_month_before][$row_emp['emp_code']]['total_calendar_days']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$one_month_before][$row_emp['emp_code']]['total_working_days']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$one_month_before][$row_emp['emp_code']]['government_holiday']+$salary_arr[$one_month_before][$row_emp['emp_code']]['festival_holiday']+$salary_arr[$one_month_before][$row_emp['emp_code']]['casual_leave']+$salary_arr[$one_month_before][$row_emp['emp_code']]['total_weekly_holiday']); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$one_month_before][$row_emp['emp_code']]['total_abs_days']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$one_month_before][$row_emp['emp_code']][1]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$one_month_before][$row_emp['emp_code']][2]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($test[$one_month_before][$row_emp['emp_code']][3]+$test[$one_month_before][$row_emp['emp_code']][11]+$test[$one_month_before][$row_emp['emp_code']][13]); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$one_month_before][$row_emp['emp_code']]['gross_salary']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$one_month_before][$row_emp['emp_code']]['b_over_time']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$one_month_before][$row_emp['emp_code']]['over_time_rate']*$salary_arr[$one_month_before][$row_emp['emp_code']]['b_over_time']); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$one_month_before][$row_emp['emp_code']][35]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$one_month_before][$row_emp['emp_code']][16]; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($salary_arr[$one_month_before][$row_emp['emp_code']]['gross_salary']+($salary_arr[$one_month_before][$row_emp['emp_code']]['over_time_rate']*$salary_arr[$one_month_before][$row_emp['emp_code']]['b_over_time'])+$test[$one_month_before][$row_emp['emp_code']][35]); ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$one_month_before][$row_emp['emp_code']]['total_absent_deduction_amount']; ?></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $test[$one_month_before][$row_emp['emp_code']][31]; ?></td>
                   <td align="center" style="font-family:Kalpurush ANSI"><? echo $salary_arr[$one_month_before][$row_emp['emp_code']]['net_payable_amount']; ?></td>
                   
                </tr>
                <tr>
                    <td colspan="2">মাসে সর্বমোট প্রকৃত কাজের দিনগুলি </td>
                    <td align="center" style="font-family:Kalpurush ANSI"><?
					 echo $total_days=($salary_arr[$three_month_before][$row_emp['emp_code']]['total_working_days']+$salary_arr[$two_month_before][$row_emp['emp_code']]['total_working_days']+$salary_arr[$one_month_before][$row_emp['emp_code']]['total_working_days']); ?></td>
                    <td colspan="13"></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $total_np=($salary_arr[$three_month_before][$row_emp['emp_code']]['net_payable_amount']+$salary_arr[$two_month_before][$row_emp['emp_code']]['net_payable_amount']+$salary_arr[$one_month_before][$row_emp['emp_code']]['net_payable_amount']);?></td>
                </tr>
                <tr>
                    <td colspan="6">গড় একদিনের সুবিধা = চূড়ান্ত প্রাপ্য /সর্বমোট প্রকৃত কাজের দিন ।</td>
                    <td colspan="10"></td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo ($total_np/$total_days);?></td>
                    <td style="border-bottom-style:hidden"></td>
                </tr>
                
                <tr>
                    <td colspan="6">মোট প্রাপ্য সুবিধা (টাকা) = ১১২ দিন (১৬ সপ্তাহ)*০১ দিনের গড় হিসাব । </td>
                    <td colspan="10">* মোট = ১১২ দিন (১৬ সপ্তাহ) এর জন্য সুবিধার পরিমান (টাকা)</td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo (($total_np/$total_days)*112);?></td>
                </tr>
                  <? $total_amount=(($total_np/$total_days)*56); ?>
                <tr>
                    <td colspan="6">প্রথম কিস্তির টাকার পরিমান - ৫৬ দিনের জন্য মোট প্রাপ্য সুবিধা (টাকা) । </td>
                    <td colspan="10">* ২য় কিস্তির টাকার পরিমান - ৫৬ দিনের জন্য মোট প্রাপ্য সুবিধা (টাকা) । </td>
                    <td align="center" style="font-family:Kalpurush ANSI"><? echo $total_amount; ?></td>
                </tr>
              
                <tr>
                    <td colspan="17">টাকা (কথায়) <? echo number_to_words_bengali($total_amount); ?></td>
                </tr> 
                
        </table>
        
    
   </div>
   
   <p></p>
<? 
	}
	echo signeture_table(8,$rpt_company,"1700px",$location_id) ; 
	
	$html = ob_get_contents();
	ob_clean();	
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) 
		{			
            @unlink($filename);
		}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
 	}
 


function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}



//new for fashion
function signeture_table_fashion($report_id,$company,$width)
{
	if ($width=="") $width="100%"; 
	$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company");
	$signeture_list = explode('#',$signeture_allow_or_no);
	if($signeture_allow_or_no!= "")	
	{
	 echo '<table width="'.$width.'">
		<tr height="90" style="font-size:18px; font-family:Arial; font-weight:bold">
		 ';
		foreach($signeture_list as $key=>$value)
		{
			//echo '<td align="center" valign="bottom" style="text-decoration:overline">'.$value.'</td> ';
			echo '<td align="center" valign="bottom" style="border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;border-bottom-style:hidden;">'.$value.'</td> ';
		}
		echo '</tr></table>';
	}
	else
	{
		echo '<table width="'.$width.'">
		<tr height="90" style="font-size:18px; font-family:Arial; font-weight:bold">
		 ';
		 
		echo '</tr></table>';
	}
}




?>