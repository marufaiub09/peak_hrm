<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');



$e_date = time();
$user_only = $_SESSION["user_name"];
//extract( $_GET );
extract( $_REQUEST );

//emp_basic
	 $sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['id']] = mysql_real_escape_string($row['company_name']);
	}	

	
//company details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}
	
	
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
//designation details
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


//Basic Salary
	$sql = "SELECT * FROM hrm_employee_salary where payroll_head=1  and is_deleted = 0 and status_active=1 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$basic_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$basic_details[$row['emp_code']] = mysql_real_escape_string( $row['amount'] );		
	}

//print_r($basic_details);

//education details
$sql = "SELECT * FROM  hrm_employee_education WHERE is_deleted = 0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$education_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$education_details[$row['emp_code']] = mysql_real_escape_string($row['exam_name']);
	}

//experience details
$sql = "SELECT * FROM  hrm_employee_experience ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$i=0;
	$tmp_code=array();
	$experience_details = array();
	$experience_details_dur = array();
	while( $row = mysql_fetch_array( $result ) ) {
		if(!in_array($row['emp_code'],$tmp_code))
		{
			$tmp_code[]=$row['emp_code'];
			$i=0;
		}
		$experience_details[$row['emp_code']][$i] = $row['joining_date']."_".$row['resigning_date'];
		$experience_details_dur[$row['emp_code']][$i] = $row['sevice_length'];
		$i++;
	}

 





 if($action=='increment_due_statement'){ 
	
	$company_id=$cbo_company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else  $department_id="and emp.department_id='$department_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
	
		if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and emp.company_id='$cbo_company_id'";
		$company_info_sql="select com.id, company_name, email, website, plot_no, level_no, road_no,	block_no,zip_code,city, cnt.country_name from lib_company com left join lib_list_country cnt on com.country_id =cnt.id where com.country_id =cnt.id $company_name";
		$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
		$company_info_result = mysql_fetch_array($result);
				
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="emp.company_id,emp.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="emp.company_id,emp.location_id,emp.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.subsection_id,emp.emp_code,";}
	
	
	
	/*if($order_by_id==0){$orderby="order by  CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by  CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by  right(emp.id_card_no, 5)";}*/
	
	//echo $order_by_id; die;	
		if($order_by_id==0){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.id_card_no as SIGNED)";}	 
		else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.designation_level as SIGNED)";}	 
		else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.emp_code as SIGNED)";}	 
		else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-13)." CAST(right(trim(emp.id_card_no), 5)as SIGNED)";}	 
	
		$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by  
	
	

	
	
	function get_service_len($emp)
	{
		global $experience_details_dur;
		foreach($experience_details_dur[$emp] as $days)
		{
		 	$dat=explode("Years",$days);
			$year+=$dat[0];
			$dat=explode("Months",$dat[1]);
			$Months+=$dat[0];
			$dat=explode("Days",$dat[1]);
			$Days+=$dat[0];
		}
		if($Days>=30){ $Months+=1; $Days=$Days-30; }
		if($Months>11){ $year+=1; $Months=$Months-12; }
		return $year." Years, ".$Months." Months,".$Days." Days";
	}
	
	
	function otherDiffDate($end, $out_in_array=false){
        $intervalo = date_diff(date_create(), date_create($end));
        $out = $intervalo->format("Years:%Y,Months:%M,Days:%d");
        if(!$out_in_array)
            return $out;
        $a_out = array();
        array_walk(explode(',',$out),
        function($val,$key) use(&$a_out){
            $v=explode(':',$val);
            $a_out[$v[0]] = $v[1];
        });
        return $a_out;
}
	
	ob_start();	
		
	?>  
	<table cellpadding="0" cellspacing="1" border="1" width="1900" align="center"  class="rpt_table"  rules="all"> 
       <thead>
       	<tr>
       		<th colspan="18" align="center" bordercolor="#FFFFFF"><br /><font size="+1"><? echo  $company_details[$company_id];?></font><br /><br />
                Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br /><br />
            	Employee Increment Due Statement<br /></th>
       </tr>
       
         <tr>
             <th width="50" align="center"><b>SL</b></th>
             <th width="100" align="center"><b>ID Card</b></th>
             <th width="100" align="center"><b>Emp Code</b></th>
             <th width="180" align="center"><b>Name</b></th>
             <th width="130" align="center"><b>Designation</b></th>
             <th width="130" align="center"><b>DOJ</b></th>
             <th width="130" align="center"><b>Service Length this Org.</b></th>
             <th width="130" align="center"><b>Total Experience in other's Org.</b></th>
             <th width="100" align="center"><b>Last Educational Qualification</b></th>
             <th width="100" align="center"><b>Previous Gross</b></th>
             <th width="100" align="center"><b>Current Gross</b></th>
             <th width="100" align="center"><b>Last Increment Amount(tk)</b></th>
             <th width="100" align="center"><b>Last Increment Date</b></th>
             <th width="100" align="center"><b>Due Date</b></th>
             <th width="80" align="center"><b>Days Over</b></th>
             <th width="100" align="center"><b>Proposed incr. Amount</b></th>
             <th width="100" align="center"><b>Approved incr. Amount</b></th>
			 <th width="" align="center"><b>Remarks</b></th>
            
        </tr>
        </thead>
    <?
				/* $sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no,emp.emp_code,emp.joining_date,emp.designation_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.subsection_id,emp.gross_salary,p.new_designation,p.last_designation, 
				a.new_gross_salary,a.old_gross_salary,a.effective_date,a.increment_amount 
				FROM 
					hrm_employee emp left join hrm_increment_mst a on emp.emp_code=a.emp_code   left join hrm_employee_promotion p on emp.emp_code=p.emp_code  and p.effective_date=(select MAX(p.effective_date) from hrm_employee_promotion p where  emp.emp_code=p.emp_code)
				WHERE
					 emp.status_active=1 $category  $cbo_company_id $department_id $location_id $division_id $section_id $subsection_id $designation_id $category group by  emp.emp_code  $orderby" ;( this is previous code and it is used when needed promotion table)*/
					 
	 $sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no,emp.emp_code,emp.joining_date,emp.designation_id,emp.location_id,
	 emp.division_id,emp.department_id,emp.section_id,emp.subsection_id,emp.gross_salary,
				a.new_gross_salary,a.old_gross_salary,a.effective_date,a.increment_amount 
				FROM 
					hrm_employee emp left join hrm_increment_mst a on emp.emp_code=a.emp_code  
				WHERE
					 emp.status_active=1 $category  $cbo_company_id $department_id $location_id $division_id $section_id $subsection_id $designation_id $category group by $dynamic_groupby
		$orderby" ;				 
					 
			//SELECT e.emp_code,e.joining_date,MAX(b.effective_date),b.* from hrm_employee e left join  hrm_increment_mst b on e.emp_code=b.emp_code WHERE DATEDIFF('2013-06-06',e.joining_date)>160 and e.emp_code='0004289' and initial=0 and is_approved=2 group by e.emp_code order by b.effective_date desc		 
		
		
		
					 
	//echo $sql; 
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		
	$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
	$sl=0;$ct=1;
	while($rows=mysql_fetch_assoc($result)){	
		
		$sl++;
 		
		   //start header print---------------------------------//
		   if($sl==1)
		   {
				$location_arr[$rows[location_id]]=$rows[location_id];
				$division_arr[$rows[division_id]]=$rows[division_id];
				$department_arr[$rows[department_id]]=$rows[department_id];
				$section_arr[$rows[section_id]]=$rows[section_id];
				$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
	
			 
			  
			 
                
				 
		   }//end if condition of header print
		   //end header print
		
 		//---------------------------------------------------------------------------------------------------	
		//---------------------------------------------------------------------------------------------------
		
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($rows[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($rows[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($rows[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($rows[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($rows[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$rows[section_id]]=$rows[section_id];
 											$subsection_arr=array();
											$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
											$new_sec=1;

											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$rows[department_id]]=$rows[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$rows[section_id]]=$rows[section_id];
										$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$rows[division_id]]=$rows[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$rows[department_id]]=$rows[department_id];
									$section_arr[$rows[section_id]]=$rows[section_id];
									$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$rows[location_id]]=$rows[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$rows[division_id]]=$rows[division_id];
						$department_arr[$rows[department_id]]=$rows[department_id];
						$section_arr[$rows[section_id]]=$rows[section_id];
						$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
		}
		
		
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$rows[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$rows[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$rows[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$rows[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$rows[subsection_id]]."  ,";
			}
		

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr><td colspan="26"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}
						
	
	//-------------get the service Length--------------//
	
	$joining_date=$rows['joining_date']; 
	$current_date=date("d-m-Y");
	$service_length=datediff('d',$joining_date,$current_date);
//---------------------end service Length----------//	
	
	
	$effective_date=$rows['effective_date']; 
	if($effective_date=="")$effective_date=$rows['joining_date'];
	$due_date=date('Y-m-d',strtotime('+'.$basis.' days', strtotime($effective_date)));
	$cur_date=date("d-m-Y");
	$days_over=datediff('d',$due_date,$cur_date);
	
	if($days_over < $basis){
		
	}else{
	?>   
    		<?
            if ($ct%2==0) $bgcolor="#EEEEEE"; 
			else $bgcolor="#FFFFFF";
			?>
            <tr style="font-size:10px;" bgcolor="<? echo $bgcolor;?>">
            	<td><? echo $ct; ?></td>
            	<td align="center">&nbsp;<? echo $rows['id_card_no']; ?></td>
                <td align="center">&nbsp;<? echo $rows['emp_code']; ?></td>
                <td align="left">&nbsp;<? echo $rows[name]; ?></td>
				<td align="left">&nbsp;<? echo $designation_chart[$rows['designation_id']]; ?></td> 
                <td align="left">&nbsp;<? echo convert_to_mysql_date($rows['joining_date']); ?></td>
                <td align="right">
				<? echo otherDiffDate($rows['joining_date']);
					?>
                </td>
              	<td align="center">
				<?   echo get_service_len($rows['emp_code']);  ?>
                 </td> 	 	
				<td align="center">&nbsp;<? echo $education_details[$rows['emp_code']]; ?></td>
                <td align="right">&nbsp;<?  echo $rows['old_gross_salary']; ?></td>
                <td align="right">&nbsp;<? echo $rows['gross_salary']; ?></td>
                <td align="right">&nbsp;<? echo $rows['increment_amount']; ?></td>
                <td align="right">&nbsp;<? echo convert_to_mysql_date($rows['effective_date']); ?></td>
                <td align="right">&nbsp;<? echo convert_to_mysql_date($due_date); ?></td>
				<td align="center">&nbsp;<? echo $days_over; ?></td>
                <td align="right">&nbsp;<? //echo $rows['increment_amount']; ?></td>
                <td align="right">&nbsp;<? //echo convert_to_mysql_date($rows['increment_amount']);approved amount ?></td>
				<td align="">&nbsp;</td>
				
			</tr>

<?
        $ct++;
	}
	}
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}



?>	

