<?

/*########################################

	Completed by :Ekram
	Date         :29-09-2014
########################################*/
session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}
	
	

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	

$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}	


	
	
	
	
	$bangArray = array(	
 						'Friday'=>'শুক্রবার', 
						'Saturday'=>'শনিবার', 
						'Sunday'=>'রবিবার', 
						'Monday'=>'সোমবার', 
						'Tuesday'=>'মঙ্গলবার', 
						'Wednesday'=>'বুধবার', 
						'Thursday'=>'বৃহস্পতিবার' 
 					   );
	
	$bangMonthArray = array('January'=>'জানুয়ারি', 'February'=>'ফেব্রুয়ারি', 'March'=>'মার্চ', 'April'=>'এপ্রিল','May'=>'মে','June'=>'জুন', 'July'=>'জুলাই','August'=>'আগস্ট', 'September'=>'সেপ্টেম্বর', 'October'=>'অক্টোবর', 'November'=>'নভেম্বর', 'December'=>'ডিসেম্বর');

	
	extract($_REQUEST);
	
	
	
?>

<style media="print">
	p{page-break-before:always}
</style>


<?

if($type=="pay_slip") // astrotext ltd formate final used
{
	
	ob_start();
	
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];	
	  
	
	$rpt_location=$location_id;
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}	
	if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
	if ($location_id==0 || $location_id=='') $location_id=""; else $location_id="and a.location_id='$location_id'"; 
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and  a.department_id in ($department_id)";   
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)"; 
	if ($subsection_id==0 || $subsection_id=='') $subsection_id="";else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and b.designation_id in ($designation_id)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
		
		if($group_by_id==0){$groupby="a.emp_code,";}
		else if($group_by_id==1){$status_com=1;$groupby="b.company_id,a.emp_code,";}
		else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="b.company_id,b.location_id,a.emp_code,";}
		else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="b.company_id,b.location_id,b.division_id,a.emp_code,";}
		else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,a.emp_code,";}
		else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,a.emp_code,";}
		else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,a.emp_code,";}

	
		//echo $order_by_id; die;	
		if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(b.id_card_no as SIGNED)";}	 
		else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(b.designation_level as SIGNED)";}	 
		else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(a.emp_code as SIGNED)";}	 
		else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(b.id_card_no), 5)as SIGNED)";}	 
	
		$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by 


	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	//new
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10)
	{
		$emp_status="and a.emp_status in ( 0 )";
		//$txt_from_date = $previous_month;
		
	}
	
	
	
	
	//salary details 
	$salary_dtls_arr = array();
	$sql_sal="select d.* from  hrm_fraction_salary_mst m,  hrm_fraction_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date'";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}
	
	
	$sql_emp="select a.*, a.gross_salary as sal_gross_salary,a.basic_salary as sal_basic_salary, a.house_rent as sal_house_rent,a.medical_allowance as sal_medical_allowance,b.* 
			  from  hrm_fraction_salary_mst a, hrm_employee b 
			  where 
			  a.salary_periods like '$txt_from_date' and
			  a.emp_code=b.emp_code 
			  $emp_code
			  $location_id 
			  $division_id
			  $section_id 
			  $subsection_id 
			  $category 
			  $payment_met 
			  $designation_id 
			  $department_id 
			  $company_id
			  $id_card_no 
			  $emp_status 
			  group by $dynamic_groupby
			  $orderby";
	//echo $sql_emp;die;
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	$numrows=mysql_num_rows($exe_sql_emp); 
	$i=1;
	$d=0;
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMP Master Qry starts 
	{ 
	
	$d++;
?>	
	
    <div style="width:350px; float:left; margin-left:20px; ">
  			<table width="350" style="font-size:10px;" cellpadding="0" cellspacing="0">
                	<tr>
                      <td>
                            <div style="font-size:10px;" align="left">
                                <span><img src="includes/Group-logo.png" width="100" height="35" align="left" /></span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:14px; font-weight:bold">
                                রিচ্ অ্যাপারেলস্
                                 লিমিটেড
                                </span><br />
                                <? echo $location_details[$rpt_location]; ?><br />
                                <div align="left" style="font-size:14px; font-weight:bold;font-family:'Kalpurush ANSI'">পে-স্লিপ <? echo $bangMonthArray[$cur_month]; ?>-<span style="font-family:'Kalpurush ANSI'"><? echo $cur_year; ?></span><span style="float:right;">ক্রমিক নং:<? echo $i; ?></span></div>
                        </div>
                      </td>                        
                   </tr>
             </table>
              
            
			<table id="tble_pay_slip" class="rpt_table" width="350" cellpadding="0" cellspacing="0" border="0" style="font-size:10px;font-weight:bold;">
            	
                <tr>
                	<td width="50%">কার্ড নং</td> 
                    <td width="50%" align="left" style="font-size:16px;">&nbsp;<? echo $row_emp[id_card_no];?></td>                 
                </tr>
                 <tr>
                	<td width="50%">গ্রেড </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[salary_grade];?></td>                    
                </tr>
                 <tr>
                	<td width="50%">নাম</td> 
                    <td width="50%" align="left">&nbsp;<? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name];?></td>  	 	                   
                </tr>
                <tr>
                	<td width="50%">পদবী </td> 
                    <td width="50%" align="left">&nbsp;<? if(strlen($designation_chart[$row_emp[designation_id]])>30){echo substr($designation_chart[$row_emp[designation_id]],0,30)."..";} else echo $designation_chart[$row_emp[designation_id]]; ?></td>                    
                </tr>
                <tr>
                	<td width="50%">যোগদানের তারিখ </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo convert_to_mysql_date($row_emp[joining_date]);?></td>                    
                </tr>
             	
                <tr>
                	<td colspan="2" align="center" style="font-size:12px; font-weight:bold"><hr /></td>                 
                </tr>  
                
                <tr>
                	<td width="50%">মাসিক বেতন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[sal_gross_salary];?></td>                    
                </tr>
                <tr>
                	<td width="50%">মুল বেতন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($salary_dtls_arr[$row_emp[emp_code]][1]);   ?></td>                  
                </tr>
                <tr>
                	<td width="50%"> বাড়ি ভাতা </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($salary_dtls_arr[$row_emp[emp_code]][2]); ?></td>                    
                </tr>
                <tr>
                	<td width="50%">চিকিৎসা  ভাতা </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo($salary_dtls_arr[$row_emp[emp_code]][3]); ?></td>                    
                </tr>
                <tr>
                	<td width="50%">যাতায়াত ভাতা</td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<?  echo $salary_dtls_arr[$row_emp[emp_code]][4]; ?></td>                    
                </tr>
                <tr>
                	<td width="50%">খাদ্য ভাতা</td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $salary_dtls_arr[$row_emp[emp_code]][13]; ?></td>                    
                </tr>
                <!--<tr>
                	<td width="50%" >প্রতি ঘণ্টা ওটি হার </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? //echo $row_emp[over_time_rate];?></td>                    
                </tr>-->
                
                <tr>
                	<td width="50%" >উপস্থিত দিন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[total_present_days]+$row_emp[total_late_days];?></td>                    
                </tr>
                <tr>
                	<td width="50%" >নৈমিত্তিক ছুটি </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[casual_leave];?></td>                    
                </tr>
                <tr>
                	<td width="50%" >অসুস্থতা  জনিত ছুটি </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[sick_leave];?></td>                    
                </tr>
                <tr>
                	<td width="50%" >বার্ষিক ছুটি </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[earn_leave];?></td>                    
                </tr>
                <tr>
                	<td width="50%" >মোট প্রদেয় দিন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[payable_days];?></td>                    
                </tr>
                <tr>
                	<td width="50%" >অনুপস্থিত  দিন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[total_abs_days];?></td>                    
                </tr>
                <tr>
                	<td width="50%" >মোট ওটি </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($row_emp[total_over_time]+$row_emp[ot_allowance_hr]);?></td>                    
                </tr>
                <tr>
                	<td width="50%" >মোট কাজের  মজুরী </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($row_emp[sal_gross_salary]);?></td>                    
                </tr>
              	<tr>
                	<td width="50%" >ওটি বেতন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? 
						$ot_rate_min=$row_emp[over_time_rate]/60;
						$ot_hr_spl=explode(".",$row_emp[total_over_time]);
						
				  		$ot_amt =$ot_amnt=($ot_hr_spl[0]*$row_emp[over_time_rate])+($ot_hr_spl[1]*$ot_rate_min);//$row_emp[b_over_time]*$row_emp[over_time_rate];
						$ot_allowance=$row_emp[ot_allowance_amount];
				 		 echo round($ot_amt+$ot_allowance);
					
					//echo ($row_emp[total_over_time]*$row_emp[over_time_rate]);
					?></td>                    
                </tr>
                <tr>
                	<td width="50%" >উপস্থিত বোনাস </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($salary_dtls_arr[$row_emp[emp_code]][35]);?></td>                    
                </tr>
                <tr>
                	<td width="50%" >অন্যান্য ভাতা </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($salary_dtls_arr[$row_emp[emp_code]][19]); ?></td>                    
                </tr>
                 <tr>
                	<td width="50%" >খাদ্য ভাতা কর্তন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($salary_dtls_arr[$row_emp[emp_code]][27]); ?></td>                    
                </tr>
                <tr>
                	<td width="50%" >অনুপস্থিত দিনের জন্য কর্তন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($row_emp[total_absent_deduction_amount]); ?></td>                    
                </tr>
                
                <tr>
                	<td width="50%" >অগ্রীম  কর্তন </td> 
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo ($salary_dtls_arr[$row_emp[emp_code]][21]);?></td>                  
                </tr>
                <tr>
                	<td width="50%" >স্ট্যাম্প </td>
                    <td width="50%" align="left" style="font-family:'Kalpurush ANSI'">&nbsp;<? echo $salary_dtls_arr[$row_emp[emp_code]][28]; ?></td>                   
                </tr>
                 <tr>
                	<td colspan="2" align="center" style="font-size:8px; font-weight:bold"><hr /></td>                 
                </tr>
                <tr>
                	<td width="50%" >মোট প্রদেয় বেতন </td>
                    <td width="50%" align="left" style="font-size:16px;font-family:'Kalpurush ANSI'">&nbsp;<? echo $row_emp[net_payable_amount];?></td>                  
                </tr>                
            </table>
           <div style="height:40px;"></div>
            <!--<div style="height:50px; width:350px"></div>-->
            <div style="font-weight:bold; font-size:12px;width:175px; float:left" align="left">কর্মীর স্বাক্ষর </div>
            <div style="font-weight:bold; font-size:12px;width:175px; float:left" align="center">এইচ আর ডি </div>
        </div> 
        
       
       
		
<?	
	
	$i++;
	/*if($i==2){echo "<p>&nbsp;</p>";$i=0;}*/
	
	 	if($d==1)
        {
        ?>
        <div style="width:50px; float:left"></div>
        <?
        }
		
		if($d==2)
        {
            ?>
            <div class="page_break"></div>
            <div style="clear:both; height:30px;">&nbsp;</div>
            <?
            $d=0;
        }
	
	}
	
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}


//month generated
if($type=="select_month_generate")
{		
		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id and b.type=1 ";//and b.is_locked=0 
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
}


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}



function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}

?>