<?
//Completed by: Sohel
//Date :29-09-2013

session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY weekend ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$weekend_chart[$row['emp_code']] = mysql_real_escape_string( $row['weekend'] );		
	}

	$sql ="select * from lib_payroll_head where  salary_head=1 and type=0 and status_active=1 and is_deleted=0 order by id";
	//echo $sql;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_head_mst = array();
	$salary_head_mst_short = array();
	$salary_head_mst_id = array();
	$i=0;
	while( $row = mysql_fetch_assoc( $result ) ) {	
		$i++;	
			$salary_head_mst[$i] = mysql_real_escape_string( $row['custom_head'] );	
			$salary_head_mst_id[$i] = mysql_real_escape_string( $row['id'] );	
			$salary_head_mst_short[$i]=	mysql_real_escape_string( $row['abbreviation'] );	
	}

//print_r($salary_head_mst_short);die;	
	
	//earning head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4) and id not in (6,7)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head = array();
	$earning_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id[] = mysql_real_escape_string( $row['id'] );	
	}
	
	//print_r($earning_head_id);die;
	//earning head Original Salary Sheet 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head_original = array();
	$earning_head_id_original = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head_original[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id_original[] = mysql_real_escape_string( $row['id'] );	
	}
	//deduction head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (1,2) and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$deduction_head = array();
	$deduction_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$deduction_head[] = mysql_real_escape_string( $row['custom_head'] );
			 $deduction_head_id[] = mysql_real_escape_string( $row['id'] );		
	}


extract ( $_GET );
extract ( $_POST );


  
if($action=="saving_fund_report_sheet") 	
{
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
	$company_info_sql="select * from lib_company where id='$company_id'";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	$rpt_company=$company_id;
	
	
	  
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id='$department_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";	
	if ($designation_id==0) $designation_id=""; else $designation_id="and  b.designation_id='$designation_id'";
	
	//if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	if ($cbo_salary_sheet=='') {$emp_status="";} 
	else if($cbo_salary_sheet==0){$emp_status="and emp.status_active=1";} //else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	else if($cbo_salary_sheet==1){$emp_status="and emp.status_active in (0,2)";}
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="a.emp_code";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,a.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,a.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,a.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.emp_code,";}

	//echo $order_by_id; die;	
	//if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(b.id_card_no as SIGNED)";}	 
	 if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(emp.designation_level as SIGNED)";}	 
	 else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(a.emp_code as SIGNED)";}	 
	 else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(emp.id_card_no), 5)as SIGNED)";}	 
	//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
	
$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by	

	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$salary_sql="SELECT a.emp_code,b.opening_count,b.pay_amount FROM hrm_salary_mst a, hrm_salary_dtls b where a.id=b.salary_mst_id and b.opening_count!=0 and b.salary_head_id=25 and a.salary_periods >='$txt_from_date'";
		$result = mysql_query( $salary_sql ) or die( $salary_sql . "<br />" . mysql_error() );
		$emp_code_ar="";
		$opening_count=array();
		$opening_amount=array();
		while($salary_emp_sql = mysql_fetch_array($result))
		{
			$opening_count[$salary_emp_sql[emp_code]]=$salary_emp_sql[opening_count];
			$opening_amount[$salary_emp_sql[emp_code]]=$salary_emp_sql[pay_amount];
		}
		
		//echo $salary_sql="SELECT a.emp_code,count(a.emp_code) as csala,b.pay_amount FROM hrm_salary_mst a, hrm_salary_dtls b where a.id=b.salary_mst_id and b.opening_count=0 and  b.salary_head_id=25 and a.salary_periods between '$txt_from_date' and '$txt_to_date' group by emp_code";
		$salary_sql="SELECT a.emp_code,b.pay_amount,a.`salary_periods` FROM hrm_salary_mst a, hrm_salary_dtls b where a.id=b.salary_mst_id and b.opening_count=0 and b.salary_head_id=25 and a.salary_periods between '$txt_from_date' and '$txt_to_date' order by b.id ";
		
		$result = mysql_query( $salary_sql ) or die( $salary_sql . "<br />" . mysql_error() );
		$emp_code_ar="";
		$salary_count=array();
		$salary_pf_total=array();
		$total_deposited_arr=array();
		while($salary_emp_sql = mysql_fetch_array($result))
		{
			$salary_count[$salary_emp_sql[emp_code]]+=1;
			$salary_pf_total[$salary_emp_sql[emp_code]]=$salary_emp_sql[pay_amount];
			$total_deposited_arr[$salary_emp_sql[emp_code]]+=$salary_emp_sql[pay_amount];
		}
		//echo $salary_count['0000166']."==".$salary_pf_total['0000166']."==".$total_deposited_arr['0000166'];
		
		
		
		
		$salary_sql="SELECT emp_code,pf_effective_date FROM hrm_employee_entitlement where pf_effective_date<>'0000-00-00'";
		$result = mysql_query( $salary_sql ) or die( $salary_sql . "<br />" . mysql_error() );
		$emp_code_ar="";
		$pf_entitle_length=array();
		while($salary_emp_sql = mysql_fetch_array($result))
		{
			$dur=datediff( "m", $salary_emp_sql[pf_effective_date], $txt_to_date ); 
			$pf_entitle_length[$salary_emp_sql[emp_code]]=$dur;
		}
	   /*$sql_emp="select CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,emp.id_card_no,emp.joining_date,a.*,b.salary_head_id,sum(b.pay_amount) as pay_amount,count(a.id) as cid
			   from 
			   hrm_salary_mst a, hrm_salary_dtls b,hrm_employee emp
			   where
			   a.id=b.salary_mst_id and a.emp_code=emp.emp_code and b.salary_head_id=25 and 
			   a.salary_periods between '$txt_from_date' and '$txt_to_date'
			   $emp_code $category $company_id $location_id $division_id $section_id $subsection_id $designation_id $department_id group by $groupby a.emp_code $orderby
			   ";*/
		 $sql_emp="select CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name ,emp.designation_id,emp.id_card_no,emp.joining_date, a.emp_code,a.company_id,a.location_id,a.department_id,a.division_id,a.section_id,a.subsection_id 
			   from 
			   hrm_salary_mst a,  hrm_employee emp
			   where
			   a.emp_code=emp.emp_code and 
			   a.salary_periods between '$txt_from_date' and '$txt_to_date'
			   $emp_code $category $company_id $location_id $division_id $section_id $subsection_id $designation_id $department_id $emp_status group by a.emp_code $orderby
			   ";
	//$i=1;
	$ct=0;
	$sl=0;
	$r=0;
	ob_start();
 	?>
     <div style="width:1350px;  height:280px" id="scroll_body" align="left">
	<fieldset style="width:1350px;">
        <table width="1350" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all" align="center">
            <tr style="text-align:center; font-weight:bold" height="70">    	 
                <td colspan="14">
                    <div style="font-size:12px;"><strong style="font-size:20px"><?	echo $company_info_result["company_name"];?></strong></div>
                    <div style="font-size:12px;margin-top:5px">Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                    <div style="font-size:12px;"><strong style="font-size:15px">Saving Fund Details Report</strong></div>
                    From:&nbsp;<? echo convert_to_mysql_date($txt_from_date);?>	&nbsp;&nbsp; To:&nbsp;<? echo convert_to_mysql_date($txt_to_date);?>
                  </div>
                </td>
            </tr>		
        </table>
        	
   <table width="1350" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all" align="left">      
<?
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{ 
	$sl++;
		   //start header print---------------------------------//
		   if($sl==1)
		   {
				$location_arr[$emp[location_id]]=$emp[location_id];
				$division_arr[$emp[division_id]]=$emp[division_id];
				$department_arr[$emp[department_id]]=$emp[department_id];
				$section_arr[$emp[section_id]]=$emp[section_id];
				$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];

			   ?> 
		<thead> 	
        <tr>
                <th width="25" align="center"><strong>SL</strong></th>
                <th width="150" align="center"><strong>Employee Name</strong></th>
                <th width="80" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Division</strong></th>
                <th width="80" align="center"><strong>Section</strong></th>
                <th width="80" align="center"><strong>Joining Date</strong></th>
                <th width="80" align="center"><strong>Deposit Per Month(TK)</strong></th>
                <th width="80" align="center"><strong>Total Deposit Month</strong></th>
                <th width="80" align="center"><strong>Total Deposited (TK)</strong></th>
                <th width="80" align="center"><strong>Matured (After 36 Month)</strong></th>
                <th width="80" align="center"><strong>Restart Month</strong></th>
                <th width="80" align="center"><strong>Remarks</strong></th>
        </tr>
    </thead>
				<?
		   }//end if condition of header print
		   //end header print
//========================================================================================================================================
	if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$emp[section_id]]=$emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$emp[department_id]]=$emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$emp[section_id]]=$emp[section_id];
										$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$emp[division_id]]=$emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$emp[department_id]]=$emp[department_id];
									$section_arr[$emp[section_id]]=$emp[section_id];
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$emp[location_id]]=$emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
		}
		
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td colspan="8" width=''><b> Subsection Total</b></td>
							<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                            <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                            <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
                            <td colspan="3" align="center">&nbsp;<?php  ?></td>
						<?
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td colspan="8" width=''><b>SectionTotal</b></td>
							<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                            <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                            <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
                            <td colspan="3" align="center">&nbsp;<?php  ?></td>
						<?
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td colspan="8" align="center">Department Total</td>
					<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                    <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                    <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
                    <td colspan="3" align="center">&nbsp;<?php  ?></td>
						<?
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td colspan="8" align="center" >Division Total</td>
					<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                    <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                    <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
                    <td colspan="3" align="center">&nbsp;<?php  ?></td>
						<?
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td colspan="8" align="center">Location Total</td>
                    <td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                    <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                    <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
                    <td colspan="3" align="center">&nbsp;<?php  ?></td>
						<?
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp[subsection_id]]."  ,";
			}
	

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr style="font-weight:bold; font-size:14px"><td colspan="8"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}
		$ct++;
 		$r++;
//==============================================================================================================================================================
	
		if ($r%2==0) $bgcolor="#EFEFEF"; 
		else $bgcolor="#FFFFFF";
		//if($salary_pf_total[$emp[emp_code]]!=0)
		//{
		?>
    		<tr bgcolor="<? echo $bgcolor; ?>">
				<td width="25" align="left"><?php echo $r;?></td>
                <td width="150" align="left">&nbsp;<?php echo $emp['name'] ; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
				<td width="80" align="center">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $emp['id_card_no'];?></td>
				<td width="80" align="center">&nbsp;<?php echo $division_details[$emp['division_id']]; ?></td>
				<td width="80" align="center"><?php echo $section_details[$emp['section_id']];  ?></td>
                <td width="80" align="center"><?php echo convert_to_mysql_date($emp['joining_date']); ?></td>
                <td width="80" align="center"><?php echo $deposit_per_month= $salary_pf_total[$emp[emp_code]]; $deposit_per_month_total +=$deposit_per_month;  ?></td>
                <td width="80" align="center">&nbsp;<?php echo $month_count=$opening_count[$emp[emp_code]]+$salary_count[$emp[emp_code]];   $month_count_total+=$month_count; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $total_deposit=$total_deposited_arr[$emp[emp_code]];
				//echo $total_deposit=$salary_pf_total[$emp[emp_code]]*$month_count; 
				$total_deposit_sum+=$total_deposit; 
				//$salary_pf_total[$emp[emp_code]]*$month_count;  $opening_amount[$emp[emp_code]]+$salary_pf_total[$emp[emp_code]];?></td>
				<td width="80" align="center">&nbsp;<?php if($emp['cid']>36) echo "Yes"; else echo "No"; ?></td>
				<td width="80" align="center"><?php  ?></td>
                <td width="80" align="center" title=""><?php  ?></td>
            </tr>
	<?	
	
		//}
$ct++;
	}//while loop end here 
	
	if($status_subsec==1)
		{
 			?><tr style="font-weight:bold">
					<td  colspan="8"><b>Subsection Total</b></td>
							<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
				<td colspan="3" align="center">&nbsp;<?php  ?></td>					
						</tr><?
				
				$subsection_total_ot=0;	

		}
		
		if($status_sec==1)
		{
 			?><tr style="font-weight:bold">
					<td colspan="8">Section Total</td>
							<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
				<td colspan="3" align="center">&nbsp;<?php  ?></td>					
						</tr><?
					
			$section_total_ot=0;
						
		}
		
		if($status_dept==1 )
		{
 			?><tr style="font-weight:bold">
					<td   colspan="8">Department Total</td>
					<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
				<td colspan="3" align="center">&nbsp;<?php  ?></td>					
			 </tr><?
				
					
		$department_total_ot=0;
			
		}
		 
		if($status_divis==1 )
		{
 			?><tr style="font-weight:bold">
							<td  colspan="8">Division Total</td>
							<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
				<td colspan="3" align="center">&nbsp;<?php  ?></td>						
						</tr><?
					
	$division_total_ot=0;
 			
		}
		 
		if($status_loc==1 )
		{
 			?><tr style="font-weight:bold">
					<td  colspan="8">Location Total</td>
							<td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
				<td colspan="3" align="center">&nbsp;<?php  ?></td>
						</tr><?
		$location_total_ot=0;		
 			
		}
		
	?>
    		<tr bgcolor="<? echo $bgcolor; ?>">
				<td align="center" colspan="8"><strong>Greand Total</strong></td>
                <td width="80" align="center"><?php echo $deposit_per_month_total;  ?></td>
                <td width="80" align="center">&nbsp;<?php echo $month_count_total; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $total_deposit_sum; ?></td>
				<td colspan="3" align="center">&nbsp;<?php  ?></td>
            </tr>
</table>
</fieldset>	
</div>
 <? 
	//echo signeture_table(9,$rpt_company,"1100px"); 
	$html = ob_get_contents();
	ob_clean();	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
		@unlink($filename);
	}		
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	echo "$html"."####"."$name";
	exit();
 	}
 ?>


<?
/*
//month generated
if($type=="select_month_generate")
	{		
		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}*/

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}



/*
ALTER TABLE `hrm_salary_dtls` ADD `opening_count` TINYINT( 2 ) NOT NULL DEFAULT '0' AFTER `pay_slip_id`

UPDATE  hrm_salary_dtls AS t1, hrm_salary_mst AS t2 
  SET
t1.opening_count=t1.pay_slip_id
  WHERE t1.salary_mst_id =t2.id and t2.salary_periods='2013-01-01' and t1.salary_head_id=25
*/

?>
