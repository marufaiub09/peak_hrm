<?
//Completed by: Sohel
//Date :29-09-2013

session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY weekend ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$weekend_chart[$row['emp_code']] = mysql_real_escape_string( $row['weekend'] );		
	}

extract ( $_GET );
extract ( $_POST );

if($action=="daily_salary_ot_report") 	
{
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit']; 
			$first_ot_limit = $res['first_ot_limit'];	
	}
	
	
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	$get_days=datediff(d,$txt_from_date,$txt_to_date);
	//echo $get_days;
	
	$part=explode("-",$txt_from_date);
	$date_part = $part[0] . "-" . $part[1];
	
	$get_days_mo=cal_days_in_month(CAL_GREGORIAN, $part[1], $part[0]);  //total day in this month
	//echo $get_days_mo;
	
	$company_info_sql="select * from lib_company where id='$company_id'";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	$rpt_company=$company_id;
	
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	
	/*if ($id_card=="") $id_card_no=""; else $id_card_no="and emp.id_card_no like '".$id_card."'";
	if ($emp_code=="") $emp_code_no=""; else $emp_code_no="and emp.emp_code in ($emp_code)";
	if ($emp_code=="") $emp_code_search=""; else $emp_code_search="and emp_code in ($emp_code)";*/
	
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and emp.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	if ($emp_code=="") $emp_code_search=""; else $emp_code_search="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	
	/*if ($cbo_salary_sheet=='') {$emp_status="";} 
	else if($cbo_salary_sheet==0){$emp_status="and emp.status_active=1";} //else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	else if($cbo_salary_sheet==1){$emp_status="and emp.status_active in (0,2)";}*/
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,emp.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,emp.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,emp.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,emp.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,emp.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,emp.emp_code,";}

	//echo $order_by_id; die;
	 if($order_by_id==0){$orderby="order by ".$groupby." CAST(emp.id_card_no as SIGNED)";}	
	 else if($order_by_id==1){$orderby="order by ".$groupby." CAST(emp.designation_level as SIGNED)";}	 
	 else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp.emp_code as SIGNED)";}	 
	 else if($order_by_id==3){$orderby="order by ".$groupby." CAST(right(trim(emp.id_card_no), $id_card_no_ord)as SIGNED)";}	 
	
$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by	

	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	
	
	  /*  $salary_sql="SELECT emp_code,gross_salary,basic_salary,daily_gross_salary FROM  hrm_salary_mst where salary_periods like '$part[0]-$part[1]-%'";
		$result_salary = mysql_query( $salary_sql ) or die( $salary_sql . "<br />" . mysql_error() );
		$basic_salary_arr=array();
		$salary_table_emp="";
		//$daily_gross_salary_arr=array();
		while($salary_row = mysql_fetch_array($result_salary))
		{
			$salary_table_emp=$salary_row[emp_code];
			$basic_salary_arr[$salary_row[emp_code]]=$salary_row[basic_salary];
			//$daily_gross_salary_arr[$salary_row[emp_code]]=$salary_row[daily_gross_salary];
		}*/
		
		
	$basic_salary_arr_emp=array();
	$sal="SELECT salary_periods,emp_code,basic_salary,gross_salary,daily_gross_salary FROM  hrm_salary_mst where  salary_periods like '$part[0]-$part[1]-%'";
	//echo $sal;die;
	$res= mysql_query( $sal ) or die( $sal . "<br />" . mysql_error() );
	
	while($salary = mysql_fetch_array($res))
	{
		 $basic_salary_arr_emp[$salary[emp_code]]=$salary[basic_salary];
	}
	
	

	$salary_sql_emp="SELECT emp_code,amount FROM hrm_employee_salary where payroll_head=1 ";
	$result_salary_emp = mysql_query( $salary_sql_emp ) or die( $salary_sql_emp . "<br />" . mysql_error() );
	$basic_salary_arr_emp_employee=array();
	while($salary_row_emp = mysql_fetch_array($result_salary_emp))
	{
		$basic_salary_arr_emp_employee[$salary_row_emp[emp_code]]=$salary_row_emp[amount];
	}
	
		
		/*$salary_sql_emp="SELECT emp_code,amount FROM hrm_employee_salary where payroll_head=1 ";
		$result_salary_emp = mysql_query( $salary_sql_emp ) or die( $salary_sql_emp . "<br />" . mysql_error() );
		$basic_salary_arr_emp=array();
		//$daily_gross_salary_arr=array();
		while($salary_row_emp = mysql_fetch_array($result_salary_emp))
		{
			$basic_salary_arr_emp[$salary_row_emp[emp_code]]=$salary_row_emp[amount];
			//$daily_gross_salary_arr[$salary_row[emp_code]]=$salary_row[daily_gross_salary];
		}*/
		
		
		$sql=" SELECT emp_code,";   
		for($i=0;$i<$get_days; $i++)
		{
		$c_date=add_date($txt_from_date,$i);
		if($i!=0) $sql .=",";
		$sql .=" sum(CASE WHEN attnd_date ='$c_date' THEN total_over_time_min END) AS 'OT".$c_date."',  sum(CASE WHEN attnd_date ='$c_date' and is_regular_day=0 THEN total_over_time_min END) AS 'BOT".$c_date."'";
		}
		$sql .="from  hrm_attendance  where attnd_date between '".$txt_from_date."' and '".$txt_to_date."' $emp_code_search group by emp_code order by emp_code";	
		$exe_sql_attnd=mysql_db_query($DB, $sql);
		$employee_ot=array();
		while($data=mysql_fetch_array($exe_sql_attnd))
		{
			for($i=0;$i<$get_days; $i++)
			{
				$c_date=add_date($txt_from_date,$i);
				$employee_ot[$data['emp_code']][$c_date]=$data['OT'.$c_date];
			}
			//print_r($employee_ot[$data['emp_code']][$c_date]."=");
		} 
 
	$subsec_total_today_salary=0;$sec_total_today_salary=0;$dep_total_today_salary=0;$divis_total_today_salary=0;$loc_total_today_salary=0;

	$subsec_today_ot_minite=0;$sec_today_ot_minite=0;$dep_today_ot_minite=0;$divis_today_ot_minite=0;$loc_today_ot_minite=0;
	
	$subsec_total_today_ot_hour=0;$sec_total_today_ot_hour=0;$dep_total_today_ot_hour=0;$divis_total_today_ot_hour=0;$loc_total_today_ot_hour=0;		
		
	$subsec_total_today_ot_amount=0;$sec_total_today_ot_amount=0;$dep_total_today_ot_amount=0;$divis_total_today_ot_amount=0;$loc_total_today_ot_amount=0;	
	
	$subsec_total_payable_tk=0;$sec_total_payable_tk=0;$dep_total_payable_tk=0;$divis_total_payable_tk=0;$loc_total_payable_tk=0;
	
	
	
			   
	$sl=0;
	$r=1;
	ob_start();
 	?>
     <div style="width:1350px;  height:280px" id="scroll_body" align="left">
	<fieldset style="width:1350px;">
        <table width="1350" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all" align="center">
            <tr style="text-align:center; font-weight:bold" height="70">    	 
                <td colspan="14">
                    <div style="font-size:12px;"><strong style="font-size:20px"><?	echo $company_info_result["company_name"];?></strong></div>
                    <div style="font-size:12px;margin-top:5px">Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
                    <div style="font-size:12px;"><strong style="font-size:15px">Daily Salary and OT Report</strong></div>
                    From:&nbsp;<? echo convert_to_mysql_date($txt_from_date);?>	&nbsp;&nbsp; To:&nbsp;<? echo convert_to_mysql_date($txt_to_date);?>
                  </div>
                </td>
            </tr>		
        </table>
        	
   <table width="1350" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all" align="left">      
<?
	 $sql_emp="select CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name ,count(a.id) as cid,emp.id_card_no,emp.joining_date, emp.emp_code,a.company_id,a.location_id,a.department_id,a.division_id,a.section_id,a.subsection_id,a.designation_id,emp.gross_salary,a.total_over_time_min,count(a.status) as status from hrm_employee emp,hrm_attendance a
			   where
					emp.emp_code=a.emp_code and 
					a.attnd_date BETWEEN '$txt_from_date' AND '$txt_to_date' and a.status<>'A' and a.status<>'LWP'
					$emp_code
					$id_card_no
					$category 
					$company_id 
					$location_id 
					$division_id 
					$department_id 
					$section_id 
					$subsection_id 
					$designation_id 
					group by $dynamic_groupby $orderby";
					
					//echo $sql_emp;
					
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{
		//echo $emp[cid]; 
	$sl++;
	
		   if($sl==1)
		   {
				$location_arr[$emp[location_id]]=$emp[location_id];
				$division_arr[$emp[division_id]]=$emp[division_id];
				$department_arr[$emp[department_id]]=$emp[department_id];
				$section_arr[$emp[section_id]]=$emp[section_id];
				$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];

			   ?> 
		<thead> 	
                <th width="25" align="center"><strong>SL</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                 <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="150" align="center"><strong>Employee Name</strong></th>
                <th width="80" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Gross Salary</strong></th>
                <th width="80" align="center"><strong>Basic Salary</strong></th>
                <th width="80" align="center"><strong>Today Salary</strong></th>
                <th width="80" align="center"><strong>Today OT Houre</strong></th>
                <th width="80" align="center"><strong>Today OT Amount</strong></th>
                <th width="80" align="center"><strong>Total Payable(TK)</strong></th>
                <th width="80" align="center"><strong> Remarks</strong></th>
     </thead>
				<?
		   }
		  //========================================================================================================================================
		if($sl!=1)
		{
			$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
			if(in_array($emp[location_id],$location_arr) && $status_loc==1)
			{
				if(in_array($emp[division_id],$division_arr) && $status_divis==1)
				{
						if(in_array($emp[department_id],$department_arr) && $status_dept==1)
						{
							if(in_array($emp[section_id],$section_arr) && $status_sec==1)
							{ 
								if(in_array($emp[subsection_id],$subsection_arr) && $status_subsec==1)
								{}
								else if($status_subsec==1)
								{
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_subsec=1;
								}
							}
							else if($status_sec==1)
							{
								$section_arr[$emp[section_id]]=$emp[section_id];
								$subsection_arr=array();
								$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
								$new_sec=1;
								$new_subsec=1;
							}
						}
						else if($status_dept==1)
						{
							$department_arr[$emp[department_id]]=$emp[department_id];
							$section_arr=array();
							$subsection_arr=array();
							$section_arr[$emp[section_id]]=$emp[section_id];
							$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}
				}//division
				else if($status_divis==1)
				{
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
				}//division else
			}//location
			else if($status_loc==1)
			{
			$location_arr[$emp[location_id]]=$emp[location_id];
			$division_arr=array();
			$department_arr=array();
			$section_arr=array();
			$subsection_arr=array();
			$division_arr[$emp[division_id]]=$emp[division_id];
			$department_arr[$emp[department_id]]=$emp[department_id];
			$section_arr[$emp[section_id]]=$emp[section_id];
			$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
			$new_loc=1;
			$new_divis=1;
			$new_dept=1;
			$new_sec=1;
			$new_subsec=1;
			}//location else
		
		}
		
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="7" align="left"><b> Subsection Total</b></td>
                    <td align="center"><?php echo $subsec_total_today_salary;  ?></td>
                    <td align="center"><?php echo $subsec_total_today_ot_hour;  ?></td>
                    <td align="center">&nbsp;<?php echo $subsec_total_today_ot_amount; ?></td>
                    <td align="center">&nbsp;<?php echo $subsec_total_payable_tk; ?></td>
                    <td align="center">&nbsp;<?php  ?></td>
               </tr>
                    <?
                    $subsec_today_ot_minite=0;
                    $subsec_total_today_ot_hour=0;		
                    $subsec_total_today_ot_amount=0;	
                    $subsec_total_payable_tk=0;
					$subsec_total_today_salary=0;
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="7" align="left"><b>SectionTotal</b></td>
                     <td align="center"><?php echo $sec_total_today_salary;  ?></td>
                    <td align="center"><?php echo $sec_total_today_ot_hour;  ?></td>
                    <td align="center">&nbsp;<?php echo $sec_total_today_ot_amount; ?></td>
                    <td align="center">&nbsp;<?php echo $sec_total_payable_tk; ?></td>
                    <td align="center">&nbsp;<?php  ?></td>
               </tr>
                    <?
                    $sec_today_ot_minite=0;
                    $sec_total_today_ot_hour=0;		
                    $sec_total_today_ot_amount=0;
                    $sec_total_payable_tk=0;
					$sec_total_today_salary=0;
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="7" align="left">Department Total</td>
                    <td align="center"><?php echo $dep_total_today_salary;  ?></td>
                    <td align="center"><?php echo $dep_total_today_ot_hour;  ?></td>
                    <td align="center">&nbsp;<?php echo $dep_total_today_ot_amount; ?></td>
                    <td align="center">&nbsp;<?php echo $dep_total_payable_tk; ?></td>
                    <td align="center">&nbsp;<?php  ?></td>
               </tr>
                    <?
                    $dep_today_ot_minite=0;
                    $dep_total_today_ot_hour=0;		
                    $dep_total_today_ot_amount=0;	
                    $dep_total_payable_tk=0;
					$dep_total_today_salary=0;
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="7" align="left" >Division Total</td>
                    <td align="center"><?php echo $divis_total_today_salary;  ?></td>
                    <td align="center"><?php echo $divis_total_today_ot_hour;  ?></td>
                    <td align="center">&nbsp;<?php echo $divis_total_today_ot_amount; ?></td>
                    <td align="center">&nbsp;<?php echo $divis_total_payable_tk; ?></td>
                    <td align="center">&nbsp;<?php  ?></td>
               </tr>
                    <?
                    $divis_today_ot_minite=0;
                    $divis_total_today_ot_hour=0;	
                    $divis_total_today_ot_amount=0;
                    $divis_total_payable_tk=0;
					$divis_total_today_salary=0;
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="7" align="left">Location Total</td>
                    <td align="center"><?php echo $loc_total_today_salary;  ?></td>
                    <td align="center"><?php echo $loc_total_today_ot_hour;  ?></td>
                    <td align="center">&nbsp;<?php echo $loc_total_today_ot_amount; ?></td>
                    <td align="center">&nbsp;<?php echo $loc_total_payable_tk; ?></td>
                    <td align="center">&nbsp;<?php  ?></td>
                </tr>
                    <?
                    $loc_today_ot_minite=0;
                    $loc_total_today_ot_hour=0;		
                    $loc_total_today_ot_amount=0;	
                    $loc_total_payable_tk=0;
					$loc_total_today_salary=0;
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp[subsection_id]]."  ,";
			}
	

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr style="font-weight:bold; font-size:14px"><td colspan="12"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}
		
//==============================================================================================================================================================
	$total_ot_hr=0;
	for($i=0;$i<$get_days; $i++)
	{
		$c_date=add_date($txt_from_date,$i);
		$total_ot_hr +=get_buyer_ot_hr($employee_ot[$emp['emp_code']][$c_date],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
	}
	
	$total_ot_hr=explode(".",$total_ot_hr);
	$ot_minit=($total_ot_hr[0]*60)+$total_ot_hr[1];
	$today_ot_hour = sprintf("%d.%02d", abs((int)($ot_minit/60)), abs((int)($ot_minit%60)));
	
	if($basic_salary_arr_emp[$emp[emp_code]]==0 || $basic_salary_arr_emp[$emp[emp_code]]=="")
	{
		$basic_salary_arr_emp[$emp[emp_code]]=$basic_salary_arr_emp_employee[$emp[emp_code]];
	}
	else
	{
		$basic_salary_arr_emp[$emp[emp_code]]=$basic_salary_arr_emp[$emp[emp_code]];
	}


/*
$sal="SELECT salary_periods,emp_code,basic_salary,gross_salary,daily_gross_salary FROM  hrm_salary_mst where emp_code=$emp[emp_code] and salary_periods like '$part[0]-$part[1]-%'";
$res= mysql_query( $sal ) or die( $sal . "<br />" . mysql_error() );
if( mysql_num_rows($res)!=0)
{
	while($salary = mysql_fetch_array($res))
	{
		 $basic_salary_arr_emp[$salary[emp_code]]=$salary[basic_salary];
	}
}
else
{
	$salary_sql_emp="SELECT emp_code,amount FROM hrm_employee_salary where payroll_head=1 ";
	$result_salary_emp = mysql_query( $salary_sql_emp ) or die( $salary_sql_emp . "<br />" . mysql_error() );
	$basic_salary_arr_emp=array();
	//$daily_gross_salary_arr=array();
	while($salary_row_emp = mysql_fetch_array($result_salary_emp))
	{
		$basic_salary_arr_emp[$salary_row_emp[emp_code]]=$salary_row_emp[amount];
		//$daily_gross_salary_arr[$salary_row[emp_code]]=$salary_row[daily_gross_salary];
	}
}
*/

							
	if ($r%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
	?>
	<tr bgcolor="<? echo $bgcolor; ?>">
		<td align="left"><?php echo $r;?></td>
		<td align="center">&nbsp;<?php echo $emp['id_card_no'];?></td>
		<td width="80" align="center">&nbsp;<?php echo $emp[emp_code]; ?></td>
		<td align="left">&nbsp;<?php echo $emp['name'] ; ?></td>
		<td align="center">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
		<td align="center">&nbsp;<?php echo round($emp[gross_salary]); ?></td>
		<td align="center"><?php echo round($basic_salary_arr_emp[$emp[emp_code]]);  ?></td>
		<td align="center"><?php echo $today_salary=round(($emp[gross_salary]/$get_days_mo)*$emp[cid]); ?></td>
		<td align="center"><?php echo $today_ot_hour;  ?></td>
		<td width="80" align="center">&nbsp;<?php echo $today_ot_amount=round((($basic_salary_arr_emp[$emp[emp_code]]/104)/60)*$ot_minit); ?></td>
		<td width="80" align="center"><?php echo $payable_tk=round(($today_salary+$today_ot_amount)); ?></td>
		<td width="80" align="center" title=""><?php //echo $emp[status]; ?></td>
	</tr>
	<?
	$new_today_ot_hour=explode(".",$today_ot_hour);
	$total_ot_minit=($new_today_ot_hour[0]*60)+$new_today_ot_hour[1];
	$today_ot_minite+=$total_ot_minit;
	$grang_total_today_ot_hour = sprintf("%d.%02d", abs((int)($today_ot_minite/60)), abs((int)($today_ot_minite%60)));	
	$grang_total_today_ot_amount+=$today_ot_amount;	
	$grang_total_payable_tk+=$payable_tk;
	$grang_total_today_salary+=$today_salary;
	
	$subsec_today_ot_hour=explode(".",$today_ot_hour);
	$subsec_total_ot_minit=($subsec_today_ot_hour[0]*60)+$subsec_today_ot_hour[1];
	$subsec_today_ot_minite+=$subsec_total_ot_minit;
	$subsec_total_today_ot_hour = sprintf("%d.%02d", abs((int)($subsec_today_ot_minite/60)), abs((int)($subsec_today_ot_minite%60)));	
	$subsec_total_today_ot_amount+=$today_ot_amount;	
	$subsec_total_payable_tk+=$payable_tk;
	$subsec_total_today_salary+=$today_salary;
	
	
	$sec_new_today_ot_hour=explode(".",$today_ot_hour);
	$sec_total_ot_minit=($sec_new_today_ot_hour[0]*60)+$sec_new_today_ot_hour[1];
	$sec_today_ot_minite+=$sec_total_ot_minit;
	$sec_total_today_ot_hour = sprintf("%d.%02d", abs((int)($sec_today_ot_minite/60)), abs((int)($sec_today_ot_minite%60)));	
	$sec_total_today_ot_amount+=$today_ot_amount;	
	$sec_total_payable_tk+=$payable_tk;
	$sec_total_today_salary+=$today_salary;
	
	$dep_new_today_ot_hour=explode(".",$today_ot_hour);
	$dep_total_ot_minit=($dep_new_today_ot_hour[0]*60)+$dep_new_today_ot_hour[1];
	$dep_today_ot_minite+=$dep_total_ot_minit;
	$dep_total_today_ot_hour = sprintf("%d.%02d", abs((int)($dep_today_ot_minite/60)), abs((int)($dep_today_ot_minite%60)));	
	$dep_total_today_ot_amount+=$today_ot_amount;	
	$dep_total_payable_tk+=$payable_tk;
	$dep_total_today_salary+=$today_salary;
	
	$divis_new_today_ot_hour=explode(".",$today_ot_hour);
	$divis_total_ot_minit=($divis_new_today_ot_hour[0]*60)+$divis_new_today_ot_hour[1];
	$divis_today_ot_minite+=$divis_total_ot_minit;
	$divis_total_today_ot_hour = sprintf("%d.%02d", abs((int)($divis_today_ot_minite/60)), abs((int)($divis_today_ot_minite%60)));	
	$divis_total_today_ot_amount+=$today_ot_amount;	
	$divis_total_payable_tk+=$payable_tk;
	$divis_total_today_salary+=$today_salary;
	
	$loc_new_today_ot_hour=explode(".",$today_ot_hour);
	$loc_total_ot_minit=($loc_new_today_ot_hour[0]*60)+$loc_new_today_ot_hour[1];
	$loc_today_ot_minite+=$loc_total_ot_minit;
	$loc_total_today_ot_hour = sprintf("%d.%02d", abs((int)($loc_today_ot_minite/60)), abs((int)($loc_today_ot_minite%60)));	
	$loc_total_today_ot_amount+=$today_ot_amount;	
	$loc_total_payable_tk+=$payable_tk;
	$loc_total_today_salary+=$today_salary;
	
	$r++;
	}//while loop end here 
	
		if($status_subsec==1)
		{
			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
				<td  colspan="7"><b>Subsection Total</b></td>
				<td align="center"><?php echo $subsec_total_today_salary; ?></td>
				<td align="center"><?php echo $subsec_total_today_ot_hour; ?></td>
				<td align="center">&nbsp;<?php echo $subsec_total_today_ot_amount; ?></td>
				<td align="center">&nbsp;<?php echo $subsec_total_payable_tk; ?></td>
				<td align="center">&nbsp;<?php  ?></td>				
			</tr><?
		}
		if($status_sec==1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="7">Section Total</td>
                <td align="center"><?php echo $sec_total_today_salary; ?></td>
                <td align="center"><?php echo $sec_total_today_ot_hour; ?></td>
                <td align="center">&nbsp;<?php echo $sec_total_today_ot_amount; ?></td>
                <td align="center">&nbsp;<?php echo $sec_total_payable_tk; ?></td>
                <td align="center">&nbsp;<?php  ?></td>				
             </tr><?
		}
		if($status_dept==1 )
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="7">Department Total</td>
                <td align="center"><?php echo $dep_total_today_salary;  ?></td>
                <td align="center"><?php echo $dep_total_today_ot_hour;  ?></td>
                <td align="center">&nbsp;<?php echo $dep_total_today_ot_amount; ?></td>
                <td align="center">&nbsp;<?php echo $dep_total_payable_tk; ?></td>
                <td align="center">&nbsp;<?php  ?></td>				
           </tr><?
		}
		if($status_divis==1 )
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="7">Division Total</td>
                <td align="center"><?php echo $divis_total_today_salary;  ?></td>
                <td align="center"><?php echo $divis_total_today_ot_hour;  ?></td>
                <td align="center">&nbsp;<?php echo $divis_total_today_ot_hour; ?></td>
                <td align="center">&nbsp;<?php echo $divis_total_payable_tk; ?></td>
                <td align="center">&nbsp;<?php  ?></td>					
           </tr><?
		}
		if($status_loc==1 )
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="7">Location Total</td>
                <td align="center"><?php echo $loc_total_today_salary;  ?></td>
                <td align="center"><?php echo $loc_total_today_ot_hour;  ?></td>
                <td align="center">&nbsp;<?php echo $loc_total_today_ot_amount; ?></td>
                <td align="center">&nbsp;<?php echo $loc_total_payable_tk; ?></td>
                <td align="center">&nbsp;<?php  ?></td>
           </tr><?
		}
	?>
    		<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
				<td colspan="7"><strong>Greand Total</strong></td>
                <td align="center"><?php echo $grang_total_today_salary;  ?></td>
                <td align="center"><?php echo $grang_total_today_ot_hour;  ?></td>
                <td align="center">&nbsp;<?php echo $grang_total_today_ot_amount; ?></td>
                <td align="center">&nbsp;<?php echo $grang_total_payable_tk; ?></td>
				<td align="center">&nbsp;<?php  ?></td>
            </tr>
</table>
</fieldset>	
</div>
 <? 
	//echo signeture_table(9,$rpt_company,"1100px"); 
	$html = ob_get_contents();
	ob_clean();	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
		@unlink($filename);
	}		
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	echo "$html"."####"."$name";
	exit();
 	}
 ?>


<?
/*
//month generated
if($type=="select_month_generate")
	{		
		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}*/

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}
	
		
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}



/*
ALTER TABLE `hrm_salary_dtls` ADD `opening_count` TINYINT( 2 ) NOT NULL DEFAULT '0' AFTER `pay_slip_id`

UPDATE  hrm_salary_dtls AS t1, hrm_salary_mst AS t2 
  SET
t1.opening_count=t1.pay_slip_id
  WHERE t1.salary_mst_id =t2.id and t2.salary_periods='2013-01-01' and t1.salary_head_id=25
*/

?>