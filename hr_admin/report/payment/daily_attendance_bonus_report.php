<? 
/*************************************
|	Developed by	: Md. Nuruzzamam
|	date			: 24.12.2013
**************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');
 
 $sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_name ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script src="../../../includes/ajax_submit.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
       <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
  <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
		<script>
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
            });
            $(document).ready(function(e){
                $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
                    //header: false, 
                    selectedText: "# of # selected",
                });
				$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
            });
            function generate_daily_attn_bonus_report()
            {
                //alert("su..");
                var error = false;
                div="report_container";
                data_panel="data_panel";
                
                if( $('#company_id').val()*1 == 0 ) 
                {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#company_id').focus();
                        $(this).html('Please Select Company.').addClass('messageboxerror').fadeTo(900,1);
                        $(this).fadeOut(5000);
                    });
                }
                else if( $('#txt_from_date').val()== '' ) 
                {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#txt_from_date').focus();
                        $(this).html('Please Select Date.').addClass('messageboxerror').fadeTo(900,1);
                        $(this).fadeOut(5000);
                    });
                }
                else if( $('#txt_to_date').val()== '' ) 
                {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#txt_to_date').focus();
                        $(this).html('Please Select Date.').addClass('messageboxerror').fadeTo(900,1);
                        $(this).fadeOut(5000);
                    });
                }
                else if( error == false )
                {
					$("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
					//$('#report_container').html('<img src="../../../resources/images/loading.gif" />');
					var data = '&category_id=' + $('#category_id').val()
						+ '&company_id=' + $('#company_id').val() 
						+ '&location_id=' + $('#location_id').val() 
						+ '&division_id=' + $('#division_id').val() 
						+ '&department_id=' + $('#department_id').val()
						+ '&section_id=' + $('#section_id').val() 
						+ '&subsection_id=' + $('#subsection_id').val() 
						+ '&designation_id=' + $('#designation_id').val()
						+'&txt_from_date=' + $('#txt_from_date').val()
						+ '&txt_to_date=' + $('#txt_to_date').val() 
						+ '&group_by_id=' + $('#group_by_id').val()
						+ '&order_by_id=' + $('#order_by_id').val()
						+ '&id_card='+$('#id_card').val()
						+ '&emp_code='+$('#emp_code').val();
					
					//alert(data);
					nocache = Math.random();
					http.open( 'GET', 'includes/generate_daily_attendance_bonus_report.php?action=daily_attendance_bonus_report' + data + '&nocache=' + nocache );
					http.onreadystatechange = response_daily_attendance_bonus;
					http.send(null);
                }
            }
            function response_daily_attendance_bonus() 
            {	
                if(http.readyState == 4) 
                {
					var data_split=(http.responseText).split('####');
					var link_data=data_split[1];			
					$('#data_panel').html( '<br><b>Convert To </b><a href="includes/tmp_report_file/' + data_split[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
					$('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );		
					$('#data_panel2').html( data_split[0] );
					hide_search_panel();
					$("#messagebox").fadeTo(200,0.1,function(){ 
					$(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
					});
                }
            }
            
            function new_window()
            {
                var w = window.open("Surprise", "#");
                var d = w.document.open();
               // d.write(document.getElementById('report_container').innerHTML);
				 d.write(document.getElementById('data_panel2').innerHTML);
                d.close();
            }
            
            function openmypage_employee_info()
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#category_id').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#emp_code').val(thee_id.value);
                }
            }	
            function openmypage_employee_info_id_card()
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#category_id').val()+'&company_id='+$('#company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }	
            
            //Numeric Value allow field script
            function numbersonly(myfield, e, dec)
            {
                var key;
                var keychar;
            
                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);
            
                // control keys
                if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
                return true;
                
                // numbers
                else if ((("0123456789.,-").indexOf(keychar) > -1))
                    return true;
                else
                    return false;
            } 
        </script>
		<style type="text/css"> 
            td {overflow:hidden; }   
            @media print {
            thead {display: table-header-group;}
            }
            @media print {
            tfoot {display: table-footer-group;}
            }
            @media print{
            html>body tbody.table_body {
            max-height:305px;
            display: block;
            overflow:auto;
            }
            }
            .verticalText 
            {               
            writing-mode: tb-rl;
            filter: flipv fliph;
            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            }
            .ui-multiselect.myClass
            {
            width:150px;
            }
            td {overflow:hidden; }
            @media print {thead {display: table-header-group;}}
            @media print {tfoot {display: table-footer-group;}}
        </style>
    </head>
    <body>
        <div style="width:100%;" align="center">
			<div align="center" style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>    
            <h3 align="left" style="top:1px;width:100%;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> - Daily Attendance Bonus Report</h3>
            <div id="content_search_panel"> 
                <form>
                    <fieldset>		
                        <table class="rpt_table"  width="1120px" cellspacing="0" align="center">
                            <thead>
                                <th width="140"><strong>Category</strong></th>
                                <th width="140"><strong>Company</strong></th>
                                <th width="140"><strong>Location</strong></th>
                                <th width="140"><strong>Division</strong></th>
                                <th width="140"><strong>Department</strong></th>
                                <th width="140"><strong>Section</strong></th>
                                <th width="140"><strong>SubSection</strong></th>
                                <th width="140"><strong>Designation</strong></th>
                            </thead>
                            <tbody>
                                <tr> 
                                    <td>
                                         <select name="category_id" id="category_id" class="combo_boxes" style="width:140px;">
                                            <option value="">All Category</option>
                                                <?
                                                foreach($employee_category as $key=>$val)
                                                {
                                                ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                                <?
                                                }
                                                ?> 
                                         </select>
                                    </td>
                                    <td>
                                        <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                                            <?php 
                                            if($company_cond=="")
                                            { 
                                            ?>
                                                <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                      </select>
                                    </td>
                                    <td>
                                        <select name="division_id" id="division_id" style="width:140px" multiple="multiple" >
                                            <?php foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                            </select>
                                    </td>        
                                    <td>
                                        <select name="department_id" id="department_id" style="width:140px" multiple="multiple">
                                            <?php 
                                            foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="section_id" id="section_id" style="width:140px" multiple="multiple" >
                                            <?php 
                                            foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="subsection_id" id="subsection_id" style="width:140px" multiple="multiple" >
                                            <?php foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="designation">
                                         <select name="designation_id" id="designation_id" style="width:140px;" multiple="multiple" >
                                            <?php 
                                            foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>  
                                </tr>
                            </tbody>
                        </table>
                        <div style="height:10px;"></div>
                        <table class="rpt_table"  width="800px" cellspacing="1" align="center"> 
                            <thead>                       
                                <th width="140" colspan="2"><strong>Date</strong></th>   
                                <th width="140"><strong>Group By</strong></th>
                                <th width="140"><strong>Order By</strong></th>
                                <th width="140"><strong>ID Card No</strong></th>
                                <th width="140"><strong>Employee Code</strong></th> 
                                <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px;" /></th>
                            </thead>
                            <tbody>
                                <tr class="general">
                                    <td align="center">
                                        <input type="text" name="txt_from_date" id="txt_from_date" value="" class="datepicker" style="width:70px;" readonly="readonly" />
                                    </td>
                                    <td> 
                                        <input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" style="width:70px;" readonly="readonly" />
                                    </td>  
                                    <td>
                                        <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:140px;" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $groupby_arr AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:140px;" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $orderby_arr AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:140px;" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>
                                    <td><input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:140px;" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" /></td> 
                                    <td><input type="button" name="search" id="search" value="Show" class="formbutton" style="width:100px;" onclick="generate_daily_attn_bonus_report()"/></td>                  
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
            <!--<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="report_container" align="left" class="demo_jui2" style="margin-top:10px;"></div>-->
		</div>
        <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel2" align="center" class="demo_jui" style="margin-top:10px;"></div>
    </body>
</html>
<?php
function convert_to_mysql_date_less( $date_field ) 
{
	if( $date_field != '' ) 
	{
		$dates = explode( "-", $date_field );
		return $dates[2]-1 . "-" . $dates[1] . "-" . $dates[0];
	}
	else return $date_field;
}
?>
