<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}



if( $action == "employee_wise_leave_report" ) {	
	
	//html to xls file name and output link
	//$temp_file_name_in_out=$_SESSION['temp_file_name_in_out'];
	//unlink('tmp_report_file/'.$temp_file_name_in_out);
	//$name=time();
	//$name="$name".".xls";
	//$_SESSION['temp_file_name_in_out']=$name;	
	//echo "<div style='width:250px;background-color:#F0F0B8;font-weight:bold; font-size:18px;'>Convert To <a href='includes/tmp_report_file/".$name."'><img src='includes/logo/excel-logo.png' name='logo_id' id='logo_id' /></a></div><br \>";
		
	$status = "and dtls.leave_status in ('CL','ML','SL','EL','SpL','LWP','EdL')";
	if($txt_from_date!='' && $txt_to_date!=''){
		$txt_from_date=convert_to_mysql_date($txt_from_date);
		$txt_to_date=convert_to_mysql_date($txt_to_date);
		$search_date = "dtls.leave_date >= '$txt_from_date' and dtls.leave_date <= '$txt_to_date' and";
		$active_deleted = "and status_active=1 and is_deleted=0";
	}
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in ($designation_id)";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
		
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and department_id in ($department_id)";			
		}
		
	if ($cbo_salary_sheet=='') { $emp_status=""; } 
	else if($cbo_salary_sheet==0){$emp_status="and status_active=1 ";} 
	else if($cbo_salary_sheet==1){$emp_status="and status_active=0 "; } 
	
	
	$emp_code = $_GET['txt_emp_code'];//search employee code
	if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and emp_code in $search_emp_code";		
		}
	else {$search_code='';}
	
	$sql_d = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where is_deleted=0 $search_code $cbo_company_id $location_id $department_id $section_id $subsection_id $designation_id $salary_based $id_card_no $category $emp_status order by company_id";	
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );	
	$html = "<table id=\"tblCustomer\" cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">";
	$sl=0;
	if( mysql_num_rows($result_d)==0)
	 {
		$html .= "No Data Found";
	 }
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{	
		
			//$sql = "SELECT *  FROM hrm_leave_transaction where  id in( select distinct leave_id from hrm_leave_transaction_details where leave_date >= '$txt_from_date' and leave_date <= '$txt_to_date')  and status_active=1 ".$status."and emp_code=".$row_dp["emp_code"]." order by 'from_date' asc";
			$sql = "SELECT dtls.*,mst.remarks FROM hrm_leave_transaction_details dtls,hrm_leave_transaction mst where $search_date mst.status_active=1 and dtls.status_active=1 and dtls.is_deleted=0 $status and mst.id=dtls.leave_id and dtls.emp_code=".$row_dp["emp_code"];
			//echo $sql;
		 	//exit();
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );						
			$emp_tot=mysql_num_rows($result);			
			if ($emp_tot>0)
				{
					$sl++;					
					//$designation_name=return_field_value("custom_designation","lib_designation","id=$row_dp[designation_id]");					
					//$department_name=return_field_value("department_name","lib_department","id=$row_dp[department_id]");
					//$company_name=return_field_value("company_name","lib_company","id=$row_dp[company_id]");
					
					$html .= "<tr id=\"rowHeader\" bgcolor=#F0F0B8>	
						<td>SL NO:$sl</td>	
						<td colspan=6>Employee Code:  <b>$row_dp[emp_code]</b>    Employee Name:    <b>$row_dp[name]</b>   Designation: <b>".$designation_chart[$row_dp['designation_id']]['custom_designation']."</b>  Joining Date: <b>".convert_to_mysql_date($row_dp['joining_date'])."</b></td>											
					</tr>
					<tr>
						<td><b>Company&Department</b></td>						
						<td><b>Leave Type</b></td>
						<td><b>From Date</b></td>
						<td><b>To Date</b></td>
						<td><b>Total Days</b></td>
						<td><b>Cumulative</b></td>
						<td><b>Remark</b></td>
					</tr>";							
			
			$grandTotal=0;
			$days=1;
			while( $row = mysql_fetch_array( $result ) ) 
			{	
				$grandTotal++;				
				
				$html .= "<tr>
							<td></td>						
						</tr>					
						<tr class=\"gradeA\">
							<td>".$company_details[$row_dp['company_id']]['company_name'].", ".$department_details[$row_dp['department_id']]['department_name']."</td>
							<td>$row[leave_status]</td>						
							<td><b>".convert_to_mysql_date($row[leave_date])."</b></td> 	
							<td><b>".convert_to_mysql_date($row[leave_date])."</b></td>
							<td>$days</td>
							<td>$grandTotal</td>
							<td>&nbsp;$row[remarks]</td>						
						</tr>";				
			}
			//bgcolor=#E0ECF8		
	$html .= "
					<tr> 
						<td colspan=4><b>Grand Total</b></td>
						<td colspan=3>$grandTotal</td>
					</tr>";
		}
		
	
	}
	
	$html .= " </table>";
	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename){			
			@unlink($filename);
		}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>