<?php
	date_default_timezone_set('Asia/Dhaka');
	session_start();
	
	include('../../../../includes/common.php');
	include('../../../../includes/array_function.php');
	
	$e_date = time();
	$user_only = $_SESSION["user_name"];
	
	extract( $_GET );

	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}

	//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
	
	//department_details
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$department_details[$row['id']] = mysql_real_escape_string( $row['department_name'] );		
	}

	//section_details
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}

	//subsection_details
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
		
	//designation_chart
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}

	function add_date($orgDate,$days)
	{
		$cd = strtotime($orgDate);
		$retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
		return $retDAY;
	}
	
	function check_attendance_status( $code, $emp_att_status, $fromdate, $todate, $ab_days, $cal_days, $absent_type)
	{
		if($absent_type==0)
		{
			$inc=0;
			for ($i=0; $i<$cal_days; $i++)
			{
				$c_date=add_date($todate,-$i);
				if ( $emp_att_status[$code][$c_date]=="A" ) 
				$inc++;
				else if ( $emp_att_status[$code][$c_date]=="W" || $emp_att_status[$code][$c_date]=="GH" || $emp_att_status[$code][$c_date]=="FH" || $emp_att_status[$code][$c_date]=="H" || $emp_att_status[$code][$c_date]=="CH" )
				$val=0;
				else
				$inc=0;
				//$c_date	
				if ($inc==$ab_days){ $tr=$c_date;  break; }	
				//cal_days
			}
			if ($inc==$ab_days) return $tr."__".$inc;
			else return 0;
		}
		else
		{
			$inc=0;
			$tr_date="";
			for ($i=0; $i<$cal_days; $i++)
			{
				$c_date=add_date($fromdate,$i);
				if ( $emp_att_status[$code][$c_date]=="A" ) 
				{
				$inc++;
				if ($inc==1){ $tr_date=$c_date;}
				}
				else if ( $emp_att_status[$code][$c_date]=="W" || $emp_att_status[$code][$c_date]=="GH" || $emp_att_status[$code][$c_date]=="FH" || $emp_att_status[$code][$c_date]=="H" || $emp_att_status[$code][$c_date]=="CH" )
				{
				$val=0;
				}
				
				//else
				//$inc=0;
				//$c_date	
				//if ($inc==$ab_days){ $tr=$tr_date;  break; }	
				//cal_days
			}
			if ($inc==$ab_days) return $tr_date."__".$inc;
			else return 0;
			//return $tr_date."__".$inc;
		}
	}

// absent_report
if($action=='absent_report')
{
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	
	$company = $cbo_company_id;
	if ( $cbo_emp_category=='' ) $category_id ="and e.category in (0,1,2,3,4) "; else $category_id ="and e.category='$cbo_emp_category'";
	if ( $cbo_company_id==0 ) $company_id=""; else $company_id="and a.company_id='$cbo_company_id'";
	if ( $location_id==0 ) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if  ($division_id==0 || $location_id=="NULL" ) $division_id=""; else $division_id="and a.division_id in ($division_id)";
    if ( $section_id==0 || $location_id=="NULL" ) $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ( $subsection_id==0 || $location_id=="NULL" ) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ( $department_id==0 || $location_id=="NULL" ) $department_id=""; else $department_id="and a.department_id in ($department_id)";	
	
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and e.salary_type_entitled='$cbo_salary_based'";

	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="e.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="e.company_id,e.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="e.company_id,e.location_id,e.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,e.subsection_id,";}
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(right(trim(e.id_card_no),$id_card_no_ord)as SIGNED)";}
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(e.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}
	else if($order_by_id==3){$orderby="order by ".$groupby." CAST(right(trim(e.id_card_no),$id_card_no_ord)as SIGNED)";}
		 
	//else if($order_by_id==3){$orderby="order by ".$groupby." right(e.id_card_no, 5)";}
	//else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(emp.id_card_no),$id_card_no_ord)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();	
	ob_start();
	
	$addrs ="";		
	if($company_details[$company]['plot_no'] !='') $addrs .= $company_details[$company]['plot_no'].", "; 
	if($company_details[$company]['level_no'] !='') $addrs .= $company_details[$company]['level_no'].", "; 
	if($company_details[$company]['block_no'] !='') $addrs .= $company_details[$company]['block_no'].", "; 
	if($company_details[$company]['road_no'] !='') $addrs .= $company_details[$company]['road_no'].", "; 
	if($company_details[$company]['city'] !='') $addrs .= $company_details[$company]['city']." "; 
	?>       
	<table cellpadding="0" cellspacing="1" border="1" width="1050" align="center" style="border:1px thin black;" class="rpt_table" rules="all" >
       <thead>
            <tr>
                <th colspan="11" align="center"><font size="+1"><? echo $company_details[$company]['company_name']; ?></font></th></tr>
            <tr><th colspan="11" align="center">Address : <? echo $addrs; ?></th></tr>        
            <tr>
                <th colspan="11" align="center"> <? if($absent_type==0) echo "Continuous"; else echo "Random"; ?> Absent <? echo $txt_days; ?> Days</th>
            </tr>
            <tr>
                <th width="40"><b>SL</b></th>
                <th width="80"><b>ID Card</b></th>
                <th width="80"><b>Emp Code</b></th>
                <th align="150"><b>Name</b></th>
                <th align="100"><b>Designation</b></th>
                <th align="90"><b>Department</b></th>
                <th align="100"><b>Section</b></th>
                <th align="100"><b>Subsection</b></th>
                <th align="90"><b>DOJ</b></th>
                <th align="90"><b>Absent Start</b></th>
                <th align="80"><b>Total Absent</b></th>
            </tr>
        </thead>
        <tbody>
		<?php
		$emp_att_status=array();
		$emp_code=array();
		$emp_id_card=array();
		$emp_name=array();
		$emp_join=array();
		$emp_loc=array();
		$emp_div=array();
		$emp_depart=array();
		$emp_sec=array();
		$emp_subsec=array();
		$emp_desig=array();
		
		/*
		$sql ="select CONCAT(e.first_name, ' ',e.middle_name, ' ',e.last_name) AS name,
		 	e.company_id,e.id_card_no,e.designation_id,e.section_id,e.subsection_id,e.department_id,e.category,
			e.joining_date,a.attnd_date,a.emp_code,a.status
			from hrm_employee e, hrm_attendance a  	
			where 
			e.status_active=1 and e.company_id=$company and a.emp_code=e.emp_code $department_id $section_id  $category
			and a.attnd_date between '$fromdate' and '$todate'
			and a.status in ('A','W','GH','FH','H') order by e.company_id,e.section_id,e.id_card_no";
		*/
		
		//echo $absent_type;die;
		
		 $sql="select CONCAT(e.first_name, ' ',e.middle_name, ' ',e.last_name) AS name,e.id_card_no,
		 	e.category,e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,e.subsection_id,e.designation_id,e.designation_level,
			e.joining_date,a.attnd_date,a.emp_code,a.status
			from hrm_employee e, hrm_attendance a  	
			where 
			e.status_active=1 
			$category_id 
			$company_id 
			$location_id 
			$division_id 
			$department_id 
			$section_id 
			$subsection_id 
			$salary_based and 
			a.emp_code=e.emp_code and 
			a.attnd_date between '$fromdate' and '$todate' and 
			a.status in ('A','W','GH','FH','H','CH' )  $orderby";
	 	//group by $dynamic_groupby $orderby group by $dynamic_groupby  and a.emp_code='0020028' and a.emp_code='41972'
		//$orderby 
		//echo $sql;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sl=0;	
		while($row=mysql_fetch_array($result))
		{
			$emp_att_status[$row[emp_code]][$row[attnd_date]]=$row[status];
			if (!in_array($row[emp_code],$emp_code))
			{
				$emp_code[]=$row[emp_code];
				$emp_id_card[$row[emp_code]]=$row[id_card_no];
				$emp_name[$row[emp_code]]=$row[name];
				$emp_join[$row[emp_code]]=$row[joining_date];
				$emp_loc[$row[emp_code]]=$row[location_id];
				$emp_div[$row[emp_code]]=$row[division_id];
				$emp_depart[$row[emp_code]]=$row[department_id];
				$emp_sec[$row[emp_code]]=$row[section_id];
				$emp_subsec[$row[emp_code]]=$row[subsection_id];
				$emp_desig[$row[emp_code]]=$row[designation_id];
			}
		}
		$kk=0;
		// print_r( $emp_code);die;
		foreach ( $emp_code as $code )
		{
			
			$ret=explode("__",check_attendance_status($code,$emp_att_status,$fromdate,$todate,$txt_days,$get_days,$absent_type));
			//echo $ret[0]."==".$ret[1];
			if ( $ret[0]!=0 )
			{
					$sl++;
					$kk++;
					//start header print---------------------------------//
					if($sl==1)
					{
						$location_arr[$emp_loc[$code]]=$emp_loc[$code];
						$division_arr[$emp_div[$code]]=$emp_div[$code];
						$department_arr[$emp_depart[$code]]=$emp_depart[$code];
						$section_arr[$emp_sec[$code]]=$emp_sec[$code];
						$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];

					}//end if condition of header print
					if($sl!=1)
					{
						$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
						if(in_array($emp_loc[$code],$location_arr) && $status_loc==1)
						{
							if(in_array($emp_div[$code],$division_arr) && $status_divis==1)
							{
								if(in_array($emp_depart[$code],$department_arr) && $status_dept==1)
								{
									if(in_array($emp_sec[$code],$section_arr) && $status_sec==1)
									{ 
										if(in_array($emp_subsec[$code],$subsection_arr) && $status_subsec==1)
										{}
										else if($status_subsec==1)
										{
										$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
										$new_subsec=1;
										}
									}
									else if($status_sec==1)
									{
										$section_arr[$emp_sec[$code]]=$emp_sec[$code];
										$subsection_arr=array();
										$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
										$new_sec=1;
										$new_subsec=1;
									}
								}
								else if($status_dept==1)
								{
									$department_arr[$emp_depart[$code]]=$emp_depart[$code];
									$section_arr=array();
									$subsection_arr=array();
									$section_arr[$emp_sec[$code]]=$emp_sec[$code];
									$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
								}
							}//division
							else if($status_divis==1)
							{
								$division_arr[$emp_div[$code]]=$emp_div[$code];
								$department_arr=array();
								$section_arr=array();
								$subsection_arr=array();
								$department_arr[$emp_depart[$code]]=$emp_depart[$code];
								$section_arr[$emp_sec[$code]]=$emp_sec[$code];
								$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
								$new_divis=1;
								$new_dept=1;
								$new_sec=1;
								$new_subsec=1;
							}//division else
						}//location
						else if($status_loc==1)
						{
							$location_arr[$emp_loc[$code]]=$emp_loc[$code];
							$division_arr=array();
							$department_arr=array();
							$section_arr=array();
							$subsection_arr=array();
							$division_arr[$emp_div[$code]]=$emp_div[$code];
							$department_arr[$emp_depart[$code]]=$emp_depart[$code];
							$section_arr[$emp_sec[$code]]=$emp_sec[$code];
							$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
							$new_loc=1;
							$new_divis=1;
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}//location else
					}
					//header print here 
					$c_part_1="";
					if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
					{
						if($status_loc==1)
						{					
							$c_part_1 .= "  Location :  ".$location_details[$emp_loc[$code]]."  ,";
						}				
						if($status_divis==1)
						{
							$c_part_1 .= "  Division :  ".$division_details[$emp_div[$code]]."  ,";
						}
						if($status_dept==1)
						{
							$c_part_1 .= "  Department :  ".$department_details[$emp_depart[$code]]."  ,";
						}
						if($status_sec==1)
						{
							$c_part_1 .= "  Section :  ".$section_details[$emp_sec[$code]]."  ,";
						}
						if($status_subsec==1)
						{
							$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp_subsec[$code]]."  ,";
						}
						if($c_part_1!='')
						{
							//$i=0;
							$sl=1;
							?>
							<tr bgcolor="#CCCCCC">
								<td colspan="11"><b><?php echo substr($c_part_1,0,-1); ?></b></td>
							</tr>
							<?php      
						}
					}
					?>
             <tr>
                <td>&nbsp;<?php echo $sl; ?></td>
                <td>&nbsp;<?php echo $emp_id_card[$code]; ?></td>
                <td>&nbsp;<?php echo $code; ?></td>
                <td>&nbsp;<?php echo $emp_name[$code]; ?></td>
                <td>&nbsp;<?php echo $designation_chart[$emp_desig[$code]]; ?></td>
                <td>&nbsp;<?php echo $department_details[$emp_depart[$code]]; ?></td>
                <td>&nbsp;<?php echo $section_details[$emp_sec[$code]]; ?></td>
                <td>&nbsp;<?php echo $subsection_details[$emp_subsec[$code]]; ?></td>
                <td>&nbsp;<?php echo convert_to_mysql_date($emp_join[$code]); ?></td>               
                <td>&nbsp;<?php echo convert_to_mysql_date($ret[0]); ?></td>
                <td align="center">&nbsp;<?php echo $ret[1]; ?></td>  			            
            </tr>
            <?
			}
		}
		?>
    	</tbody>
    </table>
 	<?php
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}

function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	
	return $m_data ;
}
?>