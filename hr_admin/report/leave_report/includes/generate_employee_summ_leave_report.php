<?php
/*##########################################

	comleted by : Ekram
	Date:         15/09/2014
##########################################*/


date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
	$sql = "SELECT * FROM variable_settings_report WHERE is_deleted = 0 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$variable_settings_report = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$variable_settings_report[$row['company_id']][$row['module_id']] = mysql_real_escape_string($row['report_list']);
	}
 
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$designation_chart = array();
	while( $row = mysql_fetch_array( $result ) ) {		
			$designation_chart[$row['id']] =  $row['custom_designation'];
	}


if( $action == "employee_summ_leave_report" ) 
{	
	
	//die;

	if($txt_from_date!='' && $txt_to_date!='')
	{
		$txt_from_date=convert_to_mysql_date($txt_from_date);
		$txt_to_date=convert_to_mysql_date($txt_to_date);
		$search_date = "leave_date >= '$txt_from_date' and leave_date <= '$txt_to_date'";
	}
	$status="and leave_status in ('CL','ML','SL','EL','SpL','LWP','EdL')";
	
	
	
	//$leave_type
	if ($leave_type_status==0 ||$leave_type_status=='' || $leave_type_status=="null") { $leave_count_arr="1,2,3,4,5,6,7";} 
	else  { $leave_count_arr=$leave_type_status;}
	
	//print_r($leave_count_arr);die;	
		
	if ($cbo_emp_category=='') $category ="and category in($employee_category_index)"; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $company_id=""; $company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id in($location_id)";
	if ($division_id==0) $division_id=""; else $division_id="and division_id in($division_id)";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id='$department_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id in($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in($designation_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	if ($txt_emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$txt_emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	if ($cbo_salary_sheet=='') $emp_status=""; 
	else if($cbo_salary_sheet==0) $emp_status="and status_active=1 "; 
	else if($cbo_salary_sheet==1) $emp_status="and status_active=0 ";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
	
	//new 
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(id_card_no), 5)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
			
 	$sql_leave = "select emp_code,leave_status, count(id) as days from hrm_leave_transaction_details where $search_date and status_active=1 group by emp_code,leave_status";
	$result = mysql_query( $sql_leave ) or die( $sql_leave . "<br />" . mysql_error() );
	$leave_status_arr=array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$leave_status_arr[$row['emp_code']][$row['leave_status']]=$row['days'];
	}	

	
	$search_dates=explode("-",$txt_from_date);
	//print_r($search_dates);die;
	$sql_leave_balance = "select emp_code,year_start,leave_type,balance from hrm_leave_balance where year_start='$search_dates[0]' and status_active=1 group by emp_code,leave_type";
	$result = mysql_query( $sql_leave_balance ) or die( $sql_leave_balance . "<br />" . mysql_error() );
	$leave_balance_arr=array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$leave_balance_arr[$row['emp_code']][$row['leave_type']]=$row['balance'];
	}
	
	
		$leave_count_array=explode(",", $leave_count_arr);
		$table_width=(count($leave_count_array)*40)+605;
		//echo $table_width;die;
	
ob_start();	

	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	?>
	<table cellpadding="0" cellspacing="0" border="1" width="<? echo $table_width ?>" align="center"  class="rpt_table"  rules="all"> 
        <thead>
            <tr style="text-align:center; font-weight:bold" height="60">
                <th colspan="<? echo (count($leave_count_array)*2)+7; ?>" align="center" bordercolor="#FFFFFF">
                <font size="+2"><? echo  $company_details[$cbo_company_id];?></font><br />
                   <font size="-2"><? echo $location_details[1]; ?></font><br />
                 </th>
            </tr>
            <tr>
                <th rowspan=2 width=25><b>SL</b></th>						
                <th rowspan=2 width=60><b>Emp Code</b></th>
                <th rowspan=2 width=70><b>Emp ID</b></th>
                <th rowspan=2 width=150><b>Name</b></th>						
                <th rowspan=2 width=130><b>Designation</b></th>
                <th rowspan=2 width=100><b>Department</b></th>
                <th rowspan=2 width=70><b>DOJ</b></th>
                <? $leave_count_array=explode(",", $leave_count_arr);?>
                <th colspan="<? echo count($leave_count_array); ?>" align="center"><b>Leave Availed</b></th>
				<th colspan="<? echo count($leave_count_array); ?>" align="center"><b>Leave Balance</b></th> 
            </tr>
            <tr>
				<?
                $leave_avail_count=explode(",", $leave_count_arr);
                foreach ($leave_avail_count as $bid=>$bname)
                {
                    //$bank_id=$bid;
                    ?>				
                    <th width="40"><? echo $leave_type[$bname];?></th>
                    <? 
                }
                
                $leave_balance_count=explode(",", $leave_count_arr);
                foreach ($leave_balance_count as $bid=>$bnames)
                {
                    //$bank_id=$bid;
                    ?>				
                    <th width="40"><? echo $leave_type[$bnames];?></th>
                    <? 
                }
                ?>   
            </tr>
        </thead>
	
	<?
	
				
	$total_leave_availed_amount=array(); $subsection_total_leave_availed_amount=array(); $section_total_leave_availed_amount=array(); $department_total_leave_availed_amount=array();$division_total_leave_availed_amount=array(); $location_total_leave_availed_amount=array();
	
	$total_leave_taken_amount=array(); $subsection_total_leave_taken_amount=array(); $section_total_leave_taken_amount=array(); $department_total_leave_taken_amount=array(); $division_total_leave_taken_amount=array(); $location_total_leave_taken_amount=array();
	
	$arr=array();
	$total_leav=array();
	$i=1;
	$sl=0;
	$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where is_deleted=0 $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $salary_based $id_card_no $emp_code $category $emp_status $orderby";
	//echo $sql;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
	while( $row_emp = mysql_fetch_array( $result ) ) 
	{
		$sl++;
		//start header print---------------------------------//
		if($sl==1)
		{
			$location_arr[$row_emp[location_id]]=$row_emp[location_id];
			$division_arr[$row_emp[division_id]]=$row_emp[division_id];
			$department_arr[$row_emp[department_id]]=$row_emp[department_id];
			$section_arr[$row_emp[section_id]]=$row_emp[section_id];
			$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
		}//end if condition of header print
		
		if($sl!=1)
		{
			$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
			if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
			{
					if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
					{
							if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
							{
								if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
								{ 
									if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
									{}
									else if($status_subsec==1)
									{
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_subsec=1;
									}
								}
								else if($status_sec==1)
								{
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr=array();
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_sec=1;
									$new_subsec=1;
								}
							}
							else if($status_dept==1)
							{
								$department_arr[$row_emp[department_id]]=$row_emp[department_id];
								$section_arr=array();
								$subsection_arr=array();
								$section_arr[$row_emp[section_id]]=$row_emp[section_id];
								$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
								$new_dept=1;
								$new_sec=1;
								$new_subsec=1;
							}
					}//division
					else if($status_divis==1)
					{
							$division_arr[$row_emp[division_id]]=$row_emp[division_id];
							$department_arr=array();
							$section_arr=array();
							$subsection_arr=array();
							$department_arr[$row_emp[department_id]]=$row_emp[department_id];
							$section_arr[$row_emp[section_id]]=$row_emp[section_id];
							$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
							$new_divis=1;
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
					}//division else
			}//location
			else if($status_loc==1)
			{
				$location_arr[$row_emp[location_id]]=$row_emp[location_id];
				$division_arr=array();
				$department_arr=array();
				$section_arr=array();
				$subsection_arr=array();
				$division_arr[$row_emp[division_id]]=$row_emp[division_id];
				$department_arr[$row_emp[department_id]]=$row_emp[department_id];
				$section_arr[$row_emp[section_id]]=$row_emp[section_id];
				$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//location else
		}
		
			if($new_subsec==1 && $status_subsec==1 && $sl!=1)
			{
				?>
				<tr style="font-weight:bold;">
					<td width='' colspan="7"><b> Subsection Total</b></td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $subsection_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $subsection_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$subsection_total_leave_availed_amount=array(); $subsection_total_leave_taken_amount=array();	
			}
			
			if($new_sec==1 && $status_sec==1 && $sl!=1)
			{
				?>
				<tr style="font-weight:bold;">
					<td width='' colspan="7"><b>Section Total</b></td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $section_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $section_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$section_total_leave_availed_amount=array(); $section_total_leave_taken_amount=array();
			}
			
			if($new_dept==1 && $status_dept==1 && $sl!=1)
			{
				?>
				<tr style="font-weight:bold;">
					<td  colspan="7">Department Total</td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $department_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $department_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$department_total_leave_availed_amount=array();	$department_total_leave_taken_amount=array();
			}
			
			if($new_divis==1 && $status_divis==1 && $sl!=1)
			{
				?>
				<tr style="font-weight:bold;">
					<td  colspan="7" >Division Total</td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $division_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $division_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$division_total_leave_availed_amount=array(); $division_total_leave_taken_amount=array();		
			}
			
			if($new_loc==1 && $status_loc==1 && $sl!=1)
			{
				?>
				<tr style="font-weight:bold;">
					<td colspan="7">Location Total</td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $location_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $location_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$location_total_leave_availed_amount=array(); $location_total_leave_taken_amount=array();			
			}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			/*if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
			}	*/			
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
			}
	
			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr><td colspan="<? echo (count($leave_count_array)*2)+7; ?>"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		}
		
		
		if ($i%2==0) $bgcolor="#EFEFEF";else $bgcolor="#FFFFFF"; 
		
		?>
    <tbody>	    	
    	<tr height="98" bgcolor="<? echo $bgcolor; ?>" >
        	<td align="center" valign="middle"><? echo $i; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[emp_code]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[id_card_no]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[name]; ?></td>
            <td align="center" valign="middle"><? echo $designation_chart[$row_emp[designation_id]]; ?></td>
            <td align="center" valign="middle"><? echo $department_details[$row_emp[department_id]]; ?></td>
            <td align="center" valign="middle"><? echo convert_to_mysql_date($row_emp[joining_date]); ?></td>
			<?
			$leave_avail_count=explode(",", $leave_count_arr); 
			//print_r($leave_avail_count);die;
			foreach ($leave_avail_count as $bid=>$bname)
			{
				//$bank_id=$bid;
				?>				
				<td width="40" align="center" valign="middle"><? echo $leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				if($leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]]>0)
				{$arr[$leave_type[$bname]]+=1;}
				
				$total_leave[$leave_type[$bname]]+=$leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				$total_leave_availed_amount[$leave_type[$bname]]+=$leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				
				$subsection_total_leave_availed_amount[$leave_type[$bname]]+=$leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				$section_total_leave_availed_amount[$leave_type[$bname]]+=$leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				$department_total_leave_availed_amount[$leave_type[$bname]]+=$leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				$division_total_leave_availed_amount[$leave_type[$bname]]+=$leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				$location_total_leave_availed_amount[$leave_type[$bname]]+=$leave_status_arr[$row_emp['emp_code']][$leave_type[$bname]];
				?>
				</td>
				<? 
			}
			$leave_balance_count=explode(",", $leave_count_arr);
			//print_r($leave_balance_count);die;
			foreach ($leave_balance_count as $bid=>$bnames)
			{
				//$bank_id=$bid;
				?>				
				<td width="40" align="center" valign="middle"><?  echo $leave_balance_arr[$row_emp['emp_code']][$leave_type[$bnames]]; ?></td>
				<? 
				$total_leave_taken_amount[$bnames]+=$leave_balance_arr[$row_emp['emp_code']][$leave_type[$bnames]]; 
				
				$subsection_total_leave_taken_amount[$bnames]+=$leave_balance_arr[$row_emp['emp_code']][$leave_type[$bnames]]; 
				$section_total_leave_taken_amount[$bnames]+=$leave_balance_arr[$row_emp['emp_code']][$leave_type[$bnames]]; 
				$department_total_leave_taken_amount[$bnames]+=$leave_balance_arr[$row_emp['emp_code']][$leave_type[$bnames]]; 
				$division_total_leave_taken_amount[$bnames]+=$leave_balance_arr[$row_emp['emp_code']][$leave_type[$bnames]]; 
				$location_total_leave_taken_amount[$bnames]+=$leave_balance_arr[$row_emp['emp_code']][$leave_type[$bnames]]; 
			}
			?>
        </tr>			
		<?					
		
		$i++;
		
	}//while loop end here 
	//print_r($arr);die;
	
			if($status_subsec==1)
			{
				?>
				<tr style="font-weight:bold;">
					<td width='' colspan="7"><b> Subsection Total</b></td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $subsection_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $subsection_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$subsection_total_leave_availed_amount=array();	$subsection_total_leave_taken_amount=array();
			}
			
			if($status_sec==1)
			{
				?>
				<tr style="font-weight:bold;">
					<td width='' colspan="7"><b>Section Total</b></td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $section_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $section_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$section_total_leave_availed_amount=array(); $section_total_leave_taken_amount=array();
			}
			
			if($status_dept==1 )
			{
				?>
				<tr style="font-weight:bold;">
					<td  colspan="7">Department Total</td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $department_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $department_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$department_total_leave_availed_amount=array();	$department_total_leave_taken_amount=array();
			}
			
			if($status_divis==1 )
			{
				?>
				<tr style="font-weight:bold;">
					<td  colspan="7" >Division Total</td>
					 <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $division_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $division_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$division_total_leave_availed_amount=array(); $division_total_leave_taken_amount=array();	
			}
			
			if($status_loc==1 )
			{
			?>
                <tr style="font-weight:bold;">
                    <td colspan="7">Location Total</td>
                    <? 
                    foreach ($leave_avail_count as $bid=>$bname)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $location_total_leave_availed_amount[$leave_type[$bname]]; 
						$total_leav[$bname];
						?>
						</td>
						<?
                    }
                    foreach ($leave_balance_count as $bid=>$bnames)
                    { 
						?>
						<td width="40" align="center" valign="middle"><?  echo $location_total_leave_taken_amount[$bnames]; ?></td>
						<?
                    }
                    ?>
                </tr>
				<?
				$location_total_leave_availed_amount=array(); $location_total_leave_taken_amount=array();
			}	
	?>
    
	</tbody>
    
    	<tr bgcolor="#CCCCCC">
            <td colspan="7">Grand Total</td>
            <? 
            foreach ($leave_avail_count as $bid=>$bname)
            { 
				?>
				<td width="40" align="center" valign="middle"><?  echo $total_leave_availed_amount[$leave_type[$bname]]; 
				$total_leav[$bname];
				?>
				</td>
				<?
            }
            foreach ($leave_balance_count as $bid=>$bnames)
            { 
				?>
				<td width="40" align="center" valign="middle"><?  echo $total_leave_taken_amount[$bnames]; ?></td>
				<?
            }
            ?>
        </tr>
		<?
		$total_leave_taken_amount=array(); $total_leave_availed_amount=array();
		?>
        
        
       <!-- <tfoot>
        <tr>
        	<td colspan="<?// echo (count($leave_count_array)*2)+7; ?> ">
				<? //echo signeture_table_fashion(8,$cbo_company_id,$table_width); ?>
             </td>
        </tr>
    </tfoot>--> 
    
    </table>
      <div style="width:100%; height:10px;"></div>
    <table cellpadding="0" cellspacing="0" border="1" width="<? echo ((count($leave_count_array)+1)*40)+100; ?>" align="center"  class="rpt_table"  rules="all"> 
    	<thead>
        	<tr>
            	<th colspan="<? echo count($leave_count_array)+1; ?>">Summary</th>
            </tr>
            <tr>
            <td align="left" width="100">Leave Type</td>
            <?
            $leave_avail_count=explode(",", $leave_count_arr);
           foreach ($leave_avail_count as $bid=>$bname)
					{
						//$bank_id=$bid;
					?>				
						<td width="40" align="center" valign="middle"><? echo $leave_type[$bname];?></td>
				<? } ?>
            
            <tr>
            </thead>
            <tr>
            	<td>Total Leave</td>
                <? foreach ($leave_avail_count as $bid=>$bname){ ?>
				<td width="40" align="center" valign="middle"><?  echo $total_leave[$leave_type[$bname]]; ?></td><?
				}?>
            </tr>
            <tr>
            	<td>Total Person</td>
                 <?
           foreach ($leave_avail_count as $bid=>$bname)
					{
						//$bank_id=$bid;
					?>				
						<td width="40" align="center" valign="middle"><? echo $arr[$leave_type[$bname]];?></td>
				<? } ?>
            
            </tr>
    </table>
    <?
		
	
	$html = ob_get_contents();
	ob_clean();	
		
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename){			
		@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

function return_field_value($fdata,$tdata,$cdata){
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	
	return $m_data ;
}




//new for fashion
function signeture_table_fashion($report_id,$company,$width)
{
	if ($width=="") $width="100%"; 
	$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company");
	$signeture_list = explode('#',$signeture_allow_or_no);
	if($signeture_allow_or_no!= "")	
	{
	 echo '<table width="'.$width.'">
		<tr height="90" style="font-size:18px; font-family:Arial; font-weight:bold">
		 ';
		foreach($signeture_list as $key=>$value)
		{
			//echo '<td align="center" valign="bottom" style="text-decoration:overline">'.$value.'</td> ';
			echo '<td align="center" valign="bottom" style="border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;border-bottom-style:hidden;">'.$value.'</td> ';
		}
		echo '</tr></table>';
	}
	else
	{
		echo '<table width="'.$width.'">
		<tr height="90" style="font-size:18px; font-family:Arial; font-weight:bold">
		 ';
		 
		echo '</tr></table>';
	}
}
?>