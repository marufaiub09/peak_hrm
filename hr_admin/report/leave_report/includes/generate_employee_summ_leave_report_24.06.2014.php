<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


if( $action == "employee_summ_leave_report" ) 
{	
	//html to xls file name and output link
	//$temp_file_name_in_out=$_SESSION['temp_file_name_in_out'];
	//unlink('tmp_report_file/'.$temp_file_name_in_out);
	//$name=time();
	//$name="$name".".xls";
	//$_SESSION['temp_file_name_in_out']=$name;	
	//echo "<div style='width:250px;background-color:#F0F0B8;font-weight:bold; font-size:18px;'>Convert To <a href='includes/tmp_report_file/".$name."'><img src='includes/logo/excel-logo.png' name='logo_id' id='logo_id' /></a></div><br \>";
		
	if($txt_from_date!='' && $txt_to_date!=''){
		$txt_from_date=convert_to_mysql_date($txt_from_date);
		$txt_to_date=convert_to_mysql_date($txt_to_date);
		$search_date = "leave_date >= '$txt_from_date' and leave_date <= '$txt_to_date' and";
	}
	$status="and leave_status in ('CL','ML','SL','EL','SpL','LWP','EdL')";		
	//if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and job.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id='$department_id'";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	//if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no like '".$id_card."'";
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
		
	if ($cbo_company_id==0) 
		{
			$company_id="";			
		}
	else
		{
			$company_id="where id='$cbo_company_id'";			
		}
		
	if ($cbo_salary_sheet=='') { $emp_status=""; } 
	else if($cbo_salary_sheet==0){$emp_status="and status_active=1 ";} 
	else if($cbo_salary_sheet==1){$emp_status="and status_active=0 "; } 
			
	$sql_company = "SELECT id, company_name  FROM lib_company $company_id order by id";
	$result_sql_company = mysql_query( $sql_company ) or die( $sql_company . "<br />" . mysql_error() );
		
	$html = "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\" style=\"table-layout:950\">";
	while( $row_company = mysql_fetch_array( $result_sql_company ) ) 
	{
		$emp_code = $_GET['txt_emp_code'];//searching employee code
		if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and emp_code in $search_emp_code";		
		}
		else {$search_code='';}
		
		$sql_d = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where is_deleted=0 $search_code and company_id=$row_company[id] $location_id $division_id $department_id $section_id $subsection_id $designation_id $salary_based $id_card_no $category $emp_status order by designation_id";	
		//echo $sql_d; die;
		$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );	
		$r=1;
		
		$check=0;$tot_emp_cl=0;$tot_emp_ml=0;$tot_emp_sl=0;$tot_emp_el=0;$tot_emp_spl=0;$tot_emp_lwp=0;$tot_emp_edl=0;
		
		while( $row_dp = mysql_fetch_array( $result_d ) ) 
			{			
					//$sql = "select distinct emp_code from hrm_leave_transaction_details where leave_date BETWEEN '$txt_from_date' and '$txt_to_date' and status_active=1 ".$status."and emp_code=".$row_dp["emp_code"]."  order by id asc";
					$sql_leave = "select count(leave_status) as total_leave,emp_code, leave_id, leave_status from hrm_leave_transaction_details where $search_date status_active=1 $status and emp_code=".$row_dp["emp_code"]." group by leave_status";
					
					//$sql_leave = "select count(CASE WHEN leave_status ='CL' THEN emp_code END) AS 'CL', count(CASE WHEN leave_status ='SL' THEN emp_code END) AS 'SL',count(CASE WHEN leave_status ='EL' THEN emp_code END) AS 'EL',count(CASE WHEN leave_status ='ML' THEN emp_code END) AS 'ML',count(CASE WHEN leave_status ='SpL' THEN emp_code END) AS 'SpL',count(CASE WHEN leave_status ='LWP' THEN emp_code END) AS 'LWP',count(CASE WHEN leave_status ='EdL' THEN emp_code END) AS 'EdL', emp_code from hrm_leave_transaction_details where leave_date BETWEEN '$txt_from_date' and '$txt_to_date' and status_active=1 and emp_code='$row_dp[emp_code]'";
					//exit();
					
					$result_leave = mysql_query( $sql_leave ) or die( $sql_leave . "<br />" . mysql_error() );
					$nurows_leave=mysql_num_rows($result_leave);
					if($nurows_leave>0)
					{													
						
						if($check==0){						
							$department_name=return_field_value("department_name","lib_department","id=$row_dp[department_id]");									
							$html .= "<tr bgcolor=\"#F0F0B8\">		
										<td colspan=21>Company Name: <b>$row_company[company_name]</b></td>											
									</tr>
									<tr>
											<td rowspan=2 width=25><b>SL</b></td>						
											<td rowspan=2 width=60><b>Emp Code</b></td>
											<td rowspan=2 width=70><b>Emp ID</b></td>
											<td rowspan=2 width=250><b>Name</b></td>						
											<td rowspan=2 width=130><b>Designation</b></td>
											<td rowspan=2 width=100><b>Department</b></td>
											<td rowspan=2 width=70><b>DOJ</b></td>					
											<td colspan=7 align=\"center\" width=200><b>Leave Availed</b></td>
											<td colspan=7 align=\"center\" width=200><b>Leave Balance</b></td>                       
										</tr>
										<tr>                      
											<td width=50><b>CL</b></td>
											<td width=50><b>SL</b></td>
											<td width=50><b>EL</b></td>
											<td width=50><b>SpL</b></td>
											<td width=50><b>LWP</b></td>
											<td width=50><b>ML</b></td>	
											<td width=50><b>EdL</b></td>	
											<td width=50><b>CL</b></td>
											<td width=50><b>SL</b></td>
											<td width=50><b>EL</b></td>
											<td width=50><b>SpL</b></td>
											<td width=50><b>LWP</b></td>
											<td width=50><b>ML</b></td>	
											<td width=50><b>EdL</b></td>
										</tr>";	
									
									$check=1;
							}				
						
						
						$cl_total=0;$sl_total=0;$el_total=0;$spl_total=0;$lwp_total=0;$ml_total=0;$edl_total=0;	
						
						while( $row = mysql_fetch_array( $result_leave ) ) 
						{				
							//total leave calculation here//	
							if($row["leave_status"]=='CL')
							{	
								$cl_total=$row["total_leave"];
								if($row["total_leave"]>0) $tot_emp_cl+=1;
							}				
							
							if($row["leave_status"]=='SL'){	
							$sl_total=$row["total_leave"];
							if($row["total_leave"]>0) $tot_emp_sl+=1;
							
							}				
							
							if($row["leave_status"]=='EL'){	
							$el_total=$row["total_leave"];
							if($row["total_leave"]>0) $tot_emp_el+=1;
							}				
							
							if($row["leave_status"]=='SpL'){ 
							$spl_total=$row["total_leave"];
							if($row["total_leave"]>0) $tot_emp_spl+=1;
							}				
							
							if($row["leave_status"]=='LWP'){ 
							$lwp_total=$row["total_leave"];
							if($row["total_leave"]>0) $tot_emp_lwp+=1;
							
							}				
							
							if($row["leave_status"]=='ML'){ 
							$ml_total=$row["total_leave"];
							if($row["total_leave"]>0) $tot_emp_ml+=1;
							}	
							
							if($row["leave_status"]=='EdL'){
							$edl_total=$row["total_leave"];
							if($row["total_leave"]>0) $tot_emp_edl+=1;
								}
							
							$cl_total_sum +=$cl_total;
							$sl_total_sum += $sl_total;
							$el_total_sum +=$el_total;
							$spl_total_sum +=$spl_total;
							$lwp_total_sum += $lwp_total;
							$ml_total_sum +=$ml_total;
							$edl_total_sum +=$edl_total;
							
								
							
							//ekr
							//leave balace calculation
							//$leave_balance_sql="select balance, leave_type from hrm_leave_balance bal where emp_code=$row[emp_code] and leave_year in ('select  .id=$row_dp[location_id] and leave_year=$leave_year ";
							
							$leave_year=return_field_value("id","lib_policy_year","type=0 and is_locked=0 limit 1");
							$cl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'CL' and status_active=1");
							if($cl_balance=='')$cl_balance='0';
							$cl_balance=$cl_balance-$cl_total;
							$sl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'SL' and status_active=1");
							if($sl_balance=='')$sl_balance='0';
							$sl_balance=$sl_balance-$sl_total;
							$el_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'EL' and status_active=1");
							if($el_balance=='')$el_balance='0';
							$el_balance=$el_balance-$el_balance;
							$spl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'SpL' and status_active=1");
							if($spl_balance=='')$spl_balance='0';
							$spl_balance=$spl_balance-$spl_total;
							$lwp_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'LWP' and status_active=1");
							if($lwp_balance=='')$lwp_balance='0';
							$lwp_balance=$lwp_balance-$lwp_total;
							//ML incomplete								
							$ml_limit=return_field_value("days_required","hrm_leave_transaction tr, hrm_maternity_leave mat","tr.emp_code=$row[emp_code] and tr.emp_code=mat.emp_code and tr.leave_type = 'ML' and tr.status_active=1 and tr.is_deleted=0");	
							if($ml_balance=='')$ml_balance='0'; else $ml_balance=$ml_limit-$ml_total;
							//Education Leave
							$edl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'EdL' and status_active=1");	
							if($edl_balance=='')$edl_balance='0';
							$edl_balance=$edl_balance-$edl_total;
							}
							
							$cl_balance_sum += $cl_balance;
							$sl_balance_sum += $sl_balance;
							$el_balance_sum +=$el_balance;
							$spl_balance_sum +=$spl_balance;
							$lwp_balance_sum += $lwp_balance;
							$ml_balance_sum +=$ml_balance;
							$edl_balance_sum +=$edl_balance;
											
						$designation_name=return_field_value("custom_designation","lib_designation","id=$row_dp[designation_id]");
						$department_name=return_field_value("department_name","lib_department","id=$row_dp[department_id]");
							
				$html .= "											
						<tr class=\"gradeA\">
							<td>$r</td>
							<td>$row_dp[emp_code]</td>
							<td>$row_dp[id_card_no]</td>
							<td>$row_dp[name]</td>
							<td>$designation_name</td>
							<td>$department_name</td>
							<td>".convert_to_mysql_date($row_dp[joining_date])."</td>						
						
								<td>$cl_total</td>
								<td>$sl_total</td>
								<td>$el_total</td>
								<td>$spl_total</td>
								<td>$lwp_total</td>
								<td>$ml_total</td>
								<td>$edl_total</td>	
						
								<td>$cl_balance</td>
								<td>$sl_balance</td>
								<td>$el_balance</td>
								<td>$spl_balance</td>
								<td>$lwp_balance</td>
								<td>$ml_balance</td>
								<td>$edl_balance</td>
								
						</tr>";	
						$r++;	
					
						
				}
			}
			
			
					$html .= "											
						<tr bgcolor='#CCCCCC' class=\"gradeA\">
							<td colspan='7' align='center'><b>Total</b></td>
										
								<td>$cl_total_sum</td>
								<td>$sl_total_sum</td>
								<td>$el_total_sum</td>
								<td>$spl_total_sum</td>
								<td>$lwp_total_sum</td>
								<td>$ml_total_sum</td>
								<td>$edl_total_sum</td>	
						
								<td>$cl_balance_sum</td>
								<td>$sl_balance_sum</td>
								<td>$el_balance_sum</td>
								<td>$spl_balance_sum</td>
								<td>$lwp_balance_sum</td>
								<td>$ml_balance_sum</td>
								<td>$edl_balance_sum</td>
						</tr>";	
					
		}
		
		
		
		$html .= " </table>";
	
		$html .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"1\"  width=\"400\">
							<thead>
							<tr bgcolor=\"#F0F0B8\">
								<th colspan=\"3\">Summary</th>
							</tr>
							<tr class=\"gradeA\">
									<th width=\"80\">Leave Type</th>
									<th width=\"80\">Total Leave</th>
									<th width=\"80\">Total Person</th>
								</tr>
							</thead>
									<tr align=\"center\">
											<td><b>CL</b></td>
											<td><b>$cl_total_sum</b></td>
											<td><b>$tot_emp_cl</b></td>
									</tr>
									<tr align=\"center\">
											<td><b>SL</b></td>
											<td><b>$sl_total_sum</b></td>
											<td><b>$tot_emp_sl</b></td>
									</tr>
									<tr align=\"center\">
											<td><b>EL</b></td>
											<td><b>$el_total_sum</b></td>
											<td><b>$tot_emp_el</b></td>
									</tr>
									<tr align=\"center\">
											<td><b>SpL</b></td>
											<td><b>$spl_total_sum</b></td>
											<td><b>$tot_emp_spl</b></td>
									</tr>
									
									<tr align=\"center\">
											<td><b>LWP</b></td>
											<td><b>$lwp_total_sum</b></td>
											<td><b>$tot_emp_lwp</b></td>
									</tr>
									<tr align=\"center\">
											<td><b>ML</b></td>
											<td><b>$ml_total_sum</b></td>
											<td><b>$tot_emp_ml</b></td>
									</tr>
									<tr align=\"center\">
											<td><b>EdL</b></td>
											<td><b>$edl_total_sum</b></td>
											<td><b>$tot_emp_edl</b></td>
									</tr>
									
											
						</tr>";
    $html .= "</table>";
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename){			
		@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

function return_field_value($fdata,$tdata,$cdata){
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	
	return $m_data ;
}
?>