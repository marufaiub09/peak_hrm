<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );


//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//print_r($company_details);


//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$department_details[$row['id']] = mysql_real_escape_string( $row['department_name'] );		
	}
//print_r($department_details);


//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
//print_r($designation_chart);


//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
//print_r($section_details);	

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}


function check_attendance_status( $code, $emp_att_status, $fromdate, $todate, $ab_days, $cal_days)
{
	
	$inc=0;
	for ($i=0; $i<$cal_days; $i++)
	{
		$c_date=add_date($todate,-$i);
		 
		if ( $emp_att_status[$code][$c_date]=="A" ) 
			$inc++;
		else if ( $emp_att_status[$code][$c_date]=="W" || $emp_att_status[$code][$c_date]=="GH" || $emp_att_status[$code][$c_date]=="FH" || $emp_att_status[$code][$c_date]=="H" || $emp_att_status[$code][$c_date]=="CH" )
			$val=0;
		else
			$inc=0;
		//$c_date	
		if ($inc==$ab_days){ $tr=$c_date;  break; }	
		//cal_days
	}
	 if ($inc==$ab_days)
	 	return $tr."__".$inc;
	else
		return 0;
	  
}


if($action=='absent_report'){
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	
	$company = $cbo_company_id;
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
    if ($section_id==0) $section_id=""; else $section_id="and e.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($department_id==0) $department_id=""; else	$department_id="and e.department_id='$department_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and e.salary_type_entitled='$cbo_salary_based'";


	
		
	ob_start();
	
	
	$addrs ="";		
	if($company_details[$company]['plot_no'] !='') $addrs .= $company_details[$company]['plot_no'].", "; 
	if($company_details[$company]['level_no'] !='') $addrs .= $company_details[$company]['level_no'].", "; 
	if($company_details[$company]['block_no'] !='') $addrs .= $company_details[$company]['block_no'].", "; 
	if($company_details[$company]['road_no'] !='') $addrs .= $company_details[$company]['road_no'].", "; 
	if($company_details[$company]['city'] !='') $addrs .= $company_details[$company]['city']." "; 
	?>       
	<table cellpadding="0" cellspacing="1" border="1" width="1050" align="center" style="border:1px thin black;" class="rpt_table" rules="all" >
       <thead>
            <tr>
                <th colspan="11" align="center"><font size="+1"><? echo $company_details[$company]['company_name']; ?></font></th></tr>
            <tr><th colspan="11" align="center">Address : <? echo $addrs; ?></th></tr>        
            <tr>
                <th colspan="11" align="center"> Continuous Absent <? echo $txt_days; ?> Or More Days</th>
            </tr>
            <tr>
                <th width="40"><b>SL</b></th>
                <th width="70"><b>ID Card</b></th>
                <th width="80"><b>Emp Code</b></th>
                <th align="130"><b>Name</b></th>
                <th align="100"><b>Designation</b></th>
                <th align="90"><b>Department</b></th>
                <th align="90"><b>Section</b></th>
                <th align="80"><b>Subsection</b></th>
                <th align="90"><b>DOJ</b></th>
                <th align="90"><b>Absent Start</b></th>
                <th align="90"><b>Total Absent</b></th>
            </tr>
       </thead>
       <tbody>
    <?
		$emp_att_status=array();
		$emp_code=array();
		$emp_name=array();
		$emp_desig=array();
		$emp_sec=array();
		$emp_subsec=array();
		$emp_join=array();
		$emp_id_card=array();
		
		/*$sql ="select CONCAT(e.first_name, ' ',e.middle_name, ' ',e.last_name) AS name,
		 	e.company_id,e.id_card_no,e.designation_id,e.section_id,e.subsection_id,e.department_id,e.category,
			e.joining_date,a.attnd_date,a.emp_code,a.status
			from hrm_employee e, hrm_attendance a  	
			where 
			e.status_active=1 and e.company_id=$company and a.emp_code=e.emp_code $department_id $section_id  $category
			and a.attnd_date between '$fromdate' and '$todate'
			and a.status in ('A','W','GH','FH','H') order by e.company_id,e.section_id,e.id_card_no";*/
		
		 $sql=mysql_query("select CONCAT(e.first_name, ' ',e.middle_name, ' ',e.last_name) AS name,
		 	e.company_id,e.id_card_no,e.designation_id,e.section_id,e.subsection_id,e.department_id,e.category,
			e.joining_date,a.attnd_date,a.emp_code,a.status
			from hrm_employee e, hrm_attendance a  	
			where 
			e.status_active=1 and e.company_id=$company and a.emp_code=e.emp_code $department_id $section_id $salary_based $category
			and a.attnd_date between '$fromdate' and '$todate'
			and a.status in ('A','W','GH','FH','H','CH' ) order by e.company_id,e.section_id,e.id_card_no");
	 
		//echo $sql;die;	
		while($row=mysql_fetch_array($sql))
		{
			$emp_att_status[$row[emp_code]][$row[attnd_date]]=$row[status];
			if (!in_array($row[emp_code],$emp_code))
			{
				$emp_code[]=$row[emp_code];
				$emp_id_card[$row[emp_code]]=$row[id_card_no];
				$emp_name[$row[emp_code]]=$row[name];
				$emp_desig[$row[emp_code]]=$row[designation_id];
				$emp_sec[$row[emp_code]]=$row[section_id];
				$emp_subsec[$row[emp_code]]=$row[subsection_id];
				$emp_join[$row[emp_code]]=$row[joining_date];
				$emp_depart[$row[emp_code]]=$row[department_id];
				
			}
		}
		$kk=0;
		//print_r( $emp_code);die;
		foreach ($emp_code as $code)
		{
			$ret=explode("__",check_attendance_status($code,$emp_att_status,$fromdate,$todate,$txt_days,$get_days));
			if ( $ret[0]!=0 )
			{
				$kk++;
				?>
                 <tr>
                    <td><? echo $kk; ?></td>
                    <td>&nbsp;<? echo $emp_id_card[$code]; ?></td>
                    <td>&nbsp;<? echo $code; ?></td>
                    <td><? echo $emp_name[$code]; ?></td>
                    <td>&nbsp;<? echo $designation_chart[$emp_desig[$code]]; ?></td>
                    <td>&nbsp;<? echo $department_details[$emp_depart[$code]]; ?></td>
                    <td>&nbsp;<? echo $section_details[$emp_sec[$code]]; ?></td>
                    <td>&nbsp;<? echo $subsection_details[$emp_subsec[$code]]; ?></td>
                    <td>&nbsp;<? echo convert_to_mysql_date($emp_join[$code]); ?></td>               
                    <td>&nbsp;<? echo convert_to_mysql_date($ret[0]); ?></td>
                    <td>&nbsp;<? echo $ret[1]; ?></td>  			            
                </tr>
				 
                <?
			}
			
			//echo $code."<br>";
		}
			 
		?>
    	</tbody>
    </table>
 	<?
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>