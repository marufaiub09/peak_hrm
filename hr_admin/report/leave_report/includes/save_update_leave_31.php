<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}



if( $action == "employee_wise_leave_report" ) {	
	
	//html to xls file name and output link
	/*$temp_file_name_in_out=$_SESSION['temp_file_name_in_out'];
	unlink('tmp_report_file/'.$temp_file_name_in_out);
	$name=time();
	$name="$name".".xls";
	$_SESSION['temp_file_name_in_out']=$name;	
	echo "<div style='width:250px;background-color:#F0F0B8;font-weight:bold; font-size:18px;'>Convert To <a href='includes/tmp_report_file/".$name."'><img src='includes/logo/excel-logo.png' name='logo_id' id='logo_id' /></a></div><br \>";
		*/
	$status = "and dtls.leave_status in ('CL','ML','SL','EL','SpL','LWP','EdL')";
	if($txt_from_date!='' && $txt_to_date!=''){
		$txt_from_date=convert_to_mysql_date($txt_from_date);
		$txt_to_date=convert_to_mysql_date($txt_to_date);
		$search_date = "dtls.leave_date >= '$txt_from_date' and dtls.leave_date <= '$txt_to_date' and";
		$active_deleted = "and status_active=1 and is_deleted=0";
	}
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
		
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and department_id='$department_id'";			
		}
	$emp_code = $_GET['txt_emp_code'];//search employee code
	if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and emp_code in $search_emp_code";		
		}
	else {$search_code='';}
	
	$sql_d = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 and is_deleted=0 $search_code $cbo_company_id $location_id $department_id $section_id $subsection_id $designation_id $category order by company_id";	
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );	
	$html = "<table id=\"tblCustomer\" cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">";
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{			
			//$sql = "SELECT *  FROM hrm_leave_transaction where  id in( select distinct leave_id from hrm_leave_transaction_details where leave_date >= '$txt_from_date' and leave_date <= '$txt_to_date')  and status_active=1 ".$status."and emp_code=".$row_dp["emp_code"]." order by 'from_date' asc";
			$sql = "SELECT dtls.*,mst.remarks FROM hrm_leave_transaction_details dtls,hrm_leave_transaction mst where $search_date mst.status_active=1 $status and mst.id=dtls.leave_id and dtls.emp_code=".$row_dp["emp_code"];
			//echo $sql;
		 	//exit();
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );						
			$emp_tot=mysql_num_rows($result);			
			if ($emp_tot>0)
				{				
					//$designation_name=return_field_value("custom_designation","lib_designation","id=$row_dp[designation_id]");					
					//$department_name=return_field_value("department_name","lib_department","id=$row_dp[department_id]");
					//$company_name=return_field_value("company_name","lib_company","id=$row_dp[company_id]");
					
	$html .= "<tr id=\"rowHeader\" bgcolor=#F0F0B8>		
						<td colspan=7>Employee Code:  <b>$row_dp[emp_code]</b>    Employee Name:    <b>$row_dp[name]</b>   Designation: <b>".$designation_chart[$row_dp['designation_id']]['custom_designation']."</b></td>											
					</tr>
					<tr>						
						<td><b>Company&Department</b></td>						
						<td><b>Leave Type</b></td>
						<td><b>From Date</b></td>
						<td><b>To Date</b></td>
						<td><b>Total Days</b></td>
						<td><b>Cumulative</b></td>
						<td><b>Remark</b></td>
					</tr>";							
			
			$grandTotal=0;
			$days=1;
			while( $row = mysql_fetch_array( $result ) ) 
			{	
						$grandTotal++;				
				
	$html .= "		<tr>
						<td></td>						
					</tr>					
					<tr class=\"gradeA\">
						<td>".$company_details[$row_dp['company_id']]['company_name'].", ".$department_details[$row_dp['department_id']]['department_name']."</td>
						<td>$row[leave_status]</td>						
						<td><b>".convert_to_mysql_date($row[leave_date])."</b></td> 	
						<td><b>".convert_to_mysql_date($row[leave_date])."</b></td>
						<td>$days</td>
						<td>$grandTotal</td>
						<td>&nbsp;$row[remarks]</td>						
					</tr>";				
			}
			//bgcolor=#E0ECF8		
	$html .= "
					<tr> 
						<td colspan=4><b>Grand Total</b></td>
						<td colspan=3>$grandTotal</td>
					</tr>";
		}
	
	}
		$html .= " </table>";
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/"."*.xls") as $filename){			
                @unlink($filename);
			}
	
	//html to xls convert
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html";//."####"."$name";
	exit();
	
}


if( $action == "employee_summ_leave_report" ) {	
	
	//html to xls file name and output link
	/*$temp_file_name_in_out=$_SESSION['temp_file_name_in_out'];
	unlink('tmp_report_file/'.$temp_file_name_in_out);
	$name=time();
	$name="$name".".xls";
	$_SESSION['temp_file_name_in_out']=$name;	
	echo "<div style='width:250px;background-color:#F0F0B8;font-weight:bold; font-size:18px;'>Convert To <a href='includes/tmp_report_file/".$name."'><img src='includes/logo/excel-logo.png' name='logo_id' id='logo_id' /></a></div><br \>";*/
		
	if($txt_from_date!='' && $txt_to_date!=''){
		$txt_from_date=convert_to_mysql_date($txt_from_date);
		$txt_to_date=convert_to_mysql_date($txt_to_date);
		$search_date = "leave_date >= '$txt_from_date' and leave_date <= '$txt_to_date' and";
	}
	$status="and leave_status in ('CL','ML','SL','EL','SpL','LWP','EdL')";		
	//if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and job.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id='$department_id'";
		
	if ($cbo_company_id==0) 
		{
			$company_id="";			
		}
	else
		{
			$company_id="where id='$cbo_company_id'";			
		}
			
	$sql_company = "SELECT id, company_name  FROM lib_company $company_id order by id";
	$result_sql_company = mysql_query( $sql_company ) or die( $sql_company . "<br />" . mysql_error() );
		
	$html = "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\" style=\"table-layout:950\">";
	while( $row_company = mysql_fetch_array( $result_sql_company ) ) 
	{
		$emp_code = $_GET['txt_emp_code'];//searching employee code
		if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and emp_code in $search_emp_code";		
		}
		else {$search_code='';}
		
		$sql_d = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 and is_deleted=0 $search_code and company_id=$row_company[id] $location_id $department_id $section_id $subsection_id $designation_id $category order by designation_id";	
		//echo $sql_d;
		$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );	
		$check=0;
		while( $row_dp = mysql_fetch_array( $result_d ) ) 
			{			
					//$sql = "select distinct emp_code from hrm_leave_transaction_details where leave_date BETWEEN '$txt_from_date' and '$txt_to_date' and status_active=1 ".$status."and emp_code=".$row_dp["emp_code"]."  order by id asc";
					$sql_leave = "select count(leave_status) as total_leave, emp_code, leave_id, leave_status from hrm_leave_transaction_details where $search_date status_active=1 $status and emp_code=".$row_dp["emp_code"]." group by leave_status";
					
					/*$sql_leave = "select count(CASE WHEN leave_status ='CL' THEN emp_code END) AS 'CL', count(CASE WHEN leave_status ='SL' THEN emp_code END) AS 'SL',count(CASE WHEN leave_status ='EL' THEN emp_code END) AS 'EL',count(CASE WHEN leave_status ='ML' THEN emp_code END) AS 'ML',count(CASE WHEN leave_status ='SpL' THEN emp_code END) AS 'SpL',count(CASE WHEN leave_status ='LWP' THEN emp_code END) AS 'LWP',count(CASE WHEN leave_status ='EdL' THEN emp_code END) AS 'EdL', emp_code from hrm_leave_transaction_details where leave_date BETWEEN '$txt_from_date' and '$txt_to_date' and status_active=1 and emp_code='$row_dp[emp_code]'";
					//exit();
					*/
					$result_leave = mysql_query( $sql_leave ) or die( $sql_leave . "<br />" . mysql_error() );
					$nurows_leave=mysql_num_rows($result_leave);
					if($nurows_leave>0)
					{													
						
						if($check==0){						
							$department_name=return_field_value("department_name","lib_department","id=$row_dp[department_id]");									
							$html .= "<tr bgcolor=\"#F0F0B8\">		
										<td colspan=19>Company Name: <b>$row_company[company_name]</b></td>											
									</tr>
									<tr>						
											<td rowspan=2 width=50><b>Emp Code</b></td>
											<td rowspan=2 width=250><b>Name</b></td>						
											<td rowspan=2 width=130><b>Designation</b></td>
											<td rowspan=2 width=100><b>Department</b></td>
											<td rowspan=2 width=70><b>DOJ</b></td>					
											<td colspan=7 align=\"center\" width=200><b>Leave Availed</b></td>
											<td colspan=7 align=\"center\" width=200><b>Leave Balance</b></td>                       
										</tr>
										<tr>                      
											<td width=50><b>CL</b></td>
											<td width=50><b>SL</b></td>
											<td width=50><b>EL</b></td>
											<td width=50><b>SpL</b></td>
											<td width=50><b>LWP</b></td>
											<td width=50><b>ML</b></td>	
											<td width=50><b>EdL</b></td>	
											<td width=50><b>CL</b></td>
											<td width=50><b>SL</b></td>
											<td width=50><b>EL</b></td>
											<td width=50><b>SpL</b></td>
											<td width=50><b>LWP</b></td>
											<td width=50><b>ML</b></td>	
											<td width=50><b>EdL</b></td>
										</tr>";	
									
									$check=1;
							}				
						
						
						$cl_total=0;$sl_total=0;$el_total=0;$spl_total=0;$lwp_total=0;$ml_total=0;$edl_total=0;							
						while( $row = mysql_fetch_array( $result_leave ) ) 
						{				
							//total leave calculation here//	
							if($row["leave_status"]=='CL'){	$cl_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='SL'){	$sl_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='EL'){	$el_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='SpL'){ $spl_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='LWP'){ $lwp_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='ML'){ $ml_total=$row["total_leave"];}	
							
							if($row["leave_status"]=='EdL'){ $edl_total=$row["total_leave"];}	
							//ekr
							//leave balace calculation
							//$leave_balance_sql="select balance, leave_type from hrm_leave_balance bal where emp_code=$row[emp_code] and leave_year in ('select  .id=$row_dp[location_id] and leave_year=$leave_year ";
							
							$leave_year=return_field_value("id","lib_policy_year","type=0 and is_locked=0 limit 1");
							$cl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'CL' and status_active=1");
							if($cl_balance=='')$cl_balance='0';
							$cl_balance=$cl_balance-$cl_total;
							$sl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'SL' and status_active=1");
							if($sl_balance=='')$sl_balance='0';
							$sl_balance=$sl_balance-$sl_total;
							$el_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'EL' and status_active=1");
							if($el_balance=='')$el_balance='0';
							$el_balance=$el_balance-$el_balance;
							$spl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'SpL' and status_active=1");
							if($spl_balance=='')$spl_balance='0';
							$spl_balance=$spl_balance-$spl_total;
							$lwp_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'LWP' and status_active=1");
							if($lwp_balance=='')$lwp_balance='0';
							$lwp_balance=$lwp_balance-$lwp_total;
							//ML incomplete								
							$ml_limit=return_field_value("days_required","hrm_leave_transaction tr, hrm_maternity_leave mat","tr.emp_code=$row[emp_code] and tr.emp_code=mat.emp_code and tr.leave_type = 'ML' and tr.status_active=1 and tr.is_deleted=0");	
							if($ml_balance=='')$ml_balance='0'; else $ml_balance=$ml_limit-$ml_total;
							//Education Leave
							$edl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'EdL' and status_active=1");	
							if($edl_balance=='')$edl_balance='0';
							$edl_balance=$edl_balance-$edl_total;
							}
											
						$designation_name=return_field_value("custom_designation","lib_designation","id=$row_dp[designation_id]");
							
				$html .= "											
						<tr class=\"gradeA\">
							<td>$row_dp[emp_code]</td>
							<td>$row_dp[name]</td>
							<td>$designation_name</td>
							<td>$department_name</td>
							<td>".convert_to_mysql_date($row_dp[joining_date])."</td>						
						
								<td>$cl_total</td>
								<td>$sl_total</td>
								<td>$el_total</td>
								<td>$spl_total</td>
								<td>$lwp_total</td>
								<td>$ml_total</td>
								<td>$edl_total</td>	
						
								<td>$cl_balance</td>
								<td>$sl_balance</td>
								<td>$el_balance</td>
								<td>$spl_balance</td>
								<td>$lwp_balance</td>
								<td>$ml_balance</td>
								<td>$edl_balance</td>
								
						</tr>";						
				}
			}
		}
		$html .= " </table>";
	
	 
		//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/"."*.xls") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
		//---------end------------//
	
	//html to xls convert
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html";//."####"."$name";
	exit();
	
	print_r( $html );
	exit();
}



if($action=='absent_report'){
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	
	$company = $cbo_company_id;
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	//if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
    if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($department_id==0) $department_id=""; else	$department_id="and department_id='$department_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
		
	ob_start();
	
	
	$addrs ="";		
	if($company_details[$company]['plot_no'] !='') $addrs .= $company_details[$company]['plot_no'].", "; 
	if($company_details[$company]['level_no'] !='') $addrs .= $company_details[$company]['level_no'].", "; 
	if($company_details[$company]['block_no'] !='') $addrs .= $company_details[$company]['block_no'].", "; 
	if($company_details[$company]['road_no'] !='') $addrs .= $company_details[$company]['road_no'].", "; 
	if($company_details[$company]['city'] !='') $addrs .= $company_details[$company]['city']." "; 
	?>       
	<table cellpadding="0" cellspacing="1" border="1" width="900" align="center" style="border:1px thin black;" class="rpt_table" rules="all" >
       <thead>
            <tr>
                <th colspan="11" align="center"><font size="+1"><? echo $company_details[$company]['company_name']; ?></font></th></tr>
            <tr><th colspan="11" align="center">Address : <? echo $addrs; ?></th></tr>        
            <tr>
                <th colspan="11" align="center"> Continuous Absent <? echo $txt_days; ?> Or More Days</th>
            </tr>
            <tr>
                <th width="40"><b>SL</b></th><th width="70"><b>ID Card</b></th><th width="80"><b>Emp Code</b></th><th align="130"><b>Name</b></th><th align="100"><b>Designation</b></th><th align="90"><b>Department</b></th><th align="90"><b>Section</b></th><th align="80"><b>Subsection</b></th><th align="90"><b>DOJ</b></th><th align="90"><b>Absent Start</b></th><th align="90"><b>Total Absent</b></th>
            </tr>
       </thead>
       <tbody>
    <?
	
		//$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ',emp.middle_name, ' ',emp.last_name) AS name, a.attnd_date, a.status FROM hrm_employee emp, hrm_attendance a WHERE emp.status_active=1 and emp.emp_code=a.emp_code and a.attnd_date between '$fromdate' and '$todate' and a.status='A' $cbo_company_id $location_id $section_id $subsection_id $designation_id $department_id order by company_id, section_id ";
		
		$sqls = "select *,CONCAT(first_name, ' ',middle_name, ' ',last_name) AS name from hrm_employee where status_active=1 $cbo_company_id $location_id  $section_id $subsection_id $designation_id $department_id $category order by company_id,section_id,id_card_no"; 
		 
		$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
		$j=0;$new_company = array();
		while($rows=mysql_fetch_array($results))
		{
			$sql = "select * from hrm_attendance where emp_code=$rows[emp_code] and attnd_date between '$fromdate' and '$todate' and status in ('A','W','GH','FH','H') order by attnd_date ";		
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$i=0;$k=0;$flag=0;$absent_days=0;$arr_attn = array();
			$numrows = mysql_num_rows($result);
						
				while($row=mysql_fetch_array($result))
				{				
					if($i==0)
						$c_date = date("Y-m-d",strtotime("+".$i." day", strtotime($row['attnd_date'])));
					else
						$c_date = date("Y-m-d",strtotime("+".$i." day", strtotime($c_date)));
					
					if(trim($row['status'])=='A')
					{
						$absent_days++;
						if($row['attnd_date']==$c_date)
						{
							$arr_attn[$k]=$row['attnd_date'];
							if($k+1==$txt_days && $flag==0) {$start_absent = $arr_attn[$k-1];$flag=1;}
							$i=1;
							$k++;
						}
						else
						{//echo $c_date."###".$row['attnd_date']."\n";die;
							$k=0;$i=0;					
							$arr_attn = array();	
							$arr_attn[$k]=$row['attnd_date'];				
							if($k+1==$txt_days && $flag==0) {$start_absent = $arr_attn[$k-1];$flag=1;}
							$k++;$i=1;
						}
					}
					
 				}
				
				if($flag==1)
				{
							
					?>
					 <tr>
						<td><? echo ++$j; ?></td>
						<td>&nbsp;<? echo $rows['id_card_no']; ?></td>
						<td>&nbsp;<? echo $rows['emp_code']; ?></td>
						<td><? echo $rows['name']; ?></td>
						<td>&nbsp;<? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td>
						<td>&nbsp;<? echo $department_details[$rows['department_id']]['department_name']; ?></td>
						<td>&nbsp;<? echo $section_details[$rows['section_id']]['section_name']; ?></td>
						<td>&nbsp;<? echo $subsection_details[$rows['subsection_id']]['subsection_name']; ?></td>
						<td>&nbsp;<? echo convert_to_mysql_date($rows['joining_date']); ?></td>               
						<td>&nbsp;<? echo convert_to_mysql_date($start_absent); ?></td>
						<td>&nbsp;<? echo $absent_days; ?></td>  			            
					</tr>
			
				<? 
				}
		}
    ?>
    	</tbody>
    </table>
 	<?
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>

