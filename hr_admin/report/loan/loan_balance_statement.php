<? 
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_name ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}
	
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/functions.js" type="text/javascript"></script>
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
    <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    
    <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
    
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        
	<script type="text/javascript" charset="utf-8">
		var location_details = division_details = department_details = section_details = new Array();
		
		
		
		<?php
		foreach( $location_details AS $location_id => $location ) {
			echo "location_details[$location_id] = new Array();\n";
			foreach( $location AS $key => $value ) {
				if( $key == 'id' || $key == 'company_id' ) echo "location_details[$location_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "location_details[$location_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $division_details AS $division_id => $division ) {
			echo "division_details[$division_id] = new Array();\n";
			foreach( $division AS $key => $value ) {
				if( $key == 'id' || $key == 'location_id' ) echo "division_details[$division_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "division_details[$division_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $department_details AS $department_id => $department ) {
			echo "department_details[$department_id] = new Array();\n";
			foreach( $department AS $key => $value ) {
				if( $key == 'id' || $key == 'division_id' ) echo "department_details[$department_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "department_details[$department_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $section_details AS $section_id => $section ) {
			echo "section_details[$section_id] = new Array();\n";
			foreach( $section AS $key => $value ) {
				if( $key == 'id' || $key == 'department_id' ) echo "section_details[$section_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "section_details[$section_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		?>		
		
	function populate_cost_center( child, parent_id, selected_id ) {
		$.ajax({
			type: "POST",
			url: "../hrm/hrm_data.php",
			data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
			success: function( html ) {
				$('#' + child + '_id').html( html )
			}
		});
	}
		
	//numeric field script
    function numbersonly(myfield, e, dec)
    {
        var key;
        var keychar;
    
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);
    
        // control keys
        if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
        return true;
        
        // numbers
        else if ((("0123456789,").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }	
	function openmypage_employee_info()
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')
		
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#txt_emp_code').val(thee_id.value);
		}
	}
		

//ajax submit starts here 10-02-2013
function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();


function fnc_emp_loan_report() {		//loan Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);		
	var error = false, data = '';
	
	if( $('#cbo_company_id').val() == "" || $('#cbo_company_id').val() == 0) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	if( error == false ) 
	{		
		data = '&cbo_emp_category=' + $('#cbo_emp_category').val() + 
		'&cbo_company_id=' + $('#cbo_company_id').val() + 
		'&location_id=' + $('#location_id').val() + 
		'&division_id=' + $('#division_id').val() +
		'&department_id=' + $('#department_id').val() + 
		'&section_id=' + $('#section_id').val()+ 
		'&subsection_id=' + $('#subsection_id').val()+ 
		'&designation_id=' + $('#designation_id').val() + 
		'&txt_emp_code=' + $('#txt_emp_code').val();
		
		/*'
		'&txt_date=' + $('#txt_date').val() + 
		&group_by_id=' + $('#group_by_id').val() + 
		'&order_by_id=' + $('#order_by_id').val() + */
		
		//alert (data);
		nocache = Math.random();
		http.open( 'GET', 'includes/generate_employee_loan_balance_report.php?action=loan_statement_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_emp_loan_report;
		http.send(null);
	}
}

function response_fnc_emp_loan_report() {	//leave Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('####');	
		
		 $('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		 $('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		 $('#data_panel2').html( response[0] );	
		 
		 	
		/* $('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
		 $('#data_panel2').html( response[0] );*/
		 
		 
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.').addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}

$(document).ready(function(e) {
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
		//header: false, 
		selectedText: "# of # selected",
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({

});
	
	$(".datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});
});

/*	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.height="auto";
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		 
		d.close();
		document.getElementById('scroll_body').style.overflow="scroll";
		document.getElementById('scroll_body').style.height="250px";
	}
*/



function new_window()
{
	document.getElementById('scroll_body').style.overflow="";
	document.getElementById('scroll_body').style.height="";
		
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel2').innerHTML);
	d.close();
	
	document.getElementById('scroll_body').style.overflow="";
	document.getElementById('scroll_body').style.height="";
}		


   
    </script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }		
	</style>
    
</head>

<body style="font-family:verdana; font-size:11px;">
        
        <div style="width:100%" align="center">
            <div style="height:45px;">
                <div align="center" style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
                <h3 align="left" style="top:1px;width:100%;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Loan Report</h3>
            </div>
            <div id="content_search_panel" > 
        <form id="loan_report_form"  autocomplete="off" method="POST">
            <fieldset>
                 <table align="center" width="1120px" cellpadding="0" cellspacing="0" class="rpt_table">
                    <thead>
                        <th><strong>Category</strong></th>
                        <th><strong>Company</strong></th>
                        <th><strong>Location</strong></th>
                        <th><strong>Division</strong></th>
                        <th><strong>Department</strong></th>
                        <th><strong>Section</strong></th>
                        <th><strong>SubSection</strong></th>
                        <th><strong>Designation</strong></th>              
                    </thead>
                    <tr class="general">
                        <td>
                             <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px;">
                                <option value="">All Category</option>
                                <?
                                foreach($employee_category as $key=>$val)
                                {
                                ?>
                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                <?
                                }
                                ?> 
                             </select>
                        </td>
                        <td>
                            <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px;">
									<? if($company_cond=="")
                                    { 
                                    ?>
                                        <option value="0">-- Select --</option>
                                    <?php 
                                    } 
                                    foreach( $company_details AS $key=>$value )
                                    { 
                                    ?>
                                        <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                    <?php 
                                    } 
                                    ?>
                            </select>
                        </td>
                        <td>
                            <select name="location_id" id="location_id" class="combo_boxes" style="width:140px;" >
                                <option value="0">-- Select --</option>
                                <?php 
                                foreach( $location_details AS $key=>$value )
                                { 
                                ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php 
                                } 
                                ?>
                          </select>
                      </td>
                        <td id="division">
                            <select name="division_id" id="division_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                <?php 
                                foreach( $division_details AS $key=>$value )
                                { 
                                ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php 
                                } 
                                ?>
                            </select>
                        </td>				
                        <td>
                            <select name="department_id" id="department_id" class="combo_boxes" style="width:140px;" multiple="multiple" >
								<?php 
                                foreach( $department_details AS $key=>$value )
                                { 
                                ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php 
                                } 
                                ?>
                            </select>
                        </td>
                        <td>
                            <select name="section_id" id="section_id" class="combo_boxes" style="width:140px;" multiple="multiple">
								<?php 
                                foreach( $section_details AS $key=>$value )
                                { 
                                ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php 
                                } 
                                ?>
                            </select>
                        </td>          
                       <td id="subsection">
                            <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px;" multiple="multiple">
                                <option value="0">-- Select --</option>
                                <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td id="designation">
                            <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px;" multiple="multiple" >
                                <option value="0">-- Select --</option>
                                <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </table>
                
                 <div style=" height:8px;"></div>
                        <table align="center" width="300" cellpadding="0" cellspacing="0" class="rpt_table">
                            <thead>
                               <!--<th><strong>Loan Date</strong></th>-->
                                <!--<th><strong>Group By</strong></th>
                                <th><strong>Order By</strong></th>-->
                                <th><strong>Employee</strong></th>
                            </thead>
                            <tbody>
                                <tr class="general">
                                    <!--<td><input type="text" name="txt_date" id="txt_date" value="" class="datepicker" style="width:100px"  /></td>-->
                                   
                                   <!-- <td>
                                        <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:140px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
											/*foreach( $groupby_arr AS $key=>$value )
											{ 
											?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
											} 
											?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:140px" >							
                                             <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $orderby_arr AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } */
                                            ?>
                                        </select>
                                    </td>-->
                                    <td>
                                    <input type="text" name="txt_emp_code" id="txt_emp_code" value="" style="width:200px;" class="text_boxes" size="40" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click For Search" /> 
                                    </td>                               
                                </tr>
                        	</tbody>
                        </table>
                    <div style="padding-top:10px;">
                    <input type="button" name="search_all" id="search_all" value="Show" class="formbutton" onclick="javascript:fnc_emp_loan_report()" style="width:100px" />
                    <!--<input type="button" name="search_unadj" id="search_unadj" value="Search Unadjusted" class="formbutton" onclick="javascript:fnc_emp_loan_report()" />-->
                    <input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" />
                    </div>
            </fieldset>
        </form>
         </div>
     </div>
     
    <fieldset>
        <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
    </fieldset>	
	
</body>
</html>