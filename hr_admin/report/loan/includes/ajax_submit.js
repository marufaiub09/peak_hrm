//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();


/*function fnc_emp_loan_report() {		//loan Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);		
	var error = false, data = '';
		
	if( error == false ) {		
		data = '&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val()
				+ '&department_id=' + $('#department_id').val() + '&section_id=' + $('#section_id').val()+ '&subsection_id=' + $('#subsection_id').val()+ '&designation_id=' + $('#designation_id').val() + '&txt_emp_code=' + $('#txt_emp_code').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_leave.php?action=loan_statement_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_emp_loan_report;
		http.send(null);
	}
}

function response_fnc_emp_loan_report() {	//leave Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('###');		
		 $('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
		 $('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.').addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}*/


/*function fnc_emp_loan_ledger_report(){
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);		
	var error = false, data = '';
		
	if( error == false ) {		
		data = '&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val()
				+ '&department_id=' + $('#department_id').val() + '&section_id=' + $('#section_id').val()+ '&subsection_id=' + $('#subsection_id').val()+ '&designation_id=' + $('#designation_id').val() + '&txt_emp_code=' + $('#txt_emp_code').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_leave.php?action=loan_ledger_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_emp_loan_ledger_report;
		http.send(null);
	}
	
}

function response_fnc_emp_loan_ledger_report() {	//leave Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('###');
		 $('#data_panel').html('<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
		 $('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.').addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/