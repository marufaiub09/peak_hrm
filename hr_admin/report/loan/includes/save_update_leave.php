<?php
date_default_timezone_set('Asia/Dhaka');
session_start();
include('../../../../includes/common.php');
include('../../../../includes/array_function.php');

extract( $_GET );
extract( $_POST );



//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	
	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


	
/*if( $action == "loan_statement_report" ) {	

	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and job.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and job.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and job.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and job.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
		
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and job.department_id='$department_id'";			
		}
	$emp_code = $_GET['txt_emp_code'];//search employee code
	if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and dtls.emp_code in $search_emp_code";		
		}
	else {$search_code='';}	
	 
	$sql_loan = "SELECT SUM(
					CASE WHEN mst.emp_code = dtls.emp_code
					$search_code
					THEN principal_amnt
					ELSE 0
					END ) AS principal_adj,
					SUM(
					CASE WHEN mst.emp_code = dtls.emp_code
					$search_code
					THEN interest
					ELSE 0
					END ) AS interest_adj,
					mst.approved_amount, mst.total_interest,app.loan_type, dtls . *
					FROM hrm_loan_payback_scheduling_details dtls, hrm_loan_payback_scheduling_mst mst, hrm_loan_application app
					WHERE mst.emp_code = dtls.emp_code
					AND app.id = mst.loan_id
					GROUP BY dtls.emp_code";
	
	$result_d = mysql_query( $sql_loan ) or die( $sql_loan . "<br />" . mysql_error() );
	ob_start();
	?>
<div>
    <div style="width:1320px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
            <thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="130" align="center"><strong>Emp Name</strong></th>
                <th width="120" align="center"><strong>Designation</strong></th>
                <th width="140" align="center"><strong>Company</strong></th>
                <th width="130" align="center"><strong>Department</strong></th>
                <th width="80" align="center"><strong>Loan Type</strong></th>
                <th width="80" align="center"><strong>Principal Amount</strong></th>
                <th width="80" align="center"><strong>Total Interest</strong></th>
                <th width="80" align="center"><strong>Principal Adjust</strong></th>
                <th width="80" align="center"><strong>Interest Adjust</strong></th>
                <th width="80" align="center"><strong>Principal Balance</strong></th>
                <th width="80" align="center"><strong>Int. Balance</strong></th>
                <th align="center"><strong>Total Balance</strong></th>
	   		</thead>
       </table>
    </div>   
    <div style="width:1320px; overflow:scroll; height:250px" id="scroll_body" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<?
        $total_aprv_amt=0;$total_interest=0;$total_prncpl_adj=0;$total_interest_adj=0;$total_amt_balance=0;$total_interest_balance=0;$total_balance=0;
        $i=1;
        while($rows = mysql_fetch_array($result_d))
        {
		?>	
            <tr>
                <td width="30"><? echo $i; ?></td> 
                <td width="80">&nbsp;<? echo $rows['emp_code']; ?></td>
                <td width="130"><? echo $emp_basic[$rows['emp_code']]['name']; ?></td>											
                <td width="120">&nbsp;<? echo $designation_chart[$emp_basic[$rows['emp_code']]['designation_id']]['system_designation']; ?></td>
                <td width="140"><? echo $company_details[$emp_basic[$rows['emp_code']]['company_id']]['company_name'] ?></td>	 							
                <td width="130">&nbsp;<? echo $department_details[$emp_basic[$rows['emp_code']]['department_id']]['department_name'] ?></td>
                <td width="80">&nbsp;<? echo $advance_loan_type[$rows['loan_type']];?></td>
                <td width="80" align="right"><? echo number_format($rows['approved_amount'],2);?></td>
                <td width="80" align="right"><? echo number_format($rows['total_interest'],2);?></td>	
                <td width="80" align="right"><? echo number_format($rows['principal_adj'],2);?></td> 	
                <td width="80" align="right"><? echo number_format($rows['interest_adj'],2);?></td> 	
                <td width="80" align="right"><? echo number_format($rows['approved_amount']-$rows['principal_adj'],2);?></td>
                <td width="80" align="right"><? echo number_format($rows['total_interest']-$rows['interest_adj'],2);?></td>
                <td align="right"><? echo number_format(($rows['approved_amount']-$rows['principal_adj'])+($rows['total_interest']-$rows['interest_adj']),2);?></td>							
            </tr>
		<?	
        $total_aprv_amt += $rows['approved_amount'];
        $total_interest += $rows['total_interest'];
        $total_prncpl_adj += $rows['principal_adj'];
        $total_interest_adj += $rows['interest_adj'];
        $total_amt_balance += $rows['approved_amount']-$rows['principal_adj'];
        $total_interest_balance += $rows['total_interest']-$rows['interest_adj'];
        $total_balance += ($rows['approved_amount']-$rows['principal_adj'])+($rows['total_interest']-$rows['interest_adj']);
        
        $i++;	
        }	
	?>
            <tfoot>
                <th colspan="7" align="right"><strong>Total:</td>
                <th align="right"><strong><? echo number_format($total_aprv_amt,2); ?></strong></th>
                <th align="right"><strong><? echo number_format($total_interest,2); ?></strong></th>
                <th align="right"><strong><? echo number_format($total_prncpl_ad,2); ?></strong></th>
                <th align="right"><strong><? echo number_format($total_interest_adj,2); ?></strong></th>
                <th align="right"><strong><? echo number_format($total_amt_balance,2); ?></strong></th>
                <th align="right"><strong><? echo number_format($total_interest_balance,2); ?></strong></th>
                <th align="right"><strong><? echo number_format($total_balance,2); ?></strong></th>
           </tfoot>
        </table>	
	</div>
</div>
	<?	
	   $html = ob_get_contents();
	   ob_clean();
	   
		foreach (glob(""."*.pdf") as $filename) {			
			@unlink($filename);
		}
		
		echo "$html####$filename";
		exit();

}*/

/*if( $action == "loan_ledger_report" ) {	

	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and job.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and job.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and job.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and job.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
		
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and job.department_id='$department_id'";			
		}
	$emp_code = $_GET['txt_emp_code'];//search employee code
	if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and dtls.emp_code in $search_emp_code";		
		}
	else {$search_code='';}	
	 	
	ob_start();
	?>
 <div>					
	<table cellpadding="0" cellspacing="0" border="1" width="1000" class="rpt_table">               
   
	<?
	$total_aprv_amt=0;$total_interest=0;$total_prncpl_adj=0;$total_interest_adj=0;$total_amt_balance=0;$total_interest_balance=0;$total_balance=0;
	$i=1;	
	
	$sql_loan = "SELECT 			
					mst.approved_amount, mst.total_interest,app.loan_type, dtls . *
					FROM hrm_loan_payback_scheduling_details dtls, hrm_loan_payback_scheduling_mst mst, hrm_loan_application app
					WHERE mst.emp_code = dtls.emp_code
					AND app.id = mst.loan_id
					ORDER BY dtls.year ASC,dtls.month ASC";
	
	$result_d = mysql_query( $sql_loan ) or die( $sql_loan . "<br />" . mysql_error() );
	$emp_arr1=array();$arr1=0;
	$emp_arr2=array();$arr2=0;
	while($rows = mysql_fetch_array($result_d))
	{
		if(!in_array($rows['emp_code'],$emp_arr1))
		{
	?>	
			<tr><td colspan="7"><b>ID Card :</b><? echo $emp_basic[$rows['emp_code']]['id_card_no']; ?> <b>Emp Code :</b> <? echo $rows['emp_code']; ?> <b>Name :</b> <? echo $emp_basic[$rows['emp_code']]['name']; ?> <b>Designation :</b> <? echo $designation_chart[$emp_basic[$rows['emp_code']]['designation_id']]['system_designation']; ?> <b>Department :</b> <? echo $department_details[$emp_basic[$rows['emp_code']]['department_id']]['department_name']; ?></td></tr>
        	<tr>
            	<td width="30"><b>SL</b></td> 
                <td width="60"><b>Date</b></td>
                <td width="130"><b>Particulars</b></td>											
                <td width="110"><b>Ref. No</b></td>
                <td width="110"><b>Given</b></td>	 							
                <td width="110"><b>Adjusted</b></td>
                <td width="80"><b>Balance</b></td>           							
            </tr>
	 
	 <?
	 	$emp_arr1[$arr1++] = $rows['emp_code'];
		}	

		$sql_qr = "select * from hrm_loan_payback_scheduling_mst where emp_code = $rows[emp_code]";		
		$result_sql_qr = mysql_query( $sql_qr ) or die( $sql_qr . "<br />" . mysql_error() );
		
		while($row_qr = mysql_fetch_array($result_sql_qr) )
		{
			if(!in_array($row_qr['emp_code'],$emp_arr2))
			{
		?>	
                <tr>
                    <td width="30"><? echo $i; ?></td>     	       
                    <td width="80">&nbsp;<? echo $row_qr['starting_month'];?></td>
                    <td width="80" align="right">&nbsp;<? echo $row_qr[''];?></td>
                    <td width="80" align="right">&nbsp;<? echo $row_qr[''];?></td>	
                    <td width="80" align="right">&nbsp;<? echo number_format($row_qr['approved_amount'],2);?></td> 
                    <td>&nbsp;</td>	
                    <td width="80" align="right">&nbsp;<? echo number_format($row_qr['approved_amount'],2);?></td>	
             	</tr> 
                <tr class="display" style="background-color:<? echo $bgcolor;?>">
                    <td width="30"><? echo ++$i; ?></td>     	       
                    <td width="80">&nbsp;<? echo $row_qr['starting_month'];?></td>
                    <td width="80" align="right">&nbsp;<? echo $row_qr[''];?></td>
                    <td width="80" align="right">&nbsp;<? echo $row_qr[''];?></td>	
                    <td width="80" align="right">&nbsp;<? echo number_format($row_qr['total_interest'],2);?></td> 	
                    <td>&nbsp; </td>
                    <td width="80" align="right">&nbsp;<? echo number_format($row_qr['ttl_amnt_with_ints'],2);?></td>	
             	</tr>   					
        <?
				$emp_arr2[$arr2++] = $row_qr['emp_code'];
			}
		
		}

		$months_arr = array(1=>"Jan",2=>"Feb",3=>"Mar",4=>"Apr",5=>"May",6=>"Jun",7=>"Jul",8=>"Aug",9=>"Sep",10=>"Oct",11=>"Nov",12=>"Dec");
	?>	
		<tr>
            <td width="30"><? echo ++$i; ?></td>     	       
            <td width="80"><? echo $rows['month'].'-'.$rows['year'];?></td>
            <td width="80" align="right"><? echo "Adjusted ".$months_arr[$rows['month']].'-'.$rows['year'];?></td>
            <td width="80" align="right"><? echo $rows[''];?></td>	
            <td width="80" align="right"><? echo number_format($rows['principal_adj'],2);?></td> 	
            <td width="80" align="right"><? echo number_format($rows['total_amnt_per_inst'],2);?></td> 
            <? 
			$total_aprv_amt=$rows['approved_amount']+$rows['total_interest'];
			$total_prncpl_adj += $rows['total_amnt_per_inst'];
			$total_amt_balance = $total_aprv_amt-$total_prncpl_adj;
			?>	
            <td width="80" align="right"><? echo number_format($total_amt_balance,2);?></td>
        </tr>  
	<?		
	
	}	
	?>    	
	</table>
</div>	
	<?	
	$html = ob_get_contents();
	   ob_clean();
	   
		foreach (glob(""."*.pdf") as $filename) {			
			@unlink($filename);
		}
		
		echo "$html####$filename";
		exit();

}
*/


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>

