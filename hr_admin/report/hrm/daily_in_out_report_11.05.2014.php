<? 
// ekram
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
		

//$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement",6=>"Govt. Holiday",7=>"Week-end",8=>"Off Day Present",9=>"Off Day Not Present");
$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement");
$holiday_status=array(1=>"Govt. Holiday",2=>"Week-end",3=>"Both");

//$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement",6=>"Govt. Holiday",7=>"Week-end");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
       <script src="includes/functions.js" type="text/javascript"></script>
       <!-- <script src="../../../functions.js" type="text/javascript"></script>-->
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
				
				$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id,#shift_id,#cbo_status").multiselect({
				//header: false, #cbo_status,
				selectedText: "# of # selected",
				});
			});
			
			/*function populate_cost_center( child, parent_id, selected_id ) {
				$.ajax({
					type: "POST",
					url: "hrm_data.php",
					data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
					success: function( html ) {
						$('#' + child + '_id').html( html )
					}
				});
			}*/
			
			
			function openmypage(page_link,title)
			{
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=450px,center=1,resize=0,scrolling=0','../../')
			}
			
			$(document).ready(function() {
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
			});
			
			//ajx submit starts here
			function createObject() 
			{
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) 
				{
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} 
				else 
				{
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
			
			function fnc_daily_attendance_report( report_mailing )
			{
				if(!report_mailing) var report_mailing=0;
				$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
				var error = false;
				var data = '';
				
				var category_id		= $('#cbo_category_id').val();
				var company_id		= $('#cbo_company_id').val();
				var location_id		= $('#cbo_location_id').val();
				var division_id		= $('#cbo_division_id').val();
				var department_id	= $('#cbo_department_id').val();
				var section_id		= $('#cbo_section_id').val();
				var subsection_id	= $('#cbo_subsection_id').val();
				var designation_id	= $('#cbo_designation_id').val();
				
				var cbo_status		= $('#cbo_status').val();
				var txt_date		= $('#txt_date').val();
				var shift_id		= $('#shift_id').val();
				var cbo_salary_based= $('#cbo_salary_based').val();
				var group_by_id		= $('#group_by_id').val();
				var order_by_id		= $('#order_by_id').val();
				var id_card			= $('#id_card').val();
				var emp_code		= $('#emp_code').val();	
				var offday_status	= $('#cbo_offday_status').val();	
				
				if( $('#txt_date').val() == "" ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_month_selector').focus();
						$(this).html('Please select Date Field to search.').addClass('messageboxerror').fadeTo(900,1);
					});
				}
				
				if( error == false ) 
				{
					var data='&category_id='+category_id+
							'&company_id='+company_id+
							'&location_id='+location_id+
							'&division_id='+division_id+
							'&department_id='+department_id+
							'&section_id='+section_id+
							'&subsection_id='+subsection_id+
							'&designation_id='+designation_id+
							'&id_card='+id_card+
							'&emp_code='+emp_code+
							'&cbo_status='+cbo_status+
							'&offday_status='+offday_status+
							'&txt_date='+txt_date+
							'&shift_id='+shift_id+
							'&cbo_salary_based='+cbo_salary_based+
							'&group_by_id='+group_by_id+
							'&order_by_id='+order_by_id+
							'&report_mailing='+report_mailing;
							
					/* ---------------before-----------------------
					data = '&cbo_status=' + $('#cbo_status').val() 
							+'&txt_date=' + $('#txt_date').val()
							+ '&cbo_emp_category=' + $('#cbo_emp_category').val() 
							+ '&cbo_company_id=' + $('#cbo_company_id').val() 
							+ '&location_id=' + $('#location_id').val() 
							+ '&division_id=' + $('#division_id').val() 
							+ '&department_id=' + $('#department_id').val() 
							+ '&section_id=' + $('#section_id').val()
							+ '&subsection_id=' + $('#subsection_id').val() 
							+ '&designation_id=' + $('#designation_id').val()
							+ '&shift_id=' + $('#shift_id').val()
							+ '&cbo_salary_based=' + $('#cbo_salary_based').val()
							+ '&group_by_id=' + $('#group_by_id').val()
							+ '&order_by_id=' + $('#order_by_id').val();
					*/
					
					//alert(data);
					nocache = Math.random();
					http.open( 'GET', 'includes/generate_daily_in_out_report.php?action=daily_in_out_report' + data + '&nocache=' + nocache );
					http.onreadystatechange = response_daily_att_report;
					http.send(null);
				}
			}
			
			//Punch Report Response	
			function response_daily_att_report() 
			{	
				if(http.readyState == 4) {
					var response=http.responseText.split('####');
					//alert(response);		
					//$('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
					$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').append( '<input type="button" onclick="new_window_short()" value="HTML Short" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').append( '<input type="button" onclick="new_window_mail()" value="Mail Report" name="email" class="formbutton" style="width:100px"/><input type="hidden"  value="'+response[1]+'" name="email_link" id="email_link" style="width:100px"/>' );
					$('#data_panel2').html( response[0] );
					$('#data_panel3').html( response[2] );
					
					hide_search_panel();
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
			}	
			
			function new_window()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel2').innerHTML);
				d.close();
			}
			
			function new_window_short()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel3').innerHTML);
				d.close();
			}

			function new_window_mail(  )
			{
				var fname=$('#email_link').val();
				var txt_date=$('#txt_date').val();
				var email = prompt("Please enter email address seperated by COMMA","");
				 
				if ( email!=null )
				{
					//fnc_daily_attendance_report( 1 );
					  $.ajax({
							type: "POST",
							url: 'includes/generate_daily_in_out_report.php',
							data: 'htmldata=' + fname +'&action=mail_full_report&email_address='+email+'&txt_date='+txt_date,
							success: function( html ) 
							{
								//alert(html)
								if(html==1)
								{
									$("#messagebox").fadeTo( 200, 0.1, function() {
										$(this).html( 'E-mail address or report is blank' ).addClass('messagebox_ok').fadeTo(900,1);
										$(this).fadeOut(5000);
									});
								}
								else if(html==2)
								{
									$("#messagebox").fadeTo( 200, 0.1, function() {
										$(this).html( 'Report was not sent.' ).addClass('messagebox_ok').fadeTo(900,1);
										$(this).fadeOut(5000);
									});
								}
								else if(html==3)
								{
									$("#messagebox").fadeTo( 200, 0.1, function() {
										$(this).html( 'Report has been sent.' ).addClass('messagebox_ok').fadeTo(900,1);
										$(this).fadeOut(5000);
									});
								}
							}
						}); 
				}
			}
			
			//new add  change dropdown value function add here
			function populate_cost_center( child, parent_id, selected_id )
			{
				selected_id = ( selected_id == undefined ) ? 0 : selected_id;
				$.ajax({
					type: "POST",
					url: "hrm_data.php",
					data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
					success: function( html ) {
						$('#' + child + '_id').html( html );
					}
				});
			}
			
			 // Employee search by emp_code
            function openmypage_employee_info(page_link,title)
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#emp_code').val(thee_id.value);
                }
            }
			
			 // Employee search by id_card_no
            function openmypage_employee_info_id_card(page_link,title)
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }
			
		</script>
		<style type="text/css">
			#filter_panel select { width:100px; }
			#filter_panel * { font-family:verdana; font-size:11px; }
		</style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:1300px">
        	<div style="width:100%;">
                <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
        		<h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Daily Attendance(Punch) Report</h3>
            </div>
            <div id="content_search_panel"> 
                <form id="punch_report_form" action="javascript:fnc_daily_attendance_report()" autocomplete="off" method="POST">
                    <fieldset>
                        <table class="rpt_table"  width="1050" cellspacing="1" align="center">
                            <thead>
                                <th width="100">Category</th>
                                <th width="140">Company Name</th>				    
                                <th width="100">Location</th>
                                <th width="140">Division</th>				    
                                <th width="140">Department</th>
                                <th width="140">Section</th>				    
                                <th width="140">Subsection</th>
                                <th width="140">Designation</th>				    
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:100px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px" >
                                            <? if($company_cond=="")
                                            { 
                                            ?>
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:100px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="designation">
                                        <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="height:8px;"></div>
                        <table class="rpt_table"  width="910" cellspacing="1" align="center">
                            <thead>
                                <th width="80">Date</th>
                                <th width="120">Off Day Status</th>
                                <th width="140">Attn Status</th>				    
                                <th width="140">Shift Policy</th>
                                <th width="100">Salary Based</th>
                                <th width="100">Group By</th>
                                <th width="100">Order By</th>
                                <th width="100">ID Card No</th>				    
                                <th width="100">System Code</th>				    
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="10" style="width:80px" /></td>
                                    <td> 
                                        <select name="cbo_offday_status" id="cbo_offday_status" class="combo_boxes" style="width:120px" onchange="load_drop_down(this.value,'change_status','td_status')" > <!--load_drop_down(this.value,'change_status','status_field')-->
                                         <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $holiday_status AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="td_status"> 
                                       <select name="cbo_status" id="cbo_status" class="combo_boxes" multiple="multiple" style="width:140px">
                                        <?php 
                                        foreach($status AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="shift_id" id="shift_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $shift_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:100px" >							
                                            <option value="">-- Select --</option>
                                            <?php 
                                            foreach( $salary_based_arr AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $groupby_arr AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $orderby_arr_punch_report AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card('../../search_employee_multiple_by_id.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" />
                                    </td>
                                    <td align="center">
                                        <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info('../../search_employee_multiple.php','Employee Information'); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                                    </td>
                                </tr>
                                <tr align="center">
                                        <td colspan="9">
                                            <input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:100px" />
                                            <input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" />
                                        </td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
            <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="data_panel2" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden; display:none;">&nbsp;aaaaa</div>
        </div>
    </body>
</html>