<?php
 
/***************************

Complted by:md ekram hossain
Date:13/03/2014

*******************************/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
		

$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement",6=>"Govt. Holiday",7=>"Week-end");

//$status=array(1=>"Present",2=>"Absent",3=>"Late",4=>"Leave",5=>"Movement",6=>"Govt. Holiday",7=>"Week-end");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        
		<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        
        <script type="text/javascript" charset="utf-8">
			
		$(document).ready(function(){
			$(".datepicker").datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
				
				
		$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id,#cbo_status").multiselect({
		//header: false, 
			selectedText: "# of # selected",
			});
			
			$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id,#cbo_status").multiselectfilter({
	
	});
		});
			

	//ajx submit starts here
	function createObject() 
	{
			var request_type;
			var browser = navigator.appName;
		if( browser == "Microsoft Internet Explorer" ) 
		{
			request_type = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		else 
		{
			request_type = new XMLHttpRequest();
		}
			return request_type;
	}
	
		var http = createObject();
		var field_array = new Array();
		
			 // Employee search by emp_code
            function openmypage_employee_info()
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_category_id').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#emp_code').val(thee_id.value);
                }
            }
			
			 // Employee search by id_card_no
            function openmypage_employee_info_id_card()
            {			
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#cbo_category_id').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }
			
	
function check_date_for_cross(){
	//alert("ekram");
	var txt_from_date	= $('#txt_from_date').val();
	var txt_to_date		= $('#txt_to_date').val();
	
	if( $('#txt_from_date').val() == "" ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#txt_from_date').focus();
		$(this).html('Please select From Date Field to search.').addClass('messageboxerror').fadeTo(900,1);
		$(this).fadeOut(5000);
		});
	}
	
	if( $('#txt_from_date').val() != "" && $('#txt_to_date').val() == "" ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#txt_to_date').focus();
		$(this).html('Please select To Date Field to search.').addClass('messageboxerror').fadeTo(900,1);
		$(this).fadeOut(5000);
		});
	}
	else
	{
		var txt_from_month=txt_from_date.split("-");
		var txt_to_month=txt_to_date.split("-");
		
		if(txt_from_month[1]!=txt_to_month[1] && txt_to_month[1]!=txt_from_month[1]){
		
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_to_date').focus();
			$(this).html('Cross month is not allowed').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
			});
		}
		
	}
}
	

function fnc_employee_attendance_report(){

	
	var error = false;
	var data = '';

	var category_id		= $('#cbo_category_id').val();
	var company_id		= $('#cbo_company_id').val();
	var location_id		= $('#cbo_location_id').val();
	var division_id		= $('#cbo_division_id').val();
	var department_id	= $('#cbo_department_id').val();
	var section_id		= $('#cbo_section_id').val();
	var subsection_id	= $('#cbo_subsection_id').val();
	var designation_id	= $('#cbo_designation_id').val();
	var cbo_status		= $('#cbo_status').val();
	var txt_from_date	= $('#txt_from_date').val();
	var txt_to_date		= $('#txt_to_date').val();
	var salary_based= $('#salary_based').val();
	var group_by_id		= $('#group_by_id').val();
	var order_by_id		= $('#order_by_id').val();
	var id_card			= $('#id_card').val();
	var emp_code		= $('#emp_code').val();	
	
	if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( $('#cbo_status').val() == 0 || $('#cbo_status').val() == "" || $('#cbo_status').val() == null) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#cbo_status').focus();
		$(this).html('Please select Status Field to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
		$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		if( error == false ) 
		{
			var data='&category_id='+category_id+
			'&company_id='+company_id+
			'&location_id='+location_id+
			'&division_id='+division_id+
			'&department_id='+department_id+
			'&section_id='+section_id+
			'&subsection_id='+subsection_id+
			'&designation_id='+designation_id+
			'&id_card='+id_card+
			'&emp_code='+emp_code+
			'&cbo_status='+cbo_status+
			'&txt_from_date='+txt_from_date+
			'&txt_to_date='+txt_to_date+
			'&salary_based='+salary_based+
			'&group_by_id='+group_by_id+
			'&order_by_id='+order_by_id;
			

		//alert(data);
		nocache = Math.random();
		http.open( 'GET', 'includes/generate_employee_attendance_info.php?action=attendace_information' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_daily_att_report;
		http.send(null);
		}
	}

		//attendance Report Response	
		function response_daily_att_report() 
		{	
			if(http.readyState == 4) {
				var response = http.responseText.split('####');
			//alert(response[1]);		 	
			 $('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/>&nbsp;&nbsp;&nbsp;</a>' ); 
			$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
			$('#data_panel2').html( response[0] );
			 hide_search_panel();
			}
		}	
			
			
			
			function new_window()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel2').innerHTML);
				d.close();
			}
			
				
			
		</script>
        
		<style type="text/css">
			#filter_panel select { width:100px; }
			#filter_panel * { font-family:verdana; font-size:11px; }
		</style>
        
    </head>
    
<body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%;">
        <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
        <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee Attendance Info</h3>
        <div id="content_search_panel"> 
            <form id="punch_report_form" action="javascript:fnc_employee_attendance_report()" autocomplete="off" method="POST">
                <fieldset>
                    <div style="border:1px solid #7F7FFF; width:1148px;">
                    <table class="rpt_table"  width="1120px" cellspacing="0" cellpadding="0" align="center" rules="all">
                        <thead>
                      		<th width="140">Category</th>
                            <th width="140">Company Name</th>				    
                            <th width="140">Location</th>
                            <th width="140">Division</th>				    
                            <th width="140">Department</th>
                            <th width="140">Section</th>				    
                            <th width="140">Subsection</th>
                            <th width="140">Designation</th>				    
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:130px;">
                                        <option value="">All Category</option>
                                        <?
                                        foreach($employee_category as $key=>$val)
                                        {
                                        ?>
                                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                        <?
                                        }
                                        ?> 
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:130px;" >
                                        <? if($company_cond=="")
                                        { 
                                        ?>
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        } 
                                        foreach( $company_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:130px;" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $location_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:130px;" multiple="multiple" >
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php foreach( $division_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:130px;" multiple="multiple" >
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php foreach( $department_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:130px;" multiple="multiple" >
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php foreach( $section_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="subsection">
                                    <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:130px;" multiple="multiple" >
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php foreach( $subsection_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="designation">
                                    <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:130px;" multiple="multiple">
                                       <!-- <option value="0">-- Select --</option>-->
                                        <?php foreach( $designation_chart AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div style="height:8px;"></div>
                    <div style="border:1px solid #7F7FFF; width:993px;">
                    <table class="rpt_table"  width="980" cellspacing="0" align="center" rules="all">
                        <thead>
                      		<th width="140" colspan="2">Date</th>
                            <th width="140">Status</th>				    
                            <th width="140">Salary Based</th>
                            <th width="140">Group By</th>
                            <th width="140">Order By</th>
                            <th width="140">ID Card No</th>				    
                            <th width="140">System Code</th>				    
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                	<input type="text" name="txt_from_date" id="txt_from_date" value="" class="datepicker" size="10" style="width:70px;" /><!--onchange="check_date_for_cross();"-->
                                </td>
                                <td>
                                	<input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" size="10" style="width:70px;" /><!--onchange="check_date_for_cross();"-->
                                </td>
                                <td> 
                                  <select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:130px;" multiple="multiple">
                                        <?php 
                                        foreach( $status AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                  <select name="salary_based" id="salary_based" class="combo_boxes" style="width:130px;" >							
                                        <option value="">-- Select --</option>
                                        <?php 
                                        foreach( $salary_based_arr AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                  <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:130px;" >							
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        foreach( $groupby_arr AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:130px;" >							
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        foreach( $orderby_arr AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                              <td>
                                    <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:130px;" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                                </td>
                                <td align="center">
                                    <input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:130px;" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div style="padding-top:10px;">
                    	<input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:100px" />&nbsp;
                        <input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" />
                    </div>
                </fieldset>
            </form>
        </div>
     <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
	<div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
	</div>
</body>
</html>