<? 
// sohel
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');

$rpt_types=array(1=>"With Percentage",2=>"Without Percentage");
$depertment_wise_orderby_arr = array(1=>"Designation Level",2=>"Department Wise",3=>"Designation Wise",4=>"Division Wise",5=>"Section Wise",6=>"Subsection Wise");


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_name ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
		<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
     
        <script type="text/javascript" charset="utf-8">
		
			$(document).ready(function(e) {
				$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
					//header: false, 
					selectedText: "# of # selected",
				});
				
				$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
				
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
			});
			
			function populate_cost_center( child, parent_id, selected_id ) 
			{
				$.ajax({
					type: "POST",
					url: "hrm_data.php",
					data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
					success: function( html ) 
					{
						$('#' + child + '_id').html( html )
					}
				});
			}
			
			function openmypage(page_link,title)
			{
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=450px,center=1,resize=0,scrolling=0','../../')
			}
			
			//ajax submit starts here
			function createObject() 
			{
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) 
				{
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} 
				else 
				{
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
			
			// fnc_daily_attn_summary_report
			function fnc_daily_attn_summary_report()
			{
				var error = false, data = '';
				
				if( $('#txt_date').val() == "" ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_date').focus();
						$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				if( $('#cbo_rpt_type').val() == 0) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_rpt_type').focus();
						$(this).html('Please select A Report Type.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else if( $('#company_id').val() == "" || $('#company_id').val() == 0) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#company_id').focus();
						$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				
				if( error == false ) 
				{
					$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
					$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
					data = '&txt_date=' + $('#txt_date').val()
						+ '&cbo_emp_category=' + $('#cbo_emp_category').val()
						+'&company_id=' + $('#company_id').val() 
						+ '&location_id=' + $('#location_id').val() 
						+ '&division_id=' + $('#division_id').val() 
						+ '&department_id=' + $('#department_id').val()
						+ '&section_id=' + $('#section_id').val() 
						+ '&subsection_id=' + $('#subsection_id').val() 
						+ '&designation_id=' + $('#designation_id').val()
						+ '&cbo_salary_based=' + $('#cbo_salary_based').val()
						+ '&group_by_id=' + $('#group_by_id').val()
						+ '&order_by_id=' + $('#order_by_id').val()
						+ '&cbo_rpt_type=' + $('#cbo_rpt_type').val();
					
					//alert(data);
					nocache = Math.random();
					http.open( 'GET', 'includes/generate_daily_attn_summary_report.php?action=daily_attn_summary_report' + data + '&nocache=' + nocache );
					http.onreadystatechange = response_fnc_daily_attn_summary_report;
					http.send(null);
				}
			}
			
			// response_fnc_daily_attn_summary_report
			function response_fnc_daily_attn_summary_report() 
			{	
				if(http.readyState == 4) 
				{
					var response = http.responseText.split('####');
					//alert(response[1]);		 	
					$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
					
					$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').append( '<input type="button" onclick="new_window_short()" value="HTML Short" name="Print" class="formbutton" style="width:100px"/>' );
					$('#data_panel').append( '<input type="button" onclick="new_window_summary()" value="HTML Summary" name="Print" class="formbutton" style="width:100px"/>' );
					
					$('#data_panel').append( '<input type="button" onclick="openmypage_email_report()" value="Mail Report" name="email" class="formbutton" style="width:100px"/><input type="hidden"  value="'+response[1]+'" name="email_link" id="email_link" style="width:100px"/>' );
					
					$('#data_panel2').html( response[0] );
					$('#data_panel3').html( response[2] );
					$('#data_panel4').html( response[3] );
					
					document.getElementById('summary_topheader').innerHTML=document.getElementById('hidden_summery').innerHTML;
					document.getElementById('hidden_summery').innerHTML="";
					
					hide_search_panel();
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				
				// 
               
			}
			
			//mail report 12.05.2014 
			function openmypage_email_report()
			{
				//alert("su..re");	
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe','../../email_report.php','Mail address window', 'width=500px,height=190px,center=1,resize=1,scrolling=0','../../')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("email_report");
					var fname=$('#email_link').val();
					var txt_date=$('#txt_date').val();
					
					if(thee_loc.value=="")
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html( 'E-mail address is blank' ).addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
					else
					{
						// alert("su..re")
						$('#messagebox').removeClass().addClass('messagebox').text('Sending....').fadeIn(1000);
						$.ajax({
							type: "POST",
							url: 'includes/generate_daily_attn_summary_report.php',
							data: 'htmldata=' + fname +'&action=mail_full_report&email_address='+thee_loc.value+'&txt_date='+txt_date,
							success: function( html ) 
							{
								if(html==2)
								{
									$("#messagebox").fadeTo( 200, 0.1, function() {
										$(this).html( 'Report was not sent.' ).addClass('messagebox_ok').fadeTo(900,1);
										$(this).fadeOut(5000);
									});
								}
								else if(html==3)
								{
									$("#messagebox").fadeTo( 200, 0.1, function() {
										$(this).html( 'Report has been sent.' ).addClass('messagebox_ok').fadeTo(900,1);
										$(this).fadeOut(5000);
									});
								}
							}
						}); 
					}
				}
			}
			
			function new_window()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel2').innerHTML);
				d.close();
			}
			
			function new_window_short()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel3').innerHTML);
				d.close();
			}
			
			function new_window_summary()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('data_panel4').innerHTML);
				d.close();
			}
        </script>
        <style type="text/css"> 
			td {overflow:hidden; }
			@media print {thead {display: table-header-group;}}
			@media print {tfoot {display: table-footer-group;}}
			.verticalText 
			{               
			writing-mode: tb-rl;
			filter: flipv fliph;
			-webkit-transform: rotate(270deg);
			-moz-transform: rotate(270deg);
			}
			
			.datahighlight {
			background-color: #ffdc87 !important;
			}
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div style="width:100%" align="center">
            <div style="height:45px;">
                <div align="center" style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
                <h3 align="left" style="top:1px;width:100%;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Daily Attendance Summary</h3>
            </div>
            <div id="content_search_panel" > 
                <form id="service_report_form" action="javascript:fnc_daily_attn_summary_report()" autocomplete="off" method="POST">
                    <fieldset>
                        <table align="center" width="1120px" cellpadding="0" cellspacing="0" class="rpt_table">
                            <thead>
                                <th width="140px"><strong>Category</strong></th>
                                <th width="140px"><strong>Company</strong></th>
                                <th width="140px"><strong>Location</strong></th>
                                <th width="140px"><strong>Division</strong></th>
                                <th width="140px"><strong>Department</strong></th>
                                <th width="140px"><strong>Section</strong></th>
                                <th width="140px"><strong>SubSection</strong></th>
                                <th width="140px"><strong>Designation</strong></th>
                            </thead>
                            <tbody>
                                <tr class="general">					
                                    <td>
                                        <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td id="company">
                                        <select name="company_id" id="company_id" class="combo_boxes" style="width:140px">
                                            <? if($company_cond=="")
                                            { 
                                            ?>
                                                <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="location">
                                        <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="division">
                                        <select name="division_id" id="division_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                            <?php 
                                            foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>					
                                    <td id="department">
                                        <select name="department_id" id="department_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                            <?php 
                                            foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="section">
                                        <select name="section_id" id="section_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                            <?php 
                                            foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                            <?php 
                                            foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="designation">
                                        <select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                            <?php 
                                            foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            }
                                             ?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style=" height:8px;"></div>
                        <table align="center" width="760" cellpadding="0" cellspacing="0" class="rpt_table">
                            <thead>
                                <th width="100px"><strong>Date</strong></th>
                                <th width="140px"><strong>Salary Based</strong></th>
                                <th width="140px"><strong>Group By</strong></th>
                                <th width="140px"><strong>Order By</strong></th>
                                <th width="140px"><strong>Report Type</strong></th> 
                                <th width="100px"><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th> 
                            </thead>
                            <tbody>
                                <tr class="general">
                                    <td><input type="text" name="txt_date" id="txt_date" value="" class="datepicker" style="width:100px"  /></td>
                                    <td>
                                        <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:140px" >							
                                            <option value="">-- Select --</option>
                                            <?php 
											foreach( $salary_based_arr AS $key=>$value )
											{ 
											?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
											} 
											?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:140px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
											foreach( $groupby_arr AS $key=>$value )
											{ 
											?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
											} 
											?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:140px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
											foreach( $depertment_wise_orderby_arr AS $key=>$value )
											{ 
											?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
											} 
											?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_rpt_type" id="cbo_rpt_type" class="combo_boxes" style="width:140px" >							
											<?php 
											foreach( $rpt_types AS $key=>$value )
											{ 
											?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
											} 
											?>
                                        </select>
                                    </td>                     
                                    <td bgcolor="#A6CAF0"><input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:100px;" /></td>
                                </tr>
                        	</tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
        </div>
        <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
        <div id="data_panel3" align="left" style="margin-top:10px; visibility:hidden; display:none;">&nbsp;</div>
        <div id="data_panel4" align="left" style="margin-top:10px; width:500px; visibility:hidden; display:none;">&nbsp;</div>
    </body>
</html>