<?php
// Sohel
// Date : 24.12.2013
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	 <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
     <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
	<script type="text/javascript" charset="utf-8">
	
		
$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
 });
				
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
	function openmypage_employee_info()
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}
function openmypage_employee_info_id_card()
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#id_card').val(thee_id.value);
			}
		}	

//ajax submit starts here

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();



function fnc_employee_job_card()
	
	{ 
		var error = false, data = '';
		
		if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_month_selector').focus();
				$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $('#cbo_company_id').val() == 0 && $('#emp_code').val() == "" ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_company_id').focus();
				$(this).html('Please select Company to search.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		if( error == false ) {
			$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			data = '&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&txt_date=' + $('#txt_date').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() +'&division_id=' + $('#division_id').val()+ '&department_id=' + $('#department_id').val()+ '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&id_card=' + $('#id_card').val() +'&emp_code=' + $('#emp_code').val() + '&cbo_salary_based=' + $('#cbo_salary_based').val()+ '&designation_id=' + $('#designation_id').val();
			
			//alert (data);
			nocache = Math.random();
			http.open( 'GET', 'includes/generate_emp_ofday_job_card.php?action=emp_ofday_job_card_report' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_fnc_emp_offday_job_card_report;
			http.send(null);
		}
	}
	
	
	function response_fnc_emp_offday_job_card_report(){	//Punch Report Response 
		if(http.readyState == 4) {
			var response = http.responseText.split('####');
			// alert(response[1]);
			$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
			$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
			 $('#data_panel2').html( response[0] );
			  hide_search_panel();
			 $("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Job Card Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}


function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel2').innerHTML);
	d.close();
}

	
	 function populate_select_month( selected_id ) 
            {
                if(selected_id!=0)
                { 
                    $("#txt_date").val('').attr("disabled", "disabled"); 
                }
                else
                { 
                    $("#txt_date").attr("disabled", false);
                }
                //else
                $.ajax({
                    type: "GET",
                    url: "includes/generate_emp_ofday_job_card.php?",
                    data: 'type=select_month_generate&id=' + selected_id,
                    success: function( html ) {
                        $('#cbo_month_selector').html( html )
                    }
                });
            }
		
	</script>
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:100%;">
	<div align="center">
		<div align="center" id="messagebox"></div>
	</div>
     <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee Job Card</h3>
    <div id="content_search_panel" > 
	<form id="punch_report_form" action="javascript:fnc_employee_job_card()" autocomplete="off" method="POST">
		<fieldset id="filter_panel" style="top:1px;" >
			<div style="padding:10px;"> 
			<table width="1000" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
				<thead>
                    
                    <th><strong>Category</strong></th>
					<th><strong>Company</strong></th>
					<th><strong>Location</strong></th>
                    <th><strong>Division</strong></th>
					<th><strong>Department</strong></th>
                    <th><strong>Section</strong></th>
                     <th><strong>SubSection</strong></th>
                    <th><strong>Designation</strong></th>
				</thead>
                <tr class="general">
					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:120px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:130px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected";?> ><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
       			  <td>
					<select name="division_id" id="division_id" class="combo_boxes" style="width:120px" multiple="multiple">
                            <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px" multiple="multiple">
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:120px" multiple="multiple">
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                     <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px" multiple="multiple">
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>    
                     <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" multiple="multiple" >
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                </tr>
           </table>
           <div style="height:8px;"></div>
           <table align="center" width="900" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
				<thead>
                    <th><strong>Year</strong></th>
                   <th><strong>Month</strong></th>
                    <th><strong>Date</strong></th>
                    <th><strong>Salary Based</strong></th>
					<th><strong>ID Card No</strong></th>
                    <th><strong>System Code</strong></th>
                     <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>		
				</thead>
                <tr class="general">
                   <td>
                                    <select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" style="width:100px;" > 
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        foreach( $policy_year AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes" style="width:120px;">							
                                         <option value="0">-- Select --</option>
                                    </select>
                                </td>
					<td>
						 <input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="12" />
					</td>
                    <td>
                    	<select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:120px" >							
                           <option value="">-- Select --</option>
                            <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:150px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                    </td>
                	<td align="center"><input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:150px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" />
                    </td> 
					<td>
                    	<input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:100px" />&nbsp;
                    </td>
				</tr>
            </table>
            </div>
		</fieldset>
	</form>
    </div>
</div>
<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>           
<div id="data_panel2" align="left" class="demo_jui2" style="margin-top:10px;"></div>

	<!--<div id="data_panel" align="center" style="margin-top:10px;"></div>
	<div id="data_panel2" align="center" style="margin-top:10px;"></div>-->
	
</body>
</html>