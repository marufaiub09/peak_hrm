<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );



//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$start_1 = substr($row["starting_date"],0,3);
		$start_2 = substr($row["starting_date"],-1,1);

		$end_1 = substr($row["ending_date"],0,3);
		$end_2 = substr($row["ending_date"],-2,2);
		
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$start_1." ".$start_2." - ".$end_1." ".$end_2."</option>";
		//echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}



//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}



	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	//$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 order by trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}	
	
if($action=='monthly_extra_ot_sheet'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	//echo $qr;die;
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}
	
	$date_range = explode("_",$cbo_month_selector);
	//$date_part = $date_range[0];
	//echo $con_date."==";
	//echo $date_part;die; 
	if ($cbo_salary_sheet=='') {$emp_status="";} 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1";} 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date'
	else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else	$department_id="and a.department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	
	//if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($cbo_emp_category=='') $category ="and emp.category in ($employee_category_index)"; else $category ="and emp.category='$cbo_emp_category'";
	
	if ($ot_type_id==0 || $ot_type_id==1) $ot_type =""; else $ot_type ="and a.total_over_time!=0.00";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and emp.salary_type_entitled='$cbo_salary_based'";
	
	
	$addrs ="";		
	if($company_details[$company_id]['plot_no'] !='') $addrs .= $company_details[$company_id]['plot_no'].", "; 
	if($company_details[$company_id]['level_no'] !='') $addrs .= $company_details[$company_id]['level_no'].", "; 
	if($company_details[$company_id]['block_no'] !='') $addrs .= $company_details[$company_id]['block_no'].", "; 
	if($company_details[$company_id]['road_no'] !='') $addrs .= $company_details[$company_id]['road_no'].", "; 
	if($company_details[$company_id]['city'] !='') $addrs .= $company_details[$company_id]['city'].", "; 
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,emp.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,emp.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,emp.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,emp.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,emp.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,emp.emp_code,";}
	
	/*$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="emp.company_id,emp.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="emp.company_id,emp.location_id,emp.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.subsection_id,emp.emp_code,";}*/
	
	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-13)." CAST(right(trim(emp.id_card_no), 5)as SIGNED)";}	
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by	
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	if($group_by_id==0){ $header_field="Company"; }
	else if($group_by_id==1){$header_field="Company"; }
	else if($group_by_id==2){$header_field="Location"; }
	else if($group_by_id==3){$header_field="Division"; }
	else if($group_by_id==4){$header_field="Department"; }
	else if($group_by_id==5){$header_field="Section"; }
	else if($group_by_id==6){$header_field="SubSection"; }

ob_start();	
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:900px">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1"><? echo $addrs; ?></font><br />
        <font size="-1">Extra OT Sheet: <? echo date("F, Y",strtotime($date_range[0])); ?> </font>
    </div>
    <table cellpadding="0" cellspacing="0" width="900" class="rpt_table" style="font-size:12px; border:1px solid #000000" rules="all"> 
    	 <thead>
                    <tr class="tbl_top">
                    <th colspan="14"><? echo $cost_center; ?></th>
                    </tr>
                    <tr class="tbl_top">
                    <th colspan="6">General Information</th>
                    <th colspan="7">Extra Over Time</th> 
                    <th rowspan="2" width="100">Signature</th>                           
                    </tr>
                    <tr class="tbl_top">
                    <th width="30"><b>SL</b></th>
                    <th width="70"><b>ID Card</b></th>
                    <th width="70"><b>Emp Code</b></th>
                    <th align="100"><b>Name</b></th>
                    <th align="100"><b>Designation</b></th>
                    <th align="80"><b>DOJ</b></th>
                    <th align="60"><b>Gross</b></th>
                    <th align="60"><b>Basic</b></th>
                    <th align="60"><b>OT Hrs</b></th>
                    <th align="60"><b>EOT Hrs</b></th>
                    <th align="60"><b>Total OT</b></th>
                    <th align="60"><b>OT Rate</b></th>
                    <th align="60"><b>OT Amt</b></th>
                    </tr>
        </thead>
    <?
	$sam=0;
	$samary .='<table cellpadding="0" cellspacing="0" width="600" class="rpt_table" style="font-size:12px; border:1px solid #000000" rules="all">
        <thead>
            <tr class="tbl_top">
				<th width="30"><b>SL</b></th>
				<th width="150"><b>'.$header_field.'</b></th>
				<th width="80"><b>OT</b></th>
				<th align="80"><b>E OT</b></th>
				<th align="80"><b>Total OT</b></th>
				<th align="80"><b>Amount</b></th>
				<th align="80"><b>Total Emp</b></th>
				<th align="80"><b>Average</b></th>
            </tr>
        </thead>';
	
	
		$total_bot=0;$subsec_total_bot=0;$sec_total_bot=0;$deper_total_bot=0;$divi_total_bot=0; $location_total_bot=0;
		$total_eot=0;$subsec_total_eot=0;$sec_total_eot=0;$deper_total_eot=0;$divi_total_eot=0; $location_total_eot=0;
		$total_ot=0;$subsec_total_ot=0;$sec_total_ot=0;$deper_total_ot=0;$divi_total_ot=0; $location_total_ot=0;
		
		$total_eot_amount=0;$subsec_total_eot_amount=0;$sec_total_eot_amount=0;$deper_total_eot_amount=0;$divi_total_eot_amount=0;$location_total_eot_amount=0;  
		
		$location_eot=0; $divi_eot=0; $deper_eot=0; $sec_eot=0; $subsec_eot=0;  			
		
		$sub_time=0; $sec_time=0; $dep_time=0; $div_time=0; $loc_time=0;	
				
		//and a.total_over_time!='0.00'
		if($cbo_salary_based==1)//Piece Rate Based (and emp.salary_grade=4)
		{
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no,emp.joining_date,emp.designation_level,emp.punch_card_no,emp.salary_grade,a.* 
				FROM 
				hrm_production_salary_mst a, hrm_employee emp 
				WHERE 
				emp.emp_code!='' and
				a.salary_periods  between '$date_range[0]' and '$date_range[1]'  and 
				a.emp_code=emp.emp_code
				$cbo_company_id  
				$division_id
				$department_id 
				$location_id 
				$section_id 				
				$subsection_id 
				$designation_id 
				$category
				$emp_status
				$ot_type
				$salary_based
				group by 
				$dynamic_groupby
				$orderby";
		}
		else//Monthly Salary Based
		{
			$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no,emp.joining_date,emp.designation_level,emp.punch_card_no,a.* 
				FROM 
				hrm_salary_mst a, hrm_employee emp 
				WHERE 
				emp.emp_code!='' and
				a.salary_periods  between '$date_range[0]' and '$date_range[1]' and 
				a.emp_code=emp.emp_code
				$cbo_company_id  
				$division_id
				$department_id 
				$location_id 
				$section_id 				
				$subsection_id 
				$designation_id 
				$category
				$emp_status
				$ot_type
				$salary_based
				group by 
				$dynamic_groupby
				$orderby";
		}
		
		//echo $sql;die;
		//order by emp.section_id, CAST(emp.id_card_no as SIGNED)";
		
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$total_emp=0;
		$subsec_total_emp=0;
		$sec_total_emp=0;
		$deper_total_emp=0;
		$divi_total_emp=0;
		$location_total_emp=0;
		$sl=0;
		$r=1;
		while($row=mysql_fetch_array($result))  //while loop start here================================
		{
			 
			 if($cbo_salary_based==1){ $row[over_time_rate]=return_field_value("rate","hrm_piece_rate_ot_mst","grade_id=$row[salary_grade]");}
			 else
			 {$row['over_time_rate']=$row['over_time_rate'];}
			 
			 
			 $sl++;
			 if($sl==1)
			   {
					$location_arr[$row[location_id]]=$row[location_id];
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
			   }
			   
			   if($sl!=1)
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row[subsection_id]]=$row[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row[section_id]]=$row[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row[subsection_id]]=$row[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row[department_id]]=$row[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row[section_id]]=$row[section_id];
										$subsection_arr[$row[subsection_id]]=$row[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row[division_id]]=$row[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row[department_id]]=$row[department_id];
									$section_arr[$row[section_id]]=$row[section_id];
									$subsection_arr[$row[subsection_id]]=$row[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row[location_id]]=$row[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
				}
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
			if($group_by_id==6)
			{
				$sam++;
				$subsec_average=round(($subsec_total_eot_amount/$subsec_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'. $header_val. '</td>
					<td align="right">'. $subsec_total_bot .'</td>
					<td align="right">'. $subsec_total_eot .'</td>
					<td align="right">'. $subsec_total_ot .'</td>
					<td align="right">'.$subsec_total_eot_amount.'</td>
					<td align="right">'.$subsec_total_emp.'</td>
					<td align="right">'.$subsec_average.'</td>
				</tr>';
			}
 			?>
                <tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="8" width='left'><b> Subsection Total</b></td>
                    <td><? echo $subsec_total_bot; ?>;</td>
                    <td><? echo $subsec_total_eot; ?></td>
                    <td><? echo $subsec_total_ot; ?></td>	
                    <td>&nbsp;</td>
                    <td><? echo $subsec_total_eot_amount; ?></td>
                    <td>&nbsp;</td>
                </tr>
			<?
			 $subsec_total_eot=0; 
			 $subsec_total_eot_amount=0;
			 $subsec_eot=0;
			 $subsec_total_bot=0;
			 $subsec_bot=0;
			 $subsec_total_ot=0;
			 $subsec_ot=0;
			 $subsec_total_emp=0;
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
			if($group_by_id==5)
			{
				$sam++;
				$sec_average=round(($sec_total_eot_amount/$sec_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'.$header_val.'</td>
					<td align="right">'.$sec_total_bot.'</td>
					<td align="right">'.$sec_total_eot.'</td>
					<td align="right">'.$sec_total_ot.'</td>
					<td align="right">'.$sec_total_eot_amount.'</td>
					<td align="right">'.$sec_total_emp.'</td>
					<td align="right">'.$sec_average.'</td>
				</tr>';
			}
 			?>
                <tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="8" width='left'><b>SectionTotal</b></td>
                    <td><? echo $sec_total_bot; ?></td>
                     <td><? echo $sec_total_eot; ?></td> 
                     <td><? echo $sec_total_ot; ?></td>
                    <td>&nbsp;</td>
                    <td><? echo $sec_total_eot_amount; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
			$sec_total_eot=0;
			$sec_total_eot_amount=0; 
			$sec_eot=0;
			$sec_total_bot=0;
			$sec_bot=0;
			$sec_total_ot=0;
			$sec_ot=0;
			
			$sec_total_emp=0;
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
			if($group_by_id==4)
			{
				$sam++;
				$deper_average=round(($deper_total_eot_amount/$deper_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'. $header_val. '</td>
					<td align="right">'. $deper_total_bot .'</td>
					<td align="right">'. $deper_total_eot .'</td>
					<td align="right">'. $deper_total_ot .'</td>
					<td align="right">'.$deper_total_eot_amount.'</td>
					<td align="right">'.$deper_total_emp.'</td>
					<td align="right">'.$deper_average.'</td>
				</tr>';
			}
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8" align="left">Department Total</td>
                <td><? echo $deper_total_bot; ?></td>
                <td><? echo $deper_total_eot; ?></td>
                <td><? echo $deper_total_ot; ?></td>		
                <td>&nbsp;</td>
                <td><? echo $deper_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<? 
			$deper_total_eot=0; 
			$deper_total_eot_amount=0; 
			$deper_eot=0; 
			$deper_total_bot=0;
			$deper_bot=0; 
			$deper_total_ot=0;
			$deper_ot=0;
			$deper_total_emp=0;
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
			if($group_by_id==3)
			{
				$sam++;
				$divi_average=round(($divi_total_eot_amount/$divi_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'.$header_val.'</td>
					<td align="right">'.$divi_total_bot.'</td>
					<td align="right">'.$divi_total_eot.'</td>
					<td align="right">'.$divi_total_ot.'</td>
					<td align="right">'.$divi_total_eot_amount.'</td>
					<td align="right">'.$divi_total_emp.'</td>
					<td align="right">'.$divi_average.'</td>
				</tr>';
			}
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8" align="left" >Division Total</td>
               <td><? echo $divi_total_bot; ?></td> 
                <td><? echo $divi_total_eot; ?></td> 
                <td><? echo $divi_total_ot; ?></td>	
                <td>&nbsp;</td>
                <td><? echo $divi_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<? 
			$divi_total_eot=0;
			$divi_total_eot_amount=0; 
			$divi_eot=0;
			$divi_total_bot=0;
			$divi_bot=0;
			$divi_total_ot=0;
			$divi_ot=0;
			$divi_total_emp=0;
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
			if($group_by_id==2)
			{
				$sam++;
				$location_average=round(($location_total_eot_amount/$location_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'.$header_val.'</td>
					<td align="right">'.$location_total_bot.'</td>
					<td align="right">'.$location_total_eot.'</td>
					<td align="right">'.$location_total_ot.'</td>
					<td align="right">'.$location_total_eot_amount.'</td>
					<td align="right">'.$location_total_emp.'</td>
					<td align="right">'.$location_average.'</td>
				</tr>';
			}
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8" align="left">Location Total</td>
                <td><? echo $location_total_bot; ?></td>
                <td><? echo $location_total_eot; ?></td>
                <td><? echo $location_total_ot; ?></td>	
                <td>&nbsp;</td>
                <td><? echo $location_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<? 
			$location_total_eot=0;
			$location_total_eot_amount=0; 
			$location_eot=0;
			$location_total_bot=0;
			$location_bot=0;
			$location_total_ot=0;
			$location_ot=0;
			$location_total_emp=0;
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row[location_id]]."  ,"; $header_val =$location_details[$row[location_id]];
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row[division_id]]."  ,"; $header_val=$division_details[$row[division_id]];
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row[department_id]]."  ,";$header_val=$department_details[$row[department_id]];
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row[section_id]]."  ,";$header_val=$section_details[$row[section_id]]; 
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row[subsection_id]]."  ,";$header_val=$subsection_details[$row[subsection_id]];
			}
		

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr><td colspan="15"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
				//$header_value=$header_val;
			}		
		 
 		}
		   
		$exploded_over_time = explode(".", $row['total_over_time']);
		$tot_over_time_min=floor($exploded_over_time[0]*60);
		$total_over_time_min=$tot_over_time_min+$exploded_over_time[1];
		
		$tot_over_time_rate_min=($row['over_time_rate']/60);
		
		$eot_amount=($total_over_time_min*$tot_over_time_rate_min);
		
		if($r%2==0) $bgcolor='#FFFFFF'; else $bgcolor='#EEEEEE';
				
			?>    
				<tr bgcolor="<? echo $bgcolor; ?>" height="50">
					<td><? echo $r; ?></td>
					<td><? echo $row['id_card_no']; ?></td>
					<td><? echo $row['emp_code']; ?></td>
					<td><? echo $row['name']; ?></td>
					<td><? echo $designation_chart[$row['designation_id']]; ?></td>
					<td><? echo convert_to_mysql_date($row['joining_date']); ?></td>
					<td><? echo $row['gross_salary']; ?></td>
					<td><? echo $row['basic_salary']; ?></td>
                    <td><? echo $row['b_over_time']; ?></td>
					<td><? echo $row['e_over_time']; ?></td>
                    <td><? echo $row['total_over_time']; ?></td> 	
					<td><? echo $row['over_time_rate']; ?></td>
					<td><? echo round($eot_amount);//$eot_amount=round($row['e_over_time']*$row['over_time_rate']); ?></td>
					<td>&nbsp;</td>               
				</tr>
			<?
			$exploded_var = explode(".", $row['e_over_time']);
			$new_tot=floor($exploded_var[0]*60);$new_tot_min=$new_tot+$exploded_var[1]; 
			$eot += $new_tot_min;
			$total_eot = sprintf("%d.%02d", abs((int)($eot/60)), abs((int)($eot%60)));
			$total_eot_amount+=$eot_amount;
			
			$exploded_b_over = explode(".", $row['b_over_time']);
			$new_bot=floor($exploded_b_over[0]*60);$new_bot_min=$new_bot+$exploded_b_over[1]; 
			$bot += $new_bot_min;
			$total_bot = sprintf("%d.%02d", abs((int)($bot/60)), abs((int)($bot%60)));
			
			$exploded_tot_over_time = explode(".", $row['total_over_time']);
			$new_tot_over_time=floor($exploded_tot_over_time[0]*60);$new_tot_over_time=$new_tot_over_time+$exploded_tot_over_time[1]; 
			$tot_ot += $new_tot_over_time;
			$total_ot = sprintf("%d.%02d", abs((int)($tot_ot/60)), abs((int)($tot_ot%60)));
			$total_emp++;
			
			//$subsec_total_eot+=$row['e_over_time'];
			$sub_time=$row['e_over_time'];
			$subsec_exploded_var = explode(".",$sub_time);
			$subsec_new_tot=floor($subsec_exploded_var[0]*60);$subsec_new_tot_min=($subsec_new_tot+$subsec_exploded_var[1]); 
			$subsec_eot += $subsec_new_tot_min;
			$subsec_total_eot = sprintf("%d.%02d", abs((int)($subsec_eot/60)), abs((int)($subsec_eot%60)));
			$subsec_total_eot_amount+=$eot_amount;
			
			$sub_btime = explode(".", $row['b_over_time']);
			$subsec_new_btot=floor($sub_btime[0]*60);$subsec_new_btot_min=$subsec_new_btot+$sub_btime[1]; 
			$subsec_bot += $subsec_new_btot_min;
			$subsec_total_bot = sprintf("%d.%02d", abs((int)($subsec_bot/60)), abs((int)($subsec_bot%60)));
			
			$sub_tot_time = explode(".", $row['total_over_time']);
			$subsec_tot_min=floor($sub_tot_time[0]*60);$subsec_new_tot_min=$subsec_tot_min+$sub_tot_time[1]; 
			$subsec_ot += $subsec_new_tot_min;
			$subsec_total_ot = sprintf("%d.%02d", abs((int)($subsec_ot/60)), abs((int)($subsec_ot%60)));
			$subsec_total_emp++;
			
			//$sec_total_eot+=$row['e_over_time'];
			$sec_time=$row['e_over_time'];
			$sec_exploded_var = explode(".",$sec_time);
			$sec_new_tot=floor($sec_exploded_var[0]*60);$sec_new_tot_min=($sec_new_tot+$sec_exploded_var[1]);
			$sec_eot += $sec_new_tot_min;
			$sec_total_eot = sprintf("%d.%02d", abs((int)($sec_eot/60)), abs((int)($sec_eot%60)));
			$sec_total_eot_amount+=$eot_amount;
			
			$sec_btime = explode(".", $row['b_over_time']);
			$sec_new_btot=floor($sec_btime[0]*60);$sec_new_btot_min=$sec_new_btot+$sec_btime[1]; 
			$sec_bot += $sec_new_btot_min;
			$sec_total_bot = sprintf("%d.%02d", abs((int)($sec_bot/60)), abs((int)($sec_bot%60)));
			
			$sec_exp_time = explode(".", $row['total_over_time']);
			$sec_tot_min=floor($sec_exp_time[0]*60);$sec_total_min=$sec_tot_min+$sec_exp_time[1]; 
			$sec_ot += $sec_total_min;
			$sec_total_ot = sprintf("%d.%02d", abs((int)($sec_ot/60)), abs((int)($sec_ot%60)));
			$sec_total_emp++;
			
			//$deper_total_eot+=$row['e_over_time'];
			$dep_time=$row['e_over_time'];
			$deper_exploded_var = explode(".", $dep_time);
			$deper_new_tot=floor($deper_exploded_var[0]*60);$deper_new_tot_min=($deper_new_tot+$deper_exploded_var[1]);
			$deper_eot += $deper_new_tot_min;
			$deper_total_eot = sprintf("%d.%02d", abs((int)($deper_eot/60)), abs((int)($deper_eot%60)));
			$deper_total_eot_amount+=$eot_amount;
			
			$deper_btime = explode(".", $row['b_over_time']);
			$deper_new_btot=floor($deper_btime[0]*60);$deper_new_btot_min=$deper_new_btot+$deper_btime[1]; 
			$deper_bot += $deper_new_btot_min;
			$deper_total_bot = sprintf("%d.%02d", abs((int)($deper_bot/60)), abs((int)($deper_bot%60)));
			
			$deper_exp_time = explode(".", $row['total_over_time']);
			$deper_tot_min=floor($deper_exp_time[0]*60);$deper_total_min=$deper_tot_min+$deper_exp_time[1]; 
			$deper_ot += $deper_total_min;
			$deper_total_ot = sprintf("%d.%02d", abs((int)($deper_ot/60)), abs((int)($deper_ot%60)));
			$deper_total_emp++;
			
			//$divi_total_eot+=$row['e_over_time'];
			$div_time=$row['e_over_time'];
			$divi_exploded_var = explode(".",$div_time);
			$divi_new_tot=floor($divi_exploded_var[0]*60);$divi_new_tot_min=($divi_new_tot+$divi_exploded_var[1]);
			$divi_eot += $divi_new_tot_min;
			$divi_total_eot = sprintf("%d.%02d", abs((int)($divi_eot/60)), abs((int)($divi_eot%60)));
			$divi_total_eot_amount+=$eot_amount;
			
			$divi_btime = explode(".", $row['b_over_time']);
			$divi_new_btot=floor($divi_btime[0]*60);$divi_new_btot_min=$divi_new_btot+$divi_btime[1]; 
			$divi_bot += $divi_new_btot_min;
			$divi_total_bot = sprintf("%d.%02d", abs((int)($divi_bot/60)), abs((int)($divi_bot%60)));
			
			$divi_exp_time = explode(".", $row['total_over_time']);
			$divi_tot_min=floor($divi_exp_time[0]*60);$divi_total_min=$divi_tot_min+$divi_exp_time[1]; 
			$divi_ot += $divi_total_min;
			$divi_total_ot = sprintf("%d.%02d", abs((int)($divi_ot/60)), abs((int)($divi_ot%60)));
			$divi_total_emp++;
			
			//$location_total_eot+=$row['e_over_time'];
			$loc_time=$row['e_over_time'];
			$location_exploded_var = explode(".", $loc_time);
			$location_new_tot=floor($location_exploded_var[0]*60);$location_new_tot_min=($location_new_tot+$location_exploded_var[1]);
			$location_eot += $location_new_tot_min;
			$location_total_eot = sprintf("%d.%02d", abs((int)($location_eot/60)), abs((int)($location_eot%60)));
			$location_total_eot_amount+=$eot_amount;
			
			$location_btime = explode(".", $row['b_over_time']);
			$location_new_btot=floor($location_btime[0]*60);$location_new_btot_min=$location_new_btot+$location_btime[1]; 
			$location_bot += $location_new_btot_min;
			$location_total_bot = sprintf("%d.%02d", abs((int)($location_bot/60)), abs((int)($location_bot%60)));
			
			$location_exp_time = explode(".", $row['total_over_time']);
			$location_tot_min=floor($location_exp_time[0]*60);$location_total_min=$location_tot_min+$location_exp_time[1]; 
			$location_ot += $location_total_min;
			$location_total_ot = sprintf("%d.%02d", abs((int)($location_ot/60)), abs((int)($location_ot%60)));
			$location_total_emp++;
			
		$r++;
		} //while loop end here===========================================================================
		
		
		if($status_subsec==1)
		{
			if($group_by_id==6)
			{
				$sam++;
				$subsec_average=round(($subsec_total_eot_amount/$subsec_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'. $header_val. '></td>
					<td align="right">'. $subsec_total_bot .'</td>
					<td align="right">'. $subsec_total_eot .'</td>
					<td align="right">'. $subsec_total_ot .'</td>
					<td align="right">'.$subsec_total_eot_amount.'</td>
					<td align="right">'.$subsec_total_emp.'</td>
					<td align="right">'.$subsec_average.'</td>
				</tr>';
			}
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8"><b>Subsection Total</b></td>
                <td><? echo $subsec_total_bot; ?></td> 
                <td><? echo $subsec_total_eot; ?></td> 
                <td><? echo $subsec_total_ot; ?></td>	
                <td>&nbsp;</td>
                <td><? echo $subsec_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<?
		} 
		
		if($status_sec==1)
		{
			if($group_by_id==5)
			{
				$sam++;
				$sec_average=round(($sec_total_eot_amount/$sec_total_emp),2);
					$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'.$header_val.'</td>
					<td align="right">'.$sec_total_bot .'</td>
					<td align="right">'.$sec_total_eot .'</td>
					<td align="right">'.$sec_total_ot .'</td>
					<td align="right">'.$sec_total_eot_amount.'</td>
					<td align="right">'.$sec_total_emp.'</td>
					<td align="right">'.$sec_average.'</td>
				</tr>';
			}
			
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8">Section Total</td>
                 <td><? echo $sec_total_bot; ?></td> 
                <td><? echo $sec_total_eot; ?></td> 
                <td><? echo $sec_total_ot; ?></td> 
                <td>&nbsp;</td>
                <td><? echo $sec_total_eot_amount; ?></td>
                <td>&nbsp;</td>			
			</tr>
			<?
		}
		
		if($status_dept==1 )
		{
			if($group_by_id==4)
			{
				$sam++;
				$deper_average=round(($deper_total_eot_amount/$deper_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'. $header_val.'</td>
					<td align="right">'.$deper_total_bot.'</td>
					<td align="right">'.$deper_total_eot.'</td>
					<td align="right">'.$deper_total_ot.'</td>
					<td align="right">'.$deper_total_eot_amount.'</td>
					<td align="right">'.$deper_total_emp.'</td>
					<td align="right">'.$deper_average.'</td>
				</tr>';
			}
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8">Department Total</td>
                <td><? echo $deper_total_bot; ?></td>
                <td><? echo $deper_total_eot; ?></td>
                <td><? echo $deper_total_ot; ?></td>
                <td>&nbsp;</td>
                <td><? echo $deper_total_eot_amount; ?></td>
                <td>&nbsp;</td>		
			</tr>
			<?
		}
		 
		if($status_divis==1 )
		{
			if($group_by_id==3)
			{
				$sam++;
				$divi_average=round(($divi_total_eot_amount/$divi_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'. $header_val. '</td>
					<td align="right">'. $divi_total_bot .'</td>
					<td align="right">'. $divi_total_eot .'</td>
					<td align="right">'. $divi_total_ot .'</td>
					<td align="right">'.$divi_total_eot_amount.'</td>
					<td align="right">'.$divi_total_emp.'</td>
					<td align="right">'.$divi_average.'</td>
				</tr>';
			}
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td  colspan="8">Division Total</td>
                <td><? echo $divi_total_bot; ?></td>
                <td><? echo $divi_total_eot; ?></td>
                <td><? echo $divi_total_ot; ?></td>	
                <td>&nbsp;</td>
                <td><? echo $divi_total_eot_amount; ?></td>
                <td>&nbsp;</td>				
			</tr>
			<?
		}
		 
		if($status_loc==1 )
		{
			if($group_by_id==2)
			{
				$sam++;
				$location_average=round(($location_total_eot_amount/$location_total_emp),2);
				$samary .='<tr>
					<td>'.$sam.'</td>
					<td>'.$header_val.'</td>
					<td align="right">'.$location_total_bot.'</td>
					<td align="right">'.$location_total_eot.'</td>
					<td align="right">'.$location_total_ot.'</td>
					<td align="right">'.$location_total_eot_amount.'</td>
					<td align="right">'.$location_total_emp.'</td>
					<td align="right">'.$location_average.'</td>
				</tr>';
			}
			
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td  colspan="8">Location Total</td>
                <td><? echo $location_total_bot; ?></td> 
                <td><? echo $location_total_eot; ?></td> 
                <td><? echo $location_total_ot; ?></td> 
                <td>&nbsp;</td>
                <td><? echo $location_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<?
		}
			$samary .='<tr>
				<td colspan="2">GRAND TOTAL</td>
				<td align="right">'. $total_bot .'</td>
				<td align="right">'. $total_eot .'</td>
				<td align="right">'. $total_ot .'</td>
				<td align="right">'.$total_eot_amount.'</td>
				<td align="right">'.$total_emp.'</td>
				<td align="right"></td>
			</tr>';
			?>
			<tr style="font-weight:bold" bgcolor="#CCCCCC">
				<td colspan="8">GRAND TOTAL</td>
				<td><? echo $total_bot; ?></td>
				<td><? echo $total_eot; ?></td>
				<td><? echo $total_ot; ?></td>	
				<td>&nbsp;</td>
				<td align="right"><? echo $total_eot_amount; ?></td>
				<td>&nbsp;</td>               
			</tr>
    </table>
    <br /><br />
  <? 
  $samary .='</table>';
  ?>
	<?
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	//echo "$html"."####"."$name";
	echo "$html"."####"."$name"."####".$samary;		
	exit();
	//$samary
}


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>