<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');



$e_date = time();
$user_only = $_SESSION["user_name"];
//extract( $_GET );
extract( $_REQUEST );

//emp_basic
	 $sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$emp_basic = array();

	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['id']] = mysql_real_escape_string($row['company_name']);
	}	
//print_r($emp_basic);
	
//company details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}
//print_r($company_details);	
	
//location details
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	
//division details
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}


//department details
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}



//section deatils
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	


//sub section details	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	
	
//designation details
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


//Overtime policy details
	$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$overtime_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$overtime_details[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}
//print_r($overtime_details);

	//Holiday policy details
	$sql = "SELECT * FROM lib_policy_holiday_incentive WHERE is_deleted = 0  ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday_policy_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$holiday_policy_details[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}
	
//print_r($holiday_policy_details);	
	
//duty roster policy
$sql = "SELECT distinct(roster_id) as id,roster_rule_name FROM lib_duty_roster_policy WHERE is_deleted = 0 and status_active=1 ORDER BY roster_rule_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$roster_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
		$roster_policy[$row['id']] = mysql_real_escape_string( $row['roster_rule_name'] );
}
	
/*//Leave policy details previous code
	$sql = "SELECT * FROM lib_policy_leave_definition WHERE is_deleted = 0 and status_active=1 ORDER BY leave_type ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$leave_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$leave_policy[$row['policy_id']] = mysql_real_escape_string( $row['leave_type'] );		
	}*/


//Leave policy details 
	$sql = "SELECT * FROM lib_policy_leave WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name  ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$leave_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$leave_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}

//print_r($leave_policy);


//Meternity Leave policy details
	$sql = "SELECT * FROM lib_policy_maternity_leave WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$meternity_leave_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$meternity_leave_details[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}

//print_r($meternity_leave_details);	


//Attendance Bonus policy details
	$sql = "SELECT * FROM lib_policy_attendance_bonus WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$attendance_bonus_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$attendance_bonus_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}
	
//Absent Deduction policy details
	$sql = "SELECT * FROM lib_policy_absent_deduction WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$absent_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$absent_deduction_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}
	
//Late Deduction policy details
	$sql = "SELECT * FROM lib_policy_late_deduction WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$late_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$late_deduction_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}
	
//Bonus policy details
	$sql = "SELECT * FROM lib_bonus_policy WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$bonus_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$bonus_policy[$row['id']] = mysql_real_escape_string( $row['policy_name'] );		
	}
	
//Tax policy details
	$sql = "SELECT * FROM  lib_tax_master WHERE is_deleted = 0 and status_active=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$tax_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$tax_details[$row['id']] = mysql_real_escape_string( $row['rule_name'] );		
	}

	
//print_r($tax_details);	


//Shift policy details
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_policy_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_policy_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
//print_r($shift_policy_details);

/*
overtime_policy
$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 and status_active=1 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$overtime_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$overtime_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}	
}
*/

 if($action=='employee_policy_info_report'){ 
	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
	//echo $id_card_no;die;
	
	//echo $cbo_company_id;
	$company_id=$cbo_company_id;	
	//if ($emp_code=='') $emp_code=""; else $emp_code="and emp_code in($emp_code)";
	//if ($id_card=='') $id_card_no=""; else $id_card_no="and id_card_no in($id_card_no)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
	/*if ($cbo_company_id==0) $company_id=""; else*/ $company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id in ($division_id)";
	if ($section_id==0) $section_id=""; else $section_id="and section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in ($designation_id)";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else	$department_id=" and department_id in ($department_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";	
	//echo $emp_code;
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(id_card_no, 5)";}
	$i=1;			
	ob_start();	
	
	?>  
	<table cellpadding="0" cellspacing="1" border="1" width="1000" align="center"  class="rpt_table"  rules="all"> 
       <thead>
       	<tr>
       		<th colspan="9" align="center" bordercolor="#FFFFFF"><font size="+1"><? echo  $company_details[$cbo_company_id];?></font><br />       		  
       		  Employee Policy Information List</th>
       </tr>
       
         <tr>
             <th width="40" align="center"><b>SL</b></th>
             <th width="100" align="center"><b>ID Card</b></th>
             <th width="100" align="center"><b>Emp Code</b></th>
             <th width="100" align="center"><b>Punch Card</b></th>
             <th width="100" align="center"><b>Name</b></th>
             <th width="100" align="center"><b>Designation</b></th>
             <th width="100" align="center"><b>DOJ</b></th>
             <th width="200" align="center"><b>Cost Centre</b></th>
             <th width="" align="center"><b>Policy Name</b></th>
           
        </tr>
        </thead>
    <?	
	
		//$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 and a.attnd_date>='$fromdate' and a.attnd_date<='$todate' and a.emp_code=emp.emp_code $category $cbo_company_id $location_id $department_id $section_id $subsection_id order by emp.section_id, emp.designation_level ASC";
		//$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 and a.attnd_date like '$date_part%' and is_manually_updated=1 and a.emp_code=emp.emp_code $cbo_company_id $category $department_id $location_id  $division_id $section_id $subsection_id $designation_id $category 
	//$orderby";
	$sql="SELECT * from hrm_employee where status_active=1
						$emp_code
						$id_card_no
						$department_id 
						$location_id 
						$division_id 
						$section_id 
						$company_id  
 						$subsection_id 
						$designation_id
						$category
						$salary_based 
						group by emp_code
						$orderby";
						//order by id;
						//echo $sql;die;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	//echo $sql;die;
	$tot_row=mysql_num_rows($result);
			if($tot_row==0)
		{
			echo "<div align='center'>".'please select a company'."</div>";exit();
		}
		$i=1;$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		//$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		while($rows=mysql_fetch_array($result))
		{
			$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
			if(in_array($rows[location_id],$location_arr))
			{
				if(in_array($rows[division_id],$division_arr))
				{
					if(in_array($rows[department_id],$department_arr))
					{
						if(in_array($rows[section_id],$section_arr))
						{
							if(in_array($rows[subsection_id],$subsection_arr))
							{}
							else
							{
								$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
								$new_subsec=1;
							}
						}
						else
						{
							$section_arr[$rows[section_id]]=$rows[section_id];
							$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
							$new_sec=1;
							$new_subsec=1;
						}
					}
					else
					{
						$department_arr[$rows[department_id]]=$rows[department_id];
						$section_arr[$rows[section_id]]=$rows[section_id];
						$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}
				}//division
				else
				{
					$division_arr[$rows[division_id]]=$rows[division_id];
					$department_arr[$rows[department_id]]=$rows[department_id];
					$section_arr[$rows[section_id]]=$rows[section_id];
					$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//division else
			}//location
			else
			{
				$location_arr[$rows[location_id]]=$rows[location_id];
				$division_arr[$rows[division_id]]=$rows[division_id];
				$department_arr[$rows[department_id]]=$rows[department_id];
				$section_arr[$rows[section_id]]=$rows[section_id];
				$subsection_arr[$rows[subsection_id]]=$rows[subsection_id];
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//location else
			//header print here 
			if($new_com==1 && $status_com==1)
			{					
				?><tr><td colspan="11" align="left" bgcolor="#CCCCCC"><strong>Company : <? echo $company_details[$rows[company_id]]; ?></strong></td></tr><?
			}
			if($new_loc==1 && $status_loc==1)
			{					
				?><tr><td colspan="11" align="left" bgcolor="#CCCCCC"><strong>Location : <? echo $location_details[$rows[location_id]]; ?></strong></td></tr><?
			}				
			if($new_divis==1 && $status_divis==1)
			{
				?><tr><td colspan="11" align="left" bgcolor="#CCCCCC"><strong>Division : <? echo $division_details[$rows[division_id]]; ?></strong></td></tr><?
			}
			if($new_dept==1 && $status_dept==1)
			{
				?><tr><td colspan="11" align="left" bgcolor="#CCCCCC"><strong>Department : <? echo $department_details[$rows[department_id]]; ?></strong></td></tr><?
			}
			if($new_sec==1 && $status_sec==1)
			{
				?><tr><td colspan="11" align="left" bgcolor="#CCCCCC"><strong>Section : <? echo $section_details[$rows[section_id]]; ?></strong></td></tr><?
			}
			if($new_subsec==1 && $status_subsec==1)
			{
				?><tr><td colspan="11" align="left" bgcolor="#CCCCCC"><strong>Sub Section : <? echo $subsection_details[$rows[subsection_id]]; ?></strong></td></tr><?
			}
								
	 $name=$rows['first_name'].'&nbsp;'.$rows['middle_name'].'&nbsp;'.$rows['last_name'];
	
	
	?>   
    		<?
			if ($i%2==0) $bgcolor="#EEEEEE"; 
			else $bgcolor="#FFFFFF";	
			?>
            <tr style="font-size:10px; height:30px;" bgcolor="<? echo $bgcolor;?>">
            	<td><? echo $i; ?></td>
            	<td>&nbsp;<? echo $rows['id_card_no']; ?></td>
                <td>&nbsp;<? echo $rows['emp_code']; ?></td>
                <td>&nbsp;<? echo $rows['punch_card_no']; ?></td>
                <td><? echo $name; ?></td>
                <td>&nbsp;<? echo $designation_chart[$rows['designation_id']]; ?></td>             
              	<td><? echo convert_to_mysql_date($rows['joining_date']);?></td> 	 	
               	<td rowspan="" valign="top">
                	<table width="200" cellpadding="0" cellspacing="0" border="1" rules="all">
                		<tr>
                    		<td width="10">Location:</td>
                        	<td align="">&nbsp;<? echo $location_details[$rows['location_id']]; ?></td>
                    	</tr>
                    	<tr>
                    		<td>Division:</td>
                        	<td align="">&nbsp;<? echo $division_details[$rows['division_id']]; ?></td>
                    	</tr>
                    	<tr>
                    		<td>Department:</td>
                        	<td align="">&nbsp;<? echo $department_details[$rows['department_id']]; ?></td>
                    	</tr>
                    	<tr>
                    		<td>Section:</td>
                        	<td align="">&nbsp;<? echo $section_details[$rows['section_id']]; ?></td>
                    	</tr>
                    	<tr>
                    		<td>Subsection:</td>
                        	<td align="">&nbsp;<? echo $subsection_details[$rows['subsection_id']]; ?></td>
                    	</tr>
                    </table>
                    
                </td>
                <td valign="top">
                	<table width="350" cellpadding="0" cellspacing="0" border="1" rules="all">
                    	<tr>
                        	<td width="180">1.Overtime Policy:</td>
                            <td align="">&nbsp;<? echo $overtime_details[$rows['overtime_policy']];  ?></td>
                        </tr>
                        <tr>
                        	<td>2.Holiday Incentive:</td>
                            <td align="">&nbsp;<? echo $holiday_policy_details[$rows['holiday_incentive_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>3.Duty Roster Policy:</td>
                            <td align="">&nbsp;<? echo $roster_policy[$rows['duty_roster_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>4.Leave Policy:</td>
                            <td align="">&nbsp;<? echo $leave_policy[$rows['leave_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>5.Maternity Leave Policy:</td>
                            <td align="">&nbsp;<? echo $meternity_leave_details[$rows['maternity_leave_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>6.Attendance Bonus Policy:</td>
                            <td align="">&nbsp;<? echo $attendance_bonus_policy[$rows['attendance_bonus_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>7.Absent Deduction Policy:</td>
                            <td align="">&nbsp;<? echo $absent_deduction_policy[$rows['absent_deduction_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>8.Late Deduction Policy:</td>
                            <td align="">&nbsp;<? echo $late_deduction_policy[$rows['late_deduction_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>9.Bonus Policy:</td>
                            <td align="">&nbsp;<? echo $bonus_policy[$rows['bonus_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>10.Tax Policy:</td>
                            <td align="">&nbsp;<? echo $tax_details[$rows['tax_policy']]; ?></td>
                        </tr>
                        <tr>
                        	<td>11.Shift Policy:</td>
                            <td align="">&nbsp;<? echo $shift_policy_details[$rows['shift_policy']]; ?></td>
                        </tr>
                    </table>
                
                </td>
               	             	
            </tr>
	<?
         $i++;
	}
		 
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}
?>	

