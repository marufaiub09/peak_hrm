<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}



if( $action == "monthly_attn_summery" ) {
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$adjust_out_time=$res['adjust_out_time'];
			$applicable_above_salary=$res['applicable_above_salary'];
			$in_out_time_format=$res['in_out_time_format'];
		}
		
	if($txt_date!="")
	{
  		list($yr,$mn,$da)=explode("-",convert_to_mysql_date($date));
		$date_part = $year . "-" . $month;
		$date_part_from = $yr . "-" . $mn."-01";
		$date_part_to = $yr . "-" . $mn."-31";
 	}
	else
	{
		$date_part = $year . "-" . $month;
		$date_part_from = $year . "-" . $month."-01";
		$date_part_to = $year . "-" . $month."-31";
	}
	
	 $total_days_att=datediff("d",$date_part_from,$date_part_to);
	
	// Employee OT Calculation
 	$sql=" SELECT emp_code,";   
	for($i=0;$i<$total_days_att; $i++)
	{
		$c_date=add_date($date_part_from,$i);
		if($i!=0) $sql .=",";
		$sql .=" sum(CASE WHEN attnd_date ='$c_date' THEN total_over_time_min END) AS 'OT".$c_date."'";
	}
	$sql .="from  hrm_attendance where attnd_date between '".$date_part_from."' and '".$date_part_to."' group by emp_code order by emp_code";	
	$exe_sql_attnd=mysql_db_query($DB, $sql);
		$employee_ot=array();
		//$b_employee_ot=array();
		while($data=mysql_fetch_array($exe_sql_attnd))
		{
			for($i=0;$i<$total_days_att; $i++)
			{
				$c_date=add_date($date_part_from,$i);
				$employee_ot[$data['emp_code']][$c_date]=$data['OT'.$c_date];
				//$b_employee_ot[$data['emp_code']][$c_date]=$data['BOT'.$c_date];
			}
		} 
		//print_r($employee_ot);die;
		
	if ($category_id	== '')		$category_id		= "and b.category in(0,1,2,3,4)";	else	$category_id	= "and b.category='$category_id'";
	if ($company_id		== 0)		$company_id		= "";		else	$company_id		= "and b.company_id='$company_id'";
	if ($location_id	== 0) 		$location_id	= ""; 		else 	$location_id	= "and b.location_id='$location_id'";
	if ($division_id	== 0 || $division_id	== '')		$division_id	= "";		else	$division_id	= "and b.division_id in($division_id)";
	if ($department_id  == 0 || $department_id	== '')		$department_id	= "";		else	$department_id	= "and b.department_id in($department_id)";
	if ($section_id		== 0 || $section_id	== '') 			$section_id		= ""; 		else 	$section_id		= "and b.section_id in($section_id)";
	if ($subsection_id	== 0 || $subsection_id	== '')		$subsection_id	= "";		else	$subsection_id	= "and b.subsection_id in($subsection_id)";
	if ($designation_id == 0 || $designation_id	== '')		$designation_id	= "";		else	$designation_id	= "and b.designation_id in($designation_id)";

	/*
	if ($cbo_company_id==0) $company_id=""; else $company_id="and  b.company_id='$cbo_company_id'";	
	if ($location_id==0) $location_id=""; else  $location_id="and  b.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else  $division_id="and  b.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and  b.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and  b.subsection_id='$subsection_id'";	
	if ($designation_id==0) $designation_id=""; else $designation_id="and  b.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and  b.category='$cbo_emp_category'";	
	if ($department_id==0) $department_id=""; else	$department_id=" and  b.department_id='$department_id'";
	*/
	
	if ($salary_based=="") $salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	if ($shift_id==0) $shift_id=""; else $shift_id=" and a.policy_shift_id in ( $shift_id )";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 

	
	?>
    <div align="center" style="width:1300px; font-size:16px"><b><? echo $company_details[$cbo_company_id]; ?></b></div>
	<table cellpadding="0" width="1280" cellspacing="0" border="1" class="rpt_table" rules="all">
		<thead>
             	<th width='38'>SL</th>
                <th width='85'>Emp Code</th>
                <th width='80'>ID Card</th>
                <th width='168'>Emp Name</th>
                <th width='120'>Designation</th>
                <th width='120'>Join Date</th>
                
                <th width='36'>Total Days</th>
                <th width='53'>Present</th>
                <th width='51'>Absent</th>
                <th width='38'>Late</th>
                <th width='30'>E.Out</th>
                <th width='45'>Leave</th>
                <th width='32'>MR</th>
                
                <th width='30'>WOD</th>
                <th width='30'>HD</th>
                <th width='32'>PL</th>
                <th width='30'>PW</th>
                <th width='32'>PH</th>
                <th width='50'>Total Present</th>
                <th width='57'>Payable Days</th>
                <th width='50'>Monthly OT</th>
                <th>Job Card</th>
    	</thead>
        <!--</table>
        <table  cellpadding="0" width="1230" cellspacing="0" border="1" class="rpt_table" id="table_body" >	--> 

	<?
			
		$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		$i=0;
		$sl=0;
		
		$sql_attnd=" SELECT   CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name,b.category,b.id_card_no,b.joining_date,a.emp_code,a.department_id,a.location_id, a.division_id, a.section_id, a.subsection_id, a.designation_id,a.total_over_time_min,
						count(CASE WHEN a.status ='P' THEN a.emp_code END) AS 'present',
						count(CASE WHEN a.status ='P' and is_regular_day=0 THEN a.emp_code END) AS 'gn_present',
						count(CASE WHEN a.status ='A' THEN a.emp_code END) AS 'absent',
 						count(CASE WHEN a.status ='D' THEN a.emp_code END) AS 'late',
 						count(CASE WHEN a.status ='GH' THEN a.emp_code END) AS 'g_holiday',
						count(CASE WHEN a.status ='FH' THEN a.emp_code END) AS 'f_holiday',
						count(CASE WHEN a.status ='CH' THEN a.emp_code END) AS 'c_holiday',
						count(CASE WHEN a.status ='H' THEN a.emp_code END) AS 'h_holiday',
						count(CASE WHEN a.status ='W' THEN a.emp_code END) AS 'weekday',
						count(CASE WHEN a.status ='MR' THEN a.emp_code END) AS 'movement',
						count(CASE WHEN a.status ='CL' THEN a.emp_code END) AS 'cl',
						count(CASE WHEN a.status ='FL' THEN a.emp_code END) AS 'fl',
						count(CASE WHEN a.status ='EL' THEN a.emp_code END) AS 'el',
						count(CASE WHEN a.status ='SL' THEN a.emp_code END) AS 'sl',
						count(CASE WHEN a.status ='ML' THEN a.emp_code END) AS 'ml',
						count(CASE WHEN a.status ='SpL' THEN a.emp_code END) AS 'spl',
						count(CASE WHEN a.status ='LWP' THEN a.emp_code END) AS 'lwp',
						count(CASE WHEN a.status ='SP' THEN a.emp_code END) AS 'sp',
						count(CASE WHEN a.early_out_min<>0 THEN a.emp_code END) AS 'eo'
 					FROM 
						hrm_attendance a,hrm_employee b
					where 
						a.emp_code=b.emp_code and a.attnd_date BETWEEN '$date_part_from' AND '$date_part_to' 
						$category_id
						$company_id 
						$location_id
						$division_id  
						$department_id 		  
						$section_id 
						$subsection_id 
						$designation_id 
						$salary_based 
						$shift_id
					group by b.emp_code
					order by b.designation_level";
					
		//echo $sql_attnd;die;  
		//b.designation_level,b.designation_id,b.emp_code
		$results1 = mysql_query( $sql_attnd ) or die( $sql_attnd . "<br />" . mysql_error() );						
 		while( $results = mysql_fetch_assoc( $results1 ) ) 
		{
			$sl++;		
			$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				//if(in_array($row[company_id],$company_arr))
				//{
					if(in_array($results[location_id],$location_arr))
					{
							if(in_array($results[division_id],$division_arr))
							{
									if(in_array($results[department_id],$department_arr))
									{
										if(in_array($results[section_id],$section_arr))
										{
											if(in_array($results[subsection_id],$subsection_arr))
											{}
											else
											{
												$subsection_arr[$results[subsection_id]]=$results[subsection_id];
												$new_subsec=1;
											}
										}
										else
										{
											$section_arr[$results[section_id]]=$results[section_id];
											$subsection_arr[$results[subsection_id]]=$results[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else
									{
										$department_arr[$results[department_id]]=$results[department_id];
										$section_arr[$results[section_id]]=$results[section_id];
										$subsection_arr[$results[subsection_id]]=$results[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else
							{
									$division_arr[$results[division_id]]=$results[division_id];
									$department_arr[$results[department_id]]=$results[department_id];
									$section_arr[$results[section_id]]=$results[section_id];
									$subsection_arr[$results[subsection_id]]=$results[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else
					{
						$location_arr[$results[location_id]]=$results[location_id];
						$division_arr[$results[division_id]]=$results[division_id];
						$department_arr[$results[department_id]]=$results[department_id];
						$section_arr[$results[section_id]]=$results[section_id];
						$subsection_arr[$results[subsection_id]]=$results[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
			
			/*	
				}//company
			else
			{
				$company_arr[$results[company_id]]=$results[company_id];
				$location_arr[$results[location_id]]=$results[location_id];
				$division_arr[$results[division_id]]=$results[division_id];
				$department_arr[$results[department_id]]=$results[department_id];
				$section_arr[$results[section_id]]=$results[section_id];
				$subsection_arr[$results[subsection_id]]=$results[subsection_id];
				$new_com=1;
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//company else
			*/	 
				 
			//header print here 
			if($new_com==1 && $status_com==1)
			{					
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Company : <? echo $company_details[$results[company_id]]; ?></th></tr><?
			}
			if($new_loc==1 && $status_loc==1)
			{					
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$results[location_id]]; ?></th></tr><?
			}				
			if($new_divis==1 && $status_divis==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$results[division_id]]; ?></th></tr><?
			}
			if($new_dept==1 && $status_dept==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Department : <? echo $department_details[$results[department_id]]; ?></th></tr><?
			}
			if($new_sec==1 && $status_sec==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Section : <? echo $section_details[$results[section_id]]; ?></th></tr><?
			}
			if($new_subsec==1 && $status_subsec==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Sub Section : <? echo $subsection_details[$results[subsection_id]]; ?></th></tr><?
			}
			
			$i++;
 			if ($sl%2==0)$bgcolor="#E9F3FF";  
			else $bgcolor="#FFFFFF";
					
 			$present=0;$absent=0;$late=0;$leave=0;$whday=0;$ghday=0;$pl=0;$ph=0;$pw=0;$ot=0;$mr=0;				
			$present=$results['present'];
			$absent=$results['absent'];
			$late=$results['late'];
			$whday=$results['weekday'];
			$ghday=$results['g_holiday']+$results['f_holiday']+$results['c_holiday']+$results['h_holiday'];
			$mr=$results['movement'];
			$leave=$results['cl']+$results['fl']+$results['el']+$results['sl']+$results['ml']+$results['spl']+$results['lwp']+$results['sp'];
			$e_out=$results['eo'];
			 
			$total_present=$present+$late+$pl+$pw+$ph+$mr;
			$total_days=$present+$late+$absent+$whday+$ghday+$leave+$mr;
			$payable_days=$total_days-$absent;
			
			$total_ot_hours=0;
			for($i=0;$i<$total_days_att; $i++)
			{
				$c_date=add_date($date_part_from,$i);
				$total_ot_hours +=get_buyer_ot_hr($employee_ot[$results['emp_code']][$c_date],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				
			}
			 
			if($late>2)$color='#EE3900';else$color='';
 			
		?>
					
            <tr bgcolor="<? echo $bgcolor; ?>">
            	<td height='50'><? echo $sl; ?></td>
                <td height='50'><? echo $results['emp_code']; ?></td>
                <td><? echo $results['id_card_no']; ?></td>
                <td><? echo $results['name']; ?></td>
                <td align='left'><? echo $designation_chart[$results['designation_id']]; ?></td>
                <td><? echo convert_to_mysql_date($results['joining_date']); ?></td>
                
                <td><? echo $total_days;?></td>
                 <td><a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=P&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $present;?></a></td>
                <td><a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=A&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $absent;?></a></td>
                <td bgcolor="<? echo $color;?>">&nbsp;<a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=D&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $late;?></a></td>
                
                <td>&nbsp;<a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report_e_out&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=EO&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $e_out;?></a></td>
                
                
                <td><a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=CL&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $leave;?></a></td>
                <td><a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=MR&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $mr;?></a></td>						
                <td><a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=W&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $whday;?></a></td>
                <td><a href='#' onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=H&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $ghday;?></a></td>
                <td><? echo $pl;?></td>
                <td><? echo $pw;?></td>
                <td><? echo $ph;?></td>
                <td><? echo $total_present;?></td>
                <td><? echo $payable_days;?></td>
                <td><? echo $total_ot_hours; ?></td>
                <td><input type="button" value="View" style="width:30px" class="formbutton" onclick="openmypage('popup_report.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&date=<? echo $year."-".$month; ?>&year=<? echo $year; ?>&month=<? echo $month; ?>&status=JC&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"/></td>
            </tr>
                    
		  <?			
  		}	
	?>
		
	</table> 

<?

	$html = ob_get_contents();
	ob_clean();
	
 	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}

function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}


//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$explode_val=explode(" ",$row["starting_date"]);
		//$value = $explode_val[0];
		$value = substr($explode_val[0],0,-3);//new line for new year
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}