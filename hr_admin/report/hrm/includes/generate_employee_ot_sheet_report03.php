<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

	
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}

if($action=='ot_sheet_payment_actual'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit']; 
			$first_ot_limit = $res['first_ot_limit'];	
	}
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	$table_width=200+($get_days*60);
	if($table_width<900) $table_width=900;
	
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and b.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and b.location_id='$location_id'";
	//echo $location_id;die;
	if ($division_id==0) $division_id=""; else $division_id="and b.division_id in ($division_id)";
	//echo $division_id;die;
	if ($department_id==0) $department_id=""; else	$department_id="and b.department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and b.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and b.subsection_id in ($subsection_id)";	
	if ($designation_id==0) $designation_id=""; else $designation_id="and b.designation_id in ($designation_id) ";	
	if ($cbo_emp_category=='') $category =""; else $category ="and a.category='$cbo_emp_category'";
	if ($emp_code=='') $emp_code =""; else $emp_code ="and a.emp_code in ($emp_code)";

	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="b.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="b.company_id,b.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="b.company_id,b.location_id,b.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(a.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(a.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(b.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(a.id_card_no, 5)";}	 

	$groupby = substr($groupby, 0, -1); 		
	ob_start();	
		
	?>   
    <style>
    .verticalText 
	{               
  		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
		-o-transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		transform: rotate(270deg);
		height:10px;
		width:10px;
		margin-top:40px;
  	}
    </style>
 	<div align="center" style="font-weight:bold">
    	<span style="font-size:18px"><? echo $company_details[$company_id]; ?></span><br />
        <span style="font-size:13px">Employee OT Sheet</span>
    </div>
    <table cellpadding="0" cellspacing="0" border="1" width="<? echo $table_width; ?>" class="rpt_table" style="font-size:10px;" align="center"> 
       <thead>        
            <tr>
            	<th width="10"><b>SL</b></th>
                <th width="70"><b>ID Card</b></th>
                <th width="70"><b>Emp Code</b></th>
                <th width="130"><b>Name</b></th>
                <th width="90"><b>Designation</b></th>
				<?
                    for($j=0;$j<$get_days;$j++){				
                    $newdate = date("Y-m-d",strtotime("+".$j." day", strtotime($fromdate)));
					$sql_part .= "SUM(CASE WHEN b.attnd_date = '".$newdate."' THEN b.total_over_time_min else 0 END ) as ot".$j.",";
                ?>
                    <th  width="2" style="padding:10px 10px;"><div class="verticalText" style="font-size:9px;" ><? echo convert_to_mysql_date($newdate); ?></div></th>
                <?	
                    }
					$sql_part = substr($sql_part, 0, -1); 
                ?>
                 
                <th width="20"><b>OT</b></th>
                <th width="20"><b>EOT</b></th>
                <th width="20">Total</th>
                <th width="20">OT Rate/Hrs</th>
                <th width="20">Total OT Amount</th>
            </tr>
       </thead>       
						
    <?	
 		
		$sql = "SELECT CONCAT(a.first_name, ' ', a.middle_name, ' ', a.last_name) AS name,a.staff_ot_entitled,
		a.id_card_no,a.emp_code,b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,
		b.subsection_id,b.designation_id,".$sql_part."
				FROM hrm_employee a,hrm_attendance b
				WHERE 
				a.emp_code=b.emp_code and b.attnd_date>='$fromdate' and  b.attnd_date<='$todate'
				$category 
				$cbo_company_id 
				$location_id
				$division_id 
				$section_id 
				$subsection_id 
				$designation_id 
				$department_id 
				$emp_code 
				group by a.emp_code $orderby";	
		  //echo $sql; die;
 				
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$k=0;$sl=0;
		$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		$overtime=0;$ot_ot=0;
		while($att_row=mysql_fetch_array($result))
		{
					$d="over_time_rate";
					$dd="hrm_salary_mst";
					$ddd="salary_periods between '$fromdate' and '$todate' and emp_code=$att_row[emp_code]";
		
					$ot_hr_rate=return_field_value($d,$dd,$ddd);
					//echo $tt;die;
			
			$i++;
			$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				//if(in_array($row[company_id],$company_arr))
				//{
	
					if(in_array($att_row[location_id],$location_arr)&& $status_loc==1)
					{
							if(in_array($att_row[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($att_row[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($att_row[section_id],$section_arr) && $status_sec==1)
										{
											if(in_array($att_row[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else
											{
												$subsection_arr[$att_row[subsection_id]]=$att_row[subsection_id];
												$new_subsec=1;
											}
										}
										else
										{
											$section_arr[$att_row[section_id]]=$att_row[section_id];
											$subsection_arr[$att_row[subsection_id]]=$att_row[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else
									{
										$department_arr[$att_row[department_id]]=$att_row[department_id];
										$section_arr[$att_row[section_id]]=$att_row[section_id];
										$subsection_arr[$att_row[subsection_id]]=$att_row[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else
							{
									$division_arr[$att_row[division_id]]=$att_row[division_id];
									$department_arr[$att_row[department_id]]=$att_row[department_id];
									$section_arr[$att_row[section_id]]=$att_row[section_id];
									$subsection_arr[$att_row[subsection_id]]=$att_row[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else
					{
						$location_arr[$att_row[location_id]]=$att_row[location_id];
						$division_arr[$att_row[division_id]]=$att_row[division_id];
						$department_arr[$att_row[department_id]]=$att_row[department_id];
						$section_arr[$att_row[section_id]]=$att_row[section_id];
						$subsection_arr[$att_row[subsection_id]]=$att_row[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
	
		
				 
 				//header print here 
				if($new_com==1 && $status_com==1)
				{					
					?><tr><th colspan="<? echo $get_days+10; ?>" align="left" bgcolor="#CCCCCC">Company : <? echo $company_details[$att_row[company_id]]; ?></th></tr><?
				}
				if($new_loc==1 && $status_loc==1)
				{					
					?><tr><th colspan="<? echo $get_days+10; ?>" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$att_row[location_id]]; ?></th></tr><?
				}				
				if($new_divis==1 && $status_divis==1)
				{
					?><tr><th colspan="<? echo $get_days+10; ?>" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$att_row[division_id]]; ?></th></tr><?
				}
 				if($new_dept==1 && $status_dept==1)
				{
					?><tr><th colspan="<? echo $get_days+10; ?>" align="left" bgcolor="#CCCCCC">Department : <? echo $department_details[$att_row[department_id]]; ?></th></tr><?
				}
				if($new_sec==1 && $status_sec==1)
				{
					?><tr><th colspan="<? echo $get_days+10; ?>" align="left" bgcolor="#CCCCCC">Section : <? echo $section_details[$att_row[section_id]]; ?></th></tr><?
				}
				if($new_subsec==1 && $status_subsec==1)
				{
					?><tr><th colspan="<? echo $get_days+10; ?>" align="left" bgcolor="#CCCCCC">Sub Section : <? echo $subsection_details[$att_row[subsection_id]]; ?></th></tr><?
				}
			
 			 
 					
					if($i%2==0) $bgcolor='#FFFFFF';
					else $bgcolor='#EEEEEE';		
				?>
                    <tr bgcolor="<? echo $bgcolor; ?>">
                        <td width="10"><? echo $i; ?></td>
                        <td width="70"><? echo $att_row['id_card_no']; ?></td>
                        <td width="70"><? echo $att_row['emp_code']; ?></td>
                        <td width="130"><? echo $att_row['name']; ?></td>
                        <td width="90"><? echo $designation_chart[$att_row['designation_id']]; ?></td> 
                <?
				$total_ot=0;$buyer_ot=0; $total_overtime=0; 
				$overtime=0;$ot_ot=0;
				for($j=0;$j<$get_days;$j++){
					$hours[0] = abs((int)($att_row['ot'.$j]/60)); //hours
					$hours[1] = abs((int)($att_row['ot'.$j]%60)); //minutes
					$ot_mins_cal=0;
					if($ot_fraction==1) //ot fraction allow
					{						
						if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = $hours[0]+.50;}
						else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = $hours[0]+1;}
						else $ot_mins_cal = $hours[0];
					}
					if($ot_fraction==2) // no ot fraction
					{						
						//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
						if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = $hours[0]+1;}
						else $ot_mins_cal = $hours[0];						
					}
					if($ot_fraction==3) //at actual minutes
					{						
						$ot_mins_cal = 	$att_row['ot'.$j];				
					}
					
					$total_hours = $ot_mins_cal; //sprintf("%d.%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
 					$total_ot += $total_hours;
					
					$ot_hr= get_buyer_ot_hr($total_hours,$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
					$new_tot=explode(".",$ot_hr);
					$ot_mins_cal_tot=($new_tot[0]*60)+$new_tot[1];
					$ot_ot += $ot_mins_cal_tot;
					$overtime = sprintf("%d.%02d", abs((int)($ot_ot/60)), abs((int)($ot_ot%60)));
					
					/*$explode_total_ot = explode(".",$total_ot);
					if($explode_total_ot[1]==6) $total_ot = $explode_total_ot[0]+1;*/
					?>
					<td align="right" width="2"><b><? echo $ot_hr;//$total_hours; ?></b></td>
					<?	
					$grand_total_on_date[$j] += $ot_hr; //$total_hours;
					if($total_hours>0)
					{						
						//get_buyer_ot_hr($act_ot_min,$one_hour_ot_unit,$allow_ot_fraction,$half_hr_ot_unit,$first_ot_limit)
						$buyer_ot += get_buyer_ot_hr($att_row['ot'.$j],$one_hr_ot_unit,$ot_fraction,$ot_start_min,$first_ot_limit);
					}
					
				}//end for getdays loop
				
					/*list($org,$frac)=explode(".",$buyer_ot); if($frac==5)$buyer_ot = $org.".30";*/
					$grand_total_total += $total_ot;
					$grand_total_on_ot += $buyer_ot;
					$grand_total_on_eot += $total_ot-$buyer_ot;
					$grand_total_ot_amount += $ot_hr_rate;
					$ct++;
				//grand total
				$grand_total_ot  +=  ($total_ot*$ot_hr_rate);	
					
  				?>
					
                    <td align="right" width="20"><b><? echo $overtime; // $buyer_ot;?></b></td>
                    <td align="right" width="20"><b><? echo $total_ot-$buyer_ot;?></b></td>
                    <td align="right" width="20"><b><? echo $total_ot;?></b></td>
                    <td align="right" width="20"><b><? echo $ot_hr_rate;?></b></td>
                    <td align="right" width="20"><b><? echo $tt_amount=($total_ot*$ot_hr_rate);?></b></td>
 			</tr>

	<?
		}// end condition while loop
	?>
    	
        
        
        <tfoot class="tbl_bottom">
        	<td colspan="5">Total</td>
            <? for($h=0;$h<$get_days;$h++){ ?>
            	<td align="center"><b><? echo $grand_total_on_date[$h];?></b></td>
          <? } ?>
          
          <td align="right"><b><? echo $grand_total_on_ot;?></b></td>
          <td align="right"><b><? echo $grand_total_on_eot;?></b></td>
          <td align="right"><b><? echo $grand_total_total;?></b></td>
          <td align="right"><b><? //echo $grand_total_total;?></b></td>
          <td align="right"><b><? echo $grand_total_ot_amount;?></b></td>
        </tfoot>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}
?>