<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


			
	
	
if($action=='absent_late_trent_report')
{
	//emp_type		
	$company_id=$cbo_company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and b.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and b.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and b.division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else  $department_id="and b.department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and b.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and b.subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and b.designation_id in ($designation_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	if ($emp_type=="") $emp_type=""; else $emp_type="and b.status_active='$emp_type'";
	if ($cbo_type_selector==1) $status="A"; else $status="D";
	
	$addrs ="";		
	if($company_details[$company_id]['plot_no'] !='') $addrs .= $company_details[$company_id]['plot_no'].", "; 
	if($company_details[$company_id]['level_no'] !='') $addrs .= $company_details[$company_id]['level_no'].", "; 
	if($company_details[$company_id]['block_no'] !='') $addrs .= $company_details[$company_id]['block_no'].", "; 
	if($company_details[$company_id]['road_no'] !='') $addrs .= $company_details[$company_id]['road_no'].", "; 
	if($company_details[$company_id]['city'] !='') $addrs .= $company_details[$company_id]['city'].", ";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="b.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="b.company_id,b.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="b.company_id,b.location_id,b.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,";}
	
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(b.id_card_no, 5)";}	 
		
	ob_start();	
 	?>    
	<table cellpadding="0" cellspacing="0" border="1" width="1300" style="border:1px solid #000; font-size:11px" class="rpt_table" rules="all">       
    	<thead>
        	<tr>
        		<th colspan="22"><font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br /><? echo $addrs; ?><br />Employee's Yearly <? if($status=='A') echo "Absent"; else echo "Late"; ?> Trent</th>
            </tr>
            <tr class="tbl_top">
                <th width="20"><b>SL</b></th>
                <th width="100"><b>ID Card</b></th>
                <th width="100"><b>Emp Code</b></th>
                <th width="130"><b>Name</b></th>
                <th width="100"><b>Designation</b></th>
                <th width="70"><b>Category</b></th>
                <th width="70"><b>Section</b></th>
                <th width="70"><b>Sub Section</b></th>
                <th width="70"><b>DOJ</b></th>
                <th width="40"><b>JAN</b></th>
                <th width="40"><b>FEB</b></th>
                <th width="40"><b>MAR</b></th>
                <th width="40"><b>APR</b></th>
                <th width="40"><b>MAY</b></th>
                <th width="40"><b>JUN</b></th>
                <th width="40"><b>JUL</b></th>
                <th width="40"><b>AUG</b></th>
                <th width="40"><b>SEP</b></th>
                <th width="40"><b>OCT</b></th>
                <th width="40"><b>NOV</b></th>
                <th width="40"><b>DEC</b></th>
                <th><b>Remarks</b></th>  
            </tr>
        </thead> 
	<?	
		//company_id 	location_id 	division_id 	department_id 	section_id 	subsection_id 	designation_id
		
		$i=0;$new_location=array();$new_division=array();$new_department=array();$new_section=array();$new_subsection=array();
			
		$attn_sql = "SELECT CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name,b.category,b.id_card_no,b.emp_code,b.company_id,b.location_id, b.division_id,b.department_id, b.section_id, b.subsection_id, b.designation_id,
		count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-01%' THEN status END) as jan,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-02%' THEN status END) as fab,
		count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-03%' THEN status END) as mar,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-03%' THEN status END) as apr,
		count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-05%' THEN status END) as may,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-06%' THEN status END) as jun,
		count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-07%' THEN status END) as jul,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-08%' THEN status END) as aug,
		count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-09%' THEN status END) as sep,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-10%' THEN status END) as oct,
		count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-11%' THEN status END) as nov,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-12%' THEN status END) as decm
		FROM hrm_attendance a,hrm_employee b
		WHERE 
		a.emp_code=b.emp_code and status='$status' $cbo_company_id $category $department_id $location_id  $division_id $section_id $subsection_id $designation_id $salary_based $emp_type group by b.emp_code $orderby";
						
		//deduct from quey on 23_01_2012 group by b.emp_code $orderby";
		//group by  b.emp_code order by b.department_id,RIGHT(b.id_card_no,4) ASC";
							
		//echo $attn_sql;
				 
	$result= mysql_query( $attn_sql ) or die( $attn_sql . "<br />" . mysql_error() );	
	$tot_row=mysql_num_rows($result);
		if($tot_row==0)
		{
			echo "<div align='center'>".'please select a company'."</div>";exit();
		}
		$i=1;$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		//$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		while($row=mysql_fetch_array($result))
		{
				$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				if(in_array($row[location_id],$location_arr))
				{
					if(in_array($row[division_id],$division_arr))
					{
						if(in_array($row[department_id],$department_arr))
						{
							if(in_array($row[section_id],$section_arr))
							{
								if(in_array($row[subsection_id],$subsection_arr))
								{}
								else
								{
									$subsection_arr[$row[subsection_id]]=$row[subsection_id];
									$new_subsec=1;
								}
							}
							else
							{
								$section_arr[$row[section_id]]=$row[section_id];
								$subsection_arr[$row[subsection_id]]=$row[subsection_id];
								$new_sec=1;
								$new_subsec=1;
							}
						}
						else
						{
							$department_arr[$row[department_id]]=$row[department_id];
							$section_arr[$row[section_id]]=$row[section_id];
							$subsection_arr[$row[subsection_id]]=$row[subsection_id];
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}
					}//division
					else
					{
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//division else
				}//location
				else
				{
					$location_arr[$row[location_id]]=$row[location_id];
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
					$new_loc=1;
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//location else
			
			
			//header print here 
			if($new_com==1 && $status_com==1)
			{					
				?><tr><td colspan="22" align="left" bgcolor="#CCCCCC"><strong>Company : <? echo $company_details[$row[company_id]][company_name]; ?></strong></td></tr><?
			}
			if($new_loc==1 && $status_loc==1)
			{					
				?><tr><td colspan="22" align="left" bgcolor="#CCCCCC"><strong>Location : <? echo $location_details[$row[location_id]][location_name]; ?></strong></td></tr><?
			}				
			if($new_divis==1 && $status_divis==1)
			{
				?><tr><td colspan="22" align="left" bgcolor="#CCCCCC"><strong>Division : <? echo $division_details[$row[division_id]][division_name]; ?></strong></td></tr><?
			}
			if($new_dept==1 && $status_dept==1)
			{
				?><tr><td colspan="22" align="left" bgcolor="#CCCCCC"><strong>Department : <? echo $department_details[$row[department_id]][department_name]; ?></strong></td></tr><?
			}
			if($new_sec==1 && $status_sec==1)
			{
				?><tr><td colspan="22" align="left" bgcolor="#CCCCCC"><strong>Section : <? echo $section_details[$row[section_id]][section_name]; ?></strong></td></tr><?
			}
			if($new_subsec==1 && $status_subsec==1)
			{
				?>
                <tr><td colspan="22" align="left" bgcolor="#CCCCCC"><strong>Sub Section : <? echo $subsection_details[$row[subsection_id]][subsection_name]; ?></strong></td></tr>
                <?
			}
						
		?>    
				<tr style="font-size:11px">
					<td><? echo $i; ?></td>
					<td>&nbsp;<? echo $row['id_card_no']; ?></td>
					<td>&nbsp;<? echo $row['emp_code']; ?></td>
					<td><? echo $row['name']; ?></td>
					<td>&nbsp;<? echo $designation_chart[$row['designation_id']]['custom_designation']; ?></td> 
                    <td>&nbsp;<? echo $employee_category[$row['category']]; ?></td>
                    <td>&nbsp;<? echo $section_details[$row['section_id']]['section_name']; ?></td> 
                    <td>&nbsp;<? echo $subsection_details[$row['subsection_id']]['subsection_name']; ?></td> 
					<td>&nbsp;<? echo convert_to_mysql_date($row['joining_date']);?></td>          	
					<td align="right"><? echo $row['jan'];?></td> 	 	
					<td align="right"><? echo $row['fab'];?></td>
					<td align="right"><? echo $row['mar'];?></td>
					<td align="right"><? echo $row['apr'];?></td>
					<td align="right"><? echo $row['may'];?></td>
					<td align="right"><? echo $row['jun'];?></td>
					<td align="right"><? echo $row['jul'];?></td>
					<td align="right"><? echo $row['aug'];?></td>
					<td align="right"><? echo $row['sep'];?></td>
					<td align="right"><? echo $row['oct'];?></td>
					<td align="right"><? echo $row['nov'];?></td>
					<td align="right"><? echo $row['decm'];?></td>     
					<td>&nbsp;</td>              	
				</tr>  
			<?	
			$i++;	
			}
    	?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}


