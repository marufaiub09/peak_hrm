<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


//ekram
//employee Religion ratio

if($action=='religion_ratio'){
 		
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	//if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	//if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	//if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";	
	//if ($department_id==0) $department_id=""; else	$department_id="and department_id='$department_id'";	
		
	ob_start();	
		
	?>    
	<table cellpadding="0" cellspacing="1" border="1" width="900">        
	<?php	
			$sql = "SELECT *, 
						COUNT(CASE WHEN status_active=1 THEN id END) as total_emp,
						COUNT(CASE WHEN religion='islam' THEN id END) as total_muslim,
						COUNT(CASE WHEN religion='hindu' THEN id END) as total_hindu 
					FROM 
						hrm_employee 
					WHERE 
						status_active=1 $cbo_company_id $location_id ";//$section_id $subsection_id $designation_id $department_id group by company_id, section_id
			//echo $sql;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
			
			$i=0;
			$new_company=array();
			$grand_total_emp = 0;$grand_total_muslim = 0;$grand_total_hindu = 0;	$grand_total_muslim_in_per = 0;$grand_total_hindu_in_per = 0;
			while($row = mysql_fetch_array($result) )
			{								
				if (in_array($row['company_id'], $new_company))
					{
						$i++;
					}
				else
					{
					   if($i>0)
					   {
					?>						
						<tr><td colspan="2"><b>Grand Total</b></td><td><? echo $grand_total_emp; ?></td><td><? echo $grand_total_muslim; ?></td><td><? echo $grand_total_hindu; ?></td><td><? echo $grand_total_muslim_in_per; ?></td><td><? echo $grand_total_hindu_in_per; ?></td></tr>
					<?
						$grand_total_emp = 0;$grand_total_muslim = 0;$grand_total_hindu = 0;	$grand_total_muslim_in_per = 0;$grand_total_hindu_in_per = 0;	
					   }
						$i=0;
					?>                            								 
						<tr><td colspan="10" align="center"><font size="+1"><? echo $company_details[$row['company_id']]['company_name']; ?></font></td></tr>
						<tr><td colspan="10" align="center">&nbsp;</td></tr>													  
						<tr>
							<td colspan="10">&nbsp;</td>
						</tr>
                        <tr>
							<td colspan="10" align="center">Employee Muslim Hindu Ratio</td>
						</tr>
						<tr class="display" style="font-weight:bold">
						   <td width="40"><b>SL</b></td> 
							<td width=""><b>Section Name</b></td>
							<td width="110"><b>Total Employee</b></td>											
							<td width="100"><b>Muslim</b></td>
							<td width="100"><b>Hindu</b></td>	 							
							<td width="100"><b>Muslim(%)</b></td>
							<td width="100"><b>Hindu(%)</b></td>												 									
						</tr>
                    <?
						$new_company[$i]=$row[company_id];
						$i++;
					}
                    ?>				                       				
                        <tr class="display">
                            <td width="40"><? echo $i; ?></td> 
                            <td width=""><? echo $section_details[$row['section_id']]['section_name'];?></td>                           					
                            <td width="110"><? echo $row['total_emp']; ?></td>
                            <td width="100"><? echo $row['total_muslim']; ?></td>
                            <td width="100"><? echo $row['total_hindu']; ?></td>
                            <?
								$muslim_in_per = number_format(($row['total_muslim']*100)/$row['total_emp']);
								$hindu_in_per = number_format(($row['total_hindu']*100)/$row['total_emp']);								
                            ?>	 								
                            <td width="100"><? echo $muslim_in_per; ?></td>
                            <td width="100"><? echo $hindu_in_per; ?></td>                                                   			 
                        </tr>	
           	<? 
			
						$grand_total_emp += $row['total_emp'];
						$grand_total_male += $row['total_male'];
						$grand_total_female += $row['total_female'];
						$muslim_in_per = number_format(($grand_total_male*100)/$grand_total_emp);
						$hindu_in_per = number_format(($grand_total_female*100)/$grand_total_emp);		
						$grand_total_male_in_per = $muslim_in_per;
						$grand_total_female_in_per = $hindu_in_per;
															
				} 
			
			?>             
                        		
				</table><br /><br /><br /><br />
				<table width="900">           		
					<tr>
						<td width="225" align="center">--------------------------</td>						
						<td width="225" align="center">--------------------------</td>
						<td width="225" align="center">--------------------------</td>
						<td width="225" colspan="4" align="center">--------------------------</td>
					</tr>
					<tr>
						<td align="center">Officer IT</td>
						<td align="center">Admin/HRM</td>
						<td align="center">PM/APM</td>						
						<td colspan="4" align="center">GM/DGM/AGM/FM</td>
					</tr>
			</table>		
           
	<?		
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}

//end raligion ratio

?>