<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	
	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


if( $action == "id_card" ) {	//ID Card Report
	require_once('../../../../resources/tcpdf_5_9_082/tcc/config/lang/eng.php');
	require_once('../../../../resources/tcpdf_5_9_082/tcc/tcpdf.php');
	header ('Content-type: text/html; charset=utf-8'); 
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'RA4', true, 'UTF-8', false);	// create new PDF document
	 
	// set document information
	$pdf->SetCreator('Md. Muntasir Enam');
	$pdf->SetAuthor('Md. Muntasir Enam');
	$pdf->SetTitle('Logic HRM & Payroll');
	$pdf->SetSubject('ID Card');
	$pdf->SetKeywords('Logic, HRM, Payroll, HRM & Payroll, ID Card');
	
	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);	//set default monospaced font
	$pdf->SetMargins(8, 8, 8);								//set margins
	$pdf->SetAutoPageBreak(TRUE, 5);						//set auto page breaks
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				//set image scale factor
	$pdf->setLanguageArray($l);								//set some language-dependent strings
	//$pdf->SetFont('times', '', 10);
	
	//$pdf->addTTFfont('kalpurush.TTF', 'TrueTypeUnicode', '', 32);
	//$pdf->AddFont('kalpurush','BI', 20,'','false');
	//$pdf->SetFont('SUTOM___', '', 10);
	// $img_file = '../../../../images/impossible.gif';
	// $pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
	 
	 $pdf->SetFont('times', '', 9);
	$employees = explode( "_", $employees );
	
	$pdf->AddPage();	//add a page
	
		
	if($option == "H"){
	$html = "<table width=\"100%\" border=\"1\" cellspacing=\"1\" ><tr>";	//set some text to print
	for( $i = 1; $i <= count( $employees ); $i++ ) 
	{
		$sql = "SELECT * FROM resource_photo WHERE module = 'HRM' AND identifier = '" . $employees[$i-1] . "' AND category = 0 AND type = 1 AND is_deleted = 0 order by id DESC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( mysql_num_rows( $result ) > 0 ) {
			$row = mysql_fetch_assoc( $result );
			$location = $row['location'];
		}
		else $location = '';
		$addrs ="";		
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['plot_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['plot_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['level_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['level_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['block_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['block_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['road_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['road_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['city'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['city']." "; 
		
			$html .= "<td width=\"204\" height=\"310\">												
						<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">							
							<tr><td><img src=\"../../../../resources/images/peak.jpg\" width=\"70\" height=\"60\" /><b>".$company_details[$emp_basic[$employees[$i-1]]['company_id']]['company_name']."</b></td>";							
								if( $location != '' && file_exists( "../../../../$location" ) ) $html .= "<td align=\"center\" colspan=\"2\"><img src=\"../../../../$location\" width=\"80\" height=\"100\" border=\"1\" /></td>";
								else $html .= "<td align=\"right\"  colspan=\"2\"><img src=\"../../../../resources/images/blank.jpg\" width=\"80\" height=\"100\" border=\"1\" /></td>";
					$html .="</tr>	
							
	
<tr height=\"20\"><td>&nbsp;</td></tr>
<tr><td width=\"40%\" style=\"font-size:35px\"><b>&nbsp;Name</b></td><td width=\"60%\" style=\"font-size:35px\" ><b>:" .str_pad($emp_basic[$employees[$i-1]]['name'],35," ",STR_PAD_RIGHT). "</b></td></tr>
<tr><td width=\"40%\" style=\"font-size:35px\"><b>&nbsp;Emp Code</b></td><td width=\"60%\" style=\"font-size:35px\"><b>:" .$emp_basic[$employees[$i-1]]['emp_code']. "</b></td></tr>	
<tr><td width=\"40%\" style=\"font-size:35px\"><b>&nbsp;ID NO</b></td><td width=\"60%\" style=\"font-size:35px\"><b>:" .$emp_basic[$employees[$i-1]]['id_card_no']. "</b></td></tr>
<tr><td style=\"font-size:35px\"><b>&nbsp;Desig</b></td><td style=\"font-size:35px\"><b>:" .$designation_chart[$emp_basic[$employees[$i-1]]['designation_id']]['custom_designation']."</b></td></tr>			
<tr><td style=\"font-size:35px\"><b>&nbsp;Section</b></td><td style=\"font-size:35px\"><b>:" .$section_details[$emp_basic[$employees[$i-1]]['section_id']]['section_name']. "</b></td></tr>					
<tr><td style=\"font-size:35px\"><b>&nbsp;DOJ</b></td><td style=\"font-size:35px\"><b>:" .convert_to_mysql_date($emp_basic[$employees[$i-1]]['joining_date']) . "</b></td></tr>
<tr><td style=\"font-size:35px\"><b>&nbsp;DOB</b></td><td style=\"font-size:35px\"><b>:" .convert_to_mysql_date($emp_basic[$employees[$i-1]]['dob']) . "</b></td></tr>
<tr><td style=\"font-size:35px\"><b>&nbsp;Blood Group</b></td><td style=\"font-size:35px\"><b>:" .$blood_group[$emp_basic[$employees[$i-1]]['dob']]. "</b> </td></tr>";

					$html .= "<tr><td colspan=\"2\">&nbsp;</td></tr>						 
							<tr>
								<td align=\"center\">&nbsp;</td>
								<td align=\"right\">&nbsp;</td>
							</tr>
						<tr style=\"position:fixed;\">
							<td align=\"left\" style=\"text-decoration:overline;font-size:30px;\" width=\"100\" ><br/><br/><font>Holder's Signature</font></td>
							<td align=\"center\" style=\"text-decoration:overline;font-size:30px\" width=\"100\" ><br/><br/><font>Authorized Signature</font></td>
						</tr>
						</table>
					</td>";
			if( $i == count( $employees ) && $i % 3 == 1 ) $html .= "<td colspan=\"2\">&nbsp;</td>";
			elseif( $i == count( $employees ) && $i % 3 == 2 ) $html .= "<td>&nbsp;</td>";
			
			if( $i % 9 == 0 && $i < count( $employees ) ) {
				$html .= "</tr></table>";
				$pdf->writeHTML($html, true, false, true, false, '');
				$pdf->AddPage();
				$html = "<table width=\"100%\" border=\"1\" cellspacing=\"1\"><tr>";
			}
			elseif( $i % 3 == 0 && $i < count( $employees ) ) $html .= "</tr><tr>";
		}
		$html .= "</tr></table>";
	}
	
	if($option == "V"){
		$html = "<table width=\"100%\" border=\"1\" cellspacing=\"30\"><tr>";	//set some text to print
		for( $i = 1; $i <= count( $employees ); $i++ ) {
		$sql = "SELECT * FROM resource_photo WHERE module = 'HRM' AND identifier = '" . $employees[$i-1] . "' AND category = 0 AND type = 1 AND is_deleted = 0 order by id DESC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( mysql_num_rows( $result ) > 0 ) {
			$row = mysql_fetch_assoc( $result );
			$location = $row['location'];
		}
		else $location = '';
	//	$emp_basic[$employees[$i-1]]['name']=str_pad($emp_basic[$employees[$i-1]]['name'],100,".",STR_PAD_LEFT);
		$html .= "<td width=\"310\" height=\"180\" colspan=\"2\">
					<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"3\" cellpadding=\"0\">	
						<tr><td colspan=\"2\">&nbsp;</td></tr>
						<tr><td width=\"70%\"><table>";
							$html .= "<tr><td>&nbsp;<img src=\"../../../../resources/images/asro_logo.png\" width=\"75\" height=\"30\" />&nbsp;&nbsp;&nbsp;</td><td></td></tr>
									  <tr><td colspan=\"2\">&nbsp;</td></tr>
									  <tr><td colspan=\"2\" align=\"center\" ><h1>" . $company_details[$emp_basic[$employees[$i-1]]['company_id']]['company_name'] . "</h1></td></tr>
									  <tr><td colspan=\"2\" align=\"center\" style=\"font-size:35px\">&nbsp;&nbsp;&nbsp;&nbsp;<b>ss" . str_pad($emp_basic[$employees[$i-1]]['name'],335," ",STR_PAD_RIGHT) . "</b></td></tr>	
									  <tr><td colspan=\"2\" align=\"center\" style=\"font-size:25px\">&nbsp;&nbsp;&nbsp;&nbsp;<b>" . $designation_chart[$emp_basic[$employees[$i-1]]['designation_id']]['custom_designation'] . "</b></td></tr>	
									  </table></td>";	
							
							if( $location != '' && file_exists( "../../../../$location" ) ) $html .= "<td align=\"right\" width=\"30%\">&nbsp;&nbsp;&nbsp;<img src=\"../../../../$location\" width=\"78\" height=\"95\" border=\"1\" />&nbsp;&nbsp;&nbsp;&nbsp;</td>";
								else $html .= "<td align=\"right\" width=\"30%\">&nbsp;</td>";			
							$html .="</tr><tr>		
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ID # " . $emp_basic[$employees[$i-1]]['id_card_no'] . "</b></td>";
							$html .="</tr><tr>		
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blood Group: ";
							if( $emp_basic[$employees[$i-1]]['blood_group'] != '0' ) $html .= $blood_group[$emp_basic[$employees[$i-1]]['blood_group']];							
							$html .= "</td></tr>							
							<br /><br />
							<tr><td colspan=\"2\">&nbsp;</td></tr>
							<tr><td colspan=\"2\">&nbsp;</td></tr>
							<tr><td align=\"left\">&nbsp;__________________</td>
							<td align=\"right\">__________________&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
							<tr><td align=\"left\">&nbsp;Holder's Signature</td>
							<td align=\"right\">Authorized Signature&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>							
					</table>
				</td>";
				
			if( $i == count( $employees ) && $i % 1 == 1 ) $html .= "<td colspan=\"2\">&nbsp;</td>";
			elseif( $i == count( $employees ) && $i % 1 == 1 ) $html .= "<td>&nbsp;</td>";
			
			if( $i % 8 == 0 && $i < count( $employees ) ) {
				$html .= "</tr></table>";
				$pdf->writeHTML($html, true, false, true, false, '');
				$pdf->AddPage();
				$html = "<table width=\"100%\" border=\"1\" cellspacing=\"30\"><tr>";
			}
			elseif( $i % 1 == 0 && $i < count( $employees ) ) $html .= "</tr><tr>";
		}
		$html .= "</tr></table>";	
	}
	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.pdf") as $filename) {			
			@unlink($filename);
		}
	
	// output the HTML content	
	$pdf->writeHTML($html, true, false, true, false, '');	
	//Close and output PDF document
	$name = 'id_card_' . date('j-M-Y_h-iA') . '.pdf';
	$pdf->Output('tmp_report_file/' . $name, 'F');
	
	echo "1###$name";
	exit();
}





?>
 