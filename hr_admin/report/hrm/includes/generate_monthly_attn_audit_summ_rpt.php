<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}



if( $action == "monthly_attn_summery_audit" ) {	// Audit Summary
			
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and emp.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
	if ($txt_date==0) $txt_date=""; else $txt_date="and attnd_date='".convert_to_mysql_date($txt_date)."'";
	
	
	if ($department_id==0) $department_id="";
	else $department_id="where id='$department_id'";
	
	ob_start();	
	
	?>
	<div align="center" style="width:2450px; font-size:16px"><b><? echo $company_details[$cbo_company_id]['company_name']; ?></b></div>
	<table width="2450" style="font-size:10px;" cellpadding="0" cellspacing="0" border="1" class="rpt_table">
    
	<?
	$new_section=array();
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
    while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{	
	 $i=0;
	 //$sqls = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,atn.attnd_date FROM hrm_employee emp, hrm_attendance atn WHERE emp.emp_code=atn.emp_code and atn.attnd_date like '$date_part%' and emp.department_id=$row_dp[id] $location_id $cbo_company_id $section_id $subsection_id $designation_id $category group by atn.emp_code order by emp.section_id,CAST(emp.id_card_no AS SIGNED)";
 	 $sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
	 		  FROM 
			  	  hrm_employee emp
			  where 
				  emp.department_id=$row_dp[id] 
				  $location_id
				  $division_id 
				  $company_id 
				  $section_id 
				  $subsection_id 
				  $designation_id 
				  $category order by emp.section_id,CAST(emp.id_card_no AS SIGNED)";
			  
	 //echo $sqls; die;
	 $results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
	 while( $rows = mysql_fetch_assoc( $results ) ) 
	 {
	 	
		$sql = "SELECT * FROM hrm_attendance  WHERE  attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]' order by attnd_date asc";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
		if( mysql_num_rows($result)>0 )
	 	{
			
			if (in_array($rows['section_id'], $new_section))						
				{
					$i++;
				}
				else
				{	
					$section_name=$section_details[$rows['section_id']]['section_name'];
				?>	
			
					<thead>
						<tr>
							<th colspan=45>Department Name: <? echo $row_dp[department_name];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    Section Name: <? echo $section_name; ?></th>
						</tr>
						<tr>
							<th width='65'><b>Emp Code</b></th>
							<th width='50'><b>ID Card</b></th>
							<th width='180'><b>Emp Name</b></th>
							<th width='120'><b>Designation</b></th>
							<th width='60'><b>Join Date</b></th>
							<th width='30'><b>Days</b></th>				
							<?		
							for($j=1; $j<=$get_days;$j++)
							   {  ?>                        
									<th width='50'><b><? echo $j; ?></b></th>
							<? } ?>
					
							<th width='50'><b>Present</b></th>
							<th width='50'><b>Absent</b></th>
							<th width='50'><b>Late</b></th>
							<th width='50'><b>Leave</b></th>
							<th width='50'><b>Movement</b></th>
							<th width='50'><b>W. Day</b></th>
							<th width='50'><b>H. Day</b></th>
							<th width='50'><b>Pay Days</b></th> 
					   </tr>
				  </thead>
				  <tbody>							
				<?	
				
				$new_section[$i]=$rows[section_id];
				$i++;			
			}		
			
			// 1-31 days column----------------------------------
			$prt=0;
			$present=0;$absent=0;$leave=0;$wday=0;$pay_day=0;$cnt=0;$is_new_join=0;$st_date=$date_part."-01";
			while( $row = mysql_fetch_assoc( $result ) ) 
			{	
		
				
						if( $prt==0 )
						{
							$prt=1;
							if ($i%2==0) $bgcolor="#E9F3FF"; 
							else $bgcolor="#FFFFFF";
						  ?>
							<tr bgcolor="<? echo $bgcolor; ?>">
								<td width='65'><? echo $rows[emp_code]; ?></td>
								<td width='50'><? echo $rows[id_card_no];?></td>
								<td width='180'><? echo $rows[name];?></td>
								<td width='120'><? echo $designation_chart[$rows['designation_id']]['custom_designation'];?></td>
								<td width='60'><?php echo convert_to_mysql_date($rows[joining_date]);?></td>
								<td width='30'><? echo $get_days;?></td>
						   <?	
						}			
						
						
						if ($cnt==0)
						{
							$start_date=datediff( "d", $st_date, $row[attnd_date]);
							if( $start_date >1 )
							{
								for($mk=0; $mk<$start_date-1; $mk++)
								{
									$cnt++;
									?> <td width='50' align="center"><b> </b></td> <?
								}
							}
						}
						$cnt++;
						?> <td width='50' align="center"><b><? echo $row[status];?></b></td> <?
			   } //3rd while loop
					
					while($get_days!=$cnt){
						?> <td width='50' align="center"><b></b></td> <?
						$cnt++;
					}
					
					$d="count(*)";
					$dd="hrm_attendance";
					$ddd="status in ('P') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$present= return_field_value($d,$dd,$ddd);
					
					$ddd="status='A ' and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$absent= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('CL','ML','SL','EL','SpL') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$leave= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('D') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$late= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('MR') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$movement= return_field_value($d,$dd,$ddd);
					
					$ddd="status='W' and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$wday= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('H','GH','CH','FH') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$hday= return_field_value($d,$dd,$ddd);
					
					$ddd="status not in ('A','LWP') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$pay_day=return_field_value($d,$dd,$ddd);
					
					?>
                    
                    <td width='50'><? echo $present;?></td>
                    <td width='50'><? echo $absent?></td>
                    <td width='50'><? echo $late;?></td>
                    <td width='50'><? echo $leave;?></td>
                    <td width='50'><? echo $movement;?></td>
                    <td width='50'><? echo $wday;?></td>
                    <td width='50'><? echo $hday;?></td>
                    <td width='50'><? echo $pay_day;?></td>
            </tbody></tr>
                 
				 <?
					
				} //end if condition 			
													
		} //2nd while loop
		
	}//1st while loop
	
	?> 
    	</table>
	<?
				
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
	
	
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>