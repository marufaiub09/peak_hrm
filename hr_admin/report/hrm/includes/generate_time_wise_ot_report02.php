<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($location_details);
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


if($action=='time_wise_ot_report'){
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	/*list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	$table_width=250+($get_days*70);*/
	
	if($table_width<900) $table_width=900;
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id_con=""; else $section_id_con="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($department_id==0) $department_id=""; else	$department_id="and department_id='$department_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	
	//new
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(id_card_no, 5)";}
			
	ob_start();	
		
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:<? echo $table_width; ?>">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1">Time Slot Wise O T Report</font>
    </div>
    
    <table cellpadding="0" cellspacing="0" border="1" width="1200px" class="rpt_table" style="font-size:10px; border:1px solid #000000"> 
       <thead>        
            <tr>
            	<th width="40"><b>SL</b></th>
                <th width="100"><strong><b>SECTION</b></strong></th>
                <th width="100"><b>Total Worker</b></th>
                <th align="100">5.00 TO 7.00 <br> Up To 2 Hour</th>
                <th align="100">7.00 TO 8.00 <br> Up To 3 Hour</th>
                <th width="100">8.00 TO 9.00 <br> Up To 4 Hour</th>
                <th width="100">9.00 TO 10.00 <br> Up To 5 Hour</th>
                <th width="110">10.00 TO 11.00 <br> Up To 6 Hour</th>
                <th width="110">11.00 TO 12.00 <br> Up To 7 Hour</th>
                <th align="110">12.00 TO 01.00 <br> Up To 8 Hour</th>
                <th align="100"><b>01.00 TO ++</b></th>
                <th width="70" >OT Amount</th>
            </tr>
       </thead>
						
    <?	
	$s=1;
	/*$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 Group BY section_name ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		 $section_details[$row['id']] = mysql_real_escape_string($row['section_name']);*/
		 
		$sql_attn = "SELECT * FROM hrm_attendance WHERE section_id=$section_id and attnd_date='$fromdate'";
		$result_attn = mysql_query( $sql_attn ) or die( $sql_attn . "<br />" . mysql_error() );
		
		$arr_attn=array(); $total_worker=0; $upto_second=0;$upto_third=0; $upto_fourth=0; $upto_fifth=0; $upto_six=0; $upto_seven=0; $upto_eight=0; $upto_nine=0;
		
		while($att_row=mysql_fetch_array($result_attn)){
			if($att_row['total_over_time_min']<=120){ $upto_second++; }
			if($att_row['total_over_time_min']<=180 && $att_row['total_over_time_min']>120){ $upto_third++; }
			if($att_row['total_over_time_min']<=240 && $att_row['total_over_time_min']>180){ $upto_fourth++;}
			if($att_row['total_over_time_min']<=300 && $att_row['total_over_time_min']>240){ $upto_fifth++;}
			if($att_row['total_over_time_min']<=360 && $att_row['total_over_time_min']>300){ $upto_six++;}
			if($att_row['total_over_time_min']<=420 && $att_row['total_over_time_min']>360){ $upto_seven++;}
			if($att_row['total_over_time_min']<=480 && $att_row['total_over_time_min']>420){ $upto_eight++;}
			if($att_row['total_over_time_min']>480){ $upto_nine++;}
			//if($att_row['total_over_time_min']>$buyer_ot_min_comp)$att_row['total_over_time_min']=$att_row['total_over_time_min']-$buyer_ot_min_comp;
			//$hours[0] = abs((int)($att_row['total_over_time_min']/60)); //hours
			//$hours[1] = abs((int)($att_row['total_over_time_min']%60)); //minutes
			$ot_mins_cal=0;
			if($ot_fraction==1) //ot fraction allow
			{						
				$ot_mins_cal=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
			}
			if($ot_fraction==2) // no ot fraction
			{						
				$ot_mins_cal=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);					
			}
			if($ot_fraction==3) //at actual minutes
			{	
				$ot_mins_cal=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);					
				//$ot_mins_cal = 	$att_row['total_over_time_min'];				
			}
			$total_worker++;
			//$arr_attn[$att_row['attnd_date']]['total_over_time_min']=$total_hours;
		}
	?>
            <tr>
                <td width="40"><b><? echo $s; ?></b></td>
                <td rowspan="2" width="100" align="center"><b><? echo $section_details[$section_id]['section_name'];  //echo $section_details[$row['id']];?></b></td>
                <td width="100" align="center"> <? echo $total_worker; ?></td>
                <td align="100" align="center"> <? echo $upto_second; ?></td>
                <td align="100" align="center"> <? echo $upto_third; ?> </td>
                <td width="100" align="center"><? echo $upto_fourth; ?></td>
                <td width="100" align="center"><? echo $upto_fifth; ?></td>
                <td width="110" align="center"><? echo $upto_six; ?></td>
                <td width="110" align="center"><? echo $upto_seven; ?></td>
                <td align="110" align="center"><? echo $upto_eight; ?></td>
                <td align="100" align="center"><? echo $upto_nine; ?></td>
                <td width="70" >OT Amount</td>
            </tr>
            <tr>
                <td width="40"><b></b></td>
                <td width="100"></td>
                <td align="100"></td>
                <td align="100"></td>
                <td width="100"></td>
                <td width="100"></td>
                <td width="110"></td>
                <td width="110"></td>
                <td align="110"></td>
                <td align="100"></td>
                <td width="70" >OT Amount</td>
            </tr>
            <tr>
                <td  colspan="2" width="40" align="center"><b>TOTAL</b></td>
                <td width="100"></td>
                <td align="100"></td>
                <td align="100"></td>
                <td width="100"></td>
                <td width="100"></td>
                <td width="110"></td>
                <td width="110"></td>
                <td align="110"></td>
                <td align="100"></td>
                <td width="70" >OT Amount</td>
            </tr>
        </table>
       <br> 
	<?
	//$s++;
	//}
	
?>
	
<?	
		
   
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}


?>