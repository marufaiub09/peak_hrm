<?php

/*#####################################

Completed  By : Ekram
Dated         :17/12/2013 

#####################################*/


date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}


$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	

	//company info
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	//location info
	$location_info_sql="select * from lib_location order by id";
	$result = mysql_query( $location_info_sql ) or die( $location_info_sql . "<br />" . mysql_error() );
	$location_info_result = mysql_fetch_array($result);
	
	//salary grade wiese ot rate
	
		$sql="select * from  hrm_piece_rate_ot_mst";
		$result = mysql_query( $sql ) or die( $sql."<br />".mysql_error() );
		$overtime_rate_grade=array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$overtime_rate_grade[$row['grade_id']]=$row['rate'];
		}

if( $action == "off_day_ot_report" ) { //Off Day Ot Report
	
	
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$search_off_day = $cbo_salary_periods[0];
	$search_date=explode("-",$search_off_day);
	$off_dates=$search_date[0]."-".$search_date[1]."-"."%%";
	$cur_month = $months["'"+$search_date[1]+"'"];
	
//dynamic table width
	
	$rpt_company=$company_id;
	$new_sec=$section_id;
	$sal_based=$cbo_salary_based;
	if ($cbo_company_id==0) $company_id=""; else $company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else	$department_id=" and  a.department_id in ($department_id)";
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	//if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ($id_card_no)";
	//if ($emp_code=="") $emp_code=""; else $emp_code="and b.emp_code in ($emp_code)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and b.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	//if ($ot_amount_id==0 || $ot_amount_id==1) $ot_amount_id =""; else $ot_amount_id ="and a.total_over_time_min!=0";
	//echo $salary_based;die;
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="b.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="b.company_id,b.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="b.company_id,b.location_id,b.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="b.company_id,b.location_id,b.division_id,b.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,b.emp_code,";}

	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-11)." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-11)." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-11)." CAST(b.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-11)." CAST(right(trim(b.id_card_no), 5)as SIGNED)";}	 
	//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$r=1;
	$total_ot=0;
	$ot_hr=0;
	$show_ot=0;
	ob_start();

	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$rpt_company' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$adjust_out_time=$res['adjust_out_time'];
			$applicable_above_salary=$res['applicable_above_salary'];
			$in_out_time_format=$res['in_out_time_format']; 
			$adjust_in_time = $res['adjust_in_time'];
		}
	
		
		
			

		$sql="select emp_code,salary_periods,gross_salary,basic_salary,over_time_rate from hrm_salary_mst where salary_periods='$search_off_day'";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql."<br />".mysql_error() );
		$sal_details_gross=array();
		$sal_details_basic=array();
		$overtime_rate=array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$sal_details_gross[$row['emp_code']] =$row['gross_salary'];
			$sal_details_basic[$row['emp_code']] =$row['basic_salary'];
			$overtime_rate[$row['emp_code']]=$row['over_time_rate'];
		}
		//ot distribution table	
		$sql="SELECT emp_code,ot_date,total_ot from hrm_off_day_ot where ot_date like '$off_dates'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$off_day_ot_distribution = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$off_day_ot_distribution[$row[emp_code]][$row['ot_date']]=$row[total_ot];
		}
		
		/*$sql="SELECT a.emp_code,a.attnd_date,a.total_over_time_min,b.emp_code,CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name,b.id_card_no,b.designation_id,b.location_id 
		FROM hrm_attendance a,hrm_employee b 
		WHERE a.status in('GH','FH','CH','W') and
			a.is_regular_day=0 and
			a.attnd_date like '$off_dates' and
			a.emp_code=b.emp_code and 
			b.status_active=1 and a.`emp_code` in(0005926,0005923,0005922,0005919)
			$cbo_company_id 
			$location_id
			$division_id 
			$section_id 
			$subsection_id  
			$designation_id 
			$department_id
			$id_card_no 
			$emp_code";*/
		//echo $sql;die;
		
		$sql="SELECT a.emp_code,a.attnd_date,a.total_over_time_min,b.emp_code,CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name,b.id_card_no,b.designation_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,b.salary_type_entitled,b.salary_grade  
		FROM hrm_attendance a,hrm_employee b 
		WHERE a.status in('GH','FH','CH','W') and
			a.is_regular_day=0 and
			a.attnd_date like '$off_dates' and
			a.emp_code=b.emp_code and 
			b.status_active=1
			$cbo_company_id 
			$location_id
			$division_id 
			$section_id 
			$subsection_id  
			$designation_id 
			$department_id
			$id_card_no 
			$salary_based
			$emp_code $orderby";
			//echo $sql;die;$ot_amount_id
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$off_day_ot_minute = array();
		$off_day_details = array();
		
		while( $row = mysql_fetch_assoc( $result ) ) {
		//$off_day_details[] = ($row['attnd_date']);
		$off_day_ot_minute[$row[emp_code]][$row['attnd_date']]=$row[total_over_time_min];
		$off_day_details[] = ($row['attnd_date']);
			if($emp_array[$row[emp_code]]=="")
			{
				$emp_array[$row[emp_code]]=$row[emp_code];
				$emp_data_array[$row[emp_code]]['id_card_no']=$row[id_card_no];
				$emp_data_array[$row[emp_code]]['name']=$row[name];
				$emp_data_array[$row[emp_code]]['designation_id']=$row[designation_id];
				$emp_data_array[$row[emp_code]]['location_id']=$row[location_id];
				$emp_data_array[$row[emp_code]]['division_id']=$row[division_id];
				$emp_data_array[$row[emp_code]]['department_id']=$row[department_id];
				$emp_data_array[$row[emp_code]]['section_id']=$row[section_id];
				$emp_data_array[$row[emp_code]]['subsection_id']=$row[subsection_id];
				$emp_data_array[$row[emp_code]]['salary_grade']=$row[salary_grade];
				$emp_data_array[$row[emp_code]]['salary_type_entitled']=$row[salary_type_entitled];
			}
		}
		//print_r($emp_array);
		//echo $table_width=(count($off_day_details)+11);die;
		//echo count($off_day_details);die;
		//print_r( count(array_unique($off_day_details)));die;
$grand_total_amount=0;
$amount=0;
?>
    
  <style>
	.verticalText 
	{               
		writing-mode: tb-rl;
		 filter: flipv fliph;
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
		-o-transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		transform: rotate(270deg);
		width: 3em;		 
	}
</style>
<?   //echo $table_width=(((count(array_unique($off_day_details))*20)+760)); //920 ?>
    
     <table cellpadding="0" cellspacing="0" border="1" width="<? echo $table_width;?>" align="center"  class="rpt_table"  rules="all"> 
            <thead> 
               <tr>
                    <th colspan="<? if($sal_based==1){echo (count(array_unique($off_day_details))+10);}else{ echo (count(array_unique($off_day_details))+10); } ?>" align="center" bordercolor="#FFFFFF"><br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br /><br />
                     <font size="2"><? echo $location_info_result["address"];?><br /><br />
                    Off-day overtime Payment Sheet For The Month of <? echo $cur_month; ?> <? echo $search_date[0]; ?></font><br /><br />
                    </th>
                </tr>
                <tr height="70" style="font-size:10px;">
                    <th width="20">SL</th>
                    <th width="80">ID No</th>
                    <th width="90">Name</th>
                    <th width="90">Designation</th>
                     <? 
					if($sal_based==1){ ?>
                    <th width="50">Grade</th>
                    <? } ?>
                    <? if($sal_based!=1){ ?>
                    <th width="70">Gross Salary</th>
                    <th width="70">Basic Salary</th>
                    <?php
					}
					$array = array_unique($off_day_details);
					sort($array);
					foreach($array  AS $key=>$value ){ ?>
					<th width="20" style="font-size:9px;"><div class="verticalText"><?php echo convert_to_mysql_date($value); ?></div></th>
					<?php 
					} 
					?>
                    <th width="50">Total OT</th>
                     <? 
					if($sal_based==1){ ?>
                    <th width="50">Rate</th>
                    <? } ?>
                    <th width="70">Amount</th>
                    <th width="110">Signature</th>
                    <th><div class="verticalText">Remarks</div></th>             
                </tr>   
            </thead>
            <?
			//echo $section_details[$new_sec];die;
			?>
			<!--<tr><td colspan="<? //if($sal_based==1){echo (count(array_unique($off_day_details)))+10;}else{ echo (count(array_unique($off_day_details)))+10;} ?>" ><b><? //echo $section_details[$new_sec]; ?></b></td></tr>-->
			
			<?
	
	//print_r($emp_array);die;
	foreach( $emp_array as $emp_code) 
	{
		
		  $sl++; 
			$total_ot=0;
			
		 ///start header print---------------------------------//
		if($sl==1)
		{
		
		$location_arr[$emp_data_array[$emp_code]['location_id']]=$emp_data_array[$emp_code]['location_id'];
		$division_arr[$emp_data_array[$emp_code]['division_id']]=$emp_data_array[$emp_code]['division_id'];
		$department_arr[$emp_data_array[$emp_code]['department_id']]=$emp_data_array[$emp_code]['department_id'];
		$section_arr[$emp_data_array[$emp_code]['section_id']]=$emp_data_array[$emp_code]['section_id'];
		$subsection_arr[$emp_data_array[$emp_code]['subsection_id']]=$emp_data_array[$emp_code]['subsection_id'];
		
		}
		   //end header print
		if($sl!=1)
			{
			$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				if(in_array($emp_data_array[$emp_code]['location_id'],$location_arr) && $status_loc==1)
				{
					if(in_array($emp_data_array[$emp_code]['division_id'],$division_arr) && $status_divis==1)
					{
						if(in_array($emp_data_array[$emp_code]['department_id'],$department_arr) && $status_dept==1)
						{
							if(in_array($emp_data_array[$emp_code]['section_id'],$section_arr) && $status_sec==1)
							{ 
								if(in_array($emp_data_array[$emp_code]['subsection_id'],$subsection_arr) && $status_subsec==1)
								{}
									else if($status_subsec==1)
									{
									$subsection_arr[$emp_data_array[$emp_code]['subsection_id']]=$emp_data_array[$emp_code]['subsection_id'];
									$new_subsec=1;
									}
									}
									else if($status_sec==1)
									{
									$section_arr[$emp_data_array[$emp_code]['section_id']]=$emp_data_array[$emp_code]['section_id'];
									$subsection_arr=array();
									$subsection_arr[$emp_data_array[$emp_code]['subsection_id']]=$emp_data_array[$emp_code]['subsection_id'];
									$new_sec=1;
									$new_subsec=1;
									}
									}
									else if($status_dept==1)
									{
									$department_arr[$emp_data_array[$emp_code]['department_id']]=$emp_data_array[$emp_code]['department_id'];
									$section_arr=array();
									$subsection_arr=array();
									$section_arr[$emp_data_array[$emp_code]['section_id']]=$emp_data_array[$emp_code]['section_id'];
									$subsection_arr[$emp_data_array[$emp_code]['subsection_id']]=$emp_data_array[$emp_code]['subsection_id'];
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
									}
									}//division
									else if($status_divis==1)
									{
									$division_arr[$emp_data_array[$emp_code]['division_id']]=$emp_data_array[$emp_code]['division_id'];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$emp_data_array[$emp_code]['department_id']]=$emp_data_array[$emp_code]['department_id'];
									$section_arr[$emp_data_array[$emp_code]['section_id']]=$emp_data_array[$emp_code]['section_id'];
									$subsection_arr[$emp_data_array[$emp_code]['subsection_id']]=$emp_data_array[$emp_code]['subsection_id'];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
									}//division else
									}//location
									else if($status_loc==1)
									{
									$location_arr[$emp_data_array[$emp_code]['location_id']]=$emp_data_array[$emp_code]['location_id'];
									$division_arr=array();
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$division_arr[$emp_data_array[$emp_code]['division_id']]=$emp_data_array[$emp_code]['division_id'];
									$department_arr[$emp_data_array[$emp_code]['department_id']]=$emp_data_array[$emp_code]['department_id'];
									$section_arr[$emp_data_array[$emp_code]['section_id']]=$emp_data_array[$emp_code]['section_id'];
									$subsection_arr[$emp_data_array[$emp_code]['subsection_id']]=$emp_data_array[$emp_code]['subsection_id'];
									$new_loc=1;
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
									}//location else
		
								 }
			//header print here 
			$c_part_1="";
			if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
			{
			
				if($status_loc==1)
					{					
					$c_part_1 .= "  Location :  ".$location_details[$emp_data_array[$emp_code]['location_id']]."  ,";
					}				
				if($status_divis==1)
					{
					$c_part_1 .= "  Division :  ".$division_details[$emp_data_array[$emp_code]['division_id']]."  ,";
					}
				if($status_dept==1)
					{
					$c_part_1 .= "  Department :  ".$department_details[$emp_data_array[$emp_code]['department_id']]."  ,";
					}
				if($status_sec==1)
					{
					$c_part_1 .= "  Section :  ".$section_details[$emp_data_array[$emp_code]['section_id']]."  ,";
					}
				if($status_subsec==1)
					{
					$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp_data_array[$emp_code]['subsection_id']]."  ,";
					}
			
			
				if($c_part_1!='')
					{
					$i=0;$sl=1;
					?><tr><td colspan="<? if($sal_based==1)
					{echo (count(array_unique($off_day_details))+12);}else{ echo (count(array_unique($off_day_details))+10); } ?>" >
                    <b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
					}		
			}
	  
	 	 	if ($r%2==0) $bgcolor="#EEEEEE"; else $bgcolor="#FFFFFF"; 
			 
			$array = array_unique($off_day_details);
			sort($array); $total_ot=0; $html='';
			foreach($array AS $key=>$value )
			{ 
                $distributed_ot=$off_day_ot_distribution[$emp_code][$value];
                $attn_ot=get_buyer_ot_hr($off_day_ot_minute[$emp_code][$value],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
                if($distributed_ot > 0)
				{ 
                    $show_ot=$attn_ot-$distributed_ot;
                }
				else{
                    $show_ot=$attn_ot;
                }
				
				$html.='<td style="font-size:9px;" align="center" valign="middle">'.$show_ot.'</td>';
				
				$total_ot+=$show_ot;
			}
			if(($ot_amount_id==2 && $total_ot>0) || $ot_amount_id==1)
			{
		    ?>
				<tr bgcolor="<? echo $bgcolor;?>" height="60">
					<td  align="center" valign="middle"><? echo $r;?> </td>
                     <td  valign="middle"><? echo $emp_data_array[$emp_code]['id_card_no']; ?></td>
					<td  valign="middle"><? echo $emp_data_array[$emp_code]['name'];?></td>
					<td  valign="middle"><? echo $designation_chart[$emp_data_array[$emp_code]['designation_id']];?></td>
                    <?  if($sal_based==1){ ?>
                     <td  valign="middle"><? echo $emp_data_array[$emp_code]['salary_grade']; ?></td>
                     <? } ?>
                     <? if($sal_based!=1){ ?>
                    <td  align="center" valign="middle"><? echo $sal_details_gross[$emp_code]; ?> </td>
                    <td  align="center" valign="middle"><? echo $sal_details_basic[$emp_code]; ?> </td> 
                    <?php 
			 }
		echo 	$html;	?>
					<td   align="center" valign="middle"><? echo  $ot_hr=$total_ot; ?></td>
                     <?  if($sal_based==1){ ?>
                     <td  valign="middle"><? echo $ot_rate=$overtime_rate_grade[$emp_data_array[$emp_code]['salary_grade']]; ?></td>
                     <? } ?>
                    <td  align="center" valign="middle"><b><?
						
						
						 if($emp_data_array[$emp_code]['salary_type_entitled']==1){
							 $ot_rate=$overtime_rate_grade[$emp_data_array[$emp_code]['salary_grade']];
							 $amounts=($ot_rate*$ot_hr);
							  $amount=round($amounts);
						 }else{
							 $ot_rate=$overtime_rate[$emp_code];
							 $amount=round($ot_rate*$ot_hr);
						 }
						
						//$grand_amt=($ot_rate*$ot_hr);
						//echo $amount=number_format($ot_rate*$ot_hr);
					
					echo $amount;
					 ?></b></td>
                    <td  valign="middle">&nbsp;</td>
                    <td  valign="middle">&nbsp;</td>
				</tr>
 	<?
    			$grand_total_amount+=$amount;
				$r++;
			}
	}

?>

                <tr style="font-weight:bold" bgcolor="#CCCCCC">
                    <td align="center" colspan="<? if($sal_based==1){echo (count(array_unique($off_day_details)))+7;}else{ echo (count(array_unique($off_day_details)))+7;} ?>">Grand Total</td>
                    <td align="center"><? echo number_format($grand_total_amount); ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                
                <tfoot>
                	<tr>
                    	<td colspan="<? if($sal_based==1){echo (count(array_unique($off_day_details))+10);}else{ echo (count(array_unique($off_day_details))+10); } ?>"><? echo signeture_table(8,$rpt_company,'') ;  ?></td>
                    </tr>
                </tfoot>
</table>		 

	
 	
<?	
	
	//echo signeture_table(8,$rpt_company,$table_width) ; 

//previous file delete code-----------------------------//

$html = ob_get_contents();
	ob_clean();		
	
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}





//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}


?>
