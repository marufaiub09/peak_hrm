<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");
include("../../../../includes/array_function.php");
include("../../../../includes/common_functions.php");

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}

extract($_GET);

if($type=="ot_based_on_report_list")
{
	// Search by id_card_no
	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
	//echo $id_card_no;die;
	//echo "su..re";die;
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		$ot_fraction = $res['allow_ot_fraction'];
		$ot_start_min = $res['ot_start_minute'];
		$one_hr_ot_unit = $res['one_hour_ot_unit'];
		$adjust_out_time=$res['adjust_out_time'];
		$applicable_above_salary=$res['applicable_above_salary'];
		$in_out_time_format=$res['in_out_time_format']; 
		$adjust_in_time = $res['adjust_in_time'];
	}
	
	
	$sql_shift="SELECT ot_date,sum(budgeted_ot) as budgeted_ot,emp_code FROM ot_requisition_dtl group by emp_code, ot_date";
	$result_shift=mysql_query($sql_shift) or die( $sql_shift . "<br />" . mysql_error() );
	$ot_requisition_array=array();
	while($emp_shift=mysql_fetch_assoc($result_shift))
	{
		$ot_requisition_array[$emp_shift[emp_code]][$emp_shift[ot_date]]=$emp_shift[budgeted_ot];
	}
	//print_r($ot_requisition_array);die;
	 //echo $new_over_total=get_buyer_ot_hr($emp['req_ot_org'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
	 
	if ($category_id	== '')	$category_id	= "and emp.category in(0,1,2,3,4)";	else	$category_id	= "and emp.category='$category_id'";
	if ($company_id		== 0)	$company_id		= "";		else	$company_id		= "and a.company_id='$company_id'";
	if ($location_id	== 0) 	$location_id	= ""; 		else 	$location_id	= "and a.location_id='$location_id'";
	if ($division_id	== 0 || $division_id	== '')		$division_id	= "";		else	$division_id	= "and a.division_id in($division_id)";
	if ($department_id  == 0 || $department_id	== '')		$department_id	= "";		else	$department_id	= "and a.department_id in($department_id)";
	if ($section_id		== 0 || $section_id	== '') 			$section_id		= ""; 		else 	$section_id		= "and a.section_id in($section_id)";
	if ($subsection_id	== 0 || $subsection_id	== '')		$subsection_id	= "";		else	$subsection_id	= "and a.subsection_id in($subsection_id)";
	if ($designation_id == 0 || $designation_id	== '')		$designation_id	= "";		else	$designation_id	= "and a.designation_id in($designation_id)";
	if ($id_card		=="")	$id_card_no=""; else $id_card_no="and emp.id_card_no in ($id_card_no)";
	if ($emp_code		=="") 	$emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";
	if ($cbo_salary_based=="") 	$salary_based=""; else $salary_based="and emp.salary_type_entitled='$cbo_salary_based'";
	
	/*
		if ($category=='') $category=" and emp.category in (0,1,2,3,4) "; else $category=" and emp.category='$category'";
		if ($company_id=='') $company_id =""; else $company_id ="and a.company_id='$company_id'";
		if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
		if ($division_id==0) $division_id=""; else $division_id="and a.division_id in ($division_id)";
		if ($department_id==0) $department_id=""; else $department_id="and a.department_id in ($department_id)";
		if ($section_id==0) $section_id=""; else $section_id="and a.section_id  in ($section_id)";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
		if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id  in ($designation_id) ";
		if ($id_card=="") $id_card_no=""; else $id_card_no="and emp.id_card_no in ($id_card_no)";
		if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";
		if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and emp.salary_type_entitled='$cbo_salary_based'";
	*/	
		//new add group by order by
		$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
		
		if($group_by_id==0){$groupby="emp.emp_code,";}
		else if($group_by_id==1){$status_com=1;$groupby="a.company_id,a.emp_code,";}
		else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,a.emp_code,";}
		else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,a.emp_code,";}
		else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.emp_code,";}
		else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.emp_code,";}
		else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.emp_code,";}
		
		
		if($order_by_id==0){$orderby="order by  CAST(right(trim(id_card_no), $id_card_no_ord)as SIGNED)";}	 
		else if($order_by_id==1){$orderby="order by  CAST(designation_level as SIGNED)";}	 
		else if($order_by_id==2){$orderby="order by  CAST(emp_code as SIGNED)";}	 
		else if($order_by_id==3){$orderby="order by  CAST(right(trim(id_card_no), $id_card_no_ord)as SIGNED)";}
		else if($order_by_id==4){$orderby="order by  sign_in_time ASC";}	 
		else if($order_by_id==5){$orderby="order by  sign_in_time DESC";}
		else if($order_by_id==6){$orderby="order by  sign_out_time ASC";}	 
		else if($order_by_id==7){$orderby="order by  sign_out_time DESC";}	 
		
		//echo $orderby;die;
		$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
		$location_arr=array();
		$division_arr=array();
		$department_arr=array();
		$section_arr=array();
		$subsection_arr=array();
		$sl=0;
	
   $sql = "SELECT CONCAT(first_name, ' ',middle_name, ' ',last_name) AS name,emp_code,id_card_no,designation_id from hrm_employee WHERE is_deleted = 0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_info_name = array();
	$emp_info_idcard = array();
	$emp_info_designation = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
	    $emp_info_name[$row['emp_code']]=$row['name'];
		$emp_info_idcard[$row['emp_code']]=$row['id_card_no'];
		$emp_info_designation[$row['emp_code']]=$row['designation_id'];
	}
	
	$sql_att = "SELECT emp_code,sign_in_time,sign_out_time,is_next_day,r_sign_in_time,r_sign_out_time from  hrm_attendance ORDER BY emp_code ASC";
	$result_att = mysql_query( $sql_att ) or die( $sql_att . "<br />" . mysql_error() );
	
	$sign_in_time_arr = array();
	$sign_out_time_arr = array();
	$is_next_day_arr = array();
	$r_sign_in_time_arr = array();
	$r_sign_out_time_arr = array();
	while( $row = mysql_fetch_assoc( $result_att ) ) {
		
		$r_sign_in_time_arr[$row['emp_code']]=$row['r_sign_in_time'];
	}
	

		/*$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.*,a.department_id,a.section_id,a.subsection_id,a.sign_in_time,a.sign_out_time,a.r_sign_in_time,a.r_sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min,a.policy_shift_id as att_policy_shift_id,a.req_out_time_org,a.req_ot_org,a.r_out_time_buffer,a.is_next_day FROM hrm_attendance a, hrm_employee emp  
		WHERE 
		a.emp_code=emp.emp_code and 
		emp.is_deleted=0 and 
		emp.status_active=1 and
		a.r_out_time_buffer < a.req_ot_org and
		a.attnd_date between '$txt_from_date' and '$txt_to_date'
		$category
		$company_id 
		$location_id 
		$division_id 
		$department_id 
		$section_id 
		$subsection_id
		$designation_id
		$emp_code
		group by $dynamic_groupby $orderby
		";*/
		//and a.req_ot_org>'$ot_start_min'
		//a.total_over_time_min=0
		//a.r_out_time_buffer < a.req_ot_org and 
				
		 $sql = "(SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.category,emp.emp_code,emp.designation_level,emp.ot_entitled,a.emp_code,a.department_id,a.section_id,a.subsection_id,a.sign_in_time,a.sign_out_time,a.r_sign_in_time,a.r_sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min,a.policy_shift_id as att_policy_shift_id,a.req_out_time_org,a.req_ot_org,a.r_out_time_buffer,a.is_next_day,c.budgeted_ot,c.ot_date FROM hrm_attendance a, hrm_employee emp,ot_requisition_dtl c 
		WHERE 
		a.emp_code=emp.emp_code and 
		a.emp_code=c.emp_code and
		emp.is_deleted=0 and 
		emp.status_active=1 and 
		a.attnd_date between '$txt_from_date' and '$txt_to_date'
		$category_id
		$company_id 
		$location_id 
		$division_id 
		$department_id 
		$section_id 
		$subsection_id
		$designation_id
		$salary_based
		$id_card_no
		$emp_code
		group by $dynamic_groupby)
		UNION
		(SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.category,emp.emp_code,emp.designation_level,emp.ot_entitled,a.emp_code,a.department_id,a.section_id,a.subsection_id,a.sign_in_time,a.sign_out_time,a.r_sign_in_time,a.r_sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min,a.policy_shift_id as att_policy_shift_id,a.req_out_time_org,a.req_ot_org,a.r_out_time_buffer,a.is_next_day,'','' FROM hrm_attendance a, hrm_employee emp  
		WHERE 
		a.emp_code=emp.emp_code and 
		emp.is_deleted=0 and 
		emp.status_active=1 and
		$ot_start_min < a.req_ot_org and
		a.total_over_time_min=0 and
		a.attnd_date between '$txt_from_date' and '$txt_to_date'
		$category_id
		$company_id 
		$location_id 
		$division_id 
		$department_id 
		$section_id 
		$subsection_id
		$designation_id
		$salary_based
		$id_card_no
		$emp_code
		group by $dynamic_groupby) $orderby
		";
				
		
		/*echo $sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.*,a.department_id,a.section_id,a.subsection_id,a.sign_in_time,a.sign_out_time,a.r_sign_in_time,a.r_sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min,a.policy_shift_id as att_policy_shift_id,a.req_out_time_org,a.req_ot_org,a.r_out_time_buffer FROM hrm_attendance a, hrm_employee emp 
		WHERE 
		a.emp_code=emp.emp_code and  
		emp.is_deleted=0 and 
		emp.status_active=1 and 
		a.attnd_date between '$txt_from_date' and '$txt_to_date'
		$category
		$company_id 
		$location_id 
		$division_id 
		$department_id 
		$section_id 
		$subsection_id
		$designation_id
		$emp_code
		group by $dynamic_groupby $orderby";*/
		
		//echo $sql;
		
		//die;
		$result=mysql_query($sql) or die( $sql . "<br />" . mysql_error() );
		ob_start();
		
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{?>
        <div style="width:1000px;" id="scroll_body_topheader" align="left">
        
        </div>
        <br />
       
        <?php                  
			$i=1;
			
			$ot_as_per_emp=0;
			$over_reqn_emp=0;
			$less_reqn_emp=0;
			$no_ot_emp=0;
			$wethout_reqn_emp=0;
			
			$ot_group_array=array();
			
			while($emp=mysql_fetch_assoc($result))
			{
		    	$sl++;
	  
				//echo $emp[attnd_date]; print_r($ot_requisition_array[$emp[emp_code]][$emp[attnd_date]]);
				$emp_budgeted_ot=$ot_requisition_array[$emp[emp_code]][$emp[attnd_date]];
				//print_r($emp_budgeted_ot);
				
				
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$bhour=floor($emp_budgeted_ot/60);
				$bmin=$emp_budgeted_ot%60;
				
				$ahour=floor($emp['total_over_time_min']/60);
				$amin=$emp['total_over_time_min']%60;
				
				$xhour=floor($emp['req_ot_org']/60);
				$xmin=$emp['req_ot_org']%60;
				
				$bhour_sum +=$bhour;
				$bmin_sum +=$bmin;
				$bmin_sum_houre=floor($bmin_sum/60);
				$bhour_sum_total=$bhour_sum+$bmin_sum_houre;
				$bmin_sum_minit=$bmin_sum%60;
				
				
                        $as_per_emp_amount=0;
                        //asper
                        if($emp_budgeted_ot>0)
                        {
                            if( $emp_budgeted_ot == $emp['req_ot_org'] )
                             { 
                                $ot_as_per_emp++; 
                                 $new_asper_total=get_buyer_ot_hr( $emp_budgeted_ot,$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
								 
                                $ot_group_array_asper[$emp[emp_code]][$emp[attnd_date]]=$new_asper_total;
								
								$sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_out_time];
								$r_sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[r_sign_out_time];
								$in_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_in_time];
								$is_next_day_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[is_next_day];
								
                                $new_asper_tot=explode(".",$new_asper_total);
                                $new_asper_tot_houre=floor($new_asper_tot[1]/60);
                                $new_asper_tot_ho=$new_asper_tot_houre+$new_asper_tot[0];
                                $new_asper_tot_mi=$new_asper_tot[1]%60;
                                
                                $asper_tot_ho +=$new_asper_tot_ho;
                                $asper_tot_mi +=$new_asper_tot_mi;
                                
                                $new_asper=floor($asper_tot_mi/60);
                                $asper_ho=$new_asper+$asper_tot_ho;
                                $asper_mi=$asper_tot_mi%60;
                                
                                 $sum_new_asper_total=($asper_ho*60)+$asper_mi;
                                
                                $as_per_man_emp_code=$emp['emp_code'] ;
                                $sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$as_per_man_emp_code'";
                                $result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
                                while($emp_salary=mysql_fetch_assoc($result_salary))
                                {
                                    $as_per_basic_sal=$emp_salary['basic_salary'];
                                }
                                $as_per_emp_amount=($as_per_basic_sal/(208*60))*$sum_new_asper_total;
                                $as_per_emp_amount_total +=$as_per_emp_amount;
                             } 
                             else echo ""; 	
                        }
                        else echo ""; 
                     
                    //OVER=0;
                        $emp_amount=0;
                        if($emp_budgeted_ot>0)
                        {
                            if( $emp['req_ot_org'] > $emp_budgeted_ot ) 
                            {
                                $over_reqn_emp++;
                                $new_over_total=get_buyer_ot_hr($emp['req_ot_org'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
								
                                $ot_group_array_over[$emp[emp_code]][$emp[attnd_date]]=$new_over_total;
								
								$sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_out_time];
								$r_sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[r_sign_out_time];
								$in_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_in_time];
								$is_next_day_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[is_next_day];
								
                                $new_over_tot=explode(".",$new_over_total);
                                $new_over_tot_houre=floor($new_over_tot[1]/60);
                                $new_over_tot_ho=$new_over_tot_houre+$new_over_tot[0];
                                $new_over_tot_mi=$new_over_tot[1]%60;
                                
                                $over_tot_ho +=$new_over_tot_ho;
                                $over_tot_mi +=$new_over_tot_mi;
                                
                                $new_over=floor($over_tot_mi/60);
                                $over_ho=$new_over+$over_tot_ho;
                                $over_mi=$over_tot_mi%60;
                            
                                $sum_new_over_total=($over_ho*60)+$over_mi;
                               
                                    $over_man_emp_code=$emp['emp_code'] ;
                                    $sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$over_man_emp_code'";
                                    $result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
                                    while($emp_salary=mysql_fetch_assoc($result_salary))
                                    {
                                        $basic_sal=$emp_salary['basic_salary'];
                                    }
                                    $emp_amount=($basic_sal/(208*60))*$sum_new_over_total;//$emp['req_ot_org'];
                                    $emp_amount_over_tot +=$emp_amount;
                            } 
                            else "";
                        }
                        else "";
                        
                    //LESS
                         $emp_amount_less=0;
                        if($emp_budgeted_ot>0)
                        {
                            if( $emp_budgeted_ot > $emp['total_over_time_min'] && $emp['total_over_time_min'] > 0 ) 
                            { 
								$less_reqn_emp++;
								$new_less_total=get_buyer_ot_hr($emp['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
								
								$ot_group_array_less[$emp[emp_code]][$emp[attnd_date]]=$new_less_total;
								
								$sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_out_time];
								$r_sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[r_sign_out_time];
								$in_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_in_time];
								$is_next_day_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[is_next_day];
								
								$new_less_tot=explode(".",$new_less_total);
								$new_lesst_tot_houre=floor($new_less_tot[1]/60);
								$new_less_tot_ho=$new_lesst_tot_houre+$new_less_tot[0];
								$new_less_tot_mi=$new_less_tot[1]%60;
								
								$less_tot_ho +=$new_less_tot_ho;
								$less_tot_mi +=$new_less_tot_mi;
								
								$new_less=floor($less_tot_mi/60);
								$less_ho=$new_less+$less_tot_ho;
								$less_mi=$less_tot_mi%60;
								
								$sum_new_less_total=($less_ho*60)+$less_mi;
                                        
                                    $less_man_emp_code=$emp['emp_code'] ;
                                    $sql_salary_less="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$less_man_emp_code'";
                                    $result_salary_less=mysql_query($sql_salary_less) or die( $sql_salary_less . "<br />" . mysql_error() );
                                    $basic_sal_less=0;
                                    while($emp_salary_less=mysql_fetch_assoc($result_salary_less))
                                    {
                                        $basic_sal_less=$emp_salary_less['basic_salary'];
                                    }
                                    $basic_sal_less;
                                    $emp_amount_less=($basic_sal_less/(208*60))*$sum_new_less_total;//$emp['total_over_time_min'];
                                    $emp_amount_less_tot +=$emp_amount_less;
                            } 
                            else ""; 
                        }else ""; 
                         
                        //NO OT
                        if( $emp_budgeted_ot>0)
                        {
                            if( $emp['total_over_time_min']==0 )
                            { 
                                 $no_ot_emp++; 
                                 //echo "0"; 
								 $ot_group_array_noot[$emp[emp_code]][$emp[attnd_date]]=0;
								 
								 $sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_out_time];
								$r_sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[r_sign_out_time];
								$in_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_in_time];
								$is_next_day_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[is_next_day];
								//print_r($is_next_day_arr[$emp[emp_code]][$emp[attnd_date]]);
                            }
                            else "";
                        }else ""; 
                        
                        if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
                        {
                            //wethout=0
                            if( $emp['total_over_time_min']>0 || $emp['req_ot_org']>0) 
                            { 
                             
                                $without_man_emp_code=$emp['emp_code'] ;
                                if($emp['ot_entitled']==1)
                                {
                                    if($emp['r_out_time_buffer']<$emp['req_ot_org'])
                                    {
                                        $wethout_reqn_emp++;
                                        $new_without_total=get_buyer_ot_hr($emp['req_ot_org'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
										
                                        $ot_group_array_without[$emp[emp_code]][$emp[attnd_date]]=$new_without_total;
										
										$sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_out_time];
										$r_sign_out_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[r_sign_out_time];
										$in_time_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[sign_in_time];
										$is_next_day_arr[$emp[emp_code]][$emp[attnd_date]]=$emp[is_next_day];
										//print_r($in_time_arr[$emp[emp_code]][$emp[attnd_date]]);
										
                                        $new_without_tot=explode(".",$new_without_total);
                                        $new_without_tot_houre=floor($new_without_tot[1]/60);
                                        $new_without_tot_ho=$new_without_tot_houre+$new_without_tot[0];
                                        $new_without_tot_mi=$new_without_tot[1]%60;
                                        
                                        $new_with_tot_ho +=$new_without_tot_ho;
                                        $new_with_tot_mi +=$new_without_tot_mi;
                                        
                                        $new_with=floor($new_with_tot_mi/60);
                                        $without_ho=$new_with+$new_with_tot_ho;
                                        $without_mi=$new_with_tot_mi%60;
                                        
                                        $sum_new_without_total=($without_ho*60)+$without_mi;
                                        
                                        $sql_salary_without="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$without_man_emp_code'";
                                        $result_salary_without=mysql_query($sql_salary_without) or die( $sql_salary_without . "<br />" . mysql_error() );
                                        $basic_sal_less=0;
                                        while($emp_salary_without=mysql_fetch_assoc($result_salary_without))
                                        {
                                        $basic_sal_without=$emp_salary_without['basic_salary'];
                                        }
                                        $basic_sal_without;
                                        $emp_amount_without=($basic_sal_without/(208*60))*$sum_new_without_total; //$emp['req_ot_org'];
                                        $emp_amount_without_tot +=$emp_amount_without;
                                 
                                    }
                                }
                            }
                        }
                    
                    ?> 
				<?
				$i++; 
				
			} //while loop end
			
			$header_arr=array(1=>"OT As Reqn",2=>"Over the Reqn",3=>"Less than Reqn",4=>"No OT from list",5=>"OT Without Reqn");
	
			 ?>
             
             <div class="form_caption">OT Details</div>
             
        <div align="left" class=""><strong>OT As Per Requisition:</strong></div>
        <table cellspacing="0" cellpadding="0" width="1100px" class="rpt_table" id="table_body_head">
        <thead>
            <th width="30" align="center"><strong>SL</strong></th>
            <th width="150" align="center"><strong>Employee Name</strong></th>
            <th width="80" align="center"><strong>Designation</strong></th>
            <th width="80" align="center"><strong>Emp Code</strong></th>
            <th width="80" align="center"><strong>Emp ID</strong></th>
            <th width="80" align="center"><strong>Atten.Date</strong></th>
            <th width="100" align="center"><strong>Requisition O.T</strong></th>
            <th width="80" align="center"><strong>Requisition out time</strong></th>
            <th width="80" align="center"><strong>Actual Out Time</strong></th>
            <th width="100" align="center"><strong>OT As Reqn.</strong></th>
            <th width="80" align="center"><strong>In Time</strong></th>
            <th width="80" align="center"><strong>Out Date</strong></th>
        </thead>
        <?
        $i=1;
        foreach ($ot_group_array_asper as $emp_code=>$emp_data)
        {
			
			foreach ($emp_data as $adate=>$othr)
			{
			?>
                <tr bgcolor="<? echo $bgcolor; ?>">
                    <td width="30" align="left"><?php echo $i;?></td>
                    <td width="150" align="center"><? echo $emp_info_name[$emp_code];?></td>
                    <td width="80" align="center"><? echo $designation_chart[$emp_info_designation[$emp_code]];?></td>
                    <td width="80" align="center"> <? echo $emp_code; ?></td>
                    <td width="80" align="center"><? echo $emp_info_idcard[$emp_code];?></td>
                    <td width="80" align="center"> <? echo $adate;?></td>
                    <td width="100" align="center"><?php  echo $req_ot_asper=get_buyer_ot_hr( $ot_requisition_array[$emp_code][$adate],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);$total_req_ot+=$req_ot_asper; ?></td>
                    <td width="80" align="center" title="<? echo $r_sign_out_time_arr[$emp_code][$adate]; ?>"><?php echo show_time_format(add_time($r_sign_out_time_arr[$emp_code][$adate],$ot_requisition_array[$emp_code][$adate]),$in_out_time_format); ?></td>
                    <td width="80" align="center"><? echo show_time_format($sign_out_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
                    <td width="100" align="center"> <? echo $othr; ?> </td>
                    <td width="80" align="center"><?php echo show_time_format($in_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
                    <td width="80" align="center"><?php if( $is_next_day_arr[$emp_code][$adate]==1 ) {echo add_date($adate,1); } else echo $adate; ?></td>
                </tr>
			<?
			}
		$i++;	
        }
        ?>
            <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong>Total</strong></td>
                <td align="center">&nbsp;<?php echo $total_req_ot; //$bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td colspan="2">&nbsp;<?php  ?></td>
                <td align="center">&nbsp;<?php   echo $asper_ho.":".$asper_mi; $sum_new_asper_total=($asper_ho*60)+$asper_mi; ?></td>
                <td colspan="2">&nbsp;<?php  ?></td>
           </tr>
        </table> 
            
        <br/>
        <div align="left" class=""><strong>OT Over the Requisition Hour:</strong></div>     
        <table cellspacing="0" cellpadding="0" width="1100px" class="rpt_table" id="table_body_head">
            <thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="150" align="center"><strong>Employee Name</strong></th>
                <th width="80" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Atten.Date</strong></th>
                <th width="100" align="center"><strong>Requisition O.T</strong></th>
                <th width="80" align="center"><strong>Requisition out time</strong></th>
                <th width="80" align="center"><strong>Actual Out Time</strong></th>
                <th width="100" align="center"><strong>Over the Reqn. (List)</strong></th>
                <th width="80" align="center"><strong>In Time</strong></th>
                <th width="80" align="center"><strong>Out Date</strong></th>
            </thead>
        <?
        $i=1;
        foreach ($ot_group_array_over as $emp_code=>$emp_data)
        {
            foreach ($emp_data as $adate=>$othr)
            {
            ?>
                <tr bgcolor="<? echo $bgcolor; ?>">
                    <td width="30" align="left"><?php echo $i;?></td>
                    <td width="150" align="center"><? echo $emp_info_name[$emp_code];?></td>
                    <td width="80" align="center"><? echo $designation_chart[$emp_info_designation[$emp_code]];?></td>
                    <td width="80" align="center"> <? echo $emp_code; ?></td>
                    <td width="80" align="center"><? echo $emp_info_idcard[$emp_code];?></td>
                    <td width="80" align="center"> <? echo $adate;?></td>
                    <td width="100" align="center">	<?php  echo $req_ot_over=get_buyer_ot_hr( $ot_requisition_array[$emp_code][$adate],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440); $total_req_ot_over+=$req_ot_over; ?></td>
                    <td width="80" align="center" title="<? echo $r_sign_out_time_arr[$emp_code][$adate]; ?>"><?php echo show_time_format(add_time($r_sign_out_time_arr[$emp_code][$adate],$ot_requisition_array[$emp_code][$adate]),$in_out_time_format); ?></td>
                    <td width="80" align="center"><? echo show_time_format($sign_out_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
                    <td width="100" align="center"> <? echo $othr; ?> </td>
                    <td width="80" align="center"><?php echo show_time_format($in_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
                    <td width="80" align="center"><?php if( $is_next_day_arr[$emp_code][$adate]==1 ) {echo add_date($adate,1); } else echo $adate; ?></td>
                </tr>
            <?
            }
		$i++;	
        }
          ?>
          		<tr bgcolor="#CCCCCC">
                    <td colspan="6" align="center"><strong>Total</strong></td>
                    <td align="center">&nbsp;<?php echo $total_req_ot_over;  //$bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                    <td colspan="2">&nbsp;<?php  ?></td>
                    <td align="center">&nbsp;<?php  echo $over_ho.":".$over_mi;        $sum_new_over_total=($over_ho*60)+$over_mi; ?></td>
                    <td colspan="2">&nbsp;<?php  ?></td>
               </tr>
        </table>
          
         <br/>
        <div align="left" class=""><strong>OT Less than Requisition Hour:</strong></div>      
        <table cellspacing="0" cellpadding="0" width="1100px" class="rpt_table" id="table_body_head">
            <thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="150" align="center"><strong>Employee Name</strong></th>
                <th width="80" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Atten.Date</strong></th>
                <th width="100" align="center"><strong>Requisition O.T</strong></th>
                <th width="80" align="center"><strong>Requisition out time</strong></th>
                <th width="80" align="center"><strong>Actual Out Time</strong></th>
                <th width="100" align="center"><strong>Less than Reqn (List)</strong></th>
                <th width="80" align="center"><strong>In Time</strong></th>
                <th width="80" align="center"><strong>Out Date</strong></th>
            </thead>
        <?
        $i=1;
        foreach ($ot_group_array_less as $emp_code=>$emp_data)
        {
			foreach ($emp_data as $adate=>$othr)
			{
			?>
				<tr bgcolor="<? echo $bgcolor; ?>">
					<td width="30" align="left"><?php echo $i;?></td>
                    <td width="150" align="center"><? echo $emp_info_name[$emp_code];?></td>
					<td width="80" align="center"><? echo $designation_chart[$emp_info_designation[$emp_code]];?></td>
					<td width="80" align="center"> <? echo $emp_code; ?></td>
					<td width="80" align="center"><? echo $emp_info_idcard[$emp_code];?></td>
					<td width="80" align="center"> <? echo $adate;?></td>
                     <td width="100" align="center"><?php  echo $req_ot_less=get_buyer_ot_hr( $ot_requisition_array[$emp_code][$adate],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440); $total_req_ot_less+=$req_ot_less; ?></td>
					<td width="80" align="center" title="<? echo $r_sign_out_time_arr[$emp_code][$adate]; ?>"><?php echo show_time_format(add_time($r_sign_out_time_arr[$emp_code][$adate],$ot_requisition_array[$emp_code][$adate]),$in_out_time_format); ?></td>
					<td width="80" align="center"><? echo show_time_format($sign_out_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
					<td width="100" align="center"> <? echo $othr;  ?> </td>
                    <td width="80" align="center"><?php echo show_time_format($in_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
                    <td width="80" align="center"><?php if( $is_next_day_arr[$emp_code][$adate]==1 ) {echo add_date($adate,1); } else echo $adate; ?></td>
				</tr>
			<? 
			}
		$i++;	
        }
        ?>
        		<tr bgcolor="#CCCCCC">
                    <td colspan="6" align="center"><strong>Total</strong></td>
                    <td align="center">&nbsp;<?php echo $total_req_ot_less; // $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                    <td colspan="2">&nbsp;<?php  ?></td>
                    <td align="center">&nbsp;<?php  echo $less_ho.":".$less_mi; $sum_new_less_total=($less_ho*60)+$less_mi; ?> </td>
                    <td colspan="2">&nbsp;<?php  ?></td>
               </tr>
        </table>
        
         <br/>
        <div align="left" class=""><strong>OT Not Done from The Requisition List:</strong></div>     
        <table cellspacing="0" cellpadding="0" width="1100px" class="rpt_table" id="table_body_head">
            <thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="150" align="center"><strong>Employee Name</strong></th>
                <th width="80" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Atten.Date</strong></th>
                <th width="100" align="center"><strong>Requisition O.T</strong></th>
                <th width="80" align="center"><strong>Requisition out time</strong></th>
                <th width="80" align="center"><strong>Actual Out Time</strong></th>
                <th width="80" align="center"><strong>No OT from list</strong></th>
                <th width="80" align="center"><strong>In Time</strong></th>
                <th width="80" align="center"><strong>Out Date</strong></th>
            </thead>
        <?
        $i=1;
        foreach ($ot_group_array_noot as $emp_code=>$emp_data)
        {
			
			foreach ($emp_data as $adate=>$othr)
			{
			?>
				<tr bgcolor="<? echo $bgcolor; ?>">
					<td width="30" align="left"><?php echo $i;?></td>
                    <td width="150" align="center"><? echo $emp_info_name[$emp_code];?></td>
					<td width="80" align="center"><? echo $designation_chart[$emp_info_designation[$emp_code]];?></td>
					<td width="80" align="center"> <? echo $emp_code; ?></td>
					<td width="80" align="center"><? echo $emp_info_idcard[$emp_code];?></td>
					<td width="80" align="center"> <? echo $adate;?></td>
                    <td width="100" align="center">	<?php  echo $req_ot_noot=get_buyer_ot_hr( $ot_requisition_array[$emp_code][$adate],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440); $total_req_ot_noot+=$req_ot_noot;?></td>
					<td width="80" align="center" title="<? echo $r_sign_out_time_arr[$emp_code][$adate]; ?>"><?php echo show_time_format(add_time($r_sign_out_time_arr[$emp_code][$adate],$ot_requisition_array[$emp_code][$adate]),$in_out_time_format); ?></td>
					<td width="80" align="center"><? echo show_time_format($sign_out_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
					<td width="100" align="center"> <? echo $othr; ?> </td>
                    <td width="80" align="center"><?php echo show_time_format($in_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
                    <td width="80" align="center"><?php if( $is_next_day_arr[$emp_code][$adate]==1 ) {echo add_date($adate,1); } else echo $adate; ?></td>
				</tr>
			<?
			}
		$i++;	
        }
        ?>
            <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong>Total</strong></td>
                <td align="center">&nbsp;<?php echo $total_req_ot_noot; //$bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td colspan="2">&nbsp;<?php  ?></td>
                <td align="center">&nbsp;<?php   //$asper_ho.":".$asper_mi;     $sum_new_asper_total=($asper_ho*60)+$asper_mi; ?></td>
                <td colspan="2">&nbsp;<?php  ?></td>
            </tr>
        </table>  
              
        <br/>
        <div align="left" class=""><strong>OT Without Requisition:</strong></div>      
        <table cellspacing="0" cellpadding="0" width="1100px" class="rpt_table" id="table_body_head">
            <thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="150" align="center"><strong>Employee Name</strong></th>
                <th width="80" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Atten.Date</strong></th>
                <th width="100" align="center"><strong>Requisition O.T</strong></th>
                <th width="80" align="center"><strong>Requisition out time</strong></th>
                <th width="80" align="center"><strong>Actual Out Time</strong></th>
                <th width="80" align="center"><strong>OT Without Reqn</strong></th>
                <th width="80" align="center"><strong>In Time</strong></th>
                <th width="80" align="center"><strong>Out Date</strong></th>
            </thead>
        <?
        $i=1;
        foreach ($ot_group_array_without as $emp_code=>$emp_data)
        {
			foreach ($emp_data as $adate=>$othr)
			{
			?>
				<tr bgcolor="<? echo $bgcolor; ?>">
					<td width="30" align="left"><? echo $i;?></td>
					<td width="150" align="center"><? echo $emp_info_name[$emp_code];?></td>
					<td width="80" align="center"><? echo $designation_chart[$emp_info_designation[$emp_code]];?></td>
					<td width="80" align="center"> <? echo $emp_code; ?></td>
					<td width="80" align="center"><? echo $emp_info_idcard[$emp_code];?></td>
					<td width="80" align="center"> <? echo $adate;?></td>
                    <td width="100" align="center">	<?php  echo $req_ot_without=get_buyer_ot_hr( $ot_requisition_array[$emp_code][$adate],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440); $total_req_ot_without+=$req_ot_without; ?></td>
					<td width="80" align="center" title="<? echo $r_sign_out_time_arr[$emp_code][$adate]; ?>"><?php echo show_time_format(add_time($r_sign_out_time_arr[$emp_code][$adate],$ot_requisition_array[$emp_code][$adate]),$in_out_time_format); ?></td>
					<td width="80" align="center"><? echo show_time_format($sign_out_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
					<td width="100" align="center"> <? echo $othr;  ?> </td>
                    <td width="80" align="center"><?php echo show_time_format($in_time_arr[$emp_code][$adate],$in_out_time_format); ?></td>
                    <td width="80" align="center"><?php if( $is_next_day_arr[$emp_code][$adate]==1 ) {echo add_date($adate,1); } else echo $adate; ?></td>
				</tr>
			<? 
			}
        $i++;
        }
        ?>
            <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong>Total</strong></td>
                <td align="center">&nbsp;<?php echo $total_req_ot_without; //$bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td colspan="2">&nbsp;<?php  ?></td>
                <td align="center">&nbsp;<?php echo $without_ho.":".$without_mi; $sum_new_without_total=($without_ho*60)+$without_mi;?> </td>
                <td colspan="2">&nbsp;<?php  ?></td>
            </tr>
        </table>
        <?
		
		
		?>
        
             	<!--<tr bgcolor="#CCCCCC">
                    <td colspan="6" align="center"><strong>Grand Total</strong></td>
                    <td>&nbsp;<?php  $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                    <td>&nbsp;<?php  ?></td>
                    <td>&nbsp;<?php  ?></td>
                    <td>&nbsp;<?php   $asper_ho.":".$asper_mi;     $sum_new_asper_total=($asper_ho*60)+$asper_mi; ?></td>
                    <td>&nbsp;<?php  $over_ho.":".$over_mi;        $sum_new_over_total=($over_ho*60)+$over_mi; ?></td>
                    <td>&nbsp;<?php  $less_ho.":".$less_mi;        $sum_new_less_total=($less_ho*60)+$less_mi; ?> </td>
                    <td>&nbsp;<?php  ?></td>
                    <td>&nbsp;<?php   $without_ho.":".$without_mi; $sum_new_without_total=($without_ho*60)+$without_mi;?> </td>
                    <td>&nbsp;<?php  ?></td>
                    <td>&nbsp;<?php ?></td>
              </tr>
        </table>-->
        
  <div id="to_summery_hidden" style="display:none">
        <div class="form_caption"> OT Analysis</div>
        	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="ot_summery_table">
        	<thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="180" align="center"><strong>Particulars</strong></th>
                <th width="80" align="center"><strong>Man Hour OT</strong></th>
                <th width="80" align="center"><strong>% on Total OT</strong></th>
                <th width="80" align="center"><strong>OT Amount</strong></th>
                <th width="80" align="center"><strong>No.of Employee</strong></th>
                <th width="80" align="center"><strong>% on Total Emp.</strong></th>
                <th width="80" align="center"><strong>Avg. OT Hr. per Head</strong></th>
                <th width="80" align="center"><strong>Avg. OT Amt. per Head</strong></th>
        	</thead>
            <?
				$requ_total_man_hour=$sum_new_asper_total+$sum_new_over_total+$sum_new_less_total;
				$req_ho=floor($requ_total_man_hour/60);
				$req_mi=$requ_total_man_hour%60;
				$total_man_houre=$sum_new_without_total+$requ_total_man_hour;
				$total_ho=floor($total_man_houre/60);
				$total_mi=$total_man_houre%60;
				
				$asper_ontotal_ot=($sum_new_asper_total/$total_man_houre)*100;
				$over_ontotal_ot=($sum_new_over_total/$total_man_houre)*100;
				$less_ontotal_ot=($sum_new_less_total/$total_man_houre)*100;
				$reqtotal_ontotal_ot=($requ_total_man_hour/$total_man_houre)*100;
				$without_ontotal_ot=($sum_new_without_total/$total_man_houre)*100;
				$total_ontotal_ot=($total_man_houre/$total_man_houre)*100;
					
				$requisition_total_amount=$as_per_emp_amount_total+$emp_amount_over_tot+$emp_amount_less_tot;
				$total_amount=$requisition_total_amount+$emp_amount_without_tot;
				
				$req_total_emp=$ot_as_per_emp+$over_reqn_emp+$less_reqn_emp+$no_ot_emp;
				$total_emp=$wethout_reqn_emp+$req_total_emp;
				
				$on_total_emp_as_per_req=($ot_as_per_emp/$total_emp)*100;
				$on_total_emp_over=($over_reqn_emp/$total_emp)*100;
				$on_total_emp_less=($less_reqn_emp/$total_emp)*100;
				$on_total_emp_no_ot=($no_ot_emp/$total_emp)*100;
				$on_total_emp_req_tot=($req_total_emp/$total_emp)*100;
				$on_total_emp_wethout=($wethout_reqn_emp/$total_emp)*100;
				$on_total_emp_total=($total_emp/$total_emp)*100;
				
				$ot_hr_per_head=$sum_new_asper_total/$ot_as_per_emp;
				$ot_hr_per_head_hou=floor($ot_hr_per_head/60);
				$ot_hr_per_head_min=$ot_hr_per_head%60;
				
				$ot_hr_over=$sum_new_over_total/$over_reqn_emp;
				$ot_hr_over_hou=floor($ot_hr_over/60);
				$ot_hr_over_min=$ot_hr_over%60;
				
				$ot_hr_less=$sum_new_less_total/$less_reqn_emp;
				$ot_hr_less_hou=floor($ot_hr_less/60);
				$ot_hr_less_min=$ot_hr_less%60;
				
				$ot_hr_req_tot=$requ_total_man_hour/$req_total_emp;
				$ot_hr_req_tot_hou=floor($ot_hr_req_tot/60);
				$ot_hr_req_tot_min=$ot_hr_req_tot%60;
				
				$ot_hr_wethout=$sum_new_without_total/$wethout_reqn_emp;
				$ot_hr_wethout_hou=floor($ot_hr_wethout/60);
				$ot_hr_wethout_min=$ot_hr_wethout%60;
				
				$ot_hr_total=$total_man_houre/$total_emp;
				$ot_hr_total_hou=floor($ot_hr_total/60);
				$ot_hr_total_min=$ot_hr_total%60;
				
				
				$as_per_avg_ot_amount_per_head=$as_per_emp_amount_total/$ot_as_per_emp;
				$over_avg_ot_amount_per_head=$emp_amount_over_tot/$over_reqn_emp;
				$less_avg_ot_amount_per_head=$emp_amount_less_tot/$less_reqn_emp;
				$req_tot_avg_ot_amount_per_head=$requisition_total_amount/$req_total_emp;
				$without_avg_ot_amount_per_head=$emp_amount_without_tot/$wethout_reqn_emp;
				$total_avg_ot_amount_per_head=$total_amount/$total_emp;
				
				
			?>
            <tr bgcolor="#FFFFFF">
				<td align="left">1</td>
                <td align="left">OT as per requisition hour</td>
                <td align="center">&nbsp;<?php echo $asper_ho.":".$asper_mi; $sum_new_asper_total; ?></td>
				<td align="center">&nbsp;<?php  echo round($asper_ontotal_ot,2)."%"; ?></td>
                <td align="center">&nbsp;<?php echo round($as_per_emp_amount_total,2); ?></td>
				<td align="center">&nbsp;<?php echo $ot_as_per_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_as_per_req,2)."%"; ?></td>
                <td align="center"><?php echo $ot_hr_per_head_hou.":".$ot_hr_per_head_min;$ot_hr_per_head; ?></td>
                <td align="center"><?php echo round($as_per_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#E9F3FF">
				<td align="left">2</td>
                <td align="left">OT over the requisition hour</td>
                <td align="center">&nbsp;<?php echo $over_ho.":".$over_mi; $sum_new_over_total;?></td>
				<td align="center">&nbsp;<?php echo round($over_ontotal_ot,2)."%"; ?></td>
                <td align="center">&nbsp;<?php echo round($emp_amount_over_tot,2); ?></td>
				<td align="center">&nbsp;<?php echo $over_reqn_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_over,2)."%"; ?></td>
                <td align="center"><?php echo $ot_hr_over_hou.":".$ot_hr_over_min; $ot_hr_over;   ?></td>
                <td align="center"><?php  echo round($over_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#FFFFFF">
				<td align="left">3</td>
                <td align="left">OT less than requisition hour</td>
                <td align="center">&nbsp;<?php echo $less_ho.":".$less_mi; $sum_new_less_total; ?></td>
				<td align="center">&nbsp;<?php echo round($less_ontotal_ot,2)."%"; ?></td>
                <td align="center">&nbsp;<?php echo round($emp_amount_less_tot,2); ?></td>
				<td align="center">&nbsp;<?php echo $less_reqn_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_less,2)."%"; ?></td>
                <td align="center"><?php echo $ot_hr_less_hou.":".$ot_hr_less_min; $ot_hr_less;   ?></td>
                <td align="center"><?php echo round($less_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#E9F3FF">
				<td align="left">4</td>
                <td align="left">No OT from requisition list</td>
                <td align="center">&nbsp;<?php  ?></td>
				<td align="center">&nbsp;<?php  ?></td>
                <td align="center">&nbsp;<?php  ?></td>
				<td align="center">&nbsp;<?php echo $no_ot_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_no_ot,2)."%"; ?></td>
                <td align="center"><?php  ?></td>
                <td align="center"><?php  ?></td>
             </tr>
             <tr bgcolor="#FFFFFF">
				<td align="left">5</td>
                <td align="left"><strong>Requisition Total</strong></td>
                <td align="center">&nbsp;<?php echo $req_ho.":".$req_mi; $requ_total_man_hour; ?></td>
				<td align="center">&nbsp;<?php echo round($reqtotal_ontotal_ot,2)."%"; ?></td>
                <td align="center">&nbsp;<?php echo round($requisition_total_amount,2); ?></td>
				<td align="center">&nbsp;<?php echo $req_total_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_req_tot,2)."%"; ?></td>
                <td align="center"><?php echo $ot_hr_per_head_hou.":".$ot_hr_per_head_min; $ot_hr_req_tot;  ?></td>
                <td align="center"><?php echo round($req_tot_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#E9F3FF">
				<td align="left">6</td>
                <td align="left">OT without requisition</td>
                <td align="center">&nbsp;<?php echo $without_ho.":".$without_mi; $sum_new_without_total; ?></td>
				<td align="center">&nbsp;<?php echo round($without_ontotal_ot,2)."%"; ?></td>
                <td align="center">&nbsp;<?php echo round($emp_amount_without_tot,2); ?></td>
				<td align="center">&nbsp;<?php echo $wethout_reqn_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_wethout,2)."%"; ?></td>
                <td align="center"><?php echo $ot_hr_wethout_hou.":".$ot_hr_wethout_min; $ot_hr_wethout;  ?></td>
                <td align="center"><?php  echo round($without_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#FFFFFF">
				<td align="left">7</td>
                <td align="left"><strong>Total</strong></td>
                <td align="center">&nbsp;<?php echo $total_ho.":".$total_mi; $total_man_houre; ?></td>
				<td align="center">&nbsp;<?php echo round($total_ontotal_ot,2)."%"; ?></td>
                <td align="center">&nbsp;<?php echo round($total_amount,2); ?></td>
				<td align="center">&nbsp;<?php echo $total_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_total,2)."%"; ?></td>
                <td align="center"><?php echo $ot_hr_total_hou.":".$ot_hr_total_min; $ot_hr_total;  ?></td>
                <td align="center"><?php  echo round($total_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr>
             <td></td>
             <td colspan="8"><strong>Comments:</strong>
             <?
             if($reqtotal_ontotal_ot>$without_ontotal_ot)
			 {echo "OT under requisition is higher than non requisition.";}
			 else echo "Non requisition OT is higher than requisition.";
             ?>
             </td>
             </tr>
        </table>
  </div>
        
        
        <?
		}
		
		$html=ob_get_contents();		
		ob_clean();
		//for report temp file delete 
		foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
				@unlink($filename);
		}		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);
		
		echo "$html"."####"."$name"."####".$small_print;		
		exit();
		
}


function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>