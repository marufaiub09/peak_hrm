<?php

date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}


$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	
	
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0  order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$in_out_time_format=$res['in_out_time_format'];
		}
		
	$company_info_sql="select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	

if( $action == "total_punch_report" ) { //total Punch Report
	
	//if ($emp_code=='') $emp_code =""; else $emp_code ="and emp.emp_code in ($emp_code)";
	//if ($id_card=='') $id_card_no =""; else $id_card_no ="and emp.id_card_no in ($id_card_no)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and  emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and  id_card_no in ('".implode("','",explode(",",$id_card))."')";
	//echo $id_card_no."sdd";die;
 	
	$r=1;
	$dates=convert_to_mysql_date($txt_date);
	
	ob_start();
 	?>
	<table width="1000" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">

            <thead> 
            	<tr>
                	 <th colspan="112"><font size="+1"><? echo  $company_info_result["company_name"];//echo $company_details[$com_name[company_name]];?></font></th>
                </tr>	
                <tr height="40">
                    <th width="20"><b>SL</b></th>
                    <th width="150"><b>Name</b></th>
                    <th width="100"><b>Designation</b></th>
                    <th width="100"><b>Emp Code</b></th>
                    <th width="80"><b>ID Card</b></th>
                    <th width="80"><b>Punch Card</b></th>
                    <th width="80"><b>Attnd. Date</b></th>
                    <th width="60"><b>Punch Time</b></th>
                    <th width="60"><b>Attn. Type</b></th>
                     <th width="60"><b>Punch Type</b></th>
                    <th width="100"><b>Shift Policy Name</b></th>
                    <th width="80"><b>Shift Start Time</b></th>
                    <th width="80"><b>Shift End Time</b></th>
                </tr>
            </thead>
   	
	
	<?	
	
	//$attn_type_arr=array(0=>"Actual",1=>"Manual");
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,
				emp.id_card_no,emp.punch_card_no,emp.designation_id,emp.emp_code,a.cid,a.dtime,
				a.ctime,a.purpose,a.shift_policy_id,a.shift_start,a.shift_end,a.punch_type,a.is_manual 	 
				FROM 
					hrm_raw_data_attnd_backup a, hrm_employee emp 
				WHERE 
					a.dtime like '$dates'
					$emp_code
					$id_card_no
					and a.cid=emp.punch_card_no order by a.ctime ASC"; 
					
 					
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
	{ 
	
		if ($r%2==0) $bgcolor="#EEEEEE"; 
		else $bgcolor="#FFFFFF";
    ?>
            <tr bgcolor="<? echo $bgcolor;?>" >
                <td height="20"><? echo $r;?> </td>
                <td width="150"><? echo $row_emp[name];?></td>
                <td width="100"><? echo $designation_chart[$row_emp['designation_id']];?></td>
                <td width="100"><? echo $row_emp[emp_code]; ?></td>
                <td width="80"><? echo $row_emp[id_card_no]; ?></td>
                <td width="80"><? echo $row_emp[punch_card_no];?></td>
                <td align='center' width="80"><? echo convert_to_mysql_date($row_emp[dtime]);?></td>
                <td  width="60"><b><? echo show_time_format( $row_emp['ctime'], $in_out_time_format ); ?></b></td>
                <td  width="60"><b><? if($row_emp[is_manual]==0){echo "Actual";} else {echo "Manual";}?></b></td>
                <td  width="60"><b><? echo $punch_type_arr[$row_emp[punch_type]]; ?></b></td>
                <td width="100"><?php echo $shift_chart[$row_emp[shift_policy_id]];?></td>
                <td  width="80"><b><? echo show_time_format( $row_emp['shift_start'], $in_out_time_format ); ?></b></td>
                <td  width="80"><b><? echo show_time_format( $row_emp['shift_end'], $in_out_time_format ); ?></b></td>
            </tr>
          
 <?      
      
	$r++;

} //end while
?>

 </table> 

<?	
	
	
	
	//previous file delete code-----------------------------//

$html = ob_get_contents();
	ob_clean();		
	

	
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}
?>

