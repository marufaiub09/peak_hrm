<div>	<table width="1300" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
	   <!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
	
   
    <td colspan="17" align="center" style="border:hidden"><font size="+1">
		Test Company Ltd.</font><br />
		<br />
        <font size="+0"> Daily Punch Report On 01-10-2014</font><br />
    </td>
    </table>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1300" rules="all">
        <thead>
            <tr height="50px">
                <th width="30px"><b>SL</b></th>
                <th width="50px"><b>Emp Code</b></th>
                <th width="50px"><b>Punch Card</b></th>
                <th width="130px"><b>Name</b></th>
                <th width="120px"><b>Designation</b></th>
                <th width="100px"><b>Department</b></th>
                <th width="100px"><b>Section</b></th>
                <th width="100px"><b>Sub Section</b></th>
                <th width="70px"><b>Attnd. Date</b></th>
                <th width="50px"><b>ID Card</b></th>
                <th width="60px"><b>In Time</b></th>
                <th width="60px"><b>Out Time</b></th>
                <th width="30px"><b>Status</b></th>
                <th width="40px"><b>Late Min</b></th>
                <th width="40px"><b>OT</b></th>
                <th width="80px"><b>Shift Name</b></th>
                <th width="120px"><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1300" rules="all" id="table_body" >
	        <tr bgcolor="#FFFFFF">
            <td width="20px">1</td>
            <td width="50px">1838</td>
            <td width="50px">0007601770</td>
            <td width="130px">
				Halima Begum    <br />cell:             </td>
            <td width="120px">Cleaner</td>
            <td width="100px">ADMIN</td>						
            <td width="100px">CLEANER</td>
            <td width="100px"></td>
            <td align='center' width="70">01-10-2014</td>
            <td width="50px">1838</td>
            <td width="60px"><b>00:00</b></td>
            <td width="60px"><b>00:00</b></td>
            <td width="30px">A</td>
            <td width="40px">0</td>
            <td width="40px" align="center">0</td>
            <td width="80px">GARMENTS- WORKER</td>
            <td width="120px"></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1700px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
</table> 


</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=1838&date=2014-10-01','Absent Information'); return false">1</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 1 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>