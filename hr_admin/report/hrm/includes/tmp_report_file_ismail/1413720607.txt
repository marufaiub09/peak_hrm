<div>	<table width="1300" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
	   <!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
	
   
    <td colspan="17" align="center" style="border:hidden"><font size="+1">
		Test Company Ltd.</font><br />
		<br />
        <font size="+0"> Daily Punch Report On 01-10-2014</font><br />
    </td>
    </table>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1300" rules="all">
        <tr height="50">
            <th width="20"><b>SL</b></th>
            <th width="50"><b>Emp Code</b></th>
            <th width="50"><b>Punch Card</b></th>
            <th width="130"><b>Name</b></th>
            <th width="120"><b>Designation</b></th>
            <th width="100"><b>Department</b></th>
            <th width="100"><b>Section</b></th>
            <th width="100"><b>Sub Section</b></th>
            <th width="70"><b>Attnd. Date</b></th>
            <th width="50"><b>ID Card</b></th>
            <th width="60"><b>In Time</b></th>
            <th width="60"><b>Out Time</b></th>
            <th width="30"><b>Status</b></th>
            <th width="40"><b>Late Min</b></th>
            <th width="40"><b>OT</b></th>
            <th width="80"><b>Shift Name</b></th>
            <th width="120"><b>Remarks</b></th>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1300" rules="all" id="table_body" >
    <tbody> 
	        <tr bgcolor="#FFFFFF">
            <td height="20">1 </td>
            <td width="50">1838</td>
            <td width="50">0007601770</td>
            <td width="130">
				Halima Begum    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100"></td>
            <td align='center' width="70">01-10-2014</td>
            <td width="50">1838</td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="30">A</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td width="120"></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            
</tbody>
<tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1700px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
</table> 


</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=1838&date=2014-10-01','Absent Information'); return false">1</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-10-01','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 1 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>