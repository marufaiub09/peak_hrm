<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <div style=" border:1px solid #7F7FFF; width:1300px;">
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Test Company Ltd.</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 01-04-2014</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">000132</td>
            <td width="80">0000711653</td>
            <td width="130">
				Md.Abdus Sattar    <br />cell: 01829683000            </td>
            <td width="120">Manager</td>
            <td width="100">SAMPLE</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000132</td>
            <td width="60"><b>9:09 am</b></td>
            <td width="60"><b>7:13 pm</b></td>
            <td width="50">D</td>
            <td width="40">9</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">000135</td>
            <td width="80">0011440731</td>
            <td width="130">
				Walid Khondokar    <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">SEWING</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000135</td>
            <td width="60"><b>9:00 am</b></td>
            <td width="60"><b>7:46 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">000139</td>
            <td width="80">0011915868</td>
            <td width="130">
				Laxman Shaha    <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">QUALITY ASSURANCE</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000139</td>
            <td width="60"><b>9:06 am</b></td>
            <td width="60"><b>7:59 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">000142</td>
            <td width="80">0011538858</td>
            <td width="130">
				MD.MONIRUZZAMAN    <br />cell:             </td>
            <td width="120">Incharge</td>
            <td width="100">ADMIN</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000142</td>
            <td width="60"><b>8:02 am</b></td>
            <td width="60"><b>7:39 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (8-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">000143</td>
            <td width="80">0013985401</td>
            <td width="130">
				Abdul Motin    <br />cell:             </td>
            <td width="120">Assistant Production Manager</td>
            <td width="100">SEWING MACHANIC</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000143</td>
            <td width="60"><b>9:03 am</b></td>
            <td width="60"><b>6:39 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">000145</td>
            <td width="80">0011547438</td>
            <td width="130">
				Abdhullah Kayes    <br />cell:             </td>
            <td width="120">Assistant General Manager</td>
            <td width="100">FABRIC FINISHING</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000145</td>
            <td width="60"><b>8:46 am</b></td>
            <td width="60"><b>7:19 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">000148</td>
            <td width="80">0002830148</td>
            <td width="130">
				MD.Nasir Uddin    <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">MAINTENANCE AND ETP</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000148</td>
            <td width="60"><b>9:09 am</b></td>
            <td width="60"><b>7:22 pm</b></td>
            <td width="50">D</td>
            <td width="40">9</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">000162</td>
            <td width="80">0014094652</td>
            <td width="130">
				Rabindra Chandra Nath    <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">AUDIT</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000162</td>
            <td width="60"><b>8:56 am</b></td>
            <td width="60"><b>3:07 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">000222</td>
            <td width="80">0012035883</td>
            <td width="130">
				SHAHIDUL ISLAM SHAHID    <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">PROCUREMENT</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000222</td>
            <td width="60"><b>9:09 am</b></td>
            <td width="60"><b>6:07 pm</b></td>
            <td width="50">D</td>
            <td width="40">9</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">000335</td>
            <td width="80">0000721638</td>
            <td width="130">
				Md.Sarwar Mamun    <br />cell:             </td>
            <td width="120">Assistant Production Manager</td>
            <td width="100">MERCHANDISING</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000335</td>
            <td width="60"><b>9:09 am</b></td>
            <td width="60"><b>7:37 pm</b></td>
            <td width="50">D</td>
            <td width="40">9</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">000358</td>
            <td width="80">0008671908</td>
            <td width="130">
				Muhammad Mizanour Rahman    <br />cell:             </td>
            <td width="120">Assistant Manager</td>
            <td width="100">FABRIC FINISHING</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000358</td>
            <td width="60"><b>8:46 am</b></td>
            <td width="60"><b>7:21 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">000361</td>
            <td width="80">0014506782</td>
            <td width="130">
				Md.Nurul Hossain    <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">MERCHANDISING</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000361</td>
            <td width="60"><b>9:06 am</b></td>
            <td width="60"><b>7:45 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">000362</td>
            <td width="80">0013677978</td>
            <td width="130">
				Mohammad Monjurul Islam    <br />cell:             </td>
            <td width="120">Deputy Manager</td>
            <td width="100">DYEING</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000362</td>
            <td width="60"><b>8:54 am</b></td>
            <td width="60"><b>8:22 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">000363</td>
            <td width="80">0014518104</td>
            <td width="130">
				Fayez Ahammed    <br />cell:             </td>
            <td width="120">Assistant Manager</td>
            <td width="100">KNITTING</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000363</td>
            <td width="60"><b>9:00 am</b></td>
            <td width="60"><b>7:19 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">000365</td>
            <td width="80">0000705611</td>
            <td width="130">
				Akter Hossain    <br />cell:             </td>
            <td width="120">General Manager</td>
            <td width="100">IT</td>						
            <td width="100"></td>
            <td width="100"></td>
            <td width="70">01-04-2014</td>
            <td width="50">000365</td>
            <td width="60"><b>7:55 am</b></td>
            <td width="60"><b>7:22 pm</b></td>
            <td width="50">P</td>
            <td width="40">0</td>
            <td width="40" align="center">0</td>
            <td width="80">Staff (9-7)</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>

</div>
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 11 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-04-01','Absent Information'); return false">0</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-04-01','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-04-01','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=000132,000148,000222,000335&date=2014-04-01','Late Information'); return false">4 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-04-01','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 15 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>