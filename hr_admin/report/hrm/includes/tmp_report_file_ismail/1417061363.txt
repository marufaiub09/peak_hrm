<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Test Company Ltd.</font><br />
                BSCIC Hosiery Shilpa Nagari , Narayangonj<br />
                <font size="+0"> Daily Punch Report On 27-11-2014</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">1839</td>
            <td width="80"></td>
            <td width="130">
				Khadiza    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>1839</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">1963</td>
            <td width="80"></td>
            <td width="130">
				Kulsum    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>1963</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">2951</td>
            <td width="80"></td>
            <td width="130">
				SHIRINA-1    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>2951</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">6481</td>
            <td width="80"></td>
            <td width="130">
				Alaya-1    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>6481</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">9062</td>
            <td width="80"></td>
            <td width="130">
				SELINA    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>9062</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">9066</td>
            <td width="80"></td>
            <td width="130">
				FATEMA -3    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>9066</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">9083</td>
            <td width="80"></td>
            <td width="130">
				OMME HANI    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>9083</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">9390</td>
            <td width="80"></td>
            <td width="130">
				TAHMINA-1    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>9390</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">10363</td>
            <td width="80"></td>
            <td width="130">
				FATEMA - 4    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>10363</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">18623</td>
            <td width="80"></td>
            <td width="130">
				Shahara-3    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>18623</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">30431</td>
            <td width="80"></td>
            <td width="130">
				Josna Rani    <br />cell:             </td>
            <td width="120">Sweeper</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>30431</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">35450</td>
            <td width="80"></td>
            <td width="130">
				Mohosina    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>35450</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">37974</td>
            <td width="80"></td>
            <td width="130">
				Morsheda-4    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>37974</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">38923</td>
            <td width="80"></td>
            <td width="130">
				Firuza-3    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>38923</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">40925</td>
            <td width="80"></td>
            <td width="130">
				Mazeda    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>40925</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">42361</td>
            <td width="80"></td>
            <td width="130">
				Johura    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>42361</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">42362</td>
            <td width="80"></td>
            <td width="130">
				Bely Khatun    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>42362</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">43604</td>
            <td width="80"></td>
            <td width="130">
				Golapy    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>43604</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">50014</td>
            <td width="80"></td>
            <td width="130">
				Shathi    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>50014</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">50875</td>
            <td width="80"></td>
            <td width="130">
				Rekha    <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">ADMIN</td>						
            <td width="100">CLEANER</td>
            <td width="100">Cleaner</td>
            <td width="70">27-11-2014</td>
            <td width="50"><p>50875</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">GARMENTS- WORKER</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=1839,1963,2951,6481,9062,9066,9083,9390,10363,18623,30431,35450,37974,38923,40925,42361,42362,43604,50014,50875&date=2014-11-27','Absent Information'); return false">20</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-11-27','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-11-27','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-11-27','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2014-11-27','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 20 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>