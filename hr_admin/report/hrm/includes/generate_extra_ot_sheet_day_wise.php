<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($location_details);
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


if($action=='extra_ot_sheet'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	$table_width=250+($get_days*70);
	
	if($table_width<900) $table_width=900;
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id in ($division_id)";
	if ($section_id==0) $section_id=""; else $section_id="and section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";	
	if ($department_id==0) $department_id=""; else	$department_id="and department_id in ($department_id)";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	
	//new
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(id_card_no, 5)";}
			
	ob_start();	
		
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:<? echo $table_width; ?>">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1">Employee Extra OT Sheet</font>
    </div>
    <table cellpadding="0" cellspacing="0" border="1" width="<? echo $table_width; ?>" class="rpt_table" style="font-size:10px; border:1px solid #000000"> 
       <thead>        
            <tr>
            	<th width="40"><b>SL</b></th>
                <th width="70"><b>ID Card</b></th>
                <th width="70"><b>Emp Code</b></th>
                <th align="120"><b>Name</b></th>
                <th align="100"><b>Designation</b></th>
				<?
                    for($j=0;$j<$get_days;$j++){				
                    $newdate = date("Y-m-d",strtotime("+".$j." day", strtotime($fromdate)));
                ?>
                    <th width="60" style="font-size:9px"><? echo convert_to_mysql_date($newdate); ?></th>
                <?	
                    }
                ?>
                <th>Total</th>  
            </tr>
       </thead>       
						
    <?	
	
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM hrm_employee 
				WHERE 
				emp_code!=''
				$category 
				$cbo_company_id 
				$location_id
				$division_id 
				$section_id 
				$subsection_id 
				$designation_id 
				$department_id 
				$salary_based
				$orderby";
				
				//order by company_id,section_id, CAST(id_card_no as SIGNED) ASC";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=1;$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		while($row=mysql_fetch_array($result))
		{
			$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
			if(in_array($row[location_id],$location_arr))
			{
				if(in_array($row[division_id],$division_arr))
				{
					if(in_array($row[department_id],$department_arr))
					{
						if(in_array($row[section_id],$section_arr))
						{
							if(in_array($row[subsection_id],$subsection_arr))
							{}
							else
							{
								$subsection_arr[$row[subsection_id]]=$row[subsection_id];
								$new_subsec=1;
							}
						}
						else
						{
							$section_arr[$row[section_id]]=$row[section_id];
							$subsection_arr[$row[subsection_id]]=$row[subsection_id];
							$new_sec=1;
							$new_subsec=1;
						}
					}
					else
					{
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}
				}//division
				else
				{
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//division else
			}//location
			else
			{
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//location else
			//header print here 
			if($new_com==1 && $status_com==1)
			{					
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Company : <? echo $company_details[$row[company_id]][company_name]; ?></strong></td></tr><?
			}
			if($new_loc==1 && $status_loc==1)
			{					
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Location : <? echo $location_details[$row[location_id]][location_name]; ?></strong></td></tr><?
			}				
			if($new_divis==1 && $status_divis==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Division : <? echo $division_details[$row[division_id]][division_name]; ?></strong></td></tr><?
			}
			if($new_dept==1 && $status_dept==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Department : <? echo $department_details[$row[department_id]][department_name]; ?></strong></td></tr><?
			}
			if($new_sec==1 && $status_sec==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Section : <? echo $section_details[$row[section_id]][section_name]; ?></strong></td></tr><?
			}
			if($new_subsec==1 && $status_subsec==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Sub Section : <? echo $subsection_details[$row[subsection_id]][subsection_name]; ?></strong></td></tr><?
			}
			
					
			if($sl%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';		
					
	?>    
    		
            <tr bgcolor="<? echo $bgcolor; ?>">
            	<td><? echo $i; ?></td>
            	<td><? echo $row['id_card_no']; ?></td>
                <td><? echo $row['emp_code']; ?></td>
                <td><? echo $row['name']; ?></td>
               <!-- <td><? //echo $designation_chart[$row['designation_id']]['custom_designation']; ?></td>-->               
                <?
			    $sql_attn = "SELECT emp_code,attnd_date,total_over_time_min,designation_id FROM hrm_attendance WHERE emp_code=$row[emp_code] and attnd_date>='$fromdate' and attnd_date<='$todate'";
				//changed rows with row here
				$result_attn = mysql_query( $sql_attn ) or die( $sql_attn . "<br />" . mysql_error() );
				$arr_attn=array();
				$designation_id_arr=array();
				while($att_row=mysql_fetch_array($result_attn)){
					if($att_row['total_over_time_min']>$buyer_ot_min_comp)$att_row['total_over_time_min']=$att_row['total_over_time_min']-$buyer_ot_min_comp;
					$hours[0] = abs((int)($att_row['total_over_time_min']/60)); //hours
					$hours[1] = abs((int)($att_row['total_over_time_min']%60)); //minutes
					$ot_mins_cal=0;
					if($ot_fraction==1) //ot fraction allow
					{						
						if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
						else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;
					}
					if($ot_fraction==2) // no ot fraction
					{						
						//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
						if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;						
					}
					if($ot_fraction==3) //at actual minutes
					{						
						$ot_mins_cal = 	$att_row['total_over_time_min'];				
					}
					
					$total_hours= get_buyer_ot_hr($att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
															
					//$total_hours = sprintf("%d.%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
					
					$arr_attn[$att_row['attnd_date']]['total_over_time_min']=$total_hours;
					$designation_id_arr[$att_row['emp_code']]=$att_row['designation_id'];
				}
				
				?>
                 <td><? echo $designation_chart[$designation_id_arr[$row['emp_code']]]['custom_designation']; ?></td>
                <?
				
				$total_ot=0;
				for($k=0;$k<$get_days;$k++){				
				$newdate = date("Y-m-d",strtotime("+".$k." day", strtotime($fromdate)));
				$total_ot += $arr_attn[$newdate]['total_over_time_min'];
				$explode_total_ot = explode(".",$total_ot);
				if($explode_total_ot[1]==6) $total_ot = $explode_total_ot[0]+1;
				?>
				<td align="right"><b><? echo $arr_attn[$newdate]['total_over_time_min']; ?></b></td>
				<?				
				}
				//$total_ot=explode(".",$total_ot); $total_ot[0]."hr ".$total_ot[1]
				?>
                <td align="right"><b><? echo number_format($total_ot,2); ?></b></td>
            </tr>
    
		<?
         $i++;   }
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}


?>