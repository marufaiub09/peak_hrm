<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}



if( $action == "monthly_attn_summery" ) {	
		
	if($txt_date!="")
	{
  		list($yr,$mn,$da)=explode("-",convert_to_mysql_date($txt_date));
		$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
		$date_part_from = $yr . "-" . $mn."-01";
		$date_part_to = $yr . "-" . $mn."-31";
 	}
	else
	{
		$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
		$date_part_from = $cbo_year_selector . "-" . $cbo_month_selector."-01";
		$date_part_to = $cbo_year_selector . "-" . $cbo_month_selector."-31";
	}
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and  b.company_id='$cbo_company_id'";	
	if ($location_id==0) $location_id=""; else  $location_id="and  b.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else  $division_id="and  b.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and  b.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and  b.subsection_id='$subsection_id'";	
	if ($designation_id==0) $designation_id=""; else $designation_id="and  b.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and  b.category='$cbo_emp_category'";	
	if ($department_id==0) $department_id=""; else	$department_id=" and  b.department_id='$department_id'";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 

	
	?>
    <div align="center" style="width:1300px; font-size:16px"><b><? echo $company_details[$cbo_company_id]; ?></b></div>
	<table cellpadding="0" width="1200" cellspacing="0" border="1" class="rpt_table" rules="all">
		<thead>
             <tr>
             	<th width='38'>SL</th>
                <th width='85'>Emp Code</th>
                <th width='80'>ID Card</th>
                <th width='168'>Emp Name</th>
                <th width='120'>Designation</th>
                <th width='120'>Join Date</th>
                
                <th width='36'>Total Days</th>
                <th width='53'>Present</th>
                <th width='51'>Absent</th>
                <th width='38'>Late</th>
                <th width='45'>Leave</th>
                <th width='32'>MR</th>
                
                <th width='30'>WOD</th>
                <th width='30'>HD</th>
                <th width='32'>PL</th>
                <th width='30'>PW</th>
                <th width='32'>PH</th>
                <th width='50'>Total Present</th>
                <th width='57'>Payable Days</th>
                <th>Job Card</th>
            </tr> 
    	</thead>
        </table>
        <table  cellpadding="0" width="1200" cellspacing="0" border="1" class="rpt_table" id="table_body" >	 

	<?
			
		$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		$i=0;
		
		$sql_attnd=" SELECT   CONCAT(b.first_name, ' ', b.middle_name, ' ', b.last_name) AS name,b.category,b.id_card_no,b.joining_date,a.emp_code,a.department_id,a.location_id, a.division_id, a.section_id, a.subsection_id, a.designation_id,
						count(CASE WHEN a.status ='P' THEN a.emp_code END) AS 'present',
						count(CASE WHEN a.status ='P' and is_regular_day=0 THEN a.emp_code END) AS 'gn_present',
						count(CASE WHEN a.status ='A' THEN a.emp_code END) AS 'absent',
 						count(CASE WHEN a.status ='D' THEN a.emp_code END) AS 'late',
 						count(CASE WHEN a.status ='GH' THEN a.emp_code END) AS 'g_holiday',
						count(CASE WHEN a.status ='FH' THEN a.emp_code END) AS 'f_holiday',
						count(CASE WHEN a.status ='CH' THEN a.emp_code END) AS 'c_holiday',
						count(CASE WHEN a.status ='H' THEN a.emp_code END) AS 'h_holiday',
						count(CASE WHEN a.status ='W' THEN a.emp_code END) AS 'weekday',
						count(CASE WHEN a.status ='MR' THEN a.emp_code END) AS 'movement',
						count(CASE WHEN a.status ='CL' THEN a.emp_code END) AS 'cl',
						count(CASE WHEN a.status ='FL' THEN a.emp_code END) AS 'fl',
						count(CASE WHEN a.status ='EL' THEN a.emp_code END) AS 'el',
						count(CASE WHEN a.status ='SL' THEN a.emp_code END) AS 'sl',
						count(CASE WHEN a.status ='ML' THEN a.emp_code END) AS 'ml',
						count(CASE WHEN a.status ='SpL' THEN a.emp_code END) AS 'spl',
						count(CASE WHEN a.status ='LWP' THEN a.emp_code END) AS 'lwp',
						count(CASE WHEN a.status ='SP' THEN a.emp_code END) AS 'sp'
 					FROM 
						hrm_attendance a,hrm_employee b
					where 
						a.emp_code=b.emp_code and a.attnd_date BETWEEN '$date_part_from' AND '$date_part_to' $company_id $category $department_id $location_id  $division_id $section_id $subsection_id $designation_id $salary_based
					group by b.emp_code 
					$orderby";
					
		//echo $sql_attnd;die;
		$results1 = mysql_query( $sql_attnd ) or die( $sql_attnd . "<br />" . mysql_error() );						
 		while( $results = mysql_fetch_assoc( $results1 ) ) 
		{
					
			$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				//if(in_array($row[company_id],$company_arr))
				//{
					if(in_array($results[location_id],$location_arr))
					{
							if(in_array($results[division_id],$division_arr))
							{
									if(in_array($results[department_id],$department_arr))
									{
										if(in_array($results[section_id],$section_arr))
										{
											if(in_array($results[subsection_id],$subsection_arr))
											{}
											else
											{
												$subsection_arr[$results[subsection_id]]=$results[subsection_id];
												$new_subsec=1;
											}
										}
										else
										{
											$section_arr[$results[section_id]]=$results[section_id];
											$subsection_arr[$results[subsection_id]]=$results[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else
									{
										$department_arr[$results[department_id]]=$results[department_id];
										$section_arr[$results[section_id]]=$results[section_id];
										$subsection_arr[$results[subsection_id]]=$results[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else
							{
									$division_arr[$results[division_id]]=$results[division_id];
									$department_arr[$results[department_id]]=$results[department_id];
									$section_arr[$results[section_id]]=$results[section_id];
									$subsection_arr[$results[subsection_id]]=$results[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else
					{
						$location_arr[$results[location_id]]=$results[location_id];
						$division_arr[$results[division_id]]=$results[division_id];
						$department_arr[$results[department_id]]=$results[department_id];
						$section_arr[$results[section_id]]=$results[section_id];
						$subsection_arr[$results[subsection_id]]=$results[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
			
			/*	
				}//company
			else
			{
				$company_arr[$results[company_id]]=$results[company_id];
				$location_arr[$results[location_id]]=$results[location_id];
				$division_arr[$results[division_id]]=$results[division_id];
				$department_arr[$results[department_id]]=$results[department_id];
				$section_arr[$results[section_id]]=$results[section_id];
				$subsection_arr[$results[subsection_id]]=$results[subsection_id];
				$new_com=1;
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//company else
			*/	 
				 
			//header print here 
			if($new_com==1 && $status_com==1)
			{					
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Company : <? echo $company_details[$results[company_id]]; ?></th></tr><?
			}
			if($new_loc==1 && $status_loc==1)
			{					
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$results[location_id]]; ?></th></tr><?
			}				
			if($new_divis==1 && $status_divis==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$results[division_id]]; ?></th></tr><?
			}
			if($new_dept==1 && $status_dept==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Department : <? echo $department_details[$results[department_id]]; ?></th></tr><?
			}
			if($new_sec==1 && $status_sec==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Section : <? echo $section_details[$results[section_id]]; ?></th></tr><?
			}
			if($new_subsec==1 && $status_subsec==1)
			{
				?><tr><th colspan="20" align="left" bgcolor="#CCCCCC">Sub Section : <? echo $subsection_details[$results[subsection_id]]; ?></th></tr><?
			}
			
			$i++;
 			if ($i%2==0)$bgcolor="#E9F3FF";  
			else $bgcolor="#FFFFFF";
					
 			$present=0;$absent=0;$late=0;$leave=0;$whday=0;$ghday=0;$pl=0;$ph=0;$pw=0;$ot=0;$mr=0;				
			$present=$results['present'];
			$absent=$results['absent'];
			$late=$results['late'];
			$whday=$results['weekday'];
			$ghday=$results['g_holiday']+$results['f_holiday']+$results['c_holiday']+$results['h_holiday'];
			$mr=$results['movement'];
			$leave=$results['cl']+$results['fl']+$results['el']+$results['sl']+$results['ml']+$results['spl']+$results['lwp']+$results['sp'];
			 
			$total_present=$present+$late+$pl+$pw+$ph+$mr;
			$total_days=$present+$late+$absent+$whday+$ghday+$leave+$mr;
			$payable_days=$total_days-$absent;
			 
			if($late>2)$color='#EE3900';else$color='';
 			
		?>
					
            <tr bgcolor="<? echo $bgcolor; ?>">
            	<td width='35' height='50'><? echo $i; ?></td>
                <td width='85' height='50'><? echo $results['emp_code']; ?></td>
                <td width='80'><? echo $results['id_card_no']; ?></td>
                <td width='165'><? echo $results['name']; ?></td>
                <td align='left' width='120'><? echo $designation_chart[$results['designation_id']]; ?></td>
                <td width='120'><? echo convert_to_mysql_date($results['joining_date']); ?></td>
                
                <td width='35'><? echo $total_days;?></td>
                 <td width='52'><a href='#' onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&status=P&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $present;?></a></td>
                <td width='50'><a href='#' onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&status=A&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $absent;?></a></td>
                <td width='38' bgcolor="<? echo $color;?>">&nbsp;<a href='#' onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&status=D&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $late;?></a></td>
                <td width='45'><a href='#' onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&status=CL&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $leave;?></a></td>
                <td width='30'><a href='#' onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&status=MR&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $mr;?></a></td>						
                <td width='31'><a href='#' onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&status=W&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $whday;?></a></td>
                <td width='30'><a href='#' onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&status=H&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"><? echo $ghday;?></a></td>
                <td width='30'><? echo $pl;?></td>
                <td width='30'><? echo $pw;?></td>
                <td width='29'><? echo $ph;?></td>
                <td width='50'><? echo $total_present;?></td>
                <td width='55'><? echo $payable_days;?></td>
                <td><input type="button" value="View" style="width:35px" class="formbutton" onclick="openmypage('includes/generate_emp_job_card.php?action=emp_job_card_report&emp_code=<? echo $results['emp_code']; ?>&cbo_year_selector=<? echo $cbo_year_selector; ?>&cbo_month_selector=<? echo $cbo_month_selector; ?>&emp_name=<? echo $results['name'];?>','Partial Attendance'); return false"/></td>
            </tr>
                    
		  <?			
  		}	
	?>
		
	</table> 

<?

	
	$html = ob_get_contents();
	ob_clean();
	
 	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}