//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();


function fnc_punch_report() {		//Punch Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#date').focus();
			$(this).html('Please provide date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please select company to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&date=' + $('#date').val() + '&company_id=' + $('#company_id').val() + '&location_id=' + $('#location_id').val()
				+ '&division_id=' + $('#division_id').val() + '&department_id=' + $('#department_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=punch_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_punch_report;
		http.send(null);
	}
}

function response_punch_report() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('###');
		//showResult(escape(response[0]),1,'data_panel2');
		$('#data_panel').html( '<a href="' + response[1] + '">PDF Report</a>' + response[0] );
		// $('#data_panel2').html( response[0] );
		arrange_table();
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
function fnc_daily_attnd_report() {		//Punch Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#date').focus();
			$(this).html('Please provide date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please select company to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&date=' + $('#date').val() + '&company_id=' + $('#company_id').val() + '&location_id=' + $('#location_id').val()
				+ '&division_id=' + $('#division_id').val() + '&department_id=' + $('#department_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=daily_attn_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_daily_attnd_report;
		http.send(null);
	}
}

function response_daily_attnd_report() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText;
		//alert(response);
		//showResult(escape(response[0]),1,'data_panel2');
		//$('#data_panel2').html( '<a href="' + response[1] + '">PDF Report</a>' + response[0] );
		 $('#data_panel2').html( '' );
		 $('#data_panel2').html( response );
		 $("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}

/*
function fnc_daily_total_punch_report() {		//Details Punch Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#date').focus();
			$(this).html('Please provide date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#txt_emp_code').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_emp_code').focus();
			$(this).html('Please Enter Employee COde.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&date=' + $('#date').val() + '&txt_emp_code=' + $('#txt_emp_code').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=total_punch_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_total_punch_report;
		http.send(null);
	}
}

function response_total_punch_report() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText;
		//alert(response);
		//showResult(escape(response[0]),1,'data_panel2');
		//$('#data_panel2').html( '<a href="' + response[1] + '">PDF Report</a>' + response[0] );
		 $('#data_panel2').html( '' );
		 $('#data_panel2').html( response );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Punch Report generated successfully' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/

/*function fnc_attn_daily_inlastout_report(){
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	if( $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_date').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	//else if( $('#department_id').val() == 0 ) {
		//error = true;
		//$('#messagebox').fadeTo( 200, 0.1, function() {
			//$('#department_id').focus();
			//$(this).html('Please select Department to search.').addClass('messageboxerror').fadeTo(900,1);
		//});
	//}
	//else if( $('#section_id').val() == 0 ) {
		//error = true;
		//$('#messagebox').fadeTo( 200, 0.1, function() {
			//$('#section_id').focus();
			//$(this).html('Please select Section to search.').addClass('messageboxerror').fadeTo(900,1);
		//});
	//}
	if( error == false ) {		
		data = '&txt_date=' + $('#txt_date').val()+ '&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() +'&division_id=' + $('#division_id').val() +'&department_id=' + $('#department_id').val() + '&section_id=' + $('#section_id').val()+ '&subsection_id=' + $('#subsection_id').val()+ '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=daily_inlastout' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_daily_inlastout;
		http.send(null);
	}
}

function response_daily_inlastout() {	
	if(http.readyState == 4) {		
		var response =http.responseText.split('####');
		//alert(response[1]);		
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/


/*function fnc_monthly_attnd_audit()
{
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		data = '&cbo_month_selector=' + $('#cbo_month_selector').val() 
				+ '&cbo_year_selector=' + $('#cbo_year_selector').val()
				+' &cbo_emp_category=' + $('#cbo_emp_category').val() 
				+ '&cbo_company_id=' + $('#cbo_company_id').val()				
				+ '&location_id=' + $('#location_id').val() 
				+ '&division_id=' + $('#division_id').val()
				+ '&department_id=' + $('#department_id').val()
				+ '&section_id=' + $('#section_id').val() 
				+ '&subsection_id=' + $('#subsection_id').val()
				+ '&designation_id=' + $('#designation_id').val();
				
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=monthly_attn_audit_rpt' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_monthly_attnd_audit;
		http.send(null); 
	}
}


function response_fnc_monthly_attnd_audit() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[0]);		
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		 $("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/
 
 
function fnc_male_female_ratio_report(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false;	
	
	if( error == false ) {		
		data = '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val();
		//alert(data);return;
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=male_female_ratio' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_male_female_ratio_report;
		http.send(null);
	}
	
}

function response_fnc_male_female_ratio_report() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[1]);		 	
		$('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}


/*
//ekram 
//report name :religion ratio report

function fnc_emp_religion_ratio(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false;	
	
	if( error == false ) {		
		data = '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val();
		//alert(data);return;
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=religion_ratio' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_emp_religion_ratio;
		http.send(null);
	}
	
}

function response_fnc_emp_religion_ratio() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[1]);		 	
		$('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}*/

/*function fnc_employee_ot_sheet(){	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#txt_fromdate').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_fromdate').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else if( $('#txt_todate').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_todate').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Comapny.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else
	{
		if( error == false ) {		
			data = '&txt_fromdate=' + $('#txt_fromdate').val()
					+ '&txt_todate=' + $('#txt_todate').val() 
					+ '&cbo_emp_category=' + $('#cbo_emp_category').val()
					+'&company_id=' + $('#cbo_company_id').val() 
					+ '&location_id=' + $('#location_id').val()
					+ '&division_id=' + $('#division_id').val() 
					+ '&department_id=' + $('#department_id').val()
					+ '&section_id=' + $('#section_id').val() 
					+ '&subsection_id=' + $('#subsection_id').val() 
					+ '&emp_code=' + $('#emp_code').val();
			
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_hrm.php?action=ot_sheet_payment_actual' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_fnc_employee_ot_sheet;
			http.send(null);
		}
	}
}

function response_fnc_employee_ot_sheet() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		//alert(response);		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/

function fnc_employee_extra_ot_sheet(){	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#txt_fromdate').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_fromdate').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else if( $('#txt_todate').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_todate').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Comapny.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else
	{
		if( error == false ) {		
			data = '&txt_fromdate=' + $('#txt_fromdate').val()
					+ '&txt_todate=' + $('#txt_todate').val() 
					+ '&cbo_emp_category=' + $('#cbo_emp_category').val()
					+'&company_id=' + $('#cbo_company_id').val() 
					+ '&location_id=' + $('#location_id').val()
					+ '&division_id=' + $('#division_id').val() 
					+ '&department_id=' + $('#department_id').val()
					+ '&section_id=' + $('#section_id').val() 
					+ '&subsection_id=' + $('#subsection_id').val() 
					+ '&emp_code=' + $('#emp_code').val();
			
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_hrm.php?action=extra_ot_sheet' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_fnc_employee_extra_ot_sheet;
			http.send(null);
		}
	}
}

function response_fnc_employee_extra_ot_sheet() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		//alert(response);		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}


/*
function fnc_monthly_extra_ot_sheet(){	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#txt_fromdate').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_fromdate').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else if( $('#txt_todate').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_todate').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Comapny.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	else
	{
		if( error == false ) {		
			data = '&cbo_year_selector=' + $('#cbo_year_selector').val()
					+ '&cbo_month_selector=' + $('#cbo_month_selector').val() 
					+ '&cbo_emp_category=' + $('#cbo_emp_category').val()
					+'&company_id=' + $('#cbo_company_id').val() 
					+ '&location_id=' + $('#location_id').val()
					+ '&division_id=' + $('#division_id').val() 
					+ '&department_id=' + $('#department_id').val()
					+ '&section_id=' + $('#section_id').val() 
					+ '&subsection_id=' + $('#subsection_id').val() 
					+ '&emp_code=' + $('#emp_code').val();
			
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_hrm.php?action=monthly_extra_ot_sheet' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_fnc_monthly_extra_ot_sheet;
			http.send(null);
		}
	}
}

function response_fnc_monthly_extra_ot_sheet() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		//alert(response);		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/



/*

function fnc_manual_attn_list(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( $('#cbo_year_selector').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_year_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
			
	data = '&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&txt_date=' + $('#txt_date').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val()+'&division_id=' + $('#division_id').val() + '&department_id=' + $('#department_id').val()+ '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&emp_code=' + $('#emp_code').val()+'&group_by_id='+$('#group_by_id').val()+    						    '&order_by_id='+$('#order_by_id').val();
					
		
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=manual_attn_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_manual_attn_list;
		http.send(null);
	}
	
}

function response_fnc_manual_attn_list() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[1]);		 
		
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
	
		//$('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
		//$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully. ' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/

/*function fnc_ques_attn_list(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( $('#cbo_year_selector').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_year_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( $('#cbo_company_id').val() == "" || $('#cbo_company_id').val() == 0) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {		
		
		//'&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&txt_date=' + $('#txt_date').val() +
		data = '&txt_from_date=' + $('#txt_from_date').val() +'&txt_to_date=' + $('#txt_to_date').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() + '&division_id=' + $('#division_id').val()+'&department_id=' + $('#department_id').val()+ '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&emp_code=' + $('#emp_code').val();
		
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=ques_attn_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_ques_attn_list;
		http.send(null);
	}
	
}

function response_fnc_ques_attn_list() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		//alert(response[0]);		 	
		//$('#data_panel').html(  '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>' );
		//$('#data_panel2').html( response[0] );
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );

		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}

*/

/*function fnc_absent_late_trent(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_type_selector').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_type_selector').focus();
			$(this).html('Please select type to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( $('#cbo_company_id').val() == "" || $('#cbo_company_id').val() == 0) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {		
		data = '&cbo_year_selector=' + $('#cbo_year_selector').val() +'&cbo_type_selector=' + $('#cbo_type_selector').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val()+'&division_id=' + $('#division_id').val() + '&department_id=' + $('#department_id').val()+ '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&emp_code=' + $('#emp_code').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=absent_late_trent_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_absent_late_trent;
		http.send(null);
	}
	
}

function response_fnc_absent_late_trent() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		//alert(response[1]);		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/


/*function fnc_line_wise_ot(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_year_selector').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_year_selector').focus();
			$(this).html('Please select year to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please Select Company To Search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else 
	{
		if( error == false ) {		
			data = '&cbo_month_selector=' + $('#cbo_month_selector').val() 
					+ '&cbo_year_selector=' + $('#cbo_year_selector').val() 
					+ '&cbo_emp_category=' + $('#cbo_emp_category').val() 
					+ '&cbo_company_id=' + $('#cbo_company_id').val() 
					+ '&location_id=' + $('#location_id').val()
					+ '&division_id=' + $('#division_id').val() 
					+ '&department_id=' + $('#department_id').val()
					+ '&section_id=' + $('#section_id').val() 
					+ '&subsection_id=' + $('#subsection_id').val() ;
			
			nocache = Math.random();
			http.open( 'GET', 'includes/save_update_hrm.php?action=line_wise_ot' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_fnc_line_wise_ot;
			http.send(null);
		}
	}
	
}

function response_fnc_line_wise_ot() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/



/*function fnc_monthly_check_sheet()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#cbo_year_selector').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_year_selector').focus();
			$(this).html('Please select Year to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
		$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() +'&division_id=' + $('#division_id').val() +'&department_id=' + $('#department_id').val()+ '&location_id=' + $('#location_id').val() + '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=monthly_check_sheet' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_monthly_check_sheet;
		http.send(null);
	}
}


function response_fnc_monthly_check_sheet() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[0]);		
		 $('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
		 $('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		 $('#data_panel2').html( response[0] );
		 $("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
*/
 
function fnc_monthly_attendance_audit_summary()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&txt_date=' + $('#txt_date').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&department_id=' + $('#department_id').val()+ '&location_id=' + $('#location_id').val() +'&division_id=' + $('#division_id').val() +'&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=monthly_attn_summery_audit' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_monthly_attendance_audit_summary;
		http.send(null);
	}
}


function response_fnc_monthly_attendance_audit_summary() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[0]);		
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		 $("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}


function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel2').innerHTML);
	d.close();
}


function new_window_short()
{
	//document.getElementById('scroll_body').style.overflow="auto";
	//document.getElementById('scroll_body').style.height="auto";
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel2').innerHTML+'</body</html>');
	d.close();
	//document.getElementById('scroll_body').style.overflow="scroll";
	//document.getElementById('scroll_body').style.height="280px";
}