<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


if($action=='monthly_check_sheet'){
			
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$tot_days = cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$txt_from_date = $cbo_year_selector . "-" . $cbo_month_selector . "-" . "01";
	$txt_to_date = $cbo_year_selector . "-" . $cbo_month_selector . "-" . $tot_days;
	//$tot_days=daysdiff($txt_from_date, $txt_to_date);
	
	//payroll head 
	$salary_head_list = array();	
	$sql_sal="select * from lib_payroll_head where id in (5,6,7,35) and status_active=1 and is_deleted=0 order by id";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);	
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_head_list[$row_sal[id]]=$row_sal[is_applicable];
	}
	

//print_r($salary_head_list);die;	
	
	//variable settings for ot
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $hrmrow = mysql_fetch_assoc( $result ) ) {
 		$var_hrm_chart[$hrmrow['company_name']] = array();
		$var_hrm_chart[$hrmrow['company_name']]['adjust_in_time'] = mysql_real_escape_string( $hrmrow['adjust_in_time'] );
		$var_hrm_chart[$hrmrow['company_name']]['adjust_out_time'] = mysql_real_escape_string( $hrmrow['adjust_out_time'] );
		$var_hrm_chart[$hrmrow['company_name']]['first_ot_limit'] = mysql_real_escape_string( $hrmrow['first_ot_limit'] );
		$var_hrm_chart[$hrmrow['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $hrmrow['one_hour_ot_unit'] );
		$var_hrm_chart[$hrmrow['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $hrmrow['allow_ot_fraction'] );
		$var_hrm_chart[$hrmrow['company_name']]['ot_start_minute'] = mysql_real_escape_string( $hrmrow['ot_start_minute'] );
	}
	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id in ($department_id)";
	if ($division_id==0) $division_id=""; else  $division_id="and division_id in ($division_id)";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in ($designation_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
			
	ob_start();	
		
	?>    
	<table cellpadding="0" cellspacing="0" border="1" width="900" align="center" style="border:1px solid #000; font-size:10px" rules="all" class="rpt_table">       
    <?	
	
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM 
					hrm_employee
				WHERE 
					emp_code!='' 
					$category
					$cbo_company_id 
					$location_id
					$division_id
					$department_id 
					$section_id 
					$subsection_id 
					$designation_id 
					$salary_based
				
					order by company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code";
				//order by section_id,id_card_no ASC"; 	group by company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_department=array();$new_section=array();
		while($rows=mysql_fetch_array($result)){			
			
			
			$emp_condition=0; // Regular Worker
			if ($rows[status_active]==0) // In active Employee
			{
				$d="separated_from";$dd="hrm_separation";$ddd="emp_code='$rows[emp_code]' order by id desc";
				$old_mod= return_field_value($d,$dd,$ddd);
				
				if (strtotime($old_mod)>=strtotime($txt_from_date) && strtotime($old_mod)<strtotime($txt_to_date))
				{
					$emp_condition=1; // Resign Worker
				}
				else $emp_condition=3; // not  Worker
			}
			
		   if ($rows[status_active]==1)
			{
				if (strtotime($rows[joining_date])>=strtotime($txt_from_date) && strtotime($rows[joining_date])<=strtotime($txt_to_date))
				{
					$emp_condition=2; // New Worker
				}
				else if (strtotime($rows[joining_date])>strtotime($txt_to_date) ) $emp_condition=3; // not Worker
			}
			
				$attn_sql = "SELECT 
							count(CASE WHEN status in ('CL','ML','SL','EL','SpL','LWP') THEN status END) as tl,
							count(CASE WHEN status in ('H','FH','CH','GH','W') THEN status END) as th,
							count(CASE WHEN status in ('P') THEN status END) as tp,
							count(CASE WHEN status in ('A') THEN status END) as ta,
							count(CASE WHEN status in ('D') THEN status END) as td
							FROM hrm_attendance
							WHERE 
							emp_code='$rows[emp_code]' and attnd_date LIKE '%$date_part%'";
						
				$results = mysql_query( $attn_sql ) or die( $attn_sql . "<br />" . mysql_error() );				
				//echo $emp_condition."##".$rows[emp_code]."-"; 
 				if($emp_condition<>3){
					
					$row=mysql_fetch_array($results); //retrieve data
					
					$total_leave_days = $row['tl'];
					$late_days	= $row['td'];								
					$absent_days = $row['ta'];
					
					//echo $absent_days;die;

 					if ($emp_condition==2){ // New
						$total_payable_days=datediff('d',$rows[joining_date],$txt_to_date);	//New Join 
						$status='N';
					}
					else if ($emp_condition==1){ // Resign	
						$total_payable_days=datediff('d',$txt_from_date,$old_mod);	// Resign
						$status='D';
					}
					else{
						$total_payable_days=$tot_days;	//Regular
						$status='R';
					}
					$calendar_days=$tot_days;
					$not_payable_days=$calendar_days-$total_payable_days;					
					
					
					//ot and eot and further ot here
					$buyer_ot=0;
					$extra_ot=0;
					$fur_extra_ot=0;
					$basic_salary=return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$rows[emp_code]");
					$ot_rate=$basic_salary/104;
					$emp_salary_sheet=0;$buyer_ot=0;
					if($salary_head_list[5]==1 ) // Over Time
					{
						
						$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";						
						$sql_data_exe=mysql_query($sql_data);
						$old_hr=0;
						$old_slot=0;
						$ot_slot=0;
						$u=0;
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							$u++;
							$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
							$ot_multiplier=$sql_data_rslt[overtime_multiplier];
							
							$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".$txt_from_date."' and '".$txt_to_date."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
							$total_ot_hr=$total_ot_hr-$old_hr;
							$old_hr= $old_hr+$total_ot_hr; 
							
							if($sql_data_rslt[overtime_head]==5)
							{
								$emp_salary_sheet= $emp_salary_sheet+($ot_rate*$ot_multiplier*$total_ot_hr);  
								$buyer_ot = $buyer_ot+$total_ot_hr;								
							}
						}						
						$ot_slot=0;
					}
					
					if($salary_head_list[6]==1 ) // Extra Over Time
					{
						$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";
						$sql_data_exe=mysql_query($sql_data);
						$old_hr=0;
						$old_slot=0;
						$ot_slot=0;
						$u=0;
						
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							$u++;
							$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
							$ot_multiplier=$sql_data_rslt[overtime_multiplier];
							
							$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".$txt_from_date."' and '".$txt_to_date."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
							$total_ot_hr=$total_ot_hr-$old_hr;
							$old_hr= $old_hr+$total_ot_hr; 
		 
							if($sql_data_rslt[overtime_head]==6)
							{
								$emp_salary_sheet= $emp_salary_sheet+($ot_rate*$ot_multiplier*$total_ot_hr);  
								$extra_ot= $extra_ot+$total_ot_hr; 
							}
						}
						
					}
					if( $salary_head_list[7]==1) // Further Extra Over Time
					{
						$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";
						$sql_data_exe=mysql_query($sql_data);
						$old_hr=0;
						$old_slot=0;
						$ot_slot=0;
						$u=0;
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							$u++;
							$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
							$ot_multiplier=$sql_data_rslt[overtime_multiplier];
							
							$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".$txt_from_date."' and '".$txt_to_date."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
							$total_ot_hr=$total_ot_hr-$old_hr;
							$old_hr= $old_hr+$total_ot_hr; 
							
							if($sql_data_rslt[overtime_head]==7)
							{
								$emp_salary_sheet = $emp_salary_sheet +($ot_rate*$ot_multiplier*$total_ot_hr);  
								$fur_extra_ot= $fur_extra_ot+$total_ot_hr; 
							}
						} 
					}
					
					$att_bonus_amount=0;
					//print_r($salary_head_list[35]);die;			
					if($salary_head_list[35]==1)  // Attendance Bonus  Rules
						{ //echo $emp_condition;die;
							if( $emp_condition==0)// Regular Worker
							{
								$sql_data="select * from  lib_policy_attendance_bonus_definition where policy_id=$rows[attendance_bonus_policy] order by id asc";
								//echo $sql_data;die;
								$sql_data_exe=mysql_query($sql_data); 
								while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
								{
									if($sql_data_rslt[total_leave_criteria]>= $total_leave_days && $sql_data_rslt[late_criteria]>=$late_days  &&   $sql_data_rslt[absent_criteria]>=$absent_days )
									{
										$att_bonus_amount=round($sql_data_rslt[amount]); break;
									}
								}							
														
							}
							
						}
						//echo $att_bonus_amount."_"."ekram";die;		
 					
					if (in_array($rows['department_id'], $new_department))
					{
							if (in_array($rows['section_id'], $new_section))
								{
									$i++;
								}
							else
								{				
									
							?>                            
									<tr><td colspan="17" align="center"><font size="+1"><? echo $company_details[$rows['company_id']]['company_name']; ?></font><br />
                                		Address: Plot No # <? echo $company_details[$rows['company_id']]["plot_no"]; ?>, Level No # <? echo $company_details[$rows['company_id']]["level_no"]; ?>, Block No # <? echo $company_details[$rows['company_id']]["block_no"]; ?>, Road No # <? echo $company_details[$rows['company_id']]["road_no"]; ?>, <? echo $company_details[$rows['company_id']]["city"]; echo $company_details[$rows['company_id']]["country_name"];?><br />
                                     </td></tr>
							<? }
					}
					else
						{
												
                            $new_department[$rows[department_id]]=trim($rows[department_id]);
							$new_section=array();
							$new_section[$rows[section_id]]=trim($rows[section_id]);
							 
                               ?><tr>
									<td colspan="7" >
                                    	Section : <? echo $section_details[$rows['section_id']]['section_name']; ?> 
                                    </td>
                                    <td colspan="10" >
                                        Department : <? echo $department_details[$rows['department_id']]['department_name']; ?>                                       
                                    </td>
								</tr>
								<tr align="center">
									<td width="20"><b>SL</b></td>
                                    <td width="50"><b>ID Card</b></td>
                                    <td width="50"><b>Emp Code</b></td>
                                    <td width="130"><b>Name</b></td>
                                    <td width="100"><b>Designation</b></td>
                                    <td width="70"><b>DOJ</b></td>
                                    <td width="70"><b>F/L</b></td>
                                    <td width="50"><b>T.Leave</b></td>
                                    <td width="50"><b>T.Late</b></td>
                                    <td width="50"><b>T.Holiday</b></td>
									<td width="40"><b>P.Day</b></td>
                                    <td width="40"><b>A.Day</b></td>
                                    <td width="50"><b>NP Day</b></td>
                                    <td width="40"><b>OT</b></td>
                                    <td width="40"><b>EOT</b></td>
                                    <td width="40"><b>AB</b></td>
                                    <td width="40"><b>ST</b></td>  
								</tr>
							<?
							
							$new_section[$i]=trim($rows[section_id]);
							$i=1;
						}
				
				
			//if ($i%2==0) { $bgcolor="#EEEEEE"; } else { $bgcolor="#FFFFFF";}
			?>    
				<tr bgcolor="<? echo $bgcolor;?>">
					<td><? echo $i; ?></td>
					<td><? echo $rows['id_card_no']; ?></td>
					<td><? echo $rows['emp_code']; ?></td>
					<td><? echo $rows['name']; ?></td>
					<td><? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td> 
                    <td><? echo convert_to_mysql_date($rows[joining_date]);?></td> 
                    <td><? echo $subsection_details[$rows['subsection_id']]['subsection_name']; ?></td>          	
					<td align="right"><? echo $row['tl'];?></td> 
                    <td align="right"><? echo $row['td'];?></td>	 	
					<td align="right"><? echo $row['th'];?></td>
					<td align="right"><? echo $row['th']+$row['tp']+$row['td'];?></td>
					<td align="right"><? echo $row['ta'];?></td>
					<td align="right"><? echo $not_payable_days;?></td>
					<td align="right"><? echo $buyer_ot;?></td>
					<td align="right"><? echo $extra_ot+$fur_extra_ot;?></td>
					<td align="right"><? echo $att_bonus_amount;?></td>
					<td align="right"><? echo $status;?></td>					            	
				</tr>  
			<?	
			}
		
		}
    ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}


//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$explode_val=explode(" ",$row["starting_date"]);
		//$value = $explode_val[0];
		$value = substr($explode_val[0],0,-3);//new line for new year
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}

//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$explode_val=explode(" ",$row["starting_date"]);
		//$value = $explode_val[0];
		$value = substr($explode_val[0],0,-3);//new line for new year
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>

