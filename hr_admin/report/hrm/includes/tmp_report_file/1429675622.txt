<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                HEAD OFFICE</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 22-04-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000409</td>
            <td width="80">0009999957</td>
            <td width="130">
				Md. Humayun Biswas  <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">Commercial</td>						
            <td width="100">Commercial</td>
            <td width="100"></td>
            <td width="70">22-04-2015</td>
            <td width="50"><p>02017</p></td>
            <td width="60"><b>8:55 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">HEAD OFFICE STAFF</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000410</td>
            <td width="80">0009999962</td>
            <td width="130">
				Md. Imam Hossain  <br />cell:             </td>
            <td width="120">Chief Accountant</td>
            <td width="100">Finance and Accounts</td>						
            <td width="100">Finance and Accounts</td>
            <td width="100"></td>
            <td width="70">22-04-2015</td>
            <td width="50"><p>02019</p></td>
            <td width="60"><b>9:14 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">HEAD OFFICE STAFF</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000411</td>
            <td width="80">0009999964</td>
            <td width="130">
				Abul Basar Md. Saifullah   <br />cell:             </td>
            <td width="120">Accounts Officer</td>
            <td width="100">Finance and Accounts</td>						
            <td width="100">Finance and Accounts</td>
            <td width="100"></td>
            <td width="70">22-04-2015</td>
            <td width="50"><p>02020</p></td>
            <td width="60"><b>9:11 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">HEAD OFFICE STAFF</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000385</td>
            <td width="80">0009999952</td>
            <td width="130">
				Md. Shafiqul Islam  <br />cell:             </td>
            <td width="120">Peon </td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">22-04-2015</td>
            <td width="50"><p>02028</p></td>
            <td width="60"><b>9:04 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">HEAD OFFICE STAFF</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000391</td>
            <td width="80">0009999960</td>
            <td width="130">
				Md. Kamrul Islam  <br />cell:             </td>
            <td width="120">Officer</td>
            <td width="100">Commercial</td>						
            <td width="100">Commercial</td>
            <td width="100"></td>
            <td width="70">22-04-2015</td>
            <td width="50"><p>02109</p></td>
            <td width="60"><b>8:55 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">HEAD OFFICE STAFF</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000359</td>
            <td width="80">0003059989</td>
            <td width="130">
				Sayed Mahamudul Hassan  <br />cell: 01711971995            </td>
            <td width="120">Accountant</td>
            <td width="100">Finance and Accounts</td>						
            <td width="100">Finance and Accounts</td>
            <td width="100"></td>
            <td width="70">22-04-2015</td>
            <td width="50"><p>02111</p></td>
            <td width="60"><b>9:10 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">HEAD OFFICE STAFF</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 6 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-04-22','Absent Information'); return false">0</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-04-22','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-04-22','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-04-22','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-04-22','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 6 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>