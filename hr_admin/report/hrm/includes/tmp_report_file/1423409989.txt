<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Peak Apparels Ltd.</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 09-02-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000023</td>
            <td width="80">0004298403</td>
            <td width="130">
				Mst. Julekha  <br />cell: 01720834232            </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00007</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000264</td>
            <td width="80">0000108925</td>
            <td width="130">
				Mst.Shahanaz  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00063</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000266</td>
            <td width="80">0004322275</td>
            <td width="130">
				Md.Hannan   Mia<br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00116</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000285</td>
            <td width="80">0011391682</td>
            <td width="130">
				Mst.Kolpona  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00132</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000328</td>
            <td width="80">0005221901</td>
            <td width="130">
				Mst. Ferdousi  <br />cell:             </td>
            <td width="120">Sr. S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00146</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000009</td>
            <td width="80">0004322254</td>
            <td width="130">
				Mst. Rohima  Khatun<br />cell: 01966854794            </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00406</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000113</td>
            <td width="80">0004298402</td>
            <td width="130">
				Mst. Rahima  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00418</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000092</td>
            <td width="80">0004296925</td>
            <td width="130">
				Mst. Lipi  Khatun<br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>00420</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000070</td>
            <td width="80">0004322306</td>
            <td width="130">
				Md. Hasibul   Hasan<br />cell: 01780628234            </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>01102</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000396</td>
            <td width="80">0005230765</td>
            <td width="130">
				Md. Badsha Alamgir  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>01106</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000397</td>
            <td width="80">0005221886</td>
            <td width="130">
				Md. Mizanur Rahman  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>01107</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000020</td>
            <td width="80">0004308206</td>
            <td width="130">
				Mst. Alema  <br />cell: 01680283540            </td>
            <td width="120">Cleaner</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Cleaning</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>01201</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000046</td>
            <td width="80">0004322315</td>
            <td width="130">
				Tagor Ali Pramanik  <br />cell: 01757902501            </td>
            <td width="120">Sweeper</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Cleaning</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>01203</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000149</td>
            <td width="80">0004306067</td>
            <td width="130">
				Mst.Anjuara  <br />cell:             </td>
            <td width="120">Folder. (Finishing)</td>
            <td width="100">Finishing</td>						
            <td width="100">Finishing</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>01509</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000305</td>
            <td width="80">0008093268</td>
            <td width="130">
				Md.Abdul  Bassd<br />cell:             </td>
            <td width="120">Operator</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>01813</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000075</td>
            <td width="80">0004298386</td>
            <td width="130">
				Md. Shahab  Uddin<br />cell:             </td>
            <td width="120">Purchase Incharge</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Purchase</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02016</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000112</td>
            <td width="80">0004322261</td>
            <td width="130">
				Md. Shaheen  Miah<br />cell:             </td>
            <td width="120">Manager Knitting</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02023</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000261</td>
            <td width="80">0015137168</td>
            <td width="130">
				Md.Rofiqul   Islam<br />cell:             </td>
            <td width="120">Feeder Man</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02024</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000077</td>
            <td width="80">0004306038</td>
            <td width="130">
				Dr. Mohammad Mazharul Islam<br />cell:             </td>
            <td width="120">Medical Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Medical</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02033</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000159</td>
            <td width="80">0004308495</td>
            <td width="130">
				Md.Rubel  Mia<br />cell:             </td>
            <td width="120">Mechanic</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02055</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000239</td>
            <td width="80">0008099634</td>
            <td width="130">
				Md.Abdur  Razzak<br />cell:             </td>
            <td width="120">Driver</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02095</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000325</td>
            <td width="80">0005240970</td>
            <td width="130">
				Md. Sirajul Islam  <br />cell:             </td>
            <td width="120">Fire Safety Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02104</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000326</td>
            <td width="80">0000485614</td>
            <td width="130">
				Md. Helal Uddin  <br />cell:             </td>
            <td width="120">Boiler Operator</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02105</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">24</td>
            <td width="50">0000356</td>
            <td width="80">0005233057</td>
            <td width="130">
				Md. Abdur Rahim  <br />cell:             </td>
            <td width="120">Supervisor</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02120</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">25</td>
            <td width="50">0000418</td>
            <td width="80">0005233070</td>
            <td width="130">
				Md. Arif  <br />cell:             </td>
            <td width="120">Mechanic</td>
            <td width="100">Sewing</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">09-02-2015</td>
            <td width="50"><p>02127</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000023,0000264,0000266,0000285,0000328,0000009,0000113,0000092,0000070,0000396,0000397,0000020,0000046,0000149,0000305,0000075,0000112,0000261,0000077,0000159,0000239,0000325,0000326,0000356,0000418&date=2015-02-09','Absent Information'); return false">25</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-09','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-09','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-09','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-09','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 25 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>