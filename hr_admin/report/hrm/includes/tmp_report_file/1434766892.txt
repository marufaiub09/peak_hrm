<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Peak Apparels Ltd.</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 20-06-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000264</td>
            <td width="80">0000108925</td>
            <td width="130">
				Mst.Shahanaz  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>00063</p></td>
            <td width="60"><b>7:21 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">21</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000553</td>
            <td width="80">0008678070</td>
            <td width="130">
				Md. Abu Sayid  <br />cell:             </td>
            <td width="120">Quality Inspector</td>
            <td width="100">Quality</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>00739</p></td>
            <td width="60"><b>7:10 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">10</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000342</td>
            <td width="80">0005215907</td>
            <td width="130">
				Mohiuddin  <br />cell:             </td>
            <td width="120">Quality Inspector</td>
            <td width="100">Quality</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>00934</p></td>
            <td width="60"><b>7:05 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">5</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000070</td>
            <td width="80">0004322306</td>
            <td width="130">
				Md. Hasibul   Hasan<br />cell: 01780628234            </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01102</p></td>
            <td width="60"><b>7:42 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">42</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000396</td>
            <td width="80">0005230765</td>
            <td width="130">
				Md. Badsha Alamgir  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01106</p></td>
            <td width="60"><b>7:10 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">10</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000508</td>
            <td width="80">0008688270</td>
            <td width="130">
				Md. Golam Robbani  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01110</p></td>
            <td width="60"><b>7:47 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">47</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000163</td>
            <td width="80">0004322309</td>
            <td width="130">
				Md.Abdus  Samad<br />cell:             </td>
            <td width="120">Loader</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Loader</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01205</p></td>
            <td width="60"><b>7:14 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">14</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000170</td>
            <td width="80">0004306071</td>
            <td width="130">
				Md.Siddik  Ali<br />cell:             </td>
            <td width="120">Loader</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Loader</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01208</p></td>
            <td width="60"><b>7:27 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">27</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000323</td>
            <td width="80">0005221850</td>
            <td width="130">
				Md. Mazeda Khatun  <br />cell:             </td>
            <td width="120">Cleaner</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Cleaning</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01211</p></td>
            <td width="60"><b>7:28 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">28</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000595</td>
            <td width="80">0008692724</td>
            <td width="130">
				Edul Bashar  <br />cell:             </td>
            <td width="120">Finishing Asst.</td>
            <td width="100">Finishing</td>						
            <td width="100">Finishing</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01537</p></td>
            <td width="60"><b>7:24 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">24</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000083</td>
            <td width="80">0004298369</td>
            <td width="130">
				Md. Zabed  Ali<br />cell:             </td>
            <td width="120">Operator</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01801</p></td>
            <td width="60"><b>8:02 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">62</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000082</td>
            <td width="80">0004322216</td>
            <td width="130">
				Md. Majedul  Islam<br />cell:             </td>
            <td width="120">Operator</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01803</p></td>
            <td width="60"><b>7:39 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">39</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000479</td>
            <td width="80">0008692699</td>
            <td width="130">
				Poritos Dewri  <br />cell:             </td>
            <td width="120">Asst. Knitting</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01818</p></td>
            <td width="60"><b>7:39 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">39</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000480</td>
            <td width="80">0008692718</td>
            <td width="130">
				Md. Sohel Mia  <br />cell:             </td>
            <td width="120">Operator</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01819</p></td>
            <td width="60"><b>7:39 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">39</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000540</td>
            <td width="80">0008678710</td>
            <td width="130">
				Md. Shakhowt Hossain  <br />cell:             </td>
            <td width="120">Operator</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01821</p></td>
            <td width="60"><b>7:55 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">55</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000596</td>
            <td width="80">0008678094</td>
            <td width="130">
				Md. Shah Alam  <br />cell:             </td>
            <td width="120">Sr. Operator</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>01822</p></td>
            <td width="60"><b>7:39 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">39</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000062</td>
            <td width="80">0004322308</td>
            <td width="130">
				Md. Ariful   Islam<br />cell: 01716860389            </td>
            <td width="120">Merchandiser</td>
            <td width="100">Marketing and Merchandising</td>						
            <td width="100">Merchandising</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02006</p></td>
            <td width="60"><b>8:05 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">65</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000156</td>
            <td width="80">0004298368</td>
            <td width="130">
				Md.Mainul Hasan  <br />cell:             </td>
            <td width="120">Store Officer (Knitting)</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02051</p></td>
            <td width="60"><b>7:56 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">56</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000162</td>
            <td width="80">0004306437</td>
            <td width="130">
				Md.All  Kaiyom<br />cell:             </td>
            <td width="120">IT Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">IT(Information Technology)</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02061</p></td>
            <td width="60"><b>7:27 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">27</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000219</td>
            <td width="80">0004318675</td>
            <td width="130">
				Md. Najrul   Islam<br />cell: 01725744730            </td>
            <td width="120">Assistant Merchandiser</td>
            <td width="100">Marketing and Merchandising</td>						
            <td width="100">Merchandising</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02068</p></td>
            <td width="60"><b>8:14 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">74</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000239</td>
            <td width="80">0008099634</td>
            <td width="130">
				Md.Abdur  Razzak<br />cell:             </td>
            <td width="120">Driver</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02095</p></td>
            <td width="60"><b>8:08 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">68</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000429</td>
            <td width="80">0005233022</td>
            <td width="130">
				Urmi Akter  <br />cell:             </td>
            <td width="120">Computer Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02129</p></td>
            <td width="60"><b>7:16 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">16</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000482</td>
            <td width="80">0008692052</td>
            <td width="130">
				Md. Hasanul Kabir  <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">Fabrics</td>						
            <td width="100">Fabrics</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02143</p></td>
            <td width="60"><b>7:46 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">46</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">24</td>
            <td width="50">0000566</td>
            <td width="80">0008678111</td>
            <td width="130">
				Md. Raqibul Hasan  <br />cell:             </td>
            <td width="120">Sr. Merchandiser</td>
            <td width="100">Marketing and Merchandising</td>						
            <td width="100">Merchandising</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02154</p></td>
            <td width="60"><b>7:45 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">45</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">25</td>
            <td width="50">0000585</td>
            <td width="80">0008678069</td>
            <td width="130">
				Md. Mazadul Islam  <br />cell:             </td>
            <td width="120">Sample Man</td>
            <td width="100">Sample</td>						
            <td width="100">Sample</td>
            <td width="100"></td>
            <td width="70">20-06-2015</td>
            <td width="50"><p>02155</p></td>
            <td width="60"><b>7:08 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">8</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-06-20','Absent Information'); return false">0</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-06-20','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-06-20','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000264,0000553,0000342,0000070,0000396,0000508,0000163,0000170,0000323,0000595,0000083,0000082,0000479,0000480,0000540,0000596,0000062,0000156,0000162,0000219,0000239,0000429,0000482,0000566,0000585&date=2015-06-20','Late Information'); return false">25 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-06-20','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 25 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>