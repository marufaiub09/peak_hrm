<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Peak Apparels Ltd.</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 14-03-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000185</td>
            <td width="80">0004296773</td>
            <td width="130">
				Mst. Shikha  <br />cell: 01937129495            </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00059</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000284</td>
            <td width="80">0011388355</td>
            <td width="130">
				Md.Rajib  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00137</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000372</td>
            <td width="80">0005232711</td>
            <td width="130">
				Mst. Salma Khatun  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00162</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000318</td>
            <td width="80">0005221849</td>
            <td width="130">
				Mst. Afroza Khatun  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-5</td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00429</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000366</td>
            <td width="80">0005230762</td>
            <td width="130">
				Mst. Amena Begum  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00443</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000426</td>
            <td width="80">0005221868</td>
            <td width="130">
				Sreemoti Chondona Rani  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00448</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000175</td>
            <td width="80">0004322156</td>
            <td width="130">
				Mst.Ayesha   Siddika<br />cell:             </td>
            <td width="120">Quality Inspector</td>
            <td width="100">Quality</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00710</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000338</td>
            <td width="80">0005221894</td>
            <td width="130">
				Shopon Hawlader  <br />cell:             </td>
            <td width="120">Sr. Quality Inspector</td>
            <td width="100">Quality</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00723</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000256</td>
            <td width="80">0011409822</td>
            <td width="130">
				Md.Abdul  Gaffar<br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>00911</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000070</td>
            <td width="80">0004322306</td>
            <td width="130">
				Md. Hasibul   Hasan<br />cell: 01780628234            </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>01102</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000397</td>
            <td width="80">0005221886</td>
            <td width="130">
				Md. Mizanur Rahman  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>01107</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000430</td>
            <td width="80">0005230760</td>
            <td width="130">
				Delwar Hossain  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>01108</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000437</td>
            <td width="80">0005223493</td>
            <td width="130">
				Shafik Mia  <br />cell:             </td>
            <td width="120">Sweeper</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Cleaning</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>01216</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000075</td>
            <td width="80">0004298386</td>
            <td width="130">
				Md. Shahab  Uddin<br />cell:             </td>
            <td width="120">Purchase Incharge</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Purchase</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02016</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000077</td>
            <td width="80">0004306038</td>
            <td width="130">
				Dr. Mohammad Mazharul Islam<br />cell:             </td>
            <td width="120">Medical Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Medical</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02033</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000127</td>
            <td width="80">0004298809</td>
            <td width="130">
				Md. Abdul  Jalil<br />cell:             </td>
            <td width="120">Quality Controler</td>
            <td width="100">Quality</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02048</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000229</td>
            <td width="80">0008163887</td>
            <td width="130">
				Md. Hira   Khan<br />cell:             </td>
            <td width="120">Sample Man</td>
            <td width="100">Sample</td>						
            <td width="100">Sample</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02081</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000316</td>
            <td width="80">0005223475</td>
            <td width="130">
				Rubaiat-E-Sultana  <br />cell:             </td>
            <td width="120">Trainee Merchandiser</td>
            <td width="100">Marketing and Merchandising</td>						
            <td width="100">Merchandising</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02103</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000325</td>
            <td width="80">0005240970</td>
            <td width="130">
				Md. Sirajul Islam  <br />cell:             </td>
            <td width="120">Fire Safety Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02104</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000326</td>
            <td width="80">0000485614</td>
            <td width="130">
				Md. Helal Uddin  <br />cell:             </td>
            <td width="120">Boiler Operator</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02105</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000356</td>
            <td width="80">0005233057</td>
            <td width="130">
				Md. Abdur Rahim  <br />cell:             </td>
            <td width="120">Supervisor</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02120</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000429</td>
            <td width="80">0005233022</td>
            <td width="130">
				Urmi Akter  <br />cell:             </td>
            <td width="120">Computer Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02129</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000436</td>
            <td width="80">000002</td>
            <td width="130">
				Md. Rubel Hawlader  <br />cell:             </td>
            <td width="120">Officer</td>
            <td width="100">Dyeing</td>						
            <td width="100">Dyeing</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02131</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">24</td>
            <td width="50">0000438</td>
            <td width="80">0005233024</td>
            <td width="130">
				Md. Rafiqul Islam  <br />cell:             </td>
            <td width="120">Iron Man (Sample)</td>
            <td width="100">Sample</td>						
            <td width="100">Sample</td>
            <td width="100"></td>
            <td width="70">14-03-2015</td>
            <td width="50"><p>02132</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000185,0000284,0000372,0000318,0000366,0000426,0000175,0000338,0000256,0000070,0000397,0000430,0000437,0000075,0000077,0000127,0000229,0000316,0000325,0000326,0000356,0000429,0000436,0000438&date=2015-03-14','Absent Information'); return false">24</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-03-14','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-03-14','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-03-14','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-03-14','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 24 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>