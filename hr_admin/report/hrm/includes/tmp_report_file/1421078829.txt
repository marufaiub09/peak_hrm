<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                </font><br />
                <br />
                <font size="+0"> Daily Punch Report On 12-01-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000007</td>
            <td width="80">0004298384</td>
            <td width="130">
				Mst. Roksana  Khatun<br />cell: 01950952987            </td>
            <td width="120">Sr. S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00001</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000052</td>
            <td width="80">0004312645</td>
            <td width="130">
				Mst. Layli   Begum<br />cell: 01730698829            </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00003</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000008</td>
            <td width="80">0004318665</td>
            <td width="130">
				Mst. Sharmin  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00011</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000053</td>
            <td width="80">0004296236</td>
            <td width="130">
				Sheuly  Akhter<br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00027</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000050</td>
            <td width="80">0004298813</td>
            <td width="130">
				Mst. Parvin  Akhter<br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00030</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000018</td>
            <td width="80">0005221929</td>
            <td width="130">
				Mst. Rehena  Akhter<br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00034</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000038</td>
            <td width="80">0004322207</td>
            <td width="130">
				Mst. Samsunnahar  Begum<br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00038</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000176</td>
            <td width="80">0004296896</td>
            <td width="130">
				Md.Rahmatulla  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00054</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000186</td>
            <td width="80">0004322202</td>
            <td width="130">
				Yasmin Akter  <br />cell: 01766146020            </td>
            <td width="120">Sr. S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00058</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000148</td>
            <td width="80">0004322158</td>
            <td width="130">
				Mst.Akhi  <br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00060</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000302</td>
            <td width="80">0011402878</td>
            <td width="130">
				Sreemoti Likha Rani  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00061</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000129</td>
            <td width="80">0004308477</td>
            <td width="130">
				Md. Mosharaf Hossen <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00100</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000143</td>
            <td width="80">0004306043</td>
            <td width="130">
				Mst.Rajia  Sultana<br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00102</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000268</td>
            <td width="80">0007296965</td>
            <td width="130">
				Mst.Moli   Akter<br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00111</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000266</td>
            <td width="80">0004322275</td>
            <td width="130">
				Md.Hannan   Mia<br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00116</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000289</td>
            <td width="80">0003582457</td>
            <td width="130">
				Ibrahim Khalil  <br />cell:             </td>
            <td width="120">Input Man</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00126</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000181</td>
            <td width="80">0000128536</td>
            <td width="130">
				Mst. Nilufar  Yasmin<br />cell: 01935673700            </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00135</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000315</td>
            <td width="80">0005221907</td>
            <td width="130">
				Mst. Laky Akter  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00143</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000329</td>
            <td width="80">0005230763</td>
            <td width="130">
				Mst. Jhuma Akter  <br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00148</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000345</td>
            <td width="80">0005223462</td>
            <td width="130">
				Rumana Begum  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00151</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000355</td>
            <td width="80">0005221905</td>
            <td width="130">
				Mst. Farida Parvin  <br />cell:             </td>
            <td width="120">Sr. S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00153</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000004</td>
            <td width="80">0004310145</td>
            <td width="130">
				Mst. Khorsida  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00404</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000005</td>
            <td width="80">0004322300</td>
            <td width="130">
				Mst. Kazal  Rekha<br />cell: 01951227218            </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00405</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">24</td>
            <td width="50">0000088</td>
            <td width="80">0004296024</td>
            <td width="130">
				Moni Rani  Sorker<br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00412</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">25</td>
            <td width="50">0000091</td>
            <td width="80">0004322294</td>
            <td width="130">
				Mst. Moyna  Khatun<br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00413</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">26</td>
            <td width="50">0000098</td>
            <td width="80">0004296028</td>
            <td width="130">
				Nargis  Akter<br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00421</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">27</td>
            <td width="50">0000179</td>
            <td width="80">0004296899</td>
            <td width="130">
				Mst.Tania Begum  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00436</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000007,0000052,0000008,0000053,0000050,0000018,0000038,0000176,0000186,0000148,0000302,0000129,0000143,0000268,0000266,0000289,0000181,0000315,0000329,0000345,0000355,0000004,0000005,0000088,0000091,0000098,0000179&date=2015-01-12','Absent Information'); return false">27</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 27 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>