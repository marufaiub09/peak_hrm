<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                </font><br />
                <br />
                <font size="+0"> Daily Punch Report On 12-01-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	                <tr><td colspan="17"><b>  Location :  Vogra, Joydevpur, Gazipur  ,  Division :  Knit Garments Production  ,  Department :  Cutting  ,  Section :  Cutting  </b></td></tr>
				        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000259</td>
            <td width="80">0003583305</td>
            <td width="130">
				Md.Ikbal  Hossain<br />cell:             </td>
            <td width="120">Writer Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00901</p></td>
            <td width="60"><b>8:24 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000250</td>
            <td width="80">0008092548</td>
            <td width="130">
				Md.Sumon   Mia<br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00902</p></td>
            <td width="60"><b>8:20 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000248</td>
            <td width="80">0000100792</td>
            <td width="130">
				Md.Alam   Mia<br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00906</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000237</td>
            <td width="80">0008106972</td>
            <td width="130">
				Md. Sobhan  <br />cell:             </td>
            <td width="120">Sticker Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00907</p></td>
            <td width="60"><b>8:25 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000251</td>
            <td width="80">0008159359</td>
            <td width="130">
				Md.Al Amin  <br />cell:             </td>
            <td width="120">Sticker Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00908</p></td>
            <td width="60"><b>8:20 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000256</td>
            <td width="80">0011409822</td>
            <td width="130">
				Md.Abdul  Gaffar<br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00911</p></td>
            <td width="60"><b>8:27 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000247</td>
            <td width="80">0008141546</td>
            <td width="130">
				Sre.Krisno   Das<br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00912</p></td>
            <td width="60"><b>8:20 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000203</td>
            <td width="80">0003575906</td>
            <td width="130">
				Md. Kusbol  Alam<br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00913</p></td>
            <td width="60"><b>8:22 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000255</td>
            <td width="80">0008100881</td>
            <td width="130">
				Md.Abdul  Hadi<br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00914</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000252</td>
            <td width="80">0008147626</td>
            <td width="130">
				Md.Saiful  Islam<br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00915</p></td>
            <td width="60"><b>8:26 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000312</td>
            <td width="80">0005221900</td>
            <td width="130">
				Md. Ruhul Amin  <br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00916</p></td>
            <td width="60"><b>8:13 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000193</td>
            <td width="80">0004318701</td>
            <td width="130">
				Md. Humayun   Kabir<br />cell: 01920243096            </td>
            <td width="120">Sticker Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00917</p></td>
            <td width="60"><b>8:24 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000198</td>
            <td width="80">0004298811</td>
            <td width="130">
				Md. Jafor Ullah  <br />cell: 01864615206            </td>
            <td width="120">Marker Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00918</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000249</td>
            <td width="80">0003573486</td>
            <td width="130">
				Md.Tarikul  Islam<br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00919</p></td>
            <td width="60"><b>8:20 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000191</td>
            <td width="80">0004306073</td>
            <td width="130">
				Anek Hasan Roni  <br />cell: 01777231591            </td>
            <td width="120">Writer Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00920</p></td>
            <td width="60"><b>8:20 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000196</td>
            <td width="80">0004298805</td>
            <td width="130">
				Md. Rafiqul  Islam<br />cell: 01956613520            </td>
            <td width="120">Sticker Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00921</p></td>
            <td width="60"><b>8:25 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000197</td>
            <td width="80">0005223508</td>
            <td width="130">
				Md. Shamim  Hossin<br />cell: 01757032641            </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00922</p></td>
            <td width="60"><b>8:18 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000236</td>
            <td width="80">0011404579</td>
            <td width="130">
				Md. Ajijul Houqe  <br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00929</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000244</td>
            <td width="80">0003579710</td>
            <td width="130">
				Md.Abdur  Razzak<br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00930</p></td>
            <td width="60"><b>8:23 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000245</td>
            <td width="80">0000248184</td>
            <td width="130">
				Md.Roman  Mia<br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00931</p></td>
            <td width="60"><b>8:27 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000243</td>
            <td width="80">0000246334</td>
            <td width="130">
				Md.Uzzal   Hossen<br />cell:             </td>
            <td width="120">Input Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00932</p></td>
            <td width="60"><b>8:23 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000211</td>
            <td width="80">0004314471</td>
            <td width="130">
				Md.Mahafuz  <br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00933</p></td>
            <td width="60"><b>8:23 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000340</td>
            <td width="80">0005216552</td>
            <td width="130">
				Sohel Mia  <br />cell:             </td>
            <td width="120">Scissor Man</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00936</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">24</td>
            <td width="50">0000334</td>
            <td width="80">0005223455</td>
            <td width="130">
				Md. Tarikul Islam  <br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00938</p></td>
            <td width="60"><b>8:27 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">25</td>
            <td width="50">0000333</td>
            <td width="80">0005233985</td>
            <td width="130">
				Md. Shariful Islam  <br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00939</p></td>
            <td width="60"><b>8:13 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">26</td>
            <td width="50">0000253</td>
            <td width="80">0003565306</td>
            <td width="130">
				Md.Rasel   Mia<br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00944</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		            <tr>
                <td colspan="14"><b>Section Total</b></td>
                <td align='right'> 0</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>						
			</tr>
			                <tr><td colspan="17"><b>  Location :  Vogra, Joydevpur, Gazipur  ,  Division :  Knit Garments Production  ,  Department :  Cutting  ,  Section :  Quality  </b></td></tr>
				        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">27</td>
            <td width="50">0000233</td>
            <td width="80">0008154274</td>
            <td width="130">
				Md. Azizul   Houqe<br />cell:             </td>
            <td width="120">Quality Inspector</td>
            <td width="100">Cutting</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00927</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">28</td>
            <td width="50">0000235</td>
            <td width="80">0008108720</td>
            <td width="130">
				Md. Mizanur   Rahman<br />cell:             </td>
            <td width="120">Quality Inspector</td>
            <td width="100">Cutting</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">12-01-2015</td>
            <td width="50"><p>00928</p></td>
            <td width="60"><b>8:20 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		            <tr style="font-weight:bold">
                <td colspan="14">Section Total</td>
				<td width='' align='right'  > 0</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>						
			</tr><tr style="font-weight:bold">
				<td   colspan="14">Department Total</td>
				<td width='' align='right'> 0</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>						
			</tr><tr style="font-weight:bold">
				<td  colspan="14">Division Total</td>
				<td width='' align='right' > 0</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>						
			</tr><tr style="font-weight:bold">
			<td  colspan="14">Location Total</td>
				<td width='' align='right' > 0</td>						
				<td>&nbsp;</td>
				 <td>&nbsp;</td>
			</tr>        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 21 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000248,0000255,0000198,0000236,0000340,0000253,0000233&date=2015-01-12','Absent Information'); return false">7</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-01-12','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 28 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>