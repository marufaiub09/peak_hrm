<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Peak Apparels Ltd.</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 31-05-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000041</td>
            <td width="80">0004322160</td>
            <td width="130">
				Mst. Maleka  <br />cell: 01930640041            </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-5</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00002</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000025</td>
            <td width="80">0004322280</td>
            <td width="130">
				Mst. Helena  Khatun<br />cell: 01929612813            </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00044</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000027</td>
            <td width="80">0004306036</td>
            <td width="130">
				Md. Alamgir  Hossain<br />cell: 01955669562            </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-5</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00046</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000190</td>
            <td width="80">0011395198</td>
            <td width="130">
				Mst. Nazma Akter  <br />cell: 01922174796            </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00051</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000268</td>
            <td width="80">0007296965</td>
            <td width="130">
				Mst.Moli   Akter<br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00111</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000171</td>
            <td width="80">0004322305</td>
            <td width="130">
				Md.Masud   Mia<br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00122</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000287</td>
            <td width="80">0008150670</td>
            <td width="130">
				Mst. Sheuly  Begum<br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00124</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000290</td>
            <td width="80">0003578653</td>
            <td width="130">
				Mst. Rupali  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00128</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000277</td>
            <td width="80">0008085270</td>
            <td width="130">
				Mst.Syida   Sultana<br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00139</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000374</td>
            <td width="80">0005221847</td>
            <td width="130">
				Md. Jaman Mia  <br />cell:             </td>
            <td width="120">Line Iron Man</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-7</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00164</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000452</td>
            <td width="80">0008674183</td>
            <td width="130">
				Mst. Rojina Akter  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-5</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00181</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000516</td>
            <td width="80">0008692705</td>
            <td width="130">
				Md. Raton  <br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00214</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000320</td>
            <td width="80">0005230764</td>
            <td width="130">
				Mst. Jamila Begum  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00430</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000349</td>
            <td width="80">0005223456</td>
            <td width="130">
				Rima Akter  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-5</td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00438</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000343</td>
            <td width="80">0005234881</td>
            <td width="130">
				Mohsina Begum  <br />cell:             </td>
            <td width="120">Quality Inspector</td>
            <td width="100">Quality</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00724</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000333</td>
            <td width="80">0005233985</td>
            <td width="130">
				Md. Shariful Islam  <br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00939</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000525</td>
            <td width="80">0008678088</td>
            <td width="130">
				Md. Alamgir Hossain  <br />cell:             </td>
            <td width="120">Asst.Cutting</td>
            <td width="100">Cutting</td>						
            <td width="100">Cutting</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>00960</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000074</td>
            <td width="80">0004322301</td>
            <td width="130">
				Mirza MD  Alauddin<br />cell: 01731897107            </td>
            <td width="120">Security Incharge</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>01100</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000293</td>
            <td width="80">0008084529</td>
            <td width="130">
				Md.Sagor   Ahmed<br />cell:             </td>
            <td width="120">Iron Man</td>
            <td width="100">Finishing</td>						
            <td width="100">Finishing</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>01516</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000440</td>
            <td width="80">0005221909</td>
            <td width="130">
				Nitul Nokrek  <br />cell:             </td>
            <td width="120">Operator</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>01817</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000221</td>
            <td width="80">0008147608</td>
            <td width="130">
				Md. Anowar   Hossain<br />cell:             </td>
            <td width="120">QC (Knitting)</td>
            <td width="100">Knitting</td>						
            <td width="100">Knitting</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02076</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000325</td>
            <td width="80">0005240970</td>
            <td width="130">
				Md. Sirajul Islam  <br />cell:             </td>
            <td width="120">Fire Safety Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02104</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000326</td>
            <td width="80">0000485614</td>
            <td width="130">
				Md. Helal Uddin  <br />cell:             </td>
            <td width="120">Boiler Operator</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02105</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">24</td>
            <td width="50">0000418</td>
            <td width="80">0005233070</td>
            <td width="130">
				Md. Arif  <br />cell:             </td>
            <td width="120">Mechanic</td>
            <td width="100">Sewing</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02127</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">25</td>
            <td width="50">0000460</td>
            <td width="80">0008691581</td>
            <td width="130">
				Saddam Hossain Titu  <br />cell:             </td>
            <td width="120">Trainee Merchandiser</td>
            <td width="100">Marketing and Merchandising</td>						
            <td width="100">Merchandising</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02138</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">26</td>
            <td width="50">0000468</td>
            <td width="80">0008678712</td>
            <td width="130">
				Md. Lutfor Rahman  <br />cell:             </td>
            <td width="120">Incharge (Knit and Dye.)</td>
            <td width="100">Dyeing</td>						
            <td width="100">Dyeing</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02140</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">27</td>
            <td width="50">0000478</td>
            <td width="80">0008686487</td>
            <td width="130">
				Md. Ruhul Amin  <br />cell:             </td>
            <td width="120">Incharge</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Electrical</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02141</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">28</td>
            <td width="50">0000560</td>
            <td width="80">0008691709</td>
            <td width="130">
				Md. Rafikul Islam  <br />cell:             </td>
            <td width="120">Sr. Merchandiser</td>
            <td width="100">Marketing and Merchandising</td>						
            <td width="100">Merchandising</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02149</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">29</td>
            <td width="50">0000561</td>
            <td width="80">0008691572</td>
            <td width="130">
				Md. Rohul Amin  <br />cell:             </td>
            <td width="120">Sample Man</td>
            <td width="100">Sample</td>						
            <td width="100">Sample</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02150</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">30</td>
            <td width="50">0000563</td>
            <td width="80">0008678090</td>
            <td width="130">
				Md. Robiul Islam  <br />cell:             </td>
            <td width="120">Incharge</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Electrical</td>
            <td width="100"></td>
            <td width="70">31-05-2015</td>
            <td width="50"><p>02151</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000041,0000025,0000027,0000190,0000268,0000171,0000287,0000290,0000277,0000374,0000452,0000516,0000320,0000349,0000343,0000333,0000525,0000074,0000293,0000440,0000221,0000325,0000326,0000418,0000460,0000468,0000478,0000560,0000561,0000563&date=2015-05-31','Absent Information'); return false">30</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-05-31','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-05-31','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-05-31','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-05-31','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 30 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>