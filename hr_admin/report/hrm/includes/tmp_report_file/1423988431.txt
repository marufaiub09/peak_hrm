<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Peak Apparels Ltd.</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 15-02-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000023</td>
            <td width="80">0004298403</td>
            <td width="130">
				Mst. Julekha  <br />cell: 01720834232            </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00007</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000097</td>
            <td width="80">0004317861</td>
            <td width="130">
				Mst. Amena  <br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00050</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000125</td>
            <td width="80">0004322161</td>
            <td width="130">
				Md. Alim  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00087</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000173</td>
            <td width="80">0004322281</td>
            <td width="130">
				Md.Rashidul  Islam<br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00108</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000266</td>
            <td width="80">0004322275</td>
            <td width="130">
				Md.Hannan   Mia<br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00116</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000035</td>
            <td width="80">0004317359</td>
            <td width="130">
				Mst. Taniya Sultana  <br />cell: 01966928453            </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00142</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000328</td>
            <td width="80">0005221901</td>
            <td width="130">
				Mst. Ferdousi  <br />cell:             </td>
            <td width="120">Sr. S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00146</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000346</td>
            <td width="80">0005233986</td>
            <td width="130">
				Jahid Hasan  <br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00152</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000360</td>
            <td width="80">0005240688</td>
            <td width="130">
				Md. Yousuf Ali  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00154</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000361</td>
            <td width="80">0005223461</td>
            <td width="130">
				Mst. Anjuma ara Begum  <br />cell:             </td>
            <td width="120">Gen. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00155</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000009</td>
            <td width="80">0004322254</td>
            <td width="130">
				Mst. Rohima  Khatun<br />cell: 01966854794            </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00406</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000113</td>
            <td width="80">0004298402</td>
            <td width="130">
				Mst. Rahima  <br />cell:             </td>
            <td width="120">A.S.M. Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00418</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000099</td>
            <td width="80">0004322159</td>
            <td width="130">
				Md. Akter  Hossain<br />cell:             </td>
            <td width="120">Quality Inspector</td>
            <td width="100">Sewing</td>						
            <td width="100">Quality</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>00707</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000396</td>
            <td width="80">0005230765</td>
            <td width="130">
				Md. Badsha Alamgir  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>01106</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000397</td>
            <td width="80">0005221886</td>
            <td width="130">
				Md. Mizanur Rahman  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>01107</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000430</td>
            <td width="80">0005230760</td>
            <td width="130">
				Delwar Hossain  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>01108</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000046</td>
            <td width="80">0004322315</td>
            <td width="130">
				Tagor Ali Pramanik  <br />cell: 01757902501            </td>
            <td width="120">Sweeper</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Cleaning</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>01203</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000149</td>
            <td width="80">0004306067</td>
            <td width="130">
				Mst.Anjuara  <br />cell:             </td>
            <td width="120">Folder. (Finishing)</td>
            <td width="100">Finishing</td>						
            <td width="100">Finishing</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>01509</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000227</td>
            <td width="80">0004298366</td>
            <td width="130">
				Mst. Laki  Khatun<br />cell:             </td>
            <td width="120">Finishing Asst.</td>
            <td width="100">Finishing</td>						
            <td width="100">Finishing</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>01514</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000294</td>
            <td width="80">0000779247</td>
            <td width="130">
				Md.Iasin  Ali<br />cell:             </td>
            <td width="120">Iron Man</td>
            <td width="100">Finishing</td>						
            <td width="100">Finishing</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>01517</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000077</td>
            <td width="80">0004306038</td>
            <td width="130">
				Dr. Mohammad Mazharul Islam<br />cell:             </td>
            <td width="120">Medical Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Medical</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>02033</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000084</td>
            <td width="80">0004296025</td>
            <td width="130">
				Md. Khaled  Hasan<br />cell:             </td>
            <td width="120">Store Assistant</td>
            <td width="100">Store</td>						
            <td width="100">Store</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>02038</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000159</td>
            <td width="80">0004308495</td>
            <td width="130">
				Md.Rubel  Mia<br />cell:             </td>
            <td width="120">Mechanic</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>02055</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">24</td>
            <td width="50">0000222</td>
            <td width="80">0004318672</td>
            <td width="130">
				Md. Manik  Mollah<br />cell:             </td>
            <td width="120">Iron Man (Sample)</td>
            <td width="100">Sample</td>						
            <td width="100">Sample</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>02077</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">25</td>
            <td width="50">0000326</td>
            <td width="80">0000485614</td>
            <td width="130">
				Md. Helal Uddin  <br />cell:             </td>
            <td width="120">Boiler Operator</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Maintenance-Mechanical</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>02105</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">26</td>
            <td width="50">0000356</td>
            <td width="80">0005233057</td>
            <td width="130">
				Md. Abdur Rahim  <br />cell:             </td>
            <td width="120">Supervisor</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">15-02-2015</td>
            <td width="50"><p>02120</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000023,0000097,0000125,0000173,0000266,0000035,0000328,0000346,0000360,0000361,0000009,0000113,0000099,0000396,0000397,0000430,0000046,0000149,0000227,0000294,0000077,0000084,0000159,0000222,0000326,0000356&date=2015-02-15','Absent Information'); return false">26</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-15','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-15','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-15','Late Information'); return false">0 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-15','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 26 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>