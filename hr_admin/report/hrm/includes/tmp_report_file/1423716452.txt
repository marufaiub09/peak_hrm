<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                HEAD OFFICE</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 12-02-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000382</td>
            <td width="80">0009999950</td>
            <td width="130">
				Md. Mahabbul Alam  <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02012</p></td>
            <td width="60"><b>8:56 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">26</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000383</td>
            <td width="80">0009999951</td>
            <td width="130">
				Md. Abdur Rahman  <br />cell:             </td>
            <td width="120">Officer (Admin)</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02014</p></td>
            <td width="60"><b>9:22 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">52</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000409</td>
            <td width="80">0009999957</td>
            <td width="130">
				Md. Humayun Biswas  <br />cell:             </td>
            <td width="120">Manager</td>
            <td width="100">Commercial</td>						
            <td width="100">Commercial</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02017</p></td>
            <td width="60"><b>9:01 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">31</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000410</td>
            <td width="80">0009999962</td>
            <td width="130">
				Md. Imam Hossain  <br />cell:             </td>
            <td width="120">Chief Accountant</td>
            <td width="100">Finance and Accounts</td>						
            <td width="100">Finance and Accounts</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02019</p></td>
            <td width="60"><b>8:44 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000411</td>
            <td width="80">0009999964</td>
            <td width="130">
				Abul Basar Md. Saifullah   <br />cell:             </td>
            <td width="120">Accounts Officer</td>
            <td width="100">Finance and Accounts</td>						
            <td width="100">Finance and Accounts</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02020</p></td>
            <td width="60"><b>9:08 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">38</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000385</td>
            <td width="80">0009999952</td>
            <td width="130">
				Md. Shafiqul Islam  <br />cell:             </td>
            <td width="120">Peon </td>
            <td width="100">Admin and HR</td>						
            <td width="100">Admin and HR</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02028</p></td>
            <td width="60"><b>8:34 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">P</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000398</td>
            <td width="80">0009999953</td>
            <td width="130">
				Md. Bashir Miah  <br />cell:             </td>
            <td width="120">Driver</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Driving</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02031</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000387</td>
            <td width="80">0009999961</td>
            <td width="130">
				M.M. Waliur Rahman  <br />cell:             </td>
            <td width="120">C and F Officer</td>
            <td width="100">Commercial</td>						
            <td width="100">C and F</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02040</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">9</td>
            <td width="50">0000388</td>
            <td width="80">0009999958</td>
            <td width="130">
				Md. Mahbub Zaman  <br />cell:             </td>
            <td width="120">Officer</td>
            <td width="100">Commercial</td>						
            <td width="100">Commercial</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02047</p></td>
            <td width="60"><b>8:48 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">18</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">10</td>
            <td width="50">0000420</td>
            <td width="80">0009999965</td>
            <td width="130">
				Mr. Partho  <br />cell:             </td>
            <td width="120"> Engineer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Construction</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02106</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">11</td>
            <td width="50">0000421</td>
            <td width="80">0009999966</td>
            <td width="130">
				Md. Mamun  <br />cell:             </td>
            <td width="120">Side Engineer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Construction</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02107</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">12</td>
            <td width="50">0000390</td>
            <td width="80">0009999959</td>
            <td width="130">
				Md. Mostak Ahmed  <br />cell:             </td>
            <td width="120">Officer</td>
            <td width="100">Commercial</td>						
            <td width="100">Commercial</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02108</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">13</td>
            <td width="50">0000391</td>
            <td width="80">0009999960</td>
            <td width="130">
				Md. Kamrul Islam  <br />cell:             </td>
            <td width="120">Officer</td>
            <td width="100">Commercial</td>						
            <td width="100">Commercial</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02109</p></td>
            <td width="60"><b>8:48 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">18</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">14</td>
            <td width="50">0000422</td>
            <td width="80">0009999967</td>
            <td width="130">
				Showrab Hossain  <br />cell:             </td>
            <td width="120">Side Engineer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Construction</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02110</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">15</td>
            <td width="50">0000359</td>
            <td width="80">0009999963</td>
            <td width="130">
				Sayed Mahamudul Hassan  <br />cell:             </td>
            <td width="120">Accountant</td>
            <td width="100">Finance and Accounts</td>						
            <td width="100">Finance and Accounts</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02111</p></td>
            <td width="60"><b>9:07 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">37</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">16</td>
            <td width="50">0000423</td>
            <td width="80">0009999968</td>
            <td width="130">
				Md. Didarul Alam  <br />cell:             </td>
            <td width="120">Side Engineer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Construction</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02112</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">17</td>
            <td width="50">0000412</td>
            <td width="80">0009999969</td>
            <td width="130">
				Md. Shafiul Bashir  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02113</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">18</td>
            <td width="50">0000413</td>
            <td width="80">0009999970</td>
            <td width="130">
				Md. Muyazzem Hossain  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02114</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">19</td>
            <td width="50">0000424</td>
            <td width="80">0009999971</td>
            <td width="130">
				Md. Ibrahim Khalil  <br />cell:             </td>
            <td width="120">Quring Man</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Construction</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02115</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">20</td>
            <td width="50">0000414</td>
            <td width="80">0009999972</td>
            <td width="130">
				Faruk Shekh  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02116</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">21</td>
            <td width="50">0000415</td>
            <td width="80">0009999973</td>
            <td width="130">
				Siddiqur Raham  <br />cell:             </td>
            <td width="120">Security Guard</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Security</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02117</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">22</td>
            <td width="50">0000407</td>
            <td width="80">0009999955</td>
            <td width="130">
				Md. Azizul Islam  <br />cell:             </td>
            <td width="120">Driver</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Driving</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02118</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">23</td>
            <td width="50">0000408</td>
            <td width="80">0009999956</td>
            <td width="130">
				Md. Reaz Hossain  <br />cell:             </td>
            <td width="120">Driver</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Driving</td>
            <td width="100"></td>
            <td width="70">12-02-2015</td>
            <td width="50"><p>02119</p></td>
            <td width="60"><b>00:00</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">A</td>
            <td width="40" align="center">0</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 2 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000398,0000387,0000420,0000421,0000390,0000422,0000423,0000412,0000413,0000424,0000414,0000415,0000407,0000408&date=2015-02-12','Absent Information'); return false">14</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-12','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-12','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000382,0000383,0000409,0000411,0000388,0000391,0000359&date=2015-02-12','Late Information'); return false">7 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-12','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 23 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>