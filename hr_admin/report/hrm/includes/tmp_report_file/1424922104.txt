<div>	<style type="text/css" media="print">
    p{ page-break-after: always;}
    </style>	
    <!--<div style=" border:1px solid #7F7FFF; width:1300px;">-->
	<table width="1280" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
        <tr>
            <td colspan="17" align="center" style="border:hidden"><font size="+1">
                Peak Apparels Ltd.</font><br />
                <br />
                <font size="+0"> Daily Punch Report On 26-02-2015</font><br />
            </td>
        </tr>
    </table>
       	<!--<div style="width:1700px;margin-top:15px;" class="page" align="center">-->
    <div style="width:1300px;">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" align="left">
        <thead>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="50"><b>Emp Code</b></th>
                <th width="80"><b>Punch Card</b></th>
                <th width="130"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100"><b>Department</b></th>
                <th width="100"><b>Section</b></th>
                <th width="100"><b>Sub Section</b></th>
                <th width="70"><b>Attnd. Date</b></th>
                <th width="50"><b>ID Card</b></th>
                <th width="60"><b>In Time</b></th>
                <th width="60"><b>Out Time</b></th>
                <th width="50"><b>Status</b></th>
                <th width="40"><b>Late Min</b></th>
                <th width="40"><b>OT</b></th>
                <th width="80"><b>Shift Name</b></th>
                <th><b>Remarks</b></th>
            </tr>
    	</thead>
    </table>
    </div>
        <div style="width:1300px; overflow-y:scroll;" id="scroll_body">
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1280" rules="all" id="table_body" align="left">
	        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">1</td>
            <td width="50">0000054</td>
            <td width="80">0004322278</td>
            <td width="130">
				Mst. Yesmin  Akter<br />cell: 01946598317            </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-3</td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>00006</p></td>
            <td width="60"><b>8:34 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">4</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">2</td>
            <td width="50">0000049</td>
            <td width="80">0004306037</td>
            <td width="130">
				Mst. Momena  khatun<br />cell: 01953163276            </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>00022</p></td>
            <td width="60"><b>8:34 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">4</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">3</td>
            <td width="50">0000034</td>
            <td width="80">0004308472</td>
            <td width="130">
				Mst. Beauty  <br />cell:             </td>
            <td width="120">S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>00031</p></td>
            <td width="60"><b>8:34 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">4</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">4</td>
            <td width="50">0000018</td>
            <td width="80">0005221929</td>
            <td width="130">
				Mst. Rehena  Akhter<br />cell:             </td>
            <td width="120">Jr. S.M Operator</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100">Line-1</td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>00034</p></td>
            <td width="60"><b>8:34 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">4</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">5</td>
            <td width="50">0000040</td>
            <td width="80">0004322163</td>
            <td width="130">
				Mst. Sharmin  Akhter<br />cell: 01767719534            </td>
            <td width="120">Needle Man</td>
            <td width="100">Sewing</td>						
            <td width="100">Sewing</td>
            <td width="100"></td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>00037</p></td>
            <td width="60"><b>8:34 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">4</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">6</td>
            <td width="50">0000166</td>
            <td width="80">0004300677</td>
            <td width="130">
				Sapon Chandra Day  <br />cell:             </td>
            <td width="120">Loader</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Loader</td>
            <td width="100"></td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>01206</p></td>
            <td width="60"><b>9:06 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">36</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#FFFFFF">
            <td width="30" align="center">7</td>
            <td width="50">0000174</td>
            <td width="80">0004298389</td>
            <td width="130">
				Md.Abu  Badsha<br />cell:             </td>
            <td width="120">Loader</td>
            <td width="100">Admin and HR</td>						
            <td width="100">Loader</td>
            <td width="100"></td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>01207</p></td>
            <td width="60"><b>8:34 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">4</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Worker</td>
            <td></td>	
        </tr>
		        <tr bgcolor="#EEEEEE">
            <td width="30" align="center">8</td>
            <td width="50">0000001</td>
            <td width="80">0004322204</td>
            <td width="130">
				MD. APPLE  MAHMUD<br />cell: 01760802052            </td>
            <td width="120">IT Officer</td>
            <td width="100">Admin and HR</td>						
            <td width="100">IT(Information Technology)</td>
            <td width="100"></td>
            <td width="70">26-02-2015</td>
            <td width="50"><p>02003</p></td>
            <td width="60"><b>8:56 am</b></td>
            <td width="60"><b>00:00</b></td>
            <td width="50" align="center">D</td>
            <td width="40" align="center">26</td>
            <td width="40" align="center">0</td>
            <td width="80">General Shift Staff</td>
            <td></td>	
        </tr>
		        <tr  bgcolor="#CCCCCC" style="font-weight:bold">
            <td  colspan="14">&nbsp;Grand Total</td>
            <td width='' align='right'> 0.00</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>							
            </tr> 
            </table> <tfoot>
        <tr>
            <td colspan="17" align="center"><table width="1260px">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 </tr></table></td>
        </tr>
</tfoot>
<!--</div>-->
</div>

</div>
<p></p>
    
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> 0 </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-26','Absent Information'); return false">0</a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-26','Leave Information'); return false">0</a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-26','Movement Information'); return false">0 </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=0000054,0000049,0000034,0000018,0000040,0000166,0000174,0000001&date=2015-02-26','Late Information'); return false">8 </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=&date=2015-02-26','Weekend Information'); return false">0 </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> 8 </td>					
		  </tr>
	</table> 
    
               	   	
        

</div>