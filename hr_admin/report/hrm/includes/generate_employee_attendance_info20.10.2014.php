<?php
/***************************
Complted by:md ekram hossain
Date:13/03/2014
*******************************/
	date_default_timezone_set('Asia/Dhaka');
	session_start();
	
	include('../../../../includes/common.php');
	include('../../../../includes/array_function.php');
	include('../../../../includes/common_functions.php');
	
	extract( $_REQUEST );

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}

	$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	
if( $action == "attendace_information" ) 
{ //Punch Report
	if ($category_id=='') $category_id =""; else $category_id ="and category='$category_id'";
	if ($company_id==0){ $company_id="";}else{$company_id=" company_id='$company_id'";}
	if ($location_id==0 || $location_id=='') $location_id=""; else $location_id="and location_id='$location_id'"; 
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and  department_id in ($department_id)";   
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and section_id in ($section_id)"; 
	if ($subsection_id==0 || $subsection_id=='') $subsection_id="";else $subsection_id="and subsection_id in ($subsection_id)";
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and designation_id in ($designation_id)";
	if ($salary_based==0 || $salary_based=='') $salary_based=""; else $salary_based="and salary_type_entitled='$salary_based'";
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
	
	//new 
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(id_card_no), 5)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
	$location_info_sql = "select * from lib_location order by id";
	$result = mysql_query( $location_info_sql ) or die( $location_info_sql . "<br />" . mysql_error() );
	$location_info_result = mysql_fetch_array($result);	
	
	$company_info_sql = "select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);	
	
		$st='';
		 if($cbo_status==0 || $cbo_status==""){
			if($st=="") $st="'P','A','D','CL','ML','SL','EL','SpL','LWP','MR','W','CH','FH','GH'"; else $st.=",'P','A','D','CL','ML','SL','EL','SpL','LWP','MR','W','CH','FH','GH'";
			}
		else if($cbo_status==1){
			if($st=="") $st="'P'"; else $st.=",'P'";
			$status="Present";
			}
		else if($cbo_status==2){
			if($st=="") $st="'A'"; else $st.=",'A'";
			$status="Absent";
			}
		else if($cbo_status==3){
			if($st=="") $st="'D'"; else $st.=",'D'";
			$status="Late";
			}
		else if($cbo_status==4){
			if($st=="") $st="'CL','ML','SL','EL','SpL','LWP'"; else $st.=",'CL','ML','SL','EL','SpL','LWP'";
			$status="Leave";
			}
		else if($cbo_status==5){
			if($st=="") $st="'MR'"; else $st.=",'MR'";
			$status="Movement";
			}
		else if($cbo_status==6){
			if($st=="") $st="'FH','GH','CH'"; else $st.=",'FH','GH','CH'";
			$status="Holiday";
			}
		else if($cbo_status==7){
			if($st=="") $st="'W'"; else $st.=",'W'";
			$status="Weekend";
			}

 	//echo $st;die;
	
	ob_start();
 	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$sl=0;
	$ct=0;
	$r=1;
	?>			
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="700px" rules="all" >
        <thead> 	
            <tr>
                <th colspan="7" align="center" bordercolor="#FFFFFF"><br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br />
					<? echo $location_info_result["location_name"]; ?><br />
                    Employee attendance Information<br /> 
                    Date between :<? echo convert_to_mysql_date($txt_from_date); ?>  To  <?  echo convert_to_mysql_date($txt_to_date); ?><br /><br />
                </th>
            </tr>
            <tr>
                <th width="20"><b>SL</b></th>
                <th width="50"><b>Emp ID</b></th>
                <th width="130"><b>Name</b></th>
                <th width="130"><b>Joining Date</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="50"><b><? echo $status; ?>&nbsp;Count</b></th>
                <th><b>Date</b></th>
            </tr>
        </thead>	
		<?	
		$sql="SELECT emp_code,count(status) as status, group_concat(' ',DATE_FORMAT(attnd_date, '%d') ORDER BY attnd_date ASC) as attnd_date  FROM  hrm_attendance 
				WHERE  attnd_date between '$txt_from_date' and '$txt_to_date' and status in($st)
				group by emp_code order by attnd_date"; 
		// echo $sql; 
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$status_arr = array();
		while( $row = mysql_fetch_array( $result ) ) 
		{
			$status_arr[$row['emp_code']]['status'] = mysql_real_escape_string($row['status']);
			$status_arr[$row['emp_code']]['date']= mysql_real_escape_string($row['attnd_date']);
		}
		// print_r($status_arr);die;
		$sql_mst = "SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name,id_card_no,punch_card_no,emp_code,company_id,location_id,division_id,
				department_id,section_id,subsection_id,joining_date,designation_id,salary_type_entitled,category  		     
				FROM 
				hrm_employee 
				WHERE 
					$company_id
					$category_id
					$location_id  
					$division_id 
					$department_id 
					$section_id 
					$subsection_id 
					$designation_id
					$salary_based
					$id_card_no
					$emp_code 
			 	$orderby";
		//echo $sql_mst;die;///  group by $dynamic_groupby
		$result = mysql_query( $sql_mst ) or die( $sql_mst . "<br />" . mysql_error() );
		while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
		{ 
			//echo $status_arr[$row_emp[emp_code]]['date'];
			if($status_arr[$row_emp[emp_code]]['status']>0)
			{
				$sl++;
				//start header print---------------------------------//
				if($sl==1)
				{
					$location_arr[$row_emp[location_id]]=$row_emp[location_id];
					$division_arr[$row_emp[division_id]]=$row_emp[division_id];
					$department_arr[$row_emp[department_id]]=$row_emp[department_id];
					$section_arr[$row_emp[section_id]]=$row_emp[section_id];
					$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
				}//end if condition of header print
				//end header print
			
				if($sl!=1)
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
					if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row_emp[section_id]]=$row_emp[section_id];
											$subsection_arr=array();
											$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row_emp[department_id]]=$row_emp[department_id];
										$section_arr=array();
										$subsection_arr=array();
										$section_arr[$row_emp[section_id]]=$row_emp[section_id];
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row_emp[division_id]]=$row_emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row_emp[department_id]]=$row_emp[department_id];
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row_emp[location_id]]=$row_emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row_emp[division_id]]=$row_emp[division_id];
						$department_arr[$row_emp[department_id]]=$row_emp[department_id];
						$section_arr[$row_emp[section_id]]=$row_emp[section_id];
						$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
				}
				if($new_subsec==1 && $status_subsec==1 && $sl!=1)
				{
				?>
                	<tr style="font-weight:bold;">
						<td width='' colspan="5"><b> Subsection Total</b></td>
						<td align="center"> <? echo $subsection_total_status; ?></td>
						 <td>&nbsp;</td>		
					</tr>
					<?
					$subsection_total_status=0;	
				}
			
				if($new_sec==1 && $status_sec==1 && $sl!=1)
				{
				?>
                	<tr style="font-weight:bold;">
						<td width='' colspan="5"><b>Section Total</b></td>
						<td align="center"> <? echo $section_total_status; ?></td>
						<td>&nbsp;</td>
					</tr>
					<?
					$section_total_status=0;
				}
			
				if($new_dept==1 && $status_dept==1 && $sl!=1)
				{
				?>
                	<tr style="font-weight:bold;">
							<td  colspan="5">Department Total</td>
									<td align="center" > <? echo $department_total_status; ?></td>
									<td>&nbsp;</td>
                    </tr>
                    <?
                    $department_total_status=0;	
				}
			 
				if($new_divis==1 && $status_divis==1 && $sl!=1)
				{
				?>
                	<tr style="font-weight:bold;">
                        <td  colspan="5" >Division Total</td>
                        <td align="center"> <? echo $division_total_status; ?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <?
					$division_total_status=0;			
				}
			 
				if($new_loc==1 && $status_loc==1 && $sl!=1)
				{
				?>
                    <tr style="font-weight:bold;">
                        <td colspan="5">Location Total</td>
                        <td align="center"> <? echo $location_total_status; ?></td>
                        <td>&nbsp;</td>
                    </tr>
					<?
					$location_total_status=0;				
				}
			
				//header print here 
				$c_part_1="";
				if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
				{
					/*if($status_loc==1)
					{					
						$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
					}	*/			
					if($status_divis==1)
					{
						$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
					}
					if($status_dept==1)
					{
						$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
					}
					if($status_sec==1)
					{
						$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
					}
					if($status_subsec==1)
					{
						$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
					}
			
					if($c_part_1!='')
					{
						$i=0;$sl=1;
						?><tr><td colspan="7"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
					}		
				}
			
				if ($r%2==0) $bgcolor="#EEEEEE";
				else $bgcolor="#FFFFFF";
				?>
                <tbody> 
					<tr bgcolor="<? echo $bgcolor;?>">
						<td height="20"><? echo $r;?> </td>
						<td width="50"><? echo $row_emp[emp_code]; ?></td>
						<td width="130"><? echo $row_emp[name];?></td>
						<td width="130"><? echo convert_to_mysql_date($row_emp[joining_date]);?></td>
						<td width="120"><? echo $designation_chart[$row_emp['designation_id']];?></td>
						<td width="50" align="center"><? echo $status_arr[$row_emp[emp_code]]['status'];?></td>						
						<td width="80"><? echo $status_arr[$row_emp[emp_code]]['date'];?></td>
					</tr>
					<?
					$ct++;
					$subsection_total_status += $status_arr[$row_emp[emp_code]]['status'];
					$section_total_status += $status_arr[$row_emp[emp_code]]['status'];
					$department_total_status += $status_arr[$row_emp[emp_code]]['status'];
					$division_total_status += $status_arr[$row_emp[emp_code]]['status'];
					$location_total_status += $status_arr[$row_emp[emp_code]]['status'];
					$grand_total_status  +=  $status_arr[$row_emp[emp_code]]['status'];
					$r++;
				}
			}//while loop end here 
			if($status_subsec==1)
			{
			?>
                <tr style="font-weight:bold;">
                    <td width='' colspan="5"><b> Subsection Total</b></td>
                    <td align="center"> <? echo $subsection_total_status; ?></td>
                    <td>&nbsp;</td>		
                </tr>
				<?
				$subsection_total_status=0;	
			}
		
			if($status_sec==1)
			{
			?>
                <tr style="font-weight:bold;">
                    <td width='' colspan="5"><b>Section Total</b></td>
                    <td align="center"> <? echo $section_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
                $section_total_status=0;
			}
		
			if($status_dept==1 )
			{
			?>
                <tr style="font-weight:bold;">
                    <td  colspan="5">Department Total</td>
                    <td align="center"> <? echo $department_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
                $department_total_status=0;	
			}
		 
			if($status_divis==1 )
			{
			?>
                <tr style="font-weight:bold;">
                    <td  colspan="5" >Division Total</td>
                    <td align="center"> <? echo $division_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
				$division_total_status=0;			
			}
		 
			if($status_loc==1 )
			{
			?>
                <tr style="font-weight:bold;">
                    <td colspan="5">Location Total</td>
                    <td align="center"> <? echo $location_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
				$location_total_status=0;		
			}
			?>
            <tr  bgcolor="#CCCCCC" style="font-weight:bold;">
                <td  colspan="5">&nbsp;Grand Total</td>
                <td align="center"> <? echo $grand_total_status;//$grand_total_ot; ?></td>
                <td>&nbsp;</td>
            </tr> 
        </tbody>
	</table> 
    <?
	//previous file delete code-----------------------------//

	$html = ob_get_contents();
	ob_clean();		
	
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}
?>