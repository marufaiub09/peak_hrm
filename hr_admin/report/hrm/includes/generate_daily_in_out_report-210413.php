<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}


$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}

if( $action == "daily_in_out_report" ) { //Punch Report

 	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$adjust_in_time=$res['adjust_in_time']; 	 
			$currency_id=$res['currency_id'];
			$adjust_out_time=$res['adjust_out_time']; 
		}

	
	list( $day, $month, $year ) = explode( "-", $txt_date );
	$date = $year . "-" . $month . "-" . $day;
	
	if($cbo_status==0) $status="";
	else if ($cbo_status==1) $status="and a.status='P '";
	else if ($cbo_status==2) $status="and a.status='A '";
	else if ($cbo_status==3) $status="and a.status='D '";
	else if ($cbo_status==4) $status="and a.status in ('CL','ML','SL','EL','SpL','LWP')";
	else if ($cbo_status==5) $status="and a.status='MR'";
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	//if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	/*if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";*/
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else	$department_id=" and  a.department_id in ($department_id)";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($shift_id==0) $shift_id=""; else $shift_id=" and a.policy_shift_id='$shift_id'";	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(emp.id_card_no,5)";}	 
			
	
	$p=0;$a=0;$mr=0;$d=0;$l=0;$w=0;$section_total_ot=0;$department_total_ot=0;$division_total_ot=0;$grand_total_ot=0;$total_grand_total=0;
	ob_start();
	
	?>
 
    <div align="center" style="width:1300px; font-size:16px"><b><? echo $company_details[$cbo_company_id]; ?></b></div>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1200" rules="all" >
	   <thead> 	
        <tr>
            <th width="20"><b>SL</b></th>
            <th width="50"><b>Emp Code</b></th>
            <th width="50"><b>ID Card</b></th>
            <th width="50"><b>Punch Card</b></th>
            <th width="140"><b>Name</b></th>
            <th width="100"><b>Designation</b></th>
            <th width="90"><b>Department</b></th>
            <th width="80"><b>Location</b></th>
            <th width="80"><b>Section</b></th>
            <th width="50"><b>Sub Section</b></th>
            <th width="70"><b>Attnd. Date</b></th>
            <th width="60"><b>In Time</b></th>
            <th width="60"><b>Out Time</b></th>
            <th width="30"><b>Status</b></th>
            <th width="40"><b>Late Min</b></th>
            <th width="20"><b>OT</b></th>
            <th width="60"><b>Shift Name</b></th>
            <th width="120"><b>Remarks</b></th>
        </tr>
    </thead> 
    <tbody>	
	
	<?	
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.*,a.sign_in_time,a.sign_out_time,a.r_sign_in_time,a.r_sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min 
				FROM 
					hrm_attendance a, hrm_employee emp 
				WHERE 
					a.attnd_date like '$date' and 
					a.emp_code=emp.emp_code 
					$department_id 
					$location_id 
					$division_id 
					$section_id 
					$company_id  
					$status 
					$subsection_id 
					$designation_id
					$category 
					$shift_id
 					$orderby";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );						
		$emp_tot=mysql_num_rows($result);
		
		if ($emp_tot>0)
		{	
			$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
			while( $row = mysql_fetch_array( $result ) ) 
			{
			 	 
  				$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;$not_sub_tot=0;$sec_sub_tot=0;$not_div_tot=0;
				//if(in_array($row[company_id],$company_arr))
				//{
					if(in_array($row[location_id],$location_arr))
					{
							if(in_array($row[division_id],$division_arr))
							{
									if(in_array($row[department_id],$department_arr))
									{
										if(in_array($row[section_id],$section_arr))
										{
											if(in_array($row[subsection_id],$subsection_arr))
											{}
											else
											{
												$subsection_arr[$row[subsection_id]]=$row[subsection_id];
												$new_subsec=1;
											}
										}
										else
										{
											$section_arr[$row[section_id]]=$row[section_id];
											$subsection_arr[$row[subsection_id]]=$row[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else
									{
										$department_arr[$row[department_id]]=$row[department_id];
										$section_arr[$row[section_id]]=$row[section_id];
										$subsection_arr[$row[subsection_id]]=$row[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else
							{
									$division_arr[$row[division_id]]=$row[division_id];
									$department_arr[$row[department_id]]=$row[department_id];
									$section_arr[$row[section_id]]=$row[section_id];
									$subsection_arr[$row[subsection_id]]=$row[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else
					{
						$location_arr[$row[location_id]]=$row[location_id];
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
			
			/*	
				}//company
			else
			{
				$company_arr[$row[company_id]]=$row[company_id];
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
				$new_com=1;
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//company else
			*/	 
				 
 				//header print here 
				if($new_com==1 && $status_com==1)
				{					
					?><tr><th colspan="18" align="left" bgcolor="#CCCCCC">Company : <? echo $company_details[$row[company_id]]; ?></th></tr><?
				}
				if($new_loc==1 && $status_loc==1)
				{	
					$not_div_tot=1;
					if($i!=0){?><tr><td  colspan="15" align="right">Grand Total</td><td><? echo $grand_total_ot;?></td></tr><? $grand_total_ot=0;}				
					?><tr><th colspan="18" align="left" bgcolor="#CCCCCC">Location : <? echo $location_details[$row[location_id]]; ?></th></tr><?
				}				
				if($new_divis==1 && $status_divis==1)
				{
					$not_sub_tot=1;
					if($i!=0 && $not_div_tot!=1){?><tr><td  colspan="15" align="right">Division Total</td><td><? echo $division_total_ot;?></td></tr><? $division_total_ot=0;}//show division total
					?><tr><th colspan="18" align="left" bgcolor="#CCCCCC">Division : <? echo $division_details[$row[division_id]]; ?></th></tr><?
				}
 				if($new_dept==1 && $status_dept==1)
				{
					//$sec_sub_tot=1;
					if($i!=0 && $not_sub_tot!=1){?><tr><td  colspan="15" bgcolor="#F0FFFF" align="right">Department Total</td><td bgcolor="#F0FFFF"><? echo $department_total_ot;?></td></tr><? $department_total_ot=0;}//show dept total
					?><tr><th colspan="18" align="left" bgcolor="#CCCCCC">Department : <? echo $department_details[$row[department_id]]; ?></th></tr><?
					
				}
				if($new_sec==1 && $status_sec==1)
				{
					/*if($i!=0 && $sec_sub_tot!=1){?><tr><td  colspan="16" bgcolor="#F0FFFF">Section Subtotal</td><td bgcolor="#F0FFFF"><? echo $section_total_ot;?></td></tr><? $section_total_ot=0;}*///show sec total
					?><tr><th colspan="18" align="left" bgcolor="#CCCCCC">Section : <? echo $section_details[$row[section_id]]; ?></th></tr><?
				}
				if($new_subsec==1 && $status_subsec==1)
				{
					?><tr><th colspan="18" align="left" bgcolor="#CCCCCC">Sub Section : <? echo $subsection_details[$row[subsection_id]]; ?></th></tr><?
				}
					

 				$hours[0] = abs((int)($row["total_over_time_min"]/60)); //hours
				$hours[1] = abs((int)($row["total_over_time_min"]%60)); //minutes
				
				$ot_mins_cal=0;
				if($ot_fraction==1) //ot fraction allow
				{						
					if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
					else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
					else $ot_mins_cal = $hours[0]*60;
				}
				if($ot_fraction==2) // no ot fraction
				{						
					//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
					if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
					else $ot_mins_cal = $hours[0]*60;						
				}
				if($ot_fraction==3) //at actual minutes
				{						
					$ot_mins_cal = 	$row["total_over_time_min"];				
				}
				
				$total_hours = sprintf("%02d:%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
				
				if($row['r_sign_in_time']!='00:00:00'){$r_in = DATE("g:i a", STRTOTIME($row[r_sign_in_time]));}else $r_in="00:00";
				if($row['r_sign_out_time']!='00:00:00'){$r_out = DATE("g:i a", STRTOTIME($row[r_sign_out_time]));}else $r_out="00:00";
				if($row['sign_in_time']!='00:00:00'){$s_in = DATE("g:i a", STRTOTIME($row[sign_in_time]));}else $s_in="00:00";
				if($row['sign_out_time']!='00:00:00'){$s_out = DATE("g:i a", STRTOTIME($row[sign_out_time]));}else $s_out="00:00";
													
 				if($row['sign_in_time']=='00:00:00') $in_time = '00:00'; else $in_time = DATE("g:i a", STRTOTIME($row[sign_in_time]));
				if($row['sign_out_time']=='00:00:00') $out_time = '00:00'; else $out_time = DATE("g:i a", STRTOTIME($row[sign_out_time]));
				
				$i++;
 				if ($i%2==0) $bgcolor="#EEEEEE"; 
				else $bgcolor="#FFFFFF";
			?>			
				<tr bgcolor="<? echo $bgcolor;?>" >
					<td height="20"><? echo $i;?> </td>
					<td width="50"><? echo $row[emp_code]; ?></td>
                    <td width="50"><? echo $row[id_card_no]; ?></td>
					<td width=""><? echo $row[punch_card_no];?></td>
					<td width="140"><? echo $row[name];?></td>
					<td width="100"><? echo $designation_chart[$row['designation_id']];?></td>
					<td width="90"><? echo $department_details[$row['department_id']];?></td>						
					<td width="80"><? echo $location_details[$row['location_id']];?></td>
					<td width="80"><? echo $section_details[$row['section_id']];?></td>
					<td width="50"><? echo $subsection_details[$row['subsection_id']];?></td>
					<td align='center' width="70"><? echo convert_to_mysql_date($row[attnd_date]);?></td>
					<td  width="60"><b><? echo $in_time; ?></b></td>
					<td  width="60"><b><? if ($row[sign_out_time]!="00:00:00") echo DATE("g:i a", STRTOTIME($row[sign_out_time])); else echo "00:00";   ?></b></td>
                   
					<td width="30"><? echo  ($row[status]=="CH" ? "H" : $row[status]); //."=".$row['sign_out_time'];
					
					 //get_buyer_out_time($row[sign_out_time], $row[r_sign_out_time], $var_hrm_chart[$row['company_id']]['adjust_out_time'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] )
					?></td>
					<td width="40"><? echo $row[late_time_min];?></td>
					<td width="20"><?php echo $total_hours;?></td>
                    <td width="60"><?php echo $shift_chart[$row[shift_policy]];?></td>
                    <td width="120"><?php //echo $shift_chart[$row[shift_policy]];?></td>						
				</tr>
			<? 
				$section_total_ot+=$total_hours;
				$department_total_ot+=$total_hours;
				$division_total_ot+=$total_hours;
				$grand_total_ot+=$total_hours;
				$total_grand_total+=$total_hours;
				
				if($row["status"]=='P'){$p++;}
				if($row["status"]=='A'){
						$a++;
						if($a>1){
								$param_emp_code .=',';
								}
						$param_emp_code .=$row["emp_code"];
						
				}
				if($row["status"]=='MR')
						{
							$mr++;
							if($mr>1){
								$mr_emp_code .=',';
								}
							$mr_emp_code .=$row["emp_code"];
						}
				if($row["status"]=='D')
						{
							$d++;
							if($d>1){
								$delay_emp_code .=',';
								}
							$delay_emp_code .=$row["emp_code"];
						}
				if($row["status"]=='CL' || $row["status"]=='SL' || $row["status"]=='ML' || $row["status"]=='EL' || $row["status"]=='LWP' || $row["status"]=='SpL')
						{
							$l++;
							if($l>1){
								$leave_emp_code .=',';
								}
							$leave_emp_code .=$row["emp_code"];
						}
				if($row["status"]=='W')
						{
							$w++;
							if($w>1){
								$weekday_emp_code .=',';
								}
							$weekday_emp_code .=$row["emp_code"];
						}					
			}
	}
	?>
    <tr><td  colspan="15" align="right">Grand Total</td><td><? echo $total_grand_total;?></td></tr><? $total_grand_total=0;?>
    <?
	$total=$p+$a+$l+$d+$mr+$w;
	?>		
		
		</tbody>
	</table> 
	<table width="1000" border="1" class="rpt_table" rules="all">
		  <tr bgcolor="#F0F0B8">
				<td colspan="17" align="center" height=50><b>Summary</b></td>
		  </tr>
		  <tr>				
				<td colspan="3" align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> <? echo $p;?> </td>								  	
				<td colspan="3"  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $param_emp_code;?>&date=<? echo $date;?>','Absent Information'); return false"><? echo $a;?></a> </td>				  			  	
				<td colspan="3"  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $leave_emp_code;?>&date=<? echo $date;?>','Leave Information'); return false"><? echo $l;?></a> </td>				  				   
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $mr_emp_code;?>&date=<? echo $date;?>','Movement Information'); return false"><? echo $mr;?> </a> </td>				  			    
				<td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $delay_emp_code;?>&date=<? echo $date;?>','Late Information'); return false"><? echo $d;?> </a></td>
                <td colspan="2"  align="left" bgcolor="#E5E5E5"><b>Weekend :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $weekday_emp_code;?>&date=<? echo $date;?>','Weekend Information'); return false"><? echo $w;?> </a></td>				  			    
				<td colspan="2"  align="left" bgcolor="#FFFFFF"><b>Total :</b> <? echo $total;?> </td>					
		  </tr>
	</table>
		
	<?	
	
	$html=ob_get_contents();
	ob_clean();
	
	
	//for report temp file delete 
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
		@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";		
	exit();
}