<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	
	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


if( $action == "id_card" ) {	//ID Card Report
	require_once('../../../../resources/tcpdf_5_9_082/tcc/config/lang/eng.php');
	require_once('../../../../resources/tcpdf_5_9_082/tcc/tcpdf.php');
	header ('Content-type: text/html; charset=utf-8'); 
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'RA4', true, 'UTF-8', false);	// create new PDF document
	 
	// set document information
	$pdf->SetCreator('Md. Muntasir Enam');
	$pdf->SetAuthor('Md. Muntasir Enam');
	$pdf->SetTitle('Logic HRM & Payroll');
	$pdf->SetSubject('ID Card');
	$pdf->SetKeywords('Logic, HRM, Payroll, HRM & Payroll, ID Card');
	
	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);	//set default monospaced font
	$pdf->SetMargins(8, 8, 8);								//set margins
	$pdf->SetAutoPageBreak(TRUE, 5);						//set auto page breaks
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				//set image scale factor
	$pdf->setLanguageArray($l);								//set some language-dependent strings
	//$pdf->SetFont('times', '', 10);
	
	//$pdf->addTTFfont('kalpurush.TTF', 'TrueTypeUnicode', '', 32);
	//$pdf->AddFont('kalpurush','BI', 20,'','false');
	//$pdf->SetFont('SUTOM___', '', 10);
	// $img_file = '../../../../images/impossible.gif';
	// $pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
	 
	 $pdf->SetFont('times', '', 9);
	$employees = explode( "_", $employees );
	
	$pdf->AddPage();	//add a page
	
	/*
	if($option == "H"){
	$html = "<table width=\"100%\" border=\"1\" cellspacing=\"30\"><tr>";	//set some text to print
	for( $i = 1; $i <= count( $employees ); $i++ ) {
		$sql = "SELECT * FROM resource_photo WHERE module = 'HRM' AND identifier = '" . $employees[$i-1] . "' AND category = 0 AND type = 1 AND is_deleted = 0 order by id DESC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( mysql_num_rows( $result ) > 0 ) {
			$row = mysql_fetch_assoc( $result );
			$location = $row['location'];
		}
		else $location = '';		
			$html .= "<td width=\"33%\" height=\"295\">
						<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"3\" cellpadding=\"0\">
							<tr><td colspan=\"2\" align=\"center\" height=\"20\"><h3>" . $company_details[$emp_basic[$employees[$i-1]]['company_id']]['company_name'] . "</h3></td></tr>
							<tr>";
								if( $location != '' && file_exists( "../../../../$location" ) ) $html .= "<td>&nbsp;&nbsp;&nbsp;<img src=\"../../../../$location\" width=\"60\" height=\"75\" border=\"1\" /></td>";
								else $html .= "<td height=\"75\">&nbsp;</td>";
								$html .= "<td><img src=\"../../../../resources/images/asro_logo.png\" />&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr><td colspan=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;<b>" . $emp_basic[$employees[$i-1]]['name'] . " </b></td></tr>
							<tr><td colspan=\"2\">&nbsp;</td></tr>
							<tr><td width=\"35%\">&nbsp;&nbsp;&nbsp;&nbsp;ID No.</td><td width=\"65%\">: " . $emp_basic[$employees[$i-1]]['id_card_no'] . "</td></tr>
							<tr><td width=\"35%\">&nbsp;&nbsp;&nbsp;&nbsp;Soft Code</td><td width=\"65%\">: " . $employees[$i-1] . "</td></tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Designation</td><td>: " . $designation_chart[$emp_basic[$employees[$i-1]]['designation_id']]['custom_designation'] . "</td></tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Department</td><td>: " . $department_details[$emp_basic[$employees[$i-1]]['department_id']]['department_name'] . "</td></tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Section</td><td>: ";
					if( $emp_basic[$employees[$i-1]]['section_id'] != 0 ) $html .= $section_details[$emp_basic[$employees[$i-1]]['section_id']]['section_name'];
					$html .= "</td></tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Joining Date</td><td>: " . $emp_basic[$employees[$i-1]]['joining_date'] . "</td></tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Salary Grade</td><td>: ";
					if( $emp_basic[$employees[$i-1]]['salary_grade'] != '' ) $html .= $emp_basic[$employees[$i-1]]['salary_grade'];
					$html .= "</td></tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Blood Group</td><td>: ";
					if( $emp_basic[$employees[$i-1]]['blood_group'] != '0' ) $html .= $blood_group[$emp_basic[$employees[$i-1]]['blood_group']];
					$html .= "</td></tr>
							<tr><td colspan=\"2\" height=\"25\">&nbsp;</td></tr>
							<tr><td colspan=\"2\" align=\"right\">...........................................&nbsp;&nbsp;</td></tr>
							<tr><td colspan=\"2\" align=\"right\">Issuing Authority&nbsp;&nbsp;</td></tr>
						</table>
					</td>";
			if( $i == count( $employees ) && $i % 3 == 1 ) $html .= "<td colspan=\"2\">&nbsp;</td>";
			elseif( $i == count( $employees ) && $i % 3 == 2 ) $html .= "<td>&nbsp;</td>";
			
			if( $i % 9 == 0 && $i < count( $employees ) ) {
				$html .= "</tr></table>";
				$pdf->writeHTML($html, true, false, true, false, '');
				$pdf->AddPage();
				$html = "<table width=\"100%\" border=\"1\" cellspacing=\"30\"><tr>";
			}
			elseif( $i % 3 == 0 && $i < count( $employees ) ) $html .= "</tr><tr>";
		}
		$html .= "</tr></table>";
	}
	*/
	//<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
	
	if($option == "H"){
	$html = "<table width=\"76%\" border=\"1\"><tr>";	//set some text to print
	for( $i = 1; $i <= count( $employees ); $i++ ) 
	{
		$sql = "SELECT * FROM resource_photo WHERE module = 'HRM' AND identifier = '" . $employees[$i-1] . "' AND category = 0 AND type = 1 AND is_deleted = 0 order by id DESC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( mysql_num_rows( $result ) > 0 ) {
			$row = mysql_fetch_assoc( $result );
			$location = $row['location'];
		}
		else $location = '';
		$addrs ="";		
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['plot_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['plot_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['level_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['level_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['block_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['block_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['road_no'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['road_no'].", "; 
		if($company_details[$emp_basic[$employees[$i-1]]['company_id']]['city'] !='') $addrs .= $company_details[$emp_basic[$employees[$i-1]]['company_id']]['city']." "; 
		
		
		if($txt_to_date=="")
		{
			$validitidate=date('d-m-Y', strtotime('+365 days'));
		}
		else 
		{
			$validitidate=$txt_to_date;
		}
							
			$html .= "<td>												
						<table>						
							<tr>
								<td valign=\"bottom\" align=\"center\"><img src=\"logo/fakir_fashion_logo.jpg\" width=\"100\" height=\"30\" /></td>
							</tr>
							<tr>
								<td style=\"font-size:48px;\" width=\"100%\" colspan=\"2\" align=\"center\">" . $company_details[$emp_basic[$employees[$i-1]]['company_id']]['company_name'] . "</td>
							</tr>
							<tr><td style=\"font-size:35px;\" height=\"18\" valign=\"top\" width=\"100%\" colspan=\"2\" align=\"center\"><b>" .$card_name ."</b></td></tr>							
							<tr>";							
								if( $location != '' && file_exists( "../../../../$location" ) ) $html .= "<td align=\"center\" colspan=\"2\"><img src=\"../../../../$location\" width=\"80\" height=\"80\" border=\"1\" /></td>";
								else $html .= "<td height=\"80\" width=\"80\" colspan=\"2\">&nbsp;</td>";
					$html .= "</tr>	
							<tr><td align=\"center\" colspan=\"2\" width=\"100%\"><b>".$emp_basic[$employees[$i-1]]['name']."</b></td></tr>						
							<tr><td width=\"100%\" colspan=\"2\" align=\"center\"><b>ID: " .strtoupper($emp_basic[$employees[$i-1]]['id_card_no']). "</b></td></tr>
							<tr><td width=\"100%\" colspan=\"2\" align=\"center\"><b>" .$designation_chart[$emp_basic[$employees[$i-1]]['designation_id']]['custom_designation']. "</b></td></tr>
							<tr><td width=\"100%\" colspan=\"2\" align=\"center\"><b>Dept: ".$department_details[$emp_basic[$employees[$i-1]]['department_id']]['department_name']. "</b></td></tr>
							<tr><td width=\"100%\" colspan=\"2\" align=\"center\">Section: ".$section_details[$emp_basic[$employees[$i-1]]['section_id']]['section_name']. "</td></tr>
							<tr><td width=\"100%\" colspan=\"2\" align=\"center\">Joining Date: " . convert_to_mysql_date($emp_basic[$employees[$i-1]]['joining_date']) . "</td></tr>	
							<tr><td width=\"100%\" colspan=\"2\" align=\"center\">&nbsp;Valid Upto: ".$validitidate."</td></tr>
							<tr><td width=\"100%\" colspan=\"2\" align=\"center\">This card is non transferable</td></tr>
							";
							
										
					$html .= "<tr><td colspan=\"2\">&nbsp;</td></tr>						 
						</table>
					</td>";
			if( $i == count( $employees ) && $i % 3 == 1 ) $html .= "<td colspan=\"2\">&nbsp;</td>";
			elseif( $i == count( $employees ) && $i % 3 == 2 ) $html .= "<td>&nbsp;</td>";
			
			if( $i % 9 == 0 && $i < count( $employees ) ) {
				$html .= "</tr></table>";
				$pdf->writeHTML($html, true, false, true, false, '');
				$pdf->AddPage();
				$html = "<table width=\"90%\" border=\"1\" cellspacing=\"1\"><tr>";
				//$html = "<table width=\"90%\" border=\"1\" cellspacing=\"1\" height=\"60%\"><tr>";
			}
			elseif( $i % 3 == 0 && $i < count( $employees ) ) $html .= "</tr><tr>";
		}
		$html .= "</tr></table>";
	}
	
	if($option == "V"){
		$html = "<table width=\"100%\" border=\"1\" cellspacing=\"30\"><tr>";	//set some text to print
		for( $i = 1; $i <= count( $employees ); $i++ ) {
		$sql = "SELECT * FROM resource_photo WHERE module = 'HRM' AND identifier = '" . $employees[$i-1] . "' AND category = 0 AND type = 1 AND is_deleted = 0 order by id DESC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( mysql_num_rows( $result ) > 0 ) {
			$row = mysql_fetch_assoc( $result );
			$location = $row['location'];
		}
		else $location = '';
		$html .= "<td width=\"310\" height=\"180\" colspan=\"2\">
					<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"3\" cellpadding=\"0\">	
						<tr><td colspan=\"2\">&nbsp;</td></tr>
						<tr><td width=\"70%\"><table>";
							$html .= "<tr><td>&nbsp;<img src=\"logo/fakir_fashion_logo.jpg\" width=\"75\" height=\"30\" />&nbsp;&nbsp;&nbsp;</td><td></td></tr>
									  <tr><td colspan=\"2\">&nbsp;</td></tr>
									  <tr><td colspan=\"2\" align=\"center\" ><h1>" . $company_details[$emp_basic[$employees[$i-1]]['company_id']]['company_name'] . "</h1></td></tr>
									  <tr><td colspan=\"2\" align=\"center\" style=\"font-size:35px\">&nbsp;&nbsp;&nbsp;&nbsp;<b>" . $emp_basic[$employees[$i-1]]['name'] . "</b></td></tr>	
									  <tr><td colspan=\"2\" align=\"center\" style=\"font-size:25px\">&nbsp;&nbsp;&nbsp;&nbsp;<b>" . $designation_chart[$emp_basic[$employees[$i-1]]['designation_id']]['custom_designation'] . "</b></td></tr>	
									  </table></td>";	
							
							if( $location != '' && file_exists( "../../../../$location" ) ) $html .= "<td align=\"right\" width=\"30%\">&nbsp;&nbsp;&nbsp;<img src=\"../../../../$location\" width=\"78\" height=\"95\" border=\"1\" />&nbsp;&nbsp;&nbsp;&nbsp;</td>";
								else $html .= "<td align=\"right\" width=\"30%\">&nbsp;</td>";			
							$html .="</tr><tr>		
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ID # " . $emp_basic[$employees[$i-1]]['id_card_no'] . "</b></td>";
							$html .="</tr><tr>		
							<td>&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Blood Group: ";
							if( $emp_basic[$employees[$i-1]]['blood_group'] != '0' ) $html .= $blood_group[$emp_basic[$employees[$i-1]]['blood_group']];							
							$html .= "</td></tr>							
							<br /><br />
							<tr><td colspan=\"2\">&nbsp;</td></tr>
							<tr><td colspan=\"2\">&nbsp;</td></tr>
							<tr><td align=\"left\">&nbsp;__________________</td>
							<td align=\"right\">__________________&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
							<tr><td align=\"left\">&nbsp;Holder's Signature</td>
							<td align=\"right\">Authorized Signature&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>							
					</table>
				</td>";
				
			if( $i == count( $employees ) && $i % 1 == 1 ) $html .= "<td colspan=\"2\">&nbsp;</td>";
			elseif( $i == count( $employees ) && $i % 1 == 1 ) $html .= "<td>&nbsp;</td>";
			
			if( $i % 8 == 0 && $i < count( $employees ) ) {
				$html .= "</tr></table>";
				$pdf->writeHTML($html, true, false, true, false, '');
				$pdf->AddPage();
				$html = "<table width=\"100%\" border=\"1\" cellspacing=\"30\"><tr>";
			}
			elseif( $i % 1 == 0 && $i < count( $employees ) ) $html .= "</tr><tr>";
		}
		$html .= "</tr></table>";	
	}
	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.pdf") as $filename) {			
			@unlink($filename);
		}
	
	// output the HTML content	
	$pdf->writeHTML($html, true, false, true, false, '');	
	//Close and output PDF document
	$name = 'id_card_' . date('j-M-Y_h-iA') . '.pdf';
	$pdf->Output('tmp_report_file/' . $name, 'F');
	
	echo "1###$name";
	exit();
}





?>
 