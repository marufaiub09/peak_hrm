<?php
//Sohel
//22.04.2014
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );


//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$start_1 = substr($row["starting_date"],0,3);
		$start_2 = substr($row["starting_date"],-1,1);

		$end_1 = substr($row["ending_date"],0,3);
		$end_2 = substr($row["ending_date"],-2,2);
		
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$start_1." ".$start_2." - ".$end_1." ".$end_2."</option>";
		//echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}



//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

		
// OT SLab POLICY Details
	$ot_policy_slab = array();
	$sql_shift = "SELECT * FROM lib_policy_overtime_slab order by id asc"; 
	$result_shift = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	$i=0;
	while($row_shift = mysql_fetch_assoc( $result_shift ) ) 
	{
		$i++;
		$ot_policy_slab[$row_shift[policy_id]][$i]=$row_shift;
		//$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]]['overtime_slot'][$i]=$row_shift[overtime_slot];
		//$ot_policy_slab[$row_shift[policy_id]][$row_shift[overtime_head]]['overtime_multiplier'][$i]=$row_shift[overtime_multiplier];
		//$ot_policy_slab[$row_shift[policy_id]][$i]['overtime_head']['overtime_head']=$row_shift[overtime_head];
	}
	
	
	
if($action=='monthly_check_sheet'){
	
	/*		
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$tot_days = cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$txt_from_date = $cbo_year_selector . "-" . $cbo_month_selector . "-" . "01";
	$txt_to_date = $cbo_year_selector . "-" . $cbo_month_selector . "-" . $tot_days;
	//$tot_days=daysdiff($txt_from_date, $txt_to_date);
	*/
	
	
	$date_range=explode("_",$cbo_month_selector);
	$tot_days=datediff("d", $date_range[0],$date_range[1]);
	//echo $date_range[0]."=".$date_range[1]."=".$tot_days;
	
	
?>	
<style>
	.verticalText 
	{               
		writing-mode: tb-rl;
		 filter: flipv fliph;
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
		-o-transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		transform: rotate(270deg);
		width: 3em;		 
	}
</style>

<?
	
	//payroll head 
	$salary_head_list = array();	
	$sql_sal="select * from lib_payroll_head where id in (5,6,7,35) and status_active=1 and is_deleted=0 order by id";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);	
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_head_list[$row_sal[id]]=$row_sal[is_applicable];
	}
	//print_r($salary_head_list);die;	
	
	//variable settings for ot
	$sql_var = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1";
	$result = mysql_query( $sql_var ) or die( $sql_var . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $hrmrow = mysql_fetch_assoc( $result ) ) {
 		$var_hrm_chart[$hrmrow['company_name']] = array();
		$var_hrm_chart[$hrmrow['company_name']]['adjust_in_time'] = mysql_real_escape_string( $hrmrow['adjust_in_time'] );
		$var_hrm_chart[$hrmrow['company_name']]['adjust_out_time'] = mysql_real_escape_string( $hrmrow['adjust_out_time'] );
		$var_hrm_chart[$hrmrow['company_name']]['first_ot_limit'] = mysql_real_escape_string( $hrmrow['first_ot_limit'] );
		$var_hrm_chart[$hrmrow['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $hrmrow['one_hour_ot_unit'] );
		$var_hrm_chart[$hrmrow['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $hrmrow['allow_ot_fraction'] );
		$var_hrm_chart[$hrmrow['company_name']]['ot_start_minute'] = mysql_real_escape_string( $hrmrow['ot_start_minute'] );
	}
	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id in ($department_id)";
	if ($division_id==0) $division_id=""; else  $division_id="and division_id in ($division_id)";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in ($designation_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
			
	ob_start();	
	
	?>  
	<style media="print">
    p{page-break-before:always}
    </style>
	<table cellpadding="0" cellspacing="0" border="1" width="1000" align="center" style="border:1px solid #000; font-size:10px" rules="all" class="rpt_table">       
    <?

	
	$sql_data="select policy_id,total_leave_criteria,late_criteria,absent_criteria,amount from  lib_policy_attendance_bonus_definition  order by id asc";
	$leave_criteria_array=array();
	$late_criteria_array=array();
	$absent_criteria_array=array();
	$attn_bonas_amount_array=array();
	
	$sql_data_exe=mysql_query($sql_data); 
	while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
	{
		$leave_criteria_array[$sql_data_rslt['policy_id']]['total_leave_criteria']=$sql_data_rslt['total_leave_criteria'];
		$late_criteria_array[$sql_data_rslt['policy_id']]['late_criteria']=$sql_data_rslt['late_criteria'];
		$absent_criteria_array[$sql_data_rslt['policy_id']]['absent_criteria']=$sql_data_rslt['absent_criteria'];
		$attn_bonas_amount_array[$sql_data_rslt['policy_id']]['amount']=$sql_data_rslt['amount'];
	}				
//print_r($attn_bonas_amount_array);die;
	
	$sql_attnd = "SELECT emp_code,designation_id,subsection_id,department_id,section_id, 
							count(CASE WHEN status in ('CL','ML','SL','EL','SpL','LWP') THEN emp_code END) as t_leave,
							count(CASE WHEN status in ('H','FH','CH','GH','W') THEN emp_code END) as t_holiday,
							count(CASE WHEN status in ('P') THEN emp_code END) as t_present,
							count(CASE WHEN status in ('A') THEN emp_code END) as t_absent,
							count(CASE WHEN status in ('D') THEN emp_code END) as t_late,
							count(CASE WHEN status in ('MR') THEN emp_code END) as t_mr
							FROM hrm_attendance
							WHERE  attnd_date between '$date_range[0]' and '$date_range[1]' 
							$cbo_company_id 
							$location_id
							$division_id
							$department_id 
							$section_id 
							$subsection_id 
							$designation_id
							group by emp_code";
	//echo $sql_attnd;die;
	
	$exe_sql_attnd=mysql_db_query($DB, $sql_attnd);  //sum(CASE WHEN total_over_time_min <>0 THEN total_over_time_min END) AS 'total_over_time_min',
	$attendance_array=array();
	$current_emp_array=array();
	$designation_id_array=array();
	$subsection_id_array=array();
	$department_id_array=array();
	$section_id_array=array();
	$i=0;
	while($data=mysql_fetch_array($exe_sql_attnd))
	{
		$attendance_array[$data['emp_code']]['leave']=$data['t_leave'];
		$attendance_array[$data['emp_code']]['holiday']=$data['t_holiday'];
		$attendance_array[$data['emp_code']]['present']=$data['t_present'];
		$attendance_array[$data['emp_code']]['absent']=$data['t_absent'];
		$attendance_array[$data['emp_code']]['late']=$data['t_late'];
		$attendance_array[$data['emp_code']]['mr']=$data['t_mr'];
		$current_emp_array[$i]=$data['emp_code'];
		$designation_id_array[$data['emp_code']]=$data['designation_id'];
		$subsection_id_array[$data['emp_code']]=$data['subsection_id'];
		$department_id_array[$data['emp_code']]=$data['department_id'];
		$section_id_array[$data['emp_code']]=$data['section_id'];
		$i++; 
	}
 
	$found_emp_code="'".implode("','",$current_emp_array)."'";
	//print_r($attendance_array); die;
	//echo $found_emp_code;die;
	$emp_code_search=" and emp_code in ( $found_emp_code )";
	
	
		
		// Employee OT Calculation bayer OT
		$sql_ot=" SELECT emp_code,";   
		for($i=0;$i<$tot_days; $i++)
		{
			$c_date=add_date($date_range[0],$i);
			if($i!=0) $sql_ot .=",";
			$sql_ot .=" sum(CASE WHEN attnd_date ='$c_date' THEN total_over_time_min END) AS 'OT".$c_date."',  sum(CASE WHEN attnd_date ='$c_date' and is_regular_day=0 THEN total_over_time_min END) AS 'BOT".$c_date."'";
		}
		$sql_ot .="from  hrm_attendance  where attnd_date between '".$date_range[0]."' and '".$date_range[1]."' and status not in ('W','GH','FH','CH','H') 
		$emp_code_search 
		$cbo_company_id 
		$location_id
		$division_id
		$department_id 
		$section_id 
		$subsection_id 
		$designation_id
		group by emp_code order by emp_code";
		
		//and is_regular_day!=0 change by SS sumon sutrodhor
			
		$exe_sql_attnd=mysql_db_query($DB, $sql_ot);
		$employee_ot=array();
		//$b_employee_ot=array();
		while($data=mysql_fetch_array($exe_sql_attnd))
		{
			for($i=0;$i<$tot_days; $i++)
			{
				$c_date=add_date($date_range[0],$i);
				$employee_ot[$data['emp_code']][$c_date]=$data['OT'.$c_date];
				//$b_employee_ot[$data['emp_code']][$c_date]=$data['BOT'.$c_date];
			}
		} 
		
		//echo $sql_ot;die;
		
		
		// Employee OT Calculation Total OT
		$sql_exot=" SELECT emp_code,";   
		for($i=0;$i<$tot_days; $i++)
		{
			$c_date=add_date($date_range[0],$i);
			if($i!=0) $sql_exot .=",";
			$sql_exot .=" sum(CASE WHEN attnd_date ='$c_date' THEN total_over_time_min END) AS 'OT".$c_date."',  sum(CASE WHEN attnd_date ='$c_date' and is_regular_day=0 THEN total_over_time_min END) AS 'BOT".$c_date."'";
		}
		$sql_exot .="from  hrm_attendance  where attnd_date between '".$date_range[0]."' and '".$date_range[1]."'
		$emp_code_search 
		$cbo_company_id 
		$location_id
		$division_id
		$department_id 
		$section_id 
		$subsection_id 
		$designation_id
		group by emp_code order by emp_code";	
		$exe_sql=mysql_db_query($DB, $sql_exot);
		$employee_tot_ot=array();
		//$b_employee_ot=array();
		while($data=mysql_fetch_array($exe_sql))
		{
			for($i=0;$i<$tot_days; $i++)
			{
				$c_date=add_date($date_range[0],$i);
				$employee_tot_ot[$data['emp_code']][$c_date]=$data['OT'.$c_date];
				//$b_employee_ot[$data['emp_code']][$c_date]=$data['BOT'.$c_date];
			}
		} 
		//echo $sql_exot;die;
		
	
	$sql = "SELECT emp_code,id_card_no,joining_date,designation_id,attendance_bonus_policy,company_id,location_id,division_id,department_id,section_id,subsection_id,status_active, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name,duty_roster_policy FROM hrm_employee WHERE emp_code!='' 
			$category
			$cbo_company_id 
			$location_id
			$division_id
			$department_id 
			$section_id 
			$subsection_id 
			$designation_id 
			$salary_based
			$emp_code_search
			order by CAST(right(trim(id_card_no), $id_card_no_ord)as SIGNED)";
	//echo $sql;
		
				
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$i=0;$new_department=array();$new_section=array();
	$sl=0;
	while($rows=mysql_fetch_array($result))
	{
		
		$emp_condition=0; // Regular Worker
		if ($rows[status_active]==0) // In active Employee
		{
			$d="separated_from";$dd="hrm_separation";$ddd="emp_code='$rows[emp_code]' order by id desc";
			$old_mod= return_field_value($d,$dd,$ddd);
			
			if (strtotime($old_mod)>=strtotime($date_range[0]) && strtotime($old_mod)<strtotime($date_range[1]))
			{
				$emp_condition=1; // Resign Worker
			}
			else 
				$emp_condition=3; // not  Worker
		}
		
	   if ($rows[status_active]==1)
		{
			if (strtotime($rows[joining_date])>=strtotime($date_range[0]) && strtotime($rows[joining_date])<=strtotime($date_range[1]))
			{
				$emp_condition=2; // New Worker
			}
			else if (strtotime($rows[joining_date])>strtotime($date_range[1]) ) $emp_condition=3; // not Worker
		}
		
	
	
		if($emp_condition<>3)
		{
			$sl++;
			if($sl==1)
			{			
			?> 
            <thead>	                           
				<tr>
					<td colspan="18" align="center"><font size="+1"><? echo $company_details[$rows['company_id']]['company_name']; ?></font><br />
                    Post Office Road,Fatullah,Narayangonj.
						<!--/*Address: Plot No # <? /*echo $company_details[$rows['company_id']]["plot_no"]; ?>, 
						Level No # <? echo $company_details[$rows['company_id']]["level_no"]; ?>, 
						Block No # <? echo $company_details[$rows['company_id']]["block_no"]; ?>, 
						Road No # <? echo $company_details[$rows['company_id']]["road_no"]; ?>, 
						<? echo $company_details[$rows['company_id']]["city"]; echo $company_details[$rows['company_id']]["country_name"];*/?>*/--><br />
					</td>
				</tr>
			
			<tr>
				<td colspan="7" >
					Section : <? echo $section_details[$section_id_array[$rows['emp_code']]]['section_name']; ?> 
				</td>
				<td colspan="11" >
					Department : <? echo $department_details[$department_id_array[$rows['emp_code']]]['department_name']; ?>                                       
				</td>
			</tr>
			<tr align="center">
				<td width="20"><b>SL NO</b></td>
				<td width="85"><b>ID Card</b></td>
				<td width="70"><b>Employee Code</b></td>
				<td width="130"><b>Name</b></td>
				<td width="100"><b>Designation</b></td>
				<td width="70"><b>Date Of Joining</b></td>
				<td width="70"><b>Floor</b></td>
				<td width="50"><b>Total Leave</b></td>
				<td width="50"><b>Total Late</b></td>
				<td width="50"><b>Total Holiday</b></td>
                <td width="50"><b>Present Days</b></td>
				<td width="50"><b>Pay Days</b></td>
				<td width="50"><b>Absent Days</b></td>
				<td width="50"><b>Not Pay Days</b></td>
				<td width="50"><b>OT</b></td>
				<td width="50"><b>Extra OT</b></td>
				<td width="50"><b>Atten. Bonus</b></td>
				<td width="50"><b>Status</b></td>  
			</tr>
       </thead>
			<?
			$new_section[$i]=trim($rows[section_id]);
			//$i=1;
			}
				
				
		if ($emp_condition==2){ // New
			$total_payable_days=datediff('d',$rows[joining_date],$date_range[1]);	//New Join 
			$status='N';
		}
		else if ($emp_condition==1){ // Resign	
			$total_payable_days=datediff('d',$date_range[0],$old_mod);	// Resign
			$status='D';
		}
		else{
			$total_payable_days=$tot_days;	//Regular
			$status='R';
		}
	    $calendar_days=$tot_days;
		$not_payable_days=$calendar_days-$total_payable_days;
		
		
		//if($salary_head_list[5]==1 ) // Over Time=======================================================
		//{		
			$total_ot_hr=0;
			$total_emp_ot=0;
			$tmppd=0;
			$hr=0;
			$min=0;
			$ot=0;
			//$total_min=0;
			$tmppdt=0;
			$thr=0;
			$tmin=0;
			
			for($i=0;$i<$tot_days; $i++)
			{
				$c_date=add_date($date_range[0],$i);
				$total_ot_hr=get_buyer_ot_hr($employee_ot[$rows['emp_code']][$c_date],$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$var_hrm_chart[$rows['company_id']]['first_ot_limit']);
				
				$tmppdt=explode(".",$total_ot_hr);
				$thr+=$tmppdt[0];
				$tmin+=$tmppdt[1];
				$total_ot_hr =($thr+ floor($tmin/60)).".".str_pad(($tmin%60),2,"0",STR_PAD_LEFT);
				
				$total_emp_ot=get_buyer_ot_hr($employee_tot_ot[$rows['emp_code']][$c_date],$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],1440);
				
				$tmppd=explode(".",$total_emp_ot);
				$hr+=$tmppd[0];
				$min+=$tmppd[1];
				$ot =($hr+ floor($min/60)).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
				
			}
			$tmpp=explode(".",$ot);
			$total_ot_min=($tmpp[0]*60)+$tmpp[1];
			$tmp=explode(".",$total_ot_hr);
			$bay_ot_min=($tmp[0]*60)+$tmp[1];
			$ex_ot_min=$total_ot_min-$bay_ot_min;
			$total_ex_ot = sprintf("%d.%02d", abs((int)($ex_ot_min/60)), abs((int)($ex_ot_min%60)));
			//echo $ot;
		//}
		
				
		// Extra Over Time=================================================================
		/*$buyer_ot=0;
		$extra_ot=0;
		$fur_extra_ot=0;
		$basic_salary=return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$rows[emp_code]");
		$ot_rate=$basic_salary/104;
		$emp_salary_sheet=0;$buyer_ot=0;
		if($salary_head_list[6]==1 ) // Extra Over Time
		{
			$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";
			$sql_data_exe=mysql_query($sql_data);
			$old_hr=0;
			$old_slot=0;
			$ot_slot=0;
			$u=0;
			
			while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
			{
				$u++;
				$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
				$ot_multiplier=$sql_data_rslt[overtime_multiplier];
				
				$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".$date_range[0]."' and '".$date_range[1]."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
				$total_ot_hr=$total_ot_hr-$old_hr;
				$old_hr= $old_hr+$total_ot_hr; 
		
				if($sql_data_rslt[overtime_head]==6)
				{
					$emp_salary_sheet= $emp_salary_sheet+($ot_rate*$ot_multiplier*$total_ot_hr);  
					$extra_ot= $extra_ot+$total_ot_hr; 
				}
			}
		}
		
		if( $salary_head_list[7]==1) // Further Extra Over Time
		{
			$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";
			$sql_data_exe=mysql_query($sql_data);
			$old_hr=0;
			$old_slot=0;
			$ot_slot=0;
			$u=0;
			while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
			{
				$u++;
				$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
				$ot_multiplier=$sql_data_rslt[overtime_multiplier];
				
				$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between'".$date_range[0]."' and '".$date_range[1]."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
				$total_ot_hr=$total_ot_hr-$old_hr;
				$old_hr= $old_hr+$total_ot_hr; 
				
				if($sql_data_rslt[overtime_head]==7)
				{
					$emp_salary_sheet = $emp_salary_sheet +($ot_rate*$ot_multiplier*$total_ot_hr);  
					$fur_extra_ot= $fur_extra_ot+$total_ot_hr; 
				}
			} 
		}*/
		
		// Extra Over Time===end==============================================================
				
						
	$att_bonus_amount=0;
	if($salary_head_list[35]==1)  // Attendance Bonus  Rules=================================================
	{ 
		if( $emp_condition==0)// Regular Worker
		{
			if($leave_criteria_array[$rows['attendance_bonus_policy']]['total_leave_criteria']>= $attendance_array[$rows['emp_code']]['leave'] && $late_criteria_array[$rows['attendance_bonus_policy']]['late_criteria']>=$attendance_array[$rows['emp_code']]['late']  &&   $absent_criteria_array[$rows['attendance_bonus_policy']]['absent_criteria']>=$attendance_array[$rows['emp_code']]['absent'] )
			{
				$att_bonus_amount=round($attn_bonas_amount_array[$rows['attendance_bonus_policy']]['amount']); 
			}
		}
	}
			
		
	?>    
	<tr bgcolor="<? echo $bgcolor;?>" height="30">
		<td><? echo $sl; ?></td>
		<td><p><? echo $rows['id_card_no']; ?></p></td>
		<td><? echo $rows['emp_code']; ?></td>
		<td><? echo $rows['name']; ?></td>
		<td><? echo $designation_chart[$designation_id_array[$rows['emp_code']]]['custom_designation']; ?></td> 
		<td><? echo convert_to_mysql_date($rows[joining_date]);?></td> 
		<td><? echo $subsection_details[$subsection_id_array[$rows['emp_code']]]['subsection_name']; ?></td>          	
		<td align="center"><? echo $attendance_array[$rows['emp_code']]['leave']; ?></td> 
		<td align="center"><? echo $attendance_array[$rows['emp_code']]['late']; ?></td>	 	
		<td align="center"><? echo $attendance_array[$rows['emp_code']]['holiday']; ?></td>
        <td align="center"><? echo $attendance_array[$rows['emp_code']]['present']; ?></td>
		<td align="center"><? echo $pday=($attendance_array[$rows['emp_code']]['leave']+$attendance_array[$rows['emp_code']]['late']+$attendance_array[$rows['emp_code']]['holiday']+$attendance_array[$rows['emp_code']]['present']+$attendance_array[$rows['emp_code']]['mr']);?></td>
		<td align="center"><? echo $attendance_array[$rows['emp_code']]['absent']; ?></td>
		<td align="center"><? echo $not_payable_days;?></td>
		<td align="right"><? echo $total_ot_hr;//."=".$ot;  ?> </td>
		<td align="right"><? echo $total_ex_ot; //echo $total_ot_min."-".$bay_ot_min."=".$ex_ot_min; //echo $extra_ot+$fur_extra_ot;?></td>
		<td align="right"><? echo $att_bonus_amount;?></td>
		<td align="center"><? echo $status;?></td>					            	
	</tr>  
	<?	
		$i++;
		}//end if condition
	
	}//end first while loop
    ?>
    </table>
    <p></p>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}


function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

	
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}

?>

