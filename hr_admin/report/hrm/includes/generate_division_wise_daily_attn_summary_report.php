<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


			
	
	
if($action=='division_wise_daily_attn_summary'){
	
	if($order_by_id==0){$orderby_fild="a.designation_id"; $header_field="Designation";}
	else if($order_by_id==1){$orderby_fild="a.department_id"; $header_field="Department";}
	else if($order_by_id==2){$orderby_fild="a.section_id"; $header_field="Section";}
	else if($order_by_id==3){$orderby_fild="a.subsection_id"; $header_field="Sub Section";}
	else if($order_by_id==4){$orderby_fild="a.designation_id"; $header_field="Designation";}
	else {$orderby_fild="a.designation_id"; $header_field="Designation";}
	
	
	//print_r($division_id);die;
	$company_id=$cbo_company_id;
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id=='null') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id=='null') $department_id=""; else  $department_id="and a.department_id in ($department_id)";
	if ($designation_id=='null') $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	if ($section_id=='null') $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id=='null') $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	if ($txt_date=='') $txt_date=""; else $txt_date="and a.attnd_date='".convert_to_mysql_date($txt_date)."'";		
	
	ob_start();	
 	?>    
	<table cellpadding="0" cellspacing="0" border="1" width="1150" style="border:1px solid #000; font-size:11px" class="rpt_table" rules="all">       
    	<thead>
        	<tr>
        		<th colspan="19"><font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br /><? ?><br />Employee's  division Wise Daily Attendance Summary</th>
            </tr>
            <tr class="tbl_top">
                <th width="20" rowspan="2"><b>SL</b></th>
                <th width="100" rowspan="2"><b>Division</b></th>
                <th width="100" rowspan="2"><b><? echo $header_field; ?></b></th>
                <th width="120" colspan="2"><b>Strength</b></th>
				<th width="110" rowspan="2"><b>Total Strength</b></th>
                <th width="120" colspan="2"><b>Present</b></th>
                <th width="70" rowspan="2"><b>Total Present</b></th>
                <th width="120" colspan="2"><b>Absent</b></th>
                <th width="70" rowspan="2"><b>Total Absent</b></th>
                <th width="" colspan="2"><b>Present %</b></th>
                <th width="" rowspan="2"><b>Total Present %</b></th>
               	<th width="" colspan="2"><b>Absent %</b></th>
              	<th width="" rowspan="2"><b>Total Absent %</b></th>
               
            </tr>
            <tr>
                <th align="center">Male</th>
                <th align="center">Female</th>
                <th align="center">Male</th>
                <th align="center">Female</th>
                <th align="center">Male</th>
                <th align="center">Female</th>
                 <th align="center">Male</th>
                <th align="center">Female</th>
                 <th align="center">Male</th>
                <th align="center">Female</th>
            </tr>
        </thead> 
	<?	
	
		$sql="SELECT a.designation_id,a.company_id,a.location_id,a.division_id,a.department_id, a.section_id, a.subsection_id,b.designation_level,
						COUNT(a.emp_code) as total_emp,
						COUNT(CASE WHEN b.sex=0 THEN 1 END) as total_male,
						COUNT(CASE WHEN b.sex=1 THEN 1 END) as total_female, 
						COUNT(CASE WHEN a.status in ('P','D','MR') THEN 1 END) as total_present,
						COUNT(CASE WHEN a.status in ('P','D','MR') and b.sex=0 THEN 1 END) as total_present_male,
						COUNT(CASE WHEN a.status in ('P','D','MR') and b.sex=1 THEN 1 END) as total_present_female, 
						COUNT(CASE WHEN a.status='A' THEN 1 END) as total_absent,
						COUNT(CASE WHEN a.status='A' and b.sex=0 THEN 1 END) as total_absent_male,
						COUNT(CASE WHEN a.status='A' and b.sex=1 THEN 1 END) as total_absent_female
					FROM 
						hrm_attendance a,hrm_employee b
					WHERE 
						a.emp_code=b.emp_code  $designation_id  $txt_date $cbo_company_id $location_id $division_id $department_id $section_id $subsection_id 		                        $category $salary_based group by a.division_id,$orderby_fild order by a.division_id, $orderby_fild";
						
			//echo $sql; 
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
			
			$i=0;$sl=1;
			$new_company=array();$new_division=array();
			$grand_total_emp = 0;$grand_total_strength=0;$grand_total_male=0;$grand_total_female=0;
			$grand_total_male_present=0;$grand_total_female_present=0;$grand_total_present=0;
			$grand_total_absent_male=0;$grand_total_absent_female=0;$grand_total_absent=0;
			$grand_total_male_present_per=0;$grand_total_female_present_per=0;$grand_total_present_in_per=0;
			$grand_total_male_absent_in_per=0;$grand_total_female_absent_in_per=0;$grand_total_absent_in_per=0;$grand_total_absent_in_per=0;
			$sub_tot_male=0;$sub_total_female=0;$sub_total_strength=0;$sub_tot_male_present=0;
			$sub_total_female_present=0;$sub_total_present=0;$sub_total_male_absent=0;$sub_total_female_absent=0;
			$sub_total_absent=0;$sub_total_present_male_in_per=0;$sub_total_present_female_in_per=0;$sub_total_present_in_per=0;
			$sub_total_absent_male_in_per=0;$sub_total_absent_female_in_per=0;$sub_total_absent_in_per=0;		        	
			
			$present_male_ratio=array();
			$present_female_ratio=array();
			$present_ratio=array();
			$absent_male_ratio=array();
			$absent_female_ratio=array();
			$absent_ratio=array();
			$desig_arr=array();
			$desig_arr_count=array();
			$m=0;
			while($row = mysql_fetch_array($result) )
			{								
			
			
			if (in_array($row['division_id'], $new_division))
					{
						$i++;
						$sub_tot_male+=$row['total_male'];
						$sub_total_female+=$row['total_female'];
						$sub_total_strength=$sub_tot_male+$sub_total_female;
						$sub_tot_male_present+=$row['total_present_male'];
						$sub_total_female_present+=$row['total_present_female'];
						$sub_total_present=$sub_tot_male_present+$sub_total_female_present;
						
						$sub_total_male_absent+=$row['total_absent_male'];
						$sub_total_female_absent+=$row['total_absent_female'];
						$sub_total_absent=$sub_total_male_absent+$sub_total_female_absent;
						$sub_total_present_male_in_per=number_format(($sub_tot_male_present/$sub_tot_male)*100,2);
						$sub_total_present_female_in_per=number_format(($sub_total_female_present/$sub_total_female)*100,2);
						$sub_total_present_in_per=number_format(($sub_total_present/$sub_total_strength)*100,2);
						$sub_total_absent_male_in_per=number_format(($sub_total_male_absent/$sub_tot_male)*100,2);
						$sub_total_absent_female_in_per=number_format(($sub_total_female_absent/$sub_total_female)*100,2);
						$sub_total_absent_in_per=number_format(($sub_total_absent/$sub_total_strength)*100,2);
						
					}
				else
					{
						$new_division[$i]=$row[division_id];
						$i++;
					
					if($i!=1){		
					?>
						<tr bgcolor="#CCCCCC">
							<td colspan="3"><b>Sub Total :</b></td>
                            <td align="right"><b><? echo $sub_tot_male; ?></b></td>
                            <td align="right"><b><? echo $sub_total_female; ?></b></td>
                            <td align="right"><b><? echo $sub_total_strength; ?></b></td>
                            <td align="right"><b><? echo $sub_tot_male_present; ?></b></td>
                            <td align="right"><b><? echo $sub_total_female_present; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present; ?></b></td>
                            <td align="right"><b><? echo $sub_total_male_absent; ?></b></td>
                            <td align="right"><b><? echo $sub_total_female_absent; ?></b></td>
                            <td align="right"><b><? echo $sub_total_absent; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present_male_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present_female_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_absent_male_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_absent_female_in_per; ?></b></td>
                             <td align="right"><b><? echo $sub_total_absent_in_per; ?></b></td>
                            
						</tr>
                    <? } ?>    
                        <tr bgcolor="#CCCCCC">
							<td colspan="18"><b>Division : <? echo $division_details[$row['division_id']]['division_name']; ?></b></td>
						</tr>
                       
                    <?  
						$sub_tot_male=$row['total_male'];
						$sub_total_female=$row['total_female'];
						$sub_total_strength=$sub_tot_male+$sub_total_female;
						$sub_tot_male_present=$row['total_present_male'];
						$sub_total_female_present=$row['total_present_female'];
						$sub_total_present=$sub_tot_male_present+$sub_total_female_present;
						$sub_total_male_absent=$row['total_absent_male'];
						$sub_total_female_absent=$row['total_absent_female'];
						$sub_total_absent=$sub_total_male_absent+$sub_total_female_absent;
						$sub_total_present_male_in_per=number_format(($sub_tot_male_present/$sub_tot_male)*100,2);
						$sub_total_present_female_in_per=number_format(($sub_total_female_present/$sub_total_female)*100,2);
						$sub_total_present_in_per=number_format(($sub_total_present/$sub_total_strength)*100,2);
						$sub_total_absent_male_in_per=number_format(($sub_tot_male_present/$sub_tot_male)*100,2);
						$sub_total_absent_male_in_per=number_format(($sub_total_male_absent/$sub_tot_male)*100,2);
						$sub_total_absent_female_in_per=number_format(($sub_total_female_absent/$sub_total_female)*100,2);
						$sub_total_absent_in_per=number_format(($sub_total_absent/$sub_total_strength)*100,2);	
					}
					
				
						
			if($sl%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';
			
					
//======================================================sohel=======================================================================


	if($order_by_id==0){ $data_field=$designation_chart[$row[designation_id]][custom_designation]; $chack_arr=$row[designation_id];}
	else if($order_by_id==1){$data_field=$department_details[$row[department_id]][department_name]; $chack_arr=$row[department_id];}
	else if($order_by_id==2){$data_field=$section_details[$row[section_id]][section_name]; $chack_arr=$row[section_id];}
	else if($order_by_id==3){$data_field=$subsection_details[$row[subsection_id]][subsection_name]; $chack_arr=$row[subsection_id];}    
	else if($order_by_id==4){$data_field=$designation_chart[$row[designation_id]][custom_designation]; $chack_arr=$row[designation_id];} 
	else {$data_field=$designation_chart[$row[designation_id]][custom_designation]; $chack_arr=$row[designation_id];}
		
					
	
	?>  
				<tr style="font-size:11px">
                        <td><? echo $sl;?></td>
                        <td></td>
                        <td align=""><? echo $data_field; //echo $designation_chart[$row['designation_id']]['custom_designation'];?></td>
                        <td align="right"><? echo $row['total_male'];?></td>
                        <td align="right"><? echo $row['total_female'];?></td>
                        <td align="right"><? echo $total_strength=($row['total_female']+$row['total_male']);?></td>
                        <td align="right"><? echo $row['total_present_male']; ?></td>
                        <td align="right"><? echo $row['total_present_female']; ?></td>
                        <td align="right"><? echo $total_strength_present=($row['total_present_male']+$row['total_present_female']);?></td>
                        <td align="right"><? echo $row['total_absent_male'];?></td>
                        <td align="right"><? echo $row['total_absent_female'];?></td>
                        <td align="right"><? echo $total_strength_absent=($row['total_absent_male']+$row['total_absent_female']);?></td>
                        <td align="right"><? echo  $male_per=number_format(($row['total_present_male']/$row['total_male'])*100,2); ?></td>
                        <td align="right"><? echo  $female_per=number_format(($row['total_present_female']/$row['total_female'])*100,2); ?></td>
                        <td align="right"><? echo  number_format(($total_strength_present/$total_strength)*100,2); ?></td>
                        <td align="right"><? echo  $male_per_abs=number_format(($row['total_absent_male']/$row['total_male'])*100,2); ?></td>
                        <td align="right"><? echo  $female_per_abs=number_format(($row['total_absent_female']/$row['total_male'])*100,2); ?></td>
                        <td align="right"><? echo  number_format(($total_strength_absent/$total_strength)*100,2); ?></td>

                </tr>
                
                 
<?					     
					            	
	
	
	
	$sl++;
	//grand total start
						$grand_total_male+=$row['total_male'];
						$grand_total_female+=$row['total_female'];	
						$grand_total_strength+=$row['total_male']+$row['total_female'];
						$grand_total_male_present+=$row['total_present_male'];	
						$grand_total_female_present+=$row['total_present_female'];
						$grand_total_present+=$row['total_present_male']+$row['total_present_female'];
						$grand_total_absent_male+=$row['total_absent_male'];
						$grand_total_absent_female+=$row['total_absent_female'];
						$grand_total_absent+=$row['total_absent_male']+$row['total_absent_female'];
						$grand_total_male_present_per=number_format(($grand_total_male_present/$grand_total_strength)*100,2);
						$grand_total_female_present_per=number_format(($grand_total_female_present/$grand_total_strength)*100,2);
						$grand_total_present_in_per=number_format(($grand_total_present/$grand_total_strength)*100,2);
						$grand_total_male_absent_in_per=number_format(($grand_total_absent_male/$grand_total_male)*100,2);
						$grand_total_female_absent_in_per=number_format(($grand_total_absent_female/$grand_total_female)*100,2);
						$grand_total_absent_in_per=number_format(($grand_total_absent/$grand_total_strength)*100,2);
 
					
						if(in_array($chack_arr,$desig_arr)){ 
							$present_male_ratio[$chack_arr]['pre_male'] += $male_per;
							$present_female_ratio[$chack_arr]['pre_female'] += $female_per;
							
							$present_ratio[$chack_arr]['total_pre'] += ($total_strength_present/$total_strength)*100;
							$absent_ratio[$chack_arr]['total_abs'] += ($total_strength_absent/$total_strength)*100; 
							
							$absent_male_ratio[$chack_arr]['abs_male'] += $male_per_abs;
							$absent_female_ratio[$chack_arr]['abs_female'] += $female_per_abs;
							$desig_arr_count[$chack_arr][] = $chack_arr;
						}else{ 
							 
							$present_male_ratio[$chack_arr]['pre_male'] = $male_per;
							$present_female_ratio[$chack_arr]['pre_female'] = $female_per;
							
							$present_ratio[$chack_arr]['total_pre'] = ($total_strength_present/$total_strength)*100;
							$absent_ratio[$chack_arr]['total_abs'] = ($total_strength_absent/$total_strength)*100; 

							$absent_male_ratio[$chack_arr]['abs_male'] = $male_per_abs;
							$absent_female_ratio[$chack_arr]['abs_female'] = $female_per_abs;
							$desig_arr[$chack_arr] = $chack_arr;
							$desig_arr_count[$chack_arr][] = $chack_arr;
						}
					

}
						
?>    
		
        				<tr bgcolor="#CCCCCC">
							<td colspan="3"><b>Sub Total :</b></td>
                            <td align="right"><b><? echo $sub_tot_male; ?></b></td>
                            <td align="right"><b><? echo $sub_total_female; ?></b></td>
                            <td align="right"><b><? echo $sub_total_strength; ?></b></td>
                            <td align="right"><b><? echo $sub_tot_male_present; ?></b></td>
                            <td align="right"><b><? echo $sub_total_female_present; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present; ?></b></td>
                            <td align="right"><b><? echo $sub_total_male_absent; ?></b></td>
                            <td align="right"><b><? echo $sub_total_female_absent; ?></b></td>
                            <td align="right"><b><? echo $sub_total_absent; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present_male_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present_female_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_present_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_absent_male_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_absent_female_in_per; ?></b></td>
                            <td align="right"><b><? echo $sub_total_absent_in_per; ?></b></td>
                           
                            
						</tr>
        
        <tr bgcolor="#CCCCCC">
            <td colspan="3">Grand Total</td> 
            <td align="right"><b><? echo $grand_total_male; ?></b></td>
            <td align="right"><b><? echo $grand_total_female; ?></b></td>
            <td align="right"><b><? echo $grand_total_strength; ?></b></td>
            <td align="right"><b><? echo $grand_total_male_present;?></b></td>
            <td align="right"><b><? echo $grand_total_female_present;?></b></td>
            <td align="right"><b><? echo $grand_total_present;?></b></td>
            <td align="right"><b><? echo $grand_total_absent_male;?></b></td>
            <td align="right"><b><? echo $grand_total_absent_female;?></b></td>
            <td align="right"><b><? echo $grand_total_absent;?></b></td>
            <td align="right"><b><? echo $grand_total_male_present_per;?></b></td>
            <td align="right"><b><? echo $grand_total_female_present_per;?></b></td>
            <td align="right"><b><? echo $grand_total_present_in_per;?></b></td>
            <td align="right"><b><? echo $grand_total_male_absent_in_per;?></b></td>
            <td align="right"><b><? echo $grand_total_female_absent_in_per;?></b></td>
            <td align="right"><b><? echo $grand_total_absent_in_per;?></b></td>
        </tr>

    </table><br />

    <!--second table-->
  
<table cellpadding="0" cellspacing="0" border="1" width="600" style="border:1px solid #000; font-size:11px" class="rpt_table" rules="all">       
<thead>
    <tr>
        <th colspan="8"><font size="+1">Overall Division Wise  Daily Attendance Ratio</font></th>
   </tr>
    <tr class="tbl_top">
        <th width="20" rowspan="2"><b>SL</b></th>
        <th width="100" rowspan="2"><b><? echo $header_field; ?></b></th>
        <th width="120" colspan="2"><b>Present</b></th>
        <th width="" rowspan="2"><b>Total Present %</b></th>
        <th width="120" colspan="2"><b>Absent</b></th>
        <th width="" rowspan="2"><b>Total Absent %</b></th>
    </tr>     
    <tr>
        <th align="center">Male</th>
        <th align="center">Female</th>
        <th align="center">Male</th>
        <th align="center">Female</th>
   </tr>
   <?php
   
   
   $small_print .=' <table cellpadding="0" cellspacing="0" border="1" width="600" style="border:1px solid #000; font-size:11px" class="rpt_table" rules="all">
   		<thead> 
		<tr>
        	<th colspan="8"><font size="+1">Overall Division Wise  Daily Attendance Ratio</font></th>
   		</tr>	
		<tr class="tbl_top">
			<th width="20" rowspan="2"><b>SL</b></th>
			<th width="100" rowspan="2"><b>'. $header_field .'</b></th>
			<th width="120" colspan="2"><b>Present</b></th>
			<th width="" rowspan="2"><b>Total Present %</b></th>
			<th width="120" colspan="2"><b>Absent</b></th>
			<th width="" rowspan="2"><b>Total Absent %</b></th>
		</tr>
		<tr>
			<th align="center">Male</th>
			<th align="center">Female</th>
			<th align="center">Male</th>
			<th align="center">Female</th>
		</tr>  ';
  
   
   
   
   $i=0; 
   foreach($desig_arr as $value)
   {
       
    if($order_by_id==0){ $data_field_sum=$designation_chart[$value][custom_designation]; }
    else if($order_by_id==1){$data_field_sum=$department_details[$value][department_name]; }
    else if($order_by_id==2){$data_field_sum=$section_details[$value][section_name]; }
    else if($order_by_id==3){$data_field_sum=$subsection_details[$value][subsection_name]; }    
    else if($order_by_id==4){$data_field_sum=$designation_chart[$value][custom_designation]; } 
    else {$data_field_sum=$designation_chart[$value][custom_designation]; }
                    
       ?>
       <tr>
            <td align="center"><? echo $i; ?></td>
            <td align="center"><? echo $data_field_sum;//echo $designation_chart[$value]['custom_designation']; ?></td>
            <td align="center"><? echo $per_mail= number_format( ( $present_male_ratio[$value]['pre_male']/count($desig_arr_count[$value]) ),2 ); ?></td>
            <td align="center"><? echo $per_femail= number_format( ( $present_female_ratio[$value]['pre_female']/count($desig_arr_count[$value]) ),2 ); ?></td>
            
            <td align="center"><? echo $total_per= number_format( ( $present_ratio[$value]['total_pre']/count($desig_arr_count[$value]) ),2 ); ?></td>
            
            <td align="center"><? echo $absent_mail= number_format( ( $absent_male_ratio[$value]['abs_male']/count($desig_arr_count[$value]) ),2 ); ?></td>
            <td align="center"><? echo $absent_femail=number_format( ( $absent_female_ratio[$value]['abs_female']/count($desig_arr_count[$value]) ),2 ); ?></td>
            
            <td align="center"><? echo $total_absent=number_format( ( $absent_ratio[$value]['total_abs']/count($desig_arr_count[$value]) ),2 ); ?></td>
        </tr>
       <?
	   
	   $small_print .='<tr>
			<td align="center">'. $i.'</td>
			<td align="center">'. $data_field_sum .'</td>
			 <td align="center">'. $per_mail .'</td>
            <td align="center">'. $per_femail .'</td>
            <td align="center">'.$total_per .'</td>
            <td align="center">'. $absent_mail .'</td>
            <td align="center">'. $absent_femail .'</td>
            <td align="center">'. $total_absent .'</td>
        </tr>';
	   
       $i++;
   }
   ?> 
</thead>
</table>	
	<?
	$small_print .='</thead>
        			</table>';
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name"."####"."$small_print";		
	exit();
	

	
}

?>
