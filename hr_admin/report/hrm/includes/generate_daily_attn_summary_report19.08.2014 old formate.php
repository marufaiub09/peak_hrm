<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
require_once('../../../../includes/PHPMailerAutoload.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

 	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
 
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
		$location_idar[] = $row['id'];
	}

	$sql = "SELECT * FROM lib_division";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
		$division_idar[] = $row['id'];
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
		$department_idar[] = $row['id'];
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
		$section_idar[] = $row['id'];
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
		$subsection_idar[] = $row['id'];
	}
	
	$sql = "SELECT * FROM lib_designation";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
		$designation_idar[] = $row['id'];
	}

	//level
	$sql = "SELECT * FROM lib_designation";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$designation_chart_level = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$designation_chart_level[$row['level']] = mysql_real_escape_string($row['system_designation']);
		}
	
//print_r($designation_chart_level);die; 
/*//Designation array
$sql = "SELECT id,custom_designation FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result )) 
{
$designation_chart[$row['id']] = $row['custom_designation'];
}*/
//$depertment_wise_orderby_arr = array(1=>"Designation Level",2=>"Department Wise",3=>"Designation Wise",4=>"Division Wise",5=>"Section Wise",6=>"Employee Code",7=>"Employee ID");
 
// report mail 12.05.2014


if( $action == "mail_full_report" ) 
{
	$htmldata=explode(".",$htmldata);
	$html=file_get_contents('tmp_report_file/'.$htmldata[0].".txt", true);
	echo send_mail_mailer($email_address,"Daily attendace summary on ".$txt_date,$html); 
}

//Daily Attendance Summary Report
if( $action == "daily_attn_summary_report" ) {
	
	if($order_by_id==0){$orderby_fild="e.designation_level"; $header_field="Designation Level"; $orderby_bu="desig_level_id";}
	else if($order_by_id==2){$orderby_fild="a.department_id"; $header_field="Department"; $orderby_bu="department_id";}
	else if($order_by_id==3){$orderby_fild="a.designation_id"; $header_field="Designation"; $orderby_bu="designation_id";}
	else if($order_by_id==4){$orderby_fild="a.division_id"; $header_field="Division"; $orderby_bu="division_id";}
	else if($order_by_id==5){$orderby_fild="a.section_id"; $header_field="Section"; $orderby_bu="section_id";}
	else if($order_by_id==6){$orderby_fild="a.subsection_id"; $header_field="Subsection"; $orderby_bu="subsection_id";} 
	else {$orderby_fild="e.designation_level"; $header_field="Designation Level"; $orderby_bu="desig_level_id";}
	
	if ($company_id==0){ $company="";}else{$company="company_id='$company_id'";}
	if ($location_id==0 || $location_id=='') $location=""; else $location="and locaion_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division=""; else $division="and division_id in ($division_id)";
	if ($section_id==0 || $section_id=='') $section=""; else $section="and section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection=""; else $subsection="and subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation=""; else $designation="and designation_id in ($designation_id)";
	if ($department_id==0 || $department_id=='') $department=""; else	$department=" and  department_id in ($department_id)";
	if ($cbo_emp_category=='') $category ="and emp_catg_id in ($employee_category_index)"; else $category ="and emp_catg_id='$cbo_emp_category'";
	
	$sql22="SELECT *,sum(no_of_post) as no_of_post FROM budgeted_manpower_setup WHERE $company $location $division $department $section $subsection $designation $category  group by $orderby_bu order by $orderby_bu";
	//echo $sql22."<br>";
	$query_result = mysql_db_query( $DB, $sql22 ) or die( $sql22 . "<br />" . mysql_error() );
	$em_post_arr=array();
	while ($selectResult = mysql_fetch_array($query_result))
	{
		$em_post_arr[$selectResult[$orderby_bu]]=$selectResult['no_of_post'];
	}

	//echo $cbo_rpt_type;die;
	$cbo_company=$company_id;
	$cbo_location=$location_id;
	if ($cbo_emp_category=='') $category ="and e.category in ($employee_category_index)"; else $category ="and e.category='$cbo_emp_category'";
	if ($location_id==0 || $location_id=='') $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";	
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else	$department_id=" and  a.department_id in ($department_id)";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and e.salary_type_entitled='$cbo_salary_based'";
	
	$date_part=convert_to_mysql_date($txt_date);
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="$orderby_fild,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,$orderby_fild,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,$orderby_fild,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,$orderby_fild,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,$orderby_fild,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,$orderby_fild,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,$orderby_fild,";}
 		
	//echo $order_by_id; die;	
	/*
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-1);}	 
	else if($order_by_id==1){
		
		//to all order
		$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
		if($group_by_id==0){$groupby="e.designation_level,";}
		else if($group_by_id==1){$status_com=1;$groupby="e.company_id,e.designation_level,";}
		else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="e.company_id,e.location_id,e.designation_level,";}
		else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="e.company_id,e.location_id,e.division_id,e.designation_level,";}
		else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.designation_level,";}
		else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,e.designation_level,";}
		else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.section_id,e.subsection_id,e.designation_level,";}
		//end
		//$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="e.company_id,e.location_id,e.division_id,e.department_id,e.designation_level,";
		
		}	 
	 else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-17)." CAST(e.emp_code as SIGNED)";}	 
	 else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-17)." CAST(right(trim(e.id_card_no), 5)as SIGNED)";}	 
	//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
	*/
	
$dynamic_groupby = substr($groupby,0, -1);//this line is used to remove last comma and dynamic group by." CAST(e.designation_level as SIGNED)"
$orderby="order by ".substr($groupby,0,-1);

 ob_start();

if($cbo_rpt_type==1){
	?>
	<table width="1100" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
	<?
	 $small_print .='<table width="1100" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">';
	 
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$holiday=0;$weekend=0;$total_leave=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_manpower=0;$new_join=0;$weekday_tot=0;				
	
	$section_total_new_join=0;$section_total_manpower_dep=0;$section_total_movement=0;$section_total_present=0;$section_total_absent=0;$section_total_late=0;$section_total_total_leave=0;	
	
	$department_total_new_join=0;$department_total_manpower_dep=0;$department_total_movement=0;$department_total_present=0;$department_total_absent=0;$department_late=0;$department_total_total_leave=0;	
	
	$division_total_new_join=0;$division_total_manpower_dep=0;$division_total_movement=0;$division_total_present=0;$division_total_absent=0;$division_late=0;$division_total_total_leave=0;	
	
	$location_total_new_join=0;$location_total_manpower_dep=0;$location_total_movement=0;$location_total_present=0;$location_total_absent=0;$location_late=0;$location_total_total_leave=0;				
	
	$grand_total_new_join=0;$grand_total_manpower_dep=0;$grand_total_movement=0;$grand_total_present=0;$grand_total_absent=0;$grand_late=0;$grand_total_total_leave=0;$grand_total_week_day=0;
	
	$sl=0;
	$ct=0;
	
		 $sql_emp="SELECT 
			a.company_id,a.location_id,a.division_id,a.department_id, a.section_id,a.subsection_id,a.designation_id,a.status,e.designation_level, 
			count(CASE WHEN a.status ='P' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'present',
			count(CASE WHEN a.status ='P' and is_regular_day=0 and a.attnd_date='$date_part' THEN a.emp_code END) AS 'gn_present',
			count(CASE WHEN a.status ='A' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'absent', 
			count(CASE WHEN a.status ='D' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'late', 
			count(CASE WHEN a.status ='GH' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'g_holiday', 
			count(CASE WHEN a.status ='FH' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'f_holiday',
			count(CASE WHEN a.status ='CH' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'c_holiday', 
			count(CASE WHEN a.status ='H' and a.attnd_date='$date_part'THEN a.emp_code END) AS 'h_holiday', 
			count(CASE WHEN a.status ='W' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'weekday', 
			count(CASE WHEN a.status ='MR' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'movement', 
			count(CASE WHEN a.status ='CL' and a.attnd_date='$date_part'THEN a.emp_code END) AS 'cl', 
			count(CASE WHEN a.status ='FL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'fl', 
			count(CASE WHEN a.status ='EL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'el', 
			count(CASE WHEN a.status ='SL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'sl', 
			count(CASE WHEN a.status ='ML' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'ml', 
			count(CASE WHEN a.status ='SpL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'spl', 
			count(CASE WHEN a.status ='LWP' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'lwp', 
			count(CASE WHEN a.status ='SP' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'sp',
			count(CASE WHEN a.attnd_date='$date_part' and e.joining_date='$date_part' THEN a.emp_code END) AS 'new_join' 
			FROM 
			hrm_attendance a,hrm_employee e 
			where a.emp_code=e.emp_code and a.attnd_date='$date_part' $company_id $category  $location_id  $division_id $department_id $section_id $subsection_id $designation_id $salary_based group by $dynamic_groupby $orderby";	

//a.section_id, this is cut fromgroup by which is listed after department_id $orderby

//echo $sql_emp;	

// count(CASE WHENe.status_active and a.attnd_date like '$date_part%' THEN a.emp_code END) AS 'sp' this line add new join emp	

	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{
		$weekday_tot=$row_emp[weekday];
		//$weekday_grand_tot+= $weekday_tot;
		//$weekday_grand_total+= $weekday_grand_tot;
		$movement=$row_emp[movement];
		$present=$row_emp[present];
		$late=$row_emp[late];
		$absent=$row_emp[absent];
		$total_leave=$row_emp[cl]+$row_emp[sl]+$row_emp[el]+$row_emp[ml]+$row_emp[spl]+$row_emp[fl]+$row_emp[lwp]+$row_emp[sp];
		$total_manpower=$movement+$present+$late+$absent+$total_leave+$weekday_tot; //+$movement;
	 	$new_join=$row_emp[new_join];
		$sl++;
		
		   //start header print---------------------------------//
		if($sl==1)
		{
			$location_arr[$row_emp[location_id]]=$row_emp[location_id];
			$division_arr[$row_emp[division_id]]=$row_emp[division_id];
			$department_arr[$row_emp[department_id]]=$row_emp[department_id];
			$section_arr[$row_emp[section_id]]=$row_emp[section_id];
			$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
			
			?> 
			<thead>
                <tr>
                    <td colspan="14" align="center"><font size="+1"><? echo $company_details[$row_emp['company_id']]['company_name']; ?></font><br />
                    Address: Plot No # <? echo $company_details[$row_emp['company_id']]["plot_no"]; ?>, Level No # <? echo $company_details[$row_emp['company_id']]["level_no"]; ?>, Block No # <? echo $company_details[$row_emp['company_id']]["block_no"]; ?>, Road No # <? echo $company_details[$row_emp['company_id']]["road_no"]; ?>, <? echo $company_details[$row_emp['company_id']]["city"]; echo $company_details[$row_emp['company_id']]["country_name"];?><br />
                    <span style="font-size:18px;"> Daily Attendance Summary as on <? echo convert_to_mysql_date($date_part);?> </span>          
                    </td>
                </tr>
                <tr>
                    <th width='20'><b>SL</b></th>
                    <th width='130'><b><? echo $header_field; ?></b></th>
                    <th width='70'><b>Sanction</b></th>
                    <th width='80'><b>Newly Joined</b></th>
                    <th width='70'><b>Running Manpower</b></th>
                    <th width='70'><b>Movement</b></th>
                    <th width='100'><b>Present(No Late)</b></th>
                    <th width='70'><b>Present(Late)</b></th>
                    <th width='100'><b>Total Present</b></th>
                    <th width='70'><b>Absent</b></th>
                    <th width='70'><b>Leave</b></th>
                    <th width='70'><b>Weekend</b></th>
                    <th width='60'><b>Short/Surplus</b></th>
                    <th width='60'><b>Remarks</b></th>
                </tr>
                </thead>
                <?
                $small_print .='<thead>
                <tr>
					<td colspan="13" align="center"><font size="+1">'.$company_details[$row_emp['company_id']]['company_name'].'</font><br />
					Address: Plot No # '.$company_details[$row_emp['company_id']]["plot_no"].', Level No # '.$company_details[$row_emp['company_id']]["level_no"].', Block No # '.$company_details[$row_emp['company_id']]["block_no"].', Road No #'.$company_details[$row_emp['company_id']]["road_no"].', '.$company_details[$row_emp['company_id']]["city"].$company_details[$row_emp['company_id']]["country_name"].'<br />
					<span style="font-size:18px;"> Daily Attendance Summary as on '.convert_to_mysql_date($date_part).' </span>          
					</td>
                </tr>
                <tr>
					<th width="130"><b>'.$header_field.'</b></th>
					<th width="70"><b>Sanction</b></th>
					<th width="70"><b>Running Manpower</b></th>
					<th width="100"><b>Total Present</b></th>
					<th width="70"><b>Absent</b></th>
					<th width="70"><b>Leave</b></th>
					<th width="70"><b>Weekend</b></th>
					<th width="60"><b>Short/Surplus</b></th>
					<th width="60"><b>Remarks</b></th>
                </tr>
			</thead>';
		}//end if condition of header print
		  
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row_emp[section_id]]=$row_emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row_emp[department_id]]=$row_emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row_emp[section_id]]=$row_emp[section_id];
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row_emp[division_id]]=$row_emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row_emp[department_id]]=$row_emp[department_id];
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row_emp[location_id]]=$row_emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row_emp[division_id]]=$row_emp[division_id];
						$department_arr[$row_emp[department_id]]=$row_emp[department_id];
						$section_arr[$row_emp[section_id]]=$row_emp[section_id];
						$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		}
		
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td width='' colspan="2"><b> Subsection Total</b></td>
							<td width='' align='right'> <?  ?></td>
							<td width='' align='right'> <? echo $subsection_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $subsection_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $subsection_total_movement; ?></td>
							<td width='' align='right'> <? echo $subsection_total_present; ?></td>						
							<td width='' align='right'> <? echo $subsection_total_late; ?></td>
                            <td width='' align='right'> <? echo ($subsection_total_present+$subsection_total_late); ?></td>
                            <td width='' align='right'> <? echo $subsection_total_absent; ?></td>	
							<td width='' align='right'> <? echo $subsection_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $subsection_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($subsection_total_movement*100)/$subsection_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($subsection_total_present*100)/$subsection_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($subsection_total_late*100)/$subsection_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($subsection_total_present*100)/$subsection_total_manpower_dep,2)+round(($subsection_total_late*100)/$subsection_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($subsection_total_absent*100)/$subsection_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($subsection_total_total_leave*100)/$subsection_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($subsection_total_week_day*100)/$subsection_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr>
						<?
					$subsec_tot_per=($subsection_total_present+$subsection_total_late);
					$in_subsec_tot_per=round(($subsection_total_present*100)/$subsection_total_manpower_dep,2)+round(($subsection_total_late*100)/$subsection_total_manpower_dep,2);
						$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td width=""><b> Subsection Total</b></td>
							<td width="" align="right"> <?  ?></td>
							<td width="" align="right"> '.$subsection_total_manpower_dep.'</td>
                            <td width="" align="right">'. $subsec_tot_per .'</td>
                            <td width="" align="right"> '. $subsection_total_absent.'</td>	
							<td width="" align="right">'.$subsection_total_total_leave.'</td>
                            <td width="" align="right">'.$subsection_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right">'.$in_subsec_tot_per.'%</td>
                            <td width="" align="right">'. round(($subsection_total_absent*100)/$subsection_total_manpower_dep,2).'%</td>
							<td width="" align="right">'.round(($subsection_total_total_leave*100)/$subsection_total_manpower_dep,2).'%</td>
                            <td width="" align="right">'.round(($subsection_total_week_day*100)/$subsection_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
				
			$subsection_total_new_join=0;$subsection_total_manpower_dep=0;$subsection_total_movement=0;$subsection_total_present=0;$subsection_total_absent=0;$subsection_total_late=0;$subsection_total_total_leave=0;$subsection_total_week_day=0;	

		
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td width='' colspan="2"><b>SectionTotal</b></td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo $section_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $section_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $section_total_movement; ?></td>
							<td width='' align='right'> <? echo $section_total_present; ?></td>						
							<td width='' align='right'> <? echo $section_total_late; ?></td>
                            <td width='' align='right'> <? echo $section_total_present+$section_total_late; ?></td>	
                            <td width='' align='right'> <? echo $section_total_absent; ?></td>	
							<td width='' align='right'> <? echo $section_total_total_leave; ?></td>	
                            <td width='' align='right'> <? echo $section_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($section_total_movement*100)/$section_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($section_total_present*100)/$section_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($section_total_late*100)/$section_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($section_total_present*100)/$section_total_manpower_dep,2)+round(($section_total_late*100)/$section_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($section_total_absent*100)/$section_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($section_total_total_leave*100)/$section_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($section_total_week_day*100)/$section_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$sec_tot_tot_per=$section_total_present+$section_total_late;
						$in_sec_tot_tot_per=round(($section_total_present*100)/$section_total_manpower_dep,2)+round(($section_total_late*100)/$section_total_manpower_dep,2);
						$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td width=""><b>SectionTotal</b></td>
							<td width="">&nbsp;</td>
							<td width="" align="right"> '.$section_total_manpower_dep.'</td>
                            <td width="" align="right">'. $sec_tot_tot_per .'</td>	
                            <td width="" align="right">'.$section_total_absent.'</td>	
							<td width="" align="right">'.$section_total_total_leave.'</td>	
                            <td width="" align="right"> '.$section_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right">'. $in_sec_tot_tot_per .'%</td>
                            <td width="" align="right"> '.round(($section_total_absent*100)/$section_total_manpower_dep,2).'</td>
							<td width="" align="right">'. round(($section_total_total_leave*100)/$section_total_manpower_dep,2).'%</td>
                            <td width="" align="right">'. round(($section_total_week_day*100)/$section_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
				
			$section_total_new_join=0;$section_total_manpower_dep=0;$section_total_movement=0;$section_total_present=0;$section_total_absent=0;$section_total_late=0;$section_total_total_leave=0;$section_total_week_day=0;
						
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td align="center" colspan="2">Department Total</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo $department_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $department_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $department_total_movement; ?></td>
							<td width='' align='right'> <? echo $department_total_present; ?></td>
                            <td width='' align='right'> <? echo $department_late; ?></td>
                            <td width='' align='right'> <? echo $department_total_present+$department_late; ?></td>						
							<td width='' align='right'> <? echo $department_total_absent; ?></td>	
							<td width='' align='right'> <? echo $department_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $department_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($department_total_movement*100)/$department_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($department_total_present*100)/$department_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($department_late*100)/$department_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($department_total_present*100)/$department_total_manpower_dep,2)+round(($department_late*100)/$department_total_manpower_dep,2); ?>%</td>	
                            <td width='' align='right'> <? echo round(($department_total_absent*100)/$department_total_manpower_dep,2); ?>%</td> 
							<td width='' align='right'> <? echo round(($department_total_total_leave*100)/$department_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($department_total_week_day*100)/$department_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						 $dep_tot__per=$department_total_present+$department_late;
						 $in_dep_tot__per=round(($department_total_present*100)/$department_total_manpower_dep,2)+round(($department_late*100)/$department_total_manpower_dep,2);
				$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center">Department Total</td>
							<td width="">&nbsp;</td>
							<td width="" align="right">'. $department_total_manpower_dep.'</td>
                            <td width="" align="right">'.$dep_tot__per.'</td>						
							<td width="" align="right">'. $department_total_absent.'</td>	
							<td width="" align="right"> '. $department_total_total_leave.'</td>
                            <td width="" align="right">'. $department_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right"> '.$in_dep_tot__per.'%</td>	
                            <td width="" align="right"> '. round(($department_total_absent*100)/$department_total_manpower_dep,2).'%</td> 
							<td width="" align="right"> '. round(($department_total_total_leave*100)/$department_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '. round(($department_total_week_day*100)/$department_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
				
		$department_total_new_join=0;$department_total_manpower_dep=0;$department_total_movement=0;$department_total_present=0;$department_total_absent=0;$department_late=0;$department_total_total_leave=0;$department_total_week_day=0;	
			
		
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td align="center" colspan="2" >Division Total</td>
				
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo $division_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $division_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $division_total_movement; ?></td>
							<td width='' align='right'> <? echo $division_total_present; ?></td>						
							<td width='' align='right'> <? echo $division_late; ?></td>
                            <td width='' align='right'> <? echo $division_total_present+$division_late; ?></td>
                            <td width='' align='right'> <? echo $division_total_absent; ?></td>
							<td width='' align='right'> <? echo $division_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $division_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                          
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($division_total_movement*100)/$division_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($division_total_present*100)/$division_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($division_late*100)/$division_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($division_total_present*100)/$division_total_manpower_dep,2)+round(($division_late*100)/$division_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($division_total_absent*100)/$division_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($division_total_total_leave*100)/$division_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($division_total_week_day*100)/$division_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$div_tot_per=$division_total_present+$division_late;
						 $in_div_tot_per=round(($division_total_present*100)/$division_total_manpower_dep,2)+round(($division_late*100)/$division_total_manpower_dep,2);
					$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center" >Division Total</td>
							<td width="">&nbsp;</td>
							<td width="" align="right">'.$division_total_manpower_dep.'</td>
                            <td width="" align="right">'.$div_tot_per.'</td>
                            <td width="" align="right"> '. $division_total_absent.'</td>
							<td width="" align="right">'. $division_total_total_leave.'</td>
                            <td width="" align="right"> '. $division_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right">'.$in_div_tot_per.'%</td>
                            <td width="" align="right"> '.round(($division_total_absent*100)/$division_total_manpower_dep,2).'%</td>
							<td width="" align="right"> '. round(($division_total_total_leave*100)/$division_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '. round(($division_total_week_day*100)/$division_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
			
	$division_total_new_join=0;$division_total_manpower_dep=0;$division_total_movement=0;$division_total_present=0;$division_total_absent=0;$division_late=0;$division_total_total_leave=0;$division_total_week_day=0;			
			
 			
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td align="center" colspan="2">Location Total</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo $location_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $location_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $location_total_movement; ?></td>
							<td width='' align='right'> <? echo $location_total_present; ?></td>						
							<td width='' align='right'> <? echo $location_late; ?></td>
                            <td width='' align='right'> <? echo $location_total_present+$location_late; ?></td>
                            <td width='' align='right'> <? echo $location_total_absent; ?></td>
							<td width='' align='right'> <? echo $location_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $location_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                            
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($location_total_movement*100)/$location_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($location_total_present*100)/$location_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($location_late*100)/$location_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($location_total_present*100)/$location_total_manpower_dep,2)+round(($location_late*100)/$location_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($location_total_absent*100)/$location_total_manpower_dep,2); ?>%</td> 
							<td width='' align='right'> <? echo round(($location_total_total_leave*100)/$location_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($location_total_week_day*100)/$location_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$loc_tot_per=$location_total_present+$location_late;
						 $in_loc_tot_per=round(($location_total_present*100)/$location_total_manpower_dep,2)+round(($location_late*100)/$location_total_manpower_dep,2);
						$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center">Location Total</td>
							<td width="">&nbsp;</td>
							<td width="" align="right">'. $location_total_manpower_dep.'</td>
                            <td width="" align="right"> '. $loc_tot_per .'</td>
                            <td width="" align="right"> '. $location_total_absent.'</td>
							<td width="" align="right"> '.$location_total_total_leave.'</td>
                            <td width="" align="right"> '. $location_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right"> '.$in_loc_tot_per.'%</td>
                            <td width="" align="right"> '. round(($location_total_absent*100)/$location_total_manpower_dep,2).'%</td> 
							<td width="" align="right"> '. round(($location_total_total_leave*100)/$location_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '.round(($location_total_week_day*100)/$location_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
						
	$location_total_new_join=0;$location_total_manpower_dep=0;$location_total_movement=0;$location_total_present=0;$location_total_absent=0;$location_late=0;$location_total_total_leave=0;$location_total_week_day=0;				
		
 			
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
			}
	

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr style="font-weight:bold; font-size:14px"><td colspan="14"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
				$small_print .='<tr style="font-weight:bold; font-size:14px"><td colspan="13"><b>'.substr($c_part_1,0,-1).'</b></td></tr>';
			}		
		 
 		}
		$ct++;
 			
		/*if ($ct%2==0) $bgcolor="#EFEFEF"; 
		else $bgcolor="#FFFFFF";*/
		//$total_leave=$cl+$sl+$ml+$el+$lwp+$spl;
//===================================================sohel===============================================================
		
	//$compa_id=$row_emp[company_id];  $loc_id=$row_emp[location_id]; $div_id=$row_emp[division_id]; $dep_id=$row_emp[department_id];
	//$sec_id=$row_emp[section_id]; $sub_sec_id=$row_emp[subsection_id]; $dg_id=$row_emp[attn_designation_id]; 
	 
	/*if ($cbo_emp_category=="") $category=" emp_catg_id in ($employee_category_index)"; else $category="emp_catg_id='$cbo_emp_category'";
	if ($compa_id==0) $company_id =""; else $company_id=" and company_id='$compa_id'";
	if ($loc_id==0) $loca_id =""; else $loca_id ="and locaion_id='$loc_id'";
	if ($div_id==0) $divit_id =""; else $divit_id ="and division_id='$div_id'";
	if ($dep_id==0) $deper_id =""; else $deper_id ="and department_id='$dep_id'";
	if ($sec_id==0) $sect_id =""; else $sect_id ="and section_id='$sec_id'";
	if ($sub_sec_id==0) $sub_sec_id =""; else $sub_sec_id ="and subsection_id='$sub_sec_id'";
	if ($dg_id==0) $desig_id =""; else $desig_id ="and designation_id='$dg_id'";*/
	
	/*$em_post_arr[$selectResult[$orderby_bu]];
	$em_post_total+=  $em_post;	
	$short_surplus=0;		
	$short_surplus=($em_post)-($total_manpower);*/
	
	
	//if($em_post>$total_manpower){$sine="(+)";} else if($em_post<$total_manpower){$sine="(-)";}
	//if ($dg_id==0) $designation_id =""; else $designation_id ="and designation_id='$dg_id'";
		
//======================================================sohel=======================================================================

	if($order_by_id==0){ $data_field=$designation_chart_level[$row_emp[designation_level]]; $data_field_bu=$row_emp[designation_level];}
	else if($order_by_id==2){$data_field=$department_details[$row_emp[department_id]]; $data_field_bu=$row_emp[department_id];}
	else if($order_by_id==3){$data_field=$designation_chart[$row_emp[designation_id]]; $data_field_bu=$row_emp[designation_id];} 
	else if($order_by_id==4){$data_field=$division_details[$row_emp[division_id]]; $data_field_bu=$row_emp[division_id];} 
	else if($order_by_id==5){$data_field=$section_details[$row_emp[section_id]]; $data_field_bu=$row_emp[section_id];}
	else if($order_by_id==6){$data_field=$subsection_details[$row_emp[subsection_id]]; $data_field_bu=$row_emp[subsection_id];}  
	else {$data_field=$designation_chart_level[$row_emp[designation_level]]; $data_field_bu=$row_emp[designation_level];}
	
	$em_post=$em_post_arr[$data_field_bu];
	$em_post_total+= $em_post;	
	$short_surplus=0;		
	$short_surplus=($em_post)-($total_manpower);
	
	$tot_per=$present+$late;
	$small_print .='<tbody>	
	<tr style="font-weight:bold; font-size:14px">
		<td width="">'.$data_field.'</td>
		<td width="" align="right">'. $em_post.'</td>
		<td width="" align="right">'. $total_manpower .'</td>
		<td width="" align="right">'.$tot_per.'</td>
		<td width="" align="right">'. $absent.'</td>
		<td width="" align="right">'.$total_leave.'</td>	
		<td width="" align="right">'. $weekday_tot.'</td>
		<td width="" align="right">'. $short_surplus.'</td>
		<td width="">&nbsp;</td>
	</tr>
	</tbody>';	
    ?>
    <tbody>	
    <? //echo $order_by_id;die;
	//print_r($designation_chart_level[$row_emp[designation_level]]);die;
	
	 ?>
        	
    <tr style="font-weight:bold; font-size:14px">
        <td width=''><? echo $sl; ?></td>
        <td width=''>
			<? if($data_field=="") echo "No".$header_field." found";
			else echo $data_field;
			?>
        </td>
        <td width='' align='right'><? echo $em_post; ?></td>
        <td width='' align='right'><? echo $new_join; ?></td>						
        <td width='' align='right'><? echo $total_manpower;?></td>
        <td width='' align='right'><? echo $movement ;?></td>
        <td width='' align='right'><? echo $present; ?></td>
        <td width='' align='right'><? echo $late; ?></td>
        <td width='' align='right'><? echo $present+$late; ?></td>
        <td width='' align='right'><? echo $absent; ?></td>						
        <td width='' align='right'><? echo $total_leave; ?></td>
        <td width='' align='right'><? echo $weekday_tot; ?></td>
        <td width='' align='right'><? echo $short_surplus; ?></td>	
        <td width=''>&nbsp;</td>
    </tr>
        <?
		$ct++;
		//sub section 
		
		$subsection_total_week_day += $weekday_tot;
		$subsection_total_new_join += $new_join;
		$subsection_total_manpower_dep += $total_manpower;				
		$subsection_total_movement += $movement;
		$subsection_total_present += $present;
		$subsection_total_absent += $absent;
		$subsection_total_late += $late;
		$subsection_total_total_leave += $total_leave;
		
		//section 
		$section_total_week_day += $weekday_tot;
		$section_total_new_join += $new_join;
		$section_total_manpower_dep += $total_manpower;				
		$section_total_movement += $movement;
		$section_total_present += $present;
		$section_total_absent += $absent;
		$section_total_late += $late;
		$section_total_total_leave += $total_leave;
		
		//department 
		$department_total_week_day += $weekday_tot;
		$department_total_new_join += $new_join;
		$department_total_manpower_dep += $total_manpower;					
		$department_total_movement += $movement;
		$department_total_present += $present;
		$department_total_absent += $absent;
		$department_late += $late;
		$department_total_total_leave += $total_leave;
		
		//division 
		$division_total_week_day += $weekday_tot;		
		$division_total_new_join += $new_join;
		$division_total_manpower_dep += $total_manpower;				
		$division_total_movement += $movement;
		$division_total_present += $present;
		$division_total_absent += $absent;
		$division_late += $late;
		$division_total_total_leave += $total_leave;
		
		//Location 
		$location_total_week_day += $weekday_tot;			
		$location_total_new_join += $new_join;
		$location_total_manpower_dep += $total_manpower;				
		$location_total_movement += $movement;
		$location_total_present += $present;
		$location_total_absent += $absent;
		$location_late += $late;
		$location_total_total_leave += $total_leave;
		
		//grand total
		$grand_total_week_day += $weekday_tot;
		$grand_total_new_join+=  $new_join;
		$grand_total_manpower_dep+= $total_manpower;		
		$grand_total_movement+= $movement;
		$grand_total_present+= $present;
		$grand_total_absent+= $absent;
		$grand_late+= $late;
		$grand_total_total_leave += $total_leave;
				
	}//while loop end here 
	//$small_print .='</tbody>';
	?>
    
	</tbody>   
    <?
    	if($status_subsec==1)
		{
 			?><tr  style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td align="center" colspan="2">Subsection Total</td>
							<td width='' align='right'><? echo $em_post_total; ?></td>
							<td width='' align='right'> <? echo $subsection_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $subsection_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $subsection_total_movement; ?></td>
							<td width='' align='right'> <? echo $subsection_total_present; ?></td>						
							<td width='' align='right'> <? echo $subsection_total_late; ?></td>
                            <td width='' align='right'> <? echo $subsection_total_present+$subsection_total_late; ?></td>
                            <td width='' align='right'> <? echo $subsection_total_absent; ?></td>
							<td width='' align='right'> <? echo $subsection_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $subsection_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($subsection_total_movement*100)/$subsection_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($subsection_total_present*100)/$subsection_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($subsection_total_late*100)/$subsection_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($subsection_total_present*100)/$subsection_total_manpower_dep,2)+round(($subsection_total_late*100)/$subsection_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($subsection_total_absent*100)/$subsection_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($subsection_total_total_leave*100)/$subsection_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($subsection_total_week_day*100)/$subsection_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$subsec_grand_per=$subsection_total_present+$subsection_total_late;
						 $in_subsec_grand_per=round(($subsection_total_present*100)/$subsection_total_manpower_dep,2)+round(($subsection_total_late*100)/$subsection_total_manpower_dep,2);
						$small_print .='<tr  style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center">Subsection Total</td>
							<td width="" align="right">'.$em_post_total.'</td>
							<td width="" align="right"> '. $subsection_total_manpower_dep.'</td>
                            <td width="" align="right"> '. $subsec_grand_per.'</td>
                            <td width="" align="right"> '. $subsection_total_absent.'</td>
							<td width="" align="right"> '. $subsection_total_total_leave.'</td>
                            <td width="" align="right"> '. $subsection_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right"> '.$in_subsec_grand_per.'%</td>
                            <td width="" align="right"> '. round(($subsection_total_absent*100)/$subsection_total_manpower_dep,2).'%</td>
							<td width="" align="right">'. round(($subsection_total_total_leave*100)/$subsection_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '. round(($subsection_total_week_day*100)/$subsection_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
				$subsection_total_new_join=0;$subsection_total_manpower_dep=0;$subsection_total_movement=0;$subsection_total_present=0;$subsection_total_absent=0;$subsection_total_late=0;$subsection_total_total_leave=0;$subsection_total_week_day=0;	

		}
		
		if($status_sec==1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td align="center" colspan="2">Section Total</td>
							<td width='' align='right'><? echo $em_post_total; ?></td>
							<td width='' align='right'> <? echo $section_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $section_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $section_total_movement; ?></td>
							<td width='' align='right'> <? echo $section_total_present; ?></td>						
							<td width='' align='right'> <? echo $section_total_late; ?></td>
                            <td width='' align='right'> <? echo $section_total_present+$section_total_late; ?></td>	
                            <td width='' align='right'> <? echo $section_total_absent; ?></td>
							<td width='' align='right'> <? echo $section_total_total_leave; ?></td>	
                            <td width='' align='right'> <? echo $section_total_week_day; ?></td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($section_total_movement*100)/$section_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($section_total_present*100)/$section_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($section_total_late*100)/$section_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($section_total_present*100)/$section_total_manpower_dep,2)+round(($section_total_late*100)/$section_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($section_total_absent*100)/$section_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($section_total_total_leave*100)/$section_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($section_total_week_day*100)/$section_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$grand_sec_per=$section_total_present+$section_total_late;
						 $in_grand_sec_per=round(($section_total_present*100)/$section_total_manpower_dep,2)+round(($section_total_late*100)/$section_total_manpower_dep,2);
						$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center">Section Total</td>
							<td width="" align="right">'.$em_post_total.'</td>
							<td width="" align="right"> '.$section_total_manpower_dep.'</td>
                            <td width="" align="right"> '.$grand_sec_per.'</td>	
                            <td width="" align="right"> '.$section_total_absent.'</td>
							<td width="" align="right">'. $section_total_total_leave.'</td>	
                            <td width="" align="right"> '. $section_total_week_day.'</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right">'.$in_grand_sec_per.'%</td>
                            <td width="" align="right"> '. round(($section_total_absent*100)/$section_total_manpower_dep,2).'%</td>
							<td width="" align="right"> '.round(($section_total_total_leave*100)/$section_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '. round(($section_total_week_day*100)/$section_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
					
			$section_total_new_join=0;$section_total_manpower_dep=0;$section_total_movement=0;$section_total_present=0;$section_total_absent=0;$section_total_late=0;$section_total_total_leave=0;$section_total_week_day=0;
						
		}
		
		if($status_dept==1 )
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td align="center" colspan="2">Department Total</td>
							<td width='' align='right'><? echo $em_post_total; ?></td>
							<td width='' align='right'> <? echo $department_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $department_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $department_total_movement; ?></td>
							<td width='' align='right'> <? echo $department_total_present; ?></td>						
							<td width='' align='right'> <? echo $department_late; ?></td>
                            <td width='' align='right'> <? echo $department_total_present+$department_late; ?></td>
                            <td width='' align='right'> <? echo $department_total_absent; ?></td>	
							<td width='' align='right'> <? echo $department_total_total_leave; ?></td>	
                            <td width='' align='right'> <? echo $department_total_week_day; ?></td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($department_total_movement*100)/$department_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($department_total_present*100)/$department_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($department_late*100)/$department_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($department_total_present*100)/$department_total_manpower_dep,2)+round(($department_late*100)/$department_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($department_total_absent*100)/$department_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($department_total_total_leave*100)/$department_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($department_total_week_day*100)/$department_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$grand_dep_per=$department_total_present+$department_late;
						 $in_grand_dep_per=round(($department_total_present*100)/$department_total_manpower_dep,2)+round(($department_late*100)/$department_total_manpower_dep,2);
						$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center">Department Total</td>
							<td width="" align="right">'. $em_post_total.'</td>
							<td width="" align="right"> '.$department_total_manpower_dep.'</td>
                            <td width="" align="right"> '.$grand_dep_per .'</td>
                            <td width="" align="right"> '.$department_total_absent.'</td>	
							<td width="" align="right"> '. $department_total_total_leave.'</td>	
                            <td width="" align="right"> '. $department_total_week_day.'</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right"> '.$in_grand_dep_per.'%</td>
                            <td width="" align="right"> '. round(($department_total_absent*100)/$department_total_manpower_dep,2).'%</td>
							<td width="" align="right"> '. round(($department_total_total_leave*100)/$department_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '. round(($department_total_week_day*100)/$department_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
					
		$department_total_new_join=0;$department_total_manpower_dep=0;$department_total_movement=0;$department_total_present=0;$department_total_absent=0;$department_late=0;$department_total_total_leave=0;$department_total_week_day=0;	
			
		}
		 
		if($status_divis==1 )
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center" colspan="2">Division Total</td>
							<td width='' align='right'><? echo $em_post_total; ?></td>
							<td width='' align='right'> <? echo $division_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $division_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $division_total_movement; ?></td>
							<td width='' align='right'> <? echo $division_total_present; ?></td>						
							<td width='' align='right'> <? echo $division_late; ?></td>
                            <td width='' align='right'> <? echo $division_total_present+$division_late; ?></td>
                            <td width='' align='right'> <? echo $division_total_absent; ?></td>	
							<td width='' align='right'> <? echo $division_total_total_leave; ?></td>	
                            <td width='' align='right'> <? echo $division_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                          
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($division_total_movement*100)/$division_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($division_total_present*100)/$division_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($division_late*100)/$division_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($division_total_present*100)/$division_total_manpower_dep,2)+round(($division_late*100)/$division_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($division_total_absent*100)/$division_total_manpower_dep,2); ?>%</td> 
							<td width='' align='right'> <? echo round(($division_total_total_leave*100)/$division_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($division_total_week_day*100)/$division_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$grand_divi_per=$division_total_present+$division_late;
						$in_grand_divi_per=round(($division_total_present*100)/$division_total_manpower_dep,2)+round(($division_late*100)/$division_total_manpower_dep,2);
						$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center">Division Total</td>
							<td width="" align="right">'.$em_post_total.'</td>
							<td width="" align="right"> '. $division_total_manpower_dep.'</td>
                            <td width="" align="right"> '.$grand_divi_per.'</td>
                            <td width="" align="right"> '. $division_total_absent.'</td>	
							<td width="" align="right"> '. $division_total_total_leave.'</td>	
                            <td width="" align="right"> '. $division_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right"> '. $in_grand_divi_per.'%</td>
                            <td width="" align="right"> '. round(($division_total_absent*100)/$division_total_manpower_dep,2).'%</td> 
							<td width="" align="right"> '. round(($division_total_total_leave*100)/$division_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '. round(($division_total_week_day*100)/$division_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
					
	$division_total_new_join=0;$division_total_manpower_dep=0;$division_total_movement=0;$division_total_present=0;$division_total_absent=0;$division_late=0;$division_total_total_leave=0;$division_total_week_day=0;
 			
		}
		 
		if($status_loc==1 )
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
					<td align="center" colspan="2">Location Total</td>
							<td width='' align='right'><? echo $em_post_total; ?></td>
							<td width='' align='right'> <? echo $location_total_new_join; ?></td>						
							<td width='' align='right'> <? echo $location_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $location_total_movement; ?></td>
							<td width='' align='right'> <? echo $location_total_present; ?></td>						
							<td width='' align='right'> <? echo $location_late; ?></td>
                            <td width='' align='right'> <? echo $location_total_present+$location_late; ?></td>
                            <td width='' align='right'> <? echo $location_total_absent; ?></td>	
							<td width='' align='right'> <? echo $location_total_total_leave; ?></td>	
                            <td width='' align='right'> <? echo $location_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                            
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width='' colspan="2"><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($location_total_movement*100)/$location_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($location_total_present*100)/$location_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($location_late*100)/$location_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($location_total_present*100)/$location_total_manpower_dep,2)+round(($location_late*100)/$location_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($location_total_absent*100)/$location_total_manpower_dep,2); ?>%</td> 
							<td width='' align='right'> <? echo round(($location_total_total_leave*100)/$location_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($location_total_week_day*100)/$location_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr><?
						$grand_loc_per=$location_total_present+$location_late;
						$in_grand_loc_per=round(($location_total_present*100)/$location_total_manpower_dep,2)+round(($location_late*100)/$location_total_manpower_dep,2);
						$small_print .='<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td align="center">Location Total</td>
							<td width="" align="right">'. $em_post_total.'</td>
							<td width="" align="right">'. $location_total_manpower_dep.'</td>
                            <td width="" align="right">'.$grand_loc_per.'</td>
                            <td width="" align="right"> '. $location_total_absent.'</td>	
							<td width="" align="right"> '. $location_total_total_leave.'</td>	
                            <td width="" align="right"> '. $location_total_week_day.'</td>	
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=""><b>(In %)</b></td>
							<td width="">&nbsp;</td>						
							<td width="">&nbsp;</td>
                            <td width="" align="right"> '. $in_grand_loc_per.'%</td>
                            <td width="" align="right"> '. round(($location_total_absent*100)/$location_total_manpower_dep,2).'%</td> 
							<td width="" align="right"> '. round(($location_total_total_leave*100)/$location_total_manpower_dep,2).'%</td>
                            <td width="" align="right"> '.round(($location_total_week_day*100)/$location_total_manpower_dep,2).'%</td>
							<td width="">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>';
		$location_total_new_join=0;$location_total_manpower_dep=0;$location_total_movement=0;$location_total_present=0;$location_total_absent=0;$location_late=0;$location_total_total_leave=0;$location_total_week_day=0;				
		
 			
		}
		
		
    ?>
    <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold; font-size:14px">
    	<td colspan="2" align="center">&nbsp;Grand Total</td>
        <td width='' align='right'><? echo $em_post_total; ?></td>
        <td width='' align='right'> <? echo $grand_total_new_join; ?></td>						
        <td width='' align='right'> <? echo $grand_total_manpower_dep; ?></td>
        <td width='' align='right'> <? echo $grand_total_movement; ?></td>
        <td width='' align='right'> <? echo $grand_total_present; ?></td>						
        <td width='' align='right'> <? echo $grand_late; ?></td>
        <td width='' align='right'> <? echo $grand_total_present+$grand_late; ?></td>
        <td width='' align='right'> <? echo $grand_total_absent; ?></td>
        <td width='' align='right'> <? echo $grand_total_total_leave; ?></td>	
        <td width='' align='right'> <? echo $grand_total_week_day; ?></td>	
        <td width=''>&nbsp;</td>
        <td width=''>&nbsp;</td>
    </tr>
    <tr style="font-weight:bold; font-size:14px">
        <td width='' colspan="2" align="center"><b>(In %)</b></td>
        <td width=''>&nbsp;</td>						
        <td width=''>&nbsp;</td>
        <td width=''>&nbsp;</td>
        <td width='' align='right'> <? echo round(($grand_total_movement*100)/$grand_total_manpower_dep,2); ?>%</td>
        <td width='' align='right'> <? echo round(($grand_total_present*100)/$grand_total_manpower_dep,2); ?>%</td>					
        <td width='' align='right'> <? echo round(($grand_late*100)/$grand_total_manpower_dep,2); ?>%</td>
        <td width='' align='right'> <? echo round(($grand_total_present*100)/$grand_total_manpower_dep,2)+round(($grand_late*100)/$grand_total_manpower_dep,2); ?>%</td>	
        <td width='' align='right'> <? echo round(($grand_total_absent*100)/$grand_total_manpower_dep,2); ?>%</td>
        <td width='' align='right'> <? echo round(($grand_total_total_leave*100)/$grand_total_manpower_dep,2); ?>%</td>
        <td width='' align='right'> <? echo round(($grand_total_week_day*100)/$grand_total_manpower_dep,2); ?>%</td>
        <td width=''>&nbsp;</td>
        <td width=''>&nbsp;</td>
    </tr> 
</table> 
<?
		$grand_tot_per=$grand_total_present+$grand_late;
		$in_per=round(($grand_total_present*100)/$grand_total_manpower_dep,2)+round(($grand_late*100)/$grand_total_manpower_dep,2);
			$small_print .='<tr align="right" bgcolor="#CCCCCC" style="font-weight:bold; font-size:14px">
				<td>&nbsp;Grand Total</td>
				<td width="" align="right">'.$em_post_total.'</td>
				<td width="" align="right">'.$grand_total_manpower_dep.'</td>
				<td width="" align="right">'.$grand_tot_per.'</td>
				<td width="" align="right">'.$grand_total_absent.'</td>
				<td width="" align="right">'.$grand_total_total_leave.'</td>	
				<td width="" align="right">'.$grand_total_week_day.'</td>	
				<td width="">&nbsp;</td>
				<td width="">&nbsp;</td>
			</tr>
				<tr style="font-weight:bold; font-size:14px">
				<td width=""><b>(In %)</b></td>
				<td width="">&nbsp;</td>						
				<td width="">&nbsp;</td>
				<td width="" align="right"> '.$in_per.'%</td>
				 <td width="" align="right">'.round(($grand_total_absent*100)/$grand_total_manpower_dep,2).'%</td>
				<td width="" align="right">'.round(($grand_total_total_leave*100)/$grand_total_manpower_dep,2).'%</td>
				<td width="" align="right">'.round(($grand_total_week_day*100)/$grand_total_manpower_dep,2).'%</td>
				<td width="">&nbsp;</td>
				<td width="">&nbsp;</td>	
			</tr>
		</table> ';

}else{
//for peak
?>	
	
<table width="1100" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
	<?
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$holiday=0;$weekend=0;$total_leave=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_manpower=0;$new_join=0;$weekday_tot=0;				

	$section_total_new_join=0;$section_total_manpower_dep=0;$section_total_movement=0;$section_total_present=0;$section_total_absent=0;$section_total_late=0;$section_total_total_leave=0;	

	$department_total_new_join=0;$department_total_manpower_dep=0;$department_total_movement=0;$department_total_present=0;$department_total_absent=0;$department_late=0;$department_total_total_leave=0;	
	
	$division_total_new_join=0;$division_total_manpower_dep=0;$division_total_movement=0;$division_total_present=0;$division_total_absent=0;$division_late=0;$division_total_total_leave=0;	
	
	$location_total_new_join=0;$location_total_manpower_dep=0;$location_total_movement=0;$location_total_present=0;$location_total_absent=0;$location_late=0;$location_total_total_leave=0;				
		
$grand_total_new_join=0;$grand_total_manpower_dep=0;$grand_total_movement=0;$grand_total_present=0;$grand_total_absent=0;$grand_late=0;$grand_total_total_leave=0;$grand_total_week_day=0;
	
 	
	$sl=0;
	$ct=0;
	 
//if ( $department_id !="" ) $department_ida=explode(",",$department_id);
//for ($jk=0;$jk<count($department_ida); $jk++)
{
	//$department_id=" and a.department_id=$department_ida[$jk] ";
	/*
	$sql_emp="SELECT e.*,CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS name,e.category,e.id_card_no,e.joining_date,a.emp_code,
	a.location_id,
	a.division_id,a.department_id, a.section_id, a.subsection_id, a.designation_id as attn_designation_id, 
	count(CASE WHEN a.status ='P' THEN a.emp_code END) AS 'present',
	count(CASE WHEN a.status ='P' and is_regular_day=0 THEN a.emp_code END) AS 'gn_present',
	count(CASE WHEN a.status ='A' THEN a.emp_code END) AS 'absent', 
	count(CASE WHEN a.status ='D' THEN a.emp_code END) AS 'late', 
	count(CASE WHEN a.status ='GH' THEN a.emp_code END) AS 'g_holiday', 
	count(CASE WHEN a.status ='FH' THEN a.emp_code END) AS 'f_holiday',
	count(CASE WHEN a.status ='CH' THEN a.emp_code END) AS 'c_holiday', 
	count(CASE WHEN a.status ='H' THEN a.emp_code END) AS 'h_holiday', 
	count(CASE WHEN a.status ='W' THEN a.emp_code END) AS 'weekday', 
	count(CASE WHEN a.status ='MR' THEN a.emp_code END) AS 'movement', 
	count(CASE WHEN a.status ='CL' THEN a.emp_code END) AS 'cl', 
	count(CASE WHEN a.status ='FL' THEN a.emp_code END) AS 'fl', 
	count(CASE WHEN a.status ='EL' THEN a.emp_code END) AS 'el', 
	count(CASE WHEN a.status ='SL' THEN a.emp_code END) AS 'sl', 
	count(CASE WHEN a.status ='ML' THEN a.emp_code END) AS 'ml', 
	count(CASE WHEN a.status ='SpL' THEN a.emp_code END) AS 'spl', 
	count(CASE WHEN a.status ='LWP' THEN a.emp_code END) AS 'lwp', 
	count(CASE WHEN a.status ='SP' THEN a.emp_code END) AS 'sp',
	count(CASE WHEN  a.attnd_date like '$date_part%' and e.joining_date='$date_part' THEN a.emp_code END) AS 'new_join' 
	
	FROM 
	hrm_attendance a,hrm_employee e 
	where a.emp_code=e.emp_code and e.status_active and a.attnd_date like '$date_part%' $company_id $category  $location_id  $division_id $department_id $section_id $subsection_id $designation_id $dynamic_groupby $orderby";	
	*/	

$sql_emp="SELECT 
			a.company_id,a.location_id,a.division_id,a.department_id, a.section_id, a.subsection_id,e.designation_level,a.status, 
		   count(CASE WHEN a.status ='P' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'present',
		   count(CASE WHEN a.status ='P' and is_regular_day=0 and a.attnd_date='$date_part' THEN a.emp_code END) AS 'gn_present',
		   count(CASE WHEN a.status ='A' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'absent', 
		   count(CASE WHEN a.status ='D' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'late', 
		   count(CASE WHEN a.status ='GH' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'g_holiday', 
		   count(CASE WHEN a.status ='FH' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'f_holiday',
		   count(CASE WHEN a.status ='CH' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'c_holiday', 
		   count(CASE WHEN a.status ='H' and a.attnd_date='$date_part'THEN a.emp_code END) AS 'h_holiday', 
		   count(CASE WHEN a.status ='W' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'weekday', 
		   count(CASE WHEN a.status ='MR' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'movement', 
		   count(CASE WHEN a.status ='CL' and a.attnd_date='$date_part'THEN a.emp_code END) AS 'cl', 
		   count(CASE WHEN a.status ='FL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'fl', 
		   count(CASE WHEN a.status ='EL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'el', 
		   count(CASE WHEN a.status ='SL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'sl', 
		   count(CASE WHEN a.status ='ML' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'ml', 
		   count(CASE WHEN a.status ='SpL' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'spl', 
		   count(CASE WHEN a.status ='LWP' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'lwp', 
		   count(CASE WHEN a.status ='SP' and a.attnd_date='$date_part' THEN a.emp_code END) AS 'sp',
		   count(CASE WHEN  a.attnd_date='$date_part' and e.joining_date='$date_part' THEN a.emp_code END) AS 'new_join' 
		   FROM 
			hrm_attendance a,hrm_employee e 
			where a.emp_code=e.emp_code and a.attnd_date='$date_part' $company_id $category  $location_id  $division_id $department_id $section_id $subsection_id $designation_id $salary_based group by $dynamic_groupby $orderby";			
					 
	
 // echo $sql_emp;die;
	
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{ 
		
		$weekday_tot=$row_emp[weekday];
		$movement=$row_emp[movement];
		$present=$row_emp[present];
		$late=$row_emp[late];
		$absent=$row_emp[absent];
		$total_leave=$row_emp[cl]+$row_emp[sl]+$row_emp[el]+$row_emp[ml]+$row_emp[spl]+$row_emp[fl]+$row_emp[lwp]+$row_emp[sp];
		$total_manpower=$movement+$present+$late+$absent+$total_leave+$weekday_tot; //+$movement;
	 	$new_join=$row_emp[new_join];
		$sl++;
 		
		   //start header print---------------------------------//
		   if($sl==1)
		   {
				$location_arr[$row_emp[location_id]]=$row_emp[location_id];
				$division_arr[$row_emp[division_id]]=$row_emp[division_id];
				$department_arr[$row_emp[department_id]]=$row_emp[department_id];
				$section_arr[$row_emp[section_id]]=$row_emp[section_id];
				$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];

			   ?> 
               <thead>
        <tr><td colspan="13" align="center"><font size="+1"><? echo $company_details[$row_emp['company_id']]['company_name']; ?></font><br />
                                		Address: Plot No # <? echo $company_details[$row_emp['company_id']]["plot_no"]; ?>, Level No # <? echo $company_details[$row_emp['company_id']]["level_no"]; ?>, Block No # <? echo $company_details[$row_emp['company_id']]["block_no"]; ?>, Road No # <? echo $company_details[$row_emp['company_id']]["road_no"]; ?>, <? echo $company_details[$row_emp['company_id']]["city"]; echo $company_details[$row_emp['company_id']]["country_name"];?><br />
                            <span style="font-size:18px;"> Daily Attendance Summary as on <? echo convert_to_mysql_date($date_part);?> </span>          
                                     </td></tr>
         
								<tr>
                                <th width='130'><b>Designation Level</b></th>
								<th width='70'><b>Sanction</b></th>
								<th width='100'><b>Exist Manpower</b></th><!--this caption is changed from Manpower to Exist Manpower for mr sayeed requirments-->
								<th width='100'><b>Present(No Late)</b></th>
                                <th width='80'><b>Present(Late)</b></th>
                                <th width='100'><b>Total Present</b></th>
                                <th width='70'><b>Absent</b></th>
								<th width='70'><b>Leave</b></th>
                                <th width='70'><b>Movement</b></th>
                                <th width='80'><b>Newly Joined</b></th>
                                <th width='70'><b>Weekend</b></th>
								<th width='60'><b>Short/Surplus</b></th>
								<th width='60'><b>Remarks</b></th>
                                </tr>
							</thead>
				<?
		   }//end if condition of header print
		   //end header print
		
 		//---------------------------------------------------------------------------------------------------	
		//---------------------------------------------------------------------------------------------------
		
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row_emp[section_id]]=$row_emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row_emp[department_id]]=$row_emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row_emp[section_id]]=$row_emp[section_id];
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row_emp[division_id]]=$row_emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row_emp[department_id]]=$row_emp[department_id];
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row_emp[location_id]]=$row_emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row_emp[division_id]]=$row_emp[division_id];
						$department_arr[$row_emp[department_id]]=$row_emp[department_id];
						$section_arr[$row_emp[section_id]]=$row_emp[section_id];
						$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td width=''><b>SectionTotal</b></td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo $section_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $section_total_present; ?></td>						
							<td width='' align='right'> <? echo $section_total_late; ?></td>
                            <td width='' align='right'> <? echo $section_total_present+$section_total_late; ?></td>
                            <td width='' align='right'> <? echo $section_total_absent; ?></td>
							<td width='' align='right'> <? echo $section_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $section_total_movement; ?></td>
                            <td width='' align='right'> <? echo $section_total_new_join; ?></td>
                            <td width='' align='right'> <? echo $section_total_week_day; ?></td>
                            <td width='' align='right'>&nbsp;</td>
				 			<td width=''>&nbsp;</td>
                           
						</tr>
						<?
				
			$section_total_new_join=0;$section_total_manpower_dep=0;$section_total_movement=0;$section_total_present=0;$section_total_absent=0;$section_total_late=0;$section_total_total_leave=0;$section_total_week_day=0;
						
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
			}
	

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr style="font-weight:bold; font-size:14px"><td colspan="13"><b><? echo substr($c_part_1,0,-1); ?><? echo $section_total_week_day; ?></b></td></tr><?
			}		
 		}
		
		$ct++;
		
		$compa_id=$row_emp[company_id];  $loc_id=$row_emp[location_id]; $div_id=$row_emp[division_id]; $dep_id=$row_emp[department_id];
		 $sec_id=$row_emp[section_id]; $sub_sec_id=$row_emp[subsection_id]; $dg_id=$row_emp[attn_designation_id]; 
		 
		if ($cbo_emp_category=="") $category=" emp_catg_id in ($employee_category_index)"; else $category="emp_catg_id='$cbo_emp_category'";
		if ($compa_id==0) $company_id =""; else $company_id=" and company_id='$compa_id'";
		if ($loc_id==0) $loca_id =""; else $loca_id ="and locaion_id='$loc_id'";
		if ($div_id==0) $divit_id =""; else $divit_id ="and division_id='$div_id'";
		if ($dep_id==0) $deper_id =""; else $deper_id ="and department_id='$dep_id'";
		if ($sec_id==0) $sect_id =""; else $sect_id ="and section_id='$sec_id'";
		if ($sub_sec_id==0) $sub_sec_id =""; else $sub_sec_id ="and subsection_id='$sub_sec_id'";
		if ($dg_id==0) $desig_id =""; else $desig_id ="and designation_id='$dg_id'";
		
		$sql22="SELECT * FROM budgeted_manpower_setup WHERE $category $company_id $loca_id $divit_id $deper_id $sect_id $sub_sec_id $desig_id ";
		//echo $sql22."<br>";
		$query_result = mysql_db_query( $DB, $sql22 ) or die( $sql22 . "<br />" . mysql_error() );
		$em_post=0;
		while ($selectResult = mysql_fetch_array($query_result))
		{
			$em_post=$selectResult['no_of_post'];
		}
		$em_post_total+=  $em_post;	
		
		$short_surplus=0;		
		$short_surplus=($em_post)-($total_manpower);
		//if($em_post>$total_manpower){$sine="(+)";} else if($em_post<$total_manpower){$sine="(-)";}
		
		
		//if ($dg_id==0) $designation_id =""; else $designation_id ="and designation_id='$dg_id'";
 			
		/*if ($ct%2==0) $bgcolor="#EFEFEF"; 
		else $bgcolor="#FFFFFF";*/
		//$total_leave=$cl+$sl+$ml+$el+$lwp+$spl;	
    ?>
    <tbody>	    	
    	<tr style="font-weight:bold; font-size:14px">
						<td width=''><? 
							echo $designation_chart_level[$row_emp[designation_level]];	
						 ?></td>
						<td width=''>&nbsp;</td>
						<td width='' align='right'><? echo $total_manpower;?></td>
						<td width='' align='right'><? echo $present; ?></td>
						<td width='' align='right'><? echo $late; ?></td>
                        <td width='' align='right'><? echo $present+$late; ?></td>
                         <td width='' align='right'><? echo $absent; ?></td> 	
						<td width='' align='right'><? echo $total_leave; ?></td>
                        <td width='' align='right'><? echo $movement ;?></td>
                        <td width='' align='right'><? echo $new_join; ?></td>
                        <td width='' align='right'><? echo $weekday_tot; ?></td>	
						<td width='' align='right'><? echo $short_surplus; ?></td>		
						<td width=''>&nbsp;</td>
					</tr>
        <?
	
				$ct++;
				
				
				//section 
				$section_total_week_day += $weekday_tot;
				$section_total_new_join += $new_join;
				$section_total_manpower_dep += $total_manpower;				
				$section_total_movement += $movement;
				$section_total_present += $present;
				$section_total_absent += $absent;
				$section_total_late += $late;
				$section_total_total_leave += $total_leave;
				
				//grand total
				$grand_total_week_day += $weekday_tot;
				$grand_total_new_join+=  $new_join;
				$grand_total_manpower_dep+= $total_manpower;		
				$grand_total_movement+= $movement;
				$grand_total_present+= $present;
				$grand_total_absent+= $absent;
				$grand_late+= $late;
				$grand_total_total_leave += $total_leave;
				
	}//while loop end here 
	
	?>
    
	</tbody>   
    <?
		
		if($status_sec==1 )
		{
 			?><tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
							<td width=''><b>SectionTotal</b></td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo $section_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $section_total_present; ?></td>						
							<td width='' align='right'> <? echo $section_total_late; ?></td>
                            <td width='' align='right'> <? echo $section_total_present+$section_total_late; ?></td>
                            <td width='' align='right'> <? echo $section_total_absent; ?></td>
							<td width='' align='right'> <? echo $section_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $section_total_movement; ?></td>
                            <td width='' align='right'> <? echo $section_total_new_join; ?></td>
                             <td width='' align='right'> <? echo $section_total_week_day; ?></td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<?
						
		$section_total_new_join=0;$section_total_manpower_dep=0;$section_total_movement=0;$section_total_present=0;$section_total_absent=0;$section_total_late=0;$section_total_total_leave=0;$section_total_week_day=0;
			
		}
		 
    ?>
  <tr align="right" bgcolor="#CCCCCC" style="font-weight:bold; font-size:14px">
                			<td>&nbsp;Grand Total</td>
                    		<td width='' align='right'>&nbsp;</td>
							<td width='' align='right'> <? echo $grand_total_manpower_dep; ?></td>
							<td width='' align='right'> <? echo $grand_total_present; ?></td>						
							<td width='' align='right'> <? echo $grand_late; ?></td>
                            <td width='' align='right'> <? echo $grand_total_present+$grand_late; ?></td>	
                            <td width='' align='right'> <? echo $grand_total_absent; ?></td>
							<td width='' align='right'> <? echo $grand_total_total_leave; ?></td>
                            <td width='' align='right'> <? echo $grand_total_movement; ?></td>
                            <td width='' align='right'> <? echo $grand_total_new_join; ?></td>
                             <td width='' align='right'> <? echo $grand_total_week_day; ?></td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                           
						</tr>
						<tr style="font-weight:bold; font-size:14px">
							<td width=''><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width='' align='right'> <? echo round(($grand_total_present*100)/$grand_total_manpower_dep,2); ?>%</td>					
							<td width='' align='right'> <? echo round(($grand_late*100)/$grand_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($grand_total_present*100)/$grand_total_manpower_dep,2)+round(($grand_late*100)/$grand_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($grand_total_absent*100)/$grand_total_manpower_dep,2); ?>%</td>
							<td width='' align='right'> <? echo round(($grand_total_total_leave*100)/$grand_total_manpower_dep,2); ?>%</td>
                            <td width='' align='right'> <? echo round(($grand_total_movement*100)/$grand_total_manpower_dep,2); ?>%</td>
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
                            <td width=''>&nbsp;</td>
                            <td width=''>&nbsp;</td>
						</tr> 
</table> 
<?
//7end peak
}
}
 signeture_table(7,$cbo_company,'1100px',$cbo_location);
// $sig=signeture_table(7,$cbo_company);
 $small_print .='<div id="signatur"> </div>';
 //$small_print .='<div>'.$sig.'</div>';
 
	//previous file delete code-----------------------------//
	  $html = ob_get_contents();
		ob_clean();	
		
	// 12.05.2014
	$name=time();
	$nametxt="$name".".txt";	
	$create_new_excel = fopen('tmp_report_file/'.$nametxt, 'w');
	$new_="<div>".$html."</div>";	
	$is_created = fwrite($create_new_excel,$new_);
		
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	//echo "$html"."####"."$name";
	echo "$html"."####"."$name"."####".$small_print;
	exit();
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}

?>