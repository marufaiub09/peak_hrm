<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


if( $action == "monthly_attn_audit_rpt" ) {	// Audit report
			
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name=$cbo_company_id order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
		}
	
	//subsection_details
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
			$shift_details[$row['id']] = $row['short_shift_name'];
	}
	
	
	$date_part = trim($cbo_year_selector)."-".trim($cbo_month_selector);
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$salary_month_date=strtotime($date_part."-01");
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and emp.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and a.category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else  $department_id="and emp.department_id='$department_id'";
	if ($designation_id==0) $designation_id=""; else  $designation_id="and emp.designation_id='$designation_id'";
	//date("F j, Y, g:i a");   date('Y-m-t',$timeStamp)
	
		
	ob_start();	
	
	?>
	<div align="center" style="width:1700px; font-size:16px"><b><? echo $company_details[$cbo_company_id]['company_name']; ?></b></div>
    <div align="center" style="width:1700px; font-size:12px"><b>Master Attendance Register</b></div>
    <div align="center" style="width:1700px; font-size:12px"><b>Salary Month: <? echo date("F, Y",$salary_month_date); ?></b></div>
	<table width="1700" style="font-size:10px; border:1px solid #000" cellpadding="0" cellspacing="0" border="1" class="rpt_table" rules="all">
    	
	<?
	 $new_section=array();
		
	 $i=0;
	 //emp.department_id=$row_dp[id] and 
 	 $sqls = "SELECT a.*, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
	 		  FROM hrm_attendance emp left join hrm_employee a on emp.emp_code=a.emp_code
			  where			  
			  emp.attnd_date like '$date_part%'
			  $company_id 
			  $location_id
			  $division_id 
			   $department_id
			  $section_id 
			  $subsection_id 
			  $designation_id 
			  $category group by emp.emp_code order by a.division_id,a.department_id,a.section_id,CAST(trim(a.id_card_no) AS SIGNED)";
			  
	 //echo $sqls;die;
	 
	 $results = mysql_query( $sqls );
	 while( $rows = mysql_fetch_array( $results ) ) 
	 {
	 	
		$sql = "SELECT * 
				FROM hrm_attendance 
				WHERE  attnd_date like '$date_part%' and emp_code='$rows[emp_code]' 
				order by attnd_date ASC";
		
		//echo $sql;
		$result = mysql_query( $sql );
			
		if(mysql_num_rows($result)>0 )
	 	{

			
			
			//if(in_array($rows['department_id'],$new_department))
			//{
			//}
			//else
			//{
				//$new_department[$rows['department_id']]=$rows['department_id'];
				//$company_details[$row['id']] = array();
			//}
			
			$index=$rows['division_id'].$rows['department_id'].$rows['section_id'];
			//echo $index;die;
			if(in_array($index, $new_section))						
				{
					$i++;
				}
				else
				{	
					$new_section[$index]=$index;
					//$new_section[$i]=$rows[section_id];
					$division_name=$division_details[$rows['division_id']]['division_name'];
					$department_name=$department_details[$rows['department_id']]['department_name'];
					$section_name=$section_details[$rows['section_id']]['section_name'];
					
				?>	
				
                    <thead>
                        <tr>
                            <td colspan='<? echo $get_days+10; ?>'><b>Division Name: <? echo $division_name;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Department Name: <? echo $department_name;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Section Name: <? echo $section_name; ?></b></td>
                        </tr>
                        <tr>
                            <th width='50'><b>Emp Code</b></th>
                            <th width='50'><b>ID Card</b></th>
                            <th width='120'><b>Emp Name</b></th>
                            <th width='100'><b>Designation</b></th>
                            <th width='50'><b>Join Date</b></th>
                            <th width='20'><b>Date</b></th>				
                            <?		
                            for($j=1; $j<=$get_days;$j++)
                               {  ?>                        
                                    <th width='30'><b><? echo $j; ?></b></th>
                            <? } ?>
                            <th width='20' style="font-size:9px"><b>Total (P,GH,FH,W)</b></th>
                            <th width='20'><b>Total Absent</b></th>
                            <th width='20'><b>Total Leave</b></th>
                            <th width='20'><b>Total Late</b></th>
                       </tr>
                  	</thead>	
				  <tbody>							
				<?	
					
				$i++;			
				
			}		
		
			// 1-31 days column----------------------------------
			$prt=0;
			$present=0;$absent=0;$leave=0;$wday=0;$pay_day=0;$cnt=0;$is_new_join=0;$st_date=$date_part."-01";$total_ot=0;
			$ot_list="";$shift_list="";
			while( $row = mysql_fetch_assoc( $result ) ) 
			{	
				
						if( $prt==0 )
						{
							$prt=1;
							if ($i%2==0) $bgcolor="#E9F3FF"; 
							else $bgcolor="#FFFFFF";
						  ?>
                          
							<tr bgcolor="<? echo $bgcolor; ?>">
								<td width='50' rowspan="3"><? echo $rows[emp_code]; ?></td>
								<td width='50' rowspan="3"><? echo $rows[id_card_no];?></td>
								<td width='120' rowspan="3"><? echo $rows[name];?></td>
								<td width='100' rowspan="3"><? echo $designation_chart[$rows['designation_id']]['custom_designation'];?></td>
								<td width='50' rowspan="3"><? echo convert_to_mysql_date ($rows[joining_date]);?></td>
								<td width='20' style="font-size:10px">Status</td>
						   <?	
						}			
						
						
						if ($cnt==0)
						{
							$start_date=datediff( "d", $st_date, $row[attnd_date]);
							if( $start_date >1 )
							{
								for($mk=0; $mk<$start_date-1; $mk++)
								{
									$cnt++;
									?> <td width='50' align="center"><b>&nbsp;</b></td> <?
									$ot_list .='<td width="50" align="center">&nbsp;</td>';
									$shift_list .='<td width="50" align="center">&nbsp;</td>';
								}
							}
						}
						$cnt++;
						
						$hours[0] = abs((int)($row["total_over_time_min"]/60)); //hours
						$hours[1] = abs((int)($row["total_over_time_min"]%60)); //minutes
						
						$ot_mins_cal=0;
						if($ot_fraction==1) //ot fraction allow
						{						
							if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
							else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
							else $ot_mins_cal = $hours[0]*60;
						}
						if($ot_fraction==2) // no ot fraction
						{						
							//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
							if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
							else $ot_mins_cal = $hours[0]*60;						
						}
						if($ot_fraction==3) //at actual minutes
						{						
							$ot_mins_cal = 	$row["total_over_time_min"];				
						}
						
						$total_hours = sprintf("%d.%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
						
						?> <td width='50' align="center"><? echo $row[status];?></td> <?
						$ot_list .='<td width="50" align="right" bgcolor="'.$bgcolor.'">'.$total_hours.'</td>';
						$shift_list .='<td width="50" align="center"  bgcolor="'.$bgcolor.'">&nbsp;'.$shift_details[$row['policy_shift_id']].'</td>';
						
						$total_ot += $total_hours;
						$explode_total_ot = explode(".",$total_ot);
						if($explode_total_ot[1]==6) $total_ot = $explode_total_ot[0]+1;
						
					
			   } //3rd while loop-----------------------
					
						$d="count(*)";
						$dd="hrm_attendance";
						$ddd="status in ('P') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$present= return_field_value($d,$dd,$ddd);
						
						$ddd="status='A ' and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$absent= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('CL','ML','SL','EL','SpL') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$leave= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('D') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$late= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('MR') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$movement= return_field_value($d,$dd,$ddd);
						
						$ddd="status='W' and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$wday= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('H','GH','CH','FH') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$hday= return_field_value($d,$dd,$ddd);
						
					if($cnt>$get_days) $cnt=$get_days; else $cnt=$cnt;// 00000255 Emp Code
					while($get_days!=$cnt)
					{
						?> <td width='50' align="center" ><b>&nbsp;</b></td> <?
						$ot_list .='<td width="50" align="right" bgcolor="'.$bgcolor.'"><b>&nbsp;</b></td>';
						$shift_list .='<td width="50" align="center"  bgcolor="'.$bgcolor.'">&nbsp;</td>';
						$cnt++;
					}
															
					?>
                    <td width='50' align="center" rowspan=""><? echo $hday+$present+$wday+$movement; ?></td>
                    <td width='50' align="center" rowspan=""><? echo $absent;  ?></td>
                    <td width='50' align="center" rowspan=""><? echo $leave;  ?></td>
                    <td width='50' align="center" rowspan=""><? echo $late;  ?></td>
                    <td width='50' align="center"  rowspan="3" colspan="4"><br /><? //echo "Total OT=".$total_ot;  ?></td>              
            	</tr>                                
                <!--<tr>
                	<td style="font-size:10px; font-weight:bold">Shift</td>
                	<? //echo $shift_list; ?>
                </tr>-->
                <? $rows="<tr>
                	<td style=\"font-size:10px\">Shift</td>
                	 $shift_list 
                </tr>"
				?>
                <?
				if($rpt_shift==1)
				echo $rows;
				
				?>
                <tr>
                	<td style="font-size:10px">OT</td>
                	<? echo $ot_list; ?>
                   <td width='50' align="center" colspan="4"><? echo "Total OT=".$total_ot;  ?></td>
                    <!--<td width='50' align="center" ><b>&nbsp;</b></td>--> 
                </tr>
            
            </tbody>
                 
				 <?
				
				} //end if condition 			
		} //1st while loop
		
	
	?> 
    	</table>
	<?
				
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
	
	
}


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>
