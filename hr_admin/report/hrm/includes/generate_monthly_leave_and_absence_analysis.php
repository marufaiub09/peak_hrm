<?
/*######################################

	Completed By
	Name : Sohel
	Date : 30-10-2014
		
######################################*/

session_start();

header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

$new_months = array(01=>'January',02=>'February',03=>'March',04=>'April',05=>'May',06=>'June',07=>'July',08=>'August',09=>'September',10=>'October',11=>'November',12=>'December');

extract ( $_REQUEST );


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}


	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}


	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
	
	
	$sql = "SELECT * FROM variable_settings_report WHERE is_deleted = 0 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$variable_settings_report = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$variable_settings_report[$row['company_id']][$row['module_id']] = mysql_real_escape_string($row['report_list']);
	}
	
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$allow_rate=array();
	$designation_chart = array();
	while( $row = mysql_fetch_array( $result ) ) {		
		$designation_chart[$row['id']] =  $row['custom_designation'];
		$designation_chart_rate[$row['id']]['rate'] = ( $row['allowance_rate'] );
		$allow_rate[$row['id']]	= $row['allowance_rate'] ;
	}
	 

if($action=="monthly_leave_and_absence_analysis") 	
{	

	//echo $from_month_selector;
	$from_month_selector = explode('_', $from_month_selector ); 
	$from_month = $from_month_selector[0];
	$to_month = $from_month_selector[1];
	$yrdata= strtotime($from_month);
    $header_from_month=date('M-Y', $yrdata);
	
	$con_date=date('Y-m-%%',strtotime($from_month));
	$from_con_date=date('Y-m-d',strtotime($from_month));
	$exp_month= explode('-', $from_con_date );
	$cal_days=cal_days_in_month (CAL_GREGORIAN,$exp_month[1],$exp_month[0]);
	
	//echo $weekday;

	//company address
	$company_info_sql="select * from lib_company where id='$company_id'";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	//if ($cbo_emp_category=='') $category =""; else $category ="and a.category='$cbo_emp_category'";
	if ($company_id==0 || $company_id=='') $company_id=""; else $company_id="and a.company_id='$company_id'";
	if ($location_id==0 || $location_id=='') $location_id=""; else $location_id="and a.location_id='$location_id'"; 
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and  a.department_id in ($department_id)";   
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)"; 
	if ($subsection_id==0 || $subsection_id=='') $subsection_id="";else $subsection_id="and a.subsection_id in ($subsection_id)";
	?>
	
	 <table width="1000" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
    	<thead>    
            <tr style="text-align:center; font-weight:bold; font-size:24px" height="20">   <!--bgcolor="#CCCCCC" -->	 
                <th colspan="10">
                <div><font size="3"><?	echo $company_info_result["company_name"];?></font></div>
                <br /><font size="2">Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?></font><br />
                <font size="2"> Leave and Absence Analysis Report For  <? echo $header_from_month; ?> </font> 
                </div>
                </th>
            </tr>
            <tr style="text-align:center; font-weight:bold;font-size:10px;" height="20" bgcolor="#CCCCCC">
                <th  width="120">Months</th>
                <th  width="80">Total Worker</th> 
                <th  width="80">Working Days</th> 
                <th  width="80"><p>Total Worker Working Days</p></th> 
                <th  width="80">Total Leave Days</th> 
                <th  width="80"><p>Total Absence Days</p></th> 
                <th  width="80"><p>Total Leave and Absence Days</p></th> 
                <th  width="80">% of Absenteeism</th> 
                <th  width="80">Left Workers</th> 
                <th  width="80">Turn Over</th>
            </tr>
        </thead>
	
	<?
		$weekday_arr=array();
		$weekday=0;
		for($j=0; $j<$cal_days;$j++)
		{
			$c_date=add_date($from_con_date,$j);
			$day=date("l",strtotime($c_date));
			//echo $day.",";
			
			if($day=='Wednesday')
			{
				$weekday++;
				$weekday_arr[]=$c_date;
			}
		}
	//print_r($weekday_arr);
	
	
	
	$sql_ho = "SELECT * FROM lib_holiday_details WHERE is_deleted = 0 and holiday_date like '$con_date' group by holiday_date ORDER BY holiday_date ASC";
	$result = mysql_query( $sql_ho ) or die( $sql_ho . "<br />" . mysql_error() );
	$holiday_chart_arr = array();
	$holiday_chart=0;
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if(!in_array($row['holiday_date'],$weekday_arr))
		{ 
			$holiday_chart++;
		}
		$holiday_chart_arr[]=$row['holiday_date'];	
			
	}
	//echo $holiday_chart;
	
	//print_r($holiday_chart_arr);
	
	
	
		
// Employee Seperation  
	$sql_sep = "SELECT id,emp_code, separated_from as separated_from FROM hrm_separation order by id desc,emp_code asc";
	$result = mysql_query( $sql_sep ) or die( $sql_sep . "<br />" . mysql_error() );
	$sepeartion_list = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		if(!in_array($row['emp_code'],$tmp_code))
		{
			$tmp_code[]=$row['emp_code'];
			$sepeartion_list[$row['emp_code']]= $row['separated_from'];
			
		}
	}
	
	//print_r($sepeartion_list);
	
	$sql="SELECT b.category,b.status_active,b.emp_code,a.attnd_date,a.status,a.emp_code,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,
	count(CASE WHEN a.status not in ('FH','CH','GH','H','W') THEN a.status END) AS total_wdays,
	count(CASE WHEN a.status in ('CL','FL','EL','SL','ML','SpL','LWP') THEN a.status END) AS total_leave,
	count(CASE WHEN a.status in ('A') THEN a.status END) AS total_absence,
	count(CASE WHEN b.status_active=0 THEN a.emp_code END) AS total_left
	from hrm_attendance a,hrm_employee b 
	where 
	a.attnd_date between '".$from_month_selector[0]."' and '".$from_month_selector[1]."' 
	and b.category=2 
	and a.emp_code=b.emp_code 
	$company_id 
	$location_id 
	$division_id  
	$department_id  
	$section_id 
	$subsection_id group by a.emp_code";
	
	//echo $sql; 
	
	$exe_sql_attnd=mysql_db_query($DB, $sql);
	$employee_info=array();
	//$monthly_employee_ot=array();
	$total_left_count=0;
	$total_emp=0;
	while($data=mysql_fetch_array($exe_sql_attnd))
	{
		//echo $data['total_wdays'].",";
		$total_emp++;
		$employee_info[$header_from_month]['total_emp']=$total_emp;
		$employee_info[$header_from_month]['wdays']=$data['total_wdays'];
		$employee_info[$header_from_month]['total_wdays']+=$data['total_wdays'];
		$employee_info[$header_from_month]['total_leave']+=$data['total_leave'];
		$employee_info[$header_from_month]['total_absence']+=$data['total_absence'];
		
		if($data['status_active']==0 && $sepeartion_list[$data['emp_code']]<=$to_month && $sepeartion_list[$data['emp_code']]>=$from_month)
		{
			$total_left_count++;
			//$employee_info[$header_from_month]['total_left']+=$data['total_left'];
		}
		
		//$wdays_info[$header_from_month]=$data['wdays'];
		
	} 
	//print_r($wdays_info);
	//die;
	
	ob_start();
	
 	foreach($employee_info as $key=>$val)
	{
    ?>
        <tr bgcolor="<? echo $bgcolor; ?>" height="20" class="print_tbody" style="font-size:11px">
            <td align="center" valign="middle" ><? echo $header_from_month;?> </td>
            <td align="center" valign="middle" ><? echo $val['total_emp']; ?></td>
            <td align="center" valign="middle" ><? echo $work_day=($cal_days-($holiday_chart+$weekday));//.","; //echo $cal_days."=h".$holiday_chart."=w".$weekday; //echo $val['wdays']; ?></td>
            <td align="center" valign="middle" ><? echo $val['total_wdays']; ?></td>
            <td align="center" valign="middle" ><? echo $val['total_leave']; ?></td>
            <td align="center" valign="middle" ><? echo $val['total_absence'];  ?></td>
            <td align="center" valign="middle" ><? echo $total_leave_absence=($val['total_leave']+$val['total_absence']); ?></td>
            <td align="center" valign="middle"><? echo round(($total_leave_absence*100)/$val['total_wdays'],2); ?>%</td>
            <td align="center" valign="middle" ><? echo $total_left_count; //echo $val['total_left']; ?></td>
            <td align="center" valign="middle"><? echo round(($total_left_count*100)/$val['total_emp'],2); ?>%</td>
        </tr>
       
  <?     

	}
?>
</table>
<? 
	
	$html = ob_get_contents();
	
	ob_clean();	
	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) 
	{			
		@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
 	}

//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$start_1 = substr($row["starting_date"],0,3);
		$start_2 = substr($row["starting_date"],-1,1);

		$end_1 = substr($row["ending_date"],0,3);
		$end_2 = substr($row["ending_date"],-2,2);
		
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$start_1." ".$start_2." - ".$end_1." ".$end_2."</option>";
		//echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}


function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}


?>