<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}
	
// emp_job_card_report
if( $action == "emp_job_card_report" ) 
{	//Job Card Report
	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
	//echo $id_card_no;die;
	//echo $shift_type;
	if($txt_from_date!="" && $txt_to_date!="")
	{
		$from_date=convert_to_mysql_date($txt_from_date);
		$to_date=convert_to_mysql_date($txt_to_date);
		$date_part="attnd_date between "."'".$from_date."' and "."'".$to_date."'";
	}
	else
	{
		$date_range=explode("_",$cbo_month_selector);
		$date_part="attnd_date between "."'".$date_range[0]."' and "."'".$date_range[1]."'";	
	}
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	//echo $get_days;
	
	//$shift_id=$shift_type_arr[$shift_type][$rows[shift_policy]];
	if ($shift_type=="") $shift_type=""; else $shift_type="and shift_type='$shift_type'";
			
	//shift policy array
	$qr_shift="select * from lib_policy_shift where status_active=1 and is_deleted=0 $shift_type order by id";
	$result_s = mysql_query( $qr_shift ) or die( $qr_shift . "<br />" . mysql_error() );
	$shift_arr = array();
	$shift_type_arr = array();
	while($res_shift=mysql_fetch_array($result_s))		
	{
		$shift_arr[$res_shift["id"]]=mysql_real_escape_string($res_shift["shift_name"]);
		//$shift_type_arr[$res_shift["id"]]=mysql_real_escape_string($res_shift["id"]);
		$shift_type_arr[] = $res_shift["id"];
	}
	$shift_id_arr="'".implode("','",$shift_type_arr)."'";
	$shift_policy_search=" and shift_policy in ( $shift_id_arr )";	
		
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		$ot_fraction = $res['allow_ot_fraction'];
		$ot_start_min = $res['ot_start_minute'];
		$one_hr_ot_unit = $res['one_hour_ot_unit'];
		$adjust_out_time=$res['adjust_out_time'];
		$applicable_above_salary=$res['applicable_above_salary'];
		$in_out_time_format=$res['in_out_time_format'];
	}
	
	$fld_name=array("company_id","location_id","division_id","department_id","section_id","subsection_id","designation_id","emp_code");
		//echo $division_id; die;
	
	 
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and salary_type_entitled='$cbo_salary_based'";
	if ($id_card=='') $id_card_no =""; else $id_card_no ="and id_card_no in($id_card_no)";
	
	if ($emp_code==0) $emp_code=""; 
	else 
	{		
		$emp_explode=explode(",",$emp_code);		
		$employee_code="";
		for($i=0;$i<count($emp_explode);$i++)
		{ if($i==0) $employee_code=	$employee_code.$emp_explode[$i]; else $employee_code=	$employee_code.','.$emp_explode[$i]; } 
		$emp_code="and emp_code in ($employee_code)";
	}
		
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";	
	if ($department_id==0) $department_id=""; else $department_id="where id='$department_id'";
	ob_start();
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{	
	 	$sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM hrm_employee 
				WHERE id!='' and 
					department_id=$row_dp[id] 
					".show_search_employee( $prefix, $emp_code, $cbo_company_id, $location_id,$division_id,$department_id,$section_id,$subsection_id,$designation, $fld_name, 1 )." order by designation_level,CAST(id_card_no as SIGNED)";
		 echo $sqls; die;
		$i=0;
		$results = mysql_query( $sqls ) or die( $sql . "<br />" . mysql_error() );	
		
		while( $rows = mysql_fetch_assoc( $results ) ) 
		{
			
			//this is used inside of while loop
		    $sql = "SELECT * FROM hrm_attendance  WHERE $date_part and  emp_code='$rows[emp_code]' order by attnd_date asc";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$numrow = mysql_num_rows($result);
			if( $numrow > 0 ){
	?>							
			<style media="print">
			p{page-break-before:always}
			</style>	
                <table width="800" border="1" cellpadding="0" cellspacing="0" style="border:1px solid black;font-size:10px" align="center" rules="all" class="rpt_table">
                <thead>	
                    <tr bgcolor="#F0F0B8" height="30">
						<th colspan=10  style="font-size:11px"><b><? echo $company_details[$rows['company_id']]; ?> <br /> Department :  <? echo $department_details[$rows['department_id']]; ?></b></th>
					</tr>
					<tr bgcolor="#FBFAFC" height="30">
						<td colspan=10><b>ID Card:</b> <? echo $rows[id_card_no];?>&nbsp;&nbsp;&nbsp;<b>Emp Code:</b> <? echo $rows[emp_code];?>&nbsp;&nbsp;&nbsp;<b>Emp Name:</b> <? echo $rows[name];?>&nbsp;&nbsp;&nbsp;<b>Designation:</b> <? echo $designation_chart[$rows['designation_id']];?>&nbsp;&nbsp;&nbsp;<b>Joining Date:</b> <? echo convert_to_mysql_date($rows[joining_date]);?></td>
					</tr>
					<tr>
						<th colspan=10></th>
					</tr>
					<tr height="40">
						<th width='60'><b>Date</b></th>
                        <th width='60'><b>Day</b></th>
						<th width=''><b>Shift</b></th>
						<th width='70'><b>Shift in </b></th>
						<th width='70'><b>Shift Out </b></th>
						<th width='70'><b>Actual In</b></th>
						<th width='70'><b>Actual Out</b></th>
						<th width='60'><b>Late Min</b></th>
						<th width='60'><b>OT Hour</b></th>
						<th width='50'><b>Status</b></th>
					</tr>					
				</thead>
				<?
				$holiday=0;$weekend=0;$working_day=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_days=0;$ot=0;
				while( $row = mysql_fetch_assoc( $result ) ) 					
				{					
					// convert minutes to hours:minutes
					// $total_hours = sprintf("%d hours %02d minutes", abs((int)($row["total_over_time_min"]/60)), abs((int)($row["total_over_time_min"]%60)));	
									
					$hours[0] = abs((int)($row["total_over_time_min"]/60)); //hours
					$hours[1] = abs((int)($row["total_over_time_min"]%60)); //minutes
					
					$ot_mins_cal=0;
					if($ot_fraction==1) //ot fraction allow
					{						
						if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
						else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;
					}
					if($ot_fraction==2) // no ot fraction
					{						
						//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
						if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;						
					}
					if($ot_fraction==3) //at actual minutes
					{						
						$ot_mins_cal = $row["total_over_time_min"];				
					}
					
					$total_hours= get_buyer_ot_hr($row["total_over_time_min"],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
					
					//$total_hours = sprintf("%d hrs %02d mins", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
					
					if($row['r_sign_in_time']!='00:00:00'){$r_in = DATE("g:i a", STRTOTIME($row[r_sign_in_time]));}else $r_in="00:00";
					if($row['r_sign_out_time']!='00:00:00'){$r_out = DATE("g:i a", STRTOTIME($row[r_sign_out_time]));}else $r_out="00:00";
					if($row['sign_in_time']!='00:00:00'){$s_in = DATE("g:i a", STRTOTIME($row[sign_in_time]));}else $s_in="00:00";
					if($row['sign_out_time']!='00:00:00'){$s_out = DATE("g:i a", STRTOTIME($row[sign_out_time]));}else $s_out="00:00";
					if ($row[status]=='W' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH'){ $bgcolor="#CCCCCC";} else { $bgcolor="#FFFFFF";}
					?>		
                    <tr height="30" bgcolor="<? echo $bgcolor; ?>">
                        <td width=''><? echo $row[attnd_date];?></td>
                        <td width='' align="center"><? echo date("D",strtotime($row[attnd_date]));?></td>
                        <td width=''><? echo $shift_arr[$row[policy_shift_id]];?></td>						
                        <td width=''><? echo show_time_format($r_in,$in_out_time_format);?></td>
                        <td width=''><? echo show_time_format($r_out,$in_out_time_format);?></td>
                        <td width=''>
						<?php 
							if ($applicable_above_salary!=0)
							{
								if ($row['sign_in_time']!="00:00:00") 
									echo  show_time_format(DATE("g:i a", STRTOTIME(get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $adjust_in_time ))),$in_out_time_format); //else echo DATE("g:i a",STRTOTIME($row[sign_in_time]));
								else echo "00:00";
							}
							else
							{
								if ($row['sign_in_time']!="00:00:00") echo show_time_format(DATE("g:i a", STRTOTIME($row[sign_in_time])),$in_out_time_format);
								else echo "00:00";
							}
							//if ($applicable_above_salary!=0) echo ;;?></td>						
							<td width=''><? 
							if ($applicable_above_salary!=0)
							{
								if ($row['sign_out_time']!="00:00:00") echo show_time_format(DATE("g:i a", STRTOTIME(get_buyer_out_time($row['sign_out_time'], $row['r_sign_out_time'], $adjust_out_time, 1440, $buyer_ot_hours,$ot_start_min,$one_hr_ot_unit,$rows['ot_entitled']))),$in_out_time_format);
								else echo "00:00";
							}
							else
							{
								if ($row['sign_out_time']!="00:00:00") echo show_time_format(DATE("g:i a", STRTOTIME($row[sign_out_time])),$in_out_time_format);
								//echo check_zero_value(show_time_format(DATE("g:i a", STRTOTIME(get_out_time_jm($row['sign_out_time'], $row['r_sign_out_time'], $buyer_ot_hours))),$in_out_time_format),$row[status],1);
								else echo "00:00";
							}
							?>
                        </td> 	
                        <td width=''><? echo $row[late_time_min];?></td>
                        <td width=''><? echo $total_hours;?></td>
                        <td width=''><? echo ($row[status]=="CH" ? "H" : $row[status]);?></td>
                    </tr>
					<?			
					if($row[status]=='CL'){$cl++;}
					if($row[status]=='SL'){$sl++;}
					if($row[status]=='ML'){$ml++;}
					if($row[status]=='EL'){$el++;}
					if($row[status]=='LWP'){$lwp++;}
					if($row[status]=='SpL'){$spl++;}
					if($row[status]=='H' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH' ){$holiday++;}
					if($row[status]=='W'){$weekend++;}
					if($row[status]=='P'){$present++;}
					if($row[status]=='D'){$late++;}
					if($row[status]=='MR'){$movement++;}
					if($row[status]=='A'){$absent++;}
					$total_days++;
					$tmppd=explode(".",$total_hours);
					$hr+=$tmppd[0];
					$min+=$tmppd[1];
					//if($row[total_over_time_min]!=''){$ot += $total_hours;}
				}
				$working_day=$total_days-$holiday-$weekend;
				//$overtime=$ot;		//number_format($ot/60,2,'.','');
				//$overtime = sprintf("%d hrs %02d mins", abs((int)($ot/60)), abs((int)($ot%60)));	
				?>
                <tr><td colspan=10 height="25" align="center"><b>Summary</b></td></tr>
                <tr bgcolor="#A5C3EB" height="30">
                    <td colspan=10 height="25">
                        <b>Total Day:</b><? echo $total_days;?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Weekend Day:</b><? echo $weekend;?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Holiday:</b><? echo $holiday;?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Working Day:</b><? echo $working_day;?> &nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Present:</b><? echo $present;?> &nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Late Present:</b><? echo $late;?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Movement:</b><? echo $movement;?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Total OT:</b>
						<?php  
							$ot =($hr+ floor($min/60)).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
							echo $ot;
							$ot=0; 
							$min=0;
							$hr=0;
						?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Payable Days:</b><? echo $total_days-$absent;?>&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr bgcolor="#A5C3EB"  height="30">
                    <td colspan=10>
                        <b>Absent:</b><? echo $absent;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Casual Leave:</b><? echo $cl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Sick Leave:</b><? echo $sl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Earn Leave:</b><? echo $el;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Special Leave:</b><? echo $spl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Leave Without Pay:</b><? echo $lwp;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Maternity Leave:</b><? echo $ml;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>	
                </tr>
            </table>
			<p></p>
			<?   
			}//end while condition
		}// end if condition
    }
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$start_1 = substr($row["starting_date"],0,3);
		$start_2 = substr($row["starting_date"],-2,2);

		$end_1 = substr($row["ending_date"],0,3);
		$end_2 = substr($row["ending_date"],-2,2);
		
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$start_1." ".$start_2." - ".$end_1." ".$end_2."</option>";
		//echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}