<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

$user_only = $_SESSION["user_name"];
$date_time=date('Y-m-d H:i:s');
extract( $_GET );

function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

	//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			//echo $key."==".$value."<br>";
			$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//location_details
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//division_details
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//department_details
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//section_details
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//subsection_details
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//designation_chart
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
// male_female_ratio
if($action=='male_female_ratio')
{
	//echo "su..re";
	$company		=$company_id;
	$location		=$location_id;
	$division		=$division_id;
	$department		=$department_id;
	$section		=$section_id;
	$subsection		=$subsection_id;
	$designation	=$designation_id;
	
	if ($category_id=='') 	$category_id ="";	else 	$category_id 	= "and category='$category_id'";
	if ($company_id==0) 	$company_id=""; 	else 	$company_id		= "and company_id='$company_id'";
	if ($location_id==0) 	$location_id="";	else 	$location_id	= "and location_id='$location_id'";
	if ($division_id==0) 	$division_id=""; 	else 	$division_id	= "and division_id='$division_id'";
	if ($department_id==0) 	$department_id=""; 	else  	$department_id	= "and department_id='$department_id'";
	if ($section_id==0) 	$section_id=""; 	else 	$section_id		= "and section_id='$section_id'";
	if ($subsection_id==0)	$subsection_id=""; 	else 	$subsection_id	= "and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id="";	else	$designation_id	= "and designation_id='$designation_id'";
	
	ob_start();	
	?>    
	<table cellpadding="0" cellspacing="0" border="1" width="750" class="rpt_table" rules="all">        
		<?php	
        $sql = "SELECT *, 
            COUNT(CASE WHEN status_active=1 THEN id END) as total_emp,
            COUNT(CASE WHEN sex=0 THEN id END) as total_male,
            COUNT(CASE WHEN sex=1 THEN id END) as total_female 
            FROM hrm_employee 
            WHERE 
            status_active=1 
            $category_id 
            $company_id 
            $location_id 
            $division_id 
            $department_id 
            $section_id 
            $subsection_id 
            $designation_id";
        //echo $sql;die;
        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
        $i=0;
        $new_company=array();
        $grand_total_emp = 0;
        $grand_total_male = 0;
        $grand_total_female = 0;	
        $grand_total_male_in_per = 0;
        $grand_total_female_in_per = 0;
        $caption_name="";
        $caption_value="";
        while($row = mysql_fetch_array($result) )
        {								
			if($designation_id!='')
			{
				$caption_name.="Designation Name";
				$caption_value.=$designation_details[$designation]['designation_name'];
			}
			else if($subsection_id!='')
			{
				$caption_name.="Subsection Name";
				$caption_value.=$subsection_details[$subsection]['subsection_name'];
			}
			else if($section_id!='')
			{
				$caption_name.="Section Name";
				$caption_value.=$section_details[$section]['section_name'];
			}
			else if($department_id!='')
			{
				$caption_name.="Department Name";
				$caption_value.=$department_details[$department]['department_name'];
			}
			else if($division_id!='')
			{
				$caption_name.="Division Name";
				$caption_value.=$division_details[$division]['division_name'];
			}
			else if($location_id!='')
			{
				$caption_name.="Location Name";
				$caption_value.=$location_details[$location]['location_name'];
			}
			
			
			if (in_array($row['company_id'], $new_company))
			{
				$i++;
			}
			else
			{
				if($i>0)
				{
				?>						
					<tr>
						<td colspan="2"><b>Grand Total</b></td>
						<td><? echo $grand_total_emp; ?></td>
						<td><? echo $grand_total_male; ?></td>
						<td><? echo $grand_total_female; ?></td>
						<td><? echo $grand_total_male_in_per; ?></td>
						<td><? echo $grand_total_female_in_per; ?></td>
					</tr>
					<?
					$grand_total_emp = 0;
					$grand_total_male = 0;
					$grand_total_female = 0;	
					$grand_total_male_in_per = 0;
					$grand_total_female_in_per = 0;	
				}
				$i=0;
				?>                            								 
				<tr bgcolor="#A6CAF0">
					<td colspan="7" align="center" height="60px" valign="middle"><font size="+1">
						<b><? echo $company_details[$company]['company_name']; ?></b></font>
						<br /><span><b>Employee Male Female Ratio</b></span>
					</td>
				</tr>
				<tr>
					
				</tr>
				<tr class="display" style="font-weight:bold" bgcolor="#A6CAF0">
					<td width="40" align="center"><b>SL</b></td> 
					<td width="200" align="center"><b><?php echo $caption_name;?></b></td>
					<td width="110" align="center"><b>Total Employee</b></td>											
					<td width="100" align="center"><b>Male</b></td>
					<td width="100" align="center"><b>Female</b></td>	 							
					<td width="100" align="center"><b>Male(%)</b></td>
					<td width="100" align="center"><b>Female(%)</b></td>												 									
				</tr>
				<?
				$new_company[$i]=$row[company_id];
				$i++;
			}
			?>				                       				
			<tr class="display">
				<td width="40" align="center"><? echo $i; ?></td> 
				<td width="160" align="center"><? echo $caption_value;?></td>                           					
				<td width="100" align="center"><? echo $row['total_emp']; ?></td>
				<td width="100" align="center"><? echo $row['total_male']; ?></td>
				<td width="100" align="center"><? echo $row['total_female']; ?></td>
				<?
				$male_in_per = number_format(($row['total_male']*100)/$row['total_emp']);
				$female_in_per = number_format(($row['total_female']*100)/$row['total_emp']);								
				?>	 								
				<td width="100" align="center"><? echo $male_in_per; ?></td>
				<td width="100" align="center"><? echo $female_in_per; ?></td>                                                   			 
			</tr>	
			<? 
			$grand_total_emp += $row['total_emp'];
			$grand_total_male += $row['total_male'];
			$grand_total_female += $row['total_female'];
			$male_in_per = number_format(($grand_total_male*100)/$grand_total_emp);
			$female_in_per = number_format(($grand_total_female*100)/$grand_total_emp);		
			$grand_total_male_in_per = $male_in_per;
			$grand_total_female_in_per = $female_in_per;
		} 
		?>             
	</table>
    <div style="height:70px;"></div>
	<table width="750">           		
        <tr>
            <td width="187" align="center" style="text-decoration:overline;"><b>Officer IT</b></td>
            <td width="187" align="center" style="text-decoration:overline;"><b>Admin/HRM</b></td>
            <td width="187" align="center" style="text-decoration:overline;"><b>PM/APM</b></td>						
            <td width="187" align="center" style="text-decoration:overline;"><b>GM/DGM/AGM/FM</b></td>
        </tr>
	</table>		
	<?		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
}
?>