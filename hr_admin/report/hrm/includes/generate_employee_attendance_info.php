<?php
/***************************
Complted by:md ekram hossain
Date:13/03/2014
*******************************/
	date_default_timezone_set('Asia/Dhaka');
	session_start();
	
	include('../../../../includes/common.php');
	include('../../../../includes/array_function.php');
	include('../../../../includes/common_functions.php');
	
	extract( $_REQUEST );

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}

	$sql = "SELECT * FROM  lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_chart[$row['id']] = mysql_real_escape_string($row['shift_name']);
	}
	
if( $action == "attendace_information" ) 
{ 
	//echo $cbo_status;
	//Punch Report
	if ($category_id=='') $category_id =""; else $category_id ="and category='$category_id'";
	if ($company_id==0){ $company_id="";}else{$company_id=" company_id='$company_id'";}
	if ($location_id==0 || $location_id=='') $location_id=""; else $location_id="and location_id='$location_id'"; 
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and  department_id in ($department_id)";   
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and section_id in ($section_id)"; 
	if ($subsection_id==0 || $subsection_id=='') $subsection_id="";else $subsection_id="and subsection_id in ($subsection_id)";
	if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and designation_id in ($designation_id)";
	if ($salary_based==0 || $salary_based=='') $salary_based=""; else $salary_based="and salary_type_entitled='$salary_based'";
	if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
	$first_date=date("Y-m-01",strtotime($txt_from_date));
	$last_date = date('Y-m-t', strtotime($txt_to_date));
	
	$total_months=datediff('m',$txt_from_date,$txt_to_date);
	//echo $first_date;die;
	
	for ($j=0;$j<=$total_months;$j++)
	{
		$all_month[$j]=date("Y-m",strtotime(add_month($txt_from_date,$j)));
	}
	//print_r($all_month);die;
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	
	if($group_by_id==0){$groupby="emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,emp_code,";}
	
	//new 
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(id_card_no), 5)as SIGNED)";}	 
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
	
	$location_info_sql = "select * from lib_location order by id";
	$result = mysql_query( $location_info_sql ) or die( $location_info_sql . "<br />" . mysql_error() );
	$location_info_result = mysql_fetch_array($result);	
	
	$company_info_sql = "select * from lib_company order by id";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);	
	
	$st='';
	
	$status_id=explode(",",$cbo_status );
	$status_id_count=count($status_id);
	$status=array();
	$status_val_arr=array();
	foreach($status_id as $val)
	{
		 //if($cbo_status==0 || $cbo_status==""){
			//if($st=="") $st="'P','A','D','CL','ML','SL','EL','SpL','LWP','MR','W','CH','FH','GH'"; else $st.=",'P','A','D','CL','ML','SL','EL','SpL','LWP','MR','W','CH','FH','GH'";
			//}
		if($val==1){
			if($st=="") $st="'P'"; else $st.=",'P'";
			$status[$val]="Present";
			$status_val_arr['P']=1;
			}
		else if($val==2){
			if($st=="") $st="'A'"; else $st.=",'A'";
			$status[$val]="Absent";
			$status_val_arr['A']=2;
			}
		else if($val==3){
			if($st=="") $st="'D'"; else $st.=",'D'";
			$status[$val]="Late";
			$status_val_arr['D']=3;
			}
		else if($val==4){
			if($st=="") $st="'CL','ML','SL','EL','SpL','LWP'"; else $st.=",'CL','ML','SL','EL','SpL','LWP'";
			$status[$val]="Leave";
			$status_val_arr['L']=4;
			}
		else if($val==5){
			if($st=="") $st="'MR'"; else $st.=",'MR'";
			$status[$val]="Movement";
			$status_val_arr['MR']=5;
			}
		else if($val==6){
			if($st=="") $st="'FH','GH','CH'"; else $st.=",'FH','GH','CH'";
			$status[$val]="Holiday";
			$status_val_arr['H']=6;
			}
		else if($val==7){
			if($st=="") $st="'W'"; else $st.=",'W'";
			$status[$val]="Weekend";
			$status_val_arr['W']=7;
			}
			
	}
 	//echo $st;die;
	
	ob_start();
 	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$cal_span=(9+$total_months+1);
	$table_width=(($total_months+1)*40)+1000;
	$sl=0;
	$ct=0;
	$r=1;
	?>			
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="<? echo $table_width; ?>" rules="all" >
        <thead> 	
            <tr>
                <th colspan="<? echo $cal_span; ?>" align="center" bordercolor="#FFFFFF"><br /><font size="+1"><? echo  $company_info_result["company_name"];?></font><br />
					<? echo $location_info_result["location_name"]; ?><br />
                    Employee attendance Information<br /> 
                    Date between :<? echo convert_to_mysql_date($txt_from_date); ?>  To  <?  echo convert_to_mysql_date($txt_to_date); ?><br /><br />
                </th>
            </tr>
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="100"><b>Employee ID</b></th>
                <th width="100"><b>Employee code</b></th>
                <th width="130"><b>Employee Name</b></th>
                <th width="80"><b>Joining Date</b></th>
                <th width="130"><b>Designation</b></th>
                <th width="100"><b>Perticulars</b></th>
                <?
                for ($i=0; $i<=$total_months; $i++)
                { 
					?>	
					<th valign="middle" width="40"><? echo date("M",strtotime($all_month[$i]))."'".date("y",strtotime($all_month[$i])); ?></th>
					<?
                }
				?>
                <th width="80"><b>Total Count</b></th>
                <th width="200"><b>Date</b></th>
            </tr>
        </thead>
         <tbody> 	
		<?	
		$sql= "SELECT emp_code,status, group_concat(' ',DATE_FORMAT(attnd_date, '%d-%M') ORDER BY attnd_date ASC) as attnd_date,";
		for ($n=0;$n<=$total_months; $n++)
		{
			$query_month=date("Y-m"."%%",strtotime($all_month[$n]));
			if ($n!=$total_months)
			$sql.="count(CASE WHEN attnd_date like '$query_month' THEN status END) AS 'status".$n."',
			group_concat(' ', CASE WHEN attnd_date like '$query_month' THEN DATE_FORMAT(attnd_date, '%d') END ORDER BY attnd_date ASC) AS 'att_date".$n."',";
			else
			$sql.="count(CASE WHEN attnd_date like '$query_month' THEN status END) AS 'status".$n."',
			group_concat(' ', CASE WHEN attnd_date like '$query_month' THEN DATE_FORMAT(attnd_date, '%d')  END ORDER BY attnd_date ASC) AS 'att_date".$n."'";
		}
		$sql.=" from hrm_attendance WHERE  attnd_date between '$first_date' and '$last_date' and status in ($st) $emp_code group by status,emp_code order by attnd_date";
			
		//echo $sql;	
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$status_arr = array();
		$date_arr = array();
		while( $row = mysql_fetch_array( $result ) ) 
		{
			//echo $row['status'].",";
			 $rowstatus="";
			if($row['status']=='CL' || $row['status']=='ML' || $row['status']=='SL' || $row['status']=='EL' || $row['status']=='SpL' || $row['status']=='LWP')
			{
				$tmp="L";
			}
			else if($row['status']=='FH' || $row['status']=='GH' || $row['status']=='CH')
			{
				$tmp="H";
			}
			else $tmp=$row['status'];
			
			//echo $tmp.",";
			 
			for($z=0; $z<=$total_months; $z++)
			{
				$query_month_val=date("Y-m",strtotime($all_month[$z])); 
				$status_arr[$row['emp_code']][$query_month_val][$status_val_arr[$tmp]] = $row['status'.$z];
				$date_arr[$row['emp_code']][$query_month_val][$status_val_arr[$tmp]] = $row['att_date'.$z];
			}
			$status_arr[$row['emp_code']][$status_val_arr[$tmp]]['date']= mysql_real_escape_string($row['attnd_date']);
		}
		 
		
		$sql_mst = "SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name,id_card_no,punch_card_no,emp_code,company_id,location_id,division_id,
		department_id,section_id,subsection_id,joining_date,designation_id,salary_type_entitled,category  		     
		FROM 
		hrm_employee 
		WHERE 
			$company_id
			$category_id
			$location_id  
			$division_id 
			$department_id 
			$section_id 
			$subsection_id 
			$designation_id
			$salary_based
			$id_card_no
			$emp_code 
		$orderby";
		//echo $sql_mst;
		
		
		///  group by $dynamic_groupby
		$result = mysql_query( $sql_mst ) or die( $sql_mst . "<br />" . mysql_error() );
		
		while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
		{ 
		
			$total_pre_emp_count=0;
			foreach($status_id as $val)
			{
				for($z=0; $z<=$total_months; $z++)
				{ 
					$query_month_val=date("Y-m",strtotime($all_month[$z])); 
					$total_pre_emp_count+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
				}
			}
			//echo $total_pre_emp_count;
			if($total_pre_emp_count>0)
			{
				$sl++;
				//start header print---------------------------------//
				if($sl==1)
				{
					$location_arr[$row_emp[location_id]]=$row_emp[location_id];
					$division_arr[$row_emp[division_id]]=$row_emp[division_id];
					$department_arr[$row_emp[department_id]]=$row_emp[department_id];
					$section_arr[$row_emp[section_id]]=$row_emp[section_id];
					$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
				}//end if condition of header print
				
				if($sl!=1)
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
					if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row_emp[section_id]]=$row_emp[section_id];
											$subsection_arr=array();
											$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row_emp[department_id]]=$row_emp[department_id];
										$section_arr=array();
										$subsection_arr=array();
										$section_arr[$row_emp[section_id]]=$row_emp[section_id];
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row_emp[division_id]]=$row_emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row_emp[department_id]]=$row_emp[department_id];
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row_emp[location_id]]=$row_emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row_emp[division_id]]=$row_emp[division_id];
						$department_arr[$row_emp[department_id]]=$row_emp[department_id];
						$section_arr[$row_emp[section_id]]=$row_emp[section_id];
						$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
				}
				if($new_subsec==1 && $status_subsec==1 && $sl!=1)
				{
					?>
                	<tr style="font-weight:bold;">
						<td width='' colspan="7"><b> Subsection Total</b></td>
						<?
                        for($z=0; $z<=$total_months; $z++)
                        { 
							$query_month_val=date("Y-m",strtotime($all_month[$z])); 
							?>	
							<td align="center" width="40"><? echo $subsection_total_per_emp_status[$query_month_val]; ?></td>
							<?
							$subsection_total_per_emp_status[$query_month_val]=0;
                        }
                        ?>
						<td align="center"> <? echo $subsection_total_status; ?></td>
						<td>&nbsp;</td>		
					</tr>
					<?
					$subsection_total_status=0;	
				}
			
				if($new_sec==1 && $status_sec==1 && $sl!=1)
				{
					?>
                	<tr style="font-weight:bold;">
						<td width='' colspan="7"><b>Section Total</b></td>
                        <?
                        for($z=0; $z<=$total_months; $z++)
                        { 
							$query_month_val=date("Y-m",strtotime($all_month[$z])); 
							?>	
							<td align="center" width="40"><? echo $section_total_per_emp_status[$query_month_val]; ?></td>
							<?
							$section_total_per_emp_status[$query_month_val]=0;
                        }
                        ?>
						<td align="center"> <? echo $section_total_status; ?></td>
						<td>&nbsp;</td>
					</tr>
					<?
					$section_total_status=0;
				}
			
				if($new_dept==1 && $status_dept==1 && $sl!=1)
				{
					?>
                	<tr style="font-weight:bold;">
                        <td  colspan="7">Department Total</td>
                        <?
                        for($z=0; $z<=$total_months; $z++)
                        { 
							$query_month_val=date("Y-m",strtotime($all_month[$z])); 
							?>	
							<td align="center" width="40"><? echo $department_total_per_emp_status[$query_month_val]; ?></td>
							<?
							$department_total_per_emp_status[$query_month_val]=0;
                        }
                        ?>
                        <td align="center" > <? echo $department_total_status; ?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <?
                    $department_total_status=0;	
				}
			 
				if($new_divis==1 && $status_divis==1 && $sl!=1)
				{
					?>
                	<tr style="font-weight:bold;">
                        <td  colspan="7" >Division Total</td>
                        <?
                        for($z=0; $z<=$total_months; $z++)
                        { 
							$query_month_val=date("Y-m",strtotime($all_month[$z])); 
							?>	
							<td align="center" width="40"><? echo $division_total_per_emp_status[$query_month_val]; ?></td>
							<?
							$division_total_per_emp_status[$query_month_val]=0;
                        }
                        ?>
                        <td align="center"> <? echo $division_total_status; ?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <?
					$division_total_status=0;			
				}
			 
				if($new_loc==1 && $status_loc==1 && $sl!=1)
				{
					?>
                    <tr style="font-weight:bold;">
                        <td colspan="7">Location Total</td>
                        <?
                        for($z=0; $z<=$total_months; $z++)
                        { 
							$query_month_val=date("Y-m",strtotime($all_month[$z])); 
							?>	
							<td align="center" width="40"><? echo $location_total_per_emp_status[$query_month_val]; ?></td>
							<?
							$location_total_per_emp_status[$query_month_val]=0;
                        }
                        ?>
                        <td align="center"> <? echo $location_total_status; ?></td>
                        <td>&nbsp;</td>
                    </tr>
					<?
					$location_total_status=0;				
				}
			
				//header print here 
				$c_part_1="";
				if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
				{
					/*if($status_loc==1)
					{					
						$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
					}	*/			
					if($status_divis==1)
					{
						$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
					}
					if($status_dept==1)
					{
						$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
					}
					if($status_sec==1)
					{
						$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
					}
					if($status_subsec==1)
					{
						$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
					}
			
					if($c_part_1!='')
					{
						$i=0;$sl=1;
						?><tr><td colspan="<? echo $cal_span; ?>"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
					}		
				}
			
				if ($r%2==0) $bgcolor="#EEEEEE"; else $bgcolor="#FFFFFF";
				$k=1;
				 foreach($status_id as $val)
				 {
				 ?>
					<tr bgcolor="<? echo $bgcolor;?>">
						<? 
                        if($k==1)
                        {
							?>
							<td valign="middle" rowspan="<? echo $status_id_count;?>"><? echo $r;?> </td>
							<td valign="middle" rowspan="<? echo $status_id_count;?>"><? echo $row_emp[id_card_no]; ?></td>
							<td valign="middle" rowspan="<? echo $status_id_count;?>"><? echo $row_emp[emp_code]; ?></td>
							<td valign="middle" rowspan="<? echo $status_id_count;?>"><? echo $row_emp[name];?></td>
							<td valign="middle" rowspan="<? echo $status_id_count;?>"><? echo convert_to_mysql_date($row_emp[joining_date]);?></td>
							<td valign="middle" rowspan="<? echo $status_id_count;?>"><? echo $designation_chart[$row_emp['designation_id']];?></td>
							<?
							$k++;
                        }
                        ?>
                            <td><? echo $status[$val];?></td>
							<?
                            $total_pre_emp=0;
                            for($z=0; $z<=$total_months; $z++)
                            { 
								$query_month_val=date("Y-m",strtotime($all_month[$z])); 
								?>	
								<td align="center"><? echo $status_arr[$row_emp[emp_code]][$query_month_val][$val]; ?></td>
								<?
								$total_pre_emp+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
								
								$grand_total_per_emp_status[$query_month_val]+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
								$subsection_total_per_emp_status[$query_month_val]+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
								$section_total_per_emp_status[$query_month_val]+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
								$department_total_per_emp_status[$query_month_val]+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
								$division_total_per_emp_status[$query_month_val]+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
								$location_total_per_emp_status[$query_month_val]+=$status_arr[$row_emp[emp_code]][$query_month_val][$val];
                            }
                            ?>
                            <td align="center"><? echo $total_pre_emp;?></td>						
                            <td><p> 
                            <? 
                            for($z=0; $z<=$total_months; $z++)
                            {
								$query_month_val=date("Y-m",strtotime($all_month[$z]));
								if($date_arr[$row_emp[emp_code]][$query_month_val][$val]!=0)
								{
									echo date("M",strtotime($query_month_val))."'".date("Y",strtotime($query_month_val))." : ".$date_arr[$row_emp[emp_code]][$query_month_val][$val]."<br>";
								}
                            }
                            ?>
                            </p></td>
                    </tr> 
					<?
						$subsection_total_status += $total_pre_emp;
						$section_total_status += $total_pre_emp;
						$department_total_status += $total_pre_emp;
						$division_total_status += $total_pre_emp;
						$location_total_status += $total_pre_emp;
						$grand_total_status +=  $total_pre_emp;
					
					 }
					 
					$ct++;
					$r++;
				}// end bleank condition chk
			}//while loop end here 
			
			if($status_subsec==1)
			{
				?>
				<tr style="font-weight:bold;">
					<td width='' colspan="7"><b> Subsection Total</b></td>
					<?
					for($z=0; $z<=$total_months; $z++)
					{ 
						$query_month_val=date("Y-m",strtotime($all_month[$z])); 
						?>	
						<td align="center" width="40"><? echo $subsection_total_per_emp_status[$query_month_val]; ?></td>
						<?
						$subsection_total_per_emp_status[$query_month_val]=0;
					}
					?>
					<td align="center"> <? echo $subsection_total_status; ?></td>
					<td>&nbsp;</td>		
				</tr>
				<?
				$subsection_total_status=0;	
			}
		
			if($status_sec==1)
			{
				?>
                <tr style="font-weight:bold;">
                    <td width='' colspan="7"><b>Section Total</b></td>
                    <?
                    for($z=0; $z<=$total_months; $z++)
                    { 
                        $query_month_val=date("Y-m",strtotime($all_month[$z])); 
                        ?>	
                        <td align="center" width="40"><? echo $section_total_per_emp_status[$query_month_val]; ?></td>
                        <?
						$section_total_per_emp_status[$query_month_val]=0;
                    }
                    ?>
                    <td align="center"> <? echo $section_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
                $section_total_status=0;
			}
		
			if($status_dept==1 )
			{
				?>
                <tr style="font-weight:bold;">
                    <td  colspan="7">Department Total</td>
                    <?
                    for($z=0; $z<=$total_months; $z++)
                    { 
                        $query_month_val=date("Y-m",strtotime($all_month[$z])); 
                        ?>	
                        <td align="center" width="40"><? echo $department_total_per_emp_status[$query_month_val]; ?></td>
                        <?
						$department_total_per_emp_status[$query_month_val]=0;
                    }
                    ?>
                    <td align="center"> <? echo $department_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
                $department_total_status=0;	
			}
		 
			if($status_divis==1 )
			{
				?>
                <tr style="font-weight:bold;">
                    <td  colspan="7" >Division Total</td>
                    <?
                    for($z=0; $z<=$total_months; $z++)
                    { 
                        $query_month_val=date("Y-m",strtotime($all_month[$z])); 
                        ?>	
                        <td align="center" width="40"><? echo $division_total_per_emp_status[$query_month_val]; ?></td>
                        <?
						$division_total_per_emp_status[$query_month_val]=0;
                    }
                    ?>
                    <td align="center"> <? echo $division_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
				$division_total_status=0;			
			}
		 
			if($status_loc==1 )
			{
			?>
                <tr style="font-weight:bold;">
                    <td colspan="7">Location Total</td>
                    <?
                    for($z=0; $z<=$total_months; $z++)
                    { 
                        $query_month_val=date("Y-m",strtotime($all_month[$z])); 
                        ?>	
                        <td align="center" width="40"><? echo $location_total_per_emp_status[$query_month_val]; ?></td>
                        <?
						$location_total_per_emp_status[$query_month_val]=0;
                    }
                    ?>
                    <td align="center"> <? echo $location_total_status; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
				$location_total_status=0;		
			}
			?>
             <tr  bgcolor="#CCCCCC" style="font-weight:bold;">
                <td  colspan="7">&nbsp;Grand Total</td>
				<?
                for($z=0; $z<=$total_months; $z++)
                { 
					$query_month_val=date("Y-m",strtotime($all_month[$z])); 
					?>	
					<td align="center"><? echo $grand_total_per_emp_status[$query_month_val]; ?></td>
					<?
                }
                ?>
                <td align="center"> <? echo $grand_total_status; ?></td>
                <td>&nbsp;</td>
            </tr> 
        </tbody>
	</table> 
    <?
	//previous file delete code-----------------------------//

	$html = ob_get_contents();
	ob_clean();		
	
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

function add_month($orgDate,$mon)
{
	$cd = strtotime($orgDate);
	$retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	return $retDAY;
}
?>