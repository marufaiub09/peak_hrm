<?php


/*##############################

By :ekram

date :2014-05-21

###############################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");
include("../../../../includes/array_function.php");
include("../../../../includes/common_functions.php");

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}

extract($_GET);


// Variable Settiings
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['not_pay_deduction'] = mysql_real_escape_string( $row['not_pay_deduction'] );
		$var_hrm_chart[$row['company_name']]['new_emp_card_charge'] = mysql_real_escape_string( $row['new_emp_card_charge'] );
		$var_hrm_chart[$row['company_name']]['ot_rate_multiplier'] = mysql_real_escape_string( $row['ot_rate_multiplier'] );
		$var_hrm_chart[$row['company_name']]['att_bonus_leave_type'] = mysql_real_escape_string( $row['att_bonus_leave_type'] ); 	 	 	
	}

if($type=="ot_based_on_report_list")
{
	
	
	$company=$company_id;
	$location=$location_id;
	
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
	 
	if ($category_id	== '')	$category_id	= "and b.category in(0,1,2,3,4)";	else	$category_id	= "and b.category='$category_id'";
	if ($company_id		== 0)	$company_id		= "";		else	$company_id		= "and b.company_id='$company_id'";
	if ($location_id	== 0) 	$location_id	= ""; 		else 	$location_id	= "and b.location_id='$location_id'";
	if ($division_id	== 0 || $division_id	== '')		$division_id	= "";		else	$division_id	= "and b.division_id in($division_id)";
	if ($department_id  == 0 || $department_id	== '')		$department_id	= "";		else	$department_id	= "and b.department_id in($department_id)";
	if ($section_id		== 0 || $section_id	== '') 			$section_id		= ""; 		else 	$section_id		= "and b.section_id in($section_id)";
	if ($subsection_id	== 0 || $subsection_id	== '')		$subsection_id	= "";		else	$subsection_id	= "and b.subsection_id in($subsection_id)";
	if ($designation_id == 0 || $designation_id	== '')		$designation_id	= "";		else	$designation_id	= "and b.designation_id in($designation_id)";
	if ($id_card_no		=="")	$id_card_no=""; else $id_card_no="and b.id_card_no in ($id_card_no)";
	if ($emp_code		=="") 	$emp_code=""; else $emp_code="and b.emp_code in ($emp_code)";
	if ($cbo_salary_based=="") 	$salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	
	
	ob_start();
?>

 <table cellspacing="0" cellpadding="0" width="1100px" class="rpt_table" id="table_body_head">
        <thead>
        <tr>
       	  <th colspan="11"><font size="+1"><? echo $company_details[$company]; ?></font><br /><? echo $location_details[$location]; ?><br />
			  <font size="+0"> OT Requisition List</font><br />
       		 </th>
        </tr>
        
        <tr>
            <th width="30" align="center"><strong>SL</strong></th>
            <th width="120" align="center"><strong>Emp ID</strong></th>
            <th width="120" align="center"><strong>Emp Code</strong></th>
            <th width="150" align="center"><strong>Employee Name</strong></th>
            <th width="120" align="center"><strong>Designation</strong></th>
            <th width="100" align="center"><strong>OT Date</strong></th>
            <th width="100" align="center"><strong>Budgeted OT (Hrs)</strong></th>
            <th width="80" align="center"><strong>Inserted By</strong></th>
            <th width="80" align="center"><strong>Inserted Date</strong></th>
            <!--<th width="80" align="center"><strong>Update By</strong></th>
            <th width="80" align="center"><strong>Update date</strong></th>-->
        </tr>   
        </thead>

<?
		
	//$sql="select * from ot_requisition_dtl where ot_date between '$txt_from_date' and '$txt_to_date' order by ot_date";
		

		
		
	$sql = "select a.*,CONCAT(b.first_name, ' ',b.middle_name, ' ',b.last_name) AS name,b.emp_code,b.id_card_no,b.designation_id,
	b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,
	b.category,b.ot_entitled,b.overtime_policy,b.shift_policy,b.duty_roster_policy
		from 
		ot_requisition_dtl a,hrm_employee b
		where
		a.emp_code=b.emp_code and
		a.ot_date between '$txt_from_date' and '$txt_to_date'  
		$emp_code $id_card_no  $category $company_id $location_id $division_id $section_id $subsection_id 
		$designation_id $department_id 
		";
		//echo $sql;
	
		$result=mysql_query($sql);
		$i=1;
		while ($row_emp = mysql_fetch_array($result))// EMp Master Qry starts 
		{ 
		if ($i%2==0) { $bgcolor="#EEEEEE"; } else { $bgcolor="#FFFFFF";}
		
		
		?>
		<tbody>
            <tr bgcolor="<? echo $bgcolor; ?>">
                <td width="30" align="center"><?php echo $i;?></td>
                <td width="120" align="center"><? echo $row_emp[emp_code];?></td>
                <td width="120" align="center"><? echo $row_emp[id_card_no];?></td>
                <td width="150" align="center"><? echo $row_emp[name];?></td>
                <td width="120" align="center"><? echo $designation_chart[$row_emp['designation_id']];?></td>
                <td width="100" align="center"> <? echo $row_emp[ot_date]; ?></td>
                <td width="80" align="center"><? 
                
                $ot_hrs=get_buyer_ot_hr($row_emp[budgeted_ot],$var_hrm_chart[$row_emp['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row_emp['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row_emp['company_id']]['ot_start_minute'], 1440);
                echo $ot_hrs;
                ?></td>
                <td width="80" align="center"> <? echo $row_emp[inserted_by];?></td>
                <td width="80" align="center"> <? echo $row_emp[insert_date];?></td>
                <!--<td width="80" align="center"> <? //echo $row_emp[updated_by];?></td>
                <td width="80" align="center"> <? //echo $row_emp[update_date];?></td>-->
            
            </tr>
        </tbody>
		<?
		
		$i++;
	}
		
		
		?>  
        
       
          </table> 
        <?
		
		
		$html = ob_get_contents();
	ob_clean();	
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) 
		{			
            @unlink($filename);
		}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
		
}


function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>