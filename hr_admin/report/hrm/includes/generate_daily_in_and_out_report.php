<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$emp_basic = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$emp_basic[$row['emp_code']] = array();
	foreach( $row AS $key => $value ) {
		$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
	}
}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


if($action=='daily_in_and_out_report')
{
 	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
 
 	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		 
		$in_out_time_format=$res['in_out_time_format']; 
	}
	
	 
	/*list( $day, $month, $year ) = explode( "-", $txt_date );
	$date = $year . "-" . $month . "-" . $day;
	$int_yestarday = strtotime ( '-1 day' , strtotime ( $date ) ) ;
	$yesterday = date ( 'd-m-Y' , $int_yestarday );*/
	
	list( $day, $month, $year ) = explode( "-", $txt_from_date );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_to_date );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	
	/*$var_to_date=convert_to_mysql_date($todate);
	$var_from_date=convert_to_mysql_date($fromdate);
	$var_hrm_chart=get_variable_settings( $cbo_company_id, $var_from_date, $var_to_date );*/
	
	
	
	//print_r($var_hrm_chart);
	//die;
	
	$company_id=$cbo_company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	/*if ($cbo_company_id==0) $cbo_company_id=""; else */$cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0 || $division_id=="") $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0 || $department_id=="") $department_id=""; else  $department_id="and a.department_id in ($department_id)";
	if ($section_id==0 || $section_id=="") $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=="") $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($designation_id==0 || $designation_id=="") $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and emp.salary_type_entitled='$cbo_salary_based'";
	if ($id_card=='') $id_card_no =""; else $id_card_no ="and emp.id_card_no in ($id_card_no)";
	//if ($emp_code=='') $emp_code =""; else $emp_code ="and a.emp_code in ($emp_code)";
	if ($emp_code_no=="") $emp_code_no=""; else $emp_code_no="and  a.emp_code in ('".implode("','",explode(",",$emp_code_no))."')";
	
	
	$addrs ="";		
	if($company_details[$company_id]['plot_no'] !='') $addrs .= $company_details[$company_id]['plot_no'].", "; 
	if($company_details[$company_id]['level_no'] !='') $addrs .= $company_details[$company_id]['level_no'].", "; 
	if($company_details[$company_id]['block_no'] !='') $addrs .= $company_details[$company_id]['block_no'].", "; 
	if($company_details[$company_id]['road_no'] !='') $addrs .= $company_details[$company_id]['road_no'].", "; 
	if($company_details[$company_id]['city'] !='') $addrs .= $company_details[$company_id]['city'].", "; 
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(emp.id_card_no, 5)";}
				
		//if($get_days>7) $get_days=7; else $get_days=$get_days;
		 $colspsn=(5+($get_days*2));
	ob_start();
	
	?>
	<table cellpadding="0" cellspacing="0" border="1" width="100%" class="rpt_table" style="border:1px solid #000; font-size:11px;" rules="all" > 
        <thead>
            <tr style="height:60px;">
            	<th colspan="<? echo $colspsn; ?>" bordercolor="#FFFFFF"><font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br /><? echo $addrs; ?><br />Employee's Daily In and Out Report</th>
            </tr>
                <tr class="display">
                <th width="30" rowspan="2">SL</th> 
                <th width="80" rowspan="2">Emp Code</th>
                <th width="80" rowspan="2">ID Card No</th>											
                <th width="150" rowspan="2">Name</th>
                <th width="150" rowspan="2">Designation</th>
                <?
				  
				  for($j=0;$j<$get_days;$j++)
				  {				
                    $newdate = date("Y-m-d",strtotime("+".$j." day", strtotime($fromdate)));
					?>	 							
					<th width="140" colspan="2">Date - <? echo convert_to_mysql_date($newdate);?></th>
					<?
				  }
				?>									
            </tr>
            	<tr>
                	<?
                    for($j=0;$j<$get_days;$j++)
				  	{				
					?>
                    <th width="70">In</th>
                    <th width="70">Out</th>
                    <?
					}
					?>
                </tr>
        </thead> 
	<?
		//echo $fromdate."=".$newdate;
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 and a.attnd_date between '$fromdate' and '$newdate' and a.emp_code=emp.emp_code  $category  $cbo_company_id $location_id $division_id $department_id   $section_id $subsection_id $designation_id $salary_based $id_card_no $emp_code_no $orderby";
		
		//(this part is reduce from quey on 23_01_2012$category group by $groupby )a.emp_code,a.attnd_date $orderby";
		//echo $sql;
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$tot_row=mysql_num_rows($result);
		if($tot_row==0)
		{
			echo "<div align='center'>".'Data Not Found'."</div>";exit();
		}
		
		$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		//$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		$i=1;
		$sl=0;	
		$emp_intime=array();$emp_outtime=array();
		while($row=mysql_fetch_array($result))
		{
			$emp_intime[$row[emp_code]][$row[attnd_date]]=$row[sign_in_time];
			$emp_outtime[$row[emp_code]][$row[attnd_date]]=$row[sign_out_time];
				
			if (!in_array($row[emp_code],$emp_code))
			{
				$emp_code[]=$row[emp_code];
				$emp_id_card[$row[emp_code]]=$row[id_card_no];
				$emp_name[$row[emp_code]]=$row[name];
				$emp_desig[$row[emp_code]]=$row[designation_id];
				$emp_loc[$row[emp_code]]=$row[location_id];
				$emp_div[$row[emp_code]]=$row[division_id];
				$emp_depart[$row[emp_code]]=$row[department_id];
				$emp_sec[$row[emp_code]]=$row[section_id];
				$emp_subsec[$row[emp_code]]=$row[subsection_id];
				$emp_company[$row[emp_code]]=$row[company_id];
				
			}
			//print_r($emp_code);
		}
				
		foreach ( $emp_code as $code )
		{
			$sl++;
			if($sl==1)
			{
				$location_arr[$emp_loc[$code]]=$emp_loc[$code];
				$division_arr[$emp_div[$code]]=$emp_div[$code];
				$department_arr[$emp_depart[$code]]=$emp_depart[$code];
				$section_arr[$emp_sec[$code]]=$emp_sec[$code];
				$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
			
			}//end if condition of header print
			if($sl!=1)
			{
				$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
				if(in_array($emp_loc[$code],$location_arr) && $status_loc==1)
				{
					if(in_array($emp_div[$code],$division_arr) && $status_divis==1)
					{
						if(in_array($emp_depart[$code],$department_arr) && $status_dept==1)
						{
							if(in_array($emp_sec[$code],$section_arr) && $status_sec==1)
							{ 
								if(in_array($emp_subsec[$code],$subsection_arr) && $status_subsec==1)
								{}
								else if($status_subsec==1)
								{
								$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
								$new_subsec=1;
								}
							}
							else if($status_sec==1)
							{
								$section_arr[$emp_sec[$code]]=$emp_sec[$code];
								$subsection_arr=array();
								$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
								$new_sec=1;
								$new_subsec=1;
							}
						}
						else if($status_dept==1)
						{
							$department_arr[$emp_depart[$code]]=$emp_depart[$code];
							$section_arr=array();
							$subsection_arr=array();
							$section_arr[$emp_sec[$code]]=$emp_sec[$code];
							$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
							$new_dept=1;
							$new_sec=1;
							$new_subsec=1;
						}
					}//division
					else if($status_divis==1)
					{
						$division_arr[$emp_div[$code]]=$emp_div[$code];
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$department_arr[$emp_depart[$code]]=$emp_depart[$code];
						$section_arr[$emp_sec[$code]]=$emp_sec[$code];
						$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//division else
				}//location
				else if($status_loc==1)
				{
					$location_arr[$emp_loc[$code]]=$emp_loc[$code];
					$division_arr=array();
					$department_arr=array();
					$section_arr=array();
					$subsection_arr=array();
					$division_arr[$emp_div[$code]]=$emp_div[$code];
					$department_arr[$emp_depart[$code]]=$emp_depart[$code];
					$section_arr[$emp_sec[$code]]=$emp_sec[$code];
					$subsection_arr[$emp_subsec[$code]]=$emp_subsec[$code];
					$new_loc=1;
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//location else
			}
			//header print here 
			$c_part_1="";
			if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
			{
				if($status_loc==1)
				{					
					$c_part_1 .= "  Location :  ".$location_details[$emp_loc[$code]][location_name]."  ,";
				}				
				if($status_divis==1)
				{
					$c_part_1 .= "  Division :  ".$division_details[$emp_div[$code]][division_name]."  ,";
				}
				if($status_dept==1)
				{
					$c_part_1 .= "  Department :  ".$department_details[$emp_depart[$code]][department_name]."  ,";
				}
				if($status_sec==1)
				{
					$c_part_1 .= "  Section :  ".$section_details[$emp_sec[$code]][section_name]."  ,";
				}
				if($status_subsec==1)
				{
					$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp_subsec[$code]][subsection_name]."  ,";
				}
				if($c_part_1!='')
				{
					//$i=0;
					$sl=1;
					?>
					<tr bgcolor="#CCCCCC">
						<td colspan="<? echo $colspsn; ?>"><b><?php echo substr($c_part_1,0,-1); ?></b></td>
					</tr>
					<?php      
				}
			}
			
			if ($i%2==0) $bgcolor="#EEEEEE"; 
			else $bgcolor="#FFFFFF";
			
			?>
							
			<tr class="display" bgcolor="<? echo $bgcolor;?>">
				<td><? echo $i; ?></td> 
				<td>&nbsp;<? echo $code;?></td>
				<td>&nbsp;<? echo $emp_id_card[$code]; ?></td>						
				<td><? echo $emp_name[$code]; ?></td>
				<td>&nbsp;<? echo $designation_chart[$emp_desig[$code]]['custom_designation']; ?></td>	
				<?
				//$var_hrm_chart[$emp_company[$code]][$newdate]['in_out_time_format']
                for($j=0;$j<$get_days;$j++)
                {
                     $newdate = date("Y-m-d",strtotime("+".$j." day", strtotime($fromdate)));				
                    ?>
                    <td><? echo show_time_format($emp_intime[$code][$newdate], $in_out_time_format);?></td>
                    <td><? echo show_time_format($emp_outtime[$code][$newdate], $in_out_time_format);?></td>
                    <?
                }
                ?>
			</tr>
			<?	
			$i++;
		}   

	?>
		<!--	</table>
				<table width="900" style="margin-top:80px">           		
					<tr>
						<td colspan="4" align="center">--------------------------</td>
						<td width="100">&nbsp;</td>
						<td colspan="4" align="center">--------------------------</td>
					</tr>
					<tr>
						<td colspan="4" align="center">Prepared By</td>
						<td>&nbsp;</td>
						<td colspan="4" align="center">Approved By</td>
					</tr>
				</table>		
           -->
	<?
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,trim($html));	
	
	echo "$html"."####"."$name";		
	exit();
	
}


//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 and b.is_locked=0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
	}		
	exit();
}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}


function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>

