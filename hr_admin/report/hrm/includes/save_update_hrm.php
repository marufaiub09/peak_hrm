
<script>$(".rpt_table tr:even").css("background", "#E9F3FF");$(".rpt_table tr:odd").css("background", "#FFFFFF");</script>
<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	
	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
 

if( $action == "monthly_attn_summery" ) {	
		
	if($txt_date!="")
	{
		$date_part=convert_to_mysql_date($txt_date);
		//$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	}
	else
	{
		$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	}
	
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and  emp.company_id='$cbo_company_id'";	
	if ($location_id==0) $location_id=""; else  $location_id="and  emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else  $division_id="and  emp.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and  emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and  emp.subsection_id='$subsection_id'";	
	if ($designation_id==0) $designation_id=""; else $designation_id="and  emp.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and  emp.category='$cbo_emp_category'";	
	if ($department_id==0) $department_id=""; else	$department_id=" and  emp.department_id='$department_id'";
	
	
	?>
    
	<table cellpadding="0" width="1050" cellspacing="0" border="1" class="rpt_table" rules="all">
	
	<?
			
			$new_dept=array();
			$new_section=array();
			$i=0;
			$sqls = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,atn.attnd_date FROM hrm_employee emp, hrm_attendance atn WHERE emp.emp_code=atn.emp_code and atn.attnd_date like '$date_part%' $location_id $cbo_company_id $section_id $subsection_id $designation_id $category $department_id  $division_id group by atn.emp_code order by emp.department_id,emp.section_id,RIGHT(emp.id_card_no, 4)";
			//echo $sqls;die;
			$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );						
			$emp_tot=mysql_num_rows($results);					
			if ($emp_tot>0)
				{						
					while( $rows = mysql_fetch_assoc( $results ) ) {
						
						if ($i%2==0)$bgcolor="#E9F3FF";  
						else $bgcolor="#FFFFFF";
						
						if (in_array($rows['section_id'], $new_section))						
							{
								$i++;
							}
							else
							{	
																
					?>
                    			<thead>
										<tr>
											<th colspan=19>
                                            	<b>Department Name : </b> <? echo $department_details[$rows['department_id']]['department_name']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                            	<b>Section Name : </b> <? echo $section_details[$rows['section_id']]['section_name']; ?>
                                            </th>												
										</tr>
										<tr>
											<th width='80'>Emp Code</th>
											<th width='80'>ID Card</th>
											<th width='190'>Emp Name</th>
											<th width='100'>Designation</th>
											<th width='70'>Join Date</th>
											
											<th width='30'>Total Days</th>
											<th width='50'>Present</th>
											<th width='45'>Absent</th>
											<th width='30'>Late</th>
											<th width='40'>Leave</th>
											<th width='30'>MR</th>
											
											<th width='30'>WOD</th>
											<th width='30'>HD</th>
											<th width='30'>PL</th>
											<th width='30'>PW</th>
											<th width='30'>PH</th>
											<th width='50'>Total Present</th>
											<th width='50'>Payable Days</th>
											<th>Job Card</th>
										</tr> 
								</thead>
							
							
							<?
									
									$new_section[$i]=$rows[section_id];
									$i++;
							}
							
				$present=0;$absent=0;$late=0;$leave=0;$whday=0;$ghday=0;$pl=0;$ph=0;$pw=0;$ot=0;$mr=0;				
				
			$sql = "SELECT * FROM hrm_attendance  WHERE  attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]' order by attnd_date  asc";	
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );			
			$r=0;			
			while( $row = mysql_fetch_assoc( $result ) ) 
			{
				if ($row['status']=="P")
				{
					$present=$present+1;
				}
				else if ($row['status']=="A")
				{
					$absent=$absent+1;
				}
				else if ($row['status']=="D")
				{
					$late=$late+1;
				}
				else if ($row['status']=="W")
				{
					$whday=$whday+1;
					if($row[sign_in_time]!='00:00:00' || $row[sign_out_time]!='00:00:00') 
					{
						$pw=$pw+1; 
					}
				}
				else if($row[status]=='H' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH' )
				{
					$ghday=$ghday+1;
					if($row[sign_in_time]!='00:00:00' || $row[sign_out_time]!='00:00:00') 
					{
						$ph=$ph+1; 
					}
					
				}
				else if ($row[status]=='' || $row[status]=="MR" )
				{
					$mr=$mr+1;
				}
				else
				{
					$leave=$leave+1;
					if($row[sign_in_time]!='00:00:00' || $row[sign_out_time]!='00:00:00') 
					{
						$pl=$pl+1; 
					}
				}
				$ot=$ot+$row['total_over_time_min'];
			}
			$total_present=$present+$late+$pl+$pw+$ph+$mr;
			$total_days=$present+$late+$absent+$whday+$ghday+$leave+$mr;
			$payable_days=$total_days-$absent;
			if($ot!=0)
			{
				$ot_hr=round(($ot/60),2);
			}
			if($late>2)
			{ 
				$color='#EE3900';
			}
			else
			{
				$color='';
			}				
			
		?>
					<tbody>
					<tr bgcolor="<? echo $bgcolor; ?>">
						<td width='80' height='50'><? echo $rows[emp_code]; ?></td>
						<td width='80'><? echo $rows[id_card_no]; ?></td>
						<td width='180'><? echo $rows[name]; ?></td>
						<td align='left' width='100'><? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td>
						<td width='80'><? echo convert_to_mysql_date($rows[joining_date]); ?></td>
						
						<td width='30'><? echo $total_days;?></td>
						<td width='50'><a href='#' onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=P&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"><? echo $present;?></a></td>
						<td width='45'><a href='#' onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=A&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"><? echo $absent;?></a></td>
						<td width='30' bgcolor="<? echo $color;?>">&nbsp;<a href='#' onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=D&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"><? echo $late;?></a></td>
						<td width='40'><a href='#' onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=CL&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"><? echo $leave;?></a></td>
						<td width='30'><a href='#' onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=MR&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"><? echo $mr;?></a></td>						
						<td width='30'><a href='#' onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=W&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"><? echo $whday;?></a></td>
						<td width='30'><a href='#' onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=H&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"><? echo $ghday;?></a></td>
						<td width='30'><? echo $pl;?></td>
						<td width='30'><? echo $pw;?></td>
						<td width='30'><? echo $ph;?></td>
						<td width='50'><? echo $total_present;?></td>
						<td width='50'><? echo $payable_days;?></td>
						<td><input type="button" value="View" style="width:35px" class="formbutton" onclick="openmypage('popup_report.php?emp_code=<? echo $rows[emp_code];?>&date=<? echo $date_part;?>&status=JC&emp_name=<? echo $rows[name];?>','Partial Attendance'); return false"/></td>
					</tr>
                    </tbody>
	<?			
					
			}		
		}	
	?>

			</table><br><br>

<?

	
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}






if( $action == "daily_in_out_report" ) { //Punch Report



//shift policy array
	//$qr_shift="select * from lib_policy_shift where status_active=1 and is_deleted=0 order by id";
	//$result_s = mysql_query( $qr_shift ) or die( $qr_shift . "<br />" . mysql_error() );
	//$shift_arr = array();
	//while($res_shift=mysql_fetch_array($result_s))		
		//{
			//$shift_arr[$res_shift["id"]]=mysql_real_escape_string($res_shift["shift_name"]);
		//}	
		
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$adjust_out_time=$res['adjust_out_time'];
			$applicable_above_salary=$res['applicable_above_salary'];
			$in_out_time_format=$res['in_out_time_format']; 
		}
 
	
	list( $day, $month, $year ) = explode( "-", $txt_date );
	$date = $year . "-" . $month . "-" . $day;
	
	if($cbo_status==0) $status="";
	else if ($cbo_status==1) $status="and a.status='P '";
	else if ($cbo_status==2) $status="and a.status='A '";
	else if ($cbo_status==3) $status="and a.status='D '";
	else if ($cbo_status==4) $status="and a.status in ('CL','ML','SL','EL','SpL','LWP')";
	else if ($cbo_status==5) $status="and a.status='MR'";
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else	$department_id=" and emp.department_id='$department_id'";	

	$p=0;$a=0;$mr=0;$d=0;$l=0;
	
	ob_start();
	
	?>
	<STYLE>
		<!--		  
		  .highlight { background-color: #8888FF }
		//-->
	</style>
    <div align="center" style="width:1100px; font-size:16px"><b><? echo $company_details[$cbo_company_id]['company_name']; ?></b></div>
    <table cellpadding="0" cellspacing="0" border="1" class="rpt_table" style="font-size:10px;" width="1100" rules="all" >
	<?	
			$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.*,a.sign_in_time,a.sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min 
					FROM 
						hrm_attendance a, hrm_employee emp 
					WHERE 
						a.attnd_date like '$date' and 
						a.emp_code=emp.emp_code 
						$department_id 
						$location_id 
						$division_id 
						$section_id 
						$company_id  
						$status 
						$subsection_id 
						$designation_id
						$category 
						order by emp.department_id, RIGHT(emp.id_card_no, 3) ASC";
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );						
			$emp_tot=mysql_num_rows($result);
			//echo $sql;
			if ($emp_tot>0)
				{	
					$new_dept=array();
					while( $row = mysql_fetch_array( $result ) ) {
					
					
					$hours[0] = abs((int)($row["total_over_time_min"]/60)); //hours
					$hours[1] = abs((int)($row["total_over_time_min"]%60)); //minutes
					
					$ot_mins_cal=0;
					 
				 
				//$total_hours = sprintf("%02d:%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
				$total_hours=number_format(get_buyer_ot_hr($row["total_over_time_min"],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440),1);
				$ot_mins_cal = $total_hours*60;	
						
						
						if (in_array($row['department_id'], $new_dept))
								{
									$i++;
								}
								else
								{									
								
								 ?>									
                                    <thead>                                    	
                                        <tr bgcolor="#F0F0B8">
											<th colspan=15><b>Department Name:</b> <? echo $department_details[$row['department_id']]['department_name'];?> </th>
										</tr>
										<tr>
											<th width=50><b>Emp Code</b></th>
											<th width=50><b>ID Card No</b></th>
											<th width=50><b>Punch Card No</b></th>
											<th width=140><b>Name</b></th>
											<th width=100><b>Designation</b></th>
											<th width=90><b>Department</b></th>
											<th width=120><b>Location</b></th>
											<th width=80><b>Section</b></th>
											<th width=50><b>Sub Section</b></th>
											<th width=70><b>Attnd. Date</b></th>
											<th width=60><b>In Time</b></th>
											<th width=60><b>Out Time</b></th>
											<th width=30><b>Status</b></th>
											<th width=40><b>Late Min</b></th>
											<th><b>Over Time</b></th>
										</tr>
                                    </thead>    
								<?		
                                        
                                        
                                        $new_dept[$i]=$row[department_id];
								$i++;
								}	
	
			
			$location_name=$location_details[$row['location_id']]['location_name'];
			$section_name=$section_details[$row['section_id']]['section_name'];
			$subsection_name=$subsection_details[$row['subsection_id']]['subsection_name'];
			
			 
			
			
				if ($i%2==0) $bgcolor="#EEEEEE"; 
				else $bgcolor="#FFFFFF";
		
			?>			
					<tr bgcolor="<? echo $bgcolor;?>">
						<td height=50><? echo $row[emp_code];?> </td>
						<td><? echo $row[id_card_no];?></td>
						<td><? echo $row[punch_card_no];?></td>
						<td><? echo $row[name];?></td>
						<td><? echo $designation_chart[$row['designation_id']]['custom_designation'];?></td>
						<td><? echo $department_details[$row['department_id']]['department_name'];?></td>						
						<td><? echo $location_name;?></td>
						<td><? echo $section_name;?></td>
						<td><? echo $subsection_name;?></td>
						<td align='center'><? echo convert_to_mysql_date($row[attnd_date]);?></td>
						<td  width="60"><b><? echo show_time_format( $row['sign_in_time'], $in_out_time_format );   ?></b></td>
					<td  width="60"><b><? echo show_time_format( $row['sign_out_time'], $in_out_time_format );   ?></b></td>
						<td><? echo($row[status]=="CH" ? "H" : $row[status]);?></td>
						<td><? echo $row[late_time_min];?></td>
                        <td><?php echo $total_hours;?>					
					</tr>
                  
			<?
        			
					if($row["status"]=='P'){$p++;}
					if($row["status"]=='A'){
							$a++;
							if($a>1){
									$param_emp_code .=',';
									}
							$param_emp_code .=$row["emp_code"];
							
					}
					if($row["status"]=='MR')
							{
								$mr++;
								if($mr>1){
									$mr_emp_code .=',';
									}
								$mr_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='D')
							{
								$d++;
								if($d>1){
									$delay_emp_code .=',';
									}
								$delay_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='CL' || $row["status"]=='SL' || $row["status"]=='ML' || $row["status"]=='EL' || $row["status"]=='LWP' || $row["status"]=='SpL')
							{
								$l++;
								if($l>1){
									$leave_emp_code .=',';
									}
								$leave_emp_code .=$row["emp_code"];
							}					
					}
		}
	
		$total=$p+$a+$l+$d+$mr;
		?>		
        	</table> 
        	<table width="1100" border="1" class="rpt_table" rules="all">
        		  <tr bgcolor="#F0F0B8">
						<td colspan=6 align="center" height=50><b>Summary</b></td>
				  </tr>
				  <tr>				
                        <td  align="left" bgcolor="#E5E5E5" height="50" ><b>Present :</b> <? echo $p;?> </td>								  	
                        <td  align="left" bgcolor="#FFFFFF"><b>Absent :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $param_emp_code;?>&date=<? echo $date;?>','Absent Information'); return false"><? echo $a;?></a> </td>				  			  	
                        <td  align="left" bgcolor="#E5E5E5"><b>Leave :</b> <a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $leave_emp_code;?>&date=<? echo $date;?>','Leave Information'); return false"><? echo $l;?></a> </td>				  				   
                        <td  align="left" bgcolor="#FFFFFF"><b>Movement :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $mr_emp_code;?>&date=<? echo $date;?>','Movement Information'); return false"><? echo $mr;?> </a> </td>				  			    
                        <td  align="left" bgcolor="#E5E5E5"><b>Late :</b><a href='#' onclick="openmypage('popup_punch_report.php?emp_code=<? echo $delay_emp_code;?>&date=<? echo $date;?>','Late Information'); return false"><? echo $d;?> </a></td>				  			    
                        <td  align="left" bgcolor="#FFFFFF"><b>Total :</b> <? echo $total;?> </td>					
				  </tr>
			</table>
			
	<?	
	
	$html=ob_get_contents();
	ob_clean();
	
	
	//for report temp file delete 
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";		
	exit();
}



if( $action == "monthly_attn_audit_rpt" ) {	// Audit report
			
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name=$cbo_company_id order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
		}
	
	//subsection_details
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
			$shift_details[$row['id']] = $row['short_shift_name'];
	}
	
	
	$date_part = trim($cbo_year_selector)."-".trim($cbo_month_selector);
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$salary_month_date=strtotime($date_part."-01");
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and emp.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and a.category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else  $department_id="and emp.department_id='$department_id'";
	if ($designation_id==0) $designation_id=""; else  $designation_id="and emp.designation_id='$designation_id'";
	//date("F j, Y, g:i a");   date('Y-m-t',$timeStamp)
	
		
	ob_start();	
	
	?>
	<div align="center" style="width:1700px; font-size:16px"><b><? echo $company_details[$cbo_company_id]['company_name']; ?></b></div>
    <div align="center" style="width:1700px; font-size:12px"><b>Master Attendance Register</b></div>
    <div align="center" style="width:1700px; font-size:12px"><b>Salary Month: <? echo date("F, Y",$salary_month_date); ?></b></div>
	<table width="1700" style="font-size:10px; border:1px solid #000" cellpadding="0" cellspacing="0" border="1" class="rpt_table" rules="all">
    	
	<?
	 $new_section=array();
		
	 $i=0;
	 //emp.department_id=$row_dp[id] and 
 	 $sqls = "SELECT a.*, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
	 		  FROM hrm_attendance emp left join hrm_employee a on emp.emp_code=a.emp_code
			  where			  
			  emp.attnd_date like '$date_part%'
			  $location_id 
			  $company_id 
			  $section_id 
			  $subsection_id 
			  $designation_id 
			  $category group by emp.emp_code order by emp.department_id,emp.section_id,CAST(a.id_card_no AS SIGNED)";
			  
	 //echo $sqls; die;
	 $results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
	 while( $rows = mysql_fetch_assoc( $results ) ) 
	 {
	 	
		$sql = "SELECT * 
				FROM hrm_attendance 
				WHERE  attnd_date like '$date_part%' and emp_code='$rows[emp_code]' 
				order by attnd_date ASC";
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
		if( mysql_num_rows($result)>0 )
	 	{
			
			/*
			if(in_array($rows['department_id'],$new_department))
			{
			}
			else
			{
				$new_department[$rows['department_id']]=$rows['department_id'];
				$company_details[$row['id']] = array();
			}
			*/
			
			if(in_array($rows['section_id'], $new_section[$rows['division_id']][$rows['department_id']]))						
				{
					$i++;
				}
				else
				{	
					$new_section[$rows['division_id']][$rows['department_id']][$rows[section_id]]=$rows[section_id];
					//$new_section[$i]=$rows[section_id];
					$division_name=$division_details[$rows['division_id']]['division_name'];
					$department_name=$department_details[$rows['department_id']]['department_name'];
					$section_name=$section_details[$rows['section_id']]['section_name'];
				?>	
			
                    <thead>
                        <tr>
                            <td colspan='<? echo $get_days+10; ?>'><b>Division Name: <? echo $division_name;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Department Name: <? echo $department_name;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Section Name: <? echo $section_name; ?></b></td>
                        </tr>
                        <tr>
                            <th width='50'><b>Emp Code</b></th>
                            <th width='50'><b>ID Card</b></th>
                            <th width='120'><b>Emp Name</b></th>
                            <th width='100'><b>Designation</b></th>
                            <th width='50'><b>Join Date</b></th>
                            <th width='20'><b>Date</b></th>				
                            <?		
                            for($j=1; $j<=$get_days;$j++)
                               {  ?>                        
                                    <th width='30'><b><? echo $j; ?></b></th>
                            <? } ?>
                            <th width='20' style="font-size:9px"><b>Total (P,GH,FH,W)</b></th>
                            <th width='20'><b>Total Absent</b></th>
                            <th width='20'><b>Total Leave</b></th>
                            <th width='20'><b>Total Late</b></th>
                       </tr>
                  	</thead>	
				  <tbody>							
				<?	
				
				$i++;			
				
			}		
			
			// 1-31 days column----------------------------------
			$prt=0;
			$present=0;$absent=0;$leave=0;$wday=0;$pay_day=0;$cnt=0;$is_new_join=0;$st_date=$date_part."-01";$total_ot=0;
			$ot_list="";$shift_list="";
			while( $row = mysql_fetch_assoc( $result ) ) 
			{	
		
				
						if( $prt==0 )
						{
							$prt=1;
							if ($i%2==0) $bgcolor="#E9F3FF"; 
							else $bgcolor="#FFFFFF";
						  ?>
                          
							<tr bgcolor="<? echo $bgcolor; ?>">
								<td width='50' rowspan="3"><? echo $rows[emp_code]; ?></td>
								<td width='50' rowspan="3"><? echo $rows[id_card_no];?></td>
								<td width='120' rowspan="3"><? echo $rows[name];?></td>
								<td width='100' rowspan="3"><? echo $designation_chart[$rows['designation_id']]['custom_designation'];?></td>
								<td width='50' rowspan="3"><? echo convert_to_mysql_date ($rows[joining_date]);?></td>
								<td width='20' style="font-size:10px; font-weight:bold">Status</td>
						   <?	
						}			
						
						
						if ($cnt==0)
						{
							$start_date=datediff( "d", $st_date, $row[attnd_date]);
							if( $start_date >1 )
							{
								for($mk=0; $mk<$start_date-1; $mk++)
								{
									$cnt++;
									?> <td width='50' align="center"><b>&nbsp;</b></td> <?
									$ot_list .='<td width="50" align="center"><b>&nbsp;</b></td>';
									$shift_list .='<td width="50" align="center"><b>&nbsp;</b></td>';
								}
							}
						}
						$cnt++;
						
						$hours[0] = abs((int)($row["total_over_time_min"]/60)); //hours
						$hours[1] = abs((int)($row["total_over_time_min"]%60)); //minutes
						
						$ot_mins_cal=0;
						if($ot_fraction==1) //ot fraction allow
						{						
							if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
							else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
							else $ot_mins_cal = $hours[0]*60;
						}
						if($ot_fraction==2) // no ot fraction
						{						
							//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
							if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
							else $ot_mins_cal = $hours[0]*60;						
						}
						if($ot_fraction==3) //at actual minutes
						{						
							$ot_mins_cal = 	$row["total_over_time_min"];				
						}
						
						$total_hours = sprintf("%d.%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
						
						?> <td width='50' align="center"><b><? echo ($row[status]=="CH" ? "H" : $row[status]);?></b></td> <?
						$ot_list .='<td width="50" align="right" bgcolor="'.$bgcolor.'"><b>'.$total_hours.'</b></td>';
						$shift_list .='<td width="50" align="center"  bgcolor="'.$bgcolor.'"><b>&nbsp;'.$shift_details[$row['policy_shift_id']].'</b></td>';
						
						$total_ot += $total_hours;
						$explode_total_ot = explode(".",$total_ot);
						if($explode_total_ot[1]==6) $total_ot = $explode_total_ot[0]+1;
						
						
			   } //3rd while loop-----------------------
					
						$d="count(*)";
						$dd="hrm_attendance";
						$ddd="status in ('P') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$present= return_field_value($d,$dd,$ddd);
						
						$ddd="status='A ' and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$absent= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('CL','ML','SL','EL','SpL') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$leave= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('D') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$late= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('MR') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$movement= return_field_value($d,$dd,$ddd);
						
						$ddd="status='W' and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$wday= return_field_value($d,$dd,$ddd);
						
						$ddd="status in ('H','GH','CH','FH') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
						$hday= return_field_value($d,$dd,$ddd);
						
					
					while($get_days!=$cnt){
						?> <td width='50' align="center"><b>&nbsp;</b></td> <?
						$cnt++;
					}
															
					?>
                    <td width='50' align="center" rowspan="2"><b><? echo $hday+$present+$wday+$movement; ?></b></td>
                    <td width='50' align="center" rowspan="2"><b><? echo $absent;  ?></b></td>
                    <td width='50' align="center" rowspan="2"><b><? echo $leave;  ?></b></td>
                    <td width='50' align="center" rowspan="2"><b><? echo $late;  ?></b></td>             
            	</tr>                                
                <tr>
                	<td style="font-size:10px; font-weight:bold">Shift</td>
                	<? echo $shift_list; ?>
                </tr>
                <tr>
                	<td style="font-size:10px; font-weight:bold">OT</td>
                	<? echo $ot_list; ?>
                    <td width='50' align="center" colspan="3"><b><? echo "Total OT=".$total_ot;  ?></b></td> 
                    <td width='50' align="center" ><b>&nbsp;</b></td> 
                </tr>
            
            </tbody>
                 
				 <?
					
				} //end if condition 			
													
		} //1st while loop
		
	
	?> 
    	</table>
	<?
				
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
	
	
}






if( $action == "monthly_attn_summery_audit" ) {	// Audit Summary
		
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
		}	
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and emp.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
	if ($txt_date==0) $txt_date=""; else $txt_date="and attnd_date='".convert_to_mysql_date($txt_date)."'";
	
	
	if ($department_id==0) $department_id="";
	else $department_id="where id='$department_id'";
	
	ob_start();	
	
	?>
	<div align="center" style="width:2450px; font-size:16px"><b><? echo $company_details[$cbo_company_id]['company_name']; ?></b></div>
	<table width="2450" style="font-size:10px;" cellpadding="0" cellspacing="0" border="1" class="rpt_table">
    
	<?
	$new_section=array();
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
    while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{	
	 $i=0;
	 //$sqls = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,atn.attnd_date FROM hrm_employee emp, hrm_attendance atn WHERE emp.emp_code=atn.emp_code and atn.attnd_date like '$date_part%' and emp.department_id=$row_dp[id] $location_id $cbo_company_id $section_id $subsection_id $designation_id $category group by atn.emp_code order by emp.section_id,CAST(emp.id_card_no AS SIGNED)";
 	 $sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
	 		  FROM 
			  	  hrm_employee emp
			  where 
				  emp.department_id=$row_dp[id] 
				  $location_id
				  $division_id 
				  $company_id 
				  $section_id 
				  $subsection_id 
				  $designation_id 
				  $category order by emp.section_id,CAST(emp.id_card_no AS SIGNED)";
			  
	 //echo $sqls; die;
	 $results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
	 while( $rows = mysql_fetch_assoc( $results ) ) 
	 {
	 	
		$sql = "SELECT * FROM hrm_attendance  WHERE  attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]' order by attnd_date asc";
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
		if( mysql_num_rows($result)>0 )
	 	{
			
			if (in_array($rows['section_id'], $new_section))						
				{
					$i++;
				}
				else
				{	
					$section_name=$section_details[$rows['section_id']]['section_name'];
				?>	
			
					<thead>
						<tr>
							<th colspan=46>Department Name: <? echo $row_dp[department_name];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    Section Name: <? echo $section_name; ?></th>
						</tr>
						<tr>
							<th width='65'><b>Emp Code</b></th>
							<th width='50'><b>ID Card</b></th>
							<th width='180'><b>Emp Name</b></th>
							<th width='120'><b>Designation</b></th>
							<th width='60'><b>Join Date</b></th>
							<th width='30'><b>Days</b></th>				
							<?		
							for($j=1; $j<=$get_days;$j++)
							   {  ?>                        
									<th width='50'><b><? echo $j; ?></b></th>
							<? } ?>
					
							<th width='50'><b>Present</b></th>
							<th width='50'><b>Absent</b></th>
							<th width='50'><b>Late</b></th>
							<th width='50'><b>Leave</b></th>
							<th width='50'><b>Movement</b></th>
							<th width='50'><b>W. Day</b></th>
							<th width='50'><b>H. Day</b></th>
							<th width='50'><b>Pay Days</b></th> 
                            <th width='50'><b>OT Hr.</b></th>
					   </tr>
				  </thead>
				  <tbody>							
				<?	
				
				$new_section[$i]=$rows[section_id];
				$i++;			
				
			}		
			
			
			
			// 1-31 days column----------------------------------
			$prt=0;
			$present=0;$absent=0;$leave=0;$wday=0;$pay_day=0;$cnt=0;$is_new_join=0;$st_date=$date_part."-01";
			
			while( $row = mysql_fetch_assoc( $result ) ) 
			{	
		
				
						if( $prt==0 )
						{
							$prt=1;
							if ($i%2==0) $bgcolor="#E9F3FF"; 
							else $bgcolor="#FFFFFF";
						  ?>
							<tr bgcolor="<? echo $bgcolor; ?>">
								<td width='65'><? echo $rows[emp_code]; ?></td>
								<td width='50'><? echo $rows[id_card_no];?></td>
								<td width='180'><? echo $rows[name];?></td>
								<td width='120'><? echo $designation_chart[$rows['designation_id']]['custom_designation'];?></td>
								<td width='60'><?php echo convert_to_mysql_date($rows[joining_date]);?></td>
								<td width='30'><? echo $get_days;?></td>
						   <?	
						}			
						
						
						if ($cnt==0)
						{
							$start_date=datediff( "d", $st_date, $row[attnd_date]);
							if( $start_date >1 )
							{
								for($mk=0; $mk<$start_date-1; $mk++)
								{
									$cnt++;
									?> <td width='50' align="center"><b> </b></td> <?
								}
							}
						}
						$cnt++;
						
						 
						?> <td width='50' align="center"><b><? $ot= get_buyer_ot_hr($row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
						
						echo ($row[status]=="CH" ? "H" : $row[status]); $total_ot_hr+=$ot; $ot=0; ?></b></td> <?
			   } //3rd while loop
					
					while($get_days!=$cnt){
						?> <td width='50' align="center"><b></b></td> <?
						$cnt++;
					}
					
					$d="count(*)";
					$dd="hrm_attendance";
					$ddd="status in ('P') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$present= return_field_value($d,$dd,$ddd);
					
					$ddd="status='A ' and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$absent= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('CL','ML','SL','EL','SpL') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$leave= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('D') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$late= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('MR') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$movement= return_field_value($d,$dd,$ddd);
					
					$ddd="status='W' and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$wday= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('H','GH','CH','FH') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$hday= return_field_value($d,$dd,$ddd);
					
					$ddd="status not in ('A','LWP') and attnd_date like '$date_part%' $txt_date  and  emp_code='$rows[emp_code]'";
					$pay_day=return_field_value($d,$dd,$ddd);
					
					?>
                    
                    <td width='50'><? echo $present;?></td>
                    <td width='50'><? echo $absent?></td>
                    <td width='50'><? echo $late;?></td>
                    <td width='50'><? echo $leave;?></td>
                    <td width='50'><? echo $movement;?></td>
                    <td width='50'><? echo $wday;?></td>
                    <td width='50'><? echo $hday;?></td>
                    <td width='50'><? echo $pay_day;?></td>
                    <td width='50'><? echo $total_ot_hr; $total_ot_hr=0; ?></td>
            </tbody></tr>
                 
				 <?
					
				} //end if condition 			
													
		} //2nd while loop
		
	}//1st while loop
	
	?> 
    	</table>
	<?
				
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
	
	
}


if( $action == "emp_job_card_report" ) {	//Job Card Report
	
	if($txt_date!="")
	{
		$date_part=convert_to_mysql_date($txt_date);
	}
	else
	{
		$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	}
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	
	//shift policy array
	$qr_shift="select * from lib_policy_shift where status_active=1 and is_deleted=0 order by id";
	$result_s = mysql_query( $qr_shift ) or die( $qr_shift . "<br />" . mysql_error() );
	$shift_arr = array();
	while($res_shift=mysql_fetch_array($result_s))		
		{
			$shift_arr[$res_shift["id"]]=mysql_real_escape_string($res_shift["shift_name"]);
		}	
		
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$adjust_out_time=$res['adjust_out_time'];
			$applicable_above_salary=$res['applicable_above_salary'];
			$in_out_time_format=$res['in_out_time_format']; 
		}
		
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	
	if ($emp_code==0) 
	{	
		$emp_code=""; 
	}
	else 
	{		
		$emp_explode=explode(",",$emp_code);		
		$employee_code="";
		for($i=0;$i<count($emp_explode);$i++)
		{
		if($i==0)
			$employee_code=	$employee_code.$emp_explode[$i];
		else
			$employee_code=	$employee_code.','.$emp_explode[$i];
		} 
		$emp_code="and emp_code in ($employee_code)";
	}
		
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";	
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="where id='$department_id'";			
		}
	ob_start();
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{	
	 	$sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM 
					hrm_employee 
				WHERE 
					id!='' $emp_code and 
					department_id=$row_dp[id] 
					$cbo_company_id $location_id $division_id $section_id $subsection_id $category 
					order by designation_level,RIGHT(id_card_no, 4)";
		 
		$i=0;
		$results = mysql_query( $sqls ) or die( $sql . "<br />" . mysql_error() );	
		
				
		while( $rows = mysql_fetch_assoc( $results ) ) 
		{
				
			//this is used inside of while loop
			$sql = "SELECT * FROM hrm_attendance  WHERE  attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]' order by attnd_date  asc";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$numrow = mysql_num_rows($result);
			if( $numrow > 0 ){
	?>							
			<style media="print">
			p{page-break-before:always}
			</style>	
                <table width="800" border="1" cellpadding="0" cellspacing="0" style="border:1px solid black;font-size:10px" align="center" rules="all" class="rpt_table">
                <thead>	
                    <tr bgcolor="#F0F0B8" height="30">
						<th colspan=9  style="font-size:11px"><b><? echo $company_details[$rows['company_id']]['company_name']; ?> <br /> Department :  <? echo $department_details[$rows['department_id']]['department_name']; ?></b></th>
					</tr>
					<tr bgcolor="#FBFAFC" height="30">
						<td colspan=9><b>ID Card:</b> <? echo $rows[id_card_no];?>&nbsp;&nbsp;&nbsp;<b>Emp Code:</b> <? echo $rows[emp_code];?>&nbsp;&nbsp;&nbsp;<b>Emp Name:</b> <? echo $rows[name];?>&nbsp;&nbsp;&nbsp;<b>Designation:</b> <? echo $designation_chart[$rows['designation_id']]['custom_designation'];?>&nbsp;&nbsp;&nbsp;<b>Joining Date:</b> <? echo convert_to_mysql_date($rows[joining_date]);?></td>
					</tr>
					<tr>
						<th colspan=9></th>
					</tr>
					<tr height="40">
						<th width='60'><b>Date</b></th>
						<th width=''><b>Shift</b></th>
						<th width='70'><b>Shift in </b></th>
						<th width='70'><b>Shift Out </b></th>
						<th width='70'><b>Actual In</b></th>
						<th width='70'><b>Actual Out</b></th>
						<th width='60'><b>Late Min</b></th>
						<th width='90'><b>OT Min</b></th>
						<th width='50'><b>Status</b></th>
					</tr>					
				</thead>
            
			<?
				
					
					$holiday=0;$weekend=0;$working_day=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_days=0;$ot=0;
					while( $row = mysql_fetch_assoc( $result ) ) 					
					{					
					// convert minutes to hours:minutes
					// $total_hours = sprintf("%d hours %02d minutes", abs((int)($row["total_over_time_min"]/60)), abs((int)($row["total_over_time_min"]%60)));	
									
					$hours[0] = abs((int)($row["total_over_time_min"]/60)); //hours
					$hours[1] = abs((int)($row["total_over_time_min"]%60)); //minutes
					
					$ot_mins_cal=0;
					if($ot_fraction==1) //ot fraction allow
					{						
						if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
						else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;
					}
					if($ot_fraction==2) // no ot fraction
					{						
						//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
						if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;						
					}
					if($ot_fraction==3) //at actual minutes
					{						
						$ot_mins_cal = $row["total_over_time_min"];				
					}
					
					$total_hours = sprintf("%d hrs %02d mins", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
					
					/*if($row['r_sign_in_time']!='00:00:00'){$r_in = DATE("g:i a", STRTOTIME($row[r_sign_in_time]));}else $r_in="00:00";
					if($row['r_sign_out_time']!='00:00:00'){$r_out = DATE("g:i a", STRTOTIME($row[r_sign_out_time]));}else $r_out="00:00";
					if($row['sign_in_time']!='00:00:00'){$s_in = DATE("g:i a", STRTOTIME($row[sign_in_time]));}else $s_in="00:00";
					if($row['sign_out_time']!='00:00:00'){$s_out = DATE("g:i a", STRTOTIME($row[sign_out_time]));}else $s_out="00:00";*/
					
			?>		
					
                    <tr height="30">
						<td width=''><? echo $row[attnd_date];?></td>
						<td width=''><? echo $shift_arr[$row[policy_shift_id]];?></td>						
						<td width=''><? echo show_time_format( $row['r_sign_in_time'], $in_out_time_format ); ?></td>
						<td width=''><? echo show_time_format( $row['r_sign_out_time'], $in_out_time_format );?></td>
						<td width=''><? 
						
						if ($applicable_above_salary!=0)
						{
							if ($row['sign_in_time']!="00:00:00")
								echo show_time_format( get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $adjust_in_time ), $in_out_time_format );
							else
								echo show_time_format( $row['sign_in_time'], $in_out_time_format );
						}
						else
						 	echo show_time_format( $row['sign_in_time'], $in_out_time_format );
						
						
						//if ($applicable_above_salary!=0) echo ;;?></td>						
						<td width=''><? 
						$ot_hr= get_buyer_ot_hr($row["total_over_time_min"],55,0,0,1440)  ;
						if ($applicable_above_salary!=0)
						{
							if ($row['sign_out_time']!="00:00:00") 
								echo DATE("H:i:s", STRTOTIME(get_buyer_out_time($row['sign_out_time'], $row['r_sign_out_time'], $adjust_out_time, 1440, $ot_hr,$ot_start_min,$one_hr_ot_unit,$rows['ot_entitled'])));
							else
								echo "00:00:00";
						}
						 else
						 {
							 if ($row['sign_out_time']!="00:00:00")
							 	echo DATE("H:i:s", STRTOTIME($row[sign_out_time]));
							 else
								echo "00:00:00";
						 }
							 ?></td>  	
						<td width=''><? echo $row[late_time_min];?></td>
						<td width=''><? echo $total_hours;?></td>
						<td width=''><? echo ($row[status]=="CH" ? "H" : $row[status]);?></td>
                    </tr>
                        					
			<?			
						if($row[status]=='CL'){$cl++;}
						if($row[status]=='SL'){$sl++;}
						if($row[status]=='ML'){$ml++;}
						if($row[status]=='EL'){$el++;}
						if($row[status]=='LWP'){$lwp++;}
						if($row[status]=='SpL'){$spl++;}
						if($row[status]=='H' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH' ){$holiday++;}
						if($row[status]=='W'){$weekend++;}
						if($row[status]=='P'){$present++;}
						if($row[status]=='D'){$late++;}
						if($row[status]=='MR'){$movement++;}
						if($row[status]=='A'){$absent++;}
						$total_days++;
						if($row[total_over_time_min]!=''){$ot += $ot_mins_cal;}
					}
					$working_day=$total_days-$holiday-$weekend;
					//$overtime=$ot;		//number_format($ot/60,2,'.','');
					$overtime = sprintf("%d hrs %02d mins", abs((int)($ot/60)), abs((int)($ot%60)));	
					
			?>
						<tr><td colspan=9 height="25" align="center"><b>Summary</b></td></tr>
                        <tr bgcolor="#A5C3EB" height="30">
							<td colspan=9 height="25">
								<b>Total Day:</b><? echo $total_days;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Weekend Day:</b><? echo $weekend;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Holiday:</b><? echo $holiday;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Working Day:</b><? echo $working_day;?> &nbsp;&nbsp;&nbsp;&nbsp;
								<b>Present:</b><? echo $present;?> &nbsp;&nbsp;&nbsp;&nbsp;
								<b>Late Present:</b><? echo $late;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Movement:</b><? echo $movement;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Total OT:</b><? echo $overtime;?>&nbsp;&nbsp;&nbsp;&nbsp;
                                <b>Payable Days:</b><? echo $total_days-$absent;?>&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
												
						</tr>
						<tr bgcolor="#A5C3EB"  height="30">
							<td colspan=9>
								<b>Absent:</b><? echo $absent;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Casual Leave:</b><? echo $cl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Sick Leave:</b><? echo $sl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Earn Leave:</b><? echo $el;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Special Leave:</b><? echo $spl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Leave Without Pay:</b><? echo $lwp;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Maternity Leave:</b><? echo $ml;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>	
						</tr>
					</table>
					<p></p>
		<?   
			}//end while condition
		
		}// end if condition
		
    }
	
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}


if( $action == "daily_attn_summary_report" ) {	//Daily Attendance Summary Report	
	
	$company = $company_id;
	if ($company_id==0) $company_id=""; else $company_id="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and e.category='$cbo_emp_category'";
	if ($department_id==0) $department_id="";else $department_id="and a.department_id='$department_id'";	
						
	$date_part=convert_to_mysql_date($txt_date);
	$html .="<div style=\"font-size:12px; width:750px;\">";
	$addrs ="";		
	if($company_details[$company]['plot_no'] !='') $addrs .= $company_details[$company]['plot_no'].", "; 
	if($company_details[$company]['level_no'] !='') $addrs .= $company_details[$company]['level_no'].", "; 
	if($company_details[$company]['block_no'] !='') $addrs .= $company_details[$company]['block_no'].", "; 
	if($company_details[$company]['road_no'] !='') $addrs .= $company_details[$company]['road_no'].", "; 
	if($company_details[$company]['city'] !='') $addrs .= $company_details[$company]['city']." "; 
	$html .= "<div align=\"center\" style=\"width:750px; font-size:12px\"><b>". $company_details[$company]['company_name']."</b></div>";
	$html .= "<div align=\"center\" style=\"width:750px; font-size:10px\"><b>Address : ". $addrs."</b></div>";
	$html .= "<div align=\"center\" style=\"width:750px; font-size:10px\"><b>Attendance Summary Report On Date : ". $txt_date."</b></div>";
	$html .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"1\" style=\"border:1px solid black;font-size:10px;\" class=\"rpt_table\" rules=\"all\">";
												
	$i=0;	
	$new_location=array();
	$new_division=array();
	$new_department=array();		
	
	
	$div_total_new_join=0;$div_total_new_join=0;$div_total_manpower_dep=0;$div_total_movement=0;
	$div_total_present=0;$div_total_late=0;$div_total_absent=0;$div_total_total_leave=0;$div_total_new_join=0;
	
	$loc_total_new_join=0;$loc_total_manpower_dep=0;$loc_total_movement=0;
	$loc_total_present=0;$loc_total_late=0;$loc_total_absent=0;$loc_total_total_leave=0;
	
	$grand_total_new_join = 0;					
	$grand_total_manpower_dep = 0;
	$grand_total_movement = 0;
	$grand_total_present = 0;

	$grand_total_late = 0;
	$grand_total_absent = 0;
	$grand_total_total_leave = 0;
		
		$sql = "SELECT 
				a.location_id,a.division_id,a.department_id,a.designation_id,e.joining_date 
			FROM 
				hrm_employee as e LEFT JOIN hrm_attendance as a ON e.emp_code=a.emp_code 
			WHERE 
				a.attnd_date like '$date_part%' 
				$company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $category 
				group by a.location_id,a.division_id,a.department_id,a.designation_id
				order by a.location_id,a.division_id,a.department_id,a.designation_id,e.designation_level";				
  /*
	$sql = "SELECT 
				a.designation_id,a.department_id,a.division_id,a.location_id 
			FROM 
				hrm_attendance a
			WHERE 
				a.attnd_date like '$date_part%'  
				$company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $category 
				group by a.location_id,a.division_id,a.department_id,a.designation_id
				order by a.location_id,a.division_id,a.department_id,a.designation_id";	  
  */
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$num_rows=mysql_num_rows($result);
	while( $row = mysql_fetch_assoc( $result ) ) 
	{		
			
			if($i==0)
			{
				$location_name_html = "<tr class=\"tbl_top\">
									<td colspan=9><b>Location Name: ".$location_details[$row['location_id']]['location_name']."</b></td>
								</tr>";
				$division_name_html = "<tr class=\"tbl_top\">
									<td colspan=9><b>Division Name: ".$division_details[$row['division_id']]['division_name']."</b></td>
								</tr>";	
								
				$new_division[$row[division_id]]=$row[division_id];	
				$new_location[$row[location_id]]=$row[location_id];			
			}
			else
			{
				$location_name_html="";
				$division_name_html="";
			}
			
			if(in_array($row[department_id],$new_department))
			{
				$holiday=0;$weekend=0;$total_leave=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_manpower=0;$new_join=0;
				$i++;
			}
			else
			{
					
				$new_department[$row[department_id]] = $row[department_id];
				if($i!=0) // sub total and percentage rows print
				{
					$html .= "<tr class=\"tbl_bottom\">
								<td width=''><b>Total</b></td>
								<td width='' align='right'>".$total_new_join."</td>						
								<td width='' align='right'>".$total_manpower_dep."</td>
								<td width='' align='right'>".$total_movement."</td>
								<td width='' align='right'>".$total_present."</td>						
								<td width='' align='right'>".$total_late."</td> 	
								<td width='' align='right'>".$total_absent."</td>
								<td width='' align='right'>".$total_total_leave."</td>	
								<td width=''>&nbsp;</td>
							</tr>
							<tr>
								<td width=''><b>(In %)</b></td>
								<td width=''></td>						
								<td width=''></td>
								<td width='' align='right'>".round(($total_movement*100)/$total_manpower_dep,2)."%</td>
								<td width='' align='right'>".round(($total_present*100)/$total_manpower_dep,2)."%</td>						
								<td width='' align='right'>".round(($total_late*100)/$total_manpower_dep,2)."%</td> 	
								<td width='' align='right'>".round(($total_absent*100)/$total_manpower_dep,2)."%</td>
								<td width='' align='right'>".round(($total_total_leave*100)/$total_manpower_dep,2)."%</td>	
								<td width=''>&nbsp;</td>
							</tr>";
				
				
					if(!in_array($row[division_id],$new_division))
					{
						
						if($i!=0) // sub total and percentage rows print
						{
							$html_division = "<tr class=\"tbl_bottom\">
										<td><b>Division Total</b></td>
										<td align='right'>".$div_total_new_join."</td>						
										<td align='right'>".$div_total_manpower_dep."</td>
										<td align='right'>".$div_total_movement."</td>
										<td align='right'>".$div_total_present."</td>						
										<td align='right'>".$div_total_late."</td> 	
										<td align='right'>".$div_total_absent."</td>
										<td align='right'>".$div_total_total_leave."</td>	
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td><b>(In %)</b></td>
										<td></td>						
										<td></td>
										<td align='right'>".round(($div_total_movement*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_present*100)/$div_total_manpower_dep,2)."%</td>						
										<td align='right'>".round(($div_total_late*100)/$div_total_manpower_dep,2)."%</td> 	
										<td align='right'>".round(($div_total_absent*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_total_leave*100)/$div_total_manpower_dep,2)."%</td>	
										<td width=''>&nbsp;</td>
									</tr>";
						
							$division_name_html = "<tr class=\"tbl_top\">
									<td colspan=9><b>Division Name: ".$division_details[$row['division_id']]['division_name']."</b></td>
								</tr>";	
						
						}
						
 						$new_division[$row[division_id]] = $row[division_id];
						$new_department=array();
						$new_department[$row[department_id]] = $row[department_id];
						
						$div_total_new_join=0;$div_total_manpower_dep=0;$div_total_movement=0;
						$div_total_present=0;$div_total_late=0;$div_total_absent=0;$div_total_total_leave=0;
								
					}else {$division_name_html="";$html_division="";}
					$html .= $html_division;
					//division end if condition
				
				
					if(!in_array($row[location_id],$new_location))
					{
						
						if($i!=0) // sub total and percentage rows print
						{
							
							if($html_division=="")
							{
								$html .= "<tr class=\"tbl_bottom\">
										<td><b>Division Total</b></td>
										<td align='right'>".$div_total_new_join."</td>						
										<td align='right'>".$div_total_manpower_dep."</td>
										<td align='right'>".$div_total_movement."</td>
										<td align='right'>".$div_total_present."</td>						
										<td align='right'>".$div_total_late."</td> 	
										<td align='right'>".$div_total_absent."</td>
										<td align='right'>".$div_total_total_leave."</td>	
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td><b>(In %)</b></td>
										<td></td>						
										<td></td>
										<td align='right'>".round(($div_total_movement*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_present*100)/$div_total_manpower_dep,2)."%</td>						
										<td align='right'>".round(($div_total_late*100)/$div_total_manpower_dep,2)."%</td> 	
										<td align='right'>".round(($div_total_absent*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_total_leave*100)/$div_total_manpower_dep,2)."%</td>	
										<td width=''>&nbsp;</td>
									</tr>";
								$div_total_new_join=0;$div_total_manpower_dep=0;$div_total_movement=0;
								$div_total_present=0;$div_total_late=0;$div_total_absent=0;$div_total_total_leave=0;
							}
							
							
							$html .= "<tr class=\"tbl_bottom\">
										<td><b>Location Total</b></td>
										<td align='right'>".$loc_total_new_join."</td>						
										<td align='right'>".$loc_total_manpower_dep."</td>
										<td align='right'>".$loc_total_movement."</td>
										<td align='right'>".$loc_total_present."</td>						
										<td align='right'>".$loc_total_late."</td> 	
										<td align='right'>".$loc_total_absent."</td>
										<td align='right'>".$loc_total_total_leave."</td>	
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td><b>(In %)</b></td>
										<td></td>						
										<td></td>
										<td align='right'>".round(($loc_total_movement*100)/$loc_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($loc_total_present*100)/$loc_total_manpower_dep,2)."%</td>						
										<td align='right'>".round(($loc_total_late*100)/$loc_total_manpower_dep,2)."%</td> 	
										<td align='right'>".round(($loc_total_absent*100)/$loc_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($loc_total_total_leave*100)/$loc_total_manpower_dep,2)."%</td>	
										<td width=''>&nbsp;</td>
									</tr>";
						
							$location_name_html = "<tr class=\"tbl_top\">
									<td colspan=9><b>Location Name: ".$location_details[$row['location_id']]['location_name']."</b></td>
								</tr>";
						}
						
  						$new_location[$row[location_id]] = $row[location_id];		
						$new_division = array();
						$new_division[$row[division_id]] = $row[division_id];
						$division_name_html = "<tr class=\"tbl_top\">
									<td colspan=9><b>Division Name: ".$division_details[$row['division_id']]['division_name']."</b></td>
								</tr>";	
						
						
						$loc_total_new_join=0;$loc_total_manpower_dep=0;$loc_total_movement=0;
						$loc_total_present=0;$loc_total_late=0;$loc_total_absent=0;$loc_total_total_leave=0;
					
					}else $location_name_html="";
					//end location if condition
				
				
				}
				
				$html .= $location_name_html.$division_name_html."<tr bgcolor=#F0F0B8  class=\"tbl_top\">
								<td colspan=9><b>Department Name: ".$department_details[$row['department_id']]['department_name']."</b></td>
							</tr>";					
				
				//header print here 
				if($i==0) 
				{
					
					$html .= "<thead>
								<th width='130'><b>Designation</b></th>
								<th width='100'><b>Newly Joined</b></th>
								<th width='70'><b>Manpower</b></th>
								<th width='70'><b>Movement</b></th>
								<th width='70'><b>Present</b></th>
								<th width='70'><b>Late</b></th>
								<th width='70'><b>Absent</b></th>
								<th width='70'><b>Leave</b></th>
								<th width='100'><b>Remarks</b></th>
							</thead>
							<tr>
								<td colspan=9></td>
							</tr>";	
				}
									
				$total_new_join=0;$grand_total=0;$total_new_join=0;$total_manpower_dep=0;$total_movement=0;
				$total_present=0;$total_late=0;$total_absent=0;$total_total_leave=0;		
				$holiday=0;$weekend=0;$total_leave=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_manpower=0;$new_join=0;
				$i++;
				
				
 			} // end in_array condition else
						
				
			$sqls = "SELECT * from hrm_attendance where designation_id=$row[designation_id] and department_id=$row[department_id] and division_id=$row[division_id] and location_id=$row[location_id] and attnd_date='$date_part'";
			//echo $sqls;
			$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
			while( $rows = mysql_fetch_assoc( $results ) ) 
			{			
				
				if($rows[status]=='CL'){$cl++;}
				if($rows[status]=='SL'){$sl++;}
				if($rows[status]=='ML'){$ml++;}
				if($rows[status]=='EL'){$el++;}
				if($rows[status]=='LWP'){$lwp++;}
				if($rows[status]=='SpL'){$spl++;}
				if($rows[status]=='H' || $rows[status]=='GH' || $rows[status]=='FH' || $rows[status]=='CH' ){$holiday++;}
				if($rows[status]=='W'){$weekend++;}
				if($rows[status]=='P'){$present++;}							
				if($rows[status]=='D'){$late++;}
				if($rows[status]=='MR'){$movement++;}
				if($rows[status]=='A'){$absent++;}
				$total_manpower++;
				
				//$joining_date=return_field_value("joining_date","hrm_employee","emp_code='$rows[emp_code]'");			
				if($rows[joining_date]==$txt_date){$new_join++;}					
			 }
			
			if($i%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';
			
			$total_leave=$cl+$sl+$ml+$el+$lwp+$spl;						
			$html .= "<tr height=\"10\" bgcolor=$bgcolor>
						<td width=''>".$designation_chart[$row[designation_id]]['custom_designation']."</td>
						<td width='' align='right'>".$new_join."</td>						
						<td width='' align='right'>".$total_manpower."</td>
						<td width='' align='right'>".$movement."</td>
						<td width='' align='right'>".$present."</td>						
						<td width='' align='right'>".$late."</td> 	
						<td width='' align='right'>".$absent."</td>
						<td width='' align='right'>".$total_leave."</td>	
						<td width=''>&nbsp;</td>
					</tr>";
		
				//department wise total sum and percentage---//
				//total department wise here 
				$total_new_join += $new_join;					
				$total_manpower_dep += $total_manpower;
				$total_movement += $movement;
				$total_present += $present;
				$total_late += $late;
				$total_absent += $absent;
				$total_total_leave += $total_leave;				
				//$grand_total=$total_new_join+$total_manpower_dep+$total_movement+$total_present+$total_late+$total_absent+$total_total_leave+$total_new_join;
		  
		  		//total division wise here 
				$div_total_new_join += $new_join;					
				$div_total_manpower_dep += $total_manpower;
				$div_total_movement += $movement;
				$div_total_present += $present;
				$div_total_late += $late;
				$div_total_absent += $absent;
				$div_total_total_leave += $total_leave;					
				
				
				//total location wise here 
				$loc_total_new_join += $new_join;					
				$loc_total_manpower_dep += $total_manpower;
				$loc_total_movement += $movement;
				$loc_total_present += $present;
				$loc_total_late += $late;
				$loc_total_absent += $absent;
				$loc_total_total_leave += $total_leave;	
				
							  
				//company wise grand_total here last
				$grand_total_new_join += $new_join;					
				$grand_total_manpower_dep += $total_manpower;
				$grand_total_movement += $movement;
				$grand_total_present += $present;
				$grand_total_late += $late;
				$grand_total_absent += $absent;
				$grand_total_total_leave += $total_leave;				
	
	
	}
				
		
		
				$html .= "<tr class=\"tbl_bottom\">
							<td width=''><b>Total</b></td>
							<td width='' align='right'>".$total_new_join."</td>						
							<td width='' align='right'>".$total_manpower_dep."</td>
							<td width='' align='right'>".$total_movement."</td>
							<td width='' align='right'>".$total_present."</td>						
							<td width='' align='right'>".$total_late."</td> 	
							<td width='' align='right'>".$total_absent."</td>
							<td width='' align='right'>".$total_total_leave."</td>	
							<td width=''>&nbsp;</td>
						</tr>
						<tr>
							<td width=''><b>(In %)</b></td>
							<td width=''></td>						
							<td width=''></td>
							<td width='' align='right'>".round(($total_movement*100)/$total_manpower_dep,2)."%</td>
							<td width='' align='right'>".round(($total_present*100)/$total_manpower_dep,2)."%</td>						
							<td width='' align='right'>".round(($total_late*100)/$total_manpower_dep,2)."%</td> 	
							<td width='' align='right'>".round(($total_absent*100)/$total_manpower_dep,2)."%</td>
							<td width='' align='right'>".round(($total_total_leave*100)/$total_manpower_dep,2)."%</td>	
							<td width=''>&nbsp;</td>
						</tr>";
		
		if($num_rows>0) //division total
		{		
				$html .= "<tr class=\"tbl_bottom\">
							<td><b>Division Total</b></td>
							<td align='right'>".$div_total_new_join."</td>						
							<td align='right'>".$div_total_manpower_dep."</td>
							<td align='right'>".$div_total_movement."</td>
							<td align='right'>".$div_total_present."</td>						
							<td align='right'>".$div_total_late."</td> 	
							<td align='right'>".$div_total_absent."</td>
							<td align='right'>".$div_total_total_leave."</td>	
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><b>(In %)</b></td>
							<td></td>						
							<td></td>
							<td align='right'>".round(($div_total_movement*100)/$div_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($div_total_present*100)/$div_total_manpower_dep,2)."%</td>						
							<td align='right'>".round(($div_total_late*100)/$div_total_manpower_dep,2)."%</td> 	
							<td align='right'>".round(($div_total_absent*100)/$div_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($div_total_total_leave*100)/$div_total_manpower_dep,2)."%</td>	
							<td width=''>&nbsp;</td>
						</tr>";
		}
		
		
		if($num_rows>0) //location total
		{		
				$html .= "<tr class=\"tbl_bottom\">
							<td><b>Location Total</b></td>
							<td align='right'>".$loc_total_new_join."</td>						
							<td align='right'>".$loc_total_manpower_dep."</td>
							<td align='right'>".$loc_total_movement."</td>
							<td align='right'>".$loc_total_present."</td>						
							<td align='right'>".$loc_total_late."</td> 	
							<td align='right'>".$loc_total_absent."</td>
							<td align='right'>".$loc_total_total_leave."</td>	
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><b>(In %)</b></td>
							<td></td>						
							<td></td>
							<td align='right'>".round(($loc_total_movement*100)/$loc_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($loc_total_present*100)/$loc_total_manpower_dep,2)."%</td>						
							<td align='right'>".round(($loc_total_late*100)/$loc_total_manpower_dep,2)."%</td> 	
							<td align='right'>".round(($loc_total_absent*100)/$loc_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($loc_total_total_leave*100)/$loc_total_manpower_dep,2)."%</td>	
							<td width=''>&nbsp;</td>
						</tr>";
		}
		
		if($num_rows>0) //company grand total
		{
			$html .= "<tr bgcolor=\"#CCCCCC\" class=\"tbl_bottom\">
						<td width=''><b>Company Total</b></td>
						<td width='' align='right'>".$grand_total_new_join."</td>						
						<td width='' align='right'>".$grand_total_manpower_dep."</td>
						<td width='' align='right'>".$grand_total_movement."</td>
						<td width='' align='right'>".$grand_total_present."</td>						
						<td width='' align='right'>".$grand_total_late."</td> 	
						<td width='' align='right'>".$grand_total_absent."</td>
						<td width='' align='right'>".$grand_total_total_leave."</td>	
						<td width=''>&nbsp;</td>
					</tr>
					<tr>
						<td width=''><b>(In %)</b></td>
						<td width=''></td>						
						<td width=''></td>
						<td width='' align='right'>".round(($grand_total_movement*100)/$grand_total_manpower_dep,2)."%</td>
						<td width='' align='right'>".round(($grand_total_present*100)/$grand_total_manpower_dep,2)."%</td>						
						<td width='' align='right'>".round(($grand_total_late*100)/$grand_total_manpower_dep,2)."%</td> 	
						<td width='' align='right'>".round(($grand_total_absent*100)/$grand_total_manpower_dep,2)."%</td>
						<td width='' align='right'>".round(($grand_total_total_leave*100)/$grand_total_manpower_dep,2)."%</td>	
						<td width=''>&nbsp;</td>
					</tr>";	
		}
	
	$html .="</table> </div>";
	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}



if($action=='daily_inlastout'){
 
	list( $day, $month, $year ) = explode( "-", $txt_date );
	$date = $year . "-" . $month . "-" . $day;
	$int_yestarday = strtotime ( '-1 day' , strtotime ( $date ) ) ;
	$yesterday = date ( 'd-m-Y' , $int_yestarday );
	
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else	$department_id=" and emp.department_id='$department_id'";	
		
	ob_start();
	
	if ($department_id==0){
		$department_id = "";
	}
	else {
		$department_id = "and department_id=". $department_id;
	}
	?>
	<table cellpadding="0" cellspacing="0" border="1" width="900" style="border:1px solid; font-size:12px" rules="all"> 
	<?
	$sl=0;
	$sql_dept = mysql_query("select * from lib_department where status_active=1 and id in (select distinct(department_id) from hrm_employee where status_active=1 $department_id order by department_id)");	
	while($dept_rows = mysql_fetch_array($sql_dept))
	{
		
	?>    
	       
	<?php	
			$sql = "SELECT 
						CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, 
						emp.id_card_no, 
						emp.punch_card_no, 
						a.* 
					FROM 
						hrm_attendance a,
						hrm_employee emp 
					WHERE 
						emp.status_active=1 and 
						a.attnd_date like '$date' and 
						a.emp_code=emp.emp_code and 
						emp.department_id=$dept_rows[id] 
						$location_id $section_id $cbo_company_id  $status $subsection_id $designation_id $category 
					order by emp.section_id, CAST(emp.id_card_no as SIGNED)";
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );						
			$emp_tot=mysql_num_rows($result);
			//echo $sql;
			if ($emp_tot>0)
				{	
					$new_section=array();
					while( $row = mysql_fetch_array( $result ) ) 
					{
						if (in_array($row['section_id'], $new_section))
							{
								$i++;
								$sl++;
							}
						else
							{									
							?>                            								 
                                <tr><td colspan="10" align="center"><b><? echo $company_details[$row['company_id']]['company_name']; ?></b></td></tr>
                                <tr><td colspan="5" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Department : </b><? echo $department_details[$row['department_id']]['department_name']; ?></td><td colspan="5" align="right">Attendance Date : <? echo convert_to_mysql_date($date); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
                                <tr><td colspan="5" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Section : </b><? echo $section_details[$row['section_id']]['section_name']; ?></td><td colspan="5"  align="right">  Out Time Date : <? echo $yesterday; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
                                <tr class="display">
                                    <td width="40"><b>SL</b></td> 
                                    <td width="90"><b>Emp Code</b></td>
                                    <td width="100"><b>ID Card No</b></td>											
                                    <td width="200"><b>Name</b></td>
                                    <td width="100"><b>Design.</b></td>	 							
                                    <td width="70"><b>In Time</b></td>
                                    <td width="70"><b>Out Time</b></td>
                                    <td width="70"><b>Status</b></td>
                                    <td width="70"><b>Late Min</b></td>	
                                    <td width="170"><b>Remarks</b></td> 									
                                </tr>
						  <?php          
                                    $new_section[$i]=$row[section_id];
                                    $i++;
									$sl=1;
                            }
					
					$lastout = return_field_value("sign_out_time","hrm_attendance","emp_code=$row[emp_code] and attnd_date='".convert_to_mysql_date($yesterday)."' ");
					
					?>
                    				
					<tr class="display">
                    	 <td width="40"><? echo $sl; ?></td> 
						<td width="90">&nbsp;<? echo $row['emp_code'];?></td>
						<td width="100">&nbsp;<? echo $row['id_card_no']; ?></td>						
						<td width="200"><? echo $row['name']; ?></td>
						<td width="100" >&nbsp;<? echo $designation_chart[$row['designation_id']]['custom_designation']; ?></td>	 								
						<td width="70"><? if($row['sign_in_time']!='00:00:00') {echo DATE("g:i a", STRTOTIME($row['sign_in_time']));} else echo "00:00"; ?></td>
						<td width="70"><? if($lastout!='00:00:00') {echo DATE("g:i a", STRTOTIME($lastout));} else echo "00:00"; ?></td>
						<td width="70"><? echo $row['status']; ?></td>
						<td width="70"><? echo $row['late_time_min']; ?></td>
                        <td width="170">&nbsp;</td>					 
					</tr>
	<?	
				}   
		   
		}
			
	}
	?>
			</table>
				<table width="900" style="margin-top:80px">           		
					<tr>
						<td colspan="4" align="center">--------------------------</td>
						<td width="100">&nbsp;</td>
						<td colspan="4" align="center">--------------------------</td>
					</tr>
					<tr>
						<td colspan="4" align="center">Prepared By</td>
						<td>&nbsp;</td>
						<td colspan="4" align="center">Approved By</td>
					</tr>
			</table>		
           
	<?
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,trim($html));	
	
	echo "$html"."####"."$name";		
	exit();
	
}


if($action=='male_female_ratio'){
 		
 	$company=$cbo_company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";		
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";	
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	ob_start();	
	
	$addrs ="";		
	if($company_details[$company]['plot_no'] !='') $addrs .= $company_details[$company]['plot_no'].", "; 
	if($company_details[$company]['level_no'] !='') $addrs .= $company_details[$company]['level_no'].", "; 
	if($company_details[$company]['block_no'] !='') $addrs .= $company_details[$company]['block_no'].", "; 
	if($company_details[$company]['road_no'] !='') $addrs .= $company_details[$company]['road_no'].", "; 
	if($company_details[$company]['city'] !='') $addrs .= $company_details[$company]['city']." "; 
	
	if($group_by_id=="company_id")$caption = "Company";	
	if($group_by_id=="location_id")$caption = "Location";	
	if($group_by_id=="division_id")$caption = "Division";	
	if($group_by_id=="department_id")$caption = "Department";	
	if($group_by_id=="section_id")$caption = "Section";	
	if($group_by_id=="subsection_id")$caption = "Sub Section";
	?> 
    <div align="center" style="width:750px; font-size:12px"><b><? echo $company_details[$company]['company_name'];?></b>
	<div align="center" style="width:750px; font-size:10px"><b>Address : <? echo $addrs;?></b>
	<div align="center" style="width:750px; font-size:10px"><b>Employee Male Female Ratio</b>   
	<table cellpadding="0" cellspacing="1" border="1" width="750" class="rpt_table" rules="all"> 
    	<thead>
            <th width="40"><b>SL</b></th> 
            <th width=""><b><? echo $caption; ?></b></th>
            <th width="110"><b>Total Employee</b></th>											
            <th width="100"><b>Male</b></th>
            <th width="100"><b>Female</b></th>	 							
            <th width="100"><b>Male(%)</b></th>
            <th width="100"><b>Female(%)</b></th>												 									
        </thead>       
		<?php	
			$sql = "SELECT *, 
						COUNT(CASE WHEN status_active=1 THEN id END) as total_emp,
						COUNT(CASE WHEN sex=0 THEN id END) as total_male,
						COUNT(CASE WHEN sex=1 THEN id END) as total_female 
					FROM 
						hrm_employee 
					WHERE
						status_active=1 
						$category $cbo_company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id 						
						group by $group_by_id";
			//echo $sql;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
			
			$i=0;
			$new_company=array();
			$grand_total_emp = 0;$grand_total_male = 0;$grand_total_female = 0;	$grand_total_male_in_per = 0;$grand_total_female_in_per = 0;
			while($row = mysql_fetch_array($result) )
			{								
				
				if($group_by_id=="company_id")$caption_value = $company_details[$row['company_id']]['company_name'];
				if($group_by_id=="location_id")$caption_value = $location_details[$row['location_id']]['location_name'];
				if($group_by_id=="division_id")$caption_value = $division_details[$row['division_id']]['division_name'];
				if($group_by_id=="department_id")$caption_value = $department_details[$row['department_id']]['department_name'];
				if($group_by_id=="section_id")$caption_value = $section_details[$row['section_id']]['section_name'];
				if($group_by_id=="subsection_id")$caption_value = $subsection_details[$row['subsection_id']]['subsection_name'];
			
				$i++;
                ?>				                       				
                        <tr class="display">
                            <td width="40"><? echo $i; ?></td> 
                            <td width=""><? echo $caption_value;?></td>                           					
                            <td width="110" align="right"><? echo $row['total_emp']; ?></td>
                            <td width="100" align="right"><? echo $row['total_male']; ?></td>
                            <td width="100" align="right"><? echo $row['total_female']; ?></td>
                            <?
								$male_in_per = number_format(($row['total_male']*100)/$row['total_emp']);
								$female_in_per = number_format(($row['total_female']*100)/$row['total_emp']);								
                            ?>	 								
                            <td width="100" align="right"><? echo $male_in_per; ?></td>
                            <td width="100" align="right"><? echo $female_in_per; ?></td>                                                   			 
                        </tr>	
           	<? 
			
						$grand_total_emp += $row['total_emp'];
						$grand_total_male += $row['total_male'];
						$grand_total_female += $row['total_female'];
						$male_in_per = number_format(($grand_total_male*100)/$grand_total_emp);
						$female_in_per = number_format(($grand_total_female*100)/$grand_total_emp);		
						$grand_total_male_in_per = $male_in_per;
						$grand_total_female_in_per = $female_in_per;
															
				} 
			
			?>             
                  	<tr class="tbl_bottom"><td colspan="2"><b>Grand Total</b></td><td><? echo $grand_total_emp; ?></td><td><? echo $grand_total_male; ?></td><td><? echo $grand_total_female; ?></td><td><? echo $grand_total_male_in_per; ?></td><td><? echo $grand_total_female_in_per; ?></td></tr>        		
			</table><br /><br /><br />
        <table width="750">           		
            <tr>
                <td width="180" align="center">--------------------------</td>						
                <td width="190" align="center">--------------------------</td>
                <td width="190" align="center">--------------------------</td>
                <td width="190" colspan="4" align="center">--------------------------</td>
            </tr>
            <tr>
                <td align="center">Officer IT</td>
                <td align="center">Admin/HRM</td>
                <td align="center">PM/APM</td>						
                <td colspan="4" align="center">GM/DGM/AGM/FM</td>
            </tr>
        </table>		
           
	<?		
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}


if($action=='religion_ratio'){
 		
 	$company=$cbo_company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";		
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";	
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	ob_start();	
	
	$addrs ="";		
	if($company_details[$company]['plot_no'] !='') $addrs .= $company_details[$company]['plot_no'].", "; 
	if($company_details[$company]['level_no'] !='') $addrs .= $company_details[$company]['level_no'].", "; 
	if($company_details[$company]['block_no'] !='') $addrs .= $company_details[$company]['block_no'].", "; 
	if($company_details[$company]['road_no'] !='') $addrs .= $company_details[$company]['road_no'].", "; 
	if($company_details[$company]['city'] !='') $addrs .= $company_details[$company]['city']." "; 
	
	if($group_by_id=="company_id")$caption = "Company";	
	if($group_by_id=="location_id")$caption = "Location";	
	if($group_by_id=="division_id")$caption = "Division";	
	if($group_by_id=="department_id")$caption = "Department";	
	if($group_by_id=="section_id")$caption = "Section";	
	if($group_by_id=="subsection_id")$caption = "Sub Section";
	?> 
    <div align="center" style="width:950px; font-size:12px"><b><? echo $company_details[$company]['company_name'];?></b>
	<div align="center" style="width:950px; font-size:10px"><b>Address : <? echo $addrs;?></b>
	<div align="center" style="width:950px; font-size:10px"><b>Employee Religion Ratio</b>   
	<table cellpadding="0" cellspacing="1" border="1" width="950" class="rpt_table" rules="all"> 
    	<thead>
            <th width="20"><b>SL</b></th> 
            <th width=""><b><? echo $caption; ?></b></th>
            <th width="70"><b>Total Emp</b></th>											
            <th width="50"><b>Muslim</b></th>
            <th width="50"><b>Hindu</b></th>
            <th width="60"><b>Christian</b></th>
            <th width="60"><b>Buddhist</b></th>
            <th width="50"><b>Others</b></th>
            <th width="70"><b>Muslim(%)</b></th>
            <th width="70"><b>Hindu(%)</b></th>
            <th width="70"><b>Christian(%)</b></th>
            <th width="70"><b>Buddhist(%)</b></th>
            <th width="70"><b>Others(%)</b></th>												 									
        </thead>       
		<?php	
			$sql = "SELECT *, 
						COUNT(CASE WHEN status_active=1 THEN id END) as total_emp,
						COUNT(CASE WHEN religion like '%Islam%' THEN id END) as total_islam,
						COUNT(CASE WHEN religion like '%Hindu%' THEN id END) as total_hindu,
						COUNT(CASE WHEN religion like '%Christian%' THEN id END) as total_christan,
						COUNT(CASE WHEN religion like '%Buddhist%' THEN id END) as total_buddhist,
						COUNT(CASE WHEN religion like '%Others%' THEN id END) as total_others 
					FROM 
						hrm_employee 
					WHERE
						status_active=1 
						$category $cbo_company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id 						
						group by $group_by_id";
			//echo $sql;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
 			$i=0;
			$new_company=array();
			$grand_total_emp = 0;
			$grand_total_muslim = 0;
			$grand_total_hindu = 0;	
			$grand_total_christian = 0;	
			$grand_total_buddhist = 0;	
			$grand_total_other = 0;	
			$grand_total_muslim_in_per =0;
			$grand_total_hindu_in_per =0;
			$grand_total_christian_in_per =0;
			$grand_total_buddhist_in_per =0;
			$grand_total_other_in_per =0;
 			while($row = mysql_fetch_array($result) )
			{								
				
				if($group_by_id=="company_id")$caption_value = $company_details[$row['company_id']]['company_name'];
				if($group_by_id=="location_id")$caption_value = $location_details[$row['location_id']]['location_name'];
				if($group_by_id=="division_id")$caption_value = $division_details[$row['division_id']]['division_name'];
				if($group_by_id=="department_id")$caption_value = $department_details[$row['department_id']]['department_name'];
				if($group_by_id=="section_id")$caption_value = $section_details[$row['section_id']]['section_name'];
				if($group_by_id=="subsection_id")$caption_value = $subsection_details[$row['subsection_id']]['subsection_name'];
			
				$i++;
				
				$grand_total_emp += $row['total_emp'];
				$grand_total_muslim += $row['total_islam'];
				$grand_total_hindu += $row['total_hindu'];
				$grand_total_christian += $row['total_christan'];
				$grand_total_buddhist += $row['total_buddhist'];
				$grand_total_other += $row['total_others'];
				$grand_total_muslim_in_per = number_format(($grand_total_muslim*100)/$grand_total_emp);
				$grand_total_hindu_in_per =number_format(($grand_total_hindu*100)/$grand_total_emp);
				$grand_total_christian_in_per =number_format(($grand_total_christian*100)/$grand_total_emp);
				$grand_total_buddhist_in_per =number_format(($grand_total_buddhist*100)/$grand_total_emp);
				$grand_total_other_in_per =number_format(($grand_total_other*100)/$grand_total_emp);
				
				if($i%2==0) $bgcolor='#FFFFFF';
				else $bgcolor='#EEEEEE'; 		 

                ?>				                       				
                        <tr class="display" bgcolor="<? echo $bgcolor; ?>" >
                            <td width="40"><? echo $i; ?></td> 
                            <td width=""><? echo $caption_value;?></td>                           					
                            <td width="" align="right"><? echo $row['total_emp']; ?></td>
                            <td width="" align="right"><? echo $row['total_islam']; ?></td>
                            <td width="" align="right"><? echo $row['total_hindu']; ?></td>
                            <td width="" align="right"><? echo $row['total_christan']; ?></td>
                            <td width="" align="right"><? echo $row['total_buddhist']; ?></td>
                            <td width="" align="right"><? echo $row['total_others']; ?></td>
                            <?
								$islam_in_per = number_format(($row['total_islam']*100)/$row['total_emp']);
								$hindu_in_per = number_format(($row['total_hindu']*100)/$row['total_emp']);	
								$christian_in_per = number_format(($row['total_christan']*100)/$row['total_emp']);
								$buddhist_in_per = number_format(($row['total_buddhist']*100)/$row['total_emp']);								
								$others_in_per = number_format(($row['total_others']*100)/$row['total_emp']);
                             ?>
                            <td width="" align="right"><? echo $islam_in_per."%"; ?></td>
                            <td width="" align="right"><? echo $hindu_in_per."%"; ?></td>
                            <td width="" align="right"><? echo $christian_in_per."%"; ?></td>
                            <td width="" align="right"><? echo $buddhist_in_per."%"; ?></td>
                            <td width="" align="right"><? echo $others_in_per."%"; ?></td>                                                   			 
                        </tr>	
           	<? 
			
						
															
				} 
			
			?>             
                  	<tr class="tbl_bottom">
                    	<td colspan="2"><b>Grand Total</b></td>
                        <td><? echo $grand_total_emp; ?></td>
                        <td><? echo $grand_total_muslim; ?></td>
                        <td><? echo $grand_total_hindu; ?></td>
                        <td><? echo $grand_total_christian; ?></td>
                        <td><? echo $grand_total_buddhist; ?></td>
                        <td><? echo $grand_total_other; ?></td>
                        <td><? echo $grand_total_muslim_in_per."%"; ?></td>
                        <td><? echo $grand_total_hindu_in_per."%"; ?></td>
                        <td><? echo $grand_total_christian_in_per."%"; ?></td>
                        <td><? echo $grand_total_buddhist_in_per."%"; ?></td>
                        <td><? echo $grand_total_other_in_per."%"; ?></td>
                     </tr>    
 			</table><br /><br /><br />
        <table width="750">           		
            <tr>
                <td width="180" align="center">--------------------------</td>						
                <td width="190" align="center">--------------------------</td>
                <td width="190" align="center">--------------------------</td>
                <td width="190" colspan="4" align="center">--------------------------</td>
            </tr>
            <tr>
                <td align="center">Officer IT</td>
                <td align="center">Admin/HRM</td>
                <td align="center">PM/APM</td>						
                <td colspan="4" align="center">GM/DGM/AGM/FM</td>
            </tr>
        </table>		
           
	<?		
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}





if($action=='ot_sheet_payment_actual'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
	}
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	$table_width=250+($get_days*70);
	if($table_width<900) $table_width=900;
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($department_id==0) $department_id=""; else	$department_id="and department_id='$department_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($emp_code=='') $emp_code =""; else $emp_code ="and emp_code in ($emp_code)";
	
			
	ob_start();	
		
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:<? echo $table_width; ?>">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1">Employee OT Sheet</font>
    </div>
    <table cellpadding="0" cellspacing="0" border="1" width="<? echo $table_width; ?>" class="rpt_table" style="font-size:10px; border:1px solid #000000"> 
       <thead>        
            <tr>
            	<th width="40"><b>SL</b></th>
                <th width="70"><b>ID Card</b></th>
                <th width="70"><b>Emp Code</b></th>
                <th align="120"><b>Name</b></th>
                <th align="100"><b>Designation</b></th>
				<?
                    for($j=0;$j<$get_days;$j++){				
                    $newdate = date("Y-m-d",strtotime("+".$j." day", strtotime($fromdate)));
                ?>
                    <th width="60" style="font-size:9px"><? echo convert_to_mysql_date($newdate); ?></th>
                <?	
                    }
                ?>
                <th>Total</th>  
            </tr>
       </thead>       
						
    <?	
	
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM hrm_employee 
				WHERE 
				emp_code!=''
				$category 
				$cbo_company_id 
				$location_id
				$division_id 
				$section_id 
				$subsection_id 
				$designation_id 
				$department_id 
				$emp_code
				order by id_card_no asc";   //order by company_id, section_id";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_section=array();
		while($rows=mysql_fetch_array($result)){
			if (in_array($rows['section_id'], $new_section))
					{
						$i++;
					}
				else
					{
						$new_section[$i]=$rows[section_id];
						$i++;
					?>
						<tr bgcolor="#CCCCCC">
							<td colspan="<? echo $get_days+6; ?>" ><b>Section : <? echo $section_details[$rows['section_id']]['section_name']; ?></b></td>
						</tr>
                    <?    
					}
					
					
			if($sl%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';		
					
	?>    
    		
            <tr bgcolor="<? echo $bgcolor; ?>">
            	<td><? echo $i; ?></td>
            	<td><? echo $rows['id_card_no']; ?></td>
                <td><? echo $rows['emp_code']; ?></td>
                <td><? echo $rows['name']; ?></td>
                <td><? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td>               
                <?
			    $sql_attn = "SELECT * FROM hrm_attendance WHERE emp_code=$rows[emp_code] and attnd_date>='$fromdate' and attnd_date<='$todate'";
				$result_attn = mysql_query( $sql_attn ) or die( $sql_attn . "<br />" . mysql_error() );
				$arr_attn=array();
				while($att_row=mysql_fetch_array($result_attn)){
					$hours[0] = abs((int)($att_row['total_over_time_min']/60)); //hours
					$hours[1] = abs((int)($att_row['total_over_time_min']%60)); //minutes
					$ot_mins_cal=0;
					if($ot_fraction==1) //ot fraction allow
					{						
						if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
						else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;
					}
					if($ot_fraction==2) // no ot fraction
					{						
						//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
						if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;						
					}
					if($ot_fraction==3) //at actual minutes
					{						
						$ot_mins_cal = 	$att_row['total_over_time_min'];				
					}
					
					$total_hours = sprintf("%d.%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
					$arr_attn[$att_row['attnd_date']]['total_over_time_min']=$total_hours;
				}
				
				$total_ot=0;
				for($k=0;$k<$get_days;$k++){				
				$newdate = date("Y-m-d",strtotime("+".$k." day", strtotime($fromdate)));
				//$total_ot += $arr_attn[$newdate]['total_over_time_min'];
				?>
				<td align="right"><b><? echo $arr_attn[$newdate]['total_over_time_min']; ?></b></td>
				<?				
				}
				$total_ot=get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '$fromdate' and '$todate' and is_regular_day=1",$one_hr_ot_unit,$ot_fraction,$ot_start_min,$ot_slot);
				?>
                <td align="right"><b><? echo $total_ot;?></b></td>
            </tr>
    
		<?
            }
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}


if($action=='ot_sheet'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
	}
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	$table_width=250+($get_days*70);
	if($table_width<900) $table_width=900;
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($department_id==0) $department_id=""; else	$department_id="and department_id='$department_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	
	$salary_breakdown = array();
	$sql_sal="select * from hrm_employee_salary";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_breakdown[$row_sal[emp_code]][$row_sal[payroll_head]]=$row_sal[amount];
	}
	
			
	ob_start();	
		
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:900px">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1">Employee OT Sheet</font>
    </div>
    <table cellpadding="0" cellspacing="0" border="1" width="950" class="rpt_table" > 
       <thead>        
            <tr>
            	<th width="40"><b>SL</b></th>
                <th width="70"><b>ID Card</b></th>
                <th width="70"><b>Emp Code</b></th>
                <th width="120"><b>Name</b></th>
                <th width="120"><b>Designation</b></th>
                <th width="100">Total OT HR</th>
                <th width="80">OT Rate</th>
                <th width="100">OT Amount</th>
                <th>Signature</th>
            </tr>
       </thead>       
						
    <?	
	
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM hrm_employee 
				WHERE 
				emp_code!=''
				$category 
				$cbo_company_id 
				$location_id
				$division_id 
				$section_id 
				$subsection_id 
				$designation_id 
				$department_id 
				order by company_id,section_id, CAST(id_card_no as SIGNED) ASC";  //order by company_id, section_id";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_section=array();
		while($rows=mysql_fetch_array($result)){
			if (in_array($rows['section_id'], $new_section))
					{
						$i++;
					}
				else
					{
						$new_section[$i]=$rows[section_id];
						$i++;
					?>
						<tr bgcolor="#CCCCCC">
							<td colspan="9"><b>Section : <? echo $section_details[$rows['section_id']]['section_name']; ?></b></td>
						</tr>
                    <?    
					}
					
					
			if($sl%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';		
			$hourly_ot_rate=number_format(($salary_breakdown[$rows[emp_code]][1]/208)*2,2);		
	?>    
    		
            <tr bgcolor="<? echo $bgcolor; ?>">
            	<td height="60"><? echo $i; ?></td>
            	<td><? echo $rows['id_card_no']; ?></td>
                <td><? echo $rows['emp_code']; ?></td>
                <td><? echo $rows['name']; ?></td>
                <td><? if ($designation_chart[$rows['designation_id']]['custom_designation']!="") echo $designation_chart[$rows['designation_id']]['custom_designation']; else echo "--"; ?></td>               
                <?
				$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".($fromdate)."' and '".($todate)."'",$one_hr_ot_unit,$ot_fraction,$ot_start_min,1200);
				 
				?>
                <td width="100" align="right"><? $grand_total_sec+=$total_ot_hr; echo $total_ot_hr; ?></td>
                <td width="80" align="right"><? echo $hourly_ot_rate; ?></td>
                <td width="100" align="right"><? $ot_amount=$total_ot_hr*$hourly_ot_rate; $ot_amount_sec+=$ot_amount; echo $ot_amount; ?></td>
                <td></td>
                 
            </tr>
    
		<?
            }
        ?>
        <tr class="tbl_bottom">
        	<td colspan="5">Total</td><td align="right"><? echo $grand_total_sec; ?></td><td></td><td align="right"><b><? echo number_format($ot_amount_sec,2); ?></b></td><td></td>
        </tr>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}

if($action=='extra_ot_sheet'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	$table_width=250+($get_days*70);
	
	if($table_width<900) $table_width=900;
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($department_id==0) $department_id=""; else	$department_id="and department_id='$department_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	
			
	ob_start();	
		
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:<? echo $table_width; ?>">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1">Employee Extra OT Sheet</font>
    </div>
    <table cellpadding="0" cellspacing="0" border="1" width="<? echo $table_width; ?>" class="rpt_table" style="font-size:10px; border:1px solid #000000"> 
       <thead>        
            <tr>
            	<th width="40"><b>SL</b></th>
                <th width="70"><b>ID Card</b></th>
                <th width="70"><b>Emp Code</b></th>
                <th align="120"><b>Name</b></th>
                <th align="100"><b>Designation</b></th>
				<?
                    for($j=0;$j<$get_days;$j++){				
                    $newdate = date("Y-m-d",strtotime("+".$j." day", strtotime($fromdate)));
                ?>
                    <th width="60" style="font-size:9px"><? echo convert_to_mysql_date($newdate); ?></th>
                <?	
                    }
                ?>
                <th>Total</th>  
            </tr>
       </thead>       
						
    <?	
	
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM hrm_employee 
				WHERE 
				emp_code!=''
				$category 
				$cbo_company_id 
				$location_id
				$division_id 
				$section_id 
				$subsection_id 
				$designation_id 
				$department_id 
				order by company_id,section_id, CAST(id_card_no as SIGNED) ASC";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_section=array();
		while($rows=mysql_fetch_array($result)){
			if (in_array($rows['section_id'], $new_section))
					{
						$i++;
					}
				else
					{
						$new_section[$i]=$rows[section_id];
						$i++;
					?>
						<tr bgcolor="#CCCCCC">
							<td colspan="<? echo $get_days+6; ?>" ><b>Section : <? echo $section_details[$rows['section_id']]['section_name']; ?></b></td>
						</tr>
                    <?    
					}
					
					
			if($sl%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';		
					
	?>    
    		
            <tr bgcolor="<? echo $bgcolor; ?>">
            	<td><? echo $i; ?></td>
            	<td><? echo $rows['id_card_no']; ?></td>
                <td><? echo $rows['emp_code']; ?></td>
                <td><? echo $rows['name']; ?></td>
                <td><? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td>               
                <?
			    $sql_attn = "SELECT * FROM hrm_attendance WHERE emp_code=$rows[emp_code] and attnd_date>='$fromdate' and attnd_date<='$todate'";
				$result_attn = mysql_query( $sql_attn ) or die( $sql_attn . "<br />" . mysql_error() );
				$arr_attn=array();
				while($att_row=mysql_fetch_array($result_attn)){
					if($att_row['total_over_time_min']>$buyer_ot_min_comp)$att_row['total_over_time_min']=$att_row['total_over_time_min']-$buyer_ot_min_comp;
					$hours[0] = abs((int)($att_row['total_over_time_min']/60)); //hours
					$hours[1] = abs((int)($att_row['total_over_time_min']%60)); //minutes
					$ot_mins_cal=0;
					if($ot_fraction==1) //ot fraction allow
					{						
						if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
						else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;
					}
					if($ot_fraction==2) // no ot fraction
					{						
						//if($hours[1]>=30 && $hours[1]<50){$ot_mins_cal = ($hours[0]*60)+30;}
						if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
						else $ot_mins_cal = $hours[0]*60;						
					}
					if($ot_fraction==3) //at actual minutes
					{						
						$ot_mins_cal = 	$att_row['total_over_time_min'];				
					}
															
					$total_hours = sprintf("%d.%02d", abs((int)($ot_mins_cal/60)), abs((int)($ot_mins_cal%60)));
					
					$arr_attn[$att_row['attnd_date']]['total_over_time_min']=$total_hours;
				}
				
				$total_ot=0;
				for($k=0;$k<$get_days;$k++){				
				$newdate = date("Y-m-d",strtotime("+".$k." day", strtotime($fromdate)));
				$total_ot += $arr_attn[$newdate]['total_over_time_min'];
				$explode_total_ot = explode(".",$total_ot);
				if($explode_total_ot[1]==6) $total_ot = $explode_total_ot[0]+1;
				?>
				<td align="right"><b><? echo $arr_attn[$newdate]['total_over_time_min']; ?></b></td>
				<?				
				}
				//$total_ot=explode(".",$total_ot); $total_ot[0]."hr ".$total_ot[1]
				?>
                <td align="right"><b><? echo number_format($total_ot,2);?></b></td>
            </tr>
    
		<?
            }
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}



if($action=='monthly_extra_ot_sheet'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}
	
	$explode_date_part = explode("_",$cbo_month_selector);
	$date_part = $explode_date_part[0];
	
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($department_id==0) $department_id=""; else	$department_id="and a.department_id='$department_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
			
	ob_start();	
	
	$addrs ="";		
	if($company_details[$company_id]['plot_no'] !='') $addrs .= $company_details[$company_id]['plot_no'].", "; 
	if($company_details[$company_id]['level_no'] !='') $addrs .= $company_details[$company_id]['level_no'].", "; 
	if($company_details[$company_id]['block_no'] !='') $addrs .= $company_details[$company_id]['block_no'].", "; 
	if($company_details[$company_id]['road_no'] !='') $addrs .= $company_details[$company_id]['road_no'].", "; 
	if($company_details[$company_id]['city'] !='') $addrs .= $company_details[$company_id]['city'].", "; 
	
		
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:800px">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1"><? echo $addrs; ?></font><br />
        <font size="-1">Extra OT Sheet: <? echo date("F, Y",strtotime($date_part)); ?> </font>
    </div>
    <table cellpadding="0" cellspacing="0" width="800" class="rpt_table" style="font-size:12px; border:1px solid #000000" rules="all"> 
           
      	
    <?	
			
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no,emp.joining_date,emp.designation_id, emp.punch_card_no, a.* 
				FROM 
				hrm_salary_mst a, hrm_employee emp 
				WHERE 
				emp.emp_code!='' and 
				a.salary_periods  like '$date_part' and 
				a.emp_code=emp.emp_code
				$cbo_company_id  
				$division_id
				$department_id 
				$location_id 
				$section_id 				
				$subsection_id 
				$designation_id 
				$category 
				order by emp.section_id, CAST(emp.id_card_no as SIGNED)";

		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_section=array();
		while($rows=mysql_fetch_array($result))
		{
			if (in_array($rows['section_id'], $new_section))
					{
						$i++;
					}
				else
					{
						$new_section[$i]=$rows[section_id];
						$i++;
						
						$division = $division_details[$rows['division_id']]['division_name'];
						$department = $department_details[$rows['department_id']]['department_name'];
						$section = $section_details[$rows['section_id']]['section_name'];
						$subsection = $subsection_details[$rows['subsection_id']]['subsection_name'];
						$cost_center="";		
						if($division !='') $cost_center .= "Division : <b>".$division."</b> "; 
						if($department !='') $cost_center .= "Department : <b>".$department."</b> "; 
						if($section !='') $cost_center .= "Section : <b>".$section."</b> "; 
						if($subsection !='') $cost_center .= "Sub Section : <b>".$subsection."</b> "; 
						
					?>						
                     
                        <tr class="tbl_top">
                            <th colspan="12"><? echo $cost_center; ?></th>
                        </tr>
                        <tr class="tbl_top">
                            <th colspan="6">General Information</th>
                            <th colspan="5">Extra Over Time</th> 
                            <th rowspan="2">Signature</th>                           
                        </tr>
                        <tr class="tbl_top">
                            <th width="30"><b>SL</b></th>
                            <th width="70"><b>ID Card</b></th>
                            <th width="70"><b>Emp Code</b></th>
                            <th align="100"><b>Name</b></th>
                            <th align="100"><b>Designation</b></th>
                            <th align="80"><b>DOJ</b></th>
                            <th align="60"><b>Gross</b></th>
                            <th align="60"><b>Basic</b></th>
                            <th align="60"><b>EOT Hrs</b></th>
                            <th align="60"><b>EOT Rate</b></th>
                            <th align="60"><b>EOT Amt</b></th>
                        </tr>
                   
                    <?    
					}
			
			
			
			if($i%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';		
					
		?>    
    		
            <tr bgcolor="<? echo $bgcolor; ?>" height="50">
            	<td><? echo $i; ?></td>
            	<td><? echo $rows['id_card_no']; ?></td>
                <td><? echo $rows['emp_code']; ?></td>
                <td><? echo $rows['name']; ?></td>
                <td><? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td>
                <td><? echo convert_to_mysql_date($rows['joining_date']); ?></td>
                <td><? echo $rows['gross_salary']; ?></td>
                <td><? echo $rows['basic_salary']; ?></td>
                <td><? echo $rows['e_over_time']; ?></td> 	
                <td><? echo $rows['over_time_rate']; ?></td>
                <td><? echo round($rows['e_over_time']*$rows['over_time_rate']); ?></td>
                <td>&nbsp;</td>               
            </tr>
    
		<?
            }
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}



if($action=='line_wise_ot'){
		
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$company_id=$cbo_company_id;
	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id='$department_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";

	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
					
	ob_start();	
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
	}
	
		
	?>
    <div align="center">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1">Employee OT Summary (Section Wise)</font>
    </div>    
	<table cellpadding="0" cellspacing="0" border="1" width="<? echo ($get_days*50)+250; ?>" class="rpt_table" style="border:1px solid #000; font-size:10px">       
    	<thead>										
            <tr>
                <th width="30"><b>SL</b></th><th width="70">&nbsp;<b>Line</b></th>
                <?
                    for($j=0;$j<$get_days;$j++)
                    {
                ?>
                        <th width="35"><b><? echo $j+1; ?></b>
                            
                 <? } ?>
                 <th width="50"><b>Total</b></th>
			</tr>
		</th>
	<?	
		
		$attn_sql = "SELECT section_id,total_over_time_min,attnd_date				
					FROM hrm_attendance
					WHERE 
					attnd_date like '%$date_part%'
					$cbo_company_id 
					$category 
					$department_id 
					$location_id 
					$division_id
					$section_id 
					$designation_id
					order by attnd_date,section_id ASC ";
								
		//echo $attn_sql;die;
		$results = mysql_query( $attn_sql ) or die( $attn_sql . "<br />" . mysql_error() );
		$i=1;
		$new_section = array();
		$new_date = array();
		$ot_arr = array();			
		if(mysql_num_rows($results)>0){
			$arrange_ot_arr=array();
			while($ot_row=mysql_fetch_assoc($results)){
				 
				$hours[0] = abs((int)($ot_row['total_over_time_min']/60)); //hours
				$hours[1] = abs((int)($ot_row['total_over_time_min']%60)); //minutes
				$ot_mins_cal=0;
				if($ot_fraction==1) //ot fraction allow
				{						
					if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
					else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
					else $ot_mins_cal = $hours[0]*60;
				}
				if($ot_fraction==2) // no ot fraction
				{						
					if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
					else $ot_mins_cal = $hours[0]*60;						
				}
				if($ot_fraction==3) //at actual minutes
				{						
					$ot_mins_cal = 	$ot_row['total_over_time_min'];				
				}
				
				//if(abs((int)($ot_mins_cal%60))==3) $ot_adjust_hr=abs((int)($ot_mins_cal/60)).".50";else $ot_adjust_hr=abs((int)($ot_mins_cal/60));
				$total_hours = sprintf("%d", abs((int)($ot_mins_cal/60)));
				$total_mints = sprintf("%02d",abs((int)($ot_mins_cal%60)));
				if($total_mints==30) $total_hours_mints = $total_hours.".50"; else $total_hours_mints = $total_hours.".".$total_mints;
				//$ot_row['total_over_time_min'] = $total_hours_mints;
				//$arrange_ot_arr = $ot_row;
				
				if( in_array($ot_row['section_id'],$new_section) )
				{
										
					if( in_array($ot_row['attnd_date'],$new_date) )
					{
						$arrange_ot_arr[$ot_row['section_id']][$ot_row['attnd_date']]+=$total_hours_mints;
					}
					else
					{
						$arrange_ot_arr[$ot_row['section_id']][$ot_row['attnd_date']]=$total_hours_mints;
						$new_date[$ot_row['attnd_date']]=$ot_row['attnd_date'];
					}
					
				}
				else
				{
					
					$new_section[$ot_row['section_id']]=$ot_row['section_id'];
					
					if( in_array($ot_row['attnd_date'],$new_date) )
					{
						$arrange_ot_arr[$ot_row['section_id']][$ot_row['attnd_date']]+=$total_hours_mints;
					}
					else
					{
						$arrange_ot_arr[$ot_row['section_id']][$ot_row['attnd_date']]=$total_hours_mints;
						$new_date[$ot_row['attnd_date']]=$ot_row['attnd_date'];
					}
					
				}
				
			}
			
			}
			//print_r($arrange_ot_arr);die;
			
			
			
			foreach( $arrange_ot_arr as $key=>$val )
			{
					if($i%2==0) $bgcolor='#FFFFFF';
					else $bgcolor='#EEEEEE'; 		 
					
					?>    
						<tr bgcolor="<? echo $bgcolor; ?>">
							<td><? echo $i++; ?></td>
							<td>&nbsp;<? echo $section_details[$key]['section_name']; ?></td>
					<?									
							$total_ot_min=0;
							$j=1;
							for($j=1;$j<=$get_days;$j++)
							{
								if(strlen($j)<2) $cdate = $date_part."-0".$j;
								else $cdate = $date_part."-".$j;
								
						?>        					
								<td align="right"><? echo $val[$cdate];?></td> 
						<?
								$total_ot_min += $val[$cdate];
							}
					?>    
							<td align="right"><? echo $total_ot_min; ?></td>              	
						</tr>  
					<?	
				
			}
    ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}





if($action=='manual_attn_report'){
	
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	//$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and emp.company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and emp.department_id='$department_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
			
	ob_start();	
		
	?>    
	<table cellpadding="0" cellspacing="1" border="1" width="900" align="center"> 
       
    <?	
	
		//$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 and a.attnd_date>='$fromdate' and a.attnd_date<='$todate' and a.emp_code=emp.emp_code $category $cbo_company_id $location_id $department_id $section_id $subsection_id order by emp.section_id, emp.designation_level ASC";
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 and a.attnd_date like '$date_part%' and is_manually_updated=1 and a.emp_code=emp.emp_code $cbo_company_id $category $department_id $location_id  $division_id $section_id $subsection_id $designation_id $category order by emp.section_id, emp.designation_level,RIGHT(emp.id_card_no,4) ASC";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_department=array();
		while($rows=mysql_fetch_array($result)){
			if (in_array($rows['department_id'], $new_department))
					{
						$i++;
					}
				else
					{
						?>
                            <tr><td colspan="10" align="center"><font size="+1"><? echo $company_details[$rows['company_id']]['company_name']; ?></font></td></tr>
                            <tr><td colspan="10" align="center">&nbsp;</td></tr>													  
                            <tr>
                                <td colspan="10" align="center">Employee Manual Adjustment List</td>
                            </tr>
                            <tr>
                                <td colspan="10" >Department : <? echo $department_details[$rows['department_id']]['department_name']; ?> </center></td>
                            </tr>
                            <tr>
                            <td width="40"><b>SL</b></td><td width="70"><b>ID Card</b></td><td width="70"><b>Emp Code</b></td><td align="130"><b>Name</b></td><td align="100"><b>Designation</b></td><td align="80"><b>Date</b></td><td align="80"><b>In Time</b></td><td align="80"><b>Out Time</b></td><td align="80"><b>Status</b></td><td><b>Remarks</b></td>  
                            </tr>
						<?
						
						$new_department[$department_id]=$rows[department_id];
						$i=1;
					}
	?>    
    		<tr>
            	<td><? echo $i; ?></td>
            	<td>&nbsp;<? echo $rows['id_card_no']; ?></td>
                <td>&nbsp;<? echo $rows['emp_code']; ?></td>
                <td><? echo $rows['name']; ?></td>
                <td>&nbsp;<? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td>             
              	<td><? echo $rows['attnd_date'];?></td> 	 	
               	<td><? echo $rows['sign_in_time'];?></td>
                <td><? echo $rows['sign_out_time'];?></td>
               	<td>&nbsp;<? echo $rows['status'];?></td>
              	<td>&nbsp;<? echo $rows['remarks'];?></td>              	
            </tr>
    
		<?
            }
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}


if($action=='ques_attn_report'){
	
	//$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and emp.company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and emp.department_id='$department_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";

	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
			
	ob_start();	
		
	?> 
   <div>
	<table cellpadding="0" cellspacing="1" border="1" width="900" align="center" rules="all" class="rpt_table">
    <?	
	
		
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* 
				FROM hrm_attendance a, hrm_employee emp 
				WHERE 
				emp.status_active=1 and 
				a.attnd_date between '$txt_from_date' and '$txt_to_date' and 
				is_questionable=1 and a.emp_code=emp.emp_code  
				$category  $cbo_company_id $department_id $location_id $division_id $section_id $subsection_id $designation_id $category order by emp.department_id, emp.designation_level,RIGHT(emp.id_card_no, 4) ASC";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_department=array();
		while($rows=mysql_fetch_array($result)){
			if (in_array($rows['department_id'], $new_department))
					{
						$i++;
					}
				else
					{
						?>
                      
                            <tr bgcolor="#F0F0B8"><td colspan="10" align="center"><font size="+1"><? echo $company_details[$rows['company_id']]['company_name']; ?></font></td></tr>
                            <tr><td colspan="10" align="center">&nbsp;</td></tr>													  
                            <tr>
                                <td colspan="10" align="center">Employee's Questionable Attendance List</td>
                            </tr>
                            <tr>
                     <td colspan="10">Department : <? echo $department_details[$rows['department_id']]['department_name']; ?></td>
                            </tr>
                            <tr>
                            <td width="40"><b>SL</b></td><td width="70"><b>ID Card</b></td><td width="70"><b>Emp Code</b></td><td align="130"><b>Name</b></td><td align="100"><b>Designation</b></td><td align="80"><b>Date</b></td><td align="80"><b>In Time</b></td><td align="80"><b>Out Time</b></td><td align="80"><b>Status</b></td><td><b>Remarks</b></td>  
                            </tr>
						<?
						
						$new_department[$department_id]=$rows[department_id];
						$i=1;
					}
	?>    
    		<tr>
            	<td><? echo $i; ?></td>
            	<td>&nbsp;<? echo $rows['id_card_no']; ?></td>
                <td>&nbsp;<? echo $rows['emp_code']; ?></td>
                <td><? echo $rows['name']; ?></td>
                <td>&nbsp;<? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td>             
              	<td><? echo convert_to_mysql_date($rows['attnd_date']);?></td> 	 	
               	<td><? echo $rows['sign_in_time'];?></td>
                <td><? echo $rows['sign_out_time'];?></td>
               	<td><? echo $rows['status'];?></td>
              	<td>&nbsp;<? echo $rows['remarks'];?></td>              	
            </tr>
    
		<?
            }
        ?>
    </table>
    </div>	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}


if($action=='absent_late_trent_report'){
			
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id='$department_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	if ($cbo_type_selector==1) $status="A"; else $status="D";
			
	ob_start();	
		
	?>    
	<table cellpadding="0" cellspacing="1" border="1" width="1200" align="center" style="border:1px solid #000; font-size:10px" rules="all">       
    <?	
	
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee WHERE status_active=1 $cbo_company_id $category $department_id $location_id  $division_id $section_id $subsection_id $designation_id order by department_id, designation_level ASC";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_department=array();
		while($rows=mysql_fetch_array($result)){
			
			$doj=explode("-",$rows['joining_date']);
			
			$attn_sql = "SELECT 
						count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-01%' THEN status END) as jan,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-02%' THEN status END) as fab,
						count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-03%' THEN status END) as mar,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-03%' THEN status END) as apr,
						count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-05%' THEN status END) as may,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-06%' THEN status END) as jun,
						count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-07%' THEN status END) as jul,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-08%' THEN status END) as aug,
						count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-09%' THEN status END) as sep,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-10%' THEN status END) as oct,
						count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-11%' THEN status END) as nov,count(CASE WHEN attnd_date LIKE '%$cbo_year_selector-12%' THEN status END) as decm
						FROM hrm_attendance
						WHERE 
						emp_code =$rows[emp_code] and status='$status'
						group by status";	
				$results = mysql_query( $attn_sql ) or die( $attn_sql . "<br />" . mysql_error() );				
				
				while($row=mysql_fetch_array($results)){
			 
					if (in_array($rows['department_id'], $new_department))
						{
							$i++;
						}
					else
						{
							?>
								<tr><td colspan="22" align="center"><font size="+1"><? echo $company_details[$rows['company_id']]['company_name']; ?></font><br />
                                		Employee's Yearly <? if($status=='A') echo "Absent"; else echo "Late"; ?> Trent</td></tr>
								<tr>
									<td colspan="22" >
                                    	Department : <? echo $department_details[$rows['department_id']]['department_name']; ?>                                       
                                    </td>
								</tr>
								<tr>
								<td width="20"><b>SL</b></td><td width="50"><b>ID Card</b></td><td width="50"><b>Emp Code</b></td><td width="130"><b>Name</b></td><td width="100"><b>Designation</b></td><td width="70"><b>Category</b></td><td width="70"><b>Section</b></td><td width="70"><b>Sub Section</b></td><td width="70"><b>DOJ</b></td>
								<td width="40"><b>JAN</b></td><td width="40"><b>FEB</b></td><td width="40"><b>MAR</b></td><td width="40"><b>APR</b></td><td width="40"><b>MAY</b></td><td width="40"><b>JUN</b></td><td width="40"><b>JUL</b></td><td width="40"><b>AUG</b></td><td width="40"><b>SEP</b></td><td width="40"><b>OCT</b></td><td width="40"><b>NOV</b></td><td width="40"><b>DEC</b></td><td><b>Remarks</b></td>  
								</tr>
							<?
							
							$new_department[$department_id]=$rows[department_id];
							$i=1;
						}
				
				
			?>    
				<tr>
					<td><? echo $i; ?></td>
					<td>&nbsp;<? echo $rows['id_card_no']; ?></td>
					<td>&nbsp;<? echo $rows['emp_code']; ?></td>
					<td><? echo $rows['name']; ?></td>
					<td>&nbsp;<? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td> 
                    <td>&nbsp;<? echo $employee_category[$rows['category']]; ?></td>
                    <td>&nbsp;<? echo $section_details[$rows['section_id']]['section_name']; ?></td> 
                    <td>&nbsp;<? echo $subsection_details[$rows['subsection_id']]['subsection_name']; ?></td> 
					<td>&nbsp;<? echo $doj[2]."-".$doj[1]."-".$doj[0];?></td>          	
					<td align="right"><? echo $row['jan'];?></td> 	 	
					<td align="right"><? echo $row['fab'];?></td>
					<td align="right"><? echo $row['mar'];?></td>
					<td align="right"><? echo $row['apr'];?></td>
					<td align="right"><? echo $row['may'];?></td>
					<td align="right"><? echo $row['jun'];?></td>
					<td align="right"><? echo $row['jul'];?></td>
					<td align="right"><? echo $row['aug'];?></td>
					<td align="right"><? echo $row['sep'];?></td>
					<td align="right"><? echo $row['oct'];?></td>
					<td align="right"><? echo $row['nov'];?></td>
					<td align="right"><? echo $row['decm'];?></td>     
					<td>&nbsp;</td>              	
				</tr>  
			<?	
			}
		
		}
    ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}






if($action=='monthly_check_sheet'){
			
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$tot_days = cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$txt_from_date = $cbo_year_selector . "-" . $cbo_month_selector . "-" . "01";
	$txt_to_date = $cbo_year_selector . "-" . $cbo_month_selector . "-" . $tot_days;
	//$tot_days=daysdiff($txt_from_date, $txt_to_date);
	
	//payroll head 
	$salary_head_list = array();	
	$sql_sal="select * from lib_payroll_head where id in (5,6,7,35) and status_active=1 and is_deleted=0 order by id";
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);	
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		  $salary_head_list[$row_sal[id]]=$row_sal[is_applicable];
	}
	
	//variable settings for ot
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $hrmrow = mysql_fetch_assoc( $result ) ) {
 		$var_hrm_chart[$hrmrow['company_name']] = array();
		$var_hrm_chart[$hrmrow['company_name']]['adjust_in_time'] = mysql_real_escape_string( $hrmrow['adjust_in_time'] );
		$var_hrm_chart[$hrmrow['company_name']]['adjust_out_time'] = mysql_real_escape_string( $hrmrow['adjust_out_time'] );
		$var_hrm_chart[$hrmrow['company_name']]['first_ot_limit'] = mysql_real_escape_string( $hrmrow['first_ot_limit'] );
		$var_hrm_chart[$hrmrow['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $hrmrow['one_hour_ot_unit'] );
		$var_hrm_chart[$hrmrow['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $hrmrow['allow_ot_fraction'] );
		$var_hrm_chart[$hrmrow['company_name']]['ot_start_minute'] = mysql_real_escape_string( $hrmrow['ot_start_minute'] );
	}
	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id='$department_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
			
	ob_start();	
		
	?>    
	<table cellpadding="0" cellspacing="0" border="1" width="900" align="center" style="border:1px solid #000; font-size:10px" rules="all">       
    <?	
	
		$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee WHERE emp_code!='' $cbo_company_id $category $department_id $location_id $section_id $subsection_id $designation_id order by section_id,id_card_no ASC";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$i=0;$new_department=array();$new_section=array();
		while($rows=mysql_fetch_array($result)){			
			
			
			$emp_condition=0; // Regular Worker
			if ($rows[status_active]==0) // In active Employee
			{
				$d="separated_from";$dd="hrm_separation";$ddd="emp_code='$rows[emp_code]' order by id desc";
				$old_mod= return_field_value($d,$dd,$ddd);
				
				if (strtotime($old_mod)>=strtotime($txt_from_date) && strtotime($old_mod)<strtotime($txt_to_date))
				{
					$emp_condition=1; // Resign Worker
				}
				else $emp_condition=3; // not  Worker
			}
			
		   if ($rows[status_active]==1)
			{
				if (strtotime($rows[joining_date])>=strtotime($txt_from_date) && strtotime($rows[joining_date])<=strtotime($txt_to_date))
				{
					$emp_condition=2; // New Worker
				}
				else if (strtotime($rows[joining_date])>strtotime($txt_to_date) ) $emp_condition=3; // not Worker
			}
			
				$attn_sql = "SELECT 
							count(CASE WHEN status in ('CL','ML','SL','EL','SpL','LWP') THEN status END) as tl,
							count(CASE WHEN status in ('H','FH','CH','GH','W') THEN status END) as th,
							count(CASE WHEN status in ('P') THEN status END) as tp,
							count(CASE WHEN status in ('A') THEN status END) as ta,
							count(CASE WHEN status in ('D') THEN status END) as td
							FROM hrm_attendance
							WHERE 
							emp_code='$rows[emp_code]' and attnd_date LIKE '%$date_part%'";
						
				$results = mysql_query( $attn_sql ) or die( $attn_sql . "<br />" . mysql_error() );				
				//echo $emp_condition."##".$rows[emp_code]."-"; 
				if($emp_condition<>3){
					$row=mysql_fetch_array($results);
					
					if ($emp_condition==2){ // New
						$total_payable_days=datediff('d',$rows[joining_date],$txt_to_date);	//New Join 
						$status='N';
					}
					else if ($emp_condition==1){ // Resign	
						$total_payable_days=datediff('d',$txt_from_date,$old_mod);	// Resign
						$status='D';
					}
					else{
						$total_payable_days=$tot_days;	//Regular
						$status='R';
					}
					$calendar_days=$tot_days;
					$not_payable_days=$calendar_days-$total_payable_days;					
					
					
					//ot and eot and further ot here
					$buyer_ot=0;
					$extra_ot=0;
					$fur_extra_ot=0;
					$basic_salary=return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$rows[emp_code]");
					$ot_rate=$basic_salary/104;
					$emp_salary_sheet=0;$buyer_ot=0;
					if($salary_head_list[5]==1 ) // Over Time
					{
						
						$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";						
						$sql_data_exe=mysql_query($sql_data);
						$old_hr=0;
						$old_slot=0;
						$ot_slot=0;
						$u=0;
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							$u++;
							$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
							$ot_multiplier=$sql_data_rslt[overtime_multiplier];
							
							$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".$txt_from_date."' and '".$txt_to_date."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
							$total_ot_hr=$total_ot_hr-$old_hr;
							$old_hr= $old_hr+$total_ot_hr; 
							
							if($sql_data_rslt[overtime_head]==5)
							{
								$emp_salary_sheet= $emp_salary_sheet+($ot_rate*$ot_multiplier*$total_ot_hr);  
								$buyer_ot = $buyer_ot+$total_ot_hr;								
							}
						}						
						$ot_slot=0;
					}
					
					if($salary_head_list[6]==1 ) // Extra Over Time
					{
						$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";
						$sql_data_exe=mysql_query($sql_data);
						$old_hr=0;
						$old_slot=0;
						$ot_slot=0;
						$u=0;
						
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							$u++;
							$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
							$ot_multiplier=$sql_data_rslt[overtime_multiplier];
							
							$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".$txt_from_date."' and '".$txt_to_date."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
							$total_ot_hr=$total_ot_hr-$old_hr;
							$old_hr= $old_hr+$total_ot_hr; 
		 
							if($sql_data_rslt[overtime_head]==6)
							{
								$emp_salary_sheet= $emp_salary_sheet+($ot_rate*$ot_multiplier*$total_ot_hr);  
								$extra_ot= $extra_ot+$total_ot_hr; 
							}
						}
						
					}
					if( $salary_head_list[7]==1) // Further Extra Over Time
					{
						$sql_data="select * from  lib_policy_overtime_slab where policy_id=$rows[overtime_policy] order by id asc";
						$sql_data_exe=mysql_query($sql_data);
						$old_hr=0;
						$old_slot=0;
						$ot_slot=0;
						$u=0;
						while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
						{
							$u++;
							$ot_slot=$ot_slot+$sql_data_rslt[overtime_slot]*60; 
							$ot_multiplier=$sql_data_rslt[overtime_multiplier];
							
							$total_ot_hr= get_buyer_tot_emp_ot_hr($rows[emp_code]," attnd_date between '".$txt_from_date."' and '".$txt_to_date."'",$var_hrm_chart[$rows['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$rows['company_id']]['allow_ot_fraction'],$var_hrm_chart[$rows['company_id']]['ot_start_minute'],$ot_slot);
							$total_ot_hr=$total_ot_hr-$old_hr;
							$old_hr= $old_hr+$total_ot_hr; 
							
							if($sql_data_rslt[overtime_head]==7)
							{
								$emp_salary_sheet = $emp_salary_sheet +($ot_rate*$ot_multiplier*$total_ot_hr);  
								$fur_extra_ot= $fur_extra_ot+$total_ot_hr; 
							}
						} 
					}
					
							
					$att_bonus_amount=0;			
					if($salary_head_list[35]==1)  // Attendance Bonus  Rules
						{
							if( $emp_condition==0)// Regular Worker
							{
								$sql_data="select * from  lib_policy_attendance_bonus_definition where policy_id=$rows[attendance_bonus_policy] order by id asc";
								$sql_data_exe=mysql_query($sql_data); 
								while($sql_data_rslt=mysql_fetch_array($sql_data_exe))
								{
									if($sql_data_rslt[leave_criteria]>= $total_leave_days && $sql_data_rslt[late_criteria]>=$late_days  &&   $sql_data_rslt[absent_criteria]>=$absent_days )
									{
										$att_bonus_amount=round($sql_data_rslt[amount]); break;
									}
								}							
														
							}
							
						}		
 					
					if (in_array($rows['department_id'], $new_department))
					{
							if (in_array($rows['section_id'], $new_section))
								{
									$i++;
								}
							else
								{				
									
							?>                            
									<tr><td colspan="16" align="center"><font size="+1"><? echo $company_details[$rows['company_id']]['company_name']; ?></font><br />
                                		Address: Plot No # <? echo $company_details[$rows['company_id']]["plot_no"]; ?>, Level No # <? echo $company_details[$rows['company_id']]["level_no"]; ?>, Block No # <? echo $company_details[$rows['company_id']]["block_no"]; ?>, Road No # <? echo $company_details[$rows['company_id']]["road_no"]; ?>, <? echo $company_details[$rows['company_id']]["city"]; echo $company_details[$rows['company_id']]["country_name"];?><br />
                                     </td></tr>
							<? }
					}
					else
						{
												
                            $new_department[$rows[department_id]]=trim($rows[department_id]);
							$new_section=array();
							$new_section[$rows[section_id]]=trim($rows[section_id]);
							 
                               ?><tr>
									<td colspan="7" >
                                    	Section : <? echo $section_details[$rows['section_id']]['section_name']; ?> 
                                    </td>
                                    <td colspan="9" >
                                        Department : <? echo $department_details[$rows['department_id']]['department_name']; ?>                                       
                                    </td>
								</tr>
								<tr align="center">
								<td width="20"><b>SL</b></td><td width="50"><b>ID Card</b></td><td width="50"><b>Emp Code</b></td><td width="130"><b>Name</b></td><td width="100"><b>Designation</b></td><td width="70"><b>DOJ</b></td><td width="70"><b>F/L</b></td><td width="50"><b>T.L</b></td><td width="50"><b>T.H</b></td>
								<td width="40"><b>P.Day</b></td><td width="40"><b>A.Day</b></td><td width="50"><b>NP Day</b></td><td width="40"><b>OT</b></td><td width="40"><b>EOT</b></td><td width="40"><b>AB</b></td><td width="40"><b>ST</b></td>  
								</tr>
							<?
							
							$new_section[$i]=trim($rows[section_id]);
							$i=1;
						}
				
				
			?>    
				<tr>
					<td><? echo $i; ?></td>
					<td><? echo $rows['id_card_no']; ?></td>
					<td><? echo $rows['emp_code']; ?></td>
					<td><? echo $rows['name']; ?></td>
					<td><? echo $designation_chart[$rows['designation_id']]['custom_designation']; ?></td> 
                    <td><? echo convert_to_mysql_date($rows[joining_date]);?></td> 
                    <td><? echo $subsection_details[$rows['subsection_id']]['subsection_name']; ?></td>          	
					<td align="right"><? echo $row['tl'];?></td> 	 	
					<td align="right"><? echo $row['th'];?></td>
					<td align="right"><? echo $row['th']+$row['tp']+$row['td'];?></td>
					<td align="right"><? echo $row['ta'];?></td>
					<td align="right"><? echo $not_payable_days;?></td>
					<td align="right"><? echo $buyer_ot;?></td>
					<td align="right"><? echo $extra_ot+$fur_extra_ot;?></td>
					<td align="right"><? echo $att_bonus_amount;?></td>
					<td align="right"><? echo $status;?></td>					            	
				</tr>  
			<?	
			}
		
		}
    ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}





//month generated
if($type=="select_month_generate")
	{		
		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 and b.is_locked=0";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>

