<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");

//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}

extract($_GET);

if($type=="ot_based_on_report_list")
{
	$txt_from_date=convert_to_mysql_date($txt_from_date);
	$txt_to_date=convert_to_mysql_date($txt_to_date);
	
		if ($category=='') $category=" and emp.category in (0,1,2) "; else $category=" and emp.category='$category'";
		if ($company_id=='') $company_id =""; else $company_id ="and a.company_id='$company_id'";
		if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
		if ($division_id==0) $division_id=""; else $division_id="and a.division_id in ($division_id)";
		if ($department_id==0) $department_id=""; else $department_id="and a.department_id in ($department_id)";
		if ($section_id==0) $section_id=""; else $section_id="and a.section_id  in ($section_id)";
		if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
		if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id  in ($designation_id) ";
		if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";
		
		//new add group by order by
		$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
		
		if($group_by_id==0){$groupby="emp.emp_code,";}
		else if($group_by_id==1){$status_com=1;$groupby="a.company_id,a.emp_code,";}
		else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,a.emp_code,";}
		else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,a.emp_code,";}
		else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.emp_code,";}
		else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.emp_code,";}
		else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,a.emp_code,";}
		
		//echo $order_by_id; die;	
		//if($order_by_id==0){$orderby="order by ".substr($groupby,0,-9)." CAST(emp.id_card_no as SIGNED)";}	 
		else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-9)." CAST(emp.designation_level as SIGNED)";}	 
		else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-9)." CAST(a.emp_code as SIGNED)";}	 
		else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-9)." CAST(right(trim(emp.id_card_no), 5)as SIGNED)";}	 
		//else if($order_by_id==3){$orderby="order by  CAST(right(c.id_card_no, 5)as SIGNED)";}
		
		$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by
		$location_arr=array();
		$division_arr=array();
		$department_arr=array();
		$section_arr=array();
		$subsection_arr=array();
		$sl=0;
		
		$sql_shift="SELECT ot_date,sum(budgeted_ot) as budgeted_ot,emp_code FROM ot_requisition_dtl group by emp_code, ot_date";
				$result_shift=mysql_query($sql_shift) or die( $sql_shift . "<br />" . mysql_error() );
				$ot_requisition_array=array();
				while($emp_shift=mysql_fetch_assoc($result_shift))
				{
					$ot_requisition_array[$emp_shift[emp_code]][$emp_shift[ot_date]]=$emp_shift[budgeted_ot];
				}
				
		echo $sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.*,a.department_id,a.section_id,a.subsection_id,a.sign_in_time,a.sign_out_time,a.r_sign_in_time,a.r_sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min,a.policy_shift_id as att_policy_shift_id,a.req_out_time_org,a.req_ot_org,a.r_out_time_buffer,a.is_next_day,c.budgeted_ot,c.ot_date FROM hrm_attendance a, hrm_employee emp,ot_requisition_dtl c 
		WHERE 
		a.emp_code=emp.emp_code and 
		a.emp_code=c.emp_code and
		emp.is_deleted=0 and 
		emp.status_active=1 and 
		a.attnd_date between '$txt_from_date' and '$txt_to_date'
		$category
		$company_id 
		$location_id 
		$division_id 
		$department_id 
		$section_id 
		$subsection_id
		$designation_id
		$emp_code
		group by $dynamic_groupby $orderby";
				
		
		/*echo $sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no,emp.*,a.department_id,a.section_id,a.subsection_id,a.sign_in_time,a.sign_out_time,a.r_sign_in_time,a.r_sign_out_time,a.attnd_date,a.status,a.late_time_min,a.total_over_time_min,a.policy_shift_id as att_policy_shift_id,a.req_out_time_org,a.req_ot_org,a.r_out_time_buffer FROM hrm_attendance a, hrm_employee emp 
		WHERE 
		a.emp_code=emp.emp_code and  
		emp.is_deleted=0 and 
		emp.status_active=1 and 
		a.attnd_date between '$txt_from_date' and '$txt_to_date'
		$category
		$company_id 
		$location_id 
		$division_id 
		$department_id 
		$section_id 
		$subsection_id
		$designation_id
		$emp_code
		group by $dynamic_groupby $orderby";*/
		
		
		$result=mysql_query($sql) or die( $sql . "<br />" . mysql_error() );
		ob_start();
		
		if(mysql_num_rows($result)==0)
		{
			echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
		}
		else
		{?>
        <div style="width:1000px;" id="scroll_body_topheader" align="left">
        
        </div>
        <br />
        <div style="width:1400px;" id="scroll_body" align="left">
        <div class="form_caption">OT Details</div>
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="table_body_head">
            <thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="150" align="center"><strong>Employee Name</strong></th>
                <th width="80" align="center"><strong>Designation</strong></th>
                <th width="80" align="center"><strong>Emp Code</strong></th>
                <th width="80" align="center"><strong>Emp ID</strong></th>
                <th width="80" align="center"><strong>Atten.Date</strong></th>
                <th width="100" align="center"><strong>Requisition O.T</strong></th>
                <th width="80" align="center"><strong>Requisition out time</strong></th>
                <th width="80" align="center"><strong>Actual Out Time</strong></th>
                <th width="100" align="center"><strong>OT As Reqn.</strong></th>
                <th width="100" align="center"><strong>Over the Reqn. (List)</strong></th>
                <th width="100" align="center"><strong>Less than Reqn (List)</strong></th>
                <th width="80" align="center"><strong>No OT from list</strong></th>
                <th width="80" align="center"><strong>OT Without Reqn</strong></th>
                <th width="80" align="center"><strong>In Time</strong></th>
                <th width="80" align="center"><strong>Out Date</strong></th>
	   		</thead>
       </table>
    </div>
    <div style="width:1400px;  height:280px" id="scroll_body" align="left">
        <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="" >	 
        <?php                  
			$i=1;
			
			$ot_as_per_emp=0;
			$over_reqn_emp=0;
			$less_reqn_emp=0;
			$no_ot_emp=0;
			$wethout_reqn_emp=0;
			
			while($emp=mysql_fetch_assoc($result))
			{
		    $sl++;
	        //header print here 
			
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($emp[division_id],$division_arr) && $status_divis==1)

							{
									if(in_array($emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$emp[section_id]]=$emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$emp[department_id]]=$emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$emp[section_id]]=$emp[section_id];
										$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$emp[division_id]]=$emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$emp[department_id]]=$emp[department_id];
									$section_arr[$emp[section_id]]=$emp[section_id];
									$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$emp[location_id]]=$emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$emp[division_id]]=$emp[division_id];
						$department_arr[$emp[department_id]]=$emp[department_id];
						$section_arr[$emp[section_id]]=$emp[section_id];
						$subsection_arr[$emp[subsection_id]]=$emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
		}
		
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?>
               <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong> Subsection Total</strong></td>
                <td>&nbsp;<?php echo $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php 
				if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot == $emp['req_ot_org'] )  echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; else echo ""; 	
					}
					else echo "0";
					//echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; ?></td>
                <td>&nbsp;<?php echo $xhour_sum_total." Hrs. ".$xmin_sum_minit." Mins";  ?></td>
                <td>&nbsp;<?php if($emp_budgeted_ot>0)
                {
                    if( $emp_budgeted_ot > $emp['total_over_time_min'] ) echo $ahour_sum." Hrs. ".$amin_sum." Mins"; else echo "0"; 
                }else echo "0";    ?></td>
                
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
                {
                   echo $xmin_sum_minit." Hrs. ".$xmin_sum_minit." Mins";
                } ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php ?></td>
          </tr>					
                           
			<?
		
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?>
               <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong>Section Total</strong></td>
                <td>&nbsp;<?php echo $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php 
				if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot == $emp['req_ot_org'] )  echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; else echo ""; 	
					}
					else echo "0";
					//echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; ?></td>
                <td>&nbsp;<?php echo $xhour_sum_total." Hrs. ".$xmin_sum_minit." Mins";  ?></td>
                <td>&nbsp;<?php if($emp_budgeted_ot>0)
                {
                    if( $emp_budgeted_ot > $emp['total_over_time_min'] ) echo $ahour_sum." Hrs. ".$amin_sum." Mins"; else echo "0"; 
                }else echo "0";    ?></td>
                
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
                {
                   echo $xmin_sum_minit." Hrs. ".$xmin_sum_minit." Mins";
                } ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php ?></td>
          </tr>					
                           
			<?
		
		}
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?>
            <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong> Department Total</strong></td>
                <td>&nbsp;<?php echo $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php 
				if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot == $emp['req_ot_org'] )  echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; else echo ""; 	
					}
					else echo "0";
					//echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; ?></td>
                <td>&nbsp;<?php echo $xhour_sum_total." Hrs. ".$xmin_sum_minit." Mins";  ?></td>
                <td>&nbsp;<?php if($emp_budgeted_ot>0)
                {
                    if( $emp_budgeted_ot > $emp['total_over_time_min'] ) echo $ahour_sum." Hrs. ".$amin_sum." Mins"; else echo "0"; 
                }else echo "0";    ?></td>
                
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
                {
                   echo $xmin_sum_minit." Hrs. ".$xmin_sum_minit." Mins";
                } ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php ?></td>
          </tr>	
			<?
		}
		
		
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?>
            <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong>Division Total</strong></td>
                <td>&nbsp;<?php echo $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php 
				if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot == $emp['req_ot_org'] )  echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; else echo ""; 	
					}
					else echo "0";
					//echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; ?></td>
                <td>&nbsp;<?php echo $xhour_sum_total." Hrs. ".$xmin_sum_minit." Mins";  ?></td>
                <td>&nbsp;<?php if($emp_budgeted_ot>0)
                {
                    if( $emp_budgeted_ot > $emp['total_over_time_min'] ) echo $ahour_sum." Hrs. ".$amin_sum." Mins"; else echo "0"; 
                }else echo "0";    ?></td>
                
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
                {
                   echo $xmin_sum_minit." Hrs. ".$xmin_sum_minit." Mins";
                } ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php ?></td>
          </tr>	
			<?
				
		}
		
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?>
           <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong>Location Total</strong></td>
                <td>&nbsp;<?php echo $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php 
				if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot == $emp['req_ot_org'] )  echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; else echo ""; 	
					}
					else echo "0";
					//echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; ?></td>
                <td>&nbsp;<?php echo $xhour_sum_total." Hrs. ".$xmin_sum_minit." Mins";  ?></td>
                <td>&nbsp;<?php if($emp_budgeted_ot>0)
                {
                    if( $emp_budgeted_ot > $emp['total_over_time_min'] ) echo $ahour_sum." Hrs. ".$amin_sum." Mins"; else echo "0"; 
                }else echo "0";    ?></td>
                
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
                {
                   echo $xmin_sum_minit." Hrs. ".$xmin_sum_minit." Mins";
                } ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php ?></td>
          </tr>	
			<?
		}
		
		$c_part_1="";
		if( $new_loc==1 || $new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$emp[subsection_id]]."  ,";
			}
	

			if($c_part_1!='')
			{
				//$i=0;
				$sl=1;
					?><tr>
               			 <td colspan="22"><b><? echo substr($c_part_1,0,-1); ?></b></td>
                	 </tr>
               </table>       

				<?
			}		
	  ?>                 
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="table_body" >	 
	  <?		 
	  }//end if
		
				//echo $emp[attnd_date]; print_r($ot_requisition_array[$emp[emp_code]][$emp[attnd_date]]);
				$emp_budgeted_ot=$ot_requisition_array[$emp[emp_code]][$emp[attnd_date]];
				
				if ($i%2==0)  
				$bgcolor="#E9F3FF";
				else
				$bgcolor="#FFFFFF";
				
				$bhour=floor($emp_budgeted_ot/60);
				$bmin=$emp_budgeted_ot%60;
				
				$ahour=floor($emp['total_over_time_min']/60);
				$amin=$emp['total_over_time_min']%60;
				
				$xhour=floor($emp['req_ot_org']/60);
				$xmin=$emp['req_ot_org']%60;
				
				
				
				
				
				
				
				?>
                <tr bgcolor="<? echo $bgcolor; ?>">
				<td width="25" align="left"><?php echo $i;?></td>
                <td width="150" align="left">&nbsp;<?php echo $emp['name'] ; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $designation_chart[$emp['designation_id']]; ?></td>
				<td width="80" align="center">&nbsp;<?php echo $emp['emp_code'] ; ?></td>
                <td width="80" align="center">&nbsp;<?php echo $emp['id_card_no'];?></td>
				<td width="80" align="center">&nbsp;<?php echo convert_to_mysql_date($emp['attnd_date']); ?></td>
				<td width="100" align="center"><?php echo $bhour.' Hrs. '.$bmin." Mins"; ?></td>
                <td width="80" align="center" title="<? echo $emp['r_sign_out_time']; ?>"><?php echo add_time($emp['r_sign_out_time'],$emp_budgeted_ot); //$hour_req.':'.$min_req; ?></td>
                <td width="80" align="center"><?php echo $emp['sign_out_time'];  ?></td>
                <td width="100" align="center">
				<?php 
					$as_per_emp_amount=0;
					if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot == $emp['req_ot_org'] )
						 { 
						 	$bhour_sum +=$bhour;
							$bmin_sum +=$bmin;
							$bmin_sum_houre=floor($bmin_sum/60);
							$bhour_sum_total=$bhour_sum+$bmin_sum_houre;
							$bmin_sum_minit=$bmin_sum%60;
							
							$ot_as_per_emp++; 
							echo $emp_budgeted_ot."mm  ".$bhour." Hrs. ".$bmin." Mins";
							$as_per_man_emp_code=$emp['emp_code'] ;
						  
						  	$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$as_per_man_emp_code'";
							$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
							while($emp_salary=mysql_fetch_assoc($result_salary))
							{
								$as_per_basic_sal=$emp_salary['basic_salary'];
							}
							//echo $basic_sal;
							$as_per_emp_amount=($as_per_basic_sal/(208*60))*$emp_budgeted_ot;
							$as_per_emp_amount_total +=$as_per_emp_amount;
						 } 
						 else echo ""; 	
					}
					else echo ""; 
					
				?></td>
                <td width="100" align="center">
				<?php 
				//OVER=0;
					$emp_amount=0;
					if($emp_budgeted_ot>0)
					{
						
						if( $emp['req_ot_org'] > $emp_budgeted_ot ) 
						{
							$xhour_sum +=$xhour;
							$xmin_sum +=$xmin;
							$xmin_sum_houre=floor($xmin_sum/60);
							$xhour_sum_total=$xhour_sum+$xmin_sum_houre;
							$xmin_sum_minit=$xmin_sum%60;
				
				
							$over_reqn_emp++;
							 echo $xhour." Hrs. ".$xmin." Mins"; //$emp['req_ot_org']."mm  ".
							 $over_man_emp_code=$emp['emp_code'] ;
							 
							 	$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$over_man_emp_code'";
								$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
								while($emp_salary=mysql_fetch_assoc($result_salary))
								{
									$basic_sal=$emp_salary['basic_salary'];
								}
								//echo $basic_sal;
							    $emp_amount=($basic_sal/(208*60))*$emp['req_ot_org'];
								$emp_amount_over_tot +=$emp_amount;
						} 
						else "";
					}
					else "";
					// echo ($emp['req_ot_org'] > $emp['total_over_time_min'] ? $xhour." Hrs. ".$xmin." Mins" : "" );   
					?></td>
                <td width="100" align="center">
				<?php 
				//echo $emp['total_over_time_min'];
				//LESS
					 $emp_amount_less=0;
					if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot > $emp['total_over_time_min'] && $emp['total_over_time_min'] > 0 ) 
						{ 
						//if( $emp['is_next_day']==1 )
						$ahour_sum +=$ahour;
						$amin_sum +=$amin;
						$less_reqn_emp++;
					    echo $ahour." Hrs. ".$amin." Mins";
						
								$less_man_emp_code=$emp['emp_code'] ;
								$sql_salary_less="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$less_man_emp_code'";
								$result_salary_less=mysql_query($sql_salary_less) or die( $sql_salary_less . "<br />" . mysql_error() );
								$basic_sal_less=0;
								while($emp_salary_less=mysql_fetch_assoc($result_salary_less))
								{
									$basic_sal_less=$emp_salary_less['basic_salary'];
								}
								$basic_sal_less;
							    $emp_amount_less=($basic_sal_less/(208*60))*$emp['total_over_time_min'];
								$emp_amount_less_tot +=$emp_amount_less;
						} 
						else ""; 
					}else ""; 
					//echo ($emp_budgeted_ot > $emp['total_over_time_min'] ? $ahour." Hrs. ".$amin." Mins" : "" ); ?></td>
                <td width="80" align="center">
					<?php 
					//NO OT
					//echo $emp_budgeted_ot."=";
					if( $emp_budgeted_ot>0)
					{
						if( $emp['total_over_time_min']==0 )
						{ 
							 $no_ot_emp++; 
							 echo "0"; 
							 
						}
						else "";
					}else ""; 
					?></td>
                <td width="80" align="center">
				<?php 
					if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
					{
						
						if( $emp['total_over_time_min']>0 || $emp['req_ot_org']>0) 
						{ 
						 
						$without_man_emp_code=$emp['emp_code'] ;
						/*$sql_ot_entitled="SELECT id,ot_entitled FROM  hrm_employee_entitlement WHERE emp_code='$without_man_emp_code'";
						$result_ot_entitled=mysql_query($sql_ot_entitled) or die( $sql_ot_entitled . "<br />" . mysql_error() );
						$valit_ot_entitled='';
							while($emp_ot_entitled=mysql_fetch_assoc($result_ot_entitled))
							{
								$valit_ot_entitled=$emp_ot_entitled['ot_entitled'];
							}*/
							
							if($emp['ot_entitled']==1)
							{
								if($emp['r_out_time_buffer']<$emp['req_ot_org'])
								{
									//$without_total_minit=$emp['req_ot_org'];
									$xhour_sum +=$xhour;
									$xmin_sum +=$xmin;
									$xmin_sum_houre=floor($xmin_sum/60);
									$xhour_sum_total_eithout=$xhour_sum+$xmin_sum_houre;
									$xmin_sum_minit_without=$xmin_sum%60;
							
							
									$wethout_reqn_emp++;
									echo $xhour." Hrs. ".$xmin." Mins";
								
								
									$sql_salary_without="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$without_man_emp_code'";
									$result_salary_without=mysql_query($sql_salary_without) or die( $sql_salary_without . "<br />" . mysql_error() );
									$basic_sal_less=0;
									while($emp_salary_without=mysql_fetch_assoc($result_salary_without))
									{
									$basic_sal_without=$emp_salary_without['basic_salary'];
									}
									$basic_sal_without;
									$emp_amount_without=($basic_sal_without/(208*60))*$emp['req_ot_org'];
									$emp_amount_without_tot +=$emp_amount_without;
							 
								}
							}
							 
						
						}
					}
				
				?></td>
                <td width="80" align="center"><?php echo $emp['sign_in_time']; ?></td>
                <td width="80" align="center"><?php if( $emp['is_next_day']==1 ) {echo add_date($emp['attnd_date'],1); } else echo $emp['attnd_date']; ?></td>
                </tr>
				<?
				$i++; 
			 
			}
		?>
         <tr bgcolor="#CCCCCC">
                <td colspan="6" align="center"><strong>Grand Total</strong></td>
                <td>&nbsp;<?php echo $bhour_sum_total." Hrs. ".$bmin_sum_minit." Mins"; ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php 
				if($emp_budgeted_ot>0)
					{
						if( $emp_budgeted_ot == $emp['req_ot_org'] )  echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; else echo ""; 	
					}
					else echo "0";
					//echo $bhour_sum." Hrs. ".$bmin_sum." Mins"; ?></td>
                <td>&nbsp;<?php echo $xhour_sum_total." Hrs. ".$xmin_sum_minit." Mins";  ?></td>
                <td>&nbsp;<?php if($emp_budgeted_ot>0)
                {
                    if( $emp_budgeted_ot > $emp['total_over_time_min'] ) echo $ahour_sum." Hrs. ".$amin_sum." Mins"; else echo "0"; 
                }else echo "0";    ?></td>
                
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php if ($emp_budgeted_ot=="" || $emp_budgeted_ot==0)
                {
                   echo $xhour_sum_total_eithout." Hrs. ".$xmin_sum_minit_without." Mins";
                } ?></td>
                <td>&nbsp;<?php  ?></td>
                <td>&nbsp;<?php ?></td>
          </tr>
        </table>
        
        <div id="to_summery_hidden" style="display:none">
        <div class="form_caption"> OT Analysis</div>
        	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="ot_summery_table">
        	<thead>
                <th width="30" align="center"><strong>SL</strong></th>
                <th width="180" align="center"><strong>Particulars</strong></th>
                <th width="80" align="center"><strong>Man Hour OT</strong></th>
                <th width="80" align="center"><strong>% on Total OT</strong></th>
                <th width="80" align="center"><strong>OT Amount</strong></th>
                <th width="80" align="center"><strong>No.of Employee</strong></th>
                <th width="80" align="center"><strong>% on Total Emp.</strong></th>
                <th width="80" align="center"><strong>Avg. OT Hr. per Head</strong></th>
                <th width="80" align="center"><strong>Avg. OT Amt. per Head</strong></th>
        	</thead>
            <?
            	$view_bhour_sum=0;
				$view_bmin_sum=0;
				if($emp_budgeted_ot>0)
				{
					if( $emp_budgeted_ot == $emp['req_ot_org'] ) $view_bhour_sum=$bhour_sum; $view_bmin_sum=$bmin_sum;
				}
				$ot_per_req_total_minit=($view_bhour_sum*60)+$view_bmin_sum;
				
				$ot_over_req_total_minit=($xhour_sum_total*60)+$xmin_sum_minit;
				$ot_less_req_total_minit=($ahour_sum*60)+$amin_sum;
					
					
				$req_tot_houre=$view_bhour_sum+$xhour_sum_total+$ahour_sum;
				$req_tot_minit=$view_bmin_sum+$xmin_sum_minit+$amin_sum;
				$req_tot_sum_houre=floor($req_tot_minit/60);
				$req_tot_minit_fr=$req_tot_minit%60;
				$req_tot=$req_tot_houre+$req_tot_sum_houre;
				$ot_req_total_minit=($req_tot*60)+$req_tot_minit_fr;
				
				$ot_without_req_total_minit=($xmin_sum_minit*60)+$xmin_sum_minit; 
				
				$total_ho=$req_tot+$xhour_sum_total_eithout; 
				$total_mi=$req_tot_minit_fr+$xmin_sum_minit_without;
				$total_mi_houre=floor($total_mi/60);
				$total_mi_minit=$total_mi%60;
				$total_houre=$total_ho+$total_mi_houre;
				
				$ot_total_minit=($total_houre*60)+$total_mi_minit;
				
				
				$on_total=($ot_per_req_total_minit/$ot_total_minit)*100;
				$on_total_over=($ot_over_req_total_minit/$ot_total_minit)*100;
				$on_total_less=($ot_less_req_total_minit/$ot_total_minit)*100;
				$on_total_requ_ot=($ot_req_total_minit/$ot_total_minit)*100;
				$on_total_wethout=($without_total_minit/$ot_total_minit)*100;
				$on_total_total=($ot_total_minit/$ot_total_minit)*100;
				
				$no_employee=$ot_as_per_emp+$over_reqn_emp+$less_reqn_emp+$no_ot_emp;
				$total_no_employee=$no_employee+$wethout_reqn_emp;
				
				$on_total_emp_as_per_req=($ot_as_per_emp/$total_no_employee)*100;
				$on_total_emp_over=($over_reqn_emp/$total_no_employee)*100;
				$on_total_emp_less=($less_reqn_emp/$total_no_employee)*100;
				$on_total_emp_no_ot=($no_ot_emp/$total_no_employee)*100;
				$on_total_emp_req_tot=($no_employee/$total_no_employee)*100;
				$on_total_emp_wethout=($wethout_reqn_emp/$total_no_employee)*100;
				$on_total_emp_total_no=($total_no_employee/$total_no_employee)*100;
				
				
				$ot_hr_per_head=$ot_per_req_total_minit/$ot_as_per_emp;
				$ot_hr_over=$ot_over_req_total_minit/$over_reqn_emp;
				$ot_hr_less=$ot_less_req_total_minit/$less_reqn_emp;
				$ot_hr_req_tot=$ot_req_total_minit/$no_employee;
				$ot_hr_wethout=$without_total_minit/$wethout_reqn_emp;
				$ot_hr_total=$ot_total_minit/$total_no_employee;
				
				$ot_hr_over_houre=floor($ot_hr_over/60);//=================
				$ot_hr_over_minit=$ot_hr_over%60;
				$ot_hr_less_houre=floor($ot_hr_less/60);//=================
				$ot_hr_less_minit=$ot_hr_less%60;
				$ot_hr_req_tot_houre=floor($ot_hr_req_tot/60);//=================
				$ot_hr_req_tot_minit=$ot_hr_req_tot%60;
				$ot_hr_wethout_houre=floor($ot_hr_wethout/60);//=================
				$ot_hr_wethout_minit=$ot_hr_wethout%60;
				$ot_hr_total_houre=floor($ot_hr_total/60);//=================
				$ot_hr_total_minit=$ot_hr_total%60;
				
				$requisition_total_amount=$as_per_emp_amount_total+$emp_amount_over_tot+$emp_amount_less_tot;
				$total_amount=$requisition_total_amount+$emp_amount_without_tot;
				
				$as_per_avg_ot_amount_per_head=	$as_per_emp_amount_total/$ot_as_per_emp;
				$over_avg_ot_amount_per_head= $emp_amount_over_tot/$over_reqn_emp;
				$less_avg_ot_amount_per_head= $emp_amount_less_tot/$less_reqn_emp;
				$req_tot_avg_ot_amount_per_head= $requisition_total_amount/$no_employee;
				$without_avg_ot_amount_per_head= $emp_amount_without_tot/$wethout_reqn_emp;
				$total_avg_ot_amount_per_head= $total_amount/$total_no_employee;
					
			?>
            <tr bgcolor="#FFFFFF">
				<td align="left">1</td>
                <td align="left">OT as per requisition hour</td>
                <td align="center">&nbsp;<?php echo $view_bhour_sum.":".$view_bmin_sum; //."==".$ot_per_req_total_minit; ?></td>
				<td align="center">&nbsp;<?php  echo round($on_total,2); //$on_total=($ot_per_req_total_minit/$ot_total_minit)*100; ?></td>
                <td align="center">&nbsp;<?php echo round($as_per_emp_amount_total,2); ?></td>
				<td align="center">&nbsp;<?php echo $ot_as_per_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_as_per_req,2); ?></td>
                <td align="center"><?php echo $ot_hr_per_head; ?></td>
                <td align="center"><?php echo round($as_per_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#E9F3FF">
				<td align="left">2</td>
                <td align="left">OT over the requisition hour</td>
                <td align="center">&nbsp;<?php echo $xhour_sum_total.":".$xmin_sum_minit."==".$ot_over_req_total_minit;?></td>
				<td align="center">&nbsp;<?php echo round($on_total_over,2); ?></td>
                <td align="center">&nbsp;<?php echo round($emp_amount_over_tot,2); ?></td>
				<td align="center">&nbsp;<?php echo $over_reqn_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_over,2); ?></td>
                <td align="center"><?php echo $ot_hr_over_houre.":".$ot_hr_over_minit;  ?></td>
                <td align="center"><?php  echo round($over_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#FFFFFF">
				<td align="left">3</td>
                <td align="left">OT less than requisition hour</td>
                <td align="center">&nbsp;<?php echo $ahour_sum.":".$amin_sum."==".$ot_less_req_total_minit; ?></td>
				<td align="center">&nbsp;<?php echo round($on_total_less,2); ?></td>
                <td align="center">&nbsp;<?php echo round($emp_amount_less_tot,2); ?></td>
				<td align="center">&nbsp;<?php echo $less_reqn_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_less,2); ?></td>
                <td align="center"><?php echo $ot_hr_less_houre.":".$ot_hr_less_minit;  ?></td>
                <td align="center"><?php echo round($less_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#E9F3FF">
				<td align="left">4</td>
                <td align="left">No OT from requisition list</td>
                <td align="center">&nbsp;<?php  ?></td>
				<td align="center">&nbsp;<?php  ?></td>
                <td align="center">&nbsp;<?php  ?></td>
				<td align="center">&nbsp;<?php echo $no_ot_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_no_ot,2); ?></td>
                <td align="center"><?php  ?></td>
                <td align="center"><?php  ?></td>
             </tr>
             <tr bgcolor="#FFFFFF">
				<td align="left">5</td>
                <td align="left"><strong>Requisition Total</strong></td>
                <td align="center">&nbsp;<?php echo $req_tot.":".$req_tot_minit_fr;  //."==".$ot_req_total_minit; ?></td>
				<td align="center">&nbsp;<?php echo round($on_total_requ_ot,2); ?></td>
                <td align="center">&nbsp;<?php echo round($requisition_total_amount,2); ?></td>
				<td align="center">&nbsp;<?php echo $no_employee; ?></td>
				<td align="center"><?php echo round($on_total_emp_req_tot,2); ?></td>
                <td align="center"><?php echo $ot_hr_req_tot_houre.":".$ot_hr_req_tot_minit;  ?></td>
                <td align="center"><?php echo round($req_tot_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#E9F3FF">
				<td align="left">6</td>
                <td align="left">OT without requisition</td>
                <td align="center">&nbsp;<?php echo $xhour_sum_total_eithout.":".$xmin_sum_minit_without."==".$without_total_minit; ?></td>
				<td align="center">&nbsp;<?php echo round($on_total_wethout,2); ?></td>
                <td align="center">&nbsp;<?php echo round($emp_amount_without_tot,2); ?></td>
				<td align="center">&nbsp;<?php echo $wethout_reqn_emp; ?></td>
				<td align="center"><?php echo round($on_total_emp_wethout,2); ?></td>
                <td align="center"><?php echo $ot_hr_wethout_houre.":".$ot_hr_wethout_minit; //$ot_hr_wethout; ?></td>
                <td align="center"><?php  echo round($without_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr bgcolor="#FFFFFF">
				<td align="left">7</td>
                <td align="left"><strong>Total</strong></td>
                <td align="center">&nbsp;<?php echo $total_houre.":".$total_mi_minit."==".$ot_total_minit; ?></td>
				<td align="center">&nbsp;<?php echo round($on_total_total,2); ?></td>
                <td align="center">&nbsp;<?php echo round($total_amount,2); ?></td>
				<td align="center">&nbsp;<?php echo $total_no_employee; ?></td>
				<td align="center"><?php echo round($on_total_emp_total_no,2); ?></td>
                <td align="center"><?php echo $ot_hr_total_houre.":".$ot_hr_total_minit; //$ot_hr_total; ?></td>
                <td align="center"><?php  echo round($total_avg_ot_amount_per_head,2); ?></td>
             </tr>
             <tr>
             <td></td>
             <td colspan="8"><strong>Comments:</strong>
             <?
             if($on_total_requ_ot>$on_total_wethout)
			 {echo "OT under requisition is higher than non requisition.";}
			 else echo "Non requisition OT is higher than requisition.";
             ?>
             </td>
             </tr>
        </table>
        </div>
        
        </div>
        <?
		}
		
		$html=ob_get_contents();		
		ob_clean();
		//for report temp file delete 
		foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
				@unlink($filename);
		}		
		//html to xls convert
		$name=time();
		$name="$name".".xls";	
		$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
		$is_created = fwrite($create_new_excel,$html);
		
		echo "$html"."####"."$name"."####".$small_print;		
		exit();
		
}


function add_time($event_time,$event_length)
{
	$timestamp = strtotime("$event_time");
	$etime = strtotime("+$event_length minutes", $timestamp);
	$etime=date('H:i:s', $etime);
	return $etime;

}
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>