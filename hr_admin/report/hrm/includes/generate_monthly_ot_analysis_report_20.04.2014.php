<?

/*######################################

	Completed By
	Name : Ekram
	Date : 16-01-2014
		
######################################*/



session_start();

header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

$new_months = array(01=>'January',02=>'February',03=>'March',04=>'April',05=>'May',06=>'June',07=>'July',08=>'August',09=>'September',10=>'October',11=>'November',12=>'December');

extract ( $_REQUEST );


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}


	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}


	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}
	
	
	$sql = "SELECT * FROM variable_settings_report WHERE is_deleted = 0 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$variable_settings_report = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$variable_settings_report[$row['company_id']][$row['module_id']] = mysql_real_escape_string($row['report_list']);
	}
	
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$allow_rate=array();
	$designation_chart = array();
	while( $row = mysql_fetch_array( $result ) ) {		
		$designation_chart[$row['id']] =  $row['custom_designation'];
		$designation_chart_rate[$row['id']]['rate'] = ( $row['allowance_rate'] );
		$allow_rate[$row['id']]	= $row['allowance_rate'] ;
	}
	 
	 
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY weekend ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$weekend_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$weekend_chart[$row['emp_code']] = mysql_real_escape_string( $row['weekend'] );		
	}
	

	$sql = "select * from lib_payroll_head where  salary_head=1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_head_mst = array();
	$salary_head_mst_short = array();
	$salary_head_mst_id = array();
	$i=0;
	while( $row = mysql_fetch_assoc( $result ) ) {	
		$i++;	
			$salary_head_mst[$i] = mysql_real_escape_string( $row['custom_head'] );	
			$salary_head_mst_id[$i] = mysql_real_escape_string( $row['id'] );	
			$salary_head_mst_short[$i]=	mysql_real_escape_string( $row['abbreviation'] );	
	}
	

// Variable Settiings
	$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$var_hrm_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		
		$var_hrm_chart[$row['company_name']] = array();
		$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
		$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
		$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
		$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
		$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
		$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
		$var_hrm_chart[$row['company_name']]['not_pay_deduction'] = mysql_real_escape_string( $row['not_pay_deduction'] );
		$var_hrm_chart[$row['company_name']]['new_emp_card_charge'] = mysql_real_escape_string( $row['new_emp_card_charge'] );
		$var_hrm_chart[$row['company_name']]['ot_rate_multiplier'] = mysql_real_escape_string( $row['ot_rate_multiplier'] );
		$var_hrm_chart[$row['company_name']]['att_bonus_leave_type'] = mysql_real_escape_string( $row['att_bonus_leave_type'] ); 	 	 	
	}


	



if($action=="monthly_ot_analysis") 	
{	
	
	
	$from_month_selector = explode('_', $from_month_selector ); 
	$to_month_selector = explode('_', $to_month_selector );
	
	 
	$from_month = $from_month_selector[0];
	$yrdata= strtotime($from_month);
   	$header_from_month=date('M-Y', $yrdata);
	
	$to_month = $to_month_selector[0];
	$yrdatato= strtotime($to_month);
   	$header_to_month=date('M-Y', $yrdatato);

	$from_con_date=date('Y-m-d',strtotime($from_month));
	$to_con_date=date('Y-m',strtotime($to_month));
	
	$mot_to_get_days=explode("-",$to_con_date);
	$no_of_days=cal_days_in_month (CAL_GREGORIAN,$mot_to_get_days[1],$mot_to_get_days[0]);
	$to_con_date.="-".$no_of_days;
	
/*	"select count(distinct a.emp_code) as no_of_emp_code, a.attnd_date from hrm_attendance a,hrm_employee b where a.emp_code=b.emp_code and a.attnd_date between '2012-11-01' and '2012-12-31' and a.company_id='1' and b.ot_entitled<>0 and b.category=2 group by left(trim(a.attnd_date),8)";*/
	//company address
	$company_info_sql="select * from lib_company where id='$company_id'";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$adjust_out_time=$res['adjust_out_time'];
			$applicable_above_salary=$res['applicable_above_salary'];
			$in_out_time_format=$res['in_out_time_format'];
		}
	
	
	
	if ($company_id==0 || $company_id=='') $company_id=""; else $company_id="and a.company_id='$company_id'";
	if ($location_id==0 || $location_id=='') $location_id=""; else $location_id="and a.location_id='$location_id'"; 
	if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0 || $department_id=='') $department_id=""; else $department_id=" and  a.department_id in ($department_id)";   
	if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and a.section_id in ($section_id)"; 
	if ($subsection_id==0 || $subsection_id=='') $subsection_id="";else $subsection_id="and a.subsection_id in ($subsection_id)";
	
	
	
	?>
	
   
	 <table width="1000" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
    
    	<thead>    
            <tr style="text-align:center; font-weight:bold; font-size:24px" height="20">   <!--bgcolor="#CCCCCC" -->	 
                <th colspan="10">
                <div><font size="3"><?	echo $company_info_result["company_name"];?></font></div>
                <br /><font size="2">Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?></font><br />
               <font size="2"> OT Analysis Report For  <? echo $header_from_month; ?> To <? echo $header_to_month; ?></font> 
                </div>
                </th>
            </tr>
           
             <tr style="text-align:center; font-weight:bold;font-size:10px;" height="20" bgcolor="#CCCCCC">
               	<th  width="120">Months</th>
                <th  width="80">OT  Worker</th> 
                <th  width="80">OT Hrs.</th> 
                <th  width="80">Avg.OT/Worker</th> 
                <th  width="80">EOT Workers</th> 
                <th  width="80">EOT Wrk. %</th> 
                <th  width="80">Inactive Wrk.</th> 
                <th  width="80">Turn Over%</th> 
                <th  width="80">Un-authorized leave Wrk.</th> 
                <th  width="80">Absent%</th>
            </tr>
            
        </thead>
	
	<?
	$total_days_att=datediff("d",$from_con_date,$to_con_date);
	/*$sql=" SELECT emp_code,";   
	for($i=0;$i<$total_days_att; $i++)
	{
		$c_date=add_date($from_con_date,$i);
		if($i!=0) $sql .=",";
		$sql .=" sum(CASE WHEN attnd_date ='$c_date' THEN total_over_time_min END) AS 'OT".$c_date."'";
	}
	$sql .=" from hrm_attendance where attnd_date between '".$from_con_date."' and '".$to_con_date."' and emp_code in(0004869,0005616) $emp_code_search group by emp_code";	*/
	 //echo $total_days_att.$sql;
	  
	/*//perfect before category,ot_entitled and all searches 20-01-2014
	$sql="SELECT emp_code, concat_ws('**',attnd_date,total_over_time_min) as overtime_date from hrm_attendance where attnd_date between '".$from_con_date."' and '".$to_con_date."'";*/
	
	$sql="SELECT b.category,b.ot_entitled,b.emp_code,a.emp_code,a.company_id,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,concat_ws('**',a.attnd_date,a.total_over_time_min) as overtime_date from hrm_attendance a,hrm_employee b where a.attnd_date between '".$from_con_date."' and '".$to_con_date."' and b.category=2 and b.ot_entitled=1 and a.emp_code=b.emp_code $company_id $location_id $division_id  $department_id  $section_id $subsection_id";
	
	//echo $sql; die;
	$exe_sql_attnd=mysql_db_query($DB, $sql);
		$employee_ot=array();
		$monthly_employee_ot=array();
		while($data=mysql_fetch_array($exe_sql_attnd))
		{
			//$monthly_employee_ot[]
			//for($i=0;$i<$total_days_att; $i++)
			//{
				$hr=0;
				$min=0;
				$tmpp=0;
				$overtime_date=explode("**",$data['overtime_date']);
				//$c_date=add_date($from_con_date,$i);
				$c_date=$overtime_date[0];
				$date=date("Y-m",strtotime($c_date));
				if($emp_tmp[$date][$data['emp_code']]=="")//   in_array($data['emp_code'],))
				{
					$emp_tmp[$date][$data['emp_code']]=$data['emp_code'];
					$emp_count[$date]+=1;
				}
				 
				//$tmpp=get_buyer_ot_hr($data['OT'.$c_date],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$tmpp=get_buyer_ot_hr($overtime_date[1],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				
				if($ot_fraction!=3) 
				{
					$employee_ot[$date]['tot_ot'] +=$tmpp;
					if($tmpp>2)  $employee_ot[$date]['eot_count']++;
				}
				else
				{
					if($tmpp>2)  $employee_ot[$date]['eot_count']++;
					$tmppd=explode(".",$tmpp);
					$hr+=$tmppd[0];
					$min+=$tmppd[1];
					$employee_ot[$date]['tot_ot'] +=($hr+ floor($min/60)).".".str_pad(($min%60),2,"0",STR_PAD_LEFT);
					
				}
				
				if($eot_tmp[$date][$data['emp_code']]=="")//   in_array($data['emp_code'],))
				{
					if($tmpp>2)
					{
						$eot_tmp[$date][$data['emp_code']]=$data['emp_code'];
						$employee_ot[$date]['eot_countt']++;
					}
				}
			//}
		} 

	//var_dump($employee_ot['emp_code']);//die;
	//die;
	
	//print_r($emp_tmp[$date]);die;
	//print_r($employee_ot);die;
	ob_start();
	
 	foreach($employee_ot as $key=>$val){
	
    ?>
        <tr bgcolor="<? echo $bgcolor; ?>" height="20" class="print_tbody" style="font-size:11px">
           <td align="center" valign="middle" ><?  //$print_month=explode("-",$key);
					  $print_month=explode("-",$key);
					 $name_of_month=$months[(int) $print_month[1]];//$months[
					echo $name_of_month."-".$print_month[0];
					 //$months_name=strtotime($row_emp[attnd_date]);// echo $print_month=date('M-Y', $months_name);?></td>
            <td align="center" valign="middle" ><? echo $emp_count[$key]; ?></td>
            <td align="center" valign="middle" ><? echo $employee_ot[$key]['tot_ot']; ?></td>
            <td align="center" valign="middle" ><? echo round(($employee_ot[$key]['tot_ot']*1)/($emp_count[$key]*1)); ?></td>
            <td align="center" valign="middle" ><? echo $employee_ot[$key]['eot_countt']; ?></td>
            <td align="center" valign="middle" ><? echo round(($employee_ot[$key]['eot_countt']/$emp_count[$key])*100); ?></td>
            <td align="center" valign="middle" ><? 
				$seperate_month=$key; 
				$search_seperate_date=$seperate_month."-%%";
				//$seperated_emp=return_field_value("emp_code,separated_from","hrm_separation","separated_from like '$search_seperate_date'");
		
		//$seperated_emp=return_field_value("count(mst.emp_code),dtls.category,dtls.ot_entitled","hrm_separation mst, hrm_employee dtls","mst.emp_code=dtls.emp_code and 	mst.separated_from like '$search_seperate_date'  and dtls.category=2 and dtls.ot_entitled=1 and mst.emp_code=dtls.emp_code");
 //$seperated_emp=return_field_value("count(mst.emp_code),dtls.category,dtls.ot_entitled","hrm_separation mst, hrm_employee dtls","mst.emp_code=dtls.emp_code and 	mst.separated_from like '$search_seperate_date'  and dtls.category=2 and dtls.ot_entitled=1 and mst.emp_code=dtls.emp_code a.emp_code=b.emp_code $company_id $location_id $division_id  $department_id  $section_id $subsection_id");
 
 //new 21-01-2014
 $seperated_emp=return_field_value("count(mst.emp_code),a.category,a.ot_entitled,a.company_id,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id","hrm_separation mst, hrm_employee a","mst.emp_code=a.emp_code and mst.separated_from like '$search_seperate_date'  and a.category=2 and a.ot_entitled=1 and mst.emp_code=a.emp_code $company_id $location_id $division_id  $department_id  $section_id $subsection_id");
 
				echo $seperated_emp;
	
				  ?></td>
            <td align="center" valign="middle"><? echo round(($seperated_emp/$emp_count[$key])*100); ?></td>
            <td align="center" valign="middle" ><? //$un_auth_emp=return_field_value("count(distinct emp_code)","hrm_attendance as a","a.attnd_date like '$search_seperate_date' and a.status='A' $company_id $location_id $division_id  $department_id  $section_id $subsection_id");
			//new 26-01-2014
			 $un_auth_emp=return_field_value("count(distinct a.emp_code),b.category,b.ot_entitled,a.company_id,a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id","hrm_attendance a, hrm_employee b","a.emp_code=b.emp_code and a.attnd_date like '$search_seperate_date' and a.status='A'  and b.category=2 and b.ot_entitled=1  $company_id $location_id $division_id  $department_id  $section_id $subsection_id");
			
				 echo $un_auth_emp; ?></td>
            <td align="center" valign="middle"><? echo round(($un_auth_emp/$emp_count[$key])*100); ?></td>
        </tr>
       
  <?     
	
	//new add for number of days
	//count specific days in a month like how many satureday in a monday or how many mondays in a month


/*function my_cal_days_in_month($year,$month,$calday="1"){ //0 Sunday, 1 Monday, 2 Tue , 3 Wed , 4. Thu , 5. Fri , 6. Sat
    // calculate total number of occurance in this month
    $num = cal_days_in_month(CAL_GREGORIAN, $month, $year); // days in month
    $dayofweek = date( "w", mktime(0, 0, 0, $month, 1, $year));    
    $adddays=0;
    if($calday > $dayofweek )
            $adddays=1 + $calday - $dayofweek;
    else if($calday < $dayofweek )
            $adddays=1 + 7 + ($calday - $dayofweek );
               
    $remainingdays=$num-$adddays;
    $leavesnum=1+intval($remainingdays / 7);
    return     $leavesnum;
}

$year=2013;
$month=7;
$day=6;
echo my_cal_days_in_month($year,$month,$day);*/
	
	
	
	
	
				
	}
		//$ct++;
	
	
	//}//while loop end here 
	
	
	?>
</table>
<? 
	
	 
	$html = ob_get_contents();
	
	ob_clean();	
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/".$_SESSION['logic_erp']["user_name"]."*.xls") as $filename) 
		{			
            @unlink($filename);
		}		
	
	//html to xls convert
	$name=time();
	$name=$_SESSION['logic_erp']["user_name"]."$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
 	}





//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}
	

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
//echo $sql_data;die;
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}


function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}


?>