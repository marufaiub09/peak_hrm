<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}



if( $action == "monthly_attn_summery_audit" ) {	// Audit Summary
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name=$cbo_company_id order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
		{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
		}
	
	//subsection_details
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
			$shift_details[$row['id']] = $row['short_shift_name'];
	}
	
	
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$fromdate = $date_part."-".'01';
	$todate = $date_part."-".$get_days;
	
	if ($cbo_company_id==0) $company_id=""; else $company_id="and emp.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else  $department_id="and emp.department_id='$department_id'";
	//if ($txt_date==0) $txt_date=""; else $txt_date="and attnd_date='".convert_to_mysql_date($txt_date)."'";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="emp.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="emp.company_id,emp.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="emp.company_id,emp.location_id,emp.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by a.attnd_date CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by a.attnd_date  CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by a.attnd_date CAST(emp.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by a.attnd_date right(emp.id_card_no, 5)";}
	
	ob_start();	
	
	?>
	<div align="center" style="width:1700px; font-size:16px"><b><? echo $company_details[$cbo_company_id]['company_name']; ?></b></div>
    <div align="center" style="width:1700px; font-size:12px"><b>Master Attendance Register</b></div>
    <div align="center" style="width:1700px; font-size:12px"><b>Salary Month: <? echo date("F, Y",$salary_month_date); ?></b></div>
	<table width="1700" style="font-size:10px; border:1px solid #000" cellpadding="0" cellspacing="0" border="1" class="rpt_table" rules="all">
    	
	<?
	 $new_section=array();
		
	 $i=0;
	 //emp.department_id=$row_dp[id] and 
 	 $sqls = "SELECT a.*,emp.*, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
	 		  FROM hrm_attendance emp left join hrm_employee a on emp.emp_code=a.emp_code
			  where			  
			 a.emp_code=emp.emp_code and emp.attnd_date between '$fromdate' and   '$todate'
 				$company_id 
				$location_id
				$division_id 
				$section_id 
				$subsection_id 
				$department_id
				   order by  emp.emp_code,emp.attnd_date ";
			//	echo $sqls ; die;
	$new_array_emp=array();
	 $result1 = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
	 $kk=0;
	 while( $rows = mysql_fetch_array( $result1 ) ) 
	 {
		
		 //  echo $rows[emp_code]."</br>";
	 	if (!in_array($rows[emp_code],$new_array_emp))
		{
			$total_ot=0;
			$new_array_emp[$rows[emp_code]]=$rows[emp_code];
			if(in_array($rows['section_id'], $new_section[$rows['division_id']][$rows['department_id']]))						
			{
				$i++;
			}
			else
			{	
				$new_section[$rows['division_id']][$rows['department_id']][$rows[section_id]]=$rows[section_id];
				//$new_section[$i]=$rows[section_id];
				$division_name=$division_details[$rows['division_id']];
				$department_name=$department_details[$rows['department_id']];
				$section_name=$section_details[$rows['section_id']];
			?>	
			
                    <thead>
                        <tr>
                            <td colspan='<? echo $get_days+10; ?>'><b>Division Name: <? echo $division_name;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Department Name: <? echo $department_name;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Section Name: <? echo $section_name; ?></b></td>
                        </tr>
                        <tr>
                            <th width='50'><b>Emp Code</b></th>
                            <th width='50'><b>ID Card</b></th>
                            <th width='120'><b>Emp Name</b></th>
                            <th width='100'><b>Designation</b></th>
                            <th width='50'><b>Join Date</b></th>
                            <th width='20'><b>Date</b></th>				
                            <?		
                            for($j=1; $j<=$get_days;$j++)
                               {  ?>                        
                                    <th width='30'><b><? echo $j; ?></b></th>
                            <? } ?>
                            <th width='20' style="font-size:9px"><b>Total (P,GH,FH,W)</b></th>
                            <th width='20'><b>Total Absent</b></th>
                            <th width='20'><b>Total Leave</b></th>
                            <th width='20'><b>Total Late</b></th>
                            <th width='20'><b>Total OT</b></th>
                       </tr>
                  	</thead>	
				  <tbody>							
			<?	
            
            $i++;			
            
			}
			if ($i%2==0) $bgcolor="#E9F3FF"; 
				else $bgcolor="#FFFFFF";
			  ?>
			  
				<tr bgcolor="<? echo $bgcolor; ?>">
					<td width='50'><? echo $rows[emp_code]; ?></td>
					<td width='50'><? echo $rows[id_card_no];?></td>
					<td width='120'><? echo $rows[name];?></td>
					<td width='100'><? echo $designation_chart[$rows['designation_id']]['custom_designation'];?></td>
					<td width='50'><? echo convert_to_mysql_date ($rows[joining_date]);?></td>
					<td width='20' style="font-size:10px; font-weight:bold">Status</td>
                    <td><? echo $rows[status]; ?></td>
                    
                
			   
				   
				<?
			 
				$total_ot=get_buyer_ot_hr($rows[total_over_time_min],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				 $kk=1;
			   $present=0;$absent=0;$leave=0;$wday=0;$pay_day=0;$cnt=0;$is_new_join=0;$st_date=$date_part."-01";$total_ot=0;
				$ot_list="";$shift_list="";
		}  
		else
		{
			$total_ot+=get_buyer_ot_hr($rows[total_over_time_min],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				?>
               <td><? echo $rows[status]; ?></td>
                <?
			// 1-31 days column----------------------------------
			 if ($get_days==$kk) { 
			 ?>
             
             <td><? echo $total_ot; ?></td></tr>
             
             <? }
			 
		}
		 $kk++;
		//$kk=0;
	} //1st while loop
		 	
	
	?> 
    	</table>
	<?
				
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
	
	
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>