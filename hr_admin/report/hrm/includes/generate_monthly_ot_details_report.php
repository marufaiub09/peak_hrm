<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );



//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$start_1 = substr($row["starting_date"],0,3);
		$start_2 = substr($row["starting_date"],-1,1);

		$end_1 = substr($row["ending_date"],0,3);
		$end_2 = substr($row["ending_date"],-2,2);
		
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$start_1." ".$start_2." - ".$end_1." ".$end_2."</option>";
		//echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}



//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}



	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	//$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 order by trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}
	
	
	//lib_list_division
	$sql = "SELECT * FROM lib_list_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_list_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_list_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}	
	
if($action=='monthly_ot_details_sheet')
{
	
	//variable settings
	/*$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	//echo $qr;die;
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}*/
	
	
	
	$date_range = explode("_",$cbo_month_selector);
	
	$yrdata= strtotime($date_range[0]);
    $header_from_month=date('M-Y', $yrdata);
	
	$cdate=date("Y-m-d",time());
	//$date_part = $date_range[0];
	//echo $con_date."==";
	//echo $date_part;die; 
	
	$var_from_date=convert_to_mysql_date($date_range[0]);
	$var_to_date=convert_to_mysql_date($date_range[1]);
	$var_hrm_chart=get_variable_settings( $company_id, $var_from_date, $var_to_date );
	//print_r($var_hrm_chart);
	
	//die;
	
	if ($cbo_salary_sheet=='') {$emp_status="";} 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1";} 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and id in (SELECT MAX(id)  from hrm_separation where separated_from between '$date_range[0]' and '$date_range[1]' group by emp_code))";} //and separated_from like '$con_date'
	else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0 || $division_id=="") $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0 || $department_id=="") $department_id=""; else	$department_id="and a.department_id in ($department_id)";
	if ($section_id==0 || $section_id=="") $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0 || $subsection_id=="") $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($designation_id==0 || $designation_id=="") $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	
	//if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($cbo_emp_category=='') $category ="and emp.category in ($employee_category_index)"; else $category ="and emp.category='$cbo_emp_category'";
	
	if ($ot_type_id==0 || $ot_type_id==1) $ot_type =""; else $ot_type ="and a.total_over_time!=0.00";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and emp.salary_type_entitled='$cbo_salary_based'";
	
	
	$addrs ="";		
	if($company_details[$company_id]['plot_no'] !='') $addrs .= $company_details[$company_id]['plot_no'].", "; 
	if($company_details[$company_id]['level_no'] !='') $addrs .= $company_details[$company_id]['level_no'].", "; 
	if($company_details[$company_id]['block_no'] !='') $addrs .= $company_details[$company_id]['block_no'].", "; 
	if($company_details[$company_id]['road_no'] !='') $addrs .= $company_details[$company_id]['road_no'].", "; 
	if($company_details[$company_id]['city'] !='') $addrs .= $company_details[$company_id]['city'].", "; 
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,emp.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,emp.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,emp.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,emp.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,emp.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,emp.emp_code,";}
	
	/*$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="emp.company_id,emp.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="emp.company_id,emp.location_id,emp.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="emp.company_id,emp.location_id,emp.division_id,emp.department_id,emp.section_id,emp.subsection_id,emp.emp_code,";}*/
	
	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-13)." CAST(right(trim(emp.id_card_no), 5)as SIGNED)";}	
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by	
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$holiday_date=date('Y-m-%%',strtotime($date_range[0]));
	
	//calculate holiday ot monthly
	$sql ="select emp_code, attnd_date, company_id, total_over_time_min AS monthly_tot_ot from  hrm_attendance where  attnd_date like '$holiday_date' and status in ('GH','FH','CH','W') order by emp_code,attnd_date";
	//echo $sql;die;
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$holiday_overtime = array();
	while( $row = mysql_fetch_assoc( $result ) )
	{
		$tmpp=get_buyer_ot_hr($row['monthly_tot_ot'],$var_hrm_chart[$row['company_id']][$row['attnd_date']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']][$row['attnd_date']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']][$row['attnd_date']]['ot_start_minute'], 1440);	
		
		$tmppd=explode(".",$tmpp);
		$hr[$row['emp_code']]+=$tmppd[0];
		$min[$row['emp_code']]+=$tmppd[1];
		$holiday_overtime[$row['emp_code']] =($hr[$row['emp_code']]+ floor($min[$row['emp_code']]/60)).".".str_pad(($min[$row['emp_code']]%60),2,"0",STR_PAD_LEFT);	 							
		//$holiday_overtime[$row['emp_code']] = mysql_real_escape_string( $row['monthly_tot_ot'] );		
	} 	
	//print_r($holiday_overtime['0024284']); die;
	// print_r($holiday_overtime);die;
	
	
//new pay_amount
$sql = "SELECT b.emp_code,a.salary_head_id,a.pay_amount FROM hrm_salary_dtls a, hrm_salary_mst b WHERE b.id=a.salary_mst_id and b.salary_periods='$date_range[0]' and a.salary_head_id in (35)";
 	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
 	$processed_bonus_amount = array();
 	while( $row = mysql_fetch_assoc( $result ) ) 
	{		
		$processed_bonus_amount[$row['emp_code']][$row['salary_head_id']] = $row['pay_amount'];
 	}
	
		
//new hrm_bonus_dtls
$sql_bonuse = "SELECT * FROM hrm_bonus_dtls WHERE bonus_salary_period='$date_range[0]'";
 	$result = mysql_query( $sql_bonuse ) or die( $sql_bonuse . "<br />" . mysql_error() );
 	$festival_bonus_amount = array();
 	while( $row = mysql_fetch_assoc( $result ) ) 
	{		
		$festival_bonus_amount[$row['emp_code']] = $row['bonus_amount'];
 	}
	
			
// Employee hrm_employee_education  
	$sql_edu = "SELECT emp_code,exam_name FROM hrm_employee_education order by id desc,emp_code asc";
	$result = mysql_query( $sql_edu ) or die( $sql_edu . "<br />" . mysql_error() );
	$employee_education_arr = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if(!in_array($row['emp_code'],$tmp_code))
		{
			$tmp_code[]=$row['emp_code'];
			$employee_education_arr[$row['emp_code']]= $row['exam_name'];
		}
	}
	
	
	
				
// Employee hrm_employee_address  
	$sql_address = "SELECT emp_code,division_id FROM hrm_employee_address WHERE address_type=1 and status_active=1 order by emp_code"; //desc,emp_code asc
	$result = mysql_query( $sql_address ) or die( $sql_address . "<br />" . mysql_error() );
	$employee_address_arr = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if(!in_array($row['emp_code'],$tmp_code))
		{
			$tmp_code[]=$row['emp_code'];
			$employee_address_arr[$row['emp_code']]= $row['division_id'];
		}
	}

ob_start();	
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:900px">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1"><? echo $addrs; ?></font><br />
        <font size="-1">Extra OT Sheet: <? echo date("F, Y",strtotime($date_range[0])); ?> </font>
    </div>
    <table cellpadding="0" cellspacing="0" width="2070" class="rpt_table" style="font-size:12px; border:1px solid #000000" rules="all"> 
    	 <thead>
                <tr class="tbl_top">
                    <th width="30">SL</th>
                    <th width="70">Month Year</th>
                    <th align="100">Name</th>
                    <th align="80">Designation</th>
                    <th width="70">Employee Code</th>
                    <th width="70">ID Card</th>
                    <th width="70">Year of Birth</th>
                    <th align="70">Gender</th>
                    <th align="70"><p>Regional Worker Category</p></th>
                    <th align="70">Department</th>
                    <th align="80">D O J</th>
                    <th align="80">Service Length</th>
                    <th align="80">Level of Education</th>
                    <th align="80">Working Hours</th>
                    <th align="80">OT Hrs</th>
                    <th align="80">E. OT Hrs</th>
                    <th align="80">Week. OT</th>
                    <th align="80">Total OT</th>
                    <th align="80">Total Working Hours</th>
                    <th align="80">Gross Salary</th>
                    <th align="80">Festival Bonus</th>
                    <th align="80">Attendance Bonus</th>
                    <th align="80">Total Bonus</th>
                    <th align="80">OT Amount</th>
                    <th align="80">E. OT Amount</th>
                    <th align="80">Week. OT Amount</th>
                    <th align="80">Total OT Amount</th>
                    <th align="80">Net Payable</th>
                </tr>
        </thead>
    <?
	$sam=0;
	
		if($cbo_salary_based==1)//Piece Rate Based (and emp.salary_grade=4)
		{
			$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no,emp.joining_date,emp.designation_level,emp.designation_id as emp_designation, emp.punch_card_no,emp.salary_grade,a.* 
					FROM 
					hrm_production_salary_mst a, hrm_employee emp 
					WHERE 
					emp.emp_code!='' and
					a.salary_periods  between '$date_range[0]' and '$date_range[1]'  and 
					a.emp_code=emp.emp_code
					$cbo_company_id  
					$division_id
					$department_id 
					$location_id 
					$section_id 				
					$subsection_id 
					$designation_id 
					$category
					$emp_status
					$ot_type
					$salary_based
					group by 
					$dynamic_groupby
					$orderby";
		}
		else//Monthly Salary Based
		{
			$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, DATEDIFF( '$cdate',joining_date) as jobdur, emp.id_card_no, emp.joining_date, emp.dob, emp.sex, emp.designation_level,emp.designation_id as emp_designation, emp.punch_card_no, emp.salary_grade, a.* 
				FROM 
				hrm_salary_mst a, hrm_employee emp 
				WHERE 
				emp.emp_code!='' and
				a.salary_periods  between '$date_range[0]' and '$date_range[1]' and 
				a.emp_code=emp.emp_code
				$cbo_company_id  
				$division_id
				$department_id 
				$location_id 
				$section_id 				
				$subsection_id 
				$designation_id 
				$category
				$emp_status
				$ot_type
				$salary_based
				group by 
				$dynamic_groupby
				$orderby";
		}
		
		//echo $sql;die;
		//order by emp.section_id, CAST(emp.id_card_no as SIGNED)";
		
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$greand_total_gross_salary=0;
		$subsection_total_gross_salary=0;
		$section_total_gross_salary=0;
		$department_total_gross_salary=0;
		$division_total_gross_salary=0;
		$location_total_gross_salary=0;
		
		$greand_total_working_hr=0;$greand_total_ot_min=0;$greand_total_e_ot=0;$greand_total_hoot_min=0;$greand_total_total_ot_min=0;$greand_total_total_working_hr=0;$greand_total_festival_bonus=0;$greand_total_atten_bonus=0;$greand_total_total_bonus=0;$greand_total_ot_amount=0;$greand_total_e_ot_amount=0;$greand_total_hoot_amount=0;$greand_total_total_ot_amount=0;$greand_total_total_amount=0;
		
		$subsection_total_working_hr=0;$subsection_total_ot_min=0;$subsection_total_e_ot=0;$subsection_total_hoot_min=0;$subsection_total_total_ot_min=0;$subsection_total_total_working_hr=0;$subsection_total_festival_bonus=0;$subsection_total_atten_bonus=0;$subsection_total_total_bonus=0;$subsection_total_ot_amount=0;$subsection_total_e_ot_amount=0;$subsection_total_hoot_amount=0;$subsection_total_total_ot_amount=0;$subsection_total_total_amount=0;
		
		$section_total_working_hr=0;$section_total_ot_min=0;$section_total_e_ot=0;$section_total_hoot_min=0;$section_total_total_ot_min=0;$section_total_total_working_hr=0;$section_total_festival_bonus=0;$section_total_atten_bonus=0;$section_total_total_bonus=0;$section_total_ot_amount=0;$section_total_e_ot_amount=0;$section_total_hoot_amount=0;$section_total_total_ot_amount=0;$section_total_total_amount=0;
		
		$department_total_working_hr=0;$department_total_ot_min=0;$department_total_e_ot=0;$department_total_hoot_min=0;$department_total_total_ot_min=0;$department_total_total_working_hr=0;$department_total_festival_bonus=0;$department_total_atten_bonus=0;$department_total_total_bonus=0;$department_total_ot_amount=0;$department_total_e_ot_amount=0;$department_total_hoot_amount=0;$department_total_total_ot_amount=0;$department_total_total_amount=0;
		
		$division_total_working_hr=0;$division_total_ot_min=0;$division_total_e_ot=0;$division_total_hoot_min=0;$division_total_total_ot_min=0;$division_total_total_working_hr=0;$division_total_festival_bonus=0;$division_total_atten_bonus=0;$division_total_total_bonus=0;$division_total_ot_amount=0;$division_total_e_ot_amount=0;$division_total_hoot_amount=0;$division_total_total_ot_amount=0;$division_total_total_amount=0;

		$location_total_working_hr=0;$location_total_ot_min=0;$location_total_e_ot=0;$location_total_hoot_min=0;$location_total_total_ot_min=0;$location_total_total_working_hr=0;$location_total_festival_bonus=0;$location_total_atten_bonus=0;$location_total_total_bonus=0;$location_total_ot_amount=0;$location_total_e_ot_amount=0;$location_total_hoot_amount=0;$location_total_total_ot_amount=0;$location_total_total_amount=0;
			
			
		$sl=0;
		$r=1;
		while($row=mysql_fetch_array($result))  //while loop start here================================
		{
			 
			if($cbo_salary_based==1)
			{ 
				$row[over_time_rate]=return_field_value("rate","hrm_piece_rate_ot_mst","grade_id=$row[salary_grade]");
			}
			else
			{
				$row['over_time_rate']=$row['over_time_rate'];
			}
			
			$sl++;
			if($sl==1)
			{
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
			}
			
			 if($sl!=1)
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row[subsection_id]]=$row[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row[section_id]]=$row[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row[subsection_id]]=$row[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row[department_id]]=$row[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row[section_id]]=$row[section_id];
										$subsection_arr[$row[subsection_id]]=$row[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row[division_id]]=$row[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row[department_id]]=$row[department_id];
									$section_arr[$row[section_id]]=$row[section_id];
									$subsection_arr[$row[subsection_id]]=$row[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row[location_id]]=$row[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
				}
				
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Subsection Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_working_hr/60)), abs((int)($subsection_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_ot_min/60)), abs((int)($subsection_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_e_ot/60)), abs((int)($subsection_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_hoot_min/60)), abs((int)($subsection_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_total_ot_min/60)), abs((int)($subsection_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_total_working_hr/60)), abs((int)($subsection_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $subsection_total_gross_salary;  ?></td>
                <td align="right"><? echo $subsection_total_festival_bonus;  ?></td>
                <td align="right"><? echo $subsection_total_atten_bonus; ?></td>
                <td align="right"><? echo $subsection_total_total_bonus; ?></td>	
                <td align="right"><? echo $subsection_total_ot_amount; ?></td>
                <td align="right"><? echo $subsection_total_e_ot_amount; ?></td>
                <td align="right"><? echo $subsection_total_hoot_amount; ?></td>
                <td align="right"><? echo $subsection_total_total_ot_amount; ?></td>
                <td align="right"><? echo $subsection_total_total_amount; ?></td>
			</tr>
			<?
			$subsection_total_working_hr=0;$subsection_total_ot_min=0;$subsection_total_e_ot=0;$subsection_total_hoot_min=0;$subsection_total_total_ot_min=0;$subsection_total_total_working_hr=0;$subsection_total_festival_bonus=0;$subsection_total_atten_bonus=0;$subsection_total_total_bonus=0;$subsection_total_ot_amount=0;$subsection_total_e_ot_amount=0;$subsection_total_hoot_amount=0;$subsection_total_total_ot_amount=0;$subsection_total_total_amount=0;$subsection_total_gross_salary=0;
			
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Section Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_working_hr/60)), abs((int)($section_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_ot_min/60)), abs((int)($section_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_e_ot/60)), abs((int)($section_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_hoot_min/60)), abs((int)($section_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_total_ot_min/60)), abs((int)($section_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_total_working_hr/60)), abs((int)($section_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $section_total_gross_salary;  ?></td>
                <td align="right"><? echo $section_total_festival_bonus;  ?></td>
                <td align="right"><? echo $section_total_atten_bonus; ?></td>
                <td align="right"><? echo $section_total_total_bonus; ?></td>	
                <td align="right"><? echo $section_total_ot_amount; ?></td>
                <td align="right"><? echo $section_total_e_ot_amount; ?></td>
                <td align="right"><? echo $section_total_hoot_amount; ?></td>
                <td align="right"><? echo $section_total_total_ot_amount; ?></td>
                <td align="right"><? echo $section_total_total_amount; ?></td>
			</tr>
			<?
			$section_total_working_hr=0;$section_total_ot_min=0;$section_total_e_ot=0;$section_total_hoot_min=0;$section_total_total_ot_min=0;$section_total_total_working_hr=0;$section_total_festival_bonus=0;$section_total_atten_bonus=0;$section_total_total_bonus=0;$section_total_ot_amount=0;$section_total_e_ot_amount=0;$section_total_hoot_amount=0;$section_total_total_ot_amount=0;$section_total_total_amount=0;$section_total_gross_salary=0;
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Department Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_working_hr/60)), abs((int)($department_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_ot_min/60)), abs((int)($department_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_e_ot/60)), abs((int)($department_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_hoot_min/60)), abs((int)($department_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_total_ot_min/60)), abs((int)($department_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_total_working_hr/60)), abs((int)($department_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $department_total_gross_salary;  ?></td>
                <td align="right"><? echo $department_total_festival_bonus;  ?></td>
                <td align="right"><? echo $department_total_atten_bonus; ?></td>
                <td align="right"><? echo $department_total_total_bonus; ?></td>	
                <td align="right"><? echo $department_total_ot_amount; ?></td>
                <td align="right"><? echo $department_total_e_ot_amount; ?></td>
                <td align="right"><? echo $department_total_hoot_amount; ?></td>
                <td align="right"><? echo $department_total_total_ot_amount; ?></td>
                <td align="right"><? echo $department_total_total_amount; ?></td>
			</tr>
			<? 
			$department_total_working_hr=0;$department_total_ot_min=0;$department_total_e_ot=0;$department_total_hoot_min=0;$department_total_total_ot_min=0;$department_total_total_working_hr=0;$department_total_festival_bonus=0;$department_total_atten_bonus=0;$department_total_total_bonus=0;$department_total_ot_amount=0;$department_total_e_ot_amount=0;$department_total_hoot_amount=0;$department_total_total_ot_amount=0;$department_total_total_amount=0;$department_total_gross_salary=0;
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Division Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_working_hr/60)), abs((int)($division_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_ot_min/60)), abs((int)($division_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_e_ot/60)), abs((int)($division_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_hoot_min/60)), abs((int)($division_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_total_ot_min/60)), abs((int)($division_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_total_working_hr/60)), abs((int)($division_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $division_total_gross_salary;  ?></td>
                <td align="right"><? echo $division_total_festival_bonus;  ?></td>
                <td align="right"><? echo $division_total_atten_bonus; ?></td>
                <td align="right"><? echo $division_total_total_bonus; ?></td>	
                <td align="right"><? echo $division_total_ot_amount; ?></td>
                <td align="right"><? echo $division_total_e_ot_amount; ?></td>
                <td align="right"><? echo $division_total_hoot_amount; ?></td>
                <td align="right"><? echo $division_total_total_ot_amount; ?></td>
                <td align="right"><? echo $division_total_total_amount; ?></td>
			</tr>
			<?
			$division_total_working_hr=0;$division_total_ot_min=0;$division_total_e_ot=0;$division_total_hoot_min=0;$division_total_total_ot_min=0;$division_total_total_working_hr=0;$division_total_festival_bonus=0;$division_total_atten_bonus=0;$division_total_total_bonus=0;$division_total_ot_amount=0;$division_total_e_ot_amount=0;$division_total_hoot_amount=0;$division_total_total_ot_amount=0;$division_total_total_amount=0;$division_total_gross_salary=0;
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Location Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_working_hr/60)), abs((int)($location_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_ot_min/60)), abs((int)($location_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_e_ot/60)), abs((int)($location_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_hoot_min/60)), abs((int)($location_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_total_ot_min/60)), abs((int)($location_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_total_working_hr/60)), abs((int)($location_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $location_total_gross_salary;  ?></td>
                <td align="right"><? echo $location_total_festival_bonus;  ?></td>
                <td align="right"><? echo $location_total_atten_bonus; ?></td>
                <td align="right"><? echo $location_total_total_bonus; ?></td>	
                <td align="right"><? echo $location_total_ot_amount; ?></td>
                <td align="right"><? echo $location_total_e_ot_amount; ?></td>
                <td align="right"><? echo $location_total_hoot_amount; ?></td>
                <td align="right"><? echo $location_total_total_ot_amount; ?></td>
                <td align="right"><? echo $location_total_total_amount; ?></td>
			</tr>
			<?
			$location_total_working_hr=0;$location_total_ot_min=0;$location_total_e_ot=0;$location_total_hoot_min=0;$location_total_total_ot_min=0;$location_total_total_working_hr=0;$location_total_festival_bonus=0;$location_total_atten_bonus=0;$location_total_total_bonus=0;$location_total_ot_amount=0;$location_total_e_ot_amount=0;$location_total_hoot_amount=0;$location_total_total_ot_amount=0;$location_total_total_amount=0;$location_total_gross_salary=0;
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row[location_id]]."  ,"; 
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row[division_id]]."  ,"; 
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row[section_id]]."  ,"; 
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row[subsection_id]]."  ,";
			}
		

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr><td colspan="27"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
				//$header_value=$header_val;
			}		
		 
 		}
			   
			
			if($r%2==0) $bgcolor='#FFFFFF'; else $bgcolor='#EEEEEE';
				
			?>    
				<tr bgcolor="<? echo $bgcolor; ?>" height="50">
					<td><? echo $r; ?></td>
					<td><? echo $header_from_month; ?></td>
					<td><? echo $row['name']; ?></td>
                    <td><? echo $designation_chart[$row['emp_designation']]; ?></td>
                    <td><? echo $row['emp_code']; ?></td>
					<td><? echo $row['id_card_no']; ?></td>
					<td><? echo date('Y', strtotime($row['dob'])); ?></td>
					<td><? echo $sex_arr[$row['sex']]; ?></td>
					<td align="center"><? echo $row['salary_grade']; //$division_list_details[$employee_address_arr[$row['emp_code']]]; ?></td>
					<td><? echo $department_details[$row['department_id']]; ?></td>
                    <td><? echo convert_to_mysql_date($row['joining_date']); ?></td>
					<td><?  $age= my_old(convert_to_mysql_date($row['joining_date']));  printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]); ?></td>
                    <td><? echo $employee_education_arr[$row['emp_code']]; ?></td> 	
					<td><?  $tot_hr=$row['total_working_days']*8; $tot_min=($tot_hr*60); echo $total_working_hr = sprintf("%d.%02d", abs((int)($tot_min/60)), abs((int)($tot_min%60)));?></td>
					<td><? echo $row['b_over_time']; $tmppd=explode(".",$row['b_over_time']); $ot_min=($tmppd[0]*60)+$tmppd[1]; ?> </td>
					<td>
					<? 
					$ho_tmppd=explode(".",$holiday_overtime[$row['emp_code']]); 
					$hoot_min=($ho_tmppd[0]*60)+$ho_tmppd[1];
					
					$e_tmppd=explode(".",$row['e_over_time']); 
					$eot_min=($e_tmppd[0]*60)+$e_tmppd[1];
					$e_ot=($eot_min-$hoot_min);
					echo $e_ot_ho =sprintf("%d.%02d", abs((int)($e_ot/60)), abs((int)($e_ot%60))); 
					?>
                    </td>
                    <td><? echo $holiday_overtime[$row['emp_code']]; ?> </td>
                    <td><? echo $row['total_over_time'];$tot_ot=explode(".",$row['total_over_time']); $total_over_time_min=($tot_ot[0]*60)+$tot_ot[1]; ?>  </td>
					<td>
					<? 
					$tot_working_min=($tot_min+$total_over_time_min); 
					echo $tot_working_houre = sprintf("%d.%02d", abs((int)($tot_working_min/60)), abs((int)($tot_working_min%60))); 
					?>
                    </td>
                    <td align="right"><? echo $row['gross_salary']; ?></td>
					<td align="right"><? echo $festival_bonus_amount[$row['emp_code']]; ?></td>
					<td align="right"><? echo $processed_bonus_amount[$row['emp_code']][35]; ?></td>
					<td align="right"><? echo $total_bonus=($festival_bonus_amount[$row['emp_code']]+$processed_bonus_amount[$row['emp_code']][35]); ?></td>
					<td align="right"><?  $over_time_rate_min=($row['over_time_rate']/60); echo $ot_amount=round($ot_min*$over_time_rate_min); ?></td>
					<td align="right"><? echo $eot_amount=round($e_ot*$over_time_rate_min); ?></td>
                    <td align="right"><? echo $hoot_amount=round($hoot_min*$over_time_rate_min); ?></td>
					<td align="right"><? echo $total_ot_amount=($ot_amount+$eot_amount+$hoot_amount); ?></td>
                    <td align="right"><? echo $total_amount=($total_bonus+$total_ot_amount); ?></td>
				</tr>
			<?
			
			$greand_total_working_hr+=$tot_min;
			$greand_total_ot_min+=$ot_min;
			$greand_total_e_ot+=$e_ot;
			$greand_total_hoot_min+=$hoot_min;
			$greand_total_total_ot_min+=$total_over_time_min;
			$greand_total_total_working_hr+=$tot_working_min;
			$greand_total_festival_bonus+=round($festival_bonus_amount[$row['emp_code']]);
			$greand_total_atten_bonus+=round($processed_bonus_amount[$row['emp_code']][35]);
			$greand_total_total_bonus+=round($total_bonus);
			$greand_total_ot_amount+=round($ot_amount);
			$greand_total_e_ot_amount+=round($eot_amount);
			$greand_total_hoot_amount+=round($hoot_amount);
			$greand_total_total_ot_amount+=round($total_ot_amount);
			$greand_total_total_amount+=round($total_amount);
			
			$subsection_total_working_hr+=$tot_min;
			$subsection_total_ot_min+=$ot_min;
			$subsection_total_e_ot+=$e_ot;
			$subsection_total_hoot_min+=$hoot_min;
			$subsection_total_total_ot_min+=$total_over_time_min;
			$subsection_total_total_working_hr+=$tot_working_min;
			$subsection_total_festival_bonus+=round($festival_bonus_amount[$row['emp_code']]);
			$subsection_total_atten_bonus+=round($processed_bonus_amount[$row['emp_code']][35]);
			$subsection_total_total_bonus+=round($total_bonus);
			$subsection_total_ot_amount+=round($ot_amount);
			$subsection_total_e_ot_amount+=round($eot_amount);
			$subsection_total_hoot_amount+=round($hoot_amount);
			$subsection_total_total_ot_amount+=round($total_ot_amount);
			$subsection_total_total_amount+=round($total_amount);
			
			$section_total_working_hr+=$tot_min;
			$section_total_ot_min+=$ot_min;
			$section_total_e_ot+=$e_ot;
			$section_total_hoot_min+=$hoot_min;
			$section_total_total_ot_min+=$total_over_time_min;
			$section_total_total_working_hr+=$tot_working_min;
			$section_total_festival_bonus+=round($festival_bonus_amount[$row['emp_code']]);
			$section_total_atten_bonus+=round($processed_bonus_amount[$row['emp_code']][35]);
			$section_total_total_bonus+=round($total_bonus);
			$section_total_ot_amount+=round($ot_amount);
			$section_total_e_ot_amount+=round($eot_amount);
			$section_total_hoot_amount+=round($hoot_amount);
			$section_total_total_ot_amount+=round($total_ot_amount);
			$section_total_total_amount+=round($total_amount);
			
			$department_total_working_hr+=$tot_min;
			$department_total_ot_min+=$ot_min;
			$department_total_e_ot+=$e_ot;
			$department_total_hoot_min+=$hoot_min;
			$department_total_total_ot_min+=$total_over_time_min;
			$department_total_total_working_hr+=$tot_working_min;
			$department_total_festival_bonus+=round($festival_bonus_amount[$row['emp_code']]);
			$department_total_atten_bonus+=round($processed_bonus_amount[$row['emp_code']][35]);
			$department_total_total_bonus+=round($total_bonus);
			$department_total_ot_amount+=round($ot_amount);
			$department_total_e_ot_amount+=round($eot_amount);
			$department_total_hoot_amount+=round($hoot_amount);
			$department_total_total_ot_amount+=round($total_ot_amount);
			$department_total_total_amount+=round($total_amount);
			
			$division_total_working_hr+=$tot_min;
			$division_total_ot_min+=$ot_min;
			$division_total_e_ot+=$e_ot;
			$division_total_hoot_min+=$hoot_min;
			$division_total_total_ot_min+=$total_over_time_min;
			$division_total_total_working_hr+=$tot_working_min;
			$division_total_festival_bonus+=round($festival_bonus_amount[$row['emp_code']]);
			$division_total_atten_bonus+=round($processed_bonus_amount[$row['emp_code']][35]);
			$division_total_total_bonus+=round($total_bonus);
			$division_total_ot_amount+=round($ot_amount);
			$division_total_e_ot_amount+=round($eot_amount);
			$division_total_hoot_amount+=round($hoot_amount);
			$division_total_total_ot_amount+=round($total_ot_amount);
			$division_total_total_amount+=round($total_amount);
			
			$location_total_working_hr+=$tot_min;
			$location_total_ot_min+=$ot_min;
			$location_total_e_ot+=$e_ot;
			$location_total_hoot_min+=$hoot_min;
			$location_total_total_ot_min+=$total_over_time_min;
			$location_total_total_working_hr+=$tot_working_min;
			$location_total_festival_bonus+=round($festival_bonus_amount[$row['emp_code']]);
			$location_total_atten_bonus+=round($processed_bonus_amount[$row['emp_code']][35]);
			$location_total_total_bonus+=round($total_bonus);
			$location_total_ot_amount+=round($ot_amount);
			$location_total_e_ot_amount+=round($eot_amount);
			$location_total_hoot_amount+=round($hoot_amount);
			$location_total_total_ot_amount+=round($total_ot_amount);
			$location_total_total_amount+=round($total_amount);
			
		$greand_total_gross_salary+=$row['gross_salary'];
		$subsection_total_gross_salary+=$row['gross_salary'];
		$section_total_gross_salary+=$row['gross_salary'];
		$department_total_gross_salary+=$row['gross_salary'];
		$division_total_gross_salary+=$row['gross_salary'];
		$location_total_gross_salary+=$row['gross_salary'];
			
		$r++;
		} //while loop end here===========================================================================
		
		if($status_subsec==1)
		{
			
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                 <td colspan="13" align="center">Subsection Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_working_hr/60)), abs((int)($subsection_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_ot_min/60)), abs((int)($subsection_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_e_ot/60)), abs((int)($subsection_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_hoot_min/60)), abs((int)($subsection_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_total_ot_min/60)), abs((int)($subsection_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($subsection_total_total_working_hr/60)), abs((int)($subsection_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $subsection_total_gross_salary;  ?></td>
                <td align="right"><? echo $subsection_total_festival_bonus;  ?></td>
                <td align="right"><? echo $subsection_total_atten_bonus; ?></td>
                <td align="right"><? echo $subsection_total_total_bonus; ?></td>	
                <td align="right"><? echo $subsection_total_ot_amount; ?></td>
                <td align="right"><? echo $subsection_total_e_ot_amount; ?></td>
                <td align="right"><? echo $subsection_total_hoot_amount; ?></td>
                <td align="right"><? echo $subsection_total_total_ot_amount; ?></td>
                <td align="right"><? echo $subsection_total_total_amount; ?></td>
			</tr>
			<?
			$subsection_total_working_hr=0;$subsection_total_ot_min=0;$subsection_total_e_ot=0;$subsection_total_hoot_min=0;$subsection_total_total_ot_min=0;$subsection_total_total_working_hr=0;$subsection_total_festival_bonus=0;$subsection_total_atten_bonus=0;$subsection_total_total_bonus=0;$subsection_total_ot_amount=0;$subsection_total_e_ot_amount=0;$subsection_total_hoot_amount=0;$subsection_total_total_ot_amount=0;$subsection_total_total_amount=0;$subsection_total_gross_salary=0;
		} 
		
		if($status_sec==1)
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Section Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_working_hr/60)), abs((int)($section_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_ot_min/60)), abs((int)($section_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_e_ot/60)), abs((int)($section_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_hoot_min/60)), abs((int)($section_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_total_ot_min/60)), abs((int)($section_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($section_total_total_working_hr/60)), abs((int)($section_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $section_total_gross_salary;  ?></td>
                <td align="right"><? echo $section_total_festival_bonus;  ?></td>
                <td align="right"><? echo $section_total_atten_bonus; ?></td>
                <td align="right"><? echo $section_total_total_bonus; ?></td>	
                <td align="right"><? echo $section_total_ot_amount; ?></td>
                <td align="right"><? echo $section_total_e_ot_amount; ?></td>
                <td align="right"><? echo $section_total_hoot_amount; ?></td>
                <td align="right"><? echo $section_total_total_ot_amount; ?></td>
                <td align="right"><? echo $section_total_total_amount; ?></td>	
			</tr>
			<?
			$section_total_working_hr=0;$section_total_ot_min=0;$section_total_e_ot=0;$section_total_hoot_min=0;$section_total_total_ot_min=0;$section_total_total_working_hr=0;$section_total_festival_bonus=0;$section_total_atten_bonus=0;$section_total_total_bonus=0;$section_total_ot_amount=0;$section_total_e_ot_amount=0;$section_total_hoot_amount=0;$section_total_total_ot_amount=0;$section_total_total_amount=0;$section_total_gross_salary=0;
		}
		
		if($status_dept==1 )
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Department Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_working_hr/60)), abs((int)($department_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_ot_min/60)), abs((int)($department_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_e_ot/60)), abs((int)($department_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_hoot_min/60)), abs((int)($department_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_total_ot_min/60)), abs((int)($department_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($department_total_total_working_hr/60)), abs((int)($department_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $department_total_gross_salary;  ?></td>
                <td align="right"><? echo $department_total_festival_bonus;  ?></td>
                <td align="right"><? echo $department_total_atten_bonus; ?></td>
                <td align="right"><? echo $department_total_total_bonus; ?></td>	
                <td align="right"><? echo $department_total_ot_amount; ?></td>
                <td align="right"><? echo $department_total_e_ot_amount; ?></td>
                <td align="right"><? echo $department_total_hoot_amount; ?></td>
                <td align="right"><? echo $department_total_total_ot_amount; ?></td>
                <td align="right"><? echo $department_total_total_amount; ?></td>
			</tr>
			<?
			$department_total_working_hr=0;$department_total_ot_min=0;$department_total_e_ot=0;$department_total_hoot_min=0;$department_total_total_ot_min=0;$department_total_total_working_hr=0;$department_total_festival_bonus=0;$department_total_atten_bonus=0;$department_total_total_bonus=0;$department_total_ot_amount=0;$department_total_e_ot_amount=0;$department_total_hoot_amount=0;$department_total_total_ot_amount=0;$department_total_total_amount=0;$department_total_gross_salary=0;
		}
		 
		if($status_divis==1 )
		{
			
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
               <td colspan="13" align="center">Division Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_working_hr/60)), abs((int)($division_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_ot_min/60)), abs((int)($division_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_e_ot/60)), abs((int)($division_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_hoot_min/60)), abs((int)($division_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_total_ot_min/60)), abs((int)($division_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($division_total_total_working_hr/60)), abs((int)($division_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $division_total_gross_salary;  ?></td>
                <td align="right"><? echo $division_total_festival_bonus;  ?></td>
                <td align="right"><? echo $division_total_atten_bonus; ?></td>
                <td align="right"><? echo $division_total_total_bonus; ?></td>	
                <td align="right"><? echo $division_total_ot_amount; ?></td>
                <td align="right"><? echo $division_total_e_ot_amount; ?></td>
                <td align="right"><? echo $division_total_hoot_amount; ?></td>
                <td align="right"><? echo $division_total_total_ot_amount; ?></td>
                <td align="right"><? echo $division_total_total_amount; ?></td>	
			</tr>
			<?
			
			$division_total_working_hr=0;$division_total_ot_min=0;$division_total_e_ot=0;$division_total_hoot_min=0;$division_total_total_ot_min=0;$division_total_total_working_hr=0;$division_total_festival_bonus=0;$division_total_atten_bonus=0;$division_total_total_bonus=0;$division_total_ot_amount=0;$division_total_e_ot_amount=0;$division_total_hoot_amount=0;$division_total_total_ot_amount=0;$division_total_total_amount=0;$division_total_gross_salary=0;
		}
		 
		if($status_loc==1 )
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="13" align="center">Location Total</td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_working_hr/60)), abs((int)($location_total_working_hr%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_ot_min/60)), abs((int)($location_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_e_ot/60)), abs((int)($location_total_e_ot%60))); ?></td>	
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_hoot_min/60)), abs((int)($location_total_hoot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_total_ot_min/60)), abs((int)($location_total_total_ot_min%60))); ?></td>
                <td><? echo sprintf("%d.%02d", abs((int)($location_total_total_working_hr/60)), abs((int)($location_total_total_working_hr%60))); ?></td>
                <td align="right"><? echo $location_total_gross_salary;  ?></td>
                <td align="right"><? echo $location_total_festival_bonus;  ?></td>
                <td align="right"><? echo $location_total_atten_bonus; ?></td>
                <td align="right"><? echo $location_total_total_bonus; ?></td>	
                <td align="right"><? echo $location_total_ot_amount; ?></td>
                <td align="right"><? echo $location_total_e_ot_amount; ?></td>
                <td align="right"><? echo $location_total_hoot_amount; ?></td>
                <td align="right"><? echo $location_total_total_ot_amount; ?></td>
                <td align="right"><? echo $location_total_total_amount; ?></td>
			</tr>
			<?
			
			$location_total_working_hr=0;$location_total_ot_min=0;$location_total_e_ot=0;$location_total_hoot_min=0;$location_total_total_ot_min=0;$location_total_total_working_hr=0;$location_total_festival_bonus=0;$location_total_atten_bonus=0;$location_total_total_bonus=0;$location_total_ot_amount=0;$location_total_e_ot_amount=0;$location_total_hoot_amount=0;$location_total_total_ot_amount=0;$location_total_total_amount=0;$location_total_gross_salary=0;
		}
		
		?>
		<tr style="font-weight:bold" bgcolor="#CCCCCC">
			<td colspan="13" align="center">GRAND TOTAL</td>
			<td><? echo sprintf("%d.%02d", abs((int)($greand_total_working_hr/60)), abs((int)($greand_total_working_hr%60))); ?></td>
			<td><? echo sprintf("%d.%02d", abs((int)($greand_total_ot_min/60)), abs((int)($greand_total_ot_min%60))); ?></td>
			<td><? echo sprintf("%d.%02d", abs((int)($greand_total_e_ot/60)), abs((int)($greand_total_e_ot%60))); ?></td>	
			<td><? echo sprintf("%d.%02d", abs((int)($greand_total_hoot_min/60)), abs((int)($greand_total_hoot_min%60))); ?></td>
			<td><? echo sprintf("%d.%02d", abs((int)($greand_total_total_ot_min/60)), abs((int)($greand_total_total_ot_min%60))); ?></td>
			<td><? echo sprintf("%d.%02d", abs((int)($greand_total_total_working_hr/60)), abs((int)($greand_total_total_working_hr%60))); ?></td>
            <td align="right"><? echo $greand_total_gross_salary;  ?></td>
            <td align="right"><? echo $greand_total_festival_bonus;  ?></td>
			<td align="right"><? echo $greand_total_atten_bonus; ?></td>
			<td align="right"><? echo $greand_total_total_bonus; ?></td>	
			<td align="right"><? echo $greand_total_ot_amount; ?></td>
			<td align="right"><? echo $greand_total_e_ot_amount; ?></td>
			<td align="right"><? echo $greand_total_hoot_amount; ?></td>
            <td align="right"><? echo $greand_total_total_ot_amount; ?></td>
			<td align="right"><? echo $greand_total_total_amount; ?></td>                              
		</tr>
    </table>
    <br /><br />
 
	<?
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	//echo "$html"."####"."$name";
	echo "$html"."####"."$name"."####".$samary;		
	exit();
	//$samary
}


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}

// my_old
function my_old($dob)
{
	$now = date('d-m-Y');
	$dob = explode('-', $dob);
	$now = explode('-', $now);
	$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
	if($now[0] < $dob[0]){
		$now[0] += $mnt[$now[1]-1];
		$now[1]--;
	}
	if($now[1] < $dob[1]){
		$now[1] += 12;
		$now[2]--;
	}
	if($now[2] < $dob[2]) return false;
	return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
}

		
function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
	
	
?>