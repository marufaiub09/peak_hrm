<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($location_details);
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


if($action=='time_wise_ot_report'){
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}
	
	list( $day, $month, $year ) = explode( "-", $txt_fromdate );
	$fromdate = $year . "-" . $month . "-" . $day;
	/*list( $day, $month, $year ) = explode( "-", $txt_todate );
	$todate = $year . "-" . $month . "-" . $day;
	$get_days=datediff(d,$fromdate,$todate);
	$table_width=250+($get_days*70);*/
	
	if($table_width<900) $table_width=900;
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
	if ($section_id==0) $section_id_con=""; else $section_id_con="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($department_id==0) $department_id=""; else	$department_id="and department_id='$department_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	
	//new
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="company_id,location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="company_id,location_id,division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="company_id,location_id,division_id,department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="company_id,location_id,division_id,department_id,section_id,subsection_id,";}
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(id_card_no, 5)";}
			
	ob_start();	
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:<? echo $table_width; ?>">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1">Time Slot Wise O T Report</font>
    </div>
   			
    <?	
	$s=1;
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 Group BY section_name ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		 $section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
		 
		// echo "SELECT * FROM hrm_attendance WHERE section_id='$row[id]' and attnd_date='$fromdate' and total_over_time_min!='0'";
		$sql_attn = "SELECT * FROM hrm_attendance WHERE section_id='$row[id]' and attnd_date='$fromdate' and total_over_time_min!='0'";
		$result_attn = mysql_query( $sql_attn ) or die( $sql_attn . "<br />" . mysql_error() );
		
		//$arr_attn=array(); 
		$total_worker=0; $upto_second=0;$upto_third=0; $upto_fourth=0; $upto_fifth=0; $upto_six=0; $upto_seven=0; $upto_eight=0; $upto_nine=0;
		
		/*$ot_mins_cal_second=0; $new_tot_second=0;$ot_mins_second=0; $ot_second=0; $overtime_second=0;
		$ot_mins_cal_third=0; $new_tot=0;$ot_mins_cal=0; $ot=0; $overtime_third=0;
		$ot_mins_cal_fourth=0; $new_tot_fourth=0;$ot_mins_fourth=0; $ot_fourth=0; $overtime_fourth=0;
		$ot_mins_cal_fifth=0; $new_tot_fifth=0;$ot_mins_fifth=0; $ot_fifth=0; $overtime_fifth=0; 
		$ot_mins_cal_six=0; $new_tot_six=0;$ot_mins_six=0; $ot_six=0; $overtime_six=0;
		$ot_mins_cal_seven=0; $new_tot_seven=0;$ot_mins_seven=0; $ot_seven=0; $overtime_seven=0;
		$ot_mins_cal_eight=0; $new_tot_eight=0;$ot_mins_eight=0; $ot_eight=0; $overtime_eight=0;
		$ot_mins_cal_nine=0; $new_tot_nine=0;$ot_mins_nine=0; $ot_nine=0; $overtime_nine=0; */
		
		$ot_second=0; $overtime_second=0;
		$ot=0; $overtime_third=0;
		$ot_fourth=0; $overtime_fourth=0;
		$ot_fifth=0; $overtime_fifth=0; 
		$ot_six=0; $overtime_six=0;
		$ot_seven=0; $overtime_seven=0;
		$ot_eight=0; $overtime_eight=0;
		$ot_nine=0; $overtime_nine=0; 
		
		/*$overtime_second=0;$overtime_third=0;$overtime_fourth=0;$overtime_fourth=0;$overtime_fifth=0;$overtime_six=0;$overtime_seven=0;$overtime_eight=0;$overtime_nine=0;*/
		$total_worker_ot=0;$total_worker_ot_sum=0;
		
		$emp_amount_total=0; $emp_amount_total_third=0; $emp_amount_total_foutrh=0; $emp_amount_total_fifth=0; $emp_amount_total_six=0; $emp_amount_total_seven=0;$emp_amount_total_eight=0; $emp_amount_total_nine=0;
		
		while($att_row=mysql_fetch_array($result_attn))
		{
			if($att_row['total_over_time_min']!=0 && $att_row['total_over_time_min']<=120)
			{ 
				$upto_second++;
				$ot_mins_cal_second=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);					
				$new_tot_second=explode(".",$ot_mins_cal_second); $ot_mins_second=($new_tot_second[0]*60)+$new_tot_second[1]; $ot_second += $ot_mins_second;
				$overtime_second = sprintf("%d.%02d", abs((int)($ot_second/60)), abs((int)($ot_second%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary=$emp_salary['basic_salary'];
				}
				$emp_amount=($basic_salary/(104*60))*$ot_second;
				$emp_amount_total +=$emp_amount;
			}
			if($att_row['total_over_time_min']<=180 && $att_row['total_over_time_min']>120)
			{
				$upto_third++;
				$ot_mins_cal_third=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$new_tot=explode(".",$ot_mins_cal_third);$ot_mins_cal=($new_tot[0]*60)+$new_tot[1];	$ot += $ot_mins_cal;
				$overtime_third = sprintf("%d.%02d", abs((int)($ot/60)), abs((int)($ot%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary_third=$emp_salary['basic_salary'];
				}
				$emp_amount_third=($basic_salary_third/(104*60))*$ot;
				$emp_amount_total_third +=$emp_amount_third;
			}
			
			if($att_row['total_over_time_min']<=240 && $att_row['total_over_time_min']>180)
			{ 
				$upto_fourth++;
				$ot_mins_cal_fourth=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$new_tot_fourth=explode(".",$ot_mins_cal_fourth); $ot_mins_fourth=($new_tot_fourth[0]*60)+$new_tot_fourth[1]; $ot_fourth += $ot_mins_fourth;
				$overtime_fourth = sprintf("%d.%02d", abs((int)($ot_fourth/60)), abs((int)($ot_fourth%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary_fourth=$emp_salary['basic_salary'];
				}
				$emp_amount_fourth=($basic_salary_fourth/(104*60))*$ot_fourth;
				$emp_amount_total_foutrh +=$emp_amount_fourth;
			}
			if($att_row['total_over_time_min']<=300 && $att_row['total_over_time_min']>240)
			{
				$upto_fifth++;
				$ot_mins_cal_fifth=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$new_tot_fifth=explode(".",$ot_mins_cal_fifth); $ot_mins_fifth=($new_tot_fifth[0]*60)+$new_tot_fifth[1]; $ot_fifth += $ot_mins_fifth;
				$overtime_fifth = sprintf("%d.%02d", abs((int)($ot_fifth/60)), abs((int)($ot_fifth%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary_fifth=$emp_salary['basic_salary'];
				}
				$emp_amount_fifth=($basic_salary_fifth/(104*60))*$ot_fifth;
				$emp_amount_total_fifth +=$emp_amount_fifth;
			}
			if($att_row['total_over_time_min']<=360 && $att_row['total_over_time_min']>300)
			{
				$upto_six++;
				$ot_mins_cal_six=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$new_tot_six=explode(".",$ot_mins_cal_six); $ot_mins_six=($new_tot_six[0]*60)+$new_tot_six[1]; $ot_six += $ot_mins_six;
				$overtime_six = sprintf("%d.%02d", abs((int)($ot_six/60)), abs((int)($ot_six%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary_six=$emp_salary['basic_salary'];
				}
				$emp_amount_six=($basic_salary_six/(104*60))*$ot_six;
				$emp_amount_total_six +=$emp_amount_six;
			}
			if($att_row['total_over_time_min']<=420 && $att_row['total_over_time_min']>360)
			{
				$upto_seven++;
				$ot_mins_cal_seven=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$new_tot_seven=explode(".",$ot_mins_cal_seven); $ot_mins_seven=($new_tot_seven[0]*60)+$new_tot_seven[1]; $ot_seven += $ot_mins_seven;
				$overtime_seven = sprintf("%d.%02d", abs((int)($ot_seven/60)), abs((int)($ot_seven%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary_seven=$emp_salary['basic_salary'];
				}
				$emp_amount_seven=($basic_salary_seven/(104*60))*$ot_seven;
				$emp_amount_total_seven +=$emp_amount_seven;
			}
			if($att_row['total_over_time_min']<=480 && $att_row['total_over_time_min']>420)
			{ 
				$upto_eight++;
				$ot_mins_cal_eight=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$new_tot_eight=explode(".",$ot_mins_cal_eight); $ot_mins_eight=($new_tot_eight[0]*60)+$new_tot_eight[1]; $ot_eight += $ot_mins_eight;
				$overtime_eight = sprintf("%d.%02d", abs((int)($ot_eight/60)), abs((int)($ot_eight%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary_eight=$emp_salary['basic_salary'];
				}
				$emp_amount_eight=($basic_salary_eight/(104*60))*$ot_eight;
				$emp_amount_total_eight +=$emp_amount_eight;
			}
			if($att_row['total_over_time_min']>480)
			{
				$upto_nine++;
				$ot_mins_cal_nine=get_buyer_ot_hr( $att_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				$new_tot_nine=explode(".",$ot_mins_cal_nine); $ot_mins_nine=($new_tot_nine[0]*60)+$new_tot_nine[1]; $ot_nine += $ot_mins_nine;
				$overtime_nine = sprintf("%d.%02d", abs((int)($ot_nine/60)), abs((int)($ot_nine%60)));
				
				$sql_salary="SELECT id,basic_salary FROM hrm_salary_mst WHERE emp_code='$att_row[emp_code]'";
				$result_salary=mysql_query($sql_salary) or die( $sql_salary . "<br />" . mysql_error() );
				while($emp_salary=mysql_fetch_assoc($result_salary))
				{
					$basic_salary_nine=$emp_salary['basic_salary'];
				}
				$emp_amount_nine=($basic_salary_nine/(104*60))*$ot_nine;
				$emp_amount_total_nine +=$emp_amount_nine;
			}
			//if($att_row['total_over_time_min']>$buyer_ot_min_comp)$att_row['total_over_time_min']=$att_row['total_over_time_min']-$buyer_ot_min_comp;
			$total_worker++;
			//$arr_attn[$att_row['attnd_date']]['total_over_time_min']=$total_hours;
			$total_worker_ot=$ot_second+$ot+$ot_fourth+$ot_fifth+$ot_six+$ot_seven+$ot_eight+$ot_nine;
			$total_worker_ot_sum = sprintf("%d.%02d", abs((int)($total_worker_ot/60)), abs((int)($total_worker_ot%60)));
			
			$total_amount=$emp_amount_total+$emp_amount_total_third+$emp_amount_total_foutrh+$emp_amount_total_fifth+$emp_amount_total_six+$emp_amount_total_seven+$emp_amount_total_eight+$emp_amount_total_nine;
		}
		
	?>
     <table cellpadding="0" cellspacing="0" border="1" width="1100px" class="rpt_table" style="font-size:10px; border:1px solid #000000"> 
       <thead>        
            <tr>
            	<th width="25"><b>SL</b></th>
                <th width="100"><strong><b>SECTION</b></strong></th>
                <th width="100"><b>Total Worker</b></th>
                <th align="100">5.00 TO 7.00 <br> Up To 2 Hour</th>
                <th align="100">7.00 TO 8.00 <br> Up To 3 Hour</th>
                <th width="100">8.00 TO 9.00 <br> Up To 4 Hour</th>
                <th width="100">9.00 TO 10.00 <br> Up To 5 Hour</th>
                <th width="100">10.00 TO 11.00 <br> Up To 6 Hour</th>
                <th width="100">11.00 TO 12.00 <br> Up To 7 Hour</th>
                <th align="100">12.00 TO 01.00 <br> Up To 8 Hour</th>
                <th align="100"><b>01.00 TO ++</b></th>
                <th width="70" >OT Amount</th>
            </tr>
       </thead>
      
            <tr>
                <td><b><? echo $s; ?></b></td>
                <td rowspan="2" align="center"><b><? echo $section_details[$row['id']]; //$section_details[$section_id]['section_name'];  //?></b></td>
                <td align="center"> <? echo $total_worker; ?></td>
                <td align="center"> <? echo $upto_second; ?></td>
                <td align="center"> <? echo $upto_third; ?> </td>
                <td align="center"><? echo $upto_fourth; ?></td>
                <td align="center"><? echo $upto_fifth; ?></td>
                <td align="center"><? echo $upto_six; ?></td>
                <td align="center"><? echo $upto_seven; ?></td>
                <td align="center"><? echo $upto_eight; ?></td>
                <td align="center"><? echo $upto_nine; ?></td>
                <td align="center"></td>
            </tr>
            <tr>
                <td></td>
                <td align="center"><? echo $total_worker_ot_sum."=".$total_worker_ot; ?></td>
                <td align="center"><? echo $overtime_second."=".$ot_second; //."=".$emp_amount_total; ?></td>
                <td align="center"><? echo $overtime_third."=".$ot; //."=".$emp_amount_total_third;?></td>
                <td align="center"><? echo $overtime_fourth."=".$ot_fourth; //."=".$emp_amount_total_foutrh; ?></td>
                <td align="center"><? echo $overtime_fifth."=".$ot_fifth; //."=".$emp_amount_total_fifth;?></td>
                <td align="center"><? echo $overtime_six."=".$ot_six; //."=".$emp_amount_total_six; ?></td>
                <td align="center"><? echo $overtime_seven."=".$ot_seven; //."=".$emp_amount_total_seven;?></td>
                <td align="center"><? echo $overtime_eight."=".$ot_eight; //."=".$emp_amount_total_eight; ?></td>
                <td align="center"><? echo $overtime_nine."=".$ot_nine; //."=".$emp_amount_total_nine; ?></td>
                <td align="center"><? echo round($total_amount, 2); ?></td>
            </tr> 
        </table>
       <br> 
	<?
	$s++;
	}
	
?>
	
<?	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}

?>