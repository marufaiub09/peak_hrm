<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

//===========================================================================================================================================
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}
//=========================================================================================================================================


$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

if($action=='line_wise_ot')
{
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	
	
	
	$company_id=$cbo_company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else  $department_id="and a.department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id in ($designation_id)";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and b.salary_type_entitled='$cbo_salary_based'";
	
	/*
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="b.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="b.company_id,b.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="b.company_id,b.location_id,b.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="b.company_id,b.location_id,b.division_id,b.department_id,b.section_id,b.subsection_id,";}
	
	$groupby = substr($groupby, 0, -1);
	*/	
	//variable settings
	/*
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
	}
	*/
	ob_start();	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		$ot_fraction = $res['allow_ot_fraction'];
		$ot_start_min = $res['ot_start_minute'];
		$one_hr_ot_unit = $res['one_hour_ot_unit'];
		$adjust_out_time=$res['adjust_out_time'];
		$applicable_above_salary=$res['applicable_above_salary'];
		$in_out_time_format=$res['in_out_time_format'];
	}
	?>
    <style>
    .verticalText 
	{               
  		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
		-o-transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		transform: rotate(270deg);
		height:10px;
		width:10px;
		margin-top:40px;
  	}
    </style>
    
    <div align="center">
    	<font size="+1"><? echo $company_details[$company_id]; ?></font><br />
        <font size="-1">Employee OT Summary (Subsection Wise)</font>
    </div>    
	<table cellpadding="0" cellspacing="0" border="1" width="<? echo ($get_days*50)+250; ?>" class="rpt_table" style="border:1px solid #000; font-size:10px">       
    	<thead>										
            <tr>
                <th width="30"><b>SL</b></th>
                <th width="70">&nbsp;<b>Line</b></th>
                <?
				for($j=0;$j<$get_days;$j++)
				{				
					$newdate = date("Y-m-d",strtotime("+".$j." day", strtotime($date_part)));
					$sql_part .= "SUM(CASE WHEN b.attnd_date = '".$newdate."' THEN b.total_over_time_min else 0 END ) as ot".$j.",";
					?>
					<th  width="2" style="padding:10px 10px;">
					<div class="verticalText" style="font-size:9px;" ><? echo convert_to_mysql_date($newdate); ?></div>
					</th>
					<?	
				}
				$sql_part = substr($sql_part, 0, -1); 
                ?>
                 <th width="50"><b>Total</b></th>
			</tr>
		</thead>
		<?	
		/*
		$attn_sql = "SELECT section_id,total_over_time_min,attnd_date,subsection_id				
						FROM hrm_attendance
						WHERE 
						attnd_date like '%$date_part%'
						$cbo_company_id 
						$category 
						$department_id 
						$location_id 
						$division_id
						$section_id 
						$designation_id
						order by attnd_date,subsection_id ASC ";
		*/
		
		$attn_sql = "SELECT DISTINCT a.emp_code, a.company_id,a.location_id,a.division_id,a.department_id, a.section_id, a.subsection_id, a.designation_id, a.total_over_time_min, a.attnd_date,b.id,b.category				
					FROM hrm_attendance a,hrm_employee b
					WHERE a.emp_code=b.emp_code and 
					a.attnd_date like '%$date_part%'
					$cbo_company_id 
					$category 
					$department_id 
					$location_id 
					$division_id
					$section_id
					$subsection_id 
					$designation_id
					$salary_based
				    order by a.attnd_date,a.subsection_id ASC ";
								
		//echo $attn_sql;
		$results = mysql_query( $attn_sql ) or die( $attn_sql . "<br />" . mysql_error() );
		//$company_arr=array();$location_arr=array();$division_arr=array();$department_arr=array();$section_arr=array();$subsection_arr=array();
		$i=1;
		$new_section = array();
		$new_date = array();
		$ot_arr = array();			
		if(mysql_num_rows($results)>0){
			$arrange_ot_arr=array();
			$location_ot_arr=array();
			while($ot_row=mysql_fetch_assoc($results)){
				
				/*$hours[0] = abs((int)($ot_row['total_over_time_min']/60)); //hours
				$hours[1] = abs((int)($ot_row['total_over_time_min']%60)); //minutes
				$ot_mins_cal=0;
				if($ot_fraction==1) //ot fraction allow
				{						
					if($hours[1]>=$ot_start_min && $hours[1]<$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+30;}
					else if($hours[1]>=$one_hr_ot_unit){$ot_mins_cal = ($hours[0]*60)+60;}
					else $ot_mins_cal = $hours[0]*60;
				}
				if($ot_fraction==2) // no ot fraction
				{						
					if($hours[1]>=$one_hr_ot_unit){	$ot_mins_cal = ($hours[0]*60)+60;}
					else $ot_mins_cal = $hours[0]*60;						
				}
				if($ot_fraction==3) //at actual minutes
				{						
					$ot_mins_cal = 	$ot_row['total_over_time_min'];				
				}*/
				
				$total_hours_mints= get_buyer_ot_hr($ot_row['total_over_time_min'],$one_hr_ot_unit,$ot_fraction,$ot_start_min,1440);
				
				//if(abs((int)($ot_mins_cal%60))==3) $ot_adjust_hr=abs((int)($ot_mins_cal/60)).".50";else $ot_adjust_hr=abs((int)($ot_mins_cal/60));
				//$total_hours = sprintf("%d", abs((int)($ot_mins_cal/60)));
				//$total_mints = sprintf("%02d",abs((int)($ot_mins_cal%60)));
				//if($total_mints==30) $total_hours_mints = $total_hours.".50"; else $total_hours_mints = $total_hours.".".$total_mints;
				//$ot_row['total_over_time_min'] = $total_hours_mints;
				//$arrange_ot_arr = $ot_row;
				
				if( in_array($ot_row['subsection_id'],$new_section) )
				{
					if( in_array($ot_row['attnd_date'],$new_date) )
					{
						$arrange_ot_arr[$ot_row['subsection_id']][$ot_row['attnd_date']]+=$total_hours_mints;
					}
					else
					{
						$arrange_ot_arr[$ot_row['subsection_id']][$ot_row['attnd_date']]=$total_hours_mints;
						$new_date[$ot_row['attnd_date']]=$ot_row['attnd_date'];
					}
				}
				else
				{
					$new_section[$ot_row['subsection_id']]=$ot_row['subsection_id'];
					if( in_array($ot_row['attnd_date'],$new_date) )
					{
						$arrange_ot_arr[$ot_row['subsection_id']][$ot_row['attnd_date']]+=$total_hours_mints;
					}
					else
					{
						$arrange_ot_arr[$ot_row['subsection_id']][$ot_row['attnd_date']]=$total_hours_mints;
						$new_date[$ot_row['attnd_date']]=$ot_row['attnd_date'];
					}
				}
			}
			
			}
			//print_r($arrange_ot_arr);die;
			
			foreach( $arrange_ot_arr as $key=>$val )
			{
					if($i%2==0) $bgcolor='#FFFFFF';
					else $bgcolor='#EEEEEE'; 		 
					
					?>    
						<tr bgcolor="<? echo $bgcolor; ?>">
							<td><? echo $i++; ?></td>
							<td>&nbsp;<? echo $subsection_details[$key]; ?></td>
					<?									
							$total_ot_min=0;
							$j=1;
							for($j=1;$j<=$get_days;$j++)
							{
								if(strlen($j)<2) $cdate = $date_part."-0".$j;
								else $cdate = $date_part."-".$j;
								
						?>        					
								<td align="right"><? echo $val[$cdate];?></td> 
                                
						<?
								$total_ot_min += $val[$cdate];
								$grand_total_on_date[$j] += $val[$cdate];;
							}
					?>    
							<td align="right"><? echo $total_ot_min; ?></td>              	
						</tr>  
					<?	
				
			}
			
    ?>
    
    		<tfoot class="tbl_bottom">
                <td align="center" colspan="2">Total</td>
                <? for($h=1;$h<=$get_days;$h++){ ?>
                    <td align="center"><b><? echo $grand_total_on_date[$h];?></b></td>
              <? } ?>
          </tfoot>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();
	
		
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
}

//month generated
if($type=="select_month_generate")
{		
	
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$explode_val=explode(" ",$row["starting_date"]);
		//$value = $explode_val[0];
		$value = substr($explode_val[0],0,-3);//new line for new year
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To 00".$row[ending_date]."</option>";
	}		
	exit();
}
?>