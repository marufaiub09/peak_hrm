<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}



if( $action == "daily_attn_summary_report" ) {	//Daily Attendance Summary Report	
	
	$company = $company_id;
	if ($company_id==0) $company_id=""; else $company_id="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and e.category='$cbo_emp_category'";
	if ($department_id==0) $department_id="";else $department_id="and a.department_id='$department_id'";	
						
	$date_part=convert_to_mysql_date($txt_date);
	$html .="<div style=\"font-size:12px; width:750px;\">";
	$addrs ="";		
	if($company_details[$company]['plot_no'] !='') $addrs .= $company_details[$company]['plot_no'].", "; 
	if($company_details[$company]['level_no'] !='') $addrs .= $company_details[$company]['level_no'].", "; 
	if($company_details[$company]['block_no'] !='') $addrs .= $company_details[$company]['block_no'].", "; 
	if($company_details[$company]['road_no'] !='') $addrs .= $company_details[$company]['road_no'].", "; 
	if($company_details[$company]['city'] !='') $addrs .= $company_details[$company]['city']." "; 
	$html .= "<div align=\"center\" style=\"width:750px; font-size:12px\"><b>". $company_details[$company]['company_name']."</b></div>";
	$html .= "<div align=\"center\" style=\"width:750px; font-size:10px\"><b>Address : ". $addrs."</b></div>";
	$html .= "<div align=\"center\" style=\"width:750px; font-size:10px\"><b>Attendance Summary Report On Date : ". $txt_date."</b></div>";
	$html .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"1\" style=\"border:1px solid black;font-size:12px;\" class=\"rpt_table\" rules=\"all\">";
												
	$i=0;	
	$new_location=array();
	$new_division=array();
	$new_department=array();
	$new_section=array();		
	
	
	$div_total_new_join=0;$div_total_new_join=0;$div_total_manpower_dep=0;$div_total_movement=0;
	$div_total_present=0;$div_total_late=0;$div_total_absent=0;$div_total_total_leave=0;$div_total_new_join=0;
	
	$loc_total_new_join=0;$loc_total_manpower_dep=0;$loc_total_movement=0;
	$loc_total_present=0;$loc_total_late=0;$loc_total_absent=0;$loc_total_total_leave=0;
	
	$grand_total_new_join = 0;					
	$grand_total_manpower_dep = 0;
	$grand_total_movement = 0;
	$grand_total_present = 0;

	$grand_total_late = 0;
	$grand_total_absent = 0;
	$grand_total_total_leave = 0;
		
		$sql = "SELECT 
				a.location_id,a.division_id,a.department_id,a.designation_id,e.joining_date 
			FROM 
				hrm_employee as e LEFT JOIN hrm_attendance as a ON e.emp_code=a.emp_code 
			WHERE 
				a.attnd_date like '$date_part%' 
				$company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $category 
				group by a.location_id,a.division_id,a.department_id,a.designation_id
				order by a.location_id,a.division_id,a.department_id,a.designation_id,e.designation_level";				
 
 	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$num_rows=mysql_num_rows($result);
	while( $row = mysql_fetch_assoc( $result ) ) 
	{		
			
			if($i==0)
			{
				$location_name_html ="<b>Location Name: ".$location_details[$row['location_id']]."</b>"; 
									
				$division_name_html = "<b>Division Name: ".$division_details[$row['division_id']]."</b>";
				
				$new_division[$row[division_id]]=$row[division_id];	
				$new_location[$row[location_id]]=$row[location_id];			
			}
			else
			{
				$location_name_html="";
				$division_name_html="";
			}
			
			if(in_array($row[department_id],$new_department))
			{
				$holiday=0;$weekend=0;$total_leave=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_manpower=0;$new_join=0;
				$i++;
			}
			else
			{
					
				$new_department[$row[department_id]] = $row[department_id];
				
				if($i!=0) // department total and percentage rows print
				{
					$html .= "<tr class=\"tbl_bottom\">
								<td width=''><b>Total</b></td>
								<td width=''>&nbsp;</td>
								<td width='' align='right'>".$total_new_join."</td>						
								<td width='' align='right'>".$total_manpower_dep."</td>
								<td width='' align='right'>".$total_movement."</td>
								<td width='' align='right'>".$total_present."</td>						
								<td width='' align='right'>".$total_late."</td> 	
								<td width='' align='right'>".$total_absent."</td>
								<td width='' align='right'>".$total_total_leave."</td>
								<td width=''>&nbsp;</td>	
								<td width=''>&nbsp;</td>
							</tr>
							<tr>
								<td width=''><b>(In %)</b></td>
								<td width=''>&nbsp;</td>						
								<td width=''>&nbsp;</td>
								<td width=''>&nbsp;</td>
								<td width='' align='right'>".round(($total_movement*100)/$total_manpower_dep,2)."%</td>
								<td width='' align='right'>".round(($total_present*100)/$total_manpower_dep,2)."%</td>						
								<td width='' align='right'>".round(($total_late*100)/$total_manpower_dep,2)."%</td> 	
								<td width='' align='right'>".round(($total_absent*100)/$total_manpower_dep,2)."%</td>
								<td width='' align='right'>".round(($total_total_leave*100)/$total_manpower_dep,2)."%</td>
								<td width=''>&nbsp;</td>	
								<td width=''>&nbsp;</td>
							</tr>";
				
				
					if(!in_array($row[division_id],$new_division))
					{
						
						if($i!=0) // sub total and percentage rows print
						{
							$html_division = "<tr class=\"tbl_bottom\">
										<td><b>Division Total</b></td>
										<td width=''>&nbsp;</td>
										<td align='right'>".$div_total_new_join."</td>						
										<td align='right'>".$div_total_manpower_dep."</td>
										<td align='right'>".$div_total_movement."</td>
										<td align='right'>".$div_total_present."</td>						
										<td align='right'>".$div_total_late."</td> 	
										<td align='right'>".$div_total_absent."</td>
										<td align='right'>".$div_total_total_leave."</td>	
										<td>&nbsp;</td>
										<td width=''>&nbsp;</td>
									</tr>
									<tr>
										<td><b>(In %)</b></td>
										<td width=''>&nbsp;</td>						
										<td width=''>&nbsp;</td>
										<td width=''>&nbsp;</td>
										<td align='right'>".round(($div_total_movement*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_present*100)/$div_total_manpower_dep,2)."%</td>						
										<td align='right'>".round(($div_total_late*100)/$div_total_manpower_dep,2)."%</td> 	
										<td align='right'>".round(($div_total_absent*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_total_leave*100)/$div_total_manpower_dep,2)."%</td>	
										<td width=''>&nbsp;</td>
										<td width=''>&nbsp;</td>
									</tr>";
						
							/*$division_name_html = "<tr class=\"tbl_top\">
									<td colspan=11><b>Division Name: ".$division_details[$row['division_id']]."</b></td>
								</tr>";new*/
								$division_name_html = "<b>Division Name: ".$division_details[$row['division_id']]."</b>";	
						
						}
						
 						$new_division[$row[division_id]] = $row[division_id];
						$new_department=array();
						$new_department[$row[department_id]] = $row[department_id];
						
						$div_total_new_join=0;$div_total_manpower_dep=0;$div_total_movement=0;
						$div_total_present=0;$div_total_late=0;$div_total_absent=0;$div_total_total_leave=0;
								
					}else {
						$division_name_html="";$html_division="";
						}
					$html .= $html_division;
					//division end if condition
				
				
					if(!in_array($row[location_id],$new_location))
					{
						
						if($i!=0) // sub total and percentage rows print
						{
							
							if($html_division=="")
							{
								$html .= "<tr class=\"tbl_bottom\">
										<td><b>Division Total</b></td>
										<td width=''>&nbsp;</td>
										<td align='right'>".$div_total_new_join."</td>						
										<td align='right'>".$div_total_manpower_dep."</td>
										<td align='right'>".$div_total_movement."</td>
										<td align='right'>".$div_total_present."</td>						
										<td align='right'>".$div_total_late."</td> 	
										<td align='right'>".$div_total_absent."</td>
										<td align='right'>".$div_total_total_leave."</td>	
										<td>&nbsp;</td>
										<td width=''>&nbsp;</td>
									</tr>
									<tr>
										<td><b>(In %)</b></td>
										<td width=''>&nbsp;</td>						
										<td width=''>&nbsp;</td>
										<td align='right'>".round(($div_total_movement*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_present*100)/$div_total_manpower_dep,2)."%</td>						
										<td align='right'>".round(($div_total_late*100)/$div_total_manpower_dep,2)."%</td> 	
										<td align='right'>".round(($div_total_absent*100)/$div_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($div_total_total_leave*100)/$div_total_manpower_dep,2)."%</td>	
										<td width=''>&nbsp;</td>
										<td width=''>&nbsp;</td>
									</tr>";
								$div_total_new_join=0;$div_total_manpower_dep=0;$div_total_movement=0;
								$div_total_present=0;$div_total_late=0;$div_total_absent=0;$div_total_total_leave=0;
							}
							
							
							$html .= "<tr class=\"tbl_bottom\">
										<td><b>Location Total</b></td>
										<td width=''>&nbsp;</td>
										<td align='right'>".$loc_total_new_join."</td>						
										<td align='right'>".$loc_total_manpower_dep."</td>
										<td align='right'>".$loc_total_movement."</td>
										<td align='right'>".$loc_total_present."</td>						
										<td align='right'>".$loc_total_late."</td> 	
										<td align='right'>".$loc_total_absent."</td>
										<td align='right'>".$loc_total_total_leave."</td>	
										<td>&nbsp;</td>
										<td width=''>&nbsp;</td>
									</tr>
									<tr>
										<td><b>(In %)</b></td>
										<td width=''>&nbsp;</td>						
										<td width=''>&nbsp;</td>
										<td align='right'>".round(($loc_total_movement*100)/$loc_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($loc_total_present*100)/$loc_total_manpower_dep,2)."%</td>						
										<td align='right'>".round(($loc_total_late*100)/$loc_total_manpower_dep,2)."%</td> 	
										<td align='right'>".round(($loc_total_absent*100)/$loc_total_manpower_dep,2)."%</td>
										<td align='right'>".round(($loc_total_total_leave*100)/$loc_total_manpower_dep,2)."%</td>	
										<td width=''>&nbsp;</td>
										<td width=''>&nbsp;</td>
									</tr>";
						
							/*$location_name_html = "<tr class=\"tbl_top\">
									<td colspan=11><b>Location Name: ".$location_details[$row['location_id']]."</b></td>
								</tr>";new*/
							$location_name_html ="<b>Location Name: ".$location_details[$row['location_id']]."</b>"; 
								
						}
						
  						$new_location[$row[location_id]] = $row[location_id];		
						$new_division = array();
						$new_division[$row[division_id]] = $row[division_id];
						/*$division_name_html = "<tr class=\"tbl_top\">
									<td colspan=11><b>Division Name: ".$division_details[$row['division_id']]."</b></td>
								</tr>";	new*/
						$division_name_html = "<b>Division Name: ".$division_details[$row['division_id']]."</b>";	
						
						$loc_total_new_join=0;$loc_total_manpower_dep=0;$loc_total_movement=0;
						$loc_total_present=0;$loc_total_late=0;$loc_total_absent=0;$loc_total_total_leave=0;
					
					}else $location_name_html="";
					//end location if condition
				
				
				}
				
				//$html .="<tr bgcolor=#F0F0B8  class=\"tbl_top\">"."<td colspan=11>".$location_name_html."&nbsp;".$division_name_html."</td></tr>"."<tr bgcolor=#F0F0B8  class=\"tbl_top\"><td colspan=11><b>Department Name: ".$department_details[$row['department_id']]."</b>"."</td>"."</tr>";
								
			if(trim($location_name_html)!="" || trim($division_name_html)!=""){
				
					$html .="<tr bgcolor=#F0F0B8  class=\"tbl_top\">"."<td colspan=11>".$location_name_html."&nbsp;".$division_name_html."</td></tr>";	
				}
				
			if($department_details[$row['department_id']]!=""){
					
			$html .="<tr bgcolor=#F0F0B8  class=\"tbl_top\"><td colspan=11><b>Department Name: ".$department_details[$row['department_id']]."</b>"."</td>"."</tr>";
					
				}
				
				//header print here 
				if($i==0) 
				{
					
					$html .= "<thead>
								<th width='130'><b>Designation</b></th>
								<th width='70'><b>Sanction</b></th>
								<th width='100'><b>Newly Joined</b></th>
								<th width='70'><b>Manpower</b></th>
								<th width='70'><b>Movement</b></th>
								<th width='70'><b>Present</b></th>
								<th width='70'><b>Late</b></th>
								<th width='70'><b>Absent</b></th>
								<th width='70'><b>Leave</b></th>
								<th width='130'><b>Short/Surplus</b></th>
								<th width='100'><b>Remarks</b></th>
							</thead>
							<tr>
								<td colspan=11></td>
							</tr>";	
				}
									
				$total_new_join=0;$grand_total=0;$total_new_join=0;$total_manpower_dep=0;$total_movement=0;
				$total_present=0;$total_late=0;$total_absent=0;$total_total_leave=0;		
				$holiday=0;$weekend=0;$total_leave=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_manpower=0;$new_join=0;
				$i++;
				
				
 			} // end in_array condition else
						
				
			$sqls = "SELECT * from hrm_attendance where designation_id=$row[designation_id] and department_id=$row[department_id] and division_id=$row[division_id] and location_id=$row[location_id] and attnd_date='$date_part'";
			//echo $sqls;
			$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
			while( $rows = mysql_fetch_assoc( $results ) ) 
			{			
				
				if($rows[status]=='CL'){$cl++;}
				if($rows[status]=='SL'){$sl++;}
				if($rows[status]=='ML'){$ml++;}
				if($rows[status]=='EL'){$el++;}
				if($rows[status]=='LWP'){$lwp++;}
				if($rows[status]=='SpL'){$spl++;}
				if($rows[status]=='H' || $rows[status]=='GH' || $rows[status]=='FH' || $rows[status]=='CH' ){$holiday++;}
				if($rows[status]=='W'){$weekend++;}
				if($rows[status]=='P'){$present++;}							
				if($rows[status]=='D'){$late++;}
				if($rows[status]=='MR'){$movement++;}
				if($rows[status]=='A'){$absent++;}
				$total_manpower++;
				
				//$joining_date=return_field_value("joining_date","hrm_employee","emp_code='$rows[emp_code]'");			
				if($rows[joining_date]==$txt_date){$new_join++;}					
			 }
			
			if($i%2==0) $bgcolor='#FFFFFF';
			else $bgcolor='#EEEEEE';
			
			$total_leave=$cl+$sl+$ml+$el+$lwp+$spl;						
			$html .= "<tr height=\"20\" bgcolor=$bgcolor>
						<td width=''>".$designation_chart[$row[designation_id]]."</td>
						<td width=''>&nbsp;</td>
						<td width='' align='right'>".$new_join."</td>						
						<td width='' align='right'>".$total_manpower."</td>
						<td width='' align='right'>".$movement."</td>
						<td width='' align='right'>".$present."</td>						
						<td width='' align='right'>".$late."</td> 	
						<td width='' align='right'>".$absent."</td>
						<td width='' align='right'>".$total_leave."</td>
						<td width=''>&nbsp;</td>	
						<td width=''>&nbsp;</td>
					</tr>";
		
				//department wise total sum and percentage---//
				//total department wise here 
				$total_new_join += $new_join;					
				$total_manpower_dep += $total_manpower;
				$total_movement += $movement;
				$total_present += $present;
				$total_late += $late;
				$total_absent += $absent;
				$total_total_leave += $total_leave;				
				//$grand_total=$total_new_join+$total_manpower_dep+$total_movement+$total_present+$total_late+$total_absent+$total_total_leave+$total_new_join;
		  
		  		//total division wise here 
				$div_total_new_join += $new_join;					
				$div_total_manpower_dep += $total_manpower;
				$div_total_movement += $movement;
				$div_total_present += $present;
				$div_total_late += $late;
				$div_total_absent += $absent;
				$div_total_total_leave += $total_leave;					
				
 				//total location wise here 
				$loc_total_new_join += $new_join;					
				$loc_total_manpower_dep += $total_manpower;
				$loc_total_movement += $movement;
				$loc_total_present += $present;
				$loc_total_late += $late;
				$loc_total_absent += $absent;
				$loc_total_total_leave += $total_leave;	
				
 				//company wise grand_total here last
				$grand_total_new_join += $new_join;					
				$grand_total_manpower_dep += $total_manpower;
				$grand_total_movement += $movement;
				$grand_total_present += $present;
				$grand_total_late += $late;
				$grand_total_absent += $absent;
				$grand_total_total_leave += $total_leave;				
	
	
	}
				
 				
				//department total
				$html .= "<tr class=\"tbl_bottom\">
							<td width=''><b>Total</b></td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'>".$total_new_join."</td>						
							<td width='' align='right'>".$total_manpower_dep."</td>
							<td width='' align='right'>".$total_movement."</td>
							<td width='' align='right'>".$total_present."</td>						
							<td width='' align='right'>".$total_late."</td> 	
							<td width='' align='right'>".$total_absent."</td>
							<td width='' align='right'>".$total_total_leave."</td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr>
						<tr>
							<td width=''><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td width='' align='right'>".round(($total_movement*100)/$total_manpower_dep,2)."%</td>
							<td width='' align='right'>".round(($total_present*100)/$total_manpower_dep,2)."%</td>						
							<td width='' align='right'>".round(($total_late*100)/$total_manpower_dep,2)."%</td> 	
							<td width='' align='right'>".round(($total_absent*100)/$total_manpower_dep,2)."%</td>
							<td width='' align='right'>".round(($total_total_leave*100)/$total_manpower_dep,2)."%</td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr>";
		
		if($num_rows>0) //division total
		{		
				$html .= "<tr class=\"tbl_bottom\">
							<td><b>Division Total</b></td>
							<td width=''>&nbsp;</td>
							<td align='right'>".$div_total_new_join."</td>						
							<td align='right'>".$div_total_manpower_dep."</td>
							<td align='right'>".$div_total_movement."</td>
							<td align='right'>".$div_total_present."</td>						
							<td align='right'>".$div_total_late."</td> 	
							<td align='right'>".$div_total_absent."</td>
							<td align='right'>".$div_total_total_leave."</td>	
							<td>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr>
						<tr>
							<td><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td align='right'>".round(($div_total_movement*100)/$div_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($div_total_present*100)/$div_total_manpower_dep,2)."%</td>						
							<td align='right'>".round(($div_total_late*100)/$div_total_manpower_dep,2)."%</td> 	
							<td align='right'>".round(($div_total_absent*100)/$div_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($div_total_total_leave*100)/$div_total_manpower_dep,2)."%</td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr>";
		}
		
		
		if($num_rows>0) //location total
		{		
				$html .= "<tr class=\"tbl_bottom\">
							<td><b>Location Total</b></td>
							<td width=''>&nbsp;</td>
							<td align='right'>".$loc_total_new_join."</td>						
							<td align='right'>".$loc_total_manpower_dep."</td>
							<td align='right'>".$loc_total_movement."</td>
							<td align='right'>".$loc_total_present."</td>						
							<td align='right'>".$loc_total_late."</td> 	
							<td align='right'>".$loc_total_absent."</td>
							<td align='right'>".$loc_total_total_leave."</td>	
							<td>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr>
						<tr>
							<td><b>(In %)</b></td>
							<td width=''>&nbsp;</td>						
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
							<td align='right'>".round(($loc_total_movement*100)/$loc_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($loc_total_present*100)/$loc_total_manpower_dep,2)."%</td>						
							<td align='right'>".round(($loc_total_late*100)/$loc_total_manpower_dep,2)."%</td> 	
							<td align='right'>".round(($loc_total_absent*100)/$loc_total_manpower_dep,2)."%</td>
							<td align='right'>".round(($loc_total_total_leave*100)/$loc_total_manpower_dep,2)."%</td>	
							<td width=''>&nbsp;</td>
							<td width=''>&nbsp;</td>
						</tr>";
		}
		
		if($num_rows>0) //company grand total
		{
			$html .= "<tr bgcolor=\"#CCCCCC\" class=\"tbl_bottom\">
						<td width=''><b>Company Total</b></td>
						<td width=''>&nbsp;</td>
						<td width='' align='right'>".$grand_total_new_join."</td>						
						<td width='' align='right'>".$grand_total_manpower_dep."</td>
						<td width='' align='right'>".$grand_total_movement."</td>
						<td width='' align='right'>".$grand_total_present."</td>						
						<td width='' align='right'>".$grand_total_late."</td> 	
						<td width='' align='right'>".$grand_total_absent."</td>
						<td width='' align='right'>".$grand_total_total_leave."</td>	
						<td width=''>&nbsp;</td>
						<td width=''>&nbsp;</td>
					</tr>
					<tr>
						<td width=''><b>(In %)</b></td>
						<td width=''>&nbsp;</td>						
						<td width=''>&nbsp;</td>
						<td width=''>&nbsp;</td>
						<td width='' align='right'>".round(($grand_total_movement*100)/$grand_total_manpower_dep,2)."%</td>
						<td width='' align='right'>".round(($grand_total_present*100)/$grand_total_manpower_dep,2)."%</td>						
						<td width='' align='right'>".round(($grand_total_late*100)/$grand_total_manpower_dep,2)."%</td> 	
						<td width='' align='right'>".round(($grand_total_absent*100)/$grand_total_manpower_dep,2)."%</td>
						<td width='' align='right'>".round(($grand_total_total_leave*100)/$grand_total_manpower_dep,2)."%</td>	
						<td width=''>&nbsp;</td>
						<td width=''>&nbsp;</td>
					</tr>";	
		}
	
	$html .="</table> </div>";
	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename) 
	{			
       @unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}


?>