<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	//$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 order by trim(custom_designation) ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}	
	
//month generated
if($type=="select_month_generate")
{		
	//echo $id;
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) {
		$explode_val=explode(" ",$row["starting_date"]);
		$value = $explode_val[0];
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}


if($action=='monthly_extra_ot_sheet'){
	
	//variable settings
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
			$ot_fraction = $res['allow_ot_fraction'];
			$ot_start_min = $res['ot_start_minute'];
			$one_hr_ot_unit = $res['one_hour_ot_unit'];
			$buyer_ot_min_comp = $res['first_ot_limit'];
	}
	
	$explode_date_part = explode("_",$cbo_month_selector);
	$date_part = $explode_date_part[0];
	//echo $date_part; 
	if ($cbo_salary_sheet=='') {$emp_status="";} 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1";} 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and id in (SELECT MAX(id)  from hrm_separation where separated_from like '$con_date' group by emp_code))";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and id in (SELECT MAX(id)  from hrm_separation where separated_from like '$con_date' group by emp_code))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and id in (SELECT MAX(id)  from hrm_separation where separated_from like '$con_date' group by emp_code))";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and id in (SELECT MAX(id)  from hrm_separation where separated_from like '$con_date' group by emp_code))";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and id in (SELECT MAX(id)  from hrm_separation where separated_from like '$con_date' group by emp_code))";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and id in (SELECT MAX(id)  from hrm_separation where separated_from like '$con_date' group by emp_code))";} //and separated_from like '$con_date'
	else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	
	
	
	if ($company_id==0) $cbo_company_id=""; else $cbo_company_id="and emp.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and emp.division_id in ($division_id)";
	if ($department_id==0) $department_id=""; else	$department_id="and emp.department_id in ($department_id)";
	if ($section_id==0) $section_id=""; else $section_id="and emp.section_id in ($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id in ($subsection_id)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id in ($designation_id)";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	if ($ot_type_id==0 || $ot_type_id==1) $ot_type =""; else $ot_type ="and a.e_over_time!=0.00";
	if ($cbo_salary_based=="") $salary_based=""; else $salary_based="and emp.salary_type_entitled='$cbo_salary_based'";
	
	
	$addrs ="";		
	if($company_details[$company_id]['plot_no'] !='') $addrs .= $company_details[$company_id]['plot_no'].", "; 
	if($company_details[$company_id]['level_no'] !='') $addrs .= $company_details[$company_id]['level_no'].", "; 
	if($company_details[$company_id]['block_no'] !='') $addrs .= $company_details[$company_id]['block_no'].", "; 
	if($company_details[$company_id]['road_no'] !='') $addrs .= $company_details[$company_id]['road_no'].", "; 
	if($company_details[$company_id]['city'] !='') $addrs .= $company_details[$company_id]['city'].", "; 
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="emp.emp_code,";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,emp.emp_code,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,emp.emp_code,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,emp.emp_code,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,emp.emp_code,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,emp.emp_code,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,emp.emp_code,";}
	
	//echo $order_by_id; die;	
	if($order_by_id==0){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".substr($groupby,0,-13)." CAST(emp.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".substr($groupby,0,-13)." CAST(right(trim(emp.id_card_no), 5)as SIGNED)";}	
	
	$dynamic_groupby = substr($groupby, 0, -1);//this line is used to remove last comma and dynamic group by	
	
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();

ob_start();	
	?>   
    <style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style> 
	<div align="center" style="width:800px">
    	<font size="+1"><? echo $company_details[$company_id]['company_name']; ?></font><br />
        <font size="-1"><? echo $addrs; ?></font><br />
        <font size="-1">Extra OT Sheet: <? echo date("F, Y",strtotime($date_part)); ?> </font>
    </div>
    <table cellpadding="0" cellspacing="0" width="860" class="rpt_table" style="font-size:12px; border:1px solid #000000" rules="all"> 
    	 <thead>
                    <tr class="tbl_top">
                    <th colspan="12"><? echo $cost_center; ?></th>
                    </tr>
                    <tr class="tbl_top">
                    <th colspan="6">General Information</th>
                    <th colspan="5">Extra Over Time</th> 
                    <th rowspan="2" width="100">Signature</th>                           
                    </tr>
                    <tr class="tbl_top">
                    <th width="30"><b>SL</b></th>
                    <th width="70"><b>ID Card</b></th>
                    <th width="70"><b>Emp Code</b></th>
                    <th align="100"><b>Name</b></th>
                    <th align="100"><b>Designation</b></th>
                    <th align="80"><b>DOJ</b></th>
                    <th align="60"><b>Gross</b></th>
                    <th align="60"><b>Basic</b></th>
                    <th align="60"><b>EOT Hrs</b></th>
                    <th align="60"><b>EOT Rate</b></th>
                    <th align="60"><b>EOT Amt</b></th>
                    </tr>
        </thead>
    <?
		
		$total_eot=0;$subsec_total_eot=0;$sec_total_eot=0;$deper_total_eot=0;$divi_total_eot=0; $location_total_eot=0; 
		
		$total_eot_amount=0;$subsec_total_eot_amount=0;$sec_total_eot_amount=0;$deper_total_eot_amount=0;$divi_total_eot_amount=0;$location_total_eot_amount=0;  
		
		$location_eot=0; $divi_eot=0; $deper_eot=0; $sec_eot=0; $subsec_eot=0;  			
		
		$sub_time=0; $sec_time=0; $dep_time=0; $div_time=0; $loc_time=0;	
				
		
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no,emp.joining_date,emp.designation_id,emp.designation_level,emp.punch_card_no,a.* 
				FROM 
				hrm_salary_mst a, hrm_employee emp 
				WHERE 
				emp.emp_code!='' and 
				a.salary_periods  like '$date_part' and 
				a.emp_code=emp.emp_code
				$cbo_company_id  
				$division_id
				$department_id 
				$location_id 
				$section_id 				
				$subsection_id 
				$designation_id 
				$category
				$emp_status
				$ot_type
				$salary_based
				group by 
				$dynamic_groupby
				$orderby";
				//echo $sql;die;
				//order by emp.section_id, CAST(emp.id_card_no as SIGNED)";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sl=0;
		$r=1;
		while($row=mysql_fetch_array($result))  //while loop start here================================
		{
			 $sl++;
			 if($sl==1)
			   {
					$location_arr[$row[location_id]]=$row[location_id];
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
			   }
			   
			   if($sl!=1)
				{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row[subsection_id]]=$row[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row[section_id]]=$row[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row[subsection_id]]=$row[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row[department_id]]=$row[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row[section_id]]=$row[section_id];
										$subsection_arr[$row[subsection_id]]=$row[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row[division_id]]=$row[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row[department_id]]=$row[department_id];
									$section_arr[$row[section_id]]=$row[section_id];
									$subsection_arr[$row[subsection_id]]=$row[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row[location_id]]=$row[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row[division_id]]=$row[division_id];
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
				}
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?>
                <tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="8" width='left'><b> Subsection Total</b></td>
                    <td><? echo $subsec_total_eot; ?></td> 	
                    <td>&nbsp;</td>
                    <td><? echo $subsec_total_eot_amount; ?></td>
                    <td>&nbsp;</td>
                </tr>
			<?
			 $subsec_total_eot=0; 
			// $subsec_total_eot_amount=0;
			 $subsec_eot=0;
		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?>
                <tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                    <td colspan="8" width='left'><b>SectionTotal</b></td>
                     <td><? echo $sec_total_eot; ?></td> 	
                    <td>&nbsp;</td>
                    <td><? echo $sec_total_eot_amount; ?></td>
                    <td>&nbsp;</td>
                </tr>
				<?
				 $sec_total_eot=0;
				 // $sec_total_eot_amount=0; 
				  $sec_eot=0;
						
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8" align="left">Department Total</td>
                <td><? echo $deper_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $deper_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<? 
			$deper_total_eot=0; 
			//$deper_total_eot_amount=0; 
			 $deper_eot=0; 
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8" align="left" >Division Total</td>
                <td><? echo $divi_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $divi_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<? 
			$divi_total_eot=0;
			 //$divi_total_eot_amount=0; 
			 $divi_eot=0;
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8" align="left">Location Total</td>
                <td><? echo $location_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $location_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<? 
			$location_total_eot=0;
			// $location_total_eot_amount=0; 
			 $location_eot=0;
		}
		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row[subsection_id]]."  ,";
			}
		

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr><td colspan="15"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}
		   
			if($r%2==0) $bgcolor='#FFFFFF'; else $bgcolor='#EEEEEE';		
			?>    
				<tr bgcolor="<? echo $bgcolor; ?>" height="50">
					<td><? echo $r; ?></td>
					<td><? echo $row['id_card_no']; ?></td>
					<td><? echo $row['emp_code']; ?></td>
					<td><? echo $row['name']; ?></td>
					<td><? echo $designation_chart[$row['designation_id']]; ?></td>
					<td><? echo convert_to_mysql_date($row['joining_date']); ?></td>
					<td><? echo $row['gross_salary']; ?></td>
					<td><? echo $row['basic_salary']; ?></td>
					<td><? echo $row['e_over_time']; ?></td> 	
					<td><? echo $row['over_time_rate']; ?></td>
					<td><? echo $eot_amount=round($row['e_over_time']*$row['over_time_rate']); ?></td>
					<td>&nbsp;</td>               
				</tr>
			<?
			$exploded_var = explode(".", $row['e_over_time']);
			$new_tot=floor($exploded_var[0]*60);$new_tot_min=$new_tot+$exploded_var[1]; 
			$eot += $new_tot_min;
			$total_eot = sprintf("%d.%02d", abs((int)($eot/60)), abs((int)($eot%60)));
			$total_eot_amount+=$eot_amount;
			
			
			//$subsec_total_eot+=$row['e_over_time'];
			$sub_time=$row['e_over_time'];
			$subsec_exploded_var = explode(".",$sub_time);
			$subsec_new_tot=floor($subsec_exploded_var[0]*60);$subsec_new_tot_min=($subsec_new_tot+$subsec_exploded_var[1]); 
			$subsec_eot += $subsec_new_tot_min;
			$subsec_total_eot = sprintf("%d.%02d", abs((int)($subsec_eot/60)), abs((int)($subsec_eot%60)));
			$subsec_total_eot_amount+=$eot_amount;
			
			
			//$sec_total_eot+=$row['e_over_time'];
			$sec_time=$row['e_over_time'];
			$sec_exploded_var = explode(".",$sec_time);
			$sec_new_tot=floor($sec_exploded_var[0]*60);$sec_new_tot_min=($sec_new_tot+$sec_exploded_var[1]);
			$sec_eot += $sec_new_tot_min;
			$sec_total_eot = sprintf("%d.%02d", abs((int)($sec_eot/60)), abs((int)($sec_eot%60)));
			$sec_total_eot_amount+=$eot_amount;
			
			
			//$deper_total_eot+=$row['e_over_time'];
			$dep_time=$row['e_over_time'];
			$deper_exploded_var = explode(".", $dep_time);
			$deper_new_tot=floor($deper_exploded_var[0]*60);$deper_new_tot_min=($deper_new_tot+$deper_exploded_var[1]);
			$deper_eot += $deper_new_tot_min;
			$deper_total_eot = sprintf("%d.%02d", abs((int)($deper_eot/60)), abs((int)($deper_eot%60)));
			$deper_total_eot_amount+=$eot_amount;
			
			
			//$divi_total_eot+=$row['e_over_time'];
			$div_time=$row['e_over_time'];
			$divi_exploded_var = explode(".",$div_time);
			$divi_new_tot=floor($divi_exploded_var[0]*60);$divi_new_tot_min=($divi_new_tot+$divi_exploded_var[1]);
			$divi_eot += $divi_new_tot_min;
			$divi_total_eot = sprintf("%d.%02d", abs((int)($divi_eot/60)), abs((int)($divi_eot%60)));
			$divi_total_eot_amount+=$eot_amount;
			
			
			//$location_total_eot+=$row['e_over_time'];
			$loc_time=$row['e_over_time'];
			$location_exploded_var = explode(".", $loc_time);
			$location_new_tot=floor($location_exploded_var[0]*60);$location_new_tot_min=($location_new_tot+$location_exploded_var[1]);
			$location_eot += $location_new_tot_min;
			$location_total_eot = sprintf("%d.%02d", abs((int)($location_eot/60)), abs((int)($location_eot%60)));
			$location_total_eot_amount+=$eot_amount;
			
		$r++;
		} //while loop end here================================
		
		
		if($status_subsec==1)
		{
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8"><b>Subsection Total</b></td>
                <td><? echo $subsec_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $subsec_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<?

		}
		
		if($status_sec==1)
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8">Section Total</td>
                <td><? echo $sec_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $sec_total_eot_amount; ?></td>
                <td>&nbsp;</td>			
			</tr>
			<?
						
		}
		
		if($status_dept==1 )
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td colspan="8">Department Total</td>
                <td><? echo $deper_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $deper_total_eot_amount; ?></td>
                <td>&nbsp;</td>		
			</tr>
			<?
		}
		 
		if($status_divis==1 )
		{
 		?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td  colspan="8">Division Total</td>
                <td><? echo $divi_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $divi_total_eot_amount; ?></td>
                <td>&nbsp;</td>				
			</tr>
			<?
 			
		}
		 
		if($status_loc==1 )
		{
 			?>
			<tr style="font-weight:bold; font-size:14px" bgcolor="#CCCCCC">
                <td  colspan="8">Location Total</td>
                <td><? echo $location_total_eot; ?></td> 	
                <td>&nbsp;</td>
                <td><? echo $location_total_eot_amount; ?></td>
                <td>&nbsp;</td>
			</tr>
			<?
 			
		}
        ?>
             <tr style="font-weight:bold" bgcolor="#CCCCCC">
                    <td colspan="8">GRAND TOTAL</td>
                    <td><? echo $total_eot; ?></td> 	
                    <td>&nbsp;</td>
                    <td><? echo $total_eot_amount; ?></td>
                    <td>&nbsp;</td>               
             </tr>
    </table>
    	
	<?
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
}

?>