<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');




function search( $array, $key, $value ) { //searh in a two dimensional array by value provided a key
	$results = array();
	
	if( is_array( $array ) ) {
		if( $array[$key] == $value ) $results[] = $array;
		foreach( $array as $subarray ) $results = array_merge( $results, search( $subarray, $key, $value ) );
	}
	return $results;
}

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//emp_basic
	$sql = "SELECT *, CONCAT(first_name,' ',middle_name,' ',last_name) as name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$emp_basic = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$emp_basic[$row['emp_code']] = array();
		foreach( $row AS $key => $value ) {
			$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
		}
	}	

	
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($designation_chart);
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//print_r($company_details);
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_name = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_name[$row['id']] = mysql_real_escape_string($row['company_name']);
	}


 if($action=='manual_attn_report'){ 
	
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	//$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$company_id=$cbo_company_id;
	if ($cbo_emp_category=='') $category =""; else $category ="and a.category='$cbo_emp_category'";
	/*if ($cbo_company_id==0) $cbo_company_id=""; else*/ $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
	
	
	
	
	
	
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(emp.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(emp.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(emp.id_card_no, 5)";}	 
			
	ob_start();	
		
	?>  
	<table cellpadding="0" cellspacing="1" border="1" width="1000" align="center"  class="rpt_table" style="border:1px solid #000;" rules="all"> 
       <thead>
       	<tr>
       		<th colspan="10" align="center" bordercolor="#FFFFFF"><font size="+1"><? echo $company_name[$company_id];?></font><br />Employee Manual Adjustment List</th>
       </tr>
       
         <tr>
             <th width="40" align="center"><b>SL</b></th>
             <th width="70" align="center"><b>ID Card</b></th>
             <th width="70" align="center"><b>Emp Code</b></th>
             <th width="150" align="center"><b>Name</b></th>
             <th width="150" align="center"><b>Designation</b></th>
             <th width="80" align="center"><b>Date</b></th>
             <th width="80" align="center"><b>In Time</b></th>
             <th width="80" align="center"><b>Out Time</b></th>
             <th width="80" align="center"><b>Status</b></th>
             <th><b>Remarks</b></th>  
        </tr>
        </thead>
    <?	
	
		//$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 and a.attnd_date>='$fromdate' and a.attnd_date<='$todate' and a.emp_code=emp.emp_code $category $cbo_company_id $location_id $department_id $section_id $subsection_id order by emp.section_id, emp.designation_level ASC";
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 and a.attnd_date like '$date_part%' and is_manually_updated=1 and a.emp_code=emp.emp_code $cbo_company_id $category $department_id $location_id  $division_id $section_id $subsection_id $designation_id $category 
	$orderby";
	//order by emp.section_id, emp.designation_level ASC";
		//echo $sql;die;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$tot_row=mysql_num_rows($result);
		if($tot_row==0)
		{
			echo "<div align='center'>".'please select a company'."</div>";exit();
		}
		
	$i=0;$new_company=array();$new_location=array();$new_division=array();$new_section=array();$new_department=array();$new_subsection=array();
			
		
		while($row=mysql_fetch_array($result))
		{
			$new_com=0;$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
			if(in_array($row[location_id],$location_arr))
			{
				if(in_array($row[division_id],$division_arr))
				{
					if(in_array($row[department_id],$department_arr))
					{
						if(in_array($row[section_id],$section_arr))
						{
							if(in_array($row[subsection_id],$subsection_arr))
							{}
							else
							{
								$subsection_arr[$row[subsection_id]]=$row[subsection_id];
								$new_subsec=1;
							}
						}
						else
						{
							$section_arr[$row[section_id]]=$row[section_id];
							$subsection_arr[$row[subsection_id]]=$row[subsection_id];
							$new_sec=1;
							$new_subsec=1;
						}
					}
					else
					{
						$department_arr[$row[department_id]]=$row[department_id];
						$section_arr[$row[section_id]]=$row[section_id];
						$subsection_arr[$row[subsection_id]]=$row[subsection_id];
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}
				}//division
				else
				{
					$division_arr[$row[division_id]]=$row[division_id];
					$department_arr[$row[department_id]]=$row[department_id];
					$section_arr[$row[section_id]]=$row[section_id];
					$subsection_arr[$row[subsection_id]]=$row[subsection_id];
					$new_divis=1;
					$new_dept=1;
					$new_sec=1;
					$new_subsec=1;
				}//division else
			}//location
			else
			{
				$location_arr[$row[location_id]]=$row[location_id];
				$division_arr[$row[division_id]]=$row[division_id];
				$department_arr[$row[department_id]]=$row[department_id];
				$section_arr[$row[section_id]]=$row[section_id];
				$subsection_arr[$row[subsection_id]]=$row[subsection_id];
				$new_loc=1;
				$new_divis=1;
				$new_dept=1;
				$new_sec=1;
				$new_subsec=1;
			}//location else
			//header print here 
			if($new_com==1 && $status_com==1)
			{					
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Company : <? echo $company_details[$row[company_id]][company_name]; ?></strong></td></tr><?
			}
			if($new_loc==1 && $status_loc==1)
			{					
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Location : <? echo $location_details[$row[location_id]][location_name]; ?></strong></td></tr><?
			}				
			if($new_divis==1 && $status_divis==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Division : <? echo $division_details[$row[division_id]][division_name]; ?></strong></td></tr><?
			}
			if($new_dept==1 && $status_dept==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Department : <? echo $department_details[$row[department_id]][department_name]; ?></strong></td></tr><?
			}
			if($new_sec==1 && $status_sec==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Section : <? echo $section_details[$row[section_id]][section_name]; ?></strong></td></tr><?
			}
			if($new_subsec==1 && $status_subsec==1)
			{
				?><tr><td colspan="15" align="left" bgcolor="#CCCCCC"><strong>Sub Section : <? echo $subsection_details[$row[subsection_id]][subsection_name]; ?></strong></td></tr><?
			}
						
	?>    
    		<tr style="font-size:10px; height:30px;" bgcolor="<? echo $bgcolor;?>">
            	<td><? echo $i; ?></td>
            	<td>&nbsp;<? echo $row['id_card_no']; ?></td>
                <td>&nbsp;<? echo $row['emp_code']; ?></td>
                <td><? echo $row['name']; ?></td>
                <td>&nbsp;<? echo $designation_chart[$row['designation_id']]['custom_designation']; ?></td>             
              	<td><? echo convert_to_mysql_date($row['attnd_date']);?></td> 	 	
               	<td><? echo $row['sign_in_time'];?></td>
                <td><? echo $row['sign_out_time'];?></td>
               	<td><? echo $row['status'];?></td>
              	<td>&nbsp;<? echo $row['remarks'];?></td>              	
            </tr>
    
		<?
         $i++;
		 }
        ?>
    </table>
    	
	<?
		
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();
	
	
}
?>	

