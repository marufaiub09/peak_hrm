<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

//echo $_SESSION['logic_erp']["company_id"];

if ( $_SESSION['logic_erp']["data_level_secured"]==1 ) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

function get_user_criteria( $prefix, $fld_name  )
{
	if($prefix!="") $prefix .="."; 
	$priv_criteria=array();
	if($_SESSION['logic_erp']["company_id"]) $priv_criteria['company']= " ".$prefix."".$fld_name[0]." in (".$_SESSION['logic_erp']["company_id"].")";
	if($_SESSION['logic_erp']["location_id"]) $priv_criteria['location']= " and ".$prefix."".$fld_name[1]." in (".$_SESSION['logic_erp']["location_id"].")";
	if($_SESSION['logic_erp']["division_id"]) $priv_criteria['division']= " and ".$prefix."".$fld_name[2]." in (".$_SESSION['logic_erp']["division_id"].")";
	if( $_SESSION['logic_erp']["department_id"]) $priv_criteria['department']= " and ".$prefix."".$fld_name[3]." in (".$_SESSION['logic_erp']["department_id"].")";
	if($_SESSION['logic_erp']["section_id"]) $priv_criteria['section']= " and ".$prefix."".$fld_name[4]." in (".$_SESSION['logic_erp']["section_id"].")";
	if($_SESSION['logic_erp']["subsection_id"]) $priv_criteria['subsection']= " and ".$prefix."".$fld_name[5]." in (".$_SESSION['logic_erp']["subsection_id"].")";
	if($_SESSION['logic_erp']["priv_emp_code"])
	{
		$i=0;
		$em=explode(",",$_SESSION['logic_erp']["priv_emp_code"]);
		foreach($em as $mep)
		{
			if($mep!=""){ if($i==0) $emp_codee ="'".$mep."'"; else $emp_codee .=",'".$mep."'"; }
			$i++;
		}
		$priv_criteria['priv_emp']= " and ".$prefix."".$fld_name[6]." in (".$emp_codee.")";
	}
	return $priv_criteria;
}

$datal_priv=get_user_criteria("frd",$fld_name);
//print_r($_SESSION['logic_erp']["department_id"]);
 
//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');
	 	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
		<script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
        
		<script type="text/javascript">
            $(document).ready(function(e) {
				$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselect({
					//header: false, 
					selectedText: "# of # selected",
				});
				$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselectfilter({
				});
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
             });
           
            function openmypage_employee_info(page_link,title)
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#emp_code').val(thee_id.value);
                }
            }
            
            function openmypage_employee_info_id_card(page_link,title)
            {			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function()
                {
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }	
            
            //ajax submit starts here
            function createObject() 
            {
                var request_type;
                var browser = navigator.appName;
                if( browser == "Microsoft Internet Explorer" ) {
                    request_type = new ActiveXObject("Microsoft.XMLHTTP");
                } else {
                    request_type = new XMLHttpRequest();
                }
                return request_type;
            }
            
            var http = createObject();
            var field_array = new Array();
            
            function fnc_employee_job_card()
            { 
                 
				var error = false, data = '';
				
				if( $('#cbo_company_id').val() == 0 ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_company_id').focus();
						$(this).html('Please select Company to search.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				/*
				else if($('#txt_from_date').val() == '' && $('#txt_to_date').val() == '')
				{
					if( $('#cbo_month_selector').val() == 0 ) 
					{
						error = true;
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#cbo_month_selector').focus();
							$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
				}
				*/
				else if($('#cbo_year_selector').val() == 0 && $('#cbo_month_selector').val() == 0)
				{
					if( $('#txt_from_date').val() == '' ) 
					{
						error = true;
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#txt_from_date').focus();
							$(this).html('Please select from date.').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
					else if( $('#txt_to_date').val() =='' ) 
					{
						error = true;
						$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#txt_to_date').focus();
							$(this).html('Please select to date.').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
				}
				
                if( error == false ) 
                {
                    $('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
					$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                    data = 	'&cbo_month_selector=' + $('#cbo_month_selector').val() + 
                            '&cbo_year_selector=' + $('#cbo_year_selector').val()+
                            '&txt_from_date=' + $('#txt_from_date').val() +
							'&txt_to_date=' + $('#txt_to_date').val() +
                            '&category_id=' + $('#cbo_category_id').val() + 
                            '&company_id=' + $('#cbo_company_id').val() + 
                            '&location_id=' + $('#cbo_location_id').val() +
                            '&division_id=' + $('#cbo_division_id').val()+ 
                            '&department_id=' + $('#cbo_department_id').val()+ 
                            '&section_id=' + $('#cbo_section_id').val() + 
                            '&subsection_id=' + $('#cbo_subsection_id').val() +
							'&designation_id=' + $('#cbo_designation_id').val() + 
                            '&id_card=' + $('#id_card').val() +
                            '&emp_code=' + $('#emp_code').val() + 
                            '&cbo_salary_based=' + $('#cbo_salary_based').val() + 
                            '&shift_type=' + $('#shift_type').val();
                    
                    //alert (data);
                    nocache = Math.random();
                    http.open( 'GET', 'includes/generate_emp_job_card.php?action=emp_job_card_report' + data + '&nocache=' + nocache );
                    http.onreadystatechange = response_fnc_emp_job_card_report;
                    http.send(null);
                }
            }
                
			function response_fnc_emp_job_card_report() 
			{	//Punch Report Response
				if(http.readyState == 4) {
					var response = http.responseText.split('####');
					 //alert(response[1]);return;	
				$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
				$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
				$('#data_panel2').html( response[0] );	
					 
					 $("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Job Card Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
					hide_search_panel();
				}
			}
            
            			
			function new_window()
            {
                var w = window.open("Surprise", "#");
                var d = w.document.open();
                d.write(document.getElementById('data_panel2').innerHTML);
                d.close();
            }		

			function populate_select_month( selected_id, page ) 
			{
				if(selected_id!=0)
				{ 
					$("#txt_from_date").val('').attr("disabled", "disabled"); 
					$("#txt_to_date").val('').attr("disabled", "disabled"); 
				}
				else
				{ 
					$("#txt_from_date").attr("disabled", false);
					$("#txt_to_date").attr("disabled", false);
				}
				//else
                $.ajax({
                    type: "GET",
                    url: "includes/"+page+".php?",
                    data: 'type=select_month_generate&id=' + selected_id,
                    success: function( html ) {
                        $('#cbo_month_selector').html( html )
                    }
                });
			}
        </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%;">
            <div style="width:100%; margin:0 auto;">
                <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%" align="center"></div></div>
                <h3 align="left" style="padding-top:5px; width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')">-Employee Job Card</h3>
            </div>
            <div id="content_search_panel"> 
                <form id="punch_report_form" action="javascript:fnc_employee_job_card()" autocomplete="off" method="POST">
                    <fieldset id="filter_panel" style="top:1px;" >
                        <div style="padding:10px;"><?php echo show_search_panel( 2,0,'generate_emp_job_card' ); ?></div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel2" align="left" class="demo_jui2" style="margin-top:10px;"></div>
    </body>
</html>