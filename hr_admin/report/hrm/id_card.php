<?
/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 01-04-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include("../../../includes/array_function.php");

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0  ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hrm.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
	<script type="text/javascript" src="../../../includes/tablefilter.js"></script>
    
     <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
     <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
 	<script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  	<link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
    <script src="../../../js/jquerybarcode.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
				
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});		
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
	$(document).ready(function(e) {
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
		//header: false, 
		selectedText: "# of # selected",
		});
		
		$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
		
	}); 
		
		function openmypage_employee_info_id_card()
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple_by_id.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
				$('#id_card').val(thee_id.value);
			}
		}
		
	function openmypage_employee_info()
	{			
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '../../search_employee_multiple.php?category_id='+$('#cbo_emp_category').val()+'&company_id='+$('#cbo_company_id').val(), 'Employee Information', 'width=1000px,height=390px,center=1,resize=0,scrolling=0','../../')			
		emailwindow.onclose=function()
		{
			var thee_loc = this.contentDoc.getElementById("txt_selected");
			var thee_id = this.contentDoc.getElementById("txt_selected_id");				
			$('#emp_code').val(thee_id.value);
		}
	}	
		
	
	//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

		function populate_data() {
	
			$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
			var error = false, data = '';
			
			
			var category 			= $('#cbo_emp_category').val();
			var company_id 			= $('#cbo_company_id').val();
			var location_id 		= $('#location_id').val();
			var department_id 		= $('#department_id').val();
			var section_id 			= $('#section_id').val();
			var subsection_id 		= $('#subsection_id').val();			
			var designation_id 		= $('#designation_id').val();  
			var cbo_salary_based 	= $('#cbo_salary_based').val();
			var id_card_no			=$('#id_card').val();
			var emp_code            =$('#emp_code').val();
			
					
	if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please Select A Company.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
			
			var passdata = 'category='+category+'&company_id='+company_id+'&location_id='+location_id+'&department_id='+department_id+'&section_id='+section_id+'&subsection_id='+subsection_id+'&designation_id='+designation_id+'&cbo_salary_based='+cbo_salary_based+'&id_card_no='+id_card_no+'&emp_code='+emp_code;
			
			
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "hrm_data.php?"+passdata,
				data: 'form=id_card',
				success: function(data) {
					$('#data_panel').html( data );
					//$('#emcode_filter').focus();	
					var tableFilters = {col_0: "none"}
 	 				setFilterGrid("employees",1,tableFilters);
					hide_search_panel();			
				}
			});
		}
	}
		function Check(chk)
			{
				
				
				var cnt=0;
				if(document.id_card_form.Check_ctr.checked==true){
					for (i = 0; i < chk.length; i++){
						chk[i].checked = true ;
						cnt++;
					}
				}else{			
					for (i = 0; i < chk.length; i++)
					chk[i].checked = false ;			
				}
				$('#count_id').html('You Are Selected '+cnt+' Employees');
			
		}
		
		function count_emp(chk){
			var cnt=0;
			for (i = 0; i < chk.length; i++){
				if(chk[i].checked) cnt++;
			}			
			$('#count_id').html('You Are Selected '+cnt+' Employees');
		}
	
function generateBarcode( valuess, rownum ){
        var value = valuess;//$("#barcodeValue").val();
	   
        var btype = 'code39';//$("input[name=btype]:checked").val();
        var renderer ='bmp';// $("input[name=renderer]:checked").val();
         
        var settings = {
          output:renderer,
          bgColor: '#FFFFFF',
          color: '#000000',
          barWidth: 1,
          barHeight: 50,
          moduleSize:5,
          posX: 0,
          posY: 0,
          addQuietZone: 1
		
        };
         value = {code:value, rect: false};
        $("#barcodeTarget"+rownum).show().barcode(value, btype, settings);
        // alert(value)
      } 
	function fnc_id_card(save_perm,edit_perm,delete_perm,approve_perm,button_id) //ID Card Report
	{
		//alert (button_id);
		var card_name="";
		var page_action="";
		if(button_id==1) 
		{
			card_name="ID Card";
			page_action="action=id_card";
		}
		else if(button_id==2)
		{
			card_name="Transport Card";
			page_action="action=transport_card";
		}
		else
		{
			card_name="Dormitory Card";
			page_action="action=dormitory_card";
		}	
			
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$(this).html('Generating....').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(10000);
		});
		
		var error = false, employee_counter = 0, option=0, data = '&employees=';
			
			var txt_from_date = $('#txt_from_date').val();
			var txt_to_date = $('#txt_to_date').val();
			var company_id = $('#cbo_company_id').val();
			var location_id = $('#location_id').val();
			//alert(company_id);
		
			$('#employees tbody input[type="checkbox"]:checked').each(function() {
			data += $(this).val() + '_';
			employee_counter++
		//alert(data);
		});
		
		if( employee_counter == 0 ) {
			$('#messagebox').fadeTo( 200, 0.1, function() {
				error = true;
				$(this).html('Please select at lease one employee for generating ID Card.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		/*else if( txt_from_date =="") {
			$('#messagebox').fadeTo( 200, 0.1, function() {
				error = true;
				$(this).html('Please select The Issue Date').addClass('messageboxerror').fadeTo(900,1);
			});
		}*/
		else{
			
		if( error == false ) { 
			data = data.substr( 0, data.length - 1 );
			if(confirm('ID Card Format?OK=Hdd and Cancel=Vdd'))
				{
					option="H";
				}
			else
				{
					option="V";
				}
			nocache = Math.random();
			http.open( 'GET', 'includes/id_card_generated_peak.php?'+page_action + data + '&option=' + option + '&nocache=' + nocache+ '&txt_from_date=' + txt_from_date+ '&txt_to_date=' + txt_to_date+ '&company_id=' + company_id+ '&card_name=' + card_name+ '&location_id=' + location_id); 
			//card_name
			//http.open( 'GET', 'includes/id_card_generated_eurotex.php?action=id_card' + data + '&option=' + option + '&nocache=' + nocache+ '&txt_from_date=' + txt_from_date+ '&txt_to_date=' + txt_to_date+ '&card_name=' + card_name);
			http.onreadystatechange = response_id_card;
			http.send(null);
		}//id_card_generated.php
	}
	}
	function response_id_card() {	//ID Card Report Response
		if(http.readyState == 4) {
			var response = http.responseText.split('###');
		 		//alert(response);
				//this lines are used for who has bar code and fatullah apparels start
				$('#data_panel2').html(response[1]);
				for(var k=1; k<=$('#emp_count').val(); k++)
				{
					generateBarcode( $('#id_no_td'+k).html(), k )
				}
				new_window();
				//this lines are used for who has bar code and fatullah apparels,fariha end
				
				//this lines are used for general start
				/*window.open('includes/tmp_report_file/'+response[1], 'ID Card', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=500, height=500');*/ 
			//end  		
			}
	}
	
	
	function new_window()
			{
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				//d.write(document.getElementById('data_panel2').innerHTML);
				d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><title></title><style type="text/css" media="print">p{ page-break-after: always;}</style></head><body>'+document.getElementById('data_panel2').innerHTML+'</body</html>');
				d.close();
			}
	

//function :: add days for adding some days with a specified date
// param   :: from_date, no_of_days
// return  :: adding date

function add_days(from_date, no_of_days)
{
    from_date = from_date.split(/\D+/);
    from_date = new Date(from_date[0],from_date[1]-1,(parseInt(from_date[2])+(no_of_days*1)));
    var ndateArr = from_date.toString().split(' ');
    var Months = 'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec';
    var adding_date = ndateArr[3]+'-'+(parseInt(Months.indexOf(ndateArr[1])/4)+1)+'-'+ndateArr[2];
    return (adding_date);
}

function calculate_date()
{
	var  txt_from_date=document.getElementById('txt_from_date').value;
	var  txt_to_date=document.getElementById('txt_to_date').value;
	
	if(txt_to_date=="")
	{
		var from_date=($('#txt_from_date').val()).split('-');
		var in_date=from_date[2]+'-'+from_date[1]+'-'+from_date[0];
		var days=365;
		var date = add_days(in_date,days);	
		var split_date=date.split('-');	
		if(split_date[1]<10)
		{		
		var res_date=split_date[2]+'-'+0+split_date[1]+'-'+split_date[0];
		}
		else
		{
		var res_date=split_date[2]+'-'+split_date[1]+'-'+split_date[0];
		}
		$('#txt_to_date').val(res_date);
	}
	else
	{
		$('#txt_to_date').val(txt_to_date.value);
	}
}
	</script>
	<style type="text/css">
		.formbutton { width:150px; }
		#select_header select { width:100%; }
		.img_cont
		{
			border:thick 1px #FF00FF;
		}
		table tr td
		{
			margin-left:25px;
		}
	</style>
    
 
</head>
<body style="font-family:verdana; font-size:11px;">
<form id="id_card_form" name="id_card_form" action="" method="POST">
	<div align="center" style="width:100%; height:50px; margin:5px 0;">
	<div><div id="messagebox" style="background-color:#FF9999;" align="center"></div></div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee ID Card</h3>
    <div id="content_search_panel" > 
	
    <fieldset>
    <div style="padding:10px;">
        <table align="center"  style="width:1120px;" border="0" cellpadding="0" cellspacing="2" class="rpt_table">
				<thead>			                
                	<th width="140px">Category</th>
					<th width="140px"> Company</th>
					<th width="140px"> Location</th>
					<th width="140px"> Department</th>
                    <th width="140px"> Section</th>
                    <th width="140px"> SubSection</th>
                    <th width="140px"> Designation</th> 
                    <th width="140px">Salary Based</th>
				</thead>
                <tr class="general">					
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:140px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
                        	<? 
							if($company_cond=="")
							{ 
							?> 
							<option value="0">-- Select --</option>
                            <?php 
							} 
							foreach( $company_details AS $key=>$value )
							{ 
							?>
                            <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
							<?php 
							} 
							?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:140px">
                          <option value="0">-- Select --</option>
                            <?php 
							foreach( $location_details AS $key=>$value )
							{ 
							?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php 
							} 
							?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:140px" multiple="multiple">
							<!--<option value="0">-- Select --</option>-->
                            <?php 
							foreach( $department_details AS $key=>$value )
							{ 
							?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php 
							} 
							?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
							<!--<option value="0">-- Select --</option>-->
                            <?php 
							foreach( $section_details AS $key=>$value )
							{ 
							?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php 
							} 
							?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px" multiple="multiple">
                       	 	<!--<option value="0">-- Select --</option>-->
                            <?php 
							foreach( $subsection_details AS $key=>$value )
							{ 
							?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php 
							} 
							?>
						</select>
					</td> 
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" multiple="multiple" >
							<!--<option value="0">-- Select --</option>-->
                            <?php 
							foreach( $designation_chart AS $key=>$value )
							{ 
							?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php 
							} 
							?>
						</select>
					</td>
                     <td>
                    	<select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:140px" >							
                           <option value="">-- Select --</option>
                            <?php 
							foreach( $salary_based_arr AS $key=>$value )
							{ 
							?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php 
							} 
							?>
                         </select>
                    </td>
                </tr>
            </table>
            <div style="height:8px;"></div>
            <table align="center" class="rpt_table" style="width:600px;" border="0" cellpadding="0" cellspacing="2">
                <thead>			                
                    <th>Issue Date</th> 
                    <th>Validity Date</th>
                    <th>ID Card No</th>
                    <th>System Code</th>                                    			
                    <th><input type="reset" name="reset2" id="reset2" value=" Reset " class="formbutton" style="width:100px" /></th>                  
                </thead>
                <tr class="general">	
                    <td>
                         <input type="text" name="txt_from_date" id="txt_from_date" value="" class="datepicker" size="10"  onchange="calculate_date()" />
                    </td>
                    <td>					
                         <input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" size="10" />
                    </td>
                    <td>
                        <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:130px" ondblclick="openmypage_employee_info_id_card(); return false" placeholder="Double Click For Search" autocomplete="off" />
                    </td>
                    <td align="center"><input type="text" id="emp_code" name="emp_code" class="text_boxes" style="width:130px" ondblclick="openmypage_employee_info(); return false" placeholder="Double Click To Search" onkeypress="return numbersonly(this,event)" /></td>                                          
                    <td><input type="button" name="search" id="search" value="Show" class="formbutton" onclick="populate_data()" style="width:100px" /></td>
                </tr> 
            </table> 
        </div> 
    </fieldset>
   
    </div>
    <fieldset>
    	<span id="count_id" style="font-weight:bold">&nbsp;</span>
		<div id="data_panel" style="margin-top:10px;"></div>
        <div id="data_panel2" style="margin-top:10px; display:none"></div>
		<div align="center" style="margin-top:10px;">
			<input type="hidden" name="checked_employees" id="checked_employees" value="" />
			<input type="button" name="save" value="Generate ID Card" class="formbutton" onclick="fnc_id_card(save_perm,edit_perm,delete_perm,approve_perm,1);" />
           <!-- <input type="button" name="save" value="Generate ID Card" class="formbutton" onclick="new_window()" />-->
            <input type="button" name="save" value="Transport Card" class="formbutton" onclick="fnc_id_card(save_perm,edit_perm,delete_perm,approve_perm,2);" />
            <input type="button" name="save" value="Dormitory Card" class="formbutton" onclick="fnc_id_card(save_perm,edit_perm,delete_perm,approve_perm,3);" />
		</div>
    </fieldset>	
  </div>  
 
   </form>
</body>
</html>