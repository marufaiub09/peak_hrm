<? 
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond;
//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
        
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
            
        <script type="text/javascript" charset="utf-8">	
            $(document).ready(function() {
                /*
				$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id,#cbo_shift_id").multiselect({
                //header: false, 
                selectedText: "# of # selected",
                });
				*/
            });
			
			// createObject
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
            
			// show
            function fnc_male_female_ratio_report()
            {
                var category_id		= $('#cbo_category_id').val();
                var company_id		= $('#cbo_company_id').val();
                var location_id		= $('#cbo_location_id').val();
                var division_id		= $('#cbo_division_id').val();
                var department_id	= $('#cbo_department_id').val();
                var section_id		= $('#cbo_section_id').val();
                var subsection_id	= $('#cbo_subsection_id').val();
                var designation_id	= $('#cbo_designation_id').val();
                
                if(company_id==0)
                {
                    $('#messagebox').fadeTo(200,0.1,function(){
                            $('#cbo_company_id').focus();
                            $(this).html('Please select company name.').addClass('messageboxerror').fadeTo(900,1);
                            $(this).fadeOut(5000);	
                        });
                }
                else 
                {
                    $('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
                    
                    var data=	'&category_id='+category_id+
                                '&company_id='+company_id+
                                '&location_id='+location_id+
                                '&division_id='+division_id+
                                '&department_id='+department_id+
                                '&section_id='+section_id+
                                '&subsection_id='+subsection_id+
                                '&designation_id='+designation_id;
                                
                    nocache = Math.random();
                    http.open( 'GET', 'includes/generate_male_female_ratio.php?action=male_female_ratio' + data + '&nocache=' + nocache );
                    http.onreadystatechange = response_fnc_male_female_ratio_report;
                    http.send(null);
                }
            }
            
            function response_fnc_male_female_ratio_report() {	
                if(http.readyState == 4) {
                    var response = http.responseText.split('####');
                    // alert(response[1]);		 	
                    $('#data_panel').html( '<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>' );
                    $('#data_panel2').html( response[0] );
                    $("#messagebox").fadeTo( 200, 0.1, function() {
                        $(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
                        $(this).fadeOut(5000);
                    });
					hide_search_panel();
                }
            }
			
			// form reset
            function reset_field()
            {
                document.getElementById('data_panel').innerHTML="";
                document.getElementById('data_panel2').innerHTML="";
            }
    
            function new_window()
            {
                var w = window.open("Surprise", "#");
                var d = w.document.open();
                d.write(document.getElementById('data_panel2').innerHTML);
                d.close();
            }
     
        </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div style="width:100%;" align="center">
            <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
            <h3 align="left" style="top:1px;width:100%;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Employee Male Female Report</h3>
            <div id="content_search_panel">
                <form id="ratio_report_form" action="javascript:fnc_male_female_ratio_report()" autocomplete="off" method="POST">
                    <fieldset><legend>Search Panel</legend>
                        <table class="rpt_table"  width="1120px" cellspacing="1" border="1" align="center" rules="all">
                            <thead>
                                <th width="140px">Category</th>
                                <th width="140px">Company Name</th>				    
                                <th width="140px">Location</th>
                                <th width="140px">Division</th>				    
                                <th width="140px">Department</th>
                                <th width="140px">Section</th>				    
                                <th width="140px">Subsection</th>
                                <th width="140px">Designation</th>				    
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                                <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px" >
                                            <? if($company_cond=="")
                                            { 
                                            ?>
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:140px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:140px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:140px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $department_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:140px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $section_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:140px" >
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $subsection_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td id="designation">
                                        <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- Select --</option>
                                            <?php foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="width:1040px; height:25px; padding-top:8px;">
                            <input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:100px" />
                            <input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" onClick="reset_field();"  style="width:100px"/>
                        </div> 
                    </fieldset>
                </form>
            </div>
            <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
            <div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
        </div>	
    </body>
</html>