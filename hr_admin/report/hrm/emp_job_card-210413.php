<?php
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script type="text/javascript" charset="utf-8">
				
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
	function openmypage_employee_info(page_link,title)
		{			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#emp_code').val(thee_id.value);
			}
		}	
	
	


//ajax submit starts here

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();



function fnc_employee_job_card()
	
	{ 
		$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
		var error = false, data = '';
		
		if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_month_selector').focus();
				$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $('#cbo_company_id').val() == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#cbo_company_id').focus();
				$(this).html('Please select Company to search.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		if( error == false ) {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			data = '&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&txt_date=' + $('#txt_date').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() +'&division_id=' + $('#division_id').val()+ '&department_id=' + $('#department_id').val()+ '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&emp_code=' + $('#emp_code').val();
			
			nocache = Math.random();
			http.open( 'GET', 'includes/generate_emp_job_card.php?action=emp_job_card_report' + data + '&nocache=' + nocache );
			http.onreadystatechange = response_fnc_emp_job_card_report;
			http.send(null);
		}
	}
	
	
	function response_fnc_emp_job_card_report(){	//Punch Report Response
		if(http.readyState == 4) {
			var response = http.responseText.split('####');
			// alert(response[1]);
			$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
			$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
			 $('#data_panel2').html( response[0] );
			 $("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Job Card Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}


function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel2').innerHTML);
	d.close();
}

	
		
	</script>
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center">
    	<div class="form_caption">
              Employee Job Card
        </div>
		<div align="center" id="messagebox"></div>
	</div>
	<form id="punch_report_form" action="javascript:fnc_employee_job_card()" autocomplete="off" method="POST">
		<fieldset id="filter_panel" style="top:1px;width:900px" >
			<legend>Filter Panel</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
				<thead>
                    <th><strong>Month</strong></th>
                    <th><strong>Year</strong></th>
                    <th><strong>Date</strong></th>
                    <th><strong>Category</strong></th>
					<th><strong>Company</strong></th>
					<th><strong>Location</strong></th>
                    <th><strong>Division</strong></th>
					<th><strong>Department</strong></th>
                    <th><strong>Section</strong></th>
                    <th><strong>SubSection</strong></th>
                    <th><strong>Designation</strong></th>
					<th><strong>Employee Code</strong></th>	
				</thead>
                <tr class="general">
					<td>
                    	<select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes">
                            <option value="0">--- ---</option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
						</select>
                   </td>
					
					<td>
						<select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" >
							<option value="0">-- Select --</option>
							<?php 
							$c_year=date("Y");
							$s_year=$c_year-25;
							$e_year=$c_year+10;
							
							 for ($year = $s_year; $year <= $e_year; $year++) { ?>
							<option value=<?php echo $year;
							if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
						 <input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="12" />
					</td>
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:110px">
                         	<option value="">All Category</option>
                            <option value="0">Top Management</option>
							<option value="1">Mid Management</option>
							<option value="2">Non Management</option>
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:110px">
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected";?> ><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:110px">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
       			<td>
					<select name="division_id" id="division_id" class="combo_boxes" style="width:110px">
							<option value="0">-- Select --</option>
                            <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:110px">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:110px">
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:110px">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>    
                     <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:110px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>      
					<td><input type="text" name="emp_code" id="emp_code" value="" class="text_boxes" size="40" style="width:140px"  onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info('../../search_employee_multiple.php','Employee Information'); return false" placeholder="Double Click For Search" /></td>
				</tr>                
                <tr>
					<td colspan="11" align="center">
                    	<input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:150px" />&nbsp;
                        <input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:150px" />
                    </td>
				</tr>
            </table>
		</fieldset>
	</form>
	<div id="data_panel" align="center" style="margin-top:10px;"></div>
	<div id="data_panel2" align="center" style="margin-top:10px;"></div>
	
</body>
</html>