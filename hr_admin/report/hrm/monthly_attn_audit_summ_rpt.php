<?php
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');

	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../../resources/multiselect/jquery.multiselect.filter.js"></script>
        <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        
     
         <script type="text/javascript" src="TableFilter/tablefilter.js"></script>
         <script type="text/javascript" src="TableFilter/tablefilter_all.js"></script>
       	<link href="TableFilter/filtergrid.css"  rel="stylesheet" type="text/css" />
         
        
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
                
                //multi select
                $("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselect({
                //header: false, 
                selectedText: "# of # selected",
                });
				$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id").multiselectfilter({ });
	
	 });
            
            function fnc_monthly_attendance_audit_summary()
            {
                if( $('#cbo_company_id').val() == 0 ) 
				{
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#cbo_company_id').focus();
                        $(this).html('Please select company name.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
                    });
                }
                else if( $('#cbo_month_selector').val() == 0) 
				{
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#cbo_month_selector').focus();
                        $(this).html('Please select month to search.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
                    });
                }
				else
				{
					$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
					var data =  '&category=' + $('#cbo_category').val()+
                                '&company_id=' + $('#cbo_company_id').val()+
								'&location_id=' + $('#cbo_location_id').val()+
								'&division_id=' + $('#cbo_division_id').val()+
                                '&department_id=' + $('#cbo_department_id').val()+
                                '&section_id=' + $('#cbo_section_id').val()+
                                '&subsection_id=' + $('#cbo_subsection_id').val()+ 
                                '&designation_id=' + $('#cbo_designation_id').val()+
								'&year_selector=' + $('#cbo_year_selector').val()+					
								'&month_selector=' + $('#cbo_month_selector').val()+                                
                                '&salary_based=' + $('#cbo_salary_based').val()+
                                '&group_by_id=' + $('#group_by_id').val()+
                                '&order_by_id=' + $('#order_by_id').val();
                    
                    //alert($('#cbo_salary_based').val());
                    if (window.XMLHttpRequest)
                      {// code for IE7+, Firefox, Chrome, Opera, Safari
                        http=new XMLHttpRequest();
                      }
                    else
                      {// code for IE6, IE5
                         http=new ActiveXObject("Microsoft.XMLHTTP");
                      }
                    
                    nocache = Math.random();
                    http.open( 'GET', 'includes/generate_details_monthly_attn_audit_summary.php?action=monthly_attn_summery_audit' + data + '&nocache=' + nocache );
                    http.onreadystatechange = response_fnc_monthly_attendance_audit_summary;
                    http.send(null);
                }
            }
            
            function response_fnc_monthly_attendance_audit_summary() {	//Punch Report Response
                if(http.readyState == 4) {
                    var response = http.responseText.split('####');
                     //alert(response[0]);		
                    $('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
                    $('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
                    $('#data_panel2').html( response[0] );
                     $("#messagebox").fadeTo( 200, 0.1, function() {
                        $(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
                        $(this).fadeOut(5000);
                    });
					
					
					var table7_Props = 	{					
					col_0: "none"
					
				};
					
					setFilterGrid("table3",'',table7_Props); 
                    hide_search_panel();
                }
            }
    
        function new_window()
        {
           
		   $('#table3').find('tr:first').hide();
		    var w = window.open("Surprise", "#");
            var d = w.document.open();
            d.write(document.getElementById('data_panel2').innerHTML);
            d.close();
			$('#table3').find('tr:first').show();
        }
        
        function populate_select_month( selected_id ) 
        {
            $.ajax({
                type: "GET",
                url: "includes/generate_details_monthly_attn_audit_summary.php?",
                data: 'type=select_month_generate&id=' + selected_id,
                success: function( html ) {
                    $('#cbo_month_selector').html( html )
                }
            });
        }	
    //	show_inner_filter
    
        </script>
        <style type="text/css">
            #filter_panel select { width:100px; }
            #filter_panel * { font-family:verdana; font-size:11px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div style="width:100%;" align="center">
            <div style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%" align="center"></div></div>
            <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> - Attendance Audit Summary</h3>
            <div id="content_search_panel">
                <form id="frm_monthly_attn_audit_summary" action="javascript:fnc_monthly_attendance_audit_summary()" autocomplete="off" method="POST">
                    <fieldset><legend>Search Panel</legend>
                        <table width="1120"  cellpadding="0" cellspacing="0" class="rpt_table" align="center">
                            <thead>
                                <th width="140px"><strong>Category</strong></th>
                                <th width="140px"><strong>Company</strong></th>
                                <th width="140px"><strong>Location</strong></th>
                                <th width="140px"><strong>Division</strong></th>
                                <th width="140px"><strong>Department</strong></th>
                                <th width="140px"><strong>Section</strong></th>
                                <th width="140px"><strong>SubSection</strong></th>
                                <th width="140px"><strong>Designation</strong></th>
                            </thead>
                            <tbody>
                                <tr> 
                                    <td>
                                         <select name="cbo_category" id="cbo_category" class="combo_boxes" style="width:140px">
                                            <option value="">All Category</option>
                                            <?
                                            foreach($employee_category as $key=>$val)
                                            {
                                            ?>
                                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                            <?
                                            }
                                            ?> 
                                         </select>
                                    </td>
                                    <td>
                                        <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px">
                                            <? 
                                            if($company_cond=="")
                                            { 
                                            ?>
                                                <option value="0">-- Select --</option>
                                            <?php 
                                            } 
                                            foreach( $company_details AS $key=>$value )
                                            { 
                                            ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:140px">
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $location_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                           <!-- <option value="0">-- Select --</option>-->
                                            <?php 
                                            foreach( $division_details AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                    <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                        <!-- <option value="0">-- Select --</option>-->
                                        <?php 
                                        foreach( $department_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    </td>
                                    <td>
                                    <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                        <!-- <option value="0">-- Select --</option>-->
                                        <?php 
                                        foreach( $section_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    </td>
                                    <td id="subsection">
                                        <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                        <!-- <option value="0">-- Select --</option>-->
                                        <?php 
                                        foreach( $subsection_details AS $key=>$value )
                                        { ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    </td>
                                    <td id="designation">
                                        <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                           <!-- <option value="0">-- Select --</option>-->
                                            <?php 
                                            foreach( $designation_chart AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="height:8px;"></div>
                        <table align="center" width="680"  cellpadding="0" cellspacing="0" class="rpt_table">
                            <thead>
                                <th width="100px"><strong>Year</strong></th>
                                <th width="120px"><strong>Month</strong></th>
                                <th width="120px"><strong>Salary Based</strong></th>
                                <th width="120px"><strong>Group By</strong></th>
                                <th width="120px"><strong>Order By</strong></th> 
                                <th> <input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:70px" /> </th>            
                            </thead>
                            <tbody>
                                <tr class="general">
                                    <td> 
                                        <select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" style="width:100px;" > 
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $policy_year AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php
                                            } 
                                            ?>
                                        </select>
                                    </td>  
                                    <td>
                                        <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes" style="width:120px;">							
                                            <option value="0">-- Select --</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:120px" >							
                                            <option value="">-- Select --</option>
                                            <?php 
                                            foreach( $salary_based_arr AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:120px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $groupby_arr AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:120px" >							
                                            <option value="0">-- Select --</option>
                                            <?php 
                                            foreach( $orderby_arr AS $key=>$value )
                                            { 
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </td>
                                    <td style="background-color:#A6CAF0;">
                                        <input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:70px"/>&nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </form>
            </div>
          </div>  
        <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel2" align="" class="demo_jui2" style="margin-top:10px;"></div>
        <div id="data_panel3" align="" class="demo_jui2" style="margin-top:10px;"></div>
    </body>
</html>