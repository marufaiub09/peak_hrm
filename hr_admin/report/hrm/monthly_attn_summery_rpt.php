<?php
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}


$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 and status_active=1 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$shift_details[$row['id']] = mysql_real_escape_string( $row['shift_name'] );		
	}
	
$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
    <link href="../../../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="../../../includes/tablefilter.js"></script>
    
	<script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	 <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
     <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
			
			$("#cbo_division_id,#cbo_department_id,#cbo_section_id,#cbo_subsection_id,#cbo_designation_id,#cbo_shift_id").multiselect({
			//header: false, 
			selectedText: "# of # selected",
			});
			
			$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});

		});
		
		function createObject() {
			var request_type;
			var browser = navigator.appName;
			if( browser == "Microsoft Internet Explorer" ) {
				request_type = new ActiveXObject("Microsoft.XMLHTTP");
			} else {
				request_type = new XMLHttpRequest();
			}
			return request_type;
		}
		
		var http = createObject();
		var field_array = new Array();
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}		
	
		function openmypage(page_link,title)
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=770px,height=380px,center=1,resize=0,scrolling=0','../../')
		}
		
		// Monthly Attendance Summary
		function fnc_monthly_attendance_summary()
		{
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			//alert("su..re");
			var category_id		= $('#cbo_category_id').val();
			var company_id		= $('#cbo_company_id').val();
			var location_id		= $('#cbo_location_id').val();
			var division_id		= $('#cbo_division_id').val();
			var department_id	= $('#cbo_department_id').val();
			var section_id		= $('#cbo_section_id').val();
			var subsection_id	= $('#cbo_subsection_id').val();
			var designation_id	= $('#cbo_designation_id').val();
			var month			= $('#cbo_month_selector').val();
			var year			= $('#cbo_year_selector').val();
			var date			= $('#txt_date').val();
			var salary_based	= $('#cbo_salary_based').val();
			var shift_id		= $('#cbo_shift_id').val();
			
			$("#messagebox").removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
			//$("#messagebox").removeClass().addClass('messagebox').html('<img src="../images/loading.gif" />').fadeIn(1000);
			
			if(cbo_company_id==0)
			{ 
				$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
					$('#cbo_company_id').focus();
					$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}		
			else if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) 
			{
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#cbo_month_selector').focus();
					$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}		
			else
			{
				var data='&category_id='+category_id+
						'&company_id='+company_id+
						'&location_id='+location_id+
						'&division_id='+division_id+
						'&department_id='+department_id+
						'&section_id='+section_id+
						'&subsection_id='+subsection_id+
						'&designation_id='+designation_id+
						'&month='+month+
						'&year='+year+
						'&date='+date+
						'&salary_based='+salary_based+
						'&shift_id='+shift_id;
				//alert(data);
				
				nocache = Math.random();
				http.open( 'POST', 'includes/generate_monthly_attn_summery_rpt.php?action=monthly_attn_summery' + data + '&nocache=' + nocache );
				http.onreadystatechange = response_monthly_attn_summary;
				http.send(null);
			}
		}
		// Response Monthly Attendance Summary
		function response_monthly_attn_summary() 
		{
			if(http.readyState == 4) 
			{
				var response = http.responseText.split("####");
				//alert(response[0]);		
				$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[2] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
				$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
				$('#data_panel').append( '<input type="button" onclick="openmypage_email_report()" value="Mail Report" name="email" class="formbutton" style="width:100px"/><input type="hidden"  value="'+response[1]+'" name="email_link" id="email_link" style="width:100px"/>' );
				
				$('#data_panel2').html( response[0] );
				 var tableFilters = 
				 {
					/* col_0: "none",
					col_12: "none",
					col_20: "none",
					display_all_text: " ---Show All---",*/
					//col_operation: {
					//id: ["value_stock_qnty","value_stock_val"],
					//col: [10,12],
					//operation: ["sum","sum"],
					//write_method: ["innerHTML","innerHTML"]
					//}	             
				}
						
				setFilterGrid("table_body",-1,tableFilters);
				hide_search_panel();
				$("#messagebox").fadeTo( 200, 0.1, function() {
					$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
		}
		
		//mail report 11.05.2014 
		function openmypage_email_report()
		{
			//alert("su..re");	
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe','../../email_report.php','Mail address window', 'width=500px,height=190px,center=1,resize=1,scrolling=0','../../')			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("email_report");
				var fname=$('#email_link').val();
				
				if( $('#txt_date').val()!="" )
				{
					var date=$('#txt_date').val().split("-");
					var from_date='01'+"-"+date[1]+"-"+date[2];
					var to_date=date[0]+"-"+date[1]+"-"+date[2];	
				}
				else
				{
					var date=$('#cbo_month_selector').val().split("_");
					var from_date=date[0];
					var to_date=date[1];	
				}
				if(thee_loc.value=="")
				{
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'E-mail address is blank' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else
				{
					// alert("su..re")
					$('#messagebox').removeClass().addClass('messagebox').text('Sending....').fadeIn(1000);
					$.ajax({
						type: "POST",
						url: 'includes/generate_monthly_attn_summery_rpt.php',
						data: 'htmldata=' + fname +'&action=mail_full_report&email_address='+thee_loc.value+'&from_date='+from_date+'&to_date='+to_date,
						success: function( html ) 
						{
							if(html==2)
							{
								$("#messagebox").fadeTo( 200, 0.1, function() {
									$(this).html( 'Report was not sent.' ).addClass('messagebox_ok').fadeTo(900,1);
									$(this).fadeOut(5000);
								});
							}
							else if(html==3)
							{
								$("#messagebox").fadeTo( 200, 0.1, function() {
									$(this).html( 'Report has been sent.' ).addClass('messagebox_ok').fadeTo(900,1);
									$(this).fadeOut(5000);
								});
							}
						}
					}); 
				}
			}
		}
	
		function new_window()
		{
			var w = window.open("Surprise", "#");
			var d = w.document.open();
			d.write(document.getElementById('data_panel2').innerHTML);
			d.close();
		}
		
function populate_select_month( selected_id ) 
	{
		if(selected_id!=0)
		{ 
			$("#txt_date").val('').attr("disabled", "disabled"); 
		}
		else
		{ 
			$("#txt_date").attr("disabled", false);
		}
		//else
		$.ajax({
			type: "GET",
			url: "includes/generate_monthly_attn_summery_rpt.php?",
			data: 'type=select_month_generate&id=' + selected_id,
			success: function( html ) {
				$('#cbo_month_selector').html( html )
			}
		});
	} </script>
</head>
    <body style="font-family:verdana; font-size:11px;">
    	<div style="width:100%" align="center">
        	<div style="height:40px;">
                <div align="center" style="height:18px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
                <h3 align="left" style="top:1px;width:100%;" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Monthly Attendance Summary</h3>
            </div>
            <div id="content_search_panel" style="width:100%;" align="center" > 
            <form id="punch_report_form" action="javascript:fnc_monthly_attendance_summary()" autocomplete="off" method="POST">
                <fieldset><legend> Search Panel</legend>			
                    <table class="rpt_table"  width="1040" cellspacing="1" border="1" align="center" rules="all">
                        <thead>
                            <th width="100">Category</th>
                            <th width="140">Company Name</th>				    
                            <th width="100">Location</th>
                            <th width="140">Division</th>				    
                            <th width="140">Department</th>
                            <th width="140">Section</th>				    
                            <th width="140">Subsection</th>
                            <th width="140">Designation</th>				    
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select name="cbo_category_id" id="cbo_category_id" class="combo_boxes" style="width:100px">
                                        <option value="">All Category</option>
                                        <?
                                        foreach($employee_category as $key=>$val)
                                        {
                                        ?>
                                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                                        <?
                                        }
                                        ?> 
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:140px" >
                                        <? if($company_cond=="")
                                        { 
                                        ?>
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        } 
                                        foreach( $company_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"<? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_location_id" id="cbo_location_id" class="combo_boxes" style="width:100px" >
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $location_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_division_id" id="cbo_division_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <?php foreach( $division_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_department_id" id="cbo_department_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <?php foreach( $department_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_section_id" id="cbo_section_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <?php foreach( $section_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="subsection">
                                    <select name="cbo_subsection_id" id="cbo_subsection_id" class="combo_boxes" style="width:140px" multiple="multiple" >
                                        <?php foreach( $subsection_details AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td id="designation">
                                    <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:140px" multiple="multiple">
                                        <?php foreach( $designation_chart AS $key=>$value )
                                        { 
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="height:8px;"></div>
                    <table class="rpt_table"  width="770" cellspacing="1" border="1" align="center" rules="all">
                        <thead>
                            <th width="140">Year</th>
                            <th width="140">Month</th>
                            <th width="140">Date</th>
                            <th width="140">Salary Based</th>
                            <th width="140">Shift Policy</th>				    
                            <th width="70"><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:70px" /></th>				    
                        </thead>
                        <tbody>
                            <tr>
                                <td> 
                            <select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" style="width:140px;" > 
                                <option value="0">-- Select --</option>
                                <?php 
                                foreach( $policy_year AS $key=>$value )
                                { 
                                ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php
                                } 
                                ?>
                            </select>
                         </td>  
                         <td>
                            <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes" style="width:140px;">							
                                <option value="0">-- Select --</option>
                            </select>
                       </td>
                                <td><input type="text" name="txt_date" id="txt_date" value="" class="datepicker"  style="width:140px" /></td>
                                <td>
                                    <select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:140px" >							
                                       <option value="">-- Select --</option>
                                        <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                     </select>
                                </td>
                                <td>
                                    <select name="cbo_shift_id" id="cbo_shift_id" class="combo_boxes" style="width:140px"  multiple="multiple">
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        foreach( $shift_details AS $key=>$value )
                                        { 
                                        ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                                <td bgcolor="#A6CAF0"><input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:70px"/></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            </form>
            </div>
        </div>
        <div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
        <div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
    </body>
</html>