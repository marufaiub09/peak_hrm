<? 
// ekram
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
		

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hrm.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
    <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
	 <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
     <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
    
	<script type="text/javascript" charset="utf-8">
	
		
$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
 });
				
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
		
		
		
	
	function openmypage(page_link,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=450px,center=1,resize=0,scrolling=0','../../')
		/*
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemailid=this.contentDoc.getElementById("txt_seleted_po_id") //Access form field with id="emailfield"
			alert(theemailid.value);
			
			knitting_dyeing_update(theemailid.value,15);
			setTimeout('showResult_search(document.getElementById("txt_program_no").value,9,"knitting_dying_container","sd",2)',150);
			
		}*/
	}
	
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		

//ajax submit starts here ekram

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();





function fnc_attn_daily_inlastout_report(){
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	if( $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_date').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	 if( $('#cbo_company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	/*else if( $('#department_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#department_id').focus();
			$(this).html('Please select Department to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#section_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#section_id').focus();
			$(this).html('Please select Section to search.').addClass('messageboxerror').fadeTo(900,1); + '&cbo_salary_based=' + $('#cbo_salary_based').val()
		});
	}*/
	if( error == false ) {		
		data = '&txt_date=' + $('#txt_date').val()+ '&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() +'&division_id=' + $('#division_id').val() +'&department_id=' + $('#department_id').val() + '&section_id=' + $('#section_id').val()+ '&subsection_id=' + $('#subsection_id').val()+ '&designation_id=' + $('#designation_id').val()+'&group_by_id='+$('#group_by_id').val()+ '&order_by_id='+$('#order_by_id').val()+ '&cbo_salary_based='+$('#cbo_salary_based').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/generate_attn_daily_in_last_out.php?action=daily_inlastout' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_daily_inlastout;
		http.send(null);
	}
}

function response_daily_inlastout() {	
	if(http.readyState == 4) {		
		var response =http.responseText.split('####');
		//alert(response[1]);		
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
		$('#data_panel').append( '<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
		$('#data_panel2').html( response[0] );
		 hide_search_panel();
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}		
		

function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel2').innerHTML);
	d.close();
}
		
		
		
	</script>
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:100%;">
    <div align="center">
    	<div align="center" id="messagebox"></div>
    </div>
    <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Daily In And Last Out Report</h3>
    <div id="content_search_panel" > 
	<form id="punch_report_form" action="javascript:fnc_attn_daily_inlastout_report()" autocomplete="off" method="POST">
			<fieldset id="filter_panel" >
            <div style="padding:10px;"> 
			<table width="1000" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <th><strong>Category</strong></th> 
					<th><strong>Company</strong></th>
					<th><strong>Location</strong></th>
                    <th><strong>Division</strong></th>
					<th><strong>Department</strong></th>
                    <th><strong>Section</strong></th>
                    <th><strong>SubSection</strong></th>
                    <th><strong>Designation</strong></th>
				</thead>
                <tr class="general">
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes" style="width:120px">
                         	<option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:130px" >
                        	<? if($company_cond=="")
							   { 
								?>
							<option value="0">-- Select --</option>
                            <?php } foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px" >
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
                  <td>
                        <select name="division_id" id="division_id" class="combo_boxes" style="width:120px" multiple="multiple" >
                        <!--  <option value="0">-- Select --</option>-->
                            <?php foreach( $division_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px" multiple="multiple" >
							 <!--  <option value="0">-- Select --</option>-->
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:120px" multiple="multiple">
							 <!--  <option value="0">-- Select --</option>-->
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                     <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px" multiple="multiple">
                       	 	 <!--  <option value="0">-- Select --</option>-->
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" multiple="multiple" >
							 <!--  <option value="0">-- Select --</option>-->
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                </tr>
             </table>
             <div style="height:8px;"></div>
             <table align="center" width="600" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <th><strong>Date</strong></th>
                    <th><strong>Salary Based</strong></th> 
                    <th>Group By</th>
                    <th>Order By</th> 
                    <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>            
				</thead>
                <tr class="general">
                       <td>
                             <input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="10" />
                        </td> 
                     <td>
                    	<select name="cbo_salary_based" id="cbo_salary_based" class="combo_boxes" style="width:120px" >							
                           <option value="">-- Select --</option>
                            <?php foreach( $salary_based_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:120px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:120px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td> 
                    <!--
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" onchange="populate_cost_center( 'section', this.value );">							
                            <option value="0">--Select--</option>
                            <option value="department">Department</option>
                            <option value="company">Company</option>
                            <option value="location">Location</option>
                        </select>
                    </td>
                    -->
					
				
                  <td>
                  		<input type="submit" name="search" id="search" value="Show" class="formbutton" style="width:100px" />
                  </td>
                </tr>
			</table>
            </div>
		</fieldset>
	</form>
    </div>
</div>
    
	<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>
	<div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
	
</body>
</html>