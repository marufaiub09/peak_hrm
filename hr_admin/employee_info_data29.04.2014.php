<?php
session_start();
include('../includes/common.php');
extract( $_POST );
extract( $_GET );


//emp_basic view_emp_basic
$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 and is_deleted=0 ORDER BY emp_code ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$emp_basic = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$emp_basic[$row['emp_code']] = array();
	foreach( $row AS $key => $value ) {
		$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

?>	

<?

if( isset( $_POST['cost_center'] ) ) {
	if( $_POST['cost_center'] == 'company' )	{ $parent = 'group';		$child_array = $company_details;		$child_name = "company_name"; }
	if( $_POST['cost_center'] == 'location' )	{ $parent = 'company';		$child_array = $location_details;		$child_name = "location_name"; }
	if( $_POST['cost_center'] == 'division' )	{ $parent = 'location';		$child_array = $division_details;		$child_name = "division_name"; }
	if( $_POST['cost_center'] == 'department' )	{ $parent = 'division';		$child_array = $department_details;		$child_name = "department_name"; }
	if( $_POST['cost_center'] == 'section' )	{ $parent = 'department';	$child_array = $section_details;		$child_name = "section_name"; }
	if( $_POST['cost_center'] == 'subsection' )	{ $parent = 'section';		$child_array = $subsection_details;		$child_name = "subsection_name"; }
	
	$parent_result = false;
	$options1 = $options2 = "<option value=\"0\">-- Select --</option>";
	
	foreach( $child_array AS $child ) {
		if( $child['status_active'] == 1 ) {
			if( $_POST['parent_id'] != 0 && $child[$parent . '_id'] == $_POST['parent_id'] ) {
				$parent_result = true;
				if( $child['id'] == $_POST['selected_id'] ) $options1 .= "<option value=\"$child[id]\" selected>" . $child["$child_name"] . "</option>";
				else $options1 .= "<option value=\"$child[id]\">" . $child["$child_name"] . "</option>";
			}
			if( $child['id'] == $_POST['selected_id'] ) $options2 .= "<option value=\"$child[id]\" selected>" . $child["$child_name"] . "</option>";
			else $options2 .= "<option value=\"$child[id]\">" . $child["$child_name"] . "</option>";
		}
	}
	if( $parent_result == true ) print_r( $options1 );
	else print_r( $options2 );
	exit();
}

if( isset( $emp_code ) ) {
	$employee_info = array();
	
	if( $link == 'basic' ) {
		$_SESSION['emp_code'] = $emp_code;
		/*$sql = "SELECT emp.*, job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
				FROM hrm_employee AS emp
				LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
				WHERE emp.emp_code = '$emp_code'";*/
		$sql = "SELECT * FROM hrm_employee WHERE emp_code = '$emp_code'";
				
		
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		
		while( $row = mysql_fetch_assoc( $result ) ) {
			echo "$('#emp_code').val('".mysql_real_escape_string($row["emp_code"])."');\n";	
			echo "$('#first_name').val('".mysql_real_escape_string($row["first_name"])."');\n";	
			echo "$('#middle_name').val('".mysql_real_escape_string($row["middle_name"])."');\n";	
			echo "$('#last_name').val('".mysql_real_escape_string($row["last_name"])."');\n";	
			echo "$('#full_name_bangla').val('".mysql_real_escape_string($row["full_name_bangla"])."');\n";	
			echo "$('#id_card_no').val('".mysql_real_escape_string($row["id_card_no"])."');\n";	
			echo "$('#punch_card_no').val('".mysql_real_escape_string($row["punch_card_no"])."');\n";	
			echo "$('#dob').val('".mysql_real_escape_string(convert_to_mysql_date($row["dob"]))."');\n";	
			echo "$('#father_name').val('".mysql_real_escape_string($row["father_name"])."');\n";	
			echo "$('#father_name_bangla').val('".mysql_real_escape_string($row["father_name_bangla"])."');\n";	
			echo "$('#mother_name').val('".mysql_real_escape_string($row["mother_name"])."');\n";	
			echo "$('#mother_name_bangla').val('".mysql_real_escape_string($row["mother_name_bangla"])."');\n";	
			echo "$('#birth_place').val('".mysql_real_escape_string($row["birth_place"])."');\n";	
			echo "$('#religion').val('".mysql_real_escape_string($row["religion"])."');\n";	
			echo "$('#blood_group').val('".$row["blood_group"]."');\n";	
			echo "$('#marital_status').val('".mysql_real_escape_string($row["marital_status"])."');\n";	
			echo "$('#nationality').val('".mysql_real_escape_string($row["nationality"])."');\n";	
			echo "$('#national_id').val('".mysql_real_escape_string($row["national_id"])."');\n";
			echo "$('#passport_no').val('".mysql_real_escape_string($row["passport_no"])."');\n";
			echo"populate_designations( $row[designation_level] );\n";
			echo "$('#designation_id').val('".mysql_real_escape_string($row["designation_id"])."');\n";
			echo "$('#designation_level').val('".mysql_real_escape_string($row["designation_level"])."');\n";
			echo "$('#joining_date').val('".mysql_real_escape_string(convert_to_mysql_date($row["joining_date"]))."');\n";
			echo "$('#confirmation_date').val('".mysql_real_escape_string(convert_to_mysql_date($row["confirmation_date"]))."');\n";
			echo "$('#service_benifit_from').val('".mysql_real_escape_string($row["service_benifit_from"])."');\n";
			echo "$('#category').val('".mysql_real_escape_string($row["category"])."');\n";
			echo "$('#functional_superior').val('".mysql_real_escape_string($row["functional_superior"])."');\n";
			echo "$('#admin_superior').val('".mysql_real_escape_string($row["admin_superior"])."');\n";
			echo "$('#salary_grade').val('".mysql_real_escape_string($row["salary_grade"])."');\n";
			echo "$('#salary_rule').val('".mysql_real_escape_string($row["salary_rule"])."');\n";
			echo "$('#gross_salary').val('".mysql_real_escape_string($row["gross_salary"])."');\n";
			echo "$('#remark').val('".mysql_real_escape_string($row["remark"])."');\n";
			echo "$('#company_id').val('".mysql_real_escape_string($row["company_id"])."');\n";
			echo "$('#location_id').val('".mysql_real_escape_string($row["location_id"])."');\n";
			echo "$('#division_id').val('".mysql_real_escape_string($row["division_id"])."');\n";
			echo "$('#department_id').val('".mysql_real_escape_string($row["department_id"])."');\n";
			echo "$('#section_id').val('".mysql_real_escape_string($row["section_id"])."');\n";
			echo "$('#subsection_id').val('".mysql_real_escape_string($row["subsection_id"])."');\n";
			echo "$('#txt_status_salary').val('".mysql_real_escape_string($row["status_salary"])."');\n";
			
			
			//$employee_info[] =  $row_arr;
		}
		//echo $bangla_name;
		//print_r($row_arr['full_name_bangla']); 
		//print_r( serialize( $employee_info ) );
						
	}
	else if( $link == 'photo' ) {
		$sql = "SELECT location FROM resource_photo WHERE identifier = '$emp_code' AND type = 1";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( mysql_num_rows( $result ) > 0 ) {
			$row =  mysql_fetch_assoc( $result );
			$link_param="$row[location]";
			echo "<li class=\"success\"><img width=\"89px\" height=\"97px\" src=\"../$row[location]\" onclick=\"fn_remove_picture('$link_param')\"><br /></li>";
		}
		else echo "";
	}
	else if( $link == 'address' ) {
		$sql = "SELECT * FROM hrm_employee_address WHERE emp_code = '$emp_code'";
		
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
		
		while( $row = mysql_fetch_assoc( $result ) ) {	
						if( $row['address_type'] == 0 ) $type = "present";
						if( $row['address_type'] == 1 ) $type = "permanent";
						if( $row['address_type'] == 2 ) $type = "alternate";
						
			//echo "$('#".$type."_house_no').val('".mysql_real_escape_string($row["subsection_id"])."');\n";			
			echo "$('#".$type."_house_no').val('".mysql_real_escape_string($row["house_no"])."');\n";
			echo "$('#".$type."_house_no_bangla').val('".mysql_real_escape_string($row["house_no_bangla"])."');\n";
			echo "$('#".$type."_road_no').val('".mysql_real_escape_string($row["road_no"])."');\n";
			echo "$('#".$type."_road_no_bangla').val('".mysql_real_escape_string($row["road_no_bangla"])."');\n";
			echo "$('#".$type."_division_id').val('".mysql_real_escape_string($row["division_id"])."');\n";
			echo "$('#".$type."_division_id_bangla').val('".mysql_real_escape_string($row["division_id_bangla"])."');\n";
			echo "$('#".$type."_district_id').val('".mysql_real_escape_string($row["district_id"])."');\n";
			echo "$('#".$type."_district_id_bangla').val('".mysql_real_escape_string($row["district_id_bangla"])."');\n";
			echo "$('#".$type."_thana').val('".mysql_real_escape_string($row["thana"])."');\n";
			echo "$('#".$type."_thana_bangla').val('".mysql_real_escape_string($row["thana_bangla"])."');\n";
			echo "$('#".$type."_village').val('".mysql_real_escape_string($row["village"])."');\n";
			echo "$('#".$type."_village_bangla').val('".mysql_real_escape_string($row["village_bangla"])."');\n";
			echo "$('#".$type."_post_code').val('".mysql_real_escape_string($row["post_code"])."');\n";
			echo "$('#".$type."_post_code_bangla').val('".mysql_real_escape_string($row["post_code_bangla"])."');\n";
			echo "$('#".$type."_phone_no').val('".mysql_real_escape_string($row["phone_no"])."');\n";
			echo "$('#".$type."_mobile_no').val('".mysql_real_escape_string($row["mobile_no"])."');\n";
			echo "$('#".$type."_email').val('".mysql_real_escape_string($row["email"])."');\n";
			echo "$('#co').val('".mysql_real_escape_string($row["co"])."');\n";
			echo "$('#co_bangla').val('".mysql_real_escape_string($row["co_bangla"])."');\n";
			
			//$employee_info[] =  $row;
		}
		//print_r( serialize( $employee_info ) );
	}
	else if( $link == 'salary' ) {
		$sql = "SELECT salary_grade,salary_rule,gross_salary FROM hrm_employee WHERE emp_code = '$emp_code' ORDER BY id ASC";
		$row =  mysql_fetch_assoc( mysql_query( $sql ) ) or die( $sql . "<br />" . mysql_error() );
		
		echo "$('#salary_grade').val(" . $row['salary_grade'] . ");";
		echo "$('#salary_rule').val(" . $row['salary_rule'] . ");";
		echo "$('#gross_salary').val(" . $row['gross_salary'] . ");";
		echo "$('#bank_salary').val(" . $row['bank_gross'] . ");";	
	}
	else if( $link == 'salary_breakdown' ) {
		$sql = "SELECT * FROM hrm_employee_salary WHERE emp_code = '$emp_code' ORDER BY id ASC";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			$i = 0;
			$append_data = '';
			while( $row = mysql_fetch_assoc( $result ) ) {
				$i++;
				$append_data .= "<tr>
									<td>
										<select name=\"payroll_head[]\" id=\"payroll_head_$i\" class=\"combo_boxes\">
											<option value=\"0\"></option>";
											foreach( $payroll_heads AS $payroll ) {
												$append_data .= "<option value=\"$payroll[id]\""; if( $payroll['id'] == $row['payroll_head'] ) $append_data .= " selected"; $append_data .= ">$payroll[custom_head]</option>";
											}
				$append_data .=	"		</select>
									</td>
									<td>
										<select name=\"type[]\" id=\"type_$i\" onchange=\"change_type( $i )\" class=\"combo_boxes\">
											<option value=\"%\""; if( $row['type'] == "%" ) $append_data .= " selected"; $append_data .= ">%</option>
											<option value=\"Fixed\""; if( $row['type'] == "Fixed" ) $append_data .= " selected"; $append_data .= ">Fixed</option>
											<option value=\"Formula\""; if( $row['type'] == "Formula" ) $append_data .= " selected"; $append_data .= ">Formula</option>
										</select>
									</td>
									<td><input type=\"text\" name=\"percentage_formula[]\" id=\"percentage_formula_$i\" onfocus=\"add_formula( $i );\" onblur=\"calculate_amount( $i );\" value=\"$row[percentage_formula]\" class=\"text_boxes numbers\" /></td>
									<td>
										<select name=\"base_head[]\" id=\"base_head_$i\" onchange=\"calculate_amount( $i );\" class=\"combo_boxes\">
											<option value=\"0\"></option>";
											foreach( $payroll_heads AS $payroll ) {
												$append_data .= "<option value=\"$payroll[id]\""; if( $payroll['id'] == $row['base_head'] ) $append_data .= " selected"; $append_data .= ">$payroll[custom_head]</option>";
											}
				$append_data .=	"		</select>
									</td>
									<td><input type=\"text\" name=\"amount[]\" id=\"amount_$i\" value=\"$row[amount]\" onblur=\"calculate_total();\" class=\"text_boxes numbers\" disabled /></td>
								</tr>";
			}
		}
		else {
			$i = 1;
			$append_data .= "<tr>
								<td>
									<select name=\"payroll_head[]\" id=\"payroll_head_$i\" class=\"combo_boxes\">";
										foreach( $payroll_heads AS $payroll ) {
											$append_data .=	"<option value=\"$payroll[id]\">$payroll[custom_head]</option>";
										}
			$append_data .=	"		</select>
								</td>
								<td>
									<select name=\"type[]\" id=\"type_$i\" onchange=\"change_type( $i )\" class=\"combo_boxes\">
										<option value=\"%\">%</option>
										<option value=\"Fixed\">Fixed</option>
										<option value=\"Formula\">Formula</option>
									</select>
								</td>
								<td><input type=\"text\" name=\"percentage_formula[]\" id=\"percentage_formula_$i\" onfocus=\"add_formula( $i );\" onblur=\"calculate_amount( $i );\" value=\"\" class=\"text_boxes numbers\" /></td>
								<td>
									<select name=\"base_head[]\" id=\"base_head_$i\" onchange=\"calculate_amount( $i );\" class=\"combo_boxes\">";
										foreach( $payroll_heads AS $payroll ) {
											$append_data .=	"<option value=\"$payroll[id]\">$payroll[custom_head]</option>";
										}
			$append_data .=	"		</select>
								</td>
								<td><input type=\"text\" name=\"amount[]\" id=\"amount_$i\" value=\"0\" onblur=\"calculate_total();\" class=\"text_boxes numbers\" disabled /></td>
							</tr>";
		}
		echo "$('#salary tbody').html( '" . mysql_real_escape_string( $append_data ) . "' );";
		echo "head_counter = $i;";
	}
	else if( $link == 'salary_bank' ) {
		$sql = "SELECT * FROM hrm_employee_salary_bank WHERE emp_code = '$emp_code'";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		while( $row = mysql_fetch_assoc( $result ) ) {
			$employee_info[] =  $row;
		}
		print_r( serialize( $employee_info ) );
	}
	else if( $link == 'entitlement' ) {
		$sql = "SELECT * FROM hrm_employee_entitlement WHERE emp_code = '$emp_code'";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			$employee_info['entitlement'] =  mysql_fetch_assoc( $result );
			
			list( $year, $month, $day ) = explode( "-", $employee_info['entitlement']['pf_effective_date'] );
			$employee_info['entitlement']['pf_effective_date'] = $day . "-" . $month . "-" . $year;
			
			list( $year, $month, $day ) = explode( "-", $employee_info['entitlement']['gi_effective_date'] );
			$employee_info['entitlement']['gi_effective_date'] = $day . "-" . $month . "-" . $year;
		}
		
		$sql = "SELECT * FROM hrm_employee_nominee WHERE emp_code = '$emp_code'";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		while( $row = mysql_fetch_assoc( $result ) ) {
			$employee_info['nominee'][] = $row;
		}
		
		print_r( serialize( $employee_info ) );
	}
	else if( $link == 'experience' ) {
		$sql = "SELECT * FROM hrm_employee_experience WHERE emp_code = '$emp_code' ORDER BY id ASC";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			while( $row = mysql_fetch_assoc( $result ) ) {
				list( $year, $month, $day ) = explode( "-", $row['joining_date'] );
				$row['joining_date'] = $day . "-" . $month . "-" . $year;
				
				list( $year, $month, $day ) = explode( "-", $row['resigning_date'] );
				$row['resigning_date'] = $day . "-" . $month . "-" . $year;
				
				$row['gross_salary'] = ( $row['gross_salary'] == '0.00' ) ? NULL : $row['gross_salary'];
				
				$employee_info[] = $row;
			}
		}
		else $employee_info = "";
		
		print_r( serialize( $employee_info ) );
	}
	else if( $link == 'education' ) {
		$sql = "SELECT * FROM hrm_employee_education WHERE emp_code = '$emp_code' ORDER BY id ASC";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			while( $row = mysql_fetch_assoc( $result ) ) {
				$employee_info[] = $row;
			}
		}
		else $employee_info = "";
		
		print_r( serialize( $employee_info ) );
	}
	else if( $link == 'family' ) {
		$sql = "SELECT * FROM hrm_employee_family WHERE emp_code = '$emp_code' ORDER BY id ASC";
		$result =  mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			while( $row = mysql_fetch_assoc( $result ) ) {
				list( $year, $month, $day ) = explode( "-", $row['dob'] );
				$row['dob'] = $day . "-" . $month . "-" . $year;
				
				$employee_info[] = $row;
			}
		}
		else $employee_info = "";
		
		print_r( serialize( $employee_info ) );
	}
	exit();
}

if( isset( $form ) && $form == 'job_separation' ) {
	
	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
	//echo $id_card_no;die;
?>
<div style="width:1090px;" align="left">
	<table cellpadding="0" cellspacing="0" class="rpt_table">
		<thead>			
				<th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.separation_form.chk_list)" placeholder="Search" /></th>
				<th width="80">Code</th>
				<th width="120">Name</th>
				<th width="80">ID Card</th>
				<th width="90">Punch Card</th>
				<th width="80">Designation</th>
				<th width="80">Company</th>
				<th width="80">Location</th>
				<th width="80">Division</th>
				<th width="80">Department</th>
				<th width="80">Section</th>
				<th width="80">Subsection</th>
				<th width="90">DOJ</th>			
		</thead>
	</table>
</div>
<div style="width:1090px; overflow-y:scroll; max-height:210px;" align="left">
	<table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_separation">
			<?php			
			if ($company_id==0) $company_id=""; else $company_id="and company_id='$company_id'";
			if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
			if ($division_id==0) $division_id=""; else $division_id="and division_id in ($division_id)";
			if ($section_id==0) $section_id=""; else $section_id="and section_id in($section_id)";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in($subsection_id)";
			if ($department_id==0) $department_id=""; else $department_id="and department_id in($department_id)";
			if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in($designation_id)";
			if ($category=='') $category =""; else $category ="and category='$category'";
			if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in($id_card_no)";
			if ($empcode=="") $emp_code=""; else $emp_code="and emp_code in($empcode)"; 	
				//echo $emp_code;die;	
			$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 $company_id $location_id 
			$division_id $department_id $section_id $subsection_id $category $designation_id $id_card_no $emp_code ORDER BY id_card_no ASC";//$id_card
 			
			//echo $sql;die;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			while( $emp = mysql_fetch_assoc( $result ) ) {?> 
			<tr>
				<td width="20"><input type="checkbox" name="chk_list" value="<?php echo $emp['emp_code']; ?>" onclick="count_emp(document.separation_form.chk_list)" /></td>
				<td width="80"><?php echo $emp['emp_code']; ?></td>
				<td width="120"><?php echo split_string($emp['name'],17); ?></td>
				<td width="80"><?php if( $emp['id_card_no'] != '' ) echo split_string($emp['id_card_no'],10); ?></td>
				<td width="90"><?php if( $emp['punch_card_no'] != '' ) echo split_string($emp['punch_card_no'],11); ?></td>
				<td width="80"><?php if( $emp['designation_id'] != '0' ) echo split_string($designation_chart[$emp['designation_id']]['custom_designation'],12); ?></td>
				<td width="80"><?php if( $emp['company_id'] != '0' ) echo split_string($company_details[$emp['company_id']]['company_name'],12); ?></td>
				<td width="80"><?php if( $emp['location_id'] != '0' ) echo split_string($location_details[$emp['location_id']]['location_name'],12); ?></td>
				<td width="80"><?php if( $emp['division_id'] != '0' ) echo split_string($division_details[$emp['division_id']]['division_name'],10); ?></td>
				<td width="80"><?php if( $emp['department_id'] != '0' ) echo split_string($department_details[$emp['department_id']]['department_name'],12); ?></td>
				<td width="80"><?php if( $emp['section_id'] != '0' ) echo split_string($section_details[$emp['section_id']]['section_name'],10); ?></td>
				<td width="80"><?php if( $emp['subsection_id'] != '0' ) echo split_string($subsection_details[$emp['subsection_id']]['subsection_name'],10); ?></td>
				<td width="90" align="center"><?php if( $emp['joining_date'] != '0000-00-00' ) echo convert_to_mysql_date($emp['joining_date']); ?></td>	            	
            </tr>
			<?php } ?>		
	</table>
</div>    
<?php
exit();
}

else if( isset( $form ) && $form == 'job_reactivation' ) {
	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
	//echo $id_card_no;die;
?>
<div style="width:1430px;" align="left">	
    <table cellpadding="0" cellspacing="0" border="0" class="rpt_table">
		<thead>			
				<th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.reactivation_form.chk_list)" placeholder="Search" /></th>
				<th width="80">Code</th>
				<th width="150">Name</th>
				<th width="80">ID Card</th>
				<th width="90">Punch Card</th>
				<th width="100">Designation</th>
				<th width="100">Company</th>
				<th width="100">Location</th>
				<th width="80">Division</th>
				<th width="100">Department</th>
				<th width="80">Section</th>
				<th width="80">Subsection</th>
				<th width="80">DOJ</th>
				<th width="80">Separated From</th>
				<th width="80">Separation Type</th>
				<th width="80">Causes of Sep.</th>		
		</thead>
	</table>
</div>
<div style="width:1430px; overflow-y:scroll; max-height:210px;" align="left">
	<table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_reactivation">
			<?php
			if ($company_id==0) $company_id=""; else $company_id="and emp.company_id='$company_id'";
			if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
			if ($division_id==0) $division_id=""; else $division_id="and emp.division_id in($division_id)";
			if ($section_id==0) $section_id=""; else $section_id="and emp.section_id in ($section_id)";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id in ($subsection_id)";
			if ($department_id==0) $department_id=""; else $department_id="and emp.department_id in ($department_id)";
			if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id in ($designation_id)";
			if ($category=='') $category =""; else $category ="and emp.category='$category'";
			if ($id_card=="") $id_card_no=""; else $id_card_no="and emp.id_card_no in($id_card_no)";
			if ($empcode==0) $emp_code=""; else $emp_code="and emp.emp_code in($empcode)"; 	
					
			//$sql = "SELECT emp.*,max(sep.separated_from) as separated_from, sep.separation_type, sep.cause_of_separation,sep.id as sepid, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp, hrm_separation sep where emp.status_active=0 and emp.emp_code=sep.emp_code $company_id $location_id $division_id $department_id $section_id $subsection_id $category $designation_id $id_card_no $emp_code group by sep.emp_code ORDER BY sep.id DESC, emp.id_card_no ASC";
			
			$sql = "SELECT emp.*,MAX(sep.id) as id, sep.separation_type, sep.cause_of_separation,sep.id as sepid, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp, hrm_separation sep where emp.status_active=0 and emp.emp_code=sep.emp_code $company_id $location_id $division_id $department_id $section_id $subsection_id $category $designation_id $id_card_no $emp_code group by sep.emp_code ORDER BY sep.id DESC, emp.id_card_no ASC";
			//echo $sql;die; 
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
 			while( $row = mysql_fetch_assoc( $result ) ) { 
			?>			
			<tr>
				<td width="20"><input type="checkbox" name="chk_list" id="chk_list" value="<?php echo $row['emp_code']; ?>" onclick="count_emp(document.reactivation_form.chk_list)" /></td>
				<td width="80"><?php echo $row['emp_code']; ?></td>
				<td width="150"><?php echo split_string($row['name'],15); ?></td>
				<td width="80"><?php if( $row['id_card_no'] != '' ) echo split_string($row['id_card_no'],10); ?></td>
				<td width="90"><?php if( $row['punch_card_no'] != '' ) echo split_string($row['punch_card_no'],11); ?></td>
				<td width="100"><?php if( $row['designation_id'] != '0' ) echo split_string($designation_chart[$row['designation_id']]['custom_designation'],12); ?></td>
				<td width="100"><?php if( $row['company_id'] != '0' ) echo split_string($company_details[$row['company_id']]['company_name'],12); ?></td>
				<td width="100"><?php if( $row['location_id'] != '0' ) echo split_string($location_details[$row['location_id']]['location_name'],12); ?></td>
				<td width="80"><?php if( $row['division_id'] != '0' ) echo split_string($division_details[$row['division_id']]['division_name'],10); ?></td>
				<td width="100"><?php if( $row['department_id'] != '0' ) echo split_string($department_details[$row['department_id']]['department_name'],12); ?></td>
				<td width="80"><?php if( $row['section_id'] != '0' ) echo split_string($section_details[$row['section_id']]['section_name'],10); ?></td>
				<td width="80"><?php if( $row['subsection_id'] != '0' ) echo split_string($subsection_details[$row['subsection_id']]['subsection_name'],10); ?></td>
				<td width="80" align="center"><?php if( $row['joining_date'] != '0000-00-00' ) echo convert_to_mysql_date($row['joining_date']); ?></td>
				<td width="80" align="center">
					<?php 
						$sep_from= return_field_value("separated_from","hrm_separation","id='$row[id]' and is_deleted=0");
						echo convert_to_mysql_date($sep_from); 
					?>
                </td>
				<td width="80"><?php echo split_string($row['separation_type'],10); ?></td>
				<td width="80"><?php echo split_string($row['cause_of_separation'],10); ?></td>                
			</tr>
			<?php } ?>		
	</table>
</div>    
<?php
exit();
}
else if( isset( $form ) && $form == 'policy_tagging' ) { ?>
<div style="width:1500px" align="left">	
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="rpt_table">
		<thead>			
            <th width="20"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.policy_tagging_form.chk_list)" placeholder="Search" /></th>
            <th width="20">SL</th>
            <th width="90">Code</th>
            <th width="170">Name</th>
            <th width="50">Sex</th>
            <th width="70">ID Card</th>
            <th width="80">Punch Card</th>
            <th width="150">Designation</th>
            <th width="150">Company</th>
            <th width="150">Location</th>
            <th width="100">Division</th>
            <th width="150">Department</th>
            <th width="100">Section</th>
            <th width="80">Subsection</th>
            <th width="80">DOJ</th>
		</thead>
	</table>
 </div>	
 <div style="width:1500px;overflow-y:scroll; max-height:210px;" align="left">
	<table  width="100%" cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_policy_tagging">
			<?php 			
			if ($company_id==0) $company_id=""; else $company_id="and company_id='$company_id'";
			if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
			if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
			if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
			if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";
			if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
			if ($category=='') $category =""; else $category ="and category='$category'";
			if ($empcode=='') $empcode =""; else $empcode ="and emp_code='$empcode'";
			
			if ($policy_id=='') $policy_id =""; 
			else if($policy_id=='0') $policy_id ="and overtime_policy=0 and holiday_incentive_policy=0 and duty_roster_policy=0 and leave_policy=0 and maternity_leave_policy=0 and attendance_bonus_policy=0 and	absent_deduction_policy=0 and late_deduction_policy=0 and bonus_policy=0 and tax_policy=0 and shift_policy=0";
			else  $policy_id ="and ".$policy_id."=0";
			
			if ($policy_tagged_id=='') $policy_tagged_id =""; 
			else if($policy_tagged_id=='0') $policy_tagged_id ="and overtime_policy!=0 and holiday_incentive_policy!=0 and duty_roster_policy!=0 and leave_policy!=0 and maternity_leave_policy!=0 and attendance_bonus_policy!=0 and	absent_deduction_policy!=0 and late_deduction_policy!=0 and bonus_policy!=0 and tax_policy!=0 and shift_policy!=0";
			else  $policy_tagged_id ="and ".$policy_tagged_id."!=0";
			
			$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 $company_id $location_id $division_id $department_id $section_id $subsection_id $category $designation_id $policy_id $policy_tagged_id ORDER BY id_card_no ASC";
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$emp_basic = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$emp_basic[$row['emp_code']] = array();
				foreach( $row AS $key => $value ) {
					$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
				}
			}
			
				$i=0;
				foreach( $emp_basic AS $emp ) 
				{				
					$sql="SELECT * FROM hrm_employee where emp_code='$emp[emp_code]' and overtime_policy=0 and holiday_incentive_policy=0 and duty_roster_policy=0 and leave_policy=0 and maternity_leave_policy=0 and	attendance_bonus_policy=0 and absent_deduction_policy=0 and late_deduction_policy=0 and bonus_policy=0 and tax_policy=0 and shift_policy=0 and status_active=1";
					$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
					$numrows_2=mysql_num_rows($result);					
					if($numrows_2>0){
						$bgcolor="bgcolor=#FF3900";
					}else{
						$bgcolor="";
					}
				$i++;
			?>
			<tr>
				<td width="20"  <? if($numrows_2>0) echo $bgcolor; ?>><input type="checkbox" name="chk_list" id="chk_list" value="<?php echo $emp['emp_code']; ?>" onclick="count_emp(document.policy_tagging_form.chk_list)" /></td>
				<td width="20" <? echo $bgcolor; ?>><? echo $i; ?></td>
                <td width="90"  <? echo $bgcolor; ?>><?php echo split_string($emp['emp_code'],9); ?></td>
				<td width="170" <? echo $bgcolor; ?>><?php echo "<a href='#' onclick=\"openmypage('policy_tagging_pop_up.php?emp_code=$emp[emp_code]','Employee Policy Tagging'); return false\">". split_string($emp[name],20)."</a>"; ?></td>
				<td width="50"><?php if( $emp['sex'] == 0 ) echo "Male"; else echo "Female"; ?></td>
				<td width="70"><?php if( $emp['id_card_no'] != '' ) echo split_string($emp['id_card_no'],11); ?></td>
				<td width="80"><?php if( $emp['punch_card_no'] != '' ) echo split_string($emp['punch_card_no'],10); ?></td>
				<td width="150"><?php if( $emp['designation_id'] != '0' ) echo split_string($designation_chart[$emp['designation_id']]['custom_designation'],15); ?></td>
				<td width="150"><?php if( $emp['company_id'] != '0' ) echo split_string($company_details[$emp['company_id']]['company_name'],18); ?></td>
				<td width="150"><?php if( $emp['location_id'] != '0' ) echo split_string($location_details[$emp['location_id']]['location_name'],18); ?></td>
				<td width="100"><?php if( $emp['division_id'] != '0' ) echo split_string($division_details[$emp['division_id']]['division_name'],15); ?></td>
				<td width="150"><?php if( $emp['department_id'] != '0' ) echo split_string($department_details[$emp['department_id']]['department_name'],15); ?></td>
				<td width="100"><?php if( $emp['section_id'] != '0' ) echo split_string($section_details[$emp['section_id']]['section_name'],15); ?></td>
				<td width="80"><?php if( $emp['subsection_id'] != '0' ) echo split_string($subsection_details[$emp['subsection_id']]['subsection_name'],12); ?></td>
				<td width="80"><?php if( $emp['joining_date'] != '0000-00-00' ) echo split_string($emp['joining_date'],10); ?></td>                
			</tr>
			<?php  } ?>		
	</table>
  </div>  
<?php
exit();
}
else if( isset( $form ) && ( $form == 'opening_leave_entry' ) ) { 


if ($company_id==0) $company_id=""; else $company_id="and company_id='$company_id'";
if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";
if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
if ($category=='') $category =""; else $category ="and category='$category'";

$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 $company_id $location_id $department_id $section_id $subsection_id $category $designation_id ORDER BY id_card_no ASC";
//echo $sql;
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
 
 ?>
	<div style="width:1500px" align="left">	
    <table width="100%" cellpadding="0" cellspacing="0" border="0"  class="rpt_table"  >
		<thead>	
			<tr>
                <th width="20"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.policy_tagging_form.chk_list)" placeholder="Search" /></th>
				<th width="90">Code</th>
				<th width="170">Name</th>
				<th width="50">Sex</th>
				<th width="70">ID Card</th>
				<th width="80">Punch Card</th>
				<th width="150">Designation</th>
				<th width="150">Company</th>
				<th width="150">Location</th>
				<th width="100">Division</th>
				<th width="150">Department</th>
				<th width="100">Section</th>
				<th width="80">Subsection</th>
                <th width="">DOJ</th>
			</tr>
        <thead>    
	</table>
 </div>	
 <div style="width:1500px;overflow-y:scroll; max-height:210px;" align="left">
	<table  width="100%" cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_employees">		
			<?php  while( $emp = mysql_fetch_assoc( $result ) ) { ?>
			<tr style="cursor:pointer" onclick="javascript: enable_leave_entry('<?php echo $emp['emp_code']; ?>')">
				<td width="20"  <? echo $bgcolor; ?>><input type="checkbox" name="chk_list" id="chk_list" value="<?php echo $emp['emp_code']; ?>" /></td>
				<td width="90"  <? echo $bgcolor; ?>><?php echo split_string($emp['emp_code'],9); ?></td>
				<td width="170" <? echo $bgcolor; ?>><?php echo split_string($emp[name],20); ?></td>
				<td width="50"><?php if( $emp['sex'] == 0 ) echo "Male"; else echo "Female"; ?></td>
				<td width="70"><?php if( $emp['id_card_no'] != '' ) echo split_string($emp['id_card_no'],11); ?></td>
				<td width="80"><?php if( $emp['punch_card_no'] != '' ) echo split_string($emp['punch_card_no'],10); ?></td>
				<td width="150"><?php if( $emp['designation_id'] != '0' ) echo split_string($designation_chart[$emp['designation_id']]['custom_designation'],15); ?></td>
				<td width="150"><?php if( $emp['company_id'] != '0' ) echo split_string($company_details[$emp['company_id']]['company_name'],18); ?></td>
				<td width="150"><?php if( $emp['location_id'] != '0' ) echo split_string($location_details[$emp['location_id']]['location_name'],18); ?></td>
				<td width="100"><?php if( $emp['division_id'] != '0' ) echo split_string($division_details[$emp['division_id']]['division_name'],15); ?></td>
				<td width="150"><?php if( $emp['department_id'] != '0' ) echo split_string($department_details[$emp['department_id']]['department_name'],15); ?></td>
				<td width="100"><?php if( $emp['section_id'] != '0' ) echo split_string($section_details[$emp['section_id']]['section_name'],15); ?></td>
				<td width="80"><?php if( $emp['subsection_id'] != '0' ) echo split_string($subsection_details[$emp['subsection_id']]['subsection_name'],12); ?></td>
				<td width=""><?php if( $emp['joining_date'] != '0000-00-00' ) echo split_string($emp['joining_date'],10); ?></td>                
			</tr>
			<?php } ?>		
	</table>
 </div>  
<?php
exit();
}


else if( isset( $data ) && $data = 'opening_leave_balance' ) { //for update 
	$emp_info = array();
	/*$sql = "SELECT * FROM hrm_employee WHERE emp_code = '$code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	if( mysql_num_rows( $result ) > 0 ) {
		$row = mysql_fetch_assoc( $result );
		if( $row['leave_policy'] != 0 ) {
			$sql2 = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $row[leave_policy]";
			$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
 			while( $row2 = mysql_fetch_assoc( $result2 ) ) 
			{
				$emp_info[] = $row2;
			}
		}
		else $emp_info['error'] = 'Leave policy has not been attached with this employee. Please attach a leave policy first.';
	}
	else $emp_info['error'] = 'Leave policy has not been attached with this employee. Please attach a leave policy first.';
	
	*/
	$sql = "SELECT * FROM hrm_leave_balance WHERE emp_code = '$code' and is_locked=0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	//echo $sql; die;
	if( mysql_num_rows( $result ) > 0 ) {
		//$row = mysql_fetch_assoc( $result );
 			while( $row = mysql_fetch_assoc( $result ) ) 
			{
				$emp_info[] = $row;
			}
		}
		else $emp_info['error'] = 'Leave policy has not been attached with this employee. Please attach a leave policy first.';
	 
	//print_r($emp_info); echo $code; die;
	
	if($emp_info['error']=="")
	{
		echo "$('#emp_code').val('".$code."');\n";	
		foreach( $emp_info as $policy_data ) {
			echo "$('#".$policy_data['leave_type']."').removeAttr('disabled').val(".$policy_data['leave_limit'].");\n";
			echo "$('#".$policy_data['leave_type'].'_limit'."').val(".$policy_data['leave_limit'].");\n";
		}
	}
	else
	{
		echo "$('#messagebox').html('".$emp_info['error']."');\n";
	}
	exit();
}

else if( isset( $form ) && ( $form == 'weekend' ) ) { ?>
<div style="width:1300px;" align="left">	
    <table cellpadding="0" cellspacing="0" border="0" class="rpt_table">
		<thead>
				<th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.weekend_form.chk_list)" placeholder="Search" /></th>
				<th width="80">Code</th>
				<th width="150">Name</th>
				<th width="50">Sex</th>
				<th width="80">ID Card</th>
				<th width="100">Punch Card</th>
				<th width="120">Designation</th>
				<th width="120">Company</th>
				<th width="120">Location</th>
				<th width="100">Division</th>
				<th width="120">Department</th>
				<th width="100">Section</th>
				<th width="90">Subsection</th>                	
		</thead>
     </table>
 </div>
<div style="width:1300px; overflow-y:scroll; max-height:210px;" align="left">
	<table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_employees">   
		
			<?php 
			
			if ($company_id==0) $company_id=""; else $company_id="and emp.company_id='$company_id'";
			if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
			if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
			if ($section_id==0) $section_id=""; else $section_id="and emp.section_id='$section_id'";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and emp.subsection_id='$subsection_id'";
			if ($department_id==0) $department_id=""; else $department_id="and emp.department_id='$department_id'";
			if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
						
			if ($category==0) 
				{
					$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp LEFT JOIN hrm_weekend wek ON emp.emp_code=wek.emp_code where emp.status_active=1 $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id ORDER BY emp.id_card_no ASC";
				}
			else if($category==1) 
				{
					$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp, hrm_weekend wek where emp.emp_code=wek.emp_code and emp.status_active=1 $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id ORDER BY emp.id_card_no ASC";
				}
			else 
				{
					$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp where emp.emp_code NOT IN (select emp_code from hrm_weekend) and emp.status_active=1 $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id ORDER BY emp.id_card_no ASC";
				}					
			
			//echo $sql;die;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
			$emp_basic = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$emp_basic[$row['emp_code']] = array();
				foreach( $row AS $key => $value ) {
					$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
				}
			}			
			
			$sql="SELECT * FROM hrm_weekend where status_active=1 and is_deleted=0 and is_locked=0";             
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$weekend_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {				
					$weekend_details[$row['emp_code']] = mysql_real_escape_string( $row['emp_code'] );				
				}
				
			foreach( $emp_basic AS $emp ){         		
				
					if(in_array($emp['emp_code'],$weekend_details)){$bgcolor="";}
					else{ $bgcolor="bgcolor=#FF3900";}	
					
            ?>
			<tr>
				<td width="20" <? echo $bgcolor; ?> align="left"><input type="checkbox" name="chk_list" value="<?php echo $emp['emp_code']; ?>" onclick="count_emp(document.weekend_form.chk_list)" /></td>
				<td width="80"  <? echo $bgcolor; ?> align="center"><?php echo split_string($emp['emp_code'],9); ?></td>
				<td width="150" <? echo $bgcolor; ?>><?php echo split_string($emp[name],20); ?></td>
				<td width="50"><?php if( $emp['sex'] == 0 ) echo "Male"; else echo "Female"; ?></td>
				<td width="80"><?php if( $emp['id_card_no'] != '' ) echo split_string($emp['id_card_no'],11); ?></td>
				<td width="100"><?php if( $emp['punch_card_no'] != '' ) echo split_string($emp['punch_card_no'],10); ?></td>
				<td width="120"><?php if( $emp['designation_id'] != '0' ) echo split_string($designation_chart[$emp['designation_id']]['custom_designation'],15); ?></td>
				<td width="120"><?php if( $emp['company_id'] != '0' ) echo split_string($company_details[$emp['company_id']]['company_name'],18); ?></td>
				<td width="120"><?php if( $emp['location_id'] != '0' ) echo split_string($location_details[$emp['location_id']]['location_name'],18); ?></td>
				<td width="100"><?php if( $emp['division_id'] != '0' ) echo split_string($division_details[$emp['division_id']]['division_name'],15); ?></td>
				<td width="120"><?php if( $emp['department_id'] != '0' ) echo split_string($department_details[$emp['department_id']]['department_name'],15); ?></td>
				<td width="100"><?php if( $emp['section_id'] != '0' ) echo split_string($section_details[$emp['section_id']]['section_name'],15); ?></td>
				<td width="90"><?php if( $emp['subsection_id'] != '0' ) echo split_string($subsection_details[$emp['subsection_id']]['subsection_name'],12); ?></td>				
			</tr>
			<?php } ?>		
	</table>
 </div>
<?php
exit();
}

else if( isset( $form ) && ( $form == 'leave_entry' ) ) { 
	$exp_id_card=explode(",",$id_card);
	foreach($exp_id_card as $card){$card_no.="'".$card."',";}
	$id_card_no=substr($card_no,0,-1);
	//echo $id_card_no;die;
?>
<div style="width:1390px;" align="left">	
    <table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_employees" width="100%">
		<thead>
				<th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.leave_entry_form.chk_list)" placeholder="Search" /></th>
				<th width="80">Code<input type="text" name="emcode_filter" id="emcode_filter" class="text_boxes" style="width:60px" onkeyup="show_inner_filter_result(event);" value="<? if($empcode!='')echo $empcode; ?>" /></th>
				<th width="150">Name</th>
				<th width="50">Sex</th>
				<th width="80">ID Card</th>
				<th width="100">Punch Card</th>
				<th width="120">Designation</th>
                 <th width="90">Join Date</th> 
				<th width="120">Company</th>
				<th width="120">Location</th>
				<th width="100">Division</th>
				<th width="120">Department</th>
				<th width="100">Section</th>
				<th width="">Subsection</th>   
                              	
		</thead>
     </table>
 </div>
<div style="width:1390px; overflow-y:scroll; max-height:210px;" align="left">
	<table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_employees" width="100%">   
		
			<?php 
			$i=1;
			if ($company_id==0) $company_id=""; else $company_id="and company_id='$company_id'";
			if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
			if ($section_id==0) $section_id=""; else $section_id="and section_id in ($section_id)";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in ($subsection_id) ";
			if ($department_id==0) $department_id=""; else $department_id="and department_id in ($department_id) ";
			if ($division_id==0) $division_id=""; else $division_id="and division_id in ($division_id) ";
			if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in ($designation_id) ";
			if ($emp_code_cost==0) $emp_code_cost=""; else $emp_code_cost="and emp_code in($emp_code_cost)"; 
			if ($id_card=='') $id_card_no=""; else $id_card_no="and id_card_no in($id_card_no)";
			if ($category=='') $category =""; else $category ="and category='$category'";
			
			$sql = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 and is_deleted=0 $company_id $location_id $department_id $division_id $section_id $subsection_id $category $designation_id $id_card_no $emp_code_cost ORDER BY emp_code ASC";
			//echo $sql;die;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$emp_basic = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$emp_basic[$row['emp_code']] = array();
				foreach( $row AS $key => $value ) {
					$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
				}
			}			
			
			foreach( $emp_basic AS $emp ){             
           							
            ?>
			<tr>
				<td width="20" <? echo $bgcolor; ?> align="left"><input type="checkbox" name="chk_list" id="chk_list<? echo $i; ?>" value="<?php echo $emp['emp_code']; ?>" onclick="count_emp(document.leave_entry_form.chk_list,<? echo $i; ?>);" /></td><!--count_leave_balace(this.checked,'<?php //echo $emp[emp_code]; ?>',<? //echo $i; ?>);-->
				<td width="80"  <? echo $bgcolor; ?> align="center"><?php echo split_string($emp['emp_code'],9); ?></td>
                <td width="150" <? echo $bgcolor; ?>>
					<?php echo "<a href='#' onclick=\"openmypage_leave_bl('leave_balance_pop_up.php?emp_code=$emp[emp_code]&id_card_no=$emp[id_card_no]&name=$emp[name]&designation_id=$emp[designation_id]&joining_date=$emp[joining_date]',' Leave Balance'); return false\">". split_string($emp[name],20)."</a>"; ?>
                </td>
				<td width="50"><?php if( $emp['sex'] == 0 ) echo "Male"; else echo "Female"; ?></td>
				<td width="80" id="id_card_<? echo  $emp[emp_code]; ?>"><?php if( $emp['id_card_no'] != '' ) echo split_string($emp['id_card_no'],11); ?></td>
				<td width="100"><?php if( $emp['punch_card_no'] != '' ) echo split_string($emp['punch_card_no'],8); ?></td>
				<td width="120"><?php if( $emp['designation_id'] != '0' ) echo split_string($designation_chart[$emp['designation_id']]['custom_designation'],15); ?></td>
                <td width="90"><?php echo convert_to_mysql_date($emp['joining_date']); ?></td>
				<td width="120"><?php if( $emp['company_id'] != '0' ) echo split_string($company_details[$emp['company_id']]['company_name'],18); ?></td>
				<td width="120"><?php if( $emp['location_id'] != '0' ) echo split_string($location_details[$emp['location_id']]['location_name'],18); ?></td>
				<td width="100"><?php if( $emp['division_id'] != '0' ) echo split_string($division_details[$emp['division_id']]['division_name'],15); ?></td>
				<td width="120"><?php if( $emp['department_id'] != '0' ) echo split_string($department_details[$emp['department_id']]['department_name'],15); ?></td>
				<td width="100"><?php if( $emp['section_id'] != '0' ) echo split_string($section_details[$emp['section_id']]['section_name'],15); ?></td>
				<td width=""><?php if( $emp['subsection_id'] != '0' ) echo split_string($subsection_details[$emp['subsection_id']]['subsection_name'],12); ?></td>
               				
			</tr>
			<?php
				$i++; 
			} 
			?>		
	</table>
 </div>
<?php
exit();
}
else if( isset( $form ) && $form == 'maternity_leave_entry_emp_info' ) {
	$emp_info = array();
	$emp_info['name'] = $emp_basic[$code]['name'];
	$emp_info['designation'] = $designation_chart[$emp_basic[$code]['designation_id']]['custom_designation'];
	$emp_info['location'] = $location_details[$emp_basic[$code]['location_id']]['location_name'];
	$emp_info['division'] = $division_details[$emp_basic[$code]['division_id']]['division_name'];
	$emp_info['department'] = $department_details[$emp_basic[$code]['department_id']]['department_name'];
	$emp_info['section'] = $section_details[$emp_basic[$code]['section_id']]['section_name'];
	
	print_r( serialize( $emp_info ) );
	exit();
}
 /*
<table cellpadding="0" cellspacing="0" border="0" class="display">
	<thead>
		<tr>
			<th>Code</th>
			<th>Employee Name</th>
			<th>ID Card</th>
			<th>Punch Card</th>
			<th>Designation</th>
			<th>Company</th>
			<th>Location</th>
			<th>Division</th>
			<th>Department</th>
			<th>Section</th>
			<th>Subsection</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach( $emp_basic AS $emp ) {
			if( ( $criteria11 == 0 || ( $criteria11 != 0 && (
					( $criteria1 == 0 && $criteria11 == $emp['company_id'] )
					|| ( $criteria1 == 1 && $criteria11 == $emp['location_id'] )
					|| ( $criteria1 == 2 && $criteria11 == $emp['division_id'] )
					|| ( $criteria1 == 3 && $criteria11 == $emp['department_id'] )
					|| ( $criteria1 == 4 && $criteria11 == $emp['section_id'] )
					|| ( $criteria1 == 5 && $criteria11 == $emp['subsection_id'] )
				) ) )
				&& ( $criteria22 == '' || ( $criteria22 != '' && (
					( $criteria2 == 0 && stristr( $emp['emp_code'], $criteria22 ) != false )
					|| ( $criteria2 == 1 && stristr( $emp['name'], $criteria22 ) != false )
					|| ( $criteria2 == 2 && stristr( $designation_chart[$emp['designation_id']]['custom_designation'], $criteria22 ) != false )
					|| ( $criteria2 == 3 && stristr( $emp['national_id'], $criteria22 ) != false )
					|| ( $criteria2 == 4 && stristr( $emp['id_card_no'], $criteria22 ) != false )
					|| ( $criteria2 == 5 && stristr( $emp['passport_no'], $criteria22 ) != false )
					|| ( $criteria2 == 6 && stristr( $emp['punch_card_no'], $criteria22 ) != false )
				) ) )
				&& $emp['status_active'] == 1
			) {
		?>
		<tr id="<?php echo $emp['emp_code']; ?>" class="gradeA">
			<td><?php echo $emp['emp_code']; ?></td>
			<td><a href="javascript:show_employee('<?php echo $emp['emp_code']; ?>')"><?php echo $emp['name']; ?></a></td>
			<td><?php echo $emp['id_card_no']; ?></td>
			<td><?php echo $emp['punch_card_no']; ?></td>
			<td><?php if( $emp['designation_id'] != 0 ) echo $designation_chart[$emp['designation_id']]['custom_designation']; ?></td>
			<td><?php if( $emp['company_id'] != 0 ) echo $company_details[$emp['company_id']]['company_name']; ?></td>
			<td><?php if( $emp['location_id'] != '0' ) echo $location_details[$emp['location_id']]['location_name']; ?></td>
			<td><?php if( $emp['division_id'] != '0' ) echo $division_details[$emp['division_id']]['division_name']; ?></td>
			<td><?php if( $emp['department_id'] != '0' ) echo $department_details[$emp['department_id']]['department_name']; ?></td>
			<td><?php if( $emp['section_id'] != '0' ) echo $section_details[$emp['section_id']]['section_name']; ?></td>
			<td><?php if( $emp['subsection_id'] != '0' ) echo $subsection_details[$emp['subsection_id']]['subsection_name']; ?></td>
		</tr>
		<?php
			}
		}
		?>
	</tbody>
</table><?php */

// training_record
else if( isset( $form ) && $form == 'training_record' ) { ?>
<div style="width:1430px;" align="left">	
    <table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="">
		<thead>			
				<th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.frm_training_record.chk_list)" placeholder="Search" /></th>
				<th width="50">Sl</th>
				<th width="80">ID Card</th>
                <th width="80">Code</th>
				<th width="90">Punch Card</th>
				<th width="170">Name</th>
				<th width="100">Designation</th>
				<th width="100">Department</th>
				<th width="80">DOJ</th>
				<th width="150">Company</th>
				<th width="100">Location</th>
				<th width="100">Division</th>
				<th width="100">Section</th>
				<th width="100">Subsection</th>
		</thead>
	</table>
</div>
<div style="width:1430px; overflow-y:scroll; max-height:210px;" align="left">
	<table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_training_list_view">
     <tbody>			

			<?php
			if ($category=='') $category =""; else $category ="and category='$category'";
			if ($company_id==0) $company_id=""; else $company_id="and company_id='$company_id'";
			if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
			if ($division_id==0) $division_id=""; else $division_id="and division_id='$division_id'";
			if ($department_id==0) $department_id=""; else $department_id="and department_id='$department_id'";
			if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
			if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
			if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
			if ($empcode=='') $empcode=""; else $empcode="and emp_code='$empcode'";
			//if ($id_card=="") $id_card=""; else $id_card="and id_card_no like '%$id_card%'";
			// 	
					
			//$sql = "SELECT emp.*,max(sep.separated_from) as separated_from, sep.separation_type, sep.cause_of_separation,sep.id as sepid, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp, hrm_separation sep where emp.status_active=0 and emp.emp_code=sep.emp_code $company_id $location_id $department_id $section_id $subsection_id $category $designation_id $emp_code group by sep.emp_code ORDER BY sep.id DESC, emp.id_card_no ASC";
			
			$sql="SELECT id_card_no,emp_code,punch_card_no,CONCAT(first_name, ' ',middle_name, ' ',last_name) AS name,designation_id,department_id,joining_date,company_id,location_id,division_id,section_id,subsection_id FROM hrm_employee WHERE status_active=1 $category $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id $empcode ORDER BY emp_code ASC";

			//echo $sql; die; 
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
 			$sl=1;
			while( $row = mysql_fetch_assoc( $result ) ) { ?>
			<tr>
				<td width="20"><input type="checkbox" name="chk_list" id="chk_list" value="<?php echo $row['emp_code'].",".$row['designation_id'] ?>" onclick="count_emp(document.frm_training_record.chk_list)" /></td>
				<td width="50"><?php echo $sl; ?></td>
				<td width="80"><?php if( $row['id_card_no'] != '' ) echo split_string($row['id_card_no'],10); ?></td>
                <td width="80"><?php echo $row['emp_code']; ?></td>
				<td width="90"><?php if( $row['punch_card_no'] != '' ) echo split_string($row['punch_card_no'],11); ?></td>
				<td width="170"><?php echo split_string($row['name'],15); ?></td>
				<td width="100"><?php if( $row['designation_id'] != '0' ) echo split_string($designation_chart[$row['designation_id']]['custom_designation'],12); ?></td>
				<td width="100"><?php if( $row['department_id'] != '0' ) echo split_string($department_details[$row['department_id']]['department_name'],12); ?></td>
				<td width="80" align="center"><?php if( $row['joining_date'] != '0000-00-00' ) echo convert_to_mysql_date($row['joining_date']); ?></td>
				<td width="150"><?php if( $row['company_id'] != '0' ) echo split_string($company_details[$row['company_id']]['company_name'],12); ?></td>
				<td width="100"><?php if( $row['location_id'] != '0' ) echo split_string($location_details[$row['location_id']]['location_name'],12); ?></td>
				<td width="100"><?php if( $row['division_id'] != '0' ) echo split_string($division_details[$row['division_id']]['division_name'],10); ?></td>
				<td width="100"><?php if( $row['section_id'] != '0' ) echo split_string($section_details[$row['section_id']]['section_name'],10); ?></td>
				<td width="100"><?php if( $row['subsection_id'] != '0' ) echo split_string($subsection_details[$row['subsection_id']]['subsection_name'],10); ?></td>
			</tr>
			<?php $sl++; } ?>
            </tbody>		
	</table>
</div>    
<?php
exit();
}

//================================================================================holiday_allowance start===============================================================
else if( isset( $form ) && ( $form == 'holiday_emp_populate' ) ) 
{ 
?>
<div style="width:1300px;" align="left">	
    <table cellpadding="0" cellspacing="0" border="0" class="rpt_table">
		<thead>
				<th width="20" align="left"><input type="checkbox" name="Check_ctr" id="Check_ctr" onclick="Check(document.weekend_form.chk_list)" placeholder="Search" /></th>
				<th width="80">Code</th>
				<th width="150">Name</th>
				<th width="50">Sex</th>
				<th width="80">ID Card</th>
				<th width="100">Punch Card</th>
				<th width="120">Designation</th>
				<th width="120">Company</th>
				<th width="120">Location</th>
				<th width="100">Division</th>
				<th width="120">Department</th>
				<th width="100">Section</th>
				<th width="90">Subsection</th>                	
		</thead>
     </table>
 </div>
<div style="width:1300px; overflow-y:scroll; max-height:210px;" align="left">
	<table cellpadding="0" cellspacing="0" border="0" class="rpt_table" id="tbl_employees">   
			<?php
			$hodate_arr=explode(',',$hodate);
			$hodate='';
			for ($i=0;$i<count($hodate_arr);$i++)
			{
			$hodate=convert_to_mysql_date( $hodate_arr[$i] ); 
			}
			
			if ($company_id==0) $company_id=""; else $company_id="and emp.company_id='$company_id'";
			if ($location_id==0) $location_id=""; else $location_id="and emp.location_id='$location_id'";
			if ($division_id==0 || $division_id=='') $division_id=""; else $division_id="and division_id in($division_id)";
			if ($section_id==0 || $section_id=='') $section_id=""; else $section_id="and emp.section_id in($section_id)";
			if ($subsection_id==0 || $subsection_id=='') $subsection_id=""; else $subsection_id="and emp.subsection_id in($subsection_id)";
			if ($department_id==0 || $department_id=='') $department_id=""; else $department_id="and emp.department_id in($department_id)";
			if ($designation_id==0 || $designation_id=='') $designation_id=""; else $designation_id="and emp.designation_id in($designation_id)";
			//if ($hodate=='') $hodate=""; else $hodate="and holiday_date ='$hodate'";
						
			if ($category==0) 
				{
					$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp LEFT JOIN lib_holiday_details hol ON emp.emp_code=hol.emp_code where emp.status_active=1 $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id ORDER BY emp.id_card_no ASC";
				}
			else if($category==1) 
				{
					$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp, lib_holiday_details hol where emp.emp_code=hol.emp_code and emp.status_active=1 $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id ORDER BY emp.id_card_no ASC";
				}
			else 
				{
					$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name FROM hrm_employee emp where emp.emp_code NOT IN (select emp_code from lib_holiday_details) and emp.status_active=1 $company_id $location_id $division_id $department_id $section_id $subsection_id $designation_id ORDER BY emp.id_card_no ASC";
				}					
			
			//echo $sql;die;
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );		
			$emp_basic = array(); //$emp_code='';
			while( $row = mysql_fetch_assoc( $result ) ) {
				$emp_basic[$row['emp_code']] = array();
				//if($emp_code=="") $emp_code=$row['emp_code']; else $emp_code.=",".$row['emp_code'];
				foreach( $row AS $key => $value ) {
					$emp_basic[$row['emp_code']][$key] = mysql_real_escape_string( $value );
				}
			}			
			
		     $sql="SELECT * FROM lib_holiday_details where status_active=1 and is_deleted=0 and holiday_date ='$hodate' ";   //holiday_date  //and emp_code IN ($emp_code) $hodate        
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$weekend_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {				
					$weekend_details[$row['emp_code']] = mysql_real_escape_string( $row['emp_code'] );
					//$weekend_date[$row['holiday_date']] = mysql_real_escape_string( $row['holiday_date'] );				
				}
				
			foreach( $emp_basic AS $emp ){         		
				
					if(in_array($emp['emp_code'],$weekend_details) ){$bgcolor="";}
					else{ $bgcolor="bgcolor=#FF3900";}	
					
            ?>
			<tr>
				<td width="20" <? echo $bgcolor; ?> align="left"><input type="checkbox" name="chk_list" value="<?php echo $emp['emp_code']; ?>" onclick="count_emp(document.weekend_form.chk_list)" /></td>
				<td width="80"  <? echo $bgcolor; ?> align="center"><?php echo split_string($emp['emp_code'],9); ?></td>
				<td width="150" <? echo $bgcolor; ?>><?php echo split_string($emp[name],20); ?></td>
				<td width="50"><?php if( $emp['sex'] == 0 ) echo "Male"; else echo "Female"; ?></td>
				<td width="80"><?php if( $emp['id_card_no'] != '' ) echo split_string($emp['id_card_no'],11); ?></td>
				<td width="100"><?php if( $emp['punch_card_no'] != '' ) echo split_string($emp['punch_card_no'],10); ?></td>
				<td width="120"><?php if( $emp['designation_id'] != '0' ) echo split_string($designation_chart[$emp['designation_id']]['custom_designation'],15); ?></td>
				<td width="120"><?php if( $emp['company_id'] != '0' ) echo split_string($company_details[$emp['company_id']]['company_name'],18); ?></td>
				<td width="120"><?php if( $emp['location_id'] != '0' ) echo split_string($location_details[$emp['location_id']]['location_name'],18); ?></td>
				<td width="100"><?php if( $emp['division_id'] != '0' ) echo split_string($division_details[$emp['division_id']]['division_name'],15); ?></td>
				<td width="120"><?php if( $emp['department_id'] != '0' ) echo split_string($department_details[$emp['department_id']]['department_name'],15); ?></td>
				<td width="100"><?php if( $emp['section_id'] != '0' ) echo split_string($section_details[$emp['section_id']]['section_name'],15); ?></td>
				<td width="90"><?php if( $emp['subsection_id'] != '0' ) echo split_string($subsection_details[$emp['subsection_id']]['subsection_name'],12); ?></td>				
			</tr>
			<?php } ?>		
	</table>
 </div>
<?php
exit();
}

function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata";
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	
	return $m_data ;
}
?>
