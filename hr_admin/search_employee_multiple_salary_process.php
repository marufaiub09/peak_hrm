<? 
session_start();
include('../includes/common.php');
include('../includes/array_function.php');

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
 	$search_string=$_GET["m_company"];
	$m_buyer=$_GET["m_buyer"];
	
	extract($_REQUEST);
 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/functions.js"></script>

<link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../includes/tablefilter.js"></script>

<script>

//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


function search_populate(str)
{
	 //alert(str);
	if(str==0)
	{
		document.getElementById('search_by_td_up').innerHTML="System Code";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" />';
	}
	else if(str==1)
	{
		document.getElementById('search_by_td_up').innerHTML="Employee Name";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==2)
	{
		var designation = '<option value="0">--- Select ---</option>';
		<?php
		$designation_sql= mysql_db_query($DB, "select * from lib_designation where is_deleted=0  and status_active=1 order by level ");
		while ($row=mysql_fetch_array($designation_sql))
		{
			echo "designation += '<option value=\"$row[id]\">".mysql_real_escape_string($row[custom_designation])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Designation";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ designation +'</select>'; 
	}	
	else if(str==3)
	{
		var division = '<option value="0">--- Select ---</option>';
		<?php
		$division_sql= mysql_db_query($DB, "select * from lib_division where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($division_sql))
		{
			echo "division += '<option value=\"$row[id]\">".mysql_real_escape_string($row[division_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Division";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ division +'</select>'; 
	}	
	else if(str==4)
	{
		var department = '<option value="0">--- Select ---</option>';
		<?php
		$department_sql= mysql_db_query($DB, "select * from lib_department where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($department_sql))
		{
			echo "department += '<option value=\"$row[id]\">".mysql_real_escape_string($row[department_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Department";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ department +'</select>'; 
	}	
   else if(str==5)
	{
		var section = '<option value="0">--- Select ---</option>';
		<?php
		$section_sql= mysql_db_query($DB, "select * from lib_section where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($section_sql))
		{
			echo "section += '<option value=\"$row[id]\">".mysql_real_escape_string($row[section_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Section";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ section +'</select>'; 
	}
	else if(str==6)
	{
		document.getElementById('search_by_td_up').innerHTML="ID Card No";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" />';
	}
	else if(str==7)
	{
		document.getElementById('search_by_td_up').innerHTML="Punch Card No";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" />';
	}
	else if(str==8)
	{
		document.getElementById('search_by_td_up').innerHTML="System Code";	
		document.getElementById('search_field_td_up').innerHTML='<input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:180px" onkeypress="return numbersonly(this,event)" />';
	}
	
	//new add
else if(str==9)
	{
		var location = '<option value="0">--- Select ---</option>';
		<?php
		$location_sql= mysql_db_query($DB, "select * from lib_location where is_deleted=0  and status_active=1 order by id ");
		while ($row=mysql_fetch_array($location_sql))
		{
			echo "location += '<option value=\"$row[id]\">".mysql_real_escape_string($row[location_name])."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Location";
		document.getElementById('search_field_td_up').innerHTML='<select	name="txt_search_common"	style="width:190px " class="combo_boxes" id="txt_search_common"	onchange="" >'+ location +'</select>'; 
	}	
	
	
	
	
	
}
var salaryreproces='<? echo $salaryreproces; ?>';

function fn_check()
	{
		var category=document.getElementById('cbo_category_name').value;
		var company=document.getElementById('cbo_company_name').value;
		var search_by=document.getElementById('cbo_search_by').value;
		var search_field=document.getElementById('txt_search_common').value;
		
		var location_id=$('#location_id').val();
		var division_id=$('#division_id').val();
		var department_id=$('#department_id').val();
		var section_id=$('#section_id').val();
		var subsection_id=$('#subsection_id').val();
		var designation_id=$('#designation_id').val();		
		//alert(company+"_"+category+"_"+search_by+"_"+location_id+"_"+division_id+"_"+department_id+"_"+section_id+"_"+subsection_id+"_"+designation_id);
				
		var param=company+"_"+category+"_"+search_by+"_"+location_id+"_"+division_id+"_"+department_id+"_"+section_id+"_"+subsection_id+"_"+designation_id+"_empinfoform_"+salaryreproces;
		
		if(company==0){ alert("Please Select Company Name."); return false;}
		else{
			showResult_multi(search_field,param,'salary_process_emp_search','td_show_result');
		}
		 hide_search_panel(); 
	}
	
var selected_id = new Array, selected_name = new Array();
		function check_all_data() {
			var tbl_row_count = document.getElementById( 'table_body' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			//alert(str);
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id =''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}


</script>
</head>
<body>
<div align="center">
 <h3 align="left" style="top:1px;width:100%" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Employee </h3>
    
<form name="search_order_frm"  id="search_order_frm" autocomplete="off">
	<fieldset style="width:800px">
    <div id="content_search_panel" > 
	<table class="rpt_table" width="800" cellspacing="2" cellpadding="0" border="0">
		<thead>
            <th align="center"><strong>Category</strong></th>
            <th align="center"><strong>Company</strong></th>
            <th align="center"><strong>Location</strong></th>
            <th align="center"><strong>Division</strong></th>
            <th align="center"><strong>Department</strong></th>
		</thead>
        <tr class="general">
          <td align="center">
          	<select name="cbo_category_name" id="cbo_category_name" class="combo_boxes"  style="width:120px">
                <option value="">-- Select --</option>
                <?
                            foreach($employee_category as $key=>$val)
                            {
                        ?>
                <option value="<? echo $key; ?>" ><? echo $val; ?></option>
                <?
                            }
                        ?>
             </select>
          </td>
          <td align="center">
         
          <select name="cbo_company_name" id="cbo_company_name" class="combo_boxes"  style="width:120px">
          	<? if($company_cond=="")
				{ 
			?>
            <option value="0">-- Select --</option>
            <?
				}
            		$sql= mysql_db_query($DB, "select * from lib_company where status_active=1 and is_deleted=0 $company_cond order by company_name");
                	$numrow=mysql_num_rows($sql);
					while ($selectResult = mysql_fetch_array($sql))
						{
					?>
            <option value="<?php echo $selectResult["id"]; ?>" <? if($numrow==1)echo "selected"; ?> ><?php echo $selectResult["company_name"]; ?></option>
            <?
						}
					?>
          </select></td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:120px" >
                          <option value="0">-- Select --</option>
                          <?php  
                                        $sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
                                        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                                        while( $row = mysql_fetch_assoc( $result ) ) {										
                                    ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['location_name']; ?></option>
                          <?php 
                                        } 
                                    ?>
                      </select>
                  </td>
                  <td>
						<select name="division_id" id="division_id" class="combo_boxes" style="width:120px">
							<option value="0">-- Select --</option>
                            <?php  
									$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
									while( $row = mysql_fetch_assoc( $result ) ) {										
								?>
                            <option value="<?php echo $row['id']; ?>"><?php echo $row['division_name']; ?></option><?php }?>
						</select>
                    </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:120px">
							<option value="0">-- Select --</option>
								<?php  
									$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
									while( $row = mysql_fetch_assoc( $result ) ) {										
								?>							
							<option value="<?php echo $row['id']; ?>"><?php echo $row['department_name']; ?></option>
								<?php 
									} 
								?> 
						</select>
					</td>
                 </tr>
                 <thead>
                 		<th align="center"><strong>Section</strong></th>
                        <th align="center"><strong>Sub Section</strong></th>
                        <th align="center"><strong>Designation</strong></th>
                        <th align="center"><strong>Search By</strong></th>
                        <th align="center" id="search_by_td_up"><strong>System Code</strong></th>
            	          
                 </thead>
                 <tr>
                        <td align="center">
                            <select name="section_id" id="section_id" class="combo_boxes" style="width:120px">
                                <option value="0">-- Select --</option>
                                    <?php  
                                        $sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
                                        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                                        while( $row = mysql_fetch_assoc( $result ) ) {										
                                    ?>							
                                <option value="<?php echo $row['id']; ?>"><?php echo $row['section_name']; ?></option>
                                    <?php 
                                        } 
                                    ?> 
                            </select>
                        </td>          
                       <td align="center" id="subsection">
                            <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px">
                                <option value="0">-- Select --</option>
                                    <?php  
                                        $sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
                                        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                                        while( $row = mysql_fetch_assoc( $result ) ) {										
                                    ?>							
                                <option value="<?php echo $row['id']; ?>"><?php echo $row['subsection_name']; ?></option>
                                    <?php 
                                        } 
                                    ?> 
                            </select>
                        </td>
                        <td align="center" id="designation">
                            <select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" >
                                <option value="0">-- Select --</option>
                                    <?php  
                                        $sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
                                        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                                        while( $row = mysql_fetch_assoc( $result ) ) {										
                                    ?>							
                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['custom_designation']; ?></option>
                                    <?php 
                                        } 
                                    ?> 
                            </select>
                        </td>
              
              <td align="center">
              <select name="cbo_search_by" id="cbo_search_by" class="combo_boxes" style="width:120px" onchange="search_populate(this.value)">
                <option value="0">Employee Code</option>
                <option value="6">Employee ID</option>
                <option value="1">Employee Name</option>
                <option value="2">Designation</option>
                 <option value="9">Location</option>
                <option value="3">Division</option>
                <option value="4">Department</option>
                <option value="5">Section</option>
                <option value="7">Punch Card</option>
                <option value="8">Separated</option>
               
              </select></td>
              <td align="center" id="search_field_td_up"><input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes"  style="width:120px" placeholder="Enter Here" onkeypress="return numbersonly(this,event)" />
              </td>
        </tr>
       
        <tr>
              <td  colspan="5" align="center">
                <input type="button" name="show_button" id="show_button" class="formbutton" value="Show" onclick="fn_check()" style="width:100px;" />
                <input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" />
               </td>
                <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" /><!-- Hidden field -->
		</tr>
   </table>
 </div>
   <div style="border:none" id="td_show_result"></div>
   <table width="930">
       	<tfoot>
			<th align="center" height="30" valign="bottom">
					<div style="width:100%"> 
							<div style="width:50%; float:left" align="left">
								<input type="checkbox" name="check_all" id="check_all" onclick="check_all_data()" /> Check / Uncheck All
                                <input type="hidden" readonly="readonly" name="txt_selected_id" id="txt_selected_id" /> 
                                <input type="hidden" name="txt_selected_emp" id="txt_selected_emp" />
                                <input type="hidden" readonly="readonly" style="width:250px" class="text_boxes" name="txt_selected" id="txt_selected" />
 							</div>
							<div style="width:50%; float:left" align="left">
							<input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px"/>
							</div>
					</div>
             </th>
		</tfoot>
	</table>
 		</fieldset>
	</form> 
       
</div>
</body>
</html>