<?php
	include('includes/common.php');
	
	echo "Truncating process started.....";
	
	$hardcoded_tables = array(
							'lib_b2b_lc_type',
							'lib_designation',
							'lib_fabric',
							'lib_fabric_construction',
							'lib_garments_item',
							'lib_inco_term',
							'lib_lc_type',
							'lib_list_country',
							'lib_list_currency',
							'lib_list_district',
							'lib_list_division',
							'lib_payment_mode',
							'lib_payroll_head',
							'lib_shipment_mode',
							'lib_trade_term',
							'lib_transport_mode',
							'lib_trim',
							'lib_unit',
							'lib_yarn_count',
							'lib_yarn_type',
							'main_menu',
							'main_module',
							'tbl_device_setup',
							'user_passwd',
							'user_priv_module',
							'user_priv_mst'
						);
	$truncate_tables = array(
							'lib_style',
							'lib_bank',
							'activeemployeefordevice',
							'com_btb_lc_master_details',
							'com_btb_master_lc_details',
							'com_btb_pi_details',
							'com_export_lc',
							'com_export_lc_amendment_info',
							'com_export_lc_attached_sales_contract_info',
							'com_export_lc_order_amend_info',
							'com_export_lc_order_info',
							'com_pi_item_details',
							'com_pi_master_details',
							'com_sales_contract',
							'com_sales_contract_order_info',
							'common_photo_library',
							'hrm_employee',
							'hrm_employee_address',
							'hrm_employee_education',
							'hrm_employee_entitlement',
							'hrm_employee_experience',
							'hrm_employee_family',
							'hrm_employee_job',
							'hrm_employee_language',
							'hrm_employee_nominee',
							'hrm_employee_promotion',
							'hrm_employee_reference',
							'hrm_employee_salary',
							'hrm_employee_salary_bank',
							'hrm_employee_suspension',
							'hrm_employee_training',
							'hrm_employee_transfer',
							'hrm_leave_balance',
							'hrm_leave_transaction',
							'hrm_maternity_leave',
							'hrm_policy_tagging',
							'hrm_salary_pay_days',
							'hrm_separation',
							'hrm_separation_history',
							'hrm_weekend',
							'lib_buyer',
							'lib_company',
							'lib_department',
							'lib_division',
							'lib_dying_chemical',
							'lib_group',
							'lib_holiday',
							'lib_location',
							'lib_marketing_team',
							'lib_marketing_team_member_info',
							'lib_policy_leave',
							'lib_policy_maternity_leave',
							'lib_policy_overtime',
							'lib_policy_overtime_slab',
							'lib_policy_leave_definition',
							'lib_policy_salary_breakdown',
							'lib_policy_salary_breakdown_definition',
							'lib_policy_shift',
							'lib_policy_year',
							'lib_policy_year_periods',
							'lib_sample',
							'lib_section',
							'lib_subsection',
							'lib_supplier',
							'resource_photo',
							'tbl_card',
							'time_schedule',
							'wo_color_wise_quanty_info',
							'wo_contrast_color_wise_quanty_info',
							'wo_details_info',
							'wo_knitting_dyeing_program_dtls',
							'wo_knitting_dyeing_program_mst',
							'wo_order_info',
							'wo_po_accessories_approval_info',
							'wo_po_accessories_approval_info_history',
							'wo_po_break_down',
							'wo_po_color_info',
							'wo_po_color_size_breakdown',
							'wo_po_cost_commercial_dtls',
							'wo_po_cost_commission_dtls',
							'wo_po_cost_details_mst',
							'wo_po_cost_embellishment_dtls',
							'wo_po_cost_fabric_dtls',
							'wo_po_cost_trims_dtls',
							'wo_po_details_master',
							'wo_po_lapdip_approval_info',
							'wo_po_lapdip_approval_info_history',
							'wo_po_measurement_bottom_details',
							'wo_po_measurement_top_details',
							'wo_po_sample_approval_info',
							'wo_po_sample_approval_info_history',
							'wo_po_set_details_info',
							'wo_po_size_info',
							'wo_po_yarn_info_details',
							'wo_size_wise_quanty_info',
							'wo_terms_info'
						);
	
	for( $i = 0; $i < count( $truncate_tables ); $i++ ) {
		$sql = "TRUNCATE TABLE `" . $truncate_tables[$i] . "`";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	}
	
	echo "Turncating process ended successfully.";
?>