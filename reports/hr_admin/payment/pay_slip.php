<?php 
// Ekram
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1){
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');
include('../../../includes/common_functions.php');

//echo $proxy_ip=get_ip_mac("tracert");
//echo $_SERVER['REMOTE_ADDR'];

 $sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM lib_policy_year WHERE type=1 ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year[$row['id']] = mysql_real_escape_string( $row['name'] );		
	}

	$sql = "SELECT * FROM lib_policy_year_periods ORDER BY id ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$policy_year_periods = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$policy_year_periods[$row['id']] = mysql_real_escape_string( $row['starting_date'] );		
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    <script type="text/javascript" src="../../../resources/jquery-1.6.2.js"></script>
    <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>
    <script src="../../../resources/multiselect/multi_select.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
    <link href="../../../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        
		<script>
            $(document).ready(function(e) {
                $(".datepicker").datepicker({
                        dateFormat: 'dd-mm-yy',
                        changeMonth: true,
                        changeYear: true
                    });
            });
			
				
$(document).ready(function(e) {
	  $("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	
	$("#division_id,#department_id,#section_id,#subsection_id,#designation_id").multiselectfilter({
	
	});
 });
            
            //onchange="populate_cost_center( 'location', this.value );"
           function populate_select_month( selected_id ) {
			$.ajax({
				type: "GET",
				url: "includes/pay_slip_report_buyer_peak.php?",
				data: 'type=select_month_generate&id=' + selected_id,
				success: function( html ) {
					$('#cbo_month_selector').html( html )
				}
			});
		}
                        
             
            function generate_salary_report()
            {
                $("#messagebox").removeClass().addClass('messagebox').text('Generating Report....').fadeIn(1000);
                var error = false;
                div="report_container";
                data_panel="data_panel";
                if( $('#cbo_month_selector').val()*1 == "0" ) {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#cbo_month_selector').focus();
                        $(this).html('Please select The month.').addClass('messageboxerror').fadeTo(900,1);
                    });
                }
				
				 if( $('#company_id').val()*1 == 0 ) {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#company_id').focus();
                        $(this).html('Please select A Company.').addClass('messageboxerror').fadeTo(900,1);
                    });
                }
                
                if($('#single_print').is(':checked'))
                {
                    var chk_box=1;
                }
                else
                {
                var chk_box=0;	
                }
                
                if( error == false )
                {
                var data = '&txt_date=' + $('#txt_date').val() 
                            + '&cbo_year_selector=' + $('#cbo_year_selector').val() 
                            + '&cbo_month_selector=' + $('#cbo_month_selector').val() 
                            + '&cbo_emp_category=' + $('#cbo_emp_category').val()
                            + '&company_id=' + $('#company_id').val() 
                            + '&location_id=' + $('#location_id').val() 
                            + '&division_id=' + $('#division_id').val() 
                            + '&department_id=' + $('#department_id').val()
                            + '&section_id=' + $('#section_id').val() 
                            + '&subsection_id=' + $('#subsection_id').val() 
                            + '&designation_id=' + $('#designation_id').val()
                            + '&cbo_salary_sheet='+$('#cbo_salary_sheet').val()
                            + '&id_card='+$('#id_card').val() 
							+ '&group_by_id='+$('#group_by_id').val()
							+ '&order_by_id='+$('#order_by_id').val() 
                            +'&emp_code='+$('#emp_code').val();
                
                //alert(data);
                if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp=new XMLHttpRequest();
                }
                else{// code for IE6, IE5
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200)
                    {			
                        var data_split=(xmlhttp.responseText).split('####');
                        var link_data=data_split[1];			
                        //alert(link_data);
                       // $('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + data_split[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
                        //$('#data_panel').append('&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
					   $('#data_panel').html('&nbsp;&nbsp;&nbsp;<br><input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>');
					   $('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="print_report()" value="Print" name="Print" class="formbutton" style="width:100px"/>' );
                        
                        document.getElementById(div).innerHTML=data_split[0];
                        $("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
                        { 
                             $(this).html('Report Generated Successfully...').addClass('messageboxerror').fadeTo(900,1);
                        });
                         
                    }
                } 
				xmlhttp.open("GET","includes/pay_slip_report_buyer_peak.php?types=5"+"&data="+data+"&single_print="+chk_box,true);
				// xmlhttp.open("GET","includes/pay_slip_report_fakir_appearls.php?type=pay_slip"+"&data="+data+"&single_print="+chk_box,true); 	
               // xmlhttp.open("GET","includes/pay_slip_report_buyer_peak.php?type=pay_slip"+"&data="+data+"&single_print="+chk_box,true);
                xmlhttp.send();
                }
            }
			
            function new_window(){
                var w = window.open("Surprise", "#");
                var d = w.document.open();
                d.write(document.getElementById('report_container').innerHTML);
                d.close();
            }
			
function print_report()
{
	$('#data_panel').hide();
	$('#messagebox').hide();
	$('#accordion_h1').hide();
	
  	window.print(document.getElementById('report_container').innerHTML);
	
	$('#data_panel').show();
	$('#messagebox').show();
	$('#accordion_h1').show();
}			
            
            function new_window_short(){
                var w = window.open("Surprise", "#");
                var d = w.document.open();
                d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
                '<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
                d.close();
            }
                
            function openmypage_employee_info(page_link,title){			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1100px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function(){
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id");				
                    $('#emp_code').val(thee_id.value);
                }
            }
            
            function openmypage_employee_info_id_card(page_link,title){			
                emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
                emailwindow.onclose=function(){
                    var thee_loc = this.contentDoc.getElementById("txt_selected");
                    var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
                    $('#id_card').val(thee_id.value);
                }
            }
        
            //Numeric Value allow field script
            function numbersonly(myfield, e, dec){
                var key;
                var keychar;
            
                if (window.event) key = window.event.keyCode;
                else if (e) key = e.which;
                else return true;
                keychar = String.fromCharCode(key);
            
                // control keys
                if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
                return true;
                
                // numbers
                else if ((("0123456789.,-").indexOf(keychar) > -1)) return true;
                else return false;
            } 
        </script>
        
        <style media="print">
            p{page-break-before:always}
        </style>	
    </head>
    
    <body>
    <div align="left" style="width:100%;">
            <div align="center">
                <div class="form_caption">Pay Slip</div>
                <div align="center" id="messagebox"></div>
            </div>
        <form>
            <fieldset>
                <div align="center">
                    <strong>Pay Slip Date</strong><input type="text" id="txt_date" class="datepicker" />
                    <input type="checkbox" name="single_print" id="single_print" />Single Print
                </div>
                <table align="center" width="1000" cellpadding="0" cellspacing="0" class="rpt_table">
                    <thead>
                        <th><strong>Category</strong></th>
                        <th><strong>Company</strong></th>
                        <th><strong>Location</strong></th>
                        <th><strong>Division</strong></th>
                        <th><strong>Department</strong></th>
                        <th><strong>Section</strong></th>
                        <th><strong>SubSection</strong></th>
                        <th><strong>Designation</strong></th>
                     </thead>
                    <tr class="general">
                        <td>
                             <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes">
                             	 <option value="">All Category</option>
                                    <?
									//$employee_category_pay_slip=array(2=>"Non Management");
									 foreach($employee_category as $key=>$val){ ?>
                                    <option value="<? echo $key; ?>"><? echo $val; ?></option>  <? } ?> 
                             </select>
                        </td>
                        <td>
                            <select name="company_id" id="company_id" class="combo_boxes" style="width:120px">
                                <? if($company_cond=="") { ?>
                                <option value="0">-- Select --</option>
                                <?php } foreach( $company_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>" <? if(count($company_details)==1) echo "selected"; ?>><?php echo $value; ?></option> <?php } ?>
                            </select>
                        </td>
                        <td>
                            <select name="location_id" id="location_id" class="combo_boxes" style="width:120px">
                                <option value="0">-- Select --</option>
                                <?php foreach( $location_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td>
                            <select name="division_id" id="division_id" class="combo_boxes" style="width:110px" multiple="multiple">
                               <!-- <option value="0">-- Select --</option>-->
                                <?php foreach( $division_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td>
                            <select name="department_id" id="department_id" class="combo_boxes" style="width:120px" multiple="multiple">
                               <!-- <option value="0">-- Select --</option>-->
                                <?php foreach( $department_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td>
                            <select name="section_id" id="section_id" class="combo_boxes" style="width:120px" multiple="multiple">
                               <!-- <option value="0">-- Select --</option>-->
                                <?php foreach( $section_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td id="subsection">
                            <select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:120px" multiple="multiple">
                               <!-- <option value="0">-- Select --</option>-->
                                <?php foreach( $subsection_details AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                        <td id="designation">
                            <select name="designation_id" id="designation_id" class="combo_boxes" style="width:120px" multiple="multiple" >
                               <!-- <option value="0">-- Select --</option>-->
                                <?php foreach( $designation_chart AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td colspan="8">&nbsp;</td></tr>
             </table>
              <table align="center" width="700" cellpadding="0" cellspacing="0" class="rpt_table">
                    <thead>
                        <th><strong>Year</strong></th>
                        <th><strong>Month</strong></th>
                        <th><strong>Employee Type</strong></th>
                         <th><strong>Group By</strong></th>
                		<th><strong>Order By</strong></th>
                        <th><strong>ID Card No</strong></th>
                        <th><strong>System Code</strong></th>
                        <th><input type="reset" name="reset" id="reset" value=" Reset " class="formbutton" style="width:100px" /></th>        
                     </thead>
                    <tr class="general">
                         <td>
                            <select name="cbo_year_selector" id="cbo_year_selector" class="combo_boxes" onchange="populate_select_month(this.value)" > 					  
                                 <option value="0">-- Select --</option>
                                <?php foreach( $policy_year AS $key=>$value ){ ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?>
                            </select>
                        </td>                    
                        <td>
                            <select name="cbo_month_selector" id="cbo_month_selector" class="combo_boxes">							
                                <option value="0">-- Select --</option>
                            </select>
                        </td>        
                       <td>
                            <select name="cbo_salary_sheet" id="cbo_salary_sheet" class="combo_boxes" style="width:120px" >                    	
                                <option value=""> All Employee </option>
                                <option value="0">Regular Employee All</option>
                                <option value="10">Regular Employee Old</option>
                                <option value="2">New Employee</option>
                                <option value="1">Separated All</option>
                                <option value="4">Separated Resigned </option>
                                <option value="5">Separated Retirement</option>
                                <option value="6">Separated Disability</option>
                                <option value="7">Separated Death</option>
                                <option value="8">Separated Terminated</option>
                                <option value="9">Separated Un-noticed</option>
                            </select>
                    	</td>
                        <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" >							
                           <option value="0">-- Select --</option>
                            <?php foreach( $groupby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                         </select>
                    </td>
                    <td>
                    	<select name="order_by_id" id="order_by_id" class="combo_boxes" style="width:100px" >							
                            <option value="0">-- Select --</option>
                            <?php foreach( $orderby_arr AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                        </select>
                    </td>        
                        <td>
                            <input type="text" name="id_card" id="id_card" class="text_boxes" style="width:100px" ondblclick="openmypage_employee_info_id_card('../../../hr_admin/search_employee_multiple_by_id.php','Employee Information'); return false" placeholder="Double Click For Search" autocomplete="off" />
                        </td>
                        <td>
                            <input type="text" name="emp_code" id="emp_code" value="" class="text_boxes" size="40" style="width:140px"  onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info('../../../hr_admin/search_employee_multiple.php','Employee Information'); return false" placeholder="Double Click For Search" />
                        </td>                               
                        <td>
                            <input type="button" name="search" id="search" value="Show" class="formbutton" style="width:100px" onclick="generate_salary_report()"/>
                        </td>               
                    </tr> 
				</table>
                <div id="data_panel" align="center"></div><br />
                <div id="report_container" align="center" style="background-color:#FFFFFF"></div>
            </fieldset>
        </form>
        </div>
    </body>
</html>