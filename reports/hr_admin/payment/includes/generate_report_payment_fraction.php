<?
session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY weekend ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$weekend_chart[$row['emp_code']] = mysql_real_escape_string( $row['weekend'] );		
	}

	//salary head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and salary_head=1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_head = array();
	$salary_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$salary_head[$row['id']] = mysql_real_escape_string( $row['custom_head'] );	
			$salary_head_id[] = mysql_real_escape_string( $row['id'] );		
	}
	//earning head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4) and id not in (6,7)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head = array();
	$earning_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id[] = mysql_real_escape_string( $row['id'] );	
	}
	
	//earning head Original Salary Sheet 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head_original = array();
	$earning_head_id_original = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head_original[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id_original[] = mysql_real_escape_string( $row['id'] );	
	}
	//deduction head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (1,2) and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$deduction_head = array();
	$deduction_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$deduction_head[] = mysql_real_escape_string( $row['custom_head'] );
			 $deduction_head_id[] = mysql_real_escape_string( $row['id'] );		
	}

	$bangArray = array(	
 						'Friday'=>'শুক্রবার', 
						'Saturday'=>'শনিবার', 
						'Sunday'=>'রবিবার', 
						'Monday'=>'সোমবার', 
						'Tuesday'=>'মঙ্গলবার', 
						'Wednesday'=>'বুধবার', 
						'Thursday'=>'বৃহস্পতিবার' 
 					   );
	
$bangMonthArray = array('January'=>'জানুয়ারি', 'February'=>'ফেব্রুয়ারি', 'March'=>'মার্চ', 'April'=>'এপ্রিল','May'=>'মে','June'=>'জুন', 'July'=>'জুলাই','August'=>'আগস্ট', 'September'=>'সেপ্টেম্বর', 'October'=>'অক্টোবর', 'November'=>'নভেম্বর', 'December'=>'ডিসেম্বর');



extract ( $_GET );
extract ( $_POST );


 	
	if($type=="wages_salary_sheet"){ //check which salary format selected
		$sql="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and salary_sheet_format!='0' order by id DESC "; //pay_slip_format
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$row = mysql_fetch_array( $result );
		$types=$row[salary_sheet_format];
		//echo $types;die;
	}
	
	if($type=="wages_salary_sheet_buyer"){ //check which salary format selected buyer
		$sql="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and salary_sheet_format!='0' order by id DESC "; //pay_slip_format
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$row = mysql_fetch_array( $result );
		$types=$row[salary_sheet_format]."_buyer";
		//echo $types;die;
	}
	
	
if($types=="wages_salary_sheet_jm_buyer")//jm buyer automated
{	
	
	
	$table_width=(count($salary_head)+count($earning_head)+count($deduction_head))*50;
?>

<style>

.verticalText 
{               
    /* IE-only DX filter */
    writing-mode: tb-rl;
    filter: flipv fliph;
    /* Safari/Chrome function */
    -webkit-transform: rotate(270deg);
    /* works in latest FX builds */	
    -moz-transform: rotate(270deg);
}

</style>

<table width="2000" cellpadding="0" cellspacing="0" border="1" style="font-size:8px" class="rpt_table" rules="all">

	<thead>    
    
    <tr style="text-align:center; font-weight:bold">
   		<th colspan="5">SECTION : </th>
        <th colspan="9">DEPARTMENT : </th>
        <th colspan="7">FLOOR :</th>
        <th colspan="3" >GH:</th>
        <th colspan="3" >WH:</th>
        <th colspan="2" >FH:</th>
      </tr>    
    <tr style="text-align:center; font-weight:bold">
    	<th width="50" rowspan="3">SL</th><th width="60" rowspan="3">Emp Code</th>
    	<th colspan="2">Emp Name</th>
    	
    	<th width="70" rowspan="3"><div class="verticalText">Joining</div></th><th width="50">Basic Wages</th>
    	<th width="50" rowspan="3">Gross Wages</th>
    	<th width="50" rowspan="2">DOM</th>
    	<th width="50">CL</th>
    	<th width="50">GH</th>
    	<th width="50" rowspan="3"><div class="verticalText">PRESENT DAYS</div></th>
    	<th width="50" rowspan="3"><div class="verticalText">PAY DAYS</div></th>
    	<th width="50" rowspan="3"><div class="verticalText">ABSENT DAYS</div></th>
    	<th width="50" rowspan="3"><div class="verticalText">NOT PAyble DAYS</div></th>
    	 
        <th colspan="<? echo count($earning_head); ?>">Others Earning</th>		
		<th width="50" rowspan="3">TOTAL SALARY</th>
		<th colspan="<? echo count($deduction_head); ?>">DEDUCTION</th>
		<th width="50" rowspan="3"><div class="verticalText">Not Payable Amount</div></th><th width="50" rowspan="3"><div class="verticalText">Total<br />Deduction</div></th><th width="70" rowspan="3">Net payable </th><th width="150" rowspan="3"> Signature</th>
    </tr>
    <tr style="text-align:center; font-weight:bold">
    	<th width="170" rowspan="2">Designation</th>
    	<th width="50" rowspan="2"><div class="verticalText">Grade</div></th>    	
    	<th width="50">House Rent</th>
    	<th width="50">SL</th>
    	<th width="50">WH</th>
    	 
       
         <?
		 //echo count($earning_head);
			for ($i=0; $i<count($earning_head); $i++)
			{
				if($earning_head_id[$i]==5)
				{
		?>	
				<th width="50" rowspan="2"><div class=""><? echo $earning_head[$i]."<br><hr>"."HRS <br> Rate <br> Amount";  ?></div></th>
        <?
				}
				else
				{
					?>
                    <th width="50" rowspan="2"><div class="verticalText"><? echo $earning_head[$i];  ?></div></th>
                    <?
				}
			}?>   
        
        <?
			for ($i=0; $i<count($deduction_head); $i++)
			{
		?>	
		<th width="50" rowspan="2"><div class="verticalText"><? echo $deduction_head[$i];  ?></div></th>
        <?
			}?>
		 
    </tr>
    <tr style="text-align:center; font-weight:bold">    	
    	<th width="50">Medical Allow.</th>
    	<th width="50">WD</th>
    	<th width="50">EL</th>
    	<th width="50">FH</th>    	
	  </tr>
    </thead>
    <tbody>	
    	<?
		$sl=0;
			$sql_emp="select a.*,b.* from hrm_salary_mst a, hrm_employee b where a.emp_code=b.emp_code";
			$exe_sql_emp=mysql_db_query($DB, $sql_emp);
			while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
			{ 
			$sl++;
		?>
    	<tr>
        	<td align="center" valign="middle"><? echo $sl; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[emp_code]; ?></td>
          
            <td align="center" valign="middle"><? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name].'<br><hr style="width:80%; border:1px dotted">'.$designation_chart[$row_emp[designation_id]]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[salary_grade]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[joining_date]; ?></td>
            
            <td align="center" valign="middle"><? echo $row_emp[basic_salary]."<br>".$row_emp[house_rent]."<br>".$row_emp[medical_allowance]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[gross_salary]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[total_calendar_days]."<br>".$row_emp[total_working_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[casual_leave]."<br>".$row_emp[sick_leave]."<br>".$row_emp[earn_leave]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[government_holiday]."<br>".$row_emp[total_weekly_holiday]."<br>".$row_emp[festival_holiday]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[total_present_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[payable_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[total_abs_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[not_payble_days]; ?></td>
            <?
			
			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($earning_head_id); $i++)
			{
				if ($i!=count($earning_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."' ";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=0 order by salary_head_id";
			  
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				for ($i=0; $i<count($earning_head_id); $i++)
				{
					if($earning_head_id[$i]==5)
					{
			?>
            	<td align="center" valign="middle"><? echo $row_emp[b_over_time]."<br>".$row_emp[over_time_rate]."<br>".$row_earn[$earning_head_id[$i]]; ?></td>
            
            <?
					}
					else
					{
			?>
            <td align="center" valign="middle"><? echo $row_earn[$earning_head_id[$i]]; ?></td>
           
            <? }
				}
			}
			?>
             <td align="center" valign="middle"><? echo $row_emp[b_total_earning_amount]; ?></td>
              <?
			
			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($deduction_head_id); $i++)
			{
				if ($i!=count($deduction_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."' ";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=1 order by salary_head_id";
			 
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				for ($i=0; $i<count($deduction_head_id); $i++)
				{
					 
			?>
            <td align="center" valign="middle"><? echo $row_earn[$deduction_head_id[$i]]; ?></td>
           
            <?  
				}
			}
			?>
            <td align="center" valign="middle"><? echo $row_emp[not_payble_days]*$row_emp[daily_gross_salary]; ?></td>
             <td align="center" valign="middle"><? echo $row_emp[total_deduction_amount]; ?></td>
             <td align="center" valign="middle"><? echo $row_emp[b_net_payable_amount]; ?></td>
             <td></td>
        </tr>
        <?
			}
			?>
	</tbody>
    <tfoot>
    </tfoot>        

</table>

<? 

	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();	



} 

?>






<?

if($types=="wages_salary_sheet4_buyer") // asrotext ltd formate-final buyer automated
{
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];
	
	//salary details 
	$salary_breakdown = array();
	$sql_sal="select * from hrm_fraction_salary_mst m, hrm_fraction_salary_dtls d where m.id=d.salary_mst_id and m.salary_periods like '$txt_from_date'";
	//echo $sql_sal;die;
	$exe_sql_sal=mysql_db_query($DB, $sql_sal);
	while ($row_sal = mysql_fetch_array($exe_sql_sal))
	{
		$salary_dtls_arr[$row_sal[emp_code]][$row_sal[salary_head_id]]=$row_sal[pay_amount];
	}

	
	
	if ($cbo_salary_sheet=='') {$emp_status="";} else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and b.category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and  b.designation_id='$designation_id'";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id='$department_id'";
	if ($payment_method==0) $payment_met =""; else if ($payment_method==1) $payment_met ="and a.emp_code not in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	else if ($payment_method==2) $payment_met ="and a.emp_code in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	if ($company_id==0){ $company="";}else{$company="and a.company_id='$company_id'";}
	if ($emp_code==""){ $emp_code="";}else{$emp_code="and a.emp_code in ($emp_code)";}
	
	
	ob_start();
	
				$company_info_sql="select * from lib_company where id='$company_id'";
				//echo $company_info_sql;die;
				$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
				$company_info_result = mysql_fetch_array($result);
	
	
	?>
    
	<style>
		.verticalText {writing-mode: tb-rl;filter: flipv fliph;-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);}
	</style>
    
	<table width="1600" cellpadding="0" cellspacing="0" style="font-size:11px; font-family:'Arial Black', Gadget, sans-serif;" class="rpt_table"  rules="all" ><!--rules="all"-->
		       
        
	<?
	$sl=0;
	$new_section=array();
	$new_department=array();
		
	
	$sql_emp = "select 
					a.*, a.gross_salary as sal_gross_salary,a.basic_salary as sal_basic_salary, a.house_rent as sal_house_rent,a.medical_allowance as sal_medical_allowance,b.* 
				from 
					hrm_fraction_salary_mst a, hrm_employee b 
				where 
					a.emp_code=b.emp_code and 
					a.salary_periods like '$txt_from_date' 
					$location_id $section_id $subsection_id $category $payment_met $designation_id $department_id $company $emp_status 
					order by b.department_id,b.section_id,CAST(b.id_card_no as SIGNED)";
	
	//echo $sql_emp;die;
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);	
	while ($row_emp = mysql_fetch_array($exe_sql_emp)) // Master Qry starts 
	{
		$sl++;
		if($sl%2==0) $bgcolor='#FFFFFF';
		else $bgcolor='#EEEEEE';
		
		$total_holiday = $row_emp[b_government_holiday]+$row_emp[b_festival_holiday]+$row_emp[compensatory_total_holiday];
			
		if(in_array($row_emp[section_id],$new_section)) //sub total
		{
			
			
		}
		else{
				$new_section[$row_emp[section_id]] = $row_emp[section_id];
				if($sl!=1){
			?>
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Sub Total</td>
                    <? foreach($sec_sub_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $sec_sub_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $sec_sub_total_overtime_amt;?></td>
                    <? foreach($sec_sub_total_earning_arr as $key=>$val){
					?>
                    	<td><? echo $val; ?></td>
                    <? } ?>                   
                    <td><? echo $sec_sub_total_earning_total_amt; ?></td>
                    <? foreach($sec_sub_total_deduc_sal as $key=>$val){
					?>
                    	<td><? echo $val; ?></td>
                    <? } ?>                    
                     <td><? echo $sec_sub_total_deduc_amt; ?></td>
                     <? foreach($sec_sub_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>                     
                    <td><? echo $sec_sub_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>
			<?
				$sec_sub_total_sal = array();
				$sec_sub_total_salary_payable_amt = 0;
				$sec_sub_total_overtime_amt = 0;
				$sec_sub_total_earning_arr = array();
				$sec_sub_total_earning_total_amt = 0;
				$sec_sub_total_deduc_sal = array();
				$sec_sub_total_deduc_amt = 0;
				$sec_sub_total_payable_amt = 0;
				$sec_sub_total_late_absent_lwp = array();
			}
			
			
			if(in_array($row_emp[department_id],$new_department)) //grand total
		{
			
		}
		else{
				$new_department[$row_emp[department_id]] = $row_emp[department_id];
				if($sl!=1){
			?>
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Grand Total</td>
                    <? foreach($dept_grand_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $dept_grand_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $dept_grand_total_overtime_amt;?></td>
                    <? foreach($dept_grand_total_earning_arr as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $dept_grand_total_earning_total_amt; ?></td>
                    <? foreach($dept_grand_total_deduc_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                     <td><? echo $dept_grand_total_deduc_amt; ?></td>
                     <? foreach($dept_grand_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?> 
                    <td><? echo $dept_grand_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>
			<?
				$dept_grand_total_sal = array();
				$dept_grand_total_salary_payable_amt = 0;
				$dept_grand_total_overtime_amt = 0;
				$dept_grand_total_earning_arr = array();
				$dept_grand_total_earning_total_amt = 0;
				$dept_grand_total_deduc_sal = array();
				$dept_grand_total_deduc_amt = 0;
				$dept_grand_total_payable_amt = 0;
				$dept_grand_total_late_absent_lwp = array();
			}
		}
			
				
			?>
          

                <thead>  
                    <tr style="text-align:center; font-weight:bold; border:solid 0 #060;">    	 
                        <th colspan="<? echo count($salary_head)+count($earning_head_id)+count($deduction_head_id)+26; ?>" >
                            <div style="border:0px; font-size:17px"><?	echo $company_info_result["company_name"];?></div>
                            <div style="border:0px; font-size:12px">
                                Address: <? echo $company_info_result["plot_no"]; ?> <? echo $company_info_result["level_no"]; ?>,<? echo $company_info_result["block_no"]; ?>, <? echo $company_info_result["road_no"]; ?>.<? echo $company_info_result["city"];?><br />
                                Fraction Salary Sheet For The Month Of <? echo $cur_month; ?>-<? echo $cur_year; ?>            	
                            </div>
                            <div align="left"><b>Department :</b> <? echo $department_details[$row_emp['department_id']]; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Section : </b> <? echo $section_details[$row_emp['section_id']]; ?></div>
                        </th>
                    </tr>
                    <tr>
                        <th rowspan="2" width="30">SL</th>
                        <th rowspan="2" width="130">Employee</th>
                        <th rowspan="2" width="70">Card No</th>
                        <th rowspan="2" width="70">Emp Code</th>
                        <th rowspan="2" width="100">Designation</th>
                        <th rowspan="2" width="50">DOJ</th>
                        <th rowspan="2" width="40">Grade</th>
                        <? foreach( $salary_head as $key=>$val) {
								if($key==20){
						?>
                                	<th rowspan="2" width="80"><? echo $val; ?></th>
                        <? 		} 
						}
						?>
                        <? foreach( $salary_head as $key=>$val) {
								if($key!=20){
						?>
                                	<th rowspan="2" width="80"><? echo $val; ?></th>
                        <? 		} 
						}
						?>
                        <th rowspan="2" width="40">DOM</th>
                        <th rowspan="2" width="40">Pre Days</th>
                        <th colspan="3" width="70">Leave</th>
                        <th width="40">WD</th>
                        <th rowspan="2" width="40">Abs Days</th>
                        <th rowspan="2" width="40">Pay Days</th>
                        <th rowspan="2" width="40">Salary Payment</th>
                        <th colspan="3" width="150">Overtime</th>
                        <?    
                        //echo count($earning_head);
                        for ($i=0; $i<count($earning_head); $i++)
                        {
                            if($earning_head_id[$i]!=5)
                            {
                        ?>	
                                <th width="50" rowspan="2"><div class="verticalText"><? echo $earning_head[$i];  ?></div></th>
                        <?
                            }
                            
                        }
                        ?>
                            <th width="50" rowspan="2">Total Earn</th>
                        <? 
                        for ($i=0; $i<count($deduction_head); $i++)
                        {
							if($deduction_head_id[$i]!=23 && $deduction_head_id[$i]!=24 && $deduction_head_id[$i]!=29)
							{
						?>				
                            	<th width="50" rowspan="2"><div class="verticalText"><? echo $deduction_head[$i];  ?></div></th>
                        <? 
							}
							else
							{
								$late_absent_lwp_head[$deduction_head_id[$i]]=$deduction_head[$i];
							}
						} 
						?>	
                        <th width="50" rowspan="2">Total Deduct</th> 
                        <?
                        foreach($late_absent_lwp_head as $key=>$val)
                        {
                        ?>
                            <th width="50" rowspan="2"><div class="verticalText"><? echo $val;  ?></div></th>
                        <?	
                        } 
						?>         
                        <th rowspan="2" width="80">Net Payable</th>
                        <th rowspan="2" width="150">Signature</th>
                    </tr>
                    <tr>
                      <th>CL</th>
                      <th>SL</th>
                      <th>EL</th>
                      <th>HD</th>
                      <th>OT Hour</th>
                      <th>OT Rate</th>
                      <th>OT Amt</th>
                    </tr>        
                </thead>
          
            
        <?    	
			
		}
		
	?>	
		
		<tbody>	    	
    		<tr height="94" bgcolor="<? echo $bgcolor; ?>" >
        		<td align="center" valign="middle"><? echo $sl; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[id_card_no]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[emp_code]; ?></td>
                <td align="center" valign="middle"><? echo $designation_chart[$row_emp[designation_id]]; ?></td>
                <td align="center" valign="middle"><div class="verticalText"><? echo $row_emp[joining_date]; ?></div></td>
                <td align="center" valign="middle"><? echo $row_emp[salary_grade]; ?></td>
                <? foreach( $salary_head as $key=>$val) { 
					if($key==20){
						 $sec_sub_total_sal[$key] += round($row_emp[sal_gross_salary]); 
						 $dept_grand_total_sal[$key] += round($row_emp[sal_gross_salary]); 
					?>
                         <td align="center" valign="middle"><? echo round($row_emp[sal_gross_salary]); ?></td>
                    <? 
						} 
					}
					
					foreach( $salary_head as $key=>$val) {
						if($key!=20){
						 $sec_sub_total_sal[$key] += round($salary_dtls_arr[$row_emp[emp_code]][$key]);
						 $dept_grand_total_sal[$key] += round($salary_dtls_arr[$row_emp[emp_code]][$key]);
					?>    
                         <td align="center" valign="middle"><? echo round($salary_dtls_arr[$row_emp[emp_code]][$key]); ?></td>
					<? 		
						} 
					} 
				?>
                
                
                <td align="center" valign="middle"><? echo $row_emp[total_calendar_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[b_net_payable_days]-$total_holiday-$row_emp[b_total_weekly_holiday]-$row_emp[total_leave_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[casual_leave]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[sick_leave]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[earn_leave]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[b_total_weekly_holiday]."<br />". $total_holiday; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[b_absent_days]; ?></td>
                <td align="center" valign="middle"><? echo $row_emp[b_net_payable_days]; ?></td>
                <? 
				$not_payable_amounts = round( $row_emp[not_payble_days]*$row_emp[daily_gross_salary] );
				//gross - absent - late - lwp - not payable   	 
				$late_absent_lwp_deduc_amt = round($row_emp[b_total_late_deduction_amount]+$row_emp[b_total_absent_deduction_amount]+$salary_dtls_arr[$row_emp[emp_code]][29]);
				$gross_salary_on_pay_days = $row_emp[sal_gross_salary]- $late_absent_lwp_deduc_amt-$not_payable_amounts;
				$salary_payment = round($gross_salary_on_pay_days);
				$sec_sub_total_salary_payable_amt += $salary_payment; 
				$dept_grand_total_salary_payable_amt += $salary_payment; 
				?>
                <td align="center" valign="middle"><? echo $salary_payment; ?></td>
                
                <td align="center" valign="middle"><? echo $row_emp[b_over_time]; ?></td>
                <td align="center" valign="middle"><? if( $row_emp[b_over_time]>0 ) echo $row_emp[over_time_rate]; else echo "0"; ?></td>
                <? $sec_sub_total_overtime_amt += round($row_emp[b_over_time]*$row_emp[over_time_rate]); ?>
                <? $dept_grand_total_overtime_amt += round($row_emp[b_over_time]*$row_emp[over_time_rate]);?>
                <td align="center" valign="middle"><? echo round($row_emp[b_over_time]*$row_emp[over_time_rate]); ?></td>
                <?
			
					$sql_attnd =" SELECT  ";
					for ($i=0; $i<count($earning_head_id); $i++)
					{
						if ($i!=count($earning_head_id)-1)
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."', ";
						}
						else
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."' ";
						}
					}
					$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=0 order by salary_head_id";
					//echo $sql_attnd;die;  
					$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
					if($row_earn = mysql_fetch_array($exe_sql_emp1))
					{
						for ($i=0; $i<count($earning_head_id); $i++)
						{
						
							if( $earning_head_id[$i]!=5 )
							{
								
								if( $earning_head_id[$i]==35 ) $row_earn[$earning_head_id[$i]] = $row_emp[b_attendance_bonus];
								
								$sec_sub_total_earning_arr[$i] += round($row_earn[$earning_head_id[$i]]);
								$dept_grand_total_earning_arr[$i] += round($row_earn[$earning_head_id[$i]]);
					?>
								<td align="center" valign="middle"><? if($row_earn[$earning_head_id[$i]]>0) echo round($row_earn[$earning_head_id[$i]]); else echo "0"; ?></td>
					
					<? //$row_earn[$earning_head_id[$i]]
							}							
						}
					}
						
						$total_eran = round( $row_emp[b_total_earning_amount]-($row_emp[b_total_late_deduction_amount]+$row_emp[b_total_absent_deduction_amount]+$salary_dtls_arr[$row_emp[emp_code]][29])-($row_emp[not_payble_days]*$row_emp[daily_gross_salary]) );
						$sec_sub_total_earning_total_amt += $total_eran;
						$dept_grand_total_earning_total_amt += $total_eran;
					?>
					 	
                        <td align="center" valign="middle"><? echo  round($total_eran); ?></td>
					
					<?
					
					$sql_attnd =" SELECT  ";
					for ($i=0; $i<count($deduction_head_id); $i++)
					{
						
						if ($i!=count($deduction_head_id)-1)
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."', ";
						}
						else
						{
							$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."' ";
						}
						
					}
					$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=1 order by salary_head_id";
					 
					$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
					if($row_earn = mysql_fetch_array($exe_sql_emp1))
					{
						for($i=0; $i<count($deduction_head_id); $i++)
						{
							 
							 if($deduction_head_id[$i]!=23 && $deduction_head_id[$i]!=24 && $deduction_head_id[$i]!=29)
							{
							 	$sec_sub_total_deduc_sal[$i] += round($row_earn[$deduction_head_id[$i]]);
							 	$dept_grand_total_deduc_sal[$i] += round($row_earn[$deduction_head_id[$i]]);
					?>
								<td align="center" valign="middle"><? if($row_earn[$deduction_head_id[$i]]>0 ) echo round($row_earn[$deduction_head_id[$i]]); else echo "0"; ?></td>
					<?  
							}
							else
							{
								
								 if($deduction_head_id[$i]==23)
								 {
									$late_absent_lwp_head_val[$deduction_head_id[$i]]=$row_emp[b_total_late_deduction_amount]; 
								 }
								 else if($deduction_head_id[$i]==24)
								 {
									$late_absent_lwp_head_val[$deduction_head_id[$i]]=$row_emp[b_total_absent_deduction_amount];  
								 }
								 else if($deduction_head_id[$i]==29)
								 {
									$late_absent_lwp_head_val[$deduction_head_id[$i]]=$row_emp[total_lwp_deduction_amount];  
								 }								
							}							 
						}
					}
						
						$sec_sub_total_deduc_amt += round($row_emp[b_total_deduction_amount]-$late_absent_lwp_deduc_amt);
						$dept_grand_total_deduc_amt += round($row_emp[b_total_deduction_amount]-$late_absent_lwp_deduc_amt);
				?>
                <td align="center" valign="middle"><? if(($row_emp[b_total_deduction_amount]-$late_absent_lwp_deduc_amt)>0) echo round($row_emp[b_total_deduction_amount]-$late_absent_lwp_deduc_amt); else echo "0"; ?></td>
               <?
				foreach($late_absent_lwp_head_val as $key=>$val)
				{
					$sec_sub_total_late_absent_lwp[$key] += $val;
					$dept_grand_total_late_absent_lwp[$key] += $val;
				?>
					<td align="center" valign="middle"><? echo round($val); ?></td>
				<?	
				}				
					$sec_sub_total_payable_amt += round($row_emp[b_net_payable_amount]); 
					$dept_grand_total_payable_amt += round($row_emp[b_net_payable_amount]); 
				?>
                <td align="center" valign="middle"><? echo round($row_emp[b_net_payable_amount]); ?></td>
                <td align="center" valign="middle">&nbsp;</td>			
    		</tr>
        </tbody>    
    <?
	}// while loop end	   
		   ?>
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Sub Total</td>
                    <? foreach($sec_sub_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $sec_sub_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $sec_sub_total_overtime_amt;?></td>
                    <? foreach($sec_sub_total_earning_arr as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $sec_sub_total_earning_total_amt; ?></td>
                    <? foreach($sec_sub_total_deduc_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $sec_sub_total_deduc_amt; ?></td>
                    <? foreach($sec_sub_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?> 
                    <td><? echo $sec_sub_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>
  
				<tr align="center" bgcolor="#CCCCCC" style="font-weight:bold">
                	<td colspan="7">&nbsp;Grand Total</td>
                    <? foreach($dept_grand_total_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td colspan="8">&nbsp;</td>
                    <td><? echo $dept_grand_total_salary_payable_amt; ?></td>
                    <td colspan="3"><? echo $dept_grand_total_overtime_amt;?></td>
                    <? foreach($dept_grand_total_earning_arr as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                    <td><? echo $dept_grand_total_earning_total_amt; ?></td>
                    <? foreach($dept_grand_total_deduc_sal as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?>
                     <td><? echo $dept_grand_total_deduc_amt; ?></td>
                    <? foreach($dept_grand_total_late_absent_lwp as $key=>$val){?>
                    	<td><? echo $val; ?></td>
                    <? } ?> 
                    <td><? echo $dept_grand_total_payable_amt; ?></td>                
                    <td>&nbsp;</td>
            	</tr>   
    
    			<tr height="130" valign="bottom">
                	<td colspan="<? echo count($salary_head)+count($earning_head_id)+count($deduction_head_id)+26; ?>" align="center">
                		<div style="width:240px;float:left; text-decoration:overline">Prepared By</div>
                        <div style="width:240px;float:left; text-decoration:overline">Asst.Manager(HR&Com.)</div>
                    	<div style="width:240px;float:left; text-decoration:overline">Asst.Manager(Acc.&Fin.)</div>				
                    	<div style="width:240px;float:left; text-decoration:overline">Manager(HR&Com.)</div>						
                    	<div style="width:240px;float:left; text-decoration:overline">Asst.Manager(Internal Audit)</div>				
                    	<div style="width:240px;float:left; text-decoration:overline">C.O.O</div>			
                    	<div style="float:240px; text-decoration:overline">Director</div>
                    </td>    
                </tr>
	</table>
    

<?


	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);	
	
	echo "$html"."####"."$name";		
	exit();	
	

}//end if condition types

?>






<?

//month generated
if($type=="select_month_generate")
	{		
		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1"; //and b.is_locked=0
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>