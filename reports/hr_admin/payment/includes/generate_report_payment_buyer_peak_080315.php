﻿<?php
session_start();
header('Content-type: text/html; charset=utf-8');
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
	
	$sql = "SELECT * FROM hrm_weekend WHERE is_deleted = 0 ORDER BY weekend ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$weekend_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$weekend_chart[$row['emp_code']] = mysql_real_escape_string( $row['weekend'] );		
	}

	//salary head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and salary_head=1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_head = array();
	$salary_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$salary_head[$row['id']] = mysql_real_escape_string( $row['custom_head'] );	
			$salary_head_id[] = mysql_real_escape_string( $row['id'] );		
	}
	//earning head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4) and id not in (6,7)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head = array();
	$earning_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id[] = mysql_real_escape_string( $row['id'] );	
	}
	
	//earning head Original Salary Sheet 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (0,3,4)  and salary_head<>1 and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$earning_head_original = array();
	$earning_head_id_original = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$earning_head_original[] = mysql_real_escape_string( $row['custom_head'] );	
		    $earning_head_id_original[] = mysql_real_escape_string( $row['id'] );	
	}
	//deduction head 	
	$sql = "select * from lib_payroll_head where is_applicable=1 and type in (1,2) and status_active=1 and is_deleted=0 order by id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$deduction_head = array();
	$deduction_head_id = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$deduction_head[] = mysql_real_escape_string( $row['custom_head'] );
			 $deduction_head_id[] = mysql_real_escape_string( $row['id'] );		
	}

	$bangArray = array(	
 						'Friday'=>'শুক্রবার', 
						'Saturday'=>'শনিবার', 
						'Sunday'=>'রবিবার', 
						'Monday'=>'সোমবার', 
						'Tuesday'=>'মঙ্গলবার', 
						'Wednesday'=>'বুধবার', 
						'Thursday'=>'বৃহস্পতিবার' 
 					   );
	
	$bangMonthArray = array('January'=>'জানুয়ারি', 'February'=>'ফেব্রুয়ারি', 'March'=>'মার্চ', 'April'=>'এপ্রিল','May'=>'মে','June'=>'জুন', 'July'=>'জুলাই','August'=>'আগস্ট', 'September'=>'সেপ্টেম্বর', 'October'=>'অক্টোবর', 'November'=>'নভেম্বর', 'December'=>'ডিসেম্বর');
	
	extract ( $_GET );
	extract ( $_POST );
 	
	if($type=="wages_salary_sheet"){ //check which salary format selected
		$sql="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and salary_sheet_format!='0' order by id DESC "; //pay_slip_format
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$row = mysql_fetch_array( $result );
		$types=$row[salary_sheet_format];
		//echo $types;die;
	}
	
	if($type=="wages_salary_sheet_buyer"){ //check which salary format selected buyer
		$sql="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and salary_sheet_format!='0' order by id DESC "; //pay_slip_format
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$row = mysql_fetch_array( $result );
		$types=$row[salary_sheet_format]."_buyer";
		//echo $types;die;
		//$types="wages_salary_sheet_buyer_peak";
	}
	//echo $types; die;
  
if($types=="wages_salary_sheet1_buyer") 	// Peak buyer salary sheet 
{	
	$cbo_salary_periods = explode('_', $cbo_month_selector ); 
	$txt_from_date = $cbo_salary_periods[0];
	$exp_month=explode("-",$txt_from_date);
	$cur_month = $months["'"+$exp_month[1]+"'"];
	$cur_year = $exp_month[0];	
	//find GH FH CH, H
	$table_width=(count($salary_head)+count($earning_head)+count($deduction_head))*50;
?>

<style>
	.verticalText 
	{               
		writing-mode: tb-rl;
		 filter: flipv fliph;
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
		-o-transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		transform: rotate(270deg);
		width: 3em;		 
	}
</style>

<?php
	$company_info_sql="select * from lib_company where id='$company_id'";
	$result = mysql_query( $company_info_sql ) or die( $company_info_sql . "<br />" . mysql_error() );
	$company_info_result = mysql_fetch_array($result);
	
	$con_date=date('Y-m-d',strtotime($txt_from_date));
	$con_date_to=add_month($txt_from_date,1);
	
	if ($cbo_salary_sheet=='') { $emp_status=" and a.emp_status in (0,1,2)"; } 
	else if($cbo_salary_sheet==0){$emp_status="and a.emp_status in (0,2)";} 
	else if($cbo_salary_sheet==1){$emp_status="and a.emp_status=1"; } 
	else if($cbo_salary_sheet==2){$emp_status="and a.emp_status=2"; } 
	else if($cbo_salary_sheet==4){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=1  and  separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==5){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=2  and separated_from between '$con_date' and '$con_date_to'))";} //and separated_from like '$con_date' 
	else if($cbo_salary_sheet==6){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=3  and separated_from between '$con_date' and '$con_date_to')";} // and separated_from like '$con_date'
	else if($cbo_salary_sheet==7){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=4  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==8){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=5  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==9){$emp_status="and a.emp_status=1 and a.emp_code in (SELECT distinct emp_code from hrm_separation where separation_type=6  and separated_from between '$con_date' and '$con_date_to')";} //and separated_from like '$con_date'
	else if($cbo_salary_sheet==10)
	{
		$emp_status="and a.emp_status in ( 0 )";
		//$txt_from_date = $previous_month;
	}
	//else $emp_status="and a.emp_status='$cbo_salary_sheet'";
	$rpt_company=$company_id;
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";	
	if ($cbo_emp_category=='') $category =""; else $category ="and a.employee_category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and  a.designation_id='$designation_id'";
	if ($department_id==0) $department_id=""; else	$department_id=" and  a.department_id='$department_id'";
	if ($payment_method==0) $payment_met =""; else if ($payment_method==1) $payment_met ="and a.emp_code not in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	else if ($payment_method==2) $payment_met ="and a.emp_code in (select emp_code from hrm_employee_salary_bank where status_active=1 and is_deleted=0 and lib_bank_id<>'')";
	if ($company_id==0){ $company_id="";}else{$company_id="and a.company_id='$company_id'";}	
	//if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ($emp_code)";
	if ($emp_code=="") $emp_code=""; else $emp_code="and a.emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and b.id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	$status_com=0;$status_loc=0;$status_divis=0;$status_dept=0;$status_sec=0;$status_subsec=0;
	if($group_by_id==0){$groupby="";}
	else if($group_by_id==1){$status_com=1;$groupby="a.company_id,";}
	else if($group_by_id==2){$status_com=1;$status_loc=1;$groupby="a.company_id,a.location_id,";}
	else if($group_by_id==3){$status_com=1;$status_loc=1;$status_divis=1;$groupby="a.company_id,a.location_id,a.division_id,";}
	else if($group_by_id==4){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,";}
	else if($group_by_id==5){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,";}
	else if($group_by_id==6){$status_com=1;$status_loc=1;$status_divis=1;$status_dept=1;$status_sec=1;$status_subsec=1;$groupby="a.company_id,a.location_id,a.division_id,a.department_id,a.section_id,a.subsection_id,";}
 		
	if($order_by_id==0){$orderby="order by ".$groupby." CAST(b.id_card_no as SIGNED)";}	 
	else if($order_by_id==1){$orderby="order by ".$groupby." CAST(b.designation_level as SIGNED)";}	 
	else if($order_by_id==2){$orderby="order by ".$groupby." CAST(a.emp_code as SIGNED)";}	 
	else if($order_by_id==3){$orderby="order by ".$groupby." right(b.id_card_no,5)";} 

	
	$sql = "SELECT b.emp_code,a.pay_amount ,a.salary_head_id FROM hrm_salary_dtls a, hrm_salary_mst b WHERE b.id=a.salary_mst_id and b.salary_periods='$txt_from_date' and salary_head_id in (5,6,7,31) ";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$processed_ot_amount = array();
	while( $row = mysql_fetch_assoc( $result ) ) {	
			if($row[salary_head_id]==31) $processed_ot_amount[$row['emp_code']]['penalty'] = ( $row['pay_amount'] );	
			else if($row[salary_head_id]==5) $processed_ot_amount[$row['emp_code']]['buyer'] = ( $row['pay_amount'] );
			if($row[salary_head_id]!=31)  $processed_ot_amount[$row['emp_code']]['org'] += ( $row['pay_amount'] );
	}
	
	ob_start();
 	?>
	<table width="1700" cellpadding="0" cellspacing="0" border="1" style="font-size:11px;font-family:Arial" class="rpt_table" rules="all">
	<?
	$location_arr=array();
	$division_arr=array();
	$department_arr=array();
	$section_arr=array();
	$subsection_arr=array();
	
	$section_total_gross=0;$section_total_salary=0;$section_total_net_payable=0;$section_total_not_payable=0;$section_total_deduction=0;$section_total_bank=0;$section_total_cash=0;	
	$section_total_earning_head_amt = array();$section_total_deduction_head_amt = array();

	$department_total_gross=0;$department_total_salary=0;$department_total_net_payable=0;$department_total_not_payable=0;$department_total_deduction=0;$department_total_bank=0;$department_total_cash=0;	
	$department_total_earning_head_amt = array();$department_total_deduction_head_amt = array();

	$division_total_gross=0;$division_total_salary=0;$division_total_net_payable=0;$division_total_not_payable=0;$division_total_deduction=0;$division_total_bank=0;$division_total_cash=0;	
	$division_total_earning_head_amt = array();$division_total_deduction_head_amt = array();

	$location_total_gross=0;$location_total_salary=0;$location_total_net_payable=0;$location_total_not_payable=0;$location_total_deduction=0;$location_total_bank=0;$location_total_cash=0;	
	$location_total_earning_head_amt = array();$location_total_deduction_head_amt = array();
	
	$grand_total_gross = 0;	$grand_total_salary = 0;$grand_total_net_payable = 0;$grand_total_not_payable = 0;$grand_total_deduction = 0;$grand_total_bank=0;$grand_total_cash=0;
	$grand_total_earning_head_amt = array();$grand_total_deduction_head_amt = array();
	
 	$sl=0;
	$ct=0;
	
 	$sql_emp = "select a.*,b.first_name,b.middle_name,b.last_name,b.id_card_no,b.punch_card_no,b.dob,b.designation_id,b.designation_level,b.joining_date,b.category,b.salary_grade,b.staff_ot_entitled,b.duty_roster_policy
			   from 
			   hrm_salary_mst a, hrm_employee b
			   where
			   a.emp_code=b.emp_code and
			   a.salary_periods='$txt_from_date'  
			   $emp_code $category $company_id $location_id $division_id $section_id $subsection_id $designation_id $department_id $payment_met $emp_status 
			   $orderby";
			   
	//echo $sql_emp;die;
	
	$exe_sql_emp=mysql_db_query($DB, $sql_emp);
	while ($row_emp = mysql_fetch_array($exe_sql_emp))// EMp Master Qry starts 
	{ 
		$sl++;
 		 
		   //start header print---------------------------------//
		   if($sl==1)
		   {
				$location_arr[$row_emp[location_id]]=$row_emp[location_id];
				$division_arr[$row_emp[division_id]]=$row_emp[division_id];
				$department_arr[$row_emp[department_id]]=$row_emp[department_id];
				$section_arr[$row_emp[section_id]]=$row_emp[section_id];
				$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
			   
			   ?>     
					
				<thead>    
					<tr style="text-align:center; font-weight:bold" height="70">    	 
						<th colspan="<? echo count($earning_head)+count($deduction_head)+21; ?> ">
							<div style="font-size:12px;"><strong style="font-size:20px"><?	echo $company_info_result["company_name"];?></strong></div>
							
							<div style="font-size:12px;margin-top:5px">Address: Plot No # <? echo $company_info_result["plot_no"]; ?>, Level No # <? echo $company_info_result["level_no"]; ?>, Block No # <? echo $company_info_result["block_no"]; ?>, Road No # <? echo $company_info_result["road_no"]; ?>, <? echo $company_info_result["city"];?><br />
								
						  </div>
						  
						  <div style="font-size:16px;margin-top:5px">
								Salary Sheet For The Month Of <? echo $cur_month; ?>-<? echo $cur_year; ?> 
						  </div>
						</th>
					</tr>			   
					<tr style="text-align:center; font-weight:bold" height="20">
                        <th width="40" rowspan="3">SL</th><th width="60" >ID</th>
                        <th colspan="2">Emp Name</th>
                        
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">Joining</div></th>
                        <th width="50" rowspan="3">
                        <? 
						
						$k=0;
						foreach($salary_head_mst_short as $key) { 
							$k++;
									if(count($salary_head_mst_short)==$k)  echo $key; else  echo $key."<br /><hr />"; 
								}
						?>
                      
                        </th>
                        <th width="50" rowspan="3">Gross Wages</th>
                        <th width="50" rowspan="2">DOM</th>
                        <th width="50">CL</th>
                        <th width="50">GH</th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">PRE DAYS</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">PAY DAYS</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">ABS DAYS</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">NP DAYS</div></th>
                         
                        <th colspan="<? echo count($earning_head); ?>">Others Earning</th>		
                        <th width="50" rowspan="3">TOTAL SALARY</th>
                        <th colspan="<? echo count($deduction_head); ?>">DEDUCTION</th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">Total<br />Deduct</div></th>
                        <th width="50" rowspan="3" valign="center"><div class="verticalText">NP Amount</div></th>
                        <th width="70" rowspan="3">Net payable </th>
                        <th width="70" rowspan="3">In Bank</th>
                        <th width="70" rowspan="3">In Cash</th>
                        <th width="150" rowspan="3"> Signature</th>
                    </tr>
                    <tr style="text-align:center; font-weight:bold">
                        <th width="60" rowspan="2">Emp Code</th>
                        <th width="170" rowspan="2">Designation</th>
                        <th width="50" rowspan="2" valign="center"><div class="verticalText">Grade</div></th>    	
                        <th width="50">SL</th>
                        <th width="50">WH</th>
                         
                       
                         <?
                         //echo count($earning_head);
                            for ($i=0; $i<count($earning_head); $i++)
                            {
                                if($earning_head_id[$i]==5)
                                {
                                    
                        ?>	
                                <th width="50" rowspan="2"><? echo $earning_head[$i]."<br><hr>"."HRS <br> Rate <br> Amount";  ?></th>
                        <?
                                }
                                else
                                {
                                    ?>
                                    <th width="50" rowspan="2" valign="center"><div class="verticalText"><? echo $earning_head[$i];  ?></div></th>
                                    <?
                                }
                            }?>   
                        
                        <?
                            for ($i=0; $i<count($deduction_head); $i++)
                            {
                        ?>	
                        <th width="50" rowspan="2" valign="center"><div class="verticalText" ><? echo $deduction_head[$i]; ?></div></th>
                        <?
                            }?>
                         
                    </tr>
                    <tr style="text-align:center; font-weight:bold">    	
                        <th width="50">WD</th>
                        <th width="50">EL</th>
                        <th width="50">FH</th>    	
				  	</tr>                  
				</thead>
				<?
		   }//end if condition of header print
		   //end header print
		
 		//---------------------------------------------------------------------------------------------------	
		//---------------------------------------------------------------------------------------------------
		
		
		if($sl!=1)
		{
					$new_loc=0;$new_divis=0;$new_dept=0;$new_sec=0;$new_subsec=0;
 					if(in_array($row_emp[location_id],$location_arr) && $status_loc==1)
					{
							if(in_array($row_emp[division_id],$division_arr) && $status_divis==1)
							{
									if(in_array($row_emp[department_id],$department_arr) && $status_dept==1)
									{
										if(in_array($row_emp[section_id],$section_arr) && $status_sec==1)
										{ 
											if(in_array($row_emp[subsection_id],$subsection_arr) && $status_subsec==1)
											{}
											else if($status_subsec==1)
											{
												$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
												$new_subsec=1;
											}
										}
										else if($status_sec==1)
										{
											$section_arr[$row_emp[section_id]]=$row_emp[section_id];
 											$subsection_arr=array();
											$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
											$new_sec=1;
											$new_subsec=1;
										}
									}
									else if($status_dept==1)
									{
										$department_arr[$row_emp[department_id]]=$row_emp[department_id];
 										$section_arr=array();
										$subsection_arr=array();
 										$section_arr[$row_emp[section_id]]=$row_emp[section_id];
										$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
										$new_dept=1;
										$new_sec=1;
										$new_subsec=1;
									}
							}//division
							else if($status_divis==1)
							{
									$division_arr[$row_emp[division_id]]=$row_emp[division_id];
									$department_arr=array();
									$section_arr=array();
									$subsection_arr=array();
									$department_arr[$row_emp[department_id]]=$row_emp[department_id];
									$section_arr[$row_emp[section_id]]=$row_emp[section_id];
									$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
									$new_divis=1;
									$new_dept=1;
									$new_sec=1;
									$new_subsec=1;
							}//division else
					}//location
					else if($status_loc==1)
					{
						$location_arr[$row_emp[location_id]]=$row_emp[location_id];
						$division_arr=array();
						$department_arr=array();
						$section_arr=array();
						$subsection_arr=array();
						$division_arr[$row_emp[division_id]]=$row_emp[division_id];
						$department_arr[$row_emp[department_id]]=$row_emp[department_id];
						$section_arr[$row_emp[section_id]]=$row_emp[section_id];
						$subsection_arr[$row_emp[subsection_id]]=$row_emp[subsection_id];
						$new_loc=1;
						$new_divis=1;
						$new_dept=1;
						$new_sec=1;
						$new_subsec=1;
					}//location else
		
		}
		
		
		if($new_subsec==1 && $status_subsec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Total</td>
					<td align="center"><? echo number_format($subsection_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($subsection_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($subsection_total_salary); ?></td> 
					<? foreach($subsection_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($subsection_total_deduction); ?></td>
					<td align="center" ><? echo number_format($subsection_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($subsection_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($subsection_total_bank); ?></td>
					<td align="center" ><? echo number_format($subsection_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$subsection_total_gross=0;$subsection_total_salary=0;$subsection_total_deduction=0;$subsection_total_not_payable=0;$subsection_total_net_payable=0;$subsection_total_bank=0;$subsection_total_cash=0;	
			$subsection_total_earning_head_amt = array();$subsection_total_deduction_head_amt = array();

		}
		
		if($new_sec==1 && $status_sec==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Section Total</td>
					<td align="center"><? echo number_format($section_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($section_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($section_total_salary); ?></td> 
					<? foreach($section_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($section_total_deduction); ?></td>
					<td align="center" ><? echo number_format($section_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($section_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($section_total_bank); ?></td>
					<td align="center" ><? echo number_format($section_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$section_total_gross=0;$section_total_salary=0;$section_total_deduction=0;$section_total_not_payable=0;$section_total_net_payable=0;$section_total_bank=0;$section_total_cash=0;	
			$section_total_earning_head_amt = array();$section_total_deduction_head_amt = array();
						
		}
		
		if($new_dept==1 && $status_dept==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Department Total</td>
					<td align="center"><? echo number_format($department_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($department_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($department_total_salary); ?></td> 
					<? foreach($department_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($department_total_deduction); ?></td>
					<td align="center" ><? echo number_format($department_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($department_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($department_total_bank); ?></td>
					<td align="center" ><? echo number_format($department_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$department_total_gross=0;$department_total_salary=0;$department_total_deduction=0;$department_total_not_payable=0;$department_total_net_payable=0;$department_total_bank=0;$department_total_cash=0;	
			$department_total_earning_head_amt = array();$department_total_deduction_head_amt = array();
		}
		 
		if($new_divis==1 && $status_divis==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Division Total</td>
					<td align="center"><? echo number_format($division_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($division_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($division_total_salary); ?></td> 
					<? foreach($division_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($division_total_deduction); ?></td>
					<td align="center" ><? echo number_format($division_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($division_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($division_total_bank); ?></td>
					<td align="center" ><? echo number_format($division_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$division_total_gross=0;$division_total_salary=0;$division_total_deduction=0;$division_total_not_payable=0;$division_total_net_payable=0;$division_total_bank=0;$division_total_cash=0;	
			$division_total_earning_head_amt = array();$division_total_deduction_head_amt = array();
 			
		}
		 
		if($new_loc==1 && $status_loc==1 && $sl!=1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Location Total</td>
					<td align="center"><? echo number_format($location_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($location_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($location_total_salary); ?></td> 
					<? foreach($location_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($location_total_deduction); ?></td>
					<td align="center" ><? echo number_format($location_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($location_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($location_total_bank); ?></td>
					<td align="center" ><? echo number_format($location_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$location_total_gross=0;$location_total_salary=0;$location_total_deduction=0;$location_total_not_payable=0;$location_total_net_payable=0;$location_total_bank=0;$location_total_cash=0;	
			$location_total_earning_head_amt = array();$location_total_deduction_head_amt = array();
 			
		}

		
		//header print here 
		$c_part_1="";
		if( $new_loc==1 ||$new_divis==1 || $new_dept==1 || $new_sec==1 || $new_subsec==1 || $sl==1)
		{
			
			if($status_loc==1)
			{					
				$c_part_1 .= "  Location :  ".$location_details[$row_emp[location_id]]."  ,";
			}				
			if($status_divis==1)
			{
				$c_part_1 .= "  Division :  ".$division_details[$row_emp[division_id]]."  ,";
			}
			if($status_dept==1)
			{
				$c_part_1 .= "  Department :  ".$department_details[$row_emp[department_id]]."  ,";
			}
			if($status_sec==1)
			{
				$c_part_1 .= "  Section :  ".$section_details[$row_emp[section_id]]."  ,";
			}
			if($status_subsec==1)
			{
				$c_part_1 .= "  Sub Section :  ".$subsection_details[$row_emp[subsection_id]]."  ,";
			}
		

			if($c_part_1!='')
			{
				$i=0;$sl=1;
				?><tr><td colspan="26"><b><? echo substr($c_part_1,0,-1); ?></b></td></tr><?
			}		
		 
 		}				
		 
 		
		//$ct++;
 			
		if ($ct%2==0) $bgcolor="#EFEFEF"; 
		else $bgcolor="#FFFFFF";
		
		//new add by ekram
		$b_ot_amt=$row_emp[over_time_rate]*$row_emp[b_over_time];
		//echo $b_ot_amt;die; 
		if($row_emp['duty_roster_policy']!=0 || $row_emp[b_over_time]==0)
		{
			$row_emp[b_net_payable_amount] = $row_emp[b_net_payable_amount]-($e_ot_amt+$b_ot_amt);
			$row_emp[b_total_earning_amount] = $row_emp[b_total_earning_amount]-($e_ot_amt+$b_ot_amt);
			$e_ot_amt = 0;
			$row_emp[b_over_time]=0;
			$row_emp[over_time_rate]=0;
		}		
    ?>
    <tbody>	
    	<tr height="102" bgcolor="<? echo $bgcolor; ?>">
        	<td align="center" valign="middle"><? echo $sl; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[id_card_no]; ?> <br><hr style="width:80%; border:1px dotted"> <? echo $row_emp[emp_code]; ?></td>
          
            <td align="center" valign="middle"><? echo $row_emp[first_name]." ".$row_emp[middle_name]." ".$row_emp[last_name].'<br><hr style="width:80%; border:1px dotted">'.$designation_chart[$row_emp[designation_id]]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[salary_grade]; ?></td>
            <td align="center" valign="middle"><div class="verticalText"><? echo $row_emp[joining_date]; ?></div></td>
            
            <td align="center" valign="middle"> 
			 <? 
			 $sql_attnd =" SELECT  ";
			// print_r($salary_head_mst_id);
			for ($i=1; $i<=count($salary_head_mst_id); $i++)
			{
				if ($i!=count($salary_head_mst_id))
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$salary_head_mst_id[$i]."' THEN round(pay_amount) END) AS 'salary_".$salary_head_mst_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$salary_head_mst_id[$i]."' THEN round(pay_amount) END) AS 'salary_".$salary_head_mst_id[$i]."'";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] order by salary_head_id";
			$sql_exe=mysql_query($sql_attnd);
			if($row_data = mysql_fetch_array($sql_exe))
		 //echo $sql_attnd;die; 
			{
				$k=0;
				foreach($salary_head_mst_id as $key) { 
					$k++;
							if(count($salary_head_mst_id)==$k)  echo $row_data['salary_'.$key]; else  echo $row_data['salary_'.$key]."<br />"; 
						}
			}
			 /*foreach($salary_head_mst as $key) { 
							echo $key."<br /><hr />";
						}
						 
			 echo number_format($row_emp[basic_salary])."<br>".number_format($row_emp[house_rent])."<br>".number_format($row_emp[medical_allowance])."<br>".number_format($row_emp[conveyence]); */ 	 ?></td>
            <td align="center" valign="middle"><? echo number_format($row_emp[gross_salary]); ?></td>
            <td align="center" valign="middle"><? echo $row_emp[total_calendar_days]."<br>".$row_emp[b_total_working_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[casual_leave]."<br>".$row_emp[sick_leave]."<br>".$row_emp[earn_leave]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[b_government_holiday]."<br>".$row_emp[b_total_weekly_holiday]."<br>".$row_emp[b_festival_holiday]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[b_total_present_days]+$row_emp[b_late_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[b_net_payable_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[b_absent_days]; ?></td>
            <td align="center" valign="middle"><? echo $row_emp[not_payble_days]; ?></td>
            <?
			
			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($earning_head_id); $i++)
			{
				if ($i!=count($earning_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$earning_head_id[$i]."' THEN pay_amount END) AS '".$earning_head_id[$i]."' ";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=0 order by salary_head_id";
			//echo $sql_attnd;die;  
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				for ($i=0; $i<count($earning_head_id); $i++)
				{
				
					if( $earning_head_id[$i]==5 )
					{
						 
						$ot_amnt = $row_emp[b_over_time]*$row_emp[over_time_rate]; 
						if($row_emp[b_over_time]==0 || $row_emp['duty_roster_policy']!=0)
							{
								$row_emp[b_total_earning_amount]= $row_emp[b_total_earning_amount] - $ot_amnt;
								$row_emp[b_net_payable_amount] = $row_emp[b_net_payable_amount] - $ot_amnt;
								$row_emp[b_over_time]=0;$row_emp[over_time_rate]=0;$ot_amnt=0;
							}	
						//section department division total
						$section_total_earning_head_amt[$i] += round($ot_amnt);
						$department_total_earning_head_amt[$i] += round($ot_amnt);
						$division_total_earning_head_amt[$i] += round($ot_amnt);
						$location_total_earning_head_amt[$i] += round($ot_amnt);
   						
						
			?>
            			<td align="center" valign="middle"><? echo $row_emp[b_over_time]."<br>".$row_emp[over_time_rate]."<br>".round($ot_amnt); ?></td>
            
            <? //$row_earn[$earning_head_id[$i]]
					}
					else
					{
						if($earning_head_id[$i]==35)
						{
							$row_earn[$earning_head_id[$i]]=round($row_emp[b_attendance_bonus]);
						}
						
						//section department division total
						$section_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						$department_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						$division_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						$location_total_earning_head_amt[$i] += round($row_earn[$earning_head_id[$i]]);
						
 			?>
            			<td align="center" valign="middle"><? echo number_format(round($row_earn[$earning_head_id[$i]])); ?></td>
           
            <? 		}
				}
			}
			
 			?>
            
             <td align="center" valign="middle"><? echo number_format(round($row_emp[b_total_earning_amount])); ?></td>
            
             <?
 			$sql_attnd =" SELECT  ";
			for ($i=0; $i<count($deduction_head_id); $i++)
			{
				if ($i!=count($deduction_head_id)-1)
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."', ";
				}
				else
				{
					$sql_attnd .=" sum(CASE WHEN salary_head_id ='".$deduction_head_id[$i]."' THEN pay_amount END) AS '".$deduction_head_id[$i]."' ";
				}
			}
			$sql_attnd .=" from hrm_salary_dtls where salary_mst_id=$row_emp[0] and head_type=1 order by salary_head_id";
			 
			$exe_sql_emp1=mysql_db_query($DB, $sql_attnd);
			if($row_earn = mysql_fetch_array($exe_sql_emp1))
			{
				
 				for ($i=0; $i<count($deduction_head_id); $i++)
				{
					 if($deduction_head_id[$i]==23)$row_earn[$deduction_head_id[$i]]=$row_emp['b_total_late_deduction_amount'];
					 if($deduction_head_id[$i]==24)$row_earn[$deduction_head_id[$i]]=$row_emp['b_total_absent_deduction_amount'];
					 //section department division total
					 $section_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 $department_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 $division_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 $location_total_deduction_head_amt[$i] += round($row_earn[$deduction_head_id[$i]]);
					 
			?>
            <td align="center" valign="middle"><? echo number_format(round($row_earn[$deduction_head_id[$i]])); ?></td>
           
            <?  
				}
			}
			?>
            
             <td align="center" valign="middle"><? echo number_format(round($row_emp[b_total_deduction_amount])); ?></td>
             <td align="center" valign="middle"><? echo number_format(round($row_emp[not_payble_days]*$row_emp[daily_gross_salary])); ?></td>
             <td align="center" valign="middle"><? echo number_format(round($row_emp[b_net_payable_amount])); ?></td>
             <? $cash = $row_emp[b_net_payable_amount]-$row_emp[bank_gross]; ?>
             <td align="center" valign="middle"><? if($cash>0){echo number_format(round($row_emp[bank_gross]));}else{$cash=0;$row_emp[bank_gross]=$row_emp[b_net_payable_amount];echo number_format(round($row_emp[bank_gross]));} ?></td>
             <td align="center" valign="middle"><? echo number_format(round($cash)); ?></td>
             <td></td>
        </tr>
        <?
	
				$ct++;
				//sub section 
				$subsection_total_gross += round($row_emp[gross_salary]);
				$subsection_total_salary += round($row_emp[b_total_earning_amount]);				
				$subsection_total_not_payable += round($row_emp[not_payble_days]*$row_emp[daily_gross_salary]);
				$subsection_total_deduction += round($row_emp[b_total_deduction_amount]);
				$subsection_total_net_payable += round($row_emp[b_net_payable_amount]);
				$subsection_total_bank += round($row_emp[bank_gross]);
				$subsection_total_cash += round($cash);
				 
 				//section 
				$section_total_gross += round($row_emp[gross_salary]);
				$section_total_salary += round($row_emp[b_total_earning_amount]);				
				$section_total_not_payable += round($row_emp[not_payble_days]*$row_emp[daily_gross_salary]);
				$section_total_deduction += round($row_emp[b_total_deduction_amount]);
				$section_total_net_payable += round($row_emp[b_net_payable_amount]);
				$section_total_bank += round($row_emp[bank_gross]);
				$section_total_cash += round($cash);
				 
				//department 
				$department_total_gross += round($row_emp[gross_salary]);
				$department_total_salary += round($row_emp[b_total_earning_amount]);				
				$department_total_not_payable += round($row_emp[not_payble_days]*$row_emp[daily_gross_salary]);
				$department_total_deduction += round($row_emp[b_total_deduction_amount]);
				$department_total_net_payable += round($row_emp[b_net_payable_amount]);
				$department_total_bank += round($row_emp[bank_gross]);
				$department_total_cash += round($cash);
				
				//division 
				$division_total_gross += round($row_emp[gross_salary]);
				$division_total_salary += round($row_emp[b_total_earning_amount]);				
				$division_total_not_payable += round($row_emp[not_payble_days]*$row_emp[daily_gross_salary]);
				$division_total_deduction += round($row_emp[b_total_deduction_amount]);
				$division_total_net_payable += round($row_emp[b_net_payable_amount]);
				$division_total_bank += round($row_emp[bank_gross]);
				$division_total_cash += round($cash);
				
				//division 
				$location_total_gross += round($row_emp[gross_salary]);
				$location_total_salary += round($row_emp[b_total_earning_amount]);				
				$location_total_not_payable += round($row_emp[not_payble_days]*$row_emp[daily_gross_salary]);
				$location_total_deduction += round($row_emp[b_total_deduction_amount]);
				$location_total_net_payable += round($row_emp[b_net_payable_amount]);
				$location_total_bank += round($row_emp[bank_gross]);
				$location_total_cash += round($cash);
				
				//grand total
				$grand_total_gross 			+= round($row_emp[gross_salary]);
				$grand_total_salary 		+= round($row_emp[b_total_earning_amount]);
				$grand_total_not_payable 	+= round($row_emp[not_payble_days]*$row_emp[daily_gross_salary]);
				$grand_total_deduction 		+= round($row_emp[b_total_deduction_amount]);
				$grand_total_net_payable 	+= round($row_emp[b_net_payable_amount]);
				$grand_total_bank 			+= round($row_emp[bank_gross]);
				$grand_total_cash 			+= round($cash);
				
	}//while loop end here 
	
	?>
    
	</tbody>   
    <?
    	if($status_subsec==1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Total</td>
					<td align="center"><? echo number_format($subsection_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($subsection_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($subsection_total_salary); ?></td> 
					<? foreach($subsection_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($subsection_total_deduction); ?></td>
					<td align="center" ><? echo number_format($subsection_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($subsection_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($subsection_total_bank); ?></td>
					<td align="center" ><? echo number_format($subsection_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$subsection_total_gross=0;$subsection_total_salary=0;$subsection_total_deduction=0;$subsection_total_not_payable=0;$subsection_total_net_payable=0;$subsection_total_bank=0;$subsection_total_cash=0;	
			$subsection_total_earning_head_amt = array();$subsection_total_deduction_head_amt = array();

		}
		
		if($status_sec==1)
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Section Total</td>
					<td align="center"><? echo number_format($section_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($section_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($section_total_salary); ?></td> 
					<? foreach($section_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($section_total_deduction); ?></td>
					<td align="center" ><? echo number_format($section_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($section_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($section_total_bank); ?></td>
					<td align="center" ><? echo number_format($section_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$section_total_gross=0;$section_total_salary=0;$section_total_deduction=0;$section_total_not_payable=0;$section_total_net_payable=0;$section_total_bank=0;$section_total_cash=0;	
			$section_total_earning_head_amt = array();$section_total_deduction_head_amt = array();
						
		}
		
		if($status_dept==1 )
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Department Total</td>
					<td align="center"><? echo number_format($department_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($department_total_earning_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>
					<td align="center"><? echo number_format($department_total_salary); ?></td> 
					<? foreach($department_total_deduction_head_amt as $key=>$val ){ ?> 
					<td align="center"><? echo number_format($val); ?></td>
					<? } ?>  
					<td align="center" ><? echo number_format($department_total_deduction); ?></td>
					<td align="center" ><? echo number_format($department_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($department_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($department_total_bank); ?></td>
					<td align="center" ><? echo number_format($department_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$department_total_gross=0;$department_total_salary=0;$department_total_deduction=0;$department_total_not_payable=0;$department_total_net_payable=0;$department_total_bank=0;$department_total_cash=0;	
			$department_total_earning_head_amt = array();$department_total_deduction_head_amt = array();
		}
		 
		if($status_divis==1 )
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Division Total</td>
					<td align="center"><? echo number_format($division_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($division_total_earning_head_amt as $key=>$val ){ ?><td align="center"><? echo number_format($val); ?></td><? } ?>
					<td align="center"><? echo number_format($division_total_salary); ?></td> 
					<? foreach($division_total_deduction_head_amt as $key=>$val ){ ?><td align="center"><? echo number_format($val); ?></td><? } ?>  
					<td align="center" ><? echo number_format($division_total_deduction); ?></td>
					<td align="center" ><? echo number_format($division_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($division_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($division_total_bank); ?></td>
					<td align="center" ><? echo number_format($division_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$division_total_gross=0;$division_total_salary=0;$division_total_deduction=0;$division_total_not_payable=0;$division_total_net_payable=0;$division_total_bank=0;$division_total_cash=0;	
			$division_total_earning_head_amt = array();$division_total_deduction_head_amt = array();
 			
		}
		 
		if($status_loc==1 )
		{
 			?><tr style="font-weight:bold">
					<td align="center" colspan="6">Location Total</td>
					<td align="center"><? echo number_format($location_total_gross); ?></td>
					<td align="center" colspan="7"></td>
					<? foreach($location_total_earning_head_amt as $key=>$val ){ ?><td align="center"><? echo number_format($val); ?></td><? } ?>
					<td align="center"><? echo number_format($location_total_salary); ?></td> 
					<? foreach($location_total_deduction_head_amt as $key=>$val ){ ?><td align="center"><? echo number_format($val); ?></td><? } ?>  
					<td align="center" ><? echo number_format($location_total_deduction); ?></td>
					<td align="center" ><? echo number_format($location_total_not_payable); ?></td>            
					<td align="center" ><? echo number_format($location_total_net_payable); ?></td>
					<td align="center" ><? echo number_format($location_total_bank); ?></td>
					<td align="center" ><? echo number_format($location_total_cash); ?></td>
					<td align="center" >&nbsp;</td>
				</tr><?
				
			$location_total_gross=0;$location_total_salary=0;$location_total_deduction=0;$location_total_not_payable=0;$location_total_net_payable=0;$location_total_bank=0;$location_total_cash=0;	
			$location_total_earning_head_amt = array();$location_total_deduction_head_amt = array();
 			
		}
    ?>
        <tfoot>
            <tr>
                <td colspan="<? echo count($earning_head)+count($deduction_head)+21; ?> "><? echo signeture_table_peak(9,$rpt_company,"1700px"); ?></td>
            </tr>
        </tfoot>
</table>
 
 <? 
	//echo signeture_table_peak(9,$rpt_company,"1700px") ; 
	
	$html = ob_get_contents();
	ob_clean();	
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/"."*.xls") as $filename) 
		{			
            @unlink($filename);
		}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
 	}


//month generated
if($type=="select_month_generate")
	{		
		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
}

function signeture_table_peak($report_id,$company,$width)
{
	if ($width=="") $width="100%"; 
	$signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company");
	$signeture_list = explode('#',$signeture_allow_or_no);
	if($signeture_allow_or_no!= "")	
	{
	 echo '<table width="'.$width.'">
		<tr height="130" style="font-size:14px; font-family:Arial; font-weight:bold">
		 ';
		foreach($signeture_list as $key=>$value)
		{
			echo '<td align="center" valign="bottom" style="border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;border-bottom-style:hidden;">'.$value.'</td> ';
		}
		echo '</tr></table>';
	}
	else
	{
		echo '<table width="'.$width.'">
		<tr height="130" style="font-size:24px; font-family:Arial; font-weight:bold">
		 ';
		 
		echo '</tr></table>';
	}
}
?>