<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

if( $action == "employee_wise_leave_report" ) {	
	
	//html to xls file name and output link
	$temp_file_name_in_out=$_SESSION['temp_file_name_in_out'];
	unlink('tmp_report_file/'.$temp_file_name_in_out);
	$name=time();
	$name="$name".".xls";
	$_SESSION['temp_file_name_in_out']=$name;	
	echo "<div style='width:250px;background-color:#F0F0B8;font-weight:bold; font-size:18px;'>Convert To <a href='includes/tmp_report_file/".$name."'><img src='includes/logo/excel-logo.png' name='logo_id' id='logo_id' /></a></div><br \>";
		
	$status = "and dtls.leave_status in ('CL','ML','SL','EL','SpL','LWP','EdL')";
	if($txt_from_date!='' && $txt_to_date!=''){
		$txt_from_date=convert_to_mysql_date($txt_from_date);
		$txt_to_date=convert_to_mysql_date($txt_to_date);
		$search_date = "dtls.leave_date >= '$txt_from_date' and dtls.leave_date <= '$txt_to_date' and";
		$active_deleted = "and status_active=1 and is_deleted=0";
	}
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and job.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and job.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and job.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and job.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and emp.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
		
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and job.department_id='$department_id'";			
		}
	$emp_code = $_GET['txt_emp_code'];//search employee code
	if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and emp_code in $search_emp_code";		
		}
	else {$search_code='';}
	
	$sql_d = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 and is_deleted=0 $search_code $cbo_company_id $location_id $department_id $section_id $subsection_id $designation_id $category order by company_id";	
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );	
	$html = "<table id=\"tblCustomer\" cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">";
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{			
			//$sql = "SELECT *  FROM hrm_leave_transaction where  id in( select distinct leave_id from hrm_leave_transaction_details where leave_date >= '$txt_from_date' and leave_date <= '$txt_to_date')  and status_active=1 ".$status."and emp_code=".$row_dp["emp_code"]." order by 'from_date' asc";
			$sql = "SELECT dtls.*,mst.remarks FROM hrm_leave_transaction_details dtls,hrm_leave_transaction mst where $search_date mst.status_active=1 $status and mst.id=dtls.leave_id and dtls.emp_code=".$row_dp["emp_code"];
			//echo $sql;
		 	//exit();
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );						
			$emp_tot=mysql_num_rows($result);			
			if ($emp_tot>0)
				{				
					$designation_name=return_field_value("custom_designation","lib_designation","id=$row_dp[designation_id]");					
					$department_name=return_field_value("department_name","lib_department","id=$row_dp[department_id]");
					$company_name=return_field_value("company_name","lib_company","id=$row_dp[company_id]");
					
	$html .= "<tr id=\"rowHeader\" bgcolor=#F0F0B8>		
						<td colspan=7>Employee Code:  <b>$row_dp[emp_code]</b>    Employee Name:    <b>$row_dp[name]</b>   Designation: <b>$designation_name</b></td>											
					</tr>
					<tr>						
						<td><b>Company&Department</b></td>						
						<td><b>Leave Type</b></td>
						<td><b>From Date</b></td>
						<td><b>To Date</b></td>
						<td><b>Total Days</b></td>
						<td><b>Cumulative</b></td>
						<td><b>Remark</b></td>
					</tr>";							
			
			$grandTotal=0;
			$days=1;
			while( $row = mysql_fetch_array( $result ) ) 
			{	
						$grandTotal++;				
				
	$html .= "		<tr>
						<td></td>						
					</tr>					
					<tr class=\"gradeA\">
						<td>$company_name"." , "."$department_name</td>
						<td>$row[leave_status]</td>						
						<td><b>$row[leave_date]</b></td> 	
						<td><b>$row[leave_date]</b></td>
						<td>$days</td>
						<td>$grandTotal</td>
						<td>$row[remarks]</td>						
					</tr>";				
			}
			//bgcolor=#E0ECF8		
	$html .= "
					<tr> 
						<td colspan=4><b>Grand Total</b></td>
						<td colspan=2>$grandTotal</td>
					</tr>";
		}
	
	}
		$html .= " </table>";
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/"."*.xls") as $filename){			
                @unlink($filename);
			}
	
	//html to xls convert
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html";//."####"."$name";
	exit();
	
}


if( $action == "employee_summ_leave_report" ) {	
	
	//html to xls file name and output link
	$temp_file_name_in_out=$_SESSION['temp_file_name_in_out'];
	unlink('tmp_report_file/'.$temp_file_name_in_out);
	$name=time();
	$name="$name".".xls";
	$_SESSION['temp_file_name_in_out']=$name;	
	echo "<div style='width:250px;background-color:#F0F0B8;font-weight:bold; font-size:18px;'>Convert To <a href='includes/tmp_report_file/".$name."'><img src='includes/logo/excel-logo.png' name='logo_id' id='logo_id' /></a></div><br \>";
		
	if($txt_from_date!='' && $txt_to_date!=''){
		$txt_from_date=convert_to_mysql_date($txt_from_date);
		$txt_to_date=convert_to_mysql_date($txt_to_date);
		$search_date = "leave_date >= '$txt_from_date' and leave_date <= '$txt_to_date' and";
	}
	$status="and leave_status in ('CL','ML','SL','EL','SpL','LWP','EdL')";		
	//if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and job.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id='$department_id'";
		
	if ($cbo_company_id==0) 
		{
			$company_id="";			
		}
	else
		{
			$company_id="where id='$cbo_company_id'";			
		}
			
	$sql_company = "SELECT id, company_name  FROM lib_company $company_id order by id";
	$result_sql_company = mysql_query( $sql_company ) or die( $sql_company . "<br />" . mysql_error() );
		
	$html = "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\" style=\"table-layout:950\">";
	while( $row_company = mysql_fetch_array( $result_sql_company ) ) 
	{
		$emp_code = $_GET['txt_emp_code'];//searching employee code
		if(strlen($emp_code)>1){
			$emp_code_explode=explode(",",trim($emp_code));
			for($i=0;$i<count($emp_code_explode);$i++)
				{
					if($i==0){$search_emp_code='('.$emp_code_explode[$i];}
					else
					{$search_emp_code .=','. $emp_code_explode[$i];}
				}
		$search_emp_code .=')';	
		$search_code = "and emp_code in $search_emp_code";		
		}
		else {$search_code='';}
		
		$sql_d = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee where status_active=1 and is_deleted=0 $search_code and company_id=$row_company[id] $location_id $department_id $section_id $subsection_id $designation_id $category order by designation_id";	
		//echo $sql_d;
		$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );	
		$check=0;
		while( $row_dp = mysql_fetch_array( $result_d ) ) 
			{			
					//$sql = "select distinct emp_code from hrm_leave_transaction_details where leave_date BETWEEN '$txt_from_date' and '$txt_to_date' and status_active=1 ".$status."and emp_code=".$row_dp["emp_code"]."  order by id asc";
					$sql_leave = "select count(leave_status) as total_leave, emp_code, leave_id, leave_status from hrm_leave_transaction_details where $search_date status_active=1 $status and emp_code=".$row_dp["emp_code"]." group by leave_status";
					
					/*$sql_leave = "select count(CASE WHEN leave_status ='CL' THEN emp_code END) AS 'CL', count(CASE WHEN leave_status ='SL' THEN emp_code END) AS 'SL',count(CASE WHEN leave_status ='EL' THEN emp_code END) AS 'EL',count(CASE WHEN leave_status ='ML' THEN emp_code END) AS 'ML',count(CASE WHEN leave_status ='SpL' THEN emp_code END) AS 'SpL',count(CASE WHEN leave_status ='LWP' THEN emp_code END) AS 'LWP',count(CASE WHEN leave_status ='EdL' THEN emp_code END) AS 'EdL', emp_code from hrm_leave_transaction_details where leave_date BETWEEN '$txt_from_date' and '$txt_to_date' and status_active=1 and emp_code='$row_dp[emp_code]'";
					//exit();
					*/
					$result_leave = mysql_query( $sql_leave ) or die( $sql_leave . "<br />" . mysql_error() );
					$nurows_leave=mysql_num_rows($result_leave);
					if($nurows_leave>0)
					{													
						
						if($check==0){						
							$department_name=return_field_value("department_name","lib_department","id=$row_dp[department_id]");									
							$html .= "<tr bgcolor=\"#F0F0B8\">		
										<td colspan=19>Company Name: <b>$row_company[company_name]</b></td>											
									</tr>
									<tr>						
											<td rowspan=2 width=50><b>Emp Code</b></td>
											<td rowspan=2 width=250><b>Name</b></td>						
											<td rowspan=2 width=130><b>Designation</b></td>
											<td rowspan=2 width=100><b>Department</b></td>
											<td rowspan=2 width=70><b>DOJ</b></td>					
											<td colspan=7 align=\"center\" width=200><b>Leave Availed</b></td>
											<td colspan=7 align=\"center\" width=200><b>Leave Balance</b></td>                       
										</tr>
										<tr>                      
											<td width=50><b>CL</b></td>
											<td width=50><b>SL</b></td>
											<td width=50><b>EL</b></td>
											<td width=50><b>SpL</b></td>
											<td width=50><b>LWP</b></td>
											<td width=50><b>ML</b></td>	
											<td width=50><b>EdL</b></td>	
											<td width=50><b>CL</b></td>
											<td width=50><b>SL</b></td>
											<td width=50><b>EL</b></td>
											<td width=50><b>SpL</b></td>
											<td width=50><b>LWP</b></td>
											<td width=50><b>ML</b></td>	
											<td width=50><b>EdL</b></td>
										</tr>";	
									
									$check=1;
							}				
						
						
						$cl_total=0;$sl_total=0;$el_total=0;$spl_total=0;$lwp_total=0;$ml_total=0;$edl_total=0;							
						while( $row = mysql_fetch_array( $result_leave ) ) 
						{				
							//total leave calculation here//	
							if($row["leave_status"]=='CL'){	$cl_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='SL'){	$sl_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='EL'){	$el_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='SpL'){ $spl_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='LWP'){ $lwp_total=$row["total_leave"];}				
							
							if($row["leave_status"]=='ML'){ $ml_total=$row["total_leave"];}	
							
							if($row["leave_status"]=='EdL'){ $edl_total=$row["total_leave"];}	
							
							//leave balace calculation
							//$leave_balance_sql="select balance, leave_type from hrm_leave_balance bal where emp_code=$row[emp_code] and leave_year in ('select  .id=$row_dp[location_id] and leave_year=$leave_year ";
							
							$leave_year=return_field_value("id","lib_policy_year","type=0 and is_locked=0 limit 1");
							$cl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'CL'");
							if($cl_balance=='')$cl_balance='0';
							$cl_balance=$cl_balance-$cl_total;
							$sl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'SL'");
							if($sl_balance=='')$sl_balance='0';
							$sl_balance=$sl_balance-$sl_total;
							$el_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'EL'");
							if($el_balance=='')$el_balance='0';
							$el_balance=$el_balance-$el_balance;
							$spl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'SpL'");
							if($spl_balance=='')$spl_balance='0';
							$spl_balance=$spl_balance-$spl_total;
							$lwp_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'LWP'");
							if($lwp_balance=='')$lwp_balance='0';
							$lwp_balance=$lwp_balance-$lwp_total;
							//ML incomplete								
							$ml_limit=return_field_value("days_required","hrm_leave_transaction tr, hrm_maternity_leave mat","tr.emp_code=$row[emp_code] and tr.emp_code=mat.emp_code and tr.leave_type = 'ML' and tr.status_active=1 and tr.is_deleted=0");	
							if($ml_balance=='')$ml_balance='0'; else $ml_balance=$ml_limit-$ml_total;
							//Education Leave
							$edl_balance=return_field_value("leave_limit","hrm_leave_balance","emp_code=$row[emp_code] and leave_year=$leave_year and leave_type = 'EdL'");	
							if($edl_balance=='')$edl_balance='0';
							$edl_balance=$edl_balance-$edl_total;
							}
											
						$designation_name=return_field_value("custom_designation","lib_designation","id=$row_dp[designation_id]");
							
				$html .= "											
						<tr class=\"gradeA\">
							<td>$row_dp[emp_code]</td>
							<td>$row_dp[name]</td>
							<td>$designation_name</td>
							<td>$department_name</td>
							<td>$row_dp[joining_date]</td>						
						
								<td>$cl_total</td>
								<td>$sl_total</td>
								<td>$el_total</td>
								<td>$spl_total</td>
								<td>$lwp_total</td>
								<td>$ml_total</td>
								<td>$edl_total</td>	
						
								<td>$cl_balance</td>
								<td>$sl_balance</td>
								<td>$el_balance</td>
								<td>$spl_balance</td>
								<td>$lwp_balance</td>
								<td>$ml_balance</td>
								<td>$edl_balance</td>
								
						</tr>";						
				}
			}
		}
		$html .= " </table>";
	
	 
		//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/"."*.xls") as $filename) {
			//if( @filemtime($filename) < (time()-$seconds_old) )
                @unlink($filename);
			}
		//---------end------------//
	
	//html to xls convert
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html";//."####"."$name";
	exit();
	
	print_r( $html );
	exit();
}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>

