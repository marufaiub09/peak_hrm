<? 
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row);
	}
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_leave.js" type="text/javascript"></script>	
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
        
	<script type="text/javascript" charset="utf-8">
		var location_details = division_details = department_details = section_details = new Array();
		
		<?php
		foreach( $location_details AS $location_id => $location ) {
			echo "location_details[$location_id] = new Array();\n";
			foreach( $location AS $key => $value ) {
				if( $key == 'id' || $key == 'company_id' ) echo "location_details[$location_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "location_details[$location_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $division_details AS $division_id => $division ) {
			echo "division_details[$division_id] = new Array();\n";
			foreach( $division AS $key => $value ) {
				if( $key == 'id' || $key == 'location_id' ) echo "division_details[$division_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "division_details[$division_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $department_details AS $department_id => $department ) {
			echo "department_details[$department_id] = new Array();\n";
			foreach( $department AS $key => $value ) {
				if( $key == 'id' || $key == 'division_id' ) echo "department_details[$department_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "department_details[$department_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $section_details AS $section_id => $section ) {
			echo "section_details[$section_id] = new Array();\n";
			foreach( $section AS $key => $value ) {
				if( $key == 'id' || $key == 'department_id' ) echo "section_details[$section_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "section_details[$section_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "../hrm/hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
		
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
	
	//numeric field script
    function numbersonly(myfield, e, dec)
    {
        var key;
        var keychar;
    
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);
    
        // control keys
        if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
        return true;
        
        // numbers
        else if ((("0123456789,").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }	
	function openmypage_employee_info(page_link,title)
		{
			// alert(page_link);
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=690px,height=420px,center=1,resize=0,scrolling=0','')
			
			emailwindow.onclose=function()
			{
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");				
				$('#txt_emp_code').val(thee_id.value);
			}
		}
	</script>
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }		
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; height:50px; margin:5px 0;">
		<h2> Employee Leave Summary Report</h2>
		<div align="center" id="messagebox"></div>
	</div>
	<form id="employee_wise_leave_report_form" action="javascript:fnc_emp_summ_leave_report()" autocomplete="off" method="POST">
		<fieldset id="filter_panel" >
			<legend>Filter Panel</legend>
			<table style="width:50%;" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td colspan="2" align="center">Select Date</td>
                    <td>Select Category</td>                   
					<td>Select Company</td>
                    <td>Select Location</td>
					<td>Select Department</td>
                    <td>Select Section</td> 
                    <td>Select SubSection</td>
                    <td>Select Designation</td>                 	
				  <td></td>
				</tr>
                <tr>
					
					<td>
						 <input type="text" name="txt_from_date" id="txt_from_date" value="" class="datepicker" size="12" />
					</td>
                    <td>
						 <input type="text" name="txt_to_date" id="txt_to_date" value="" class="datepicker" size="12" />
					</td>
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes">
                         	<option value="">All Category</option>
                            <option value="0">Top Management</option>
							<option value="1">Mid Management</option>
							<option value="2">Non Management</option>
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:160px" onchange="populate_cost_center( 'location', this.value );">
							<option value="0">-- Select --</option>
                              <?php  
									$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );									
									while( $row = mysql_fetch_assoc( $result ) ) {										
								?>							
							<option value="<?php echo $row['id']; ?>"><?php echo $row['company_name']; ?></option>
								<?php 
									} 
								?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:160px" onchange="populate_cost_center( 'division', this.value );">
                          <option value="0">-- Select --</option>
                          <?php  
                                        $sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
                                        $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
                                        while( $row = mysql_fetch_assoc( $result ) ) {										
                                    ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['location_name']; ?></option>
                          <?php 
                                        } 
                                    ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:160px" onchange="populate_cost_center( 'section', this.value );">
							<option value="0">-- Select --</option>
								<?php  
									$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
									while( $row = mysql_fetch_assoc( $result ) ) {										
								?>							
							<option value="<?php echo $row['id']; ?>"><?php echo $row['department_name']; ?></option>
								<?php 
									} 
								?> 
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:150px" onchange="">
							<option value="0">-- Select --</option>
								<?php  
									$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
									$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );								
									while( $row = mysql_fetch_assoc( $result ) ) {										
								?>							
							<option value="<?php echo $row['id']; ?>"><?php echo $row['section_name']; ?></option>
								<?php 
									} 
								?> 
						</select>
                    </td>          
                   <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td><!--<input type="submit" name="search" id="search" value="Search" class="formbutton" />--></td>
				</tr>
                <tr>					
                    <td colspan="5" align="right"><input type="text" name="txt_emp_code" id="txt_emp_code" value="" class="text_boxes" size="40" onkeypress="return numbersonly(this,event)" ondblclick="openmypage_employee_info('includes/search_emp_info_conf.php','Employee Information'); return false" placeholder="Double Click For Search" /></td>                   					
                    <td colspan="4"><input type="submit" name="search" id="search" value="Search" class="formbutton" /></td>                 	
				  <td></td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>

	<div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
	
</body>
</html>