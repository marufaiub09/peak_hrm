<? 
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}
		

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hrm.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script type="text/javascript" charset="utf-8">
				
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
		
		
		
	
	function openmypage(page_link,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=450px,center=1,resize=0,scrolling=0','../../')
		/*
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemailid=this.contentDoc.getElementById("txt_seleted_po_id") //Access form field with id="emailfield"
			alert(theemailid.value);
			
			knitting_dyeing_update(theemailid.value,15);
			setTimeout('showResult_search(document.getElementById("txt_program_no").value,9,"knitting_dying_container","sd",2)',150);
			
		}*/
	}
	
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; height:50px; margin:5px 0;">
		<h2> Daily OT Report</h2>
		<div align="center" id="messagebox"></div>
	</div>
	<form id="punch_report_form" action="javascript:fnc_daily_ot_report()" autocomplete="off" method="POST">
		<fieldset id="filter_panel" >
			<legend>Filter Panel</legend>
			<table style="width:50%;" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>Select Date</td>
                    <td>Status</td>
                    <td>Select Category</td>
					<td>Select Company</td>
                    <td>Select Location</td>
					<td>Select Department</td>
                    <td>Select Section</td>
                    <td>Select SubSection</td>
                    <td>Select Designation</td>
                   <!-- <td>Group By</td> -->			
					<td></td>
				</tr>
                <tr>
					
					<td>
						 <input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="10" />
					</td>
                    <td>
						 <select name="cbo_status" id="cbo_status" class="combo_boxes">
                         	<option value="0">All Status</option>
                            <option value="1">Present</option>
                            <option value="2">Absent</option>
                            <option value="3">Late</option>
                            <option value="4">Leave</option>
                            <option value="5">Movement</option>
                         </select>
					</td>
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes">
                         	 <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td>
						<select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:180px" onchange="populate_cost_center( 'location', this.value );">
							<option value="0">-- Select --</option>
                            <?php foreach( $company_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                        <select name="location_id" id="location_id" class="combo_boxes" style="width:170px" onchange="populate_cost_center( 'division', this.value );">
                          <option value="0">-- Select --</option>
                            <?php foreach( $location_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
                      </select>
                  </td>
					<td>
						<select name="department_id" id="department_id" class="combo_boxes" style="width:160px" onchange="populate_cost_center( 'section', this.value );">
							<option value="0">-- Select --</option>
                            <?php foreach( $department_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
                    	<select name="section_id" id="section_id" class="combo_boxes" style="width:160px" onchange="">
							<option value="0">-- Select --</option>
                            <?php foreach( $section_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
                    </td>
                    <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px">
                       	 	<option value="0">-- Select --</option>
                            <?php foreach( $subsection_details AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" >
							<option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</td>
                    <!--
                    <td>
                    	<select name="group_by_id" id="group_by_id" class="combo_boxes" style="width:100px" onchange="populate_cost_center( 'section', this.value );">							
                            <option value="0">--Select--</option>
                            <option value="department">Department</option>
                            <option value="company">Company</option>
                            <option value="location">Location</option>
                        </select>
                    </td>
                    -->
					<td><input type="submit" name="search" id="search" value="Search" class="formbutton" /></td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>

	<div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
	
</body>
</html>