<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>

<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/functions.js"></script>
<script>

function showResult_source_transport(str,type,div)
{
	//alert(str+type+com_name+div);
var str2=document.getElementById('cbo_search_by').value;

if (str.length==0)
  {
  document.getElementById(div).innerHTML="";
  document.getElementById(div).style.border="0px";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
        document.getElementById(div).innerHTML=xmlhttp.responseText;   
    }
  }
// alert( type );
xmlhttp.open("GET","includes/emp_info_list_view.php?search_string="+str+"&search_string2="+str2+"&type="+type,true);
xmlhttp.send();

}

var selected_id = new Array, selected_name = new Array();
		
		function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}


</script>

</head>

<? 
include('../includes/common.php');
include('../includes/array_function.php');
?>
<body>
<div>
<form name="search_order_frm"  id="search_order_frm">
	<fieldset style="width:650px">
	<table width="650" cellspacing="2" cellpadding="0" border="0">
    <tr>
						<td align="center">
							You Have Selected: <textarea readonly="readonly" style="width:350px" class="text_area" name="txt_selected" id="txt_selected" ></textarea>
							<input type="hidden" readonly="readonly" style="width:250px" class="text_boxes" name="txt_selected_id" id="txt_selected_id" />
						</td>
					</tr>
		<tr>
			<td align="center">
				Search By&nbsp;
				<select name="cbo_search_by" id="cbo_search_by" class="combo_boxes" style="width:150px">
                <option value="1">Employee Name</option>
				<option value="0">Employee Code</option>
				<option value="2">Designation</option>
				<option value="3">Division</option>
				<option value="4">Department</option>
				<option value="5">Section</option>
				</select> &nbsp;Search&nbsp;<input type="text" name="search_text" id="search_text" class="text_boxes" style="width:150px" onkeyup="showResult_source_transport(this.value,'search_emp_info_conf','search_div_conf')" autocomplete=off /><input type="hidden" name="txt_selected_emp" id="txt_selected_emp" /> 
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div style="width:650px; overflow-y:scroll; min-height:260px; max-height:260px;" id="search_div_conf" align="left">
					
				</div>
			</td>
        </tr>
        <tr>
			<td align="center" height="30" valign="bottom">
					<div style="width:100%"> 
							<div style="width:50%; float:left" align="left">
								<input type="checkbox" name="check_all" id="check_all" onclick="check_all_data()" /> Check / Uncheck All
							</div>
							<div style="width:50%; float:left" align="left">
							<input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close" />
							</div>
					</div>
			</td>
		</tr>
	</table>
	</fieldset>
	</form>
</div>
</body>
</html>