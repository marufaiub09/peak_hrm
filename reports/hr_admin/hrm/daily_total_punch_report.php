<?php
include('../../../includes/common.php');

if( file_exists( "../../../cache/company_details.txt" ) ) {
	$company_details = unserialize( file_get_contents( "../../../cache/company_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/company_details.txt", serialize( $company_details ) );
}
if( file_exists( "../../../cache/location_details.txt" ) ) {
	$location_details = unserialize( file_get_contents( "../../../cache/location_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/location_details.txt", serialize( $location_details ) );
}
if( file_exists( "../../../cache/division_details.txt" ) ) {
	$division_details = unserialize( file_get_contents( "../../../cache/division_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/division_details.txt", serialize( $division_details ) );
}
if( file_exists( "../../../../cache/department_details.txt" ) ) {
	$department_details = unserialize( file_get_contents( "../../../../cache/department_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row;
	}
	file_put_contents( "../../../../cache/department_details.txt", serialize( $department_details ) );
}
if( file_exists( "../../../../cache/section_details.txt" ) ) {
	$section_details = unserialize( file_get_contents( "../../../../cache/section_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row;
	}
	file_put_contents( "../../../../cache/section_details.txt", serialize( $section_details ) );
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hrm.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		var location_details = division_details = department_details = section_details = new Array();
		
		<?php
		foreach( $location_details AS $location_id => $location ) {
			echo "location_details[$location_id] = new Array();\n";
			foreach( $location AS $key => $value ) {
				if( $key == 'id' || $key == 'company_id' ) echo "location_details[$location_id]['$key'] = $value;\n";
				else echo "location_details[$location_id]['$key'] = '$value';\n";
			}
		}
		foreach( $division_details AS $division_id => $division ) {
			echo "division_details[$division_id] = new Array();\n";
			foreach( $division AS $key => $value ) {
				if( $key == 'id' || $key == 'location_id' ) echo "division_details[$division_id]['$key'] = $value;\n";
				else echo "division_details[$division_id]['$key'] = '$value';\n";
			}
		}
		foreach( $department_details AS $department_id => $department ) {
			echo "department_details[$department_id] = new Array();\n";
			foreach( $department AS $key => $value ) {
				if( $key == 'id' || $key == 'division_id' ) echo "department_details[$department_id]['$key'] = $value;\n";
				else echo "department_details[$department_id]['$key'] = '$value';\n";
			}
		}
		foreach( $section_details AS $section_id => $section ) {
			echo "section_details[$section_id] = new Array();\n";
			foreach( $section AS $key => $value ) {
				if( $key == 'id' || $key == 'department_id' ) echo "section_details[$section_id]['$key'] = $value;\n";
				else echo "section_details[$section_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
		
	</script>
	<style type="text/css">
		#filter_panel select { width:150px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; height:50px; margin:5px 0;">
		<h2>Daily Total Punch Report</h2>
		<div align="center" id="messagebox"></div>
	</div>
	<form id="punch_report_form" action="javascript:fnc_daily_total_punch_report()" method="POST">
		<fieldset id="filter_panel">
			<legend>Filter Panel</legend>
			<table style="width:100%;" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>Date:</td>
					<td><input type="text" name="date" id="date" value="" class="datepicker"   style="width:150px"/></td>
					
					<td>Emp Code</td>
					<td>
						<input type="text" name="txt_emp_code" id="txt_emp_code" class="text_boxes" style="width:150px" />
					</td>
					
					<td><input type="submit" name="search" id="search" value="Search" class="formbutton" /></td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>

	<div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
	
</body>
</html>