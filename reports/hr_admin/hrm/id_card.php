<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hrm.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			populate_data();
		});
		
		(function($) {
			$.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
				// check that we have a column id
				if( typeof iColumn == "undefined" ) return new Array();
				
				// by default we only wany unique data
				if( typeof bUnique == "undefined" ) bUnique = true;
				
				// by default we do want to only look at filtered data
				if( typeof bFiltered == "undefined" ) bFiltered = true;
				
				// by default we do not wany to include empty values
				if( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;
				
				// list of rows which we're going to loop through
				var aiRows;
				
				// use only filtered rows
				if( bFiltered == true ) aiRows = oSettings.aiDisplay; 
				// use all rows
				else aiRows = oSettings.aiDisplayMaster; // all row numbers

				// set up data array	
				var asResultData = new Array();
				
				for( var i = 0, c = aiRows.length; i < c; i++ ) {
					iRow = aiRows[i];
					var aData = this.fnGetData( iRow );
					var sValue = aData[iColumn];
					
					// ignore empty values?
					if( bIgnoreEmpty == true && sValue.length == 0 ) continue;

					// ignore unique values?
					else if( bUnique == true && jQuery.inArray(sValue, asResultData ) > -1 ) continue;
					
					// else push the value onto the result data array
					else asResultData.push( sValue );
				}
				if( asResultData.length == 0 ) return null;
				return asResultData;
			}
		}(jQuery));
		
		function fnCreateSelect( aData ) {
			var r = '<select><option value=""></option>', i, iLen = aData.length;
			for( i = 0 ; i < iLen ; i++ ) {
				r += '<option value="' + aData[i] + '">' + aData[i] + '</option>';
			}
			return r + '</select>';
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'form=id_card',
				success: function(data) {
					$('#data_panel').html( data );
					arrange_table();
				}
			});
		}
		
		function arrange_table() {
			oTable = $('#employees').dataTable({
				"bJQueryUI": true,
				"bPaginate": false,
				"aoColumns": [ { "bSortable": false }, null, null, null, null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false },
								{ "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false } ],
				"aaSorting": [[ 1, "asc" ]],
				"oLanguage": { "sSearch": "Search all columns:" },
				"sScrollX": "100%",
				"sScrollY": "250px",
				"sScrollXInner": "150%",
				"bScrollCollapse": true
			});
			/* Add a select menu for each TH element in the table header */
			var j = 1;
			$("#select_header th").each( function ( i ) {
				if( j > 5 && j < 13 ) {
					var data = oTable.fnGetColumnData( i );
					if( data != null ) {
						$(this).html( fnCreateSelect( data ) ).css('padding', '3px');
						$('select', this).change( function () {
							oTable.fnFilter( $(this).val(), i );
						});
					}
				}
				j++;
			});
		}
		
		function check_all( check ) {
			$('input', oTable.fnGetNodes()).attr('checked', check.checked);
		}
		
		function reset_form() {
			$('input', oTable.fnGetNodes()).attr('checked', false);
			$('#messagebox').html( '' ).removeClass();
		}
	</script>
	<style type="text/css">
		.formbutton { width:150px; }
		#select_header select { width:100%; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; height:50px; margin:5px 0;">
		<h2>ID Card</h2>
		<div align="center" id="messagebox"></div>
	</div>
	<form id="id_card_form" action="javascript:fnc_id_card()" method="POST">
		<div id="data_panel" class="demo_jui" align="center"></div>
		<div align="center" style="margin-top:10px;">
			<input type="hidden" name="checked_employees" id="checked_employees" value="" />
			<input type="submit" name="save" value="Generate ID Card" class="formbutton" />
		</div>
	</form>
</body>
</html>