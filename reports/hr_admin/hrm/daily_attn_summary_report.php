<? 
include('../../../includes/common.php');
include('../../../includes/array_function.php');


	//company_details
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	//location_details
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	//division_details
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	//department_details
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	//section_details
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	//designation_chart
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	//subsection_details
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hrm.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
    <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
	<script type="text/javascript" charset="utf-8">
		var location_details = division_details = department_details = section_details = subsection_details = new Array();
		
		<?php
		foreach( $location_details AS $location_id => $location ) {
			echo "location_details[$location_id] = new Array();\n";
			foreach( $location AS $key => $value ) {
				if( $key == 'id' || $key == 'company_id' ) echo "location_details[$location_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "location_details[$location_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $division_details AS $division_id => $division ) {
			echo "division_details[$division_id] = new Array();\n";
			foreach( $division AS $key => $value ) {
				if( $key == 'id' || $key == 'location_id' ) echo "division_details[$division_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "division_details[$division_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $department_details AS $department_id => $department ) {
			echo "department_details[$department_id] = new Array();\n";
			foreach( $department AS $key => $value ) {
				if( $key == 'id' || $key == 'division_id' ) echo "department_details[$department_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "department_details[$department_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		foreach( $section_details AS $section_id => $section ) {
			echo "section_details[$section_id] = new Array();\n";
			foreach( $section AS $key => $value ) {
				if( $key == 'id' || $key == 'department_id' ) echo "section_details[$section_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "section_details[$section_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}	
		foreach( $subsection_details AS $subsection_id => $subsection ) {
			echo "subsection_details[$subsection_id] = new Array();\n";
			foreach( $subsection AS $key => $value ) {
				if( $key == 'id' || $key == 'section_id' ) echo "subsection_details[$subsection_id]['$key'] = ".mysql_real_escape_string($value).";\n";
				else echo "subsection_details[$subsection_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
		
		function populate_cost_center( child, parent_id, selected_id ) {
			$.ajax({
				type: "POST",
				url: "hrm_data.php",
				data: 'cost_center=' + child + '&parent_id=' + parent_id + '&selected_id=' + selected_id,
				success: function( html ) {
					$('#' + child + '_id').html( html )
				}
			});
		}
		
		
		
	
	function openmypage(page_link,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=450px,center=1,resize=0,scrolling=0','../../')
		/*
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemailid=this.contentDoc.getElementById("txt_seleted_po_id") //Access form field with id="emailfield"
			alert(theemailid.value);
			
			knitting_dyeing_update(theemailid.value,15);
			setTimeout('showResult_search(document.getElementById("txt_program_no").value,9,"knitting_dying_container","sd",2)',150);
			
		}*/
	}
	
	$(document).ready(function() {
			$(".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:100%; height:50px; margin:5px 0;">
		<h2> Daily Attendance Summary </h2>
		<div align="center" id="messagebox"></div>
	</div>
	<form id="service_report_form" action="javascript:fnc_daily_attn_summary_report()" autocomplete="off" method="POST">
		<fieldset id="filter_panel" >
			<legend>Filter Panel</legend>
			<table style="width:50%;" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>Select Date</td>
                    <td>Select Category</td>
					<td>Select Company</td>
                    <td>Select Location</td>
                    <td>Select Division</td>
					<td>Select Department</td>
                    <td>Select Section</td>
                    <td>Select SubSection</td>
                    <td>Select Designation</td>
                   <!-- <td>Group By</td> -->			
					<td></td>
				</tr>
                <tr>
					
					<td>
						 <input type="text" name="txt_date" id="txt_date" value="" class="datepicker" size="12" />
					</td>
                    <td>
						 <select name="cbo_emp_category" id="cbo_emp_category" class="combo_boxes">
                         	 <option value="">All Category</option>
                            <?
                            foreach($employee_category as $key=>$val)
                            {
                            ?>
                            <option value="<? echo $key; ?>"><? echo $val; ?></option>
                            <?
                            }
                            ?> 
                         </select>
					</td>
                    <td id="company">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:140px" onchange="populate_cost_center( 'location', this.value );" >
							<option value="0">-- Select --</option>
                            <?php foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
							<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
							<?php } } ?>							
						</select>
					</td>
					<td id="location">
						<select name="location_id" id="location_id" class="combo_boxes" style="width:140px" > <!--onchange="populate_cost_center( 'division', this.value );"-->
							<option value="0">-- Select --</option>
							<?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
                            <option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
                            <?php } } ?>
                         </select>
					</td>
                  <td id="division">
						<select name="division_id" id="division_id" class="combo_boxes" style="width:140px" > <!--onchange="populate_cost_center( 'department', this.value );"-->
							<option value="0">-- Select --</option>
							<?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
                            <option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
                            <?php } } ?>
						</select>
                    </td>					
                    <td id="department">
                    <select name="department_id" id="department_id" class="combo_boxes" style="width:140px">  <!-- onchange="populate_cost_center( 'section', this.value );" -->
                        <option value="0">-- Select --</option>
                        <?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
                        <option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
                        <?php } } ?>
                    </select>
                    </td>
					
					 <td id="section">
						<select name="section_id" id="section_id" class="combo_boxes" style="width:140px">  <!--onchange="populate_cost_center( 'subsection', this.value );"-->
                        	<option value="0">-- Select --</option>
							<?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
							<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
                     <td id="subsection">
						<select name="subsection_id" id="subsection_id" class="combo_boxes" style="width:140px">
                       	 	<option value="0">-- Select --</option>
							<?php foreach( $subsection_details AS $subsection ) { if( $subsection['status_active'] == 1 ) { ?>
							<option value="<?php echo $subsection['id']; ?>"><?php echo $subsection['subsection_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
                    <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:140px" >
							<option value="0">-- Select --</option>
							<?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
							<option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
							<?php } } ?>
						</select>
					</td>                   
					<td><input type="submit" name="search" id="search" value="Search" class="formbutton" /></td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" align="center" class="demo_jui" style="margin-top:10px;"></div>

	<div id="data_panel2" align="center" class="demo_jui2" style="margin-top:10px;"></div>
	
</body>
</html>