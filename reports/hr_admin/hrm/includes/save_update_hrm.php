

<?php
session_start();

include('../../../../includes/common.php');
include('../../../../includes/common_functions.php');
include('../../../../includes/array_function.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );


//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Variable Settings
$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$var_hrm_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$var_hrm_chart[$row['company_name']] = array();
	$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
	$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
	$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
	$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
	$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
	$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
	

} 
if( $action == "monthly_attn_summery" ) {	
		
	if($txt_date!="")
	{
		$date_part=convert_to_mysql_date($txt_date);
		//$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	}
	else
	{
		$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	}
	
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";	
	if ($location_id==0) $location_id=""; else  $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";	
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";	
	
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="where id='$department_id'";				
		}
	
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
	
	$html = "<div align=center><table cellpadding=\"0\" width=\"1050\" cellspacing=\"1\" border=\"1\">";
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{			
			$sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee WHERE status_active=1 and department_id=$row_dp[id] ".$cbo_company_id."  ".$location_id." ".$section_id." ".$subsection_id." ".$category." ".$designation_id." order by designation_id asc";
			//echo $sqls;			
			$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );						
			$emp_tot=mysql_num_rows($results);		
			
			if ($emp_tot>0)
				{
			
	$html .= "
				 	<tr bgcolor=#F0F0B8>
						<td colspan=18><b>Department Name:</b> $row_dp[department_name]</td>												
					</tr>
					<tr>
						<td width='80'><b>Emp Code</b></td>
						<td width='190'><b>Emp Name</b></td>
						<td width='100'><b>Designation</b></td>
						<td width='70'><b>Join Date</b></td>
						
						<td width='30'><b>Total Days</b></td>
						<td width='50'><b>Present</b></td>
						<td width='45'><b>Absent</b></td>
						<td width='30'><b>Late</b></td>
						<td width='40'><b>Leave</b></td>
						<td width='30'><b>MR</b></td>
						
						<td width='30'><b>WOD</b></td>
						<td width='30'><b>HD</b></td>
						<td width='30'><b>PL</b></td>
						<td width='30'><b>PW</b></td>
						<td width='30'><b>PH</b></td>
						<td width='50'><b>Total Present</b></td>
						<td width='50'><b>Payable Days</b></td>
						<td><b>Job Card</b></td>
					</tr>
				 ";
	
 
 	$i=0;
	//$sqls = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.* from hrm_employee emp  order by emp.emp_code asc";
	
	while( $rows = mysql_fetch_assoc( $results ) ) {
		
				$present=0;
				$absent=0;
				$late=0;
				$leave=0;
				$whday=0;
				$ghday=0;
				$pl=0;
				$ph=0;
				$pw=0;
				$ot=0;
				$mr=0;
				$i++;
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";	
	$sql = "SELECT * FROM hrm_attendance  WHERE  attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]' order by attnd_date  asc";
	
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$r=0;
			
			while( $row = mysql_fetch_assoc( $result ) ) 
			{
				if ($row['status']=="P")
				{
					$present=$present+1;
				}
				else if ($row['status']=="A")
				{
					$absent=$absent+1;
				}
				else if ($row['status']=="D")
				{
					$late=$late+1;
				}
				else if ($row['status']=="W")
				{
					$whday=$whday+1;
					if($row[sign_in_time]!='00:00:00' || $row[sign_out_time]!='00:00:00') 
					{
						$pw=$pw+1; 
					}
				}
				else if($row[status]=='H' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH' )
				{
					$ghday=$ghday+1;
					if($row[sign_in_time]!='00:00:00' || $row[sign_out_time]!='00:00:00') 
					{
						$ph=$ph+1; 
					}
					
				}
				else if ($row[status]=='' || $row[status]=="MR" )
				{
					$mr=$mr+1;
				}
				else
				{
					$leave=$leave+1;
					if($row[sign_in_time]!='00:00:00' || $row[sign_out_time]!='00:00:00') 
					{
						$pl=$pl+1; 
					}
				}
				$ot=$ot+$row['total_over_time_min'];
			}
			$total_present=$present+$late+$pl+$pw+$ph+$mr;
			$total_days=$present+$late+$absent+$whday+$ghday+$leave+$mr;
			$payable_days=$total_days-$absent;
			if($ot!=0)
			{
				$ot_hr=round(($ot/60),2);
			}
			if($late>2)
			{ 
				$color='#EE3900';
			}
			else
			{
				$color='';
			}
				
			
			
			$html .= "
					<tr><td></td></tr>
					<tr class=\"gradeA\" bgcolor=$bgcolor>
						<td width='80'>$rows[emp_code]</td>
						<td width='190'>$rows[name]</td>
						<td align='left' width='100'>".$designation_chart[$rows['designation_id']]."</td>
						<td width='70'>$rows[joining_date]</td>
						
						<td width='30'>$total_days</td>
						<td width='50'><a href='#' onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=P&emp_name=$rows[name]','Partial Attendance'); return false\"> $present</a></td>
						<td width='45'><a href='#' onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=A&emp_name=$rows[name]','Partial Attendance'); return false\">$absent</a></td>
						<td width='30' bgcolor='$color'><a href='#' onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=D&emp_name=$rows[name]','Partial Attendance'); return false\">$late</a></td>
						<td width='40'><a href='#' onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=CL&emp_name=$rows[name]','Partial Attendance'); return false\">$leave</a></td>
						<td width='30'><a href='#' onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=MR&emp_name=$rows[name]','Partial Attendance'); return false\">$mr</a></td>
						
						<td width='30'><a href='#' onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=W&emp_name=$rows[name]','Partial Attendance'); return false\">$whday</a></td>
						<td width='30'><a href='#' onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=H&emp_name=$rows[name]','Partial Attendance'); return false\">$ghday</a></td>
						<td width='30'>$pl</td>
						<td width='30'>$pw</td>
						<td width='30'>$ph</td>
						<td width='50'>$total_present</td>
						<td  width='50'>$payable_days</td>
						<td  ><input type=\"button\" value=\"View\" style=\"width:35px\" class=\"formbutton\" onclick=\"openmypage('popup_report.php?emp_code=$rows[emp_code]&date=$date_part&status=JC&emp_name=$rows[name]','Partial Attendance'); return false\"</td>
					</tr>";
					
					//$r=0;
			}		
		}
	}	
		$html .= "</div> 
			</table></div><br><br>";
	
	//for report temp file delete 
	foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

if( $action == "daily_in_out_report" ) { //Punch Report
		
	list( $day, $month, $year ) = explode( "-", $txt_date );
	$date = $year . "-" . $month . "-" . $day;
	if($cbo_status==0) $status="";
	else if ($cbo_status==1) $status="and a.status='P '";
	else if ($cbo_status==2) $status="and a.status='A '";
	else if ($cbo_status==3) $status="and a.status='D '";
	else if ($cbo_status==4) $status="and a.status in ('CL','ML','SL','EL','SpL','LWP')";
	else if ($cbo_status==5) $status="and a.status='MR'";
	
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		 
		$in_out_time_format=$res['in_out_time_format']; 
	}
	
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
		
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and a.department_id='$department_id'";				
		}
	
	
	$new_dept=array();
		ob_start();
			$total_ot_hr=0; $total_ot_amnt=0;
			$p=0;$a=0;$mr=0;$d=0;$l=0;$total_buyer_ot_amount=0;
			$sql="SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 $department_id and a.attnd_date like '$date' and a.emp_code=emp.emp_code $location_id $section_id ".$cbo_company_id."  $status $subsection_id $designation_id $category order by a.department_id asc";
			 
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		//	echo $sql;			
			$emp_tot=mysql_num_rows($result);
			
			$new_dept=array();
			$i=0;
			while( $row = mysql_fetch_array( $result ) ) 
			{
				if (in_array($row['department_id'], $new_dept))
				{
					$i++;
				}
				else
				{
					
				  echo "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">
						<tr bgcolor=#F0F0B8>
							<td colspan=15><b>Department Name:</b> ".$department_details[$row[department_id]]."</td>
						</tr>
						<tr>
							<td width=50><b>Emp Code</b></td>
							<td width=50><b>ID Card No</b></td>
							<td width=50><b>Punch Card No</b></td>
							<td width=140><b>Name</b></td>
							<td width=100><b>Designation</b></td>
							<td width=90><b>Department</b></td>
							<td width=120><b>Location</b></td>
							<td width=80><b>Section</b></td>
							<td width=50><b>Sub Section</b></td>
							<td width=80><b>Attnd. Date</b></td>
							<td width=40><b>In Time</b></td>
							<td width=40><b>Out Time</b></td>
							<td width=30><b>Status</b></td>
							<td width=50><b>Late Min</b></td>
							<td width=50><b>Over Time</b></td>
						</tr>";
						$new_dept[$i]=$row[department_id];
						$i++;
				}
			
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$buyer_ot_rate=round(($basic_salary/208)*2,2);
			$buyer_ot_hr=get_buyer_ot_hr($row[total_over_time_min],$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit']);
					 					
				echo "<tr>
						<td></td>						
					</tr>
					 
					<tr class=\"gradeA\">
						<td>$row[emp_code]</td>
						<td>$row[id_card_no]</td>
						<td>$row[punch_card_no]</td>
						<td>$row[name]</td>
						<td>".$designation_chart[$row['designation_id']]."</td>
						<td>".$department_details[$row['department_id']]."</td>						
						<td>".$location_details[$row['location_id']]."</td>
						<td>".$section_details[$row['section_id']]."</td>
						<td>".$subsection_details[$row['subsection_id']]."</td>
						<td align='center'>".convert_to_mysql_date($row[attnd_date])."</td>
						<td width=''>".show_time_format( get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $var_hrm_chart[$row['company_id']]['adjust_in_time'] ), $in_out_time_format )."</td>						
						<td width=''>".show_time_format( get_buyer_out_time($row[sign_out_time], $row[r_sign_out_time], $var_hrm_chart[$row['company_id']]['adjust_out_time'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] ), $in_out_time_format )."</td> 
						<td>$row[status]</td>
						<td align='right'>$row[late_time_min]</td>
						<td width='' align='right'>".get_buyer_ot_hr($row[total_over_time_min], $var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] )."</td>
					</tr>";
					$total_ot_hr=$total_ot_hr+$buyer_ot_hr;
					$total_ot_amnt=$total_ot_amnt+($buyer_ot_rate*$buyer_ot_hr);
					if($row["status"]=='P'){$p++;}
					if($row["status"]=='A'){
							$a++;
							if($a>1){
									$param_emp_code .=',';
									}
							$param_emp_code .=$row["emp_code"];
							
						}
					if($row["status"]=='MR')
							{
								$mr++;
								if($mr>1){
									$mr_emp_code .=',';
									}
								$mr_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='D')
							{
								$d++;
								if($d>1){
									$delay_emp_code .=',';
									}
								$delay_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='CL' || $row["status"]=='SL' || $row["status"]=='ML' || $row["status"]=='EL' || $row["status"]=='LWP' || $row["status"]=='SpL')
							{
								$l++;
								if($l>1){
									$leave_emp_code .=',';
									}
								$leave_emp_code .=$row["emp_code"];
							}
			}
			
		$total=$p+$a+$l+$d+$mr;
		echo "<tr bgcolor=#F0F0B8>
					<td colspan=15 align=\"center\"><b>Summary</b></td>
				  </tr>
				  <tr>				  	
					<td colspan=2></td>
				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Present :</b> $p </td>								  	
					<td  align=\"left\" bgcolor=#FFFFFF><b>Absent :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$param_emp_code&date=$date&type=1','Absent Information'); return false\"> $a</a> </td>				  			  	
 				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Leave :</b> <a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$leave_emp_code&date=$date&type=1','Leave Information'); return false\">$l</a> </td>				  				   
					<td  align=\"left\" bgcolor=#FFFFFF><b>Movement :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$mr_emp_code&date=$date&type=1','Movement Information'); return false\"> $mr </a> </td>				  			    
					<td  align=\"left\" bgcolor=#E5E5E5><b>Late :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$delay_emp_code&date=$date&type=1','Late Information'); return false\"> $d </a></td>				  			    
					<td  colspan=2 align=\"left\" bgcolor=#FFFFFF><b>Total :</b> $total </td>					
				  </tr>
				 </table>";
			
	
	//for report temp file delete 
	foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	//$is_created = fwrite($create_new_excel,$html);
	$is_created = fwrite($create_new_excel,ob_get_contents());
	echo "$html"."####"."$name";
	exit();
}

if($action=="daily_ot_report")// daily buyer ot report
{
	//echo $cbo_company_id;die;
	//-------------
	list( $day, $month, $year ) = explode( "-", $txt_date );
	$date = $year . "-" . $month . "-" . $day;
	if($cbo_status==0) $status="";
	else if ($cbo_status==1) $status="and a.status='P '";
	else if ($cbo_status==2) $status="and a.status='A '";
	else if ($cbo_status==3) $status="and a.status='D '";
	else if ($cbo_status==4) $status="and a.status in ('CL','ML','SL','EL','SpL','LWP')";
	else if ($cbo_status==5) $status="and a.status='MR'";
		
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
	
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and a.department_id='$department_id'";				
		}
		$new_dept=array();
		ob_start();
			$total_ot_hr=0; $total_ot_amnt=0;
			$p=0;$a=0;$mr=0;$d=0;$l=0;$total_buyer_ot_amount=0;
			$sql="SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 $department_id and a.attnd_date like '$date' and a.emp_code=emp.emp_code $location_id $section_id ".$cbo_company_id."  $status $subsection_id $designation_id $category order by a.department_id asc";
			 
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			// echo $sql;			
			$emp_tot=mysql_num_rows($result);
			
			$new_dept=array();
			$j=1;
			$i=0;
			$k=0;
			while( $row = mysql_fetch_array( $result ) ) 
			{
				 $j++;
				if (in_array($row['department_id'], $new_dept))
				{
					$i++;
				}
				else
				{
					
					if ($k!=0)
					{
						
						echo "					
							<tr>
								<td></td>						
							</tr>
							<tr class=\"gradeA\">
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>						
								<td></td>
								<td></td>
								<td></td>
								<td>Total</td>
								<td align=right>$total_ot_hr</td>
								<td></td>
								<td align=right>$total_ot_amnt</td>
							</tr>";
								$total_buyer_ot_amount=$total_buyer_ot_amount+$total_ot_amnt; 
								$total_ot_hr=0; $total_ot_amnt=0;
					}
					
					$k++; 
					
				  echo "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">
						<tr bgcolor=#F0F0B8>
							<td colspan=15><b>Department Name:</b> ".$department_details[$row[department_id]]."</td>
						</tr>
						<tr>
							<td width=50><b>Emp Code</b></td>
							<td width=50><b>ID Card No</b></td>
							<td width=50><b>Punch Card No</b></td>
							<td width=140><b>Name</b></td>
							<td width=100><b>Designation</b></td>
							<td width=90><b>Department</b></td>
							<td width=120><b>Location</b></td>
							<td width=80><b>Section</b></td>
							<td width=50><b>Sub Section</b></td>
							<td width=80><b>Attnd. Date</b></td>
							<td width=40><b>OT Hour</b></td>
							<td width=50><b>OT Rate</b></td>
							<td width=50><b>OT Total</b></td>
						</tr>";
						$new_dept[$i]=$row[department_id];
				$i++;
				}
			
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$buyer_ot_rate=round(($basic_salary/208)*2,2);
			$buyer_ot_hr=get_buyer_ot_hr($row[total_over_time_min],$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit']);
					 					
				echo "<tr>
						<td></td>						
					</tr>
					<tr class=\"gradeA\">
						<td>$row[emp_code]</td>
						<td>$row[id_card_no]</td>
						<td>$row[punch_card_no]</td>
						<td>$row[name]</td>
						<td>".$designation_chart[$row['designation_id']]."</td>
						<td>".$department_details[$row['department_id']]."</td>						
						<td>".$location_details[$row['location_id']]."</td>
						<td>".$section_details[$row['section_id']]."</td>
						<td>".$subsection_details[$row['subsection_id']]."</td>
						<td align='center'>".convert_to_mysql_date($row[attnd_date])."</td>
						<td align=right>".get_buyer_ot_hr($row[total_over_time_min], $var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] )."</td>
						<td align=right>".number_format($buyer_ot_rate,2)."</td>
						<td align=right>".$buyer_ot_rate*$buyer_ot_hr."</td>
					</tr>";
					$total_ot_hr=$total_ot_hr+$buyer_ot_hr;
					$total_ot_amnt=$total_ot_amnt+($buyer_ot_rate*$buyer_ot_hr);
					if($row["status"]=='P'){$p++;}
					if($row["status"]=='A'){
							$a++;
							if($a>1){
									$param_emp_code .=',';
									}
							$param_emp_code .=$row["emp_code"];
							
						}
					if($row["status"]=='MR')
							{
								$mr++;
								if($mr>1){
									$mr_emp_code .=',';
									}
								$mr_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='D')
							{
								$d++;
								if($d>1){
									$delay_emp_code .=',';
									}
								$delay_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='CL' || $row["status"]=='SL' || $row["status"]=='ML' || $row["status"]=='EL' || $row["status"]=='LWP' || $row["status"]=='SpL')
							{
								$l++;
								if($l>1){
									$leave_emp_code .=',';
									}
								$leave_emp_code .=$row["emp_code"];
							}
											
			}
			
			 if($emp_tot==$j-1)
							{
								echo "					
									<tr>
										<td></td>						
									</tr>
									<tr class=\"gradeA\">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>						
										<td></td>
										<td></td>
										<td></td>
										<td>Total</td>
										<td align=right>$total_ot_hr</td>
										<td></td>
										<td align=right>$total_ot_amnt</td>
									</tr>";
								$total_buyer_ot_amount=$total_buyer_ot_amount+$total_ot_amnt; 
								
							}
	 
		$total=$p+$a+$l+$d+$mr;
		 echo "<tr bgcolor=#F0F0B8>
					<td colspan=13 align=\"center\"><b>Summary</b></td>
				  </tr>
				  <tr>				  	
					<td></td>
				  	<td colspan=2 align=\"left\" bgcolor=#E5E5E5><b>Present :</b> $p </td>								  	
					<td  align=\"left\" bgcolor=#FFFFFF><b>Absent :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$param_emp_code&date=$date','Absent Information'); return false\"> $a</a> </td>				  			  	
 				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Leave :</b> <a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$leave_emp_code&date=$date','Leave Information'); return false\">$l</a> </td>				  				   
					<td  align=\"left\" bgcolor=#FFFFFF><b>Movement :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$mr_emp_code&date=$date','Movement Information'); return false\"> $mr </a> </td>				  			    
					<td  align=\"left\" bgcolor=#E5E5E5><b>Late :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$delay_emp_code&date=$date','Late Information'); return false\"> $d </a></td>				  			    
					<td  colspan=2 align=\"left\" bgcolor=#FFFFFF><b>Total :</b> $total </td>	
					<td  colspan=4 align=\"right\" bgcolor=#FFFFFF><b>Total Amount:</b> $total_buyer_ot_amount </td>
				  </tr>
				 </table>";
	
	//for report temp file delete 
	foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	//$is_created = fwrite($create_new_excel,$html);
	$is_created = fwrite($create_new_excel,ob_get_contents());
	echo "$html"."####"."$name";
	exit();	
}
if($action=="ot_report")//  buyer ot report summmary
{
	//echo $txt_from_date;die;
	//-------------
	$from_date =convert_to_mysql_date($txt_from_date);
	$to_date =convert_to_mysql_date($txt_to_date);
	if($txt_to_date=="")
	{
		$to_date=$from_date;
	}
	$str_date="a.attnd_date between '$from_date' and '$to_date'";
	$str_date2="attnd_date between '$from_date' and '$to_date'";
	if($cbo_status==0) $status="";
	else if ($cbo_status==1) $status="and a.status='P '";
	else if ($cbo_status==2) $status="and a.status='A '";
	else if ($cbo_status==3) $status="and a.status='D '";
	else if ($cbo_status==4) $status="and a.status in ('CL','ML','SL','EL','SpL','LWP')";
	else if ($cbo_status==5) $status="and a.status='MR'";
		
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
	
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="and a.department_id='$department_id'";				
		}
$p=0;$a=0;$mr=0;$d=0;$l=0;$total_buyer_ot_amount=0;
	
	/*//$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	$sql_d = "SELECT distinct(department_id) FROM hrm_attendance $department_id order by department_id";
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
	
	$p=0;$a=0;$mr=0;$d=0;$l=0;$total_buyer_ot_amount=0;
	$html = "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">";
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{		
			$total_ot_hr=0; $total_ot_amnt=0;
			//echo $row_dp['id'];
			//if($flag==1){$group_by_condition="";}
			//else {$group_by_condition="'and a.".$group_by_name=$row_dp[id]."'";};
			
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp, lib_designation des WHERE emp.status_active=1 and a.designation_id=des.id and $str_date and a.emp_code=emp.emp_code $location_id $section_id ".$cbo_company_id." and a.department_id='$row_dp[department_id]' $status $subsection_id $designation_id $category order by des.level asc";
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		//	echo $sql;			
			$emp_tot=mysql_num_rows($result);
			//echo "asd  $row_dp[id] $emp_tot";die;
			if ($emp_tot>0)
				{
	$html .= "		<tr bgcolor=#F0F0B8>
						<td colspan=12><b>Department Name:</b> ".$department_details[$row_dp[department_id]]."</td>
					</tr>
					<tr>
						<td width=50><b>Emp Code</b></td>
						<td width=50><b>ID Card No</b></td>
						<td width=50><b>Punch Card No</b></td>
						<td width=140><b>Name</b></td>
						<td width=100><b>Designation</b></td>
						<td width=90><b>Department</b></td>
						<td width=120><b>Location</b></td>
						<td width=80><b>Section</b></td>
						<td width=50><b>Sub Section</b></td>
						<td width=40><b>OT Hour</b></td>
						<td width=50><b>OT Rate</b></td>
						<td width=50><b>OT Total</b></td>
					</tr>";
							
			
			while( $row = mysql_fetch_array( $result ) ) {
				
			//$location_name=return_field_value("location_name","lib_location","id=$row[location_id]");
			//$section_name=return_field_value("section_name","lib_section","id=$row[section_id]");
			//$subsection_name=return_field_value("subsection_name","lib_section","id=$row[section_id]");
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$buyer_ot_rate=round(($basic_salary/208)*2,2);
			$buyer_ot_hr=get_buyer_ot_hr($row[total_over_time_min],$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit']);
			$html .= "					
					<tr>
						<td></td>						
					</tr>
					<tr class=\"gradeA\">
						<td>$row[emp_code]</td>
						<td>$row[id_card_no]</td>
						<td>$row[punch_card_no]</td>
						<td>$row[name]</td>
						<td>".$designation_chart[$row['designation_id']]."</td>
						<td>".$department_details[$row['department_id']]."</td>						
						<td>".$location_details[$row['location_id']]."</td>
						<td>".$section_details[$row['section_id']]."</td>
						<td>".$subsection_details[$row['subsection_id']]."</td>
						<td align=right>".$buyer_ot_hr."</td>
						<td align=right>".number_format($buyer_ot_rate,2)."</td>
						<td align=right>".$buyer_ot_rate*$buyer_ot_hr."</td>
					</tr>";	
					$total_ot_hr=$total_ot_hr+$buyer_ot_hr;
					$total_ot_amnt=$total_ot_amnt+($buyer_ot_rate*$buyer_ot_hr);
					//--------------

					//--------------
					if($row["status"]=='P'){$p++;}
					if($row["status"]=='A'){
							$a++;
							if($a>1){
									$param_emp_code .=',';
									}
							$param_emp_code .=$row["emp_code"];
							
						}
					if($row["status"]=='MR')
							{
								$mr++;
								if($mr>1){
									$mr_emp_code .=',';
									}
								$mr_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='D')
							{
								$d++;
								if($d>1){
									$delay_emp_code .=',';
									}
								$delay_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='CL' || $row["status"]=='SL' || $row["status"]=='ML' || $row["status"]=='EL' || $row["status"]=='LWP' || $row["status"]=='SpL')
							{
								$l++;
								if($l>1){
									$leave_emp_code .=',';
									}
								$leave_emp_code .=$row["emp_code"];
							}					
					}
			
			$html .= "					
					<tr>
						<td></td>						
					</tr>
					<tr class=\"gradeA\">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>						
						<td></td>
						<td></td>
						<td>Total</td>
						<td align=right>$total_ot_hr</td>
						<td></td>
						<td align=right>$total_ot_amnt</td>
					</tr>";
			$total_buyer_ot_amount=$total_buyer_ot_amount+$total_ot_amnt;
		}
	}
		$total=$p+$a+$l+$d+$mr;
		
		$html .= "<tr bgcolor=#F0F0B8>
					<td colspan=12 align=\"center\"><b>Summary</b></td>
				  </tr>
				  <tr>				  	
					<td colspan=2></td>
				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Present :</b> $p </td>								  	
					<td  align=\"left\" bgcolor=#FFFFFF><b>Absent :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$param_emp_code&date=$date','Absent Information'); return false\"> $a</a> </td>				  			  	
 				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Leave :</b> <a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$leave_emp_code&date=$date','Leave Information'); return false\">$l</a> </td>				  				   
					<td  align=\"left\" bgcolor=#FFFFFF><b>Movement :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$mr_emp_code&date=$date','Movement Information'); return false\"> $mr </a> </td>				  			    
					<td  align=\"left\" bgcolor=#E5E5E5><b>Late :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$delay_emp_code&date=$date','Late Information'); return false\"> $d </a></td>				  			    
					<td  colspan=2 align=\"left\" bgcolor=#FFFFFF><b>Total :</b> $total </td>
					<td  colspan=3 align=\"right\" bgcolor=#FFFFFF><b>Total Amount:</b> $total_buyer_ot_amount </td>
				  </tr>
				 </table>";
			
	
	//for report temp file delete 
	foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();*/
			$new_dept=array();
		ob_start();
			$total_ot_hr=0; $total_ot_amnt=0;
			
			$sql="SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp WHERE emp.status_active=1 $department_id and $str_date and a.emp_code=emp.emp_code $location_id $section_id ".$cbo_company_id."  $status $subsection_id $designation_id $category  group by a.emp_code order by a.department_id asc";
			 
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		//	echo $sql;			
			$emp_tot=mysql_num_rows($result);
			
			$new_dept=array();
			$j=1;
			$i=0;
			$k=0;
			while( $row = mysql_fetch_array( $result ) ) 
			{
				 $j++;
				if (in_array($row['department_id'], $new_dept))
				{
					$i++;
				}
				else
				{
					
					if ($k!=0)
					{
						
						echo "					
							<tr>
								<td></td>						
							</tr>
							<tr class=\"gradeA\">
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>						
								<td></td>
								<td></td>
								<td>Total</td>
								<td align=right>$total_ot_hr</td>
								<td></td>
								<td align=right>$total_ot_amnt</td>
							</tr>";
								$total_buyer_ot_amount=$total_buyer_ot_amount+$total_ot_amnt; 
								$total_ot_hr=0; $total_ot_amnt=0;
					}
					
					$k++; 
					
				  echo "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">
						<tr bgcolor=#F0F0B8>
							<td colspan=15><b>Department Name:</b> ".$department_details[$row[department_id]]."</td>
						</tr>
						<tr>
							<td width=50><b>Emp Code</b></td>
							<td width=50><b>ID Card No</b></td>
							<td width=50><b>Punch Card No</b></td>
							<td width=140><b>Name</b></td>
							<td width=100><b>Designation</b></td>
							<td width=90><b>Department</b></td>
							<td width=120><b>Location</b></td>
							<td width=80><b>Section</b></td>
							<td width=50><b>Sub Section</b></td>
							<td width=40><b>OT Hour</b></td>
							<td width=50><b>OT Rate</b></td>
							<td width=50><b>OT Total</b></td>
						</tr>";
						$new_dept[$i]=$row[department_id];
				$i++;
				}
			
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$buyer_ot_rate=round(($basic_salary/208)*2,2);	
			$buyer_ot_hr=get_buyer_tot_emp_ot_hr($row[emp_code],$str_date2,$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit']);
							
				echo "<tr>
						<td></td>						
					</tr>
					<tr class=\"gradeA\">
						<td>$row[emp_code]</td>
						<td>$row[id_card_no]</td>
						<td>$row[punch_card_no]</td>
						<td>$row[name]</td>
						<td>".$designation_chart[$row['designation_id']]."</td>
						<td>".$department_details[$row['department_id']]."</td>						
						<td>".$location_details[$row['location_id']]."</td>
						<td>".$section_details[$row['section_id']]."</td>
						<td>".$subsection_details[$row['subsection_id']]."</td>
						<td align=right>".get_buyer_tot_emp_ot_hr($row[emp_code],$str_date2,$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit'])."</td>
						<td align=right>".number_format($buyer_ot_rate,2)."</td>
						<td align=right>".$buyer_ot_rate*$buyer_ot_hr."</td>
					</tr>";
					$total_ot_hr=$total_ot_hr+$buyer_ot_hr;
					$total_ot_amnt=$total_ot_amnt+($buyer_ot_rate*$buyer_ot_hr);
					if($row["status"]=='P'){$p++;}
					if($row["status"]=='A'){
							$a++;
							if($a>1){
									$param_emp_code .=',';
									}
							$param_emp_code .=$row["emp_code"];
							
						}
					if($row["status"]=='MR')
							{
								$mr++;
								if($mr>1){
									$mr_emp_code .=',';
									}
								$mr_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='D')
							{
								$d++;
								if($d>1){
									$delay_emp_code .=',';
									}
								$delay_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='CL' || $row["status"]=='SL' || $row["status"]=='ML' || $row["status"]=='EL' || $row["status"]=='LWP' || $row["status"]=='SpL')
							{
								$l++;
								if($l>1){
									$leave_emp_code .=',';
									}
								$leave_emp_code .=$row["emp_code"];
							}
											
			}
			
			 if($emp_tot==$j-1)
							{
								echo "					
									<tr>
										<td></td>						
									</tr>
									<tr class=\"gradeA\">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>						
										<td></td>
										<td></td>
										<td>Total</td>
										<td align=right>$total_ot_hr</td>
										<td></td>
										<td align=right>$total_ot_amnt</td>
									</tr>";
								$total_buyer_ot_amount=$total_buyer_ot_amount+$total_ot_amnt; 
								
							}
	 
		$total=$p+$a+$l+$d+$mr;
		 echo "<tr bgcolor=#F0F0B8>
					<td colspan=13 align=\"center\"><b>Summary</b></td>
				  </tr>
				  <tr>				  	
					<td></td>
				  	<td colspan=2 align=\"left\" bgcolor=#E5E5E5><b>Present :</b> $p </td>								  	
					<td  align=\"left\" bgcolor=#FFFFFF><b>Absent :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$param_emp_code&date=$from_date*$to_date&type=2','Absent Information'); return false\"> $a</a> </td>				  			  	
 				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Leave :</b> <a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$leave_emp_code&date=$from_date*$to_date&type=2','Leave Information'); return false\">$l</a> </td>				  				   
					<td  align=\"left\" bgcolor=#FFFFFF><b>Movement :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$mr_emp_code&date=$from_date*$to_date&type=2','Movement Information'); return false\"> $mr </a> </td>				  			    
					<td  align=\"left\" bgcolor=#E5E5E5><b>Late :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$delay_emp_code&date=$from_date&type=*$to_date&type=2','Late Information'); return false\"> $d </a></td>				  			    
					<td  colspan=2 align=\"left\" bgcolor=#FFFFFF><b>Total :</b> $total </td>	
					<td  colspan=4 align=\"right\" bgcolor=#FFFFFF><b>Total Amount:</b> $total_buyer_ot_amount </td>
				  </tr>
				 </table>";
	
	//for report temp file delete 
	foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	//$is_created = fwrite($create_new_excel,$html);
	$is_created = fwrite($create_new_excel,ob_get_contents());
	echo "$html"."####"."$name";
	exit();
}
if( $action == "monthly_attn_summery_audit" ) {	// Audit Summary
			
	$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);	

	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
	
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="where id='$department_id'";			
		}
	
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
	
	$html .="<div style=\"height:350px;font-size:8px; width:2450px;\">";
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{	
	 $sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name FROM hrm_employee WHERE status_active=1 and department_id=$row_dp[id] $location_id $cbo_company_id $section_id $subsection_id $designation_id $category order by designation_id asc";
 	//echo $sqls;die; 
	$i=0;
	//$sqls = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.* from hrm_employee emp  order by emp.emp_code asc";
	$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
	
	if (mysql_num_rows($results)>0)
	{
		$html .= "<table cellpadding=\"0\" style=\"font-size:8px;\" width=\"2450\" cellspacing=\"0\" border=\"1\">
					<tr bgcolor=#F0F0B8>
						<td colspan=44>Department Name: $row_dp[department_name]</td>
					</tr>
					<tr>
						<td width='65'><b>Emp Code</b></td>
						<td width='180'><b>Emp Name</b></td>
						<td width='120'><b>Designation</b></td>
						<td width='60'><b>Join Date</b></td>
						<td width='30'><b>Days</b></td>";
						for($j=1; $j<=$get_days;$j++)
						{
				$html .= "<td width='50'><b>".$j."</b></td>";
						}
				$html .= "<td width='50'><b>Present</b></td><td width='50'><b>Absent</b></td><td width='50'><b>Late</b></td><td width='50'><b>Leave</b></td><td width='50'><b>Movement</b></td><td width='50'><b>W. Day</b></td><td width='50'><b>H. Day</b></td><td width='50'><b>Pay Days</b></td>";
				
				$html .="</tr>";
				
						/*</table> <div style=\"height:350px;font-size:8px; width:2450px;\"><table width=\"2450\" cellpadding=\"0\" cellspacing=\"0\" border=\"1\">*/				 		
						
				while( $rows = mysql_fetch_assoc( $results ) ) 
				{
					$html .= "<tr>
						<td width='65'>".$rows[emp_code]."</td>
						<td width='180'>".$rows[name]."</td>
						<td width='120'>".$designation_chart[$rows['designation_id']]."</td>
						<td width='60'>".$rows[joining_date]."</td>
						<td width='30'>".$get_days."</td>";
					
					$present=0;
					$absent=0;
					$leave=0;
					$wday=0;
					$pay_day=0;
					$sql = "SELECT * FROM hrm_attendance  WHERE  attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]' order by attnd_date  asc";
					$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
					while( $row = mysql_fetch_assoc( $result ) ) 
					{
						$html .= "<td width='50' align=\"center\"><b>".$row[status]."</b></td>";
					}
					$d="count(*)";
					$dd="hrm_attendance";
					$ddd="status in ('P') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$present= return_field_value($d,$dd,$ddd);
					
					$ddd="status='A ' and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$absent= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('CL','ML','SL','EL','SpL') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$leave= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('D') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$late= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('MR') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$movement= return_field_value($d,$dd,$ddd);
					
					$ddd="status='W' and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$wday= return_field_value($d,$dd,$ddd);
					
					$ddd="status in ('H','GH','CH','FH') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$hday= return_field_value($d,$dd,$ddd);
					
					$ddd="status not in ('A','LWP') and attnd_date like '$date_part%'  and  emp_code='$rows[emp_code]'";
					$pay_day=return_field_value($d,$dd,$ddd);
					
					$html .= "<td width='50'>".$present."</td><td width='50'>".$absent."</td><td width='50'>".$late."</td><td width='50'>".$leave."</td><td width='50'>".$movement."</td><td width='50'>".$wday."</td><td width='50'>".$hday."</td><td width='50'>".$pay_day."</td>";
				}
				
				$html .="</tr>";
						
		}
	}
	$html .="</table> </div>";
				 		
						
	
	//previous file delete code-----------------------------//
	foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}


if( $action == "emp_job_card_report" ) {	//Job Card Report
	
	
	if($txt_date_from!="" && $txt_date_to!="")
	{
		$txt_date_from=convert_to_mysql_date($txt_date_from);
		$txt_date_to=convert_to_mysql_date($txt_date_to);
		$search_date_part="attnd_date between '$txt_date_from' and '$txt_date_to'";
	}
	else
	{
		//$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
		//$search_date_part="attnd_date like '$date_part%'";
		
		$date_range=explode("_",$cbo_month_selector);
		$txt_date_from=$date_range[0];
		$txt_date_to=$date_range[1];
		$search_date_part="attnd_date between '$txt_date_from' and '$txt_date_to'";
	}
	//$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	$get_days=datediff("d",$txt_date_from,$txt_date_to);
	//echo $get_days;die;
	/*
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		 
		$in_out_time_format=$res['in_out_time_format']; 
	}
	*/
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		$ot_fraction = $res['allow_ot_fraction'];
		$ot_start_min = $res['ot_start_minute'];
		$one_hr_ot_unit = $res['one_hour_ot_unit'];
		$adjust_out_time=$res['adjust_out_time'];
		$applicable_above_salary=$res['applicable_above_salary'];
		$in_out_time_format=$res['in_out_time_format']; 
		$adjust_in_time = $res['adjust_in_time'];
	}
	
	
	//shift policy array
	$qr_shift="select * from lib_policy_shift where status_active=1 and is_deleted=0 order by id";
	$result_s = mysql_query( $qr_shift ) or die( $qr_shift . "<br />" . mysql_error() );
	$shift_arr = array();
	while($res_shift=mysql_fetch_array($result_s))		
		{
			$shift_arr[$res_shift["id"]]=mysql_real_escape_string($res_shift["shift_name"]);
		}	
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id in($division_id)";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id in($department_id)";	
	if ($section_id==0) $section_id=""; else $section_id="and section_id in($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in($subsection_id)";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ($id_card_no)";
	if ($emp_code==0) $emp_code=""; else $emp_code="and emp_code in ($emp_code)";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in ($designation_id)";
 	
	 	$sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM hrm_employee 
				WHERE emp_code!=''
 				$cbo_company_id 
				$location_id
				$division_id 
				$department_id 
				$section_id 
				$subsection_id 
				$category
				$id_card_no 
				$emp_code
				$designation_id
				order by location_id,division_id,department_id,section_id,subsection_id,category,CAST(id_card_no as SIGNED)";
				//echo $sqls;die;
				
		$i=0;
		$results = mysql_query( $sqls ) or die( $sql . "<br />" . mysql_error() );	
				
		while( $rows = mysql_fetch_assoc( $results ) ) 
		{
			//this is used inside of while loop
			$sql = "SELECT * FROM hrm_attendance  WHERE  $search_date_part and  emp_code='$rows[emp_code]' order by attnd_date  asc";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$numrow = mysql_num_rows($result);
			if( $numrow > 0 ){
	?>							
			<style media="print">
			p{page-break-before:always}
			</style>
                <table width="780" border="1" cellpadding="0" cellspacing="0" style="border:1px solid black;font-size:10px" align="center" rules="all" class="rpt_table">
                <thead>	
                    <tr bgcolor="#F0F0B8" height="30">
						<th colspan=10  style="font-size:11px"><b><? echo $company_details[$rows['company_id']]; ?> <br /> Department :  <? echo $department_details[$rows['department_id']]; ?></b></th>
					</tr>
					<tr bgcolor=#FBFAFC height="30">
						<td colspan=10><b>ID Card:</b> <? echo $rows[id_card_no];?>&nbsp;&nbsp;&nbsp;<b>Emp Code:</b> <? echo $rows[emp_code];?>&nbsp;&nbsp;&nbsp;<b>Emp Name:</b> <? echo $rows[name];?>&nbsp;&nbsp;&nbsp;<b>Designation:</b> <? echo $designation_chart[$rows['designation_id']];?>&nbsp;&nbsp;&nbsp;<b>Joining Date:</b> <? echo $rows[joining_date];?></td>
					</tr>
					<tr>
						<th colspan=10></th>
					</tr>
					<tr height="35">
						<th width='60'><b>Date</b></th>
                        <th width='60'><b>Day</b></th>
						<th width=''><b>Shift</b></th>
						<th width='50'><b>Shift in </b></th>
						<th width='50'><b>Shift Out </b></th>
						<th width='70'><b>Actual In</b></th>
						<th width='70'><b>Actual Out</b></th>
						<th width='50'><b>Late Min</b></th>
						<th width='50'><b>OT Hrs</b></th>
						<th width='50'><b>Status</b></th>
					</tr>					
				</thead>
            
			<?
					$holiday=0;$weekend=0;$working_day=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_days=0;$ot=0;$overt=0;
					while( $row = mysql_fetch_assoc( $result ) ) 					
					{		
					
					//if($row['r_sign_in_time']!='00:00:00'){$r_in = DATE("g:i a", STRTOTIME($row[r_sign_in_time]));}else $r_in="00:00";
					//if($row['r_sign_out_time']!='00:00:00'){$r_out = DATE("g:i a", STRTOTIME($row[r_sign_out_time]));}else $r_out="00:00";
					$r_in=$row['r_sign_in_time'];					
					$r_out=$row['r_sign_out_time'];
					
					//$buyer_ot_hours_t="";
					
					if(trim($row[status])=='GH' || trim($row[status])=='FH' || trim($row[status])=='CH' || trim($row[status])=='H' || trim($row[status])=='W'){
						$s_in="00:00:00";
						$s_out="00:00:00";	
						$buyer_ot_hours_t="0";
						$buyer_late_min='0';
					}
					else if($row[is_regular_day]==0)
					{
						$s_in="00:00:00";
						$s_out="00:00:00";	
						$buyer_ot_hours_t="0";
						$buyer_late_min='0';
						
 						$day_status_id = return_field_value("type","lib_holiday","from_date='$row[attnd_date]'");
						$day_status = $holiday_category[$day_status_id];						
						if(trim($day_status_id)==''){
							$day_status=return_field_value("weekend","hrm_weekend","emp_code=$row[emp_code]");	
							if(trim($day_status)!='')$day_status='W';					
						}
						$row[status]=$day_status;
					}
					else{
						
						//buyer ot hour
						if($rows['duty_roster_policy']!=0){ $var_hrm_chart[$row['company_id']]['first_ot_limit']=0;$row['total_over_time_min']=0;}
						$buyer_ot_hours_t = get_buyer_ot_hr($row[total_over_time_min], $var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] );
						$buyer_late_min=$row[late_time_min]; 					
						if($row[over_time_slot_1_min]!=''){$ot += $buyer_ot_hours_t;}
						
						//get_buyer_ot_hr($row[total_over_time_min], $var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] );
						 		
						//buyer in out time
						$s_in=get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $var_hrm_chart[$row['company_id']]['adjust_in_time'] );
						//if($buyer_in_time!='00:00:00'){$s_in = DATE("g:i a", STRTOTIME($buyer_in_time));}else $s_in="00:00";
						$s_out= get_buyer_out_time($row['sign_out_time'], $row['r_sign_out_time'], $adjust_out_time, 120, $buyer_ot_hours_t, $ot_start_min,$one_hr_ot_unit,$rows['ot_entitled'],$row[r_sign_in_time]	); 
						
						//echo $buyer_ot_hours_t;
						
						}
						
						
						
						 if($row[status]=="CH") $row[status]="H";
						 if ($row[status]=='W' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH'){ $bgcolor="#CCCCCC";} else { $bgcolor="#FFFFFF";}
					
			?>		
					
                    <tr height="27" bgcolor="<? echo $bgcolor; ?>">
						<td width=''><? echo $row[attnd_date];?></td>
                        <td width='' align="center"><? echo date("D",strtotime($row[attnd_date]));?></td>
						<td width=''><? echo $shift_arr[$row[policy_shift_id]];?></td>						
						<td width='' align="center"><? echo show_time_format( $r_in, $in_out_time_format );?></td>
						<td width='' align="center"><? echo show_time_format( $r_out, $in_out_time_format );?></td>
						<td width='' align="center"><? echo show_time_format( $s_in, $in_out_time_format );?></td>						
						<td width='' align="center"><? echo show_time_format( $s_out, $in_out_time_format );?></td> 	
						<td width='' align="center"><? echo $buyer_late_min;?></td>
						<td width='' align="center"><? echo $buyer_ot_hours_t;?></td>
						<td width='' align="center"><? echo $row[status];?></td>
                    </tr>
                        					
			<?					
						
						if($row[status]=='CL'){$cl++;}
						if($row[status]=='SL'){$sl++;}
						if($row[status]=='ML'){$ml++;}
						if($row[status]=='EL'){$el++;}
						if($row[status]=='LWP'){$lwp++;}
						if($row[status]=='SpL'){$spl++;}
						if($row[status]=='H' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH'){$holiday++;}
						if($row[status]=='W'){$weekend++;}
						if($row[status]=='P'){$present++;}
						if($row[status]=='D'){$late++;}
						if($row[status]=='MR'){$movement++;}
						if($row[status]=='A'){$absent++;}
						$total_days++;
						if($row[total_over_time_min]!=''){$ot += $buyer_ot_hours_t;}
						$overt += $buyer_ot_hours_t;
					}
					$working_day=$total_days-$holiday-$weekend;					 
					//$total_elapsedHoursMins = sprintf("%d hours %02d minutes", abs((int)($ot/60)), abs((int)($ot%60)));	
					
				?>
						<tr><td colspan=10 height="25" align="center"><b>Summary</b></td></tr>
                        <tr bgcolor="#F0F0B8" height="30">
							<td colspan=10 height="25">
								<b>Total Day:</b><? echo $total_days;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Weekend Day:</b><? echo $weekend;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Holiday:</b><? echo $holiday;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Working Day:</b><? echo $working_day;?> &nbsp;&nbsp;&nbsp;&nbsp;
								<b>Present:</b><? echo $present;?> &nbsp;&nbsp;&nbsp;&nbsp;
								<b>Late Present:</b><? echo $late;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Movement:</b><? echo $movement;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Total OT:</b><? echo $overt;?>&nbsp;&nbsp;&nbsp;&nbsp;
                                <b>Payable Days:</b><? echo $total_days-$absent;?>&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
												
						</tr>
						<tr bgcolor="#F0F0B8"  height="30">
							<td colspan=10>
								<b>Absent:</b><? echo $absent;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Casual Leave:</b><? echo $cl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Sick Leave:</b><? echo $sl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Earn Leave:</b><? echo $el;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Special Leave:</b><? echo $spl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Leave Without Pay:</b><? echo $lwp;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Maternity Leave:</b><? echo $ml;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>	
						</tr>
					</table><br /><br /><br />
                    <p></p>
		<?   
			}//end while condition
		
		}// end if condition
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}




if( $action == "daily_attn_summary_report" ) {	//Daily Attendance Summary Report	
	
	if ($company_id==0) $company_id=""; else $company_id="and a.company_id='$company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and a.division_id='$division_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and e.category='$cbo_emp_category'";
	
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="where id='$department_id'";			
		}
	
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
	$date_part=convert_to_mysql_date($txt_date);
	$html .="<div style=\"font-size:12px; width:100%;\">";
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{	
	$sql = "SELECT DISTINCT(a.designation_id) as designation_id from hrm_employee as e LEFT JOIN hrm_attendance as a ON e.emp_code=a.emp_code where a.attnd_date='$date_part' and a.department_id=$row_dp[id] $company_id $location_id $division_id $section_id $subsection_id $designation_id $category order by a.designation_id asc";
 	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );											
	$numrows=mysql_num_rows($result);
	
	$i=0;	
	$html .= "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">";
	if($numrows>0){
				
				$html .= "<tr bgcolor=#F0F0B8>
							<td colspan=9><b>Department Name: $row_dp[department_name]</b></td>
						</tr>					
						<tr>
							<td width='150'><b>Designation</b></td>
							<td width='100'><b>Newly Joined</b></td>
							<td width='100'><b>Total Manpower</b></td>
							<td width='100'><b>Movement Register</b></td>
							<td width='85'><b>Present</b></td>
							<td width='85'><b>Late</b></td>
							<td width='85'><b>Absent</b></td>
							<td width='85'><b>Leave</b></td>
							<td width='100'><b>Remarks</b></td>
						</tr>
						<tr>
							<td colspan=9></td>
						</tr>";	
				$total_new_join=0;$grand_total=0;$total_new_join=0;$total_manpower_dep=0;$total_movement=0;$total_present=0;$total_late=0;$total_absent=0;$total_total_leave=0;$total_new_join=0;		
				while( $row = mysql_fetch_assoc( $result ) ) 
					{	
						$holiday=0;$weekend=0;$total_leave=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_manpower=0;$new_join=0;
						$sqls = "SELECT * from hrm_attendance where designation_id=$row[designation_id] and department_id=$row_dp[id] and attnd_date='$date_part'";
						//echo $sqls;
						$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
						while( $rows = mysql_fetch_assoc( $results ) ) 
						{			
							
							if($rows[status]=='CL'){$cl++;}
							if($rows[status]=='SL'){$sl++;}
							if($rows[status]=='ML'){$ml++;}
							if($rows[status]=='EL'){$el++;}
							if($rows[status]=='LWP'){$lwp++;}
							if($rows[status]=='SpL'){$spl++;}
							if($rows[status]=='H' || $rows[status]=='GH' || $rows[status]=='FH' || $rows[status]=='CH' ){$holiday++;}
							if($rows[status]=='W'){$weekend++;}
							if($rows[status]=='P'){$present++;}							
							if($rows[status]=='D'){$late++;}
							if($rows[status]=='MR'){$movement++;}
							if($rows[status]=='A'){$absent++;}
							$total_manpower++;
							
							$joining_date=return_field_value("joining_date","hrm_employee","emp_code='$rows[emp_code]'");			
							if($joining_date==$txt_date){$new_join++;}					
						}
						
						$total_leave=$cl+$sl+$ml+$el+$lwp+$spl;
						$designation=return_field_value("custom_designation","lib_designation","id='$row[designation_id]'");
						
					$html .= "<tr>
							<td width=''>".$designation."</td>
							<td width='' align='right'>".$new_join."</td>						
							<td width='' align='right'>".$total_manpower."</td>
							<td width='' align='right'>".$movement."</td>
							<td width='' align='right'>".$present."</td>						
							<td width='' align='right'>".$late."</td> 	
							<td width='' align='right'>".$absent."</td>
							<td width='' align='right'>".$total_leave."</td>	
							<td width=''></td></tr>";
					
					//department wise total sum and percentage---//
					$total_new_join += $new_join;					
					$total_manpower_dep += $total_manpower;
					$total_movement += $movement;
					$total_present += $present;
					$total_late += $late;
					$total_absent += $absent;
					$total_total_leave += $total_leave;				
					$grand_total=$total_new_join+$total_manpower_dep+$total_movement+$total_present+$total_late+$total_absent+$total_total_leave+$total_new_join;
			  }
			  		$html .= "<tr bgcolor=''>
							<td width=''><b>Total</b></td>
							<td width='' align='right'>".$total_new_join."</td>						
							<td width='' align='right'>".$total_manpower_dep."</td>
							<td width='' align='right'>".$total_movement."</td>
							<td width='' align='right'>".$total_present."</td>						
							<td width='' align='right'>".$total_late."</td> 	
							<td width='' align='right'>".$total_absent."</td>
							<td width='' align='right'>".$total_total_leave."</td>	
							<td width=''></td></tr>
							<tr>
							<td width=''><b>(In %)</b></td>
							<td width=''></td>						
							<td width=''></td>
							<td width='' align='right'>".round(($total_movement*100)/$total_manpower_dep,2)."%</td>
							<td width='' align='right'>".round(($total_present*100)/$total_manpower_dep,2)."%</td>						
							<td width='' align='right'>".round(($total_late*100)/$total_manpower_dep,2)."%</td> 	
							<td width='' align='right'>".round(($total_absent*100)/$total_manpower_dep,2)."%</td>
							<td width='' align='right'>".round(($total_total_leave*100)/$total_manpower_dep,2)."%</td>	
							<td width=''></td></tr>";
					
		}
				
				
	}
	$html .="</table> </div>";
	
	//previous file delete code-----------------------------//
		foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
                @unlink($filename);
		}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}

//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$start_1 = substr($row["starting_date"],0,3);
		$start_2 = substr($row["starting_date"],-1,1);

		$end_1 = substr($row["ending_date"],0,3);
		$end_2 = substr($row["ending_date"],-2,2);
		
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$start_1." ".$start_2." - ".$end_1." ".$end_2."</option>";
		//echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}

function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}

?>

