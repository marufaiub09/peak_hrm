//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

function fnc_id_card() {		//ID Card Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, employee_counter = 0, option=0, data = '&employees=';
	
	$('#employees tbody input[type="checkbox"]:checked').each(function() {
		data += $(this).val() + '_';
		employee_counter++
	});
	
	if( employee_counter == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$(this).html('Please select at lease one employee for generating ID Card.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		data = data.substr( 0, data.length - 1 );
		if(confirm('ID Card Format?OK=H and Cancel=V'))
			{
				option="H";
			}
		else
			{
				option="V";
			}
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=id_card' + data + '&option=' + option + '&nocache=' + nocache );
		http.onreadystatechange = response_id_card;
		http.send(null);
	}
}

function response_id_card() {	//ID Card Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('###');
		window.open('includes/tmp_report_file/'+response[1], 'ID Card', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=500, height=500');
		reset_form();
	}
}

function fnc_punch_report() {		//Punch Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#date').focus();
			$(this).html('Please provide date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please select company to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&date=' + $('#date').val() + '&company_id=' + $('#company_id').val() + '&location_id=' + $('#location_id').val()
				+ '&division_id=' + $('#division_id').val() + '&department_id=' + $('#department_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=punch_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_punch_report;
		http.send(null);
	}
}

function response_punch_report() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('###');
		//showResult(escape(response[0]),1,'data_panel2');
		$('#data_panel').html( '<a href="' + response[1] + '">PDF Report</a>' + response[0] );
		// $('#data_panel2').html( response[0] );
		arrange_table();
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
function fnc_daily_attnd_report() {		//Punch Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#date').focus();
			$(this).html('Please provide date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please select company to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&date=' + $('#date').val() + '&company_id=' + $('#company_id').val() + '&location_id=' + $('#location_id').val()
				+ '&division_id=' + $('#division_id').val() + '&department_id=' + $('#department_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=daily_attn_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_daily_attnd_report;
		http.send(null);
	}
}

function response_daily_attnd_report() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText;
		//alert(response);
		//showResult(escape(response[0]),1,'data_panel2');
		//$('#data_panel2').html( '<a href="' + response[1] + '">PDF Report</a>' + response[0] );
		 $('#data_panel2').html( '' );
		 $('#data_panel2').html( response );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}


function fnc_daily_total_punch_report() {		//Details Punch Report
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#date').focus();
			$(this).html('Please provide date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#txt_emp_code').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_emp_code').focus();
			$(this).html('Please Enter Employee COde.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&date=' + $('#date').val() + '&txt_emp_code=' + $('#txt_emp_code').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=total_punch_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_total_punch_report;
		http.send(null);
	}
}

function response_total_punch_report() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText;
		//alert(response);
		//showResult(escape(response[0]),1,'data_panel2');
		//$('#data_panel2').html( '<a href="' + response[1] + '">PDF Report</a>' + response[0] );
		 $('#data_panel2').html( '' );
		 $('#data_panel2').html( response );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}

function fnc_monthly_attendance_summary()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&txt_date=' + $('#txt_date').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val()+ '&department_id=' + $('#department_id').val()+ '&location_id=' + $('#location_id').val() + '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=monthly_attn_summery' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_monthly_att_summary;
		http.send(null);
	}
}


function response_monthly_att_summary() {
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		//var response = http.responseText;
		//alert(response);		
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>' );
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}

function fnc_daily_attendance_report()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	/*if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}*/
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&cbo_status=' + $('#cbo_status').val() +'&txt_date=' + $('#txt_date').val()+ '&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() + '&department_id=' + $('#department_id').val() + '&section_id=' + $('#section_id').val()+ '&subsection_id=' + $('#subsection_id').val()+ '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=daily_in_out_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_daily_att_report;
		http.send(null);
	}
}


function response_daily_att_report() {	//Punch Report Response
	if(http.readyState == 4) {
		//var response = http.responseText;
		var response =http.responseText.split('####');
		 //alert(response[1]);		
		 $('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>' );
		 $('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Punch Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}

function fnc_monthly_attendance_audit_summary()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#cbo_month_selector').val() == 0 && $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_month_selector').focus();
			$(this).html('Please select Month to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&cbo_month_selector=' + $('#cbo_month_selector').val() + '&cbo_year_selector=' + $('#cbo_year_selector').val()+'&txt_date=' + $('#txt_date').val() +'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() + '&department_id=' + $('#department_id').val()+ '&location_id=' + $('#location_id').val() + '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=monthly_attn_summery_audit' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_monthly_attendance_audit_summary;
		http.send(null);
	}
}


function response_fnc_monthly_attendance_audit_summary() {	//Punch Report Response
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[0]);		
		 $('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>' );
		 $('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}



function fnc_daily_attn_summary_report(){
	
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_date').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&txt_date=' + $('#txt_date').val() + '&cbo_emp_category=' + $('#cbo_emp_category').val()+'&company_id=' + $('#company_id').val() + '&location_id=' + $('#location_id').val() + '&division_id=' + $('#division_id').val() + '&department_id=' + $('#department_id').val()+ '&section_id=' + $('#section_id').val() + '&subsection_id=' + $('#subsection_id').val() + '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=daily_attn_summary_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_daily_attn_summary_report;
		http.send(null);
	}
	
}

function response_fnc_daily_attn_summary_report() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[1]);		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>' );//"<div align="center" style="color:#920505"><h2>Convert To &nbsp<a href="includes/tmp_report_file/"'+response[1]+'"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a></h2></div>
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully. To view in Excel format click the "ICON" below.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
function fnc_daily_ot_report()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	
	if( $('#txt_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_date').focus();
			$(this).html('Please select Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}

	
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&cbo_status=' + $('#cbo_status').val() +'&txt_date=' + $('#txt_date').val()+ '&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() + '&department_id=' + $('#department_id').val() + '&section_id=' + $('#section_id').val()+ '&subsection_id=' + $('#subsection_id').val()+ '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=daily_ot_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_daily_ot_report;
		http.send(null);
	}
}
function response_daily_ot_report() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[1]);		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>' );//"<div align="center" style="color:#920505"><h2>Convert To &nbsp<a href="includes/tmp_report_file/"'+response[1]+'"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a></h2></div>
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully. To view in Excel format click the "ICON" below.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
function fnc_ot_report()
{
	$('#messagebox').removeClass().addClass('messagebox').text('Generating....').fadeIn(1000);
	var error = false, data = '';
	if( $('#txt_from_date').val() == "" ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_from_date').focus();
			$(this).html('Please select From Date to search.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
		//$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
		data = '&cbo_status=' + $('#cbo_status').val() +'&txt_from_date=' + $('#txt_from_date').val()+ '&txt_to_date=' + $('#txt_to_date').val()+'&cbo_emp_category=' + $('#cbo_emp_category').val() + '&cbo_company_id=' + $('#cbo_company_id').val() + '&location_id=' + $('#location_id').val() + '&department_id=' + $('#department_id').val() + '&section_id=' + $('#section_id').val()+ '&subsection_id=' + $('#subsection_id').val()+ '&designation_id=' + $('#designation_id').val();
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hrm.php?action=ot_report' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_ot_report;
		http.send(null);
	}
}
function response_ot_report() {	
	if(http.readyState == 4) {
		var response = http.responseText.split('####');
		 //alert(response[1]);		 	
		$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a>' );//"<div align="center" style="color:#920505"><h2>Convert To &nbsp<a href="includes/tmp_report_file/"'+response[1]+'"><img src="includes/logo/excel-logo.png" name="logo_id" id="logo_id" /></a></h2></div>
		$('#data_panel2').html( response[0] );
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$(this).html( 'Report generated successfully. To view in Excel format click the "ICON" below.' ).addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
}
function new_window()
{
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write(document.getElementById('data_panel2').innerHTML);
	d.close();
}