<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../../../includes/common.php');
include('../../../../includes/array_function.php');
include('../../../../includes/common_functions.php');

extract( $_REQUEST );

	
 	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
	}

	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
	}

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
	}

	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
	}
	
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = mysql_real_escape_string($row['subsection_name']);
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}
	

//Variable Settings
$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$var_hrm_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$var_hrm_chart[$row['company_name']] = array();
	$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
	$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
	$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
	$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
	$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
	$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
} 


if( $action == "emp_ofday_job_card_byer_report" ) {	//Job Card Report
	
	
	if($txt_date_from!="" & $txt_date_to!="")
	{
		$txt_date_from=convert_to_mysql_date($txt_date_from);
		$txt_date_to=convert_to_mysql_date($txt_date_to);
		$search_date_part="attnd_date between '$txt_date_from' and '$txt_date_to'";
	}
	else
	{
		$date_part = $cbo_year_selector . "-" . $cbo_month_selector;
		$search_date_part="attnd_date like '$date_part%'";
	}
	$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
	
	/*$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		 
		$in_out_time_format=$res['in_out_time_format']; 
	}*/
	$qr="select * from variable_settings_hrm where status_active=1 and is_deleted=0 and company_name='$cbo_company_id' order by id";
	$result_qr = mysql_query( $qr ) or die( $qr . "<br />" . mysql_error() );
	while($res=mysql_fetch_array($result_qr))		
	{
		$ot_fraction = $res['allow_ot_fraction'];
		$ot_start_min = $res['ot_start_minute'];
		$one_hr_ot_unit = $res['one_hour_ot_unit'];
		$adjust_out_time=$res['adjust_out_time'];
		$applicable_above_salary=$res['applicable_above_salary'];
		$in_out_time_format=$res['in_out_time_format']; 
		$adjust_in_time = $res['adjust_in_time'];
	}
	
	
	//shift policy array
	$qr_shift="select * from lib_policy_shift where status_active=1 and is_deleted=0 order by id";
	$result_s = mysql_query( $qr_shift ) or die( $qr_shift . "<br />" . mysql_error() );
	$shift_arr = array();
	while($res_shift=mysql_fetch_array($result_s))		
		{
			$shift_arr[$res_shift["id"]]=mysql_real_escape_string($res_shift["shift_name"]);
		}	
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and location_id='$location_id'";
	if ($division_id==0) $division_id=""; else $division_id="and division_id in($division_id)";
	if ($department_id==0) $department_id=""; else  $department_id="and department_id in($department_id)";	
	if ($section_id==0) $section_id=""; else $section_id="and section_id in($section_id)";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and subsection_id in($subsection_id)";	
	if ($cbo_emp_category=='') $category =""; else $category ="and category='$cbo_emp_category'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and designation_id in($designation_id)";
	
	//if ($emp_code==0) $emp_code=""; else $emp_code="and emp_code in ($emp_code)";
	//if ($id_card=='') $id_card_no =""; else $id_card_no ="and id_card_no in($id_card_no)";
 	if ($emp_code=="") $emp_code=""; else $emp_code="and emp_code in ('".implode("','",explode(",",$emp_code))."')";
	if ($id_card=="") $id_card_no=""; else $id_card_no="and id_card_no in ('".implode("','",explode(",",$id_card))."')";
	
	
	 	$sqls = "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name 
				FROM hrm_employee 
				WHERE emp_code!=''
 				$cbo_company_id 
				$location_id
				$division_id 
				$department_id 
				$section_id 
				$subsection_id 
				$category 
				$emp_code
				$id_card_no
				$designation_id
				order by location_id,division_id,department_id,section_id,subsection_id,category,CAST(id_card_no as SIGNED)";
				
		$i=0;
		$results = mysql_query( $sqls ) or die( $sql . "<br />" . mysql_error() );	
				
		while( $rows = mysql_fetch_assoc( $results ) ) 
		{
				
			//this is used inside of while loop
			$sql = "SELECT * FROM hrm_attendance  WHERE  $search_date_part and  emp_code='$rows[emp_code]' order by attnd_date  asc";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$numrow = mysql_num_rows($result);
			if( $numrow > 0 ){
	?>							
			<style media="print">
			p{page-break-before:always}
			</style>
                <table width="700" border="1" cellpadding="0" cellspacing="0" style="border:1px solid black;font-size:10px" align="center" rules="all" class="rpt_table">
                <thead>	
                    <tr bgcolor="#F0F0B8" height="30">
						<th colspan=9  style="font-size:11px"><b><? echo $company_details[$rows['company_id']]; ?> <br /> Department :  <? echo $department_details[$rows['department_id']]; ?></b></th>
					</tr>
					<tr bgcolor=#FBFAFC height="30">
						<td colspan=9><b>ID Card:</b> <? echo $rows[id_card_no];?>&nbsp;&nbsp;&nbsp;<b>Emp Code:</b> <? echo $rows[emp_code];?>&nbsp;&nbsp;&nbsp;<b>Emp Name:</b> <? echo $rows[name];?>&nbsp;&nbsp;&nbsp;<b>Designation:</b> <? echo $designation_chart[$rows['designation_id']];?>&nbsp;&nbsp;&nbsp;<b>Joining Date:</b> <? echo $rows[joining_date];?></td>
					</tr>
					<tr>
						<th colspan=9></th>
					</tr>
					<tr height="35">
						<th width='60'><b>Date</b></th>
						<th width=''><b>Shift</b></th>
						<th width='50'><b>Shift in </b></th>
						<th width='50'><b>Shift Out </b></th>
						<th width='70'><b>Actual In</b></th>
						<th width='70'><b>Actual Out</b></th>
						<th width='50'><b>Late Min</b></th>
						<th width='50'><b>OT Hrs</b></th>
						<th width='50'><b>Status</b></th>
					</tr>					
				</thead>
            
			<?
					$holiday=0;$weekend=0;$working_day=0;$present=0;$late=0;$movement=0;$absent=0;$cl=0;$sl=0;$ml=0;$el=0;$lwp=0;$spl=0;$total_days=0;$ot=0;$overt=0;
					while( $row = mysql_fetch_assoc( $result ) ) 					
					{		
					$r_in=$row['r_sign_in_time'];					
					$r_out=$row['r_sign_out_time'];
					
					if(trim($row[status])=='GH' || trim($row[status])=='FH' || trim($row[status])=='CH' || trim($row[status])=='H' || trim($row[status])=='W')
					{
						$buyer_late_min='0';
						$ofday_ot="select * from hrm_off_day_ot where emp_code='$row[emp_code]' and ot_date='$row[attnd_date]'";
						$result_ot = mysql_query( $ofday_ot ) or die( $ofday_ot . "<br />" . mysql_error() );
						if(mysql_num_rows($result_ot)==0)
						{
							$buyer_ot_hours_t="0";
							$s_in="00:00:00";
							$buyer_late_min="0";
						}
						else
						{
							while($res_ot=mysql_fetch_array($result_ot))		
							{
								$buyer_ot_hours_t=$res_ot["total_ot"];
								$buyer_late_min=$row[late_time_min];
								if ($buyer_ot_hours_t!="0.00")
								{
									$s_in=get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $var_hrm_chart[$row['company_id']]['adjust_in_time'] );
								}
								else
								{
									$s_in="00:00:00";
								}
							}
						}
						
						if ($buyer_ot_hours_t!="0.00")
						{
							$sing_in=DATE("g:i", STRTOTIME(get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $var_hrm_chart[$row['company_id']]['adjust_in_time'])));
							
							$total_hou=explode(".",$buyer_ot_hours_t);
							$of_hour=($total_hou[0]+1);
							$tot_ho=$of_hour.":".$total_hou[1];
							
							$midnight = strtotime("0:00");
							$ssm1 = strtotime($sing_in) - $midnight;
							$ssm2 = strtotime($tot_ho) - $midnight;
							$totalseconds = $ssm1 + $ssm2; // will be 21960 (6 hours and 6 minutes worth of seconds)
							$formattedTime = date("H:i", $midnight + $totalseconds);
							//$s_out= DATE("h:i a", STRTOTIME($formattedTime));
							$s_out=show_time_format( $formattedTime, $in_out_time_format );
						}
						else
							$s_out= "00:00";
					}
					/*else if($row[is_regular_day]==0)
					{
						$buyer_late_min='0';
						$ofday_ot="select * from hrm_off_day_ot where emp_code='$row[emp_code]' and ot_date='$row[attnd_date]'";
							$result_ot = mysql_query( $ofday_ot ) or die( $ofday_ot . "<br />" . mysql_error() );
							if(mysql_num_rows($result_ot)==0)
							{
								$buyer_ot_hours_t="0";
								$s_in="00:00:00";
							}
							else
							{
								while($res_ot=mysql_fetch_array($result_ot))		
									{
										$buyer_ot_hours_t=$res_ot["total_ot"];
										if ($buyer_ot_hours_t!="0.00")
										{
										$s_in=get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $var_hrm_chart[$row['company_id']]['adjust_in_time'] );
										}
										else
										{
											$s_in="00:00:00";
										}
									}
							}
							
							
							if ($buyer_ot_hours_t!="0.00")
								{
								$sing_in=DATE("g:i", STRTOTIME(get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $var_hrm_chart[$row['company_id']]['adjust_in_time'])));
									
									$total_hou=explode(".",$buyer_ot_hours_t);
									$of_hour=($total_hou[0]+1);
									$tot_ho=$of_hour.":".$total_hou[1];
									
									$midnight = strtotime("0:00");
									$ssm1 = strtotime($sing_in) - $midnight;
									$ssm2 = strtotime($tot_ho) - $midnight;
									$totalseconds = $ssm1 + $ssm2; // will be 21960 (6 hours and 6 minutes worth of seconds)
									$formattedTime = date("H:i", $midnight + $totalseconds);
									$s_out= DATE("h:i a", STRTOTIME($formattedTime));
								}
								else
									$s_out= "00:00";
						
 						$day_status_id = return_field_value("type","lib_holiday","from_date='$row[attnd_date]'");
						$day_status = $holiday_category[$day_status_id];						
						if(trim($day_status_id)==''){
							$day_status=return_field_value("weekend","hrm_weekend","emp_code=$row[emp_code]");	
							if(trim($day_status)!='')$day_status='W';					
						}
						$row[status]=$day_status;
					}*/
					else{
						
						//buyer ot hour
						if($rows['duty_roster_policy']!=0){ $var_hrm_chart[$row['company_id']]['first_ot_limit']=0;$row['total_over_time_min']=0;}
						$buyer_ot_hours_t = get_buyer_ot_hr($row[total_over_time_min], $var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] );
						$buyer_late_min=$row[late_time_min]; 					
						if($row[over_time_slot_1_min]!=''){$ot += $buyer_ot_hours_t;}//get_buyer_ot_hr($row[total_over_time_min], $var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit'] );
						 		
						//buyer in out time
						$s_in=get_buyer_in_time( $row[sign_in_time], $row[r_sign_in_time], $var_hrm_chart[$row['company_id']]['adjust_in_time'] );
						//if($buyer_in_time!='00:00:00'){$s_in = DATE("g:i a", STRTOTIME($buyer_in_time));}else $s_in="00:00";
						$s_out=  show_time_format( get_buyer_out_time($row['sign_out_time'], $row['r_sign_out_time'], $adjust_out_time, 120, $buyer_ot_hours_t, $ot_start_min,$one_hr_ot_unit,$rows['ot_entitled'],$row[r_sign_in_time]), $in_out_time_format ); 
						
						}
						
						 if($row[status]=="CH") $row[status]="H";
					
			?>		
					
                    <tr height="27">
						<td width=''><? echo $row[attnd_date];?></td>
						<td width=''><? echo $shift_arr[$row[policy_shift_id]];?></td>						
						<td width=''><? echo show_time_format( $r_in, $in_out_time_format );?></td>
						<td width=''><? echo show_time_format( $r_out, $in_out_time_format );?></td>
						<td width=''><? echo show_time_format( $s_in, $in_out_time_format );?></td>						
						<td width=''><? echo $s_out;//."=".$row['sign_out_time'];?></td> 	
						<td width=''><? echo $buyer_late_min;?></td>
						<td width=''><? echo $buyer_ot_hours_t;?></td>
						<td width=''><? echo $row[status];?></td>
                    </tr>
                        					
			<?					
						
						if($row[status]=='CL'){$cl++;}
						if($row[status]=='SL'){$sl++;}
						if($row[status]=='ML'){$ml++;}
						if($row[status]=='EL'){$el++;}
						if($row[status]=='LWP'){$lwp++;}
						if($row[status]=='SpL'){$spl++;}
						if($row[status]=='H' || $row[status]=='GH' || $row[status]=='FH' || $row[status]=='CH'){$holiday++;}
						if($row[status]=='W'){$weekend++;}
						if($row[status]=='P'){$present++;}
						if($row[status]=='D'){$late++;}
						if($row[status]=='MR'){$movement++;}
						if($row[status]=='A'){$absent++;}
						$total_days++;
						if($row[total_over_time_min]!=''){$ot += $buyer_ot_hours_t;}
						$overt += $buyer_ot_hours_t;
					}
					$working_day=$total_days-$holiday-$weekend;					 
					//$total_elapsedHoursMins = sprintf("%d hours %02d minutes", abs((int)($ot/60)), abs((int)($ot%60)));	
					
				?>
						<tr><td colspan=9 height="25" align="center"><b>Summary</b></td></tr>
                        <tr bgcolor="#A5C3EB" height="30">
							<td colspan=9 height="25">
								<b>Total Day:</b><? echo $total_days;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Weekend Day:</b><? echo $weekend;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Holiday:</b><? echo $holiday;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Working Day:</b><? echo $working_day;?> &nbsp;&nbsp;&nbsp;&nbsp;
								<b>Present:</b><? echo $present;?> &nbsp;&nbsp;&nbsp;&nbsp;
								<b>Late Present:</b><? echo $late;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Movement:</b><? echo $movement;?>&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Total OT:</b><? echo $overt;?>&nbsp;&nbsp;&nbsp;&nbsp;
                                <b>Payable Days:</b><? echo $total_days-$absent;?>&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
												
						</tr>
						<tr bgcolor="#A5C3EB"  height="30">
							<td colspan=9>
								<b>Absent:</b><? echo $absent;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Casual Leave:</b><? echo $cl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Sick Leave:</b><? echo $sl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Earn Leave:</b><? echo $el;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Special Leave:</b><? echo $spl;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Leave Without Pay:</b><? echo $lwp;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Maternity Leave:</b><? echo $ml;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>	
						</tr>
					</table><br /><br /><br />
                    <p></p>
		<?   
			}//end while condition
		
		}// end if condition
	
	$html = ob_get_contents();
	ob_clean();	
	
	foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();
}


//month generated
if($type=="select_month_generate")
{		
	//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
	$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id  and b.type=1 ";//and b.is_locked=0
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	echo "<option value=0>-- Select --</option>";
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$start_1 = substr($row["starting_date"],0,3);
		$start_2 = substr($row["starting_date"],-1,1);

		$end_1 = substr($row["ending_date"],0,3);
		$end_2 = substr($row["ending_date"],-2,2);
		
		echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$start_1." ".$start_2." - ".$end_1." ".$end_2."</option>";
		//echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>".$row[starting_date]." To ".$row[ending_date]."</option>";
	}		
	exit();
}