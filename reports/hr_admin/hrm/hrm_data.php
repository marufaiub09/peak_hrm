<?php
session_start();
include('../../../includes/common.php');
extract( $_POST );

if( file_exists( "../../../cache/company_details.txt" ) ) {
	$company_details = unserialize( file_get_contents( "../../../cache/company_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/company_details.txt", serialize( $company_details ) );
}
if( file_exists( "../../../cache/location_details.txt" ) ) {
	$location_details = unserialize( file_get_contents( "../../../cache/location_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = mysql_real_escape_string($row);
	}
	file_put_contents( "../../../cache/location_details.txt", serialize( $location_details ) );
}
if( file_exists( "../../../cache/division_details.txt" ) ) {
	$division_details = unserialize( file_get_contents( "../../../cache/division_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/division_details.txt", serialize( $division_details ) );
}
if( file_exists( "../../../cache/department_details.txt" ) ) {
	$department_details = unserialize( file_get_contents( "../../../cache/department_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/department_details.txt", serialize( $department_details ) );
}
if( file_exists( "../../../cache/section_details.txt" ) ) {
	$section_details = unserialize( file_get_contents( "../../../cache/section_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/section_details.txt", serialize( $section_details ) );
}
if( file_exists( "../../../cache/subsection_details.txt" ) ) {
	$subsection_details = unserialize( file_get_contents( "../../../cache/subsection_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row;
	}
	file_put_contents( "../../../cache/subsection_details.txt", serialize( $subsection_details ) );
}

if( isset( $form ) && $form == 'id_card' ) {
	if( file_exists( "../../../cache/emp_basic.txt" ) ) {
		$emp_basic = unserialize( file_get_contents( "../../../cache/emp_basic.txt" ) );
	}
	else {
		$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,
					job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
				FROM hrm_employee AS emp
				LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
				WHERE emp.is_deleted = 0 AND emp.status_active = 1";
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$emp_basic = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$emp_basic[$row['emp_code']] = $row;
		}
		file_put_contents( "../../../cache/emp_basic.txt", serialize( $emp_basic ) );
	}
	if( file_exists( "../../../cache/designation_chart.txt" ) ) {
		$designation_chart = unserialize( file_get_contents( "../../../cache/designation_chart.txt" ) );
	}
	else {
		$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$designation_chart = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$designation_chart[$row['id']] = $row;
		}
		file_put_contents( "../../../cache/designation_chart.txt", serialize( $designation_chart ) );
	}
?>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="employees">
		<thead>
			<tr>
				<th align="left"><input type="checkbox" name="chk_all" id="chk_all" value="1" onclick="check_all( this )" /></th>
				<th>Code</th>
				<th>Employee Name</th>
				<th>ID Card</th>
				<th>Punch Card</th>
				<th>Designation</th>
				<th>Company</th>
				<th>Location</th>
				<th>Division</th>
				<th>Department</th>
				<th>Section</th>
				<th>Subsection</th>
			</tr>
			<tr id="select_header">
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $emp_basic AS $emp ) { if( $emp['status_active'] == 1 ) { ?>
			<tr class="gradeA">
				<td width="20"><input type="checkbox" name="chk_<?php echo $emp['emp_code']; ?>" value="<?php echo $emp['emp_code']; ?>" /></td>
				<td><?php echo $emp['emp_code']; ?></td>
				<td><?php echo $emp['name']; ?></td>
				<td><?php if( $emp['id_card_no'] != '' ) echo $emp['id_card_no']; ?></td>
				<td><?php if( $emp['punch_card_no'] != '' ) echo $emp['punch_card_no']; ?></td>
				<td><?php if( $emp['designation_id'] != '0' ) echo $designation_chart[$emp['designation_id']]['custom_designation']; ?></td>
				<td><?php if( $emp['company_id'] != '0' ) echo $company_details[$emp['company_id']]['company_name']; ?></td>
				<td><?php if( $emp['location_id'] != '0' ) echo $location_details[$emp['location_id']]['location_name']; ?></td>
				<td><?php if( $emp['division_id'] != '0' ) echo $division_details[$emp['division_id']]['division_name']; ?></td>
				<td><?php if( $emp['department_id'] != '0' ) echo $department_details[$emp['department_id']]['department_name']; ?></td>
				<td><?php if( $emp['section_id'] != '0' ) echo $section_details[$emp['section_id']]['section_name']; ?></td>
				<td><?php if( $emp['subsection_id'] != '0' ) echo $subsection_details[$emp['subsection_id']]['subsection_name']; ?></td>
			</tr>
			<?php } } ?>
		</tbody>
	</table>
<?php
	exit();
}
if( isset( $cost_center ) ) {
	if( $cost_center == 'location' )	{ $parent = 'company';		$child_array = $location_details;		$child_name = "location_name"; }
	if( $cost_center == 'division' )	{ $parent = 'location';		$child_array = $division_details;		$child_name = "division_name"; }
	if( $cost_center == 'department' )	{ $parent = 'division';		$child_array = $department_details;		$child_name = "department_name"; }
	if( $cost_center == 'section' )		{ $parent = 'department';	$child_array = $section_details;		$child_name = "section_name"; }
	if( $cost_center == 'subsection' )	{ $parent = 'section';		$child_array = $subsection_details;		$child_name = "subsection_name"; }
	
	$parent_result = false;
	$options1 = $options2 = "<option value=\"0\">-- Select --</option>";
	
	foreach( $child_array AS $child ) {
		if( $child['status_active'] == 1 ) {
			if( $_POST['parent_id'] != 0 && $child[$parent . '_id'] == $_POST['parent_id'] ) {
				$parent_result = true;
				if( $child['id'] == $_POST['selected_id'] ) $options1 .= "<option value=\"$child[id]\" selected>" . $child["$child_name"] . "</option>";
				else $options1 .= "<option value=\"$child[id]\">" . $child["$child_name"] . "</option>";
			}
			if( $child['id'] == $_POST['selected_id'] ) $options2 .= "<option value=\"$child[id]\" selected>" . $child["$child_name"] . "</option>";
			else $options2 .= "<option value=\"$child[id]\">" . $child["$child_name"] . "</option>";
		}
	}
	if( $parent_result == true ) print_r( $options1 );
	else print_r( $options2 );
	exit();
}
?>