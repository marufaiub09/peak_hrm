<?php
session_start();
include("../../../includes/common.php");
include('../../../includes/common_functions.php');
		
$separation_type=array(1=>"Resignation",2=>"Retirement",3=>"Disability",4=>"Death",5=>"Terminated",6=>"Un-noticed");
$sex=array(0=>"Male",1=>"Female");
$marital_status=array(0=>"Single",1=>"Maried",2=>"Separated",3=>"Widow");
$blood_group=array(0=>"-- Select --",1=>"A+",2=>"A-",3=>"B+",4=>"B-",5=>"AB+",6=>"AB-",7=>"O+",8=>"O-");
$emp_relation=array(0=>"-- Select --",1=>"Husband",2=>"Wife",3=>"Son",4=>"Daughter");
//Designation array
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result )) 
	{
		$designation_chart[$row['id']] = $row['custom_designation'];
	}
	
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
//Department array

	$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$department_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$department_details[$row['id']] = $row['department_name'];
	}
//Diviion
	$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$division_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$division_details[$row['id']] = $row['division_name'];
	}
//Location
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] =$row['location_name'];
	}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$section_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$section_details[$row['id']] = $row['section_name'];
	}
//Sub Section
	$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$subsection_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$subsection_details[$row['id']] = $row['subsection_name'];
	}
//Address Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_division = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_division[$row['id']] = $row['division_name'];
	}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$lib_list_district = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$lib_list_district[$row['id']] = $row['district_name'];
	}
//Variable Settings
$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 and status_active=1 ";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$var_hrm_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	
	$var_hrm_chart[$row['company_name']] = array();
	$var_hrm_chart[$row['company_name']]['adjust_in_time'] = mysql_real_escape_string( $row['adjust_in_time'] );
	$var_hrm_chart[$row['company_name']]['adjust_out_time'] = mysql_real_escape_string( $row['adjust_out_time'] );
	$var_hrm_chart[$row['company_name']]['first_ot_limit'] = mysql_real_escape_string( $row['first_ot_limit'] );
	$var_hrm_chart[$row['company_name']]['one_hour_ot_unit'] = mysql_real_escape_string( $row['one_hour_ot_unit'] );
	$var_hrm_chart[$row['company_name']]['allow_ot_fraction'] = mysql_real_escape_string( $row['allow_ot_fraction'] );
	$var_hrm_chart[$row['company_name']]['ot_start_minute'] = mysql_real_escape_string( $row['ot_start_minute'] );
	

}	
extract($_GET);

$active_data=explode("_", $data);
//print_r($active_data);
//print $search_string;

function my_old($dob){
	$now = date('d-m-Y');
	$dob = explode('-', $dob);
	$now = explode('-', $now);
	$mnt = array(1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if (($now[2]%400 == 0) or ($now[2]%4==0 and $now[2]%100!=0)) $mnt[2]=29;
	if($now[0] < $dob[0]){
		$now[0] += $mnt[$now[1]-1];
		$now[1]--;
	}
	if($now[1] < $dob[1]){
		$now[1] += 12;
		$now[2]--;
	}
	if($now[2] < $dob[2]) return false;
	return  array('year' => $now[2] - $dob[2], 'mnt' => $now[1] - $dob[1], 'day' => $now[0] - $dob[0]);
}
//$age = my_old('10-1-1979');
//print_r($age);
//printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);

?>
<style type="text/css">
    @page { size 8.5in 11in; margin: 0.5in; }
    div.page { page-break-after: always; background-color:#FFF;}
</style>
<?php
	//Active Employee List
if($type=="active_emp_list")
	{
		if($active_data[0]=="") $category="%%"; else $category=$active_data[0];
		if($active_data[1]==0) $company_name="%%"; else $company_name=$active_data[1];
		if($active_data[2]==0) $location_name="%%"; else $location_name=$active_data[2];
		if($active_data[3]==0) $division_name="%%"; else $division_name=$active_data[3];
		if($active_data[4]==0) $department_name="%%"; else $department_name=$active_data[4];
		if($active_data[5]==0) $section_name="%%"; else $section_name=$active_data[5];
		if($active_data[6]==0) $designation_name="%%"; else $designation_name=$active_data[6];
		if($active_data[7]==0) $subsection_name="%%"; else $subsection_name=$active_data[7];
		if($search_string=="") $search_string="%%"; else $search_string=$search_string;
		
			/*$sql= "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS  name,emp.designation_id,emp.dob,emp.joining_date,emp.sex,
							job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
						FROM hrm_employee AS emp
						LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
						WHERE emp.emp_code like '$search_string' and emp.category like '$category' and emp.designation_id like '$designation_name' and job.company_id like '$company_name' and job.location_id like '$location_name' and job.division_id like '$division_name' and job.department_id like '$department_name' and job.section_id like '$section_name' and job.subsection_id like '$subsection_name' and emp.is_deleted=0 and emp.status_active=1 order by emp.emp_code ASC";*/
						$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
						FROM hrm_employee 
						WHERE emp_code like '$search_string' and category like '$category' and designation_id like '$designation_name' and company_id like '$company_name' and location_id like '$location_name' and division_id like '$division_name' and department_id like '$department_name' and section_id like '$section_name' and subsection_id like '$subsection_name' and is_deleted=0 and status_active=1 order by emp_code ASC";
						$result=mysql_query($sql);
						
						if(mysql_num_rows($result)==0)
						{
							echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
						}
						else
						{?>
	<table width="2000" cellpadding="0" cellspacing="1" border="0">
		<tr bgcolor="#8FAA8C">
			<td width="30" align="center">SL. No</td>
            <td width="100" align="center">Emp Photo</td>
			<td width="100" align="center">Emp Code</td>
			<td width="130" align="center">Emp Name</td>
			<td width="120" align="center">Designation</td>
			<td width="100" align="center">DOB</td>
			<td width="100" align="center">DOJ</td>
			<td width="300" align="center">Address</td>
			<td width="80" align="center">Sex</td>
			<td width="100" align="center">Religion</td>
			<td width="130" align="center">Age</td>
			<td width="160" align="center">Division</td>
			<td width="130" align="center">Department</td>
			<td width="100" align="center">Section</td>
			<td width="100" align="center">Sub Section</td>
            <td width="130" align="center">Route</td>
			<td width="100" align="center">Vehicle No.</td>
            <td></td>
	   </tr>
       </table>
       <div style="overflow:scroll; height:400px; width:2020px">
            <table width="2000" cellspacing="1" cellpadding="0" border="0">
	  <?php                  
	$i=1;
	while($emp=mysql_fetch_assoc($result))
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
			$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=1 and status_active=1 and is_deleted=0";
			$result1=mysql_query($sql1);
			$row=mysql_fetch_assoc($result1);
			if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
			if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
			if($row['village']=="") $village=""; else $village=$row['village'].", ";
			if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
			if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
			$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
			
			$sql2="SELECT * FROM `hrm_transport_mst` WHERE `vehicle_no` in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
			$result2=mysql_query($sql2);
			$row2=mysql_fetch_assoc($result2);
			
			//$sql3="select * from resource_photo where identifier ='$emp[emp_code]' and type=0 and is_deleted=0";
			//$result3=mysql_query($sql3);
			//$row3=mysql_fetch_assoc($result3);
			$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
			$image="";
			if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
	?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
				<td width="30" align="center"><?php echo $i;?></td>
                <td width="100" align="center"><?php echo $image; ?></td>
				<td width="100" align="center"><?php echo $emp['emp_code'] ; ?></td>
				<td width="130" align="left"><?php echo $emp['name'] ; ?></td>
				<td width="120" align="center"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
				<td width="100" align="center"><?php echo $emp['dob'] ; ?></td>
				<td width="100" align="center"><?php echo $emp['joining_date'] ; ?></td>
				<td width="300" align="center"><?php echo $address ; ?></td>
				<td width="80" align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
				<td width="100" align="center"><?php echo $emp['religion'] ; ?></td>
				<td width="130" align="center"><?php
					$birth_date=convert_to_mysql_date($emp['dob']);
					$age = my_old($birth_date);
					printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);
					?></td>
				   <td width="160" align="center"><?php echo $division_details[$emp['division_id']]; ?></td>
				   <td width="130" align="center"><?php echo $department_details[$emp['department_id']]; ?></td>
				   <td width="100" align="center"><?php echo $section_details[$emp['section_id']]; ?></td>
				   <td width="100" align="center"><?php echo $subsection_details[$emp['subsection_id']]; ?></td> 
                   <td width="130" align="center"><?php echo $row2['route']; ?></td>
                   <td width="100" align="center"><?php echo $row2['vehicle_no']; ?></td> 
                    <td></td>
			</tr>
			<?php $i++;}
						} // end else
			?>
            </table></div>
		<?php	
			} //end Active Employee List if condition
			
		//separated Employee List start if condition
if($type=="separated_emp_list")
			{
				if($active_data[0]=="") $category="%%"; else $category=$active_data[0];
				if($active_data[1]==0) $company_name="%%"; else $company_name=$active_data[1];
				if($active_data[2]==0) $location_name="%%"; else $location_name=$active_data[2];
				if($active_data[3]==0) $division_name="%%"; else $division_name=$active_data[3];
				if($active_data[4]==0) $department_name="%%"; else $department_name=$active_data[4];
				if($active_data[5]==0) $section_name="%%"; else $section_name=$active_data[5];
				if($active_data[6]==0) $designation_name="%%"; else $designation_name=$active_data[6];
				if($active_data[7]=="") $from_date=""; else $from_date=convert_to_mysql_date($active_data[7]);
				if($active_data[8]=="") $to_date=""; else $to_date=convert_to_mysql_date($active_data[8]);
				if($from_date!="" && $to_date=="")
				{
					$to_date=$from_date;	
				}
				if($from_date=="") $str_date=""; else $str_date=" and separated_from between '$from_date' and '$to_date' ";
				if($search_string=="") $search_string="%%"; else $search_string=$search_string;
				
					/*$query="SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
					FROM hrm_employee AS emp
					LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
			WHERE emp.emp_code like '$search_string' and emp.designation_id like '$designation_name' and job.company_id like '$company_name' and job.location_id like '$location_name' and job.division_id like '$division_name' and job.department_id like '$department_name' and job.section_id like '$section_name' and emp.is_deleted=0 and emp.status_active=0 and emp.emp_code in(select emp_code from hrm_separation where status_active=1 and is_deleted=0 and separation_type like '$category' $str_date) order by emp.designation_level ASC";*/
				 	$query="SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name
					FROM hrm_employee
			WHERE emp_code like '$search_string' and designation_id like '$designation_name' and company_id like '$company_name' and location_id like '$location_name' and division_id like '$division_name' and department_id like '$department_name' and section_id like '$section_name' and is_deleted=0 and status_active=0 and emp_code in(select emp_code from hrm_separation where status_active=1 and is_deleted=0 and separation_type like '$category' $str_date) order by designation_level ASC";
					$result=mysql_query($query);
					
					if(mysql_num_rows($result)==0)
						{
							echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
						}
						else
						{?>
	<table width="100%" border="0">
		<tr bgcolor="#8FAA8C">
			<td align="center">SL. No</td>
			<td align="center">Emp Code</td>
			<td align="center">Emp Name</td>
            <td align="center">Card No.</td>
			<td align="center">Designation</td>
            <td align="center">Department</td>
            <td align="center">DOJ</td>
            <td align="center">DOC</td>
            <td align="center">Gross Salary</td>
            <td align="center">Sex</td>
			<td align="center">Religion</td>
			<td align="center">DOB</td>
			<!--<th align="center">Age</th>-->
			<td align="center">Sep. Type</td>
			<td align="center">DOS</td>
			<td align="center">Cause of Sep.</td>
	   </tr>
	  <?php  
	  			$i=1;
	while($emp=mysql_fetch_assoc($result))
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
			$sql1="select * from hrm_separation where emp_code='$emp[emp_code]' and status_active=1 and is_deleted=0";
			$result1=mysql_query($sql1);
			$row=mysql_fetch_assoc($result1);
	?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
				<td align="center"><?php echo $i;?></td>
				<td align="center"><?php echo $emp['emp_code'] ; ?></td>
				<td align="left"><?php echo $emp['name'] ; ?></td>
                <td align="center"><?php echo $emp['id_card_no'] ; ?></td>
				<td align="center"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
                <td align="center"><?php echo $department_details[$emp['department_id']]; ?></td>
                <td align="center"><?php echo $emp['joining_date'] ; ?></td>
                <td align="center"><?php echo $emp['confirmation_date'] ; ?></td>
                <td align="center"><?php echo $emp['gross_salary'] ; ?></td>
                <td align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
				<td align="center"><?php echo $emp['religion'] ; ?></td>
				<td align="center"><?php echo $emp['dob'] ; ?></td>
				<!--<td align="center">
					
					?></td>-->
                    <td align="center"><?php echo $separation_type[$row['separation_type']]; ?></td>
				   <td align="center"><?php echo $row['separated_from']; ?></td>
                   <td align="center"><?php echo $row['cause_of_separation']; ?></td>
			</tr>
			<?php $i++;}
						} // end else
			?>
            </table>
    <?php                
			}// end separated Employee List  if condition 
			
			// Headcount of Employees start if condition
if($type=="headcount_of_employees")
			{
				
				$active_data=explode("_", $data);
				//print_r($active_data);
				
				$to_date=convert_to_mysql_date($active_data[2]);
				 
				$month_prev=add_month($active_data[2],-11);
				 
			    $start_mon=date("M",strtotime($month_prev));
				$end_mon=date("M",strtotime($to_date));
				  
				 //$str_date=date("Y-m"."%%",strtotime($start_mon)) ;
				 //echo $str_date;
				 
				
				echo "<table border='0' width='82%' cellspacing='1'><tr bgcolor='#8FAA8C'><td align='center'>Head Count</td>";
						 
						for($i=0;$i<=11;$i++)
						{
							
							echo "<td align='center'>".date("M",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."</td>";
						}
					//test
				echo"</tr>";
				//die;
						$bar="";
						$total=0;
						$str_cond="";
						$str_cond_all="";
						
						if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_all="location_id"; $bar = &$location_details;}
						if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_all="division_id"; $bar = &$division_details;}
						if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_all="department_id"; $bar = &$department_details;} 
						if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_all="section_id"; $bar = &$section_details;}
						if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_all="subsection_id"; $bar = &$subsection_details;} 
						if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_all="designation_id"; $bar = &$designation_chart;}
						    $sql="select distinct($str_cond_all) from hrm_employee where company_id='$active_data[0]' and status_active=1 and is_deleted=0 $str_cond order by $str_cond_all desc";	
							$result=mysql_query($sql);
							$co=1;
							while($row=mysql_fetch_array($result))
							{
								if ($co%2==0)  
									$tr_bgcolor="#cccccc";
								else
									$tr_bgcolor="#FFFFFF";
									
								echo"<tr bgcolor='$tr_bgcolor' id='$co' onclick='toggle($co,\"$tr_bgcolor\")'>";
									echo"<td>".$bar[$row[0]]."</td>";
									for($cn=0;$cn<=11;$cn++)
									{
										$cbo_year_selector=date("Y"."%%",strtotime(add_month($month_prev,$cn)));
										$cbo_month_selector=date("m"."%%",strtotime(add_month($month_prev,$cn)));
										$get_days=cal_days_in_month(CAL_GREGORIAN, $cbo_month_selector, $cbo_year_selector);
										$str_date=date("Y-m",strtotime(add_month($month_prev,$cn)));
										$str_last=$str_date."-".$get_days;
							 			//echo $str_last;
										// $sql1="select count(*) as data from hrm_employee a,hrm_employee_job b  where a.emp_code=b.emp_code and b.company_id='$active_data[0]' and a.joining_date<='$str_last' and $str_cond_search='$row[0]' and a.emp_code not in (select emp_code from hrm_separation where separated_from<='$str_last') and a.status_active=1 and a.is_deleted=0 $str_cond2";
								    	$sql1="select count(*) as data from hrm_employee where company_id='$active_data[0]' and joining_date<='$str_last' and $str_cond_all='$row[0]' and emp_code not in (select emp_code from hrm_separation where separated_from<='$str_last') and status_active=1 and is_deleted=0 $str_cond";
										$result1=mysql_query($sql1);
										$row_data=mysql_fetch_assoc($result1);
										if($row_data['data']=="") $row_data['data']=0; else $row_data['data']=$row_data['data']; 
										echo "<td align='right'>".$row_data['data']."</td>";
										$month_total[$cn]=$month_total[$cn]+$row_data['data'];
									}
								
								echo"</tr>";
							$co++;}
						echo "
							<tr bgcolor='#aacc9a'>
								<td align='right'>Total=</td>";
								for($j=0;$j<=11;$j++)
								{
								echo "<td align='right'>$month_total[$j]</td>";
								}
						echo"</tr>";
				echo"</table>";
			
			}// end head count type if condition
			
	// start leavers report if condition		
if($type=="leavers_report")
{
			    $active_data=explode("_", $data);
				//print_r($active_data);
				
				$to_date=convert_to_mysql_date($active_data[2]);
				 
				$month_prev=add_month($active_data[2],-11);
				 
				$start_mon=date("M",strtotime($month_prev));
				$end_mon=date("M",strtotime($to_date));
				
				echo "<table border='0' width='82%' cellspacing='1'><tr bgcolor='#8FAA8C'><td align='center'>leavers Head Count</td>";
						 
						for($i=0;$i<=11;$i++)
						{
						 	
							echo "<td align='center'>".date("M",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."</td>";
						}
				echo"</tr>";
				
						$bar="";
						$total=0;
						$str_cond="";
						$str_cond_all="";

						if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_all="location_id"; $bar = &$location_details;}
						if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_all="division_id"; $bar = &$division_details;}
						if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_all="department_id"; $bar = &$department_details;} 
						if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_all="section_id"; $bar = &$section_details;}
						if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_all="subsection_id"; $bar = &$subsection_details;} 
						if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_all="designation_id"; $bar = &$designation_chart;}
						
						   $sql="select distinct($str_cond_all) from hrm_employee where company_id='$active_data[0]' and status_active=1 and is_deleted=0 $str_cond order by $str_cond_all desc";	

							$result=mysql_query($sql);
							$co=1;
							while($row=mysql_fetch_array($result))
							{
								if ($co%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
									
								echo"<tr bgcolor='$bgcolor'>";
									echo"<td>".$bar[$row[0]]."</td>";
									for($cn=0;$cn<=11;$cn++)
									{
										$str_date=date("Y-m"."%%",strtotime(add_month($month_prev,$cn)));
							 			//echo $str_date;
									    $sql1="select count(*) as data from hrm_separation a,hrm_employee b where a.emp_code=b.emp_code and b.company_id='$active_data[0]' and a.separated_from like '$str_date' and b.$str_cond_all='$row[0]' and b.status_active=0 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0";
										$result1=mysql_query($sql1);
										$row_data=mysql_fetch_assoc($result1);
										if($row_data['data']=="") $row_data['data']=0; else $row_data['data']=$row_data['data']; 
										echo "<td align='right'>".$row_data['data']."</td>";
										$month_total[$cn]=$month_total[$cn]+$row_data['data'];
									}
								
								echo"</tr>";
							$co++;}
						echo "<tr bgcolor='#aacc9a'>
								<td align='right'>Total=</td>";
								for($j=0;$j<=11;$j++)
								{
								echo "<td align='right'>$month_total[$j]</td>";
								}
						echo"</tr>";
							
				echo"</table>";
}// end leavers report if condition
	
	// start if condition starters report
if($type=="starters_report")
{
				$active_data=explode("_", $data);
				//print_r($active_data);
				
				$to_date=convert_to_mysql_date($active_data[2]);
				 
				$month_prev=add_month($active_data[2],-11);
				 
				$start_mon=date("M",strtotime($month_prev));
				$end_mon=date("M",strtotime($to_date));
				
				echo "<table border='0' width='82%' cellspacing='1'><tr bgcolor='#8FAA8C'><td align='center'>Starters Head Count</td>";
						 
						for($i=0;$i<=11;$i++)
						{
						 	
							echo "<td align='center'>".date("M",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."</td>";
						}
				echo"</tr>";
				
						$bar="";
						$str_cond="";
						$str_cond_all="";
						$total=0;
						/*$str_cond_search="";
						$str_cond2="";if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_search="b.location_id"; $str_cond2="group by b.location_id"; $str_cond_all="location_id"; $bar = &$location_details;} 
						else if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_search="b.division_id"; $str_cond2="group by b.division_id"; $str_cond_all="division_id"; $bar = &$division_details;} 
						else if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_search="b.department_id"; $str_cond2="group by b.department_id"; $str_cond_all="department_id"; $bar = &$department_details;} 
						else if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_search="b.section_id"; $str_cond2="group by b.section_id"; $str_cond_all="section_id"; $bar = &$section_details; }
						else if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_search="b.subsection_id"; $str_cond2="group by b.subsection_id"; $str_cond_all="subsection_id"; $bar = &$subsection_details;} 
						
						if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_search="a.designation_id"; $str_cond2="group by a.designation_id"; $str_cond_all="designation_id"; $bar = &$designation_chart;
						
							 $sql="select distinct(a.designation_id) from hrm_employee a, hrm_employee_job b where b.company_id='$active_data[0]' and a.status_active=1 and a.is_deleted=0 $str_cond2";		
						}
						else{
							 $sql="select distinct($str_cond_all) from hrm_employee_job where company_id='$active_data[0]' and status_active=1 and is_deleted=0";
							} */
						if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_all="location_id"; $bar = &$location_details;}
						if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_all="division_id"; $bar = &$division_details;}
						if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_all="department_id"; $bar = &$department_details;} 
						if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_all="section_id"; $bar = &$section_details;}
						if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_all="subsection_id"; $bar = &$subsection_details;} 
						if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_all="designation_id"; $bar = &$designation_chart;}
						    $sql="select distinct($str_cond_all) from hrm_employee where company_id='$active_data[0]' and status_active=1 and is_deleted=0 $str_cond order by $str_cond_all desc";	
								$result=mysql_query($sql);
							$co=1;
							while($row=mysql_fetch_array($result))
							{
								if ($co%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
									
								echo"<tr bgcolor='$bgcolor'>";
									echo"<td>".$bar[$row[0]]."</td>";
									for($cn=0;$cn<=11;$cn++)
									{
										
										$str_date=date("Y-m"."%%",strtotime(add_month($month_prev,$cn)));
							 			//echo $str_date;
										
									  $sql1="select count(*) as data from hrm_employee where company_id='$active_data[0]' and joining_date like '$str_date' and $str_cond_all='$row[0]' and status_active=1 and is_deleted=0 $str_cond";
									   
										$result1=mysql_query($sql1);
										$row_data=mysql_fetch_assoc($result1);
										if($row_data['data']=="") $row_data['data']=0; else $row_data['data']=$row_data['data']; 
										echo "<td align='right'>".$row_data['data']."</td>";
										$month_total[$cn]=$month_total[$cn]+$row_data['data'];
									}
								
								echo"</tr>";
							$co++;}
							
						echo "<tr bgcolor='#aacc9a'>
								<td align='right'>Total=</td>";
								for($j=0;$j<=11;$j++)
								{
								echo "<td align='right'>$month_total[$j]</td>";
								}
						echo"</tr>";
							
				echo"</table>";
}// end if condition starters report

// start if condition starters report detail
if($type=="starters_report_detail")
{			
		$active_data=explode("_", $data);
		//print_r($active_data);

		$from_date=convert_to_mysql_date($active_data[2]);
		$to_date=convert_to_mysql_date($active_data[3]);
		$from_month=convert_to_mysql_date($active_data[4]);
		$to_month=convert_to_mysql_date($active_data[5]);
		
		$from_month_str=date("Y-M",strtotime($from_month));
		$to_month_str=date("Y-M",strtotime($to_month));
		$diff = abs(strtotime($to_month_str) - strtotime($from_month_str));
		$total_months = floor($diff / (30*60*60*24));
		
		$month_prev=add_month($active_data[5],-$total_months);

		$start_mon=date("M",strtotime($month_prev));
		$end_mon=date("M",strtotime($to_month));
		
		$str_cond="";
		$str_cond_all="";
		
		if($active_data[1]==1) {$str_cond="group by location_id"; $str_cond_all="location_id";} 
		else if($active_data[1]==2) {$str_cond="group by division_id"; $str_cond_all="division_id";} 
		else if($active_data[1]==3) {$str_cond="group by department_id"; $str_cond_all="department_id";} 
		else if($active_data[1]==4) {$str_cond="group by section_id"; $str_cond_all="section_id";}
		else if($active_data[1]==5) {$str_cond="group by subsection_id"; $str_cond_all="subsection_id";} 
		else if($active_data[1]==6) {$str_cond="group by designation_id"; $str_cond_all="designation_id";}
		
		if($active_data[2]!="")
		{
			$sql="SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
			FROM hrm_employee 
			WHERE company_id='$active_data[0]' and joining_date between '$from_date' and '$to_date' and is_deleted=0 and status_active=1 $str_cond, joining_date order by designation_level ASC";
			$result=mysql_query($sql);
			
			if(mysql_num_rows($result)==0)
			{
					echo "<p align='center' style='font-size:16px' style='color:#000'><b>No Data Found.</b></p>";
			}
			else
			{
				 ?>
						<table width="100%" cellpadding="0" cellspacing="1" border="0">
                        	<tr bgcolor="#F0F0B8"><td colspan="17" style="font-size:16px"><?php echo "Starters Report From $active_data[2] To $active_data[3]";?></td></tr>
							<tr bgcolor="#8FAA8C">
								<td align="center">SL. No</td>
								<td align="center">Emp Photo</td>
								<td align="center">Emp Code</td>
								<td align="center">Emp Name</td>
								<td align="center">Designation</td>
								<td align="center">DOB</td>
								<td align="center">DOJ</td>
								<td align="center">Address</td>
								<td align="center">Sex</td>
								<td align="center">Religion</td>
								<td align="center">Age</td>
								<td align="center">Division</td>
								<td align="center">Department</td>
								<td align="center">Section</td>
								<td align="center">Sub Section</td>
								<td align="center">Route</td>
								<td align="center">Vehicle No.</td>
						   </tr>
						
				 <?php  
						$j=1;
						while($emp=mysql_fetch_assoc($result))
						{
							if ($j%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=1 and status_active=1 and is_deleted=0";
								//$sql1="select a.*,b.location from hrm_employee_address a LEFT JOIN resource_photo AS b ON a.emp_code=b.identifier where a.emp_code='$emp[emp_code]' and a.address_type=0 and a.status_active=1 and a.is_deleted=0 and b.type=0 and b.is_deleted=0";
								$result1=mysql_query($sql1);
								$row=mysql_fetch_assoc($result1);
								if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
								if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
								if($row['village']=="") $village=""; else $village=$row['village'].", ";
								if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
								if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
								$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
								
								$sql2="SELECT * FROM `hrm_transport_mst` WHERE `vehicle_no` in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
								$result2=mysql_query($sql2);
								$row2=mysql_fetch_assoc($result2);
								
								//$sql3="select * from resource_photo where identifier ='$emp[emp_code]' and type=0 and is_deleted=0";
								//$result3=mysql_query($sql3);
								//$row3=mysql_fetch_assoc($result3);
								$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
								$image="";
								if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
						?>
								<tr bgcolor="<?php echo $bgcolor; ?>">
									<td align="center"><?php echo $j;?></td>
									<td align="center"><?php echo $image; ?></td>
									<td align="center"><?php echo $emp['emp_code'] ; ?></td>
									<td align="left"><?php echo $emp['name'] ; ?></td>
									<td align="center"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
									<td align="center"><?php echo $emp['dob'] ; ?></td>
									<td align="center"><?php echo $emp['joining_date'] ; ?></td>
									<td align="center"><?php echo $address ; ?></td>
									<td align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
									<td align="center"><?php echo $emp['religion'] ; ?></td>
									<td align="center"><?php
										$birth_date=convert_to_mysql_date($emp['dob']);
										$age = my_old($birth_date);
										printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);
										?></td>
                                    <td align="center"><?php echo $division_details[$emp['division_id']]; ?></td>
                                    <td align="center"><?php echo $department_details[$emp['department_id']]; ?></td>
                                    <td align="center"><?php echo $section_details[$emp['section_id']]; ?></td>
                                    <td align="center"><?php echo $subsection_details[$emp['subsection_id']]; ?></td> 
                                    <td align="center"><?php echo $row2['route']; ?></td>
                                    <td align="center"><?php echo $row2['vehicle_no']; ?></td> 
								</tr>
						   
								<?php $j++;}// end while loop
									?>
								</table>
					
		  <?php
				}//end else
		}
		else
		{
			echo "<table border='0' width='100%' cellspacing='1'>";
			 
			for($i=0;$i<=$total_months;$i++)
			{
				
				echo "<tr bgcolor='#F0F0B8'><td align='left'><b>"."Starters during the month of ".date("F",strtotime(add_month($month_prev,$i)))."'".date("y",strtotime(add_month($month_prev,$i)))."<b></td></tr>";
				$str_date2=date("Y-m"."%%",strtotime(add_month($month_prev,$i)));
				$sql="SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
			FROM hrm_employee
			WHERE company_id='$active_data[0]' and joining_date like '$str_date2' and is_deleted=0 and status_active=1 $str_cond, joining_date order by designation_level ASC";
			$result=mysql_query($sql);
			
			if(mysql_num_rows($result)==0)
			{
					echo "<tr><td><p align='center' style='color:#000'><b>No Data Found.</b></p></td></tr>";
			}
			else// start search by month
			{
				 ?>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="1" border="0">
							<tr bgcolor="#8FAA8C">
								<td align="center">SL. No</td>
								<td align="center">Emp Photo</td>
								<td align="center">Emp Code</td>
								<td align="center">Emp Name</td>
								<td align="center">Designation</td>
								<td align="center">DOB</td>
								<td align="center">DOJ</td>
								<td align="center">Address</td>
								<td align="center">Sex</td>
								<td align="center">Religion</td>
								<td align="center">Age</td>
								<td align="center">Division</td>
								<td align="center">Department</td>
								<td align="center">Section</td>
								<td align="center">Sub Section</td>
								<td align="center">Route</td>
								<td align="center">Vehicle No.</td>
						   </tr>
						
				 <?php  
						$j=1;
						while($emp=mysql_fetch_assoc($result))
						{
							if ($j%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								$sql1="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=1 and status_active=1 and is_deleted=0";
								//$sql1="select a.*,b.location from hrm_employee_address a LEFT JOIN resource_photo AS b ON a.emp_code=b.identifier where a.emp_code='$emp[emp_code]' and a.address_type=0 and a.status_active=1 and a.is_deleted=0 and b.type=0 and b.module='HRM'and b.category=0 and b.is_deleted=0";
								$result1=mysql_query($sql1);
								$row=mysql_fetch_assoc($result1);
								if($row['house_no']=="") $house_no=""; else $house_no="House#".$row['house_no'].", ";
								if($row['road_no']=="") $road_no=""; else $road_no="Road#".$row['road_no'].", ";
								if($row['village']=="") $village=""; else $village=$row['village'].", ";
								if($row['post_code']=="") $post_code=""; else $post_code=$row['post_code'].", ";
								if($row['thana']=="") $thana=""; else $thana=$row['thana'].", ";
								$address=$house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row['district_id']];
								
							$sql2="SELECT * FROM `hrm_transport_mst` WHERE `vehicle_no` in (select vehicle_no from hrm_transport_employee_list where emp_code='$emp[emp_code]')";
								$result2=mysql_query($sql2);
								$row2=mysql_fetch_assoc($result2);
								
								$row3=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
								$image="";
								if($row3=="") $image=""; else $image="<img src='../../../$row3' height='60' width='60' />";
						?>
								<tr bgcolor="<?php echo $bgcolor; ?>">
									<td align="center"><?php echo $j;?></td>
									<td align="center"><?php echo $image; ?></td>
									<td align="center"><?php echo $emp['emp_code'] ; ?></td>
									<td align="left"><?php echo $emp['name'] ; ?></td>
									<td align="center"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
									<td align="center"><?php echo $emp['dob'] ; ?></td>
									<td align="center"><?php echo $emp['joining_date'] ; ?></td>
									<td align="center"><?php echo $address ; ?></td>
									<td align="center"><?php if($emp['sex']==0){ echo "Male";}else{echo "Female";} ?></td>
									<td align="center"><?php echo $emp['religion'] ; ?></td>
									<td align="center"><?php
										$birth_date=convert_to_mysql_date($emp['dob']);
										$age = my_old($birth_date);
										printf("%d years, %d months, %d days\n", $age[year], $age[mnt], $age[day]);
										?></td>
									  <td align="center"><?php echo $division_details[$emp['division_id']]; ?></td>
									  <td align="center"><?php echo $department_details[$emp['department_id']]; ?></td>
									  <td align="center"><?php echo $section_details[$emp['section_id']]; ?></td>
									  <td align="center"><?php echo $subsection_details[$emp['subsection_id']]; ?></td> 
									  <td align="center"><?php echo $row2['route']; ?></td>
									  <td align="center"><?php echo $row2['vehicle_no']; ?></td> 
								</tr>
						   
								<?php $j++;}// end while loop
									?>
								</table>
					</td>
				</tr>
		  <?php
				}//end else
						
			}// end for loop
			echo"</table>";
		}// end serch by month
}// end if condition starters report detail

if($type=="emp_profile")// Start emp Profile if
{
		if($active_data[0]=="") $category="%%"; else $category=$active_data[0];
		if($active_data[1]==0) $company_name="%%"; else $company_name=$active_data[1];
		if($active_data[2]==0) $location_name="%%"; else $location_name=$active_data[2];
		if($active_data[3]==0) $division_name="%%"; else $division_name=$active_data[3];
		if($active_data[4]==0) $department_name="%%"; else $department_name=$active_data[4];
		if($active_data[5]==0) $section_name="%%"; else $section_name=$active_data[5];
		if($active_data[6]==0) $designation_name="%%"; else $designation_name=$active_data[6];
		if($active_data[7]==0) $subsection_name="%%"; else $subsection_name=$active_data[7];
		if($search_string=="") $search_string="%%"; else $search_string=$search_string;
		/*$sql= "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS  name,emp.designation_id,emp.dob,emp.joining_date,emp.sex,
							job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
						FROM hrm_employee AS emp
						LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
						WHERE emp.emp_code like '$search_string' and emp.category like '$category' and emp.designation_id like '$designation_name' and job.company_id like '$company_name' and job.location_id like '$location_name' and job.division_id like '$division_name' and job.department_id like '$department_name' and job.section_id like '$section_name' and job.subsection_id like '$subsection_name' and emp.is_deleted=0 order by emp.designation_level ASC";*/
						$sql= "SELECT *, CONCAT(first_name, ' ', middle_name, ' ', last_name) AS  name
						FROM hrm_employee
						WHERE emp_code like '$search_string' and category like '$category' and designation_id like '$designation_name' and company_id like '$company_name' and location_id like '$location_name' and division_id like '$division_name' and department_id like '$department_name' and section_id like '$section_name' and subsection_id like '$subsection_name' and is_deleted=0 order by designation_level ASC";
						$result=mysql_query($sql);
						
						if(mysql_num_rows($result)==0)
						{
							echo "<p align='center' style='font-size:18px; color:#000'><b>No Data Found Under Selected Search Criteria.</b></p>";
						}
						else
						{
			$i=0;				
		while($emp=mysql_fetch_assoc($result))
	{
		
			$row=return_field_value("location","resource_photo","identifier ='$emp[emp_code]' and type=0 and is_deleted=0");
			$image="";
			if($row=="") $image=""; else $image="<img src='../../../$row' height='160' width='160' />";
?>
<div style="width:900px;" class="page">
	 <div align="center" style="font-size:16px; margin-top:15px"><b>Employee Profile</b></div>
     <div style="float: right; clear: left; width:300px; margin-top:30px"><table border="1"><tr height="160px"><td width="160px"><?php echo $image; ?></td></tr></table></div>
     <div style="width:800px;margin-left:30px;">
	<table  width="500px" cellpadding="2" cellspacing="1" border="0">
        <tr>
            <td width="200">Employee Code</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[emp_code]; ?></td>
       </tr>
       <tr>
            <td width="200">Name</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[name]; ?></td>
       </tr>
       <tr>
            <td width="200">Father's Name</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[father_name]; ?></td>
       </tr>
       <tr>
            <td width="200">Mother's Name</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp[mother_name]; ?></td>
       </tr>
       <?php
		//$sql5="select * from hrm_employee_address where emp_code='$emp[emp_code]' and (address_type=1 || address_type=0) and status_active=1 and is_deleted=0";

$sql5="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=1 and status_active=1 and is_deleted=0";

		 $result5=mysql_query($sql5);
		 if(mysql_num_rows($result5)==0)
		 {?>
	   <tr>
            <td valign="top"  width="200">Present Address </td>
            <td valign="top" width="20">:</td>
            <td width="200">&nbsp;</td>
       </tr>
	   <tr>
            <td valign="top"  width="200">Permanent Address </td>
            <td valign="top" width="20">:</td>
            <td width="200">&nbsp;</td>
       </tr> 
	<?php }
	else
	{
		while($row5=mysql_fetch_assoc($result5))
		{
			if($row5['house_no']=="") $house_no=""; else $house_no="House#".$row5['house_no'].", ";
			if($row5['road_no']=="") $road_no=""; else $road_no="Road#".$row5['road_no'].", <br />";
			if($row5['village']=="") $village=""; else $village=$row5['village'].", <br />";
			if($row5['post_code']=="") $post_code=""; else $post_code=$row5['post_code'].", ";
			if($row5['thana']=="") $thana=""; else $thana=$row5['thana'].", ";
			
			 if($row5['address_type']==1)
			 {
			 ?>
	   <tr>
            <td valign="top"  width="200">Present Address </td>
            <td valign="top" width="20">:</td>
            <td width="200"><?php echo $house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row5['district_id']]; ?>
            </td>
       </tr>
	   <?php }
			 else if($row5['address_type']==0)
			 {?>
	   <tr>
            <td valign="top"  width="200">Permanent Address </td>
            <td valign="top" width="20">:</td>
            <td width="200"><?php echo $house_no.$road_no.$village.$post_code.$thana.$lib_list_district[$row5['district_id']]; ?>
            </td>
       </tr> 
			<?php }
		}}?>
        <tr>
            <td width="200">Date of Birth</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['dob']; ?></td>
       </tr>
        <tr>
            <td width="200">Marital Status</td>
            <td width="20">:</td>
            <td width="200"><?php echo $marital_status[$emp['marital_status']]; ?></td>
       </tr>
        <tr>
            <td width="200">Nationality</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['nationality']; ?></td>
       </tr>
        <tr>
            <td width="200">Religion</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['religion']; ?></td>
       </tr>
       <tr>
            <td width="200">Education</td>
            <td width="20">:</td>
            <td width="200">&nbsp;</td>
       </tr>
   </table>
   <br />
   <table class="display" border="1"  width="80%">
        <tr>
            <td align="center">Exam Name</td>
            <td align="center">Institute Name</td>
            <td align="center">Passing Year</td>
            <td align="center">Board/University</td>
            <td align="center">Group</td>
            <td align="center">Division/CGPA</td>
        </tr>
        <?php 
			$sql2="select * from hrm_employee_education where emp_code='$emp[emp_code]' and is_deleted=0 order by passing_year ASC";
			$result2=mysql_query($sql2);
			while($row2=mysql_fetch_assoc($result2))
			{
		?>
        <tr>
            <td><?php echo $row2['exam_name']; ?></td>
            <td><?php echo $row2['institution']; ?></td>
            <td align="right"><?php echo $row2['passing_year']; ?></td>
            <td align="center"><?php echo $row2['board']; ?></td>
            <td align="center"><?php echo $row2['discipline']; ?></td>
            <td align="center"><?php echo $row2['result']; ?></td>
        </tr><?php } ?>
    </table>
    <br />
    <table width="500px" border="0">
    	<tr>
        	<td width="200">Employment History</td>
            <td width="20">:</td>
            <td width="200">&nbsp;</td>
        </tr>
    </table>
     <table class="display" border="1" width="600px">
        <tr>
            <td align="center">Company Name</td>
            <td align="center">Designation</td>
            <td align="center">Start Date</td>
            <td align="center">End Date</td>
        </tr>
         <?php 
			$sql3="select * from hrm_employee_experience where emp_code='$emp[emp_code]' and is_deleted=0";
			$result3=mysql_query($sql3);
			while($row3=mysql_fetch_assoc($result3))
			{
		?>
        <tr>
            <td><?php echo $row3['organization_name']; ?></td>
            <td><?php echo $row3['designation']; ?></td>
            <td align="center"><?php echo $row3['joining_date']; ?></td>
            <td align="center"><?php echo $row3['resigning_date']; ?></td>
        </tr><?php }?>
    </table>
    <br />
   <table  width="500px" cellpadding="2" cellspacing="1" border="0">
        <tr>
            <td width="200">Blood Group</td>
            <td width="20">:</td>
            <td width="200"><?php echo $blood_group[$emp['blood_group']]; ?></td>
       </tr>
       <tr>
            <td width="200">Department</td>
            <td width="20">:</td>
            <td width="200"><?php echo $department_details[$emp['department_id']]; ?></td>
       </tr>
       <tr>
            <td width="200">Designation</td>
            <td width="20">:</td>
            <td width="200"><?php echo $designation_chart[$emp['designation_id']]; ?></td>
       </tr>
       <tr>
            <td width="200">Joining Date</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['joining_date']; ?></td>
       </tr>
       <tr>
            <td width="200">Confirmation Date</td>
            <td width="20">:</td>
            <td width="200"><?php echo $emp['confirmation_date']; ?></td>
       </tr>
       <?php 
	   		if($emp[sex]==0) $str_con=" and relation=2"; else $str_con=" and relation=1";
			
	   		$sql8="select * from hrm_employee_family where emp_code='$emp[emp_code]' and status_active=1 and is_deleted=0 $str_con";
			$result8=mysql_query($sql8);
			$row8=mysql_fetch_assoc($result8);
		?>
            <tr>
                <td width="200">Spouse Name</td>
                <td width="20">:</td>
                <td width="200"><?php echo $row8[name]; ?></td>
           </tr>
           <tr>
                <td width="200">Spouse Occupation</td>
                <td width="20">:</td>
                <td width="200"><?php echo $row8[occupation]; ?></td>
           </tr>
            <tr>
                <td width="200">Number Of Children</td>
                <td width="20">:</td>
                <td width="200"><?php $num_child=return_field_value("count(*)","hrm_employee_family","emp_code ='$emp[emp_code]' and (relation=3 || relation=4) and status_active=1 and is_deleted=0"); echo $num_child; ?></td>
           </tr>
       <?php 
	   		$sql7="select * from hrm_employee_address where emp_code='$emp[emp_code]' and address_type=1 and status_active=1 and is_deleted=0";
			$result7=mysql_query($sql7);
			$row7=mysql_fetch_assoc($result7);
		?>
        <tr>
            <td width="200">Phone No.</td>
            <td width="20">:</td>
            <td width="200"><?php echo $row7['phone_no']; ?></td>
       </tr>
        <tr>
            <td width="200">Cell No.</td>
            <td width="20">:</td>
            <td width="200"><?php echo $row7['mobile_no']; ?></td>
       </tr>
       <tr>
            <td width="200">Email</td>
            <td width="20">:</td>
            <td width="200"><?php echo $row7['email']; ?></td>
       </tr>
       
   </table>
   </div>
   <div style="float: right; clear: left; width:400px; margin-top:-50px"><?php if($emp[status_active]==1) echo "<b>Status: Active</b>"; else echo "<b>Status: Inactive</b>";  ?></div>
</div>
<?php $i++;}
}// end else                          
}// end emp Profile if

 
 if($type=="ot_report")//  buyer ot report summmary
{
	//echo $txt_from_date;die;
	//-------------
	$from_date =convert_to_mysql_date($txt_from_date);
	$to_date =convert_to_mysql_date($txt_to_date);
	if($txt_to_date=="")
	{
		$to_date=$from_date;
	}
	$str_date="a.attnd_date between '$from_date' and '$to_date'";
	if($cbo_status==0) $status="";
	else if ($cbo_status==1) $status="and a.status='P '";
	else if ($cbo_status==2) $status="and a.status='A '";
	else if ($cbo_status==3) $status="and a.status='D '";
	else if ($cbo_status==4) $status="and a.status in ('CL','ML','SL','EL','SpL','LWP')";
	else if ($cbo_status==5) $status="and a.status='MR'";
		
	if ($cbo_company_id==0) $cbo_company_id=""; else $cbo_company_id="and a.company_id='$cbo_company_id'";
	if ($location_id==0) $location_id=""; else $location_id="and a.location_id='$location_id'";
	if ($section_id==0) $section_id=""; else $section_id="and a.section_id='$section_id'";
	if ($subsection_id==0) $subsection_id=""; else $subsection_id="and a.subsection_id='$subsection_id'";
	if ($designation_id==0) $designation_id=""; else $designation_id="and a.designation_id='$designation_id'";
	if ($cbo_emp_category=='') $category =""; else $category ="and emp.category='$cbo_emp_category'";
	//if ($department_id==0) $department_id=""; else  $department_id="and a.department_id='$department_id'";
	
	if ($department_id==0) 
		{
			$department_id="";			
		}
	else
		{
			$department_id="where id='$department_id'";				
		}

	
	$sql_d = "SELECT id, department_name  FROM lib_department $department_id order by id";
	
	$result_d = mysql_query( $sql_d ) or die( $sql_d . "<br />" . mysql_error() );
	
	$p=0;$a=0;$mr=0;$d=0;$l=0;$total_buyer_ot_amount=0;
	$html = "<table cellpadding=\"0\" cellspacing=\"1\" border=\"1\" class=\"display\">";
	
	while( $row_dp = mysql_fetch_array( $result_d ) ) 
	{		
			$total_ot_hr=0; $total_ot_amnt=0;
			//echo $row_dp['id'];
			//if($flag==1){$group_by_condition="";}
			//else {$group_by_condition="'and a.".$group_by_name=$row_dp[id]."'";};
			
		$sql = "SELECT CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id, emp.id_card_no, emp.punch_card_no, a.* FROM hrm_attendance a, hrm_employee emp, lib_designation des WHERE emp.status_active=1 and a.designation_id=des.id and $str_date and a.emp_code=emp.emp_code $location_id $section_id ".$cbo_company_id." and a.department_id='$row_dp[id]' $status $subsection_id $designation_id $category order by des.level asc";
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		//	echo $sql;			
			$emp_tot=mysql_num_rows($result);
			//echo "asd  $row_dp[id] $emp_tot";die;
			if ($emp_tot>0)
				{
	$html .= "		<tr bgcolor=#F0F0B8>
						<td colspan=14><b>Department Name:</b> $row_dp[department_name]</td>
					</tr>
					<tr>
						<td width=50><b>Emp Code</b></td>
						<td width=50><b>ID Card No</b></td>
						<td width=50><b>Punch Card No</b></td>
						<td width=140><b>Name</b></td>
						<td width=100><b>Designation</b></td>
						<td width=90><b>Department</b></td>
						<td width=120><b>Location</b></td>
						<td width=80><b>Section</b></td>
						<td width=50><b>Sub Section</b></td>
						<td width=40><b>OT Hour</b></td>
						<td width=50><b>OT Rate</b></td>
						<td width=50><b>OT Total</b></td>
					</tr>";
							
			
			while( $row = mysql_fetch_array( $result ) ) {
				
			$location_name=return_field_value("location_name","lib_location","id=$row[location_id]");
			$section_name=return_field_value("section_name","lib_section","id=$row[section_id]");
			$subsection_name=return_field_value("subsection_name","lib_section","id=$row[section_id]");
			$basic_salary = return_field_value("amount","hrm_employee_salary","payroll_head=1 and emp_code=$row[emp_code]");
			$buyer_ot_rate=round(($basic_salary/208)*2,2);
			$buyer_ot_hr=get_buyer_ot_hr($row[total_over_time_min],$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit']);
			$html .= "					
					<tr>
						<td></td>						
					</tr>
					<tr class=\"gradeA\">
						<td>$row[emp_code]</td>
						<td>$row[id_card_no]</td>
						<td>$row[punch_card_no]</td>
						<td>$row[name]</td>
						<td>"; $html .=  $designation_chart[$row['designation_id']]['custom_designation']; $html .= "</td>
						<td>". $department_details[$row['department_id']]['department_name']."</td>						
						<td>".$location_name."</td>
						<td>".$section_name."</td>
						<td>".$subsection_name."</td>
						<td align=right>".$buyer_ot_hr."</td>
						<td align=right>".number_format($buyer_ot_rate,2)."</td>
						<td align=right>".$buyer_ot_rate*$buyer_ot_hr."</td>
					</tr>";					$total_ot_hr=$total_ot_hr+$buyer_ot_hr;

					$total_ot_amnt=$total_ot_amnt+($buyer_ot_rate*$buyer_ot_hr);
					//--------------

					//--------------
					if($row["status"]=='P'){$p++;}
					if($row["status"]=='A'){
							$a++;
							if($a>1){
									$param_emp_code .=',';
									}
							$param_emp_code .=$row["emp_code"];
							
						}
					if($row["status"]=='MR')
							{
								$mr++;
								if($mr>1){
									$mr_emp_code .=',';
									}
								$mr_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='D')
							{
								$d++;
								if($d>1){
									$delay_emp_code .=',';
									}
								$delay_emp_code .=$row["emp_code"];
							}
					if($row["status"]=='CL' || $row["status"]=='SL' || $row["status"]=='ML' || $row["status"]=='EL' || $row["status"]=='LWP' || $row["status"]=='SpL')
							{
								$l++;
								if($l>1){
									$leave_emp_code .=',';
									}
								$leave_emp_code .=$row["emp_code"];
							}					
					}
			
			$html .= "					
					<tr>
						<td></td>						
					</tr>
					<tr class=\"gradeA\">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>						
						<td></td>
						<td></td>
						<td>Total</td>
						<td align=right>$total_ot_hr</td>
						<td></td>
						<td align=right>$total_ot_amnt</td>
					</tr>";
			$total_buyer_ot_amount=$total_buyer_ot_amount+$total_ot_amnt;
		}
	}
		$total=$p+$a+$l+$d+$mr;
		
		$html .= "<tr bgcolor=#F0F0B8>
					<td colspan=14 align=\"center\"><b>Summary</b></td>
				  </tr>
				  <tr>				  	
					<td colspan=2></td>
				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Present :</b> $p </td>								  	
					<td  align=\"left\" bgcolor=#FFFFFF><b>Absent :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$param_emp_code&date=$date','Absent Information'); return false\"> $a</a> </td>				  			  	
 				  	<td  align=\"left\" bgcolor=#E5E5E5><b>Leave :</b> <a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$leave_emp_code&date=$date','Leave Information'); return false\">$l</a> </td>				  				   
					<td  align=\"left\" bgcolor=#FFFFFF><b>Movement :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$mr_emp_code&date=$date','Movement Information'); return false\"> $mr </a> </td>				  			    
					<td  align=\"left\" bgcolor=#E5E5E5><b>Late :</b><a href='#' onclick=\"openmypage('popup_punch_report.php?emp_code=$delay_emp_code&date=$date','Late Information'); return false\"> $d </a></td>				  			    
					<td  colspan=2 align=\"left\" bgcolor=#FFFFFF><b>Total :</b> $total </td>
					<td  colspan=4 align=\"right\" bgcolor=#FFFFFF><b>Total Amount:</b> $total_buyer_ot_amount </td>
				  </tr>
				 </table>";
			
	
	//for report temp file delete 
	foreach (glob("tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
	}		
	
	//html to xls convert
	$name=time();
	$name="$name".".xls";	
	$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
	$is_created = fwrite($create_new_excel,$html);
	
	echo "$html"."####"."$name";
	exit();	
}
function add_month($orgDate,$mon){
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,1,date('Y',$cd)));
	  return $retDAY;
	}	
	
function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];
return $m_data ;
}

?>
     
	
