<?php
date_default_timezone_set('UTC');
session_start();

include('../../includes/common.php');

$e_date = time();
$user_only = $_SESSION["user_name"];
extract( $_GET );

//Employee Basic of Employee Information ( Save / Update )
if( $action == "emp_basic" ) {
	
	/*
	$company_sql1= mysql_db_query($DB, "select emp_code,punch_card_no from hrm_employee where emp_code not in (select distinct EmployeeCode from activeemployeefordevice)");
	while ($row_sample=mysql_fetch_array($company_sql1))  // Details Job  table queery ends here
	{
	
		
		for ($d=1; $d<=8;$d++)
		{
			$sql = "insert into activeemployeefordevice (EmployeeCode,PunchCardNo,Status,DevId) values("."'".$row_sample[emp_code]."'".","."'".$row_sample[punch_card_no]."'".",'2','$d')";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
	}
	
	echo "9";
	exit();
	*/
	$unique_fields = array( "national_id", "passport_no", "punch_card_no" );
	$table_name = "hrm_employee";
	
	$confirmation_date = convert_to_mysql_date( $confirmation_date );
	$dob = convert_to_mysql_date( $dob );
	$joining_date = convert_to_mysql_date( $joining_date );
	
	for( $i = 0; $i < count( $unique_fields ); $i++ ) {
		if( $$unique_fields[$i] != '' ) {
			$unique_check = check_uniqueness( $unique_fields[$i], $table_name, $unique_fields[$i] . "='" . $$unique_fields[$i] . "'", $emp_code );
			if( $unique_check == false ) {
				echo "3_$i";
				exit();
			}
		}
	}
	
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$tables = array(
				array(
					"table_name"	=> "hrm_employee",
					"fields"		=> array( "emp_code","first_name","middle_name","last_name","id_card_no","punch_card_no","dob","father_name","mother_name",
											"birth_place","religion","blood_group","marital_status","sex","nationality","national_id","passport_no","designation_id",
											"designation_level","joining_date","confirmation_date","category","functional_superior","admin_superior","remark" )
				),
				array(
					"table_name"	=> "hrm_employee_job",
					"fields"		=> array( "emp_code","company_id","location_id","division_id","department_id","section_id","subsection_id" )
				)
			);
	
	if( $emp_code == '' ) {		//Insert
		$id = return_next_id( "id", $table_name );
		$emp_code = sprintf( "%07d", $id );
		
		foreach( $tables AS $tbl ) {
			
			$sql = "INSERT INTO $tbl[table_name] ( ";
			for( $i = 0; $i < count( $tbl['fields'] ); $i++ ) {
				if( $i > 0 ) $sql .= ", ";
				$sql .= $tbl['fields'][$i];
			}
			$sql .=	" ) VALUES ( ";
			for( $i = 0; $i < count( $tbl['fields'] ); $i++ ) {
				if( $i > 0 ) $sql .= ", ";
				$sql .= "'" . $$tbl['fields'][$i] . "'";
				if ($$tbl['fields'][5]!=0) $punch_card=$$tbl['fields'][5] ;
			}
			$sql .=	" )";
			mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
		}
		for ($d=1; $d<=8;$d++)
		{
			$sql = "insert into activeemployeefordevice (EmployeeCode,PunchCardNo,Status,DevId) values('$emp_code','$txt_punch_card','2','$d')";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
		
		echo "2_" . $emp_code;
	}
	else {						//Update
		foreach( $tables AS $tbl ) {
			$sql = "UPDATE $tbl[table_name] SET ";
			for( $i = 1; $i < count( $tbl['fields'] ); $i++ ) {
				if( $i > 1 ) $sql .= ", ";
				$sql .= $tbl['fields'][$i] . " = '" . $$tbl['fields'][$i] . "'";
				if ($$tbl['fields'][5]!=0) $punch_card=$$tbl['fields'][5] ;
			}
			$sql .= " WHERE emp_code = $emp_code";
			mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
		}
			
			$sql="select * from activeemployeefordevice where EmployeeCode='$emp_code'";
 			$rs=mysql_query($sql);
			if($info=mysql_fetch_row($rs))
			{
				$sql = "UPDATE activeemployeefordevice
					SET Status = 2, punchcardno='$txt_punch_card'
					WHERE EmployeeCode = '" . $emp_code . "'";
				$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
				
			}
			else
			{
				for ($d=1; $d<=8;$d++)
				{
					$sql = "insert into activeemployeefordevice (EmployeeCode,PunchCardNo,Status,DevId) values('$emp_code','$txt_punch_card','2','$d')";
					$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
				}
			}
		
		
		echo "9";
	}
	
	if( file_exists( "../../cache/emp_basic.txt" ) ) {
		$emp_basic = unserialize( file_get_contents( "../../cache/emp_basic.txt" ) );
	}
	else {
		$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
				FROM hrm_employee AS emp
				LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
				WHERE emp.is_deleted = 0 AND emp.status_active = 1";
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$emp_basic = array();
		while( $row = mysql_fetch_assoc( $result ) ) {
			$emp_basic[$row['emp_code']] = $row;
		}
	}
	
	$emp_code = ( $emp_code != '' ) ? $emp_code : $id;
	$emp_basic[$emp_code] = array();
	
	$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,
					job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
			FROM hrm_employee AS emp
			LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
			WHERE emp.emp_code = '$emp_code' AND emp.is_deleted = 0 AND emp.status_active = 1";
	
	$emp_basic[$emp_code] = mysql_fetch_assoc( mysql_query( $sql ) ) or die( $sql . "<br />" . mysql_error() );
	
	file_put_contents( "../../cache/emp_basic.txt", serialize( $emp_basic ) );
	exit();
}
//Employee Address of Employee Information ( Save / Update )
if( $action == "emp_address" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$fields = array( "house_no","road_no","division_id","district_id","thana","village","post_code","phone_no","mobile_no","email","co" );
	
	for( $i = 0; $i < 3; $i++ ) {
		if( $i == 0 ) $type = "present";
		else if( $i == 1 ) $type = "permanent";
		else if( $i == 2 ) $type = "alternate";
		
		$sql = "SELECT * FROM hrm_employee_address WHERE emp_code = '$emp_code' AND address_type = $i";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) == 0 ) {	//Insert
			$sql = "INSERT INTO hrm_employee_address ( emp_code, address_type";
			for( $j = 0; $j < count( $fields ); $j++ ) $sql .= ", " . $fields[$j];
			$sql .=	" ) VALUES ( '$emp_code', $i";
			for( $j = 0; $j < count( $fields ); $j++ ) {
				$var = $type . "_" . $fields[$j];
				$sql .= ", '" . $$var . "'";
			}
			$sql .=	" )";
			mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
		else {									//Update
			$sql = "UPDATE hrm_employee_address SET ";
			for( $j = 0; $j < count( $fields ); $j++ ) {
				if( $j > 0 ) $sql .= ", ";
				$var = $type . "_" . $fields[$j];
				$sql .= $fields[$j] . " = '" . $$var . "'";
			}
			$sql .= " WHERE emp_code = '$emp_code' AND address_type = $i";
			mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
	}
	echo "2";
	exit();
}
//Employee Salary of Employee Information ( Save / Update )
if( $action == "emp_salary" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$sql = "UPDATE hrm_employee
			SET
				salary_grade = $salary_grade,
				salary_rule = $salary_rule,
				gross_salary = $gross_salary
			WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$sql = "DELETE FROM hrm_employee_salary WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$sql = "INSERT INTO hrm_employee_salary (
					emp_code,
					payroll_head,
					type,
					percentage_formula,
					base_head,
					amount
				) VALUES";
	for( $i = 1; $i <= $head_counter; $i++ ) {
		$var_name = "head" . $i;
		$head_details = explode( "_", $$var_name );
		if( $i > 1 ) $sql .= ", ";
		$sql .= "(
					'$emp_code',
					$head_details[0],
					'$head_details[1]',
					'$head_details[2]',
					$head_details[3],
					$head_details[4]
				)";
	}
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	echo "2";
	exit();
}
//Employee Bank Info of Employee Information ( Save / Update )
if( $action == "emp_salary_bank" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$fields = array( "emp_code","bank_id","branch_name","account_no","tin_no" );
	
	$sql = "SELECT * FROM hrm_employee_salary_bank WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	if( mysql_num_rows( $result ) == 0 ) {	//Insert
		$response = 1;
		$sql = "INSERT INTO hrm_employee_salary_bank ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . $$fields[$i] . "'";
		}
		$sql .= " )";
	}
	else {									//Update
		$response = 2;
		$sql = "UPDATE hrm_employee_salary_bank SET ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . $$fields[$i] . "'";
		}
		$sql .= " WHERE emp_code = '$emp_code'";
	}
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	echo $response;
	exit();
}
//Employee Entitlement of Employee Information ( Save / Update )
if( $action == "emp_entitlement" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$sql = "SELECT * FROM hrm_employee_entitlement WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	if( mysql_num_rows( $result ) == 0 ) {	//Insert
		$response = 1;
		$sql = "INSERT INTO hrm_employee_entitlement (
					emp_code,
					ot_entitled,
					holiday_allowance_entitled,
					salary_type,
					pf_entitled,
					pf_effective_date,
					gi_entitled,
					gi_effective_date
				) VALUES (
					'$emp_code',
					'$ot_entitled',
					'$holiday_allowance_entitled',
					'$salary_type',
					" . ( isset( $pf_entitled ) ? $pf_entitled : 0 ) . ",
					'" . ( isset( $pf_effective_date ) ? $pf_effective_date : 0 ) . "',
					" . ( isset( $gi_entitled ) ? $gi_entitled : 0 ) . ",
					'" . ( isset( $gi_effective_date ) ? $gi_effective_date : 0 ) . "'
				)";
	}
	else {									//Update
		$response = 2;
		$sql = "UPDATE hrm_employee_entitlement
				SET
					ot_entitled = '$ot_entitled',
					holiday_allowance_entitled = '$holiday_allowance_entitled',
					salary_type = '$salary_type',
					pf_entitled = " . ( isset( $pf_entitled ) ? $pf_entitled : 0 ) . ",
					pf_effective_date = '" . ( isset( $pf_effective_date ) ? $pf_effective_date : 0 ) . "',
					gi_entitled = " . ( isset( $gi_entitled ) ? $gi_entitled : 0 ) . ",
					gi_effective_date = '" . ( isset( $gi_effective_date ) ? $gi_effective_date : 0 ) . "'
				WHERE
					emp_code = '$emp_code'";
	}
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$sql = "DELETE FROM hrm_employee_nominee WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	if( isset( $pf_entitled ) ) {
		for( $i = 0; $i < $pf_nominee_counter; $i++ ) {
			$nominee = "pf_nominee_" . $i;
			$data = explode( "_", $$nominee );
			$sql = "INSERT INTO hrm_employee_nominee (
						emp_code,
						name,
						relation,
						ratio,
						type
					) VALUES (
						'$emp_code',
						'$data[0]',
						'$data[1]',
						$data[2],
						0
					)";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
	}
	if( isset( $gi_entitled ) ) {
		for( $i = 0; $i < $gi_nominee_counter; $i++ ) {
			$nominee = "gi_nominee_" . $i;
			$data = explode( "_", $$nominee );
			$sql = "INSERT INTO hrm_employee_nominee (
						emp_code,
						name,
						relation,
						ratio,
						type
					) VALUES (
						'$emp_code',
						'$data[0]',
						'$data[1]',
						$data[2],
						1
					)";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
	}
	
	echo $response;
	exit();
}
//Employee Experience of Employee Information ( Save / Update )
if( $action == "emp_experience" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$sql = "DELETE FROM hrm_employee_experience WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	for( $i = 0; $i < $counter; $i++ ) {
		$organization_name	= "organization_name_$i";	$organization_name	= $$organization_name;
		$designation		= "designation_$i";			$designation		= $$designation;
		$joining_date		= "joining_date_$i";		$joining_date		= $$joining_date;		$joining_date = convert_to_mysql_date( $joining_date );
		$resigning_date		= "resigning_date_$i";		$resigning_date		= $$resigning_date;		$resigning_date = convert_to_mysql_date( $resigning_date );
		$service_length		= "service_length_$i";		$service_length		= $$service_length;
		$gross_salary		= "gross_salary_$i";		$gross_salary		= $$gross_salary;
		
		$sql = "INSERT INTO hrm_employee_experience (
					emp_code,
					organization_name,
					designation,
					joining_date,
					resigning_date,
					sevice_length,
					gross_salary
				) VALUES (
					'$emp_code',
					'" . mysql_real_escape_string( $organization_name ) . "',
					'" . mysql_real_escape_string( $designation ) . "',
					'$joining_date',
					'$resigning_date',
					'$service_length',
					'$gross_salary'
				)";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	}
	
	echo 1;
	exit();
}
//Employee Education of Employee Information ( Save / Update )
if( $action == "emp_education" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$sql = "DELETE FROM hrm_employee_education WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	for( $i = 0; $i < $counter; $i++ ) {
		$exam_name		= "exam_name_$i";		$exam_name		= $$exam_name;
		$board			= "board_$i";			$board			= $$board;
		$institution	= "institution_$i";		$institution	= $$institution;
		$discipline		= "discipline_$i";		$discipline		= $$discipline;
		$major_subject	= "major_subject_$i";	$major_subject	= $$major_subject;
		$passing_year	= "passing_year_$i";	$passing_year	= $$passing_year;
		$result			= "result_$i";			$result			= $$result;
		
		$sql = "INSERT INTO hrm_employee_education (
					emp_code,
					exam_name,
					board,
					institution,
					discipline,
					major_subject,
					passing_year,
					result
				) VALUES (
					'$emp_code',
					'" . mysql_real_escape_string( $exam_name ) . "',
					'" . mysql_real_escape_string( $board ) . "',
					'" . mysql_real_escape_string( $institution ) . "',
					'" . mysql_real_escape_string( $discipline ) . "',
					'" . mysql_real_escape_string( $major_subject ) . "',
					'" . mysql_real_escape_string( $passing_year ) . "',
					'" . mysql_real_escape_string( $result ) . "'
				)";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	}
	
	echo 1;
	exit();
}
//Employee Family of Employee Information ( Save / Update )
if( $action == "emp_family" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$sql = "DELETE FROM hrm_employee_family WHERE emp_code = '$emp_code'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	for( $i = 0; $i < $counter; $i++ ) {
		$name		= "name_$i";		$name			= $$name;
		$relation	= "relation_$i";	$relation		= $$relation;
		$dob		= "dob_$i";			$dob			= $$dob;		$dob = convert_to_mysql_date( $dob );
		$occupation	= "occupation_$i";	$occupation		= $$occupation;
		$contact_no	= "contact_no_$i";	$contact_no		= $$contact_no;
		
		$sql = "INSERT INTO hrm_employee_family (
					emp_code,
					name,
					relation,
					dob,
					occupation,
					contact_no
				) VALUES (
					'$emp_code',
					'" . mysql_real_escape_string( $name ) . "',
					'" . mysql_real_escape_string( $relation ) . "',
					'$dob',
					'" . mysql_real_escape_string( $occupation ) . "',
					'" . mysql_real_escape_string( $contact_no ) . "'
				)";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	}
	
	echo 1;
	exit();
}
//Employee Job Separation ( Save / Update )
if( $action == "job_separation" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$employees = explode( '_', $chk_values );
	
	for( $i = 0; $i < count( $employees ); $i++ ) {
		$sql = "UPDATE hrm_employee SET status_active = 0 WHERE emp_code = '" . $employees[$i] . "'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$sql = "INSERT INTO hrm_separation (
					emp_code,
					separated_from,
					separation_type,
					cause_of_separation
				) VALUES (
					'" . $employees[$i] . "',
					'" . convert_to_mysql_date( $separated_from ) . "',
					'" . $separation_type . "',
					'" . $cause_of_separation . "'
				)";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( file_exists( "../../cache/emp_basic.txt" ) ) {
			$emp_basic = unserialize( file_get_contents( "../../cache/emp_basic.txt" ) );
		}
		else {
			$sql = "SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
					FROM hrm_employee AS emp
					LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
					WHERE emp.is_deleted = 0 AND emp.status_active = 1";
			
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$emp_basic = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$emp_basic[$row['emp_code']] = $row;
			}
			file_put_contents( "../../cache/emp_basic.txt", serialize( $emp_basic ) );
		}
		$months = array(
			array( 1, 'January' ),
			array( 2, 'February' ),
			array( 3, 'March' ),
			array( 4, 'April' ),
			array( 5, 'May' ),
			array( 6, 'June' ),
			array( 7, 'July' ),
			array( 8, 'August' ),
			array( 9, 'September' ),
			array( 10, 'October' ),
			array( 11, 'November' ),
			array( 12, 'December' )
		);
		$sql = "SELECT * FROM hrm_separation_history
				WHERE
					year				= " . intval( substr( convert_to_mysql_date( $separated_from ), 0, 4 ) ) . "
					AND company_id		= '" . $emp_basic[$employees[$i]]['company_id'] . "'
					AND location_id		= '" . $emp_basic[$employees[$i]]['location_id'] . "'
					AND division_id		= '" . $emp_basic[$employees[$i]]['division_id'] . "'
					AND department_id	= '" . $emp_basic[$employees[$i]]['department_id'] . "'
					AND section_id		= '" . $emp_basic[$employees[$i]]['section_id'] . "'
					AND subsection_id	= '" . $emp_basic[$employees[$i]]['subsection_id'] . "'";
		
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			$sql = "UPDATE hrm_separation_history SET ";
			
			for( $j = 0; $j < 12; $j++ ) {
				if( intval( substr( convert_to_mysql_date( $separated_from ), 5, 2 ) ) == $months[$j][0] ) {
					$sql .= $months[$j][1] . " = " . $months[$j][1] . " + 1";
					break;
				}
			}
			$sql .= " WHERE
						year				= " . intval( substr( convert_to_mysql_date( $separated_from ), 0, 4 ) ) . "
						AND company_id		= " . $emp_basic[$employees[$i]]['company_id'] . "
						AND location_id		= " . $emp_basic[$employees[$i]]['location_id'] . "
						AND division_id		= " . $emp_basic[$employees[$i]]['division_id'] . "
						AND department_id	= " . $emp_basic[$employees[$i]]['department_id'] . "
						AND section_id		= " . $emp_basic[$employees[$i]]['section_id'] . "
						AND subsection_id	= " . $emp_basic[$employees[$i]]['subsection_id'];
		}
		else {
			$sql = "INSERT INTO hrm_separation_history (
						group_id,
						company_id,
						location_id,
						division_id,
						department_id,
						section_id,
						subsection_id,
						year,";
			
			for( $j = 0; $j < 12; $j++ ) {
				if( intval( substr( convert_to_mysql_date( $separated_from ), 5, 2 ) ) == $months[$j][0] ) {
					$sql .= $months[$j][1];
					break;
				}
			}
			$sql .= ") values (
						'0',
						" . $emp_basic[$employees[$i]]['company_id'] . ",
						" . $emp_basic[$employees[$i]]['location_id'] . ",
						" . $emp_basic[$employees[$i]]['division_id'] . ",
						" . $emp_basic[$employees[$i]]['department_id'] . ",
						" . $emp_basic[$employees[$i]]['section_id'] . ",
						" . $emp_basic[$employees[$i]]['subsection_id'] . ",
						" . intval( substr( convert_to_mysql_date( $separated_from ), 0, 4 ) ) . ",
						1
					)";
		}
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$sql = "UPDATE activeemployeefordevice
				SET Status = 3
				WHERE EmployeeCode = '" . $employees[$i] . "'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	}
	
	if( file_exists( "../../cache/emp_basic.txt" ) ) unlink( "../../cache/emp_basic.txt" );
	echo "1_" . count( $employees );
	exit();
}
//Employee Job Reactivation ( Save / Update )
if( $action == "job_reactivation" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$employees = explode( '_', $chk_values );
	
	for( $i = 0; $i < count( $employees ); $i++ ) {
		$codes = explode( '-', $employees[$i] );
		
		$sql = "UPDATE hrm_employee
				SET
					status_active = 1,
					service_benifit_from = $service_benefit_from
				WHERE
					emp_code = '" . $codes[0] . "'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$sql = "UPDATE hrm_separation SET rejoin_date = '" . convert_to_mysql_date( $rejoin_date ) . "' WHERE id = '" . $codes[1] . "'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		$sql = "UPDATE activeemployeefordevice
				SET Status = 1
				WHERE EmployeeCode = '" . $codes[0] . "'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	}
	
	if( file_exists( "../../cache/emp_basic.txt" ) ) unlink( "../../cache/emp_basic.txt" );
	echo "1_" . count( $employees );
	exit();
}
//Policy Tagging ( Save / Update )
if( $action == "policy_tagging" ) {
	//location_details
	$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 AND status_active = 1 ORDER BY location_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$location_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$location_details[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
	
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$employees = explode( '_', $employees );
	$policy = explode( '|', $policy );
	$policies = array();
	$leave_policy = 0;
	for( $i = 0; $i < count( $policy ); $i++ ) {
		$policy_parts = explode( '_', $policy[$i] );
		if( $policy_parts[0] == 'leave' ) $leave_policy = $policy_parts[1];
		$policies[] = array( $policy_parts[0], $policy_parts[1] );
	}
	$unsaved_employees = array();
	$msg = '';
	
	for( $i = 0; $i < count( $employees ); $i++ ) {
		$emp_code = $employees[$i];
		$query = true;
		
		$sql = "SELECT id FROM hrm_policy_tagging WHERE emp_code = '$emp_code'";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( mysql_num_rows( $result ) > 0 ) {
			$sql = "UPDATE hrm_policy_tagging SET ";
			for( $j = 0; $j < count( $policies ); $j++ ) {
				$leave = true;
				if( $policies[$j][0] == 'leave' ) {
					$sql2 = "SELECT id FROM hrm_leave_transaction WHERE emp_code = '$emp_code' AND leave_policy_id = " . $policies[$j][1];
					$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
					
					if( mysql_num_rows( $result2 ) > 0 ) {
						$unsaved_employees[] = $emp_code;
						$msg = 'Leave policy cannot be re-attached because leave already taken using existing policy.';
						$leave = false;
						if( count( $policies ) == 1 ) $query = false;
					}
				}
				if( $leave == true ) {
					if( $j > 0 ) $sql .= ", ";
					$sql .= $policies[$j][0] . "_policy = " . $policies[$j][1];
				}
			}
			$sql .= " WHERE emp_code = '$emp_code'";
		}
		else {
			$sql = "INSERT INTO hrm_policy_tagging ( emp_code";
			for( $j = 0; $j < count( $policies ); $j++ ) {
				$sql .= ", " . $policies[$j][0] . "_policy";
			}
			$sql .= " ) VALUES ( '$emp_code'";
			for( $j = 0; $j < count( $policies ); $j++ ) {
				$sql .= ", " . $policies[$j][1];
			}
			$sql .= " )";
		}
		if( $query == true ) $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		
		if( $leave_policy != 0 ) {
			$sql = "SELECT location_id FROM hrm_employee_job WHERE emp_code = '$emp_code'";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$row = mysql_fetch_assoc( $result );
			$leave_year = $location_details[$row['location_id']]['leave_year'];
			
			$sql = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $leave_policy";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$policy_definition = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$policy_definition[$row['leave_type']] = $row;
			}
			
			foreach( $policy_definition AS $leave_type => $definition ) {
				$sql2 = "SELECT * FROM hrm_leave_balance WHERE emp_code = '$emp_code' AND leave_year = $leave_year AND leave_type = '$row[leave_type]'";
				$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
				
				if( mysql_num_rows( $result2 ) > 0 ) {
					$sql3 = "UPDATE hrm_leave_balance
							SET
								leave_year		= $leave_year,
								year_start		= " . date("Y") . ",
								emp_code		= '$emp_code',
								leave_policy	= $leave_policy,
								leave_type		= '$leave_type',
								leave_limit		= $definition[max_limit],
								balance			= $definition[max_limit] - leave_availed
							WHERE
								emp_code = '$emp_code'
								AND leave_year = $leave_year
								AND leave_type = '$leave_type'";
					$result3 = mysql_query( $sql3 ) or die( $sql3 . "<br />" . mysql_error() );
				}
				else {
					$sql3 = "INSERT INTO hrm_leave_balance (
								leave_year,
								year_start,
								emp_code,
								leave_policy,
								leave_type,
								leave_limit,
								leave_availed,
								balance
							) VALUES (
								$leave_year,
								" . date("Y") . ",
								'$emp_code',
								$leave_policy,
								'$leave_type',
								$definition[max_limit],
								0,
								$definition[max_limit]
							)";
					$result3 = mysql_query( $sql3 ) or die( $sql3 . "<br />" . mysql_error() );
				}
			}
		}
	}
	
	if( count( $unsaved_employees ) == 0 ) echo "1_" . count( $policy ) . "_" . count( $employees );
	else echo "2_" . implode( $unsaved_employees ) . "_" . $msg;
	exit();
}

//disciplinary_information and Update Here
if ($action=="disciplinary_information") {
   if( $isupdate == "" ) {	//Insert Here
		$id_field_name = "id";
		$table_name = "hrm_disciplinary_info_mst";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	

		$sql =  "INSERT INTO hrm_disciplinary_info_mst (
							id,
							emp_code,
							occurrence_date,
							action_date,
							occurrence_details,
							investigating_members,
							investigation,
							action_taken,
							withdrawn_date,
							commitee_form_date,
							reporting_date
						) 
										
						VALUES (
						     '$id',
							'$txt_emp_code',
							'$txt_occurrence_date',
							'$txt_action_date',
							'$txt_occurrence_details',
							'$txt_investigating_members',
							'$txt_investigation',
							'$cbo_action_taken',
							'$txt_withdawn_date',
							'$txt_comm_form_date',
							'$txt_report_date'
							)"; 
								
		mysql_query( $sql ) or die (mysql_error());	
		
		$cbo_impact_salary_head = explode(",", $cbo_impact_salary_head);
		$txt_impact_salary_head_perc = explode(",", $txt_impact_salary_head_perc);
		
		$num=count($txt_impact_salary_head_perc);
		$i=0;

		for ($i=0; $i<=$num; $i++)
		{
			if($txt_impact_salary_head_perc[$i]!="")
			{	
				$id_field_name = "id";
				$table_name = "hrm_disciplinary_info_dtls";
				$ids= return_next_id($id_field_name,$table_name);
				
				$sql =  "INSERT INTO hrm_disciplinary_info_dtls (
							id, disc_mst_id, impact_salary_head, impact_salary_head_perc, is_deleted
						) 	
						VALUES (
							'$ids',
							'$id',
							'$cbo_impact_salary_head[$i]',
							'$txt_impact_salary_head_perc[$i]',
							'0'
							)"; 
						mysql_query( $sql ) or die (mysql_error());	
			}
		}
		echo "2";		
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql =  "update hrm_disciplinary_info_mst set
							occurrence_date='$txt_occurrence_date',
							action_date='$txt_action_date',
							occurrence_details='$txt_occurrence_details',
							investigating_members='$txt_investigating_members',
							investigation='$txt_investigation',
							action_taken='$cbo_action_taken',
							withdrawn_date='$txt_withdawn_date',
							commitee_form_date='$txt_comm_form_date',
							reporting_date='$txt_report_date'
							where id='$isupdate'"; 
								
		mysql_query( $sql ) or die (mysql_error());	
		
		$sql =  "delete from hrm_disciplinary_info_dtls where disc_mst_id ='$isupdate'";
		mysql_query( $sql ) or die (mysql_error());	
		
		$cbo_impact_salary_head = explode(",", $cbo_impact_salary_head);
		$txt_impact_salary_head_perc = explode(",", $txt_impact_salary_head_perc);
		
		$num=count($txt_impact_salary_head_perc);
		$i=0;

		for ($i=0; $i<=$num; $i++)
		{
			if($txt_impact_salary_head_perc[$i]!="")
			{	
				$id_field_name = "id";
				$table_name = "hrm_disciplinary_info_dtls";
				$ids= return_next_id($id_field_name,$table_name);
				
				$sql =  "INSERT INTO hrm_disciplinary_info_dtls (
							id, disc_mst_id, impact_salary_head, impact_salary_head_perc, is_deleted
						) 	
						VALUES (
							'$ids',
							'$isupdate',
							'$cbo_impact_salary_head[$i]',
							'$txt_impact_salary_head_perc[$i]',
							'0'
							)"; 
						mysql_query( $sql ) or die (mysql_error());	
			}
		}	
		
		echo "3";		
		exit();
	}
}
?>