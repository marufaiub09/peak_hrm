<?php
include("../../../includes/common.php");
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$separation_type=array(1=>"Resignation",2=>"Retirement",3=>"Disability",4=>"Death",5=>"Terminated",6=>"Un-noticed");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.1.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	function populate() {
			if(document.getElementById('to_date').value!="" && document.getElementById('from_date').value=="")
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
		else
		{
		$('#messagebox').removeClass().addClass('messagebox').text('Report Generating....').fadeIn(1000);
		var div="data_panel2";
		document.getElementById(div).innerHTML = "";
		
		var data=document.getElementById('separation_type').value+"_"+document.getElementById('company_id').value+"_"+document.getElementById('location_id').value+"_"+document.getElementById('division_id').value+"_"+document.getElementById('department_id').value+"_"+document.getElementById('section_id').value+"_"+document.getElementById('designation_id').value+"_"+document.getElementById('from_date').value+"_"+document.getElementById('to_date').value;
		//alert(data);
		if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
		else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
		
		xmlhttp.onreadystatechange = function() {
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				document.getElementById(div).innerHTML = xmlhttp.responseText;
				
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).html('Report has been generated succesfully.....').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
		}
		xmlhttp.open( "GET", "generate_report.php?data=" + data +"&type=separated_emp_list"+"&search_string="+document.getElementById('search_string').value, true );
		xmlhttp.send();
		}
	}

	</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px">
	<div align="center" style="width:100%; height:40px; margin:5px 0;">
		<h2> Separated Employee List</h2>
		<div align="center" id="messagebox"></div>
	</div>
    
    <table style="width:100%;" align="center">
    <tr>
    <td>
		<fieldset id="filter_panel">
			<legend>Search Separeted Employee</legend>
			<table style="width:70%;" border="0" cellpadding="0" cellspacing="2">
				<tr>
                	<th>Select Type</th>
					<th>Select Company</th>
					<th>Select Location</th>
                    <th>Select Division</th>
					<th>Select Department</th>
                    <th>Select Section</th>
					<th>Select Designation</th>
                    <th>From Date</th>
                    <th>To Date</th>
					<th>Employee Code</th>
				</tr>
                <tr>
                	 <td>
						<select name="separation_type" id="separation_type" class="combo_boxes">
							<option value="">-- Select --</option>
                            <?php
							foreach($separation_type as $key => $value)
							{
							?>
							<option value="<?php echo $key;?>"><?php echo $value; ?></option>
							<?php }?>
						</select>
					</td>
                
                    <td id="company">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:130px">
							<option value="0">-- Select --</option>
                            <?php foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
					<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
					<?php } } ?>
							
						</select>
					</td>
                    
                     <td id="location">
						<select name="location_id" id="location_id" class="combo_boxes" style="width:130px">
							<option value="0">-- Select --</option>
							<?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
					<option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                    <td id="division">
						<select name="division_id" id="division_id" class="combo_boxes" style="width:130px">
							<option value="0">-- Select --</option>
							<?php foreach( $division_details AS $division ) { if( $division['status_active'] == 1 ) { ?>
					<option value="<?php echo $division['id']; ?>"><?php echo $division['division_name']; ?></option>
					<?php } } ?>
						</select>
                    </td>
                   <td id="department">
						<select name="department_id" id="department_id" class="combo_boxes" style="width:130px">
                        	<option value="0">-- Select --</option>
							<?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
					<option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    
                     <td id="section">
						<select name="section_id" id="section_id" class="combo_boxes" style="width:130px">
                        <option value="0">-- Select --</option>
							<?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
					<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
					<?php } } ?>
						</select>
					</td>

                    
                     <td id="designation">
						<select name="designation_id" id="designation_id" class="combo_boxes" style="width:130px" >
							<option value="0">-- Select --</option>
							<?php foreach( $designation_chart AS $designation ) { if( $designation['status_active'] == 1 ) { ?>
					<option value="<?php echo $designation['id']; ?>"><?php echo $designation['custom_designation']; ?></option>
					<?php } } ?>
						</select>
					</td>
                    <td><input type="text" name="from_date" id="from_date" value="" class="text_boxes" style="width:100px"/>
                    							<script type="text/javascript">
                                                    $( "#from_date" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               </script>
                    </td>
                    <td><input type="text" name="to_date" id="to_date" value="" class="text_boxes" style="width:100px"/>
                   								 <script type="text/javascript">
                                                    $( "#to_date" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               	</script>
                    </td>
                    <td><input type="text" id="search_string" name="search_string" class="text_boxes" style="width:100px" /></td>
                    <td><input type="submit" name="rpo_search" id="rpo_search" value="Search" class="formbutton" onClick="populate();" /></td>
                </tr>
			</table>
		</fieldset>
        </td>
    	</tr>
        </table>
</div>
	<div id="data_panel2" align="left" class="demo_jui" style="margin-top:10px;"></div>
</body>
</html>