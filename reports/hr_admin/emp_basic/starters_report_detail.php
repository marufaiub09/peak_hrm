<?php
include("../../../includes/common.php");
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.1.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	function populate() {
			
			if(document.getElementById('company_id').value==0)
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#company_id').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			else if(document.getElementById('cost_center').value==0)
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cost_center').focus();
			$(this).html('Please Select Cost Center').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			else if(document.getElementById('from_date').value=="" && document.getElementById('to_date').value=="" && document.getElementById('from_month').value=="" && document.getElementById('to_month').value=="")
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#from_date').focus();
			$(this).html('Please Select At Least One Date Range').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			else if(document.getElementById('from_date').value=="" && document.getElementById('to_date').value!="")
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#from_date').focus();
			$(this).html('Please Select From Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			else if(document.getElementById('to_date').value=="" && document.getElementById('from_date').value!="")
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#to_date').focus();
			$(this).html('Please Select To Date').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			else if(document.getElementById('from_month').value=="" && document.getElementById('to_month').value!="")
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#from_month').focus();
			$(this).html('Please Select From Month').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
			else if(document.getElementById('to_month').value=="" && document.getElementById('from_month').value!="")
			{
					$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#to_month').focus();
			$(this).html('Please Select To Month').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
			}
		else
		{
		$('#messagebox').removeClass().addClass('messagebox').text('Report Generating....').fadeIn(1000);
		var div="data_panel5";
		document.getElementById(div).innerHTML = "";
		
		var data=document.getElementById('company_id').value+"_"+document.getElementById('cost_center').value+"_"+document.getElementById('from_date').value+"_"+document.getElementById('to_date').value+"_"+document.getElementById('from_month').value+"_"+document.getElementById('to_month').value;
		//alert(data);
		if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
		else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
		
		xmlhttp.onreadystatechange = function() {
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				document.getElementById(div).innerHTML = xmlhttp.responseText;
				
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).html('Report has been generated succesfully.....').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
		}
		xmlhttp.open( "GET", "generate_report.php?data=" + data +"&type=starters_report_detail", true );
		xmlhttp.send();
		}
	}

	</script>
    
	<style type="text/css">
		#filter_panel select { width:100px; }
		#filter_panel * { font-family:verdana; font-size:11px; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px">
	<div align="center" style="width:100%; height:40px; margin:5px 0;">
		<p style="font-size:14px"><b>Starters Report of Employees</b></p>
		<div align="center" id="messagebox"></div>
	</div>
    <fieldset id="filter_panel">
		<legend>Search Starter Employees</legend>
    		<table style="width:80%;" align="center" border="0">
    			<tr>
					<td align="center">Select Company</td>
                    <td align="center">Select Cost Center</td>
                    <td align="center">From Date</td>
                    <td align="center">To Date</td>
                    <td align="center">From Month</td>
                    <td align="center">To Month</td>
                 </tr>
                <tr>
                    <td id="company">
						<select name="company_id" id="company_id" class="combo_boxes" style="width:150px">
							<option value="0">-- Select --</option>
                            <?php foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
					<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
					<?php } } ?>
							
						</select>
					</td>
                    <td><select name="cost_center" id="cost_center" class="combo_boxes" style="width:150px">
                              <option value="0">---Select---</option>
                              <option value="1">by Location</option>
                              <option value="2">by Division</option>
                              <option value="3">by Department</option>
                              <option value="4">by Section</option>
                              <option value="5">by Subsection</option>
                              <option value="6">by Designation</option>
                  		</select></td>
                        
                    <td><input type="text" name="from_date" id="from_date" value="" class="text_boxes" style="width:130px"/>
                   								 <script type="text/javascript">
                                                    $( "#from_date" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               	</script>
                    </td>
                   
                    <td><input type="text" name="to_date" id="to_date" value="" class="text_boxes" style="width:130px"/>
                   								 <script type="text/javascript">
                                                    $( "#to_date" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               	</script>
                    </td>
                     <td><input type="text" name="from_month" id="from_month" value="" class="text_boxes" style="width:130px"/>
                   								 <script type="text/javascript">
                                                    $( "#from_month" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               	</script>
                    </td>
                   
                    <td><input type="text" name="to_month" id="to_month" value="" class="text_boxes" style="width:130px"/>
                   								 <script type="text/javascript">
                                                    $( "#to_month" ).datepicker({
                                                    dateFormat: 'dd-mm-yy',
                                                    changeMonth: true,
                                                    changeYear: true
                                                    });
                                               	</script>
                    </td>
                    <td><input type="submit" name="rpo_search" id="rpo_search" value="Search" class="formbutton" onClick="populate();" /></td>
                </tr>
			</table>
	</fieldset>
        
</div>
	<div id="data_panel5" align="left" style="margin-top:10px;"></div>
</body>
</html>