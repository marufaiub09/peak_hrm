<script type="text/javascript" src="functions.js"></script>
<?php
date_default_timezone_set('UTC');
include('../../includes/common.php');

$search_string=$_GET["q"];

$type=$_GET["type"];
if ($type==1)
{
?>
<div id="cost_info_breakdown">
<fieldset style="width:510px; margin-left:10px">
<form name="frm_fabric_cost_info_breakdown" id="frm_cost_info_breakdown" action="javascript:fnc_fabric_cost_info_breakdown()">
	<table cellspacing="2" cellpadding="0" border="0">
		<tr>
			<td colspan="4" valign="top" align="center">Fabric Cost Component Breakdown</td>
		</tr>
		<tr>
			<td width="">Item</td>
			<td>Cons/Dzn</td>
			<td>Rate</td>
			<td>Amount</td>
		</tr>
		<?	// echo $search_string;
			$company_sql= mysql_db_query($DB, "select * from wo_po_cost_fabric_dtls where job_no_cost_fab_mst='$search_string' and is_deleted =0 order by is_yarn desc");
			$row_num_td=mysql_num_rows($company_sql);
			//echo $row_num_td;
			if ($row_num_td==0)
			{
				$row_id=1;
				$sql="select * from wo_po_yarn_info_details where job_no_mst_yarn ='$search_string'"; 
				$rs=mysql_query($sql);
				while($row=mysql_fetch_array($rs))
				{				
					$d="yarn_count";
					$dd="lib_yarn_count";
					$ddd="id='$row[yarn_count_id]'";
					$yarn_count= return_field_value($d,$dd,$ddd);
					
					$d="yarn_type";
					$dd="lib_yarn_composition";
					$ddd="id='$row[yarn_comp_type1st]'";
					$yarn_type1= return_field_value($d,$dd,$ddd);
					
					$d="yarn_type";
					$dd="lib_yarn_composition";
					$ddd="id='$row[yarn_comp_type2nd]'";
					$yarn_type2= return_field_value($d,$dd,$ddd);
					
					//$row_id++; 
					//exit();
			?>
			<tr>
				<td><? 
				//echo $row_id;
				if ($row['yarn_comp_percent2nd']!="0.00")
				{  
					$v_data= "$yarn_count"." "."$yarn_type1"." ".$row['yarn_comp_percent']." "."%"." "."$yarn_type2"." ". $row['yarn_comp_percent2nd'] ." %";
				}
				else
				{
					$v_data= "$yarn_count"." "."$yarn_type1"." ".$row['yarn_comp_percent']." "."%";
				}
				echo $v_data;
				?>	
					<input type="hidden" name="txt_yarn_id_db<? echo $row_id; ?>" id="txt_yarn_id_db<? echo $row_id; ?>" value="<? echo $v_data; ?>" />				</td>
				<td><input name="txt_fabric_cost<? echo $row_id; ?>" type="text"  id="txt_fabric_cost<? echo $row_id; ?>"  value="<? echo $row['consumption_per_unit']; ?>" class='text_boxes_numeric' onlynumeric="d" onchange="calculate_row_cost(<? echo $row_id; ?>);calculate_main_cost(<? echo $row_id; ?>)"  readonly="readonly" style="width:50px" /></td>
				<td>
					<input name="txt_fabric_cost_rate<? echo $row_id; ?>" type="text"  id="txt_fabric_cost_rate<? echo $row_id; ?>"  value="<? echo str_replace("0.00","",$row['FabricCostRate']); ?>" onkeyup="calculate_row_cost(<? echo $row_id; ?>);" class='text_boxes_numeric'  style="width:50px"  onlyNumeric="d" />			</td>
				<td>
					<input name="txt_fabric_cost_amount<? echo $row_id; ?>" type="text"  id="txt_fabric_cost_amount<? echo $row_id; ?>"  value="<? echo str_replace("0.00","",$row['FabricCostAmount']); $TotalFabricCostAmount+=$row['FabricCostAmount'];  ?>"  readonly="readonly" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<? 
				$row_id=$row_id+1;}
				$row_id=$row_id-1;
			?>
			<input name="txt_dbase_row" type="hidden"  id="txt_dbase_row"  value="<? echo $row_id; ?>"  class='text_boxes_numeric'  readonly="readonly" style="width:50px" onlyNumeric="d"/>
			<input type="hidden" name="tot_dyn_row" id="tot_dyn_row" value="<? echo $row_id; ?>" />
			<tr>
				<td>
					Knitting Charge <input name="txt_dbase_row" type="hidden"  id="txt_dbase_row"  value="<? echo $row_id; ?>"  class='text_boxes_numeric'  readonly="readonly" style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_knitting_charge" type="text"  id="txt_knitting_charge"  value=""  class='text_boxes_numeric'  readonly="readonly" style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_knitting_charge_rate" type="text"  id="txt_knitting_charge_rate"  value=""  onkeyup="calculate_row_cost_sttc('txt_knitting_charge');"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
				<input name="txt_knitting_charge_amount" type="text"  id="txt_knitting_charge_amount"  value="" onchange="CalCulateYarnCostReverse('KnittingCharge','KnittingChargeRate','KnittingChargeAmount');CalCulateFabricCost()"  readonly="readonly" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<tr>
				<td>Dyeing Charge</td>
				<td><input name="txt_dyeing_charge" type="text"  readonly="readonly" id="txt_dyeing_charge"  value=""  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_dyeing_charge_rate" type="text"  id="txt_dyeing_charge_rate"  value=""  onkeyup="calculate_row_cost_sttc('txt_dyeing_charge');" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_dyeing_charge_amount" type="text"  id="txt_dyeing_charge_amount"  value="" onchange="CalCulateYarnCostReverse('DyeingCharge','DyeingChargeRate','DyeingChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly"  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<tr>
				<td>Stantering Charge</td>
				<td>
					<input name="txt_stantering_charge" type="text"  readonly="readonly"  id="txt_stantering_charge"  value="" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/></td>
				<td>
					<input name="txt_stantering_charge_rate" type="text"  id="txt_stantering_charge_rate"  value=""  onkeyup="calculate_row_cost_sttc('txt_stantering_charge');"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_stantering_charge_amount" type="text"  id="txt_stantering_charge_amount"  value="" onchange="CalCulateYarnCostReverse('StanteringCharge','StanteringChargeRate','StanteringChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly" style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<tr>
				<td>Brush Peach Charge</td>
				<td>
					<input name="txt_brush_peach_charge" type="text"  readonly="readonly" id="txt_brush_peach_charge"  value=""   class='text_boxes_numeric'  style="width:50px"  onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_brush_peach_charge_rate" type="text"  id="txt_brush_peach_charge_rate"  value=""  onkeyup="calculate_row_cost_sttc('txt_brush_peach_charge');"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_brush_peach_charge_amount" type="text"  id="txt_brush_peach_charge_amount"  value="" onchange="CalCulateYarnCostReverse('BrushPeachCharge','BrushPeachChargeRate','BrushPeachChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly"  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<tr>
				<td>Washing Charge</td>
				<td>
					<input name="txt_washing_charge" type="text"  readonly="readonly" id="txt_washing_charge"  value="" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_washing_charge_rate" type="text"  id="txt_washing_charge_rate"  value="" onkeyup="calculate_row_cost_sttc('txt_washing_charge');"   class='text_boxes_numeric' style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_washing_charge_amount" type="text"  id="txt_washing_charge_amount"  value="" onchange="CalCulateYarnCostReverse('WashingCharge','WashingChargeRate','WashingChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly" style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<tr>
				<td>Others</td>
				<td>
					<input name="txt_other_fabric_cost" type="text"  onkeyup="calculate_row_cost_sttc('txt_other_fabric_cost');"  id="txt_other_fabric_cost"  value=""  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_other_fabric_cost_rate" type="text"  id="txt_other_fabric_cost_rate"  value=""  onkeyup="calculate_row_cost_sttc('txt_other_fabric_cost');" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_other_fabric_cost_amount" type="text"  id="txt_other_fabric_cost_amount"  value=""  onchange="calculate_row_cost_sttc('txt_other_fabric_cost');" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<tr>
				<td colspan="4" height="10" align="center">			</td>
			</tr>
		<?
		}
		else
		{
			$row_num=$row_num_td-6; 
			if($row_num_td==6)
			{
				$sql="select * from wo_po_yarn_info_details where job_no_mst_yarn ='$search_string'"; 
				$rs=mysql_query($sql);
				$row_id=1;
				while($row=mysql_fetch_array($rs))
				{				
					$d="yarn_count";
					$dd="lib_yarn_count";
					$ddd="id='$row[yarn_count_id]'";
					$yarn_count= return_field_value($d,$dd,$ddd);
					
					$d="yarn_type";
					$dd="lib_yarn_composition";
					$ddd="id='$row[yarn_comp_type1st]'";
					$yarn_type1= return_field_value($d,$dd,$ddd);
					
					$d="yarn_type";
					$dd="lib_yarn_composition";
					$ddd="id='$row[yarn_comp_type2nd]'";
					$yarn_type2= return_field_value($d,$dd,$ddd);
					
					//$row_id++; 
					//exit();
			?>
			<tr>
				<td><? 
			//echo $row['yarn_comp_percent2nd'];
				if ($row['yarn_comp_percent2nd']!="0.00")
				{  
					$v_data= "$yarn_count"." "."$yarn_type1"." ".$row['yarn_comp_percent']." "."%"." "."$yarn_type2"." ". $row['yarn_comp_percent2nd'] ." %";
				}
				else
				{
					$v_data= "$yarn_count"." "."$yarn_type1"." ".$row['yarn_comp_percent']." "."%";
				}
				echo $v_data;
				?>	
					<input type="hidden" name="txt_yarn_id_db<? echo $row_id; ?>" id="txt_yarn_id_db<? echo $row_id; ?>" value="<? echo $v_data; ?>" />				</td>
				<td><input name="txt_fabric_cost<? echo $row_id; ?>" type="text"  id="txt_fabric_cost<? echo $row_id; ?>"  value="<? echo $row['consumption_per_unit']; ?>" class='text_boxes_numeric' onlynumeric="d" onchange="calculate_row_cost(<? echo $row_id; ?>);calculate_main_cost(<? echo $row_id; ?>)"  readonly="readonly" style="width:50px" /></td>
				<td>
					<input name="txt_fabric_cost_rate<? echo $row_id; ?>" type="text"  id="txt_fabric_cost_rate<? echo $row_id; ?>"  value="<? echo str_replace("0.00","",$row['FabricCostRate']); ?>" onkeyup="calculate_row_cost(<? echo $row_id; ?>);" class='text_boxes_numeric'  style="width:50px"  onlyNumeric="d" />			</td>
				<td>
					<input name="txt_fabric_cost_amount<? echo $row_id; ?>" type="text"  id="txt_fabric_cost_amount<? echo $row_id; ?>"  value="<? echo str_replace("0.00","",$row['FabricCostAmount']); $TotalFabricCostAmount+=$row['FabricCostAmount'];  ?>"  readonly="readonly" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<? 
				$row_id=$row_id+1;}
				$row_id=$row_id-1;
			}
			
			$i=1;
			
			//echo $row_num;
			while($row=mysql_fetch_array($company_sql))
			{	
				
			?>
			
			<input type="hidden" name="txt_dbase_row" id="txt_dbase_row" value="<? if ($row_num==0)
			{
				echo $row_id;
			}
			else
			{
			 echo $row_num; } ?>" />
			
			<tr>
				<td><? 	
				//echo "asdad";
				if ($i<=$row_num)
				{
					//echo $i;
					$data=explode("%", $row['item_code_desc']);
					if (strlen($data[1])>7)
					{
						$data_sh= $row['item_code_desc']; 
					}
					else
					{
						$data_sh= "$data[0]"." %";
						//echo "<br>strlen($data[1])";
						
					}
				echo $data_sh;
					?>
					<input type="hidden" name="txt_yarn_id_db<? echo $i; ?>" id="txt_yarn_id_db<? echo $i; ?>" value="<? echo $data_sh; ?>" />				</td>
				<td><input name="txt_fabric_cost<? echo $i; ?>" type="text"  id="txt_fabric_cost<? echo $i; ?>"  value="<? echo $row['item_cost_per_unit']; ?>" class='text_boxes_numeric' onlynumeric="d" onchange="calculate_row_cost(<? echo $i; ?>);calculate_main_cost(<? echo $i; ?>)"  readonly="readonly" style="width:50px" /></td>
				<td>
					<input name="txt_fabric_cost_rate<? echo $i; ?>" type="text"  id="txt_fabric_cost_rate<? echo $i; ?>"  value="<? echo str_replace("0.00","",$row['item_rate']); ?>" onkeyup="calculate_row_cost(<? echo $i; ?>);" class='text_boxes_numeric'  style="width:50px"  onlyNumeric="d" />			</td>
				<td>
					<input name="txt_fabric_cost_amount<? echo $i; ?>" type="text"  id="txt_fabric_cost_amount<? echo $i; ?>"  value="<? echo str_replace("0.00","",$row['item_cost']);  ?>"  readonly="readonly" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<? 
				}
				?>
				<?
				$is=$i-1;
				if ($i>$row_num)
				{ // echo $is;
			?>
			
			 <input type="hidden" name="tot_dyn_row" id="tot_dyn_row" value="<? if ($row_num==0)
			{
				echo $row_id;
			}
			else
			{
			 echo $row_num; } ?>" />
			
			<?
			if ($row['item_code_desc']=="Knitting Charge")
			{
			
			?>
			<tr>
				<td>
					Knitting Charge 			</td>
				<td>
					<input name="txt_knitting_charge" type="text"  id="txt_knitting_charge"  value="<? echo $row['item_cost_per_unit']; ?>"  class='text_boxes_numeric'  readonly="readonly" style="width:50px" />			</td>
				<td>
					<input name="txt_knitting_charge_rate" type="text"  id="txt_knitting_charge_rate"  value="<? echo $row['item_rate']; ?>"  onkeyup="calculate_row_cost_sttc('txt_knitting_charge');"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
				<input name="txt_knitting_charge_amount" type="text"  id="txt_knitting_charge_amount"  value="<? echo $row['item_cost']; ?>" onchange="CalCulateYarnCostReverse('KnittingCharge','KnittingChargeRate','KnittingChargeAmount');CalCulateFabricCost()"  readonly="readonly" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<?
			}
			
			if ($row['item_code_desc']=="Dyeing Charge")
			{
			?>
			<tr>
				<td>Dyeing Charge</td>
				<td><input name="txt_dyeing_charge" type="text"  readonly="readonly" id="txt_dyeing_charge"  value="<? echo $row['item_cost_per_unit']; ?>"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_dyeing_charge_rate" type="text"  id="txt_dyeing_charge_rate"  value="<? echo $row['item_rate']; ?>"  onkeyup="calculate_row_cost_sttc('txt_dyeing_charge');" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_dyeing_charge_amount" type="text"  id="txt_dyeing_charge_amount"  value="<? echo $row['item_cost']; ?>" onchange="CalCulateYarnCostReverse('DyeingCharge','DyeingChargeRate','DyeingChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly"  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<?
			}
			
			if ($row['item_code_desc']=="Stantering Charge")
			{
			?>
			<tr>
				<td>Stantering Charge</td>
				<td>
					<input name="txt_stantering_charge" type="text"  readonly="readonly"  id="txt_stantering_charge"  value="<? echo $row['item_cost_per_unit']; ?>" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/></td>
				<td>
					<input name="txt_stantering_charge_rate" type="text"  id="txt_stantering_charge_rate"  value="<? echo $row['item_rate']; ?>"  onkeyup="calculate_row_cost_sttc('txt_stantering_charge');"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_stantering_charge_amount" type="text"  id="txt_stantering_charge_amount"  value="<? echo $row['item_cost']; ?>" onchange="CalCulateYarnCostReverse('StanteringCharge','StanteringChargeRate','StanteringChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly" style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<?
			}
			
			if ($row['item_code_desc']=="Brush Peach Charge")
			{
			?>
			<tr>
				<td>Brush Peach Charge</td>
				<td>
					<input name="txt_brush_peach_charge" type="text"  readonly="readonly" id="txt_brush_peach_charge"  value="<? echo $row['item_cost_per_unit']; ?>"   class='text_boxes_numeric'  style="width:50px"  onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_brush_peach_charge_rate" type="text"  id="txt_brush_peach_charge_rate"  value="<? echo $row['item_rate']; ?>"  onkeyup="calculate_row_cost_sttc('txt_brush_peach_charge');"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_brush_peach_charge_amount" type="text"  id="txt_brush_peach_charge_amount"  value="<? echo $row['item_cost']; ?>" onchange="CalCulateYarnCostReverse('BrushPeachCharge','BrushPeachChargeRate','BrushPeachChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly"  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<?
			}
			
			if ($row['item_code_desc']=="Washing Charge")
			{
			?>
			<tr>
				<td>Washing Charge</td>
				<td>
					<input name="txt_washing_charge" type="text"  readonly="readonly" id="txt_washing_charge"  value="<? echo $row['item_cost_per_unit']; ?>" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_washing_charge_rate" type="text"  id="txt_washing_charge_rate"  value="<? echo $row['item_rate']; ?>" onkeyup="calculate_row_cost_sttc('txt_washing_charge');"   class='text_boxes_numeric' style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_washing_charge_amount" type="text"  id="txt_washing_charge_amount"  value="<? echo $row['item_cost']; ?>" onchange="CalCulateYarnCostReverse('WashingCharge','WashingChargeRate','WashingChargeAmount');CalCulateFabricCost()"  class='text_boxes_numeric'  readonly="readonly" style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<?
			}
			
			if ($row['item_code_desc']=="Others")
			{
			?>
			<tr>
				<td>Others</td>
				<td>
					<input name="txt_other_fabric_cost" type="text"  onkeyup="calculate_row_cost_sttc('txt_other_fabric_cost');"  id="txt_other_fabric_cost"  value="<? echo $row['item_cost_per_unit']; ?>"  class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_other_fabric_cost_rate" type="text"  id="txt_other_fabric_cost_rate"  value="<? echo $row['item_rate']; ?>"  onkeyup="calculate_row_cost_sttc('txt_other_fabric_cost');" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
				<td>
					<input name="txt_other_fabric_cost_amount" type="text"  id="txt_other_fabric_cost_amount"  value="<? echo $row['item_cost']; ?>"  onchange="calculate_row_cost_sttc('txt_other_fabric_cost');" class='text_boxes_numeric'  style="width:50px" onlyNumeric="d"/>			</td>
			</tr>
			<?
			}
			} $i=$i+1;
			?>
			
		
		<?
			}
		}
		?>
			<tr>
				<td colspan="4" height="10" align="center">			</td>
			</tr>
		<tr>
			<td colspan="4" align="center">
				<input type="submit" name="Submit3" class="formbutton" value="Save" style="width:100px" />	</td>
		</tr>
	</table>
  </form>
</fieldset>
</div>
<? } 
if ($type==2)
{
?>
	<div id="TrimCostTable" align="center">
		<fieldset  style="width:510px; margin-left:10px">
		<form name="frm_trim_breakdown" id="frm_trim_breakdown" action="javascript:fnc_trims_cost_info_breakdown()">
		<table width="510" cellpadding="0" cellspacing="2" border="0" id="trim_info_cost">
			<tr>
				<td colspan="5" align="center">Trim Cost  Component Breakdown</td>
			</tr>
			<tr>
				<td>Item</td>
				<td>UOM</td>
				<td>Cons/Dzn</td>
				<td>Rate</td>
				<td>Amount</td>
			</tr>
			<? 
			$company_sql= mysql_db_query($DB, "select * from  wo_po_cost_trims_dtls where job_no_cost_trim_mst ='$search_string'");
			$num_rows = mysql_num_rows($company_sql);
			if ($num_rows>0)
			{	
				$i=1;
				while($row=mysql_fetch_array($company_sql))
				{
					
			?>
			<tr><? //echo $num_rows; ?>
				<td>
					<select style="width:140px" class="combo_boxes" onchange="set_cost_info_uom(this.value,'13',<? echo $i; ?>)" id="cbo_trim_item_name_cost<? echo $i; ?>" name="cbo_trim_item_name_cost<? echo $i; ?>" >
					<option value="0">-------</option>
                  <?
				  
							$company_sql1= mysql_db_query($DB, "select item_name,id from lib_trim order by item_name");
							while ($r_company=mysql_fetch_array($company_sql1))
							{
							?>
							<option value=<? echo $r_company["id"];
							if ($row["item_code"]==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[item_name]" ?> </option>
							<?
							}
							
						?>
                </select></td>
				<td>
					<div id="trim_uom<? echo $i; ?>">
					<input name="txt_trim_item_uom<? echo $i; ?>" type="text" class='text_boxes'  id="txt_trim_item_uom<? echo $i; ?>"  value="<? echo  $row['item_uom']; ?>" onlyNumeric="d" style="width:50px"/></div>				</td>
				<td>
					<input name="txt_trim_item_consm<? echo $i; ?>" type="text" class='text_boxes'  id="txt_trim_item_consm<? echo $i; ?>"  value="<? echo  $row['consumption_per_unit']; ?>" onlyNumeric="d" style="width:80px"/>				</td>
				<td>
					<input name="txt_trim_item_name_rate<? echo $i; ?>" class='text_boxes_numeric' type="text"  id="txt_trim_item_name_rate<? echo $i; ?>"  value="<? echo  $row['item_rate']; ?>" onkeyup="calculate_trim_cost_row('<? echo $i; ?>',1);" onlyNumeric="d" style="width:80px"/>				</td>
				<td>
					<input name="txt_trim_item_name_amount<? echo $i; ?>" type="text" class='text_boxes_numeric'  id="txt_trim_item_name_amount<? echo $i; ?>"  value="<? echo $row['item_cost']; ?>" onkeyup="calculate_trim_cost_row('<? echo $i; ?>',2)" <? if($i==$num_rows){ echo "onfocus='add_trim_row($i);'";}?> onlyNumeric="d" style="width:100px"/>				</td>
			</tr>
			<?
				$i++;	}
			}
			else
			{
				for($i=1; $i<=4; $i++)
				{
			?>
				<tr>
				<td>
					<select style="width:140px" class="combo_boxes" name="cbo_trim_item_name_cost<? echo $i; ?>" onchange="set_cost_info_uom(this.value,'13',<? echo $i; ?>)" id="cbo_trim_item_name_cost<? echo $i; ?>" >
					<option value="0">-------</option>
						<?
							$company_sql= mysql_db_query($DB, "select item_name,id from lib_trim order by item_name");
							while ($r_company=mysql_fetch_array($company_sql))
							{
							?>
							<option value=<? echo $r_company["id"];
							if ($cbo_item1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[item_name]" ?> </option>
							<?
							}
						?>
					</select>				</td>
				<td><div id="trim_uom<? echo $i; ?>">
					<input name="txt_trim_item_uom<? echo $i; ?>" type="text" class='text_boxes'  id="txt_trim_item_uom<? echo $i; ?>"  value="<? echo  $row['TrimItemNameCon']; ?>" onlyNumeric="d" style="width:50px"/>	</div>			</td>
				<td>
					<input name="txt_trim_item_consm<? echo $i; ?>" type="text" class='text_boxes'  id="txt_trim_item_consm<? echo $i; ?>"  value="<? echo  $row['TrimItemNameCon']; ?>" onlyNumeric="d" style="width:80px"/>				</td>
				<td>
					<input name="txt_trim_item_name_rate<? echo $i; ?>" class='text_boxes_numeric' type="text"  id="txt_trim_item_name_rate<? echo $i; ?>"  value="<? echo  $row['TrimItemNameRate']; ?>" onkeyup="calculate_trim_cost_row('<? echo $i; ?>',1);" onlyNumeric="d" style="width:80px"/>				</td>
				<td><input name="txt_trim_item_name_amount<? echo $i; ?>" type="text" class='text_boxes_numeric'  id="txt_trim_item_name_amount<? echo $i; ?>"  value="" onkeyup="calculate_trim_cost_row('<? echo $i; ?>',2)"  onfocus="add_trim_row(4);" onlynumeric="d" style="width:100px"/></td>
			</tr>
			<?
				}
			}
			?>
		</table>
<table width="510" cellpadding="0" cellspacing="2" border="0">
			
<tr>
<td align="center" height="35" colspan="4" valign="middle">
<input name="TotalTrimItemValue" type="hidden" id="TotalTrimItemValue"  value="<? echo $TotalTrimItemValue; ?>">

<input type="submit" name="Submit3" class="formbutton" value="Save" style="width:150px " /></td>
</tr>
</table>
</form>
</fieldset>
</div>

<?
}
// Finish Trims

if ($type==3)
{
?>

<div align="center">
<fieldset  style="width:510px; margin-left:10px">
<form name="frm_embellishment_breakdown" id="frm_embellishment_breakdown" action="javascript:fnc_embellishment_cost_info_breakdown()">
	<table width="510" cellpadding="0" cellspacing="2" border="0">
		<tr>
			<td colspan="4" align="center">Embellishment Cost Component Breakdown </td>
		</tr>
		<tr>
			<td  align="center">Item</td>
			<td align="center">Cons/Dzn</td>
			<td align="center">Rate</td>
			<td align="center">Amount</td>
		</tr>
		<? 
			$company_sql= mysql_db_query($DB, "select * from  wo_po_cost_embellishment_dtls where job_no_cost_embl_mst ='$search_string' order by id");
			$num_rows = mysql_num_rows($company_sql);
			if ($num_rows>0)
			{
				$i=1;
				while($row=mysql_fetch_array($company_sql))
				{
					
					if ($i==1)
					{
				?>
				
		<tr>
			<td>Printing</td>
			<td>
				<input name="txt_printing_cost" type="text" class='text_boxes'  id="txt_printing_cost" style="width:100px"  value="<? echo $row['item_cons_per_unit']; ?>" onlyNumeric="d" />
			</td>
			<td>
				<input name="txt_printing_cost_rate" type="text"   class='text_boxes_numeric' id="txt_printing_cost_rate"  value="<? echo $row['item_rate']; ?>" onkeyup="calculate_embelishment_cost_row('txt_printing_cost',1)" onlyNumeric="d" style="width:100px" />
			</td>
			<td>
				<input name="txt_printing_cost_amount" type="text"  class='text_boxes_numeric' id="txt_printing_cost_amount"  value="<? echo $row['item_cost']; ?>"  onkeyup="calculate_embelishment_cost_row('txt_printing_cost',2)" onlyNumeric="d" style="width:100px" />
			</td>
		</tr>
		<?
		}
		if ($i==2)
		{
		?>
		<tr>
			<td>Embroidery</td>
			<td>
				<input name="txt_embroidery_cost" type="text"  class='text_boxes' id="txt_embroidery_cost"  value="<? echo $row['item_cons_per_unit']; ?>" style="width:100px" onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_embroidery_cost_rate" type="text"   class='text_boxes_numeric' id="txt_embroidery_cost_rate"  value="<? echo $row['item_rate']; ?>" onkeyup="calculate_embelishment_cost_row('txt_embroidery_cost',1)" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_embroidery_cost_amount" type="text"  class='text_boxes_numeric' id="txt_embroidery_cost_amount"  value="<? echo $row['item_cost']; ?>" onkeyup="calculate_embelishment_cost_row('txt_embroidery_cost',2)" onlyNumeric="d"  style="width:100px" />
			</td>
		</tr>
		<?
		}
		if ($i==3)
		{
		?>
		<tr>
			<td>Washing</td>
			<td>
				<input name="txt_washing_cost" type="text"  class='text_boxes' id="txt_washing_cost"  value="<? echo $row['item_cons_per_unit']; ?>" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_washing_cost_rate" type="text"  class='text_boxes_numeric' id="txt_washing_cost_rate"  value="<? echo $row['item_rate']; ?>"   onkeyup="calculate_embelishment_cost_row('txt_washing_cost',1)" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_washing_cost_amount" type="text"  class='text_boxes_numeric' id="txt_washing_cost_amount"  value="<? echo $row['item_cost']; ?>"  style="width:100px" onkeyup="calculate_embelishment_cost_row('txt_washing_cost',2)" onlyNumeric="d"/>
			</td>
		</tr>
		<?
		}
		if ($i==4)
		{
		?>
		<tr>
			<td>Other </td>
			<td>
				<input name="txt_other_embellishment_Cost" type="text"  class='text_boxes'  id="txt_other_embellishment_Cost"  value="<? echo $row['item_cons_per_unit']; ?>" onkeyup="CalCulateYarnCost('OtherEmbellishmentCost','OtherEmbellishmentCostRate','OtherEmbellishmentCostAmount');CalCulateEmbellishment()" onlyNumeric="d" style="width:100px"/>
			</td>
			<td>
				<input name="txt_other_embellishment_Cost_rate" type="text"  class='text_boxes_numeric'  id="txt_other_embellishment_Cost_rate"  value="<? echo $row['item_rate']; ?>" onkeyup="calculate_embelishment_cost_row('txt_other_embellishment_Cost',1)" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_other_embellishment_Cost_amount" type="text"  class='text_boxes_numeric' id="txt_other_embellishment_Cost_amount"  value="<? echo $row['item_cost']; ?>" onkeyup="calculate_embelishment_cost_row('txt_other_embellishment_Cost',2)" onlyNumeric="d"  style="width:100px"/>
			</td>
		</tr>
		<?
		}
		 $i=$i+1;	}
		}
		else
		{
		?>
		<tr>
			<td>Printing</td>
			<td>
				<input name="txt_printing_cost" type="text" class='text_boxes'  id="txt_printing_cost" style="width:100px"  value="" onlyNumeric="d" />
			</td>
			<td>
				<input name="txt_printing_cost_rate" type="text"   class='text_boxes_numeric' id="txt_printing_cost_rate"  value="" onkeyup="calculate_embelishment_cost_row('txt_printing_cost',1)" onlyNumeric="d" style="width:100px" />
			</td>
			<td>
				<input name="txt_printing_cost_amount" type="text"  class='text_boxes_numeric' id="txt_printing_cost_amount"  value=""  onkeyup="calculate_embelishment_cost_row('txt_printing_cost',2)" onlyNumeric="d" style="width:100px" />
			</td>
		</tr>
		<tr>
			<td>Embroidery</td>
			<td>
				<input name="txt_embroidery_cost" type="text"  class='text_boxes' id="txt_embroidery_cost"  value="" style="width:100px" onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_embroidery_cost_rate" type="text"   class='text_boxes_numeric' id="txt_embroidery_cost_rate"  value="" onkeyup="calculate_embelishment_cost_row('txt_embroidery_cost',1)" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_embroidery_cost_amount" type="text"  class='text_boxes_numeric' id="txt_embroidery_cost_amount"  value="" onkeyup="calculate_embelishment_cost_row('txt_embroidery_cost',2)" onlyNumeric="d"  style="width:100px" />
			</td>
		</tr>
		<tr>
			<td>Washing</td>
			<td>
				<input name="txt_washing_cost" type="text"  class='text_boxes' id="txt_washing_cost"  value="" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_washing_cost_rate" type="text"  class='text_boxes_numeric' id="txt_washing_cost_rate"  value=""   onkeyup="calculate_embelishment_cost_row('txt_washing_cost',1)" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_washing_cost_amount" type="text"  class='text_boxes_numeric' id="txt_washing_cost_amount"  value=""  style="width:100px" onkeyup="calculate_embelishment_cost_row('txt_washing_cost',2)" onlyNumeric="d"/>
			</td>
		</tr>
		<tr>
			<td>Other </td>
			<td>
				<input name="txt_other_embellishment_Cost" type="text"  class='text_boxes'  id="txt_other_embellishment_Cost"  value="" onkeyup="CalCulateYarnCost('OtherEmbellishmentCost','OtherEmbellishmentCostRate','OtherEmbellishmentCostAmount');CalCulateEmbellishment()" onlyNumeric="d" style="width:100px"/>
			</td>
			<td>
				<input name="txt_other_embellishment_Cost_rate" type="text"  class='text_boxes_numeric'  id="txt_other_embellishment_Cost_rate"  value="" onkeyup="calculate_embelishment_cost_row('txt_other_embellishment_Cost',1)" onlyNumeric="d"  style="width:100px"/>
			</td>
			<td>
				<input name="txt_other_embellishment_Cost_amount" type="text"  class='text_boxes_numeric' id="txt_other_embellishment_Cost_amount"  value="" onkeyup="calculate_embelishment_cost_row('txt_other_embellishment_Cost',2)" onlyNumeric="d"  style="width:100px"/>
			</td>
		</tr>
		<?
		}
		?>
		
		<tr>
			<td colspan="4" height="15" align="center">
				
			</td>
		</tr>
		<tr>
		  <td colspan="4" align="center">
			<input type="submit" name="Submit3" class="formbutton" value="Save" style="width:100px " />			</td>
		</tr>
	</table>
	</form>
	</fieldset>
</div>
<!-- End of Embelishment -->
<?
}
//

if ($type==4)
{
?>
<!-- Start of Procurement -->

<div id='ProcurementCostTable' align="center">
<fieldset  style="width:510px; margin-left:10px">
<form name="frm_commercial_breakdown" id="frm_commercial_breakdown" action="javascript:fnc_commercial_cost_info_breakdown()">

	<table width="410" cellpadding="0" cellspacing="2" border="0">
		<tr>
			<td colspan="3" align="center">Commercial Cost Component Breakdown</td>
		</tr>
		<tr>
			<td align="center">Item</td>
			<td align="center">Percentage</td>
			<td align="center">Amount</td>
		</tr>
		<? 
			$company_sql= mysql_db_query($DB, "select * from  wo_po_cost_commercial_dtls where job_no_cost_comm_mst ='$search_string' order by id");
			$num_rows = mysql_num_rows($company_sql);
			if ($num_rows>0)
			{
				$i=1;
				while($row=mysql_fetch_array($company_sql))
				{
					
					if ($i==1)
					{
				?>
		<tr>
			<td>LC Cost </td>
			<td>
				<input name="txt_lc_cost_rate" type="text"  id="txt_lc_cost_rate" onkeyup="calculate_commercial_cost_row('txt_lc_cost',1)" class='text_boxes' style="width:100px"  value="<? echo $row['item_rate']; ?>" onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_lc_cost_amount" type="text"  id="txt_lc_cost_amount" onkeyup="calculate_commercial_cost_row('txt_lc_cost',2)"  class='text_boxes' style="width:100px" value="<? echo $row['item_cost']; ?>" onlyNumeric="d"/>
			</td>
		</tr>
		<?
		}
		if ($i==2)
		{
		?>
		<tr>
			<td>Port &amp; Clearing </td>
			<td>
				<input name="txt_port_clearing_cost_rate" type="text"  id="txt_port_clearing_cost_rate" onkeyup="calculate_commercial_cost_row('txt_port_clearing_cost',1)" class='text_boxes' value="<? echo $row['item_rate']; ?>"  style="width:100px" onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_port_clearing_cost_amount" type="text"  id="txt_port_clearing_cost_amount" onkeyup="calculate_commercial_cost_row('txt_port_clearing_cost',2)"  class='text_boxes'  value="<? echo $row['item_cost']; ?>"  style="width:100px" onlyNumeric="d"/>
			</td>
		</tr>
		<?
		}
		if ($i==3)
		{
		?>
		<tr>
			<td>Transportation</td>
			<td>
				<input name="txt_transport_cost_rate" type="text"  id="txt_transport_cost_rate" class='text_boxes' onkeyup="calculate_commercial_cost_row('txt_transport_cost',1)"   style="width:100px" value="<? echo $row['item_rate']; ?>" onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_transport_cost_amount"  onchange="calculate_commercial_cost_row('txt_transport_cost',2)" type="text"  id="txt_transport_cost_amount" class='text_boxes' value="<? echo $row['item_cost']; ?>"  style="width:100px" onlyNumeric="d"/>
			</td>
		</tr>
		<?
			}
		$i=$i+1;	}
		}
		else
		{
		?>
		<tr>
			<td>LC Cost </td>
			<td>
				<input name="txt_lc_cost_rate" type="text"  id="txt_lc_cost_rate" onkeyup="calculate_commercial_cost_row('txt_lc_cost',1)" class='text_boxes' style="width:100px"   onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_lc_cost_amount" type="text"  id="txt_lc_cost_amount" onkeyup="calculate_commercial_cost_row('txt_lc_cost',2)"  class='text_boxes' style="width:100px" onlyNumeric="d"/>
			</td>
		</tr>
		
		<tr>
			<td>Port &amp; Clearing </td>
			<td>
				<input name="txt_port_clearing_cost_rate" type="text"  id="txt_port_clearing_cost_rate" onkeyup="calculate_commercial_cost_row('txt_port_clearing_cost',1)" class='text_boxes'   style="width:100px" onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_port_clearing_cost_amount" type="text"  id="txt_port_clearing_cost_amount" onkeyup="calculate_commercial_cost_row('txt_port_clearing_cost',2)"  class='text_boxes'  style="width:100px" onlyNumeric="d"/>
			</td>
		</tr>
		
		<tr>
			<td>Transportation</td>
			<td>
				<input name="txt_transport_cost_rate" type="text"  onkeyup="calculate_commercial_cost_row('txt_transport_cost',1)" id="txt_transport_cost_rate" class='text_boxes'   style="width:100px"  onlyNumeric="d"/>
			</td>
			<td>
				<input name="txt_transport_cost_amount" type="text"   onkeyup="calculate_commercial_cost_row('txt_transport_cost',2)" id="txt_transport_cost_amount" class='text_boxes'   style="width:100px" onlyNumeric="d"/>
			</td>
		</tr>
		<? } ?>
		<tr>
		  <td colspan="3" align="center">
			<input type="submit" name="Submit3" class="formbutton" value="Save" style="width:100px " />			</td>
		</tr>
	</table>
	</form>
	</fieldset>
</div>
		<!--  End of Procurement  -->
				
<?
}

if ($type==5)
{
?>	
				
<!-- Commision cost table -->
<div id='CommissionCostTable' align="center">
<fieldset  style="width:510px; margin-left:10px">
<form name="frm_commission_breakdown" id="frm_commission_breakdown" action="javascript:fnc_commission_cost_info_breakdown()">

	<table width="510" cellpadding="0" cellspacing="2" border="0">
		<tr>
			<td colspan="3" align="center">Commission Cost Component Breakdown</td>
		</tr>
		<tr>
			<td>Item</td>
			<td>Percentage</td>
			<td>Amount</td>
		</tr>
		<? 
			$company_sql= mysql_db_query($DB, "select * from  wo_po_cost_commission_dtls where job_no_cost_commsn_mst ='$search_string' order by id");
			$num_rows = mysql_num_rows($company_sql);
			if ($num_rows>0)
			{
				$i=1;
				while($row=mysql_fetch_array($company_sql))
				{
					
					if ($i==1)
					{
				?>
		<tr>
			<td>Foreign Commission</td>
			<td><input name="txt_foreign_comission_cost_rate" type="text" onkeyup="calculate_commission_cost_row('txt_foreign_comission_cost',1)" id="txt_foreign_comission_cost_rate" class='text_boxes' value="<? echo $row['item_rate']; ?>" onlynumeric="d"/></td>
			<td>
				<input name="txt_foreign_comission_cost_amount" type="text" onkeyup="calculate_commission_cost_row('txt_foreign_comission_cost',2)"  id="txt_foreign_comission_cost_amount" value="<? echo $row['item_cost']; ?>" class='text_boxes' onlyNumeric="d"/>			</td>
		</tr>
		<?
		}
		if ($i==2)
		{
		?>
		<tr>
			<td>Local  Commission</td>
			<td>
				<input name="txt_local_comission_cost_rate" type="text" value="<? echo $row['item_rate']; ?>" onkeyup="calculate_commission_cost_row('txt_local_comission_cost',1)"  id="txt_local_comission_cost_rate" class='text_boxes'  onlyNumeric="d"/>			</td>
			<td>
				<input name="txt_local_comission_cost_amount" type="text" onkeyup="calculate_commission_cost_row('txt_local_comission_cost',2)" id="txt_local_comission_cost_amount" class='text_boxes' value="<? echo $row['item_cost']; ?>"  onlyNumeric="d"/>			</td>
		</tr>
		
		<?
			}
			$i=$i+1;
			}
		}
		else
		{
		?>
		<tr>
			<td>Foreign Commission</td>
			<td>
				<input name="txt_foreign_comission_cost_rate" type="text" onkeyup="calculate_commission_cost_row('txt_foreign_comission_cost',1)" id="txt_foreign_comission_cost_rate" class='text_boxes' onlynumeric="d"/>			</td>
			<td>
				<input name="txt_foreign_comission_cost_amount" type="text" onkeyup="calculate_commission_cost_row('txt_foreign_comission_cost',2)"  id="txt_foreign_comission_cost_amount" class='text_boxes' onlyNumeric="d"/>			</td>
		</tr>
		<tr>
			<td>Local  Commission</td>
			<td>
				<input name="txt_local_comission_cost_rate" type="text" onkeyup="calculate_commission_cost_row('txt_local_comission_cost',1)"  id="txt_local_comission_cost_rate" class='text_boxes'  onlyNumeric="d"/>			</td>
			<td>
				<input name="txt_local_comission_cost_amount" type="text" onkeyup="calculate_commission_cost_row('txt_local_comission_cost',2)" id="txt_local_comission_cost_amount" class='text_boxes'  onlyNumeric="d"/>			</td>
		</tr>
		<? } ?>
		<tr>
		  <td colspan="3" align="center">
				<input type="submit" name="Submit3" class="formbutton" value="Save" style="width:150px " />			</td>
		</tr>
</table>
</form>
</fieldset>
</div>
<?
}


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>