<?

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<script type="text/javascript">
        </script>
         <script>
		 	function set_present_graph()
			{
				document.getElementById('caption_text').innerHTML="Last 15 Days Attendance Summary ";
				var params = 
				{
					bgcolor:"#CCCCCC"
				};
				
				var flashVars = 
				{
					path: "reports/amcharts/flash/", 
					settings_file: "settings_present.php",
					data_file: "data_present.php"
					
				};        
				// change 8 to 80 to test javascript version            
				if (swfobject.hasFlashPlayerVersion("8"))
				{
					swfobject.embedSWF("reports/amcharts/flash/amcolumn.swf", "chartdiv", "800", "400", "8.0.0", "../../../../amcharts/flash/expressInstall.swf", flashVars, params);
				}
				else
				{
					// Note, as this example loads external data, JavaScript version might only work on server
					var amFallback = new AmCharts.AmFallback();
					amFallback.pathToImages = "../../../../amcharts/javascript/images/";
					amFallback.settingsFile = flashVars.settings_file;
					amFallback.dataFile = flashVars.data_file;				
					amFallback.type = "column";
					amFallback.write("chartdiv");
				}
				 
			}
			 
		 	function set_salary_graph()
			{
				document.getElementById('caption_text').innerHTML="Last 12 Months Salary";
				var params = 
				{
					bgcolor:"#FFFFFF"
				};
				
				var flashVars = 
				{
					path: "reports/amcharts/flash/", 
					settings_file: "salary_settings.php",
					data_file: "salary_data.php"
					
				};
				// change 8 to 80 to test javascript version            
				if (swfobject.hasFlashPlayerVersion("8"))
				{
					swfobject.embedSWF("reports/amcharts/flash/amcolumn.swf", "chartdiv", "800", "400", "8.0.0", "../../../../amcharts/flash/expressInstall.swf", flashVars, params);
				}
				else
				{
					// Note, as this example loads external data, JavaScript version might only work on server
					var amFallback = new AmCharts.AmFallback();
					amFallback.pathToImages = "../../../../amcharts/javascript/images/";
					amFallback.settingsFile = flashVars.settings_file;
					amFallback.dataFile = flashVars.data_file;				
					amFallback.type = "column";
					amFallback.write("chartdiv");
				}
				 
			}
			 
			window.onload = function()
				{
					//set_present_graph();
				}
		 </script>
    </head>
  <?
  $date=date("Y",time());
$month_prev=add_month(date("Y-m-d",time()),-3);
// echo date("Y-m-d",time());
$month_next=add_month(date("Y-m-d",time()),8);
// echo $month_next;

$start_yr=date("Y",strtotime($month_prev));
$end_yr=date("Y",strtotime($month_next));
  
  function add_month($orgDate,$mon){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,date('d',$cd),date('Y',$cd)));
  return $retDAY;
}

  ?>
  
        <!-- saved from url=(0013)about:internet -->
        <!-- amcharts script-->
        <!-- swf object (version 2.2) is used to detect if flash is installed and include swf in the page -->
     
        <script type="text/javascript" src="reports/amcharts/flash/swfobject.js"></script>

		<!-- following scripts required for JavaScript version. The order is important! -->
		<script type="text/javascript" src="reports/amcharts/javascript/amcharts.js"></script>
		<script type="text/javascript" src="reports/amcharts/javascript/amfallback.js"></script>
		<script type="text/javascript" src="reports/amcharts/javascript/raphael.js"></script>
        <!-- chart is placed in this div. if you have more than one chart on a page, give unique id for each div -->
        <table width="1050" cellpadding="0" cellspacing="0">
    	<tr>
        	<td height="40" valign="middle" align="center" colspan="2">
            	<font size="2" color="#4D4D4D"> <strong><span id="caption_text"></span> <? echo "$start_yr"."-"."$end_yr"; ?></strong></font>
            </td>
            <td colspan="2" rowspan="2" valign="top" align="center"> 
            	<a href="##" onClick="set_present_graph()">Last 15 Days Present</a><br><a href="##" onClick="set_salary_graph()">Last 12 Month Salary</a>
                <!-- <br>
            	 <a href="##" onClick="set_intimates_qnty_graph()">Product Category WIse Quantity</a><br><a href="##" onClick="set_intimates_value_graph()">Product Category WIse Value</a><br>
                  -->
            </td>
        </tr>
        
        <tr>
        	<td width="8" bgcolor=" ">
        	<td align="center" width="794">
        		<div id="chartdiv" style="width:794px; height:200px; background-color:#FFFFFF"></div>
            </td>
             
        </tr>
        <tr>
        	<td height="8" colspan="2" bgcolor=" "></td>
            <td width="8" bgcolor=""></td> <!--#00CCFF-->
            <td></td>
        </tr>
        <tr>
        	<td colspan="2">
            	<table width="100%">
                	<tr>
                    	<td width="150"></td>
                        <td  align="right" valign="top">Copyright</td>
                        <td align="right" valign="top" width="310"> <img src="images/logic/logic_bottom_logo.png" height="65" width="300" /> 
                        </td>
                    </tr>
                </table>
            </td>
        	 <td colspan="7"></td>
        </tr>
	</table>
    </body>
</html>











