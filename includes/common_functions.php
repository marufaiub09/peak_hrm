<?php
//include('common.php');
//Padding number with zeroes to left
function padWithZeros($s, $n) {
    return sprintf("%0" . $n . "d", $s);
}
function change_date_format($date)
{
$new_date=explode("-",$date);
$new_date_format=$new_date[2]."-".$new_date[1]."-".$new_date[0];
return $new_date_format;
}

function add_time($event_time,$event_length)
{
    if( ( $event_length*1 )==0){ return $event_time; die;}
    $timestamp = strtotime("$event_time");
    $etime = strtotime("+$event_length minutes", $timestamp);
    $etime=date('H:i:s', $etime);
    return $etime;

}

function show_time_format( $aTime, $vFormat )
{

    if($vFormat==0) //AM PM
    {
        if ($aTime=="00:00:00" || $aTime=="") return "00:00";
        else return DATE("g:i a", STRTOTIME($aTime));
    }
    else
    {
        if ($aTime=="00:00:00" || $aTime=="") return "00:00:00";
        else return DATE("H:i:s", STRTOTIME( $aTime ));
    }
}

function get_buyer_in_time($in_time,$r_in_time,$allow_time)
{
    if ($allow_time==0 or $allow_time=="") $allow_time=14;
    $timeDiffin=datediff(n,$in_time,$r_in_time);
    if ($in_time=="00:00:00" || $in_time=="00:00") {return $in_time; die;}
    if ($allow_time>$timeDiffin){ return $in_time; die; }
    else
    {
        if ( strlen( $timeDiffin )<2 )
        {
            return add_time($r_in_time,-$timeDiffin);
        }
        else
        {
            $adds=substr($timeDiffin,0,1)+substr($timeDiffin,1,1);
            return add_time($r_in_time,-$adds); die;
        }
    }
}

function get_buyer_out_time( $out_time, $r_out_time, $allow_time, $ot_limit, $buyer_ot_hours, $ot_start_minute, $one_hour_ot_unit, $ot_entitled, $r_in_time, $is_regular_day )
{

    if ($allow_time==0 or $allow_time=="") $allow_time=14;
    if ($ot_limit==0 or $ot_limit=="") $ot_limit=120;
    if($out_time=="00:00:00") return $out_time;
    $buyer_ot_hours_t=explode(".",$buyer_ot_hours);
    if(strlen($buyer_ot_hours_t[1])==1) $buyer_ot_hours_t[1]=$buyer_ot_hours_t[1]."0";

    $buyer_ot_hours=($buyer_ot_hours_t[0]*60)+$buyer_ot_hours_t[1];

    if( $ot_limit>$buyer_ot_hours ) { return $out_time;  }

    //if( $ot_limit>$buyer_ot_hours ) { return add_time($r_out_time,$ot_limit); }//sohel

    //buyer_ot_hours
    $cross_date=0;
    if( $out_time<$r_in_time && $out_time>"00:00:00") $cross_date=1;

    if($cross_date==1)
    {
        $nmin=datediff( "n", $r_out_time,"23:59:59" )+ datediff( "n","00:00:00",$out_time );
    }
    else
    {
        $nmin=datediff( "n", $r_out_time,$out_time );
    }


    if( $nmin<=$buyer_ot_hours ) return $out_time;

    //return $ot_entitled;
    if($ot_entitled==1)
    { //this condition is used for sign_out time will not exceed 2hours
        if( $buyer_ot_hours==0 )
        {
            $min=date("i" ,strtotime($out_time));
            if ($min>$allow_time)
            {
                $new_min=substr($min,0,1)+substr($min,1,1);
                if (strlen($new_min)==1) $new_min="0".$new_min;
            }
            else
            {
                $new_min= $min; if (strlen($new_min)==1) $new_min="0".$new_min;
            }
            // return "sohel";
            return date("H:".$new_min.":s" ,strtotime($out_time));
        }



        if($is_regular_day==0)
        {
            $max_out_time=add_time($r_in_time,$ot_limit);
            $new_out_time=add_time($r_in_time,$buyer_ot_hours);
            //return $new_out_time."pp";
            if($new_out_time<$max_out_time) return $new_out_time;
        }
        else
        {
            $max_out_time=add_time($r_out_time,$ot_limit);
            $new_out_time=add_time($r_out_time,$buyer_ot_hours);
            //return $new_out_time."pp".$max_out_time;
            if($new_out_time<$max_out_time) return $new_out_time;
        }


        $min=date("i" ,strtotime($out_time));



        if ($ot_start_minute==0)
        {
            if ($min>$allow_time)
            {
                $new_min=substr($min,0,1)+substr($min,1,1);
                if (strlen($new_min)==1) $new_min="0".$new_min;
            }
            else {  $new_min=substr($min,0,1)+substr($min,1,1); if (strlen($new_min)==1 && $new_min<8) $new_min="0".$new_min+$min; }//change here
        }
        else
        {
            if ($min>$allow_time && $min<$ot_start_minute)
            {
                $new_min=substr($min,0,1)+substr($min,1,1);
                if (strlen($new_min)==1) $new_min="0".$new_min;
            }
            else
            {
                $new_min=substr($min,0,1)+substr($min,1,1);
                if (strlen($new_min)==1) $new_min="0".$new_min; /* $new_min=  $min; if (strlen($new_min)==1) $new_min="0".$new_min;*/
                //return $new_min."kkkk";
            }
        }

        if ($ot_entitled>0) return add_time($new_out_time,$new_min);//  date("H:".$new_min.":s" ,strtotime($new_out_time));
        else return date("H:i:s" ,strtotime($out_time));
    }
    else
    {
        //return $nmin."ssss";
        if ($out_time=="00:00:00" || $out_time=="00:00" ) { return $out_time;}
        $timeDiffin=$nmin;//datediff("n",$r_out_time,$out_time);
        if ($timeDiffin<-($buyer_ot_hours*60)) $out_time=add_time($r_out_time,($buyer_ot_hours*70));
        //return $out_time;
        //echo $buyer_ot_hours; die;
        if ($timeDiffin<$allow_time && $timeDiffin>-$allow_time){ return $out_time;  }
        //if($ot_limit > $timeDiffin && $timeDiffin > 0) {return "ss".$out_time; die;}
        if( $ot_limit > $timeDiffin && $timeDiffin>0)
        {
            if ($timeDiffin <=$allow_time) {return $out_time;}
            $min=date("i",strtotime($out_time));
            if ( $min>$allow_time and  $min<$one_hour_ot_unit )
            {
                $min=substr($min,0,1)+substr($min,1,1);
                if (strlen($min)==1)$min="0".$min; else $min=$min;
                return date("H:".$min.":s" ,strtotime($out_time));
            }
            else { return $out_time; }
        }
        else if ($timeDiffin<0)
        {
            $min=date("i",strtotime($out_time));
            if ( $min>$allow_time and  $min<$one_hour_ot_unit )
            {
                $min=substr($min,0,1)+substr($min,1,1);
                if (strlen($min)==1)$min="0".$min; else $min=$min;
                return date("H:".$min.":s" ,strtotime($out_time));
            }
        }
          //return $ot_limit ."-". $timeDiffin; die;


        //if($ot_limit > $timeDiffin && $timeDiffin > 0) {return "ss".$out_time; die;}

        $b_out_time= add_time($r_out_time,$ot_limit);
        $min=substr($out_time,3,1)+substr($out_time,4,1);
        //echo $out_time;die;
        //for top or mid management buyer out time adjust

        if($ot_entitled==0)
        {
            if($timeDiffin>=$ot_limit)
            {
                $b_act_out=substr($b_out_time,0,2).":".str_pad($min,2,"0",STR_PAD_LEFT).":".substr($out_time,6,2);
                return $b_act_out;
            }
            else if($timeDiffin<$ot_limit)
            {
                while($min>$allow_time)
                    {
                        $flag=1;
                        $min= $min-$allow_time;
                    }
                if($flag==1)
                    {
                        if($min>=0){$b_act_out = date('H:i:s', ((strtotime(add_time($r_out_time,$ot_limit+$allow_time)))-(60*$min))); }
                        else if($min<0){$b_act_out = date('H:i:s', ((strtotime(add_time($r_out_time,$ot_limit+$allow_time)))+(60*$min))); }
                        return $b_act_out;
                    }
                else
                    {
                        if (strlen($min)==1)$min="0".$min; else $min=$min;
                    }
                $b_act_out=substr($b_out_time,0,2).":".$min.":".substr($out_time,6,2);
                return $b_act_out;
            }
            else
            {
                return $out_time;
            }
        }

        //for non management buyer out time adjust
        if ($timeDiffin<0)
        {

            if($buyer_ot_hours==0){
                return $out_time;
            }
            else{
                    while($min>$allow_time)
                        {
                            $flag=1;
                            $min= $min-$allow_time;
                        }
                    if($flag==1)
                        {
                            if($min>=0){$b_act_out = date('H:i:s', ((strtotime(add_time($r_out_time,$ot_limit+$allow_time)))-(60*$min))); }
                            else if($min<0){$b_act_out = date('H:i:s', ((strtotime(add_time($r_out_time,$ot_limit+$allow_time)))+(60*$min))); }
                            return $b_act_out;
                        }
                    else
                        {
                            if (strlen($min)==1)$min="0".$min; else $min=$min;
                        }
                    $b_act_out=substr($b_out_time,0,2).":".$min.":".substr($out_time,6,2);
                    return $b_act_out;
                }
        }
        else if ($timeDiffin<=$ot_limit && $timeDiffin>=0)
        {

              if($buyer_ot_hours*60>=$ot_start_minute && $buyer_ot_hours*60<$one_hour_ot_unit)
                {
                    return date('H:i:s', ((strtotime(add_time($r_out_time,30)))-(60*$min)));
                }
              else if($buyer_ot_hours*60>$one_hour_ot_unit)
                {
                    if($buyer_ot_hours*60>=($one_hour_ot_unit+$one_hour_ot_unit) )
                    {
                        return date('H:i:s', ((strtotime(add_time($r_out_time,120)))-($min)));
                    }
                    else if($buyer_ot_hours*60>=($one_hour_ot_unit+$ot_start_minute) && $buyer_ot_hours*60<($one_hour_ot_unit+$one_hour_ot_unit))
                    {
                        return date('H:i:s', ((strtotime(add_time($r_out_time,90)))-($min)));
                    }
                        return date('H:i:s', ((strtotime(add_time($r_out_time,60)))-($min)));
                }
              else
                {
                    return date('H:i:s', ((strtotime(add_time($r_out_time,$allow_time)))-(60*$min)));
                }

        }
        else
        {

            while($min>$allow_time)
                {
                    $flag=1;
                    $min= $min-$allow_time;
                }
            if($flag==1)
                {
                    if($min>=0){$b_act_out = date('H:i:s', ((strtotime(add_time($r_out_time,$ot_limit+$allow_time)))-(120*$min))); }
                    else if($min<0){$b_act_out = date('H:i:s', ((strtotime(add_time($r_out_time,$ot_limit+$allow_time)))+(120*$min))); }
                    return $b_act_out;
                }
            else
                {
                    if (strlen($min)==1)$min="0".$min; else $min=$min;
                }

            $b_act_out=substr($b_out_time,0,2).":".$min.":".substr($out_time,6,2);
            return $b_act_out;
        }
    }
}

function get_buyer_ot_hr( $act_ot_min, $one_hour_ot_unit, $allow_ot_fraction, $half_hr_ot_unit, $first_ot_limit )
{
    //echo $act_ot_min."ff";die;
    $full_hr=0;
    if ($act_ot_min==0) { return 0; }

    if ($first_ot_limit==0 or $first_ot_limit=="")
    {
        $timeDiffin=$act_ot_min;
        if ($timeDiffin<0) {return 0; }
        $full_hr=floor($timeDiffin/60);

        $rest=$timeDiffin % 60;
        if( $allow_ot_fraction==3 )
        {// $rest=str_pad($rest,2,"0",STR_PAD_LEFT);

            if ( $rest>=$half_hr_ot_unit ){ if( strlen($rest)<2 )  return $full_hr.".0".$rest; else  return $full_hr.".".$rest;  }
            else return $full_hr;

            /*if($full_hr<2)
            {
                if ( $rest>=$half_hr_ot_unit ){  return $full_hr.".".$rest;  }
                else  {  return $full_hr;  }
            }
            else
            {
                return $full_hr.".".$rest;
            }*/
        }
        else if ( $rest>=$one_hour_ot_unit )
        {
            $full_hr=$full_hr+1;
        }
        else if ($allow_ot_fraction==1)
        {
            if ($rest>=$half_hr_ot_unit && $rest<$one_hour_ot_unit )
            {
                $full_hr=$full_hr+.5;
            }
        }
        return $full_hr;
    }
    else
    {
        $timeDiffin=$act_ot_min; //datediff(n,$r_out_time,$out_time);
        if ($timeDiffin<0) {return 0;}

        if ($timeDiffin>=$first_ot_limit) {return ($first_ot_limit/60); }
        else
        {
            $full_hr=floor($timeDiffin/60);
            //if ($first_ot_limit==1200) {return $full_hr; die;}

            $rest=$timeDiffin % 60;

            if( $allow_ot_fraction==3 )
            {
                if ( $rest>=$half_hr_ot_unit )
                {
                    return $full_hr.".".str_pad($rest,2,"0",STR_PAD_LEFT);   // if( strlen($rest)==1 )  return $full_hr.".".str_pad($rest,2,"0",STR_PAD_LEFT); else
                }
                else return $full_hr;

            }
            else if ( $rest>=$one_hour_ot_unit )
            {
                $full_hr=$full_hr+1;
            }
            else if ($allow_ot_fraction==1)
            {
                if ($rest>=$half_hr_ot_unit && $rest<$one_hour_ot_unit )
                {
                    $full_hr=$full_hr+.5;
                }
            }
        }
        return $full_hr;
    }

}


function get_buyer_tot_emp_ot_hr($emp_code,$str_date,$one_hour_ot_unit,$allow_ot_fraction,$half_hr_ot_unit,$first_ot_limit)
{
    $emp_ot_hr=0;
    $sql="select * from hrm_attendance where emp_code=$emp_code and $str_date";
    $result=mysql_query($sql);
    while($row=mysql_fetch_array($result))
    {
        $ot_hr='';
        $ot_hr=get_buyer_ot_hr($row[total_over_time_min],$one_hour_ot_unit,$allow_ot_fraction,$half_hr_ot_unit,$first_ot_limit);
        $emp_ot_hr=$emp_ot_hr+$ot_hr;
//$emp_ot_hr=$emp_ot_hr+get_buyer_ot_hr($row[total_over_time_min],$var_hrm_chart[$row['company_id']]['one_hour_ot_unit'],$var_hrm_chart[$row['company_id']]['allow_ot_fraction'],$var_hrm_chart[$row['company_id']]['ot_start_minute'],$var_hrm_chart[$row['company_id']]['first_ot_limit']);
        //$emp_ot_hr=$emp_ot_hr+$row[total_over_time_min];
    }
    return $emp_ot_hr;die;
}

function fnc_gray_fabric_consumption($job_no,$order_qnty,$consumption_type)
{
    // total_grey_fab_consumption_top,consumption_per_number_top,consumption_per_unit_top
    $sql="select * from wo_po_measurement_top_details where job_no_mst_top='$job_no' and status_active=1";
    $result=mysql_query($sql);
    $row=mysql_fetch_array($result);
    // total_grey_fab_consumption_bottom,consumption_per_number_bottom,consumption_per_unit_bottom
    $sql="select * from wo_po_measurement_bottom_details where job_no_mst_bottom='$job_no' and status_active=1";
    $result_bt=mysql_query($sql);
    $row_bt=mysql_fetch_array($result_bt);

    if ($consumption_type==1) // Gray Consumption
         $consumption= $row[total_grey_fab_consumption_top]+ $row_bt[total_grey_fab_consumption_bottom];
    else if ($consumption_type==2) // FInish
        $consumption= $row[total_finish_fab_consumption_top]+ $row_bt[total_finish_fab_consumption_bottom];
    else if ($consumption_type==3) // Yarn
        $consumption= $row[txt_total_yarn_needed_top]+ $row_bt[txt_total_yarn_needed_bottom];
    else if ($consumption_type==4) // trims
    {
        $consumption= 1;
        if(mysql_num_rows($result) == 0 && mysql_num_rows($result_bt) == 0)
        {
            $total_req=$order_qnty/12;
            return number_format($total_req,2,'.',''); die;
        }
    }


    if ($row[consumption_per_unit_top]==0) $total_qnty=$order_qnty/(12*$row[consumption_per_number_top]);
    else  $total_qnty=$order_qnty/(1*$row[consumption_per_number_top]);
    $total_req=$consumption* $total_qnty;
    return number_format($total_req,2,'.',''); die;

}

// function :: set signeture list based on company and report name
// written by :: m@mit

function signeture_table($report_id,$company,$width,$location)
{
    if ($width=="") $width="100%";
    $signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company and location_id=$location");
    $signeture_list = explode('#',$signeture_allow_or_no);
    if($signeture_allow_or_no!= "")
    {
     echo '<table width="'.$width.'">
        <tr height="100" style="font-size:11px; font-family:Arial; font-weight:bold">';
        foreach($signeture_list as $key=>$value)
        {
            //echo '<td align="center" valign="bottom" style="text-decoration:overline">'.$value.'</td> ';
            echo '<td align="center" valign="bottom" style="text-decoration:overline; border-top-style:hidden; border-right-style:hidden; border-left-style:hidden; border-bottom-style:hidden;">'.$value.'</td>';
        }
        echo '</tr></table>';
    }
    else
    {
        echo '<table width="'.$width.'">
        <tr height="100" style="font-size:11px; font-family:Arial; font-weight:bold">';

        echo '</tr></table>';
    }
}

function company_logo($report_id,$company,$src)
{
    $signeture_allow_or_no = return_field_value("report_list","variable_settings_report","module_id=$report_id and company_id=$company");
    $signeture_list = explode('#',$signeture_allow_or_no);
    if(signeture_allow_or_no!= "")
    {
     echo "<img style='padding:5px; margin-left:10px;' src='../".$src."' width='70' height='70' />";
    }
}

// createDateRangeArray
function createDateRangeArray($strDateFrom,$strDateTo)
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}

//ekram
function check_zero_value($val,$status,$type){

        if( $status=="CL" || $status=="SL" || $status=="ML" || $status=="EL" || $status=="LWP" || $status=="SpL" || $status=="EdL" )
        {
            if($type==1){$val="00:00";}else {$val="0";}
        }
        else
        {
            $val=$val;
        }

        return  $val;
}

function bulk_update_sql_statement( $table, $id_column, $update_column, $data_values, $id_count, $extra_cond )
{
    $field_array=explode("*",$update_column);
    //$id_count=explode("*",$id_count);
    //$data_values=explode("*",$data_values);
    //print_r($data_values);die;
    $sql_up.= "UPDATE $table SET ";

     for ($len=0; $len<count($field_array); $len++)
     {
         $sql_up.=" ".$field_array[$len]." = CASE $id_column ";
         for ($id=0; $id<count($id_count); $id++)
         {
             if (trim($data_values[$id_count[$id]][$len])=="") $sql_up.=" when ".$id_count[$id]." then  '".$data_values[$id_count[$id]][$len]."'" ;
             else $sql_up.=" when ".$id_count[$id]." then  ".$data_values[$id_count[$id]][$len]."" ;
         }
         if ($len!=(count($field_array)-1)) $sql_up.=" END, "; else $sql_up.=" END ";
     }
     $sql_up.=" where id in (".implode(",",$id_count).") $extra_cond";
     return $sql_up;
}

function send_mail_mailer($to,$sub,$html)
{
    $mail = new PHPMailer();
    $mail->From     = "info@logicsoftbd.com";
    $mail->FromName = "LOGIC HRM";
    $tos=explode(",",$to);
    for($i=0; $i<count($tos); $i++)
    {
        if( $tos[$i]!="") $mail->AddAddress($tos[$i],"SYSTEM USER");
    }

    $mail->WordWrap = 50;                              // set word wrap
    $mail->IsHTML(true);                               // send as HTML

    $mail->Subject  =  $sub;
    $mail->Body     =  $html;
    $mail->AltBody  =  "This is the text-only body";

    if(!$mail->Send())
    {
       //echo "Message was not sent <p>";
      // echo "Mailer Error: " . $mail->ErrorInfo;
      echo "2";
      exit;
    }
    //echo "Mail sent ";
    echo "3";
    die;
}

function get_out_time_jm( $out_time, $r_out_time, $buyer_ot_hours )
{
    $min=date("i" ,strtotime($out_time));
    if($min>54 &&  $buyer_ot_hours>0) $buyer_ot_hours=$buyer_ot_hours-1;

    if($min>14 && $min<55)
    {
        $new_min=substr($min,0,1)+substr($min,1,1);
        if (strlen($new_min)==1) $new_min="0".$new_min;
    } else { $new_min=$min;if (strlen($new_min)==1) $new_min="0".$new_min; }

    if( $buyer_ot_hours==0 )
    {
        return date("H:".$new_min.":s" ,strtotime($out_time));
    }
    else
    {
        $buyer_ot_hours_t=explode(".",$buyer_ot_hours);
        $buyer_ot_hours=($buyer_ot_hours_t[0]*60)+$buyer_ot_hours_t[1];
        $new_out_time=add_time($r_out_time,$buyer_ot_hours);
        return date("H:".$new_min.":s" ,strtotime($new_out_time));
    }

}

 //============zaman======== 22.10.2014======
 function load_html_head_contents($title, $path, $filter, $popup, $unicode, $multi_select, $am_chart)
 {
     $html='
     <!DOCTYPE HTML>
     <html>
     <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
     <title>'.$title.'</title>
     <link href="'.$path.'css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
     <script src="'.$path.'includes/functions.js" type="text/javascript"></script>
     <script type="text/javascript" src="'.$path.'js/jquery.js"></script>';

     if ( $filter!="" )
         $html .='
             <link href="'.$path.'css/filtergrid.css" rel="stylesheet" type="text/css" media="screen" />
             <script src="'.$path.'js/tablefilter.js" type="text/javascript"></script>';

    if ( $popup!="" )
         $html .='
            <link href="'.$path.'css/modal_window.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="'.$path.'js/modal_window.js"></script>';
    if ( $unicode!="" )
         $html .='
             <script type="text/javascript" src="'.$path.'js/driver.phonetic.js" ></script>
            <script type="text/javascript" src="'.$path.'js/driver.probhat.js" ></script>
            <script type="text/javascript" src="'.$path.'js/engine.js" ></script>';

    if ( $multi_select!="" )
         $html .='
             <script src="'.$path.'js/multi_select.js" type="text/javascript"></script>';

    if ($am_chart!="")
            $html .='
                <script type="text/javascript" src="'.$path.'ext_resource/amcharts/flash/swfobject.js" ></script>
                <script type="text/javascript" src="'.$path.'ext_resource/amcharts/javascript/amcharts.js" ></script>
                <script type="text/javascript" src="'.$path.'ext_resource/amcharts/javascript/amfallback.js" ></script>
                <script type="text/javascript" src="'.$path.'ext_resource/amcharts/javascript/raphael.js" ></script>
                <script type="text/javascript" src="'.$path.'js/chart/logic_chart.js" ></script>';

     return $html; die;

 }

function get_file_ext($key) {
    $key=strtolower(substr(strrchr($key, "."), 1));
    $key=str_replace("jpeg","jpg",$key);
    return $key;
}

// sql_select
function sql_select($strQuery, $is_single_row, $new_conn, $un_buffered)
{
    $result_select = mysql_query( $strQuery );
    $rows = array();
    while( $row = mysql_fetch_array( $result_select ))
    {
        $rows[] = $row;
    }
    return $rows; die;
}

// execute_query
function execute_query( $strQuery, $commit )
{
    $result=mysql_query($strQuery);
    return $result;
}

// sql_insert
function sql_insert($strTable,$arrNames,$arrValues, $commit )
{
    $strQuery= "INSERT INTO ".$strTable." (".$arrNames.") VALUES ".$arrValues."";
    $result=mysql_query($strQuery);
    return $result;
    die;
}

?>
