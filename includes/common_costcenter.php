<?php
// company_id
if( $_SESSION['logic_erp']['company_id'] == 0 || $_SESSION['logic_erp']['company_id']=="")
{
    $user_company_id="";
    $loc_company_id="";
}
else
{
    $user_company_id="id in(".$_SESSION['logic_erp']['company_id'].") and";
    $loc_company_id="company_id in(".$_SESSION['logic_erp']['company_id'].") and";
}

// location_id
if( $_SESSION['logic_erp']['location_id'] == 0 || $_SESSION['logic_erp']['location_id']=="")
{
    $user_location_id="";
    $div_location_id="";
}
else
{
    $user_location_id="id in(".$_SESSION['logic_erp']['location_id'].") and";
    $div_location_id="location_id in(".$_SESSION['logic_erp']['location_id'].") and";
}

// division_id
if( $_SESSION['logic_erp']['division_id'] == 0 || $_SESSION['logic_erp']['division_id']=="")
{
    $user_division_id="";
    $dpt_division_id="";
}
else
{
    $user_division_id="id in(".$_SESSION['logic_erp']['division_id'].") and";
    $dpt_division_id="division_id in(".$_SESSION['logic_erp']['division_id'].") and";
}

// department_id
if( $_SESSION['logic_erp']['department_id'] == 0 || $_SESSION['logic_erp']['department_id']=="")
{
    $user_department_id="";
    $sec_department_id="";
}
else
{
    $user_department_id="id in(".$_SESSION['logic_erp']['department_id'].") and";
    $sec_department_id="department_id in(".$_SESSION['logic_erp']['department_id'].") and";
}

// section_id
if( $_SESSION['logic_erp']['section_id'] == 0 || $_SESSION['logic_erp']['section_id']=="")
{
    $user_section_id="";
    $subsec_section_id="";
}
else
{
    $user_section_id="id in(".$_SESSION['logic_erp']['section_id'].") and";
    $subsec_section_id="section_id in(".$_SESSION['logic_erp']['section_id'].") and";
}

// subsection_id
if( $_SESSION['logic_erp']['subsection_id'] == 0 || $_SESSION['logic_erp']['subsection_id']=="")
{
    $user_subsection_id="";
}
else
{
    $user_subsection_id="id in(".$_SESSION['logic_erp']['subsection_id'].") and";
}

//company_details
$sql = "SELECT id, company_name FROM lib_company WHERE $user_company_id status_active=1 and is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) )
{
    $company_details[$row['id']] = mysql_real_escape_string( $row['company_name'] );
}

//location_details
$sql = "SELECT id, location_name FROM lib_location WHERE $user_location_id $loc_company_id status_active=1 and is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) )
{
    $location_details[$row['id']] = mysql_real_escape_string( $row['location_name'] );
}

//division_details
$sql = "SELECT id, division_name FROM lib_division WHERE $user_division_id $div_location_id status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) )
{
    $division_details[$row['id']] = mysql_real_escape_string( $row['division_name'] );
}

//department_details
$sql = "SELECT id, department_name FROM lib_department WHERE $user_department_id $dpt_division_id status_active=1 and is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) )
{
    $department_details[$row['id']] = mysql_real_escape_string( $row['department_name'] );
}

//section_details
$sql = "SELECT id, section_name FROM lib_section WHERE $user_section_id $sec_department_id status_active=1 and is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) )
{
    $section_details[$row['id']] = mysql_real_escape_string( $row['section_name'] );
}

//subsection_details
$sql = "SELECT id, subsection_name FROM lib_subsection WHERE $user_subsection_id $subsec_section_id status_active=1 and is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) )
{
    $subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );
}
?>
