//Do not change 
function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer") {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else request_type = new XMLHttpRequest();
	
	return request_type;
}
var http = createObject();

function fnc_main_module(save_perm,edit_perm,delete_perm,approve_perm) {
	var txt_module_name = encodeURI( document.getElementById('txt_module_name').value );
	var txt_module_link = encodeURI( document.getElementById('txt_module_link').value );
	var txt_module_seq = encodeURI( document.getElementById('txt_module_seq').value );
	var cbo_module_sts = encodeURI( document.getElementById('cbo_module_sts').value );
	var save_up = encodeURI( document.getElementById('save_up').value );
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if (cbo_module_sts==2 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( $('#txt_module_name').val() == "" ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_module_name').focus();
			$(this).html('Please Enter Module Name').addClass('messageboxerror').fadeTo( 900, 1 );
		});
	}
	else if( $('#txt_module_seq').val() == "" ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_module_seq').focus();
			$(this).html('Please Enter Module Sequence').addClass('messageboxerror').fadeTo( 900, 1 );
		});
	}
	else if( IsNumeric( document.getElementById('txt_module_seq').value ) == false ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_module_seq').focus();
			$(this).html('Please Enter numeric Value').addClass('messageboxerror').fadeTo( 900, 1 );
		});
	}
	else if( $('#cbo_module_sts').val() == 0 ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#cbo_module_sts').focus();
			$(this).html('Please Enter Module status').addClass('messageboxerror').fadeTo( 900, 1 );
		});
	}
	else {
		nocache = Math.random();
		http.open( 'get', '../save_update_common.php?action=main_module&isupdate=' + save_up + '&txt_module_name=' + txt_module_name + '&txt_module_link=' + txt_module_link + '&txt_module_seq=' + txt_module_seq + '&cbo_module_sts=' + cbo_module_sts + '&nocache =' + nocache );
		http.onreadystatechange = userReply_module;
		http.send( null );
	}
}

function userReply_module() {
	if( http.readyState == 4 ) {
		//var response = http.responseText;	
		var response = http.responseText.split('*');
		document.getElementById('txt_module_name').value="";
		document.getElementById('txt_module_link').value="";
		document.getElementById('txt_module_seq').value="";
		document.getElementById('cbo_module_sts').value=0;
		if (response[0]==9)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			}
			);
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.'+response[2]).addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"admin","create_main_module","update","../");
				document.getElementById('save_up').value="";
			}
			);
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.'+response[2]).addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"admin","create_main_module","insert","../");
				document.getElementById('save_up').value="";
			}
			);
		}
		//alert(response);
	}
	}			
//---------------------------Main Menu Save / Update-----------------------------------------------------------------------

function fnc_main_menu(save_perm,edit_perm,delete_perm,approve_perm)
{
	$("#chk_self_service").attr("checked") ? self_service=1 : self_service=0;
	var cbo_module_name = encodeURI(document.getElementById('cbo_module_name').value);
	var txt_menu_name = encodeURI(document.getElementById('txt_menu_name').value);
	var txt_menu_link = encodeURI(document.getElementById('txt_menu_link').value);
	var cbo_root_menu = encodeURI(document.getElementById('cbo_root_menu').value);
	var cbo_root_menu_under = encodeURI(document.getElementById('cbo_root_menu_under').value);
	var txt_menu_seq = encodeURI(document.getElementById('txt_menu_seq').value);
	var cbo_menu_sts = encodeURI(document.getElementById('cbo_menu_sts').value);
	var save_up = encodeURI(document.getElementById('save_up').value);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if (cbo_menu_sts==2 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if($('#cbo_module_name').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#cbo_module_name').focus();
			$(this).html('Please Select Module Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_menu_name').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_menu_name').focus();
			$(this).html('Please Enter Menu Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	/*
	else if($('#txt_menu_link').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_menu_link').focus();
			$(this).html('Please Enter Menu Link Page').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	*/
	else if($('#txt_menu_seq').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_menu_seq').focus();
			$(this).html('Please Enter Menu Sequence').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if(IsNumeric(document.getElementById('txt_menu_seq').value)==false)
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_menu_seq').focus();
			$(this).html('Please Enter numeric Value').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else
	{
		nocache = Math.random();
		http.open('get','../save_update_common.php?action=main_menu&isupdate='+save_up+'&cbo_module_name='+cbo_module_name+'&txt_menu_name='+txt_menu_name+'&txt_menu_link='+txt_menu_link+'&cbo_root_menu='+cbo_root_menu+'&cbo_root_menu_under='+cbo_root_menu_under+'&txt_menu_seq='+txt_menu_seq+'&cbo_menu_sts='+cbo_menu_sts+'&self_service='+self_service+'&nocache ='+nocache);
		http.onreadystatechange = userReply_main_menu;
		http.send(null); 
	//}
}
}

function userReply_main_menu()
{
	if(http.readyState == 4)
	{
		//var response = http.responseText;
		//alert(response);
		var response = http.responseText.split('*');
		document.getElementById('cbo_module_name').value=0;
		document.getElementById('txt_menu_name').value="";
		document.getElementById('txt_menu_link').value="";
		document.getElementById('cbo_root_menu').value=0;
		document.getElementById('cbo_root_menu_under').value=0;
		document.getElementById('txt_menu_seq').value="";
		document.getElementById('cbo_menu_sts').value=0;
		if (response[0]==9)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate menu name, please check.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			}
			);
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.'+response[2]).addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				save_activities_history('',"admin","create_menu","update","../");
				document.getElementById('save_up').value="";
			}
			);
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.'+response[2]).addClass('messageboxerror').fadeTo(900,1);
				save_activities_history('',"admin","create_menu","insert","../");
				document.getElementById('save_up').value="";
			}
			);
		}
		//alert(response);
	}
}
//--------------------------------------------------User Management
function fnc_user_creation(save_perm,edit_perm,delete_perm,approve_perm) {
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn( 1000 );
	
	var valid = true;
	var txt_user_id = encodeURI(document.getElementById('txt_user_id').value);
	var txt_passwd = encodeURI(document.getElementById('txt_passwd').value);
	var txt_conf_passwd = encodeURI(document.getElementById('txt_conf_passwd').value);
	var cbo_user_level = encodeURI(document.getElementById('cbo_user_level').value);
	var cbo_user_buyer = encodeURI(document.getElementById('cbo_user_buyer').value);
	var cbo_data_level_sec = encodeURI(document.getElementById('cbo_data_level_sec').value);
	
	var cbo_unit_name = encodeURI(document.getElementById('cbo_unit_name').value);
	var txt_ip_addres = encodeURI(document.getElementById('txt_ip_addres').value);
	var txt_exp_date = encodeURI(document.getElementById('txt_exp_date').value);
	var cbo_user_sts = encodeURI(document.getElementById('cbo_user_sts').value);
	var save_up = encodeURI(document.getElementById('save_up').value);
	
	if( $('#txt_user_id').val() == "" ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_user_id').focus();
			$(this).html('Please Enter a unique user name').addClass('messageboxerror').fadeTo( 900, 1 );
		});
		valid = false;
	}
	else if( $('#txt_passwd').val() == "" ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_passwd').focus();
			$(this).html('Please Enter Password').addClass('messageboxerror').fadeTo( 900, 1 );
		});
		valid = false;
	}
	else if(txt_passwd.length <4 ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_passwd').focus();
			$(this).html('Please Enter a Password of at least 4 characters..').addClass('messageboxerror').fadeTo( 900, 1 );
		});
		valid = false;
	}
	else if( $('#txt_conf_passwd').val() == "" ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_conf_passwd').focus();
			$(this).html('Please Confirm  same password.').addClass('messageboxerror').fadeTo( 900, 1 );
		});
		valid = false;
	}
	else if( $('#txt_conf_passwd').val() != $('#txt_passwd').val() ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#txt_conf_passwd').focus();
			$(this).html('New Password and Confirm password does not match.').addClass('messageboxerror').fadeTo( 900, 1 );
		});
		valid = false;
	}
	else if( $('#cbo_user_level').val() == 0 ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#cbo_user_level').focus();
			$(this).html('Please Select an user level.').addClass('messageboxerror').fadeTo( 900, 1 );
		});
		valid = false;
	}
	else if( $('#txt_ip_addres').val() != '' ) {
		var ip = $('#txt_ip_addres').val();
		var ipsub = new Array();
		ipsub = ip.split(".");
		
		if( ipsub.length < 4 ) valid = false;
		else {
			for( var i = 0; i < ipsub.length; i++ ) {
				var sub = parseInt( ipsub[i] );
				if( ( isNaN( sub ) ) || ( i == 0 && sub == 0 ) || ( i == 3 && sub == 0 ) || ( sub < 0 ) || ( sub >255 ) ) valid = false;
			}
		}
		
		if( valid == false ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#txt_ip_addres').focus();
				$(this).html('Please provide a correct IP address.').addClass('messageboxerror').fadeTo( 900, 1 );
			});
		}
	}
	if( valid == true ) {
		nocache = Math.random();
		http.open('get','../save_update_common.php?action=create_user&isupdate='+save_up+'&txt_user_id='+txt_user_id+'&txt_passwd='+txt_passwd+'&cbo_user_level='+cbo_user_level+'&cbo_user_buyer='+cbo_user_buyer+'&cbo_unit_name='+cbo_unit_name+'&txt_ip_addres='+txt_ip_addres+'&txt_exp_date='+txt_exp_date+'&cbo_user_sts='+cbo_user_sts+'&cbo_data_level_sec='+cbo_data_level_sec+'&nocache='+nocache); //+'&txt_emp_id='+txt_emp_id
		http.onreadystatechange = fnc_user_creation_response;
		http.send( null );
	}
}

function fnc_user_creation_response() {
	if( http.readyState == 4 ) {
		var response = http.responseText.split('_');
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo( 900, 1 );
			});
		}
		else if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('User Created Successfully').addClass('messageboxerror').fadeTo( 900, 1 );
				save_activities_history('',"admin","create_user","insert","../");
				document.getElementById('save_up').value = "";
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Information Updated Successfully.').addClass('messageboxerror').fadeTo( 900, 1 );
				save_activities_history('',"admin","create_user","update","../");
				document.getElementById('save_up').value = "";
			});
		}
		else if( response[0] == 7 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html(response[1]).addClass('messageboxerror').fadeTo( 900, 1 );
			});
		}
		if( response[0] == 1 || response[0] == 2 ) {
			document.getElementById('txt_user_id').value		= "";
			document.getElementById('txt_passwd').value			= "";
			document.getElementById('txt_conf_passwd').value	= "";
			document.getElementById('cbo_user_level').value		= 0;
			document.getElementById('cbo_user_buyer').value		= 0;
			document.getElementById('cbo_unit_name').value		= 0;
			document.getElementById('txt_ip_addres').value		= "";
			document.getElementById('txt_exp_date').value		= "";
			document.getElementById('cbo_user_sts').value		= 1;
		}
	}
}

function fnc_change_pass(save_perm,edit_perm,delete_perm,approve_perm)
{
	//alert(type);
	var txt_user_id = encodeURI(document.getElementById('txt_user_id').value);
	var txt_old_passwd = encodeURI(document.getElementById('txt_old_passwd').value);
	var txt_new_passwd = encodeURI(document.getElementById('txt_new_passwd').value);
	var txt_conf_passwd = encodeURI(document.getElementById('txt_conf_passwd').value);
	var type=1;
	if (type==2 || type==4 )// and 
	{
		var cbo_user_level = encodeURI(document.getElementById('cbo_user_level').value);
		var cbo_unit_name = encodeURI(document.getElementById('cbo_unit_name').value);
		var txt_ip_addres = encodeURI(document.getElementById('txt_ip_addres').value);
		var txt_exp_date = encodeURI(document.getElementById('txt_exp_date').value);
		var cbo_user_sts = encodeURI(document.getElementById('cbo_user_sts').value);
	}
	 
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	 if (edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#txt_user_id').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_user_id').focus();
			$(this).html('Please enter an User Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_old_passwd').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_old_passwd').focus();
			$(this).html('Please Enter Old Password').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_new_passwd').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_new_passwd').focus();
			$(this).html('Please Enter a New Password').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_conf_passwd').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_conf_passwd').focus();
			$(this).html('Please Confirm Password.').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_new_passwd').val()!=$('#txt_conf_passwd').val())
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#txt_conf_passwd').focus();
			$(this).html('New Passsowrd and Confirm Password does not match.').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if (type==2 || type==4 )// and 
	{
		if($('#cbo_user_level').val()==0)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$('#cbo_user_level').focus();
				$(this).html('Please select user level.').addClass('messageboxerror').fadeTo(900,1);
			}
			);
		}
		else if($('#cbo_unit_name').val()==0)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$('#cbo_unit_name').focus();
				$(this).html('Please select unit Name.').addClass('messageboxerror').fadeTo(900,1);
			}
			);
		}
		else if($('#cbo_user_sts').val()==0)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$('#cbo_user_sts').focus();
				$(this).html('Please select user Status.').addClass('messageboxerror').fadeTo(900,1);
			}
			);
		}
	}
	else
	{
		nocache = Math.random();
		http.open('get','../save_update_common.php?action=change_pass&isupdate='+type+'&txt_user_id='+txt_user_id+'&txt_old_passwd='+txt_old_passwd+'&txt_new_passwd='+txt_new_passwd+'&cbo_user_level='+cbo_user_level+'&cbo_unit_name='+cbo_unit_name+'&txt_ip_addres='+txt_ip_addres+'&txt_exp_date='+txt_exp_date+'&cbo_user_sts ='+cbo_user_sts+'&nocache ='+nocache);
		http.onreadystatechange = userReply_change_pass;
		http.send(null); 
	//}
}
}

function userReply_change_pass()
{
	if(http.readyState == 4)
	{
		//var response = http.responseText;
		//alert(response);
		var response = http.responseText.split('_');
		//document.getElementById('txt_user_id').value=0;
		document.getElementById('txt_old_passwd').value="";
		document.getElementById('txt_new_passwd').value="";
		document.getElementById('txt_conf_passwd').value="";
		if (response[0]==2 || response[0]==4 )
		{
			document.getElementById('txt_ip_addres').value="";
			document.getElementById('txt_exp_date').value="";
		}
		if (response[0]==9)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Your Old Password is not Right.').addClass('messageboxerror').fadeTo(900,1);
			}
			);
		}
		else
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully').addClass('messageboxerror').fadeTo(900,1);
				save_activities_history('',"admin","change_passwd","update","../");
			}
			);
		}
		//alert(response);
	}
	}			

//--------------------------------------------------Priviledge Management
function fnc_user_priviledge(save_perm,edit_perm,delete_perm,approve_perm) 
{
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn( 1000 );
	
	var cbo_user_name			= encodeURI( document.getElementById('cbo_user_name').value );
	var cbo_main_module			= encodeURI( document.getElementById('cbo_main_module').value );
	var admin_privilege_all		= ( document.getElementById('admin_privilege_all').checked == true ) ? 1 : 0;
	var cbo_set_module_privt	= encodeURI( document.getElementById('cbo_set_module_privt').value );
	var cbo_main_menu_name		= encodeURI( document.getElementById('cbo_main_menu_name').value );
	var cbo_sub_main_menu_name	= encodeURI( document.getElementById('cbo_sub_main_menu_name').value );
	var cbo_sub_menu_name		= encodeURI( document.getElementById('cbo_sub_menu_name').value );
	var cbo_visibility			= encodeURI( document.getElementById('cbo_visibility').value );
	var cbo_insert				= encodeURI( document.getElementById('cbo_insert').value );
	var cbo_edit				= encodeURI( document.getElementById('cbo_edit').value );
	var cbo_delete				= encodeURI( document.getElementById('cbo_delete').value );
	var cbo_approve				= encodeURI( document.getElementById('cbo_approve').value );
	var save_up					= encodeURI( document.getElementById('save_up').value );
	
	if (save_perm==2 || edit_perm==2 || delete_perm==2) // save_perm,edit_perm,delete_perm
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have permission on this page.').fadeIn(1000);
	}
	else if( cbo_set_module_privt == 0 &&   $('#cbo_main_menu_name').val() == 0   ) {
		$("#messagebox").fadeTo( 200, 0.1, function() {
			$('#cbo_main_menu_name').focus();
			$(this).html('Please select a menu item for privilege.').addClass('messageboxerror').fadeTo( 900, 1 );
		});
	}
	else {
		nocache = Math.random();
		http.open( 'get', '../save_update_common.php?action=user_priviledge&cbo_user_name=' + cbo_user_name + '&cbo_main_module=' + cbo_main_module
				  			+ '&admin_privilege_all=' + admin_privilege_all + '&cbo_main_menu_name=' + cbo_main_menu_name + '&cbo_sub_main_menu_name=' + cbo_sub_main_menu_name
							+ '&cbo_sub_menu_name=' + cbo_sub_menu_name + '&cbo_visibility=' + cbo_visibility + '&cbo_insert=' + cbo_insert + '&cbo_edit=' + cbo_edit
							+ '&cbo_delete=' + cbo_delete + '&cbo_approve=' + cbo_approve + '&isupdate=' + save_up + '&cbo_set_module_privt=' + cbo_set_module_privt
							+ '&nocache=' + nocache );
		http.onreadystatechange = fnc_user_priviledge_response;
		http.send( null );
	}
}

function fnc_user_priviledge_response() {
	if( http.readyState == 4 ) {
		//var response = http.responseText;
		// alert(http.responseText); 
		var response = http.responseText.split('_');
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Admin priviledge for the selected module is set for the selected user.').addClass('messageboxerror').fadeTo( 900, 1 );
				save_activities_history('',"admin","user_previledge","update","../");
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Priviledge for the selected menu item is set for the selected user.').addClass('messageboxerror').fadeTo( 900, 1 );
				save_activities_history('',"admin","user_previledge","update","../");
			});
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('The selected module and menu will not be shown to the selected user anymore.').addClass('messageboxerror').fadeTo( 900, 1 );
				save_activities_history('',"admin","user_previledge","update","../");
			});
		}
		showResult_userpriv(document.getElementById('cbo_main_module').value, document.getElementById('cbo_user_name').value,'3','load_list_priv');
	}
}

function fnc_group_details()
{
	var group = encodeURI(document.getElementById('group').value);
	var group_name = encodeURI(document.getElementById('group_name').value);
	var contact_person = encodeURI(document.getElementById('contact_person').value);
	//alert(group);
	//alert(save_up);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if($('#group').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#group').focus();
			$(this).html('Please Enter group Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#group_name').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#group_name').focus();
			$(this).html('Please Enter group_name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#contact_person').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
		{ 
			$('#contact_person').focus();
			$(this).html('Please Enter contact_person').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else
	{
		nocache = Math.random();
		http.open('get','save_update_common.php?action=main_moduless&group='+group+'&group_name='+group_name+'&contact_person='+contact_person+'&nocache ='+nocache);
		http.onreadystatechange = userReply_grp;
		http.send(null); 
	//}
}
}

function userReply_grp()
{
	if(http.readyState == 4)
	{
		var response = http.responseText;	
		//alert(response);
		//alert(response);
	}
	}	
//--------------------- dying &amp; Chemical--------------------------//

function fnc_dying_info()
{
	var cbo_item_category 	= encodeURI(document.getElementById('cbo_item_category').value);
	var txt_item_group		= encodeURI(document.getElementById('txt_item_group').value);
	var save_up 			= encodeURI(document.getElementById('save_up').value);
	//alert(cbo_item_category);
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if($('#cbo_item_category').val()==0)
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#cbo_item_category').focus();
			$(this).html('Please Enter Item Category').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_item_group').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_item_group').focus();
			$(this).html('Please Enter Item Group').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=dying_info&isupdate='+save_up+
		'&cbo_item_category='+cbo_item_category+
		'&txt_item_group='+txt_item_group+
		'&nocache ='+nocache);
		http.onreadystatechange = dyingReply_info;
		http.send(null);
	}
}

function dyingReply_info()
{
	if(http.readyState == 4)
	{
		var response = http.responseText;	
		//alert(response);
		document.getElementById('cbo_item_category').value="";
		document.getElementById('txt_item_group').value="";
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			}
			);
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			}
			);
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			}
			);
		}
		//alert(response);
	}
	}	

function fnc_reader_conf(save_perm,edit_perm,delete_perm,approve_perm)
{
	var cbo_company_mst 	= escape(document.getElementById('cbo_company_mst').value);
	var txt_user_name 	= escape(document.getElementById('txt_user_name').value);
	var txt_user_id 	= escape(document.getElementById('txt_user_id').value);
	var txt_total_reader 	= escape(document.getElementById('txt_total_reader').value);
	var txt_reader_model 	= escape(document.getElementById('txt_reader_model').value);
	var cbo_db_type 	= escape(document.getElementById('cbo_db_type').value);
	var txt_server_name 	= escape(document.getElementById('txt_server_name').value);
	var txt_database_name 	= escape(document.getElementById('txt_database_name').value);
	var txt_table_name 	= escape(document.getElementById('txt_table_name').value);
	var txt_db_user 	= escape(document.getElementById('txt_db_user').value);
	var txt_db_password 	= escape(document.getElementById('txt_db_password').value);
	var txt_rf_code_fld 	= escape(document.getElementById('txt_rf_code_fld').value);
	var txt_date_fld 	= escape(document.getElementById('txt_date_fld').value);
	var txt_time_fld 	= escape(document.getElementById('txt_time_fld').value);
	var txt_reader_no_fld 	= escape(document.getElementById('txt_reader_no_fld').value);
	var txt_net_no_fld		= escape(document.getElementById('txt_net_no_fld').value);
	var txt_sts_fld 			= escape(document.getElementById('txt_sts_fld').value);
	var save_up 			= escape(document.getElementById('save_up').value);
	var cbo_date_format= escape(document.getElementById('cbo_date_format').value);
	var txt_id_fld= escape(document.getElementById('txt_id_fld').value);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	
	else if($('#txt_user_id').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_user_name').focus();
			$(this).html('Please Select a Valid User.').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_reader_model').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_reader_model').focus();
			$(this).html('Please Enter Reader Model or Group').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#cbo_db_type').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#cbo_db_type').focus();
			$(this).html('Please Select a Databse Type.').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_database_name').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_database_name').focus();
			$(this).html('Please Enter Database Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_table_name').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_table_name').focus();
			$(this).html('Please Enter Table Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_rf_code_fld').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_rf_code_fld').focus();
			$(this).html('Please Enter RF Code Field Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_date_fld').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_date_fld').focus();
			$(this).html('Please Enter Date Field Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_time_fld').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_time_fld').focus();
			$(this).html('Please Enter Time Field Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_reader_no_fld').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_reader_no_fld').focus();
			$(this).html('Please Enter Reader No. Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_net_no_fld').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_net_no_fld').focus();
			$(this).html('Please Enter Network No Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else if($('#txt_sts_fld').val()=="")
	{
		$("#messagebox").fadeTo(200,0.1,function()
		{
			//start fading the messagebox
			$('#txt_sts_fld').focus();
			$(this).html('Please Enter Status Field Name').addClass('messageboxerror').fadeTo(900,1);
		}
		);
	}
	else{	
		nocache = Math.random();
		http.open('get','../save_update_common.php?action=reader_conf&isupdate='+save_up+
		'&cbo_company_mst='+cbo_company_mst+
		'&txt_user_name='+txt_user_name+
		'&txt_user_id='+txt_user_id+
		'&txt_total_reader='+txt_total_reader+
		'&txt_reader_model='+txt_reader_model+
		'&cbo_db_type='+cbo_db_type+
		'&txt_server_name='+txt_server_name+
		'&txt_database_name='+txt_database_name+
		'&txt_table_name='+txt_table_name+
		'&txt_db_user='+txt_db_user+
		'&txt_db_password='+txt_db_password+
		'&txt_rf_code_fld='+txt_rf_code_fld+
		'&txt_date_fld='+txt_date_fld+
		'&txt_time_fld='+txt_time_fld+
		'&txt_reader_no_fld='+txt_reader_no_fld+
		'&txt_net_no_fld='+txt_net_no_fld+
		'&txt_sts_fld='+txt_sts_fld+
		'&txt_id_fld='+txt_id_fld+
		'&cbo_date_format='+cbo_date_format+
		'&nocache ='+nocache);
		http.onreadystatechange = reader_conf_info;
		http.send(null);
	}
}

function reader_conf_info()
{
	if(http.readyState == 4)
	{
		//var response = http.responseText;	
		//alert(response);
		var response = http.responseText.split('_');
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			}
			);
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"admin","reader_configuration","update","../");
				document.getElementById('save_up').value="";
			}
			);
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"admin","reader_configuration","insert","../");
				document.getElementById('save_up').value="";
			}
			);
		}
		//alert(response);
	}
}



function check_session_status()
{
	nocache = Math.random();
		http.open('get','save_update_common.php?action=check_session'+
		'&nocache ='+nocache);
		http.onreadystatechange = check_session_status_reply;
		http.send(null);
	
}

function check_session_status_reply()
{
	if(http.readyState == 4)
	{
		//var response = http.responseText;	
		//alert(response);
		var response = http.responseText;
		 if(response=="Close")
		 {
			window.location.href = "logout.php"; 
		 }
	}
}