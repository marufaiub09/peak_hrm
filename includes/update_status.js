function update_status( option, url, cache, tbl_name, id, menu, status_active ) {
	if( option == 'delete' ) var opt = 'delete';
	else if( status_active == 1 ) var opt = 'deactivate';
	else var opt = 'activate';
	if( menu == undefined ) menu = '';
	
	if( confirm( 'Are you sure to ' + opt + ' this?' ) ) {
		cache = (typeof cache == 'undefined') ? '' : cache;
		status_active = (typeof status_active == 'undefined') ? '' : status_active;
		$.ajax({
			type: "POST",
			url: url,
			data: 'update_status=' + option + '&tbl_name=' + tbl_name + '&id=' + id + '&status_active=' + status_active + '&cache=' + cache + '&menu=' + escape( menu ),
			success: function( data ) {
				var output = data.split('###');
				if( output[0] == 7 ) {
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html(output[1]).addClass('messagebox_ok').fadeTo( 900, 1 );
					});
					return false;
				}
				if( typeof window.reset_form == 'function' ) reset_form();
				populate_data();
				
				if( cache != '' ) {
					jQuery.globalEval( output[1] );
				}
				$("#messagebox").fadeTo( 200, 0.1, function() {
					$(this).html(output[0]).addClass('messagebox_ok').fadeTo( 900, 1 );
					$(this).fadeOut(5000);
				});
			}
		});
	}
}