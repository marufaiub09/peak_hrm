<?php
date_default_timezone_set('UTC');
include('includes/common.php');
/* Replace the data in these two lines with data for your db connection */
$type = $_GET["type"];
extract($_REQUEST);

	
	//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
	
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = mysql_real_escape_string($row['custom_designation']);
	}


if( isset( $_GET['getClientId'] ) ) {
	if( $type == "get_password" ) {
		
		echo "document.getElementById('txt_old_passwd').value = '" . encrypt( $_GET['getClientId'] ) . "';\n";
		die;
			 
	}
	if( $type == "get_Ptext" ) {
		
		echo "document.getElementById('txt_old_passwd').value = '" . decrypt( $_GET['getClientId'] ) . "';\n";
		die;
			 
	}
	if( $type == 1 ) {
		$res = mysql_query( "select * from main_module where m_mod_id = '" . $_GET['getClientId'] . "'" ) or die( mysql_error() );
		if( $inf = mysql_fetch_array( $res ) ) {
			echo "formObj.txt_module_name.value = '" . mysql_real_escape_string( $inf["main_module"] ) . "';\n";
			echo "formObj.txt_module_link.value = '" . mysql_real_escape_string( $inf["file_name"] ) . "';\n";
			echo "formObj.txt_module_seq.value = '" . $inf["mod_slno"] . "';\n";
			echo "formObj.save_up.value = '" . $inf["m_mod_id"] . "';\n";
		}
	}
	if( $type == 2 ) {
		$sql = "SELECT t1.*, t2.id, t2.show_priv, t2.delete_priv, t2.save_priv, t2.edit_priv, t2.approve_priv
				FROM main_menu AS t1
				LEFT JOIN user_priv_mst AS t2 ON t1.m_menu_id = t2.main_menu_id
				WHERE t1.m_menu_id = '" . $_GET['getClientId'] . "' AND t2.user_id = " . $_GET['user_id'];
		$res = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( $inf = mysql_fetch_assoc( $res ) ) {
			if( $inf['root_menu'] == 0 && $inf['sub_root_menu'] == 0 ) {
				$cbo_main_menu_name = $inf['m_menu_id'];
				$cbo_sub_main_menu_name = 0;
				$cbo_sub_menu_name = 0;
			}
			else if( $inf['sub_root_menu'] == 0 ) {
				$cbo_main_menu_name = $inf['root_menu'];
				$cbo_sub_main_menu_name = $inf['m_menu_id'];
				$cbo_sub_menu_name = 0;
			}
			else {
				$cbo_main_menu_name = $inf['root_menu'];
				$cbo_sub_main_menu_name = $inf['sub_root_menu'];
				$cbo_sub_menu_name = $inf['m_menu_id'];
			}
			echo "document.forms['$_GET[form_id]'].save_up.value = $inf[id];\n";
			echo "document.forms['$_GET[form_id]'].cbo_main_menu_name.value = $cbo_main_menu_name;\n";
			echo "load_drop_down( $cbo_main_menu_name, '3', 'cbo_sub_main_menu_name' );\n";
			echo "setTimeout( \"document.forms['$_GET[form_id]'].cbo_sub_main_menu_name.value = $cbo_sub_main_menu_name;\", 300 );\n"; 
			echo "load_drop_down( $cbo_sub_main_menu_name, '4', 'cbo_sub_menu_name' );\n";
			echo "setTimeout( \"document.forms['$_GET[form_id]'].cbo_sub_menu_name.value = $cbo_sub_menu_name;\", 300 );\n";
			echo "document.forms['$_GET[form_id]'].cbo_visibility.value = $inf[show_priv];\n";
			echo "document.forms['$_GET[form_id]'].cbo_insert.value = $inf[save_priv];\n";
			echo "document.forms['$_GET[form_id]'].cbo_edit.value = $inf[edit_priv];\n";
			echo "document.forms['$_GET[form_id]'].cbo_delete.value = $inf[delete_priv];\n";
			echo "document.forms['$_GET[form_id]'].cbo_approve.value = $inf[approve_priv];\n";
		}
	}
	if( $type == 3 ) {
		$sql = "select a.*, b.custom_designation from hrm_employee a, lib_designation b where a.designation_id = b.id and emp_code = '" . $_GET['getClientId'] . "'";
		$res = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		if( $inf = mysql_fetch_array( $res ) ) {
			echo "formObj.txt_emp_name.value = '" . mysql_real_escape_string( $inf["first_name"] ) . " " . mysql_real_escape_string( $inf["middle_name"] ) . " " . mysql_real_escape_string( $inf["last_name"] ) . "';\n";
			echo "formObj.txt_emp_name.title = '" . mysql_real_escape_string( $inf["first_name"] ) . " " . mysql_real_escape_string( $inf["middle_name"] ) . " " . mysql_real_escape_string( $inf["last_name"] ) . "';\n";
			echo "formObj.txt_desig.value = '" . mysql_real_escape_string( $inf["custom_designation"] ) . "';\n";
			echo "formObj.txt_dept.value = '" . mysql_real_escape_string( $inf["m_mod_id"] ) . "';\n";
			
			$res = mysql_query( "select * from user_passwd where employee_id = '" . $_GET['getClientId'] . "'" ) or die( mysql_error() );
			if( $inf2 = mysql_fetch_array( $res ) ) {
				echo "formObj.txt_user_id.value = '" . mysql_real_escape_string( $inf2["user_name"] ) . "';\n";
				echo "formObj.txt_passwd.value = '" . mysql_real_escape_string( $inf2["password"] ) . "';\n";
				echo "formObj.cbo_user_level.value = '" . mysql_real_escape_string( $inf2["user_level"] ) . "';\n";
				echo "formObj.cbo_unit_name.value = '" . $inf2["unit_id"] . "';\n";
				echo "formObj.txt_exp_date.value = '" . $inf2["expire_on"] . "';\n";
				echo "formObj.txt_ip_addres.value = '" . $inf2["access_ip"] . "';\n";
				echo "formObj.cbo_user_sts.value = '" . $inf2["valid"] . "';\n";
				echo "formObj.save_up.value = '" . $inf2["id"] . "';\n";
			}
			else {
				echo "formObj.txt_user_id.value = '';\n";  
				echo "formObj.txt_passwd.value = '';\n";
				echo "formObj.cbo_user_level.value = '';\n";
				echo "formObj.save_up.value = '';\n";
			}
		} 
		else {
			echo "formObj.txt_emp_name.value = '';\n";  
			echo "formObj.txt_desig.value = '';\n";
			echo "formObj.txt_dept.value = '';\n";
		}
	}
	if( $type == 4 ) {
		$sql = "SELECT * FROM main_menu WHERE m_menu_id = $_GET[getClientId]";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$row = mysql_fetch_assoc( $result );
		
		echo "document.forms['$_GET[form_id]'].save_up.value = $_GET[getClientId];\n";
		echo "document.forms['$_GET[form_id]'].cbo_module_name.value = $row[m_module_id];\n";
		echo "document.forms['$_GET[form_id]'].txt_menu_name.value = '$row[menu_name]';\n";
		echo "document.forms['$_GET[form_id]'].txt_menu_link.value = '$row[f_location]';\n";
		echo "load_drop_down( $row[m_module_id], '1', 'rootdiv' );\n";
		echo "setTimeout( \"document.forms['$_GET[form_id]'].cbo_root_menu.value = $row[root_menu];\", 300 );\n"; 
		echo "load_drop_down( $row[root_menu], '2', 'subrootdiv' );\n";
		echo "setTimeout( \"document.forms['$_GET[form_id]'].cbo_root_menu_under.value = $row[sub_root_menu];\", 300 );\n";
		echo "document.forms['$_GET[form_id]'].txt_menu_seq.value = '$row[slno]';\n";
		echo "document.forms['$_GET[form_id]'].cbo_menu_sts.value = '$row[status]';\n";
		if( $row['self_service'] == 1 ) echo "$('#chk_self_service').attr( 'checked', true );\n";
	}
	if($type == "user_id_update")
	{
		//echo "select * from user_passwd where user_name = '" . $_GET['getClientId'] . "'"; die;
		$res = mysql_query( "select * from user_passwd where user_name = '" . $_GET['getClientId'] . "'" ) or die( mysql_error() );
			if( $inf2 = mysql_fetch_array( $res ) ) {
				echo "formObj.txt_user_id.value = '" . mysql_real_escape_string( $inf2["user_name"] ) . "';\n";
				echo "formObj.txt_passwd.value = '" . mysql_real_escape_string( decrypt($inf2["password"]) ) . "';\n";
				echo "formObj.txt_conf_passwd.value = '" . mysql_real_escape_string( decrypt($inf2["password"]) ) . "';\n";
				echo "formObj.cbo_user_level.value = '" . mysql_real_escape_string( $inf2["user_level"] ) . "';\n";
				echo "formObj.cbo_user_buyer.value = '" . mysql_real_escape_string( $inf2["buyer_id"] ) . "';\n";
				echo "formObj.cbo_unit_name.value = '" . $inf2["unit_id"] . "';\n";
				echo "formObj.txt_exp_date.value = '" . convert_to_mysql_date($inf2["expire_on"] ). "';\n";
				echo "formObj.txt_ip_addres.value = '" . $inf2["access_ip"] . "';\n";
				echo "formObj.cbo_data_level_sec.value = '" . $inf2["is_data_level_secured"] . "';\n";
				echo "formObj.cbo_user_sts.value = '" . $inf2["valid"] . "';\n";
				echo "formObj.save_up.value = '" . $inf2["id"] . "';\n";
				 //echo "show_hide_buyer($inf2[user_level]);";
				 //$company_details[$row['id']]
				 
				 $unit_id=explode(",",$inf2["unit_id"]);
				 $company_name='';
				 for($i=0; $i<count($unit_id); $i++)
				 {
					 if( $company_name=='')  $company_name=$company_details[$unit_id[$i]]; else $company_name .=','.$company_details[$unit_id[$i]];
				 }
				 echo "formObj.cbo_unit_name_show.value = '" . $company_name . "';\n";
				 
			}
	}
}

// 
if($type == "type_user_creation")
{
	//echo $user_id."==su..re";die;
	//echo "select * from user_passwd where user_name = '" . $_GET['getClientId'] . "'"; die;
	$sql= "select * from user_passwd where user_name = '".$getClientId."'";
	$result = mysql_query($sql ) or die( mysql_error() );
	if( $inf2 = mysql_fetch_array( $result ) ) 
	{
		//echo $inf2["expire_on"]."==su..re";die;
		echo "formObj.txt_user_id.value = '" . mysql_real_escape_string( $inf2["user_name"] ) . "';\n";
		echo "formObj.txt_expiry_date.value = '" .mysql_real_escape_string( convert_to_mysql_date($inf2["expire_on"] )). "';\n";
		echo "formObj.txt_password.value = '" . mysql_real_escape_string( decrypt($inf2["password"]) ) . "';\n";
		echo "formObj.txt_confirm_password.value = '" . mysql_real_escape_string( decrypt($inf2["password"]) ) . "';\n";
		echo "formObj.cbo_user_level.value = '" . mysql_real_escape_string( $inf2["user_level"] ) . "';\n";
		echo "formObj.cbo_data_level_security.value = '" . $inf2["is_data_level_secured"]. "';\n";
		echo "formObj.txt_ip_addres.value = '" . $inf2["access_ip"] . "';\n";
		echo "formObj.cbo_user_status.value = '" . $inf2["valid"] . "';\n";
		/*
		echo "multi_up_check('cbo_company_id','".$inf2['company_id']."');\n"; 
		echo "multi_up_check('cbo_location_id','".$inf2['location_id']."');\n";
		echo "multi_up_check('cbo_division_id','".$inf2['division_id']."');\n";
		echo "multi_up_check('cbo_department_id','".$inf2['department_id']."');\n";
		echo "multi_up_check('cbo_section_id','".$inf2['section_id']."');\n";
		echo "multi_up_check('cbo_subsection_id','".$inf2['subsection_id']."');\n";
		*/
		echo "formObj.txt_emp_code.value = '" . $inf2["emp_code"] . "';\n";
		echo "formObj.txt_update_id.value = '" . $inf2["id"] . "';\n";
		echo "formObj.txt_user_name.value = '".return_field_value("CONCAT(first_name, ' ', middle_name, ' ', last_name) AS name","hrm_employee","emp_code='".$inf2[emp_code]."'")."';\n";
		echo "formObj.txt_designation.value = '" .$designation_chart[return_field_value("designation_id","hrm_employee","emp_code='".$inf2[emp_code]."'")] . "';\n";
		//echo "formObj.cbo_self_service.value = '" . $inf2["self_service"] . "';\n";
		echo "formObj.txt_email_address.value = '" . $inf2["email_address"] . "';\n";
	}
}

if($type == "tab_load_data_form")
	{
		$user_id = $_GET["user_id"];
		$id = $_GET["id"];
		$user_id = $_GET["user_id"];
		$getClientId = $_GET["getClientId"];
		//echo "select * from user_passwd where user_name = '" . $_GET['getClientId'] . "'"; die;
		$res = mysql_query( "select * from user_priv_tab where  id='$id' and user_id = '$getClientId' " ) or die( mysql_error() );
			if( $inf2 = mysql_fetch_array( $res ) ) {
				echo "document.forms['$_GET[form_id]'].cbo_user_name.value 		= '" . mysql_real_escape_string( $inf2["user_id"] ) . "';\n";
				echo "document.forms['$_GET[form_id]'].cbo_form_name.value	 	= '" . mysql_real_escape_string($inf2["form_id"]) . "';\n";
				//load_drop_down(this.value,'tab_dropdown','tab_name_td');
				//echo "load_drop_down( $inf2[form_id], 'tab_dropdown', 'tab_name_td' );\n";
				//echo "document.forms['$_GET[form_id]'].cbo_tab_name.value 		= '" . $inf2["tab_id"] . "';\n";
				//echo "$('#cbo_tab_name').val('". $inf2["tab_id"]."' );\n";
				
				echo "document.forms['$_GET[form_id]'].cbo_visibility.value 	= '" . $inf2["visibility"] . "';\n";
				echo "document.forms['$_GET[form_id]'].cbo_insert.value 		= '" . $inf2["insert_id"] . "';\n";
				echo "document.forms['$_GET[form_id]'].cbo_edit.value 			= '" . $inf2["edit_id"] . "';\n";
				echo "document.forms['$_GET[form_id]'].cbo_delete.value 		= '" . $inf2["delete_id"]. "';\n";
				echo "document.forms['$_GET[form_id]'].cbo_approve.value 		= '" . $inf2["approve"] . "';\n";
				echo "document.forms['$_GET[form_id]'].is_update.value 			= '" . $inf2["id"] . "';\n";
				
				 //echo "show_hide_buyer($inf2[user_level]);";
				 
			}
	}
	
if($type == "change_main_module")
{
	//echo "su..re";
	echo "<option value=0>-- Select --</option>";
	$query="select * from main_menu where  m_module_id='$id' and status=1 and position='1' order by menu_name" ;
	//"select * from main_menu where position='1' and m_module_id='$module_id' and status=1 order by m_menu_id";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row[m_menu_id]."'>".$row[menu_name]."</option>";
	}		
	exit();
}
 
if($type == "change_main_menu")
{
	//echo "su..re";
	echo "<option value=0>-- Select --</option>";
	$query = "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY menu_name ASC";
	//$query="select * from main_menu where  m_module_id='$id' and status=1";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row[m_menu_id]."'>".$row[menu_name]."</option>";
	}		
	exit();
}

if($type == "change_sub_main_menu")
{
	//echo "su..re";
	echo "<option value=0>-- Select --</option>";
	//$sql= "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY m_menu_id ASC";
	$query = "select * from main_menu where sub_root_menu = '$id' and position = '3' and status=1 order by menu_name ASC";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row[m_menu_id]."'>".$row[menu_name]."</option>";
	}		
	exit();
}

// type_change_company
if($type == "type_change_company")
{
	//echo "su..re";
	echo "<option value=0>-- select --</option>";
	//$sql= "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY m_menu_id ASC";
	$query = "select id,location_name from lib_location where company_id = '$id' and status_active=1 order by location_name ASC";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row['id']."'>".$row['location_name']."</option>";
	}		
	exit();
}

// type_change_location
if($type == "type_change_location")
{
	//echo "su..re";
	echo "<option value=0>-- select --</option>";
	//$sql= "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY m_menu_id ASC";
	$query = "select id,division_name from lib_division where location_id = '$id' and status_active=1 order by division_name ASC";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row['id']."'>".$row['division_name']."</option>";
	}		
	exit();
}

// type_change_division
if($type == "type_change_division")
{
	//echo "su..re";
	echo "<option value=0>-- select --</option>";
	//$sql= "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY m_menu_id ASC";
	$query = "select id,department_name from lib_department where division_id = '$id' and status_active=1 order by department_name ASC";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row['id']."'>".$row['department_name']."</option>";
	}		
	exit();
}

// type_change_department
if($type == "type_change_department")
{
	//echo "su..re";
	echo "<option value=0>-- select --</option>";
	//$sql= "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY m_menu_id ASC";
	$query = "select id,section_name from lib_section where department_id = '$id' and status_active=1 order by section_name ASC";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row['id']."'>".$row['section_name']."</option>";
	}		
	exit();
}

// type_change_section
if($type == "type_change_section")
{
	//echo "su..re";
	echo "<option value=0>-- select --</option>";
	//$sql= "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY m_menu_id ASC";
	$query = "select id,subsection_name from lib_subsection where section_id = '$id' and status_active=1 order by subsection_name ASC";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row['id']."'>".$row['subsection_name']."</option>";
	}		
	exit();
}

// type_change_designation_level
if($type == "type_change_designation_level")
{
	//echo "su..re";
	echo "<option value=0>-- select --</option>";
	//$sql= "SELECT * FROM main_menu WHERE root_menu = '$id' AND position='2' and status=1 ORDER BY m_menu_id ASC";
	$query = "select id,custom_designation from lib_designation where level = '$id' and status_active=1 order by custom_designation ASC";
	$result=mysql_query($query) or die( mysql_error() );
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		echo "<option value='".$row['id']."'>".$row['custom_designation']."</option>";
	}		
	exit();
}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}?> 