<? 
include('includes/common.php');
include('includes/array_function.php');
include('includes/common_functions.php');
?>
<link href="reports/shipment/table.css" type="text/css" rel="stylesheet" />

<div align="left">
<h3 style="font-size:16px "><em style="font-size:24px; font-weight:bold ">T</em>op Buyer  Summary , 2011</h3>
<h2 style="font-size:16px "><em style="font-size:20px; font-weight:bold ">T</em>he Delta Composite Knitting Industries Ltd.</h2>
</div>
  <?
 $sql_summary="select *,lib_buyer.buyer_name as buyer_name_display,
lib_company.company_name as working_company_display,
sum(wp.po_quantity) as po_quantity,
sum(wp.po_total_price) as po_total_price 
from wo_po_break_down as wp,
wo_po_details_master as wm,
lib_buyer,
lib_company
 where wp.job_no_mst=wm.job_no 
and wp.shipment_date > '2011%' and wp.shipment_date < '2012%'
and wp.working_company=lib_company.id
and wm.buyer_name=lib_buyer.id
group by wm.buyer_name
 order  by po_total_price";
 
   $rs_summary=mysql_query($sql_summary);
   while($row_summary=mysql_fetch_array($rs_summary))
      { 
if($i<6) {
	   if($i%2==0) { $class="alt"; $th='specalt';} else { $class='';	 $th='spec'; }
	  $data=$data."<tr class='$class'><td class='$th'>".$row_summary['buyer_name_display']."</td><td>".number_format($row_summary['po_quantity'])."</td><td>".number_format($row_summary['po_total_price'])."</td></tr> ";
 	  $chart_data2=$chart_data2.$row_summary['buyer_name_display'].";".$row_summary['po_total_price']."\\n";	 
	  $i++;
	  $TotalPOQuantity+=$row_summary['po_quantity'];
	  $TotalTotalValue+=$row_summary['po_total_price'];
}
	  }
	  
 substr($data,0,-2);
if($data) {
echo "";
echo "<table  border='1'  id='mytable'>";
echo "
<tr  class='heading'><th>Buyer Name</th>
<th>Po Quantity</th>
<th>Value</th>
</tr>";
echo $data;
echo "</table>";
}


?>
<div>
<div  >

	<? if($chart_data2) { ?>

<script type="text/javascript" src="reports/amcharts/flash/swfobject.js"></script>

		<!-- following scripts required for JavaScript version. The order is important! -->
		<script type="text/javascript" src="reports/amcharts/javascript/amcharts.js"></script>
		<script type="text/javascript" src="reports/amcharts/javascript/amfallback.js"></script>
		<script type="text/javascript" src="reports/amcharts/javascript/raphael.js"></script>
		<div id="chartdiv44"  style="width:95%"></div>

	    <script type="text/javascript">
	    
            var params = {
                bgcolor:"#FFFFFF"
                };

		    var flashVars = {
		        path: "reports/amcharts/flash/",   
				chart_data: "<? echo substr($chart_data2,0,-2); ?>",
                chart_settings: "<settings><background><file>../../bg.gif</file></background><data_type>csv</data_type><legend><enabled>0</enabled></legend><pie><inner_radius>15</inner_radius><height>7</height><angle>10</angle><gradient></gradient></pie><animation><start_time>1</start_time><pull_out_time>1</pull_out_time></animation><data_labels><show>{title}</show><max_width>100</max_width></data_labels></settings>"
			};

            if (swfobject.hasFlashPlayerVersion("8")){
	    		swfobject.embedSWF("reports/amcharts/flash/ampie.swf", "chartdiv44", "700", "400", "8.0.0", "reports/amcharts/flash/expressInstall.swf", flashVars, params);
	    	}
			else{
				var amFallback = new AmCharts.AmFallback();
				// amFallback.settingsFile = flashVars.settings_file;  		// doesn't support multiple settings files or additional_chart_settins as flash does
				// amFallback.dataFile = flashVars.data_file;
				amFallback.chartSettings = flashVars.chart_settings;
				amFallback.chartData = flashVars.chart_data;
				amFallback.type = "pie";
				amFallback.write("chartdiv44");
			}

		</script>

</div>
<div >
<?
}
?>
</div>
</div>

