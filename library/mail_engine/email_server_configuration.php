<?php
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date :04/04/2012
		
######################################*/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../includes/common.php');
include('../../includes/array_function.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Email Server Configuration</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
	<script src="includes/ajax_submit_mail.js" type="text/javascript"></script>
	<script src="includes/functions_mail.js" type="text/javascript"></script>
    <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
	<script type="text/javascript" src="../../juery/lib.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	<script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>

<script>
function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length-1
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("@ - Missing")
		   return false
		}
		if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Before .(dot) - type a letter")
		    return false
		 }
		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert(".(dot) - Missing")
		   return false
		}
		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("After .(dot) type a letter")
		    return false
		}
 		 return true					
	}



function formValidation()
{
	
	var emailID=document.email_server_configuration.txt_sender_email;
	if ((emailID.value==null)||(emailID.value=="")){
		alert("Please Enter your Email ID")
		emailID.focus()
		return false
	}
	if (echeck(emailID.value)==false){
		//emailID.value=""
		emailID.focus()
		return false
	}
}

function reset_button()
{
	document.getElementById('save_up').value="";
}
</script>

</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:900px;">
    	<div>
            <div class="form_caption">
                Email Server Configuration
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update.$delete;
                ?>
           </span>
			<div id="messagebox" style="background-color:#FF9999; color:#000000; width:700px" align="center"></div>
		</div>
		<!-- Start Field Set -->
		<fieldset style="width:660px ">
			<legend>Email Server Configuration</legend>
			<!-- Start Form -->
			<form name="email_server_configuration" id="email_server_configuration" method="" autocomplete="off" action="javascript:fnc_email_server_configuration(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="return formValidation()" >
				<fieldset>
					<div style="width:660px; overflow:auto" >
						<table width="100%">
							<tr>
								<td width="130px">POP3 Server</td>
								<td width="200px">
									<input type="text" name="txt_pop3_server" id="txt_pop3_server" class="text_boxes" style="width:200px" />
								</td>
                                <td width="130px">SMTP Server</td>
								<td width="200px">
									<input type="text" name="txt_smtp_server" id="txt_smtp_server" class="text_boxes" style="width:200px" />
								</td>
							</tr>
                            <tr>
								<td width="130px">Domain</td>
								<td width="200px">
									<input type="text" name="txt_domain" id="txt_domain" class="text_boxes" style="width:200px" />
								</td>
                                <td width="130px">Sender Mail</td>
								<td width="200px">
									<input type="text" name="txt_sender_email" id="txt_sender_email" class="text_boxes" style="width:200px" />
								</td>
                           	</tr>
							<tr>
								<td width="130px">Status</td>
                                <td width="200px">
                                    <select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:210px" >
                                     <?php
                                        foreach($status_active as $key=>$value):
                                        ?>
                                        <option value=<? echo "$key";?> ><? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>
                                    </select>
                                </td>
                                <td width="130px">Delete</td>
                                <td width="200px">
                                    <select name="cbo_is_deleted" id="cbo_is_deleted" class="combo_boxes" style="width:210px" >
                                        <?php
                                        foreach($is_deleted as $key=>$value):
                                        ?>
                                        <option value=<? echo "$key";
                                        if ($cbo_po_status2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>						
                                    </select>	
                                </td>
							</tr>
                            <tr>
                                <td colspan="4" align="center">&nbsp;						
                                    <input type="hidden" name="save_up" id="save_up" >
                                </td>					
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="button_container">
                                    <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                                    <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton" onClick="reset_button()"/>	
                                </td>				
                            </tr>
						</table>
					</div>
				</fieldset>
                <div style="width:660px; float:left; margin:auto" align="center">
                    <fieldset style="width:660px; margin-top:10px">
                        <legend>Press space for mail server configuration  Info</legend>
                            <input type="text" size="30" class="text_boxes" onKeyUp="showResult(this.value,'3','mail_server_cont');" />
                            <div style="width:660px; margin-top:10px" id="mail_server_cont" align="left"></div>
                    </fieldset>	
                </div>	
				<!--<div style="height:10px;"></div>
				<fieldset>
					<div style="width:660px;" >
						<table cellspacing="1" width="100%">
                            <tr>
                                <td>
                                    <div id="mail_server_cont">
                                    </div>
                                </td>
                            </tr>		
						</table>
					</div>
				</fieldset>-->
				
			</form>
			<!-- End Form -->
		</fieldset>
		<!-- End Field Set -->
	</div>
</body>
</html>


