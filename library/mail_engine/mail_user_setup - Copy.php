<?php
//Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Mail User Setup</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script src="includes/ajax_submit_mail.js" type="text/javascript"></script>
<script src="includes/functions_mail.js" type="text/javascript"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>

<script>

		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
function echeck(str) {	
		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length-1
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("@ - Missing")
		   return false
		}
		if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Before .(dot) - type a letter")
		    return false
		 }
		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert(".(dot) - Missing")
		   return false
		}
		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("After .(dot) type a letter")
		    return false
		}
 		 return true					
	}



function formValidation()
{
	
	var emailID=document.mail_user_setup.txt_email_address
	if ((emailID.value==null)||(emailID.value=="")){
		alert("Please Enter your Email ID")
		emailID.focus()
		return false
	}
	if (echeck(emailID.value)==false){
		//emailID.value=""
		emailID.focus()
		return false
	}
}


function search_populate(str)
{
	//alert(str);
	if(str==0)
	{
		
		document.getElementById('search_by_td').innerHTML='<input	type="text"	name="recipient_name" style="width:390px " class="text_boxes"	id="recipient_name"	value="" onkeyup="showResult_multi(this.value+\'_\'+'+document.getElementById('id_field').value+','+document.getElementById('recipient_type').value+',\'yarn_receive_search\',\'search_div\')" />';
	}
	else if(str==1)
	{
		var team_member_name = '<option value="0">----Select Team Member----</option>';
		<?php
		$company_sql= mysql_db_query($DB, "select id, team_member_name from lib_marketing_team_member_info where is_deleted=0  and status_active=1 order by id");
		while ($row=mysql_fetch_array($company_sql))
		{
			echo "team_member_name += '<option value=\"$row[id]\">".mysql_real_escape_string($row['team_member_name'])."</option>';";
		}
		?>
		
		document.getElementById('search_by_td').innerHTML='<select	name="recipient_name" style="width:400px " class="combo_boxes" id="recipient_name"	onchange="showResult_multi(this.value+\'_\'+'+document.getElementById('id_field').value+','+document.getElementById('recipient_type').value+',\'yarn_receive_search\',\'search_div\')" >'+ team_member_name +'</select>';
	}
	else if(str==2)
	{
		var user_name = '<option value="0">--- Select User ---</option>';
		<?php
		$company_sql= mysql_db_query($DB, "select id, user_name from user_passwd order by id");
		while ($row=mysql_fetch_array($company_sql))
		{
			echo "user_name += '<option value=\"$row[id]\">".mysql_real_escape_string($row['user_name'])."</option>';";
		}
		?>		
		
		document.getElementById('search_by_td').innerHTML='<select	name="recipient_name"	style="width:400px " class="combo_boxes" id="recipient_name"	onchange="showResult_multi(this.value+\'_\'+'+document.getElementById('id_field').value+','+document.getElementById('recipient_type').value+',\'yarn_receive_search\',\'search_div\')" >'+ user_name +'</select>';
	}
	
}
function js_set_value(str)
{
	document.getElementById('id_field').value=str;
}
function reset_all()
{
	document.getElementById('save_up').value="";
	document.getElementById('txt_mst_id').value="";
	document.getElementById('mail_list_view').innerHTML="";
}
</script>


</head>
<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px;">
	 <div>
    	<div class="form_caption">
		Mail User Setup
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000; width:620px" align="center"></div>
	</div>
   
  <fieldset style="width:600px;">
		<legend>Mail User Setup</legend>
		<form name="mail_user_setup" id="mail_user_setup" method="" action="javascript:fnc_mail_user_setup(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="return formValidation()">	
			<table cellpadding="0" cellspacing="2" width="100%">
				<tr>
                    <td width="130" align="center">
                        Recipient Type
                    </td>
					<td width="400" colspan="3">
                        <select name="recipient_type" id="recipient_type" class="combo_boxes" style="width:400px" onchange="search_populate(this.value)">
                           <?php
								foreach($mail_recipient_type as $key=>$value):
								?>
								<option value=<? echo "$key";
								if ($cbo_po_status2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
								<?		
								endforeach;
							?>
                        </select>
					</td><input type="hidden" name="id_field" id="id_field" value="<? echo $company; ?>" />
				</tr>
				<tr>
					<td id="search_by_td_up" width="130" align="center" >Recipient Name</td>
					<td id="search_by_td" width="400" colspan="3">
						<input type="text" name="recipient_name" id="recipient_name" class="text_boxes" style="width:390px" onkeyup="showResult_multi(this.value+'_'+document.getElementById('id_field').value, document.getElementById('recipient_type').value,'yarn_receive_search','search_div')" />
                	</td>
				</tr>
				<tr>
					<td width="130" align="center">Email Address</td>
					<td width="400" colspan="3"><input type="text" name="txt_email_address" id="txt_email_address" class="text_boxes" style="width:390px" /></td>
				</tr>
				<tr>
					<td width="130" align="center">
						Delete
					</td>
					<td width="125">
						<select name="cbo_is_deleted" id="cbo_is_deleted" class="combo_boxes" style="width:125px" >
							<?php
							foreach($is_deleted as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($cbo_po_status2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>						
						</select>	
					</td>
					<td width="70">
						Status
					</td>
					<td width="125">
						<select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:125px" ><!--onFocus="javascript:hide_div('div_user_id');"-->
							<?php
                                foreach($status_active as $key=>$value):
                                ?>
                                <option value=<? echo "$key";
                                if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                <?		
                                endforeach;
                            ?>
						</select>
					 
					</td>
				</tr>
				<tr>
					<td colspan="4" align="center">&nbsp;						
						<input type="hidden" name="save_up" id="save_up" >
						<input type="hidden" name="txt_mst_id" id="txt_mst_id">	
					</td>					
				</tr>
				<tr>
                    <td colspan="4" align="center" class="button_container">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton" onClick="reset_all();"/>	
                    </td>				
                </tr>
			</table> 
		
		</form>
	</fieldset>
	<div style="width:900px; float:left; margin:auto" align="center">
		<fieldset style="width:600px; margin-top:10px">
			<legend>Press space for mail user Info</legend>
			<form>
				<input type="text" size="30" class="text_boxes" onKeyUp="showResult(this.value,'1','mail_list_view')" />
				<div style="width:600px; margin-top:10px" id="mail_list_view" align="left"></div>
			</form>
		</fieldset>	
	</div>
</div>
</body>
</html>
	
			