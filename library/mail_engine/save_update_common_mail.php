<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
$date=time();
$user_name=$_SESSION['logic_erp']["user_name"];
$date_time= date("Y-m-d H:i:s");

include('../../includes/common.php');	
extract($_GET);

//--------------------------Mail User setup Start-----------------------------------------------------------------------

if ($action=="mail_user_setup"){ 
   if  ($isupdate==""){//Insert Here if  ($txt_projected_ref_no!="")
		if  ($recipient_type==0){
   
			$unique_field_name = "recipient_name";
			$table_name = "mail_user_setup";
			$query = "recipient_name='$recipient_name'";
			
			$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
			if( $unique_check == false )  // Duplicate
			{
				echo "1";
				exit();
			}
			
			$id_field_name = "id";
			$table_name = "mail_user_setup";
			$id= return_next_id($id_field_name,$table_name);
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
	
	
			$sql = "INSERT INTO mail_user_setup (
								id,
								recipient_type,
								recipient_name,
								email_address,
								inserted_by,
								insert_date,
								updated_by,
								status_active,
								is_deleted
							) VALUES (
								'$id',
								'".mysql_real_escape_string($recipient_type)."',
								'".mysql_real_escape_string($recipient_name)."',
								'".mysql_real_escape_string($txt_email_address)."',
								'".mysql_real_escape_string($user_name)."',
								'$date_time',
								'$updated_by',
								'$cbo_status',
								'$cbo_is_deleted'
							)";
			mysql_query( $sql ) or die (mysql_error());	
			//echo "2_"."$id"."_".encrypt($sql,"logic_erp_2011_2012");
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo "2_"."$id";
			exit();
		}
		else{
			$unique_field_name = "recipient_id";
			$table_name = "mail_user_setup";
			$query = "recipient_id='$recipient_name'";
			
			$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
			if( $unique_check == false )  // Duplicate
			{
				echo "1";
				exit();
			}
			
			$id_field_name = "id";
			$table_name = "mail_user_setup";
			$id= return_next_id($id_field_name,$table_name);
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
	
	
			$sql = "INSERT INTO mail_user_setup (
								id,
								recipient_type,
								recipient_id,
								email_address,
								inserted_by,
								insert_date,
								updated_by,
								status_active,
								is_deleted
							) VALUES (
								'$id',
								'".mysql_real_escape_string($recipient_type)."',
								'".mysql_real_escape_string($recipient_name)."',
								'".mysql_real_escape_string($txt_email_address)."',
								'".mysql_real_escape_string($user_name)."',
								'$date_time',
								'$updated_by',
								'$cbo_status',
								'$cbo_is_deleted'
							)";
			mysql_query( $sql ) or die (mysql_error());	
			//echo "2_"."$id"."_".encrypt($sql,"logic_erp_2011_2012");
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo "2_"."$id";
			exit();
			}

	} else {
			if  ($recipient_type==0)
				{
					mysql_query("SET CHARACTER SET utf8");
					mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
							
					$sql = "UPDATE mail_user_setup
							SET
								recipient_type	= '".mysql_real_escape_string($recipient_type)."',
								recipient_name	= '".mysql_real_escape_string($recipient_name)."',
								email_address	= '".mysql_real_escape_string($txt_email_address)."',
								updated_by		= '".mysql_real_escape_string($user_name)."',
								update_date 	= '$date_time',
								status_active	= '$cbo_status',
								is_deleted		= '$cbo_is_deleted'
								WHERE id = '$isupdate'
							 ";
					mysql_query( $sql ) or die (mysql_error());
					$sql_history=encrypt($sql, "logic_erp_2011_2012");
					$_SESSION['sql_history']=$sql_history;
					//echo "3_"."$isupdate"."_".encrypt($sql,"logic_erp_2011_2012");		
					echo "3";
					exit();
				}
				else
				{
					mysql_query("SET CHARACTER SET utf8");
					mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
							
					$sql = "UPDATE mail_user_setup
							SET
								recipient_type	= '".mysql_real_escape_string($recipient_type)."',
								recipient_id	= '".mysql_real_escape_string($recipient_name)."',
								email_address	= '".mysql_real_escape_string($txt_email_address)."',
								updated_by		= '".mysql_real_escape_string($user_name)."',
								update_date 	= '$date_time',
								status_active	= '$cbo_status',
								is_deleted		= '$cbo_is_deleted'
								WHERE id = '$isupdate'
							 ";
					mysql_query( $sql ) or die (mysql_error());
					$sql_history=encrypt($sql, "logic_erp_2011_2012");
					$_SESSION['sql_history']=$sql_history;
					//echo "3_"."$isupdate"."_".encrypt($sql,"logic_erp_2011_2012");
					echo "3";		
					exit();
				}
			}
}
//--------------------------Mail User setup End-----------------------------------------------------------------------

//--------------------------Mail Group setup Start-----------------------------------------------------------------------

if ($action=="mail_group")
{ 
	if  ($isupdate=="")
	{//Insert Here if  ($txt_projected_ref_no!="")
		
		if($cbo_old_group==0)
		{
			$unique_field_name = "group_name";
			$table_name = "mail_group_mst";
			$query = "group_name='$txt_new_group'";
			
			$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
			if( $unique_check == false )  // Duplicate
			{
				echo "1";
				exit();
			}
			
			$id_field_name = "id";
			$table_name = "mail_group_mst";
			$id= return_next_id($id_field_name,$table_name);
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
		
		
			$sql = "INSERT INTO mail_group_mst (
								id,
								group_name,
								inserted_by,
								insert_date,
								updated_by,
								status_active,
								is_deleted
							) VALUES (
								'$id',
								'".mysql_real_escape_string($txt_new_group)."',
								'".mysql_real_escape_string($user_name)."',
								'$date_time',
								'$updated_by',
								'$cbo_status',
								'&cbo_is_deleted'
							)";
			mysql_query( $sql ) or die (mysql_error());	
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo "2_"."$id";
			exit();
		 
		}
		else
		{
			if ($txt_selected_id!="")
			{
				$id_grp=$cbo_old_group;
				$a=mysql_query("delete from  mail_group_child where mail_group_mst_id='$id_grp'") or die (mysql_error());
			
			$id_grp=$cbo_old_group; 
			$mail_user_group=explode(",",$txt_selected_id);
			$mail_user_group1=count($mail_user_group)-1;
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			$sql_all="";
			for($i=0; $i<=$mail_user_group1; $i++)
			{
				if ( $chk_dupl==false) // Insert mail_user_setup_id  cond=="mail_group_mst_id='' and mail_user_setup_id=''"
				{
					$id_field_name = "id";
					$table_name = "mail_group_child";
					$id= return_next_id($id_field_name,$table_name);
					
					$sql2="INSERT INTO mail_group_child(
							id,
							mail_group_mst_id,
							mail_user_setup_id,
							inserted_by,
							insert_date,
							updated_by,
							status_active
						) VALUES(
							'$id',
							'$id_grp',
							'$mail_user_group[$i]',
							'".mysql_real_escape_string($user_name)."',
							'$date_time',
							'$updated_by',
							1
							)";	
					mysql_query( $sql2 ) or die (mysql_error());
					$sql_all.=$sql2;

				}
				else  // Update
				{
					
				}
				
			}
			$sql_history=encrypt($sql_all, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo "2_"."$txt_selected_id";
			exit();
			}
		}
			
	} // Update else
}

//--------------------------Mail group setup End-----------------------------------------------------------------------

//--------------------------Mail Server Configure Start-----------------------------------------------------------------------

if ($action=="email_server_configuration")
{ 
	
	if  ($isupdate=="")
	{
	
		$id_field_name = "id";
		$table_name = "mail_server_configure";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
	
		$query="select * from mail_server_configure where pop3_server='$txt_pop3_server' and smtp_server='$txt_smtp_server' and is_deleted=0";
		$result=mysql_query($query);
		if(mysql_num_rows($result)==0)
		{
			$sql = "INSERT INTO mail_server_configure (
							id,
							pop3_server,
							smtp_server,
							domain_name,
							sender_mail,
							inserted_by,
							insert_date,
							updated_by,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'".mysql_real_escape_string($txt_pop3_server)."',
							'".mysql_real_escape_string($txt_smtp_server)."',
							'".mysql_real_escape_string($txt_domain)."',
							'".mysql_real_escape_string($txt_sender_email)."',
							'".mysql_real_escape_string($user_name)."',
							'$date_time',
							'$updated_by',
							'$cbo_status',
							'$cbo_is_deleted'
						)";
		
				mysql_query( $sql ) or die (mysql_error());	
				$sql_history=encrypt($sql, "logic_erp_2011_2012");
				$_SESSION['sql_history']=$sql_history;
				echo "1"."_";
				//echo "1"."_".encrypt($sql,"logic_erp_2011_2012");
				exit();
		}
		else
		{
			echo "4";	
			exit();
		}
	}
	else {
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			
			if($cbo_is_deleted==1)
			{
				$query="UPDATE mail_server_configure SET updated_by	= '".mysql_real_escape_string($user_name)."',update_date = '$date_time',is_deleted=1 WHERE id = '$isupdate'";
				mysql_query( $query ) or die (mysql_error());
				$sql_history=encrypt($query, "logic_erp_2011_2012");
				$_SESSION['sql_history']=$sql_history;
				echo "3";
				exit();
			}
			else{
		
				$sql = "UPDATE mail_server_configure
						SET
							pop3_server	= '".mysql_real_escape_string($txt_pop3_server)."',
							smtp_server = '".mysql_real_escape_string($txt_smtp_server)."',
							domain_name	= '".mysql_real_escape_string($txt_domain)."',
							sender_mail = '".mysql_real_escape_string($txt_sender_email)."',
							updated_by	= '".mysql_real_escape_string($user_name)."',
							update_date = '$date_time',
							status_active= '$cbo_status'
							WHERE id = '$isupdate'
						 ";
						  
				mysql_query( $sql ) or die (mysql_error());
				$sql_history=encrypt($sql, "logic_erp_2011_2012");
				$_SESSION['sql_history']=$sql_history;
				echo "2";
				//echo "2"."_".encrypt($sql,"logic_erp_2011_2012");		
				exit();
			}
		}
	
}
//--------------------------Mail Server Configure End-----------------------------------------------------------------------


function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}

?>