//Do not change 
function createObject() {
var request_type;
var browser = navigator.appName;
if(browser == "Microsoft Internet Explorer"){
request_type = new ActiveXObject("Microsoft.XMLHTTP");
}else{
request_type = new XMLHttpRequest();
}
return request_type;
}

var http = createObject();

//--------------------------Mail User setup Start-----------------------------------------------------------------------
function fnc_mail_user_setup(save_perm,edit_perm,delete_perm,approve_perm){
	var txt_mst_id			= escape(document.getElementById('txt_mst_id').value);
	var recipient_type		= escape(document.getElementById('recipient_type').value);
	var recipient_name		= escape(document.getElementById('recipient_name').value);
	var txt_email_address	= escape(document.getElementById('txt_email_address').value);
	var cbo_is_deleted		= escape(document.getElementById('cbo_is_deleted').value);
	var cbo_status			= escape(document.getElementById('cbo_status').value);
	var save_up 			= escape(document.getElementById('save_up').value);
	//alert(recipient_name);
	//return false;
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( (cbo_status==0 || cbo_is_deleted==1) && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common_mail.php?action=mail_user_setup&isupdate='+save_up+
					'&txt_mst_id='+txt_mst_id+
					'&recipient_type='+recipient_type+
					'&recipient_name='+recipient_name+
					'&txt_email_address='+txt_email_address+
					'&cbo_is_deleted='+cbo_is_deleted+
					'&cbo_status='+cbo_status+
					'&nocache ='+nocache);
		http.onreadystatechange = mail_user_setupReply_info;
		http.send(null); 
	}
}

function mail_user_setupReply_info() {
	if(http.readyState == 4){ 		
		//var response = http.responseText;	
		 
		var response = http.responseText.split('_');	
		//if( response[0] == 3 )
	// alert(response);
	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Recipient name of this ID, Please use New Recipient name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history('',"library","mail_user_setup","update","../../");
				showResult(' ','1','mail_list_view');
				//(response[1],'5','bank_list_view');
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history('',"library","mail_user_setup","insert","../../");
				showResult(' ','1','mail_list_view');
				document.getElementById('save_up').value="";
			});
		}
		
		
	//alert(response);
	
	}
}	

//-------------------------Mail User setup End-----------------------------------------------------------------------

//--------------------------Mail Group setup Start-----------------------------------------------------------------------
function fnc_mail_group(save_perm,edit_perm,delete_perm,approve_perm){
	var id_m			= escape(document.getElementById('id_m').value);
	var cbo_old_group	= escape(document.getElementById('cbo_old_group').value);
	var txt_new_group	= escape(document.getElementById('txt_new_group').value);
	var txt_selected_id	= escape(document.getElementById('txt_selected_id').value);
	var cbo_status		= escape(document.getElementById('cbo_status').value);
	var cbo_is_deleted	= escape(document.getElementById('cbo_is_deleted').value);
	var save_up 		= escape(document.getElementById('save_up').value);
	//alert(txt_new_group);
	//return false;
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);

	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( (cbo_status==0 || cbo_is_deleted==1) && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common_mail.php?action=mail_group&isupdate='+save_up+
					'&id_m='+id_m+
					'&cbo_old_group='+cbo_old_group+
					'&txt_new_group='+txt_new_group+
					'&txt_selected_id='+txt_selected_id+
					'&cbo_status='+cbo_status+
					'&cbo_is_deleted='+cbo_is_deleted+
					'&nocache ='+nocache);
		http.onreadystatechange = mail_group_setupReply_info;
		http.send(null); 
	}
}

function mail_group_setupReply_info() {
	if(http.readyState == 4){ 		
		//var response = http.responseText;	
		//alert(http.responseText);
		var response = http.responseText.split('_');	
		//if( response[0] == 3 )
	 
	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Group name of this ID, Please use New Group name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"library","mail_group_setup","update","../../");
				document.getElementById('id_m').value=response[1];
				showResult(' ','2','mail_group_cont');
				//(response[1],'5','bank_list_view');
				//document.getElementById('txt_selected_id').value="";
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"library","mail_group_setup","insert","../../");
				document.getElementById('id_m').value=response[1];
				showResult(' ','2','mail_group_cont');
				//document.getElementById('txt_selected_id').value="";
				document.getElementById('save_up').value="";
			});
		}
		
	//alert(response);
	
	}
}	

//-------------------------Mail Group setup End-----------------------------------------------------------------------

//--------------------------Mail Server Configuration Start-----------------------------------------------------------------------
function fnc_email_server_configuration(save_perm,edit_perm,delete_perm,approve_perm){
	var txt_pop3_server	= escape(document.getElementById('txt_pop3_server').value);
	var txt_smtp_server	= escape(document.getElementById('txt_smtp_server').value);
	var txt_domain	= escape(document.getElementById('txt_domain').value);
	var txt_sender_email	= escape(document.getElementById('txt_sender_email').value);
	var cbo_status		= escape(document.getElementById('cbo_status').value);
	var cbo_is_deleted	= escape(document.getElementById('cbo_is_deleted').value);
	var save_up 		= escape(document.getElementById('save_up').value);
	//alert(cbo_is_deleted);
	//return false;
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( (cbo_status==0 || cbo_is_deleted==1) && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if($('#txt_pop3_server').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_pop3_server').focus();
			$(this).html('Please Enter POP3 Server Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_smtp_server').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_smtp_server').focus();
			$(this).html('Please Enter SMTP Server Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_domain').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_domain').focus();
			$(this).html('Please Enter Domain Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#save_up').val()=="" && $('#cbo_is_deleted').val()=="1"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_domain').focus();
			$(this).html('Please First Insert Then You Can Delete').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common_mail.php?action=email_server_configuration&isupdate='+save_up+
					'&txt_pop3_server='+txt_pop3_server+
					'&txt_smtp_server='+txt_smtp_server+
					'&txt_domain='+txt_domain+
					'&txt_sender_email='+txt_sender_email+
					'&cbo_status='+cbo_status+
					'&cbo_is_deleted='+cbo_is_deleted+
					'&nocache ='+nocache);
		//alert(&cbo_is_deleted);
		http.onreadystatechange = mail_server_setupReply_info;
		http.send(null); 
	}
}

function mail_server_setupReply_info() {
	if(http.readyState == 4){ 		
		//var response = http.responseText;	
	 //alert(http.responseText);
		var response = http.responseText.split('_');	
		//if( response[0] == 3 )
	 
	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"library","email_server_configuration","insert","../../");
				showResult(' ','3','mail_server_cont');
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"library","email_server_configuration","update","../../");
				showResult(' ','3','mail_server_cont');
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Deleted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"library","email_server_configuration","delete","../../");
				showResult(' ','3','mail_server_cont');
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==4)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Entry.').addClass('messagebox_ok').fadeTo(900,1);
				showResult(' ','3','mail_server_cont');
				document.getElementById('save_up').value="";
			});
		}
	
	}
}	

//-------------------------Mail Server Configuration End-----------------------------------------------------------------------


