<?php
// Fuad

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

	include('../../includes/common.php');
	include('../../includes/array_function.php');
	?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Mail Group Setup</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
	<script src="includes/ajax_submit_mail.js" type="text/javascript"></script>
	<script src="includes/functions_mail.js" type="text/javascript"></script>
    <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
	<script type="text/javascript" src="../../juery/lib.js"></script>
	<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
		
	
	<script type="text/javascript">
		var selected_id = new Array, selected_name = new Array();
		
		function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			//alert(tbl_row_count);
			tbl_row_count = tbl_row_count - 2;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			//$('#cbo_old_group').change('#FFFFCC');
			//alert(origColor);
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			//alert(str);
		 
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}
		
	</script>
	<script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>
</head>

<!--<body style="font-family:verdana; font-size:11px;" onLoad="showResult(' ','2','mail_group_cont');">-->
<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:900px;">
    	<div>
            <div class="form_caption">
           		Mail Group Setup
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update.$delete;
                ?>
               </span>
			<div id="messagebox" style="background-color:#FF9999; color:#000000; width:530px" align="center"></div>
		</div>
		<!-- Start Field Set -->
		<fieldset style="width:500px ">
			<legend>Mail Group</legend>
			<!-- Start Form -->
			<form name="mail_group" id="mail_group" method="" autocomplete="off" action="javascript:fnc_mail_group(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">
				<fieldset>
					<div style="width:500px; overflow:auto" >
						<table width="100%">
							<tr>
								<td width="110px">Old Group</td>
								<td width="390px" colspan="3">
									<select name="cbo_old_group" id="cbo_old_group" onChange="update_old_data(this.value,'4');" class="combo_boxes"  style="width:390px ">
										<option value="0">---Select Old Group---</option>
									<?
									$company_sql= mysql_db_query($DB, "select group_name,id from mail_group_mst where is_deleted=0  and status_active=1 order by id");
									while ($r_company=mysql_fetch_array($company_sql))
									{
									?>
								<option value="<? echo $r_company["id"]; ?>" <? if($cbo_buyer_name1==$r_company["id"]){?>selected<?php }?>><? echo "$r_company[group_name]" ?> </option>
								<? } ?>
									</select>	
								</td>
							</tr>
							<tr>
								<td width="110px">New Group</td>
								<td width="390px" colspan="3">
									<input type="text" name="txt_new_group" id="txt_new_group" class="text_boxes" style="width:380px" />
								</td>
                           	</tr>
							<tr>
								<td width="110px">Status</td>
                                <td width="140px">
                                    <select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:140px" >
                                     <?php
                                        foreach($status_active as $key=>$value):
                                        ?>
                                        <option value=<? echo "$key";
                                        if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>
                                    </select>
                                </td>
                                <td width="95px">Delete</td>
                                <td width="140px">
                                    <select name="cbo_is_deleted" id="cbo_is_deleted" class="combo_boxes" style="width:145px" >
                                        <?php
                                        foreach($is_deleted as $key=>$value):
                                        ?>
                                        <option value=<? echo "$key";
                                        if ($cbo_po_status2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>						
                                    </select>	
                                </td>
							</tr>
                            <tr>
                            	<td align="center" colspan="4">
                                    You Have Selected: <input type="text"  readonly="readonly" style="width:350px" class="text_boxes" name="txt_selected" id="txt_selected" />
                                    <input type="hidden" readonly style="width:250px" class="text_boxes" name="txt_selected_id" id="txt_selected_id" />
								</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">&nbsp;						
                                    <input type="hidden" name="save_up" id="save_up" >
                                    <input type="hidden" name="id_m" id="id_m">	
                                </td>					
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="button_container">
                                    <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                                    <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                                </td>				
                            </tr>
                            <tr>
                            	<td width="150" colspan="4">
									<?php echo $company_details[$location['company_id']]['company_name']; ?>
                                    <input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $location['location_name']; ?>" />
                                    <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $location['id']; ?>" />
								</td>
                            </tr>
                           
						</table>
					</div>
				</fieldset>
                <div style="width:500px; float:left; margin:auto" align="center">
                        <fieldset style="width:500px; margin-top:10px">
                            <legend>Press space for mail user Info</legend>
                                <input type="text" size="30" class="text_boxes" onKeyUp="showResult(this.value,'2','mail_group_cont');" />
                                <div style="width:500px; margin-top:10px" id="mail_group_cont" align="left"></div>
                        </fieldset>	
                    </div>				
                    <!--<div style="height:10px;"></div>
				<fieldset>
					<div style="width:500px;" >
						<table cellspacing="1" width="100%">
                            <tr>
                                <td>
                                    <div id="mail_group_cont">
                                    </div>
                                </td>
                            </tr>		
						</table>
					</div>
				</fieldset>-->
				
			</form>
			<!-- End Form -->
		</fieldset>
		<!-- End Field Set -->
	</div>
</body>
</html>


