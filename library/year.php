<?
	session_start();
	date_default_timezone_set('UTC');
	include('../includes/common.php');
	include('../includes/array_function.php');
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

	
	$sql=mysql_db_query($DB, "select * from lib_policy_year where type=0 and is_locked=0");
	 if ($selectResult = mysql_fetch_array($sql)){
		 $act_leave_year=$selectResult["name"];
		 $act_leave_year_id=$selectResult["id"];

	 }
	$sql=mysql_db_query($DB, "select * from lib_policy_year where type=1 and is_locked=0");
	 if ($selectResult = mysql_fetch_array($sql)){
		 $act_salary_year=$selectResult["name"];
		 $act_salary_year_id=$selectResult["id"];
	 }
	 $sql=mysql_db_query($DB, "select * from lib_policy_year where type=3 and is_locked=0");
	 if ($selectResult = mysql_fetch_array($sql)){
		 $act_financial_year_name=$selectResult["name"];
		 $act_financial_year_id=$selectResult["id"];
	 }
	 
	 $sql=mysql_db_query($DB, "select * from lib_policy_year where type=2 and is_locked=0");
	 if ($selectResult = mysql_fetch_array($sql)){
		 $act_tax_year=$selectResult["name"];
		 $act_tax_year_id=$selectResult["id"];
	 }
	
		 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/popup_window.js"></script>
	<script type="text/javascript" src="../js/modal.js"></script>
	
	<script src="../resources/jquery_ui/jquery-1.4.4.min.js" type="text/javascript"></script>
	<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../resources/jquery_dataTable/jquery.dataTables.js"></script>
	<script src="../resources/ui.tabs.paging.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		var salary_periods = 12;
		var months = new Array(
						new Array('January', 31),
						new Array('February', 28),
						new Array('March', 31),
						new Array('April', 30),
						new Array('May', 31),
						new Array('June', 30),
						new Array('July', 31),
						new Array('August', 31),
						new Array('September', 30),
						new Array('October', 31),
						new Array('November', 30),
						new Array('December', 31)
					);
		
		$(document).ready(function() {
			$('#year').tabs({ cache:true, selectOnAdd:true });
			$('#year').tabs('paging', { cycle:true, follow:true, followOnSelect:true, selectOnAdd:true });
			
			populate_data( 'leave' );
			populate_data( 'salary' );
			populate_data( 'financial' );
			populate_data( 'tax' );
			
			$('#tbl_leave_year_definition tbody input[type="text"]').each(function() {
				$(this).attr('readonly', 'readonly');
			});
			$('#tbl_salary_year_definition tbody input[type="text"]').each(function() {
				//$(this).attr('readonly', 'readonly');
			});
			$('#tbl_tax_year_definition tbody input[type="text"]').each(function() {
				$(this).attr('readonly', 'readonly');
			});
		
			$('input[name="salary_year_period"]').blur(function() {
				if( $(this).val() != '' ) {
					alert('hello');
				}
			});
		});
		
		function leave_year_calculate() {
			var starting_month = parseInt( $('select[name="leave_year_start"]').val() );
			var ending_month = parseInt( $('select[name="leave_year_end"]').val() );
			var diff = ending_month - starting_month;
			if( diff == -1 || diff == 11 ) {
				for( var i = 0; i < 12; i++ ) {
					var current_month = ( starting_month + i ) > 12 ? ( starting_month + i - 13 ) : ( starting_month + i - 1 );
					$('#leave_year_definition_container_' + ( i + 1 ) + ' input[name="leave_year_starting_date[]"]').val( months[current_month][0] + ' 1' );
					$('#leave_year_definition_container_' + ( i + 1 ) + ' input[name="leave_year_ending_date[]"]').val( months[current_month][0] + ' ' + months[current_month][1] );
				}
			} else {
				alert('Invalid year range selected.');
			}
		}
		
		function salary_year_calculate() {
			
			var starting_month = parseInt( $('select[name="salary_year_start"]').val() );
			var ending_month = parseInt( $('select[name="salary_year_end"]').val() );
		
		//alert(starting_month+"===="+ending_month);
			var diff = ending_month - starting_month;
			if( diff == -1 || diff == 11 ) {
				for( var i = 0; i < 12; i++ ) {
					var current_month = ( starting_month + i ) > 12 ? ( starting_month + i - 13 ) : ( starting_month + i - 1 );
					$('#salary_year_definition_container_' + ( i + 1 ) + ' input[name="salary_year_starting_date[]"]').val( months[current_month][0] + ' 1' );
					$('#salary_year_definition_container_' + ( i + 1 ) + ' input[name="salary_year_ending_date[]"]').val( months[current_month][0] + ' ' + months[current_month][1] );
				}
			} else {
				alert('Invalid year range selected.');
			}
		}
		
		
		function financial_year_calculate() {
			var starting_month = parseInt( $('select[name="financial_year_start"]').val() );
			var ending_month = parseInt( $('select[name="financial_year_end"]').val() );
			
			var diff = ending_month - starting_month;
			if( diff == -1 || diff == 11 ) 
			{
				if ( diff==11) 
				{
					document.getElementById('financial_year_name').value=document.getElementById('financial_year_name').value=document.getElementById('cbo_year_select1').value;
				}
				else 
				{
					
					document.getElementById('financial_year_name').value=document.getElementById('cbo_year_select1').value+"-"+((document.getElementById('cbo_year_select1').value*1)+1);
				}
				
				var k=0;
				for( var i = 0; i < 12; i++ ) 
				{
					var current_month = ( starting_month + i ) > 12 ? ( starting_month + i - 13 ) : ( starting_month + i - 1 );
					//alert(k);
					if(i==0)
					{
						k=k+1;
						document.getElementById('financial_year_starting_date_1' ).value=months[current_month][0] + ' 1';
						document.getElementById('financial_year_ending_date_1' ).value=months[current_month][0] + ' 1'; 
						document.getElementById('financial_year_title_1' ).value='Opening'; 
						k=k+1;
						document.getElementById('financial_year_starting_date_2' ).value=months[current_month][0] + ' 1';
						document.getElementById('financial_year_ending_date_2' ).value=months[current_month][0] + ' ' + months[current_month][1]; 
						document.getElementById('financial_year_title_2' ).value=months[current_month][0];  
					}
					
					else if(i==11) 
					{ 
						k=k+1;
						document.getElementById('financial_year_starting_date_'+k ).value=months[current_month][0] + ' 1';
						document.getElementById('financial_year_ending_date_'+k ).value=months[current_month][0] + ' ' + months[current_month][1]; 
						document.getElementById('financial_year_title_'+k ).value=months[current_month][0]; 
						
						k=k+1;
						document.getElementById('financial_year_starting_date_'+k ).value=months[current_month][0] + ' ' + months[current_month][1];
						document.getElementById('financial_year_ending_date_'+k ).value=months[current_month][0] + ' ' + months[current_month][1]; 
						document.getElementById('financial_year_title_'+k ).value='Closing'; 
						k=k+1;
						document.getElementById('financial_year_starting_date_'+k ).value=months[current_month][0] + ' ' + months[current_month][1];
						document.getElementById('financial_year_ending_date_'+k ).value=months[current_month][0] + ' ' + months[current_month][1]; 
						document.getElementById('financial_year_title_'+k ).value='Post Closing'; 
					}
					else
					{
						k=k+1;
						document.getElementById('financial_year_starting_date_'+k ).value=months[current_month][0] + ' 1';
						document.getElementById('financial_year_ending_date_'+k ).value=months[current_month][0] + ' ' + months[current_month][1]; 
						document.getElementById('financial_year_title_'+k ).value=months[current_month][0];
					}
					
				}
			} else {
				alert('Invalid year range selected.');
			}
		}
		 
		
		function tax_year_calculate() {
			var starting_month = parseInt( $('select[name="tax_year_start"]').val() );
			var ending_month = parseInt( $('select[name="tax_year_end"]').val() );
			var diff = ending_month - starting_month;
			if( diff == -1 || diff == 11 ) {
				for( var i = 0; i < 12; i++ ) {
					var current_month = ( starting_month + i ) > 12 ? ( starting_month + i - 13 ) : ( starting_month + i - 1 );
					$('#tax_year_definition_container_' + ( i + 1 ) + ' input[name="tax_year_starting_date[]"]').val( months[current_month][0] + ' 1' );
					$('#tax_year_definition_container_' + ( i + 1 ) + ' input[name="tax_year_ending_date[]"]').val( months[current_month][0] + ' ' + months[current_month][1] );
				}
			} else {
				alert('Invalid year range selected.');
			}
		}
		
		/*function add_salary_year_definition( period ) {
			if( salary_periods < 14 ) {
				salary_periods++;
				var row = $('#salary_year_definition_container_' + period).clone();
				$('#salary_year_definition_container_' + period).after( row );
				$('#tbl_salary_year_definition tbody tr').each(function() {
					
				});
			}
		}*/
		
		function openmypage( page_link, title, year_type ) {
			emailwindow = dhtmlmodal.open( 'EmailBox', 'iframe', page_link, title, 'width=560px,height=400px,center=1,resize=0,scrolling=0', '' );
			
			emailwindow.onclose = function() {
				var thee_loc = this.contentDoc.getElementById("txt_selected");
				var thee_id = this.contentDoc.getElementById("txt_selected_id");
				
			 	document.getElementById( year_type + '_location' ).value = thee_loc.value;
				document.getElementById( year_type + '_location_id' ).value = thee_id.value;
			}
		}
		
		function populate_data( form ) {
			$('#' + form + '_data').html('<img src="../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "year_data.php",
				data: 'form=' + form,
				success: function( data ) {
					$('#' + form + '_data').html( data );
					$('#' + form + '_data table').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"aoColumns": [ { "sType": "html" }, null, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "desc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]]
					});
				}
			});
		}
		
		function update_status( form, option, url, cache, tbl_name, id, status_active ) {
			if( option == 'delete' ) var opt = 'delete';
			else if( status_active == 1 ) var opt = 'deactivate';
			else var opt = 'activate';
			
			if( confirm( 'Are you sure to ' + opt + ' this?' ) ) {
				cache = (typeof cache == 'undefined') ? '' : cache;
				status_active = (typeof status_active == 'undefined') ? '' : status_active;
				$.ajax({
					type: "POST",
					url: url,
					data: 'update_status=' + option + '&tbl_name=' + tbl_name + '&id=' + id + '&status_active=' + status_active + '&cache=' + cache,
					success: function( data ) {
						if( typeof window.reset_form == 'function' ) reset_form();
						
						populate_data( form );
						
						var output = data.split('###');
						if( cache != '' ) {
							jQuery.globalEval( output[1] );
						}
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html(output[0]).addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
						});
					}
				});
			}
		}
		
		function myFunction(i){
		 
			if ( document.getElementById('financial_period_locked_'+i).checked==true)
				document.getElementById('financial_period_locked_'+i).value=1;
			else
				document.getElementById('financial_period_locked_'+i).value=0;
			
			 // alert(document.getElementById('financial_period_locked_'+i).value);
		}
		
		$(document).ready(function() {
				$(".datepicker").datepicker({
					dateFormat: 'dd-mm-yy',
					changeMonth: true,
					changeYear: true
				});
			});	
		
	
//new add ekram



function create_period(){
	
	
			var txt_date_year=$("#txt_date").val();
			var starting_year=txt_date_year.split("-");
			var ending_year=$("#cbo_year_select").val();
			
			/*if(starting_year[2]!=ending_year){ //checks for cross year
				
				alert("Starting Year Must Be The Same As Date");
				populate_data( 'salary' );
				return;
				}else{*/
			
				
			$('#salary_year_name').val($('#cbo_year_select').val());
			var txt_date=$("#txt_date").val();
			
 			$.ajax({
				type: "POST",
				url: "get_data_update_variable_settings.php?&txt_date="+txt_date,
				data: 'link=sal_month',
				success: function(data) {
				 eval(data);
				 
				      }
				});
			 	
				
			//}//end checking for cross year
	
	
}

/*function checks_year(){
	
	//alert("ekram");
			var txt_date=$("#txt_date").val();
			var starting_year=txt_date.split("-");
			var ending_year=$("#cbo_year_select").val();
			if(starting_year[2]!=ending_year){
				alert("Starting Year Must Be The Same As Date");
				$("#sal_period_create").css
				return;
				}
			
			
}*/
	
		
	</script>
	<style type="text/css">
		#tbl_leave_year_definition input[type="text"], #tbl_tax_year_definition input[type="text"] { width:152px; }
		#tbl_salary_year_definition input[type="text"] { width:143px; }
		.header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:1084px; position:relative; height:50px; margin:5px 0;">
		<h2>Year</h2>
		<div align="center" id="messagebox"></div>	
	</div>
	<div id="year" style="width:1072px;">
		<ul class="tabs">
			<li><a href="#leave_year">Leave Year</a></li>
			<li><a href="#salary_year">Salary Year</a></li>
			<li><a href="#tax_year">Tax Year</a></li>
            <li><a href="#financial_year">Financial Year</a></li>
		</ul>
		<div id="leave_year">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="34%">
						<form id="leave_year_form" action="javascript:fnc_leave_year();" method="POST">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr><th colspan="4" class="header">Leave Year</th></tr>
								<tr><td colspan="4" height="10"></td></tr>
								<tr>
									<td width="24%">Name:</td>
									<td colspan="3">
										<input type="text" name="leave_year_name" id="leave_year_name" class="text_boxes" value="" style="width:251px;" />
										<input type="hidden" name="leave_year_id" id="leave_year_id" value="" />
									</td>
								</tr>
								<tr><td colspan="4" height="2"></td></tr>
								<tr>
									<td>Status:</td>
									<td>
										<select name="leave_status_active" id="leave_status_active" class="combo_boxes" >
											<option value="0">Inactive</option>
											<option value="1" selected>Active</option>
										</select>
									</td>
                                    <td>Cur. Year:</td>
									<td>										
										<select name="active_leave_year" id="active_leave_year" class="combo_boxes" style="width:150" disabled="disabled" >											
											<option value="<? echo $act_leave_year_id; ?>" selected><? echo $act_leave_year; ?></option>
										</select>
                                        <input type="hidden" id="hidden_active_leave_year" value="<? echo $act_leave_year; ?>" />            
                                   </td>
								</tr>
								<tr><td colspan="4" height="10"></td></tr>
                                <tr><td> New Year:</td>
                                	<td colspan="3"> 
                                    <select name="cbo_leave_year_select" id="cbo_leave_year_select" class="combo_boxes" >
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        $c_year=date("Y");
                                        $s_year=$c_year-25;
                                        $e_year=$c_year+10;
                                        
                                         for ($year = $s_year; $year <= $e_year; $year++) { ?>
                                        <option value=<?php echo $year;
                                        if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
                                        <?php } ?>
                                    </select>
                                    </td></tr>
								<tr>
									<td>Year Start:</td>
									<td>
										<select name="leave_year_start" id="leave_year_start" class="combo_boxes" onchange="leave_year_calculate()">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
									<td>Year End:</td>
									<td>
										<select name="leave_year_end" id="leave_year_end" class="combo_boxes" onchange="leave_year_calculate()">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
								</tr>
								<tr><td colspan="4" height="10"></td></tr>
								<tr>
									<td colspan="4">
										<table id="tbl_leave_year_definition" width="100%" cellspacing="2" cellpadding="0">
											<thead>
												<tr>
													<th>SN</th>
													<th>Starting Date</th>
													<th>Ending Date</th>
												</tr>
											</thead>
											<tbody>
												<?php for( $i = 1; $i <= 12; $i++ ) { ?>
												<tr id="leave_year_definition_container_<?php echo $i; ?>">
													<td><?php echo $i; ?></td>
													<td><input type="text" name="leave_year_starting_date[]" class="text_boxes" value=""  /></td>
													<td><input type="text" name="leave_year_ending_date[]" class="text_boxes" value=""  /></td>
												</tr>
												<?php } ?>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3" style="padding:10px 0;">
														<!--
                                                        Location:
														<input type="text" name="leave_location" id="leave_location" class="text_boxes" value="" placeholder="Double click for location" style="margin-left:21px; width:260px;" ondblclick="openmypage( 'search_location.php', 'Location Info', 'leave' ); return false;" />
														<input type="hidden" name="leave_location_id" id="leave_location_id" value="" />
														-->
                                                    </td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td align="center" colspan="2">
														<input type="submit" name="leave_submit" value="Save" class="formbutton" />&nbsp;&nbsp;
														<input type="reset" name="leave_reset" value="Cancel" class="formbutton" />
													</td>
												</tr>
											</tfoot>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
					<td width="2%">&nbsp;</td>
					<td valign="top" align="center" id="leave_data"></td>
				</tr>
			</table>
		</div>
        
		<div id="salary_year">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="34%">
						<form id="salary_year_form" action="javascript:fnc_salary_year();" method="post" ><!-- onsubmit="return checkform( 'salary' );" -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr><th colspan="4" class="header">Salary Year</th></tr>
								<tr><td colspan="4" height="10"></td></tr>
								<tr>
									<td width="24%">Name:</td>
									<td colspan="3">
										<input type="text" name="salary_year_name" readonly="readonly" id="salary_year_name" class="text_boxes" value="" style="width:251px;" />
										<input type="hidden" name="salary_year_id" id="salary_year_id" value="" />
									</td>
								</tr>
								<tr><td colspan="4" height="2"></td></tr>
								<tr>
									<td>Status:</td>
									<td>
										<select name="salary_status_active" id="salary_status_active" class="combo_boxes">
											<option value="0">Inactive</option>
											<option value="1" selected>Active</option>
										</select>
									</td>
                                    <td>Cur. Year:</td>
                                    <td>										
										<select name="active_salary_year" id="active_salary_year" class="combo_boxes" style="width:150" disabled="disabled" >											
											<option value="<? echo $act_salary_year_id; ?>" selected><? echo $act_salary_year; ?></option>
										</select>
                                        <input  type="hidden" id="hidden_active_salary_year" value="<? echo $act_salary_year; ?>" />            
                                   </td>
								</tr>
								<tr><td colspan="4" height="10"></td></tr>
                                <tr><td> Start Year</td>
                                	<td colspan="3"> 
                                    <select name="cbo_year_select" id="cbo_year_select" class="combo_boxes" >
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        $c_year=date("Y");
                                        $s_year=$c_year-25;
                                        $e_year=$c_year+10;
                                        
                                         for ($year = $s_year; $year <= $e_year; $year++) { ?>
                                        <option value=<?php echo $year;
                                        if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
                                        <?php } ?>
                                    </select>
                                    </td></tr>
								<tr style="display:none">
									<td>Month Start:</td>
									<td>
										<select name="salary_year_start" id="salary_year_start" onchange="salary_year_calculate()" class="combo_boxes">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
									<td>Year End:</td>
									<td>
										<select name="salary_year_end" id="salary_year_end" onchange="salary_year_calculate()" class="combo_boxes">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
								</tr>
                              <tr>
                                	<td>Date:</td>
                                	<td><input type="text" name="txt_date" id="txt_date" style="width:80px" value="" class="datepicker" /></td><!--onchange="checks_year()"-->
                                    <td colspan="2"><input type="button" name="sal_period_create" id="sal_period_create" class="formbutton" value="Year Create" onclick="create_period()"/></td>
                                </tr>

								<tr><td colspan="4" height="10"></td></tr>
								<tr>
									<td colspan="4">
										<table id="tbl_salary_year_definition" width="100%" cellspacing="2" cellpadding="0">
											<thead>
												<tr>
													<th>SN</th>
													<th>Starting Date</th>
													<th>Ending Date</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
											<tbody>												
                                                <?php for( $i = 1; $i <= 12; $i++ ) { ?>
												<tr id="salary_year_definition_container_<?php echo $i; ?>">
													<td><?php echo $i; ?></td>
                    <td>
                        <input type="text" name="salary_year_starting_date[]" class="text_boxes" value="" id="salary_year_starting_date<? echo $i; ?>" />
                        <input type="hidden" name="salary_year_starting_date_hidden[]" class="text_boxes" value="" id="salary_year_starting_date_hidden<? echo $i; ?>" />
                    </td>
                    <td>
                        <input type="text" name="salary_year_ending_date[]" id="salary_year_ending_date<? echo $i; ?>" class="text_boxes" value=""  />
                        <input type="hidden" name="salary_year_ending_date_hidden[]" id="salary_year_ending_date_hidden<? echo $i; ?>" class="text_boxes" value=""  />
                        <input type="hidden" name="salary_year_dtls_id[]" id="salary_year_dtls_id<? echo $i; ?>" class="text_boxes" value=""  />
                    </td>
                                                   <!-- <td><a href="javascript:add_salary_year_definition( <?php echo $i; ?> )"><span class="ui-icon ui-icon-circle-plus"></span></a></td>-->
												</tr>
												<?php } ?>
											</tbody>
											<tfoot>
												<tr><td colspan="4" height="10"></td></tr>
                                                <!--
												<tr>
													<td colspan="4">
														Location:
														<input type="text" name="salary_location" id="salary_location" class="text_boxes" value="" placeholder="Double click for location" style="margin-left:21px; width:260px;" ondblclick="openmypage( 'search_location.php', 'Location Info', 'salary' ); return false;" />
														<input type="hidden" name="salary_location_id" id="salary_location_id" value="" />
													</td>
												</tr>
                                                -->
												<tr><td colspan="4" height="10"></td></tr>
												<tr>
													<td>&nbsp;</td>
													<td align="center" colspan="3">
														<input type="submit" name="salary_submit" value="Save" class="formbutton" />&nbsp;&nbsp;
														<input type="reset" name="salary_reset" value="Cancel" class="formbutton" />
													</td>
												</tr>
											</tfoot>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
					<td width="2%">&nbsp;</td>
					<td valign="top" align="center" id="salary_data"></td>
				</tr>
			</table>
		</div>
        
        <div id="financial_year">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="34%">
						<form id="financial_year_form" action="javascript:fnc_financial_year();" method="post" ><!-- onsubmit="return checkform( 'salary' );" -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <th colspan="4" class="header">Financial Year</th></tr>
								<tr><td colspan="4" height="10"></td></tr>
                                <tr><td colspan="4" height="10"></td></tr>
                                <tr>
                                	<td>Company</td>
                                    <td colspan="1">
                                    	<select name="cbo_company" id="cbo_company" class="combo_boxes" style="width:88%" tabindex="1">
                                       
                                            <option value="0">-- Select Company Name --</option>
                                            <?php
                                              
                                            $company_sql = mysql_db_query( $DB, "select company_name, id from lib_company where is_deleted=0 and status_active = 1 $company_cond order by company_name");
                                            while( $r_company = mysql_fetch_array( $company_sql ) ) {
                                            ?>
                                            <option value=<?php echo $r_company["id"]; if( $cbo_beneficiary_name1 == $r_company["id"] ) { ?> selected <?php }?>><?php echo "$r_company[company_name]"; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                	<td>Starting Year:</td>
                                	<td> 
                                    <select name="cbo_year_select1" id="cbo_year_select1" class="combo_boxes" >
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        $c_year=date("Y");
                                        $s_year=$c_year-3;
                                        $e_year=$c_year+5;
                                        
                                         for ($year = $s_year; $year <= $e_year; $year++) { ?>
                                        <option value=<?php echo $year;
                                        if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
                                        <?php } ?>
                                    </select>
                                    </td>
                                    <td>Cur. Year:</td>
                                    <td>	
                                    
                                    	<select name="is_current" id="is_current" class="combo_boxes" style="width:97px;">
											<option value="1" selected>Yes</option>
											<option value="0">No</option>
										</select>
                                    
                                    									
										<!--<select name="active_financial_year" id="active_financial_year" class="combo_boxes" style="width:89%" disabled="disabled" >											
											<option value="<? //echo $act_financial_year; ?>" selected><? //echo $act_financial_year_name; ?></option>
										</select>-->
                                        <input type="hidden" id="hidden_active_financial_year" value="<? echo $act_financial_year_name; ?>" />            
                                   </td>
                                </tr>
								<tr>
									<td>Starting Month:</td>
									<td>
										<select name="financial_year_start" id="financial_year_start" onchange="financial_year_calculate()" class="combo_boxes">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
									<td>Ending Month:</td>
									<td>
										<select name="financial_year_end" id="financial_year_end" onchange="financial_year_calculate()" class="combo_boxes">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
								</tr>
								<tr>
									<td width="24%">Name:</td>
									<td>
										<input type="text" readonly="readonly" name="financial_year_name" id="financial_year_name" class="text_boxes" value="" style="width:90%;" />
										<input type="hidden" name="financial_year_id" id="financial_year_id" value="" />
									</td>
                                    <td>Is Active:</td>
									<td>
										<select name="financial_status_active" id="financial_status_active" class="combo_boxes" style="width:97px;">
											<option value="0">Inactive</option>
											<option value="1" selected>Active</option>
										</select>
									</td>
								</tr>
								<tr><td colspan="4" height="2"></td></tr>
								<tr>
									
                                    
								</tr>
								
								<tr><td colspan="4" height="10"></td></tr>
								<tr>
									<td colspan="4">
										<table id="tbl_financial_year_definition" width="100%" cellspacing="2" cellpadding="0">
											<thead>
												<tr>
													<th>SN</th>
													<th>Starting Date</th>
													<th>Ending Date</th>
                                                    <th>Period</th>
													<th>Locked</th>
												</tr>
											</thead>
											<tbody>												
                                                <?php $kk=0; for( $i = 1; $i <= 15; $i++ ) { ?>
												<tr>
													<td id="financial_year_period_id_<?php echo $i;?>"><?php echo $kk; $kk++; ?></td>
													<td><input type="text"  id="financial_year_starting_date_<?php echo $i; ?>" class="text_boxes" value=""  /></td>
													<td><input type="text"  id="financial_year_ending_date_<?php echo $i; ?>" class="text_boxes" value=""  /></td>
                                                    <td><input type="text"  id="financial_year_title_<?php echo $i; ?>" class="text_boxes" value=""  /></td>
                                                    <td><input type="checkbox" id="financial_period_locked_<?php echo $i; ?>" onclick="myFunction(<? echo $i; ?> )" /></td>
												</tr>
												<?php } ?>
											</tbody>
											<tfoot>
												<tr><td colspan="4" height="10"></td></tr>
                                                <!--
												<tr>
													<td colspan="4">
														Location:
														<input type="text" name="salary_location" id="salary_location" class="text_boxes" value="" placeholder="Double click for location" style="margin-left:21px; width:260px;" ondblclick="openmypage( 'search_location.php', 'Location Info', 'salary' ); return false;" />
														<input type="hidden" name="salary_location_id" id="salary_location_id" value="" />
													</td>
												</tr>
                                                -->
												<tr><td colspan="4" height="10"></td></tr>
												<tr>
													<td>&nbsp;</td>
													<td align="center" colspan="3">
														<input type="submit" name="financial_submit" value="Save" class="formbutton" />&nbsp;&nbsp;
														<input type="reset" name="financial_submit" value="Cancel" class="formbutton" />
													</td>
												</tr>
											</tfoot>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
					<td width="2%">&nbsp;</td>
					<td valign="top" align="center" id="financial_data"></td>
				</tr>
			</table>
		</div>
        
		<div id="tax_year">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="34%">
												<form id="tax_year_form" action="javascript:fnc_tax_year();" method="post" >
<!-- onsubmit="return checkform( 'salary' );" -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr><th colspan="4" class="header">Tax Year</th></tr>
								<tr><td colspan="4" height="10"></td></tr>
								<tr>
									<td width="24%">Name:</td>
									<td colspan="3">
										<input type="text" name="tax_year_name"  id="tax_year_name" class="text_boxes" value="" style="width:251px;" />
										<input type="hidden" name="tax_year_id" id="tax_year_id" value="" />
									</td>
								</tr>
								<tr><td colspan="4" height="2"></td></tr>
								<tr>
									<td>Status:</td>
									<td>
										<select name="tax_status_active" id="tax_status_active" class="combo_boxes">
											<option value="0">Inactive</option>
											<option value="1" selected>Active</option>
										</select>
									</td>
                                    <td>Cur. Year:</td>
                                    <td>										
										<select name="active_tax_year" id="active_tax_year" class="combo_boxes" style="width:150" disabled="disabled" >											
											<option value="<? echo $act_salary_year_id; ?>" selected><? echo $act_salary_year; ?></option>
										</select>
                                        <input type="hidden" id="hidden_active_salary_year" value="<? echo $act_salary_year; ?>" />            
                                   </td>
								</tr>
								<tr><td colspan="4" height="10"></td></tr>
                                <tr><td> Year Start:</td>
                                	<td colspan="3"> 
                                    <select name="cbo_year_select" id="cbo_year_select" class="combo_boxes" >
                                        <option value="0">-- Select --</option>
                                        <?php 
                                        $c_year=date("Y");
                                        $s_year=$c_year-25;
                                        $e_year=$c_year+10;
                                        
                                         for ($year = $s_year; $year <= $e_year; $year++) { ?>
                                        <option value=<?php echo $year;
                                        if ($year==$c_year){?> selected <?php }?>><?php echo $year; ?></option>
                                        <?php } ?>
                                    </select>
                                    </td></tr>
								<tr>
									<td>Month Start:</td>
									<td>
										<select name="tax_year_start" id="tax_year_start" onchange="tax_year_calculate()" class="combo_boxes">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
									<td>Year End:</td>
									<td>
										<select name="tax_year_end" id="tax_year_end" onchange="tax_year_calculate()" class="combo_boxes">
											<option value="1">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
									</td>
								</tr>
								<tr><td colspan="4" height="10"></td></tr>
								<tr>
									<td colspan="4">
										<table id="tbl_tax_year_definition" width="100%" cellspacing="2" cellpadding="0">
											<thead>
												<tr>
													<th>SN</th>
													<th>Starting Date</th>
													<th>Ending Date</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
											<tbody>												
                                                <?php for( $i = 1; $i <= 12; $i++ ) { ?>
												<tr id="tax_year_definition_container_<?php echo $i; ?>">
													<td><?php echo $i; ?></td>
													<td><input type="text" name="tax_year_starting_date[]" class="text_boxes" value=""  /></td>
													<td><input type="text" name="tax_year_ending_date[]" class="text_boxes" value=""  /></td>
                                                   <!-- <td><a href="javascript:add_salary_year_definition( <?php echo $i; ?> )"><span class="ui-icon ui-icon-circle-plus"></span></a></td>-->
												</tr>
												<?php } ?>
											</tbody>
											<tfoot>
												<tr><td colspan="4" height="10"></td></tr>
                                                <!--
												<tr>
													<td colspan="4">
														Location:
														<input type="text" name="salary_location" id="salary_location" class="text_boxes" value="" placeholder="Double click for location" style="margin-left:21px; width:260px;" ondblclick="openmypage( 'search_location.php', 'Location Info', 'salary' ); return false;" />
														<input type="hidden" name="salary_location_id" id="salary_location_id" value="" />
													</td>
												</tr>
                                                -->
												<tr><td colspan="4" height="10"></td></tr>
												<tr>
													<td>&nbsp;</td>
													<td align="center" colspan="3">
														<input type="submit" name="tax_submit" value="Save" class="formbutton" />&nbsp;&nbsp;
														<input type="reset" name="tax_reset" value="Cancel" class="formbutton" />
													</td>
												</tr>
											</tfoot>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
					<td width="2%">&nbsp;</td>
					<td valign="top" align="center" id="tax_data"></td>
				</tr>
			</table>
		</div>
       
	</div>
</body>
</html>