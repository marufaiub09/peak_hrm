//Do not change
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

function fnc_leave_year() {					//Leave Year Policy Save / Update
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	answer = confirm("Current Leave Year: "+$('#hidden_active_leave_year').val()+"\nDo you want to Create New Leave Year?");

	if (answer !=0)	{error = false;}
	else{error = true;}	
	
	if( $('#leave_year_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#leave_year_name').focus();
			$(this).html('Leave year name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
		var starting_month = parseInt( $('#leave_year_start').val() );
		var ending_month = parseInt( $('#leave_year_end').val() );
		
		if( ( ( months[starting_month - 1][0] + ' 1' ) != $('#leave_year_definition_container_1 input[name="leave_year_starting_date[]"]').val() )
			|| ( ( months[ending_month - 1][0] + ' ' + months[ending_month - 1][1] ) != $('#leave_year_definition_container_12 input[name="leave_year_ending_date[]"]').val() )
		) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#leave_year_start').focus();
				$(this).html('Invalid leave year definition can not be saved.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
	}
	/*if( error == false && $('#leave_location_id').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#leave_location').focus();
			$(this).html('Please select at least one location.').addClass('messageboxerror').fadeTo(900,1);
		});
	}*/
	
	if( error == false ) {
		nocache = Math.random();
		var data = '&leave_year_id=' + escape( $('#leave_year_id').val() ) + '&leave_year_name=' + escape( $('#leave_year_name').val() ) + '&leave_status_active=' + $('#leave_status_active').val()
					+ '&active_leave_year=' + $('#active_leave_year').val()+ '&leave_period_counter=' + $('#tbl_leave_year_definition tbody tr').length + '&leave_year_start=' + $('#leave_year_start').val()
					+ '&leave_year_end=' + $('#leave_year_end').val()+'&cbo_leave_year_select='+$('#cbo_leave_year_select').val();
		
		for( var i = 1; i <= $('#tbl_leave_year_definition tbody tr').length; i++ ) {
			data += '&period_' + i + '_start=' + $('#leave_year_definition_container_' + i + ' input[name="leave_year_starting_date[]"]').val();
			data += '&period_' + i + '_end=' + $('#leave_year_definition_container_' + i + ' input[name="leave_year_ending_date[]"]').val();
		}
		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=leave_year' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_leave_year;
		http.send(null);
	}
}

function response_leave_year() {				//Leave Year Policy response Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','year','year_details','save','../../');
				$(this).fadeOut(5000);
			});
			populate_data( 'leave' );
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','year','year_details','update','../../');
				$(this).fadeOut(5000);
			});
			populate_data( 'leave' );
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inactive Leave Year Save is not Allow!!!!!').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 5 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Leave Year allready Used. Update is not Allow!!!!!').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
	}
}


function fnc_financial_year() {					//Salary Year Save / Update
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if($('#financial_year_id').val()=='')
	answer = confirm("Do you want to Create Financial Year '"+ $('#financial_year_name').val()+"'");
	else
	answer = confirm("Do you want to Update Financial Year '"+ $('#financial_year_name').val()+"'");
	/*
	var error = false;
	answer = confirm("Current Financial Year: "+$('#hidden_active_financial_year').val()+"\nDo you want to Create New Financial Year?");
	if (answer !=true)	{error = false;}
	else{error = true;}	
	
	
	if( error == false ) {
		
		var starting_month = parseInt( $('#financial_year_start').val() );
		var ending_month = parseInt( $('#financial_year_end').val() );
		
		if( ( ( months[starting_month - 1][0] + ' 1' ) != $('#financial_year_definition_container_1 input[name="financial_year_starting_date[]"]').val() )|| ( ( months[ending_month - 1][0] + ' ' + months[ending_month - 1][1] ) != $('#financial_year_definition_container_12 input[name="financial_year_ending_date[]"]').val() )
		) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#salary_year_start').focus();
				$(this).html('Invalid Financial year definition can not be saved.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
	}	*/
	
	if( answer == true ) {
		
		nocache = Math.random();
		var data = '&financial_year_id=' + escape( $('#financial_year_id').val() ) + '&financial_company_id=' + escape( $('#cbo_company').val() )  + '&financial_year_name=' + escape( $('#financial_year_name').val() ) + '&starting_year=' + escape( $('#cbo_year_select1').val() ) + '&is_current=' + escape( $('#is_current').val() ) +'&financial_status_active=' + $('#financial_status_active').val() + '&active_financial_year=' + $('#active_financial_year').val()+ '&financial_period_counter=' + $('#tbl_financial_year_definition tbody tr').length + '&financial_year_start=' + $('#financial_year_start').val() + '&financial_year_end=' + $('#financial_year_end').val()+'&cbo_year_select='+$('#cbo_year_select1').val();
		
	 
		
		for( var i = 1; i <= 15; i++ ) {
			
			var satrting_date = document.getElementById('financial_year_starting_date_'+i).value;
			var ending_date = document.getElementById('financial_year_ending_date_'+i).value;
			var financial_period = document.getElementById('financial_year_title_'+i).value;
			var financial_period_locked = document.getElementById('financial_period_locked_'+i).value;
		
			var financial_year_period_id = document.getElementById('financial_year_period_id_'+i).innerHTML;

			
			
			
			data += '&period_' + i + '_start=' + satrting_date;
			data += '&period_' + i + '_end=' + ending_date;
			data += '&period_' + i + '_finperiod=' + financial_period;
			data += '&period_' + i + '_finperiodlocked=' + financial_period_locked;
			data += '&period_' + i + '_periodid=' + financial_year_period_id;
		}
		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=financial_year' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_salary_year;
		http.send(null);
	}
}
function fnc_salary_year() {					//Salary Year Save / Update

	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	answer = confirm("Current Salary Year: "+$('#hidden_active_salary_year').val()+"\nsame year found,System will update existing data");

	if (answer !=0)	{error = false;}
	else{ return;}	
	
	
	$('#salary_year_name').val($('#cbo_year_select').val());
	
	if( $('#salary_year_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#salary_year_name').focus();
			$(this).html('Salary year name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
		//var starting_month = parseInt( $('#salary_year_start').val() );
		//var ending_month = parseInt( $('#salary_year_end').val() );
		
		/*if( ( ( months[starting_month - 1][0] + ' 1' ) != $('#salary_year_definition_container_1 input[name="salary_year_starting_date[]"]').val() )
			|| ( ( months[ending_month - 1][0] + ' ' + months[ending_month - 1][1] ) != $('#salary_year_definition_container_12 input[name="salary_year_ending_date[]"]').val() )
		) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#salary_year_start').focus();
				$(this).html('Invalid Salary year definition can not be saved.').addClass('messageboxerror').fadeTo(900,1);
			});
		}*/
	}	
	if( error == false ) {
		nocache = Math.random();
		var data = '&salary_year_id=' + escape( $('#salary_year_id').val() )
				   + '&salary_year_name=' + escape( $('#salary_year_name').val() ) 
				   + '&salary_status_active=' + $('#salary_status_active').val()
				   + '&active_salary_year=' + $('#active_salary_year').val()
				   + '&salary_period_counter=' + $('#tbl_salary_year_definition tbody tr').length 
				   +'&cbo_year_select='+$('#cbo_year_select').val()
				    +'&txt_date='+$('#txt_date').val();
					
		
		//cbo_year_select+ '&salary_year_start=' + $('#salary_year_start').val(); + '&salary_year_end=' + $('#salary_year_end').val()
				  
		/*
		for( var i = 1; i <= $('#tbl_salary_year_definition tbody tr').length; i++ ) {
				data += '&period_' + i + '_start=' + $('#salary_year_definition_container_' + i + ' input[name="salary_year_starting_date[]"]').val();
				data += '&period_' + i + '_end=' + $('#salary_year_definition_container_' + i + ' input[name="salary_year_ending_date[]"]').val();
		}
		*/
		
		var i=0;
		$('#tbl_salary_year_definition tbody tr').each(function() {
			i++;
			data +='&period_' + i + '_start=' + $(this).find('input[name="salary_year_starting_date[]"]').val();
			data +='&period_' + i + '_end=' + $(this).find('input[name="salary_year_ending_date[]"]').val();
			data +='&period_' + i + '_start_date_hidden=' + $(this).find('input[name="salary_year_starting_date_hidden[]"]').val();
			data +='&period_' + i + '_end_date_hidden=' + $(this).find('input[name="salary_year_ending_date_hidden[]"]').val();
			data +='&period_' + i + '_year_dtls_id=' + $(this).find('input[name="salary_year_dtls_id[]"]').val();
		});	
		
		
		//alert(data);return;
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=salary_year' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_salary_year;
		http.send(null);
	}
}


function response_salary_year() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		 //alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			populate_data( 'salary' );
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			populate_data( 'salary' );
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inactive Salary Year Save is not Allow!!!!!').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 5 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Leave Salary allready Used. Update is not Allow!!!!!').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 10 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Already Have One Current Year').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 11 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('One or more period already exists in another year').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
	}
}
//ekram 
//Tax Year Save / Update
function fnc_tax_year() {
						
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	answer = confirm("Current Salary Year: "+$('#hidden_active_salary_year').val()+"\nDo you want to Create New Salary Year?");

	if (answer !=0)	{error = false;}
	else{error = true;}	
	
	if( $('#tax_year_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#tax_year_name').focus();
			$(this).html('Tax year name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	if( error == false ) {
		var starting_month = parseInt( $('#tax_year_start').val() );
		var ending_month = parseInt( $('#tax_year_end').val() );
		
		if( ( ( months[starting_month - 1][0] + ' 1' ) != $('#tax_year_definition_container_1 input[name="tax_year_starting_date[]"]').val() )
			|| ( ( months[ending_month - 1][0] + ' ' + months[ending_month - 1][1] ) != $('#tax_year_definition_container_12 input[name="tax_year_ending_date[]"]').val() )
		) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#tax_year_start').focus();
				$(this).html('Invalid Tax year definition can not be saved.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
	}	
	if( error == false ) {
		nocache = Math.random();
		//var rr=$('#tax_year_id').val();
		//alert(rr);
		var data = '&tax_year_id=' + escape( $('#tax_year_id').val() ) + '&tax_year_name=' + escape( $('#tax_year_name').val() ) + '&tax_status_active=' + $('#tax_status_active').val()
					+ '&active_tax_year=' + $('#active_tax_year').val()+ '&salary_period_counter=' + $('#tbl_tax_year_definition tbody tr').length + '&tax_year_start=' + $('#tax_year_start').val()
					+ '&tax_year_end=' + $('#tax_year_end').val()+'&cbo_year_select='+$('#cbo_year_select').val();
		
		
		
		/*
		for( var i = 1; i <= $('#tbl_salary_year_definition tbody tr').length; i++ ) {
				data += '&period_' + i + '_start=' + $('#salary_year_definition_container_' + i + ' input[name="salary_year_starting_date[]"]').val();
				data += '&period_' + i + '_end=' + $('#salary_year_definition_container_' + i + ' input[name="salary_year_ending_date[]"]').val();
		}
		*/
		
		var i=0;
		$('#tbl_tax_year_definition tbody tr').each(function() {
			i++;
			data +='&period_' + i + '_start=' + $(this).find('input[name="tax_year_starting_date[]"]').val();
			data +='&period_' + i + '_end=' + $(this).find('input[name="tax_year_ending_date[]"]').val()
		});	
		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=tax_year' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_tax_year;
		http.send(null);
	}
}


function response_tax_year() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		 //alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			populate_data( 'tax' );
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			populate_data( 'tax' );
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inactive Salary Year Save is not Allow!!!!!').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 5 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Leave Salary allready Used. Update is not Allow!!!!!').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 10 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Already Have One Current Year').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 11 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('One or more period already exists in another year').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
	}
}




function show_leave_year(id)
	{				
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=edit_leave_year&id=' + id );
		http.onreadystatechange = response_show_leave_year;
		http.send(null);		
	}
	
function response_show_leave_year()
	{		
		eval(http.response);	    
	}
		
function show_salary_year(id)
	{				
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=edit_salary_year&id=' + id );
		http.onreadystatechange = response_show_salary_year;
		http.send(null);		
	}
function response_show_salary_year()
	{		
		eval(http.response);
		    
	}	
	
function show_financial_year(id,year)
	{
		year=year.split('-');
		document.getElementById('cbo_year_select1').value=year[0];
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=edit_financial_year&id=' + id );
		http.onreadystatechange = response_show_financial_year;
		http.send(null);		
	}	
function response_show_financial_year()
	{		
		eval(http.response);	    
	}	
	
function show_tax_year(id)
	{				
		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=edit_tax_year&id=' + id );
		http.onreadystatechange = response_show_tax_year;
		http.send(null);		
	}
function response_show_tax_year()
	{		
		
		eval(http.response);	    
	}	
