<?php
date_default_timezone_set('Asia/Dhaka');
session_start();

include('../../includes/common.php');

$e_date = time();
$user_only = $_SESSION['logic_erp']['user_name'];
$date_time= date("Y-m-d H:i:s");

extract( $_GET ); // receive data and extract
extract( $_POST );

//Leave Year Policy Save / Update
if( $action == "leave_year" ) 
{				
	$table_name = "lib_policy_year"; 
	$id_field_name = "id"; 
	$unique_field_name = "name";
		
	$sql = "SELECT $unique_field_name FROM $table_name WHERE name = $leave_year_name AND type = 0 ";
	if( $leave_year_id != '' ) $sql .= " AND id != $leave_year_id";
	$sql_exe = mysql_query( $sql );
	if( mysql_num_rows( $sql_exe ) != 0 ) {
		echo "1";
		exit();
	}
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$leave_year_start = $months[intval( $leave_year_start ) - 1];
	$leave_year_end = $months[intval( $leave_year_end ) - 1];
	
	 
	if( $leave_year_id == '' ) {	//Insert	
		if($active_leave_year!=0){
			//lock the current leave year
			$sql_up = "UPDATE $table_name set 
											updated_by='".mysql_real_escape_string($user_only)."',
											update_date='$date_time',
											is_locked=1
											where id=$active_leave_year";
											//echo $sql_up;die;
			mysql_query( $sql_up ) or die( $sql_up . "<br />" . mysql_error() );
			//echo "4";
			//exit();
		}
				
		//new leave year insert	
		$id = return_next_id( $id_field_name, $table_name );		
		$insert_sql = "INSERT INTO $table_name (
												id,
												name,
												type,
												year_start,
												year_end,
												inserted_by,
												insert_date,
												updated_by,
												status_active ) VALUES (
												$id,
												'".mysql_real_escape_string($leave_year_name)."',
												0,
												'$leave_year_start',
												'$leave_year_end',
												'".mysql_real_escape_string($user_only)."',
												'$date_time',
												'$updated_by',
												$leave_status_active)";
		//echo $insert_sql; die;
		mysql_query( $insert_sql ) or die( $insert_sql . "<br />" . mysql_error() );		
			
		//lock the current leave year
		$sql_up_balance = "UPDATE hrm_leave_balance set 
													updated_by='".mysql_real_escape_string($user_only)."',
													update_date='$date_time',
													is_locked=1 
													where leave_year=$active_leave_year";
													//echo $sql_up_balance;die; 
		mysql_query( $sql_up_balance ) or die( $sql_up_balance . "<br />" . mysql_error() );
		
		//lock the current leave_transaction
		$sql_leave_transaction = "UPDATE hrm_leave_transaction set 
																updated_by='".mysql_real_escape_string($user_only)."',
																update_date='$date_time',
																status_active=1,
																is_deleted=0,
																is_locked=1 
																where status_active=1 and is_deleted=0 and is_locked=0";
		mysql_query( $sql_leave_transaction ) or die( $sql_leave_transaction . "<br />" . mysql_error() );
		
		//lock the current leave_transaction_details
		$sql_transaction_details = "UPDATE hrm_leave_transaction_details set 
																		updated_by='".mysql_real_escape_string($user_only)."',
																		update_date='$date_time',
																		status_active=1,
																		is_deleted=0,
																		is_locked=1 
																		where status_active=1 and is_deleted=0 and is_locked=0";
		mysql_query( $sql_transaction_details ) or die( $sql_transaction_details . "<br />" . mysql_error() );
		
		//$leave_year =return_field_value("id","lib_policy_year","type = 0 and is_locked = 0 and is_deleted=0 and status_active=1");
		$leave_balance_sql="select emp.emp_code,emp.leave_policy as leave_policy,def.leave_type as leave_type,def.max_limit as leave_limit from hrm_employee emp, lib_policy_leave_definition def where emp.status_active=1 and emp.is_deleted=0 and emp.is_locked=0 and def.policy_id=emp.leave_policy";
		$leave_balance_result =mysql_query( $leave_balance_sql ) or die( $leave_balance_sql . "<br />" . mysql_error() );	 
		while($rowresult=mysql_fetch_array($leave_balance_result))
		{			
			$leave_balance_sql = "INSERT INTO hrm_leave_balance (
									emp_code,
									leave_year,
									year_start,										
									leave_policy,
									leave_type,
									leave_limit,
									leave_availed,
									balance,
									inserted_by,
									insert_date,
									updated_by ) VALUES (
									'".$rowresult["emp_code"]."',
									'".$id."',
									'".date("Y")."',										
									'".$rowresult["leave_policy"]."',
									'".$rowresult["leave_type"]."',
									'".$rowresult["leave_limit"]."',
									0,
									'".$rowresult["leave_limit"]."',
									'".mysql_real_escape_string($user_only)."',
									'$date_time',
									'$updated_by')";
			mysql_query( $leave_balance_sql ) or die( $leave_balance_sql . "<br />" . mysql_error() );
		}
		$sql_history=encrypt($leave_balance_sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_" . $id;
	}
	else 
	{ 	//Update
		//echo "5";
		$chk=check_uniqueness( "id", "hrm_leave_balance", "leave_year=$leave_year_id and leave_availed!=0 and is_locked=0");
		if($chk=false ) 
			{
				echo "5";
				exit();
			}
		$update_sql = "UPDATE $table_name SET
										name = '".mysql_real_escape_string($leave_year_name)."',
										year_start = '$leave_year_start',
										year_end = '$leave_year_end',
										updated_by='".mysql_real_escape_string($user_only)."',
										update_date='$date_time',
										status_active = $leave_status_active
										WHERE id = $leave_year_id AND type = 0";
		//echo $update_sql; die;
		mysql_query( $update_sql ) or die( $update_sql . "<br />" . mysql_error() );
		
		$delete_sql = "DELETE FROM lib_policy_year_periods WHERE year_id = $leave_year_id";
		mysql_query( $delete_sql ) or die( $delete_sql . "<br />" . mysql_error() );
		
		$sql_history=encrypt($update_sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "3_" . $leave_year_id;
	}
	
	$id = ( $leave_year_id != 0 ) ? $leave_year_id : $id;
	$monthsname = array('01' => 'January', '02'=>'February', '03'=>'March', '04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September', '10'=>'October', '11'=>'November', '12'=>'December');
	
	for( $i = 1; $i <= $leave_period_counter; $i++ ) 
	{
		$starting_date = "period_" . $i . "_start";
		$ending_date = "period_" . $i . "_end";
		
		$dy=explode(" ",$$starting_date );
		
		$starting_days =$cbo_leave_year_select."-".array_search(trim($dy[0]), $monthsname)."-01";
		$ending_days = date("Y-m-t", strtotime($starting_days));
		//echo $starting_days; die;
		$policy_insert_sql = "INSERT INTO lib_policy_year_periods (
					year_id,
					starting_date,
					ending_date,
					actual_starting_date,
					actual_ending_date,
					inserted_by,
					insert_date,
					updated_by
				) VALUES (
					$id,
					'" . $$starting_date . "',
					'" . $$ending_date . "',
					'" . $starting_days . "',
					'" . $ending_days . "',
					'".mysql_real_escape_string($user_only)."',
					'$date_time',
					'$updated_by')";
		$result = mysql_query( $policy_insert_sql ) or die( $policy_insert_sql . "<br />" . mysql_error() );
	}
	exit();
}

if( $action == "salary_year" ) {	//Leave Year Policy Save / Update
	
	$table_name = "lib_policy_year"; $id_field_name = "id"; $unique_field_name = "name";	
	$sql = "SELECT $unique_field_name FROM $table_name WHERE name = '$salary_year_name' AND type = 1 ";
	//echo $sql;die;
	if( $salary_year_id != '' ) $sql .= " AND id != $leave_year_id";
	$sql_exe = mysql_query( $sql );
	if( mysql_num_rows( $sql_exe ) != 0 ) {
		echo "1";
		exit();
	}
	
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	//$months_key = array('January'=>'01', 'February'=>'02', 'March'=>'03', 'April'=>'04', 'May'=>'05', 'June'=>'06', 'July'=>'07', 'August'=>'08', 'September'=>'09', 'October'=>'10', 'November'=>'11', 'December'=>'12');
	//$months_val = array('01'=>'January', '02'=>'February', '03'=>'March', '04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September', '10'=>'October', '11'=>'November', '12'=>'December');
	//$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$months_key_new = array('01'=>'January', '02'=>'February', '03'=>'March', '04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September', '10'=>'October', '11'=>'November', '12'=>'December');
	
	//$salary_year_start = $months[intval( $salary_year_start ) - 1];
	//$salary_year_end = $months[intval( $salary_year_end ) - 1];
	
		//this lines are used for lib_policy_year table year_start and year_end
		$st_date=convert_to_mysql_date($txt_date);
		$month_date_arr=explode("-",$st_date);
		$salary_year_start=($months_key_new[$month_date_arr[1]])."-".$month_date_arr[2];
		$st_end_date=explode("-",add_date(add_month($st_date,12),-1));
		$salary_year_end=($months_key_new[$st_end_date[1]])."-".$st_end_date[2];
		//end
		
	
	if( $salary_year_id == '' ) {	//Insert	
		if($active_salary_year!=0){
			//lock the current salary year
			$sql_up = "UPDATE $table_name set 
										updated_by='".mysql_real_escape_string($user_only)."',
										update_date='$date_time',
										status_active=0,
										is_deleted=1,
										is_locked=1 
										where id=$active_salary_year";
			mysql_query( $sql_up ) or die( $sql_up . "<br />" . mysql_error() );
		}		
		
		//new salary year insert	
		$id = return_next_id( $id_field_name, $table_name );		
		$insert_sql = "INSERT INTO $table_name (
					id,
					name,
					type,
					year_start,
					year_end,
					inserted_by,
					insert_date,
					updated_by,
					status_active
				) VALUES (
					$id,
					'".mysql_real_escape_string($salary_year_name)."',
					1,
					'$salary_year_start',
					'$salary_year_end',
					'".mysql_real_escape_string($user_only)."',
					'$date_time',
					'$updated_by',
					$salary_status_active)";
		mysql_query( $insert_sql ) or die( $insert_sql . "<br />" . mysql_error() );	
		
		for( $i = 1; $i <= $salary_period_counter; $i++ ) {
			$starting_date = "period_" . $i . "_start";
			$ending_date = "period_" . $i . "_end";
			$starting_date_hidden = "period_" . $i . "_start_date_hidden";
			$ending_date_hidden = "period_" . $i . "_end_date_hidden";
			
			$starting_date = $$starting_date;
			$ending_date = $$ending_date;
			$starting_date_hidden = $$starting_date_hidden;
			$ending_date_hidden = $$ending_date_hidden;
			
			//echo $starting_date."<br>";
			/*
			$month_start_arr = explode(' ',$starting_date);			
			if($months_key[$month_start_arr[0]]==12) $cbo_year_select=$cbo_year_select+1;			
			$actual_starting_date = $cbo_year_select.'-'.$months_key[$month_start_arr[0]].'-'.sprintf ("%02u", $month_start_arr[1]);
			
			$month_last_arr = explode(' ',$ending_date);			
			$actual_ending_date = $cbo_year_select.'-'.$months_key[$month_last_arr[0]].'-'.sprintf ("%02u", $month_last_arr[1]);
			*/
			//$dy=explode(" ",$starting_date );
		
			//$actual_starting_date =$starting_date;
			//$actual_ending_date = date("Y-m-t", strtotime($actual_starting_date));
			
			$policy_insert_sql = "INSERT INTO 
			lib_policy_year_periods 
			(year_id,starting_date,ending_date,actual_starting_date,actual_ending_date,inserted_by,insert_date,updated_by)
			 VALUES 
			 ($id,
			 '".($starting_date)."',
			 '".($ending_date)."',
			 '".convert_to_mysql_date($starting_date_hidden)."',
			 '".convert_to_mysql_date($ending_date_hidden)."',
			 '$user_only',
			 '$date_time',
			 '$updated_by')";
			//echo $policy_insert_sql;die;
			$result = mysql_query( $policy_insert_sql ) or die( $policy_insert_sql . "<br />" . mysql_error() );
		}
		//echo $policy_insert_sql;die;				
		echo "2_" . $id;
		exit();
	}
	else {					//Update
	//echo $date_time;die;
		$update_sql = "UPDATE $table_name SET
					name = '".mysql_real_escape_string($salary_year_name)."',
					year_start = '$salary_year_start',
					year_end = '$salary_year_end',
					updated_by='".mysql_real_escape_string($user_only)."',
					update_date='$date_time',
					status_active = $salary_status_active
					WHERE id = $salary_year_id AND type = 1";
					//echo $update_sql;
	mysql_query( $update_sql ) or die( $update_sql . "<br />" . mysql_error() );		
		
		//$delete_sql = "DELETE FROM lib_policy_year_periods WHERE year_id = $salary_year_id";
		//mysql_query( $delete_sql ) or die( $delete_sql . "<br />" . mysql_error() );
		
		 	
		$id = ( $salary_year_id != 0 ) ? $salary_year_id : $id;		
		for( $i = 1; $i <= $salary_period_counter; $i++ ) {
			$starting_date = "period_" . $i . "_start";
			$ending_date = "period_" . $i . "_end";
			$starting_date_hidden = "period_" . $i . "_start_date_hidden";
			$ending_date_hidden = "period_" . $i . "_end_date_hidden";
			$year_dtls_id = "period_" . $i . "_year_dtls_id";
			$starting_date = $$starting_date;
			$ending_date = $$ending_date;
			$starting_date_hidden = $$starting_date_hidden;
			$ending_date_hidden = $$ending_date_hidden;
			$dtls_id = $$year_dtls_id;
			
			/*
			$month_start_arr = explode(' ',$starting_date);			
			if($months_key[$month_start_arr[0]]==12) $cbo_year_select=$cbo_year_select+1;			
			$actual_starting_date = $cbo_year_select.'-'.$months_key[$month_start_arr[0]].'-'.sprintf ("%02u", $month_start_arr[1]);
			
			$month_last_arr = explode(' ',$ending_date);			
			$actual_ending_date = $cbo_year_select.'-'.$months_key[$month_last_arr[0]].'-'.sprintf ("%02u", $month_last_arr[1]);
			*/
			/*$dy=explode(" ",$starting_date );
			//echo $dy[0];
			$actual_starting_date =$cbo_year_select."-".array_search(trim($dy[0]), $months_val)."-01";
			$actual_ending_date = date("Y-m-t", strtotime($actual_starting_date));*/
			
			//$actual_starting_date =$starting_date;
			//$actual_ending_date = date("Y-m-t", strtotime($ending_date));
			//$actual_ending_date =$ending_date;
		
			/*$policy_insert_sql = "INSERT INTO lib_policy_year_periods (
									year_id,
									starting_date,
									ending_date,
									actual_starting_date,
									actual_ending_date,
									inserted_by,
									insert_date,
									updated_by
								) VALUES (
									$id,
									'$starting_date',
									'$ending_date',
									'$actual_starting_date',
									'$actual_ending_date',
									'".mysql_real_escape_string($user_only)."',
									'$date_time',
									'$updated_by')";*/
								
				$policy_insert_sql = "UPDATE lib_policy_year_periods SET
					year_id = $id,
					starting_date ='".($starting_date)."',
					ending_date ='".($ending_date)."',
					actual_starting_date='".convert_to_mysql_date($starting_date_hidden)."',
					actual_ending_date='".convert_to_mysql_date($ending_date_hidden)."',
					updated_by = '".mysql_real_escape_string($user_only)."',
					update_date = '".mysql_real_escape_string($date_time)."'
					WHERE id = $dtls_id";// AND type = 1 updated_by = '".mysql_real_escape_string($updated_by)."'					
									
				//echo $policy_insert_sql."<br>";die;					
									
			$result = mysql_query( $policy_insert_sql ) or die( $policy_insert_sql . "<br />" . mysql_error() );
		}
		//echo $new_end_date;die;
		//echo $months_key_new[$month_date_arr[0]]."-".$month_date_arr[0];die;
		echo "3_" .$salary_year_id;
		exit();
	}	

}



if( $action == "financial_year" ) {				//Financial Year Policy Save / Update
	//Year Name Validation
	$table_name = "ac_year"; $id_field_name = "id"; $unique_field_name = "name";	
	$sql = "SELECT $unique_field_name FROM $table_name WHERE name = $financial_year_name and company_id='$financial_company_id'  AND type =3 ";
	if( $financial_year_id != '' ) $sql .= " AND id != $id_field_name";
	$sql_exe = mysql_query( $sql );
	if( mysql_num_rows( $sql_exe ) != 0 ) {
		echo "1";
		exit();
	}
	//Current Year Validation
	$sql_current = "SELECT is_locked FROM ac_year WHERE is_locked=1 id!=$financial_year_id and type =3 ";
	if( $is_current != 1 ) $sql_current .= " AND id != $id_field_name";
	
	$sql_exe_current = mysql_query( $sql_current );
	if( mysql_num_rows( $sql_exe_current ) != 0 ) {
		echo "10";
		exit();
	}
	
	
	
	
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$months_key = array('January'=>'01', 'February'=>'02', 'March'=>'03', 'April'=>'04', 'May'=>'05', 'June'=>'06', 'July'=>'07', 'August'=>'08', 'September'=>'09', 'October'=>'10', 'November'=>'11', 'December'=>'12');
	$months_val = array('01'=>'January', '02'=>'February', '03'=>'March', '04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September', '10'=>'October', '11'=>'November', '12'=>'December');
	$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	
	$financial_year_start = $months[intval( $financial_year_start ) - 1];
	$financial_year_end = $months[intval( $financial_year_end ) - 1];
	
	
	if($financial_year_start==$months[0])
	{
		$year_start_date = $starting_year.'-01-01';
		$year_end_date = $starting_year.'-12-31';
	}
	if($financial_year_start==$months[6])
	{
		$starting_year2 = $starting_year+1;
		$year_start_date = $starting_year.'-07-01';
		$year_end_date = $starting_year2.'-06-30';
	}
	
	
 
	
	if( $financial_year_id == '' ) {	//Insert	
		 
		
		 $sql_period_start_date = mysql_query("SELECT a.actual_starting_date FROM ac_year_periods a, ac_year b WHERE a.year_id=b.id AND b.company_id='$financial_company_id'");
	 
		 $count_sql_period_start_date = mysql_num_rows( $sql_period_start_date );
	
		while($period_start_date = mysql_fetch_array($sql_period_start_date))
		{ 
			if($year_start_date == $period_start_date[actual_starting_date])
			{
			echo "11";
			exit();
			}
		}
		$id = return_next_id( $id_field_name, $table_name );		
		$insert_sql = "INSERT INTO $table_name (
					id,
					name,
					type,
					company_id,
					year_start,
					year_end,	
					year_start_date,
					year_end_date,		
					inserted_by,
					insert_date,
					updated_by,
					status_active,
					is_locked
				) VALUES (
					$id,
					'".mysql_real_escape_string($financial_year_name)."',
					3,
					'$financial_company_id',
					'$financial_year_start',
					'$financial_year_end',
					'$year_start_date',
					'$year_end_date',		
					'".mysql_real_escape_string($user_only)."',
					'$date_time',
					'$updated_by',
					'$financial_status_active',
					$is_current)";
					
		mysql_query( $insert_sql) or die( $insert_sql . "<br />" . mysql_error() );	
		
		  for( $i = 1; $i <= 15; $i++ ) 
		  {
			 
			$starting_date = "period_" . $i . "_start";			
			$ending_date = "period_" . $i . "_end";
			$financial_period = "period_" . $i . "_finperiod";
			$financial_period_locked = "period_" . $i . "_finperiodlocked";
			$financial_period_id = "period_" . $i . "_periodid";
			
			$starting_date = $$starting_date;
			$ending_date = $$ending_date;
			$financial_period = $$financial_period;
			$financial_period_locked = $$financial_period_locked;
			$financial_period_id = $$financial_period_id;
			
			$month_start_arr = explode(' ',$starting_date);			
			if($financial_year_start==$months[6] && $months_key[$month_start_arr[0]]==1) $cbo_year_select=$cbo_year_select+1;		
			$actual_starting_date = $cbo_year_select.'-'.$months_key[$month_start_arr[0]].'-'.sprintf ("%02u", $month_start_arr[1]);
			
			$month_last_arr = explode(' ',$ending_date);			
			$actual_ending_date = $cbo_year_select.'-'.$months_key[$month_last_arr[0]].'-'.sprintf ("%02u", $month_last_arr[1]);
			
			$id_dtls = return_next_id(id,ac_year_periods);	
				
			$policy_insert_sql = "INSERT INTO ac_year_periods (id,
									year_id,
									period,
									starting_date,
									ending_date,
									actual_starting_date,
									actual_ending_date,
									financial_period,
									period_locked,
									inserted_by,
									insert_date,
									updated_by
								) VALUES (
									$id_dtls,
									$id,
									$financial_period_id,
									'$starting_date',
									'$ending_date',
									'$actual_starting_date',
									'$actual_ending_date',
									'$financial_period',
									'$financial_period_locked',
									'".mysql_real_escape_string($user_only)."',
									'$date_time',
									'$updated_by')";
							
			$result = mysql_query( $policy_insert_sql ) or die( $policy_insert_sql . "<br />" . mysql_error() );
		
		  }
		
		
		//Copy and Insert ac_coa_period_dtls  $insert_period_dtls
		 if ($id!=1)
		{
			$previous_year_id = ($id-1);
			$insert_period_dtls = ("INSERT INTO ac_coa_period_dtls (ac_code,company_id,year_id,inserted_by,insert_date,updated_by)
			SELECT ac_code,'$financial_company_id',$id,'$user_only','$date_time','$updated_by' FROM ac_coa_period_dtls WHERE year_id = $previous_year_id");
			$result_period_dtls = mysql_query( $insert_period_dtls ) or die( $insert_period_dtls . "<br />" . mysql_error() );
			
		} 
		
		echo "2_" . $id.$sss;
		exit();
	}
	else {					//Update
		
		$update_sql = "UPDATE $table_name SET updated_by='".mysql_real_escape_string($user_only)."',update_date='$date_time',is_locked = 0 where is_locked=1";
		 mysql_query( $update_sql ) or die( $update_sql . "<br />" . mysql_error() );
		
		$update_sql2 = "UPDATE $table_name SET
					company_id = '$financial_company_id',
					name = '".mysql_real_escape_string($financial_year_name)."',
					year_start = '$financial_year_start',
					year_end = '$financial_year_end',
					year_start_date = '$year_start_date',
					year_end_date = '$year_end_date',
					updated_by='".mysql_real_escape_string($user_only)."',
					update_date='$date_time',
					status_active = $financial_status_active,
					is_locked=$is_current
				WHERE id = $financial_year_id AND type = 3";

		 mysql_query( $update_sql2 ) or die( $update_sql2 . "<br />" . mysql_error() );		
		
		$delete_sql = "DELETE FROM ac_year_periods WHERE year_id = $financial_year_id";
		 mysql_query( $delete_sql ) or die( $delete_sql . "<br />" . mysql_error() );
		 	
		$id = ( $financial_year_id != 0 ) ? $financial_year_id : $id;		
		 for( $i = 1; $i <= 15; $i++ ) {
			$starting_date = "period_" . $i . "_start";
			$ending_date = "period_" . $i . "_end";
			$financial_period = "period_" . $i . "_finperiod";
			$financial_period_locked = "period_" . $i . "_finperiodlocked";
			$financial_period_id = "period_" . $i . "_periodid";
			
			$starting_date = $$starting_date;
			$ending_date = $$ending_date;
			$financial_period = $$financial_period;
			$financial_period_locked = $$financial_period_locked;
			$financial_period_id = $$financial_period_id;
		
			$month_start_arr = explode(' ',$starting_date);	
					
			if($financial_year_start==$months[6] && $months_key[$month_start_arr[0]]==1) $cbo_year_select=$cbo_year_select+1;		
		
			$actual_starting_date = $cbo_year_select.'-'.$months_key[$month_start_arr[0]].'-'.sprintf ("%02u", $month_start_arr[1]);
			 
			$month_last_arr = explode(' ',$ending_date);			
			$actual_ending_date = $cbo_year_select.'-'.$months_key[$month_last_arr[0]].'-'.sprintf ("%02u", $month_last_arr[1]);
			
			$id_dtls = return_next_id(id,ac_year_periods);	
				
			$policy_insert_sql = "INSERT INTO ac_year_periods (
															id,
															year_id,
															period,
															starting_date,
															ending_date,
															actual_starting_date,
															actual_ending_date,
															financial_period,
															period_locked,
															inserted_by,
															insert_date,
															updated_by
														) VALUES ( 
															$id_dtls,
															$id,
															$financial_period_id,
															'$starting_date',
															'$ending_date',
															'$actual_starting_date',
															'$actual_ending_date',
															'$financial_period ',
															'$financial_period_locked',
															'".mysql_real_escape_string($user_only)."',
															'$date_time',
															'$updated_by')";
			
			$result = mysql_query( $policy_insert_sql ) or die( $policy_insert_sql . "<br />" . mysql_error() );
		}
		//echo $policy_insert_sql;die;
		echo "3_" .$financial_year_id;
		exit();
	}	

}


//ekram

if( $action == "tax_year" ) {				//Leave Year Policy Save / Update
	
	$table_name = "lib_policy_year"; $id_field_name = "id"; $unique_field_name = "name";	
	$sql = "SELECT $unique_field_name FROM $table_name WHERE name = $leave_year_name AND type = 2 ";
	if( $tax_year_id != '' ) $sql .= " AND id != $leave_year_id";
	$sql_exe = mysql_query( $sql );
	if( mysql_num_rows( $sql_exe ) != 0 ) {
		echo "1";
		exit();
	}
	
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$months_key = array('January'=>'01', 'February'=>'02', 'March'=>'03', 'April'=>'04', 'May'=>'05', 'June'=>'06', 'July'=>'07', 'August'=>'08', 'September'=>'09', 'October'=>'10', 'November'=>'11', 'December'=>'12');
	$months_val = array('01'=>'January', '02'=>'February', '03'=>'March', '04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September', '10'=>'October', '11'=>'November', '12'=>'December');
	$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$tax_year_start = $months[intval( $tax_year_start ) - 1];
	$tax_year_end = $months[intval( $tax_year_end ) - 1];
	
	if( $tax_year_id == '' ) {	//Insert
		//echo $tax_year_id;die;	
		if($active_tax_year!=0){
			//lock the current tax year
			$sql_up = "UPDATE $table_name set updated_by='".mysql_real_escape_string($user_only)."',update_date='$date_time',status_active=0,is_deleted=1,is_locked=1 where id=$active_tax_year";
			mysql_query( $sql_up ) or die( $sql_up . "<br />" . mysql_error() );
		}		
		
		//new tax year insert	
		$id = return_next_id( $id_field_name, $table_name );		
		$insert_sql = "INSERT INTO $table_name (
					id,
					name,
					type,
					year_start,
					year_end,
					inserted_by,
					insert_date,
					updated_by,
					status_active
				) VALUES (
					$id,
					'".mysql_real_escape_string($tax_year_name)."',
					2,
					'$tax_year_start',
					'$tax_year_end',
					'".mysql_real_escape_string($user_only)."',
					'$date_time',
					'$updated_by',
					$tax_status_active
				)";
		mysql_query( $insert_sql ) or die( $insert_sql . "<br />" . mysql_error() );	
		
		for( $i = 1; $i <= $salary_period_counter; $i++ ) {
			$starting_date = "period_" . $i . "_start";
			$ending_date = "period_" . $i . "_end";
			$starting_date = $$starting_date;
			$ending_date = $$ending_date;
			
			$month_start_arr = explode(' ',$starting_date);			
			if($months_key[$month_start_arr[0]]==12) $cbo_year_select=$cbo_year_select+1;			
			$actual_starting_date = $cbo_year_select.'-'.$months_key[$month_start_arr[0]].'-'.sprintf ("%02u", $month_start_arr[1]);
			
			$month_last_arr = explode(' ',$ending_date);			
			$actual_ending_date = $cbo_year_select.'-'.$months_key[$month_last_arr[0]].'-'.sprintf ("%02u", $month_last_arr[1]);
			
			$policy_insert_sql = "INSERT INTO lib_policy_year_periods ( 
									year_id,
									starting_date,
									ending_date,
									actual_starting_date,
									actual_ending_date,
									inserted_by,
									insert_date,
									updated_by
								) VALUES ( 
									$id,
									'$starting_date',
									'$ending_date',
									'$actual_starting_date',
									'$actual_ending_date',
									'".mysql_real_escape_string($user_only)."',
									'$date_time',
									'$updated_by')";
			$result = mysql_query( $policy_insert_sql ) or die( $policy_insert_sql . "<br />" . mysql_error() );
		}				
		//echo "2_" . $id;
		echo "2_";
		exit();
	}
	else {					//Update
	
		$update_sql = "UPDATE $table_name SET
					name = '".mysql_real_escape_string($tax_year_name)."',
					year_start = '$tax_year_start',
					year_end = '$salary_year_end',
					updated_by='".mysql_real_escape_string($user_only)."',
					update_date='$date_time',
					status_active = $tax_status_active
					WHERE id = $tax_year_id AND type = 2";
		mysql_query( $update_sql ) or die( $update_sql . "<br />" . mysql_error() );		
		
		$delete_sql = "DELETE FROM lib_policy_year_periods WHERE year_id = $tax_year_id";
		//echo $delete_sql;die;
		mysql_query( $delete_sql ) or die( $delete_sql . "<br />" . mysql_error() );
		 	
		$id = ( $tax_year_id != 0 ) ? $tax_year_id : $id;		
		for( $i = 1; $i <= $salary_period_counter; $i++ ) {
			$starting_date = "period_" . $i . "_start";
			$ending_date = "period_" . $i . "_end";
			$starting_date = $$starting_date;
			$ending_date = $$ending_date;
			
			$month_start_arr = explode(' ',$starting_date);			
			if($months_key[$month_start_arr[0]]==12) $cbo_year_select=$cbo_year_select+1;			
			$actual_starting_date = $cbo_year_select.'-'.$months_key[$month_start_arr[0]].'-'.sprintf ("%02u", $month_start_arr[1]);
			
			$month_last_arr = explode(' ',$ending_date);			
			$actual_ending_date = $cbo_year_select.'-'.$months_key[$month_last_arr[0]].'-'.sprintf ("%02u", $month_last_arr[1]);
			
			$policy_insert_sql = "INSERT INTO lib_policy_year_periods ( 
									year_id,
									starting_date,
									ending_date,
									actual_starting_date,
									actual_ending_date,
									inserted_by,
									insert_date,
									updated_by
								) VALUES ( 
									$id,
									'$starting_date',
									'$ending_date',
									'$actual_starting_date',
									'$actual_ending_date',
									'".mysql_real_escape_string($user_only)."',
									'$date_time',
									'$updated_by')";
			$result = mysql_query( $policy_insert_sql ) or die( $policy_insert_sql . "<br />" . mysql_error() );
		}
		
		echo "3_" .$tax_year_id;
		exit();
	}	

}
//end tax
if( $action == "edit_leave_year" ) {	
	
	$sql = "SELECT * from lib_policy_year WHERE id = $id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() ); 
	$array_month=array('January'=>1,'February'=>2,'March'=>3,'April'=>4,'May'=>5,'June'=>6,'July'=>7,'August'=>8,'September'=>9,'October'=>10,'November'=>11,'December'=>12);
	//print_r($array_month);die;
	while($inf=mysql_fetch_array($result))
		{
			echo "$('#leave_year_name').val('".mysql_real_escape_string($inf["name"])."')".";\n";
			echo "$('#leave_year_start').val('".mysql_real_escape_string($array_month[$inf["year_start"]])."')".";\n";
			echo "$('#leave_year_end').val('".mysql_real_escape_string($array_month[$inf["year_end"]])."')".";\n";					
			echo "leave_year_calculate();\n";	
			$location = return_field_value("location_name","lib_location","leave_year=$inf[id]");
			//echo "$('#leave_location').val('".mysql_real_escape_string($location)."')".";\n";
			echo "$('#leave_year_id').val('".mysql_real_escape_string($id)."')".";\n";
		}
	exit();
}

if( $action == "edit_salary_year" ) {	
	
	$sql = "SELECT * from lib_policy_year WHERE id = $id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() ); 
	//$array_month=array('January'=>1,'February'=>2,'March'=>3,'April'=>4,'May'=>5,'June'=>6,'July'=>7,'August'=>8,'September'=>9,'October'=>10,'November'=>11,'December'=>12);
	//print_r($array_month);die;
	while($inf=mysql_fetch_array($result))
		{
			echo "$('#salary_year_name').val('".mysql_real_escape_string($inf["name"])."')".";\n";
			//echo "$('#salary_year_start').val('".mysql_real_escape_string($array_month[$inf["year_start"]])."')".";\n";
			//echo "$('#salary_year_end').val('".mysql_real_escape_string($array_month[$inf["year_end"]])."')".";\n";					
			//echo "salary_year_calculate();\n";	
			//$location = return_field_value("location_name","lib_location","leave_year=$inf[id]");
			echo "$('#salary_status_active').val('".mysql_real_escape_string($inf["status_active"])."')".";\n";
			echo "$('#salary_year_id').val('".mysql_real_escape_string($id)."')".";\n";
			
			
		}
		
	$sql_first = "SELECT min(actual_starting_date) as dates from lib_policy_year_periods WHERE year_id = $id";
	$show_query = mysql_query( $sql_first );
	$show_date=mysql_fetch_array($show_query);
	//echo $show_date[dates];die;
	echo "$('#txt_date').val('".convert_to_mysql_date(mysql_real_escape_string( $show_date[dates]))."')".";\n";	

	
	
	$sqls = "SELECT * from lib_policy_year_periods WHERE year_id = $id";
	$results = mysql_query( $sqls ) or die( $sqls . "<br />" . mysql_error() );
	$i=1;
	while($info=mysql_fetch_array($results)){
		
		
		echo "$('#salary_year_starting_date".$i."').val('".(mysql_real_escape_string($info[starting_date]))."')".";\n";
		echo "$('#salary_year_starting_date_hidden".$i."').val('".convert_to_mysql_date(mysql_real_escape_string($info[actual_starting_date]))."')".";\n";
		//echo "$('#salary_year_ending_date".$i."').val('".(mysql_real_escape_string($info[ending_date]))."')".";\n";
		echo "$('#salary_year_ending_date".$i."').val('".(mysql_real_escape_string($info[ending_date]))."')".";\n";
		echo "$('#salary_year_ending_date_hidden".$i."').val('".convert_to_mysql_date(mysql_real_escape_string($info[actual_ending_date]))."')".";\n";
		echo "$('#salary_year_dtls_id".$i."').val('".($info[id])."')".";\n";
		$i++;
		
		
	}
		
		
	exit();
}


if( $action == "edit_financial_year" ) {	
	
	$sql = "SELECT * from ac_year WHERE id = $id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() ); 
	$array_month=array('January'=>1,'February'=>2,'March'=>3,'April'=>4,'May'=>5,'June'=>6,'July'=>7,'August'=>8,'September'=>9,'October'=>10,'November'=>11,'December'=>12);
	//print_r($array_month);die;
	while($inf=mysql_fetch_array($result))
		{
			echo "$('#cbo_company').val('".mysql_real_escape_string($inf["company_id"])."')".";\n";
			echo "$('#financial_year_name').val('".mysql_real_escape_string($inf["name"])."')".";\n";
			echo "$('#financial_year_start').val('".mysql_real_escape_string($array_month[$inf["year_start"]])."')".";\n";			
			echo "$('#financial_year_end').val('".mysql_real_escape_string($array_month[$inf["year_end"]])."')".";\n";				
			if($array_month[$inf["year_start"]]==1){
				echo "$('#cbo_year_select1').val('".mysql_real_escape_string($inf["name"])."')".";\n";	
			}
			echo "$('#is_current').val('".mysql_real_escape_string($inf["is_locked"])."')".";\n";
			//echo "financial_year_calculate();\n";	
			//$location = return_field_value("location_name","lib_location","leave_year=$inf[id]");
			echo "$('#financial_status_active').val('".mysql_real_escape_string($inf["status_active"])."')".";\n";
			echo "$('#financial_year_id').val('".mysql_real_escape_string($id)."')".";\n";
			
			$sql_dtls = "SELECT * from ac_year_periods WHERE year_id = $id";
			$result_dtls = mysql_query( $sql_dtls ) or die( $sql_dtls . "<br />" . mysql_error() ); 
			$i=1;
			while($inf_dtls=mysql_fetch_array($result_dtls))
			{
				echo "$('#financial_year_starting_date_".$i."').val('".mysql_real_escape_string($inf_dtls["starting_date"])."')".";\n";
				echo "$('#financial_year_ending_date_".$i."').val('".mysql_real_escape_string($inf_dtls["ending_date"])."')".";\n";
				echo "$('#financial_year_title_".$i."').val('".mysql_real_escape_string($inf_dtls["financial_period"])."')".";\n";
				if($inf_dtls["period_locked"]==1){
					echo "$('#financial_period_locked_".$i."').attr('checked','true')".";\n";
				}
				
				$i++;
			}
			
		}
	exit();
}

if( $action == "edit_tax_year" ) {	
	
	$sql = "SELECT * from lib_policy_year WHERE id = $id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() ); 
	$array_month=array('January'=>1,'February'=>2,'March'=>3,'April'=>4,'May'=>5,'June'=>6,'July'=>7,'August'=>8,'September'=>9,'October'=>10,'November'=>11,'December'=>12);
	//print_r($array_month);die;
	while($inf=mysql_fetch_array($result))
		{
			echo "$('#tax_year_name').val('".mysql_real_escape_string($inf["name"])."')".";\n";
			echo "$('#tax_year_start').val('".mysql_real_escape_string($array_month[$inf["year_start"]])."')".";\n";
			echo "$('#tax_year_end').val('".mysql_real_escape_string($array_month[$inf["year_end"]])."')".";\n";					
			echo "tax_year_calculate();\n";	
			//$location = return_field_value("location_name","lib_location","leave_year=$inf[id]");
			echo "$('#tax_status_active').val('".mysql_real_escape_string($inf["status_active"])."')".";\n";
			echo "$('#tax_year_id').val('".mysql_real_escape_string($id)."')".";\n";
		}
	exit();
}

function return_field_value($fdata,$tdata,$cdata)
{
	$sql_data="select $fdata from  $tdata where $cdata"; 
	//echo $sql_data;die;
	$sql_data_exe=mysql_query($sql_data);
	$sql_data_rslt=mysql_fetch_array($sql_data_exe);
	$m_data  = $sql_data_rslt[0];
	return $m_data ;
}




function add_month($orgDate,$mon){
	$new_date=explode("-",$orgDate);
	  $cd = strtotime($orgDate);
	  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd)+$mon,$new_date[2],date('Y',$cd)));
	  return $retDAY;
}

function add_date($orgDate,$days){
  $cd = strtotime($orgDate);
  $retDAY = date('Y-m-d', mktime(0,0,0,date('m',$cd),date('d',$cd)+$days,date('Y',$cd)));
  return $retDAY;
}
?>