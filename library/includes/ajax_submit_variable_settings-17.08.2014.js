//Do not change 
function createObject() {
var request_type;
var browser = navigator.appName;
if(browser == "Microsoft Internet Explorer"){
request_type = new ActiveXObject("Microsoft.XMLHTTP");
}else{
request_type = new XMLHttpRequest();
}
return request_type;
}

var http = createObject();


//--------------------------Variable_Settings_Commercial Start-----------------------------------------------------------------------
function fnc_commercial_variable_settings(){
	
	var txt_mst_id						= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_com			= escape(document.getElementById('cbo_company_name_com').value);
	var cbo_variable_list_commercial	= escape(document.getElementById('cbo_variable_list_commercial').value);
	if (cbo_variable_list_commercial==5)
	{
		var capacity_in_value	= escape(document.getElementById('capacity_in_value').innerHTML);
		var txt_capacity_value	= escape(document.getElementById('txt_capacity_value').value);
		var currency			= escape( $('#currency').text() );
		var cbo_currency_id		= escape(document.getElementById('cbo_currency_id').value);
	}
	else if(cbo_variable_list_commercial==6)
	{
		var max_btb_limit		= escape( $('#max_btb_limit').text() );
		var txt_max_btb_limit	= escape(document.getElementById('txt_max_btb_limit').value);
	}
	else if(cbo_variable_list_commercial==7)
	{
		var max_pc_limit		= escape( $('#max_pc_limit').text() );
		var txt_max_pc_limit	= escape(document.getElementById('txt_max_pc_limit').value);
	}
	else if(cbo_variable_list_commercial==17)
	{
		var cost_heads			= escape( $('#cbo_cost_heads').val() );
		var cbo_cost_heads_status	= escape(document.getElementById('cbo_cost_heads_status').value);
	}
	var save_up_com 					= escape(document.getElementById('save_up_comm').value);
	//alert(save_up_com);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_com').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_com').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		if (cbo_variable_list_commercial==5)
		{
			http.open('get','save_update_common_variable_settings.php?action=commercial_variable_settings&isupdate='+save_up_com+
			'&cbo_company_name_com='+cbo_company_name_com+
			'&cbo_variable_list_commercial='+cbo_variable_list_commercial+
			'&capacity_in_value='+capacity_in_value+
			'&txt_capacity_value='+txt_capacity_value+
			'&currency='+currency+
			'&cbo_currency_id='+cbo_currency_id+
			'&nocache ='+nocache);
			//alert(save_up_com);
		}
		else if(cbo_variable_list_commercial==6)
		{
			http.open('get','save_update_common_variable_settings.php?action=commercial_variable_settings&isupdate='+save_up_com+
				'&cbo_company_name_com='+cbo_company_name_com+
				'&cbo_variable_list_commercial='+cbo_variable_list_commercial+
				'&max_btb_limit='+max_btb_limit+
				'&txt_max_btb_limit='+txt_max_btb_limit+
				'&nocache ='+nocache);
				//alert(isupdate_com);
		}
		else if(cbo_variable_list_commercial==7)
		{
				http.open('get','save_update_common_variable_settings.php?action=commercial_variable_settings&isupdate='+save_up_com+
				'&cbo_company_name_com='+cbo_company_name_com+
				'&cbo_variable_list_commercial='+cbo_variable_list_commercial+
				'&max_pc_limit='+max_pc_limit+
				'&txt_max_pc_limit='+txt_max_pc_limit+
				'&nocache ='+nocache);
				//alert(isupdate_com);
		}
		else if(cbo_variable_list_commercial==17)
		{
				http.open('get','save_update_common_variable_settings.php?action=commercial_variable_settings_cost_heads&isupdate='+save_up_com+
				'&cbo_company_name_com='+cbo_company_name_com+
				'&cbo_variable_list_commercial='+cbo_variable_list_commercial+
				'&cost_heads='+cost_heads+
				'&cbo_cost_heads_status='+cbo_cost_heads_status+
				'&nocache ='+nocache);
				//alert(isupdate_com);
		}
		http.onreadystatechange = variable_settings_comReply_info;
		http.send(null); 
	}
}

function variable_settings_comReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		//alert(response);
		cbo_variable_list_commercial=document.getElementById('cbo_variable_list_commercial').value;
		save_up_com=document.getElementById('save_up_comm').value;
		if (response[0]==1)
		{
			if(cbo_variable_list_commercial==17)
			{
				$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
				{ 
					$(this).html('Duplicate Entry.').addClass('messageboxerror').fadeTo(900,1);
					//document.getElementById('save_up').value="";
				});
			}
			else
			{
				$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
				{ 
					$(this).html('There is a Work Order ID, Please use New  Work Order ID.').addClass('messageboxerror').fadeTo(900,1);
					//document.getElementById('save_up').value="";
				});
			}
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_commercial==5)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('capacity_in_value').value="";
					document.getElementById('txt_capacity_value').value="";
					document.getElementById('currency').value="";
					document.getElementById('cbo_currency_id').value="";
				}
				else if(cbo_variable_list_commercial==6)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_btb_limit').value="";
					document.getElementById('txt_max_btb_limit').value="";
				}
				else if(cbo_variable_list_commercial==7)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_pc_limit').value="";
					document.getElementById('txt_max_pc_limit').value="";
				}
				else if(cbo_variable_list_commercial==17)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','update','../');
					$(this).fadeOut(5000);
					showResult_variable_settings(document.getElementById('cbo_company_name_com').value,17,'variable_list_cont4');
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_cost_heads').value=0;
					document.getElementById('cbo_cost_heads_status').value=0;
				}
			});
		}
		else if(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_commercial==5)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('capacity_in_value').value="";
					document.getElementById('txt_capacity_value').value="";
					document.getElementById('currency').value="";
					document.getElementById('cbo_currency_id').value="";
				}
				else if(cbo_variable_list_commercial==6)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_btb_limit').value="";
					document.getElementById('txt_max_btb_limit').value="";
				}
				else if(cbo_variable_list_commercial==7)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_company_name_com').value="";
					document.getElementById('cbo_variable_list_commercial').value="";
					document.getElementById('max_pc_limit').value="";
					document.getElementById('txt_max_pc_limit').value="";
				}
				else if(cbo_variable_list_commercial==17)
				{
					$(this).html('Data Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_commercial','update','../');
					$(this).fadeOut(5000);
					showResult_variable_settings(document.getElementById('cbo_company_name_com').value,17,'variable_list_cont4');
					document.getElementById('save_up_comm').value="";
					document.getElementById('cbo_cost_heads').value=0;
					document.getElementById('cbo_cost_heads_status').value=0;
				}
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Commercial End-----------------------------------------------------------------------


//--------------------------Variable_Settings_Order Tracking Start-----------------------------------------------------------------------
function fnc_order_tracking_variable_settings(){
	
	var txt_mst_id				= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_wo		= escape(document.getElementById('cbo_company_name_wo').value);
	var cbo_variable_list_wo	= escape(document.getElementById('cbo_variable_list_wo').value);
	
	if (cbo_variable_list_wo*1==12)
	{
		var sales_year_started	= escape(document.getElementById('sales_year_started').innerHTML);
		
		var cbo_sales_year_started_date	= escape(document.getElementById('cbo_sales_year_started_date').value);
		
	}
	else if(cbo_variable_list_wo==14)
	{
		
		var cbo_tna_integrated	= escape(document.getElementById('cbo_tna_integrated').value);
	}
	else if(cbo_variable_list_wo==15)
	{
		
		var cbo_profit_calculative	= escape(document.getElementById('cbo_profit_calculative').value);
	}
	else if(cbo_variable_list_wo==18)
	{
		var category_details='';
		var item_category_id='';
		var process_loss_method='';
		var save_up='';
		for(i=1;i<=6;i++)
		{
			item_category_id=document.getElementById('item_category_id_'+i).value;
			process_loss_method=document.getElementById('process_loss_method_'+i).value;
			save_up=document.getElementById('save_up_woo_'+i).value;
			if(category_details=="") 
			{
				category_details=item_category_id+"*"+process_loss_method+"*"+save_up;
			}
			else 
			{
				category_details+=","+item_category_id+"*"+process_loss_method+"*"+save_up;
			}
			
		}

	}
	var save_up_wo 					= escape(document.getElementById('save_up_woo').value);
	
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_wo').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_wo').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_variable_list_wo').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_variable_list_wo').focus();
			$(this).html('Please Select Variable List').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		if (cbo_variable_list_wo==12)
		{
			http.open('get','save_update_common_variable_settings.php?action=order_tracking_variable_settings&isupdate='+save_up_wo+
			'&cbo_company_name_wo='+cbo_company_name_wo+
			'&cbo_variable_list_wo='+cbo_variable_list_wo+
			'&sales_year_started='+sales_year_started+
			'&cbo_sales_year_started_date='+cbo_sales_year_started_date+
			'&nocache ='+nocache);
			//alert(save_up_com);
		}
		else if(cbo_variable_list_wo==14)
		{
			//alert("shajjad");
			http.open('get','save_update_common_variable_settings.php?action=order_tracking_variable_settings&isupdate='+save_up_wo+
			'&cbo_company_name_wo='+cbo_company_name_wo+
			'&cbo_variable_list_wo='+cbo_variable_list_wo+
			'&cbo_tna_integrated='+cbo_tna_integrated+
			'&nocache ='+nocache);
			//alert(save_up_com);
		}
		else if(cbo_variable_list_wo==15)
		{
			//alert("shajjad");
			http.open('get','save_update_common_variable_settings.php?action=order_tracking_variable_settings&isupdate='+save_up_wo+
			'&cbo_company_name_wo='+cbo_company_name_wo+
			'&cbo_variable_list_wo='+cbo_variable_list_wo+
			'&cbo_profit_calculative='+cbo_profit_calculative+
			'&nocache ='+nocache);
			//alert(save_up_com);
		}
		else if(cbo_variable_list_wo==18)
		{
			//alert("shajjad");
			http.open('get','save_update_common_variable_settings.php?action=order_tracking_variable_settings&isupdate='+save_up_wo+
			'&cbo_company_name_wo='+cbo_company_name_wo+
			'&cbo_variable_list_wo='+cbo_variable_list_wo+
			'&category_details='+category_details+
			'&nocache ='+nocache);
			//alert(save_up_com);
		}
		http.onreadystatechange = variable_settings_woReply_info;
		http.send(null); 
	}
}

function variable_settings_woReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		//alert(response);
		cbo_variable_list_wo=document.getElementById('cbo_variable_list_wo').value;
		//alert(cbo_variable_list_wo);
		save_up_wo=document.getElementById('save_up_woo').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Work Order ID, Please use New  Work Order ID.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				save_activities_history('','variable_setting','variable_setting_order','update','../');
				
				if (cbo_variable_list_wo==12)
				{
					
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value="";
					document.getElementById('sales_year_started').value="";
					document.getElementById('cbo_sales_year_started_date').value=0;
				}
				else if(cbo_variable_list_wo==14)
				{
					//$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					//save_activities_history('','variable_setting','variable_setting_order','update','../');
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value=0;
					document.getElementById('cbo_tna_integrated').value=0;
				}
				else if(cbo_variable_list_wo==15)
				{
					//$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					//save_activities_history('','variable_setting','variable_setting_order','update','../');
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value=0;
					document.getElementById('cbo_profit_calculative').value=0;
				}
				else if(cbo_variable_list_wo==18)
				{
					for(i=1;i<=6;i++)
					{
						document.getElementById('save_up_woo_'+i).value="";
						document.getElementById('process_loss_method_'+i).value=1;
					}
					document.getElementById('cbo_company_name_wo').value=0;
					document.getElementById('cbo_variable_list_wo').value=0;
					document.getElementById('save_up_woo').value="";
				}
			});
		}
		else if(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','variable_setting','variable_setting_order','save','../');
				if (cbo_variable_list_wo==12)
				{
					
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value="";
					document.getElementById('sales_year_started').value="";
					document.getElementById('cbo_sales_year_started_date').value="";
				}
				else if(cbo_variable_list_wo==14)
				{
					//$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					//save_activities_history('','variable_setting','variable_setting_order','save','../');
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value="";
					document.getElementById('cbo_tna_integrated').value="";
				}
				else if(cbo_variable_list_wo==15)
				{
					//$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					//save_activities_history('','variable_setting','variable_setting_order','save','../');
					document.getElementById('save_up_woo').value="";
					document.getElementById('cbo_company_name_wo').value="";
					document.getElementById('cbo_variable_list_wo').value="";
					document.getElementById('cbo_profit_calculative').value="";
				}
				else if(cbo_variable_list_wo==18)
				{
					for(i=1;i<=6;i++)
					{
						document.getElementById('save_up_woo_'+i).value="";
						document.getElementById('process_loss_method_'+i).value=1;
					}
					document.getElementById('cbo_company_name_wo').value=0;
					document.getElementById('cbo_variable_list_wo').value=0;
					document.getElementById('save_up_woo').value="";
				}
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Order Tracking End-----------------------------------------------------------------------

//--------------------------Variable_Settings_Production Start-----------------------------------------------------------------------
function fnc_production_variable_settings(){
	
	var txt_mst_id						= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_production			= escape(document.getElementById('cbo_company_name_production').value);
	var cbo_variable_list_production	= escape(document.getElementById('cbo_variable_list_production').value);
	if (cbo_variable_list_production==1)
	{
		var cutting_update				= escape(document.getElementById('cutting_update').innerHTML);
		var cbo_cutting_update			= escape(document.getElementById('cbo_cutting_update').value);
		var printing_emb_production		= escape(document.getElementById('printing_emb_production').innerHTML);
		var cbo_printing_emb_production	= escape(document.getElementById('cbo_printing_emb_production').value);
		var sewing_production			= escape(document.getElementById('sewing_production').innerHTML);
		var cbo_sewing_production		= escape(document.getElementById('cbo_sewing_production').value);
		var iron_update					= escape(document.getElementById('iron_update').innerHTML);
		var cbo_iron_update				= escape(document.getElementById('cbo_iron_update').value);
		var finishing_update			= escape(document.getElementById('finishing_update').innerHTML);
		var cbo_finishing_update		= escape(document.getElementById('cbo_finishing_update').value);
		var cbo_ex_factory				= escape(document.getElementById('cbo_ex_factory').value);
		var cbo_iron_input				= escape(document.getElementById('cbo_iron_input').value);
	}
	else if(cbo_variable_list_production==2)
	{
		var txt_slab_rang_start="";
		var txt_slab_rang_end="";
		var txt_excess_percent="";
		var table = document.getElementById('yarn_container1');
		var rowCount = table.rows.length;
		for(var s=1; s<=rowCount; s++)
		{
			var txt_order_qty_data = escape(document.getElementById('txt_slab_rang_start'+s).value);
			var txt_order_qty_data1 = escape(document.getElementById('txt_slab_rang_end'+s).value);
			var txt_excess_percent_data = escape(document.getElementById('txt_excess_percent'+s).value);
			
			txt_slab_rang_start+=txt_order_qty_data + ",";
			txt_slab_rang_end+=txt_order_qty_data1 + ",";
			txt_excess_percent+=txt_excess_percent_data + ",";
		}
	}
	else if(cbo_variable_list_production==3)
	{
		var fabric_roll_level		= escape(document.getElementById('fabric_roll_level').innerHTML);
		var cbo_fabric_roll_level	= escape(document.getElementById('cbo_fabric_roll_level').value);
	}
	else if(cbo_variable_list_production==4)
	{
		var fabric_machine_level		= escape(document.getElementById('fabric_machine_level').innerHTML);
		var cbo_fabric_machine_level	= escape(document.getElementById('cbo_fabric_machine_level').value);
	}
	else
	{
		var batch_maintained		= escape(document.getElementById('batch_maintained').innerHTML);
		var cbo_batch_maintained	= escape(document.getElementById('cbo_batch_maintained').value);
	}
	var save_up_pro 					= escape(document.getElementById('save_up_proo').value);
	//alert(save_up_pro);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_production').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_production').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		if (cbo_variable_list_production==1)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
				'&cbo_company_name_production='+cbo_company_name_production+
				'&cbo_variable_list_production='+cbo_variable_list_production+
				'&cutting_update='+cutting_update+
				'&cbo_cutting_update='+cbo_cutting_update+
				'&printing_emb_production='+printing_emb_production+
				'&cbo_printing_emb_production='+cbo_printing_emb_production+
				'&sewing_production='+sewing_production+
				'&cbo_sewing_production='+cbo_sewing_production+
				'&iron_update='+iron_update+
				'&cbo_iron_update='+cbo_iron_update+
				'&finishing_update='+finishing_update+
				'&cbo_finishing_update='+cbo_finishing_update+
				'&cbo_ex_factory='+cbo_ex_factory+
				'&cbo_iron_input='+cbo_iron_input+
				'&nocache ='+nocache);
				//alert(save_up_pro);
			}
		else if(cbo_variable_list_production==2)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings_excess_cutting_slab&isupdate='+save_up_pro+
					'&cbo_company_name_production='+cbo_company_name_production+
					'&cbo_variable_list_production='+cbo_variable_list_production+
					'&txt_slab_rang_start='+txt_slab_rang_start+
					'&txt_slab_rang_end='+txt_slab_rang_end+
					'&txt_excess_percent='+txt_excess_percent+
					'&nocache ='+nocache);
					//alert(save_up_pro);
			}
		else if(cbo_variable_list_production==3)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
					'&cbo_company_name_production='+cbo_company_name_production+
					'&cbo_variable_list_production='+cbo_variable_list_production+
					'&fabric_roll_level='+fabric_roll_level+
					'&cbo_fabric_roll_level='+cbo_fabric_roll_level+
					'&nocache ='+nocache);
					//alert(save_up_pro);
			}
		else if(cbo_variable_list_production==4)
			{
				http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
					'&cbo_company_name_production='+cbo_company_name_production+
					'&cbo_variable_list_production='+cbo_variable_list_production+
					'&fabric_machine_level='+fabric_machine_level+
					'&cbo_fabric_machine_level='+cbo_fabric_machine_level+
					'&nocache ='+nocache);
					//alert(save_up_pro);
			}
		else
		{
			http.open('get','save_update_common_variable_settings.php?action=production_variable_settings&isupdate='+save_up_pro+
				'&cbo_company_name_production='+cbo_company_name_production+
				'&cbo_variable_list_production='+cbo_variable_list_production+
				'&batch_maintained='+batch_maintained+
				'&cbo_batch_maintained='+cbo_batch_maintained+
				'&nocache ='+nocache);
				//alert(save_up_pro);
		}
		http.onreadystatechange = variable_settings_proReply_info;
		http.send(null); 
	}
}

function variable_settings_proReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		//alert(response);
		cbo_variable_list_production=document.getElementById('cbo_variable_list_production').value;
		save_up_pro=document.getElementById('save_up_proo').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Work Order ID, Please use New  Work Order ID.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_production==1)
				{
					
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('cutting_update').value="";
					document.getElementById('cbo_cutting_update').value="";
					document.getElementById('printing_emb_production').value="";
					document.getElementById('cbo_printing_emb_production').value="";
					document.getElementById('sewing_production').value="";
					document.getElementById('cbo_sewing_production').value="";
					document.getElementById('iron_update').value="";
					document.getElementById('cbo_iron_update').value="";
					document.getElementById('finishing_update').value="";
					document.getElementById('cbo_finishing_update').value="";
					document.getElementById('cbo_ex_factory').value="";
					document.getElementById('cbo_iron_input').value="";
				}
				else if(cbo_variable_list_production==2)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					//document.getElementById('txt_slab_rang_start').value="";
					//document.getElementById('txt_slab_rang_end').value="";
					//document.getElementById('txt_excess_percent').value="";
				}
				else if(cbo_variable_list_production==3)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_roll_level').value="";
					document.getElementById('cbo_fabric_roll_level').value="";
				}
				else if(cbo_variable_list_production==4)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_machine_level').value="";
					document.getElementById('cbo_fabric_machine_level').value="";
				}
				else
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('batch_maintained').value="";
					document.getElementById('cbo_batch_maintained').value="";
				}
			});
		}
		else if(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_production==1)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('cutting_update').value="";
					document.getElementById('cbo_cutting_update').value="";
					document.getElementById('printing_emb_production').value="";
					document.getElementById('cbo_printing_emb_production').value="";
					document.getElementById('sewing_production').value="";
					document.getElementById('cbo_sewing_production').value="";
					document.getElementById('iron_update').value="";
					document.getElementById('cbo_iron_update').value="";
					document.getElementById('finishing_update').value="";
					document.getElementById('cbo_finishing_update').value="";
					document.getElementById('cbo_ex_factory').value="";
					document.getElementById('cbo_iron_input').value="";
				}
				else if(cbo_variable_list_production==2)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					//document.getElementById('txt_slab_rang_start').value="";
					//document.getElementById('txt_slab_rang_end').value="";
					//document.getElementById('txt_excess_percent').value="";
				}
				else if(cbo_variable_list_production==3)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_roll_level').value="";
					document.getElementById('cbo_fabric_roll_level').value="";
				}
				else if(cbo_variable_list_production==4)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('fabric_machine_level').value="";
					document.getElementById('cbo_fabric_machine_level').value="";
				}
				else
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_production','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_proo').value="";
					document.getElementById('cbo_company_name_production').value="";
					document.getElementById('cbo_variable_list_production').value="";
					document.getElementById('batch_maintained').value="";
					document.getElementById('cbo_batch_maintained').value="";
				}
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Production End-----------------------------------------------------------------------

//--------------------------Variable_Settings_Inventory Start-----------------------------------------------------------------------

function fnc_inventory_variable_settings(){
	
	var txt_mst_id						= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_inventory		= escape(document.getElementById('cbo_company_name_inventory').value);
	var cbo_variable_list_inventory		= escape(document.getElementById('cbo_variable_list_inventory').value);
	if (cbo_variable_list_inventory==8)
	{
		
		var cbo_category="";
		var cbo_item_group="";
		var cbo_source="";
		var txt_standard="";
		var table = document.getElementById('yarn_container');
		var rowCount = table.rows.length;
	
		for(var s=1; s<=rowCount; s++)
		{
			
			var cbo_category_data = escape(document.getElementById('cbo_category'+s).value);
			var cbo_item_group_data = escape(document.getElementById('cbo_item_group'+s).value);
			var cbo_source_data = escape(document.getElementById('cbo_source'+s).value);
			var txt_standard_data = escape(document.getElementById('txt_standard'+s).value);
			
			cbo_category+=cbo_category_data + ",";
			cbo_item_group+=cbo_item_group_data + ",";
			cbo_source+=cbo_source_data + ",";
			txt_standard+=txt_standard_data + ",";
		}
	}
	else if(cbo_variable_list_inventory==9)
	{
		var hide_opening_stock_flag			= escape(document.getElementById('hide_opening_stock_flag').innerHTML);
		var cbo_hide_opening_stock_flag		= escape(document.getElementById('cbo_hide_opening_stock_flag').value);
	}
	else if(cbo_variable_list_inventory==10)
	{
		var item_rate_optional		= escape(document.getElementById('item_rate_optional').innerHTML);
		var cbo_item_rate_optional	= escape(document.getElementById('cbo_item_rate_optional').value);
	}
	else if(cbo_variable_list_inventory==11)
	{
		var item_qc			= escape(document.getElementById('item_qc').innerHTML);
		var cbo_item_qc		= escape(document.getElementById('cbo_item_qc').value);
	}
	else
	{
		var item_category		= escape(document.getElementById('item_category').innerHTML);
		var cbo_item_category	= escape(document.getElementById('cbo_item_category').value);
		var cbo_item_status		= escape(document.getElementById('cbo_item_status').value);
	}
	var save_up_inven 					= escape(document.getElementById('save_up_invenn').value);
	
	//alert(save_up_inven); 
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_inventory').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_inventory').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_variable_list_inventory').val()==16 && $('#cbo_item_category').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#item_category').focus();
			$(this).html('Please Select Item Category').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_variable_list_inventory').val()==16 && $('#cbo_item_status').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_item_status').focus();
			$(this).html('Please Select Staus').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else
	{					
		nocache = Math.random();
		if (cbo_variable_list_inventory==8)
			{
				http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings_ile_cost_standard&isupdate='+save_up_inven+
				'&cbo_company_name_inventory='+cbo_company_name_inventory+
				'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
				'&cbo_category='+cbo_category+
				'&cbo_item_group='+cbo_item_group+
				'&cbo_source='+cbo_source+
				'&txt_standard='+txt_standard+
				'&nocache ='+nocache);
				//alert(save_up_inven);
			}
		else if(cbo_variable_list_inventory==9)
			{
				http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings&isupdate='+save_up_inven+
					'&cbo_company_name_inventory='+cbo_company_name_inventory+
					'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
					'&hide_opening_stock_flag='+hide_opening_stock_flag+
					'&cbo_hide_opening_stock_flag='+cbo_hide_opening_stock_flag+
					'&nocache ='+nocache);
					//alert(save_up_inven);
			}
		else if(cbo_variable_list_inventory==10)
			{
				http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings&isupdate='+save_up_inven+
					'&cbo_company_name_inventory='+cbo_company_name_inventory+
					'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
					'&item_rate_optional='+item_rate_optional+
					'&cbo_item_rate_optional='+cbo_item_rate_optional+
					'&nocache ='+nocache);
					//alert(save_up_inven);
			}
		else if(cbo_variable_list_inventory==11)
		{
			http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings&isupdate='+save_up_inven+
				'&cbo_company_name_inventory='+cbo_company_name_inventory+
				'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
				'&item_qc='+item_qc+
				'&cbo_item_qc='+cbo_item_qc+
				'&nocache ='+nocache);
				//alert(save_up_inven);
		}
		else
		{
			http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings_user_given_code&isupdate='+save_up_inven+
				'&cbo_company_name_inventory='+cbo_company_name_inventory+
				'&cbo_variable_list_inventory='+cbo_variable_list_inventory+
				'&cbo_item_category='+cbo_item_category+
				'&cbo_item_status='+cbo_item_status+
				'&nocache ='+nocache);
				//alert(save_up_inven);
		}
		http.onreadystatechange = variable_settings_inventoryReply_info;
		http.send(null); 
	}
}

function variable_settings_inventoryReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		//alert(http.responseText);
		cbo_variable_list_inventory=document.getElementById('cbo_variable_list_inventory').value;
		save_up_inven=document.getElementById('save_up_invenn').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Entry.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_inventory==8)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					//document.getElementById('cbo_category').value="";
					//document.getElementById('cbo_item_group').value="";
					//document.getElementById('cbo_source').value="";
					//document.getElementById('txt_standard').value="";
				}
				else if(cbo_variable_list_inventory==9)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('hide_opening_stock_flag').value="";
					document.getElementById('cbo_hide_opening_stock_flag').value="";
				}
				else if(cbo_variable_list_inventory==10)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_rate_optional').value="";
					document.getElementById('cbo_item_rate_optional').value="";
				}
				else if(cbo_variable_list_inventory==11)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_qc').value="";
					document.getElementById('cbo_item_qc').value="";
				}
				else
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','update','../');
					$(this).fadeOut(10000);
					showResult_variable_settings(document.getElementById('cbo_company_name_inventory').value,16,'variable_list_cont5');
					document.getElementById('cbo_item_category').value=0;
					document.getElementById('cbo_item_status').value=0;
					document.getElementById('save_up_invenn').value="";
				}
			});
		}
		else if(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if (cbo_variable_list_inventory==8)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					//document.getElementById('cbo_category').value="";
					//document.getElementById('cbo_item_group').value="";
					//document.getElementById('cbo_source').value="";
					//document.getElementById('txt_standard').value="";
				}
				else if(cbo_variable_list_inventory==9)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('hide_opening_stock_flag').value="";
					document.getElementById('cbo_hide_opening_stock_flag').value="";
				}
				else if(cbo_variable_list_inventory==10)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_rate_optional').value="";
					document.getElementById('cbo_item_rate_optional').value="";
				}
				else if(cbo_variable_list_inventory==11)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_invenn').value="";
					document.getElementById('cbo_company_name_inventory').value="";
					document.getElementById('cbo_variable_list_inventory').value="";
					document.getElementById('item_qc').value="";
					document.getElementById('cbo_item_qc').value="";
				}
				else
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					save_activities_history('','variable_setting','variable_setting_inventory','save','../');
					$(this).fadeOut(10000);
					showResult_variable_settings(document.getElementById('cbo_company_name_inventory').value,16,'variable_list_cont5');
					document.getElementById('cbo_item_category').value=0;
					document.getElementById('cbo_item_status').value=0;
					document.getElementById('save_up_invenn').value="";
				}
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Inventory End-----------------------------------------------------------------------
//--------------------------Variable_Settings_Accounts Start-----------------------------------------------------------------------

function fnc_wo_save_order_details_top(){
	
	var txt_mst_id						= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_account		= escape(document.getElementById('cbo_company_name_account').value);
	var cbo_variable_list_accounts		= escape(document.getElementById('cbo_variable_list_accounts').value);
	 
	 if(cbo_variable_list_accounts==31)
	{
		//var item_qc			= escape(document.getElementById('item_qc').innerHTML);
		var cbo_integreted		= escape(document.getElementById('cbo_integreted').value);
	}
	 
	var save_up_ac 					= escape(document.getElementById('save_up_ac').value);
	
	//alert(save_up_inven); 
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#cbo_company_name_account').val()==0){						
	
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_inventory').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	 
	else
	{					
		nocache = Math.random();
		 
		 if(cbo_variable_list_accounts==31)
		{
			http.open('get','save_update_common_variable_settings.php?action=accounts_variable_settings&isupdate='+save_up_ac+
				'&cbo_company_name_account='+cbo_company_name_account+
				'&cbo_variable_list_accounts='+cbo_variable_list_accounts+
				'&cbo_integreted='+cbo_integreted+
				'&nocache ='+nocache);
				//alert(save_up_inven);
		}
		else
		{
			http.open('get','save_update_common_variable_settings.php?action=inventory_variable_settings_user_given_code&isupdate='+save_up_ac+
				'&cbo_company_name_account='+cbo_company_name_account+
				'&cbo_variable_list_accounts='+cbo_variable_list_accounts+
				'&cbo_integreted='+cbo_integreted+ 
				'&nocache ='+nocache);
				//alert(save_up_inven);
		}
		http.onreadystatechange = variable_settings_inventoryReply_info;
		http.send(null); 
	}
}

function variable_settings_inventoryReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');
		//alert(http.responseText);
		cbo_variable_list_accounts=document.getElementById('cbo_variable_list_accounts').value;
		save_up_ac=document.getElementById('save_up_ac').value;
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Entry.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{  
				 if(cbo_variable_list_inventory==31)
				{
					$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					//save_activities_history('','variable_setting','variable_setting_inventory','update','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_ac').value="";
					document.getElementById('cbo_company_name_account').value="";
					document.getElementById('cbo_variable_list_accounts').value=""; 
				}
				 
			});
		}
		else if(response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				if(cbo_variable_list_inventory==31)
				{
					$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
					//save_activities_history('','variable_setting','variable_setting_inventory','save','../');
					$(this).fadeOut(5000);
					document.getElementById('save_up_ac').value="";
					document.getElementById('cbo_company_name_account').value="";
					document.getElementById('cbo_variable_list_accounts').value=""; 
				}
				 
			});
		}
		
	}
}	



//--------------------------Variable_Settings_Accounts End-----------------------------------------------------------------------
//---------------------------Variable_Settings_HRM_Start---------------------------------------------------------------------
function fnc_hrm_variable_settings(save_perm,edit_perm,delete_perm,approve_perm){
	
	var txt_mst_id					= escape(document.getElementById('txt_mst_id').value);
	var cbo_company_name_hrm		= escape(document.getElementById('cbo_company_name_hrm').value);
	
	//var cbo_month_selector			= escape(document.getElementById('cbo_month_selector').value);
	
	var txt_emp_code_length			= escape(document.getElementById('txt_emp_code_length').value);
	var txt_card_no_length			= escape(document.getElementById('txt_card_no_length').value);
	var cbo_save_login_history		= escape(document.getElementById('cbo_save_login_history').value);
	var txt_total_working_day		= escape(document.getElementById('txt_total_working_day').value);
	var txt_total_working_hour		= escape(document.getElementById('txt_total_working_hour').value);
	var cbo_currency_id				= escape(document.getElementById('cbo_currency_id').value);
	var txt_salary_above			= escape(document.getElementById('txt_salary_above').value);
	var txt_payment_in_bank			= escape(document.getElementById('txt_payment_in_bank').value);
	
	var txt_adjust_in_time			= escape(document.getElementById('txt_adjust_in_time').value);
	var txt_adjust_out_time			= escape(document.getElementById('txt_adjust_out_time').value);
	var txt_maximum_in_time			= escape(document.getElementById('txt_maximum_in_time').value);
	var txt_complains_ot			= escape(document.getElementById('txt_complains_ot').value);
	var txt_one_hour_ot_unit		= escape(document.getElementById('txt_one_hour_ot_unit').value);
	var cbo_allow_ot_fraction		=escape(document.getElementById('cbo_allow_ot_fraction').value);
	var txt_ot_start_minute			= escape(document.getElementById('txt_ot_start_minute').value);
	var txt_last_punch_time_with_ot	= escape(document.getElementById('txt_last_punch_time_with_ot').value);
	var txt_tiffin_start_time		= escape(document.getElementById('txt_tiffin_start_time').value);
	var txt_dinner_start_time		= escape(document.getElementById('txt_dinner_start_time').value);
	var cbo_not_pay_deduction		= escape(document.getElementById('cbo_not_pay_deduction').value);
	var cbo_salary_type				= escape(document.getElementById('cbo_salary_type').value);
	var card_chr_new		        = escape(document.getElementById('card_chr_new').value);
	var cbo_late_deduction_from_ot	= escape(document.getElementById('cbo_late_deduction_from_ot').value);
	var cbo_early_deduction_from_ot	= escape(document.getElementById('cbo_early_deduction_from_ot').value);
	var cbo_show_gross_total_on_pay_slip= escape(document.getElementById('cbo_show_gross_total_on_pay_slip').value);
	var cbo_salary_sheet_format		= escape(document.getElementById('cbo_salary_sheet_format').value);
	var cbo_salary_top_sheet_format		= escape(document.getElementById('cbo_salary_top_sheet_format').value);
	var cbo_pay_slip_format			= escape(document.getElementById('cbo_pay_slip_format').value);
	var cbo_ot_dinner				= escape(document.getElementById('cbo_ot_dinner').value);
	var txt_dinner_ot_min			= escape(document.getElementById('txt_dinner_ot_min').value);
	var txt_overtime_rate_value			= escape(document.getElementById('txt_overtime_rate_value').value);
	var cbo_att_bonus_leave_type		= escape(document.getElementById('cbo_att_bonus_leave_type').value);
	var cbo_in_out_time_format		= escape(document.getElementById('cbo_in_out_time_format').value);
	var cbo_reqn_based_ot		= escape(document.getElementById('cbo_reqn_based_ot').value);
	var cbo_device_fixed		= escape(document.getElementById('cbo_device_fixed').value);
	var cbo_outtime_reqn		= escape(document.getElementById('cbo_outtime_reqn').value);
	var emp_budget_control		= escape(document.getElementById('emp_budget_control').value);
	var treat_as_resign			= escape(document.getElementById('treat_as_resign').value);
	var cbo_access_control		= escape(document.getElementById('cbo_access_control').value);
	var emp_abs_deduct			= escape(document.getElementById('new_emp_abs_deduct').value);
	
	var cbo_card_pattern		    = escape(document.getElementById('cbo_card_pattern').value);
	if(cbo_card_pattern==1){  var pattern_integreted	= escape(document.getElementById('cbo_write').value);}
	else if(cbo_card_pattern==2){ var pattern_integreted	= escape(document.getElementById('cbo_code_employe').value);}
	else if(cbo_card_pattern==3){ var pattern_integreted	= escape(document.getElementById('given_emp_code').value);}
	else if(cbo_card_pattern==4){ var pattern_integreted	= document.getElementById('blanding_option').value;}
	
	var ignor_quest_ot 				= escape(document.getElementById('cbo_ignor_questionable_ot').value);
	var ot_manipulation 			= escape(document.getElementById('ot_manipulation').value);
	var cbl_out 					= escape(document.getElementById('cbl_out').value);
	var save_up_hrm 				= escape(document.getElementById('save_up_hrm').value);
	//alert (ot_manipulation);return;
	//alert(cbo_salary_sheet_format+cbo_pay_slip_format);	
	//alert(cbo_salary_top_sheet_format);				
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if (save_up_hrm=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up_hrm!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#cbo_company_name_hrm').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_hrm').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_overtime_rate_value').val()<1){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_overtime_rate_value').focus();
			$(this).html('Please Enter a valid Overtime rate').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	else{					
		nocache = Math.random();
		http.open('get','save_update_common_variable_settings.php?action=hrm_variable_settings&isupdate='+save_up_hrm+
					'&cbo_company_name_hrm='+cbo_company_name_hrm+
					'&txt_emp_code_length='+txt_emp_code_length+
					'&txt_card_no_length='+txt_card_no_length+
					'&cbo_save_login_history='+cbo_save_login_history+
					'&txt_total_working_day='+txt_total_working_day+
					'&txt_total_working_hour='+txt_total_working_hour+
					'&cbo_currency_id='+cbo_currency_id+
					'&txt_salary_above='+txt_salary_above+
					'&txt_payment_in_bank='+txt_payment_in_bank+
					'&txt_adjust_in_time='+txt_adjust_in_time+
					'&txt_adjust_out_time='+txt_adjust_out_time+
					'&txt_maximum_in_time='+txt_maximum_in_time+
					'&txt_complains_ot='+txt_complains_ot+
					'&txt_one_hour_ot_unit='+txt_one_hour_ot_unit+
					'&cbo_allow_ot_fraction='+cbo_allow_ot_fraction+
					'&txt_ot_start_minute='+txt_ot_start_minute+
					'&txt_last_punch_time_with_ot='+txt_last_punch_time_with_ot+
					'&txt_tiffin_start_time='+txt_tiffin_start_time+
					'&txt_dinner_start_time='+txt_dinner_start_time+
					'&cbo_not_pay_deduction='+cbo_not_pay_deduction+
					'&cbo_salary_type='+cbo_salary_type+
					'&card_chr_new='+card_chr_new+
					'&cbo_late_deduction_from_ot='+cbo_late_deduction_from_ot+
					'&cbo_early_deduction_from_ot='+cbo_early_deduction_from_ot+
					'&cbo_show_gross_total_on_pay_slip='+cbo_show_gross_total_on_pay_slip+
					'&cbo_salary_sheet_format='+cbo_salary_sheet_format+
					'&cbo_salary_top_sheet_format='+cbo_salary_top_sheet_format+
					'&cbo_pay_slip_format='+cbo_pay_slip_format+
					'&cbo_ot_dinner='+cbo_ot_dinner+
					'&txt_dinner_ot_min='+txt_dinner_ot_min+
					'&txt_overtime_rate_value='+txt_overtime_rate_value+
					'&cbo_att_bonus_leave_type='+cbo_att_bonus_leave_type+
					'&cbo_in_out_time_format='+cbo_in_out_time_format+
					'&cbo_reqn_based_ot='+cbo_reqn_based_ot+
					'&cbo_device_fixed='+cbo_device_fixed+
					'&cbo_outtime_reqn='+cbo_outtime_reqn+
					'&emp_budget_control='+emp_budget_control+
					'&treat_as_resign='+treat_as_resign+
					'&emp_abs_deduct='+emp_abs_deduct+
					'&cbo_access_control='+cbo_access_control+
					'&cbo_card_pattern='+cbo_card_pattern+
					'&pattern_integreted='+pattern_integreted+
					'&ignor_quest_ot='+ignor_quest_ot+
					'&ot_manipulation='+ot_manipulation+
					'&cbl_out='+cbl_out+
					'&nocache ='+nocache);
		http.onreadystatechange = hrm_variable_settings_reply;
		http.send(null); 
	}
}
//'&cbo_round_salary_head='+cbo_round_salary_head+emp_abs_deduct
//'&cbo_ot_calculation_type='+cbo_ot_calculation_type+
function hrm_variable_settings_reply() {
	if(http.readyState == 4){
		
		var response = http.responseText.split('_');
	 	// alert(response[0]);
		
		if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				save_activities_history('','variable_setting','variable_setting_hrm','update','../');
				
				document.getElementById('txt_mst_id').value=response[1];
				//showResult_variable_settings_hrm(' ','4','bank_list_view');
				//document.getElementById('txt_mst_id').value="";
			});
		}
		else if (response[0]==11)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('This month is locked for variable setting.').addClass('messagebox_ok').fadeTo(900,1);
				
			});
		}
		else
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				save_activities_history('','variable_setting','variable_setting_hrm','save','../');
				
				document.getElementById('txt_mst_id').value=response[1];
				//showResult(' ','4','bank_list_view');
				//document.getElementById('txt_mst_id').value="";
			});
		}
	}
}	
//---------------------------Variable_Settings_HRM_Start-----------------------------------------------------------------------

//---------------------------Variable_Settings_HRM_Start---------------------------------------------------------------------
function fnc_report_sign_variable_settings(save_perm,edit_perm,delete_perm,approve_perm){
	
	var txt_mst_id_rtrn				= escape(document.getElementById('txt_mst_id_rtrn').value);
	var cbo_company_name		    = escape(document.getElementById('cbo_company_name').value);
	var cbo_location_id			    = escape(document.getElementById('cbo_location_id').value);
	var cbo_module_name			    = escape(document.getElementById('cbo_module_name').value);
	var signeture_list              = escape(document.getElementById('signeture_list').value);
	//alert (cbo_location_id);
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if (txt_mst_id_rtrn=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (txt_mst_id_rtrn!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if(cbo_company_name==0 ){					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	} 
	else if(cbo_module_name==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_module_name').focus();
			$(this).html('Please Select Module Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if(signeture_list==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#signeture_list').focus();
			$(this).html('Please Enter Signeture List').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common_variable_settings.php?action=report_variable_settings&mst_id='+txt_mst_id_rtrn+
					'&company='+cbo_company_name+
					'&location='+cbo_location_id+
					'&module='+cbo_module_name+
					'&signeture_list='+signeture_list+
					'&nocache ='+nocache);
		http.onreadystatechange = report_variable_settings_reply;
		http.send(null); 
	}
}

function report_variable_settings_reply() {
	if(http.readyState == 4){
		//alert (http.responseText);
		var response = http.responseText.split('_');
		if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				save_activities_history('','variable_setting','variable_setting_hrm','update','../');				
				document.getElementById('txt_mst_id_rtrn').value=response[1];
			});
		}
		else if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				save_activities_history('','variable_setting','variable_setting_hrm','save','../');				
				document.getElementById('txt_mst_id_rtrn').value=response[1];
			});
		}
		else
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Failed to Save.').addClass('messagebox_ok').fadeTo(900,1);				
			});
		}
	}
}	
//---------------------------Variable_Settings_HRM_Start-----------------------------------------------------------------------

//------------------------------------ID CARD VARIABLA SETTING---------------------------sohel---------------------
function fnc_idcard_variable_settings()
{
	var cbo_company_name				= escape(document.getElementById('cbo_company_name_idcard').value);
	var cbo_card_pattern		    = escape(document.getElementById('cbo_card_pattern').value);
	var update_id		    = escape(document.getElementById('update_id').value);
	//var cbo_module_name			    = escape(document.getElementById('cbo_module_name').value);
	//var signeture_list              = escape(document.getElementById('signeture_list').value);
	if(cbo_card_pattern==1){  var pattern_integreted	= escape(document.getElementById('cbo_write').value);}
	else if(cbo_card_pattern==2){ var pattern_integreted	= escape(document.getElementById('cbo_code_employe').value);}
	else if(cbo_card_pattern==3){ var pattern_integreted	= escape(document.getElementById('given_emp_code').value);}
	else if(cbo_card_pattern==4){ var pattern_integreted	= document.getElementById('hidden_blanding_option_id').value;}
	//alert (pattern_integreted);
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	/*if (txt_mst_id_rtrn=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (txt_mst_id_rtrn!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	 */
	 if(cbo_company_name==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name_idcard').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if(cbo_card_pattern==0 ){					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_card_pattern').focus();
			$(this).html('Please Select Card pattern').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if(pattern_integreted==0 ){					
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_write').focus();
			$(this).html('Please Select Integreted Option').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common_variable_settings.php?action=idcard_variable_settings&update_id='+update_id+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_card_pattern='+cbo_card_pattern+
					'&pattern_integreted='+pattern_integreted+
					'&nocache ='+nocache);
		http.onreadystatechange = idcard_variable_settings_reply;
		http.send(null); 
	}
	
}


function idcard_variable_settings_reply() {
	if(http.readyState == 4)
	{
		alert (http.responseText);
		//var response = http.responseText.split('_');
		/*if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				save_activities_history('','variable_setting','variable_setting_hrm','update','../');				
				document.getElementById('txt_mst_id_rtrn').value=response[1];
			});
		}
		else if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				save_activities_history('','variable_setting','variable_setting_hrm','save','../');				
				document.getElementById('txt_mst_id_rtrn').value=response[1];
			});
		}
		else
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Failed to Save.').addClass('messagebox_ok').fadeTo(900,1);				
			});
		}*/
		 	
	}
}	

