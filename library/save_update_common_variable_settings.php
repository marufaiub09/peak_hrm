<?php
date_default_timezone_set('Asia/Dhaka');
session_start();
$date=time();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
$date=time();

$user_name=$_SESSION['logic_erp']["user_name"];
$date_time= date("Y-m-d H:i:s");
$sql_history = "";
include('../includes/common.php');
include('../includes/common_functions.php');
extract($_GET);



//--------------------------Variable_Settings_Commercial Start-----------------------------------------------------------------------

if ($action=="commercial_variable_settings"){ 
	
   if  ($isupdate==""){//Insert Here
   		
	
		$unique_field_name = "company_name";
		$table_name = "variable_settings_commercial";
		$query = "company_name='$cbo_company_name_com' and variable_list='$cbo_variable_list_commercial'";

		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "variable_settings_commercial";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO variable_settings_commercial(
							id,
							company_name,
							variable_list,
							capacity_in_value_hcode,
							capacity_in_value,
							currency_hcode,
							currency_id,
							max_btb_limit_hcode,
							max_btb_limit,
							max_pc_limit_hcode,
							max_pc_limit,
							cost_heads,
							cost_heads_status,
							inserted_by,
							insert_date,
							status_active
						) VALUES (
							'$id',
							'$cbo_company_name_com',
							'$cbo_variable_list_commercial',
							'$capacity_in_value',
							'$txt_capacity_value',
							'$currency',
							'$cbo_currency_id',
							'$max_btb_limit',
							'$txt_max_btb_limit',
							'$max_pc_limit',
							'$txt_max_pc_limit',
							'',
							'0',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1'
						)";
		//print_r($_GET);
		//exit();
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_".$id;
		exit();
	}  else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_commercial
				SET
					company_name			= '$cbo_company_name_com',
					variable_list			= '$cbo_variable_list_commercial',
					capacity_in_value_hcode	= '$capacity_in_value',
					capacity_in_value		= '$txt_capacity_value',
					currency_hcode			= '$currency',
					currency_id				= '$cbo_currency_id',
					max_btb_limit_hcode		= '$max_btb_limit',
					max_btb_limit			= '$txt_max_btb_limit',
					max_pc_limit_hcode		= '$max_pc_limit',
					max_pc_limit			= '$txt_max_pc_limit',
					updated_by				= '".mysql_real_escape_string($user_name)."',
					update_date 			= '$date',
					status_active			= '1'
					WHERE id				= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_".$id;		
		exit();
	}
	
}

//--------------------------Variable_Settings_Commercial End---------------------------------------------------------------------

//--------------------------Variable_Settings_Order Tracking Start-----------------------------------------------------------------------

if ($action=="order_tracking_variable_settings")
{ 

	if($cbo_variable_list_wo==18)
	{
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 

		$all_category=explode(",",$category_details);
		
		for($i=0;$i<count($all_category);$i++)
		{
			$item_category_details=explode("*",$all_category[$i]);

			if($item_category_details[2]=="" ) 
			{
				$id_field_name = "id";
				$table_name = "variable_settings_order_tracking";
				$id= return_next_id($id_field_name,$table_name);
			
				$sql = "INSERT INTO variable_settings_order_tracking(
									id,
									company_name,
									variable_list,
									sales_year_started_hcode,
									sales_year_started,
									tna_integrated,
									profit_calculative,
									process_loss_method,
									item_category_id,
									inserted_by,
									insert_date,
									status_active
								) VALUES (
									'$id',
									'$cbo_company_name_wo',
									'$cbo_variable_list_wo',
									'$sales_year_started',
									'$cbo_sales_year_started_date',
									'$cbo_tna_integrated',
									'$cbo_profit_calculative',
									'$item_category_details[1]',
									'$item_category_details[0]',
									'".mysql_real_escape_string($user_name)."',
									'$date',
									'1'
								)";
				mysql_query( $sql ) or die (mysql_error());	
				$sql_history.=$sql;
			}
			else
			{
				$sql = "UPDATE variable_settings_order_tracking
						SET
							company_name				= '$cbo_company_name_wo',
							variable_list				= '$cbo_variable_list_wo',
							process_loss_method			= '$item_category_details[1]',
							item_category_id			= '$item_category_details[0]',
							updated_by					= '".mysql_real_escape_string($user_name)."',
							update_date 				= '$date',
							status_active				= '1'
							WHERE id					= '$item_category_details[2]'";
				mysql_query( $sql ) or die (mysql_error());
				
				$sql_history.=$sql;
			}
			
		}
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		if($isupdate==0) echo "2_".$id;	 else echo "3_".$id;
		exit();
	}
	else
	{
		if($isupdate=="")
		{//Insert Here
			$unique_field_name = "company_name";
			$table_name = "variable_settings_order_tracking";
			$query = "company_name='$cbo_company_name_wo' and variable_list='$cbo_variable_list_wo'";
			
			$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
			if( $unique_check == false )  // Duplicate
			{
				echo "1";
				exit();
			}
			
			$id_field_name = "id";
			$table_name = "variable_settings_order_tracking";
			$id= return_next_id($id_field_name,$table_name);
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
	
	
			$sql = "INSERT INTO variable_settings_order_tracking(
								id,
								company_name,
								variable_list,
								sales_year_started_hcode,
								sales_year_started,
								tna_integrated,
								profit_calculative,
								process_loss_method,
								item_category_id,
								inserted_by,
								insert_date,
								status_active
							) VALUES (
								'$id',
								'$cbo_company_name_wo',
								'$cbo_variable_list_wo',
								'$sales_year_started',
								'$cbo_sales_year_started_date',
								'$cbo_tna_integrated',
								'$cbo_profit_calculative',
								'0',
								'0',
								'".mysql_real_escape_string($user_name)."',
								'$date',
								'1'
							)";
			//print_r($_GET);
			//exit();
			mysql_query( $sql ) or die (mysql_error());	
			
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			
			echo "2_".$id;
			exit();
		}  else {
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			
			$sql = "UPDATE variable_settings_order_tracking
					SET
						company_name				= '$cbo_company_name_wo',
						variable_list				= '$cbo_variable_list_wo',
						sales_year_started_hcode	= '$sales_year_started',
						sales_year_started			= '$cbo_sales_year_started_date',
						tna_integrated				= '$cbo_tna_integrated',
						profit_calculative			= '$cbo_profit_calculative',
						updated_by					= '".mysql_real_escape_string($user_name)."',
						update_date 				= '$date',
						status_active				= '1'
						WHERE id					= '$isupdate'";
			mysql_query( $sql ) or die (mysql_error());
			
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			
			echo "3_".$id;		
			exit();
		}
	}
	
}

//--------------------------Variable_Settings_Order Tracking End---------------------------------------------------------------------

//--------------------------Variable_Settings_Production Start-----------------------------------------------------------------------

if ($action=="production_variable_settings"){ 
	
   if  ($isupdate==""){//Insert Here
   		
	
		$unique_field_name = "company_name";
		$table_name = "variable_settings_production";
		$query = "company_name='$cbo_company_name_production' and variable_list='$cbo_variable_list_production'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "variable_settings_production";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO variable_settings_production(
							id,
							company_name,
							variable_list,
							cutting_update_hcode,
							cutting_update,
							printing_emb_production_hcode,
							printing_emb_production,
							sewing_production_hcode,
							sewing_production,
							iron_update_hcode,
							iron_update,
							finishing_update_hcode,
							finishing_update,
							ex_factory,
							fabric_roll_level_hcode,
							fabric_roll_level,
							fabric_machine_level_hcode,
							fabric_machine_level,
							batch_maintained_hcode,
							batch_maintained,
							iron_input,
							inserted_by,
							insert_date,
							status_active
						) VALUES (
							'$id',
							'$cbo_company_name_production',
							'$cbo_variable_list_production',
							'$cutting_update',
							'$cbo_cutting_update',
							'$printing_emb_production',
							'$cbo_printing_emb_production',
							'$sewing_production',
							'$cbo_sewing_production',
							'$iron_update',
							'$cbo_iron_update',
							'$finishing_update',
							'$cbo_finishing_update',
							'$cbo_ex_factory',
							'$fabric_roll_level',
							'$cbo_fabric_roll_level',
							'$fabric_machine_level',
							'$cbo_fabric_machine_level',
							'$batch_maintained',
							'$cbo_batch_maintained',
							'$cbo_iron_input',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1'
						)";
		//print_r($_GET);
		//exit();
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_".$id;
		exit();
	}  
    else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_production
				SET
					company_name					= '$cbo_company_name_production',
					variable_list					= '$cbo_variable_list_production',
					cutting_update_hcode			= '$cutting_update',
					cutting_update					= '$cbo_cutting_update',
					printing_emb_production_hcode	= '$printing_emb_production',
					printing_emb_production			= '$cbo_printing_emb_production',
					sewing_production_hcode			= '$sewing_production',
					sewing_production				= '$cbo_sewing_production',
					iron_update_hcode				= '$iron_update',
					iron_update						= '$cbo_iron_update',
					finishing_update_hcode			= '$finishing_update',
					finishing_update				= '$cbo_finishing_update',
					ex_factory						= '$cbo_ex_factory',
					fabric_roll_level_hcode			= '$fabric_roll_level',
					fabric_roll_level				= '$cbo_fabric_roll_level',
					fabric_machine_level_hcode		= '$fabric_machine_level',
					fabric_machine_level			= '$cbo_fabric_machine_level',
					batch_maintained_hcode			= '$batch_maintained',
					batch_maintained				= '$cbo_batch_maintained',
					iron_input				= '$cbo_iron_input',
					updated_by				= '".mysql_real_escape_string($user_name)."',
					update_date 			= '$date',
					status_active			= '1'
					WHERE id						= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_".$isupdate;		
		exit();
	}
	
}

//--------------------------Variable_Settings_Production End---------------------------------------------------------------------

//--------------------------Variable_Settings_Production_Excess_Cutting_Slab Start-----------------------------------------------------------------------

if ($action=="production_variable_settings_excess_cutting_slab"){ 
	
   if  ($isupdate==""){//Insert Here
   
  
   		
		$a=mysql_query("delete from  variable_settings_production_excess_cutting_slab where company_name='$cbo_company_name_production' and variable_list='$cbo_variable_list_production'") or die (mysql_error());
		
		$unique_field_name = "company_name";
		$table_name = "variable_settings_production_excess_cutting_slab";
		$query = "company_name='$cbo_company_name_production' and variable_list='$cbo_variable_list_production'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		
		mysql_query("SET CHARACTER SET utf8");
		
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");
		$txt_slab_rang_start			= explode(",", $txt_slab_rang_start);
		$txt_slab_rang_end			= explode(",", $txt_slab_rang_end);
		$txt_excess_percent		= explode(",", $txt_excess_percent);
		$num=count($txt_slab_rang_start);
		
		 //exit();
		for ($i=0; $i<=$num; $i++)
		{
			//echo "ss$cbo_count_name[$i] ";
			if($txt_excess_percent[$i]!=0 && $txt_excess_percent[$i]!="")
			{
				$id_field_name = "id";
				$table_name = "variable_settings_production_excess_cutting_slab";
				$id= return_next_id($id_field_name,$table_name);
		
				$sql = "INSERT INTO variable_settings_production_excess_cutting_slab(
							id,
							company_name,
							variable_list,
							slab_rang_start,
							slab_rang_end,
							excess_percent,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted,
							is_locked
						) VALUES (
							'$id',
							'$cbo_company_name_production',
							'$cbo_variable_list_production',
							'".mysql_real_escape_string($txt_slab_rang_start[$i])."',
							'".mysql_real_escape_string($txt_slab_rang_end[$i])."',
							'$txt_excess_percent[$i]',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1',
							'0',
							'0'
						)";
						mysql_query( $sql ) or die (mysql_error());
						$sql_history=encrypt($sql, "logic_erp_2011_2012");
						$_SESSION['sql_history']=$sql_history;
						echo "2_".$id;
				}
		}
		
   }
}

//--------------------------Variable_Settings_Production_Excess_Cutting_Slab End---------------------------------------------------------------------

//--------------------------Variable_Settings_Inventory Start-----------------------------------------------------------------------

if ($action=="inventory_variable_settings"){ 
	
   if  ($isupdate==""){//Insert Here
   		
	
		$unique_field_name = "company_name";
		$table_name = "variable_settings_inventory";
		$query = "company_name='$cbo_company_name_inventory' and variable_list='$cbo_variable_list_inventory'";

		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		$id_field_name = "id";
		$table_name = "variable_settings_inventory";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");

		$sql = "INSERT INTO variable_settings_inventory(
							id,
							company_name,
							variable_list,
							hide_opening_stock_flag_hcode,
							hide_opening_stock_flag,
							item_rate_optional_hcode,
							item_rate_optional,
							item_qc_hcode,
							item_qc,
							item_category_id,
							user_given_code_status,
							inserted_by,
							insert_date,
							status_active
						) VALUES (
							'$id',
							'$cbo_company_name_inventory',
							'$cbo_variable_list_inventory',
							'$hide_opening_stock_flag',
							'$cbo_hide_opening_stock_flag',
							'$item_rate_optional',
							'$cbo_item_rate_optional',
							'$item_qc',
							'$cbo_item_qc',
							'0',
							'0',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1'
						)";
		//print_r($_GET);
		//exit();
		mysql_query( $sql ) or die (mysql_error());
		
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
			
		echo "2_".$id;
		exit();
		}  else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_inventory
				SET
					company_name					= '$cbo_company_name_inventory',
					variable_list					= '$cbo_variable_list_inventory',
					hide_opening_stock_flag_hcode	= '$hide_opening_stock_flag',
					hide_opening_stock_flag			= '$cbo_hide_opening_stock_flag',
					item_rate_optional_hcode		= '$item_rate_optional',
					item_rate_optional				= '$cbo_item_rate_optional',
					item_qc_hcode					= '$item_qc',
					item_qc							= '$cbo_item_qc',
					updated_by						= '".mysql_real_escape_string($user_name)."',
					update_date 					= '$date',
					status_active					= '1'
					WHERE id						= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "3_".$id;		
		exit();
	}
	
}

//--------------------------Variable_Settings_Inventory_user_given_code End---------------------------------------------------------------------


//--------------------------Variable_Settings_AccountsStart-----------------------------------------------------------------------

if ($action=="accounts_variable_settings"){ 
	
   if  ($isupdate==""){//Insert Here
   		
	
		$unique_field_name = "company_id";
		$table_name = "variable_settings_accounts";
		$query = "company_id='$cbo_company_name_account' and variable_list='$cbo_variable_list_accounts'";

		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		$id_field_name = "id";
		$table_name = "variable_settings_accounts";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");

		$sql = "INSERT INTO variable_settings_accounts(
							id,
							company_id,
							variable_list,
							integrated,
							inserted_by,
							insert_date,
							status_active
						) VALUES (
							'$id',
							'$cbo_company_name_account',
							'$cbo_variable_list_accounts',
							'$cbo_integreted', 
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1'
						)";
		//print_r($_GET);
		//exit();
		mysql_query( $sql ) or die (mysql_error());
		
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
			
		echo "2_".$id;
		exit();
		}  else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_accounts
				SET
					company_id						= '$cbo_company_name_account',
					variable_list					= '$cbo_variable_list_accounts',
					integrated						= '$cbo_integreted',
					 
					updated_by						= '".mysql_real_escape_string($user_name)."',
					update_date 					= '$date',
					status_active					= '1'
					WHERE id						= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "3_".$id;		
		exit();
	}
	
}

//--------------------------Variable_Settings_Inventory_user_given_code End---------------------------------------------------------------------


if ($action=="inventory_variable_settings_user_given_code"){ 
	
   if  ($isupdate==""){//Insert Here
   		
	
		$unique_field_name = "item_category_id";
		$table_name = "variable_settings_inventory";
		$query = "company_name='$cbo_company_name_inventory' and variable_list='$cbo_variable_list_inventory' and item_category_id='$cbo_item_category'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		$id_field_name = "id";
		$table_name = "variable_settings_inventory";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");

		$sql = "INSERT INTO variable_settings_inventory(
							id,
							company_name,
							variable_list,
							hide_opening_stock_flag_hcode,
							hide_opening_stock_flag,
							item_rate_optional_hcode,
							item_rate_optional,
							item_qc_hcode,
							item_qc,
							item_category_id,
							user_given_code_status,
							inserted_by,
							insert_date,
							status_active
						) VALUES (
							'$id',
							'$cbo_company_name_inventory',
							'$cbo_variable_list_inventory',
							'$hide_opening_stock_flag',
							'$cbo_hide_opening_stock_flag',
							'$item_rate_optional',
							'$cbo_item_rate_optional',
							'$item_qc',
							'$cbo_item_qc',
							'$cbo_item_category',
							'$cbo_item_status',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1'
						)";
		//print_r($_GET);
		//exit();
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;	
		echo "2_".$id;
		exit();
		}  else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_inventory
				SET
					company_name					= '$cbo_company_name_inventory',
					variable_list					= '$cbo_variable_list_inventory',
					hide_opening_stock_flag_hcode	= '$hide_opening_stock_flag',
					hide_opening_stock_flag			= '$cbo_hide_opening_stock_flag',
					item_rate_optional_hcode		= '$item_rate_optional',
					item_rate_optional				= '$cbo_item_rate_optional',
					item_qc_hcode					= '$item_qc',
					item_qc							= '$cbo_item_qc',
					item_category_id				= '$cbo_item_category',
					user_given_code_status			= '$cbo_item_status',
					updated_by						= '".mysql_real_escape_string($user_name)."',
					update_date 					= '$date',
					status_active					= '1'
					WHERE id						= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_".$id;		
		exit();
	}
	
}

//--------------------------Variable_Settings_Inventory_user_given_code End---------------------------------------------------------------------

//--------------------------Variable_Settings_Inventory_ILE_Cost_Standard_Start-----------------------------------------------------------------------

if ($action=="inventory_variable_settings_ile_cost_standard"){ 
	
   if  ($isupdate==""){//Insert Here
   
  
   		
		$a=mysql_query("delete from  variable_settings_inventory_ile_cost_standard where company_name='$cbo_company_name_inventory' and variable_list='$cbo_variable_list_inventory'") or die (mysql_error());
		
		$unique_field_name = "company_name";
		$table_name = "variable_settings_inventory_ile_cost_standard";
		$query = "company_name='$cbo_company_name_inventory' and variable_list='$cbo_variable_list_inventory'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");
		
		$cbo_category		= explode(",", $cbo_category);
		$cbo_item_group		= explode(",", $cbo_item_group);
		$cbo_source			= explode(",", $cbo_source);
		$txt_standard 		= explode(",", $txt_standard);
		
		$num=count($cbo_category);
		//echo $num;
		 //exit();
		for ($i=0; $i<=$num; $i++)
		{
			//echo "ss$cbo_count_name[$i] ";
			if($txt_standard[$i]!=0 && $txt_standard[$i]!="")
			{
				$id_field_name = "id";
				$table_name = "variable_settings_inventory_ile_cost_standard";
				$id= return_next_id($id_field_name,$table_name);
		
				$sql = "INSERT INTO variable_settings_inventory_ile_cost_standard(
							id,
							company_name,
							variable_list,
							category,
							item_group,
							source,
							standard,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted,
							is_locked
						) VALUES (
							'$id',
							'$cbo_company_name_inventory',
							'$cbo_variable_list_inventory',
							'$cbo_category[$i]',
							'$cbo_item_group[$i]',
							'$cbo_source[$i]',
							'$txt_standard[$i]',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1',
							'0',
							'0'
						)";
						mysql_query( $sql ) or die (mysql_error());
						$sql_history=encrypt($sql, "logic_erp_2011_2012");
						$_SESSION['sql_history']=$sql_history;
						echo "2_".$id;
				}
			}
		
   }
}

//--------------------------Variable_Settings_Inventory_ILE_Cost_Standard_End---------------------------------------------------------------------

//--------------------------Variable_Settings_HRM Start-----------------------------------------------------------------------


//month generated
if($type=="select_month_generate")
	{		
		
		//$sql = "SELECT * FROM lib_policy_year_periods WHERE status_active=1 and is_deleted = 0 and year_id=$id ORDER BY id ASC";
		$sql= "SELECT a.* FROM lib_policy_year_periods a, lib_policy_year b where b.id=a.year_id and a.year_id=$id ";// and b.type=1 and b.is_locked=0";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		echo "<option value=0>-- Select --</option>";
		while( $row = mysql_fetch_assoc( $result ) ) {
			$explode_val=explode(" ",$row["starting_date"]);
			$value = $explode_val[0];
			echo "<option value='".$row[actual_starting_date]."_".$row[actual_ending_date]."'>$value</option>";
		}		
		exit();
	}
	
	

if ($action=="hrm_variable_settings"){
	
	$variable_date = explode('_', $cbo_month_selector ); 
	$txt_from_date = $variable_date[0]; 
	$txt_to_date = $variable_date[1];  
	//echo $txt_from_date;
	 
   if($isupdate=="")//Insert Here
   {
		$unique_field_name = "company_name";
		$table_name = "variable_settings_hrm";
		$query = "company_name='$cbo_company_name_hrm'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "variable_settings_hrm";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO variable_settings_hrm(
							id,
							company_name,
							variable_date,
							emp_code_length,
							card_no_length,
							save_login_history,
							total_working_day,
							total_working_hour,
							currency_id,
							applicable_above_salary,
							bank_salary_percentage,
							adjust_in_time, 	
							adjust_out_time,
							maximum_in_time,
							first_ot_limit,
							one_hour_ot_unit,
							allow_ot_fraction,
							ot_start_minute,
							last_punch_time_with_ot,
							tiffin_start_time,
							dinner_start_time,
							not_pay_deduction,
							salary_type,
							new_emp_card_charge,
							late_deduction_from_ot,
							early_deduction_from_ot,
							show_gross_total_on_pay_slip,
							salary_sheet_format,
							salary_top_sheet_format,
							pay_slip_format,
							dinner_ot_treatment,
							dinner_ot_treatment_time,
							inserted_by,
							insert_date,
							updated_by,
							ot_rate_formula,
							ot_rate_multiplier,
							att_bonus_leave_type,
							in_out_time_format,
							reqn_based_ot,
							device_fixed,
							outtime_reqn,
							emp_budget,
							is_xcess_controlled,
							card_pattern,
							pattern_integret,
							status_active,
							ignor_questionable_ot,
							treat_as_resign,
							emp_abs_deduct,
							ot_manipulation,
							combined_late_early_out
						) VALUES (
							'$id',
							'$cbo_company_name_hrm',
							'$txt_from_date',
							'$txt_emp_code_length',
							'$txt_card_no_length',
							'$cbo_save_login_history',
							'$txt_total_working_day',
							'$txt_total_working_hour',
							'$cbo_currency_id',
							'$txt_salary_above',
							'$txt_payment_in_bank',
							'$txt_adjust_in_time',
							'$txt_adjust_out_time',
							'$txt_maximum_in_time',
							'$txt_complains_ot',
							'$txt_one_hour_ot_unit',
							'$cbo_allow_ot_fraction',
							'$txt_ot_start_minute',
							'$txt_last_punch_time_with_ot',
							'$txt_tiffin_start_time',
							'$txt_dinner_start_time',
							'$cbo_not_pay_deduction',
							'$cbo_salary_type',
							'$card_chr_new',
							'$cbo_late_deduction_from_ot',
							'$cbo_early_deduction_from_ot',
							'$cbo_show_gross_total_on_pay_slip',
							'$cbo_salary_sheet_format',
							'$cbo_salary_top_sheet_format',
							'$cbo_pay_slip_format',
							'$cbo_ot_dinner',
							'$txt_dinner_ot_min',
							'".mysql_real_escape_string($user_name)."',
							'$date_time',
							'$updated_by',
							'$cbo_overtime_rate_formula',
							'$txt_overtime_rate_value',
							'$cbo_att_bonus_leave_type',
							'$cbo_in_out_time_format',
							'$cbo_reqn_based_ot',
							'$cbo_device_fixed',
							'$cbo_outtime_reqn',
							'$emp_budget_control',
							'$cbo_access_control',
							'$cbo_card_pattern',
							'$pattern_integreted',
							'1',
							'$ignor_quest_ot',
							'$treat_as_resign',
							'$emp_abs_deduct',
							'$ot_manipulation',
							'$cbl_out'
						)";
		//print_r($_GET);round_salary_head change to not_pay_deduction,ot_calculation_type change to new_emp_card_charge,
		//exit();
		//treat_as_resign
		
		mysql_query( $sql ) or die (mysql_error());
			
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "2_".$id;
		
		
		exit();
	}  else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");
		
		$sql = "UPDATE variable_settings_hrm
				SET
					company_name		= '$cbo_company_name_hrm',
					emp_code_length		= '$txt_emp_code_length',
					card_no_length		= '$txt_card_no_length',
					save_login_history	= '$cbo_save_login_history',
					total_working_day	= '$txt_total_working_day',
					total_working_hour	= '$txt_total_working_hour',
					currency_id			= '$cbo_currency_id',
					applicable_above_salary			= '$txt_salary_above',
					bank_salary_percentage			= '$txt_payment_in_bank',
					adjust_in_time		= '$txt_adjust_in_time',
					adjust_out_time		= '$txt_adjust_out_time',
					maximum_in_time		= '$txt_maximum_in_time',
					first_ot_limit		= '$txt_complains_ot',
					one_hour_ot_unit	= '$txt_one_hour_ot_unit',
					allow_ot_fraction	= '$cbo_allow_ot_fraction',
					ot_start_minute		= '$txt_ot_start_minute',
					last_punch_time_with_ot		= '$txt_last_punch_time_with_ot',
					tiffin_start_time		= '$txt_tiffin_start_time',
					dinner_start_time		= '$txt_dinner_start_time',
					not_pay_deduction	= '$cbo_not_pay_deduction',
					salary_type			= '$cbo_salary_type',
					new_emp_card_charge 	= '$card_chr_new',
					late_deduction_from_ot		= '$cbo_late_deduction_from_ot',
					early_deduction_from_ot		= '$cbo_early_deduction_from_ot',
					show_gross_total_on_pay_slip	= '$cbo_show_gross_total_on_pay_slip',
					salary_sheet_format		= '$cbo_salary_sheet_format',
					salary_top_sheet_format = '$cbo_salary_top_sheet_format',
					pay_slip_format			= '$cbo_pay_slip_format',
					dinner_ot_treatment		= '$cbo_ot_dinner',
					dinner_ot_treatment_time= '$txt_dinner_ot_min',
					updated_by				= '".mysql_real_escape_string($user_name)."',
					update_date 			= '$date_time',
					status_active			= '1',
					ot_rate_formula='$cbo_overtime_rate_formula',
					ot_rate_multiplier='$txt_overtime_rate_value',
					att_bonus_leave_type='$cbo_att_bonus_leave_type',
					in_out_time_format='$cbo_in_out_time_format',
					reqn_based_ot='$cbo_reqn_based_ot',
					device_fixed='$cbo_device_fixed',
					outtime_reqn='$cbo_outtime_reqn',
					emp_budget='$emp_budget_control',
					is_xcess_controlled='$cbo_access_control',
					card_pattern='$cbo_card_pattern',
					pattern_integret='$pattern_integreted',
					ignor_questionable_ot='$ignor_quest_ot',
					treat_as_resign='$treat_as_resign',
					emp_abs_deduct='$emp_abs_deduct',
					ot_manipulation='$ot_manipulation',
					combined_late_early_out='$cbl_out'
					WHERE id= '$isupdate'";
		//echo $sql;die; 
		//round_salary_head change to not_pay_deduction,ot_calculation_type change to new_emp_card_charge,
		mysql_query( $sql ) or die (mysql_error());
		
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "3_".$id;		
		exit();
	}
	
}

//--------------------------Variable_Settings_HRM End---------------------------------------------------------------------
//--------------------------commercial_variable_settings_cost_heads End---------------------------------------------------------------------
if ($action=="commercial_variable_settings_cost_heads"){ 
	
   if  ($isupdate==""){//Insert Here
   		
		$unique_field_name = "company_name";
		$table_name = "variable_settings_commercial";
		$query = "company_name='$cbo_company_name_com' and variable_list='$cbo_variable_list_commercial' and cost_heads='$cost_heads'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "variable_settings_commercial";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO variable_settings_commercial(
							id,
							company_name,
							variable_list,
							capacity_in_value_hcode,
							capacity_in_value,
							currency_hcode,
							currency_id,
							max_btb_limit_hcode,
							max_btb_limit,
							max_pc_limit_hcode,
							max_pc_limit,
							cost_heads,
							cost_heads_status,
							inserted_by,
							insert_date,
							updated_by,
							status_active
						) VALUES (
							'$id',
							'$cbo_company_name_com',
							'$cbo_variable_list_commercial',
							'$capacity_in_value',
							'$txt_capacity_value',
							'$currency',
							'$cbo_currency_id',
							'$max_btb_limit',
							'$txt_max_btb_limit',
							'$max_pc_limit',
							'$txt_max_pc_limit',
							'".mysql_real_escape_string($cost_heads)."',
							'$cbo_cost_heads_status',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'1'
						)";
		//print_r($_GET);
		//exit();
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_".$id;
		exit();
	 } 
	 else 
	 {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_commercial
				SET
					company_name			= '$cbo_company_name_com',
					variable_list			= '$cbo_variable_list_commercial',
					capacity_in_value_hcode	= '$capacity_in_value',
					capacity_in_value		= '$txt_capacity_value',
					currency_hcode			= '$currency',
					currency_id				= '$cbo_currency_id',
					max_btb_limit_hcode		= '$max_btb_limit',
					max_btb_limit			= '$txt_max_btb_limit',
					max_pc_limit_hcode		= '$max_pc_limit',
					max_pc_limit			= '$txt_max_pc_limit',
					cost_heads				= '".mysql_real_escape_string($cost_heads)."',
					cost_heads_status		= '$cbo_cost_heads_status',
					updated_by				= '".mysql_real_escape_string($user_name)."',
					update_date 			= '$date',
					status_active			= '1'
					WHERE id				= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_".$id;		
		exit();
	
	}
	
}

//--------------------------commercial_variable_settings_cost_heads End---------------------------------------------------------------------


//--------------------------Variable_Settings_Production_Excess_Cutting_Slab Start-----------------------------------------------------------------------

if ($action=="report_variable_settings")
{
	//echo $mst_id."ggggggg";die;
	 
	if($mst_id=="")
	{
	   $id= return_next_id("id","variable_settings_report");
	   $insert_sql = "INSERT INTO variable_settings_report (id,company_id,location_id,module_id,report_list,inserted_by,insert_date,updated_by )VALUES ($id ,'$company','$location','$module','$signeture_list','".mysql_real_escape_string($user_name)."', '$date_time','$updated_by')" ;
		mysql_query( $insert_sql ) or die (mysql_error());
		
		$sql_history=encrypt($insert_sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "1_".$id;
		exit();
	}
	else 
	{
		$update_sql = "update variable_settings_report set company_id=$company,location_id='$location',module_id=$module , report_list='$signeture_list',updated_by= '".mysql_real_escape_string($user_name)."',update_date= '$date_time'  where id=$mst_id";
		mysql_query( $update_sql ) or die (mysql_error());		
		$sql_history=encrypt($update_sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_".$id;
	}
}

//--------------------------Variable_Settings_Production_Excess_Cutting_Slab End---------------------------------------------------------------------

//==========================id card======================id card====================================sohel===================================

if ($action=="idcard_variable_settings")
{ 

 if  ($update_id==""){//Insert Here
	echo "sohel";die;
		$unique_field_name = "company_id";
		$table_name = "variable_settings_idcard";
		$query = "company_id='$cbo_company_name' and card_pattern='$cbo_card_pattern'";

		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		$id_field_name = "id";
		$table_name = "variable_settings_accounts";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");

		$sql = "INSERT INTO variable_settings_accounts(
							id,
							company_id,
							variable_list,
							integrated,
							inserted_by,
							insert_date,
							status_active
						) VALUES (
							'$id',
							'$cbo_company_name_account',
							'$cbo_variable_list_accounts',
							'$cbo_integreted', 
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'1'
						)";
		//print_r($_GET);
		//exit();
		mysql_query( $sql ) or die (mysql_error());
		
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
			
		echo "2_".$id;
		exit();
		}  else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_accounts
				SET
					company_id						= '$cbo_company_name_account',
					variable_list					= '$cbo_variable_list_accounts',
					integrated						= '$cbo_integreted',					 
					updated_by						= '".mysql_real_escape_string($user_name)."',
					update_date 					= '$date',
					status_active					= '1'
					WHERE id						= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "3_".$id;		
		exit();
	}
}

//==========================id card======================id card====================================sohel===================================


//==========================piecerate_variable_settings======================start====================================sohel===================================
if ($action=="piecerate_variable_settings")
{ 
	
   if($isupdate=="")//Insert Here
   {
		$unique_field_name = "company_name";
		$table_name = "variable_settings_piece_rate";
		$query = "company_name='$cbo_company_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "variable_settings_piece_rate";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	

		$sql = "INSERT INTO variable_settings_piece_rate(
							id,
							company_name,
							piecerate_wages,
							applicable_salaryhead,
							offday_payment,
							leave_type,
							job_assign_day,
							job_assign_day_amount,
							percent_fixed,
							job_salary_head,
							over_base_salary, 	
							under_base_salary,
							subsidy_for,
							inserted_by, 
							insert_date, 
							updated_by
						) VALUES (
							'$id',
							'$cbo_company_name',
							'$cbo_piecerate_wages',
							'$applicable_salaryhead',
							'$offday_payment',
							'$leave_type',
							'$cbo_job_assign',
							'$txt_assign_day_number',
							'$cbo_percent_fixed',
							'$cbo_jobassignday_salary_head',
							'$cbo_over_base_salary',
							'$cbo_under_base_salary',
							'$cbo_subsidy',
							'$user_name',
							'$date_time',
							'$updated_by'
						)";
		//echo $sql;die;
		mysql_query( $sql ) or die (mysql_error());
			
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "2_".$id;
		exit();
	}  
	else 
	{
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		$sql = "UPDATE variable_settings_piece_rate
				SET
					company_name			= '$cbo_company_name',
					piecerate_wages			= '$cbo_piecerate_wages',
					applicable_salaryhead	= '$applicable_salaryhead',
					offday_payment			= '$offday_payment',
					leave_type				= '$leave_type',
					job_assign_day			= '$cbo_job_assign',
					job_assign_day_amount	= '$txt_assign_day_number',
					percent_fixed			= '$cbo_percent_fixed',
					job_salary_head			= '$cbo_jobassignday_salary_head',
					over_base_salary		= '$cbo_over_base_salary',
					under_base_salary		= '$cbo_under_base_salary',
					subsidy_for				= '$cbo_subsidy',
					updated_by				= '$user_name',
					update_date 			= '$date_time'
					WHERE id				= '$isupdate'";
		//echo $sql;die;
		
		mysql_query( $sql ) or die (mysql_error());
		
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		echo "3_".$isupdate;		
		exit();
	}
	
}

if($type=="get_report_signeture")
{

	$is_update  = return_field_value("id","variable_settings_report","company_id=$company_id and module_id=$module_id");
	if($is_update != "" )
	{
		$report_list  = return_field_value("report_list","variable_settings_report","id=$is_update ");
		echo "$('#signeture_list').val('".mysql_real_escape_string($report_list)."');\n";
		echo "$('#txt_mst_id_rtrn').val($is_update);\n";
	}
}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>
