<?php
include('../../includes/common.php');
extract( $_POST );
// formula
$formula_allowance=array(	0=>"-- Select --",
							1=>"((Basic/30)/WH)*OT Hour",
							2=>"((Basic/DOM)/WH)*OT Hour",
							3=>"((Gross/DOM)/WH)*OT Hour",
							4=>"((Gross/30)/WH)*OT Hour",
							5=>"((Basic/208)*2)*OT Hour",
							6=>"(Basic/208)*OT Hour",
							7=>"((Gross/208)*2)*OT Hour",
							8=>"(Gross/208)*OT Hour");

// formula_type
$formula_type=array(1=>"Fixed Amount",2=>"Percentage (%)",3=>"Formula");

// pay_with_salary
$pay_with_salary=array(0=>"-- Select --", 1=>"Yes", 2=>"No");

// day_type
$day_type=array(1=>"All Type",2=>"General Day",3=>"Off-day",4=>"Holiday");

// Company 
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}

// designation 
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_details[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}

// salary grade 
$sql = "SELECT * FROM lib_salary_grade WHERE is_deleted = 0 and status_active=1 ORDER BY grade_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$grade_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$grade_details[$row['id']] = mysql_real_escape_string($row['grade_name']);
}


//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


if( isset( $form ) && $form == "designation" ) {
	//designation_chart
	$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$designation_chart[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="designations">
	<thead>
		<tr>
			<th>Level</th>
			<th>System Designation</th>
			<th>Custom Designation</th>
			<th>Custom Designation (local)</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $designation_chart AS $designation ) { ?>
		<tr class="gradeA" id="<?php echo $designation['id']; ?>">
			<td width="7%" align="right"><?php echo $designation['level']; ?></td>
			<td width="28%"><a href="javascript:show_designation(<?php echo $designation['id']; ?>)"><?php echo $designation['system_designation']; ?></a></td>
			<td width="30%"><?php echo $designation['custom_designation']; ?></td>
			<td width="28%"><?php echo $designation['custom_designation_local']; ?></td>
			<td width="7%">
				<a id="status_<?php echo $designation['id']; ?>" href="javascript:update_status( 'active', '../../includes/common.php', 'designation_chart', 'lib_designation', <?php echo $designation['id']; ?>, <?php echo $designation['status_active']; ?> )">
					<span class="ui-icon <?php if( $designation['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../includes/common.php', 'designation_chart', 'lib_designation', <?php echo $designation['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "payroll_heads" ) {
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="payroll_heads">
	<thead>
		<tr>
			<th>System Head</th>
			<th>Custom_head</th>
			<th>Custom Head (Local)</th>
			<th>Applicable</th>
			<th>Type</th>
			<th>Salary Head</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($payroll_heads as $payroll) { ?>
		<tr class="gradeA" id="<?php echo $payroll['id']; ?>">
			<td width="18%"><a href="javascript:show_payroll_head(<?php echo $payroll['id']; ?>)"><?php echo $payroll['system_head']; ?></a></td>
			<td width="18%"><?php echo $payroll['custom_head']; ?></td>
			<td width="18%"><?php echo $payroll['custom_head_local']; ?></td>
			<td width="13%" align="center"><?php echo $payroll['is_applicable']; ?></td>
			<td width="13%" align="center">
				<?php
				if( $payroll['type'] == 0 ) echo "Earning";
				if( $payroll['type'] == 1 ) echo "Deduction";
				if( $payroll['type'] == 2 ) echo "Taxation";
				if( $payroll['type'] == 3 ) echo "LFA Earning";
				if( $payroll['type'] == 4 ) echo "Festival Earning";
				?>
			</td>
			<td width="13%" align="center"><?php if( $payroll['salary_head'] == 1 ) echo "Yes"; else echo "No"; ?></td>
			<td width="7%">
				<a id="status_<?php echo $payroll['id']; ?>" href="javascript:update_status( 'active', '../../includes/common.php', 'payroll_heads', 'lib_payroll_head', <?php echo $payroll['id']; ?>, <?php echo $payroll['status_active']; ?> )">
					<span class="ui-icon <?php if( $payroll['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../includes/common.php', 'payroll_heads', 'lib_payroll_head', <?php echo $payroll['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "shift_policy" ) {
	//shift_policy
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$shift_policy[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="shifts">
	<thead>
		<tr>
			<th>ID</th>
			<th>Shift Name</th>
            <th>Short Name</th>
			<th>Shift Type</th>
			<th>Shift start time</th>
			<th>Shift end time</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach( $shift_policy AS $shift ) { ?>
		<tr class="gradeA">
			<td width="30"><?php echo $shift['id']; ?></td>
			<td width="150"><a href="javascript:show_shift(<?php echo $shift['id']; ?>)"><?php echo $shift['shift_name']; ?></a></td>
            <td width="80"><?php echo $shift['short_shift_name']; ?></td>
			<td width="80"><?php if( $shift['shift_type'] == 0 ) echo "Fixed"; else echo "Roster"; ?></td>
			<td width="80" align="center"><?php echo $shift['shift_start']; ?></td>
			<td width="80" align="center"><?php echo $shift['shift_end']; ?></td>
			<td width="50">
				<a id="status_<?php echo $shift['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', 'shift_policy', 'lib_policy_shift', <?php echo $shift['id']; ?>, <?php echo $shift['status_active']; ?> )">
					<span class="ui-icon <?php if( $shift['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', 'shift_policy', 'lib_policy_shift', <?php echo $shift['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "salary_breakdown_policy" ) {
	
	$sql = "SELECT * FROM lib_policy_salary_breakdown WHERE status_active=1 and is_deleted = 0 ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_breakdown_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$salary_breakdown_policy[$row['id']] = $row;
		
		$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		$salary_breakdown_policy[$row['id']]['definition'] = array();
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_salary_breakdown_policy">
	<thead>
		<tr>
			<th>ID</th>
			<th>Policy Name</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach( $salary_breakdown_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td width="5%"><?php echo $policy['id']; ?></td>
			<td><a href="javascript:show_policy(<?php echo $policy['id']; ?>)"><?php echo $policy['policy_name']; ?></a></td>
			<td width="7%">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', 'salary_breakdown_policy', 'lib_policy_salary_breakdown', <?php echo $policy['id']; ?>, 'Library__HR & Admin__Policy Setup__Salary Breakdown Policy', <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', 'salary_breakdown_policy', 'lib_policy_salary_breakdown', <?php echo $policy['id']; ?>, 'Library__HR & Admin__Policy Setup__Salary Breakdown Policy' )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "leave_policy" ) {
	//leave_policy
	$sql = "SELECT * FROM lib_policy_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$leave_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$leave_policy[$row['id']] = $row;
		
		$sql2 = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $row[id] AND is_deleted = 0 ORDER BY policy_id, id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		$leave_policy[$row['id']]['definition'] = array();
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$leave_policy[$row['id']]['definition'][] = $row2;
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_leave_policy">
	<thead>
		<tr>
			<th width="5%">ID</th>
			<th>Policy Name</th>
			<th width="7%">Options</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach( $leave_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td width="5%"><?php echo $policy['id']; ?></td>
			<td><a href="javascript:show_policy(<?php echo $policy['id']; ?>)"><?php echo $policy['policy_name']; ?></a></td>
			<td width="7%">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', 'leave_policy', 'lib_policy_leave', <?php echo $policy['id']; ?>, <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', 'leave_policy', 'lib_policy_leave', <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "maternity_leave_policy" ) {
	//maternity_leave_policy
	$sql = "SELECT * FROM lib_policy_maternity_leave ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$maternity_leave_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$maternity_leave_policy[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$maternity_leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="maternity_leave_policy">
	<thead>
		<tr>
			<th>Policy Name</th>
			<th>Max Limit</th>
			<th>Minimum Service Length</th>
			<th>Allowed Number of Issue</th>
			<th width="7%">Options</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $maternity_leave_policy AS $mtl ) { ?>
		<tr class="gradeA">
			<td><a href="javascript:show_policy(<?php echo $mtl['id']; ?>)"><?php echo $mtl['policy_name']; ?></a></td>
			<td align="center"><?php echo $mtl['max_limit']; ?></td>
			<td align="center"><?php echo $mtl['min_service_length']; ?></td>
			<td align="center"><?php echo $mtl['allowed_issue_no']; ?></td>
			<td width="7%">
				<a id="status_<?php echo $mtl['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', 'maternity_leave_policy', 'lib_policy_maternity_leave', <?php echo $mtl['id']; ?>, <?php echo $mtl['status_active']; ?> )">
					<span class="ui-icon <?php if( $mtl['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', 'maternity_leave_policy', 'lib_policy_maternity_leave', <?php echo $mtl['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "holiday_incentive_policy" ) {
	//holiday_incentive_policy
	$sql = "SELECT * FROM lib_policy_holiday_incentive WHERE is_deleted = 0 ORDER BY id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday_incentive_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday_incentive_policy[$row['id']]				= $row;
		$holiday_incentive_policy[$row['id']]['breakdown']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_holiday_incentive_definition WHERE policy_id = $row[id] ORDER BY id";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$holiday_incentive_policy[$row['id']]['breakdown'][] =	array(
																		'parameter_type'		=> mysql_real_escape_string( $row2['parameter_type'] ),
																		'percentage_formula'	=> mysql_real_escape_string( $row2['percentage_formula'] ),
																		'base_head'				=> mysql_real_escape_string( $row2['base_head'] ),
																		'amount'				=> mysql_real_escape_string( $row2['amount'] )
																	);
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_holiday_incentive_policy">
	<thead>
		<tr>
			<th width="30%">Policy Name</th>
			<th>Minmum working hours</th>
			<th>Pay For</th>
			<th>Earning Head</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $holiday_incentive_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td width="30%"><a href="javascript:show_policy(<?php echo $policy['id']; ?>);"><?php echo $policy['policy_name']; ?></a></td>
			<td align="center"><?php echo $policy['min_working_hour']; ?></td>
			<td align="center"><?php if( $policy['pay_for'] == 0 ) echo "Hours worked"; else echo "The whole day"; ?></td>
			<td align="center"><?php echo $payroll_heads[$policy['earning_head']]['custom_head']; ?></td>
			<td width="7%">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', '', 'lib_policy_holiday_incentive', <?php echo $policy['id']; ?>, <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', '', 'lib_policy_holiday_incentive', <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "overtime_policy" ) {
	//overtime_policy
	$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$overtime_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$overtime_policy[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
		}
		$overtime_policy[$row['id']]['shift'] = '';
		$shifts = explode( ',', $overtime_policy[$row['id']]['shift_id'] );
		for( $i = 0; $i < count( $shifts ); $i++ ) {
			$sql2 = "SELECT shift_name FROM lib_policy_shift WHERE id = $shifts[$i]";
			$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
			$row2 = mysql_fetch_assoc( $result2 );
			$overtime_policy[$row['id']]['shift'] .= $row2['shift_name'] . '*';
		}
		$overtime_policy[$row['id']]['shift'] = substr( $overtime_policy[$row['id']]['shift'], 0, strlen( $overtime_policy[$row['id']]['shift'] ) - 1 );
		
		$overtime_policy[$row['id']]['slab'] = array();
		
		$sql2 = "SELECT * FROM lib_policy_overtime_slab WHERE policy_id = $row[id] ORDER BY id";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$overtime_policy[$row['id']]['slab'][] =	array(
															'overtime_slot'			=> mysql_real_escape_string( $row2['overtime_slot'] ),
															'overtime_multiplier'	=> mysql_real_escape_string( $row2['overtime_multiplier'] ),
															'overtime_head'			=> mysql_real_escape_string( $row2['overtime_head'] )
														);
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_overtime_policy">
	<thead>
		<tr>
			<th>Policy Name</th>
			<th>Shift</th>
			<th>OT rate per hour</th>
			<th width="20">Options</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $overtime_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td><a href="javascript:show_policy(<?php echo $policy['id']; ?>);"><?php echo $policy['policy_name']; ?></a></td>
			<td><?php echo $policy['shift']; ?></td>
			<td><?php echo $policy['overtime_rate']; ?></td>
			<td width="20">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', '', 'lib_policy_overtime', <?php echo $policy['id']; ?>, <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', '', 'lib_policy_overtime', <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "attendance_bonus_policy" ) {
	//attendance_bonus_policy
	$sql = "SELECT * FROM lib_policy_attendance_bonus ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$attendance_bonus_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$attendance_bonus_policy[$row['id']]				= $row;
		$attendance_bonus_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_attendance_bonus_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$attendance_bonus_policy[$row['id']]['criteria'][] =	array(
																			'total_leave_criteria'	=> $row2['total_leave_criteria'],
																			'cl_leave_criteria'	=> $row2['cl_leave_criteria'],
																			'sl_leave_criteria'	=> $row2['sl_leave_criteria'],
																			'lwp_leave_criteria'=> $row2['lwp_leave_criteria'],
																			'spl_leave_criteria'=> $row2['spl_leave_criteria'],
																			'el_leave_criteria'=> $row2['el_leave_criteria'],
																			'ml_leave_criteria'	=> $row2['ml_leave_criteria'],
																			'late_criteria'		=> $row2['late_criteria'],
																			'absent_criteria'	=> $row2['absent_criteria'],
																			'amount'			=> $row2['amount']
																);
		}
	}
//print_r($attendance_bonus_policy);die;	
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_attendance_bonus_policy">
	<thead>
    	<tr>
            <th width="25%">Policy Name</th>	
            <th>Earning Head</th>
             <th>Total Leave</th>
            <th>CL</th>
            <th>SL</th>
            <th>LWP</th>
            <th>SpL</th>
            <th>EL</th>
            <th>ML</th>
            <th>Late Criteria</th>
            <th>Absent Criteria</th>
            <th>Amount</th>
			<th>Options</th>
        </tr>
	</thead>
	<tbody>
		<?php foreach( $attendance_bonus_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td width="25%"><a href="javascript:show_policy(<?php echo $policy['id']; ?>);"><?php echo $policy['policy_name']; ?></a></td>
			<td><?php echo $payroll_heads[$policy['earning_head']]['custom_head']; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['total_leave_criteria'] . "<br />"; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['cl_leave_criteria'] . "<br />"; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['sl_leave_criteria'] . "<br />"; ?></td
            ><td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['lwp_leave_criteria'] . "<br />"; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['spl_leave_criteria'] . "<br />"; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['el_leave_criteria'] . "<br />"; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['ml_leave_criteria'] . "<br />"; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['late_criteria'] . "<br />"; ?></td>
            <td align="center"><?php foreach( $policy['criteria'] AS $criteria ) echo $criteria['absent_criteria'] . "<br />"; ?></td>
            <td align="right" style="padding-right:25px;"><?php foreach( $policy['criteria'] AS $criteria ) echo number_format( $criteria['amount'], 2, '.', '' ) . "<br />"; ?></td>
			<td width="20">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', '', 'lib_policy_attendance_bonus', <?php echo $policy['id']; ?>, <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', '', 'lib_policy_attendance_bonus', <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "absent_deduction_policy" ) {
	//absent_deduction_policy
	$sql = "SELECT * FROM lib_policy_absent_deduction ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$absent_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$absent_deduction_policy[$row['id']]				= $row;
		$absent_deduction_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_absent_deduction_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$absent_deduction_policy[$row['id']]['criteria'][] =	array(
																		'parameter_type'		=> $row2['parameter_type'],
																		'percentage_formula'	=> $row2['percentage_formula'],
																		'base_head'				=> $row2['base_head'],
																		'amount'				=> $row2['amount']
																	);
		}
	}
	
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_absent_deduction_policy">
	<thead>
        <tr>
            <th>Policy Name</th>
            <th>Deduction Head</th>
            <th>Parameter Type</th>
            <th>Percentage/Formula</th>
            <th>Base Head</th>
            <th>Amount</th>
			<th>Options</th>
        </tr>
	</thead>
	<tbody>
		<?php foreach( $absent_deduction_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td><a href="javascript:show_policy(<?php echo $policy['id']; ?>);"><?php echo $policy['policy_name']; ?></a></td>
			<td><?php echo $payroll_heads[$policy['deduction_head']]['custom_head']; ?></td>
			<td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['parameter_type'] == 0 ) echo "Percentage<br />";
                    elseif( $criteria['parameter_type'] == 1 ) echo "Formula<br />";
                    elseif( $criteria['parameter_type'] == 2 ) echo "Fixed<br />";
                }
                ?>
            </td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['percentage_formula'] == '' ) echo "-<br />";
                    else echo $criteria['percentage_formula'] . "<br />";
                }
                ?>
            </td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['base_head'] == 0 ) echo "-<br />";
                    else echo $payroll_heads[$criteria['base_head']]['custom_head'] . "<br />";
                }
				
                ?>
            </td>
            <td style="padding-right:25px;">
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                   // if( $criteria['amount'] == 0 ) echo "-<br />";
                   // else 
					echo number_format( $criteria['amount'], 2, '.', '' ) . "<br />";
                }
				
                ?>
            </td>
			<td width="20">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', '', 'lib_policy_absent_deduction', <?php echo $policy['id']; ?>, <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', '', 'lib_policy_absent_deduction', <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
else if( isset( $form ) && $form == "late_deduction_policy" ) {
	//late_deduction_policy
	$sql = "SELECT * FROM lib_policy_late_deduction ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$late_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$late_deduction_policy[$row['id']]				= $row;
		$late_deduction_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_late_deduction_criteria WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$late_deduction_policy[$row['id']]['criteria'][] =	array(
																		'late_slot'				=> $row2['late_slot'],
																		'parameter_type'		=> $row2['parameter_type'],
																		'percentage_formula'	=> $row2['percentage_formula'],
																		'base_head'				=> $row2['base_head'],
																		'amount'				=> $row2['amount']
																	);
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_late_deduction_policy">
	<thead>
        <tr>
            <th>Policy Name</th>
            <th>Deduct For</th>
            <th>Deduction Head</th>
            <th>Late Slot</th>
            <th>Parameter Type</th>
            <th>Percentage/Formula</th>
            <th>Base Head</th>
            <th>Amount</th>
			<th>Options</th>
        </tr>
	</thead>
	<tbody>
		<?php foreach( $late_deduction_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td><a href="javascript:show_policy(<?php echo $policy['id']; ?>);"><?php echo $policy['policy_name']; ?></a></td>
			<td><?php echo $policy['deduct_for']; ?></td>
			<td><?php echo $payroll_heads[$policy['deduction_head']]['custom_head']; ?></td>
			<td><?php foreach( $policy['criteria'] AS $criteria )  echo $criteria['late_slot'] . "<br />"; ?></td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['parameter_type'] == 0 ) echo "Percentage<br />";
                    elseif( $criteria['parameter_type'] == 1 ) echo "Formula<br />";
                    elseif( $criteria['parameter_type'] == 2 ) echo "Fixed<br />";
                }
                ?>
            </td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['percentage_formula'] == '' ) echo "-<br />";
                    else echo $criteria['percentage_formula'] . "<br />";
                }
                ?>
            </td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['base_head'] == 0 ) echo "-<br />";
                    else echo $payroll_heads[$criteria['base_head']]['custom_head'] . "<br />";
                }
                ?>
            </td>
            <td style="padding-right:25px;">
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    //if( $criteria['amount'] == 0 ) echo "-<br />";
                    //else 
					echo number_format( $criteria['amount'], 2, '.', '' ) . "<br />";
                }
                ?>
            </td>
			<td width="20">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', '', 'lib_policy_late_deduction', <?php echo $policy['id']; ?>, <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', '', 'lib_policy_late_deduction', <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }

// early_deduction_policy
else if( isset( $form ) && $form == "early_deduction_policy" ) {
	$sql = "SELECT * FROM lib_policy_early_deduction ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$early_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$early_deduction_policy[$row['id']]				= $row;
		$early_deduction_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_early_deduction_criteria WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$early_deduction_policy[$row['id']]['criteria'][] =	array(
																		'early_slot'			=> $row2['early_slot'],
																		'parameter_type'		=> $row2['parameter_type'],
																		'percentage_formula'	=> $row2['percentage_formula'],
																		'base_head'				=> $row2['base_head'],
																		'amount'				=> $row2['amount']
																	);
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_early_deduction_policy">
	<thead>
        <tr>
            <th>Policy Name</th>
            <th>Deduct For</th>
            <th>Deduction Head</th>
            <th>Early Slot</th>
            <th>Parameter Type</th>
            <th>Percentage/Formula</th>
            <th>Base Head</th>
            <th>Amount</th>
			<th>Options</th>
        </tr>
	</thead>
	<tbody>
		<?php foreach( $early_deduction_policy AS $policy ) { ?>
		<tr class="gradeA">
			<td><a href="javascript:show_policy(<?php echo $policy['id']; ?>);"><?php echo $policy['policy_name']; ?></a></td>
			<td><?php echo $policy['deduct_for']; ?></td>
			<td><?php echo $payroll_heads[$policy['deduction_head']]['custom_head']; ?></td>
			<td><?php foreach( $policy['criteria'] AS $criteria )  echo $criteria['late_slot'] . "<br />"; ?></td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['parameter_type'] == 0 ) echo "Percentage<br />";
                    elseif( $criteria['parameter_type'] == 1 ) echo "Formula<br />";
                    elseif( $criteria['parameter_type'] == 2 ) echo "Fixed<br />";
                }
                ?>
            </td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['percentage_formula'] == '' ) echo "-<br />";
                    else echo $criteria['percentage_formula'] . "<br />";
                }
                ?>
            </td>
            <td>
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['base_head'] == 0 ) echo "-<br />";
                    else echo $payroll_heads[$criteria['base_head']]['custom_head'] . "<br />";
                }
                ?>
            </td>
            <td style="padding-right:25px;" align="right">
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    //if( $criteria['amount'] == 0 ) echo "-<br />";
                    //else 
					echo number_format( $criteria['amount'], 2, '.', '' ) . "<br />";
                }
                ?>
            </td>
			<td width="20">
				<a id="status_<?php echo $policy['id']; ?>" href="javascript:update_status( 'active', '../../../includes/common.php', '', 'lib_policy_early_deduction', <?php echo $policy['id']; ?>, <?php echo $policy['status_active']; ?> )">
					<span class="ui-icon <?php if( $policy['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'delete', '../../../includes/common.php', '', 'lib_policy_early_deduction', <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }


else if( isset( $form ) && $form == "bonus_policy_form" ) {
	//late_deduction_policy
	$sql = "SELECT * FROM lib_bonus_policy ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$bonus_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$bonus_policy[$row['id']]				= $row;
		$bonus_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_bonus_policy_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$bonus_policy[$row['id']]['criteria'][] =	array(
																		'policy_id'				=> mysql_real_escape_string( $row2['policy_id'] ),
																		'slot'					=> mysql_real_escape_string( $row2['slot'] ),
																		'comfirmation'			=> mysql_real_escape_string( $row2['comfirmation'] ),
																		'percentage'			=> mysql_real_escape_string( $row2['percentage'] ),
																		'base_head'				=> mysql_real_escape_string( $row2['base_head'] ),
																		'amount_type'				=> mysql_real_escape_string( $row2['amount_type'] ),
																		'status_active'			=> mysql_real_escape_string( $row2['status_active'] )
																	);
		}
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_bonus_policy">
	<thead>
        <tr>
            <th>Policy Name</th>
            <th>Policy ID</th>
            <th>Slot</th>
            <th>Comfirmation</th>
            <th>Amount Type</th>
            <th>Percentage</th>
            <th>Base Head</th>
            <th>Status Active</th>
            <th>Delete</th>
        </tr>
	</thead>
	<tbody>
		<?php foreach( $bonus_policy AS $policy ) { ?>
		<tr class="gradeA" style="cursor:pointer">
			<td width="100"><a href="javascript:show_policy(<?php echo $policy['id']; ?>);"><?php echo $policy['policy_name']; ?></a></td>
			<td><?php echo $policy['id']; ?></td>
			<td><?php foreach( $policy['criteria'] AS $criteria )  echo $criteria['slot'] . "<br />"; ?></td>
            <td><?php foreach( $policy['criteria'] AS $criteria )  echo $criteria['comfirmation'] . "<br />"; ?></td>
            <td><?php foreach( $policy['criteria'] AS $criteria )  echo ( $criteria['amount_type'] == 1 ) ? "Percentage". "<br />" : "Fixed"  . "<br />"; ?></td>
            <td><?php foreach( $policy['criteria'] AS $criteria )  echo $criteria['percentage'] . "<br />"; ?></td>
            <td width="120"> 
                <?php
                foreach( $policy['criteria'] AS $criteria ) {
                    if( $criteria['base_head'] == 0 ) echo "-<br />";
                    else echo $payroll_heads[$criteria['base_head']]['custom_head'] . "<br />";
                }
                ?>
            </td>
            <td><?php echo $policy['status_active']; ?></td>
			<td width="20">				
				<a href="javascript:fn_bonus_policy_delete( <?php echo $policy['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php }
// Tiffin Bill Policy
else if( isset( $form ) && $form == "tiffin_bill_policy")
{
?>
    <table class="display" cellpadding="0" cellspacing="0" border="0" id="tbl_tiffin_bill_policy">
        <thead>
            <tr>
                <th>Sl</th>
                <th>policy_name</th>
                <th>late_applicable</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $i=1;
            $sql="SELECT  id,policy_name,late_applicable,earning_head FROM  lib_policy_tiffin_bill_mst WHERE status_active=1 and is_deleted=0 group by policy_name";
            $result=mysql_query($sql);
            while($row=mysql_fetch_assoc($result))
            {
                if($row['late_applicable']==1) $late_applicable="Yes";
                else if($row['late_applicable']==0) $late_applicable="No";
        ?>
            <tr class="gradeA" style="cursor:pointer" onclick=" js_set_value('<?php echo $row['id']; ?>','<?php echo $row['policy_name'] ; ?>','<?php echo $row['late_applicable'] ; ?>','<?php echo $row['earning_head'] ; ?>');">
                <td align="center"><?php echo $row['id'] ; ?>
                <td align="left"><?php echo $row['policy_name'] ; ?></td>
                <td align="center"><?php echo $late_applicable ; ?></td>
            </tr>
	<?php  } ?>
        </tbody>
    </table>
<?php
}
// Allowance Policy
else if( isset( $form ) && $form == "frm_allowance_policy")
{
?>
    <table class="display" cellpadding="0" cellspacing="0" border="0" id="tbl_allowance_policy_listview">
        <thead>
            <tr>
                <th>Sl</th>
                <th>policy_name</th>
                <th>late_applicable</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $i=1;
            $sql="SELECT * FROM lib_policy_allowance WHERE status_active=1 and	is_deleted=0 group by policy_name";
            $result=mysql_query($sql);
            while($row=mysql_fetch_assoc($result))
            {
                if($row['late_applicable']==1) $late_applicable="Yes";
                else if($row['late_applicable']==2) $late_applicable="No";
        ?>
            <tr class="gradeA" style="cursor:pointer" onclick=" js_set_value('<?php echo $row['policy_id']; ?>','<?php echo $row['policy_name'] ; ?>','<?php echo $row['late_applicable'] ; ?>');">
                <td align="center"><?php echo $row['policy_id'] ; ?>
                <td align="left"><?php echo $row['policy_name'] ; ?></td>
                <td align="center"><?php echo $late_applicable ; ?></td>
            </tr>
	<?php  } ?>
        </tbody>
    </table>
<?php
}


else if( isset( $form ) && $form == "transport_allowance_policy_from" ) {
	//transport_allowance_policy_from
	$sql = "SELECT * FROM lib_transport_policy  ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$transport_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$bonus_policy[$row['mst_id']]				= $row;
		$bonus_policy[$row['mst_id']]['criteria']	= array();
		
			/*$sql2 = "SELECT * FROM lib_transport_policy WHERE mst_id=$row[mst_id] ORDER BY id ASC";
			$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
			
			while( $row2 = mysql_fetch_assoc( $result2 ) ) {
				$transport_policy[$row['mst_id']]['criteria'][] =	array(
																			'policy_name'		=> mysql_real_escape_string( $row2['policy_name'] ),
																			'amount'			=> mysql_real_escape_string( $row2['amount'] ),
																			'mini_working_days'	=> mysql_real_escape_string( $row2['mini_working_days'] ),
																			'emp_status'		=> mysql_real_escape_string( $row2['emp_status'] ),
																			'amount_type'		=> mysql_real_escape_string( $row2['amount_type'] ),
																			'amount_value'		=> mysql_real_escape_string( $row2['amount_value'] ),
																			'status_active'		=> mysql_real_escape_string( $row2['status_active'] )
																		);
			}*/
	}
?>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl_transport_allowance_policy">
	<thead>
        <tr>
            <th>Policy Name</th>
            <th>Policy ID</th>
            <th>Amount</th>
           <!-- <th>Minimum Working Days</th>
            <th>Emp Status</th>
            <th>Amount Type</th>
            <th>Value</th>-->
            <th>Status Active</th>
            <th>Delete</th>
        </tr>
	</thead>
	<tbody>
		<?php foreach( $bonus_policy AS $policy ) { ?>
		<tr class="gradeA" style="cursor:pointer">
			<td width="200"><a href="javascript:show_policy('<?php echo $policy['mst_id']; ?>','<?php echo $policy['policy_name']; ?>','<?php echo $policy['amount']; ?>','<?php echo $policy['status_active']; ?>');"><?php echo $policy['policy_name']; ?></a></td>
			<td align="center"><?php echo $policy['mst_id']; ?></td>
			<td align="center"><?php echo $policy['amount']; ?></td>
            <!--<td><?php //print_r( $policy);//foreach( $policy['criteria'] AS $criteria )  echo $criteria['mini_working_days'] . "<br />"; ?></td>
            <td><?php //foreach( $policy['criteria'] AS $criteria )  echo ( $criteria['emp_status'] == 1 ) ? "New". "<br />" : "Regular"  . "<br />"; ?></td>
            <td><?php //foreach( $policy['criteria'] AS $criteria )  echo ($criteria['amount_type']  == 1 ) ? "Percent". "<br />" : "Fixed"  . "<br />"; ?></td>
             <td><?php //foreach( $policy['criteria'] AS $criteria )  echo $criteria['amount_value'] . "<br />"; ?></td>-->
            <td align="center"><?php if($policy['status_active']==1) echo "Active"; else echo "Inactive"; ?></td>
			<td width="50">				
				<a href="javascript:fn_transport_policy_delete( <?php echo $policy['mst_id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php 

}

//-------------------------------- ---------- ----------------------------------------------------//
//-------------------------------- END Data Populate ---------------------------------------------//
//-------------------------------- ---------- ----------------------------------------------------//



//-------------------------------- ---------- ----------------------------------------------------//
//-------------------------------- for update ----------------------------------------------------//
//-------------------------------- ---------- ----------------------------------------------------//

if( isset( $data ) && $data == 'salary_breakdown_policy' ) {
	//salary_breakdown_policy
	$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$salary_breakdown_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$salary_breakdown_policy[$row['id']] = $row;
		
		$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		$salary_breakdown_policy[$row['id']]['definition'] = array();
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
		}
	}
	$policy_data = $salary_breakdown_policy[$policy_id];
	print_r( serialize( $policy_data ) );
}
else if( isset( $data ) && $data == 'holiday_incentive_policy' ) {
	//holiday_incentive_policy
	$sql = "SELECT * FROM lib_policy_holiday_incentive WHERE is_deleted = 0 ORDER BY id";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$holiday_incentive_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$holiday_incentive_policy[$row['id']]				= $row;
		$holiday_incentive_policy[$row['id']]['breakdown']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_holiday_incentive_definition WHERE policy_id = $row[id] ORDER BY id";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$holiday_incentive_policy[$row['id']]['breakdown'][] =	array(
																		'parameter_type'		=> mysql_real_escape_string( $row2['parameter_type'] ),
																		'percentage_formula'	=> mysql_real_escape_string( $row2['percentage_formula'] ),
																		'base_head'				=> mysql_real_escape_string( $row2['base_head'] ),
																		'amount'				=> mysql_real_escape_string( $row2['amount'] )
																	);
		}
	}
	$policy_data = $holiday_incentive_policy[$policy_id];
	print_r( serialize( $policy_data ) );
}
else if( isset( $data ) && $data == 'overtime_policy' ) {
	//overtime_policy
	$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$overtime_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$overtime_policy[$row['id']] = array();
		foreach( $row AS $key => $value ) {
			$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
		}
		$overtime_policy[$row['id']]['shift'] = '';
		$shifts = explode( ',', $overtime_policy[$row['id']]['shift_id'] );
		for( $i = 0; $i < count( $shifts ); $i++ ) {
			$sql2 = "SELECT shift_name FROM lib_policy_shift WHERE id = $shifts[$i]";
			$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
			$row2 = mysql_fetch_assoc( $result2 );
			$overtime_policy[$row['id']]['shift'] .= $row2['shift_name'] . '*';
		}
		$overtime_policy[$row['id']]['shift'] = substr( $overtime_policy[$row['id']]['shift'], 0, strlen( $overtime_policy[$row['id']]['shift'] ) - 1 );
		
		$overtime_policy[$row['id']]['slab'] = array();
		
		$sql2 = "SELECT * FROM lib_policy_overtime_slab WHERE policy_id = $row[id] ORDER BY id";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$overtime_policy[$row['id']]['slab'][] =	array(
															'overtime_slot'			=> mysql_real_escape_string( $row2['overtime_slot'] ),
															'overtime_multiplier'	=> mysql_real_escape_string( $row2['overtime_multiplier'] ),
															'overtime_head'			=> mysql_real_escape_string( $row2['overtime_head'] )
														);
		}
	}
	$policy_data = $overtime_policy[$policy_id];
	print_r( serialize( $policy_data ) );
}
else if( isset( $data ) && $data == 'attendance_bonus_policy' ) {
	//attendance_bonus_policy
	$sql = "SELECT * FROM lib_policy_attendance_bonus ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$attendance_bonus_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$attendance_bonus_policy[$row['id']]				= $row;
		$attendance_bonus_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_attendance_bonus_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$attendance_bonus_policy[$row['id']]['criteria'][] =	array(
																	'total_leave_criteria'	=> $row2['total_leave_criteria'],
																	'cl_leave_criteria'	=> $row2['cl_leave_criteria'],
																	'sl_leave_criteria'	=> $row2['sl_leave_criteria'],
																	'lwp_leave_criteria'=> $row2['lwp_leave_criteria'],
																	'spl_leave_criteria'=> $row2['spl_leave_criteria'],
																	'el_leave_criteria'=> $row2['el_leave_criteria'],
																	'ml_leave_criteria'	=> $row2['ml_leave_criteria'],
																	'late_criteria'		=> $row2['late_criteria'],
																	'absent_criteria'	=> $row2['absent_criteria'],
																	'amount'			=> $row2['amount']
																);
		}
	}
	$policy_data = $attendance_bonus_policy[$policy_id];
	print_r( serialize( $policy_data ) );
}
else if( isset( $data ) && $data == 'absent_deduction_policy' ) {
	//absent_deduction_policy
	$sql = "SELECT * FROM lib_policy_absent_deduction ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$absent_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$absent_deduction_policy[$row['id']]				= $row;
		$absent_deduction_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_absent_deduction_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$absent_deduction_policy[$row['id']]['criteria'][] =	array(
																		'parameter_type'		=> mysql_real_escape_string( $row2['parameter_type'] ),
																		'percentage_formula'	=> mysql_real_escape_string( $row2['percentage_formula'] ),
																		'base_head'				=> mysql_real_escape_string( $row2['base_head'] ),
																		'amount'				=> mysql_real_escape_string( $row2['amount'] )
																	);
		}
	}
	$policy_data = $absent_deduction_policy[$policy_id];
	print_r( serialize( $policy_data ) );
}
else if( isset( $data ) && $data == 'late_deduction_policy' ) {
	//late_deduction_policy
	$sql = "SELECT * FROM lib_policy_late_deduction ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$late_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$late_deduction_policy[$row['id']]				= $row;
		$late_deduction_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_late_deduction_criteria WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$late_deduction_policy[$row['id']]['criteria'][] =	array(
																		'late_slot'				=> mysql_real_escape_string( $row2['late_slot'] ),
																		'parameter_type'		=> mysql_real_escape_string( $row2['parameter_type'] ),
																		'percentage_formula'	=> mysql_real_escape_string( $row2['percentage_formula'] ),
																		'base_head'				=> mysql_real_escape_string( $row2['base_head'] ),
																		'amount'				=> mysql_real_escape_string( $row2['amount'] )
																	);
		}
	}
	$policy_data = $late_deduction_policy[$policy_id];
	print_r( serialize( $policy_data ) );
}

//early_deduction_policy
else if( isset( $data ) && $data == 'early_deduction_policy' ) {
	$sql = "SELECT * FROM lib_policy_early_deduction ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$early_deduction_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$early_deduction_policy[$row['id']]				= $row;
		$early_deduction_policy[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_policy_early_deduction_criteria WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$early_deduction_policy[$row['id']]['criteria'][] =	array(
																		'early_slot'			=> mysql_real_escape_string( $row2['early_slot'] ),
																		'parameter_type'		=> mysql_real_escape_string( $row2['parameter_type'] ),
																		'percentage_formula'	=> mysql_real_escape_string( $row2['percentage_formula'] ),
																		'base_head'				=> mysql_real_escape_string( $row2['base_head'] ),
																		'amount'				=> mysql_real_escape_string( $row2['amount'] )
																	);
		}
	}
	$policy_data = $early_deduction_policy[$policy_id];
	print_r( serialize( $policy_data ) );
}


else if( isset( $data ) && $data == 'bonus_policy_form' ) {
	//bonus_policy
	$sql = "SELECT * FROM lib_bonus_policy ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$bonus_policy_arr = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$bonus_policy_arr[$row['id']]				= $row;
		$bonus_policy_arr[$row['id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_bonus_policy_definition WHERE policy_id = $row[id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$bonus_policy_arr[$row['id']]['criteria'][] =	array(
																		'policy_id'				=> mysql_real_escape_string( $row2['policy_id'] ),
																		'slot'					=> mysql_real_escape_string( $row2['slot'] ),
																		'comfirmation'			=> mysql_real_escape_string( $row2['comfirmation'] ),
																		'percentage'			=> mysql_real_escape_string( $row2['percentage'] ),
																		'base_head'				=> mysql_real_escape_string( $row2['base_head'] ),
																		'amount_type'			=> mysql_real_escape_string( $row2['amount_type'] ),
																		'status_active'			=> mysql_real_escape_string( $row2['status_active'] )
																	);
		}
	}
	$policy_data = $bonus_policy_arr[$policy_id];
	print_r( serialize( $policy_data ) );
}



else if( isset( $data ) && $data == 'transport_policy_data' ) {
	//bonus_policy
	/*$sql = "SELECT * FROM lib_transport_policy group by mst_id ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$bonus_policy_arr = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$bonus_policy_arr[$row['mst_id']]				= $row;
		$bonus_policy_arr[$row['mst_id']]['criteria']	= array();
		
		$sql2 = "SELECT * FROM lib_transport_policy WHERE mst_id= $row[mst_id] ORDER BY id ASC";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		
		while( $row2 = mysql_fetch_assoc( $result2 ) ) {
			$bonus_policy_arr[$row['mst_id']]['criteria'][] =	array(
																		'policy_name'		=> mysql_real_escape_string( $row2['policy_name'] ),
																		'amount'			=> mysql_real_escape_string( $row2['amount'] ),
																		'mini_working_days'	=> mysql_real_escape_string( $row2['mini_working_days'] ),
																		'emp_status'		=> mysql_real_escape_string( $row2['emp_status'] ),
																		'amount_type'		=> mysql_real_escape_string( $row2['amount_type'] ),
																		'amount_value'		=> mysql_real_escape_string( $row2['amount_value'] ),
																		'status_active'		=> mysql_real_escape_string( $row2['status_active'] )
																	);
		}
	}
	$policy_data = $bonus_policy_arr[$policy_id];
	print_r( serialize( $policy_data ) );*/
	
	$emp_type_arr_tr=array(1=>"New",2=>"Regular",3=>"Resign");
	$amount_type_arr_tr=array(1=>"Percent",2=>"Fixed");
	
	$sql = "SELECT * FROM lib_transport_policy WHERE mst_id= $policy_id and is_deleted=0 and status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$num_rows=mysql_num_rows($result);
	$i=1;
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if($ids_dtls=="") $ids_dtls=$row[id]; else $ids_dtls=$ids_dtls.",".$row[id];
      ?>
       <tr>
<td><input type="text" id="slot_<?php echo $i; ?>" name="slot[]" class="text_boxes" value="<?php echo $row[mini_working_days]; ?>" style="width:250px"  /></td>
    <td align="center">
        <select name="type[]" id="type_<?php echo $i; ?>" class="combo_boxes">
            <option value="">-- Select --</option>
            <?php foreach( $emp_type_arr_tr AS $key=>$value ) {  ?>
            <option value="<?php echo $key; ?>" <? if($key==$row['emp_status']) echo "selected"; ?>><?php echo $value; ?></option>
            <?php }  ?>
        </select>
    </td>
    <td>
        <select name="amounttype[]"  id="amounttype_<?php echo $i; ?>" class="combo_boxes">
            <?php foreach( $amount_type_arr_tr AS $key=>$value ) { ?>
            <option value="<?php echo $key; ?>" <? if($key==$row['amount_type']) echo "selected"; ?>><?php echo $value; ?></option>
            <?php  }  ?>
        </select>
    </td>
    <td>
    <input type="text" name="amount[]" id="amount_<?php echo $i; ?>" value="<?php echo $row[amount_value]; ?>" class="text_boxes numbers" placeholder=" % " onkeypress="return numbersonly(this,event)" />
    </td>
       </tr>	
	<?php
	$i++;	
	}
                
             
				
}


/*************************************
|	Tiffin bill policy
|	Developed by : Md. Nuruzzaman 
*************************************/
if( isset( $data ) && $data == 'tiffin_bill_policy' ) 
{
	$sql = "SELECT * FROM  lib_policy_tiffin_bill_dtls where policy_id='$policy_id' and is_deleted=0 and status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$num_rows=mysql_num_rows($result);
	$i=1;
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if($ids_dtls=="") $ids_dtls=$row[id]; else $ids_dtls=$ids_dtls.",".$row[id];
      ?>
       <tr>
           <td align="center"><? if($num_rows==$i){ ?><input type="hidden" id="db_row_ids" name="db_row_ids" class="text_boxes" value="<?php echo $ids_dtls; ?>" /><? } ?>
           <input type="hidden" id="update_id_<?php echo $i; ?>" name="update_id[]" class="text_boxes" value="<?php echo $row[id]; ?>" style="width:120px; text-align:center;" /></td>
           <td align="center"><input type="text" id="must_stay_<?php echo $i; ?>" name="must_stay[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" value="<?php echo $row[must_stay]; ?>" style="width:120px; text-align:center;" /></td>
            <td align="center"><input type="text" id="bill_amt__<?php echo $i; ?>" name="bill_amt[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" value="<?php echo $row[bill_amt]; ?>"  onblur="check_duplicacy(this.id,this.value)"  style="width:120px;text-align:right;" /></td>
       </tr>	
	<?php
	$i++;	
	}
}
// END of Tiffin Bill Policy 

/*************************************
|	allowance policy
|	Developed by : Md. Nuruzzaman 
*************************************/
if( isset( $data ) && $data == 'allowance_policy' ) 
{
	//payroll_heads
	$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
	$results = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$payroll_heads = array();
	array_unshift($payroll_heads,"-- Select --");
	$payroll_heads_salary = array();
	array_unshift($payroll_heads_salary,"-- Select --");
	while( $rows = mysql_fetch_assoc( $results ) ) 
	{
		if( $rows[salary_head]!=1) $payroll_heads[$rows['id']] = mysql_real_escape_string( $rows[custom_head]);
		if( $rows[salary_head]==1)$payroll_heads_salary[$rows['id']]= mysql_real_escape_string(  $rows[custom_head]);
	}
	$sql = "SELECT * FROM  lib_policy_allowance where policy_id='$policy_id' and is_deleted=0 and status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$num_rows=mysql_num_rows($result);
	$i=1;
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if($id_dtls=="") $id_dtls=$row[id]; else $id_dtls=$id_dtls.",".$row[id];
		?>
		<tr>
            <td align="center">
				<?
				if($num_rows==$i)
				{ 
				?>
                    <input type="hidden" id="db_row_ids" name="db_row_ids" class="text_boxes" value="<?php echo $id_dtls; ?>" />
				<? 
				} 
				?>
                    <input type="hidden" id="update_id_<?php echo $i; ?>" name="update_id[]" class="text_boxes" value="<?php echo $row[id]; ?>" style="width:120px; text-align:center;" />
            </td>
            <td align="center">
                <input type="text" name="must_stay[]" id="must_stay_<?php echo $i; ?>" value="<?php echo $row[must_stay]; ?>" class="text_boxes" onKeyPress="return numbersonly(this, event);" style=" width:120px; text-align:center;" />
            </td>
            <td align="center" id="search_formula_type_<?php echo $i; ?>">
                <select name="formula_type[]" id="formula_type_<?php echo $i; ?>" onchange="change_formula_type(<?php echo $i; ?>)" class="combo_boxes" style=" width:120px;">
					<?php  
					foreach( $formula_type AS $id=>$value )
					{ 
					?>
                        <option value="<?php echo $id; ?>" <? if($row['formula_type']==$id) echo "selected"; ?>><?php echo $value; ?></option>
                    <?php 
					} 
					?>
                </select>
            </td>
            <td align="center" id="search_value_<?php echo $i; ?>">
				<?php 
				if($row['formula_type']==1 || $row['formula_type']==2)
				{ 
				?>
                    <input type="text" name="value[]" id="value_<?php echo $i; ?>" onKeyPress="return numbersonly(this, event);" value="<?php echo $row[value]; ?>" class="text_boxes" style=" width:188px; text-align:center;" />
                <?php
				 } 
                if($row['formula_type']==3)
				{
				?>
                    <select name="value[]" id="value_<?php echo $i; ?>" class="combo_boxes" style=" width:200px;">
                    	<?php 
						foreach( $formula_allowance AS $id=>$formula ) 
						{
						?>
                            <option value="<?php echo $id; ?>" <? if($row['value']==$id) echo "selected"; ?>><?php echo $formula; ?></option>
                        <?php 
						} 
						?>
                    </select> 
				<?php 
                } 
                ?>
            </td>
            <td align="center">
				<?php 
				if($row['formula_type']==1 || $row['formula_type']==3 )
				{ 
				?> 
                    <select name="base_head[]" id="base_head_<?php echo $i; ?>" disabled="disabled" class="combo_boxes" style=" width:120px;">
                        <?php 
						foreach( $payroll_heads_salary AS $id=>$salary_head ) 
						{
						?>
                            <option value="<?php echo $id; ?>" <? if($row['base_head']==$id) echo "selected"; ?>><?php echo $salary_head; ?></option>
                        <?php 
						} 
						?>
                    </select>
                <?php 
				} 
				if($row['formula_type']==2 )
				{ 
				?>
                    <select name="base_head[]" id="base_head_<?php echo $i; ?>" class="combo_boxes" style=" width:120px;">
                        <?php 
						foreach( $payroll_heads_salary AS $id=>$salary_head ) 
						{
						?>
                            <option value="<?php echo $id; ?>" <? if($row['base_head']==$id) echo "selected"; ?>><?php echo $salary_head; ?></option>
                        <?php 
						} 
						?>
                    </select>
                <?php 
				} 
				?>
            </td>
            <td align="center">
                <select name="day_type[]" id="day_type_<?php echo $i; ?>" class="combo_boxes" style=" width:120px;">
					<?php 
					foreach( $day_type AS $id=>$day ) 
					{
					?>
                        <option value="<?php echo $id; ?>" <? if($row['day_type']==$id) echo "selected"; ?>><?php echo $day; ?></option>
                    <?php 
					} 
					?>
                </select>
            </td>
            <td align="center">
                <select name="pay_with_salary[]" id="pay_with_salary_<?php echo $i; ?>" class="combo_boxes" style=" width:120px;">
					<?php 
					foreach( $pay_with_salary AS $id=>$pay_salary ) 
					{
					?>
                        <option value="<?php echo $id; ?>" <? if($row['pay_with_salary']==$id) echo "selected"; ?>><?php echo $pay_salary; ?></option>
                    <?php 
					} 
					?>
                </select>
            </td>
            <td align="center">
                <select name="earning_head[]" id="earning_head_<?php echo $i; ?>" class="combo_boxes" style="width:120px;">
					<?php 
					foreach( $payroll_heads AS $id=>$payroll )
					{ 
					?>
                        <option value="<?php echo $id; ?>" <? if($row['earning_head']==$id) echo "selected"; ?>><?php echo $payroll; ?></option>
                    <?php 
					} 
					?>
                </select>
            </td>
		</tr>	
		<?php
		$i++;	
	}
}
// END of Tiffin Allowance Policy 
/*************************************
|	Training
|	Developed by : Md. Nuruzzaman 
*************************************/
if( isset( $data ) && $data == 'tiffin_bill_policy' ) 
{
	$sql = "SELECT * FROM  lib_policy_tiffin_bill_dtls where id_mst='$id_mst' and is_deleted=0 and status_active=1";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$num_rows=mysql_num_rows($result);
	$i=1;
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		if($ids_dtls=="") $ids_dtls=$row[id_dtls]; else $ids_dtls=$ids_dtls.",".$row[id_dtls];
      ?>
       <tr>
           <td align="center"><? if($num_rows==$i){ ?><input type="hidden" id="db_row_ids" name="db_row_ids" class="text_boxes" value="<?php echo $ids_dtls; ?>" /><? } ?>
           <input type="hidden" id="update_id_<?php echo $i; ?>" name="update_id[]" class="text_boxes" value="<?php echo $row[id_dtls]; ?>" style="width:120px; text-align:center;" /></td>
           <td align="center"><input type="text" id="must_stay_<?php echo $i; ?>" name="must_stay[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" value="<?php echo $row[must_stay]; ?>" style="width:120px; text-align:center;" /></td>
            <td align="center"><input type="text" id="bill_amt__<?php echo $i; ?>" name="bill_amt[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" value="<?php echo $row[bill_amt]; ?>"  onblur="check_duplicacy(this.id,this.value)"  style="width:120px;text-align:right;" /></td>
       </tr>	
	<?php
	$i++;	
	}
}

// Training
else if( isset( $form ) && $form == "frm_training")
{
?>
    <table class="display" cellpadding="0" cellspacing="0" border="0" id="tbl_tiffin_bill_policy">
        <thead><tr>
                <th>Sl</th>
                <th>System ID</th>
                <th>Training Name</th>
                <th>Short Name</th>
                <th>Status</th></tr>
        </thead>
        <tbody>
        <?php 
            $i=1;
            $sql="SELECT  id,training_name,short_name,status FROM  lib_training WHERE status_active=1 and	is_deleted=0";
            $result=mysql_query($sql);
            while($row=mysql_fetch_assoc($result))
            {
				if($row['status']==1) $status="Active";
				if($row['status']==2) $status="Inactive";?>
                <tr class="gradeA" style="cursor:pointer" onclick=" js_set_value('<?php echo $row['id']; ?>','<?php echo $row['training_name'] ; ?>','<?php echo $row['short_name'] ; ?>','<?php echo $row['status'] ; ?>');">
                    <td align="center"><?php echo $i ; ?>
                    <td align="center"><?php echo $row['id'] ; ?>
                    <td align="left"><?php echo $row['training_name'] ; ?>
                    <td align="left"><?php echo $row['short_name'] ; ?></td>
                    <td align="left"><?php echo $status; ?></td>
                </tr>
	<?php $i++; } ?>
        </tbody>
    </table>
<?php
}
// Training
else if( isset( $form ) && $form == "frm_skill_assign")
{
?>
    <table class="display" cellpadding="0" cellspacing="0" border="0" id="tbl_skill_assign">
        <thead><tr>
                <th>Sl</th>
                <th>Sequence</th>
                <th>Skill Name</th>
                <th>Status</th></tr>
        </thead>
        <tbody>
        <?php 
            $i=1;
            $sql="SELECT  id,sequence,skill_name,status FROM  lib_skill WHERE status_active=1 and is_deleted=0 order by sequence ASC";
            $result=mysql_query($sql);
            while($row=mysql_fetch_assoc($result))
            {
				if($row['status']==1) $status="Active";
				if($row['status']==2) $status="Inactive";?>
                <tr class="gradeA" style="cursor:pointer" onclick=" js_set_value('<?php echo $row['id']; ?>','<?php echo $row['skill_name'] ; ?>','<?php echo $row['status'] ; ?>','<?php echo $row['sequence'] ; ?>');">
                    <td align="center"><?php echo $i ; ?></td>
                    <td align="center"><?php echo $row['sequence'] ; ?></td>
                    <td align="left"><?php echo $row['skill_name'] ; ?></td>
                    <td align="left"><?php echo $status; ?></td>
                </tr>
			<?php 
			$i++; 
			} 
		?>
        </tbody>
    </table>
<?php
}

// Piece Rate OT Chart List View
else if( isset( $form ) && $form == "frm_piece_rate_ot_chart")
{
?>
    <table class="display" cellpadding="0" cellspacing="0" border="0" id="tbl_piece_rate_ot_chart">
        <thead><tr>
                <th>Sl</th>
                <th>Company Name</th>
                <th>Grade</th>
                <th>Designation</th>
                <th>Rate</th>
                <th>Working Minute</th>
                <th>Effective Date</th>
                </tr>
        </thead>
        <tbody>
        <?php 
            $sl=1;
            $sql="SELECT  * FROM  hrm_piece_rate_ot_mst WHERE status_active=1 and is_deleted=0 order by company_id ASC";
            $result=mysql_query($sql);
            while($row=mysql_fetch_assoc($result))
            {
		?>
                <tr class="gradeA" style="cursor:pointer" onclick=" js_set_value('<?php echo $row['id']; ?>','<?php echo $row['company_id'] ; ?>','<?php echo $row['grade_id'] ; ?>','<?php echo $row['designation_id'] ; ?>','<?php echo $row['rate'] ; ?>','<?php echo $row['working_minute'] ; ?>','<?php echo $row['eff_date'] ; ?>');">
                    <td align="center"><?php echo $sl ; ?></td>
                    <td align="left"><?php echo $company_details[$row['company_id']] ; ?></td>
                    <td align="center"><?php echo $grade_details[$row['grade_id']] ; ?></td>
                    <td align="center"><?php echo $designation_details[$row['designation_id']]; ?></td>
                    <td align="center"><?php echo $row['rate']; ?></td>
                    <td align="center"><?php echo $row['working_minute']; ?></td>
                    <td align="center"><?php echo convert_to_mysql_date($row['eff_date']); ?></td>
                </tr>
	<?php $sl++; } ?>
        </tbody>
    </table>
<?php
}


																	/****************************************
																	|	END Data Load For Update			|
																	*****************************************/
																	
																	


/*************************************
|	shift_time_distribution
|	Developed by : Md. Sohel 
*************************************/
if( isset( $data ) && $data == 'shift_time_distribution_load' ) 
{
	$value_status=array(0=>"--Select--",1=>"Increase",2=>"Decrease");
	//payroll_heads
	/*$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
	$results = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$payroll_heads = array();
	array_unshift($payroll_heads,"-- Select --");
	$payroll_heads_salary = array();
	array_unshift($payroll_heads_salary,"-- Select --");
	while( $rows = mysql_fetch_assoc( $results ) ) 
	{
		if( $rows[salary_head]!=1) $payroll_heads[$rows['id']] = mysql_real_escape_string( $rows[custom_head]);
		if( $rows[salary_head]==1)$payroll_heads_salary[$rows['id']]= mysql_real_escape_string(  $rows[custom_head]);
	}*/
	
		
	//shift_policy
	$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$shift_policy = array();
	array_unshift($shift_policy,"-- Select --");
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$shift_policy[$row['id']]= mysql_real_escape_string($row['shift_name']);
	}


	$sql_shift = "SELECT * FROM lib_shift_time_distribution where status_active=1 and is_deleted=0";
	$result = mysql_query( $sql_shift ) or die( $sql_shift . "<br />" . mysql_error() );
	//$result=mysql_db_query($DB, $sql_shift);
	$num_rows=mysql_num_rows($result);
	$i=1;
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		?>
		<tr>
            <td align="center">
              <input type="hidden" id="update_id_<?php echo $i; ?>" name="update_id[]" class="text_boxes" value="<?php echo $row[id]; ?>" style="width:120px; text-align:center;" />
            </td>
            <td align="center" id="search_formula_type_<?php echo $i; ?>">
                <select name="shift_id[]" id="shift_id_<?php echo $i; ?>"  class="combo_boxes" style=" width:180px;">
					<?php  
					foreach( $shift_policy AS $id=>$val )
					{ 
					?>
                        <option value="<?php echo $id; ?>" <? if($row['shift_id']==$id) echo "selected"; ?>><?php echo $val; ?></option>
                    <?php 
					} 
					?>
                </select>
            </td>
            <td align="center">
                    <input type="text" name="value_minute[]" id="value_minute_<?php echo $i; ?>" onKeyPress="return numbersonly(this, event);" value="<?php echo $row[minutes]; ?>" class="text_boxes" style=" width:100px; text-align:center;" />
              
            </td>
            <td align="center">
                    <select name="status_type[]" id="status_type_<?php echo $i; ?>"  class="combo_boxes" style=" width:120px;">
                       <?php 
					foreach( $value_status AS $id=>$val ) 
					{
					?>
                        <option value="<?php echo $id; ?>" <? if($row['type']==$id) echo "selected"; ?>><?php echo $val; ?></option>
                    <?php 
					} 
					?>
                    </select>
            </td>
		</tr>	
		<?php
		$i++;	
	}
}
?>