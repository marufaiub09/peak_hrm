//Do not change
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();






//-------------------------------appointment letter start
function fnc_appointment_letter_generate(){
	var company_name=document.getElementById('company_id').value;
	//var custom_designation=document.getElementById('report_template').value;
	var report_name=document.getElementById('form_report_name').value;
	var report_template=document.getElementById('report_template').value;
	//var txt_write=editor.getData();
	var letter_body_text=editor.getData();
	//var save_up=document.getElementById('save_up').value;
	//alert(letter_body_text);return;
	//var employee_code=document.getElementById('employee_code').value;
  $("#messagebox").removeClass().addClass('messagebox').text('').fadeIn(500);
	
	letter_body_text=letter_body_text.split('&').join('**'); // replace("&","**");
	letter_body_text=letter_body_text.split('?').join('****'); // replace("&","**");
	
	letter_body_text=letter_body_text.replace(/<div.*>/gi, "\n");
     
	 
	/* if(company_name==0) {
		  //alert("hhh");
	$('#messagebox').fadeTo( 200, 0.1, function() {
	$('#company_id').focus();
		$(this).html('Please Select A Company name ').addClass('messageboxerror').fadeTo(900,1);
		$(this).fadeOut(5000);
	});
  }
	else if(report_name==0) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#form_report_name').focus();
			$(this).html('Please Select A  Report Name').addClass('messageboxerror').fadeTo(900,1);
		$(this).fadeOut(5000);
		});
	  }
	  
	else if(report_template==0) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#report_template').focus();
			$(this).html('Please Select A  Report Template').addClass('messageboxerror').fadeTo(900,1);
		$(this).fadeOut(5000);
		});
	  }  
	 else{*/
	//txt_write = $(txt_write).find('div').remove().end().html();
	 
	var url = "includes/save_update_hr_admin.php";
	var parameters ="action=appointment_letter_generate&company_name="+company_name
			+"&report_name="+report_name
			+"&report_template="+report_template
			+"&letter_body_text="+letter_body_text ;
	
		//var parameters ="action=appointment_letter_generate&company_name="+company_name
//			+"&custom_designation="+custom_designation
//			+"&form_report="+form_report
//			+"&txt_write="+txt_write ;

	http.open("POST", url, true);
	 
	 http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	 http.setRequestHeader("Content-length", parameters .length);
	 http.setRequestHeader("Connection", "close");
	
	  
	http.onreadystatechange = function() {//Handler function for call back on state change.
		if(http.readyState == 4) {
		   
			var response = http.responseText.split('_');
			//alert(response);
			
			 if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
			
		else if (response[0]==4)
		
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
				$(this).fadeOut(5000);
			});
			
		}	
		}
	}
	http.send( parameters );
	  
	 
}
//}
//--------------------------------appointment letter end

//--------------------------------Report Template Start here
/*
function fnc_report_template()
{
	//alert("su..re"); return;
	if($('#report_format').val()=="")
	{
		$('#messagebox').fadeTo( 200, 0.1, function(){
			$('#report_format').focus();
			$(this).html('Enter report format name').addClass('messagebox_ok').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else
	{
		$('#messagebox').removeClass().addClass('messagebox').text('please wait....').fadeIn(1000);
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=letter_appointment_all_type&update_id=' + $('#update_id').val() + '&report_format=' + $('#report_format').val() + '&report_type=' + $('#report_type').val() );
		http.onreadystatechange =response_letter_appointment;
		http.send(null);
	}
}
function response_letter_appointment() 
{
	if(http.readyState == 4) 
	{
		var response = http.responseText;
		//alert(response);
		if( response == 3 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				document.getElementById('update_id').value="";
				showResult_type_all(response,'search_all_type_appointment','appointment_letter_type_list_view');
			});
		}
		else if( response == 4 ) 
		{
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				document.getElementById('update_id').value="";
				showResult_type_all(response,'search_all_type_appointment','appointment_letter_type_list_view');
			});
		}
	}
}
*/
//--------------------------------Report Template Start End

//------------------------------------Designation Chart
function fnc_designation_details(save_perm,edit_perm,delete_perm,approve_perm) {
	var level 						= $('#level').val();
	var system_designation 			= $('#system_designation').val();
	var custom_designation			= $('#custom_designation').val();
	var custom_designation_local 	= $('#custom_designation_local').val();
	var status_active 				= $('#status_active').val();
	var allowance_rate 				= $('#allowance_rate').val();
	var allowance_treatment 		= $('#allowance_treatment').val();
	var salary_grade 				= $('#salary_grade').val();
	var is_update 					= $('#is_update').val();	
	
	field_array[0] = new Array( 'level', level );
	field_array[1] = new Array( 'system_designation', system_designation );
	field_array[2] = new Array( 'custom_designation', custom_designation );
	field_array[3] = new Array( 'custom_designation_local', custom_designation_local );
	field_array[4] = new Array( 'status_active', status_active );
	field_array[5] = new Array( 'is_update', is_update );
	field_array[6] = new Array( 'allowance_rate', allowance_rate );
	field_array[7] = new Array( 'allowance_treatment', allowance_treatment );
	field_array[8] = new Array( 'salary_grade', salary_grade );
	
	
	$('#messagebox').removeClass().addClass('messageboxerror').text('Validating....').fadeIn(1000);
	if (is_update=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete/Inactive permission.').fadeIn(1000);		
	}
	else if( custom_designation == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#custom_designation').focus();
			$(this).html('Please Enter Custom Designation ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	/*else if( allowance_rate != ""  && $('#allowance_treatment').val()==0) {
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#allowance_treatment').focus();
				$(this).html('Please Select Allowance Treatment ').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);

			});
		}
		*/
	
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' +  field_array[i][1];
		}
			
		//alert(data);	
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=designation_details' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_designation_details;
		http.send(null);
	}
}

function response_designation_details() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response);
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','designation_details','update','../../');
				
				$(this).fadeOut(5000);
				populate_data();
				reset_form();
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','designation_details','save','../../');
				
				$(this).fadeOut(5000);
				populate_data();
				reset_form();
			});
		}
		
		designation_chart[response[1]] = new Array();
		designation_chart[response[1]]['id'] = response[1];
		for( var i = 0; i < field_array.length; i++ ) {
			designation_chart[response[1]][field_array[i][0]] = field_array[i][1];
		}
		
		
	}
}

//------------------------------------Payroll Heads
function fnc_payroll_heads(save_perm,edit_perm,delete_perm,approve_perm) {
	//alert("opp");
	var system_head 		= $('#system_head').val();
	var custom_head 		= $('#custom_head').val();
	var custom_head_local 	= $('#custom_head_local').val();
	var type 				= $('#type').val();
	var salary_head 		= $('#salary_head').val();
	var abbreviation		= $('#abbreviation').val();	
	var sequence_number		= $('#sequence_number').val();
	var chk_update			= $('#chk_update').val();//for check update
	
	if(document.getElementById('is_applicable').checked){
		is_applicable=1;
	}
	else{
		is_applicable=0;
	}
	
	if(document.getElementById('applicable_in_process').checked){
		applicable_in_process=1;
	}
	else{
		applicable_in_process=0;
	}
		
	if(document.getElementById('applicable_in_salary_sheet').checked){
		applicable_in_salary_sheet=1;
	}
	else{
		applicable_in_salary_sheet=0;
	}		
	
	if(document.getElementById('salary_head').checked){
		salary_head=1;
	}
	else{
		salary_head=0;
	}	
	field_array[0] = new Array( 'id', system_head );
	field_array[1] = new Array( 'custom_head', custom_head );
	field_array[2] = new Array( 'custom_head_local', custom_head_local );
	field_array[3] = new Array( 'type', type );
	field_array[4] = new Array( 'salary_head', salary_head );
	field_array[5] = new Array( 'abbreviation', abbreviation );
	field_array[6] = new Array( 'is_applicable', is_applicable );
	field_array[7] = new Array( 'applicable_in_process', applicable_in_process );
	field_array[8] = new Array( 'applicable_in_salary_sheet', applicable_in_salary_sheet );
	field_array[9] = new Array( 'sequence_number', sequence_number );
	
	$('#messagebox').removeClass().addClass('messageboxerror').text('Validating....').fadeIn(1000);
		
	if (chk_update=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (chk_update!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if( system_head == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#system_head').focus();
			$(this).html('Please Select System Head').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( custom_head == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#custom_head').focus();
			$(this).html('Please Enter Custom Head').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		//alert(data);
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=payroll_head' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_payroll_heads;
		http.send(null);
	}
}

function response_payroll_heads() {				//Payroll Heads Response of Hr & Admin Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');
		//alert(response);
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				save_activities_history('','library','payroll_head','update','../../');				
				$(this).fadeOut(5000);
				//populate_data();
			});
			$('#chk_update').val('');
		}
		
		payroll_heads[response[1]] = new Array();
		payroll_heads[response[1]]['id'] = response[1];
		for( var i = 1; i < field_array.length; i++ ) {
			payroll_heads[response[1]][field_array[i][0]] = field_array[i][1];
			//alert(payroll_heads[response[1]][field_array[i][0]]); return;
		}
		populate_data();
		reset_form('payroll_heads_form');
	}
}

//------------------------------------Salary Breakdown Policy
function fnc_salary_breakdown_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '', correct_head_counter = 0, counter = 0;
	
	var is_update=$('#is_update').val();//for check update
	if (is_update=='' && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}	
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}	
	$('#salary_breakdown tbody tr').each(function() {
		if( $(this).find('select[name="payroll_head[]"]').val() == 0 ) $(this).remove();
		else if( ( $(this).find('select[name="type[]"]').val() == '%' || $(this).find('select[name="type[]"]').val() == 'Formula' ) && $(this).find('input[name="percentage_formula[]"]').val() == '' ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).find('select[name="payroll_head[]"]').focus();
				$(this).html('Please fill out the percentage/formula value for this head.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $(this).find('select[name="type[]"]').val() == '%' && $(this).find('select[name="base_head[]"]').val() == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).find('select[name="base_head[]"]').focus();
				$(this).html('Please select a base head for this head.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else correct_head_counter++;
	});
	
	if( error == false && correct_head_counter == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$(this).html('Please provide at least one correct head.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		data += '&policy_id=' + $('#policy_id').val() + '&policy_name=' + escape($('#policy_name').val());
		if( $('#approved').is(':checked') ) data += '&status_active=1';
		else data += '&status_active=0';
		data += '&counter=' + correct_head_counter;
		
		
		
		$('#salary_breakdown tbody tr').each(function() {
			data += '&head' + counter + '=' + $(this).find('select[name="payroll_head[]"]').val() + '_' + $(this).find('select[name="type[]"]').val()
											+ '_' + $(this).find('input[name="percentage_formula[]"]').val() + '_' + $(this).find('select[name="base_head[]"]').val()
											+ '_' + $(this).find('input[name="amount[]"]').val();
			counter++;
		});
		
		nocache = Math.random();
		http.open( 'GET', '../includes/save_update_hr_admin.php?action=salary_breakdown_policy' + data + '&nocache=' + nocache + '&save_perm=' + save_perm + '&edit_perm=' + edit_perm + '&delete_perm=' + delete_perm + '&approve_perm=' + approve_perm );
		http.onreadystatechange = response_salary_breakdown_policy;
		http.send(null);
	}
}

function response_salary_breakdown_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');
		//alert(response);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Policy inserted successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','salary_breakdown_policy','save','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + $('#policy_name').val() + '</option>' );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Policy updated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','salary_breakdown_policy','update','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( $('#policy_name').val() );
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#policy_name').focus();
				$(this).html('Duplicate name. Please use another one.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 7 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html(response[1]).addClass('messageboxerror').fadeTo( 900, 1 );
			});
		}
		//var salary_breakdown_policy=new Array();
		if( response[0] == 1 || response[0] == 2 ) {
			populate_data();
			
			salary_breakdown_policy[response[1]] = new Array();
			salary_breakdown_policy[response[1]]['id'] = response[1];
			salary_breakdown_policy[response[1]]['policy_name'] = $('#policy_name').val();
			salary_breakdown_policy[response[1]]['status_active'] = $('#approved').val();
			salary_breakdown_policy[response[1]]['definition'] = new Array();
			
			var counter = 0;
			$('#salary_breakdown tbody tr').each(function() {
				salary_breakdown_policy[response[1]]['definition'][counter] = new Array();
				salary_breakdown_policy[response[1]]['definition'][counter]['payroll_head'] = $(this).find('select[name="payroll_head[]"]').val();
				salary_breakdown_policy[response[1]]['definition'][counter]['type'] = $(this).find('select[name="type[]"]').val();
				salary_breakdown_policy[response[1]]['definition'][counter]['percentage_formula'] = $(this).find('input[name="percentage_formula[]"]').val();
				salary_breakdown_policy[response[1]]['definition'][counter]['base_head'] = $(this).find('select[name="base_head[]"]').val();
				salary_breakdown_policy[response[1]]['definition'][counter]['amount'] = $(this).find('input[name="amount[]"]').val();
			});
		}
	}
}

//------------------------------------Leave Policy---------------
function fnc_leave_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	var fields = new Array( 'policy_id','policy_name','status_active','leave_type','limit_type','formula','max_limit','consecutive_leave_days','leave_calc_starts_from','leave_can_avail_after',
						   'leave_avail_criteria','off_day_leave_count','carrying_forward_allowed','max_acc_limit','fractional_leave','el_calulation_days' );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var  error = false;

	var is_update=$('#is_update').val();//for check update
	
	if (is_update=='' && save_perm==2)
	{
		 error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( $('#policy_name').val() == '' ) {
		 error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy Name is necessary.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#leave_type').val() == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			 error = true;
			$('#leave_type').focus();
			$(this).html('Please select a leave type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#limit_type').val() == 0 && $('#max_limit').val() == '' ) {
		 error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#max_limit').focus();
			$(this).html('Please define max limit for this leave type. Enter 0 for unlimited.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( parseInt( escape( $('#max_limit').val() ) ) < parseInt( escape( $('#max_availed').val() ) ) ) {
		 error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#max_limit').focus();
			$(this).html('Max Limit cannot be less than ' + $('#max_availed').val() + ' as leave type already availed.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#limit_type').val() == 1 && $('#formula').val() == 0 ) {
		 error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#formula').focus();
			$(this).html('Please select a formula.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#consecutive_leave_days').val() == '' ) {
		 error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#consecutive_leave_days').focus();
			$(this).html('Please define consecutive leave days. Enter 0 for maximum.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#carrying_forward_allowed').is(':checked') && $('#max_acc_limit').val() == '' ) {
		 error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#max_acc_limit').focus();
			$(this).html('Please define maximum accumulation limit. Enter 0 for maximum.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		
		for( var i = 0; i < fields.length; i++ ) {
			if( fields[i] == 'carrying_forward_allowed' ) {
				if( $('#carrying_forward_allowed').is(':checked') ) field_array[i] = new Array( fields[i], 1 );
				else field_array[i] = new Array( fields[i], 0 );
			}
			else field_array[i] 	= new Array( fields[i], escape( $('#' + fields[i]).val()));
		}
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		
		var deduction = '';
		$('#deduction tbody tr').each(function() {
			if( $(this).find('select[name="deduction_calculation[]"]').val() == 2 ) {
				if( $(this).find('input[name="deduction_fixed_amount[]"]').val() != '' ) deduction += '2_' + $(this).find('input[name="deduction_fixed_amount[]"]').val() + '|';
			}
			else {
				if( $(this).find('select[name="deduction_payroll_head[]"]').val() != 0 ) deduction += $(this).find('select[name="deduction_calculation[]"]').val() + '_'
																									+ $(this).find('select[name="deduction_payroll_head[]"]').val() + '|';
			}
		});
		if( deduction != '' ) deduction = deduction.substr( 0, deduction.length - 1 );
		data += '&deduction=' + deduction;
		field_array[field_array.length] = new Array( 'deduction', deduction );
		
		var earning = '';
		$('#earning tbody tr').each(function() {
			if( $(this).find('select[name="earning_calculation[]"]').val() == 2 ) {
				if( $(this).find('input[name="earning_fixed_amount[]"]').val() != '' ) earning += '2_' + $(this).find('input[name="earning_fixed_amount[]"]').val() + '|';
			}
			else {
				if( $(this).find('select[name="earning_payroll_head[]"]').val() != 0 ) earning += $(this).find('select[name="earning_calculation[]"]').val() + '_'
																								+ $(this).find('select[name="earning_payroll_head[]"]').val() + '|';
			}
		});
		earning = earning.substr( 0, earning.length - 1 );
		data += '&earning=' + earning;
		field_array[field_array.length] = new Array( 'earning', earning );
		
		http.open( 'GET', '../includes/save_update_hr_admin.php?action=leave_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_leave_policy;
		http.send(null);
	}
}

function response_leave_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#policy_name').focus();
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','leave_policy','save','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','leave_policy','update','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( field_array[1][1] );
		}
		
		
		/*if( response[0] == 2 ) leave_policy[response[1]] = new Array();
		leave_policy[response[1]]['id'] = response[1];
		leave_policy[response[1]]['policy_name'] = field_array[1][1];
		leave_policy[response[1]]['status_active'] = field_array[2][1];
		
		if( response[0] == 2 ) leave_policy[response[1]]['definition'] = new Array();
		leave_policy[response[1]]['definition'][field_array[2][1]] = new Array();
		for( var i = 3; i < field_array.length; i++ ) {
			leave_policy[response[1]]['definition'][field_array[2][1]][field_array[i][0]] = field_array[i][1];
		}*/
		//populate_data();
		location.reload();
	}
}

//------------------------------------Maternity Leave Policy
function fnc_maternity_leave_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	
	
	
	var fields = new Array( 'policy_id','policy_name','max_limit','min_service_length','allowed_issue_no','gap_between_issues','leave_before_delivery','leave_after_delivery','payment_calculation','payment_disbursement','status_active' );
						  
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	var is_update=$('#is_update').val();//for check update
	if (is_update=='' && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}	
	else if( $('#policy_name').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Please Enter Policy Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#max_limit').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#max_limit').focus();
			$(this).html('Please Enter Max Limit.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#allowed_issue_no').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#allowed_issue_no').focus();
			$(this).html('Please Enter Allowed No of Issue.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		
		for( var i = 0; i < fields.length; i++ ) {
			field_array[i] = new Array( fields[i], escape($('#' + fields[i]).val() ));
		}
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		http.open( 'GET', '../includes/save_update_hr_admin.php?action=maternity_leave_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_maternity_leave_policy;
		http.send(null);
	}
}

function response_maternity_leave_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','maternity_leave_policy','save','../../../');
					$(this).fadeOut(5000);
				
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','maternity_leave_policy','update','../../../');
					$(this).fadeOut(5000);
				
			});
			$('#policy_id option[value="' + response[1] + '"]').text( field_array[1][1] );
		}
		
		maternity_leave_policy[response[1]] = new Array();
		maternity_leave_policy[response[1]]['id'] = response[1];
		for( var i = 1; i < field_array.length; i++ ) {
			maternity_leave_policy[response[1]][field_array[i][0]] = field_array[i][1];
		}
		//populate_data();
		//reset_form();
		location.reload();
	}
}




//-------------------------------Bonus policy here---------------------------------//
function fnc_bonus_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '', correct_head_counter = 0, counter = 0;
	
	var is_update=$('#is_update').val();//for check update
	if (is_update=='' && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}	
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
	$('#bonus_policy_form tbody tr').each(function() {
		if( $(this).find('input[name="amount[]"]').val() != '' && $(this).find('select[name="base_head[]"]').val() == 0 &&  $(this).find('select[name="amounttype[]"]').val()==1) {
			 
				error = true;
				
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).find('select[name="base_head[]"]').focus();
					$(this).html('Please Select At least One Base head.').addClass('messageboxerror').fadeTo(900,1);
				});
			 
		}
		else if( $(this).find('input[name="amount[]"]').val() == '' && $(this).find('select[name="base_head[]"]').val() != 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).find('input[name="amount[]"]').focus();
				$(this).html('Please Enter Amount (Percentage).').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $(this).find('input[name="amount[]"]').val() == '' && $(this).find('select[name="base_head[]"]').val() == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).find('input[name="amount[]"]').focus();
				$(this).html('Please Enter Amount (Percentage).').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else correct_head_counter++;
		
	});
	}
	
	if( error == false ) {
		data += '&is_update=' + $('#is_update').val() + '&policy_name=' + escape($('#policy_name').val());
		if( $('#approved').is(':checked') ) data += '&status_active=1';
		else data += '&status_active=0';
		data += '&counter=' + correct_head_counter;
		
		$('#bonus_policy_form tbody tr').each(function() {
			data += '&head' + counter + '=' + $(this).find('input[name="slot[]"]').val() + '_' + $(this).find('select[name="type[]"]').val()
											+ '_' + $(this).find('input[name="amount[]"]').val() + '_' + $(this).find('select[name="base_head[]"]').val()+'_'+ $(this).find('select[name="amounttype[]"]').val();											
			counter++;
		});
		//alert(data);
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=bonus_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_bonus_policy;
		http.send(null);
	}
}

function response_fnc_bonus_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Policy inserted successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','bonus_policy','save','../../../');
				$(this).fadeOut(5000);
			});
			reset_form();
			populate_data();			
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Policy updated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','bonus_policy','update','../../../');
				$(this).fadeOut(5000);
			});
			reset_form();
			populate_data();			
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#policy_name').focus();
				$(this).html('Duplicate name. Please use another one.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}			
	}
}
// taining name
function func_training(save_perm,edit_perm,delete_perm,approve_perm) {
	//alert("ok......");
	var training_name	= $('#txt_training_name').val();
	var short_name		= $('#txt_short_name').val();
	var status			= $('#cbo_status').val();
	var is_update		= $('#is_update').val();
	//alert(training_name+", "+short_name+", "+status+", "+is_update);
	
	//$('#messagebox').removeClass().addClass('messageboxerror').text('Validating....').fadeIn(1000);
	if (is_update=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
/*	
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete/Inactive permission.').fadeIn(1000);		
	}
*/	
	else if( training_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_training_name').focus();
			$(this).html('Please Enter Training Name ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if( short_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_short_name').focus();
			$(this).html('Please Enter Short Name ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else {
		//nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=training&training_name='+ training_name +'&short_name='+ short_name +'&status='+ status+'&is_update='+ is_update);
		http.onreadystatechange = response_training;
		http.send(null);
	}
}
function response_training() 
{
	if(http.readyState == 4) 
	{
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted successfully.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				populate_data();
			});
		}
		else if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','designation_details','save','../../');
				$(this).fadeOut(5000);
				populate_data();
				//reset_form();
			});
		}
	}
}

// Skill Assign
function fnc_skill_assign(save_perm,edit_perm,delete_perm,approve_perm) {
	//alert("sure...");
	var skill_name	= $('#skill_name').val();
	var sequence	= $('#sequence').val();
	var status		= $('#cbo_status').val();
	var is_update	= $('#is_update').val();
	//alert(skill_name+", "+status+", "+is_update); return;
	
	//$('#messagebox').removeClass().addClass('messageboxerror').text('Validating....').fadeIn(1000);
	if (is_update=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
/*	
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete/Inactive permission.').fadeIn(1000);		
	}
*/	
	else if( skill_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#skill_name').focus();
			$(this).html('Please Enter Skill Name ').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else {
		//nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=skill_assign&skill_name='+ skill_name+'&sequence='+ sequence +'&status='+ status+'&is_update='+ is_update);
		http.onreadystatechange = response_skill_assign;
		http.send(null);
	}
}
function response_skill_assign() 
{
	if(http.readyState == 4) 
	{
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted successfully.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				populate_data();
			});
		}
		else if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','designation_details','save','../../');
				$(this).fadeOut(5000);
				populate_data();
				//reset_form();
			});
		}
		else if( response == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Sequence Number.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','designation_details','save','../../');
				$(this).fadeOut(5000);
				populate_data();
				//reset_form();
			});
		}
	}
}

function fun_piece_rate_ot_chart(save_perm,edit_perm,delete_perm,approve_perm)
{
	//alert("su..re");
	var error = false; 
	var data = '';
	
	if( $('#cbo_company_id').val() ==0 ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_company_id').focus();
			$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	else if( $('#cbo_grade_id').val() ==0 ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#cbo_grade_id').focus();
			$(this).html('Please select Grade.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}

	else if( $('#txt_rate').val() =='' ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_rate').focus();
			$(this).html('Please enter Rate.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	else if( $('#txt_working_mnt').val() =='' ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_working_mnt').focus();
			$(this).html('Please enter Working Minute.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	else if( $('#txt_eff_date').val() =='' ) 
	{
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#txt_eff_date').focus();
			$(this).html('Please enter Effective Date.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	
	else
	{
		//alert("su..re");
		//alert($('#cbo_designation_id').val()); return;
		//alert($('#cbo_company_id').val()+"="+$('#cbo_grade_id').val()+"="+$('#cbo_designation_id').val()+"="+$('#txt_rate').val()+"="+$('#txt_eff_date').val());
		data = 	'&update_id			=' + $('#txt_update_id').val()+
				'&company_id		=' + $('#cbo_company_id').val()+ 
				'&grade_id			=' + $('#cbo_grade_id').val()+ 
				'&designation_id	=' + $('#cbo_designation_id').val()+ 
				'&rate				=' + $('#txt_rate').val()+ 
				'&working_minute	=' + $('#txt_working_mnt').val()+
				'&eff_date			=' + $('#txt_eff_date').val();
				/*'&location_id		=' + $('#cbo_location_id').val()+
				'&division_id		=' + $('#cbo_division_id').val()+ 
				'&department_id		=' + $('#cbo_department_id').val()+ 
				'&section_id		=' + $('#cbo_section_id').val()+ 
				'&subsection_id		=' + $('#cbo_subsection_id').val()+
				'&id_card			=' + $('#txt_id_card').val()+
				'&emp_code			=' + $('#txt_emp_code').val()*/
				
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=save_piece_rate_ot_chart' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_save_piece_rate_ot_chart;
		http.send(null); 
	}
}
function response_save_piece_rate_ot_chart() 
{
	//alert("su..re");
	if(http.readyState == 4) 
	{
		var response = http.responseText;	
		//alert(response);
		if( response == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted successfully.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000);
				populate_data();
			});
		}
		else if( response == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','designation_details','save','../../');
				$(this).fadeOut(5000);
				populate_data();
				//reset_form();
			});
		}
		else if( response == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Already Exist.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','designation_details','save','../../');
				$(this).fadeOut(10000);
				//populate_data();
				//reset_form();
			});
		}
	}
}



//===================Start===============transport_allowance_policy====================================================================


function fnc_transport_allowance_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '', correct_head_counter = 0, counter = 0;
	
	var is_update=$('#is_update').val();//for check update
	if (is_update=='' && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}	
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#policy_amount').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_amount').focus();
			$(this).html('Policy Amount can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else{	
	$('#transport_allowance_policy_form tbody tr').each(function() {
		
		if( $(this).find('input[name="slot[]"]').val() == '' ) {
				error = true;
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$(this).find('input[name="slot[]"]').focus();
					$(this).html('Please Select At least One Policy.').addClass('messageboxerror').fadeTo(900,1);
				});
		}
		else if( $(this).find('select[name="type[]"]').val() == "") {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).find('select[name="type[]"]').focus();
				$(this).html('Please Enter Emp Status.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $(this).find('input[name="amount[]"]').val() == '' ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$(this).find('input[name="amount[]"]').focus();
				$(this).html('Please Enter Amount (Percentage or Fixed).').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else
		
		 correct_head_counter++;
	});
	}
	
	if( error == false ) {
		data += '&is_update=' + $('#is_update').val() + '&policy_name=' + escape($('#policy_name').val()) + '&policy_amount=' + escape($('#policy_amount').val()) + '&cbo_status=' + escape($('#cbo_status').val());
		data += '&counter=' + correct_head_counter;
		$('#transport_allowance_policy_form tbody tr').each(function() {
			data += '&head' + counter + '=' + $(this).find('input[name="slot[]"]').val() + '_' + $(this).find('select[name="type[]"]').val()+ '_' + $(this).find('select[name="amounttype[]"]').val()+ '_' + $(this).find('input[name="amount[]"]').val();											
			counter++;
		});
		//alert(data);
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=transport_allowance_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_fnc_transport_allowance_policy;
		http.send(null);
	}
}

function response_fnc_transport_allowance_policy() {
	if(http.readyState == 4) {
		//alert (http.responseText);
		var response = http.responseText.split('_');
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Policy inserted successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','bonus_policy','save','../../../');
				$(this).fadeOut(5000);
			});
			reset_form();
			populate_data();			
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Policy updated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','library','bonus_policy','update','../../../');
				$(this).fadeOut(5000);
			});
			reset_form();
			populate_data();			
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$('#policy_name').focus();
				$(this).html('Duplicate name. Please use another one.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}			
	}
}

//==============================================================================================================
