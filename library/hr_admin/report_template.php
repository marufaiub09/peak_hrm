<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <!--
        <script type="text/javascript" src="includes/ajax_submit_hr_admin.js"></script>
        <script src="includes/functions.js"></script>
        -->
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script type="text/javascript" language="javascript">
			$(document).ready(function() {
             	//alert("su..re");
				listview_template_report();  
            });
			
			//ajax submit starts here
			function createObject() {
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			
			var http = createObject();
			var field_array = new Array();
			
			// listview_template_report
			function listview_template_report()
			{
				//alert("su..re");
				http.open("GET","includes/list_view_appointment_letter.php?type=search_all_type_appointment");
				http.onreadystatechange =response_list_view_appointment_letter;
				http.send();
			}
			
			function response_list_view_appointment_letter()
			{
				if(http.readyState == 4) 
				{
					document.getElementById('div_template_report').innerHTML= http.responseText;
				}
			}
			
			// fnc_report_template
			function fnc_report_template()
			{
				//alert("su..re"); return;
				if($('#report_format').val()=="")
				{
					$('#messagebox').fadeTo( 200, 0.1, function(){
						$('#report_format').focus();
						$(this).html('Enter report format name').addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				else
				{
					$('#messagebox').removeClass().addClass('messagebox').text('please wait....').fadeIn(1000);
					http.open( 'GET', 'includes/save_update_hr_admin.php?action=letter_appointment_all_type&update_id=' + $('#update_id').val() + '&report_format=' + $('#report_format').val() + '&report_type=' + $('#report_type').val() );
					http.onreadystatechange =response_letter_appointment;
					http.send(null);
				}
			}
			function response_letter_appointment() 
			{
				if(http.readyState == 4) 
				{
					var response = http.responseText;
					//alert(response);
					if( response == 3 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
							document.getElementById('update_id').value="";
							listview_template_report();
						});
					}
					else if( response == 4 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
							$(this).fadeOut(5000);
							document.getElementById('update_id').value="";
							listview_template_report();
						});
					}
				}
			}
			
			function js_set_data(rpt_format,rpt_type,update_id)
			{
				//alert("su..re");
				$('#report_format').val(rpt_format);
				$('#report_type').val(rpt_type);
				$('#update_id').val(update_id);
			}
		</script>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:100%;">
            <div class="form_caption">Report Template</div>
            <div align="center" style="height:15px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div></div>
            <form name="report_all_type" id="report_all_type" action="javascript:fnc_report_template()">
                <fieldset>
                    <legend>Generate Report</legend>
                    <table align="center">
                        <tr>
                            <td>Report Format Name</td>
                            <td><input type="text" name="report_format" id="report_format"  class="text_boxes" /></td>
                            <td>Report Type</td>
                            <td>
                                <select name="report_type" id="report_type" style="width:100px" class="combo_boxes">
                                <option value="1">Letter</option>
                                <option value="2">Form</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div style="height:30px;margin-top:20px;">
                        <input type="submit" name="rpo_search" id="rpo_search" value="Save" class="formbutton" style="width:100px;" />
                        <input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton" style="width:100px;" />
                        <input type="hidden" name="update_id" id="update_id" style="width:100px;" />
                    </div>
                </fieldset>
            </form>
            <fieldset>
                <div id="div_template_report"></div>
            </fieldset>
		</div>
    </body>
</html>