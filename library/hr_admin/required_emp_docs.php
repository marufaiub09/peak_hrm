<?php 
/*######################################

	Completed By
	Name : Ekram
	Date : 15-06-2013
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$designation_chart = array();
	while( $row = mysql_fetch_assoc( $result ) ) {		
			$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        <script type="text/javascript" src="includes/functions.js"></script>
        
        <!--<script src="../../resources/multiselect/multi_select.js" type="text/javascript"></script>
        <link href="../../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />-->
        
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function(e) {
				/*$("#designation_id").multiselect({
				//header: false, 
				selectedText: "# of # selected",
				});*/
				set_multiselect('designation_id','0','0','','');
			});   
        
			function createObject() 
			{
				var request_type;
				var browser = navigator.appName;
				if( browser == "Microsoft Internet Explorer" ) {
					request_type = new ActiveXObject("Microsoft.XMLHTTP");
				} else {
					request_type = new XMLHttpRequest();
				}
				return request_type;
			}
			var http = createObject();
    
			//Numeric Value allow field script
			function numbersonly(myfield, e, dec)
			{
				var key;
				var keychar;
			
				if (window.event) key = window.event.keyCode;
				else if (e) key = e.which;
				else return true;
				keychar = String.fromCharCode(key);
			
				// control keys
				if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
				return true;
				
				// numbers
				else if ((("0123456789.").indexOf(keychar) > -1)) return true;
				else return false;
			}
        
			//save update function      
			function   save_update_required_docs()
			{
				//alert("su..re");
				var error = false;
				var  document_name	= $('#document_name').val();
				var  document_type	= $('#doc_type').val();
				var  requer_copy	= $('#requer_copy').val();
				var  sub_tenure		= $('#sub_tenure').val();
				var  designation_id	= $('#designation_id').val();
				var  status_active	= $('#status_active').val();
				var save_up 		= $('#save_up').val();
				
				if( $('#document_name').val() == "") 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#document_name').focus();
						$(this).html('Enter document name.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(3000);
					});
				}
				else if( $('#doc_type').val() == 0) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#doc_type').focus();
						$(this).html('Select document type.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(3000);
					});
				}
				else if( $('#doc_type').val() == 2 && $('#requer_copy').val()=="") 
				{
					//if($('#requer_copy').val()=="")
					//{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#requer_copy').focus();
						$(this).html('Enter required copy.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(3000);
					});
				}
				else if($('#doc_type').val() == 2 && $('#sub_tenure').val()=="")
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#sub_tenure').focus();
						$(this).html('Enter submission tenure.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(3000);
					});
				}
				else if($('#doc_type').val() == 2 && $('#designation_id').val()=="")
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#designation_id').focus();
						$(this).html('Select designation.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(3000);
					});
				}
				else
				{
					if(error==false)
					{
						//alert("su..re...");
						http.open( 'GET', 'includes/save_update_hr_admin.php?action=required_docs&isupdate='+save_up
								+'&document_name='+document_name
								+'&document_type='+document_type
								+'&requer_copy='+requer_copy
								+'&sub_tenure='+sub_tenure
								+'&designation_id='+designation_id
								+'&status_active='+status_active);
						
						http.onreadystatechange=response_required_documents;
						http.send(null);
					}
				}
			}
			
			// response_required_documents
			function response_required_documents() 
			{
				if(http.readyState == 4) 
				{
					var response = http.responseText.split('_');
					//alert(response);
					if( response[0] == 3 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
							showResult_type_all('','list_required_documents','list_view_documents');
							$(this).fadeOut(5000);
							document.getElementById('required_docs_form').reset();
						});
					}
					else if( response[0] == 4 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() {
							$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
							showResult_type_all('','list_required_documents','list_view_documents');
							document.getElementById('save_up').value="";
							$(this).fadeOut(5000);
							document.getElementById('required_docs_form').reset();
						});
					}
				}
			}
			
			// row_select
			function row_select(id)
			{
				//alert("su..re");
				var data=id.split('.');
				if(data[0]=="s")
				{
					document.getElementById('search'+data[1]).bgColor='#A6CAF0';
				}
				else
				{
					document.getElementById('row_bgcolor_'+id).bgColor='#A6CAF0';
				}
			}
		
			// row_unselect
			function row_unselect(id)
			{
				var data=id.split('.');
				if(data[0]=="s")
				{
					(data[1]%2==0)?document.getElementById('search'+data[1]).bgColor='#E9F3FF':document.getElementById('search'+data[1]).bgColor='#FFFFFF';
				}
				else
				{
					(id%2==0)?document.getElementById('row_bgcolor_'+id).bgColor='#E9F3FF':document.getElementById('row_bgcolor_'+id).bgColor='#FFFFFF';
				}
			}
        </script>
    </head>
    <body style="font-family:verdana; font-size:11px;" onload="showResult_type_all('','list_required_documents','list_view_documents');">
        <div align="center" style="width:99%;">
            <div class="form_caption">Employeement Required Documents</div>
            <div style="height:19px;"><div id="messagebox" style="background-color:#FF9999; color:#000000; border-radius:5px; width:820px;" align="center"></div></div>
            <form id="required_docs_form" action="" method="POST">
                <fieldset style="width:800px;">
                    <!--<legend>Required Documents Information</legend>-->
                    <table width="760px" border="0" cellpadding="0" cellspacing="2">
                        <tr>
                            <td width="130px">Document's Name</td>
                            <td width="140px"><input type="text" name="document_name" id="document_name" class="text_boxes" value="" style="width:140px;" /></td>
                            <td width="100px">Document Type</td>
                            <td  width="150px"> 
                            	<select name="doc_type" id="doc_type" class="combo_boxes" style="width:145px;">
									<?php 
										foreach($document_type_arr as $key=>$value)
										{
											echo "<option value=".$key.">".$value."</option>";
										}
									?>
                                </select>
                            </td>
                            <td width="100px">Required Copy</td>
                            <td  width="140px">
                            <input type="text" name="requer_copy" id="requer_copy" class="text_boxes" value="" style="width:140px;"  onKeyPress=" return numbersonly(this,event)"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Submission Tenure</td>
                            <td>
                            <input type="text" name="sub_tenure" id="sub_tenure" class="text_boxes" value="" style="width:140px;"  onKeyPress=" return numbersonly(this,event)"/>
                            </td>
                            <td>Designation</td>
                            <td>
                            <select name="designation_id" id="designation_id" class="combo_boxes" style="width:145px"  multiple="multiple" >
                            <option value="0">-- Select --</option>
                            <?php foreach( $designation_chart AS $key=>$value ){ ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                            </select>
                            </td> 
                            <td>Status</td>
                            <td>
                            <select name="status_active" id="status_active" class="combo_boxes" style="width:150px;">
                            <?php
                            foreach($status_active as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                            if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?></option>
                            <?		
                            endforeach;
                            ?>
                            </select>
                            </td>
                        </tr>
                    </table>
                    <div style="padding-top:10px;" align="center"> 
                        <input  type="button" name="save" class="formbutton" value="Save"  style="width:100px;"  onclick="javascript:save_update_required_docs();"/>
                        <input type="reset" name="refresh" class="formbutton" value="Refresh"  style="width:100px;"/>
                        <input type="hidden" name="save_up" id="save_up" value="" />
                    </div>
                </fieldset>
            </form>
            <div style="height:10px;"></div>
            <div id="list_view_documents"  align="center" style="width:620px;"></div>
        </div>
    </body>
</html>