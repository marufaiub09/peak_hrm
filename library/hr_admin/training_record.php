<?php
/**************************************
|	Developed : Md. Nuruzzaman
|	Date : 03-12-2013
***************************************/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/array_function.php');
include('../../includes/common.php');

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$level_designation = array();
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 GROUP BY system_designation ORDER BY level, system_designation, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
while( $row = mysql_fetch_assoc( $result ) ) {
	$level_designation[$row['level']] = $row['system_designation'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="../../includes/update_status.js" type="text/javascript"></script>
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        
        <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
         <script type="text/javascript" src="../../js/driver.phonetic.js" ></script>
        <script type="text/javascript" src="../../js/driver.probhat.js" ></script>
        <script type="text/javascript" src="../../js/engine.js" ></script>
        
        <script>
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
			
            $(document).ready(function() {
				populate_data();	
            });
			function populate_data()
			{
               $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			    $.ajax({
                    type: "POST",					
                    url: "hr_admin_data.php",
                    data: 'form=frm_training',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#tbl_tiffin_bill_policy').dataTable({
                            "bRetrieve": true,
                            "bDestroy": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                         	 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
                            //"aaSorting": [[ 0, "asc" ]],*/
                          	"oLanguage": { "sSearch": "Search all columns:" },
                            "sScrollX": "100%",
                            "sScrollY": "200px",
                            "sScrollXInner": "100%",
                            "bScrollCollapse": true
                        });
                    }
                });
			}
			function js_set_value(update_id,training_name,short_name,status)
			{
				//alert(update_id+', '+training_name+', '+short_name+', '+status);
				$('#is_update').val(update_id);
				$('#txt_training_name').val(training_name);
				$('#txt_short_name').val(short_name);	
				$('#cbo_status').val(status);	
			}
			function fnc_length_check(val,max_length)
			{
				//alert(val);
				var str_length=val.length;
				if(str_length>=max_length)
				{
					alert("You can enter only 10 character");
					return;
					//document.getElementById(field_id).value=max_val;
				}
			}
			function reset_form()
			{
				$('#is_update').val('');
				$('#txt_training_name').val('');
				$('#txt_short_name').val('');	
				$('#cbo_status').val(1);	
			}

      </script>
        <style>
            #designation_details input[type="text"] { width:150px; }
            #designation_details select { width:150px; }
            .formbutton { width:100px; }
            .ui-icon { cursor:pointer; }
            .ui-icon-check, .ui-icon-closethick { float:left; }
            .ui-icon-trash { float:right; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:1000px;">
            <div style="height:48px;">
                <div class="form_caption">Training Record</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update.$delete; ?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000; width:820px; margin-left:65px;" align="center" ></div>
            </div>
            <form name="frm_training" id="frm_training" action="javascript:func_training(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
                <fieldset style="margin-top:10px; margin-left:65px; width:800px;"><legend>Training Name</legend>	
                    <table border="0" cellpadding="0" cellspacing="2" align="center">
                        <tr>
                            <td>Training Name</td>
                            <td><input type="text" name="txt_training_name" id="txt_training_name" class="text_boxes" /></td>
                            <td>Short Name</td>
                            <td><input type="text" name="txt_short_name" id="txt_short_name" class="text_boxes" onKeyUp="fnc_length_check(this.value,10);" maxlength="10" /></td>
                            <td>Status</td>
                            <td>
                                <select name="cbo_status" id="cbo_status" class="combo_boxes">
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="padding-top:10px;">
                                <input type="hidden" name="is_update" id="is_update" value="" />
                                <input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
                                <input type="button" name="cancel" id="cancel" class="formbutton" value="Refresh" onclick="reset_form()" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
        <div id="data_panel" class="demo_jui" align="center" style="width:824px; margin-top:10px; margin-left:122px;"></div>
    </body>
</html>