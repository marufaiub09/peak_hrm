<?php

/*######################################

	Completed By
	Name : Md.Ekram Hossain
	Date : 10-21-2012
		
######################################*/





session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//echo $company_cond."xcz";
//---------------------------------------------------------------------------------------------------------
include("../../includes/common.php");
include("../../includes/array_function.php");
//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY level ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}


//report type chart
$sql = "SELECT * FROM lib_report  ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$report_info = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$report_info[$row['id']] = $row['report_format']; 
}





//$form_report=array(1=>"Assistant Manager",2=>"Executive Officer or Assistant officer",3=>"Staff or Supervisor",4=>"peon",5=>"Worker",6=>"Application Letter for all");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="includes/ajax_submit_hr_admin.js"></script>
    <script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
    
    <script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" />


<script>

	function showResult(str,type,div)
	{
		
		if (str.length==0)
		  {
			  document.getElementById(div).innerHTML="";
			  document.getElementById(div).style.border="0px";
		  return;
		  }
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  	xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			   {
					//document.getElementById(div).innerHTML=xmlhttp.responseText;
					//alert(xmlhttp.responseText);
					editor.setData(xmlhttp.responseText);
					//document.getElementById(div).style.border="1px solid #A5ACB2";
			   }
		  }
		  //alert(type);
		xmlhttp.open("GET","includes/list_view_appointment_letter.php?search_string="+str+"&type="+type,true);
		xmlhttp.send();
	
	}
</script>


 
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:1050px">
    
        <div class="form_caption">
            General Report Template</div>
		<div align="center" id="messagebox"></div>
	
    <form autocomplete="off" id="appointment_letter_generate"  method="post">
		<fieldset  style="width:100%">
			<legend>Search Panel</legend>
<table>
      <tr>
               <td width=""> Company</td>
				<td width="130">
					<select name="company_id" id="company_id" class="combo_boxes" style="width:130px;">
                     <? if($company_cond=="")
					 { 
						?>
					 <option value="0">---Select---</option>
                      <?php } foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
						<option value="<?php echo $company['id']; ?>" <? if(count($company_details)==1)echo"selected"; ?>><?php echo $company[						                         'company_name']; ?></option>
					    <?php } } ?>
					</select>
				</td>
                <td width="">Form/Report Name</td>
               <td width="130"><select name="form_report_name"  id="form_report_name" class="combo_boxes" style="width:130px;" >
                   		<option value="0">Select Report Name</option>
                          <?php
						  		 
                                foreach($report_name as $key=>$value):
                                ?>
                    <option value=<? echo "$key"; ?>> <? echo "$value" ;?> </option>							
                                <?php		
                                endforeach;
                                ?>      
                    </select>
                </td>
                 <td width=""> Report Template</td>
			 		<td width="130"><select name="report_template"  id="report_template" class="combo_boxes" style="width:130px;" onchange="showResult( document.getElementById('company_id').value+'_'+document.getElementById('form_report_name').value+'_'+this.value,'search_employee_appointment_letter','appointment_letter_list_view')" >
					<option value="0">Select Report Template</option>
                                <?php
                                foreach($report_info as $key=>$value):
                                ?>
                    <option value=<? echo "$key"; ?>> <? echo "$value" ;?> </option>							
                                <?php		
                                endforeach;
                                ?>
                    </select>
			</td>
            
		 </tr>
		 <tr>
            <td>Write Text Here:</td>
            <td colspan="6"><input type="text" id="letter_body_text" name="letter_body_text" /> 
            		<script>
						var editor = CKEDITOR.replace('letter_body_text');
						/*
							var editor =CKEDITOR.replace( 'letter_body_text',{
							fullPage: true,
							allowedContent: true
							});
						*/
					</script>
			</td>
       </tr>
       <tr>   
           <td colspan="6"  style="padding-top:10px;" align="center">
           <div class="button_container"> 
          <input type="button" name="rpo_search" id="rpo_search" value="Save" class="formbutton"  style="width:130px;" onclick="javascript:fnc_appointment_letter_generate()"/>
          <input type="reset" name="rpo_reset" id="rpo_reset" value="Refresh" class="formbutton"   style="width:130px;"/>
           <input type="hidden" name="save_up" id="save_up">
           </div>
           </td>
        </tr>
</table>
 </fieldset>
<!-- <fieldset>-->
 		<div id="appointment_letter_list_view" ></div>
       
<!--</fieldset>-->
 </form>

</div>
</body>
</html>