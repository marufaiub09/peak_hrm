<?php 

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');

//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var payroll_heads = new Array();
		<?php 
		foreach( $payroll_heads AS $payroll_id => $payroll ) {
			echo "payroll_heads[$payroll_id] = new Array();\n";
			foreach( $payroll AS $key => $value ) {
				if( $key == 'id' ) echo "payroll_heads[$payroll_id]['$key'] = $value;\n";
				else echo "payroll_heads[$payroll_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		function reset_form(form) 
    { 
        // iterate over all of the inputs for the form
        // element that was passed in
        $('#'+form).find(':input').each(function() {
            var type = this.type;
            var tag = this.tagName.toLowerCase(); // normalize case
            // it's ok to reset the value attr of text inputs,
            // password inputs, and textareas
            if (type == 'text' || type == 'password' || tag == 'textarea')
                this.value = "";
            // checkboxes and radios need to have their checked state cleared
            // but should *not* have their 'value' changed
            else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
            // select elements need to have their 'selectedIndex' property set to -1
            // (this works for both single and multiple select elements)
            else if (type == 'select-one')
                this.selectedIndex = 0;
            else if (type == 'hidden')
                this.value = "";
        });
    }
	
		$(document).ready(function() {
			populate_data();
		});
		
		function show_payroll_head( payroll_id ) {
			if( payroll_id != 0 ) {
				$('#system_head').val( payroll_id );
				$('#chk_update').val(payroll_id);//for update check
				$('#custom_head').val( payroll_heads[payroll_id]['custom_head'] );
				$('#custom_head_local').val( payroll_heads[payroll_id]['custom_head_local'] );
				$('#type').val( payroll_heads[payroll_id]['type'] );
				$('#abbreviation').val( payroll_heads[payroll_id]['abbreviation'] );
				if( payroll_heads[payroll_id]['salary_head'] == 1 ) $('#salary_head').attr( 'checked', true );
				else $('#salary_head').attr( 'checked', false );
				
				if( payroll_heads[payroll_id]['is_applicable'] == 1 ) $('#chk_applicable').attr( 'checked', true );
				else $('#chk_applicable').attr( 'checked', false );
				
				$('#status_active').val( payroll_heads[payroll_id]['status_active'] );
			}
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "hr_admin_data.php",
				data: 'form=payroll_heads',
				success: function(data) {
					$('#data_panel').html( data );
					$('#payroll_heads').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
		

//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

		
		
		
		
	</script>
	<style>
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%;">
     <div>
    	<div class="form_caption">
		Payroll Head
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>	
	<form id="payroll_heads_form" action="javascript:fnc_payroll_heads(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset>
			<legend>Payroll Head Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>System Head:</td>
					<td>
						<select name="system_head" id="system_head" class="combo_boxes" style="width:190px;" onchange="show_payroll_head( this.value )">
							<option value="0">-- Select Head  --</option>
							<?php foreach( $payroll_heads  as $payroll ) { ?>
							<option value="<?php echo $payroll['id']; ?>"><?php echo $payroll['system_head']; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>Custom:</td>
					<td>
						<input type="text" name="custom_head" id="custom_head" class="text_boxes" value="" style="width:140px;" />
						<input type="text" name="custom_head_local" id="custom_head_local" class="text_boxes" value="" style="width:140px;" placeholder="Local Language" />
					</td>
					<td>Type:</td>
					<td>
						<select name="type" id="type" class="combo_boxes" style="width:120px;">
							<option value="0">Earning</option>
							<option value="1">Deduction</option>
							<option value="2">Taxation</option>
							<option value="3">LFA Earning</option>
							<option value="4">Festival Earning</option>
						</select>
					</td>
					<td>Abbreviation:</td>
					<td><input type="text" name="abbreviation" class="text_boxes" id="abbreviation" value="" style="width:30px;" /></td>
					<td>Salary Head:</td>
					<td><input type="checkbox" name="salary_head" id="salary_head" value="yes" /></td>
				</tr>
                <tr height="20">
                </tr>
                </table>
                
                <table>
        <tr>
            <td colspan="2">Applicable This Company: <input type="checkbox" name="chk_applicable" id="chk_applicable" value="0" /></td>
            <td colspan="2">Use In Salary Process: <input type="checkbox" name="app_in_process" id="app_in_process" value="0" /></td>
            <td colspan="2">Use In Salary Sheet: <input type="checkbox" name="app_salary_sheet" id="app_salary_sheet" value="0" /></td>
            <td colspan="2">Sequence Number:<input type="text" name="sq_number" id="sq_number" class="text_boxes"  style="width:60px;" onkeypress="return numbersonly(this,event)"/></td>
            <td colspan="2">&nbsp;</td>
        </tr>
                <tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="10" align="center" style="padding-top:10px;" class="button_container">
						<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="reset" name="cancel" class="formbutton" value="Cancel" />
                        <input type="hidden" id="chk_update" name="chk_update" value="" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div>
    </div>
</body>
</html>