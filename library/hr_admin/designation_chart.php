<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/array_function.php');
include('../../includes/common.php');





/*

//designation_chart
$sql = "SELECT emp_code,count(*) as dayss FROM hrm_attendance WHERE attnd_date like '2013-01%' and status='' group by emp_code";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
 
while( $row = mysql_fetch_assoc( $result ) ) {
	 mysql_query( "update hrm_salary_mst set total_present_days=total_present_days+'$row[dayss]' where emp_code='$row[emp_code]' and salary_periods='2013-01-01'" ); // =(SELECT count(*) FROM `hrm_attendance` WHERE attnd_date like '2013-01%' and status='' and  group by emp_code)
}
 mysql_query( "update hrm_attendance set status='P' where attnd_date like '2013-01%' and status=''" );
 echo "Doneeeeeee";
 die;*/

//designation_chart
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$designation_chart[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$designation_chart[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

$level_designation = array();
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 GROUP BY system_designation ORDER BY level, system_designation, custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
while( $row = mysql_fetch_assoc( $result ) ) {
	$level_designation[$row['level']] = $row['system_designation'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
     <script type="text/javascript" src="../../js/driver.phonetic.js" ></script>
    <script type="text/javascript" src="../../js/driver.probhat.js" ></script>
    <script type="text/javascript" src="../../js/engine.js" ></script>
    
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var designation_chart = new Array();
		<?php
		foreach( $designation_chart AS $designation_id => $designation ) {
			echo "designation_chart[$designation_id] = new Array();\n";
			foreach( $designation AS $key => $value ) {
				if( $key == 'id' || $key == 'level' ) echo "designation_chart[$designation_id]['$key'] = $value;\n";
				else echo "designation_chart[$designation_id]['$key'] = '$value';\n";
			}
		}		
		?>
		
		function fn_set_level_id(sys_desig)
		{
			var desig_chart = new Array();
			<?php
				$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
				$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
				
				$designation = array();
				while( $row = mysql_fetch_assoc( $result ) ) {
					$designation[$row['system_designation']] = $row['level'];				
				}
				foreach( $designation AS $key => $value ) {
				 echo "desig_chart['$key'] = $value;\n";			
				}
			?>
			$('#level').val(desig_chart[sys_desig]);
		}
		
		$(document).ready(function() {
			populate_data();
		});
		
		function show_designation( designation_id ) {
			$('#new').css( "visibility", "visible" );
			$('#level').val( designation_chart[designation_id]['level'] );
			$('#system_designation').val( designation_chart[designation_id]['system_designation'] );
			$('#custom_designation').val( designation_chart[designation_id]['custom_designation'] );
			$('#custom_designation_local').val( designation_chart[designation_id]['custom_designation_local'] );
			$('#status_active').val( designation_chart[designation_id]['status_active'] );
			$('#allowance_rate').val( designation_chart[designation_id]['allowance_rate'] );
			$('#allowance_treatment').val( designation_chart[designation_id]['allowance_treatment'] );
			$('#salary_grade').val( designation_chart[designation_id]['salary_grade'] );
			$('#is_update').val( designation_id );
		}
		
		function reset_form() {
			$('#new').css( "visibility", "hidden" );
			$('#level').val( 1 );
			$('#system_designation').val( 1 );
			$('#custom_designation').val( '' );
			$('#custom_designation_local').val( '' );
			$('#allowance_rate').val( '' );
			$('#allowance_treatment').val( '' );
			$('#status_active').val( 1 );
			$('#is_update').val( 0 );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "hr_admin_data.php",
				data: 'form=designation',
				success: function(data) {
					$('#data_panel').html( data );
					$('#designations').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ null, { "sType": "html" }, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
		
		$(document).ready(function(){
                $(".bangla").bnKb({
                    'switchkey': {"webkit":"k","mozilla":"y","safari":"k","chrome":"k","msie":"y"},
                    'driver': phonetic
                });
            });
		
	</script>
  <script>
  //Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
  </script>
	<style>
		#designation_details input[type="text"] { width:150px; }
		#designation_details select { width:150px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:1000px;">
	 <div>
    	<div class="form_caption">
		Designation Chart
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form id="designation_details" action="javascript:fnc_designation_details(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="margin-top:10px;">
			<legend>Designation Information</legend>	
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>Level:</td>
					<td>
						<select name="level" id="level" class="combo_boxes"  disabled="disabled">
							<?php foreach( $level_designation AS $level => $system_designation ) { ?>
							<option value="<?php echo $level; ?>"><?php echo $level; ?></option>
							<?php } ?>
						</select>
					</td>
					<td width="150">System Designation:</td>
					<td >
						<select name="system_designation" class="combo_boxes" id="system_designation" onchange="fn_set_level_id(this.value)">
							<?php foreach( $level_designation AS $level => $system_designation ) { ?>
							<option value="<?php echo $system_designation; ?>"><?php echo $system_designation; ?></option>
							<?php } ?>
						</select>
						<img id="new" src="../../resources/images/new.jpg" title="New Section" alt="New" onclick="reset_form()" style="cursor:pointer; visibility:hidden;" />
					</td>
					<td width="140">Custom Designation:</td>
					<td  style="font-family:'Kalpurush ANSI'">
						<input type="text" name="custom_designation" class="text_boxes" id="custom_designation" value="" />
                        <input type="text" name="custom_designation_local" class="bangla" id="custom_designation_local" placeholder="local language" />
						
					</td>
					<td>Status:</td>
					<td>
						<select name="status_active" id="status_active" class="combo_boxes" style="width:80px;">
							 <?php
							foreach($status_active as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
				</tr>
                
                <tr>
					<tr>
					
                    <td width="100">Allowance Rate</td>
                    <td><input type="text" name="allowance_rate" id="allowance_rate" class="text_boxes"   onkeypress="return numbersonly(this,event)" /></td>
                    <td >Allowance Treatment</td>
					<td>
						<select name="allowance_treatment" id="allowance_treatment" class="combo_boxes">
							<option value="0">----Select----</option>
                            <option value="1">Holiday OT</option>
                            <option value="2">Generalday OT</option>
                            <option value="3">Both Day</option>
						</select>
					</td>
                     <td width="100">Salary Grade</td>
                    <td><input type="text" name="salary_grade" id="salary_grade" class="text_boxes"/></td><!--onkeypress="return numbersonly(this,event)"  maxlength="2"-->			
				</tr>			
				</tr>
				<tr>
					<td colspan="8" align="center" style="padding-top:10px;" class="button_container">
						<input type="hidden" name="is_update" id="is_update" value="0" />
						<input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" id="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div>
</body>
</html>
<?php mysql_close( $host_connect ); ?>