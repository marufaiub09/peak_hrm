<?php
/*************************************
|	Tiffin allowance Policy
|	Developed by : Md. Nuruzzaman
*************************************/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

$formula_allowance=array(	0=>"-- Select --",
							1=>"((Basic/30)/WH)*OT Hour",
							2=>"((Basic/DOM)/WH)*OT Hour",
							3=>"((Gross/DOM)/WH)*OT Hour",
							4=>"((Gross/30)/WH)*OT Hour",
							5=>"((Basic/208)*2)*OT Hour",
							6=>"(Basic/208)*OT Hour",
							7=>"((Gross/208)*2)*OT Hour",
							8=>"(Gross/208)*OT Hour");
							
// formula_type
$formula_type=array(0=>"-- Select --", 1=>"Fixed Amount", 2=>"Percentage (%)", 3=>"Formula");

// pay_with_salary
$pay_with_salary=array(0=>"-- Select --", 1=>"Yes", 2=>"No");

// day_type
$day_type=array(0=>"-- Select --",1=>"All Type",2=>"General Day",3=>"Off-day",4=>"Holiday");

//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
array_unshift($payroll_heads,"-- Select --");
$payroll_heads_salary = array();
array_unshift($payroll_heads_salary,"-- Select --");
while( $row = mysql_fetch_assoc( $result ) ) {
	if( $row[salary_head]!=1) $payroll_heads[$row['id']] = mysql_real_escape_string( $row[custom_head] );
	if( $row[salary_head]==1)$payroll_heads_salary[$row['id']]= mysql_real_escape_string(  $row[custom_head]  );
}


//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
array_unshift($shift_policy,"-- Select --");
while( $row = mysql_fetch_assoc( $result ) ) 
{
	$shift_policy[$row['id']]= mysql_real_escape_string($row['shift_name']);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="../../../includes/update_status.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
        <script src="includes/functions.js" type="text/javascript" ></script>
        <script type="text/javascript" charset="utf-8">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
        
            $(document).ready(function() {
				page_load();
                //populate_data();
            });
			
			// page_load
			function page_load() 
			{
				$('#tbl_shift_time_distribution tbody tr').remove();
				//add_row();
				js_set_value(0)
				
			}
			
            // When load the page then this function will be execute
           /* function populate_data() 
			{
                $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                $.ajax({
                    type: "POST",
                    url: "../hr_admin_data.php",
                    data: 'form=frm_allowance_policy',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#tbl_allowance_policy_listview').dataTable({
                            "bRetrieve": true,
                            "bDestroy": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                         	 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
                            //"aaSorting": [[ 0, "asc" ]],
                          	"oLanguage": { "sSearch": "Search all columns:" },
                            "sScrollX": "100%",
                            "sScrollY": "200px",
                            "sScrollXInner": "100%",
                            "bScrollCollapse": true
                        });
                    }
                });
            }*/
			
			// Click on the ADD ROW button this function will be execute
			function add_row()
			{
				var rowCount=$("#tbl_shift_time_distribution tbody tr").length;  
				/*if( $('#txt_allowance_'+rowCount).val()=="" ) return;*/
					
				rowCount++;
				$('#tbl_shift_time_distribution tbody').append(
					'<tr>'
						+'<td>'
							+'<input type="hidden" name="update_id[]" id="update_id_' + rowCount + '" class="text_boxes" style="width:120px; text-align:center;" /></td>'
						+'</td>'
						+'<td>'
							+'<select name="shift_id[]" id="shift_id_' + rowCount + '"  class="combo_boxes" style=" width:180px;">'
									<?php foreach( $shift_policy AS $id=>$val ) {?>
									+'<option value="<?php echo $id; ?>"><?php echo $val; ?></option>'
									<?php } ?>
							+'</select>'
						+'</td>'
						+'<td  id="search_value_' + rowCount + '">'
							+'<input type="text" id="value_minute_' + rowCount + '" name="value_minute[]" class="text_boxes" onKeyPress="return numbersonlyminutes(this, event);" style=" width:100px; text-align:center;" />'
						+'</td>'
						+'<td>'
							+'<select name="status_type[]" id="status_type_' + rowCount + '" class="combo_boxes" style=" width:120px;">'
									<?php foreach( $value_status AS $id=>$val ) {?>
									+'<option value="<?php echo $id; ?>"><?php echo $val; ?></option>'
									<?php } ?>
							+'</select>'
						+'</td>'
					+'</tr>'
				);
			}
			
			// Click on the REMOVE ROW button this function will be execute
			function  remove_row()
			{
				var r=confirm("Are you sure want to REMOVE ROW !!");
				if(r==true)
				{
					var rowCount = $('#tbl_shift_time_distribution tbody tr').length;
					if(rowCount!=1)
					{
						$("#tbl_shift_time_distribution tr:last").remove();
					}
				}
			}
			
			// Click on the REFRESH button this function will be execute
			function reset_form() {
				var r=confirm("Are you sure want to REFRESH fields !!");
				if(r==true)
				{
					$('#tbl_shift_time_distribution tbody tr').remove();
					add_row();
				}
			}
			
			// Click on th SAVE button this function will be execute
			function shift_time_distribution_save(save_perm,edit_perm,delete_perm)
			{
				//alert("I am from ACTION");
				var update_id='';
				var shift_id='';
				var value_minute='';
				var status_type='';
			
				// Validation checking here
					
					var rowCount = $('#tbl_shift_time_distribution tbody tr').length;
					for (var i=1; i<=rowCount; i++)
					{
						if( $('#shift_id_'+i).val()== 0 ) 
						{
							error = true;
							$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#shift_id_'+i).focus();
							$(this).html('Please Select Shift Name.').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
							});
							return;
						}
						/*else if($('#value_minute_'+i).val()== "")
						{
							error = true;
							$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#value_minute_'+i).focus();
							$(this).html('Please Select Time (Minutes).').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
							});
							return;
						}*/
						else if($('#status_type_'+i).val()== 0)
						{
							error = true;
							$('#messagebox').fadeTo( 200, 0.1, function() {
							$('#status_type_'+i).focus();
							$(this).html('Please Select type.').addClass('messageboxerror').fadeTo(900,1);
							$(this).fadeOut(5000);
							});
							return;
						}
						else
						{
							if(update_id=='') update_id=$('#update_id_'+i).val(); else update_id=update_id+"__"+$('#update_id_'+i).val(); 
							if(shift_id=='') shift_id=$('#shift_id_'+i).val(); else shift_id=shift_id+"__"+$('#shift_id_'+i).val();
							if(value_minute=='') value_minute=$('#value_minute_'+i).val(); else value_minute=value_minute+"__"+$('#value_minute_'+i).val();
							if(status_type=='') status_type=$('#status_type_'+i).val(); else status_type=status_type+"__"+$('#status_type_'+i).val();
							//alert(allowance);
						}
					}
					//alert(rowCount);return;
					http.open( 'GET', 'includes/save_update_policy.php?action=shift_time_distribution&update_id='+update_id+
						'&shift_id='+shift_id+
						'&value_minute='+value_minute+
						'&status_type='+status_type);
						
					http.onreadystatechange = response_shift_time_distribution;
					http.send(null);
				
			}
			
			// Response comes here
			function response_shift_time_distribution() 
			{
				if(http.readyState == 4)
				{
					//alert (http.responseText);
					var response = http.responseText;
					if( response == 1 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() 
						{
							$(this).html('Shift time Distribution has been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
							$(this).fadeOut(5000);
							//document.getElementById('tiffin_allowance_policy').reset();
						});
						page_load();
						//populate_data();
					}
					/*else if( response == 2 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() 
						{
							$(this).html('Allowance policy has been Updated successfully .').addClass('messagebox_ok').fadeTo( 900, 1 )
							$(this).fadeOut(5000);
							//document.getElementById('tiffin_allowance_policy').reset();
						});
						page_load();
						populate_data();
					}*/
				}
			}
			
			// Click on the each ROW of list view this function will be execute
			function js_set_value(policy_id)
			{
				
				$.ajax({
						type: "POST",
						url: "../hr_admin_data.php",
						data: 'data=shift_time_distribution_load&policy_id=' + policy_id,
						success: function( data ) 
						{
							$('#tbl_shift_time_distribution tbody tr').remove();
							$('#tbl_shift_time_distribution tbody').append(data);
							//document.getElementById('db_row_ids_mst').value=document.getElementById('db_row_ids').value;
							//alert(data);
							if(data=="")
							{
								add_row();
							}
						}
				});
			}
			
//formula type change
/*function change_formula_type(counter)
{
	//alert(counter);
	switch( $("#formula_type_" + counter).val() ) {
				case '1':
					//alert('ok');
					$("#base_head_" + counter).val('0').attr( 'disabled', 'disabled' );
					document.getElementById('search_value_' + counter).innerHTML='<input type="text" name="value[]" id="value_'+ counter +'" onKeyPress="return numbersonly(this, event);" class="text_boxes" style=" width:188px; text-align:center;" />';
					break;
				
				case '2':
					$("#base_head_" + counter).val('0').removeAttr( 'disabled' );
					document.getElementById('search_value_' + counter).innerHTML='<input type="text" name="value[]" id="value_'+ counter +'" onKeyPress="return numbersonly(this, event);" class="text_boxes" style=" width:188px; text-align:center;" />';
					break;
					
				case '3':
					$("#base_head_" + counter).val('0').attr( 'disabled', 'disabled' );
					document.getElementById('search_value_' + counter).innerHTML='<select name="value[]" id="value_'+ counter +'" class="combo_boxes" style=" width:200px;">'
																	<?php foreach( $formula_allowance AS $id=>$formula ) {?>
																	+' <option value="<?php echo $id; ?>"><?php echo $formula; ?></option>'
																	 <?php } ?>
																+'</select>';
					break;
	}
}*/

//Numeric Value allow field script
function numbersonlyminutes(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
	return true;
	
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
		return true;
	else
		return false;
}

        </script>
        <style>
            .header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div style="height:60px;" align="center">    	
            <div class="form_caption">Shift wise time distribution</div>
            <span id="permission_caption" ><? echo "Your Permissions--> \n".$insert.$update.$delete;?></span>
            <div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div>
        </div>
        <div align="center" style="width:80%px;">     
            <form id="frm_allowance_policy" name="frm_allowance_policy" action="javascript:shift_time_distribution_save(save_perm,edit_perm,delete_perm)" autocomplete="off" method="post">
                <fieldset style="width:60%;">
                    <legend>Shift wise time distribution</legend>
                   
                        <fieldset style="width:80%;">
                            <table width="80%">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="1" cellspacing="1" id="tbl_shift_time_distribution" align="center">
                                            <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th class="header" width="180px">Shift Name</th>
                                                    <th class="header" width="100px">Time (minutes)</th>
                                                    <th class="header" width="120px">Type</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table>
                                            <tr><td><input type="button" name="btn_add" id="btn_add" value="Add Row" class="formbutton" onclick="add_row();" style="width:90px;" /></td></tr>
                                            <tr><td><input type="button" name="btn_remove" id="btn_remove" value="Remove Row" class="formbutton" style="width:90px;" onclick="remove_row();" /></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <table align="center">
                            <tr><td colspan="3">&nbsp; </td></tr>
                            <tr>
                                <td><input type="submit" name="btn_save" id="btn_save" class="formbutton" value="Save" style="width:90px;"/></td>
                                <td><input type="button" name="btn_refresh" id="btn_refresh" class="formbutton" value="Refresh" onclick="reset_form()" style="width:90px;" /></td>
                            </tr>
                        </table>
                </fieldset>
            </form> 
        </div>
        <div id="data_panel" class="demo_jui" align="center" style="width:824px; margin-top:15px; margin-left:135px;"></div>
    </body>
</html>