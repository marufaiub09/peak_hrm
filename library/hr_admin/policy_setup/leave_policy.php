<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 31-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

//leave_policy
$sql = "SELECT * FROM lib_policy_leave WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$leave_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$leave_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$leave_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_leave_definition WHERE policy_id = $row[id] AND is_deleted = 0 ORDER BY policy_id, id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$leave_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$leave_policy[$row['id']]['definition'][$row2['leave_type']] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$leave_policy[$row['id']]['definition'][$row2['leave_type']][$key2] = mysql_real_escape_string( $value2 );
		}
		$sql3 = "SELECT MAX(leave_availed) AS max_availed FROM hrm_leave_balance WHERE leave_policy = $row[id] AND leave_type = '$row2[leave_type]' AND year_start = " . date("Y");
		$result3 = mysql_query( $sql3 ) or die( $sql3 . "<br />" . mysql_error() );
		$row3 = mysql_fetch_assoc( $result3 );
		$leave_policy[$row['id']]['definition'][$row2['leave_type']]['max_availed'] = $row3['max_availed'];
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<!--meta http-equiv="Content-Type" content="text/html; charset=UTF-8"--->
    <meta http-equiv="Content-Type" content="charset=UTF-8" />
    
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var salary_heads = '<option value="0">-- Select --</option>', fixed_deduction = fixed_earning = false, leave_policy = new Array();
		var fields = new Array( 'leave_type','limit_type','el_calulation_days','formula','max_limit','max_availed','consecutive_leave_days','leave_calc_starts_from','leave_can_avail_after',
								'leave_avail_criteria','off_day_leave_count','carrying_forward_allowed','max_acc_limit','fractional_leave' );
		<?php
		foreach( $leave_policy AS $policy_id => $policy ) {
			echo "leave_policy[$policy_id] = new Array();\n";
			foreach( $policy AS $key => $value ) {
				if( $key != 'definition' ) echo "leave_policy[$policy_id]['$key'] = '".mysql_real_escape_string($value)."';\n";
				else {
					echo "leave_policy[$policy_id]['definition'] = new Array();\n";
					foreach( $value AS $key2 => $value2 ) {
						echo "leave_policy[$policy_id]['definition']['$key2'] = new Array();\n";
						foreach( $value2 AS $key3 => $value3 ) {
							echo "leave_policy[$policy_id]['definition']['$key2']['$key3'] = '".mysql_real_escape_string($value3)."';\n";
						}
					}
				}
			}
		}
		foreach( $payroll_heads AS $payroll_id => $payroll ) {
			if( $payroll['salary_head'] == 1 ) echo "salary_heads += '<option value=\"$payroll[id]\">".mysql_real_escape_string($payroll[custom_head])."</option>';\n";
		}
		?>
		
		$(document).ready(function() {
			populate_data();
		});
		
		function leave_type_change( leave_type ) {
			var policy_id = $('#policy_id').val();
			reset_leave_data();
			$('#leave_type').val( leave_type );
			if( policy_id != 0 ) {
				for( var key in leave_policy[policy_id]['definition'] ) {
					if( key == leave_type ) {
						for( var i = 0; i < fields.length; i++ ) $('#' + fields[i]).val( leave_policy[policy_id]['definition'][key][fields[i]] );
						if( leave_policy[policy_id]['definition'][key]['carrying_forward_allowed'] == 1 ) $('#carrying_forward_allowed').attr( 'checked', true );
						//alert( leave_policy[policy_id]['definition'][key]['max_availed'] );
						leave_type_change_attr( leave_policy[policy_id]['definition'][key]['leave_type'] );
						limit_type_change( leave_policy[policy_id]['definition'][key]['limit_type'] );
						carrying_forward_allowed_click();
						
						$('#deduction tbody').html( '' );
						add_deduction_head();
						if( leave_policy[policy_id]['definition'][key]['deduction'] != '' ) {
							var deductions = leave_policy[policy_id]['definition'][key]['deduction'].split( '|' );
							for( var i = 0; i < deductions.length; i++ ) {
								var parts = deductions[i].split( '_' );
								if( i > 0 ) add_deduction_head();
								var j = 0;
								$('#deduction tbody tr').each(function() {
									if( i == j ) {
										$(this).find('select[name="deduction_calculation[]"]').val( parts[0] );
										$(this).find('select[name="deduction_payroll_head[]"]').val( parts[1] );
									}
									j++;
								});
							}
						}
						$('#earning tbody').html( '' );
						add_earning_head();
						if( leave_policy[policy_id]['definition'][key]['earning'] != '' ) {
							var earnings = leave_policy[policy_id]['definition'][key]['earning'].split( '|' );
							for( var i = 0; i < earnings.length; i++ ) {
								var parts = earnings[i].split( '_' );
								if( i > 0 ) add_earning_head();
								var j = 0;
								$('#earning tbody tr').each(function() {
									if( i == j ) {
										$(this).find('select[name="earning_calculation[]"]').val( parts[0] );
										$(this).find('select[name="earning_payroll_head[]"]').val( parts[1] );
									}
									j++;
								});
							}
						}
						break;
					}
				}
			}
			leave_type_change_attr( leave_type );
		}
		
		function leave_type_change_attr( leave_type ) {
			if( leave_type == 'EL' ) {
				$('#limit_type').removeAttr( 'disabled' );
				$('#formula').removeAttr( 'disabled' );
				$('#el_calulation_days').removeAttr( 'disabled' );
				
			}
			else {
				$('#limit_type').val( 0 ).attr( 'disabled', 'disabled' );
				$('#formula').val( 0 ).attr( 'disabled', 'disabled' );
				$('#el_calulation_days').val( 0 ).attr( 'disabled', 'disabled' );
			}
		}
		
		function limit_type_change( limit_type ) {
			if( limit_type == 0 ) $('#max_limit').removeAttr( 'readonly' );
			else $('#max_limit').val( 0 ).attr( 'readonly', 'readonly' );
		}
		
		function carrying_forward_allowed_click() {
			if( $('#carrying_forward_allowed').is(':checked') ) $('#max_acc_limit').removeAttr( 'disabled' );
			else $('#max_acc_limit').attr( 'disabled', 'disabled' );
		}
		
		function add_deduction_head() {
			if( fixed_deduction == true ) {
				$('select[name="deduction_calculation[]"]:first').focus();
				alert( 'Change calculation type first.' );
			}
			else {
				$('#deduction tbody').append(
					'<tr>'
						+ '<td>'
							+ '<select name="deduction_calculation[]" class="combo_boxes" onchange="deduction_calculation_change( this.value );">'
								+ '<option value="0">100% Per Day</option>'
								+ '<option value="1">50% Per Day</option>'
								+ '<option value="2">Fixed Amount Per Day</option>'
							+ '</select>'
						+ '</td>'
						+ '<td>'
							+ '<select name="deduction_payroll_head[]" class="combo_boxes">'
								+ salary_heads
							+ '</select>'
							+ '<input type="text" name="deduction_fixed_amount[]" class="text_boxes" value="" style="display:none;" />'
						+ '</td>'
					+ '</tr>'
				);
			}
		}
		
		function add_earning_head() {
			if( fixed_earning == true ) {
				$('select[name="earning_calculation[]"]:first').focus();
				alert( 'Change calculation type first.' );
			}
			else {
				$('#earning tbody').append(
					'<tr>'
						+ '<td>'
							+ '<select name="earning_calculation[]" class="combo_boxes" onchange="earning_calculation_change( this.value );">'
								+ '<option value="0">100% Per Day</option>'
								+ '<option value="1">50% Per Day</option>'
								+ '<option value="2">Fixed Amount Per Day</option>'
							+ '</select>'
						+ '</td>'
						+ '<td>'
							+ '<select name="earning_payroll_head[]" class="combo_boxes">'
								+ salary_heads
							+ '</select>'
							+ '<input type="text" name="earning_fixed_amount[]" class="text_boxes" value="" style="display:none;" />'
						+ '</td>'
					+ '</tr>'
				);
			}
		}
		
		function deduction_calculation_change( option ) {
			if( option == 2 ) {
				$('#deduction tbody').html( '' );
				add_deduction_head();
				fixed_deduction = true;
				$('#deduction tbody tr:first').find('select[name="deduction_calculation[]"]').val( 2 );
				$('#deduction tbody tr:first').find('select[name="deduction_payroll_head[]"]').css( 'display', 'none' );
				$('#deduction tbody tr:first').find('input[name="deduction_fixed_amount[]"]').css( 'display', '' );
			}
			else {
				if( fixed_deduction == true ) {
					fixed_deduction = false;
					$('#deduction tbody tr:first').find('select[name="deduction_payroll_head[]"]').css( 'display', '' );
					$('#deduction tbody tr:first').find('input[name="deduction_fixed_amount[]"]').css( 'display', 'none' );
				}
			}
		}
		
		function earning_calculation_change( option ) {
			if( option == 2 ) {
				$('#earning tbody').html( '' );
				add_earning_head();
				fixed_earning = true;
				$('#earning tbody tr:first').find('select[name="earning_calculation[]"]').val( 2 );
				$('#earning tbody tr:first').find('select[name="earning_payroll_head[]"]').css( 'display', 'none' );
				$('#earning tbody tr:first').find('input[name="earning_fixed_amount[]"]').css( 'display', '' );
			}
			else {
				if( fixed_earning == true ) {
					fixed_earning = false;
					$('#earning tbody tr:first').find('select[name="earning_payroll_head[]"]').css( 'display', '' );
					$('#earning tbody tr:first').find('input[name="earning_fixed_amount[]"]').css( 'display', 'none' );
				}
			}
		}
		
		function show_policy( policy_id ) {
			if( policy_id == 0 ) reset_form();
			else {
				
				$('#policy_id').val( policy_id );
				$('#is_update').val( policy_id );//for update
				$('#policy_name').val( leave_policy[policy_id]['policy_name'] );
				$('#status_active').val( leave_policy[policy_id]['status_active'] );
				
				for( var key in leave_policy[policy_id]['definition'] ) {
					leave_type_change( key );
					break;
				}
			}
		}
		
		function validate_limit() {
			if( parseInt( escape( $('#max_limit').val() ) ) < parseInt( escape( $('#max_availed').val() ) ) ) {
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#max_limit').focus();
					$(this).html('Max Limit cannot be less than ' + $('#max_availed').val() + ' as leave type already availed.').addClass('messageboxerror').fadeTo(900,1);
					$(this).fadeOut(5000);
				});
			}
		}
		
		function reset_form() {
			$('#policy_id').val( 0 );
			$('#policy_name').val( '' );
			$('#is_update').val( '' );
			reset_leave_data();
		}
		
		function reset_leave_data() {
			$('#leave_type').val( 0 );
			leave_type_change_attr( 0 );
			$('#limit_type').val( 0 );
			$('#formula').val( 0 );
			$('#el_calulation_days').val( 0 );
			$('#max_limit').val( 0 );
			$('#max_availed').val( 0 );
			$('#consecutive_leave_days').val( 0 );
			$('#leave_calc_starts_from').val( 0 );
			$('#leave_can_avail_after').val( 0 );
			$('#leave_avail_criteria').val( 0 );
			$('#off_day_leave_count').val( 0 );
			$('#carrying_forward_allowed').attr( 'checked', false );
			$('#max_acc_limit').attr( 'disabled', 'disabled' ).val( '' );
			$('#fractional_leave').val( 0 );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'form=leave_policy',
				success: function(data) {
					$('#data_panel').html( data );
					$('#tbl_leave_policy').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ null, { "sType": "html" }, { "bSortable": false } ],
						"aaSorting": [[ 1, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
		
				//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
	</script>
	<style type="text/css">
		#leave_policy_form input[type="text"] { width:158px; }
		#leave_policy_form select { width:170px; }
		#deduction select, #earning select { width:257px; }
		#deduction input[type="text"], #earning input[type="text"] { width:245px; }
		.formbutton { width:100px; }
		.header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%; position:relative; height:60px; margin-bottom:5px; margin-top:5px">       
    <div>
    	<div class="form_caption">
		Leave Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
		</div>	
	<form id="leave_policy_form" action="javascript:fnc_leave_policy(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:1062px;">
			<legend>Edit Filter</legend>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						&nbsp;Leave Policy:
						<select name="policy_id" id="policy_id" class="combo_boxes" style="width:auto; margin-left:67px;" onchange="show_policy( this.value );">
							<option value="0">-- Select policy to edit --</option>
							<?php foreach( $leave_policy AS $policy ) { ?>
							<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:1062px; margin-top:10px;">
			<legend>Policy Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>Policy Name:</td>
					<td colspan="3"><input type="text" name="policy_name" id="policy_name" class="text_boxes" value="" style="width:500px;" /></td>
					<td>Status:</td>
					<td>
						<select name="status_active" id="status_active" class="combo_boxes">
									<?php
									foreach($status_active as $key=>$value):
									?>
									<option value=<? echo "$key";
									if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
									<?		
									endforeach;
									?>
						</select>
					</td>
				</tr>
				<tr><td colspan="6" height="10"></td></tr>
				<tr>
					<td>Leave Type:</td>
					<td>
						<select name="leave_type" id="leave_type" class="combo_boxes" onchange="leave_type_change( this.value );">
							<option value="0">-- Select --</option>
							<option value="CL">Casual Leave</option>
							<option value="SL">Sick Leave</option>
							<option value="LWP">Leave Without Pay</option>
							<option value="RL">Recreation Leave</option>
							<option value="SpL">Special Leave</option>
							<option value="EL">Earned Leave</option>
							<option value="EdL">Education Leave</option>
						</select>
					</td>
					<td>Limit Type:</td>
					<td>
						<select name="limit_type" id="limit_type" class="combo_boxes" disabled="disabled" onchange="limit_type_change( this.value );">
							<option value="0">Fixed</option>
							<option value="1">Calculative</option>
						</select>
					</td>
					<td>1 Day Leave for every<input type="text" name="el_calulation_days" disabled="disabled" id="el_calulation_days" class="text_boxes_numeric" style="width:70px" onkeypress="return numbersonly(this,event)"  /></td>
					<td>
						<select name="formula" id="formula" class="combo_boxes" disabled="disabled">
							<option value="0">-- Select --</option>
							<option value="1">days present</option>
							<option value="2">working days</option>
							<option value="3">days present including leave</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Max Limit:</td>
					<td>
                    	<input type="text" name="max_limit" id="max_limit" value="0" class="text_boxes" onblur="validate_limit(); " onkeypress="return numbersonly(this,event)" />
                        <input type="hidden" name="max_availed" id="max_availed" value="" />
                    </td>
					<td>Consecutive Leave Days:</td>
					<td><input type="text" name="consecutive_leave_days" id="consecutive_leave_days" value="0" class="text_boxes" onkeypress="return numbersonly(this,event)"/></td>
					<td>Leave Calculation Starts From:</td>
					<td>
						<select name="leave_calc_starts_from" id="leave_calc_starts_from" class="combo_boxes">
							<option value="0">Date of Joining</option>
							<option value="1">Date of Confirmation</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Leave Can Avail After:</td>
					<td>
						<select name="leave_can_avail_after" id="leave_can_avail_after" class="combo_boxes">
							<option value="0">Date of Joining</option>
							<option value="1">Date of Confirmation</option>
							<option value="2">365 days of DOJ</option>
							<option value="3">2 Years of DOJ</option>
						</select>
					</td>
					<td>Leave Avail Criteria:</td>
					<td>
						<select name="leave_avail_criteria" id="leave_avail_criteria" class="combo_boxes">
							<option value="0">All at any time</option>
							<option value="1">Proportionately</option>
						</select>
					</td>
					<td>Off-day Leave Count:</td>
					<td>
						<select name="off_day_leave_count" id="off_day_leave_count" class="combo_boxes">
							<option value="0">Excluding off-days </option>
							<option value="1">Include in-between, preceding and succeeding off-days</option>
							<option value="2">Include in-between off-days only</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Carrying Forward Allowed?</td>
					<td><input type="checkbox" name="carrying_forward_allowed" id="carrying_forward_allowed" value="1" onclick="carrying_forward_allowed_click();" /></td>
					<td>Max Accumulation Limit:</td>
					<td><input type="text" name="max_acc_limit" id="max_acc_limit" class="text_boxes" value="" disabled="disabled" onkeypress="return numbersonly(this,event)" /></td>
					<td>Fractional Leave:</td>
					<td>
						<select name="fractional_leave" id="fractional_leave" class="combo_boxes">
							<option value="0">Not Allowed</option>
							<option value="1">Allowed</option>
						</select>
					</td>
				</tr>
				<tr><td colspan="6" height="10"></td></tr>
			</table>
			<div style="width:100%; float:left;">
				<div style="width:50%; float:left;">
					<table align="left" width="98%" border="0" cellpadding="0" cellspacing="2" id="deduction">
						<thead>
							<tr><th colspan="2" class="header">Deduction Policy</th></tr>
							<tr>
								<th>Calculation</th>
								<th>Payroll Head</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select name="deduction_calculation[]" class="combo_boxes" onchange="deduction_calculation_change( this.value );">
										<option value="0">100% Per Day</option>
										<option value="1">50% Per Day</option>
										<option value="2">Fixed Amount Per Day</option>
									</select>
								</td>
								<td>
									<select name="deduction_payroll_head[]" class="combo_boxes">
										<option value="0">-- Select --</option>
										<?php foreach( $payroll_heads AS $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
										<option value="<?php echo $payroll['id']; ?>"><?php echo $payroll['custom_head']; ?></option>
										<?php } } ?>
									</select>
									<input type="text" name="deduction_fixed_amount[]" class="text_boxes" value="" style="display:none;" />
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr><th colspan="2" style="padding-top:10px;"><input type="button" name="btn_add_deduction" value="Add head" class="formbutton" onclick="add_deduction_head();" /></th></tr>
						</tfoot>
					</table>
				</div>
				<div style="width:50%; float:right;">
					<table align="right" width="98%" border="0" cellpadding="0" cellspacing="2" id="earning">
						<thead>
							<tr><th colspan="2" class="header">Earning Policy</th></tr>
							<tr>
								<th>Calculation</th>
								<th>Payroll Head</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select name="earning_calculation[]" class="combo_boxes" onchange="earning_calculation_change( this.value );">
										<option value="0">100% Per Day</option>
										<option value="1">50% Per Day</option>
										<option value="2">Fixed Amount Per Day</option>
									</select>
								</td>
								<td>
									<select name="earning_payroll_head[]" class="combo_boxes">
										<option value="0">-- Select --</option>
										<?php foreach( $payroll_heads AS $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
										<option value="<?php echo $payroll['id']; ?>"><?php echo $payroll['custom_head']; ?></option>
										<?php } } ?>
									</select>
									<input type="text" name="earning_fixed_amount[]" class="text_boxes" value="" style="display:none;" />
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr><th colspan="2" style="padding-top:10px;"><input type="button" name="btn_add_earning" value="Add head" class="formbutton" onclick="add_earning_head();" /></th></tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div style="width:100%; float:left;">
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td align="center" style="padding-top:10px;" class="button_container">
							<input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
							<input type="button" name="cancel" id="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
                            <input type="hidden" id="is_update" name="is_update" value="" />
						</td>
					</tr>
				</table>
			</div>
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:1084px; margin-top:15px;"></div>
</body>
</html>