<?php
date_default_timezone_set('UTC');

include('../../includes/common.php');	
extract($_GET);

if($action=="sample_details"){ //Zone List Registrartion and Update Here
   if  ($isupdate==""){//Insert Here
		
		$unique_field_name = "sample_name";
		$table_name = "lib_sample";
		$query = "sample_name='$txt_sample_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false ) 
		{	
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "lib_sample";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO lib_sample (
							id,
							sample_name,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							STATUS,
							is_deleted
						) VALUES (
							$id,
							'$txt_sample_name',
							'$inserted_by',
							'$insert_date',
							'$updated_by',
							'$update_date',
							'$STATUS',
							'$is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		echo "2";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		
		$sql = "UPDATE lib_sample
				SET
					sample_name		= '$txt_sample_name',
					updated_by 			= '$updated_by',
					update_date 		= '$update_date',
					STATUS				= '$STATUS',
					is_deleted			= '$is_deleted'
					WHERE id            = '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		echo "3";		
		exit();
	}
	
}

if ($action=="style_details"){ //Zone List Registrartion and Update Here
   if  ($isupdate==""){//Insert Here
		
		
		//echo "$id";
		//exit(();
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
		
		$unique_field_name = "gmt_item_name";
		$table_name = "lib_garments_item";
		$query = "gmt_item_name='$txt_style_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false ) 
		{	
			echo "9";
			exit();
		}
		$id_field_name = "id";
		$id= return_next_id($id_field_name,$table_name);
		$sql = "INSERT INTO lib_garments_item (
							id,
							gmt_item_name,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'$txt_style_name',
							'$inserted_by',
							'$insert_date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'$cbo_is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		echo "2";
		exit();
	} else {  // Updates here
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		
		$sql = "UPDATE lib_garments_item
				SET
					gmt_item_name		= '$txt_style_name',
					updated_by 			= '$updated_by',
					update_date 		= '$update_date',
					status_active		= '$cbo_status',
					is_deleted			= '$cbo_is_deleted'
					WHERE id 			= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		echo "3";		
		exit();
	}
	
}

if ($action=="trim_details")
{ //Zone List Registrartion and Update Here
   if  ($isupdate==""){//Insert Here
		
		$id_field_name = "id";
		$table_name = "lib_trim";
		$id= return_next_id($id_field_name,$table_name);
		//echo "$id";
		//exit(();
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO lib_trim (
							id,
							item_category,
							item_name,
							trim_type,
							order_uom,
							trim_uom,
							remark,
							conversion_factor,
							fancy_item,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							STATUS,
							is_deleted
						) VALUES (
							'$id',
							'$cbo_item_category',
							'$txt_item_name',
							'$cbo_trim_type',
							'$cbo_order_uom',
							'$cbo_trim_uom',
							 '$txt_remark',
							 '$txt_conversion_factor',
							 '$cbo_fancy_item',
							'$inserted_by',
							'$insert_date',
							'$updated_by',
							'$update_date',
							'$STATUS',
							'$is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		echo "2";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		
		 $sql = "UPDATE lib_trim
				SET
					item_name= 	'$txt_item_name',
					trim_type= 	'$cbo_trim_type',
					order_uom=	'$cbo_order_uom',
					trim_uom=	'$cbo_trim_uom',
					remark=		'$txt_remark',
					item_category = '$cbo_item_category',				
					updated_by 			= '$updated_by',
					update_date 		= '$update_date',
					conversion_factor   ='$txt_conversion_factor',
					fancy_item        ='$cbo_fancy_item',
					STATUS				= '$STATUS',
					is_deleted			= '$is_deleted'
					WHERE id 			= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		echo "3";		
		exit();
	}
	
}

//team_info List Registrartion and Update Here
if ($action=="team_info")
{ 

   if  ($isupdate==""){//Insert Here
		
		$unique_field_name = "team_name";
		$table_name = "lib_marketing_team";
		$query = "team_name='$txt_team_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "lib_marketing_team";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	

		$sql = "INSERT INTO lib_marketing_team (
							id,
							team_name,
							team_leader_name,
							team_leader_desig,
							team_status,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'$txt_team_name',
							'$txt_team_leader_name',
							'$txt_team_leader_desig',
							'$cbo_team_status',
							'$inserted_by',
							'$insert_date',
							'$updated_by',
							'$update_date',
							'1',
							'$is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$id_field_name = "id";
		$table_name = "lib_marketing_team_member_info";
		$id_m= return_next_id($id_field_name,$table_name);
		
		$sql = "INSERT INTO lib_marketing_team_member_info (
							id,
							designation,
							team_member_name,
							team_member_status,
							team_id,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id_m',
							'$txt_team_leader_desig',
							'$txt_team_leader_name',
							'$cbo_team_status',
							'$id',
							'$inserted_by',
							'$insert_date',
							'$updated_by',
							'$update_date',
							'1',
							'$is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		echo "2_"."$id";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		 
		$sql ="update lib_marketing_team set team_name='$txt_team_name', team_leader_name='$txt_team_leader_name', team_leader_desig='$txt_team_leader_desig', team_status='$cbo_team_status' where id='$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		echo "3_"."$id";		
		exit();
	}
	
}

if ($action=="team_info_dtls")
{ 
	if  ($txt_mst_id!="")//Insert Here
	{
	   if  ($save_up_dtls==""){//Insert Here
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
	
			$id_field_name = "id";
			$table_name = "lib_marketing_team_member_info";
			$id= return_next_id($id_field_name,$table_name);
			
			$sql = "INSERT INTO lib_marketing_team_member_info (
								id,
								designation,
								team_member_name,
								team_member_status,
								team_id,
								inserted_by,
								insert_date,
								updated_by,
								update_date,
								status_active,
								is_deleted
							) VALUES (
								'$id',
								'$txt_member_designation',
								'$txt_member_name',
								'$cbo_menber_status',
								'$txt_mst_id',
								'$inserted_by',
								'$insert_date',
								'$updated_by',
								'$update_date',
								'1',
								'$is_deleted'
							)";
			mysql_query( $sql ) or die (mysql_error());	
			echo "2_"."$txt_mst_id";
			exit();
		} 
		else  // Update
		{
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			
			$sql ="update lib_marketing_team_member_info set team_member_name='$txt_member_name', designation='$txt_member_designation', team_member_status='$cbo_menber_status' where id='$save_up_dtls'";
			mysql_query( $sql ) or die (mysql_error());
			echo "3";		
			exit();
		}
	}// txt_mst_id close
}


//Standard CM  Registrartion and Update Here
if ($action=="standard_cm_entry")
{ 

   if  ($isupdate==""){//Insert Here
		
		$unique_field_name = "company_name";
		$table_name = "lib_standard_cm_entry";
		$query = "company_name='$cbo_company_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "3";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "lib_standard_cm_entry";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	

		$sql = "INSERT INTO lib_standard_cm_entry (
							id,
							company_name,
							applying_period_date,
							applying_period_to_date,
							bep_cm,
							asking_profit,
							asking_cm,
							status_active,
							is_deleted,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							is_locked
						) VALUES (
							'$id',
							'$cbo_company_name',
							'$txt_applying_period_date',
							'$txt_applying_period_to_date',
							'$txt_bep_cm',
							'$txt_asking_profit',
							'$txt_asking_cm',
							'$cbo_status',
							'$cbo_is_deleted',
							'$inserted_by',
							'$insert_date',
							'$updated_by',
							'$update_date',
							'$is_locked'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		
		echo "2_"."$id";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		 
		$sql ="update lib_standard_cm_entry set company_name='$cbo_company_name', applying_period_date='$txt_applying_period_date', applying_period_to_date='$txt_applying_period_to_date', bep_cm='$txt_bep_cm', asking_profit='$txt_asking_profit', asking_cm='$txt_asking_cm', status_active='$cbo_status' where id='$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		echo "3_"."$id";	
		//echo "<pre>";
		//print_r($sql);	
		exit();
	}
	
}



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}
?>