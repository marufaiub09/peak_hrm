<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 31-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');
include('../../../includes/array_function.php');

//company_details
$sql = "SELECT * FROM lib_company WHERE status_active=1 and is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Duty Roster Policy</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
	<script src="includes/ajax_submit_policy.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
    
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    
     <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
    <script type="text/javascript" src="../../../js/modal.js"></script>


<script language="javascript">
	var save_perm = <? echo $permission[0]; ?>;
	var edit_perm = <? echo $permission[1]; ?>;
	var delete_perm = <? echo $permission[2]; ?>;
	var approve_perm = <? echo $permission[3]; ?>;

function reset_button()
{
	document.getElementById('save_up').value="";
}
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

function roster_rule_search(page_link,title)
{
	var com_name=document.getElementById('cbo_company_name').value;
	if(com_name==0)
	{
		if($('#cbo_company_name').val()=="0"){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
		}
	}
	
	else
	{		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link+'?com_name='+com_name, title, 'width=500px,height=300px,center=1,resize=0,scrolling=0','../../');
				
			emailwindow.onclose=function()
			{
				
				var thechange=this.contentDoc.getElementById("txt_selected_change")//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("txt_selected_roster_rule") //Access form field with id="emailfield"
				var rosterid=this.contentDoc.getElementById("roster_rule_id");
				
				document.getElementById('txt_roster_rule_name').value=theemail.value;
				document.getElementById('cbo_change_on_weekend').value=thechange.value;
				document.getElementById('txt_roster_rule_id').value=rosterid.value;
				//alert(thechange.value);
				showResult_search(document.getElementById('cbo_company_name').value,rosterid.value,'duty_roster_policy','duty_roster');
			}
	}
}

</script>

</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:900px;">		
        <div>
    	<div class="form_caption">
		Duty Roster Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
		</div>
		<!-- Start Field Set -->
		<fieldset style="width:660px ">
			
			<!-- Start Form -->
			<form name="duty_roster_policy" id="duty_roster_policy" method="" autocomplete="off" action="javascript:fnc_duty_roster_policy(save_perm,edit_perm,delete_perm,approve_perm)">
				<fieldset>
					<div style="width:600px; overflow:auto" >
						<table width="100%" border="0">
                            <tr>
                                <td width="100px" >
                                    Company
                                </td>
                                <td width="120px"> 
                                 <select name="cbo_company_name" id="cbo_company_name" class="combo_boxes" style="width:150px">
                                 <option value='0'>Select</option>
                                 <?php
                                 while($row=mysql_fetch_assoc($result))
                                 {?>
                                  <option value="<?php echo $row['id']; ?>"><?php echo mysql_real_escape_string($row['company_name']); ?></option>
                                 <?php } ?>
                                 </select>
                                </td>
							</tr>
                            <tr>
								<td width="120px">Roster Rule Name</td>
								<td width="120px">
									<input type="text" name="txt_roster_rule_name" id="txt_roster_rule_name" placeholder="Double click for search" class="text_boxes" style="width:140px" onDblClick="roster_rule_search('search_roster_rule.php','Roster Rule Info');" />
									<input type="hidden" name="txt_roster_rule_id" id="txt_roster_rule_id" />
                                </td>
                              </tr>
							  <tr>
                                    <td width="120px">Current Shift Name</td>
                                    <td width="120px">
                                    	<select name="cbo_shift_name" id="cbo_shift_name" class="combo_boxes" style="width:150px" >
                                            <option value='0'>Select</option>
                                             <?php
                                             $sql = "SELECT * FROM lib_policy_shift WHERE status_active=1 and is_deleted = 0 and shift_type=1 ORDER BY shift_name ASC";
                                             $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );	
                                             while($row=mysql_fetch_assoc($result))
                                             {?>
                                              <option value="<?php echo $row['id']; ?>"><?php echo mysql_real_escape_string($row['shift_name']).", Shift Start ".$row['shift_start']." - Shift End ".$row['shift_end']; ?></option>
                                             <?php } ?>
										 </select>
                                    </td>
                                    <td align="center">Change after Weekend</td>
                                    <td width="120px">
                                    	<select name="cbo_change_on_weekend" id="cbo_change_on_weekend" class="combo_boxes" style="width:100px" >
                                            <option value='0'>No</option>
                                            <option value='1'>Yes</option>
										 </select>
                                    </td>
                               </tr>
                               <tr>
                                    <td width="100px">Shift Duartion</td>
                                    <td width="120px">
                                       <input type="text" name="txt_shift_days" id="txt_shift_days" class="text_boxes" style="width:140px" onKeyPress="return numbersonly(this,event)"/>
                                    </td>
                                    <td align="center">Change on Weekend</td>
                                    <td width="120px">
                                    	<select name="cbo_new_shift_on_weekend" id="cbo_new_shift_on_weekend" class="combo_boxes" style="width:100px" >
                                            <option value='0'>No</option>
                                            <option value='1'>Yes</option>
										 </select>
                                    </td>
                               </tr>
                               <tr>
                                    <td width="100px">Next Shift Name</td>
                                    <td width="120px">
                                        <select name="cbo_next_shift" id="cbo_next_shift" class="combo_boxes" style="width:150px" >
                                         <option value='0'>Select</option>
                                             <?php
                                             $sql = "SELECT * FROM lib_policy_shift WHERE status_active=1 and is_deleted = 0 and shift_type=1 ORDER BY shift_name ASC";
                                             $result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );	
                                             while($row=mysql_fetch_assoc($result))
                                             {?>
                                              <option value="<?php echo $row['id']; ?>"><?php echo mysql_real_escape_string($row['shift_name']).", Shift Start ".$row['shift_start']." - Shift End ".$row['shift_end']; ?></option>
                                             <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                	<td height="15px"><input type="hidden" name="save_up" id="save_up" ></td>
                                <tr>
                                </tr>
                                <tr>
                                    <td colspan="4">&nbsp;</td>
                            	</tr>
                            	<tr>
                                    <td colspan="4" align="center" class="button_container">
                                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton" onClick="reset_button()"/>	
                                    </td>				
                            	</tr>
						</table>
					</div>
				</fieldset>
				<div style="height:10px;"></div>
				<fieldset>
					<div style="width:660px;" >
						<table cellspacing="1" width="100%">
                            <tr>
                                <td>
                                    <div id="duty_roster">
                                    </div>
                                </td>
                            </tr>		
						</table>
					</div>
				</fieldset>
				
			</form>
			<!-- End Form -->
		</fieldset>
		<!-- End Field Set -->
	</div>
</body>
</html>


