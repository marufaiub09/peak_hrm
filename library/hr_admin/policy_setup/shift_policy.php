<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_policy.js" type="text/javascript"></script>
	<script src="../../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
	
	<script type="text/javascript" charset="utf-8">
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	
		var shift_policy = new Array();
		<?php 
		foreach( $shift_policy AS $shift_id => $shift ) {
			echo "shift_policy[$shift_id] = new Array();\n";
			foreach( $shift AS $key => $value ) {
				echo "shift_policy[$shift_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {	
			populate_data();
			
			$('#shift_id').change(function(){
				if( $(this).val() == 0 ) reset_form();
				else show_shift( $(this).val() );
			});
		
			$('.timepicker').timepicker({
				H: true
			});
			
			//$("input.numbers").numbers();
		});
		
		function show_shift( shift_id){	
			$('#shift_id').val( shift_id );
			$('#shift_name').val( shift_policy[shift_id]['shift_name'] );
			$('#shift_type').val( shift_policy[shift_id]['shift_type'] );
			$('#cross_date').val( shift_policy[shift_id]['cross_date'] );
			$('#shift_start').val( shift_policy[shift_id]['shift_start'] );
			$('#shift_end').val( shift_policy[shift_id]['shift_end'] );
			$('#grace_minutes').val( shift_policy[shift_id]['grace_minutes'] );
			$('#exit_buffer_minutes').val( shift_policy[shift_id]['exit_buffer_minutes'] );
			$('#early_out_start').val( shift_policy[shift_id]['early_out_start'] );
			$('#entry_restriction_start').val( shift_policy[shift_id]['entry_restriction_start'] );
			$('#first_break_start').val( shift_policy[shift_id]['first_break_start'] );
			$('#first_break_end').val( shift_policy[shift_id]['first_break_end'] );
			$('#second_break_start').val( shift_policy[shift_id]['second_break_start'] );
			$('#second_break_end').val( shift_policy[shift_id]['second_break_end'] );
			$('#lunch_time_start').val( shift_policy[shift_id]['lunch_time_start'] );
			$('#lunch_time_end').val( shift_policy[shift_id]['lunch_time_end'] );
			$('#short_shift_name').val( shift_policy[shift_id]['short_shift_name'] );
			$('#status_active').val( shift_policy[shift_id]['status_active'] );
		}
		
		function reset_form() {
			$('#shift_id').val( 0 );
			$('#shift_name').val( '' );
			$('#shift_type').val( 0 );
			$('#cross_date').val( 0 );
			$('#shift_start').val( '' );
			$('#shift_end').val( '' );
			$('#grace_minutes').val( '' );
			$('#exit_buffer_minutes').val( '' );
			$('#early_out_start').val( '' );
			$('#entry_restriction_start').val( '' );
			$('#first_break_start').val( '' );
			$('#first_break_end').val( '' );
			$('#second_break_start').val( '' );
			$('#second_break_end').val( '' );
			$('#lunch_time_start').val( '' );
			$('#lunch_time_end').val( '' );
			$('#short_shift_name').val( '' );
			$('#status_active').val( 1 );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'form=shift_policy',
				success: function(data) {
					$('#data_panel').html( data );
					$('#shifts').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
		
		
				//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
	</script>
	<style>
		#shift_policy_form input[type="text"] { width:125px; }
		#shift_policy_form select { width:137px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:1070px;">     	
    
    	<div class="form_caption">
		Shift Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	
	<form id="shift_policy_form" action="javascript:fnc_shift_policy(save_perm,edit_perm,delete_perm,approve_perm)" method="post">
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						&nbsp;Shift:
						<select name="shift_id" id="shift_id" class="combo_boxes" style="width:auto; margin-left:67px;">
							<option value="0">-- Select shift to edit --</option>
							<?php foreach( $shift_policy AS $shift ) { ?>
							<option value="<?php echo $shift['id']; ?>"><?php echo $shift['shift_name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97%; margin-top:10px;">
			<legend>Shift Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td>Shift Name:</td>
								<td><input type="text" name="shift_name" id="shift_name" class="text_boxes" value="" /></td>
                                <td>Cross Date:</td>
								<td>
                                	<select name="cross_date" id="cross_date" class="combo_boxes">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
                                </td>
                                <td>Shift Start:</td>
								<td><input type="text" name="shift_start" id="shift_start" value="" class="timepicker text_boxes" /></td>
								<td>Shift End:</td>
								<td><input type="text" name="shift_end" id="shift_end" value="" class="timepicker text_boxes" /></td>
							</tr>
							<tr>
								<td>Grace Minutes:</td>
								<td><input type="text" name="grace_minutes" id="grace_minutes" value="" placeholder="Minutes" class="numbers text_boxes" onkeypress="return numbersonly(this,event)" /></td>
								<td>Exit Buffer Minutes:</td>
								<td><input type="text" name="exit_buffer_minutes" id="exit_buffer_minutes" value="" placeholder="Minutes" class="numbers text_boxes" onkeypress="return numbersonly(this,event)" /></td>
                                <td>Lunch time start:</td>
								<td><input type="text" name="lunch_time_start" id="lunch_time_start" value="" class="timepicker text_boxes" /></td>
								<td>Lunch time end:</td>
								<td><input type="text" name="lunch_time_end" id="lunch_time_end" value="" class="timepicker text_boxes" /></td>
							</tr>
							<tr>
								<td>Early Out Start:</td>
								<td><input type="text" name="early_out_start" id="early_out_start" value="" placeholder="Minutes" class="numbers text_boxes" onkeypress="return numbersonly(this,event)" /></td>
								<td>Entry Restriction Starts:</td>
								<td><input type="text" name="entry_restriction_start" id="entry_restriction_start" value="" placeholder="Minutes" class="numbers text_boxes" onkeypress="return numbersonly(this,event)" /></td>
                                <td>Shift Type:</td>
								<td>
									<select name="shift_type" id="shift_type" class="combo_boxes">
										<option value="0">Fixed</option>
										<option value="1">Roster</option>
									</select>
								</td>
								<td>Status:</td>
								<td>
									<select name="status_active" id="status_active" class="combo_boxes">
										<?php
											foreach($status_active as $key=>$value):
											?>
											<option value=<? echo "$key";
											if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
											<?		
											endforeach;
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Tiffin Break Start:</td>
								<td><input type="text" name="first_break_start" id="first_break_start" value="" class="timepicker text_boxes" /></td>
								<td>Tiffin Break End:</td>
								<td><input type="text" name="first_break_end" id="first_break_end" value="" class="timepicker text_boxes" /></td>
                                <td>Dinner Break Start:</td>
								<td><input type="text" name="second_break_start" id="second_break_start" value="" class="timepicker text_boxes" /></td>
								<td>Dinner Break End:</td>
								<td><input type="text" name="second_break_end" id="second_break_end" value="" class="timepicker text_boxes" /></td>
							</tr>
                            <tr>
								<td>Shift Name(Short):</td>
								<td><input type="text" name="short_shift_name" id="short_shift_name" value="" class="text_boxes" /></td>
								<td></td>
								<td></td>
                                <td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td colspan="2">&nbsp;</td>				
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-top:10px;" class="button_container">
						<input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" id="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div>
    </div>
</body>
</html>