//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();





//Tax Slab

function fnc_save_update_tax_slab(){

		var  slab_number=document.getElementById('slab_number').value;
		var  slab_name=document.getElementById('slab_name').value;
		var  slab_amount=document.getElementById('slab_amount').value;
		var  slab_tax=document.getElementById('slab_tax').value;
		var  save_up=document.getElementById('save_up').value;
		//alert(save_up);
		//alert(slab_number);
		
		
		
		 var error = false;
		
		
		if( $('#slab_number').val()==0){					
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#slab_number').focus();
			$(this).html('Slab Number Can Not Be Empty.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
			});		
		} 
		else if( $('#slab_name').val()==''){					
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#slab_name').focus();
			$(this).html('Slab Name Can Not Be Empty.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
			});		
		} 
		else if( $('#slab_amount').val()==''){					
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#slab_amount').focus();
			$(this).html('Slab Amount Can Not Be Empty.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
			});		
		} 
		
		
else{
	
	 http.open('get','tax_calculation/includes/save_update_tax.php?action=slab_insert&isupdate='+save_up
				
				 +'&slab_number='+slab_number
				 +'&slab_name='+slab_name
				 +'&slab_amount='+slab_amount
				 +'&slab_tax='+slab_tax
				  );
	
	http.onreadystatechange=response_emp_tax_slab;
	http.send(null);

	
	
	}

}

function response_emp_tax_slab() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	

		 if( response[0] == 9) 
		 {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(800,1);
			
				document.getElementById('slab_number').value="";
				document.getElementById('slab_name').value="";
				document.getElementById('slab_amount').value="";
				document.getElementById('slab_tax').value="";
				document.getElementById('slab_number').focus();
				
				showResult_search_slab(response[0],'list_tax_slab','slab_list_view');
				$(this).fadeOut(3000);
			});
			
	
		}
		else if(response[0]== 10){
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(800,1);
				document.getElementById('slab_number').value="";
				document.getElementById('slab_name').value="";
				document.getElementById('slab_amount').value="";
				document.getElementById('slab_tax').value="";
				document.getElementById('slab_number').focus();
				//$('#slab_number').val()="";
				showResult_search_slab(response[1],'list_tax_slab','slab_list_view');
				$(this).fadeOut(3000);
				//$('#slab_number').focus();
				
        	 	
   				

				
			});
		}
	}
	
}







//Tax rebate Information(save/update)
function fnc_save_update_tax_rebate(){
		
		var  payroll_head=document.getElementById('payroll_head3').value;
		var  fixed_amount_rebate=document.getElementById('fixed_amount_rebate').value;
		var  taxable_income=document.getElementById('taxable_income').value;
		var  whichever_is=document.getElementById('whichever_is_rebate').value;
		var  tax_rebate_amount=document.getElementById('tax_rebate_amount').value;
		var  system_id=document.getElementById('system_id').value;
		var  tax_rebate_hidden=document.getElementById('tax_rebate_hidden').value;
		
		//alert(whichever_is);
		
		 var error = false;
		 
	 if( $('#payroll_head3').val()==0){					
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#payroll_head3').focus();
			$(this).html('Please Select The  Payroll Head.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
			});		
		} 
	
else{
 http.open('get','tax_calculation/includes/save_update_tax.php?action=rebate_insert&system_id='+system_id
 			
			  +'&payroll_head='+payroll_head
			  +'&fixed_amount_rebate='+fixed_amount_rebate
			  +'&taxable_income='+taxable_income
			  +'&whichever_is='+whichever_is
			  +'&tax_rebate_amount='+tax_rebate_amount
			   +'&tax_rebate_hidden='+tax_rebate_hidden
			  
			  );
	
	http.onreadystatechange=response_emp_tax_rebate;
	http.send(null);

}
}
function response_emp_tax_rebate() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response);

	 if( response[0] == 7) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				showResult_search(response[0],'list_tax_rebate','tax_list_view_rebate');
				$(this).fadeOut(5000);
				
			
			});
			//$('#system_id').val( response[1] );
		}
		else if(response[0]==8){
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				showResult_search(response[1],'list_tax_rebate','tax_list_view_rebate');
				$(this).fadeOut(5000);
				//$('#system_id').val( response[1] );
			//document.getElementById('save_up').value="";
			});
		}



	}
}



//Tax Examption Information(save/update)
function  fnc_save_update_examption(){
		var  payroll_head=document.getElementById('payroll_head2').value;
		//var  payroll_head=document.getElementById('payroll_head2').value;
		var  examption_ratio=document.getElementById('examption_ratio').value;
		var  fixed_amount=document.getElementById('fixed_amount').value;
		var  partial_examption=document.getElementById('partial_examption').value;
		var  base_head=document.getElementById('base_head').value;
		var  whichever_is=document.getElementById('whichever_is').value;
		var  status=document.getElementById('status2').value;
		var  comments=document.getElementById('comments').value;
		//var  save_up=document.getElementById('save_up').value;
		var  examption_id=document.getElementById('examption_id').value;
		var  system_id=document.getElementById('system_id').value;
		//alert(whichever_is);

	 var error = false;
		 
	 if( $('#payroll_head2').val()==0){					
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#payroll_head2').focus();
			$(this).html('Please Select The  Payroll Head.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
			});		
		} 
	
		
		 else if( $('#fixed_amount').val()==''){					
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#fixed_amount').focus();
			$(this).html('Please Select The Amount.').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
			});		
		 
 }else{
 http.open('get','tax_calculation/includes/save_update_tax.php?action=examption_insert&system_id='+system_id
 			
			  +'&payroll_head='+payroll_head
			  +'&examption_ratio='+examption_ratio
			  +'&fixed_amount='+fixed_amount
			  +'&partial_examption='+partial_examption
			  +'&base_head='+base_head
			  +'&whichever_is='+whichever_is
			  +'&status='+status
			  +'&examption_id='+examption_id
			  );
	
	http.onreadystatechange=response_emp_tax_examption;
	http.send(null);

}
}
function response_emp_tax_examption() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response);

	 if( response[0] == 5 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				showResult_search(response[0],'list_tax_examption','tax_list_view_examption');
				$(this).fadeOut(5000);
				
			
			});
			//$('#system_id').val( response[1] );
		}
		else if(response[0]==6){
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				showResult_search(response[1],'list_tax_examption','tax_list_view_examption');
				$(this).fadeOut(5000);
				
			//document.getElementById('save_up').value="";
			});
		}



	}
}

//save update for tax master

function fnc_save_update_basic()
{
	//basic_id
	var  system_id=document.getElementById('system_id').value;
	var  company_id=document.getElementById('company_id').value;
	var  tax_year=document.getElementById('tax_year').value;
	
	var  rule_name=document.getElementById('rule_name').value;
	var  status=document.getElementById('status').value;
	var  comments=document.getElementById('comments').value;
	var  basic_id=document.getElementById('basic_id').value;
	var  taxable=document.getElementById('taxable').value;
	var  payroll_head=document.getElementById('payroll_head').value;
	//var  save_up=document.getElementById('save_up').value;
	
	//alert(tax_year);
	 $("#messagebox").removeClass().addClass('messagebox').text('').fadeIn(500);
	 /* var error = false;
	
	 if( $('#system_id').val()==''){						
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
		$('#dob').focus();
		$(this).html('Please Double click  to get system id.').addClass('messageboxerror').fadeTo(900,1);
		$(this).fadeOut(5000);
		});		
	} 
	
	
		
else{*/

 http.open('get','tax_calculation/includes/save_update_tax.php?action=tax_insert&system_id='+system_id
 			
			  +'&company_id='+company_id  
			  +'&tax_year='+tax_year
			  +'&rule_name='+rule_name
			  +'&status='+status
			  +'&comments='+comments
			  +'&basic_id='+basic_id
			  +'&taxable='+taxable
			  +'&payroll_head='+payroll_head
			  
			  
			  );
	
	http.onreadystatechange=response_emp_tax;
	http.send(null);

}
//}
function response_emp_tax() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response[1]);

	 if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				showResult_search(response[1],'list_tax_basic','tax_list_view');
				//showResult_search(response[2],'list_tax_basic','tax_list_view');
				$(this).fadeOut(5000);
				//$('#system_id').val( response[1] );
			
			});
			$('#system_id').val( response[1] );
		}
		
	
	else if(response[0]==3){
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data update Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				showResult_search(response[1],'list_tax_basic','tax_list_view');
				$(this).fadeOut(5000);
				//$('#system_id').val( response[1] );
			//document.getElementById('save_up').value="";
			});
		}
		else if(response[0]==6){
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Insert basic Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
				showResult_search(response[2],'list_tax_basic','tax_list_view');
				$(this).fadeOut(5000);
				//$('#system_id').val( response[1] );
			//document.getElementById('save_up').value="";
			});
		}
	}


}





