<?php
session_start();
date_default_timezone_set('UTC');
include('../../../../../includes/common.php');
include('../../../../../includes/array_function.php');
/* Replace the data in these two lines with data for your db connection */
$type=$_GET["type"];

if(isset($_GET['getClientId']))
{  
	
	if ($type==1) // Employee for Disciplinary Info
	{
		//Designation array
			$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY level, custom_designation ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$designation_chart = array();
			while( $row = mysql_fetch_assoc( $result ) ) 
			{
				$designation_chart[$row['id']] = $row['custom_designation'];
			}
			
		//Company array
			$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$company_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$company_details[$row['id']] = $row['company_name'];
			}
		//Department array
			$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$department_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$department_details[$row['id']] = $row['department_name'];
			}
		//Diviion
			$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$division_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$division_details[$row['id']] = $row['division_name'];
			}
		//Location
			$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$location_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$location_details[$row['id']] =$row['location_name'];
			}
		//Section
		$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$section_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$section_details[$row['id']] = $row['section_name'];
			}
		//Sub Section
			$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
			$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			
			$subsection_details = array();
			while( $row = mysql_fetch_assoc( $result ) ) {
				$subsection_details[$row['id']] = $row['subsection_name'];
			}
		
		
	$res = mysql_query("SELECT emp.*, CONCAT(emp.first_name, ' ', emp.middle_name, ' ', emp.last_name) AS name,emp.designation_id,
						job.company_id, job.location_id, job.division_id, job.department_id, job.section_id, job.subsection_id
					FROM hrm_employee AS emp
					LEFT JOIN hrm_employee_job AS job ON job.emp_code = emp.emp_code
					WHERE emp.emp_code = '".$_GET['getClientId']."' and emp.is_deleted=0 and emp.status_active=1 order by emp.emp_code")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_emp_code.value = '".$inf["emp_code"]."';\n";    
			echo "formObj.txt_emp_name.value = '".mysql_real_escape_string($inf["name"])."';\n";    
			echo "formObj.txt_designation.value = '".$designation_chart[$inf["designation_id"]]."';\n";    
			echo "formObj.txt_company.value = '".$company_details[$inf["company_id"]]."';\n"; 
			echo "formObj.txt_division.value = '".$division_details[$inf["division_id"]]."';\n"; 
			echo "formObj.txt_department.value = '".$department_details[$inf["department_id"]]."';\n"; 
			echo "formObj.txt_section.value = '".$section_details[$inf["section_id"]]."';\n"; 
			//echo "formObj.txt_sub_section.value = '".$section_details[$inf["subsection_id"]]."';\n";  
			//echo "formObj.save_up_po.value = '".$inf["id"]."';\n"; 
		} 
	}
	
	if ($type==2) // Disc for Disciplinary Info
	{
		$res = mysql_query("SELECT * from hrm_disciplinary_info_mst
					WHERE emp_code = '".$_GET['getClientId']."' and withdrawn_date='0000-00-00' order by id desc")  or die(mysql_error());
		
		if($inf = mysql_fetch_array($res))
		{
			echo "formObj.txt_occurrence_date.value = '".$inf["occurrence_date"]."';\n";    
			echo "formObj.txt_action_date.value = '".$inf["action_date"]."';\n";    
			echo "formObj.txt_occurrence_details.value = '".mysql_real_escape_string($inf["occurrence_details"])."';\n";    
			echo "formObj.txt_investigating_members.value = '".mysql_real_escape_string($inf["investigating_members"])."';\n"; 
			echo "formObj.txt_investigation.value = '".mysql_real_escape_string($inf["investigation"])."';\n"; 
			echo "formObj.cbo_action_taken.value = '".mysql_real_escape_string($inf["action_taken"])."';\n"; 
			echo "formObj.txt_withdawn_date.value = '".$inf["withdrawn_date"]."';\n"; 
			echo "formObj.save_up_disc.value = '".$inf["id"]."';\n";  
			//echo "formObj.save_up_po.value = '".$inf["id"]."';\n"; 
		} 
		else{
			echo "formObj.txt_occurrence_date.value = '';\n";    
			echo "formObj.txt_action_date.value = '';\n";    
			echo "formObj.txt_occurrence_details.value = '';\n";    
			echo "formObj.txt_investigating_members.value = '';\n"; 
			echo "formObj.txt_investigation.value = '';\n"; 
			echo "formObj.cbo_action_taken.value = '';\n"; 
			echo "formObj.txt_withdawn_date.value = '';\n"; 
			echo "formObj.save_up_disc.value = '';\n";  
		}
		
	}
	
		if ($type==5){														
													
							
							//$res=mysql_query("SELECT a.*,b.payroll_head,b.taxable, b.tax_mst_id, b.id as basic_id  FROM  lib_tax_master a,lib_tax_basic b WHERE  a.id=b.tax_mst_id and  a.id= '".$_GET['getClientId']."' order by a.id") or die(mysql_error()); 	
							
							$res=mysql_query("SELECT a.*,b.payroll_head,b.taxable, b.tax_mst_id,b.id as basic_id   FROM  lib_tax_master a,lib_tax_basic b WHERE  a.id=b.tax_mst_id and  b.id= '".$_GET['getClientId']."' order by a.id") or die(mysql_error()); 		
							
							
							if($inf = mysql_fetch_array($res)){
							//echo "formObj.company_id.value = '".mysql_real_escape_string($inf["company_id"])."';\n";
							echo "document.getElementById('company_id').value = '".mysql_real_escape_string($inf["company_id"])."';\n"; 
							echo "formObj.tax_year.value = '".convert_to_mysql_date($inf["tax_year"])."';\n"; 
							echo "formObj.rule_name.value = '".$inf["rule_name"]."';\n";
							echo "document.getElementById('status').value = '".$inf["status_active"]."';\n"; 
							echo "document.getElementById('comments').value = '".$inf["comments"]."';\n";
							echo "document.getElementById('payroll_head').value = '".$inf["payroll_head"]."';\n";
							echo "document.getElementById('taxable').value = '".$inf["taxable"]."';\n";
							//echo "formObj.taxable.value = '".$inf["taxable"]."';\n";
							echo "document.getElementById('system_id').value = '".$inf["id"]."';\n";
							echo "document.getElementById('basic_id').value = '".$inf["basic_id"]."';\n";													
							//echo "formObj.save_up.value = '".$inf["id"]."';\n"; 
							} 
											
		
		
	}
	
	if ($type==6){														
													
			
				//$res=mysql_query("SELECT a.*,b.company_id,b.id  as examption_id  FROM  lib_tax_examption a,lib_tax_master b WHERE  a.id = '".$_GET['getClientId']."' order by a.id") or die(mysql_error());
				$res=mysql_query("SELECT a.*,b.company_id,a.id  as examption_id  FROM  lib_tax_examption a,lib_tax_master b WHERE  a.id = '".$_GET['getClientId']."' order by a.id") or die(mysql_error());
							
						
						if($inf = mysql_fetch_array($res)){
							
							echo "document.getElementById('payroll_head2').value = '".$inf["payroll_head"]."';\n";
							echo "document.getElementById('examption_ratio').value = '".$inf["examption_ratio"]."';\n";
							echo "document.getElementById('fixed_amount').value = '".$inf["fixed_amount"]."';\n";
							echo "document.getElementById('partial_examption').value = '".$inf["partial_examption"]."';\n";	
							echo "document.getElementById('base_head').value = '".$inf["base_head"]."';\n";		
							echo "document.getElementById('whichever_is').value = '".$inf["which_ever_is"]."';\n";
							echo "document.getElementById('status2').value = '".$inf["status_active"]."';\n";
							echo "document.getElementById('examption_id').value = '".$inf["examption_id"]."';\n"; 
							 										
							//echo "formObj.save_up.value = '".$inf["id"]."';\n"; 
							} 
											
		
		
	}
	
	if ($type==7){														
													
							
				
				$res = mysql_query("select * from   lib_tax_rebate  where id='".$_GET['getClientId']."'") or die(mysql_error());			
							if($inf = mysql_fetch_array($res)){
							
							echo "document.getElementById('payroll_head3').value = '".$inf["payroll_head"]."';\n";
							echo "document.getElementById('fixed_amount_rebate').value = '".$inf["fixed_amount"]."';\n";
							echo "document.getElementById('taxable_income').value = '".$inf["percentage_of_taxable_income"]."';\n";
							echo "document.getElementById('whichever_is').value = '".$inf["which_ever_is"]."';\n";	
							echo "document.getElementById('tax_rebate_amount').value = '".$inf["tax_rebate"]."';\n";		
							echo "document.getElementById('tax_rebate_hidden').value = '".$inf["id"]."';\n"; 
							 										
							//echo "formObj.save_up.value = '".$inf["id"]."';\n"; 
							} 
											
		
		
	}
	
	
	
	
}	

	
/*function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}*/
?> 