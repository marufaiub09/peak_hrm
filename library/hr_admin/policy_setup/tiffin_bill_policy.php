<?php
/*************************************
|	Tiffin bill Policy
|	Developed by : Md. Nuruzzaman
*************************************/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="../../../includes/update_status.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
        <script src="includes/functions.js" type="text/javascript" ></script>
        <script type="text/javascript" charset="utf-8">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
        
            $(document).ready(function() {	
                populate_data();
                
                $('.timepicker').timepicker({
                    H: true
                });
            });
            // When load the page then this function will be execute
            function populate_data() {
                $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                $.ajax({
                    type: "POST",
                    url: "../hr_admin_data.php",
                    data: 'form=tiffin_bill_policy',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#tbl_tiffin_bill_policy').dataTable({
                            "bRetrieve": true,
                            "bDestroy": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                         	 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
                            //"aaSorting": [[ 0, "asc" ]],*/
                          	"oLanguage": { "sSearch": "Search all columns:" },
                            "sScrollX": "100%",
                            "sScrollY": "200px",
                            "sScrollXInner": "100%",
                            "bScrollCollapse": true
                        });
                    }
                });
            }
            // Click on the ADD ROW button this function will be execute
            function add_row()
            {
			    var rowCount=$("#tiffin_bill tbody tr").length;  
                if( $('#bill_amt__'+rowCount).val()=="" ) return;
				
					for(var j=1; j<=rowCount; j++)
					{
						if($('#bill_amt__'+rowCount).val()==$('#bill_amt__'+(rowCount-j)).val()) return;
					}
					
				rowCount++;
                $('#tiffin_bill tbody').append(
                    '<tr>'
                        +'<td><input type="hidden" name="update_id[]" id="update_id_' + rowCount + '" value="" class="text_boxes" style="width:120px; text-align:center;" /></td>'
                        +'<td><input type="text" name="must_stay[]" id="must_stay_' + rowCount + '" value="" class="text_boxes" onKeyPress="return numbersonly(this, event);" style="width:120px; text-align:center;" /></td>'
                        +'<td><input type="text" name="bill_amt[]" id="bill_amt__' + rowCount + '" value="" class="text_boxes" onKeyPress="return numbersonly(this, event);" onblur="check_duplicacy(this.id,this.value)" style="width:120px; text-align:right;" /></td>'
                    +'</tr>'
                );
                $('.timepicker').timepicker({
                    H: true
                });
            }
            // Click on the REMOVE ROW button this function will be execute
            function  remove_row()
            {
                var r=confirm("Are you sure want to REMOVE ROW !!");
				if(r==true)
				{
					var rowCount = $('#tiffin_bill tbody tr').length;
					if(rowCount!=1)
					{
						$("#tiffin_bill tr:last").remove();
					}
				}
            }
            // Click on the REFRESH button this function will be execute
            function reset_form() {
                var r=confirm("Are you sure want to REFRESH fields !!");
				if(r==true)
				{
					$('#tiffin_bill tbody tr').remove();
					$('#policy_name').val( '' );
					$('#late_applicable').val( 0 );
					$('#earning_head').val( 0 );
					add_row();
				}
            }
            // Click on th SAVE button this function will be execute
            function fun_tiffin_policy(save_perm,edit_perm,delete_perm)
            {
                //alert("I am from FORM ACTION");
                var policy_name=document.getElementById('policy_name').value;
                var late_applicable=document.getElementById('late_applicable').value;
				var earning_head=document.getElementById('earning_head').value;
				//alert(earning_head);
                var policy_id=document.getElementById('policy_id_mst').value;
				var db_row_ids_mst=document.getElementById('db_row_ids_mst').value;
				
                var update_id='';
				var must_stay='';
				var bill_amt='';
				
                
                // Validation checking here
                if( policy_name== '' ) 
                {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#policy_name').focus();
                        $(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
                    });
                    return;
                }
				if( earning_head==0 ) 
                {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#earning_head').focus();
                        $(this).html('Select earning head.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
                    });
                    return;
                }
                else 
                {			
                    var rowCount = $('#tiffin_bill tbody tr').length;
                    for (var i=1; i<=rowCount; i++)
                    {
						if ($('#must_stay_'+i).val()==0) $('#must_stay_'+i).val('');
						if ($('#bill_amt__'+i).val()==0) $('#bill_amt__'+i).val('');
						
						if($('#must_stay_'+i).val()!="" && $('#bill_amt__'+i).val()=="" )
						{
							$('#messagebox').fadeTo( 200, 0.1, function() {
								$('#allowance_'+i).focus();
								$(this).html('Bill amount field is blank.').addClass('messageboxerror').fadeTo(900,1);
								$(this).fadeOut(5000);
							});
							return;
						}
						else if($('#must_stay_'+i).val()=="" && $('#bill_amt__'+i).val()!="" )
						{
							$('#messagebox').fadeTo( 200, 0.1, function() {
								$('#must_stay_'+i).focus();
								$(this).html('Must stay field is blank.').addClass('messageboxerror').fadeTo(900,1);
								$(this).fadeOut(5000);
							});
							return;
						}
						else if($('#bill_amt__'+i).val()!="" )
						{
							for(var j=1; j<=rowCount; j++)
							{
								if($('#bill_amt__'+i).val()==$('#bill_amt__'+(i+j)).val())
								{
									$('#messagebox').fadeTo( 200, 0.1, function() {
										$('#bill_amt__'+(i+j)).focus();
										$(this).html('You are entered duplicate value').addClass('messageboxerror').fadeTo(900,1);
										$(this).fadeOut(5000);
									});
									return;
								}
							}
						}
						
                        if(update_id=='') update_id=$('#update_id_'+i).val(); else update_id=update_id+"_"+$('#update_id_'+i).val(); 
                        if(must_stay=='') must_stay=$('#must_stay_'+i).val(); else must_stay=must_stay+"_"+$('#must_stay_'+i).val();
                        if(bill_amt=='') bill_amt=$('#bill_amt__'+i).val(); else bill_amt=bill_amt+"__"+$('#bill_amt__'+i).val();
                        //alert(allowance);
                    }
                    //alert(rowCount);
                    http.open( 'GET', 'includes/save_update_policy.php?action=tiffin_bill_policy&policy_name='+policy_name+
                        '&late_applicable='+late_applicable+
						'&earning_head='+earning_head+
                        '&policy_id='+policy_id+
                        '&update_id='+update_id+
                        '&must_stay='+must_stay+
                        '&bill_amt='+bill_amt+
						'&db_row_ids_mst='+db_row_ids_mst);
                        
                    http.onreadystatechange = response_tiffin_policy;
                    http.send(null);
                }
            }
			// Response comes here
            function response_tiffin_policy() 
            {
                if(http.readyState == 4)
                {
                    //alert (http.responseText);
                    var response = http.responseText;
                    if( response == 1 ) 
                    {
                        $("#messagebox").fadeTo( 200, 0.1, function() 
                        {
                            $(this).html('Tiffin bill policy have been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
                            $(this).fadeOut(5000);
                            //document.getElementById('tiffin_allowance_policy').reset();
                        });
						populate_data();
                    }
                    else if( response == 2 ) 
                    {
                        $("#messagebox").fadeTo( 200, 0.1, function() 
                        {
                            $(this).html('Tiffin bill policy Update successfully .').addClass('messagebox_ok').fadeTo( 900, 1 )
                            $(this).fadeOut(5000);
                            //document.getElementById('tiffin_allowance_policy').reset();
                        });
						populate_data();
                    }
                }
            }
            // Click on the each ROW of list view this function will be execute
            function js_set_value(id,policy_name,late_applicable,earning_head)
            {
                //alert("I am from ROW click");
                document.getElementById('policy_id_mst').value=id;
                document.getElementById('policy_name').value=policy_name;
                document.getElementById('late_applicable').value=late_applicable;
				document.getElementById('earning_head').value=earning_head;
                $.ajax({
                            type: "POST",
                            url: "../hr_admin_data.php",
                            data: 'data=tiffin_bill_policy&policy_id=' + id,
                            success: function( data ) {
                            $('#tiffin_bill tbody tr').remove();
                            $('#tiffin_bill tbody').append(data);
                            document.getElementById('db_row_ids_mst').value=document.getElementById('db_row_ids').value;
                        $('.timepicker').timepicker({
                            H: true
                        });
                    }
                });
            }
			//duplicacy check here
			function check_duplicacy( id, str )
			{
				//alert(id+"**"+str);
				var rowCount = $('#tiffin_bill tbody tr').length;
				var dd=id.split("__");
				for (var i=1; i<=rowCount; i++)
				{
					if( dd[1]!=i )
					{
						if( $('#bill_amt__'+i).val()==str )
						{
							/*$('#bill_amt__'+i).val()==str;*/
							/*$('#'+id).val('');*/
							$('#messagebox').fadeTo( 200, 0.1, function() {
								$('#bill_amt__'+rowCount).focus();
								$(this).html('You are entered duplicate value').addClass('messageboxerror').fadeTo(900,1);
								$(this).fadeOut(5000);
							});
							return false;
						}
					}
				} 
			}
			
        </script>
        <style>
            .header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:1070px;">
            <div style="height:60px;">    	
                <div class="form_caption">Tiffin Bill Policy</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update.$delete;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div>
            </div>
            <form id="tiffin_bill_policy" name="tiffin_bill_policy" action="javascript:fun_tiffin_policy(save_perm,edit_perm,delete_perm)" autocomplete="off" method="post">
                <fieldset style="width:75%;">
                    <legend>Tiffin Bill Policy</legend>
                    <fieldset style="width:65%;">
                        <table width="65%" cellspacing="1" cellpadding="0" border="0" align="center">
                        	<thead>
                            	<tr>
                                    <th class="header" width="200px">Policy Name</th>
                                    <th class="header" width="200px">Late Applicable</th>
                                    <th class="header" width="200px">Earning Head</th>
                                </tr>
                            </thead>
                            <tr>
                                <td>
                                    <input type="text" name="policy_name" id="policy_name" class="text_boxes" style="width:150px;" />
                                    <input type="hidden" id="policy_id_mst" name="policy_id_mst" class="text_boxes" style="width:100px;" />
                                    <input type="hidden" id="db_row_ids_mst" name="db_row_ids_mst" class="text_boxes" style="width:100px;" />
                                </td>
                                <td>
                                    <select id="late_applicable" name="late_applicable" class="combo_boxes" style="width:150px;">
                                        <option value="0">-- select --</option>
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                    </select>	
                                </td>
                                <td>
                                    <select name="earning_head" id="earning_head" class="combo_boxes" style="width:150px;">
                                        <option value="0">-- Select --</option>
                                        <?php foreach( $payroll_heads AS $payroll ) { if( $payroll['type'] == 0 ) { ?>
                                        <option value="<?php echo $payroll['id']; ?>" <?php if( $payroll['system_head'] == 'Tiffin allowance' ) echo "selected"; ?>><?php echo $payroll['custom_head']; ?></option>
                                        <?php } } ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                        <fieldset style="width:65%;">
                            <table width="250px">
                                <tr>
                                    <td>
                                        <table width="250px" border="0" cellpadding="1" cellspacing="1" id="tiffin_bill" align="center">
                                            <thead>
                                            <tr>
                                                <th width="250px">&nbsp;</th>
                                                <th class="header" width="250px">Must Stay</th>
                                                <th class="header" width="250px">Bill Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><input type="hidden" id="policy_id_1" name="policy_id[]" class="text_boxes" style=" width:100px; text-align:center;" /></td>
                                                <td><input type="text" id="must_stay_1" name="must_stay[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" style=" width:120px; text-align:center;" /></td>
                                                <td><input type="text" id="bill_amt__1" name="bill_amt[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" onblur="check_duplicacy(this.id,this.value)" style=" width:120px; text-align:right;" /></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table>
                                            <tr><td><input type="button" name="add" id="add" value="Add Row" class="formbutton" onclick="add_row();" style="width:90px;" /></td></tr>
                                            <tr><td><input type="button" name="remove" id="remove" value="Remove Row" class="formbutton" style="width:90px;" onclick="remove_row();" /></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <table align="center">
                            <tr><td colspan="3">&nbsp; </td></tr>
                            <tr>
                                <td><input type="submit" name="save" id="save" class="formbutton" value="Save" style="width:90px;"/></td>
                                <td><input type="button" name="refresh" id="refresh" class="formbutton" value="Refresh" onclick="reset_form()" style="width:90px;" /></td>
                            </tr>
                        </table>
                </fieldset>
            </form> 
        </div>
        <div id="data_panel" class="demo_jui" align="center" style="width:824px; margin-top:15px; margin-left:122px;"></div>
	</body>
</html>