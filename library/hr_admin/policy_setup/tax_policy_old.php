<?php
/*######################################

	Completed By
	Name : Md.Ekram Hossain
	Date : 28-12-2012
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');


//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}

//print_r($company_details);

$ConsumptionTabIndex=-1;
extract($_POST);
if(!$TabIndexNo) { $TabIndexNo=0; }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Tax Calculation</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    

    <script src="../../../resources/ui.tabs.paging.js" type="text/javascript"></script>
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
  
    <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css">
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen">
    <script src="tax_calculation/includes/functions.js" type="text/javascript"></script>
    
	<script type="text/javascript">
        $(document).ready(function() {
            $('#example').tabs();
            $('#example').tabs('paging', { cycle: true, follow: true } );
            $('#example').tabs('select',<?php echo $TabIndexNo; ?>);
        });
    </script>

</head>
<body>
<div align="center" style="width:900px; position:relative; height:30px; margin-bottom:3px; margin-top:3px">        
	<div class="form_caption">
           Tax Rule
        </div>
        
        
	</div>
    
     <div id="example2" align="center" style="width:900px; font-size:11px;">
     <form name="item_resource_allocation" id="item_resource_allocation" action="" method="post" style="margin-top:25px"> 

     <!-- Start Field Set -->
	<fieldset>
	<legend>HRM  Module</legend>
	<!-- Start Form -->
			 <div id="example" align="center" style="width:900px; margin-top:20px; font-size:11px;">
         	<ul class="tabs">
			<li><a href="#basic_tax" onClick="">Basic</a></li>
			<li><a href="#tax_examption" onClick="">Examptions</a></li>
			<li><a href="#tax_rebates" onClick="">Tax Rebate</a></li>
			
		</ul>
        <div id="basic_tax"> <?php include('tax_calculation/basic_tax.php'); ?></div>
        <div id="tax_examption"> <?php include('tax_calculation/tax_examption.php'); ?></div>
		<div id="tax_rebates"> <?php include('tax_calculation/tax_rebate.php'); ?></div>
		<!--<div id="basic_tax_list_view"></div>-->
    </div>
			
	</fieldset>
    </form>

<fieldset>
	<div id="list_view_taxation"></div>
</fieldset>   
 </div>	  
</div>
</body>
</html>