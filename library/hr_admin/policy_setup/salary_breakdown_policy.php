<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');

//salary_breakdown_policy
//$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
$sql = "SELECT * FROM lib_policy_salary_breakdown WHERE status_active=1 and is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = $row;
	
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var head_counter = 0, salary_heads = all_heads = '<option value="0">&nbsp;</option>';
		<?php 
		foreach( $payroll_heads AS $payroll_id => $payroll ) {
			if( $payroll['salary_head'] == 1 ) echo "salary_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';";
			echo "all_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';";
		}
		?>
		
		$(document).ready(function() {	
			$('#formula_editor').hide();
			reset_form();
			populate_data();
			
			$('#policy_id').change(function(){
				if( $(this).val() == 0 ) reset_form();
				else show_policy( $(this).val() );
			});
			
		});
		
		
				//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
		
		function add_row() {
			head_counter++;
			$('#salary_breakdown tbody').append(
				'<tr>'
					+ '<td>'
						+ '<select name="payroll_head[]" id="payroll_head_' + head_counter + '" class="combo_boxes">'
							+ all_heads
						+ '</select>'
					+ '</td>'
					+ '<td>'
						+ '<select name="type[]" id="type_' + head_counter + '" class="combo_boxes" onchange="change_type( ' + head_counter + ' )">'
							+ '<option value="%">%</option>'
							+ '<option value="Fixed">Fixed</option>'
							+ '<option value="Formula">Formula</option>'
						+ '</select>'
					+ '</td>'
					+ '<td><input type="text" name="percentage_formula[]" id="percentage_formula_' + head_counter + '" value="" class="text_boxes" onfocus="add_formula( ' + head_counter + ' );" onkeypress="return numbersonly(this,event)" /></td>'
					+ '<td>'
						+ '<select name="base_head[]" id="base_head_' + head_counter + '" class="combo_boxes">'
							+ salary_heads
						+ '</select>'
					+ '</td>'
					+ '<td><input type="text" name="amount[]" id="amount_' + head_counter + '" value="" class="text_boxes numbers" onkeypress="return numbersonly(this,event)" /></td>'
				+ '</tr>'
			);
			change_type( head_counter );
		}
		
		function change_type( counter ) {
			switch( $("#type_" + counter).val() ) {
				case '%':
					$("#percentage_formula_" + counter).removeAttr( 'disabled' );
					$("#base_head_" + counter).removeAttr( 'disabled' );
					$("#amount_" + counter).attr( 'disabled', 'disabled' );
					break;
				case 'Fixed':
					$("#percentage_formula_" + counter).attr( 'disabled', 'disabled' );
					$("#base_head_" + counter).attr( 'disabled', 'disabled' );
					$("#percentage_formula_" + counter).val( '' );
					$("#base_head_" + counter).val( '0' );
					$("#amount_" + counter).removeAttr( 'disabled' );
					break;
				case 'Formula':
					$("#percentage_formula_" + counter).removeAttr( 'disabled' );
					$("#base_head_" + counter).attr( 'disabled', 'disabled' );
					$("#amount_" + counter).attr( 'disabled', 'disabled' );
					break;
			}
		}
		
		function add_formula( counter ) {
			if( $("#type_" + counter).val() == 'Formula' ) {
				$('input[name="formula"]').val( $("#percentage_formula_" + counter).val() );
				$( "#formula_editor" ).dialog({
					modal: true,
					width: 395,
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
							$("#percentage_formula_" + counter).val( $('input[name="formula"]').val() )
						},
						Cancel: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		}
		
		function write_formula( btn_name ) {
			if( btn_name != 'backspace' ) {
				$('input[name="formula"]').val( $('input[name="formula"]').val() + $('input[name="' + btn_name + '"]').attr('fval') );
			}
			else {
				var text = $('input[name="formula"]').val();
				var backSpace = text.substr( 0, text.length - 1 );
				$('input[name="formula"]').val( backSpace );
			}
		}
		
		function show_policy( policy_id ) {
			reset_form();
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'data=salary_breakdown_policy&policy_id=' + policy_id,
				success: function( data ) {
					var salary_breakdown_policy = unserialize( data );
					
					$('#policy_id').val( policy_id );
					$('#is_update').val( policy_id );//for update
					$('#policy_name').val( salary_breakdown_policy['policy_name'] );
					if( salary_breakdown_policy['status_active'] == 1 ) $('#approved').attr( 'checked', true );
					
					for( var key in salary_breakdown_policy['definition'] ) {
						if( Number( key ) > 0 ) add_row();
						$('#payroll_head_' + (Number(key)+1)).val( salary_breakdown_policy['definition'][key]['payroll_head'] );
						$('#type_' + (Number(key)+1)).val( salary_breakdown_policy['definition'][key]['type'] );
						$('#percentage_formula_' + (Number(key)+1)).val( salary_breakdown_policy['definition'][key]['percentage_formula'] );
						$('#base_head_' + (Number(key)+1)).val( salary_breakdown_policy['definition'][key]['base_head'] );
						$('#amount_' + (Number(key)+1)).val( salary_breakdown_policy['definition'][key]['amount'] );
					}
				}
			});
		}
		
		function reset_form() {
			$('#salary_breakdown tbody').html( '' );
			$('#policy_id').val( 0 );
			$('#is_update').val( '' );//for update
			$('#policy_name').val( '' );
			$('#approved').attr( 'checked', false );
			head_counter = 0;
			add_row();
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'form=salary_breakdown_policy',
				success: function( data ) {
					$('#data_panel').html( data );
					$('#tbl_salary_breakdown_policy').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ null, { "sType": "html" }, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
	</script>
	<style>
		.header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
		#salary_breakdown_policy_form input[type="text"], select { width:205px; }
		.numbers { text-align:right; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
		.operator { width:58px; padding:2px 0; }
	</style>
</head>
<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%; position:relative; height:70px; margin-bottom:5px; margin-top:5px">    
    <div>
    	<div class="form_caption">
		Salary Breakdown Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete.$approve;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>	
	<form id="salary_breakdown_policy_form" action="javascript:fnc_salary_breakdown_policy(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						&nbsp;Policy:
						<select name="policy_id" id="policy_id" class="combo_boxes" style="width:auto; margin-left:163px;">
							<option value="0">-- Select policy to edit --</option>
							<?php foreach( $salary_breakdown_policy AS $policy ) { ?>
							<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97%; margin-top:10px;">
			<legend>Policy Information</legend>
			<table id="salary_breakdown" width="100%" border="0" cellpadding="0" cellspacing="2">
				<thead>
					<tr>
						<td>Policy Name:</td>
						<td colspan="2"><input type="text" name="policy_name" id="policy_name" class="text_boxes" value="" style="width:100%;" /></td>
						<td align="right">Approved:</td>
						<td><input type="checkbox" name="approved" id="approved" value="1" /></td>
					</tr>
					<td colspan="5" height="10"></td>
					<tr>
						<th class="header">Payroll Head</th>
						<th class="header">Type</th>
						<th class="header">Percentage/Formula</th>
						<th class="header">Base Head</th>
						<th class="header">Amount</th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>									
						<td colspan="5"><input type="button" name="add" id="add" class="formbutton" value="Add" onclick="add_row()" />&nbsp;</td>
					</tr>
                    <tr>
						<td colspan="5" align="center"  class="button_container">
						  <input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						  <input type="button" name="reset" id="reset" class="formbutton" value="Reset" onclick="reset_form()" />
						  <input type="hidden" id="is_update" name="is_update" value="" />					    </td>
					</tr>
				</tfoot>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div>
	<div id="formula_editor" title="Formula Editor">
		<table width="100%">
			<tr><td><input type="text" name="formula" value="" class="text_boxes" style="width:360px; padding:3px; margin-bottom:4px;" readonly /></td></tr>
			<tr>
				<td>
					<input type="button" name="brace_start" value="(" fval="(" class="operator" onclick="write_formula( 'brace_start' );" />
					<input type="button" name="brace_end" value=")" fval=")" class="operator" onclick="write_formula( 'brace_end' );" />
					<input type="button" name="plus" value="+" fval="+" class="operator" onclick="write_formula( 'plus' );" />
					<input type="button" name="minus" value="-" fval="-" class="operator" onclick="write_formula( 'minus' );" />
					<input type="button" name="multiply" value="*" fval="*" class="operator" onclick="write_formula( 'multiply' );" />
					<input type="button" name="divide" value="/" fval="/" class="operator" onclick="write_formula( 'divide' );" />
					<?php for( $i = 0; $i <= 9; $i++ ) { ?>
					<input type="button" name="number_<?php echo $i; ?>" value="<?php echo $i; ?>" fval="<?php echo $i; ?>" class="operator" onclick="write_formula( 'number_<?php echo $i; ?>' );" />
					<?php } ?>
					<input type="button" name="point" value="." fval="." class="operator" onclick="write_formula( 'point' );" />
					<input type="button" name="backspace" value="&larr;" class="operator" onclick="write_formula( 'backspace' );" />
				</td>
			</tr>
			<tr>
				<td>
					<?php foreach( $payroll_heads AS $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
					<input type="button" name="<?php echo $payroll['custom_head']; ?>" value="<?php echo $payroll['custom_head'] . " (" . $payroll['abbreviation'] . ")"; ?>" fval="<?php echo $payroll['abbreviation']; ?>" style="width:182px; padding:2px 0;" onclick="write_formula( '<?php echo $payroll['custom_head']; ?>' );" />
					<?php } } ?>
				</td>
			</tr>
		</table>
	</div>
    </div>
</body>
</html>