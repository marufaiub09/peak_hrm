<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 31-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

//attendance_bonus_policy
$sql = "SELECT * FROM lib_policy_attendance_bonus ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$attendance_bonus_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$attendance_bonus_policy[$row['id']]				= $row;
	$attendance_bonus_policy[$row['id']]['criteria']	= array();
	
	$sql2 = "SELECT * FROM lib_policy_attendance_bonus_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$attendance_bonus_policy[$row['id']]['criteria'][] =	array(
																	'total_leave_criteria'	=> $row2['total_leave_criteria'],
																	'cl_leave_criteria'	=> $row2['cl_leave_criteria'],
																	'sl_leave_criteria'	=> $row2['sl_leave_criteria'],
																	'lwp_leave_criteria'=> $row2['lwp_leave_criteria'],
																	'spl_leave_criteria'=> $row2['spl_leave_criteria'],
																	'el_leave_criteria'=> $row2['el_leave_criteria'],
																	'ml_leave_criteria'	=> $row2['ml_leave_criteria'],
																	'late_criteria'		=> $row2['late_criteria'],
																	'absent_criteria'	=> $row2['absent_criteria'],
																	'amount'			=> $row2['amount']
																);
				
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//att_bonus_leave_type
$var_att_bonus_leave_type=0;
$sql = "SELECT * FROM variable_settings_hrm WHERE is_deleted = 0 AND status_active = 1 and att_bonus_leave_type=1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
if( $row = mysql_fetch_array( $result ) ) {
	 $var_att_bonus_leave_type=1;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_policy.js" type="text/javascript"></script>
	<script src="../../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		var var_att_bonus_leave_type=<? echo $var_att_bonus_leave_type; ?>;
		
		function numbersonly(myfield, e, dec)
		{
			var key;
			var keychar;		
			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);		
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;			
			// numbers
			else if ((("0123456789.").indexOf(keychar) > -1))
				return true;
			else
				return false;
		}

		var bonus_counter = 0, earning_heads = '<option value="0">-- Select --</option>';
		<?php
		foreach( $payroll_heads AS $payroll ) {
			if( $payroll['type'] == 0 ) echo "earning_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';\n";
		}
		?>
		
		$(document).ready(function() {
			reset_form();
			populate_data();
		});
		var lv_disable,tlv_disabled;
		if (var_att_bonus_leave_type==1) tlv_disabled='disabled'; else lv_disable='disabled';
		function add_bonus() {
			bonus_counter++;
			$('#attendance_bonus tbody').append(
				'<tr>'
					+ '<td><input type="text" name="total_leave_criteria[]" id="total_leave_criteria_' + bonus_counter + '" class="text_boxes" '+tlv_disabled+' value="0" onKeyPress="return numbersonly(this, event);" style="width:50px"/></td>'
					+ '<td><input type="text" name="cl_leave_criteria[]" id="cl_leave_criteria_' + bonus_counter + '" class="text_boxes" '+lv_disable+' value="0" onKeyPress="return numbersonly(this, event);" style="width:50px"/></td>'
					+ '<td><input type="text" name="sl_leave_criteria[]" id="sl_leave_criteria_' + bonus_counter + '" class="text_boxes" '+ lv_disable +' value="0" onKeyPress="return numbersonly(this, event);" style="width:50px"/></td>'
					+ '<td><input type="text" name="lwp_leave_criteria[]" id="lwp_leave_criteria_' + bonus_counter + '" class="text_boxes" '+lv_disable+' value="0" onKeyPress="return numbersonly(this, event);" style="width:50px"/></td>'
					+ '<td><input type="text" name="spl_leave_criteria[]" id="spl_leave_criteria_' + bonus_counter + '" class="text_boxes" '+lv_disable+' value="0" onKeyPress="return numbersonly(this, event);" style="width:50px"/></td>'
					+ '<td><input type="text" name="el_leave_criteria[]" id="el_leave_criteria_' + bonus_counter + '" class="text_boxes" '+lv_disable+' value="0" onKeyPress="return numbersonly(this, event);" style="width:50px"/></td>'
					+ '<td><input type="text" name="ml_leave_criteria[]" id="ml_leave_criteria_' + bonus_counter + '" class="text_boxes" '+lv_disable+' value="0" onKeyPress="return numbersonly(this, event);" style="width:50px"/></td>'
					+ '<td><input type="text" name="late_criteria[]" id="late_criteria_' + bonus_counter + '" class="text_boxes" value="" onKeyPress="return numbersonly(this, event);" style="width:120px"/></td>'
					+ '<td><input type="text" name="absent_criteria[]" id="absent_criteria_' + bonus_counter + '" class="text_boxes" value="" onKeyPress="return numbersonly(this, event);" style="width:90px"/></td>'
					+ '<td><input type="text" name="amount[]" id="amount_' + bonus_counter + '" class="text_boxes" value="" onKeyPress="return numbersonly(this, event);" style="width:120px"/></td>'
				+ '</tr>'
			);
		}
		
		function show_policy( policy_id ) {
			//alert("ekram");
			reset_form();
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'data=attendance_bonus_policy&policy_id=' + policy_id,
				success: function( data ) {
					var attendance_bonus_policy = unserialize( data );
					
					$('#policy_id').val( policy_id );
					$('#is_update').val( policy_id );//for update
					$('#policy_name').val( attendance_bonus_policy['policy_name'] );
					$('#earning_head').val( attendance_bonus_policy['earning_head'] );
					$('#status_active').val( attendance_bonus_policy['status_active'] );
					
					for( var key in attendance_bonus_policy['criteria'] ) {
						if( Number( key ) > 0 ) add_bonus();
						$('#total_leave_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['total_leave_criteria'] );
						$('#cl_leave_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['cl_leave_criteria'] );
						$('#sl_leave_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['sl_leave_criteria'] );
						$('#lwp_leave_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['lwp_leave_criteria'] );
						$('#spl_leave_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['spl_leave_criteria'] );
						$('#el_leave_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['el_leave_criteria'] );
						$('#ml_leave_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['ml_leave_criteria'] );
						$('#late_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['late_criteria'] );
						$('#absent_criteria_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['absent_criteria'] );
						$('#amount_' + (Number(key)+1)).val( attendance_bonus_policy['criteria'][key]['amount'] );
					}
				}
			});
		}
		
		function reset_form() {
			$('#attendance_bonus tbody').html( '' );
			$('#policy_id').val( 0 );
			$('#policy_name').val( '' );
			$('#earning_head').val( 0 );
			$('#status_active').val( 1 );
			$('#is_update').val( '' );//for update
			bonus_counter = 0;
			add_bonus();
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'form=attendance_bonus_policy',
				success: function( data ) {
					$('#data_panel').html( data );
					$('#tbl_attendance_bonus_policy').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ { "sType": "html" }, null, null, null, null, null, null, null, null, null, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
	</script>
	<style type="text/css">
		#attendance_bonus_policy_form input[type="text"] { width:180px; }
		#attendance_bonus_policy_form select { width:192px; }
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
		.formbutton { width:120px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center">	
        <div>
    	<div class="form_caption">
		Attendance Bonus Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
		</div>
		<form id="attendance_bonus_policy_form" action="javascript:fnc_attendance_bonus_policy(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete="off" method="POST">
        	<fieldset style="width:925px;">
				<legend>Edit Filter</legend>
				<table width="100%" cellpadding="0" cellspacing="2">
					<tr>
						<td width="10%">Policy:</td>
						<td>
							<select name="policy_id" id="policy_id" class="combo_boxes">
								<option value="0">-- Select policy to edit --</option>
								<?php foreach( $attendance_bonus_policy AS $policy ) { ?>
                                <option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
                                <?php } ?>
							</select>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</fieldset>
            <fieldset style="width:925px; margin-top:10px;">
				<legend>Policy Information</legend>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
						<td colspan="2" style="padding-bottom:10px;">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="10%">Name:</td>
									<td><input type="text" name="policy_name" id="policy_name" class="text_boxes" value="" /></td>
									<td>Earning Head:</td>
									<td>
										<select name="earning_head" id="earning_head" class="combo_boxes">
											<option value="0">-- Select --</option>
											<?php foreach( $payroll_heads AS $payroll ) { if( $payroll['type'] == 0 ) { ?>
											<option value="<?php echo $payroll['id']; ?>" <?php if( $payroll['system_head'] == 'Holiday Allowance' ) echo "selected"; ?>><?php echo $payroll['custom_head']; ?></option>
											<?php } } ?>
										</select>
									</td>
									<td>Status:</td>
									<td>
										<select name="status_active" id="status_active" class="combo_boxes">
											 <?php
												foreach($status_active as $key=>$value):
												?>
												<option value=<? echo "$key";
												if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
												<?		
												endforeach;
												?>
										</select>
									</td>
                                    <td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
                    <tr>
						<td width="778">
							<table width="100%" border="0" cellpadding="0" cellspacing="2" id="attendance_bonus">
								<thead>
                                 <tr>
                                 	 
                                    <th colspan="7" align="center" class="header">Minimum Leave Criteria Breakdown</th>
                                    <th colspan="2" align="center" class="header">Minimum Attendance</th>
                                    <th rowspan="2" align="center" class="header">Amount</th>
                                 </tr>
                              <tr>
                              	<th align="center" class="header">Total Leave</th>
                                <th align="center" class="header">CL</th>
                                <th align="center" class="header">SL</th>
                                <th align="center" class="header">LWP</th>
                                <th align="center" class="header">SpL</th>
                                <th align="center" class="header">EL</th>
                                <th align="center" class="header">ML</th>
                                <th align="center" class="header">Late Criteria</th>
                                <th align="center" class="header">Absent Criteria</th>
                             </tr>
                                </thead>
								<tbody></tbody>
							</table>
						</td>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr><td align="right"><input type="button" name="add" value="Add Bonus" class="formbutton" onclick="add_bonus();" /></td></tr>
								<tr><td align="right" style="padding:5px 0;"><input type="submit" name="save" value="Save" class="formbutton" /></td></tr>
								<tr><td align="right"><input type="button" name="reset" value="Reset" class="formbutton" onclick="reset_form();" />
                                <input type="hidden" id="is_update" name="is_update" value="" /></td></tr>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>
		</form>
		<div id="data_panel" class="demo_jui" align="center" style="width:948px; margin-top:15px;"></div>
	</div>
</body>
</html>