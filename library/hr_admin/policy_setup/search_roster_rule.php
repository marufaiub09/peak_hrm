<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vehicle List</title>
<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function js_set_value(str,roster_id)
{ 
	document.getElementById('txt_selected_roster_rule').value=document.getElementById('txt_roster_no'+str).value;
	document.getElementById('roster_rule_id').value=roster_id;
	document.getElementById('txt_selected_change').value=document.getElementById('cbo_change'+str).value;
}
</script>
</head>

<body>
<fieldset>
<?php
include('../../../includes/common.php');
//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}
?>
<table width="100%">
	<tr>
    	<td colspan="3" align="center"><b>Roster Rule Information</b></td>
    </tr>
    <tr bgcolor="#8FAA8C">
    	<td align="center">SL No.</td>
        <td align="center">Company Name</td>
        <td align="center">Roster Rule Name</td>
    </tr>
<?php
extract($_GET);
$i=1;
$query="Select roster_rule_name,roster_id,company_name,change_on_weekend from lib_duty_roster_policy where company_name='$com_name' and status_active=1 and is_deleted=0 group by roster_id";
$result=mysql_query($query);
	while($row=mysql_fetch_assoc($result))
	{
		if ($i%2==0)  
		$bgcolor="#E9F3FF";
	else
		$bgcolor="#FFFFFF";	
		
?>
		<tr style="text-decoration:none; cursor:pointer"  bgcolor="<?php echo "$bgcolor"; ?>" onclick="js_set_value(<?php echo $i; ?>,<?php echo $row[roster_id]; ?>); parent.emailwindow.hide();"> 
              <td align="center"><?php echo $i; ?></td>
              <td align="center"><?php echo $company_details[$row['company_name']]; ?></td>
              <td align="center"><?php echo $row['roster_rule_name']; ?><input type="hidden" name="txt_roster_no<? echo $i; ?>" id="txt_roster_no<? echo $i; ?>" value="<?php echo $row['roster_rule_name']; ?>" /><input type="hidden" name="cbo_change<? echo $i; ?>" id="cbo_change<? echo $i; ?>" value="<?php echo $row['change_on_weekend']; ?>" /></td>
       	 </tr>
	<?php 
		$i++;
	}
?>
		<tr>
			<td align="center">
            	<input type="hidden" name="roster_rule_id" id="roster_rule_id" />
                <input type="hidden" name="txt_selected_roster_rule" id="txt_selected_roster_rule" />
                <input type="hidden" name="txt_selected_change" id="txt_selected_change" />
				<input type="hidden" name="close" onclick="parent.emailwindow.hide();"  class="formbutton" value="Close" />
			</td>
		</tr>
</table>
</fieldset>
</body>
</html>