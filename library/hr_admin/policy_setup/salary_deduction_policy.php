<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	
	<link href="../../../resources/jquery.multiselect.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery.multiselect.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('select[name="policy"]').multiselect({
				noneSelectedText: "Select policy to edit"
			});
			
			oTable = $('#holiday_incentive_policy').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"aoColumns": [ null, { "sType": "html" }, null, null, null, null, null, null ],
				"aaSorting": [[ 0, "asc" ]],
				"oLanguage": { "sSearch": "Search all columns:" },
				"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]]
			});
		});
	</script>
	<style>
		em { font-size:22px; font-family:Georgia; }
		.header { height:25px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
		input[type="button"], input[type="submit"], input[type="reset"] { width:120px; padding:3px; }
		#salary_deduction_policy_form input[type="text"] { width:152px; }
		#salary_deduction_policy_form select { width:157px; }
	</style>
</head>

<body background="../../../resources/images/body3.jpg" style="font-family:verdana; font-size:11px;">
	<h2 style="width:560px; padding:0 10px; margin-bottom:15px; border-bottom:2px solid #000; text-shadow:1px 1px #FFFFFF;">
		<em>S</em>alary <em>D</em>eduction <em>P</em>olicy
	</h2>
	<form id="salary_deduction_policy_form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<div class="ui-widget-content ui-corner-all" style="width:935px; padding:10px; margin-bottom:10px;">
			<table width="100%" cellpadding="0" cellspacing="4">
				<tr>
					<td>Policy:</td>
					<td colspan="5">
						<select name="policy" style="width:auto;">
							
						</select>
					</td>
				</tr>
				<tr>
					<td width="10%">Name:</td>
					<td><input type="text" name="policy_name" value="" /></td>
					<td>Deduct for (days):</td>
					<td><input type="text" name="deduct_for" value="" /></td>
					<td>Earning Head:</td>
					<td>
						<select name="earning_head">
							<option value="0">-- Select --</option>
							<?php
							$result = mysql_query( "SELECT * FROM payroll_head ORDER BY id" );
							while( $row = mysql_fetch_assoc( $result ) ) {
							?>
							<option value="<?php echo $row['id']; ?>"><?php echo $row['custom_head']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<table style="width:955px" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="85%">
					<table width="100%" cellspacing="4" cellpadding="0">
						<thead>
							<tr>
								<th class="header">Late Slot</th>
								<th class="header">Parameter Type</th>
								<th class="header">Percentage / Formula</th>
								<th class="header">Base Head</th>
								<th class="header">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select name="late_slot[]">
										<option value="1">Each 2 Late</option>
										<option value="2">Each 3 Late</option>
										<option value="3">Each 4 Late</option>
										<option value="4">Each 5 Late</option>
										<option value="5">Each 6 Late</option>
									</select>
								</td>
								<td>
									<select name="parameter_type[]">
										<option value="0">Percentage</option>
										<option value="1">Fixed</option>
									</select>
								</td>
								<td><input type="text" name="percentage_formula[]" value="" /></td>
								<td>
									<select name="base_head[]">
										<option value="0">-- Select --</option>
										<?php
										$result = mysql_query( "SELECT * FROM payroll_head ORDER BY id" );
										while( $row = mysql_fetch_assoc( $result ) ) {
										?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['custom_head']; ?></option>
										<?php } ?>
									</select>
								</td>
								<td><input type="text" name="amount[]" value="" /></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td align="right">
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr><td align="right"><input type="button" name="add" value="Add" /></td></tr>
						<tr><td align="right" style="padding:3px 0;"><input type="submit" name="save" value="Save" /></td></tr>
						<tr><td align="right"><input type="reset" name="cancel" value="Cancel" /></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	<div class="demo_jui" style="width:955px; margin-top:10px;">
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="holiday_incentive_policy">
			<thead>
				<tr>
					<th width="5%">ID</th>
					<th>Policy Name</th>
					<th>Minmum working hours</th>
					<th>Calculation For</th>
					<th>Type</th>
					<th>Percentage / Formula</th>
					<th>Base Head</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</body>
</html>