<?php
/*************************************
|	Tiffin bill Policy
|	Developed by : Md. Nuruzzaman
*************************************/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');
include('../../../includes/array_function.php');

// Company 
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}

$sql = "SELECT * FROM lib_location WHERE is_deleted = 0  ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = mysql_real_escape_string($row['location_name']);
}

$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = mysql_real_escape_string($row['division_name']);
}

$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = mysql_real_escape_string($row['department_name']);
}

$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = mysql_real_escape_string($row['section_name']);
}

$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$subsection_details[$row['id']] = mysql_real_escape_string( $row['subsection_name'] );		
}

$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$designation_chart = array();
while( $row = mysql_fetch_assoc( $result ) ) {		
		$designation_chart[$row['id']] = mysql_real_escape_string( $row['custom_designation'] );		
}

// salary grade 
$sql = "SELECT * FROM lib_salary_grade WHERE is_deleted = 0 and status_active=1 ORDER BY grade_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$grade_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$grade_details[$row['id']] = mysql_real_escape_string($row['grade_name']);
}

//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//shift_policy
$sql = "SELECT * FROM lib_policy_shift WHERE is_deleted = 0 ORDER BY shift_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$shift_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$shift_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$shift_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="includes/functions.js" type="text/javascript" ></script>
        
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../../includes/update_status.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
        
        <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../../../js/popup_window.js"></script>
        <script type="text/javascript" src="../../../js/modal.js"></script>
        
        <script type="text/javascript" charset="utf-8">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
        
            $(document).ready(function() {	
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });
				populate_data();
            });
			
			// When load the page then this function will be execute
            function populate_data() {
                $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                $.ajax({
                    type: "POST",
                    url: "../hr_admin_data.php",
                    data: 'form=frm_piece_rate_ot_chart',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#tbl_piece_rate_ot_chart').dataTable({
                            "bRetrieve": true,
                            "bDestroy": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                         	 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
                            //"aaSorting": [[ 0, "asc" ]],*/
                          	"oLanguage": { "sSearch": "Search all columns:" },
                            "sScrollX": "100%",
                            "sScrollY": "200px",
                            "sScrollXInner": "100%",
                            "bScrollCollapse": true
                        });
                    }
                });
            }

			// Employee code wise browse
			function openmypage_employee_info_code(page_link,title)
			{			
				//alert("su..re");
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id");
					$('#txt_emp_code').val(thee_id.value);
				}
			}
			
			// Employee id card wise browse
			function openmypage_employee_info_id_card(page_link,title)
			{			
				//alert("su..re");
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title,  'width=1000px,height=420px,center=1,resize=0,scrolling=0','../../')			
				emailwindow.onclose=function()
				{
					var thee_loc = this.contentDoc.getElementById("txt_selected");
					var thee_id = this.contentDoc.getElementById("txt_selected_id_card_no");
					$('#txt_id_card').val(thee_id.value);
				}
			}
			
			function search_piece_rate_ot_chart()
			{
				//alert("su..re");
				var error = false; 
				var data = '';
				
				if( $('#cbo_company_id').val() ==0 ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_company_id').focus();
						$(this).html('Please select Company.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				
				else if( $('#cbo_grade_id').val() ==0 ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#cbo_grade_id').focus();
						$(this).html('Please select Grade.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}

				else if( $('#txt_rate').val() =='' ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_rate').focus();
						$(this).html('Please enter Rate.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				
				else if( $('#txt_working_mnt').val() =='' ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_working_mnt').focus();
						$(this).html('Please enter Working Minute.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				
				else if( $('#txt_eff_date').val() =='' ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#txt_eff_date').focus();
						$(this).html('Please enter Effective Date.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				
				else
				{
					//alert("su..re");
					data = 	'&company_id		=' + $('#cbo_company_id').val()+ 
							'&grade				=' + $('#cbo_grade_id').val()+ 
							'&designation_id	=' + $('#cbo_designation_id').val()+ 
							'&rate				=' + $('#txt_rate').val()+ 
							'&eff_date			=' + $('#txt_eff_date').val()+
							'&working_minute	=' + $('#txt_working_mnt').val()+ 
							'&location_id		=' + $('#cbo_location_id').val()+
							'&division_id		=' + $('#cbo_division_id').val()+ 
							'&department_id		=' + $('#cbo_department_id').val()+ 
							'&section_id		=' + $('#cbo_section_id').val()+ 
							'&subsection_id		=' + $('#cbo_subsection_id').val()+
							'&id_card			=' + $('#txt_id_card').val()+
							'&emp_code			=' + $('#txt_emp_code').val();
							
					nocache = Math.random();
					http.open( 'GET', 'includes/generate_piece_rate_ot_chart.php?type=generate_piece_rate_ot_chart' + data + '&nocache=' + nocache );
					http.onreadystatechange = response_generate_piece_rate_ot_chart;
					http.send(null); 
				}
			}
			
			function response_generate_piece_rate_ot_chart() {	
				if(http.readyState == 4) {
					var response = http.responseText.split('####');
					 //alert(response[0]);		
					/*$('#data_panel').html( 'Convert To : <a href="includes/tmp_report_file/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;&nbsp;' );
					$('#data_panel').append( '<input type="button" onclick="new_window_short()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );*/
					$('#data_panel').html( response[0] );
					var tableFilters ={col_0: "none",}
					setFilterGrid("table_body",-1,tableFilters);
					hide_search_panel();
					$("#messagebox").fadeTo( 200, 0.1, function() {
						$(this).html( 'Report generated successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
				}
				document.getElementById('scroll_body_topheader').innerHTML=document.getElementById('to_summery_hidden').innerHTML;
				document.getElementById('to_summery_hidden').innerHTML="";
			}
			
            // Click on the each ROW of list view this function will be execute
            function js_set_value(id_mst,company,grade,designation,rate,working_minute,eff_date)
            {
                //alert("I am from ROW click");
                //alert(designation);
				var splt_date=eff_date.split("-");
				var edate=splt_date[2]+"-"+splt_date[1]+"-"+splt_date[0];
				
				document.getElementById('txt_update_id').value=id_mst;
                document.getElementById('cbo_company_id').value=company;
                document.getElementById('cbo_grade_id').value=grade;
				document.getElementById('cbo_designation_id').value=designation;
				document.getElementById('txt_rate').value=rate;
				document.getElementById('txt_working_mnt').value=working_minute;
				document.getElementById('txt_eff_date').value=edate;
               /* $.ajax({
                            type: "POST",
                            url: "../hr_admin_data.php",
                            data: 'data=tiffin_bill_policy&id_mst=' + id_mst,
                            success: function( data ) {
                            $('#tiffin_bill tbody tr').remove();
                            $('#tiffin_bill tbody').append(data);
                            document.getElementById('db_row_ids_mst').value=document.getElementById('db_row_ids').value;
                        $('.timepicker').timepicker({
                            H: true
                        });
                    }
                });*/
            }
			
			// Grade Change function 
			function fnc_grade_change(selected_id)
			{
				//alert("su..re");
				$.ajax({
					type: "POST",
					url: "includes/get_data_update.php?",
					data: 'type=select_grade_wise_designation&id=' + selected_id,
					success: function( html ) {
						$('#cbo_designation_id').html( html )
					}
				});
			}
			
        </script>
        <style>
            .header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:1050px;">
            <div style="height:60px;">    	
                <div class="form_caption">Piece Rate OT Chart</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update.$delete;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div>
            </div>
            <form id="frm_piece_rate_ot_chart" name="frm_piece_rate_ot_chart" action="javascript:fun_piece_rate_ot_chart(save_perm,edit_perm,delete_perm)" autocomplete="off" method="post">
                <fieldset style="width:90%;"><legend>Piece Rate OT Chart</legend>
                    <fieldset style="width:90%;">
                        <table width="80%" cellspacing="1" cellpadding="0" border="0" align="center">
                        	<thead>
                            	<tr>
                                    <th class="header" width="180px">Company Name</th>
                                    <th class="header" width="100px">Grade</th>
                                    <th class="header" width="150px">Designation</th>
                                    <th class="header" width="100px">Rate</th>
                                    <th class="header" width="150px">Min Working Minute</th>
                                    <th class="header" width="150px">Effective Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <select name="cbo_company_id" id="cbo_company_id" class="combo_boxes" style="width:180px">
                                    <? if($company_cond=="")
                                       { 
                                    ?>
                                    <option value="0">-- Select --</option>
                                    <?php } foreach( $company_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>" <? if(count($company_details)==1)echo "selected"; ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td> 
                                
                                <td>
                                    <select name="cbo_grade_id" id="cbo_grade_id" class="combo_boxes" onchange="fnc_grade_change(this.value);" style="width:100px">
                                    <option value="0">-- Select --</option>
                                    <?php foreach( $grade_details AS $key=>$value ){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="cbo_designation_id" id="cbo_designation_id" class="combo_boxes" style="width:150px" >
                                        <option value="0">-- Select --</option>
                                        <?php // foreach( $designation_chart AS $key=>$value ){ ?>
                                        <option value="<?php //echo $key; ?>"><?php //echo $value; ?></option>
                                        <?php // } ?>
                                    </select>	
                                </td>
                                <td><input type="text" id="txt_rate" name="txt_rate" class="text_boxes" onKeyPress="return numbersonly(this, event);" style="width:100px;" /></td>
                                <td><input type="text" id="txt_working_mnt" name="txt_working_mnt" class="text_boxes" onKeyPress="return numbersonly(this, event);" style="width:150px;" /></td>
                                <td><input type="text" id="txt_eff_date" name="txt_eff_date" class="datepicker" style="width:150px;" /></td>
                            </tr>
                            </tbody>
                        </table>
                        </fieldset>
                        <div style="height:10px;"></div>
                        <table align="center">
                            <tr>
                                <td><input type="submit" name="save" id="save" class="formbutton" value="Save" style="width:90px;"/></td>
                                <td>
                                	<input type="button" name="refresh" id="refresh" class="formbutton" value="Refresh" onclick="reset_form('frm_piece_rate_ot_chart','')" style="width:90px;" />
                                    <input type="hidden" name="txt_update_id" id="txt_update_id" value="" />
                                </td>
                            </tr>
                        </table>
                </fieldset>
            </form> 
        </div>
        <div id="data_panel" class="demo_jui" align="center" style="width:970px; margin-top:15px; margin-left:40px;"></div>
	</body>
</html>