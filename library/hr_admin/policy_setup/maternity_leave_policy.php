<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 31-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');


	$sql = "SELECT * FROM lib_policy_maternity_leave ORDER BY policy_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$maternity_leave_policy = array();
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
		$maternity_leave_policy[$row['id']] = $row;
	}
	
	//print_r($maternity_leave_policy);die;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" charset="utf-8" content="UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
	<script src="../../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="../../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		
		var maternity_leave_policy = new Array();
		<?php
		
		foreach( $maternity_leave_policy AS $policy_id => $policy ) {
			echo "maternity_leave_policy[$policy_id] = new Array();\n";
			foreach( $policy AS $key => $value ) {
				if( $key == 'policy_name' ) echo "maternity_leave_policy[$policy_id]['$key']='".mysql_real_escape_string( $value )."';\n";
				else {
					if( $value != '') echo "maternity_leave_policy[$policy_id]['$key']='".mysql_real_escape_string( $value )."';\n";
					else echo "maternity_leave_policy[$policy_id]['$key']=0;\n";
					}
			}
		}
		
		?>
		//alert("asdad"+maternity_leave_policy[1]['policy_name']);
		$(document).ready(function() {
			populate_data();
			
			$('select[name="policy"]').change(function() {
				if( $(this).val() == 0 ) {
					reset_form();
				} else {
					show_policy( $(this).val() );
				}
			});
		});
		
		function show_policy( policy_id ) {
			//alert(maternity_leave_policy[policy_id]['policy_name']);
			$('#policy_id').val( policy_id );
			$('#is_update').val( policy_id );//for update
			$('#policy_name').val( maternity_leave_policy[policy_id]['policy_name'] );
			$('#max_limit').val( maternity_leave_policy[policy_id]['max_limit'] );
			$('#min_service_length').val( maternity_leave_policy[policy_id]['min_service_length'] );
			$('#allowed_issue_no').val( maternity_leave_policy[policy_id]['allowed_issue_no'] );
			$('#gap_between_issues').val( maternity_leave_policy[policy_id]['gap_between_issues'] );
			$('#leave_before_delivery').val( maternity_leave_policy[policy_id]['leave_before_delivery'] );
			$('#leave_after_delivery').val( maternity_leave_policy[policy_id]['leave_after_delivery'] );
			$('#payment_calculation').val( maternity_leave_policy[policy_id]['payment_calculation'] );
			$('#payment_disbursement').val( maternity_leave_policy[policy_id]['payment_disbursement'] );
			$('#status_active').val( maternity_leave_policy[policy_id]['status_active'] );
		}
		
		function reset_form() {
			$('#policy_id').val( 0 );
			$('#is_update').val( '' );
			$('#policy_name').val( '' );
			$('#max_limit').val( '' );
			$('#min_service_length').val( '' );
			$('#allowed_issue_no').val( '' );
			$('#gap_between_issues').val( '' );
			$('#leave_before_delivery').val( '' );
			$('#leave_after_delivery').val( '' );
			$('#payment_calculation').val( 0 );
			$('#payment_disbursement').val( 0 );
			$('#status_active').val( 1 );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'form=maternity_leave_policy',
				success: function(data) {
					$('#data_panel').html( data );
					oTable = $('#maternity_leave_policy').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ { "sType": "html" }, null, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
		
				//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
	</script>
	<style type="text/css">
		#maternity_leave_policy_form input[type="text"] { width:168px; }
		#maternity_leave_policy_form select { width:180px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%; position:relative; height:70px; margin-bottom:5px; margin-top:5px">     
    <div>
    	<div class="form_caption">
		Maternity Leave Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form id="maternity_leave_policy_form" action="javascript:fnc_maternity_leave_policy(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:1062px;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellspacing="2" cellpadding="0">
				<tr>
					<td>
						Policy:
						<select name="policy_id" id="policy_id" class="combo_boxes" style="width:auto; margin-left:119px;">
							<option value="0">-- Select --</option>
							<?php foreach( $maternity_leave_policy AS $mtl ) { ?>
							<option value="<?php echo $mtl['id']; ?>"><?php echo $mtl['policy_name']; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:1062px; margin-top:10px;">
			<legend>Maternity Leave Policy Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td>Name:</td>
					<td colspan="3"><input type="text" name="policy_name" id="policy_name" class="text_boxes" value="" style="width:522px;" /></td>
					<td>Minimum Service Length:</td>
					<td><input type="text" name="min_service_length" id="min_service_length" class="text_boxes" value="" placeholder="Days" onkeypress="return numbersonly(this,event)" /></td>
				</tr>
				<tr>
					<td>Max Limit:</td>
					<td><input type="text" name="max_limit" id="max_limit" class="text_boxes" value="" placeholder="Days" onkeypress="return numbersonly(this,event)" /></td>
					<td>Allowed No of Issue:</td>
					<td><input type="text" name="allowed_issue_no" id="allowed_issue_no" class="text_boxes" value="" onkeypress="return numbersonly(this,event)" /></td>
					<td>Gap Between Issues:</td>
					<td><input type="text" name="gap_between_issues" id="gap_between_issues" class="text_boxes" value="" placeholder="Days" onkeypress="return numbersonly(this,event)" /></td>
				</tr>
				<tr>
					<td>Leave Before Delivery:</td>
					<td><input type="text" name="leave_before_delivery" id="leave_before_delivery" class="text_boxes" value="" placeholder="Days" onkeypress="return numbersonly(this,event)" /></td>
					<td>Leave After Delivery:</td>
					<td><input type="text" name="leave_after_delivery" id="leave_after_delivery" class="text_boxes" value="" placeholder="Days" onkeypress="return numbersonly(this,event)" /></td>
					<td>Payment Calculation:</td>
					<td>
						<select name="payment_calculation" id="payment_calculation" class="combo_boxes">
							<option value="0">Per day average of 3 month's earnings</option>
							<option value="1">Regular Salary</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Payment Disbursement:</td>
					<td>
						<select name="payment_disbursement" id="payment_disbursement" class="combo_boxes">
							<option value="0">50% Before &amp; 50% after Delivary</option>
							<option value="1">100% Before Delivary</option>
							<option value="2">With Regular Monthly Salary</option>
						</select>
					</td>
					<td>Status:</td>
					<td colspan="3">
						<select name="status_active" id="status_active" class="combo_boxes">
									<?php
									foreach($status_active as $key=>$value):
									?>
									<option value=<? echo "$key";
									if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
									<?		
									endforeach;
									?>
						</select>
					</td>
				</tr>
                <tr>
					<td colspan="6">&nbsp;</td>						
				</tr>
				<tr>
					<td align="center" colspan="6" style="padding-top:10px;"  class="button_container">
						<input type="submit" name="save" value="Save" class="formbutton" />&nbsp;&nbsp;
						<input type="button" name="cancel" value="Cancel" class="formbutton" onclick="reset_form();" />
                        <input type="hidden" id="is_update" name="is_update" value="" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:1084px; margin-top:15px;"></div>
</body>
</html>