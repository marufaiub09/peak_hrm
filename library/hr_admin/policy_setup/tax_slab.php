
<?
include('../../../includes/common.php');
//payroll head
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = mysql_real_escape_string($row['custom_head']);
	}

//print_r($payroll_heads);


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tax Slab</title>
         <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen">
         <script type="text/javascript" src="tax_calculation/includes/ajax_submit_hr_admin.js"></script>
         <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
         <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
         <link rel="stylesheet" type="text/css" href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css">
         <script type="text/javascript" src="includes/functions.js"></script>
    
    
    

	<script>
    function numbersonly(myfield, e, dec)
    {
        var key;
        var keychar;
    
        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);
    
        // control keys
        if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
        return true;
        
        // numbers
        else if ((("0123456789.").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }
    </script>

</head>

<body onload="showResult_search_slab('','list_tax_slab','slab_list_view');">

<div id="example2" align="left" style="width:600px; font-size:11px;">
        <div class="form_caption">
           Tax Slab
        </div>
       <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
  <form name="tax_slab" id="tax_slab" action="" method="post" style="margin-top:25px"> 
	<fieldset>
	<legend>Tax Slab</legend>
		<table cellpadding="0" cellspacing="1" width="100%">
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;Slab No</td>
				<td><input type="text" name="slab_number" id="slab_number"  class="numbersOnly" style="width:130px;"  onKeyPress=" return numbersonly(this,event)" /></td><input type="hidden" id="save_up" name="save_up" value="" />
                <td>&nbsp;&nbsp;&nbsp;&nbsp;Slab Name</td>
                <td><input type="text" name="slab_name"   class="text_boxes" id="slab_name"  style="width:130px;" /></td>
		   </tr>
           <tr>
              	<td>&nbsp;&nbsp;&nbsp;&nbsp;1st Amount</td>
                <td><input type="text" name="slab_amount" id="slab_amount"  class="numbersOnly" style="width:130px;"  onKeyPress=" return numbersonly(this,event)" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;Tax</td>
                <td><input type="text" name="slab_tax" id="slab_tax"  class="numbersOnly" style="width:130px;"  onKeyPress=" return numbersonly(this,event)" /></td>
          </tr>
	 </table>

<div class="button_container" align="center">
    <input type="button" value="Save" id="save" onclick="fnc_save_update_tax_slab()" class="formbutton" name="save" style="width:110px;">
	 <input  type="reset" value="Refresh" id="refresh" class="formbutton" name="refresh" style="width:110px;" >
</div>
</fieldset>
</form>
<div id="slab_list_view"></div>
</div>

</body>
</html>