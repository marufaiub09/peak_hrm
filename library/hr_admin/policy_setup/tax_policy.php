<?php
/*######################################

	Completed By
	Name : Md.Ekram Hossain
	Date : 28-12-2012
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/common.php');


//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
	}



//payroll head
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = mysql_real_escape_string($row['custom_head']);
	}

//print_r($payroll_heads);

//Year_details
$sql = "select * from lib_policy_year where type=2 and  status_active=1 order by id";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$tax_year_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$tax_year_details[$row['id']] = mysql_real_escape_string($row['name']);
	}

//print_r($tax_year_details);






$ConsumptionTabIndex=-1;
extract($_POST);
if(!$TabIndexNo) { $TabIndexNo=0; }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Tax Calculation</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
	<script src="../../../resources/ui.tabs.paging.js" type="text/javascript"></script>
    <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
  
    <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css">
    <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen">
    <script src="tax_calculation/includes/functions.js" type="text/javascript"></script>
    <script src="tax_calculation/includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
    
     <link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../../js/modal.js"></script>
    
    

<script type="text/javascript">
        $(document).ready(function() {
            $('#example').tabs();
            $('#example').tabs('paging', { cycle: true, follow: true } );
            $('#example').tabs('select',<?php echo $TabIndexNo; ?>);
        });
    </script>
    
    

<script>
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}


</script>

<script>
function openmypage(page_link,title)
		{
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=290px,center=1,resize=0,scrolling=0','../../')
			
			emailwindow.onclose=function()
			{
				var hidden_id = this.contentDoc.getElementById("hidden_id").value;
				var hidden_company = this.contentDoc.getElementById("hidden_company").value;
				var hidden_tax_year = this.contentDoc.getElementById("hidden_tax_year").value;
				var hidden_rule_name = this.contentDoc.getElementById("hidden_rule_name").value;
				var hidden_status_active = this.contentDoc.getElementById("hidden_status_active").value;
				var hidden_comments = this.contentDoc.getElementById("hidden_comments").value;
				var hidden_tax_mst_id = this.contentDoc.getElementById("hidden_tax_mst_id").value;
				//var hidden_payroll_head = this.contentDoc.getElementById("hidden_payroll_head").value;
				//var hidden_taxable = this.contentDoc.getElementById("hidden_taxable").value;
				//alert(hidden_id)
				
				$('#system_id').val(hidden_id);
				$('#company_id').val(hidden_company);
				$('#tax_year').val(hidden_tax_year);
				$('#rule_name').val(hidden_rule_name);
				$('#status').val(hidden_status_active);
				$('#comments').val(hidden_comments);
				//$('#payroll_head').val(hidden_payroll_head);
				//$('#taxable').val(hidden_taxable);
				
				showResult_search(hidden_id,'list_tax_basic','tax_list_view');
			}
		}
</script>



</head>
<body>

<div align="center" style="width:1000px;">        
	<div class="form_caption">
           Tax Rule
        </div>
        
        
	<div id="messagebox" style="background-color:#FF9999; color:#000000; width:900px" align="center"></div>
    
     <div id="example2" align="center" style="width:900px; font-size:11px;">
     <form name="tax_policy" id="tax_policy" action="" method="post" style="margin-top:25px"> 
		<fieldset>
			<legend>HRM  Module</legend>

			<table cellpadding="0" cellspacing="1" >
				
				<tr>
					<td>
						System Id
					</td>
                    
					<td> 
                     <input name="system_id" id="system_id"   style="width:130px;"  placeholder="Double Click to Search" ondblclick="openmypage('search_employee_tax.php','Employee Info'); return false" readonly class="text_boxes">
                     <input type="hidden" id="system_id" value="system_id">
					</td>
                    
                    
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;Company
					</td>
					<td>
					 <select name="company_id" id="company_id" class="combo_boxes" style="width:120px;">
                   
					 <option value="0">---Select---</option>
                      <?php  foreach( $company_details as $key=>$value ):?>
                        <option value=<? echo "$key"; ?>> <? echo "$value" ;?> </option>				
                                <?php		
                                endforeach;
                                ?> 
					  </select>
					</td>
                    <td>
						&nbsp;&nbsp;&nbsp;&nbsp;Tax Year
					</td>
					<td><select name="tax_year" id="tax_year" class="combo_boxes" style="width:140px;">
                     <option value="0">---Select---</option>
                      <?php  foreach( $tax_year_details as $key=>$value ):?>
                        <option value=<? echo "$key"; ?>> <? echo "$value" ;?> </option>				
                                <?php		
                                endforeach;
                                ?> 
				</select>
				</td>
					
				</tr>
			
			<tr>
					
					<td>
						Rule Name
					</td>
					<td> <!-- Select & Display-->
					<input name="rule_name" id="rule_name" class="text_boxes" style="width:130px;" >
					</td>
                    <td>
                    	&nbsp;&nbsp;&nbsp;&nbsp;Status
                     </td>
                    <td>
                     	<select name="status" id="status" class="combo_boxes" style="width:120px;">
                            <option value="1">Active</option>
                     		<option value="2">Inactive</option>
                     	</select>
                     </td>
                    
                    
                     <td colspan="">&nbsp;&nbsp;&nbsp;&nbsp;Comments</td>
                     <td colspan="3"><input type="text" name="comments" id="comments" class="text_boxes" style="width:130px;" size="20"></td>
                    </tr>
                    <tr>
					
                     
                    
              </tr>
             	
			</table>

            <input type="hidden" id="save_up"  name="save_up">
           
             <input type="hidden" name="hidden_id" id="hidden_id" value="" />
           <input type="hidden" name="hidden_company" id="hidden_company" value="" /><!-- Hidden field -->
            <input type="hidden" name="hidden_tax_year" id="hidden_tax_year" value="" />
             <input type="hidden" name="hidden_rule_name" id="hidden_rule_name" value="" />
              <input type="hidden" name="hidden_status_active" id="hidden_status_active" value="" />
               <input type="hidden" name="hidden_comments" id="hidden_comments" value="" />
                <input type="hidden" name="hidden_tax_mst_id" id="hidden_tax_mst_id" value="" />
                 <input type="hidden" name="hidden_payroll_head" id="hidden_payroll_head" value="" />
                  <input type="hidden" name="hidden_taxable" id="hidden_taxable" value="" />
	</fieldset>
    </form>
</div>	
   
    <div id="example" align="center" style="width:900px; margin-top:20px; font-size:11px;">
         <ul class="tabs">
			<li><a href="#basic_tax" onClick="">Basic</a></li>
			<li><a href="#tax_examption" onClick="">Examptions</a></li>
			<li><a href="#tax_rebates" onClick="">Tax Rebate</a></li>
           
			
		</ul>
        <div id="basic_tax" align="left"> <?php include('tax_calculation/basic_tax.php'); ?></div>
        <div id="tax_examption" align="left"> <?php include('tax_calculation/tax_examption.php'); ?></div>
		<div id="tax_rebates" align="left"> <?php include('tax_calculation/tax_rebate.php'); ?></div>
      
		
    </div>
    	
</div>


</body>
</html>