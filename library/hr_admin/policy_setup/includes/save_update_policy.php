<?php
date_default_timezone_set('UTC');
session_start();

include('../../../../includes/common.php');

$e_date = time();
$user_name=$_SESSION['logic_erp']["user_name"];
extract( $_GET );

//Shift Policy ( Save / Update )
if( $action == "shift_policy" ) {
	
	$fields = array( "id","shift_name","shift_type","cross_date","shift_start","shift_end","grace_minutes","exit_buffer_minutes","early_out_start","entry_restriction_start",
					"first_break_start","first_break_end","second_break_start","second_break_end","lunch_time_start","lunch_time_end","short_shift_name","status_active" );
	
	$unique_check = check_uniqueness( "shift_name", "lib_policy_shift", "shift_name='$shift_name'", $shift_id );
	if( $unique_check == false ) {
		echo "3";
		exit();
	}
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	if( $shift_id == 0 ) {	//Insert
		$id = return_next_id( "id", "lib_policy_shift" );

		$sql = "INSERT INTO lib_policy_shift ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " )";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_" . $id."_0";
	}
	else {					//Update
		$sql = "UPDATE lib_policy_shift SET ";
		for( $i = 1; $i < count( $fields ); $i++ ) {
			if( $i > 1 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " WHERE id = $shift_id";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "9_" . $shift_id."_0";
	}
	exit();
}



//Overtime Policy ( Save / Update )
if( $action == "overtime_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	//print_r($_GET);exit();
	$unique_check = check_uniqueness( "policy_name", "lib_policy_overtime", "policy_name='$policy_name'", $policy_id );
	if( $unique_check == false ) {
		echo "3";
		exit();
	}

	$fields = array( "id","policy_name","shift_id","overtime_rate","overtime_calculation_rule","max_overtime","status_active" );
	$id = ( $policy_id == 0 ) ? return_next_id( "id", "lib_policy_overtime" ) : $policy_id;
	
	if( $policy_id == 0 ) {
		
		$unique_check = check_duplicate("id", "lib_overtime_with_shift", "shift_policy in ($shift_id)");
		if( $unique_check == false ) {
			echo "4";
			exit();
		}
		
		$sql = "INSERT INTO lib_policy_overtime ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " )";
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$operation = 1;
		
		$shift_arr=explode(",",$shift_id);$tblID=0;
		for($ct=0;$ct<count($shift_arr);$ct++)
		{
			if( $tblID == 0 ) $tblID = return_next_id( "id", "lib_overtime_with_shift" ); else $tblID = $tblID+1;
			$sql = "INSERT INTO lib_overtime_with_shift (id,overtime_policy,shift_policy)values($tblID,$id,$shift_arr[$ct])";
			mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
		
	}
	else {
				
		$sql = "UPDATE lib_policy_overtime SET ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " WHERE id = " . $id;
		$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$operation = 2;
		
		$sql = mysql_query("DELETE FROM lib_overtime_with_shift WHERE overtime_policy = $id");
		$shift_arr=explode(",",$shift_id);$tblID=0;
		for($ct=0;$ct<count($shift_arr);$ct++)
		{
			if( $tblID == 0 ) $tblID = return_next_id( "id", "lib_overtime_with_shift" ); else $tblID = $tblID+1;
			$sql = "INSERT INTO lib_overtime_with_shift (id,overtime_policy,shift_policy)values($tblID,$id,$shift_arr[$ct])";
			mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
	}
	 
	
	$sql_history=$sql;
	if( $policy_id != 0 ) {
		$sql = "DELETE FROM lib_policy_overtime_slab WHERE policy_id = $id";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	for( $i = 1; $i <= $slab_counter; $i++ ) {
		$slab_id = return_next_id( "id", "lib_policy_overtime_slab" );
		$name = "slab$i";
		$slab = explode( '_', $$name );
		$sql = "INSERT INTO lib_policy_overtime_slab (
					id,
					policy_id,
					overtime_slot,
					overtime_multiplier,
					overtime_head
				) VALUES (
					$slab_id,
					$id,
					'$slab[0]',
					$slab[1],
					$slab[2]
				)";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation . "_" . $id . "_" . $policy_name."_0";
	exit();
}
//Holiday Incentive Policy ( Save / Update )
if( $action == "holiday_incentive_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$unique_check = check_uniqueness( "policy_name", "lib_policy_holiday_incentive", "policy_name='$policy_name'", $policy_id );
	if( $unique_check == false ) {
		echo "3";
		exit();
	}
	
	$fields = array( "id","policy_name","pay_for","min_working_hour","earning_head","status_active" );
	$id = ( $policy_id == 0 ) ? return_next_id( "id", "lib_policy_holiday_incentive" ) : $policy_id;
	
	if( $policy_id == 0 ) {
		$sql = "INSERT INTO lib_policy_holiday_incentive ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . mysql_real_escape_string($$fields[$i] ) . "'";
		}
		$sql .= " )";
		$operation = 1;
	}
	else {
		$sql = "UPDATE lib_policy_holiday_incentive SET ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . mysql_real_escape_string($$fields[$i] ) . "'";
		}
		$sql .= " WHERE id = " . $id;
		$operation = 2;
	}
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$sql_history =$sql;
	
	if( $policy_id != 0 ) {
		$sql = "DELETE FROM lib_policy_holiday_incentive_definition WHERE policy_id = $id";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	for( $i = 1; $i <= $incentive_counter; $i++ ) {
		$def_id = return_next_id( "id", "lib_policy_holiday_incentive_definition" );
		$name = "incentive$i";
		$incentive = explode( '_', $$name );
		$incentive[3] = ( $incentive[3] == '' ) ? 0 : $incentive[3];
		$sql = "INSERT INTO lib_policy_holiday_incentive_definition (
					id,
					policy_id,
					parameter_type,
					percentage_formula,
					base_head,
					amount
				) VALUES (
					$def_id,
					$id,
					'$incentive[0]',
					'$incentive[1]',
					'$incentive[2]',
					'$incentive[3]'
				)";
				
			//echo $sql;
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation . "_" . $id . "_" . $policy_name ."_0";
	exit();
}

//Attendance Bonus Policy ( Save / Update )
if( $action == "attendance_bonus_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	//print_r($_GET);exit();
	$unique_check = check_uniqueness( "policy_name", "lib_policy_attendance_bonus", "policy_name='$policy_name'", $policy_id );
	if( $unique_check == false ) {
		echo "3";
		exit();
	}
	
	/*$unique_check = check_uniqueness( "attendance_bonus_policy", "hrm_employee", "attendance_bonus_policy !=0" );
	if( $unique_check == false ) {
		echo "4";
		exit();
	}*/
	
	$fields = array( "id","policy_name","earning_head","status_active" );
	$id = ( $policy_id == 0 ) ? return_next_id( "id", "lib_policy_attendance_bonus" ) : $policy_id;
	
	if( $policy_id == 0 ) {
		$sql = "INSERT INTO lib_policy_attendance_bonus ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " )";
		$operation = 1;
	}
	else {
		$sql = "UPDATE lib_policy_attendance_bonus SET ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " WHERE id = " . $id;
		$operation = 2;
	}
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$sql_history .=$sql;
	
	if( $policy_id != 0 ) {
		$sql = "DELETE FROM lib_policy_attendance_bonus_definition WHERE policy_id = $id";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	for( $i = 1; $i <= $criteria_counter; $i++ ) {
		$criteria_id = return_next_id( "id", "lib_policy_attendance_bonus_definition" );
		$name = "criteria$i";
		$criteria = explode( '_', $$name );
		
		$sql = "INSERT INTO lib_policy_attendance_bonus_definition (
					id,
					policy_id,
					total_leave_criteria,
					cl_leave_criteria,
					sl_leave_criteria,
					lwp_leave_criteria,
					spl_leave_criteria,
					el_leave_criteria,
					ml_leave_criteria,
					late_criteria,
					absent_criteria,
					amount
				) VALUES (
					$criteria_id,
					$id,
					'$criteria[0]',
					'$criteria[1]',
					'$criteria[2]',
					'$criteria[3]',
					'$criteria[4]',
					'$criteria[5]',
					'$criteria[6]',
					'$criteria[7]',
					'$criteria[8]',
					'$criteria[9]'
				)";
//echo $sql;die;	
		
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation . "_" . $id . "_" . $policy_name."_0";
	exit();
}

//Absent Deduction Policy ( Save / Update )
if( $action == "absent_deduction_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$unique_check = check_uniqueness( "policy_name", "lib_policy_absent_deduction", "policy_name='$policy_name'", $policy_id );
	if( $unique_check == false ) {
		echo "3";
		exit();
	}
	$unique_check = check_uniqueness( "absent_deduction_policy", "hrm_policy_tagging", "absent_deduction_policy !=0" );
	if( $unique_check == false ) {
		echo "4";
		exit();
	}
	$fields = array( "id","policy_name","deduction_head","status_active" );
	$id = ( $policy_id == 0 ) ? return_next_id( "id", "lib_policy_absent_deduction" ) : $policy_id;
	
	if( $policy_id == 0 ) {
		$sql = "INSERT INTO lib_policy_absent_deduction ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " )";
		$operation = 1;
	}
	else {
		$sql = "UPDATE lib_policy_absent_deduction SET ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " WHERE id = " . $id;
		$operation = 2;
	}
	
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$sql_history .=$sql;
	
	if( $policy_id != 0 ) {
		$sql = "DELETE FROM lib_policy_absent_deduction_definition WHERE policy_id = $id";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	for( $i = 1; $i <= $criteria_counter; $i++ ) {
		$def_id = return_next_id( "id", "lib_policy_absent_deduction_definition" );
		$name = "criteria$i";
		$criteria = explode( '_', $$name );
		$criteria[3] = ( $criteria[3] == '' ) ? 0 : $criteria[3];
		$sql = "INSERT INTO lib_policy_absent_deduction_definition (
					id,
					policy_id,
					parameter_type,
					percentage_formula,
					base_head,
					amount
				) VALUES (
					$def_id,
					$id,
					$criteria[0],
					'$criteria[1]',
					$criteria[2],
					$criteria[3]
				)";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation . "_" . $id . "_" . $policy_name."_0";
	exit();
}
//Late Deduction Policy ( Save / Update )
if( $action == "late_deduction_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$unique_check = check_uniqueness( "policy_name", "lib_policy_late_deduction", "policy_name='$policy_name'", $policy_id );
	if( $unique_check == false ) {
		echo "3";
		exit();
	}
	$unique_check = check_uniqueness( "late_deduction_policy", "hrm_policy_tagging", "late_deduction_policy !=0" );
	if( $unique_check == false ) {
		echo "4";
		exit();
	}
	$fields = array( "id","policy_name","deduct_for","deduction_head","status_active" );
	$id = ( $policy_id == 0 ) ? return_next_id( "id", "lib_policy_late_deduction" ) : $policy_id;
	
	if( $policy_id == 0 ) {
		$sql = "INSERT INTO lib_policy_late_deduction ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " )";
		$operation = 1;
	}
	else {
		$sql = "UPDATE lib_policy_late_deduction SET ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " WHERE id = " . $id;
		$operation = 2;
	}
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$sql_history .=$sql;
	
	if( $policy_id != 0 ) {
		$sql = "DELETE FROM lib_policy_late_deduction_criteria WHERE policy_id = $id";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	for( $i = 1; $i <= $criteria_counter; $i++ ) {
		$def_id = return_next_id( "id", "lib_policy_late_deduction_criteria" );
		$name = "criteria$i";
		$criteria = explode( '_', $$name );
		$criteria[4] = ( $criteria[4] == '' ) ? 0 : $criteria[4];
		$sql = "INSERT INTO lib_policy_late_deduction_criteria (
					id,
					policy_id,
					late_slot,
					parameter_type,
					percentage_formula,
					base_head,
					amount
				) VALUES (
					$def_id,
					$id,
					$criteria[0],
					$criteria[1],
					'$criteria[2]',
					$criteria[3],
					$criteria[4]
				)";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation . "_" . $id . "_" . $policy_name."_0";
	exit();
}

//Early Deduction Policy ( Save / Update )
if( $action == "early_deduction_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	
	$unique_check = check_uniqueness( "policy_name", "lib_policy_early_deduction", "policy_name='$policy_name'", $policy_id );
	if( $unique_check == false ) {
		echo "3";
		exit();
	}
	$unique_check = check_uniqueness( "early_deduction_policy", "hrm_policy_tagging", "early_deduction_policy !=0" );
	if( $unique_check == false ) {
		echo "4";
		exit();
	}
	$fields = array( "id","policy_name","deduct_for","deduction_head","status_active" );
	$id = ( $policy_id == 0 ) ? return_next_id( "id", "lib_policy_early_deduction" ) : $policy_id;
	
	if( $policy_id == 0 ) {
		$sql = "INSERT INTO lib_policy_early_deduction ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i];
		}
		$sql .= " ) VALUES ( ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= "'" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " )";
		$operation = 1;
	}
	else {
		$sql = "UPDATE lib_policy_early_deduction SET ";
		for( $i = 0; $i < count( $fields ); $i++ ) {
			if( $i > 0 ) $sql .= ", ";
			$sql .= $fields[$i] . " = '" . mysql_real_escape_string($$fields[$i] ) . "'";
			
		}
		$sql .= " WHERE id = " . $id;
		$operation = 2;
	}
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$sql_history .=$sql;
	
	if( $policy_id != 0 ) {
		$sql = "DELETE FROM lib_policy_early_deduction_criteria WHERE policy_id = $id";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	for( $i = 1; $i <= $criteria_counter; $i++ ) {
		$def_id = return_next_id( "id", "lib_policy_early_deduction_criteria" );
		$name = "criteria$i";
		$criteria = explode( '_', $$name );
		$criteria[4] = ( $criteria[4] == '' ) ? 0 : $criteria[4];
		$sql = "INSERT INTO lib_policy_early_deduction_criteria (
					id,
					policy_id,
					early_slot,
					parameter_type,
					percentage_formula,
					base_head,
					amount
				) VALUES (
					$def_id,
					$id,
					$criteria[0],
					$criteria[1],
					'$criteria[2]',
					$criteria[3],
					$criteria[4]
				)";
		mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		$sql_history .=$sql;
	}
	
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation . "_" . $id . "_" . $policy_name."_0";
	exit();
}


if( $action == "duty_roster_policy" )
{
	
	if  ($isupdate=="")
	{
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		 
		$txt_roster_rule_name=trim($txt_roster_rule_name);
				
		if($txt_roster_rule_id=="") 
			$roster_id= return_next_id("roster_id","lib_duty_roster_policy");
		else  
			$roster_id=$txt_roster_rule_id; 
		
		$chk=check_uniqueness("roster_rule_name","lib_duty_roster_policy","is_deleted=0 and status_active=1 and roster_rule_name='$txt_roster_rule_name'");
		if($chk==false && $txt_roster_rule_id==""){ echo "3";exit();} //message duplicate roster name
		
		
			$id= return_next_id( "id","lib_duty_roster_policy");	
			$sql = "INSERT INTO lib_duty_roster_policy (
							id,
							company_name,
							roster_id,
							roster_rule_name,
							shift_name,
							shift_days,
							next_shift,
							change_on_weekend,
							new_shift_on_weekend,
							inserted_by,
							insert_date
						) VALUES (
							'$id',
							'".mysql_real_escape_string($cbo_company_name)."',
							'".mysql_real_escape_string($roster_id)."',
							'".mysql_real_escape_string($txt_roster_rule_name)."',
							'".mysql_real_escape_string($cbo_shift_name)."',
							'".mysql_real_escape_string($txt_shift_days)."',
							'".mysql_real_escape_string($cbo_next_shift)."',
							'".mysql_real_escape_string($cbo_change_on_weekend)."',
							'".$cbo_new_shift_on_weekend."',
							'".mysql_real_escape_string($user_name)."',
							'$e_date'
						)";
		
				mysql_query( $sql ) or die (mysql_error());	
				$sql_history=encrypt($sql,"logic_erp_2011_2012");
				$_SESSION['sql_history']=$sql_history;
				echo "1"."_".$txt_roster_rule_id;
				exit();
	}
	else
	{
				$sql = "UPDATE lib_duty_roster_policy
						SET
							company_name	= '".mysql_real_escape_string($cbo_company_name)."',
							roster_rule_name='".mysql_real_escape_string($txt_roster_rule_name)."',
							shift_name	= '".mysql_real_escape_string($cbo_shift_name)."',
							shift_days = '".mysql_real_escape_string($txt_shift_days)."',
							next_shift = '".mysql_real_escape_string($cbo_next_shift)."',
							updated_by	= '".mysql_real_escape_string($user_name)."',
							change_on_weekend='".$cbo_change_on_weekend."',
							new_shift_on_weekend='".$cbo_new_shift_on_weekend."',
							update_date = '$e_date'
							WHERE id = '$isupdate'
						 ";
						  
				mysql_query( $sql ) or die (mysql_error());
				$sql_history=encrypt($sql,"logic_erp_2011_2012");
				$_SESSION['sql_history']=$sql_history;
				echo "2";
				exit();

	}
}




//Bonus Policy ( Save / Update )
if( $action == "bonus_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	 
	$operation='';
		//echo $is_update; die;								
	if( $is_update == '' ) 
	{
		
		$unique_check = check_uniqueness( "policy_name", "lib_bonus_policy", "policy_name='$policy_name'", $policy_id );
		if( $unique_check == false ) {
			echo "3";
			exit();
		}	
		 
		$mid = return_next_id( "id", "lib_bonus_policy" );
		$mst_sql = "INSERT INTO lib_bonus_policy(id, policy_name, inserted_by, insert_date, status_active)
					VALUES ($mid, '$policy_name', '$user_name',$e_date,$status_active)";
		mysql_query( $mst_sql ) or die( $mst_sql . "<br />" . mysql_error() );
		$sql_history .=$mst_sql;
		for($i=0;$i<$counter;$i++)
			{
				$head = 'head'.$i;
				$data = explode("_",$$head);
				$slot = $data[0];
				$comfirmation = $data[1];
				$percentage = $data[2];
				$base_head = $data[3];
				$amounttype = $data[4];
				
					$did = return_next_id( "id", "lib_bonus_policy_definition" );
					$dtls_sql = "INSERT INTO lib_bonus_policy_definition(id, policy_id, slot, comfirmation, percentage, base_head,amount_type, inserted_by, insert_date, status_active)
								 VALUES ($did, '$mid', '$slot','$comfirmation', '$percentage', '$base_head',$amounttype,'$user_name',$e_date,$status_active)";		
					mysql_query( $dtls_sql ) or die( $dtls_sql . "<br />" . mysql_error() );
			}
		$sql_history .=$dtls_sql;
		$operation=1;
	}
	else 
	{
			//echo "ss".$counter; die;
			$sql = "UPDATE lib_bonus_policy SET policy_name='$policy_name',status_active=$status_active WHERE id = $is_update";
			mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$sql_history .=$sql;
			
			$delete_sql = "DELETE FROM lib_bonus_policy_definition WHERE policy_id = $is_update";
			mysql_query( $delete_sql ) or die( $delete_sql . "<br />" . mysql_error() );
			$sql_history .=$delete_sql;
			
			for($i=0;$i<$counter;$i++)
			{
				$head = 'head'.$i;
				$data = explode("_",$$head);
				$slot = $data[0];
				$comfirmation = $data[1];
				$percentage = $data[2];
				$base_head = $data[3];
				$amounttype = $data[4];
				
					$did = return_next_id( "id", "lib_bonus_policy_definition" );
					$dtls_sql = "INSERT INTO lib_bonus_policy_definition(id, policy_id, slot, comfirmation, percentage, base_head,amount_type, inserted_by, insert_date, status_active)
								 VALUES ($did, '$is_update', '$slot','$comfirmation', '$percentage', '$base_head',$amounttype,'$user_name',$e_date,$status_active)";				
					mysql_query( $dtls_sql ) or die( $dtls_sql . "<br />" . mysql_error() );
			}							
			$sql_history .=$dtls_sql;									
			$operation=2;									
	}
		
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation."_".$id;
	exit();
}


if( $action == "bonus_policy_delete" ) {
	
	$sql=mysql_query("select * from hrm_employee where bonus_policy=$policy_id and status_active=1");
	if( mysql_num_rows($sql)>0 ) {
			echo "3";
			exit();
		}	
	
	$mst_delete_sql = "DELETE FROM lib_bonus_policy WHERE id = $policy_id";
	mysql_query( $mst_delete_sql ) or die( $mst_delete_sql . "<br />" . mysql_error() );
	
	$dtls_delete_sql = "DELETE FROM lib_bonus_policy_definition WHERE policy_id = $policy_id";
	mysql_query( $dtls_delete_sql ) or die( $dtls_delete_sql . "<br />" . mysql_error() );
	
	$sql_history = $mst_delete_sql.$dtls_delete_sql;
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo "1";
	exit();

}

/*************************************
|	Tiffin bill policy
|	Developed by : Md. Nuruzzaman 
*************************************/
if($action=="tiffin_bill_policy")
{
	if($policy_id=="") // Insert
	{
		$id_mst = return_next_id( "id", "lib_policy_tiffin_bill_mst" );
		//$update_id=explode("_",$update_id);
		$must_stay=explode("_",$must_stay);
		$bill_amt=explode("__",$bill_amt);
		
		$sql="INSERT INTO lib_policy_tiffin_bill_mst(
		id,
		policy_name,
		late_applicable,
		earning_head
		) VALUES (
			'$id_mst',
			'$policy_name',
			'$late_applicable',
			'$earning_head')";
		mysql_query( $sql) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo 1;
				
		for($i=0; $i<count($must_stay);$i++)
		{
			$id_dtls = return_next_id( "id", "lib_policy_tiffin_bill_dtls" );
			$sql="INSERT INTO lib_policy_tiffin_bill_dtls(
				id,
				policy_id,
				must_stay,
				bill_amt				
				) VALUES (  
				'$id_dtls',
				'$id_mst',
				'".$must_stay[$i]."',
				'".$bill_amt[$i]."')";
				 
			mysql_query( $sql ) or die (mysql_error());
		}
		exit();
	}
	else //Update
	{
		$sql="update lib_policy_tiffin_bill_mst set
			policy_name='$policy_name',
			late_applicable='$late_applicable',
			earning_head='$earning_head'
			where id='$policy_id'";
			mysql_query( $sql ) or die (mysql_error());
		
			$update_id=explode("_",$update_id);
			$must_stay=explode("_",$must_stay);
			$bill_amt=explode("__",$bill_amt);
			$db_row_ids_mst=explode(",",$db_row_ids_mst);
			//print_r($db_row_ids_mst); die;
			
			$delete_id=implode(",",array_diff($db_row_ids_mst,$update_id)); //delete
			//print_r($delete_id); die;
			if($delete_id!="") mysql_query( "update lib_policy_tiffin_bill_dtls set status_active=0 , is_deleted=1 where id in ( $delete_id )" );
		
			//print_r($bill_amt); die;
			
			for($i=0; $i<=count($must_stay);$i++) // update
			{
				//$id_dtls = return_next_id( "id_dtls", "lib_policy_tiffin_bill_dtls" );
				if($update_id[$i]!="")
				{
					$sql="update lib_policy_tiffin_bill_dtls set
						must_stay='".$must_stay[$i]."',
						bill_amt='".$bill_amt[$i]."'
						where id='".$update_id[$i]."'";
						
						mysql_query( $sql ) or die (mysql_error());
				}
			}
			for($i=0; $i<count($must_stay);$i++) //insert
			{
				if($update_id[$i]=="")
				{
					$id_dtls = return_next_id( "id", "lib_policy_tiffin_bill_dtls" );
					$sql="INSERT INTO lib_policy_tiffin_bill_dtls(
						id,
						policy_id,
						must_stay,
						bill_amt				
						) VALUES (  
						'$id_dtls',
						'$policy_id',
						'".$must_stay[$i]."',
						'".$bill_amt[$i]."')";
						 
					mysql_query( $sql ) or die (mysql_error());
				}
			}
		echo 2;
		exit();
	}
}
// END of Tiffin bill policy

if($action=="allowance_policy")
{
	if($policy_id=="") // Insert
	{
		$id_dtls = return_next_id( "id", "lib_policy_allowance" );
		$id_mst = return_next_id( "policy_id", "lib_policy_allowance" );
		
		//$update_id=explode("__",$update_id);
		$must_stay=explode("__",$must_stay);
		$formula_type=explode("__",$formula_type);
		$value=explode("__",$value);
		$base_head=explode("__",$base_head);
		$day_type=explode("__",$day_type);
		$pay_with_salary=explode("__",$pay_with_salary);
		$earning_head=explode("__",$earning_head);
		
		$sql="INSERT INTO lib_policy_allowance(
		id,
		policy_id,
		policy_name,
		late_applicable,
		must_stay,
		formula_type,
		value,
		base_head,
		day_type,
		pay_with_salary,
		earning_head
		) VALUES "; 
		for($i=0; $i<count($must_stay);$i++)
		{
			$id_dtlss=$id_dtls+$i;
			if($sqlpa!="") $sqlpa.=",";
			$sqlpa.="(
				'$id_dtlss',
				'$id_mst',
				'$policy_name',
				'$late_applicable',
				'".$must_stay[$i]."',
				'".$formula_type[$i]."',
				'".$value[$i]."',
				'".$base_head[$i]."',
				'".$day_type[$i]."',
				'".$pay_with_salary[$i]."',
				'".$earning_head[$i]."'
			)";
		}
			//echo $sql.$sqlpa; die;
			mysql_query( $sql.$sqlpa ) or die (mysql_error());
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo 1;
			exit();
	}
	else //Update
	{
		$update_id=explode("__",$update_id);
		$must_stay=explode("__",$must_stay);
		$formula_type=explode("__",$formula_type);
		$value=explode("__",$value);
		$base_head=explode("__",$base_head);
		$day_type=explode("__",$day_type);
		$pay_with_salary=explode("__",$pay_with_salary);
		$earning_head=explode("__",$earning_head);
		
		$db_row_ids_mst=explode(",",$db_row_ids_mst);
		//print_r($db_row_ids_mst); die;
		$delete_id=implode(",",array_diff($db_row_ids_mst,$update_id)); //delete
		//print_r($delete_id); die;
		if($delete_id!="") mysql_query( "update lib_policy_allowance set status_active=0 , is_deleted=1 where id in ( $delete_id )" );

		
		for($i=0; $i<count($must_stay); $i++)
		{
			if($update_id[$i]=="") // Insert
			{
				$id_dtls = return_next_id( "id", "lib_policy_allowance" );
				//$id_mst = return_next_id( "id_mst", "lib_policy_allowance" );
				$sql="INSERT INTO lib_policy_allowance(
				id,
				policy_id,
				policy_name,
				late_applicable,
				must_stay,
				formula_type,
				value,
				base_head,
				day_type,
				pay_with_salary,
				earning_head
				) VALUES (
						'$id_dtls',
						'$policy_id',
						'$policy_name',
						'$late_applicable',
						'".$must_stay[$i]."',
						'".$formula_type[$i]."',
						'".$value[$i]."',
						'".$base_head[$i]."',
						'".$day_type[$i]."',
						'".$pay_with_salary[$i]."',
						'".$earning_head[$i]."'
					)";
				mysql_query( $sql) or die (mysql_error());
			}
		}
		
for($i=0; $i<=count($must_stay); $i++)
{
		if($update_id[$i]!="") // Update
			{
				$sql="update lib_policy_allowance set 
					policy_name='".$policy_name."',
					late_applicable='".$late_applicable."',
					must_stay='".$must_stay[$i]."',
					formula_type='".$formula_type[$i]."',
					value='".$value[$i]."',
					base_head='".$base_head[$i]."',
					day_type='".$day_type[$i]."',
					pay_with_salary='".$pay_with_salary[$i]."',
					earning_head='".$earning_head[$i]."'					
					where id='".$update_id[$i]."'";
				
				mysql_query( $sql ) or die (mysql_error());
			}//echo $sql;
		}
		echo 2;
		exit();
	}
}
// END of Tiffin bill policy

//======================Start============transport_allowance_policy===============================================================================================

//transport_allowance_policy ( Save / Update )

if( $action == "transport_allowance_policy" ) {
	mysql_query( "SET CHARACTER SET utf8" );
	mysql_query( "SET SESSION collation_connection ='utf8_unicode_ci'" );
	 
	$operation='';
	$in_date= date("Y-m-d H:i:s");
	//echo $is_update; die;
	//is_update=&policy_name=sohel&policy_amount=100&cbo_status=1&counter=1&head0=15_2_1_20&head1=5_3_2_500
	//echo "sohel";die;								
	if( $is_update == '' ) 
	{
		
		$unique_check = check_uniqueness( "policy_name", "lib_transport_policy", "policy_name='$policy_name'", $policy_id );
		if( $unique_check == false ) 
		{
			echo "3";
			exit();
		}	
		
		$mst_id = return_next_id( "mst_id","lib_transport_policy" );
		
		for($i=0;$i<$counter;$i++)
			{
				$head = 'head'.$i;
				$data = explode("_",$$head);
				$mini_working_days = $data[0];
				$emp_status = $data[1];
				$amount_type = $data[2];
				$amount_value = $data[3];
				
				$id = return_next_id( "id", "lib_transport_policy" );
				$dtls_sql = "INSERT INTO lib_transport_policy(id,mst_id,policy_name,amount,mini_working_days,emp_status,amount_type,amount_value,inserted_by,insert_date, status_active) VALUES ($id, '$mst_id', '$policy_name','$policy_amount','$mini_working_days','$emp_status',$amount_type,'$amount_value','$user_name','$in_date',$cbo_status)";		
				mysql_query( $dtls_sql ) or die( $dtls_sql . "<br />" . mysql_error() );
			}
		$sql_history .=$dtls_sql;
		$operation=1;
	}
	else 
	{
		$delete_sql = "DELETE FROM lib_transport_policy WHERE mst_id = $is_update";
		mysql_query( $delete_sql ) or die( $delete_sql . "<br />" . mysql_error() );
		$sql_history .=$delete_sql;
		
		//$mst_id = return_next_id( "mst_id","lib_transport_policy" );
		
		for($i=0;$i<$counter;$i++)
			{
				$head = 'head'.$i;
				$data = explode("_",$$head);
				$mini_working_days = $data[0];
				$emp_status = $data[1];
				$amount_type = $data[2];
				$amount_value = $data[3];
				
				$id = return_next_id( "id", "lib_transport_policy" );
				$dtls_sql = "INSERT INTO lib_transport_policy(id,mst_id,policy_name,amount,mini_working_days,emp_status,amount_type,amount_value,updated_by,update_date, status_active) VALUES ($id, '$is_update', '$policy_name','$policy_amount','$mini_working_days','$emp_status',$amount_type,'$amount_value','$user_name','$in_date',$cbo_status)";		
				mysql_query( $dtls_sql ) or die( $dtls_sql . "<br />" . mysql_error() );
			}
			$sql_history .=$dtls_sql;
			$operation=2;									
	}
		
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo $operation."_".$id;
	exit();
}


if( $action == "transport_policy_delete" ) {
	
	/*$sql=mysql_query("select * from hrm_employee where transport_policy=$policy_id and status_active=1");
	if( mysql_num_rows($sql)>0 ) {
			echo "3";
			exit();
		}*/	
	
	$mst_delete_sql = "DELETE FROM lib_transport_policy WHERE mst_id = $policy_id";
	mysql_query( $mst_delete_sql ) or die( $mst_delete_sql . "<br />" . mysql_error() );
	
	$sql_history = $mst_delete_sql;
	$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
	$_SESSION['sql_history']=$sql_history;
	echo "1";
	exit();

}

//============================================end ===================transport_policy==============================================================================



if($action=="shift_time_distribution")
{
	
		$in_date= date("Y-m-d H:i:s");
		$id_dtls = return_next_id( "id", "lib_shift_time_distribution" );
		//$id_mst = return_next_id( "policy_id", "lib_policy_allowance" );
		
		//$update_id=explode("__",$update_id);
		$shift_id=explode("__",$shift_id);
		$value_minute=explode("__",$value_minute);
		$status_type=explode("__",$status_type);
		
		if($shift_id!="")
		{
			for($d=0; $d<count($shift_id);$d++)
			{
				mysql_query("DELETE FROM lib_shift_time_distribution WHERE shift_id = '".$shift_id[$d]."'");
				//$delete_sql = "DELETE FROM lib_shift_time_distribution WHERE shift_id = '".$shift_id[$d]."'";
				//$result=mysql_db_query($DB, $delete_sql);
				//mysql_query( $delete_sql );// or die( $delete_sql . "<br />" . mysql_error() );
			}
		}
		
		//print_r($shift_id);die;
		
		$sql="INSERT INTO lib_shift_time_distribution(
		id,
		shift_id,
		minutes,
		type,
		inserted_by,
		insert_date
		) VALUES "; 
		for($i=0; $i<count($shift_id);$i++)
		{
			$id_dtlss=$id_dtls+$i;
			if($sqlpa!="") $sqlpa.=",";
			$sqlpa.="(
				'$id_dtlss',
				'".$shift_id[$i]."',
				'".$value_minute[$i]."',
				'".$status_type[$i]."',
				'$user_name',
				'$in_date'
			)";
		}
			//echo $sql.$sqlpa; die;
			mysql_query( $sql.$sqlpa ) or die (mysql_error());
			//$sql_history=encrypt($sql, "logic_erp_2011_2012");
			//$_SESSION['sql_history']=$sql_history;
			echo 1;
			exit();
	
}
?>