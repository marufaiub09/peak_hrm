//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

//Employee Basic of Employee Information ( Save / Update )
function fnc_emp_basic() {
	var fields = new Array( 'emp_code','first_name','middle_name','last_name','id_card_no','punch_card_no','dob','father_name','mother_name','birth_place','religion','blood_group',
							'marital_status','sex','nationality','national_id','passport_no','designation_id','designation_level','joining_date','confirmation_date','category',
							'functional_superior','admin_superior','remark','company_id','location_id','division_id','department_id','section_id','subsection_id' );
	for( var i = 0; i < fields.length; i++ ) {
		field_array[i]	= new Array( fields[i], $('#' + fields[i]).val() );
	}
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	
	if( $('#first_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#first_name').focus();
			$(this).html('Please Enter First Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#designation_level').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#designation_level').focus();
			$(this).html('Please Select a Designation Level').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#designation_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#designation_id').focus();
			$(this).html('Please Select a Designation').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#joining_date').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#joining_date').focus();
			$(this).html('Please Select a Joining Date').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#company_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please Select a Company').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#designation_level').val() == 47 ) {
		if( $('#location_id').val() == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#location_id').focus();
				$(this).html('Please Select a Location').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $('#division_id').val() == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#division_id').focus();
				$(this).html('Please Select a Division').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $('#department_id').val() == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#department_id').focus();
				$(this).html('Please Select a Department').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( $('#section_id').val() == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#section_id').focus();
				$(this).html('Please Select a Section').addClass('messageboxerror').fadeTo(900,1);
			});
		}
	}
	
	if( error == false ) {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_basic' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_basic;
		http.send(null);
	}
}
//Employee Basic Response of Employee Information ( Save / Update )
function response_emp_basic() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			if( response[1] == 0 ) var field = 'National ID';
			if( response[1] == 1 ) var field = 'Passport No';
			if( response[1] == 2 ) var field = 'ID Card No';
			if( response[1] == 3 ) var field = 'Punch Card No';
			
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate ' + field + '. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
			$('#emp_code').val( response[1] );
			$('#new').css( "visibility", "visible" );
			enable_tabs();
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Address of Employee Information ( Save / Update )
function fnc_emp_address() {
	var fields = new Array( 'village','house_no','road_no','post_code','thana','district_id','division_id','phone_no','mobile_no','email','co' );
	
	for( var i = 0; i < 3; i++) {
		if( i == 0  ) { var fields_counter = 10; var type = 'present'; }
		else if( i == 1  ) { var fields_counter = 10; var type = 'permanent'; }
		else { var fields_counter = 11; var type = 'alternate'; }
		
		for( var j = 0; j < fields_counter; j++ ) {
			field_array[j + i * 10] = new Array( type + '_' + fields[j], $('#' + type + '_' + fields[j]).val() );
		}
	}
	field_array[31] = new Array( 'emp_code', $('#emp_code').val() );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	nocache = Math.random();
	var data = '';
	for( var i = 0; i < field_array.length; i++ ) {
		data += '&' + field_array[i][0] + '=' + field_array[i][1];
	}
	http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_address' + data + '&nocache=' + nocache );
	http.onreadystatechange = response_emp_address;
	http.send(null);
}
//Employee Address Response of Employee Information ( Save / Update )
function response_emp_address() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Salary of Employee Information ( Save / Update )
function fnc_emp_salary() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	calculate_all();
	
	if( $('#salary_grade').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#salary_grade').focus();
			$(this).html('Please Enter salary_grade').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	var gross = parseFloat( $('#gross_salary').val() );
	var amount = 0;
	for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
		for( var j = 1; j <= payroll_heads.length; j++ ) {
			if( payroll_heads[j] != undefined && payroll_heads[j]['id'] == $('#payroll_head_' + i).val() && payroll_heads[j]['salary_head'] == 1 ) {
				amount += parseFloat( $('#amount_' + i).val() );
				break;
			}
		}
	}
	var diff = gross - amount;
	if( diff != 0 && ( diff < - 0.02 || diff > 0.02 ) ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#gross_salary').focus();
			$(this).html('Gross salary (' + gross + ') and total salary (' + amount + ') do not match (' + diff + ').').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( diff != 0 ) {
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			if( $('#payroll_head_' + i).val() == 1 ) {
				$('#amount_' + i).val( parseFloat( $('#amount_' + i).val() ) + diff );
				break;
			}
		}
	}
	
	if( error == false ) {
		field_array[0] = new Array( 'emp_code', $('#emp_code').val() );
		field_array[1] = new Array( 'salary_grade', $('#salary_grade').val() );
		field_array[2] = new Array( 'salary_rule', $('#salary_rule').val() );
		field_array[3] = new Array( 'gross_salary', $('#gross_salary').val() );
		
		for( var i = 1; i <= $('#salary tbody tr').length; i++ ) {
			field_array[i + 3] = new Array( $('#payroll_head_' + i).val(), $('#type_' + i).val(), $('#percentage_formula_' + i).val(), $('#base_head_' + i).val(), $('#amount_' + i).val() );
		}
		
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			if( i > 3 ) {
				data += '&head' + ( i - 3 ) + '=';
				for( var j = 0; j < field_array[i].length; j++ ) {
					if( j > 0 ) data += '_';
					data += field_array[i][j];
				}
			}
			else data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		data += '&head_counter=' + $('#salary tbody tr').length;
		
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_salary' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_salary;
		http.send(null);
	}
}
//Employee Address Response of Employee Information ( Save / Update )
function response_emp_salary() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');
		
		if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Bank Info of Employee Information ( Save / Update )
function fnc_emp_salary_bank() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false;
	
	if( $('#bank_id').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#bank_id').focus();
			$(this).html('Please Select a Salary Bank.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#branch_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#branch_name').focus();
			$(this).html('Please Enter a Branch Name.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#account_no').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#account_no').focus();
			$(this).html('Please Enter an Account No.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		field_array[0] = new Array( 'emp_code', $('#emp_code').val() );
		field_array[1] = new Array( 'bank_id', $('#bank_id').val() );
		field_array[2] = new Array( 'branch_name', $('#branch_name').val() );
		field_array[3] = new Array( 'account_no', $('#account_no').val() );
		field_array[4] = new Array( 'tin_no', $('#tin_no').val() );
		
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_salary_bank' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_salary_bank;
		http.send(null);
	}
}
//Employee Bank Info Response of Employee Information ( Save / Update )
function response_emp_salary_bank() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Entitlement of Employee Information ( Save / Update )
function fnc_emp_entitlement() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	field_array = new Array();
	var error = false;
	
	field_array['emp_code'] = $('#emp_code').val();
	field_array['ot_entitled'] = $('#ot_entitled').is(':checked') ? 1 : 0;
	field_array['holiday_allowance_entitled'] = $('#holiday_allowance_entitled').is(':checked') ? 1 : 0;
	
	if( $('input[name="salary_type"]:checked').val() == undefined ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('input[name="salary_type"]:first').focus();
			$(this).html('Please Select Salary Type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else field_array['salary_type'] = $('input[name="salary_type"]:checked').val();
	
	if( $('#pf_entitled').is(':checked') ) {
		field_array['pf_entitled'] = 1;
		
		if( $('#pf_effective_date').val() == '' ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#pf_effective_date').focus();
				$(this).html('Please Provide Provident Fund Effective Date.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else field_array['pf_effective_date'] = $('#pf_effective_date').val();
		
		$('#pf tbody tr').each(function() {
			if( $(this).find('input[name="pf_nominee_name[]"]').val() == '' && $(this).find('input[name="pf_nominee_relation[]"]').val() == '' && $(this).find('input[name="pf_nominee_ratio[]"]').val() == '' ) {
				if( this != this.parentNode.firstChild ) $(this).remove();
			}
		});
		var correct_pf_nominee_counter = 0;
		field_array['pf_nominee'] = new Array();
		$('#pf tbody tr').each(function() {
			if( $(this).find('input[name="pf_nominee_name[]"]').val() == '' || $(this).find('input[name="pf_nominee_relation[]"]').val() == '' || $(this).find('input[name="pf_nominee_ratio[]"]').val() == '' ) {
				error = true;
				$(this).find('input[name="pf_nominee_name[]"]').focus();
				$('#messagebox').html('Please Provide All Nominee Information.').addClass('messageboxerror');
			}
			else {
				field_array['pf_nominee'][correct_pf_nominee_counter] =[{
					'pf_nominee_name'		: $(this).find('input[name="pf_nominee_name[]"]').val(),
					'pf_nominee_relation'	: $(this).find('input[name="pf_nominee_relation[]"]').val(),
					'pf_nominee_ratio'		: $(this).find('input[name="pf_nominee_ratio[]"]').val()
				}];
				correct_pf_nominee_counter++;
			}
		});
	}
	
	if( $('#gi_entitled').is(':checked') ) {
		field_array['gi_entitled'] = 1;
		
		if( $('#gi_effective_date').val() == '' ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#gi_effective_date').focus();
				$(this).html('Please Provide Group Insurance Effective Date.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else field_array['gi_effective_date'] = $('#gi_effective_date').val();
		
		$('#gi tbody tr').each(function() {
			if( $(this).find('input[name="gi_nominee_name[]"]').val() == '' && $(this).find('input[name="gi_nominee_relation[]"]').val() == '' && $(this).find('input[name="gi_nominee_ratio[]"]').val() == '' ) {
				if( this != this.parentNode.firstChild ) $(this).remove();
			}
		});
		var correct_gi_nominee_counter = 0;
		field_array['gi_nominee'] = new Array();
		$('#gi tbody tr').each(function() {
			if( $(this).find('input[name="gi_nominee_name[]"]').val() == '' || $(this).find('input[name="gi_nominee_relation[]"]').val() == '' || $(this).find('input[name="gi_nominee_ratio[]"]').val() == '' ) {
				error = true;
				$(this).find('input[name="gi_nominee_name[]"]').focus();
				$('#messagebox').html('Please Provide All Nominee Information.').addClass('messageboxerror');
			}
			else {
				field_array['gi_nominee'][correct_gi_nominee_counter] = [{
					'gi_nominee_name'		: $(this).find('input[name="gi_nominee_name[]"]').val(),
					'gi_nominee_relation'	: $(this).find('input[name="gi_nominee_relation[]"]').val(),
					'gi_nominee_ratio'		: $(this).find('input[name="gi_nominee_ratio[]"]').val()
				}];
				correct_gi_nominee_counter++;
			}
		});
	}
	
	if( error == false ) {
		nocache = Math.random();
		var data = '';
		for( var key in field_array ) {
			if( key != 'pf_nominee' && key != 'gi_nominee' ) data += '&' + key + '=' + field_array[key];
		}
		if( $('#pf_entitled').is(':checked') ) {
			data += '&pf_nominee_counter=' + $('#pf tbody tr').length;
			var i = 0;
			$('#pf tbody tr').each(function() {
				data += '&pf_nominee_' + i + '=' + $(this).find('input[name="pf_nominee_name[]"]').val()
												+ '_' + $(this).find('input[name="pf_nominee_relation[]"]').val()
												+ '_' + $(this).find('input[name="pf_nominee_ratio[]"]').val();
			});
		}
		if( $('#gi_entitled').is(':checked') ) {
			data += '&gi_nominee_counter=' + $('#gi tbody tr').length;
			var i = 0;
			$('#gi tbody tr').each(function() {
				data += '&gi_nominee_' + i + '=' + $(this).find('input[name="gi_nominee_name[]"]').val()
												+ '_' + $(this).find('input[name="gi_nominee_relation[]"]').val()
												+ '_' + $(this).find('input[name="gi_nominee_ratio[]"]').val();
			});
		}
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_entitlement' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_entitlement;
		http.send(null);
	}
}
//Employee Entitlement Response of Employee Information ( Save / Update )
function response_emp_entitlement() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Experience of Employee Information ( Save / Update )
function fnc_emp_experience() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = true, counter = 0, data = '&emp_code=' + $('#emp_code').val();
	
	$('#tbl_experience tbody tr').each(function() {
		if( $(this).find('input[name="organization_name[]"]').val() != '' && $(this).find('input[name="designation[]"]').val() != ''
			&& $(this).find('input[name="joining_date[]"]').val() != '' && $(this).find('input[name="resigning_date[]"]').val() != '' ) {
			error = false;
			data += '&' + 'organization_name_' + counter + '=' + $(this).find('input[name="organization_name[]"]').val()
					+ '&' + 'designation_' + counter + '=' + $(this).find('input[name="designation[]"]').val()
					+ '&' + 'joining_date_' + counter + '=' + $(this).find('input[name="joining_date[]"]').val()
					+ '&' + 'resigning_date_' + counter + '=' + $(this).find('input[name="resigning_date[]"]').val()
					+ '&' + 'service_length_' + counter + '=' + $(this).find('input[name="service_length[]"]').val()
					+ '&' + 'gross_salary_' + counter + '=' + $(this).find('input[name="gross_salary[]"]').val();
			counter++;
		}
	});
	data += '&counter=' + counter;
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_experience' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_experience;
		http.send(null);
	}
	else {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#tbl_experience tbody input:first').focus();
			$(this).html('Please Enter Atleast one correct experience with first four mandatory fields.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
}
//Employee Experience Response of Employee Information ( Save / Update )
function response_emp_experience() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Education of Employee Information ( Save / Update )
function fnc_emp_education() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = true, counter = 0, data = '&emp_code=' + $('#emp_code').val();
	
	$('#tbl_education tbody tr').each(function() {
		if( $(this).find('input[name="exam_name[]"]').val() != '' && $(this).find('input[name="result[]"]').val() != '' ) {
			error = false;
			data += '&' + 'exam_name_' + counter + '=' + $(this).find('input[name="exam_name[]"]').val()
					+ '&' + 'board_' + counter + '=' + $(this).find('input[name="board[]"]').val()
					+ '&' + 'institution_' + counter + '=' + $(this).find('input[name="institution[]"]').val()
					+ '&' + 'discipline_' + counter + '=' + $(this).find('input[name="discipline[]"]').val()
					+ '&' + 'major_subject_' + counter + '=' + $(this).find('input[name="major_subject[]"]').val()
					+ '&' + 'passing_year_' + counter + '=' + $(this).find('input[name="passing_year[]"]').val()
					+ '&' + 'result_' + counter + '=' + $(this).find('input[name="result[]"]').val();
			counter++;
		}
	});
	data += '&counter=' + counter;
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_education' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_education;
		http.send(null);
	}
	else {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#tbl_experience tbody input:first').focus();
			$(this).html('Please enter atleast one correct education info with first and last mandatory fields.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
}
//Employee Education Response of Employee Information ( Save / Update )
function response_emp_education() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Family of Employee Information ( Save / Update )
function fnc_emp_family() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = true, counter = 0, data = '&emp_code=' + $('#emp_code').val();
	
	$('#tbl_family tbody tr').each(function() {
		if( $(this).find('input[name="fname[]"]').val() != '' && $(this).find('input[name="frelation[]"]').val() != '' && $(this).find('input[name="fdob[]"]').val() != '' ) {
			error = false;
			data += '&' + 'name_' + counter + '=' + $(this).find('input[name="fname[]"]').val()
					+ '&' + 'relation_' + counter + '=' + $(this).find('input[name="frelation[]"]').val()
					+ '&' + 'dob_' + counter + '=' + $(this).find('input[name="fdob[]"]').val()
					+ '&' + 'occupation_' + counter + '=' + $(this).find('input[name="foccupation[]"]').val()
					+ '&' + 'contact_no_' + counter + '=' + $(this).find('input[name="fcontact_no[]"]').val();
			counter++;
		}
	});
	data += '&counter=' + counter;
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=emp_family' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_emp_family;
		http.send(null);
	}
	else {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#tbl_family tbody input:first').focus();
			$(this).html('Please enter atleast one correct family info with first three mandatory fields.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
}
//Employee Family Response of Employee Information ( Save / Update )
function response_emp_family() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Saved Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
	}
}
//Employee Job Separation ( Save / Update )
function fnc_job_separation() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, counter = 0, data = '&chk_values=';
	
	if( $('#separated_from').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$('#separated_from').focus();
			$(this).html('Please enter a date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#separation_type').val() == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$('#separation_type').focus();
			$(this).html('Please select separation type.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		var employee_counter = 0;
		$('#tbl_separation tbody input[type="checkbox"]:checked').each(function() {
			data += $(this).val() + '_';
			employee_counter++;
		});
		data = data.substr( 0, data.length - 1 );
		
		if( employee_counter == 0 ) {
			$('#messagebox').fadeTo( 200, 0.1, function() {
				error = true;
				$(this).html('Please select at least one employee.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else data += '&separated_from=' + $('#separated_from').val() + '&separation_type=' + $('#separation_type').val() + '&cause_of_separation=' + $('#cause_of_separation').val();
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=job_separation' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_job_separation;
		http.send(null);
	}
}
//Employee Job Separation ( Save / Update )
function response_job_separation() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' employee/(s) has been separated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		populate_data();
	}
}
//Employee Job Reactivation ( Save / Update )
function fnc_job_reactivation() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, counter = 0, data = '&chk_values=';
	
	if( $('#rejoin_date').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$('#rejoin_date').focus();
			$(this).html('Please enter a date.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		var employee_counter = 0;
		$('#tbl_reactivation tbody input[type="checkbox"]:checked').each(function() {
			data += $(this).val() + '_';
			employee_counter++;
		});
		data = data.substr( 0, data.length - 1 );
		
		if( employee_counter == 0 ) {
			$('#messagebox').fadeTo( 200, 0.1, function() {
				error = true;
				$(this).html('Please select at least one employee.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else data += '&rejoin_date=' + $('#rejoin_date').val() + '&service_benefit_from=' + $('#service_benefit_from').val();
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=job_reactivation' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_job_reactivation;
		http.send(null);
	}
}
//Employee Job Reactivation ( Save / Update )
function response_job_reactivation() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' employee/(s) has been reactivated successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		populate_data();
	}
}
//Policy Tagging ( Save / Update )
function fnc_policy_tagging() {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, employee_counter = policy_counter = 0, data = employees = policy = '';
	
	$('#tbl_policy_tagging tbody input[type="checkbox"]:checked').each(function() {
		employees += $(this).val() + '_';
		employee_counter++;
	});
	if( employee_counter == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			error = true;
			$(this).html('Please select at lease one employee for policy tagging.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	
	if( error == false ) {
		$('input[name="policy[]"]:checked').each(function() {
			policy_counter++;
			if( $('select[name="' + $(this).val() + '_policy"]').val() == 0 ) {
				error = true;
				alert( 'Please specify ' + $(this).val() + ' policy.' );
			}
			else policy += $(this).val() + ':' + $('select[name="' + $(this).val() + '_policy"]').val() + '_';
		});
	}
	if( error == false ) {
		if( policy_counter == 0 ) {
			$('#messagebox').fadeTo( 200, 0.1, function() {
				error = true;
				$(this).html('Please select at lease one policy for policy tagging.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
	}
	
	if( error == false ) {
		employees = employees.substr( 0, employees.length - 1 );
		policy = policy.substr( 0, policy.length - 1 );
		data = '&employees=' + employees + '&policy=' + policy;
		
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_hr_admin.php?action=policy_tagging' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_policy_tagging;
		http.send(null);
	}
}
//Policy Tagging Response ( Save / Update )
function response_policy_tagging() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		alert(http.responseText);
		if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( response[1] + ' policy/(s) has been tagged successfully with ' + response[2] + ' employee/(s).' ).addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
			});
		}
		reset_form();
	}
}
//Disciplinary Info
function fnc_disciplinary_information() {
	var txt_emp_code				= escape(document.getElementById('txt_emp_code').value);
	var txt_emp_name				= escape(document.getElementById('txt_emp_name').value);
	var txt_designation				= escape(document.getElementById('txt_designation').value);
	var txt_company					= escape(document.getElementById('txt_company').value);
	var txt_division				= escape(document.getElementById('txt_division').value);
	var txt_department				= escape(document.getElementById('txt_department').value);
	var txt_section					= escape(document.getElementById('txt_section').value);
	var txt_sub_section				= escape(document.getElementById('txt_sub_section').value);
	var txt_occurrence_date			= escape(document.getElementById('txt_occurrence_date').value);
	var txt_action_date				= escape(document.getElementById('txt_action_date').value);
	var txt_occurrence_details		= escape(document.getElementById('txt_occurrence_details').value);
	var txt_investigating_members	= escape(document.getElementById('txt_investigating_members').value);
	var txt_investigation			= escape(document.getElementById('txt_investigation').value);
	var cbo_action_taken			= escape(document.getElementById('cbo_action_taken').value);
	var txt_withdawn_date			= escape(document.getElementById('txt_withdawn_date').value);
	var save_up						= escape(document.getElementById('save_up_disc').value);
	
	var tbl_row_count=document.getElementById('td_sal_head').rows.length;
	//tbl_row_count=tbl_row_count-2;
	var cbo_impact_salary_head="";
	var txt_impact_salary_head_perc="";
	
	for( s=1; s<=tbl_row_count; s++) {
		var cbo_impact_salary_head_data		= escape(document.getElementById('cbo_impact_salary_head'+s).value);
		var txt_impact_salary_head_perc_data		= escape(document.getElementById('txt_impact_salary_head_perc'+s).value);
		cbo_impact_salary_head+=cbo_impact_salary_head_data + ",";
		txt_impact_salary_head_perc+=txt_impact_salary_head_perc_data + ",";
	}
	
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#txt_emp_code').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_emp_code').focus();
			$(this).html('Please Select Employee Information').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#txt_occurrence_date').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_occurrence_date').focus();
			$(this).html('Please Select Occurance Date').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		http.open('get','includes/save_update_hr_admin.php?action=disciplinary_information&isupdate='+save_up+
					'&txt_emp_code='+txt_emp_code+
					'&txt_emp_name='+txt_emp_name+
					'&txt_designation='+txt_designation+
					'&txt_company='+txt_company+
					'&txt_division='+txt_division+
					'&txt_department='+txt_department+
					'&txt_section='+txt_section+
					'&txt_occurrence_date='+txt_occurrence_date+
					'&txt_action_date='+txt_action_date+
					'&txt_occurrence_details='+txt_occurrence_details+
					'&txt_investigating_members='+txt_investigating_members+
					'&txt_investigation='+txt_investigation+
					'&cbo_action_taken='+cbo_action_taken+
					'&txt_withdawn_date='+txt_withdawn_date+
					'&cbo_impact_salary_head='+cbo_impact_salary_head+
					'&txt_impact_salary_head_perc='+txt_impact_salary_head_perc);
		http.onreadystatechange = disciplinary_information_reply;
		http.send(null); 
	}
}
//Disciplinary Info Response
function disciplinary_information_reply() {
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		
		alert(response);
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Data, Please Check.').addClass('messageboxerror').fadeTo(900,1);
				// document.getElementById('save_up').value="";
			});
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
							
		
	//alert(response);
	
	}
}