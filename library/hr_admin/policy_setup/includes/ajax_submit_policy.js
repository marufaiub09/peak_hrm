//Do not change 
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

//Shift Policy ( Save / Update )
function fnc_shift_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	var fields = new Array( 'shift_id','shift_name','cross_date','shift_type','shift_start','shift_end','grace_minutes','exit_buffer_minutes','early_out_start','entry_restriction_start',
						   'first_break_start','first_break_end','second_break_start','second_break_end','lunch_time_start','lunch_time_end','short_shift_name','status_active' );
	
	for( var i = 0; i < fields.length; i++ ) {
		field_array[i] 	= new Array( fields[i], $('#' + fields[i]).val() );
	}
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if ($('#shift_id').val()=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if ($('#shift_id').val()!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}	
	else if( $('#shift_name').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#shift_name').focus();
			$(this).html('Shift Name is necessary.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#shift_start').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#shift_start').focus();
			$(this).html('Shift Start is necessary.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#shift_end').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#shift_end').focus();
			$(this).html('Shift End is necessary.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#grace_minutes').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#grace_minutes').focus();
			$(this).html('Grace Minutes is necessary.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#early_out_start').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#early_out_start').focus();
			$(this).html('Early Out Start is necessary.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( $('#entry_restriction_start').val() == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#entry_restriction_start').focus();
			$(this).html('Entry Restriction Start is necessary').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + field_array[i][1];
		}
		http.open( 'GET', 'includes/save_update_policy.php?action=shift_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_shift_policy;
		http.send(null);
	}
}
//Shift Policy response ( Save / Update )
function response_shift_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response);
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','shift_policy','update','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#shift_id option[value="' + response[1] + '"]').text( field_array[1][1] );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','shift_policy','save','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#shift_id').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}
		
		shift_policy[response[1]] = new Array();
		shift_policy[response[1]]['id'] = response[1];
		for( var i = 0; i < field_array.length; i++ ) {
			shift_policy[response[1]][field_array[i][0]] = field_array[i][1];
		}
		populate_data();
		reset_form();
	}
}
//Overtime Policy ( Save / Update )
function fnc_overtime_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '&policy_id=' + $('#policy_id').val(), correct_slab_counter = 0, total_slot = 0, above = 0;
	var is_update=$('#is_update').val();//for check update
	if (is_update=='' && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&policy_name=' + escape( $('#policy_name').val() );
	
	if( error == false && $('#overtime_rate').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#overtime_rate').focus();
			$(this).html('Overtime rate can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&overtime_rate=' + $('#overtime_rate').val();
	
	if( error == false && $('#max_overtime').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#max_overtime').focus();
			$(this).html('Max overtime can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&max_overtime=' + $('#max_overtime').val();
	
	if( error == false && $('#shift_id').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#shift').focus();
			$(this).html('Please select at least one shift.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&shift_id=' + $('#shift_id').val();
	
	data += '&overtime_calculation_rule=' + $('#overtime_calculation_rule').val();
	data += '&status_active=' + $('#status_active').val();
	
	if( error == false ) {
		var correct_slab_counter = 0;
		for( var i = 1; i <= 5; i++ ) {
			if( $('#overtime_slab_' + i).val() != '' && $('#overtime_slot_' + i).val() != '' && $('#overtime_multiplier_' + i).val() != '' && $('#overtime_head_' + i).val() != 0 ) {
				correct_slab_counter++;
				if( $('#overtime_slot_' + i).val() == 'Above' ) above = i;
				else total_slot += parseInt( $('#overtime_slot_' + i).val() );
				data += '&slab' + i + '=' + $('#overtime_slot_' + i).val() + '_' + $('#overtime_multiplier_' + i).val() + '_' + $('#overtime_head_' + i).val();
			}
		}
		if( correct_slab_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#overtime_slab_1').focus();
				$(this).html('Please provide at least one correct slab.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		/*else {
			if( ( above == 0 && total_slot != parseInt( $('#max_overtime').val() ) ) || ( above != 0 && total_slot >= parseInt( $('#max_overtime').val() ) ) ) {
				error = true;
				$('#messagebox').fadeTo( 200, 0.1, function() {
					$('#overtime_slab_1').focus();
					$(this).html('Max OT and overtime slab do not match.').addClass('messageboxerror').fadeTo(900,1);
				});
			}
		}*/
		data += '&slab_counter=' + correct_slab_counter;
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=overtime_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_overtime_policy;
		http.send(null);
	}
}
//Overtime Policy Response ( Save / Update )
function response_overtime_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('This Shift Policy Has Already Attached With Overtime Policy.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Saved Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				if (response[3]!='')
				{
					save_activities_history(response[3],'library','overtime_policy','save','../../../');
				}
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + response[2] + '</option>' );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Updated Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				if (response[3]!='')
				{
					save_activities_history(response[3],'library','overtime_policy','update','../../../');
				}
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( response[2] );
		}
		populate_data();
		reset_form();
	}
}
//Holiday Incentive Policy ( Save / Update )
function fnc_holiday_incentive_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '&policy_id=' + $('#policy_id').val(), correct_incentive_counter = 0;
	var is_update=$('#is_update').val();//for check update
	
	if (is_update=='' && save_perm==2)
	{
		 error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&policy_name=' + escape( $('#policy_name').val() );
	
	if( error == false && $('#min_working_hour').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#min_working_hour').focus();
			$(this).html('Please provide minimum working hour.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&min_working_hour=' + $('#min_working_hour').val();
	
	if( error == false && $('#earning_head').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#earning_head').focus();
			$(this).html('Please select an earning head.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&earning_head=' + $('#earning_head').val();
	
	data += '&pay_for=' + $('#pay_for').val();
	data += '&status_active=' + $('#status_active').val();
	
	if( error == false ) {
		$('#holiday_incentive tbody tr').each( function() {
			if( ( $(this).find('select[name="parameter_type[]"]').val() == 0 && $(this).find('input[name="percentage_formula[]"]').val() != '' && $(this).find('select[name="base_head[]"]').val() != 0 )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 1 && $(this).find('input[name="percentage_formula[]"]').val() != '' )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 2 && $(this).find('input[name="amount[]"]').val() != '' ) ) {
					correct_incentive_counter++;
					
					if($(this).find('select[name="parameter_type[]"]').val() == 0 || $(this).find('select[name="parameter_type[]"]').val() == 2)
				{
				data += '&incentive' + correct_incentive_counter + '=' + $(this).find('select[name="parameter_type[]"]').val() + '_' + encodeURIComponent( $(this).find('input[name="percentage_formula[]"]').val() )
						+ '_' + $(this).find('select[name="base_head[]"]').val() + '_' + $(this).find('input[name="amount[]"]').val();
				}
				else
				{
				data += '&incentive' + correct_incentive_counter + '=' + $(this).find('select[name="parameter_type[]"]').val() + '_' + encodeURIComponent( $(this).find('select[name="percentage_formula[]"]').val() )
		+ '_' + $(this).find('select[name="base_head[]"]').val() + '_' + $(this).find('input[name="amount[]"]').val();
	
				}
			}
		});
		if( correct_incentive_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#holiday_incentive tbody tr:first').find('select[name="parameter_type[]"]').focus();
				$(this).html('Please provide at least one correct incentive.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		data += '&incentive_counter=' + correct_incentive_counter;
	}
	//alert(data);return false;
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=holiday_incentive_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_holiday_incentive_policy;
		http.send(null);
	}
}
//Holiday Incentive Policy Response ( Save / Update )
function response_holiday_incentive_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Saved Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				if (response[3]!='')
				{
					save_activities_history(response[3],'library','holiday_incentive_policy','save','../../../');
				}
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + response[2] + '</option>' );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Updated Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				if (response[3]!='')
				{
					save_activities_history(response[3],'library','holiday_incentive_policy','update','../../../');
				}
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( response[2] );
		}
		populate_data();
		reset_form();
	}
}
//Attendance Bonus Policy ( Save / Update )
function fnc_attendance_bonus_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '&policy_id=' + $('#policy_id').val(), correct_criteria_counter = 0;
	
	var is_update=$('#is_update').val();//for check update
	if (is_update=='' && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (is_update!='' && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&policy_name=' + escape( $('#policy_name').val() );
	
	if( error == false && $('#earning_head').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#earning_head').focus();
			$(this).html('Please select an earning head.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&earning_head=' + $('#earning_head').val();
	
	data += '&status_active=' + $('#status_active').val();
	
	if( error == false ) {
		$('#attendance_bonus tbody tr').each( function() {
			if( $(this).find('input[name="total_leave_criteria[]"]').val() != '' && $(this).find('input[name="cl_leave_criteria[]"]').val() != '' && $(this).find('input[name="sl_leave_criteria[]"]').val() != '' && $(this).find('input[name="lwp_leave_criteria[]"]').val() != '' && $(this).find('input[name="spl_leave_criteria[]"]').val() != '' && $(this).find('input[name="el_leave_criteria[]"]').val() != '' && $(this).find('input[name="ml_leave_criteria[]"]').val() != '' && $(this).find('input[name="late_criteria[]"]').val() != '' && $(this).find('input[name="absent_criteria[]"]').val() != ''&& $(this).find('input[name="amount[]"]').val() != '' )
				 {
				correct_criteria_counter++;
				data += '&criteria' + correct_criteria_counter + '=' + $(this).find('input[name="total_leave_criteria[]"]').val()+ '_' + $(this).find('input[name="cl_leave_criteria[]"]').val()+ '_' + $(this).find('input[name="sl_leave_criteria[]"]').val()+ '_' + $(this).find('input[name="lwp_leave_criteria[]"]').val()+ '_' + $(this).find('input[name="spl_leave_criteria[]"]').val()+ '_' + $(this).find('input[name="el_leave_criteria[]"]').val()+ '_' + $(this).find('input[name="ml_leave_criteria[]"]').val()+'_' + $(this).find('input[name="late_criteria[]"]').val()+ '_' + $(this).find('input[name="absent_criteria[]"]').val() + '_' + $(this).find('input[name="amount[]"]').val();
			}
			
		});
		if( correct_criteria_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#attendance_bonus tbody tr:first').find('input[name="cl_leave_criteria[]"]').focus();
				$(this).html('Please provide at least one correct criteria.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		

		data += '&criteria_counter=' + correct_criteria_counter;
	}
	//alert(data);
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=attendance_bonus_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_attendance_bonus_policy;
		http.send(null);
	}
}
//Attendance Bonus Policy Response ( Save / Update )
function response_attendance_bonus_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		//alert(response[0]);
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Attendance Bonus Policy Attached!!!Change Not Allow').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Saved Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','attendance_bonus_policy','save','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + response[2] + '</option>' );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Updated Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','attendance_bonus_policy','update','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( response[2] );
		}
		populate_data();
		reset_form();
	}
}
//Absent Deduction Policy ( Save / Update )
function fnc_absent_deduction_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '&policy_id=' + $('#policy_id').val(), correct_criteria_counter = 0;
	
	if ($('#policy_id').val()==0 && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if ($('#policy_id').val()!=0 && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&policy_name=' + escape( $('#policy_name').val() );
	
	if( error == false && $('#deduction_head').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#deduction_head').focus();
			$(this).html('Please select a deduction head.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&deduction_head=' + $('#deduction_head').val();
	
	data += '&status_active=' + $('#status_active').val();
	
	if( error == false ) {
		$('#absent_deduction tbody tr').each( function() {
			if( ( $(this).find('select[name="parameter_type[]"]').val() == 0 && $(this).find('input[name="percentage_formula[]"]').val() != '' && $(this).find('select[name="base_head[]"]').val() != 0 )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 1 && $(this).find('input[name="percentage_formula[]"]').val() != '' )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 2 && $(this).find('input[name="amount[]"]').val() != '' ) ) {
				correct_criteria_counter++;
				data += '&criteria' + correct_criteria_counter + '=' + $(this).find('select[name="parameter_type[]"]').val() + '_' + $(this).find('input[name="percentage_formula[]"]').val()
						+ '_' + $(this).find('select[name="base_head[]"]').val() + '_' + $(this).find('input[name="amount[]"]').val();
			}
		});
		if( correct_criteria_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#absent_deduction tbody tr:first').find('select[name="parameter_type[]"]').focus();
				$(this).html('Please provide at least one correct criteria.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		data += '&criteria_counter=' + correct_criteria_counter;
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=absent_deduction_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_absent_deduction_policy;
		http.send(null);
	}
}
//Absent Deduction Policy Response ( Save / Update )
function response_absent_deduction_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Attendance Bonus Policy Attached!!!Change Not Allow').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Saved Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);				
				
					save_activities_history('','library','absent_deduction_policy','save','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + response[2] + '</option>' );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Updated Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','absent_deduction_policy','update','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( response[2] );
		}
		populate_data();
		reset_form();
	}
}
//Late Deduction Policy ( Save / Update )
function fnc_late_deduction_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	//alert("ok..");
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '&policy_id=' + $('#policy_id').val(), correct_criteria_counter = 0;
	
	if ($('#policy_id').val()==0 && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if ($('#policy_id').val()!=0 && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&policy_name=' + escape( $('#policy_name').val() );
	
	if( error == false && $('#deduct_for').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#deduct_for').focus();
			$(this).html('Deduct For can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&deduct_for=' + escape( $('#deduct_for').val() );
	
	if( error == false && $('#deduction_head').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#deduction_head').focus();
			$(this).html('Please select a deduction head.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&deduction_head=' + $('#deduction_head').val();
	
	data += '&status_active=' + $('#status_active').val();
	
	if( error == false ) {
		$('#late_deduction tbody tr').each( function() {
			if( $(this).find('select[name="late_slot[]"]').val() != 0
				&& (
				( $(this).find('select[name="parameter_type[]"]').val() == 0 && $(this).find('input[name="percentage_formula[]"]').val() != '' && $(this).find('select[name="base_head[]"]').val() != 0 )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 1 && $(this).find('input[name="percentage_formula[]"]').val() != '' )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 2 && $(this).find('input[name="amount[]"]').val() != '' ) )
				) {
				correct_criteria_counter++;
				data += '&criteria' + correct_criteria_counter + '=' + $(this).find('select[name="late_slot[]"]').val() + '_' + $(this).find('select[name="parameter_type[]"]').val()
						+ '_' + $(this).find('input[name="percentage_formula[]"]').val() + '_' + $(this).find('select[name="base_head[]"]').val() + '_' + $(this).find('input[name="amount[]"]').val();
			}
		});
		if( correct_criteria_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#late_deduction tbody tr:first').find('select[name="late_slot[]"]').focus();
				$(this).html('Please provide at least one correct criteria.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		data += '&criteria_counter=' + correct_criteria_counter;
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=late_deduction_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_late_deduction_policy;
		http.send(null);
	}
}
//Late Deduction Policy Response ( Save / Update )
function response_late_deduction_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Attendance Bonus Policy Attached!!!Change Not Allow').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Saved Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','late_deduction_policy','save','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + response[2] + '</option>' );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Updated Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','late_deduction_policy','update','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( response[2] );
		}
		populate_data();
		reset_form();
	}
}
//Eary Deduction Policy ( Save / Update )
function fnc_early_deduction_policy(save_perm,edit_perm,delete_perm,approve_perm) {
	//alert("ok....");
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	var error = false, data = '&policy_id=' + $('#policy_id').val(), correct_criteria_counter = 0;
	
	if ($('#policy_id').val()==0 && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if ($('#policy_id').val()!=0 && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( $('#status_active').val()==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( $('#policy_name').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#policy_name').focus();
			$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&policy_name=' + escape( $('#policy_name').val() );
	
	if( error == false && $('#deduct_for').val() == '' ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#deduct_for').focus();
			$(this).html('Deduct For can not be empty.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&deduct_for=' + escape( $('#deduct_for').val() );
	
	if( error == false && $('#deduction_head').val() == 0 ) {
		error = true;
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#deduction_head').focus();
			$(this).html('Please select a deduction head.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else data += '&deduction_head=' + $('#deduction_head').val();
	
	data += '&status_active=' + $('#status_active').val();
	
	if( error == false ) {
		$('#early_deduction tbody tr').each( function() {
			if( $(this).find('select[name="early_slot[]"]').val() != 0
				&& (
				( $(this).find('select[name="parameter_type[]"]').val() == 0 && $(this).find('input[name="percentage_formula[]"]').val() != '' && $(this).find('select[name="base_head[]"]').val() != 0 )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 1 && $(this).find('input[name="percentage_formula[]"]').val() != '' )
				|| ( $(this).find('select[name="parameter_type[]"]').val() == 2 && $(this).find('input[name="amount[]"]').val() != '' ) )
				) {
				correct_criteria_counter++;
				data += '&criteria' + correct_criteria_counter + '=' + $(this).find('select[name="early_slot[]"]').val() + '_' + $(this).find('select[name="parameter_type[]"]').val()
						+ '_' + $(this).find('input[name="percentage_formula[]"]').val() + '_' + $(this).find('select[name="base_head[]"]').val() + '_' + $(this).find('input[name="amount[]"]').val();
			}
		});
		if( correct_criteria_counter == 0 ) {
			error = true;
			$('#messagebox').fadeTo( 200, 0.1, function() {
				$('#early_deduction tbody tr:first').find('select[name="early_slot[]"]').focus();
				$(this).html('Please provide at least one correct criteria.').addClass('messageboxerror').fadeTo(900,1);
			});
		}
		data += '&criteria_counter=' + correct_criteria_counter;
	}
	
	if( error == false ) {
		nocache = Math.random();
		http.open( 'GET', 'includes/save_update_policy.php?action=early_deduction_policy' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_early_deduction_policy;
		http.send(null);
	}
}
//Early Deduction Policy Response ( Save / Update )
function response_early_deduction_policy() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Attendance Bonus Policy Attached!!!Change Not Allow').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 1 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Saved Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','early_deduction_policy','save','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id').append( '<option value="' + response[1] + '">' + response[2] + '</option>' );
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html( 'Data Updated Successfully.' ).addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','early_deduction_policy','update','../../../');
				
				$(this).fadeOut(5000);
			});
			$('#policy_id option[value="' + response[1] + '"]').text( response[2] );
		}
		populate_data();
		reset_form();
	}
}


//--------------------------Duty Roster Policy Start-----------------------------------------------------------------------
function fnc_duty_roster_policy(save_perm,edit_perm,delete_perm,approve_perm){
	var cbo_company_name		= escape(document.getElementById('cbo_company_name').value);
	var txt_roster_rule_name	= escape(document.getElementById('txt_roster_rule_name').value);
	var cbo_change_on_weekend	= escape(document.getElementById('cbo_change_on_weekend').value);
	var cbo_new_shift_on_weekend	= escape(document.getElementById('cbo_new_shift_on_weekend').value);
	
	var cbo_shift_name			= escape(document.getElementById('cbo_shift_name').value);
	var txt_shift_days			= escape(document.getElementById('txt_shift_days').value);
	var cbo_next_shift			= escape(document.getElementById('cbo_next_shift').value);
	var save_up 				= escape(document.getElementById('save_up').value);
	var txt_roster_rule_id 		= escape(document.getElementById('txt_roster_rule_id').value);

	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#cbo_shift_name').val()== $('#cbo_next_shift').val()){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_pop3_server').focus();
			$(this).html('Please Select Different Shift Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});		
	}
	else{					
		nocache = Math.random();
		http.open('get','includes/save_update_policy.php?action=duty_roster_policy&isupdate='+save_up+
					'&cbo_company_name='+cbo_company_name+
					'&txt_roster_rule_name='+txt_roster_rule_name+
					'&cbo_change_on_weekend='+cbo_change_on_weekend+
					'&cbo_shift_name='+cbo_shift_name+
					'&txt_shift_days='+txt_shift_days+
					'&cbo_next_shift='+cbo_next_shift+
					'&txt_roster_rule_id='+txt_roster_rule_id+
					'&cbo_new_shift_on_weekend='+cbo_new_shift_on_weekend+
					'&nocache ='+nocache);
		//alert(&cbo_is_deleted);
		http.onreadystatechange = duty_roster_policy_Reply_info;
		http.send(null); 
	}
}

function duty_roster_policy_Reply_info() {
	if(http.readyState == 4){ 		
		//var response = http.responseText;	
	 //alert(http.responseText);
		var response = http.responseText.split('_');	
		//if( response[0] == 3 )
	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('',"library","duty_roster_policy","insert","../../../");
				$(this).fadeOut(10000);
				
				showResult_search(document.getElementById('cbo_company_name').value,document.getElementById('txt_roster_rule_id').value,'duty_roster_policy','duty_roster');
				document.getElementById('save_up').value="";
				document.getElementById('txt_roster_rule_id').value=response[1];
				document.getElementById('cbo_shift_name').value=0;
				document.getElementById('txt_shift_days').value="";
				document.getElementById('cbo_next_shift').value=0;
				document.getElementById('cbo_change_on_weekend').value=0;
				document.getElementById('cbo_shift_name').focus();
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(10000);				
				showResult_search(document.getElementById('cbo_company_name').value,document.getElementById('txt_roster_rule_id').value,'duty_roster_policy','duty_roster');
				document.getElementById('save_up').value="";
				document.getElementById('cbo_shift_name').value=0;
				document.getElementById('txt_shift_days').value="";
				document.getElementById('cbo_next_shift').value=0;
				document.getElementById('cbo_change_on_weekend').value=0;
				save_activities_history('',"library","duty_roster_policy","update","../../../");
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Found!!!Duplicate Roster Name.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(10000);
			});
		}
	}
}	

//------------------------- Duty Roster Policy End-----------------------------------------------------------------------

