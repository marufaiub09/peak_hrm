<?php
/*************************************
|	Developed By	: Md. Nuruzzaman
|	Date			: 08.12.2013
**************************************/ 
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include("../../../../includes/common.php");
include("../../../../includes/array_function.php");
include("../../../../includes/common_functions.php");
extract($_GET);

//Designation array
$sql = "SELECT * FROM lib_designation WHERE is_deleted = 0 and status_active=1 ORDER BY custom_designation ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$designation_chart = array();
while( $row = mysql_fetch_assoc( $result )){
$designation_chart[$row['id']] = $row['custom_designation'];
}
//Company array
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$company_details[$row['id']] = $row['company_name'];
}
//Department array
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 and status_active=1 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$department_details[$row['id']] = $row['department_name'];
}
//Diviion
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 and status_active=1 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$division_details[$row['id']] = $row['division_name'];
}
//Location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 and status_active=1 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$location_details[$row['id']] =$row['location_name'];
}
//Section
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 and status_active=1 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$section_details[$row['id']] = $row['section_name'];
}
//Sub Section
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 and status_active=1 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$subsection_details[$row['id']] = $row['subsection_name'];
}
//Division 
$sql = "SELECT * FROM lib_list_division WHERE status_active=1 and is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$lib_list_division = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$lib_list_division[$row['id']] = $row['division_name'];
}
//District
$sql = "SELECT * FROM lib_list_district WHERE status_active=1 and is_deleted = 0 ORDER BY district_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$lib_list_district = array();
while( $row = mysql_fetch_assoc( $result ) ) {
$lib_list_district[$row['id']] = $row['district_name'];
}
//	Training Name
$sql="SELECT * FROM  lib_training";
$result=mysql_query($sql);
$training_details=array();
while($row=mysql_fetch_assoc($result)){$training_details[$row['id']]=$row['short_name'];}
?>

<style>
a{ color:#0000FF;}
a:hover{ color:#900;}
</style>

<?php
if($type=="generate_piece_rate_ot_chart")
{ 
	echo "su..re"."<br>";
?>            <table id="scroll_body" cellspacing="0" cellpadding="0" class="rpt_table">
                <thead>                            
                    <th width="50">Sl No</th>
                    <th width="150">System Code</th>
                    <th width="180">ID Card No</th>
                    <th width="120">Employee Name</th>
                    <th width="120">Grade</th>
                    <th width="180">Designation</th>
                    <th width="120">Rate</th>
                </thead>
			</table>
<?php		
			$html=ob_get_contents();		
			ob_clean();
			//for report temp file delete 
			foreach (glob( "tmp_report_file/"."*.xls") as $filename) {			
			@unlink($filename);
			}		
			//html to xls convert
			$name=time();
			$name="$name".".xls";	
			$create_new_excel = fopen('tmp_report_file/'.$name, 'w');	
			$is_created = fwrite($create_new_excel,$html);
			echo "$html"."####"."$name"."####".$small_print;		
			exit();
}// end if condition
?>