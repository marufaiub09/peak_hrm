<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 31-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

//absent_deduction_policy
$sql = "SELECT * FROM lib_policy_absent_deduction ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$absent_deduction_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$absent_deduction_policy[$row['id']]				= $row;
	$absent_deduction_policy[$row['id']]['criteria']	= array();
	
	$sql2 = "SELECT * FROM lib_policy_absent_deduction_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$absent_deduction_policy[$row['id']]['criteria'][] =	array(
																	'parameter_type'		=> $row2['parameter_type'],
																	'percentage_formula'	=> $row2['percentage_formula'],
																	'base_head'				=> $row2['base_head'],
																	'amount'				=> $row2['amount']
																);
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_policy.js" type="text/javascript"></script>
	<script src="../../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="../../../resources/jquery_dataTable/jquery.dataTables.js"></script>
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var criteria_counter = 0, earning_heads = salary_heads = '<option value="0">-- Select --</option>';
		<?php
		foreach( $payroll_heads AS $payroll ) {
			if( $payroll['type'] == 0 ) echo "earning_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';\n";
			if( $payroll['salary_head'] == 1 ) echo "salary_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';\n";
		}
		?>
		
		$(document).ready(function() {
			reset_form();
			populate_data();
			$('#formula_editor').hide();
		});
		
				//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}
		
		function add_criteria() {
			criteria_counter++;
			$('#absent_deduction tbody').append(
				'<tr>'
					+ '<td>'
						+ '<select name="parameter_type[]" id="parameter_type_' + criteria_counter + '" class="combo_boxes" onchange="arrange_criteria( ' + criteria_counter + ' )">'
							+ '<option value="0">Percentage</option>'
							+ '<option value="1">Formula</option>'
							+ '<option value="2">Fixed</option>'
						+ '</select>'
					+ '</td>'
					+ '<td><input type="text" name="percentage_formula[]" id="percentage_formula_' + criteria_counter + '" class="text_boxes" onfocus="add_formula( ' + criteria_counter + ' );" value="" onkeypress="return numbersonly(this,event)" /></td>'
					+ '<td>'
						+ '<select name="base_head[]" id="base_head_' + criteria_counter + '" class="combo_boxes">'
							+ salary_heads
						+ '</select>'
					+ '</td>'
					+ '<td><input type="text" name="amount[]" id="amount_' + criteria_counter + '" class="text_boxes numbers" value="" onkeypress="return numbersonly(this,event)" disabled /></td>'
				+ '</tr>'
			);
			arrange_criteria( criteria_counter );
		}
		
		function arrange_criteria( type ) {
			switch( $('#parameter_type_' + type).val() ) {
				case '0':	//percentage
					$('#percentage_formula_' + type).removeAttr("disabled");
					$('#base_head_' + type).val( 0 ).removeAttr("disabled");
					$('#amount_' + type).val( '' ).attr("disabled", "disabled");
					break;
				case '1':	//formula
					$('#percentage_formula_' + type).removeAttr("disabled");
					$('#base_head_' + type).val( 0 ).attr("disabled", "disabled");
					$('#amount_' + type).val( '' ).attr("disabled", "disabled");
					break;
				case '2':	//fixed
					$('#percentage_formula_' + type).val( '' ).attr("disabled", "disabled");
					$('#base_head_' + type).val( 0 ).attr("disabled", "disabled");
					$('#amount_' + type).removeAttr("disabled");
					break;
			}
		}
		
		function add_formula( counter ) {
			if( $("#parameter_type_" + counter).val() == 1 ) {
				$('input[name="formula"]').val( $("#percentage_formula_" + counter).val() );
				$('#formula_editor').dialog({
					modal: true,
					width: 395,
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
							$("#percentage_formula_" + counter).val( $('input[name="formula"]').val() )
						},
						Cancel: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		}
		
		function write_formula( btn_name ) {
			if( btn_name != 'backspace' ) {
				$('input[name="formula"]').val( $('input[name="formula"]').val() + $('input[name="' + btn_name + '"]').attr('fval') );
			}
			else {
				var text = $('input[name="formula"]').val();
				var backSpace = text.substr( 0, text.length - 1 );
				$('input[name="formula"]').val( backSpace );
			}
		}
		
		$(document).ready(function() {
		$('#policy_id').change(function(){
				if( $(this).val() == 0 ) reset_form();
				else show_policy( $(this).val() );
			});
		});
		
		function show_policy( policy_id ) {
			reset_form();
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'data=absent_deduction_policy&policy_id=' + policy_id,
				success: function( data ) {
					var absent_deduction_policy = unserialize( data );
					
					$('#policy_id').val( policy_id );
					$('#policy_name').val( absent_deduction_policy['policy_name'] );
					$('#deduction_head').val( absent_deduction_policy['deduction_head'] );
					$('#status_active').val( absent_deduction_policy['status_active'] );
					
					for( var key in absent_deduction_policy['criteria'] ) {
						if( Number( key ) > 0 ) add_criteria();
						$('#parameter_type_' + (Number(key)+1)).val( absent_deduction_policy['criteria'][key]['parameter_type'] );
						$('#percentage_formula_' + (Number(key)+1)).val( absent_deduction_policy['criteria'][key]['percentage_formula'] );
						$('#base_head_' + (Number(key)+1)).val( absent_deduction_policy['criteria'][key]['base_head'] );
						$('#amount_' + (Number(key)+1)).val( absent_deduction_policy['criteria'][key]['amount'] );
						
						//arrange_criteria( Number(key)+1 );
					}
				}
			});
		}
		
		function reset_form() {
			$('#absent_deduction tbody').html( '' );
			$('#policy_id').val( 0 );
			$('#policy_name').val( '' );
			$('#deduction_head').val( 0 );
			$('#status_active').val( 1 );
			criteria_counter = 0;
			add_criteria();
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'form=absent_deduction_policy',
				success: function( data ) {
					$('#data_panel').html( data );
					$('#tbl_absent_deduction_policy').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "100%",
						"bScrollCollapse": true
					});
				}
			});
		}
	</script>
	<style type="text/css">
		#absent_deduction_policy_form input[type="text"] { width:180px; }
		#absent_deduction_policy_form select { width:192px; }
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
		.formbutton { width:120px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
		.operator { width:58px; padding:2px 0; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center">
        <div>
    	<div class="form_caption">
		Absent Deduction Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
		</div>
		<form id="absent_deduction_policy_form" action="javascript:fnc_absent_deduction_policy(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete="off" method="POST">
        	<fieldset style="width:925px;">
				<legend>Edit Filter</legend>
				<table width="100%" cellpadding="0" cellspacing="2">
					<tr>
						<td width="10%">Policy:</td>
						<td>
							<select name="policy_id" id="policy_id" class="combo_boxes">
								<option value="0">-- Select policy to edit --</option>
								<?php foreach( $absent_deduction_policy AS $policy ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } ?>
							</select>
						</td>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</fieldset>
            <fieldset style="width:925px; margin-top:10px;">
				<legend>Policy Information</legend>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2" style="padding-bottom:10px;">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="10%">Name:</td>
									<td><input type="text" name="policy_name" id="policy_name" class="text_boxes" value="" /></td>
									<td>Deduction Head:</td>
									<td>
										<select name="deduction_head" id="deduction_head" class="combo_boxes">
											<option value="0">-- Select --</option>
											<?php foreach( $payroll_heads AS $payroll ) { if( $payroll['type'] == 1 ) { ?>
											<option value="<?php echo $payroll['id']; ?>"><?php echo $payroll['custom_head']; ?></option>
											<?php } } ?>
										</select>
									</td>
									<td>Status:</td>
									<td>
										<select name="status_active" id="status_active" class="combo_boxes">
											<?php
											foreach($status_active as $key=>$value):
											?>
											<option value=<? echo "$key";
											if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
											<?		
											endforeach;
											?>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="778">
							<table width="100%" border="0" cellpadding="0" cellspacing="2" id="absent_deduction">
								<thead>
									<tr>
										<th class="header">Parameter Type</th>
										<th class="header">Percentage / Formula</th>
										<th class="header">Base Head</th>
										<th class="header">Amount</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</td>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr><td align="right"><input type="button" name="add" value="Add Criteria" class="formbutton" onclick="add_criteria();" /></td></tr>
								<tr><td align="right" style="padding:5px 0;"><input type="submit" name="save" value="Save" class="formbutton" /></td></tr>
								<tr><td align="right"><input type="button" name="reset" value="Reset" class="formbutton" onclick="reset_form();" /></td></tr>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>
		</form>
        <div id="data_panel" class="demo_jui" align="center" style="width:948px; margin-top:15px;"></div>
	</div>
    <div id="formula_editor" title="Formula Editor">
        <table width="100%">
            <tr><td><input type="text" name="formula" value="" class="text_boxes" style="width:360px; padding:3px; margin-bottom:4px;" readonly /></td></tr>
            <tr>
                <td>
                    <input type="button" name="brace_start" value="(" fval="(" class="operator" onclick="write_formula( 'brace_start' );" />
                    <input type="button" name="brace_end" value=")" fval=")" class="operator" onclick="write_formula( 'brace_end' );" />
                    <input type="button" name="plus" value="+" fval="+" class="operator" onclick="write_formula( 'plus' );" />
                    <input type="button" name="minus" value="-" fval="-" class="operator" onclick="write_formula( 'minus' );" />
                    <input type="button" name="multiply" value="*" fval="*" class="operator" onclick="write_formula( 'multiply' );" />
                    <input type="button" name="divide" value="/" fval="/" class="operator" onclick="write_formula( 'divide' );" />
                    <?php for( $i = 0; $i <= 9; $i++ ) { ?>
                    <input type="button" name="number_<?php echo $i; ?>" value="<?php echo $i; ?>" fval="<?php echo $i; ?>" class="operator" onclick="write_formula( 'number_<?php echo $i; ?>' );" />
                    <?php } ?>
                    <input type="button" name="point" value="." fval="." class="operator" onclick="write_formula( 'point' );" />
                    <input type="button" name="backspace" value="&larr;" class="operator" onclick="write_formula( 'backspace' );" />
                </td>
            </tr>
            <tr>
                <td>
                    <?php foreach( $payroll_heads AS $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
                    <input type="button" name="<?php echo $payroll['custom_head']; ?>" value="<?php echo $payroll['custom_head'] . " (" . $payroll['abbreviation'] . ")"; ?>" fval="<?php echo $payroll['abbreviation']; ?>" style="width:182px; padding:2px 0;" onclick="write_formula( '<?php echo $payroll['custom_head']; ?>' );" />
                    <?php } } ?>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>