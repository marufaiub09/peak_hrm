<?php
/*************************************
|	Tiffin allowance Policy
|	Developed by : Md. Nuruzzaman
*************************************/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

/*$formula_allowance=array(	0=>"-- Select --",
							1=>"((Basic/30)/WH)*OT Hour",
							2=>"((Basic/DOM)/WH)*OT Hour",
							3=>"((Gross/DOM)/WH)*OT Hour",
							4=>"((Gross/30)/WH)*OT Hour",
							5=>"((Basic/208)*2)*OT Hour",
							6=>"(Basic/208)*OT Hour",
							7=>"((Gross/208)*2)*OT Hour",
							8=>"(Gross/208)*OT Hour");
							
// formula_type
$formula_type=array(0=>"-- Select --", 1=>"Fixed Amount", 2=>"Percentage (%)", 3=>"Formula");

// pay_with_salary
$pay_with_salary=array(0=>"-- Select --", 1=>"Yes", 2=>"No");

// day_type
$day_type=array(0=>"-- Select --",1=>"All Type",2=>"General Day",3=>"Off-day",4=>"Holiday");*/

//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY id ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
array_unshift($payroll_heads,"-- Select --");
$payroll_heads_salary = array();
array_unshift($payroll_heads_salary,"-- Select --");
while( $row = mysql_fetch_assoc( $result ) ) {
	if( $row[salary_head]!=1) $payroll_heads[$row['id']] = mysql_real_escape_string( $row[custom_head] );
	if( $row[salary_head]==1)$payroll_heads_salary[$row['id']]= mysql_real_escape_string(  $row[custom_head]  );
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="../../../includes/update_status.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
        <script src="includes/functions.js" type="text/javascript" ></script>
        <script type="text/javascript" charset="utf-8">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
        
            $(document).ready(function() {
				page_load();
                populate_data();
            });
			
			// page_load
			function page_load() 
			{
				$('#tbl_allowance_policy tbody tr').remove();
				$('#policy_id_mst').val( '' );
				$('#policy_name').val( '' );
				$('#late_applicable').val( 0 );
				add_row();
			}
			
            // When load the page then this function will be execute
            function populate_data() 
			{
                $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                $.ajax({
                    type: "POST",
                    url: "../hr_admin_data.php",
                    data: 'form=frm_allowance_policy',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#tbl_allowance_policy_listview').dataTable({
                            "bRetrieve": true,
                            "bDestroy": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                         	 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
                            //"aaSorting": [[ 0, "asc" ]],*/
                          	"oLanguage": { "sSearch": "Search all columns:" },
                            "sScrollX": "100%",
                            "sScrollY": "200px",
                            "sScrollXInner": "100%",
                            "bScrollCollapse": true
                        });
                    }
                });
            }
			
			// Click on the ADD ROW button this function will be execute
			function add_row()
			{
				var rowCount=$("#tbl_allowance_policy tbody tr").length;  
				/*if( $('#txt_allowance_'+rowCount).val()=="" ) return;*/
					
				rowCount++;
				$('#tbl_allowance_policy tbody').append(
					'<tr>'
						+'<td>'
							+'<input type="hidden" name="update_id[]" id="update_id_' + rowCount + '" class="text_boxes" style="width:120px; text-align:center;" /></td>'
						+'</td>'
						+'<td>'
							+'<input type="text" name="must_stay[]" id="must_stay_' + rowCount + '" class="text_boxes" onKeyPress="return numbersonly(this, event);" style=" width:120px; text-align:center;" />'
						+'</td>'
						+'<td>'
							+'<select name="formula_type[]" id="formula_type_' + rowCount + '" onchange="change_formula_type( ' + rowCount + ' )" class="combo_boxes" style=" width:120px;">'
									<?php foreach( $formula_type AS $id=>$formula ) {?>
									+'<option value="<?php echo $id; ?>"><?php echo $formula; ?></option>'
									<?php } ?>
							+'</select>'
						+'</td>'
						+'<td  id="search_value_' + rowCount + '">'
							+'<input type="text" id="value_' + rowCount + '" name="value[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" style=" width:188px; text-align:center;" />'
						+'</td>'
						+'<td>'
							+'<select name="base_head[]" id="base_head_' + rowCount + '" class="combo_boxes" style=" width:120px;">'
									<?php foreach( $payroll_heads_salary AS $id=>$salary_head ) {?>
									+'<option value="<?php echo $id; ?>"><?php echo $salary_head; ?></option>'
									<?php } ?>
							+'</select>'
						+'</td>'
						+'<td>'
							+'<select name="day_type[]" id="day_type_' + rowCount + '" class="combo_boxes" style=" width:120px;">'
									<?php foreach( $day_type AS $id=>$value ) {?>
									+'<option value="<?php echo $id; ?>"><?php echo $value; ?></option>'
									<?php } ?>
							+'</select>'
						+'</td>'
						+'<td>'
							+'<select id="pay_with_salary_' + rowCount + '" name="pay_with_salary[]" class="combo_boxes" style=" width:120px;">'
									<?php foreach( $pay_with_salary AS $id=>$value ) {?>
									+'<option value="<?php echo $id; ?>"><?php echo $value; ?></option>'
									<?php } ?>
							+'</select>'
						+'</td>'
						+'<td>'
							+'<select name="earning_head[]" id="earning_head_' + rowCount + '" class="combo_boxes" style="width:120px;">'
									<?php foreach( $payroll_heads AS $id=>$payroll ){ ?>
									+'<option value="<?php echo $id; ?>"><?php echo $payroll; ?></option>'
									<?php } ?>
							+'</select>'
						+'</td>'
					+'</tr>'
				);
			}
			
			// Click on the REMOVE ROW button this function will be execute
			function  remove_row()
			{
				var r=confirm("Are you sure want to REMOVE ROW !!");
				if(r==true)
				{
					var rowCount = $('#tbl_allowance_policy tbody tr').length;
					if(rowCount!=1)
					{
						$("#tbl_allowance_policy tr:last").remove();
					}
				}
			}
			
			// Click on the REFRESH button this function will be execute
			function reset_form() {
				var r=confirm("Are you sure want to REFRESH fields !!");
				if(r==true)
				{
					$('#tbl_allowance_policy tbody tr').remove();
					$('#policy_id_mst').val( '' );
					$('#policy_name').val( '' );
					$('#late_applicable').val( 0 );
					add_row();
				}
			}
			
			// Click on th SAVE button this function will be execute
			function allowance_policy_save(save_perm,edit_perm,delete_perm)
			{
				//alert("I am from FORM ACTION");
				var policy_name=document.getElementById('policy_name').value;
				var late_applicable=document.getElementById('late_applicable').value;
				
				var policy_id=document.getElementById('policy_id_mst').value;
				var db_row_ids_mst=document.getElementById('db_row_ids_mst').value;
				
				var update_id='';
				var must_stay='';
				var formula_type='';
				var value='';
				var base_head='';
				var day_type='';
				var pay_with_salary='';
				var earning_head='';
			
				// Validation checking here
				if( policy_name== '' ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#policy_name').focus();
						$(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
					return;
				}
				if( late_applicable==0 ) 
				{
					error = true;
					$('#messagebox').fadeTo( 200, 0.1, function() {
						$('#late_applicable').focus();
						$(this).html('Select late applicable.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
					});
					return;
				}
				else 
				{			
					var rowCount = $('#tbl_allowance_policy tbody tr').length;
					for (var i=1; i<=rowCount; i++)
					{
						if(update_id=='') update_id=$('#update_id_'+i).val(); else update_id=update_id+"__"+$('#update_id_'+i).val(); 
						if(must_stay=='') must_stay=$('#must_stay_'+i).val(); else must_stay=must_stay+"__"+$('#must_stay_'+i).val();
						if(formula_type=='') formula_type=$('#formula_type_'+i).val(); else formula_type=formula_type+"__"+$('#formula_type_'+i).val();
						if(value=='') value=$('#value_'+i).val(); else value=value+"__"+$('#value_'+i).val();
						if(base_head=='') base_head=$('#base_head_'+i).val(); else base_head=base_head+"__"+$('#base_head_'+i).val();
						if(day_type=='') day_type=$('#day_type_'+i).val(); else day_type=day_type+"__"+$('#day_type_'+i).val();
						if(pay_with_salary=='') pay_with_salary=$('#pay_with_salary_'+i).val(); else pay_with_salary=pay_with_salary+"__"+$('#pay_with_salary_'+i).val();
						if(earning_head=='') earning_head=$('#earning_head_'+i).val(); else earning_head=earning_head+"__"+$('#earning_head_'+i).val();
						//alert(allowance);
					}
					//alert(rowCount);
					http.open( 'GET', 'includes/save_update_policy.php?action=allowance_policy&policy_name='+policy_name+
						'&late_applicable='+late_applicable+
						'&policy_id='+policy_id+
						'&update_id='+update_id+
						'&must_stay='+must_stay+
						'&formula_type='+formula_type+
						'&value='+value+
						'&base_head='+base_head+
						'&day_type='+day_type+
						'&pay_with_salary='+pay_with_salary+
						'&earning_head='+earning_head+
						'&db_row_ids_mst='+db_row_ids_mst);
						
					http.onreadystatechange = response_allowance_policy;
					http.send(null);
				}
			}
			
			// Response comes here
			function response_allowance_policy() 
			{
				if(http.readyState == 4)
				{
					//alert (http.responseText);
					var response = http.responseText;
					if( response == 1 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() 
						{
							$(this).html('Allowance policy has been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
							$(this).fadeOut(5000);
							//document.getElementById('tiffin_allowance_policy').reset();
						});
						page_load();
						populate_data();
					}
					else if( response == 2 ) 
					{
						$("#messagebox").fadeTo( 200, 0.1, function() 
						{
							$(this).html('Allowance policy has been Updated successfully .').addClass('messagebox_ok').fadeTo( 900, 1 )
							$(this).fadeOut(5000);
							//document.getElementById('tiffin_allowance_policy').reset();
						});
						page_load();
						populate_data();
					}
				}
			}
			
			// Click on the each ROW of list view this function will be execute
			function js_set_value(policy_id,policy_name,late_applicable,earning_head)
			{
				//alert("I am from ROW click");
				document.getElementById('policy_id_mst').value=policy_id;
				document.getElementById('policy_name').value=policy_name;
				document.getElementById('late_applicable').value=late_applicable;
				$.ajax({
						type: "POST",
						url: "../hr_admin_data.php",
						data: 'data=allowance_policy&policy_id=' + policy_id,
						success: function( data ) 
						{
							$('#tbl_allowance_policy tbody tr').remove();
							$('#tbl_allowance_policy tbody').append(data);
							document.getElementById('db_row_ids_mst').value=document.getElementById('db_row_ids').value;
						}
				});
			}
			
//formula type change
function change_formula_type(counter)
{
	//alert(counter);
	switch( $("#formula_type_" + counter).val() ) {
				case '1':
					//alert('ok');
					$("#base_head_" + counter).val('0').attr( 'disabled', 'disabled' );
					document.getElementById('search_value_' + counter).innerHTML='<input type="text" name="value[]" id="value_'+ counter +'" onKeyPress="return numbersonly(this, event);" class="text_boxes" style=" width:188px; text-align:center;" />';
					break;
				
				case '2':
					$("#base_head_" + counter).val('0').removeAttr( 'disabled' );
					document.getElementById('search_value_' + counter).innerHTML='<input type="text" name="value[]" id="value_'+ counter +'" onKeyPress="return numbersonly(this, event);" class="text_boxes" style=" width:188px; text-align:center;" />';
					break;
					
				case '3':
					$("#base_head_" + counter).val('0').attr( 'disabled', 'disabled' );
					document.getElementById('search_value_' + counter).innerHTML='<select name="value[]" id="value_'+ counter +'" class="combo_boxes" style=" width:200px;">'
																	<?php foreach( $formula_allowance AS $id=>$formula ) {?>
																	+' <option value="<?php echo $id; ?>"><?php echo $formula; ?></option>'
																	 <?php } ?>
																+'</select>';
					break;
	}
}
        </script>
        <style>
            .header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div style="height:60px;" align="center">    	
            <div class="form_caption">Allowance Policy</div>
            <span id="permission_caption" ><? echo "Your Permissions--> \n".$insert.$update.$delete;?></span>
            <div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div>
        </div>
        <div align="center" style="width:100%px;">     
            <form id="frm_allowance_policy" name="frm_allowance_policy" action="javascript:allowance_policy_save(save_perm,edit_perm,delete_perm)" autocomplete="off" method="post">
                <fieldset style="width:75%;">
                    <legend>Allowance Policy</legend>
                    <fieldset style="width:98%;">
                        <table width="30%" cellspacing="1" cellpadding="0" border="0" align="center">
                            <thead>
                                <tr>
                                    <th class="header" width="200px">Policy Name</th>
                                    <th class="header" width="200px">Late Applicable</th>
                                </tr>
                            </thead>
                            <tr>
                                <td>
                                    <input type="text" name="policy_name" id="policy_name" class="text_boxes" style="width:150px;" />
                                    <input type="hidden" id="policy_id_mst" name="policy_id_mst" class="text_boxes" style="width:100px;" />
                                    <input type="hidden" id="db_row_ids_mst" name="db_row_ids_mst" class="text_boxes" style="width:100px;" />
                                </td>
                                <td>
                                    <select id="late_applicable" name="late_applicable" class="combo_boxes" style="width:150px;">
                                        <?php foreach( $pay_with_salary AS $id=>$value ) {?>
                                        <option value="<?php echo $id; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>	
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                        <fieldset style="width:98%;">
                            <table width="98%">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="1" cellspacing="1" id="tbl_allowance_policy" align="center">
                                            <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th class="header" width="120px">Must Stay(Minute)</th>
                                                    <th class="header" width="120px">Formula Type</th>
                                                    <th class="header" width="120px">Value</th>
                                                    <th class="header" width="120px">Base Head</th>
                                                    <th class="header" width="120px">Day Type</th>
                                                    <th class="header" width="120px">Pay With Salary</th>
                                                    <th class="header" width="120px">Earning Head</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table>
                                            <tr><td><input type="button" name="btn_add" id="btn_add" value="Add Row" class="formbutton" onclick="add_row();" style="width:90px;" /></td></tr>
                                            <tr><td><input type="button" name="btn_remove" id="btn_remove" value="Remove Row" class="formbutton" style="width:90px;" onclick="remove_row();" /></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <table align="center">
                            <tr><td colspan="3">&nbsp; </td></tr>
                            <tr>
                                <td><input type="submit" name="btn_save" id="btn_save" class="formbutton" value="Save" style="width:90px;"/></td>
                                <td><input type="button" name="btn_refresh" id="btn_refresh" class="formbutton" value="Refresh" onclick="reset_form()" style="width:90px;" /></td>
                            </tr>
                        </table>
                </fieldset>
            </form> 
        </div>
        <div id="data_panel" class="demo_jui" align="center" style="width:824px; margin-top:15px; margin-left:135px;"></div>
    </body>
</html>