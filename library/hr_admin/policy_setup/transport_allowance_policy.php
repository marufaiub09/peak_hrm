<?php
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../../includes/common.php');

//salary_breakdown_policy
$sql = "SELECT * FROM lib_policy_salary_breakdown ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$salary_breakdown_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$salary_breakdown_policy[$row['id']] = $row;
	
	$sql2 = "SELECT * FROM lib_policy_salary_breakdown_definition WHERE policy_id = $row[id] ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	$salary_breakdown_policy[$row['id']]['definition'] = array();
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$salary_breakdown_policy[$row['id']]['definition'][] = $row2;
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 ORDER BY custom_head";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>	
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var head_counter = 0, salary_heads = '<option value="0">--- Select ---</option>';
		<?php 
		foreach( $payroll_heads AS $payroll_id => $payroll ) {
			if( $payroll['salary_head'] == 1 ) echo "salary_heads += '<option value=\"$payroll[id]\">$payroll[custom_head]</option>';";			
		}
		?>
		
$(document).ready(function() {	
	$('#formula_editor').hide();
	reset_form();
	populate_data();
	
	$('#policy_id').change(function(){
		if( $(this).val() == 0 ) reset_form();
		else show_policy( $(this).val() );
	});
	
});
		
		//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

		
function add_row() {
	head_counter++;
	
	$('#table_bonus_policy tbody').append(
		'<tr>'
			+ '<td>'
				+ '<input type="text" class="text_boxes" name="slot[]" id="slot_' + head_counter + '" style="width:250px" onkeypress="return numbersonly(this,event)" />'
			+ '</td>'
			+ '<td>'
				+ '<select name="type[]" id="type_' + head_counter + '" class="combo_boxes">'
					+ '<option value="">--Select--</option>'
					+ '<option value="1">New</option>'
					+ '<option value="2">Regular</option>'
					+ '<option value="3">Resign</option>'
				+ '</select>'
			+ '</td>'
			+ '<td><select name="amounttype[]" onchange="change_status(this.value,' + head_counter + ')" id="amounttype_' + head_counter + '" class="combo_boxes">'
					+ '<option value="1">Percent</option>'
					+ '<option value="2">Fixed</option>'
				+ '</select></td>'
			+ '<td><input type="text" name="amount[]" id="amount_' + head_counter + '" class="text_boxes numbers" placeholder=" % " onkeypress="return numbersonly(this,event)" /></td>'
		+ '</tr>'
	);
	
}
		
function change_status(val,id)
{
	$('#base_head_'+id).val(0);
	if(val==1)
		$('#base_head_'+id).removeAttr('disabled');
	else
		$('#base_head_'+id).attr('disabled','true');
}
		
function add_formula( counter ) {
	if( $("#type_" + counter).val() == 'Formula' ) {
		$('input[name="formula"]').val( $("#percentage_formula_" + counter).val() );
		$( "#formula_editor" ).dialog({
			modal: true,
			width: 395,
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
					$("#percentage_formula_" + counter).val( $('input[name="formula"]').val() )
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
	}
}
		
function write_formula( btn_name ) {
	if( btn_name != 'backspace' ) {
		$('input[name="formula"]').val( $('input[name="formula"]').val() + $('input[name="' + btn_name + '"]').attr('fval') );
	}
	else {
		var text = $('input[name="formula"]').val();
		var backSpace = text.substr( 0, text.length - 1 );
		$('input[name="formula"]').val( backSpace );
	}
}
		
function show_policy( policy_id,policy_name,amount,status_active )
{
	reset_form();
	$('#is_update').val( policy_id );
	$('#policy_name').val( policy_name);
	$('#policy_amount').val(amount );
	$('#cbo_status').val(status_active );
	
	/*$.ajax({
		type: "POST",
		url: "../hr_admin_data.php",
		data: 'data=transport_policy_form&policy_id=' + policy_id,
		success: function( data ) {
			var bonus_policy = unserialize( data );
			
			$('#policy_id').val( policy_id );
			$('#is_update').val( policy_id );//for update
			$('#policy_name').val( bonus_policy['policy_name'] );
			$('#policy_amount').val( bonus_policy['amount'] );
			$('#cbo_status').val( bonus_policy['status_active'] );
			if( bonus_policy['status_active'] == 1 ) $('#approved').attr( 'checked', true );
			
			for( var key in bonus_policy['criteria'] ) {
				if( Number( key ) > 0 )add_row();
				$('#slot_' + (Number(key)+1)).val( bonus_policy['criteria'][key]['slot'] );
				$('#type_' + (Number(key)+1)).val( bonus_policy['criteria'][key]['comfirmation'] );
				$('#amount_' + (Number(key)+1)).val( bonus_policy['criteria'][key]['percentage'] );
				$('#base_head_' + (Number(key)+1)).val( bonus_policy['criteria'][key]['base_head'] );
				$('#amounttype_' + (Number(key)+1)).val( bonus_policy['criteria'][key]['amount_type'] );
				if(bonus_policy['criteria'][key]['amount_type']==1)
					$('#base_head_'+ (Number(key)+1)).removeAttr('disabled');
				else
					$('#base_head_'+ (Number(key)+1)).attr('disabled','true');
				//if (bonus_policy['criteria'][key]['amount_type']==2);
			}
		}
	});*/
	
	$.ajax({
		type: "POST",
		url: "../hr_admin_data.php",
		data: 'data=transport_policy_data&policy_id=' + policy_id,
		success: function( data ) {
		$('#table_bonus_policy tbody tr').remove();
		$('#table_bonus_policy tbody').append(data);
	   /* document.getElementById('db_row_ids_mst').value=document.getElementById('db_row_ids').value;
	$('.timepicker').timepicker({
		H: true
	});*/
	}
	});
}
		
function reset_form() {
	$('#table_bonus_policy tbody').html( '' );
	$('#policy_id').val( 0 );
	$('#is_update').val( '' );//for update 
	$('#policy_name').val( '' );
	$('#policy_amount').val( '' );
	$('#cbo_status').val( '' );
	head_counter = 0;
	add_row();
}
		
function populate_data() {
	$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
	$.ajax({
		type: "POST",
		url: "../hr_admin_data.php",
		data: 'form=transport_allowance_policy_from',
		success: function( data ) {
			$('#data_panel').html( data );
			$('#tbl_transport_allowance_policy').dataTable({
				"bRetrieve": true,
				"bDestroy": true,
				"bJQueryUI": true,
				"bPaginate": false,
				//"aoColumns": [ null,null,null,null,null,null,null, { "sType": "html" }, { "bSortable": false } ],
				"aaSorting": [[ 1, "asc" ]],
				"oLanguage": { "sSearch": "Search all columns:" },
				"sScrollX": "100%",
				"sScrollY": "200px",
				"sScrollXInner": "100%",
				"bScrollCollapse": true
			});
		}
	});
}
		
function fn_transport_policy_delete( policy_id ) 
{			
	$.ajax({
		type: "POST",
		url: "includes/save_update_policy.php?action=transport_policy_delete&policy_id="+policy_id,
			success: function( data ) 
			{
				if(data==1)
				{	
					$("#messagebox").fadeTo( 200, 0.1, function() 
					{
						$(this).html('Transport Policy Deleted Successfully Done.').addClass('messagebox_ok').fadeTo(900,1);
						save_activities_history('','library','bonus_policy','delete','../../../');
						$(this).fadeOut(5000);
						populate_data();
						reset_form();
					});
				}
				if(data==3)
				{
					$("#messagebox").fadeTo( 200, 0.1, function() 
					{
						$(this).html('Delete Restricted!!!! Bonus Policy Already Attached With Employees. ').addClass('messagebox_ok').fadeTo(900,1);
						$(this).fadeOut(5000);
					});	
				}
			}
	});
}
		
	</script>
	<style>
		.header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
		#salary_breakdown_policy_form input[type="text"], select { width:205px; }
		.numbers { text-align:right; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
		.operator { width:58px; padding:2px 0; }
	</style>
</head>
<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%; position:relative; height:70px; margin-bottom:5px; margin-top:5px">    
    <div>
    	<div class="form_caption">
		Transport Allowance Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>	
	<form id="transport_allowance_policy_form" action="javascript:fnc_transport_allowance_policy(save_perm,edit_perm,delete_perm,approve_perm)" method="POST" autocomplete="off" >
		
		<fieldset style="width:900px; margin-top:10px;">
		  <legend>Policy Information</legend>
			<table id="table_bonus_policy" width="900" border="0" cellpadding="0" cellspacing="2">
				<thead>
                    <tr>
                        <td>Policy Name:<input type="text" name="policy_name" id="policy_name" class="text_boxes" value="" style="width:150px;" />
                         <input type="hidden" id="is_update" name="is_update" value="" />	
                        </td>
                        <td>Amount:<input type="text" name="policy_amount" id="policy_amount" class="text_boxes" value="" style="width:150px;" onkeypress="return numbersonly(this,event)" /></td>
                        <td>Status: 
                            <select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:100px;">
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                            </select>
                        </td>	
                    </tr>
                     <tr><td colspan="3">&nbsp; </td></tr>
					<tr>
						<th class="header">Minimum Working Days</th>
						<th class="header">Emp Status</th>
                        <th class="header">Amount Type</th>
						<th class="header">Value </th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>									
						<td colspan="5"><input type="button" name="add" id="add" class="formbutton" value="Add" onclick="add_row()" />&nbsp;</td>
					</tr>
                    <tr>
						<td colspan="5" align="center"  class="button_container">
						  <input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						  <input type="button" name="reset" id="reset" class="formbutton" value="Reset" onclick="reset_form()" />
                        </td>
					</tr>
				</tfoot>
			</table>
		</fieldset>
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:900px; margin-top:15px;"></div>
	
</body>
</html>