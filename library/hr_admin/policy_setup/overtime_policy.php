<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 31-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');

//overtime_policy
$sql = "SELECT * FROM lib_policy_overtime WHERE is_deleted = 0 ORDER BY policy_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$overtime_policy = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$overtime_policy[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$overtime_policy[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$overtime_policy[$row['id']]['shift'] = '';
	$shifts = explode( ',', $overtime_policy[$row['id']]['shift_id'] );
	for( $i = 0; $i < count( $shifts ); $i++ ) {
		$sql2 = "SELECT shift_name FROM lib_policy_shift WHERE id = $shifts[$i]";
		$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
		$row2 = mysql_fetch_assoc( $result2 );
		$overtime_policy[$row['id']]['shift'] .= $row2['shift_name'] . '*';
	}
	$overtime_policy[$row['id']]['shift'] = substr( $overtime_policy[$row['id']]['shift'], 0, strlen( $overtime_policy[$row['id']]['shift'] ) - 1 );
	
	$overtime_policy[$row['id']]['slab'] = array();
	
	$sql2 = "SELECT * FROM lib_policy_overtime_slab WHERE policy_id = $row[id] ORDER BY id";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$overtime_policy[$row['id']]['slab'][] =	array(
														'overtime_slot'			=> mysql_real_escape_string( $row2['overtime_slot'] ),
														'overtime_multiplier'	=> mysql_real_escape_string( $row2['overtime_multiplier'] ),
														'overtime_head'			=> mysql_real_escape_string( $row2['overtime_head'] )
													);
	}
}
//payroll_heads
$sql = "SELECT * FROM lib_payroll_head WHERE is_deleted = 0 AND status_active = 1 ORDER BY custom_head ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$payroll_heads = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$payroll_heads[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$payroll_heads[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_policy.js" type="text/javascript"></script>
	<script src="../../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<link href="../../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script src="../../../js/popup_window.js" type="text/javascript"></script>
	<script src="../../../js/modal.js" type="text/javascript"></script>
	
	<script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
    
    <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			populate_data();
			$("#formula_editor").hide();		
		
		$('.timepicker').timepicker({
				//H: true,				
				showSecond: true,
				timeFormat: 'hh:mm:ss'
			});
		
		});
		
		function shift_multiselect( page_link, title ) {
			emailwindow = dhtmlmodal.open( 'EmailBox', 'iframe', page_link, title, 'width=560px,height=400px,center=1,resize=0,scrolling=0', '../../' );
			
			emailwindow.onclose = function() {
				document.getElementById( 'shift' ).value = this.contentDoc.getElementById("txt_selected").value;
				document.getElementById( 'shift_id' ).value = this.contentDoc.getElementById("txt_selected_id").value;
			}
		}
		
		function add_formula() {
			$('input[name="formula"]').val( $('input[name="overtime_rate"]').val() );
			$( "#formula_editor" ).dialog({
				modal: true,
				width: 395,
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
						$('input[name="overtime_rate"]').val( $('input[name="formula"]').val() );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				}
			});
		}
		
		function write_formula( btn_name ) {
			if( btn_name != 'backspace' ) {
				$('input[name="formula"]').val( $('input[name="formula"]').val() + $('input[name="' + btn_name + '"]').attr('fval') );
			} else {
				var text = $('input[name="formula"]').val();
				var backSpace = text.substr( 0, text.length - 1 );
				$('input[name="formula"]').val( backSpace );
			}
		}
		
		function show_policy( policy_id ) {
			reset_form();
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'data=overtime_policy&policy_id=' + policy_id,
				success: function( data ) {
					var overtime_policy = unserialize( data );
					
					$('#policy_id').val( policy_id );
					$('#is_update').val(policy_id);//for check update
					$('#policy_name').val( overtime_policy['policy_name'] );
					$('#overtime_calculation_rule').val( overtime_policy['overtime_calculation_rule'] );
					$('#overtime_rate').val( overtime_policy['overtime_rate'] );
					$('#max_overtime').val( overtime_policy['max_overtime'] );
					$('#shift').val( overtime_policy['shift'] );
					$('#shift_id').val( overtime_policy['shift_id'] );
					$('#status_active').val( overtime_policy['status_active'] );
					
					for( var key in overtime_policy['slab'] ) {
						$('#overtime_slab_' + (Number(key)+1)).val( (Number(key)+1) );
						$('#overtime_slot_' + (Number(key)+1)).val( overtime_policy['slab'][key]['overtime_slot'] );
						$('#overtime_multiplier_' + (Number(key)+1)).val( overtime_policy['slab'][key]['overtime_multiplier'] );
						$('#overtime_head_' + (Number(key)+1)).val( overtime_policy['slab'][key]['overtime_head'] );
					}
				}
			});
		}
		
		function reset_form() {
			$('#policy_id').val( 0 );
			$('#policy_name').val( '' );
			$('#overtime_calculation_rule').val( 0 );
			$('#overtime_rate').val( '' );
			$('#max_overtime').val( '' );
			$('#shift').val( '' );
			$('#shift_id').val( '' );
			$('#status_active').val( 1 );
			$('#is_update').val( '' );//for update
			for( var i = 1; i <= 5; i++ ) {
				$('#overtime_slab_' + i).val( '' );
				$('#overtime_slot_' + i).val( '' );
				$('#overtime_multiplier_' + i).val( '' );
				$('#overtime_head_' + i).val( 0 );
			}
		}
		
		function numbersonly(myfield, e, dec)
		{
			var key;
			var keychar;					
			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);					
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;						
			// numbers
			else if ((("0123456789.").indexOf(keychar) > -1))
				return true;
			else
				return false;
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "../hr_admin_data.php",
				data: 'form=overtime_policy',
				success: function(data) {
					$('#data_panel').html( data );
					$('#tbl_overtime_policy').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"bPaginate": false,
						"aoColumns": [ { "sType": "html" }, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"sScrollX": "100%",
						"sScrollY": "200px",
						"sScrollXInner": "125%",
						"bScrollCollapse": true
					});
				}
			});
		}
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
	</script>
	<style>
		#overtime_policy_form input[type="text"] { width:158px; }
		#overtime_policy_form select { width:170px; }
		.numbers { text-align:right; }
		.operator { width:58px; padding:2px 0; }
		.header { height:20px; border-color:#350402; border-bottom:double; background-color:#8FAA8C; font-size:12px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center">		
        <div>
    	<div class="form_caption">
		Overtime Policy
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
		<form id="overtime_policy_form" action="javascript:fnc_overtime_policy(save_perm,edit_perm,delete_perm,approve_perm);" autocomplete="off" method="post">
			<fieldset style="width:694px;">
				<legend>Edit Filter</legend>
				<table width="100%" border="0" cellspacing="2" cellpadding="0">
					<tr>
						<td>
							Policy:
							<select name="policy_id" id="policy_id" class="combo_boxes" style="margin-left:98px;" onchange="show_policy( this.value );">
								<option value="0">-- Select policy to edit --</option>
								<?php foreach( $overtime_policy AS $policy ) { ?>
								<option value="<?php echo $policy['id']; ?>"><?php echo $policy['policy_name']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
				</table>
			</fieldset>
			<fieldset style="width:694px; margin-top:10px;">
				<legend>Policy Information</legend>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td>Name:</td>
						<td><input type="text" name="policy_name" id="policy_name" class="text_boxes" value="" /></td>
						<td>OT calculation rule:</td>
						<td>
							<select name="overtime_calculation_rule" id="overtime_calculation_rule"  class="combo_boxes">
								<option value="0">Actual Out-time - Actual In-time - Shift Hour</option>
								<option value="1">Actual Out-time - Shift Out-time</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>OT rate per hour:</td>
						<td><input type="text" name="overtime_rate" id="overtime_rate" class="text_boxes" value="" onfocus="add_formula();" /></td>
						<td>Max OT this shift:</td>
						<td><input type="text" name="max_overtime" id="max_overtime" class="timepicker text_boxes" value="" placeholder="00:00:00" /></td>				
					</tr>
					<tr>
						<td>Shift:</td>
						<td>
							<input type="text" name="shift" id="shift" class="text_boxes" value="" placeholder="Double click for shifts" ondblclick="shift_multiselect( 'shift_multiselect.php', 'Select shift/(s)' ); return false;" />
							<input type="hidden" name="shift_id" id="shift_id" class="text_boxes" value="" />
						</td>
						<td>Status:</td>
						<td>
							<select name="status_active" id="status_active" class="combo_boxes">
								 <?php
									foreach($status_active as $key=>$value):
									?>
									<option value=<? echo "$key";
									if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
									<?		
									endforeach;
									?>
							</select>
						</td>
					</tr>
					<tr><td colspan="2" height="10"></td></tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="2" id="overtime_slab">
					<thead>
						<tr>
							<th class="header">OT Slab No</th>
							<th class="header">OT Slot</th>
							<th class="header">OT Multiplier</th>
							<th class="header">Payroll Head</th>
						</tr>
					</thead>
					<tbody>
						<?php for( $i = 1; $i <= 5; $i++ ) { ?>
						<tr>
							<td><input type="text" name="overtime_slab[]" id="overtime_slab_<?php echo $i; ?>" onKeyPress="return numbersonly(this, event)" style="text-align:right" class="text_boxes" value="" /></td>
							<td><input type="text" name="overtime_slot[]" id="overtime_slot_<?php echo $i; ?>" onKeyPress="return numbersonly(this, event)" style="text-align:right" class="text_boxes" value="" /></td>
							<td><input type="text" name="overtime_multiplier[]" id="overtime_multiplier_<?php echo $i; ?>" onKeyPress="return numbersonly(this, event)" style="text-align:right" class="text_boxes" value="" /></td>
							<td>
								<select name="overtime_head[]" id="overtime_head_<?php echo $i; ?>" class="combo_boxes">
									<option value="0">-- Select --</option>
									<?php foreach( $payroll_heads AS $payroll ) { if( $payroll['type'] == 0 ) { ?>
									<option value="<?php echo $payroll['id']; ?>"><?php echo $payroll['custom_head']; ?></option>
									<?php } } ?>
								</select>
							</td>
						</tr>
						<?php } ?>
					</tbody>
					<tfoot>
                    	<tr>
							<td colspan="4">&nbsp;</td>						
						</tr>
						<tr>
							<td align="center" colspan="4" style="padding-top:10px;" class="button_container">
								<input type="submit" name="save" id="save" value="Save" class="formbutton" />&nbsp;&nbsp;
								<input type="button" name="reset" id="reset" value="Reset" class="formbutton" onclick="reset_form();" />
                                <input type="hidden" id="is_update" name="is_update" value="" />
							</td>
						</tr>
					</tfoot>
				</table>
			</fieldset>
		</form>
		<div id="data_panel" class="demo_jui" align="center" style="width:716px; margin-top:15px;"></div>
		<div id="formula_editor" title="Formula Editor">
			<table width="100%">
				<tr><td><input type="text" name="formula" value="" class="text_boxes" style="width:360px; padding:3px; margin-bottom:4px;" readonly /></td></tr>
				<tr>
					<td>
						<input type="button" name="brace_start" value="(" fval="(" class="operator" onclick="write_formula( 'brace_start' );" />
						<input type="button" name="brace_end" value=")" fval=")" class="operator" onclick="write_formula( 'brace_end' );" />
						<input type="button" name="plus" value="+" fval="+" class="operator" onclick="write_formula( 'plus' );" />
						<input type="button" name="minus" value="-" fval="-" class="operator" onclick="write_formula( 'minus' );" />
						<input type="button" name="multiply" value="*" fval="*" class="operator" onclick="write_formula( 'multiply' );" />
						<input type="button" name="divide" value="/" fval="&divide;" class="operator" onclick="write_formula( 'divide' );" />
						<?php for( $i = 0; $i <= 9; $i++ ) { ?>
						<input type="button" name="number_<?php echo $i; ?>" value="<?php echo $i; ?>" fval="<?php echo $i; ?>" class="operator" onclick="write_formula( 'number_<?php echo $i; ?>' );" />
						<?php } ?>
						<input type="button" name="point" value="." fval="." class="operator" onclick="write_formula( 'point' );" />
						<input type="button" name="backspace" value="&larr;" class="operator" onclick="write_formula( 'backspace' );" />
					</td>
				</tr>
				<tr>
					<td>
						<?php foreach( $payroll_heads AS $payroll ) { if( $payroll['salary_head'] == 1 ) { ?>
						<input type="button" name="<?php echo $payroll['custom_head']; ?>" value="<?php echo $payroll['custom_head'] . " (" . $payroll['abbreviation'] . ")"; ?>" fval="<?php echo $payroll['abbreviation']; ?>" style="width:182px; padding:2px 0;" onclick="write_formula( '<?php echo $payroll['custom_head']; ?>' );" />
						<?php } } ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>