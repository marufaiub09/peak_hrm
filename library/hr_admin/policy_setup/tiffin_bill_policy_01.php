<?php
/*************************************
|	Tiffin bill Policy
|	Developed by : Md. Nuruzzaman
*************************************/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../../includes/array_function.php');
include('../../../includes/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
        <link href="../../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="../../../includes/update_status.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../../../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
        <script src="includes/functions.js" type="text/javascript" ></script>
        <script type="text/javascript" charset="utf-8">
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
        
            $(document).ready(function() {	
                populate_data();
                
                $('.timepicker').timepicker({
                    H: true
                });
            });
            // When load the page then this function will be execute
            function populate_data() {
                $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                $.ajax({
                    type: "POST",
                    url: "../hr_admin_data.php",
                    data: 'form=tiffin_bill_policy',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#tbl_tiffin_bill_policy').dataTable({
                            "bRetrieve": true,
                            "bDestroy": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                         	 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
                            //"aaSorting": [[ 0, "asc" ]],*/
                          	"oLanguage": { "sSearch": "Search all columns:" },
                            "sScrollX": "100%",
                            "sScrollY": "200px",
                            "sScrollXInner": "100%",
                            "bScrollCollapse": true
                        });
                    }
                });
            }
            // Click on the ADD ROW button this function will be execute
            function add_row()
            {
			    var rowCount=$("#tiffin_bill tbody tr").length;  
                if( $('#allowance_'+rowCount).val()=="" ) return;
					
				rowCount++;
                $('#tiffin_bill tbody').append(
                    '<tr>'
                        +'<td><input type="hidden" name="update_id[]" id="update_id_' + rowCount + '" value="" class="text_boxes" style="width:120px; text-align:center;" /></td>'
                        +'<td><input type="text" name="work_upto[]" id="work_upto_' + rowCount + '" value="" class="timepicker text_boxes" style="width:120px; text-align:center;" /></td>'
                        +'<td><input type="text" name="allowance[]" id="allowance_' + rowCount + '" value="" class="text_boxes" onKeyPress="return numbersonly(this, event);" onblur="check_duplicacy(this.id,this.value)" style="width:120px; text-align:right;" /></td>'
                    +'</tr>'
                );
                $('.timepicker').timepicker({
                    H: true
                });
            }
            // Click on the REMOVE ROW button this function will be execute
            function  remove_row()
            {
                var r=confirm("Are you sure want to REMOVE ROW !!");
				if(r==true)
				{
					var rowCount = $('#tiffin_bill tbody tr').length;
					if(rowCount!=1)
					{
						$("#tiffin_bill tr:last").remove();
					}
				}
            }
            // Click on the REFRESH button this function will be execute
            function reset_form() {
                var r=confirm("Are you sure want to REFRESH fields !!");
				if(r==true)
				{
					$('#tiffin_bill tbody tr').remove();
					$('#policy_name').val( '' );
					$('#etitled_late').val( 0 );
					add_row();
				}
            }
            // Click on th SAVE button this function will be execute
            function fun_tiffin_policy(save_perm,edit_perm,delete_perm)
            {
                //alert("I am from FORM ACTION");
                var policy_name=document.getElementById('policy_name').value;
                var etitled_late=document.getElementById('etitled_late').value;
                var policy_id=document.getElementById('policy_id_mst').value;
				var db_row_ids_mst=document.getElementById('db_row_ids_mst').value;
				
                var update_id='';
				var work_upto='';
				var allowance='';
				
                
                // Validation checking here
                if( policy_name== '' ) 
                {
                    error = true;
                    $('#messagebox').fadeTo( 200, 0.1, function() {
                        $('#policy_name').focus();
                        $(this).html('Policy name can not be empty.').addClass('messageboxerror').fadeTo(900,1);
						$(this).fadeOut(5000);
                    });
                    return;
                }
                else 
                {			
                    var rowCount = $('#tiffin_bill tbody tr').length;
					
                    for (var i=1; i<=rowCount; i++)
                    {
						if ($('#work_upto_'+i).val()==0) $('#work_upto_'+i).val('');
						if ($('#allowance_'+i).val()==0) $('#allowance_'+i).val('');
						
						if($('#work_upto_'+i).val()!="" && $('#allowance_'+i).val()=="" )
						{
							$('#messagebox').fadeTo( 200, 0.1, function() {
								$('#allowance_'+i).focus();
								$(this).html('Bill amount field is blank.').addClass('messageboxerror').fadeTo(900,1);
								$(this).fadeOut(5000);
							});
							return;
						}
						else if($('#work_upto_'+i).val()=="" && $('#allowance_'+i).val()!="" )
						{
							$('#messagebox').fadeTo( 200, 0.1, function() {
								$('#work_upto_'+i).focus();
								$(this).html('Work upto field is blank.').addClass('messageboxerror').fadeTo(900,1);
								$(this).fadeOut(5000);
							});
							return;
						}
						else if($('#work_upto_'+i).val()=="" && $('#allowance_'+i).val()=="" )
						{
							$('#messagebox').fadeTo( 200, 0.1, function() {
								$('#work_upto_'+i).focus();
								$(this).html('Both fields are blank.').addClass('messageboxerror').fadeTo(900,1);
								$(this).fadeOut(5000);
							});
							return;
						}
						
                        if(update_id=='') update_id=$('#update_id_'+i).val(); else update_id=update_id+"_"+$('#update_id_'+i).val(); 
                        if(work_upto=='') work_upto=$('#work_upto_'+i).val(); else work_upto=work_upto+"_"+$('#work_upto_'+i).val();
                        if(allowance=='') allowance=$('#allowance_'+i).val(); else allowance=allowance+"_"+$('#allowance_'+i).val();
                        //alert(allowance);
                    }
                    //alert(rowCount);
                    http.open( 'GET', 'includes/save_update_policy.php?action=tiffin_bill_policy&policy_name='+policy_name+
                        '&etitled_late='+etitled_late+
                        '&policy_id='+policy_id+
                        '&update_id='+update_id+
                        '&work_upto='+work_upto+
                        '&allowance='+allowance+
						'&db_row_ids_mst='+db_row_ids_mst);
                        
                    http.onreadystatechange = response_tiffin_policy;
                    http.send(null);
                }
            }
			// Response comes here
            function response_tiffin_policy() 
            {
                if(http.readyState == 4)
                {
                    //alert (http.responseText);
                    var response = http.responseText;
                    if( response == 1 ) 
                    {
                        $("#messagebox").fadeTo( 200, 0.1, function() 
                        {
                            $(this).html('Tiffin bill policy have been inserted successfully.').addClass('messagebox_ok').fadeTo( 900, 1 )
                            $(this).fadeOut(5000);
                            //document.getElementById('tiffin_allowance_policy').reset();
                        });
						populate_data();
                    }
                    else if( response == 2 ) 
                    {
                        $("#messagebox").fadeTo( 200, 0.1, function() 
                        {
                            $(this).html('Tiffin bill policy Update successfully .').addClass('messagebox_ok').fadeTo( 900, 1 )
                            $(this).fadeOut(5000);
                            //document.getElementById('tiffin_allowance_policy').reset();
                        });
						populate_data();
                    }
                }
            }
            // Click on the each ROW of list view this function will be execute
            function js_set_value(policy_id,policy_name,etitled_late)
            {
                //alert("I am from ROW click");
                document.getElementById('policy_id_mst').value=policy_id;
                document.getElementById('policy_name').value=policy_name;
                document.getElementById('etitled_late').value=etitled_late;
                $.ajax({
                            type: "POST",
                            url: "../hr_admin_data.php",
                            data: 'data=tiffin_bill_policy&policy_id=' + policy_id,
                            success: function( data ) {
                            $('#tiffin_bill tbody tr').remove();
                            $('#tiffin_bill tbody').append(data);
                            document.getElementById('db_row_ids_mst').value=document.getElementById('db_row_ids').value;
                        $('.timepicker').timepicker({
                            H: true
                        });
                    }
                });
            }
			//duplicacy check here
			function check_duplicacy( id, str )
			{
				alert(id);
				var rowCount = $('#tiffin_bill tbody tr').length;
				var dd=id.split("_");
				for (var i=1; i<=rowCount; i++)
				{
					if( dd[1]!=i )
					{
						if( $('#allowance_'+i).val()==str )
						{
							$('#'+id).val('');
							$('#messagebox').fadeTo( 200, 0.1, function() {
								$('#allowance_'+(i+1)).focus();
								$(this).html('Duplicate value can not be enter.').addClass('messageboxerror').fadeTo(900,1);
								$(this).fadeOut(5000);
							});
							return false;
						}
					}
				} 
			}
			
        </script>
        <style>
            .header { background-color:#8FAA8C; border-bottom:medium double; border-color:#350402 #350402 -moz-use-text-color; font-size:12px; height:20px; }
        </style>
    </head>
    <body style="font-family:verdana; font-size:11px;">
        <div align="center" style="width:1070px;">
            <div style="height:60px;">    	
                <div class="form_caption">Tiffin Bill Policy</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update.$delete;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000; width:100%;" align="center"></div>
            </div>
            <form id="tiffin_bill_policy" name="tiffin_bill_policy" action="javascript:fun_tiffin_policy(save_perm,edit_perm,delete_perm)" autocomplete="off" method="post">
                <fieldset style="width:75%;">
                    <legend>Tiffin Bill Policy</legend>
                    <fieldset style="width:65%;">
                        <table width="70%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td>Policy Name</td>
                                
                                <td>
                                    <input type="text" name="policy_name" id="policy_name" class="text_boxes" style="width:100px;" />
                                    <input type="hidden" id="policy_id_mst" name="policy_id_mst" class="text_boxes" style="width:100px;" />
                                    <input type="hidden" id="db_row_ids_mst" name="db_row_ids_mst" class="text_boxes" style="width:100px;" />
                                </td>
                                <td>Entitled for Late</td>
                                <td>
                                    <select id="etitled_late" name="etitled_late" class="combo_boxes">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>	
                                </td>
                            </tr>
                        </table>
                        </fieldset>
                        <fieldset style="width:65%;">
                            <table width="250px">
                                <tr>
                                    <td>
                                        <table width="250px" border="0" cellpadding="1" cellspacing="1" id="tiffin_bill" align="center">
                                            <thead>
                                            <tr>
                                                <th width="250px">&nbsp;</th>
                                                <th class="header" width="250px">Must Stay</th>
                                                <th class="header" width="250px">Bill Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><input type="hidden" id="policy_id_1" name="policy_id[]" class="text_boxes" style=" width:100px; text-align:center;" /></td>
                                                <td><input type="text" id="work_upto_1" name="work_upto[]" class="timepicker text_boxes" style=" width:120px; text-align:center;" /></td>
                                                <td><input type="text" id="allowance_1" name="allowance[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" onblur="check_duplicacy(this.id,this.value)" style=" width:120px; text-align:right;" /></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table>
                                            <tr><td><input type="button" name="add" id="add" value="Add Row" class="formbutton" onclick="add_row();" style="width:90px;" /></td></tr>
                                            <tr><td><input type="button" name="remove" id="remove" value="Remove Row" class="formbutton" style="width:90px;" onclick="remove_row();" /></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <table align="center">
                            <tr><td colspan="3">&nbsp; </td></tr>
                            <tr>
                                <td><input type="submit" name="save" id="save" class="formbutton" value="Save" style="width:90px;"/></td>
                                <td><input type="button" name="refresh" id="refresh" class="formbutton" value="Refresh" onclick="reset_form()" style="width:90px;" /></td>
                            </tr>
                        </table>
                </fieldset>
            </form> 
        </div>
        <div id="data_panel" class="demo_jui" align="center" style="width:824px; margin-top:15px; margin-left:122px;"></div>
	</body>
</html>
<?php
/*************************************
|	Tiffin allowance policy
|	Developed by : Md. Nuruzzaman 
*************************************/
if($action=="tiffin_policy")
{
	if($policy_id=="") // Insert
	{
		$mid = return_next_id( "id", "lib_policy_tiffin_allowance" );
		$policy_id_mst = return_next_id( "policy_id", "lib_policy_tiffin_allowance" );
		
		//$update_id=explode("_",$update_id);
		$work_upto=explode("_",$work_upto);
		$allowance=explode("_",$allowance);
		$sql="INSERT INTO lib_policy_tiffin_allowance(
			id,
			policy_id,
			policy_name,
			work_upto,
			etitled_late,
			allowance
			) VALUES";
		for($i=0; $i<count($work_upto);$i++)
		{
			if($work_upto[$i]!="")
			{
				$mid1=$mid+$i;
				if($sqlpa!="") $sqlpa.=",";
				$sqlpa.="(
				'$mid1',
				'$policy_id_mst',
				'$policy_name',
				'".$work_upto[$i]."',
				'$etitled_late',
				'".$allowance[$i]."')";
			}
		}
			mysql_query( $sql.$sqlpa ) or die (mysql_error());
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			echo 1;
			exit();
	}
	else //Update
	{
		$update_id=explode("_",$update_id);
		$work_upto=explode("_",$work_upto);
		$allowance=explode("_",$allowance);
		for($i=0; $i<count($work_upto);$i++)
		{
			if($update_id[$i]=="") // Insert
			{
				$mid = return_next_id( "id", "lib_policy_tiffin_allowance" );
				$sql="INSERT INTO lib_policy_tiffin_allowance(
					id,
					policy_id,
					policy_name,
					work_upto,
					etitled_late,
					allowance
					) VALUES (  
					'$mid',
					'$policy_id',
					'$policy_name',
					'".$work_upto[$i]."',
					'$etitled_late',
					'".$allowance[$i]."')";
					 
					mysql_query( $sql ) or die (mysql_error());
			}
			else // Update
			{
				$sql="update lib_policy_tiffin_allowance set
					policy_name='".$policy_name."',
					work_upto='".$work_upto[$i]."',
					allowance='".$allowance[$i]."'
					where id='".$update_id[$i]."'";
					mysql_query( $sql ) or die (mysql_error());
			}
		}
		echo 2;
		exit();
	}
}
// END of Tiffin Allowance Policy

/*************************************
|	Tiffin allowance policy
|	Developed by : Md. Nuruzzaman 
*************************************/
if( isset( $data ) && $data == 'tiffin_allowance_policy' ) 
{
	$sql = "SELECT * FROM  lib_policy_tiffin_allowance where policy_id='$policy_id'";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

	$i=1;
	while( $row = mysql_fetch_assoc( $result ) ) 
	{
      ?>
       <tr>
           <td align="center"><input type="hidden" id="update_id_<?php echo $i; ?>" name="update_id[]" class="text_boxes" value="<?php echo $row[id]; ?>" style="width:120px; text-align:center;" /></td>
           <td align="center"><input type="text" id="work_upto_<?php echo $i; ?>" name="work_upto[]" class="timepicker text_boxes" value="<?php echo $row[work_upto]; ?>" style="width:120px; text-align:center;" /></td>
            <td align="center"><input type="text" id="allowance_<?php echo $i; ?>" name="allowance[]" class="text_boxes" value="<?php echo $row[allowance]; ?>" style="width:120px;text-align:right;" /></td>
       </tr>	
	<?php
	$i++;	
	}
}
// END of Tiffin Allowance Policy 


//-------------------------------- ---------- ----------------------------------------------------//
//-------------------------------- END Data Load For Update  -------------------------------------//
//-------------------------------- ---------- ----------------------------------------------------//


?>