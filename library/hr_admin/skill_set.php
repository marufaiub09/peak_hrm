<?php
/**************************************
|	Developed by	: Md. Nuruzzaman
|	Date			: 20.01.2014
***************************************/
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');


//company_details
$sql = "SELECT * FROM lib_company WHERE status_active=1 and is_deleted = 0 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Logic Payroll Software</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
        <meta name="description" content="Logic Payroll Software" />
        
		<script src="includes/ajax_submit_hr_admin.js" type="text/javascript"></script>
        <script src="../../includes/update_status.js" type="text/javascript"></script>
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        
        <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
        
         <script type="text/javascript" src="../../js/driver.phonetic.js" ></script>
        <script type="text/javascript" src="../../js/driver.probhat.js" ></script>
        <script type="text/javascript" src="../../js/engine.js" ></script>
        
        <script>
            var save_perm = <? echo $permission[0]; ?>;
            var edit_perm = <? echo $permission[1]; ?>;
            var delete_perm = <? echo $permission[2]; ?>;
            var approve_perm = <? echo $permission[3]; ?>;
            
            $(document).ready(function() {
                populate_data();	
            });
            function populate_data()
            {
               $('#data_panel').html('<img src="../../../resources/images/loading.gif" />');
                $.ajax({
                    type: "POST",					
                    url: "hr_admin_data.php",
                    data: 'form=frm_skill_assign',
                    success: function(data) {
                        $('#data_panel').html( data );
                        $('#tbl_skill_assign').dataTable({
                            "bRetrieve": true,
                            "bDestroy": true,
                            "bJQueryUI": true,
                            "bPaginate": false,
                             //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
                            //"aaSorting": [[ 0, "asc" ]],*/
                            "oLanguage": { "sSearch": "Search all columns:" },
                            "sScrollX": "100%",
                            "sScrollY": "200px",
                            "sScrollXInner": "100%",
                            "bScrollCollapse": true
                        });
                    }
                });
            }
            function js_set_value(update_id,skill_name,status,sequence)
            {
                //alert(update_id+', '+training_name+', '+short_name+', '+status);
                $('#is_update').val(update_id);
                $('#skill_name').val(skill_name);
                $('#cbo_status').val(status);
				$('#sequence').val(sequence);	
            }
            
            function reset_form()
            {
                $('#is_update').val('');
                $('#skill_name').val('');
                $('#cbo_status').val(1);	
            }
			
		//Numeric Value allow field script
		function numbersonly(myfield, e, dec)
		{
			var key;
			var keychar;
		
			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);
		
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			
			// numbers
			else if ((("0123456789.").indexOf(keychar) > -1))
				return true;
			else
				return false;
		}
        
        </script>
		<style>
            #designation_details input[type="text"] { width:150px; }
            #designation_details select { width:150px; }
            .formbutton { width:100px; }
            .ui-icon { cursor:pointer; }
            .ui-icon-check, .ui-icon-closethick { float:left; }
            .ui-icon-trash { float:right; }
        </style>
    </head>
    <body>
        <div align="center">
            <div style="width:920px; height:55px;" >
                <div class="form_caption">Skill Set</div>
                <span id="permission_caption"><? echo "Your Permissions--> \n".$insert.$update;?> </span>
                <div style="height:12px; width:624px;"><div id="messagebox" style="background-color:#FF9999; color:#000000;" align="center"></div></div>
            </div>
            <form name="frm_skill_assign" id="frm_skill_assign" action="javascript:fnc_skill_assign(save_perm,edit_perm,delete_perm,approve_perm);" method="post">
                <fieldset style="margin-top:10px; width:600px;"><legend>Skill Set</legend>
                    <table border="1" cellpadding="0" cellspacing="2" align="center">
                        <thead>
                        	<th class="table_header"  width="80px">Sequence</th>
                            <th class="table_header"  width="200px">Skill Name</th>
                        	<th class="table_header">Status</th>
                            <th class="table_header"><input type="submit" name="save" id="save" class="formbutton" value="Save" /></th>
                        </thead>
                        <tbody>
                        	<tr> 
                            	<td><input type="text" name="sequence" id="sequence" class="text_boxes" onkeypress="return numbersonly(this,event)" style="width:80px;" /></td>
                                <td><input type="text" name="skill_name" id="skill_name" class="text_boxes" style="width:250px;" /></td>
                                <td>
                                    <select name="cbo_status" id="cbo_status" class="combo_boxes">
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                    </select>
                                </td>
                                <th>
                                <input type="hidden" name="is_update" id="is_update" value="" />
                                <input type="button" name="cancel" id="cancel" class="formbutton" value="Refresh" onclick="reset_form()" /></th>
                            </tr>
                        </tbody>
                    </table>            	
                </fieldset>
            </form>
        </div>
        <table align="center">
        <tr>
        <td>
        <div id="data_panel" class="demo_jui" align="center" style="width:624px; margin-top:7px;"></div>
        </td>
        </tr>
        </table>
    </body>
</html>
