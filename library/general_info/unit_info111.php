<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 28-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------


include('../../includes/common.php');
include('../../includes/array_function.php');

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Sample Info</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script src="includes/ajax_submit.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>

	<script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
				
		
	</script>

</head>
<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px;">	
    <div>
    	<div class="form_caption">
		Unit Information
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;?>		
        </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<fieldset style="width:400px;">
		<legend>Unit Info</legend>
		<form name="unit_info" id="unit_info" method="" action="javascript:fnc_unit_info(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">
  <table cellpadding="0" cellspacing="2" width="65%"  align="center">

     <tr>
        <td width="130">Product </td>
        <td><input type="text" name="unit_name" id="unit_name" class="text_boxes" style="width:205px;"></td>
      </tr>
      <tr>
        <td>Unit Price</td>
        <td colspan="2"><input type="text" name="description" id="description" class="text_boxes"  style="width:205px;"></td>
      </tr>
      <tr>
        <td>Delete</td>
        <td valign="top" width="456"><select name="cbo_delete" class="combo_boxes" id="cbo_delete" style="width:86px;">
          <option >---Select---</option>
          <? foreach($is_deleted as $key=>$value):?>
          <option  value="<?php echo $key;?>"><?php echo $value;?></option>
          <?php endforeach;?>
        </select>
          Status
          <select name="cbo_status" class="combo_boxes" id="cbo_status" style="width:86px;">
          <option >---Select---</option>
          <? foreach($status_active as $key=>$value):?>
          <option  value="<?php echo $key;?>"><?php echo $value;?></option>
          <?php endforeach;?>
        </select>
          </td>
      
      </tr>
     <tr>
     
        <td colspan="6" align="center" style="padding-top:5px;" class="button_container"><input type="button" name="submit" id="submit" value="Submit" class="formbutton" onClick="fnc_unit_info(save_perm,edit_perm,delete_perm,approve_perm)">
          &nbsp;&nbsp;
          <input type="button" name="update" id="update" value="Update" class="formbutton">
          <input type="hidden" name="save_up" id="save_up"></td>
      </tr>

      </table>
  </form>

<!--<div style="width:900px; float:left; margin:auto" align="center">
-->		<fieldset style="margin-top:20px">
			<legend>Press Space For Unit Info</legend>
            <table cellpadding="0" cellspacing="2" width="100%">        	
                <tr>
                    <td align="center">
				<input type="text" size="30" style="width:200px;" placeholder="Press Space For Search" class="text_boxes" id="txt_search"  name="txt_search"onKeyUp="showResult(this.value,'1','unit_list_view')" />
<div id="unit_list_view">
</div>
 </td>
 </tr>            
</table>
</fieldset>
</fieldset>

</body>
</html>





