<?php
/*************************************
|	Developed by	: Md. Nuruzzaman
|	Date			: 04.02.2014
**************************************/
session_start();
 
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);

$permission=explode('_',$permission);
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
	
include('../../includes/common.php');
include('../../includes/array_function.php');

// company
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 $company_cond ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = mysql_real_escape_string($row['company_name']);
}
// buyer 
$sql="select * from lib_buyer where is_deleted=0 and status_active=1 order by buyer_name";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
$buyer_details = array();
while ($row=mysql_fetch_array($result)) {
	$buyer_details[$row['id']]=mysql_real_escape_string($row['buyer_name']);
}
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <LINK REL="SHORTCUT ICON" HREF="images/logic_logo.png">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <script src="includes/ajax_submit.js" type="text/javascript"></script>
        <script src="includes/functions.js" type="text/javascript"></script>
        <link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
        <link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
        
        <link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
		<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
		<script>
			var save_perm 		=<? echo $permission[0]; ?>;
			var edit_perm		=<? echo $permission[1]; ?>;
			var delete_perm 	=<? echo $permission[2]; ?>;
			var approve_perm 	=<? echo $permission[3]; ?>;
			
			$(document).ready(function() {
				add_process(0);
				populate_data();
			});

			function populate_data() {			
				$('#data_panel').html('<img src="../../resources/images/loading.gif" />');
				$.ajax({
					type: "POST",
					url: "list_view.php",
					data: 'form=frm_sewing_process',
					success: function(data) {
						$('#data_panel').html( data );
						$('#tbl_sewing_process').dataTable({
							"bRetrieve": true,
								"bDestroy": true,
								"bJQueryUI": true,
								"bPaginate": false,
								 //"aoColumns": [ { "sType": "html" }, null, null, null, null, null, { "bSortable": false } ],
								//"aaSorting": [[ 0, "asc" ]],*/
								"oLanguage": { "sSearch": "Search all columns:" },
								"sScrollX": "100%",
								"sScrollY": "200px",
								"sScrollXInner": "100%",
								"bScrollCollapse": true
						});
					}
				});
			}

			<?
			$sql= mysql_db_query($DB,"select process_name from  hrm_process_mst where is_deleted=0 and status_active=1 group by process_name");
			while ($row=mysql_fetch_array($sql)) {
				$process_name_dtls.= '"'.mysql_real_escape_string($row['process_name']).'",';
			}
			?>
			$(function() {
				var process_name = [<? echo substr($process_name_dtls, 0, -1); ?>];
				$("#process_name").autocomplete({
					source: process_name 
				});
			});
			
			<?
			$sql= mysql_db_query($DB,"select machine_type from lib_machine_type where is_deleted=0 and status_active=1 order by machine_type");
			while ($row=mysql_fetch_array($sql)) {
				$machine_type_dtls.= '"'.mysql_real_escape_string($row['machine_type']).'",';
			}
			?>
			$(function() {
				var machine_type = [<? echo substr($machine_type_dtls, 0, -1); ?>];
				$("#machine_type").autocomplete({
					source: machine_type 
				});
			});

			//Numeric Value allow field script
			function numbersonly(myfield, e, dec)
			{
				var key;
				var keychar;
			
				if (window.event)
					key = window.event.keyCode;
				else if (e)
					key = e.which;
				else
					return true;
				keychar = String.fromCharCode(key);
			
				// control keys
				if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
				return true;
				
				// numbers
				else if ((("0123456789.,-").indexOf(keychar) > -1))
					return true;
				else
					return false;
			}
			
			// add row 
			function add_process(rowcnt)
			{
				//alert("su..re");
				var rowCount=$("#tbl_process_rate_dtls tbody tr").length;
				if( rowcnt == rowCount ) 
				{
					rowCount++;
					$('#tbl_process_rate_dtls tbody').append(
					'<tr>'
						+'<td>'
							+'<input type="hidden" id="update_id_' + rowCount + '" name="update_id[]" value="" class="text_boxes" />'
							+'<select name="buyer_id[]" id="buyer_id_' + rowCount + '" class="combo_boxes" style="width:200px;" onfocus="add_process(' + rowCount + ' );">'
								+'<option value="0">-- Select --</option>'
								<?php foreach( $buyer_details AS $key=>$value ){ ?>
								+'<option value="<?php echo $key; ?>"><?php echo $value; ?></option>'
								<?php } ?>
							+'</select>'
						+'</td>'
						/*+'<td><input type="text" id="style_ref_' + rowCount + '" name="style_ref[]" class="text_boxes" style="width:150px;" /></td>'*/
						+'<td><input type="text" id="rate_' + rowCount + '" name="rate[]" class="text_boxes" onKeyPress="return numbersonly(this, event);" style="width:150px;" /></td>'
						+ '<td>'
							+'<select name="uom[]" id="uom_' + rowCount + '" class="combo_boxes" style="width:150px;">'
							<?php foreach($uom_arr as $key=>$value) { ?>
							+'<option value="<?php echo $key;?>"><?php echo $value;?></option>'<? }?> 
						+'</select>'
						+'</td>'
					+ '</tr>'
					);
				}
			}
			
			function js_set_value(id,company,process,machine,rate,id_machine_type)
			{
				//alert("su..re");
				var piece_id=$('#piece_id').val(id);
				var update_id_machine_type=$('#update_id_machine_type').val(id_machine_type);
				var company_id=$('#company_id').val(company);
				var process_name=$('#process_name').val(process);
				var machine_type=$('#machine_type').val(machine);
				var sewing_rate=$('#sewing_rate').val(rate);
				
                $.ajax({
					type: "POST",
					url: "list_view.php",
					data: 'data=show_sewing_process_dtls&process_id=' + id,
					success: function( data ) 
					{
						//alert(data);
						if(data==1)
						{
							$('#tbl_process_rate_dtls tbody tr').remove();
							add_process(0);
						}
						else
						{
							$('#tbl_process_rate_dtls tbody tr').remove();
							$('#tbl_process_rate_dtls tbody').append(data);
							document.getElementById('db_row_ids_mst').value=document.getElementById('db_row_ids').value;
						}
					}
                });
			}
			
			// frm_refresh
			function frm_refresh()
			{
				//alert("su..re");
				$('#company_id').val(0);
				$('#process_name').val('');
				$('#machine_type').val('');
				$('#sewing_rate').val(0);
				$('#piece_id').val('');
				$('#update_id_machine_type').val('');
				$('#db_row_ids_mst').val('');
				
				$('#tbl_process_rate_dtls tbody tr').remove();
				add_process(0);
			}
		</script>
		<style>
        </style>	
    </head>
    <body style="font-family:verdana; font-size:11px;">
    <div align="center" style="width:900px;">
            <div>
                <div class="form_caption">Piece Rate Settings</div>
                <span id="permission_caption"><?php echo "Your Permissions--> \n".$insert.$update.$delete;?></span>
                <div id="messagebox" style="background-color:#FF9999; color:#000000; width:720px;" align="center"></div>
            </div>
            <fieldset style="width:700px;"><legend>Piece Rate</legend>
                <form name="frm_sewing_process" id="frm_sewing_process" method="post" action="javascript: fnc_sewing_process(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete="off">
                	<fieldset>	
                        <table cellpadding="0" cellspacing="0" width="100%" class="rpt_table" >
                            <thead>
                                <th width="180px"><strong>Company Name</strong></th>
                                <th width="160px"><strong>Process Name</strong></th>
                                <th width="160px"><strong>Machine Type</strong></th>
                                <th width="160px"><strong>Rate For</strong></th>
                            </thead>
                            <tbody>
                                <td>
                                    <select id="company_id" name="company_id" class="combo_boxes" style="width:180px;">
                                        <?php if($company_cond=="")
                                        { 
                                        ?>
                                        <option value="0">-- Select --</option>
                                        <?php } foreach( $company_details AS $key=>$value ){ ?>
                                        <option value="<?php echo $key; ?>" <?php if(count($company_details)==1)echo "selected"; ?>><?php echo $value; ?></option>
                                        <?php } ?>                                
                                    </select>
                                </td>
                                <td><input type="text" id="process_name" name="process_name" class="text_boxes" style="width:160px;" /> </td>
                                <td><input type="text" id="machine_type" name="machine_type" class="text_boxes" style="width:160px;" /></td>
                                <td>
                                    <select id="sewing_rate" name="sewing_rate" class="combo_boxes" style="width:160px;">
                                        <?php
                                            foreach($sewing_process_rate_arr as $key=>$value)
                                            {
                                        ?>
                                            <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                        <?php										
                                            }
                                        ?> 
                                    </select>
                                </td>
                            </tbody>
                        </table>
                    </fieldset>
                    <div style="height:15px;"></div>
                	<fieldset style="width:500px">	
                        <table cellpadding="0" cellspacing="0" width="500px" class="rpt_table" id="tbl_process_rate_dtls" >
                            <thead>
                                <th width="200px"><strong>Buyer Name</strong></th>
                                <!--<th width="150px"><strong>Style Ref.</strong></th>-->
                                <th width="150px"><strong>Rate</strong></th>
                                <th width="150px"><strong>UOM</strong></th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </fieldset>
                    </table>
                    <div style="height:15px;"></div>
                    <table  cellpadding="0" cellspacing="2" width="100%">	
                        <tr>
                            <td colspan="2" align="center" class="button_container">
                            <input type="submit" value="Save" name="save" style="width:80px" id="save"   class="formbutton"/>&nbsp;&nbsp;
                            <input type="reset" value="Refresh" style="width:80px" name="reset" id="reset" class="formbutton" onclick="frm_refresh();"/>
                            <input type="hidden" name="piece_id" id="piece_id" value=""/>
                            <input type="hidden" name="update_id_machine_type" id="update_id_machine_type" value=""/>
                            <input type="hidden" id="db_row_ids_mst" name="db_row_ids_mst" class="text_boxes" style="width:100px;" />	
                            </td>					
                        </tr>				
                    </table>
                </form>	
            </fieldset>
            <div id="data_panel" class="demo_jui" align="center" style="width:750px; margin-top:15px;"></div>	
        </div>
    </body>
</html>