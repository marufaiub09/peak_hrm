//Do not change 
function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer"){
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	}else{
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();

//---------------------------Bank Info-----------------------------------------------------------------------
function fnc_bank_info(save_perm,edit_perm,delete_perm,approve_perm){
	
	var txt_mst_id				= escape(document.getElementById('txt_mst_id').value);
	var txt_bank_name			= escape(document.getElementById('txt_bank_name').value);
	var txt_branch_name			= escape(document.getElementById('txt_branch_name').value);
	var txt_bank_code			= escape(document.getElementById('txt_bank_code').value);
	var txt_bank_address		= escape(document.getElementById('txt_bank_address').value);
	var txt_bank_email			= escape(document.getElementById('txt_bank_email').value);
	var txt_bank_website		= escape(document.getElementById('txt_bank_website').value);
	var txt_bank_contact_person	= escape(document.getElementById('txt_bank_contact_person').value);
	var txt_bank_phone_no		= escape(document.getElementById('txt_bank_phone_no').value);	
	var cb_lien_bank			= escape(document.getElementById('cb_lien_bank').value);
	var cb_issuing_bank			= escape(document.getElementById('cb_issuing_bank').value);
	var cb_salary_bank			= escape(document.getElementById('cb_salary_bank').value);
	var cb_advs_bank			=escape(document.getElementById('cb_advs_bank').value);	
	var txt_remarks				= escape(document.getElementById('txt_remarks').value);
	
	var save_up 				= escape(document.getElementById('save_up').value);
	
	var bank_type="";	
	if (document.getElementById('cb_lien_bank').checked==true)
	{
		cb_lien_bank=1;
	}
	else
	{	
		cb_lien_bank=0;
	}
	if (document.getElementById('cb_issuing_bank').checked==true)
	{
		cb_issuing_bank=1;
	}
	else
	{
		cb_issuing_bank=0;	
	}

	if (document.getElementById('cb_salary_bank').checked==true)
	{
		cb_salary_bank=1;
	}
	else
	{
		cb_salary_bank=0;
	}

	if (document.getElementById('cb_advs_bank').checked==true)
	{
		cb_advs_bank=1;
	}
	else
	{
		cb_advs_bank=0;
	}

	
	//	alert(txt_bank_name);	
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}	
	else if($('#txt_bank_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_bank_name').focus();
			$(this).html('Please Enter Bank Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=bank_info&isupdate='+save_up+
					'&txt_bank_name='+txt_bank_name+
					'&txt_branch_name='+txt_branch_name+
					'&txt_bank_code='+txt_bank_code+
					'&txt_bank_address='+txt_bank_address+
					'&txt_bank_email='+txt_bank_email+
					'&txt_bank_website='+txt_bank_website+
					'&txt_bank_contact_person='+txt_bank_contact_person+
					'&txt_bank_phone_no='+txt_bank_phone_no+
					'&cb_lien_bank='+cb_lien_bank+
					'&cb_issuing_bank='+cb_issuing_bank+
					'&cb_salary_bank='+cb_salary_bank+
					'&cb_advs_bank='+cb_advs_bank+
					'&txt_remarks='+txt_remarks+
					'&nocache ='+nocache);
		http.onreadystatechange = bank_info_reply;
		http.send(null); 
	}
}

function bank_info_reply() {
	if(http.readyState == 4){ 		
		var responseString = (http.responseText).split("_");		
		var response = responseString[0];	
		//alert(response);
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Bank Name, Please Check agian.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','bank_info','update','../../');
				
				showResult(' ','4','bank_list_view');				
				document.getElementById('save_up').value="";
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','bank_info','save','../../');
				
				showResult(' ','4','bank_list_view');
				document.getElementById('save_up').value="";
			});
		}
							
		
	//alert(response);
	
	}
}	



//---------------------------Bank Account Info ----------------------------------------------------------------------
function fnc_bank_acc_info(save_perm,edit_perm,delete_perm,approve_perm){
	
	var txt_mst_id	= escape(document.getElementById('txt_mst_id').value);
	var cbo_account_type	= escape(document.getElementById('cbo_account_type').value);
	var txt_account_no	= escape(document.getElementById('txt_account_no').value);
	var cbo_currency	= escape(document.getElementById('cbo_currency').value);
	var txt_loan_limit	= escape(document.getElementById('txt_loan_limit').value);
	var cbo_company_name	= escape(document.getElementById('cbo_company_name').value);
	var cbo_po_status	= escape(document.getElementById('cbo_po_status').value);
	var save_up_dtls = escape(document.getElementById('save_up_dtls').value);
	var cbo_loan_type = escape(document.getElementById('cbo_loan_type').value);
			
			//alert(txt_mst_id)		
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up_dtls=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up_dtls!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( cbo_po_status==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}	
	else if($('#txt_mst_id').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_mst_id').focus();
			$(this).html('Please Insert Bank Info').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_account_type').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_account_type').focus();
			$(this).html('Please Select Account Type').addClass('messageboxerror').fadeTo(900,1);
		});		
	}				
	else if($('#txt_account_no').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_account_no').focus();
			$(this).html('Please Enter Account Number').addClass('messageboxerror').fadeTo(900,1);
		});		
	}	
	
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=account_info_dtls'+
					'&txt_mst_id='+txt_mst_id+
					'&save_up_dtls='+save_up_dtls+
					'&cbo_account_type='+cbo_account_type+
					'&txt_account_no='+txt_account_no+
					'&cbo_currency='+cbo_currency+
					'&txt_loan_limit='+txt_loan_limit+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_po_status='+cbo_po_status+
					'&cbo_loan_type='+cbo_loan_type+
					'&nocache ='+nocache);
		http.onreadystatechange = fnc_account_info_dtls_reply;
		http.send(null); 
	}
}

function fnc_account_info_dtls_reply() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('_');		
		//alert(response[2]);
		
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Entry, Please Check.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				if (response[2]!='')
				{
					save_activities_history(response[2],'library','account_info','update','../../');
				}
				document.getElementById('txt_mst_id').value=response[1];
				document.getElementById('save_up_dtls').value="";
				document.getElementById('cbo_account_type').value="";
				document.getElementById('txt_account_no').value="";
				document.getElementById('cbo_currency').value="";
				document.getElementById('txt_loan_limit').value="";
				document.getElementById('cbo_company_name').value="";
				document.getElementById('cbo_po_status').value="";
				showResult(response[1],'5','account_list_view');
				//showResultTeamMember(document.getElementById('txt_mst_id').value,'5','account_list_view');
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				if (response[2]!='')
				{
					save_activities_history(response[2],'library','account_info','save','../../');
				}
				document.getElementById('txt_mst_id').value=response[1];		
				document.getElementById('save_up_dtls').value="";
				document.getElementById('cbo_account_type').value="";
				document.getElementById('txt_account_no').value="";
				document.getElementById('cbo_currency').value="";
				document.getElementById('txt_loan_limit').value="";
				document.getElementById('cbo_company_name').value="";
				document.getElementById('cbo_po_status').value="";
				showResult(response[1],'5','account_list_view');
			});
		}
							
		
	//alert(response);
	
	}
}	


//---------------------------Buyer Info-----------------------------------------------------------------------
function fnc_buyer_info(){
	
	var txt_buyer_name		= escape(document.getElementById('txt_buyer_name').value);
	var txt_contact_person	= escape(document.getElementById('txt_contact_person').value);
	var txt_contact_no  	= escape(document.getElementById('txt_contact_no').value);
	var txt_web_site		= escape(document.getElementById('txt_web_site').value);
	var txt_buyer_email     = escape(document.getElementById('txt_buyer_email').value);
	var txt_address         = escape(document.getElementById('txt_address').value);
	var cbo_lib_country_id 	= escape(document.getElementById('cbo_lib_country_id').value);
	var txt_remark 			= escape(document.getElementById('txt_remark').value);
	
	var save_up 			= escape(document.getElementById('save_up').value);
					
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#txt_buyer_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_buyer_name').focus();
			$(this).html('Please Enter Buyer Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else if($('#txt_contact_person').val()==""){		
		$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
			$('#txt_contact_person').focus();
			$(this).html('Please Enter Contact Person Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else if($('#txt_contact_no').val()==""){		
		$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
			$('#txt_contact_no').focus();
			$(this).html('Please Enter Contact Number').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_lib_country_id').val()==""){		
		$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
			$('#cbo_lib_country_id').focus();
			$(this).html('Please Enter Buyer Counttry').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else if($('#cbo_module_sts').val()==0){		
		$("#messagebox").fadeTo(200,0.1,function() { //start fading the messagebox
			$('#cbo_module_sts').focus();
			$(this).html('Please Enter Module status').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=buyer_details&isupdate='+save_up+
					'&txt_buyer_name='+txt_buyer_name+
					'&txt_contact_person='+txt_contact_person+
					'&txt_contact_no='+txt_contact_no+
					'&txt_web_site='+txt_web_site+
					'&txt_address='+txt_address+
					'&cbo_lib_country_id='+cbo_lib_country_id+
					'&txt_remark='+txt_remark+
					'&txt_buyer_email='+txt_buyer_email+
					'&nocache ='+nocache);
		http.onreadystatechange = buyerReply_info;
		http.send(null); 
	}
}

function buyerReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		///alert(response);
		document.getElementById('txt_buyer_name').value="";
		document.getElementById('txt_contact_person').value="";
		document.getElementById('txt_contact_no').value="";
		document.getElementById('txt_web_site').value="";
		document.getElementById('txt_address').value="";
		document.getElementById('cbo_lib_country_id').value="";
		document.getElementById('txt_remark').value="";
		document.getElementById('txt_buyer_email').value="";
	
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
							
		
	//alert(response);
	
	}
}	


//---------------------------Unit Info Start-----------------------------------------------------------------------
function fnc_unit_info(save_perm,edit_perm,delete_perm,approve_perm){
	
	var txt_mst_id				= escape(document.getElementById('txt_mst_id').value);
	var txt_unit_name			= escape(document.getElementById('txt_unit_name').value);
	var txt_unit_description	= escape(document.getElementById('txt_unit_description').value);
	var cbo_status 				= escape(document.getElementById('cbo_status').value);
	var cbo_is_deleted			= escape(document.getElementById('cbo_is_deleted').value);
	var save_up 				= escape(document.getElementById('save_up').value);

	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( (cbo_status==0 || cbo_is_deleted==1) && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	
	else if($('#txt_unit_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_unit_name').focus();
			$(this).html('Please Enter Unit Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=unit_info&isupdate='+save_up+
					'&txt_unit_name='+txt_unit_name+
					'&txt_unit_description='+txt_unit_description+
					'&cbo_status='+cbo_status+
					'&cbo_is_deleted='+cbo_is_deleted+
					'&nocache ='+nocache);
		
		http.onreadystatechange = unitReply_info;
		http.send(null); 
	}
}

function unitReply_info() {
		if(http.readyState == 4){ 		
		//var response = http.responseText;		
		var response = http.responseText.split('***');	
		//if( response[0] == 3 )	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Unit Name, Please Check agian.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{			
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				if (response[1]!='')
				{
					save_activities_history(response[1],'library','unit_info','update','../../');
				}
				showResult(' ','1','unit_list_view');				
				document.getElementById('save_up').value="";
				
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 				
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				if (response[1]!='')
				{
					save_activities_history(response[1],'library','unit_info','save','../../');
				}
				showResult(' ','1','unit_list_view');
				document.getElementById('save_up').value="";
			});
		}			
		else if (response[0]==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Updated Restricted!!!Already used this unit.').addClass('messageboxerror').fadeTo(900,1);				
			});
		}
	
	}
}	
//---------------------------Unit Info End--------------------------------------------------------------------------
//--------------------------Currency Info Start-----------------------------------------------------------------------
function fnc_currency_info(save_perm,edit_perm,delete_perm,approve_perm){
	var txt_mst_id	= escape(document.getElementById('txt_mst_id').value);
	var txt_currency_name	= escape(document.getElementById('txt_currency_name').value);
	var cbo_status	= escape(document.getElementById('cbo_status').value);
	var cbo_is_domestic	= escape(document.getElementById('cbo_is_domestic').value);
	var cbo_is_deleted	= escape(document.getElementById('cbo_is_deleted').value);
	var save_up = escape(document.getElementById('save_up').value);
	//alert(txt_mst_id);
	//return false;
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( (cbo_status==0 || cbo_is_deleted==1) && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if($('#txt_currency_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_currency_name').focus();
			$(this).html('Please Enter Currency Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=currency_info&isupdate='+save_up+
					'&txt_currency_name='+txt_currency_name+
					'&cbo_status='+cbo_status+
					'&cbo_is_domestic='+cbo_is_domestic+
					'&cbo_is_deleted='+cbo_is_deleted+
					'&nocache ='+nocache);
		http.onreadystatechange = currencyReply_info;
		http.send(null); 
	}
}

function currencyReply_info() {
	if(http.readyState == 4){ 		
		//alert(http.responseText);
		var response = http.responseText.split('_');	
			
		if (response[0]==1)
		{ 
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Currency name of this ID, Please use New Currency name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{alert(http.responseText);
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','currency_info','update','../../');
				
				document.getElementById('txt_mst_id').value=response[1];
				showResult(' ','2','currency_list_view');
				//(response[1],'5','bank_list_view');
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','currency_info','save','../../');
				
				document.getElementById('txt_mst_id').value=response[1];
				showResult(' ','2','currency_list_view');
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==4)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Domestic Currency available in DB, Please Check.').addClass('messagebox_ok').fadeTo(900,1);
			});
		}					
		
	//alert(response);
	
	}
}	

//--------------------------Currency Info End-----------------------------------------------------------------------

//---------------------------Sample Info Start-----------------------------------------------------------------------
function fnc_sample_info(){
	
	var txt_sample_name	= escape(document.getElementById('txt_sample_name').value);
	var save_up 			= escape(document.getElementById('save_up').value);
					
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#txt_sample_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_sample_name').focus();
			$(this).html('Please Enter Sample Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	} else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=sample_details&isupdate='+save_up+
					'&txt_sample_name='+txt_sample_name+
								'&nocache ='+nocache);
		
		http.onreadystatechange = sampleReply_info;
		http.send(null); 
	}
}

function sampleReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		//alert(response);
		document.getElementById('txt_sample_name').value="";
		
	
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
							
		
	//alert(response);
	
	}
}	


function fnc_store_location(save_perm,edit_perm,delete_perm,approve_perm) /// Save Update Store Information
{
	var txt_store_name	= escape(document.getElementById('txt_store_name').value);
	var cbo_company_name	= escape(document.getElementById('cbo_company_name').value);
	var txt_store_location	= escape(document.getElementById('txt_store_location').value);
	var cbo_status	= escape(document.getElementById('cbo_status').value);
	var save_up 			= escape(document.getElementById('save_up').value);					
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!='' && edit_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( cbo_status==2 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have delete permission.').fadeIn(1000);		
	}
	
	else if($('#txt_store_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_store_name').focus();
			$(this).html('Please Enter Store Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_company_name').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select a Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}  
	else
	{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=store_location&isupdate='+save_up+
					'&txt_store_name='+txt_store_name+
					'&cbo_company_name='+cbo_company_name+
					'&txt_store_location='+txt_store_location+
					'&cbo_status='+cbo_status+
								'&nocache ='+nocache);
		
		http.onreadystatechange = store_locationReply_info;
		http.send(null); 
	}
}


function store_locationReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText.split('###');			
	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo(900,1);				
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				if (response[1]!='')
				{
					save_activities_history(response[1],'library','store_location','update','../../');
				}
				showResult(' ','6','store_list_view');
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);				
				if (response[1]!='')
				{
					save_activities_history(response[1],'library','store_location','save','../../');
				}
				showResult(' ','6','store_list_view');
				document.getElementById('Submit').value="Save";
				document.getElementById('save_up').value="";
			});
		}	
		else if (response[0]==5)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Updated Restricted!!!Already used this store location.').addClass('messageboxerror').fadeTo(900,1);				
			});
		}
		
	}
}

//---------------------------Buyer Info sewing process-----------------------------------------------------------------------
function fnc_buyer_info_sp(save_perm,edit_perm,delete_perm,approve_perm){
	//alert("su..re");
	var txt_mst_id			= escape(document.getElementById('txt_mst_id').value);
	var txt_buyer_name		= escape(document.getElementById('txt_buyer_name').value);
	var txt_contact_person	= escape(document.getElementById('txt_contact_person').value);
	var txt_contact_no  	= escape(document.getElementById('txt_contact_no').value);
	var txt_web_site		= escape(document.getElementById('txt_web_site').value);
	var txt_buyer_email     = escape(document.getElementById('txt_buyer_email').value);
	var txt_address         = escape(document.getElementById('txt_address').value);
	var cbo_subcontract_party 	= escape(document.getElementById('cbo_subcontract_party').value);
	var cbo_status 			= escape(document.getElementById('cbo_status').value);
	var txt_remark 			= escape(document.getElementById('txt_remark').value);
	
	var save_up = escape(document.getElementById('save_up').value);
	//alert(save_up+"sure..");
					
		//alert (edit_perm);			
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (edit_perm==2 && save_up!="") // no edit 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Edit Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
	}
	else if (save_perm==2 && save_up=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have New Entry Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
		
	}
	else if (delete_perm==2 && cbo_status==2 ) 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Delete Permission.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_buyer_name').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_buyer_name').focus();
			$(this).html('Please Insert Buyer Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#cbo_subcontract_party').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_subcontract_party').focus();
			$(this).html('Please Select a Subcontract Party Option').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=buyer_info_sp&isupdate='+save_up+
					'&txt_buyer_name='+txt_buyer_name+
					'&txt_contact_person='+txt_contact_person+
					'&txt_contact_no='+txt_contact_no+
					'&txt_web_site='+txt_web_site+
					'&txt_buyer_email='+txt_buyer_email+
					'&txt_address='+txt_address+
					'&cbo_subcontract_party='+cbo_subcontract_party+
					'&cbo_status='+cbo_status+
					'&txt_remark='+txt_remark+
					'&nocache ='+nocache);
		http.onreadystatechange = buyerReply_info_sp;
		http.send(null); 
	}
}

function buyerReply_info_sp() {
		if(http.readyState == 4){ 		
		//var response = http.responseText;	
		
		var response = http.responseText.split('_');	
		 
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Buyer Name, Please Check agian.').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000); 
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history('',"library","buyer_info","update","../../");
				
				showResult_multi(' ',' ','1','buyer_list_view');
				 
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				$(this).fadeOut(5000);
				document.getElementById('txt_mst_id').value=response[1];
				//save_activities_history(response[2],"library","buyer_info_sp","insert","../../");
				showResult_multi(' ',' ','1','buyer_list_view');
				document.getElementById('save_up').value="";
				
			});
		}
	//alert(response);
	}
}	

// Sewing Process
function fnc_sewing_process(save_perm,edit_perm,delete_perm,approve_perm)
{
	//alert("su..re");
	
	var company_id=$('#company_id').val();
	var process_name=$('#process_name').val();
	var machine_type=$('#machine_type').val();
	var sewing_rate=$('#sewing_rate').val();
	
	var buyer_id=$('#buyer_id').val();
	var style_ref=$('#style_ref').val();
	var rate=$('#rate').val();
	var uom=$('#uom').val();

	//alert("P="+process_name+"M="+machine_type+"S="+sewing_rate+"Sl="+sl+"B="+buyer_id+"Stl="+style_ref+"R="+rate+"U="+uom );
	
	
	
	if (edit_perm==2) // no edit 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Edit Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
	}
	else if (save_perm==2)
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have New Entry Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
		
	}
	else if (delete_perm==2) 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Delete Permission.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#buyer_id').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#buyer_id').focus();
			$(this).html('Please Insert Buyer Name').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});
	}
	else if($('#rate').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#rate').focus();
			$(this).html('Please Insert Rate').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else if($('#uom').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#uom').focus();
			$(this).html('Please Select UOM').addClass('messageboxerror').fadeTo(900,1);
			$(this).fadeOut(5000);
		});	
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=sewing_process&company_id='+company_id+
					'&process_name='+process_name+
					'&machine_type='+machine_type+
					'&sewing_rate='+sewing_rate+
					
					'&' + 'buyer_id_' + counter + '=' + $(this).find('select[name="buyer_id[]"]').val()+ 
					'&' + 'style_ref_' + counter + '=' + $(this).find('input[name="style_ref[]"]').val()+ 
					'&' + 'rate_' + counter + '=' + $(this).find('input[name="rate[]"]').val()+
					'&' + 'uom_' + counter + '=' + $(this).find('select[name="uom[]"]').val()+ 

					'&buyer_id='+buyer_id+
					'&style_ref='+style_ref+
					'&rate='+rate+
					'&uom='+uom+
					'&nocache ='+nocache);
		http.onreadystatechange = response_sewing_process;
		http.send(null); 
	}
}
function response_sewing_process()
{
	if(http.readyState == 4)
	{
		var response = http.responseText.split('_');	
		 
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Machine Type').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000); 
			});
		}
		
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Insert Successfully').addClass('messageboxerror').fadeTo(900,1);
				$(this).fadeOut(5000); 
			});
		}
	}
}
	