
<script src="includes/ajax_submit.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Country Info</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<?php
date_default_timezone_set('UTC');
include('../../includes/common.php');
?>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px;">
	<div style="height:40px; width:100%; margin-bottom:30px;">
		Country Information
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
  	</div>
	<fieldset style="width:450px; text-align:center">
		<legend>Library Module</legend>
		<form name="country_info" id="country_info" method="" action="javascript:fnc_unit_info()" onSubmit="">	
			<table cellpadding="0" cellspacing="0" width="100%">
			  <!--DWLayoutTable-->
				<tr>
					<td width="677" height="40" valign="top">
						<table  cellpadding="3" cellspacing="0" width="100%">
					  	<!--DWLayoutTable-->				
							<tr>
								<td width="30%">Country Name</td>
								<td colspan="3">
								<input type="text" name="txt_country_name" id="txt_country_name" class="text_boxes" style="width:210px" />
								<!--div id="livesearch" style="width:150px"></div-->
								</td>
							</tr>			
							<tr >
								<td height="30">
									Delete
								</td>
								<td width="60" valign="top">
									<select name="cbo_is_deleted" id="cbo_is_deleted" class="combo_boxes" style="width:66px" >
										<option	 value="0" >No</option>
										<option	 value="1" >Yes</option>							
									</select>							
								</td>
								<td width="55" valign="top">Status </td>
								<td width="149" valign="top">
									<select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:74px" >
									  <option	 value="1" >Visible</option>
									  <option	 value="2" >Not visible</option>
                        			</select>
								</td>
							</tr>						
				    	</table>
					</td>
			  		<td width="217">&nbsp;</td>
			  	</tr>
				<tr>
				  <td height="1"></td>
				  <td></td>
			  	</tr>
				<tr>
					<td height="22" align="center" valign="top">			
						<input type="text" value="" name="save_up" id="save_up"  style="display:none"/>			  
					</td>					
			  	</tr>
				<tr>
			  		<td height="24" align="center" valign="top">
						<input type="submit" value="<?php if($save_up==""){ echo "Save"; }else{ echo "Update"; } ?>" name="save" id="save" class="formbutton"/>
					 	<input type="reset" value="  Refresh  " name="reset" id="reset" class="formbutton"/>
	            		<input type="submit" value="  Close  " name="close" id="close" class="formbutton"/>			  
					</td>					
				</tr>
			<tr>
				<td colspan="">
					<div style="width:470px; margin:auto" align="center">
						<fieldset style="width:470px; margin-top:10px">
							<legend>Enter search words or press space</legend>
							<form>
								<input type="text" size="30" class="text_boxes" onKeyUp="showResult(this.value,'1','livesearch1')" />
								<div style="width:400px; margin-top:10px" id="livesearch1" align="left"></div>
							</form>
						</fieldset>	
					</div>
				</td>
			</tr>
			</table>
		</form>	
		
	</fieldset>	
	
</div>
</body>
</html>
	