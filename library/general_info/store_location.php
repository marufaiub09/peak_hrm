<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 28-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------


include('../../includes/common.php');
include('../../includes/array_function.php');
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Store Location</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script src="includes/ajax_submit.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>

	<script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;		
	
    	function fn_reset_form()
		{
			window.location.reload(true);
		}
    
    </script>
    
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:900px;">       
        <div>
            <div class="form_caption">
            	Store Creation
            </div>
                <span id="permission_caption">
                    <? echo "Your Permissions--> \n".$insert.$update.$delete;?>		
                </span>
            <div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
		</div>
        <fieldset style="width:600px;">
       	 <legend>Store Location</legend>
            <form name="store_location" id="store_location" method="" action="javascript:fnc_store_location(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete = "off">	
            <table cellpadding="0" cellspacing="2" width="100%">
              <tr>
                <td width="121">Store Name </td>
                <td width="375"><input type="text" name="txt_store_name" id="txt_store_name" class="text_boxes" style="width:212px" /></td>            
              </tr>
              <tr>
                <td width="121">Company</td>
                <td>
                	<select name="cbo_company_name" id="cbo_company_name" class="text_boxes" style="width:222px">
                    	<option value="0"> ---- Select -----</option>
						<?
                        $sql= mysql_db_query($DB, "select * from lib_company where status_active=1 and is_deleted=0 order by id");
                        while ($selectResult = mysql_fetch_array($sql))
                        {
                        ?>
                    	<option value="<?php echo $selectResult["id"]; ?>"><?php echo $selectResult["company_name"]; ?></option>
						<? } ?>
                        
                        
                	</select>
                </td>                
              </tr>
              <tr>
                <td width="121">Store Location </td>
                <td><textarea  name="txt_store_location" id="txt_store_location" class="text_area" style="width:212px " onKeyUp="block_character_text_area()" ></textarea>            
                <input type="hidden" name="save_up" id="save_up">
                </td>
                
              </tr>
              <tr>
                <td width="121">Status</td>
                <td><select name="cbo_status" id="cbo_status" class="text_boxes" style="width:222px">
                  <?php
                        foreach($status_active as $key=>$value):
                        ?>
                        <option value=<? echo "$key";
                        if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                        <?		
                        endforeach;
                    ?>	
                </select></td>
              </tr>
              <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                  <tr>
                        <td align="center" colspan="2" class="button_container">
                          <input type="submit" name="Submit" id="Submit" class="formbutton" style="width:100px" value="Save">
                          <input type="reset" name="reset" class="formbutton" style="width:100px" value="Refresh" onClick="fn_reset_form()">                        
                         </td>
                  </tr>
            </table>
            </form>
          <fieldset style="margin-top:20px">
            <legend>Press space for Location Search</legend>
            <table cellpadding="0" cellspacing="2" width="100%">        	
                <tr>
                    <td align="center">      
                        <input type="text" name="txt_search" id="txt_search" style="width:200px" class="text_boxes" onKeyUp="showResult(this.value,6,'store_list_view')" placeholder="Press Space For Search">
                        <div id="store_list_view" align="left"></div>
                    </td>
                </tr>            
            </table>
        </fieldset>
        </fieldset>
        </div>
	</body>
</html>
