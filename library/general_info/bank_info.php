<?php
//Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="includes/functions.js" type="text/javascript"></script>

	<script src="includes/ajax_submit.js" type="text/javascript"></script>

	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	
    <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		/*function numbersonly(myfield, e, dec)
		{
			var key;
			var keychar;
			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			// numbers
			else if ((("0123456789.").indexOf(keychar) > -1))
				return true;
			else
				return false;
		}
		*/
		//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

	</script>
</head>
<body>
<div align="center" style="width:1000px;">
     <div>
       <div class="form_caption">
            Bank Information
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
         ?>
       </span>
   	<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
  </div>
		<fieldset style="width:900px;">
			<legend>Bank Info</legend>
			<table cellpadding="0" cellspacing="2" width="100%">					
					<tr>
							<td colspan="6" height="10"></td>
					</tr>
					<form name="bank_info" id="bank_info" method="post" action="javascript:fnc_bank_info(save_perm,edit_perm,delete_perm,approve_perm)">
						<tr>
							<td width="130" >Bank Name</td>
							<td width="170"> <!-- System Generated -->
								<input name="txt_bank_name" id="txt_bank_name" class="text_boxes" style="width:165px" />
							</td>
							<td width="130">Branch Name</td>
							<td width="170"> <!-- Selected & Add New-->
								<input name="txt_branch_name" id="txt_branch_name" class="text_boxes" style="width:165px">							
							</td>
							<td width="130">Bank Code</td>
							<td width="170"> <!-- Selected -->
								<input name="txt_bank_code" id="txt_bank_code" class="text_boxes" style="width:165px" />
							</td>
						</tr>
						<tr>
							<td width="130" >Address</td>
							<td colspan="3"> <!-- Display -->
								<input name="txt_bank_address" id="txt_bank_address" class="text_boxes" style="width:497px" />						  
							</td>
							<td width="130" >Email</td>
							<td width="170"> <!-- Display -->
								<input type="email" name="txt_bank_email" id="txt_bank_email" class="text_boxes" style="width:165px">							
							</td>
						</tr>
						<tr>
							<td width="130" >Web Site</td>
							<td width="170" > 
								<input name="txt_bank_website" id="txt_bank_website" class="text_boxes" style="width:165px">							
							</td>
							<td width="130" >Contact Person	</td>
							<td width="170"> <!-- Display -->
								<input name="txt_bank_contact_person" id="txt_bank_contact_person" class="text_boxes"style="width:165px" >						
							</td>
							<td width="130" >Phone No</td>
							<td width="170"> <!-- Display -->
								<input name="txt_bank_phone_no" id="txt_bank_phone_no" class="text_boxes"style="width:165px" onkeypress="return numbersonly(this,event)">							
							</td>
						</tr>
                        <tr>
							<td width="130" ><b>Signature-1</b> : Name</td>
							<td width="170" ><input name="signature_one_name" id="signature_one_name" class="text_boxes"style="width:165px" ></td> 
							<td width="130" >Designation</td>
							<td width="170"><input name="txt_designation_one" id="txt_designation_one" class="text_boxes"style="width:165px"></td>
							<td width="130" ></td>
							<td width="170"></td>
						</tr>
                        <tr>
							<td width="130" ><b>Signature-2</b> : Name</td>
							<td width="170" ><input name="signature_two_name" id="signature_two_name" class="text_boxes"style="width:165px" ></td> 
							<td width="130" >Designation</td>
							<td width="170"><input name="txt_designation_two" id="txt_designation_two" class="text_boxes"style="width:165px"></td>
							<td width="130" ></td>
							<td width="170"></td>
						</tr>
						<tr>
							<td width="130" height="30" valign="middle" align="center"> <!-- System Generated -->
								Bank Type
							</td>
							 <td width="170" colspan="5">
							 		<input type='checkbox' name="cb_lien_bank" id="cb_lien_bank">Lien Bank&nbsp;&nbsp;<input type='checkbox' name="cb_issuing_bank" id="cb_issuing_bank">Issuing Bank&nbsp;&nbsp;<input type='checkbox' name="cb_salary_bank" id="cb_salary_bank">Salary Bank&nbsp;&nbsp;<input type='checkbox' name="cb_advs_bank" id="cb_advs_bank">Advising Bank
							 </td>
							
							 <td width="170"></td>
						</tr>
						<tr>
							<td width="130" >Remarks</td>
							<td colspan="5"> <!-- Display -->
								<input name="txt_remarks" id="txt_remarks" class="text_boxes"style="width:810px" >							
							</td>
						</tr>
						<tr>
							<td colspan="4" align="center" height="10">
								<input type="hidden" name="save_up" id="save_up" >
								<input type="hidden" name="txt_mst_id" id="txt_mst_id">							
							</td>		 
						</tr>
						<tr>
							<td colspan="6" align="center" class="button_container">
								<input type="submit" value="<?php if($save_up==""){ echo "Save"; }else{ echo "Update"; } ?>" name="save" style="width:80px" id="save" class="formbutton"/>&nbsp;&nbsp;
								<input type="reset" value="  Refresh  " style="width:80px" name="reset" id="reset" class="formbutton"/>							
							</td>		 
						</tr>	
					</form>	
					<tr>
						<td colspan="6" height="15"></td>
					</tr>
					<tr>
						<td colspan="6" align="center">
							<fieldset style="width:90%">
							<legend>Add Account Info</legend>
							<form name="bank_acc_info" id="bank_acc_info" method="post" action="javascript:fnc_bank_acc_info(save_perm,edit_perm,delete_perm,approve_perm)">
								<table  cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
									<!--<tr bgcolor="#CCCCCC">
										<td width="223" align="center">Account Type</td>
										<td width="123" align="center">Account No</td>
										<td width="71" align="center">Currency</td>
										<td width="80" align="center">Loan Limit</td>
                                        <td width="80" align="center">Limit Type</td>
										<td width="186" align="center">Company Name</td>
										<td width="110" align="center">Status</td>
                                        <td width="50" align="center">Status</td>
										<td width="50" rowspan="2" valign="bottom"><input type="hidden" name="save_up_dtls" id="save_up_dtls" value="" />
                                            <input type="reset" class="formbutton" name="Submit3" value="Refresh" style="width:50px" onclick="document.getElementById('account_list_view').innerHTML = '';" />
                                            <input type="submit" value="  add  " name="save_member" id="save_member" class="formbutton"/>
                                        </td>
									</tr>-->
                                    <thead>
                                        <th width="223" align="center"><strong>Account Type</strong></th>
                                        <th width="123" align="center"><strong>Account No</strong></th>
                                        <th width="71" align="center"><strong>Currency</strong></th>
                                        <th width="80" align="center"><strong>Loan Limit</strong></th>
                                        <th width="80" align="center"><strong>Limit Type</strong></th>
                                        <th width="186" align="center"><strong>Company Name</strong></th>
                                        <th width="110" align="center"><strong>Status</strong></th>
                                        <th width="50" align="center"><input type="hidden" name="save_up_dtls" id="save_up_dtls" value="" />
                                        	<input type="reset" class="formbutton" name="Submit3" value="Refresh" style="width:52px" onclick="document.getElementById('account_list_view').innerHTML = '';" />
                                        </th>
                                    </thead>
									<tr class="general">
										<td>
											<select name="cbo_account_type" id="cbo_account_type" class="combo_boxes" style="width:220px ">
												<?php
														foreach($account_type as $key=>$value):
														?>
														<option value=<? echo "$key";
														if ($cbo_account_type==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
														<?		
														endforeach;
														?>
											</select>										
                                          </td>
										<td>
												<input type="text" name="txt_account_no" id="txt_account_no"  class="text_boxes" style="width:120px" onkeypress="return numbersonly(this,event)"  />										</td>
										<td>
											<select name="cbo_currency" id="cbo_currency" class="combo_boxes" style="width:70px ">
												<?php
												
												$sql = "SELECT * FROM lib_list_currency WHERE is_deleted = 0 AND status_active = 1 ORDER BY currency_name ASC";
												$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
												while( $row = mysql_fetch_assoc( $result ) ) {
												?>
												<option value="<?php echo $row['id']; ?>" <?php if( $row['currency_name'] == 'TAKA' || $row['currency_name'] == 'taka' ) echo "selected"; ?>><?php echo $row['currency_name']; ?></option>
												<?php } ?>
											</select>										
                                        </td>
										<td>
												<input type="text" name="txt_loan_limit" id="txt_loan_limit" onKeyPress=" return numbersonly(this,event)" onchange="" class="text_boxes" style="width:80px; text-align:right"  />										</td>
                                        <td>
											<select name="cbo_loan_type" id="cbo_loan_type" class="combo_boxes" style="width:80px ">
												<?php
												
												foreach($loan_type as $key=>$value):
												?>
												<option value=<? echo "$key";
												if ($cbo_po_status2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
												<?		
												endforeach;
												?>
											</select>										
										</td>
										<td>
											<select name="cbo_company_name" id="cbo_company_name" class="combo_boxes"  style="width:185px ">
												<? if($company_cond=="")
											   { 
												?> 
                                                <option value="0">--- Select Company Name ---</option>
												<?
												}
												$company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 $company_cond order by company_name");
												while ($r_company=mysql_fetch_array($company_sql))
												{
												?>
												<option value="<? echo $r_company["id"]; ?>" <? if($cbo_company_name1==$r_company["id"]){?>selected<?php }?>><? echo "$r_company[company_name]" ?> </option>
												<? } ?>
											</select>										
                                        </td>
										<td>
											<select name="cbo_po_status" id="cbo_po_status" class="combo_boxes" style="width:100px ">
												<?php
												foreach($status_active as $key=>$value):
												$cbo_po_status1=1;
												?>
												<option value=<? echo "$key";
												if ($cbo_po_status1==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
												<?		
												endforeach;
												?>
											</select>
										</td>
                                        <td width="50" align="center"><input type="submit" value="add" name="save_member" id="save_member" class="formbutton"/></td>
									</tr>
									<tr>
										<td colspan="8" align="center">
											<div id="account_list_view" style="width:820px" align="center"></div>										
										</td>
									</tr>
								</table>
							</form>	
							</fieldset>			
						</td>
					</tr>
					<tr>
						<td colspan="6" align="center" height="15"></td>
						</tr>
					<tr>
					<tr>
						<td colspan="6" height="35" valign="middle" align="center">
							<div style="width:100%"	>
								<fieldset style="width:80%" >
									<legend>Show Bank Info</legend>
										<input type="text" name="txt_account_no" onKeyUp="showResult(this.value,'4','bank_list_view')" id="txt_account_no" class="text_boxes" style="width:170px" placeholder="Please Press The Space Bar">
											<div style="margin-top:10px; width:100%">
												<div id="bank_list_view" style="width:100%"></div>
											</div>
								</fieldset>
							</div>				
						</td>
					</tr>
					<tr>
						<td colspan="6"></td>
					</tr>
			</table>
		</fieldset>
	</div>
</div>		
</body>
</html>