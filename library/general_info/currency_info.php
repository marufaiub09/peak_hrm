<?php
// Fuad
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Sample Info</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<script src="includes/ajax_submit.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	</script>    
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px;">
         <div>
            <div class="form_caption">
                Currency Information
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update.$delete;
             ?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
  	</div>
	<fieldset style="width:400px;">
		<legend>Currency Info</legend>
		<form name="currency_info" id="currency_info" method="" action="javascript:fnc_currency_info(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">	
			<table cellpadding="0" cellspacing="2" width="100%">
				<tr>
					<td width="130">
						Currency					</td>
					<td colspan="3">
						<input type="text" name="txt_currency_name" id="txt_currency_name" class="text_boxes" style="width:232px" />
					</td>
				</tr>
                <tr>
					<td width="130">
						Is Domestic					</td>
					<td colspan="3">
						<select name="cbo_is_domestic" id="cbo_is_domestic" class="combo_boxes" style="width:242px">
                        	<option value="0">Foreign Currency</option>
                            <option value="1">Domestic Currency</option>
                        </select>
					</td>
				</tr>
				<tr>
					<td>
						Delete
					</td>
					<td>
						<select name="cbo_is_deleted" id="cbo_is_deleted" class="combo_boxes" style="width:86px" >
							<?php
							foreach($is_deleted as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($cbo_po_status2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>						
						</select>	
					</td>
					<td>
						Status
					</td>
					<td>
						<select name="cbo_status" id="cbo_status"  class="combo_boxes" style="width:86px" ><!--onFocus="javascript:hide_div('div_user_id');"-->
						 <?php
							foreach($status_active as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					 
					</td>
				</tr>
				<tr>
				 	<td colspan="4" height="15" align="center" valign="top">
					  
					</td>
				</tr>
				<tr>
					<td colspan="4" align="center">&nbsp;						
						<input type="hidden" name="save_up" id="save_up" >
						<input type="hidden" name="txt_mst_id" id="txt_mst_id">	
					</td>					
				</tr>
				<tr>
				  <td colspan="4" align="center" class="button_container">
						<input type="submit" value="Save" name="save" style="width:80px" id="save" class="formbutton"/>&nbsp;&nbsp;
						<input type="reset" value="  Refresh  " style="width:80px" name="reset" id="reset" class="formbutton"/>	
					</td>				
				</tr>
			</table>
		
		</form>
	</fieldset>
	<div style="width:900px; float:left; margin:auto" align="center">
		<fieldset style="width:400px; margin-top:10px">
			<legend>Press space for Currency Info</legend>
			<form>
				<input type="text" size="30" style="width:200" placeholder="Press Space For Search" class="text_boxes" onKeyUp="showResult(this.value,'2','currency_list_view')" />
				<div style="width:400px; margin-top:10px" id="currency_list_view" align="left"></div>
			</form>
		</fieldset>	
	</div>
</div>
</body>
</html>
	
			