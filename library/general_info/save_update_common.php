 <?php

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

date_default_timezone_set('Asia/Dhaka');
$date=time();
$user_name=$_SESSION['logic_erp']["user_name"];
$date_time= date("Y-m-d H:i:s");

include('../../includes/common.php');	
extract($_GET);


//--------------------------Buyer Info Start-----------------------------------------------------------------------

if ($action=="style_details"){ //Zone List Registrartion and Update Here
   if  ($isupdate==""){//Insert Here
		
		$id_field_name = "id";
		$table_name = "lib_style";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");

		$sql = "INSERT INTO lib_buyer (
							id,
							style_name,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							$id,
							'".mysql_real_escape_string($txt_style_name)."',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$STATUS',
							'$is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2***0";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");		
		
		$sql = "UPDATE lib_buyer
				SET
					style_name		= '".mysql_real_escape_string($txt_style_name)."',
					updated_by 			= '$user_name',
					update_date 		= '$date',
					status_active		= '$STATUS',
					is_deleted			= '$is_deleted',
					WHERE id = 			$isupdate";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3***0";	
		exit();
	}
	
}
//--------------------------Buyer Info End-----------------------------------------------------------------------


//------------------------ekram

if ($action=="unit_info"){ //Unit Registrartion and Update Here

   if  ($isupdate==""){//Insert Here
   
   		$unique_field_name = "unit_name";
		$table_name = "lib_unit";
		$query = "unit_name='$txt_unit_name'";
   
   		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "lib_unit";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 

		 $sql = "INSERT INTO lib_unit (
							id,
							unit_name,
							description,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							$id,
							'".mysql_real_escape_string($txt_unit_name)."',
							'".mysql_real_escape_string($txt_unit_description)."',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'$cbo_is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2***0";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 		
		
		//check for update
		$unique_check = check_uniqueness( "id", "lib_trim", "order_uom=$isupdate and status_active=1" );
		//false means this id used in lib_trim table. so update restricted
		if( ( $cbo_is_deleted==1 || $cbo_status==0) && $unique_check==false)
			{
				echo "5";
				exit();
			}
			
		$sql = "UPDATE lib_unit
				SET
					unit_name			= '".mysql_real_escape_string($txt_unit_name)."',
					description			= '".mysql_real_escape_string($txt_unit_description)."',
					inserted_by 		= '$inserted_by',
					insert_date			= '$insert_date',
					updated_by			= '".mysql_real_escape_string($user_name)."',
					update_date 		= '$date',
					status_active		= '$cbo_status',
					is_deleted			= '$cbo_is_deleted'
					WHERE id = '$isupdate'
				 ";
				 
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3***0";		
		exit();
	}
}

//End--------------------------------------ekram




//--------------------------Currency Info Start-----------------------------------------------------------------------

if ($action=="currency_info"){ 
   if  ($isupdate==""){//Insert Here
   		
		$unique_field_name = "currency_name";
		$table_name = "lib_list_currency";
		$query = "currency_name='$txt_currency_name'";
		// echo "$txt_currency_name";die;
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}		
		if ($cbo_is_domestic==1)
		{
			$company_sql= mysql_db_query($DB, "select id from lib_list_currency where is_domestic=1 order by id");
			if ($r_company=mysql_fetch_array($company_sql))
			{
			
				echo "4";
				exit();
			}
		}		
		$id_field_name = "id";
		$table_name = "lib_list_currency";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");

		$sql = "INSERT INTO lib_list_currency(
							id,
							currency_name,
							is_domestic,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							$id,
							"."'".mysql_real_escape_string($txt_currency_name)."'".",
							'$cbo_is_domestic',
							'$user_name',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'$cbo_is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_".$sql;
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		if ($cbo_is_domestic==1)
		{
			$company_sql= mysql_db_query($DB, "select id from lib_list_currency where is_domestic=1 order by id");
			if ($r_company=mysql_fetch_array($company_sql))
			{
			
				echo "4";
				exit();
			}
		}
		
		$sql = "UPDATE lib_list_currency
				SET
					currency_name		= '".mysql_real_escape_string($txt_currency_name)."',
					is_domestic			='$cbo_is_domestic',
					updated_by 			= '$user_name',
					update_date 		= '$date',
					status_active		= '$cbo_status',
					is_deleted			= '$cbo_is_deleted'
					WHERE id			= '$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_".$sql;	
		exit();
	}
	
}

//--------------------------Currency Info End----------------------------------------------------------------------

//Bank_Info Registrartion and Update Here
if ($action=="bank_info")
{ 

   if  ($isupdate==""){//Insert Here
		
		$unique_field_name = "bank_name";
		$table_name = "lib_bank";
		$query = "bank_name='$txt_bank_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "lib_bank";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	

		$sql = "INSERT INTO lib_bank (
							id,
							bank_name,
							branch_name,
							bank_code,
							address,
							email,
							web_site,
							contact_person,
							contact_no,
							signature_one_name,
							txt_designation_one,
							signature_two_name,
							txt_designation_two,
							lien_bank,
							issusing_bank,
							salary_bank,
							advising_bank,
							remark,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'".mysql_real_escape_string($txt_bank_name)."',
							'".mysql_real_escape_string($txt_branch_name)."',
							'".mysql_real_escape_string($txt_bank_code)."',
							'".mysql_real_escape_string($txt_bank_address)."',
							'".mysql_real_escape_string($txt_bank_email)."',
							'".mysql_real_escape_string($txt_bank_website)."',
							'".mysql_real_escape_string($txt_bank_contact_person)."',
							'$txt_bank_phone_no',
							'$signature_one_name',
							'$txt_designation_one',
							'$signature_two_name',
							'$txt_designation_two',
							'".mysql_real_escape_string($cb_lien_bank)."',
							'".mysql_real_escape_string($cb_issuing_bank)."',
							'".mysql_real_escape_string($cb_salary_bank)."',
							'".mysql_real_escape_string($cb_advs_bank)."',
							'".mysql_real_escape_string($txt_remarks)."',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'1',
							'$is_deleted'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		//echo "2_"."$id"."_".$sql;
		echo "2_"."$id";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		 
		$sql ="update lib_bank 
							set 
								bank_name		='".mysql_real_escape_string($txt_bank_name)."', 
								branch_name		='".mysql_real_escape_string($txt_branch_name)."', 
								bank_code		='".mysql_real_escape_string($txt_bank_code)."', 
								address			='".mysql_real_escape_string($txt_bank_address)."', 
								email			='".mysql_real_escape_string($txt_bank_email)."', 
								web_site		='".mysql_real_escape_string($txt_bank_website)."', 
								contact_person	='".mysql_real_escape_string($txt_bank_contact_person)."', 
								contact_no		='".mysql_real_escape_string($txt_bank_phone_no)."',
								signature_one_name		='".mysql_real_escape_string($signature_one_name)."',
								txt_designation_one		='".mysql_real_escape_string($txt_designation_one)."',
								signature_two_name		='".mysql_real_escape_string($signature_two_name)."',
								txt_designation_two		='".mysql_real_escape_string($txt_designation_two)."',
								lien_bank		='".mysql_real_escape_string($cb_lien_bank)."', 
								issusing_bank	='".mysql_real_escape_string($cb_issuing_bank)."', 
								salary_bank		='".mysql_real_escape_string($cb_salary_bank)."', 
								advising_bank	='".mysql_real_escape_string($cb_advs_bank)."',
								updated_by 		='".mysql_real_escape_string($user_name)."',
								update_date 	='$date',
								status_active	='1',
								is_deleted		='$cbo_is_deleted',
								remark			='".mysql_real_escape_string($txt_remarks)."' 
								where id		='$isupdate'";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		//echo "3_"."$id"."_".$sql;
		echo "3_"."$id";				
		exit();
	}
	
}

if ($action=="account_info_dtls")
{ 
	if  ($txt_mst_id!="")//Insert Here
	{
	   if  ($save_up_dtls==""){//Insert Here
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
	
			$id_field_name = "id";
			$table_name = "lib_bank_account";
			$id= return_next_id($id_field_name,$table_name);
			
			$sql = "INSERT INTO lib_bank_account (
							id,
							account_type,
							account_no,
							currency,
							loan_limit,
							loan_type,
							company_name,
							account_id,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id_m',
							'".mysql_real_escape_string($cbo_account_type)."',
							'".mysql_real_escape_string($txt_account_no)."',
							'".mysql_real_escape_string($cbo_currency)."',
							'$txt_loan_limit',
							'".mysql_real_escape_string($cbo_loan_type)."',
							'".mysql_real_escape_string($cbo_company_name)."',
							'".mysql_real_escape_string($txt_mst_id)."',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_po_status',
							'$is_deleted'
						)";
			mysql_query( $sql ) or die (mysql_error());	
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			//echo "2_"."$txt_mst_id"."_".$sql;
			echo "2_"."$txt_mst_id";			
			exit();
		} 
		else  // Update
		{
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			
			$sql ="update lib_bank_account 
					set 
						account_type  ='".mysql_real_escape_string($cbo_account_type)."',
						loan_type     ='".mysql_real_escape_string($cbo_loan_type)."', 
						account_no    ='".mysql_real_escape_string($txt_account_no)."', 
						currency      ='".mysql_real_escape_string($cbo_currency)."', 
						loan_limit    ='".mysql_real_escape_string($txt_loan_limit)."', 
						company_name  ='".mysql_real_escape_string($cbo_company_name)."', 
						updated_by		='".mysql_real_escape_string($user_name)."',
						update_date 	='$date',
						status_active ='$cbo_po_status' 
						where id      ='$save_up_dtls'";
			mysql_query( $sql ) or die (mysql_error());
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			//echo "3_"."$txt_mst_id"."_".$sql;
			echo "3_"."$txt_mst_id";
			exit();
		}
	}// txt_mst_id close
}



if ($action=="store_location")
{ 
   if  ($isupdate==""){//Insert Here
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	

		$id_field_name = "id";
		$table_name = "lib_store_location";
		$id= return_next_id($id_field_name,$table_name);
		
		$sql = "INSERT INTO lib_store_location (
						id,
						store_name,
						company_name,
						store_location,
						inserted_by,
						insert_date,
						updated_by,
						update_date,
						status_active,
						is_deleted
					) VALUES (
						'$id',
						'".mysql_real_escape_string($txt_store_name)."',
						'".mysql_real_escape_string($cbo_company_name)."',
						'".mysql_real_escape_string($txt_store_location)."',
						'".mysql_real_escape_string($user_name)."',
						'$date',
						'$updated_by',
						'$update_date',
						'$cbo_status',
						0
					)";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2";		
		exit();
	} 
	else  // Update
	{
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
		
		//check for update
		$unique_check = check_uniqueness( "store_id", "inv_product_info_master", "store_id=$isupdate and status_active=1" );
		//false means this id used in lib_trim table. so update restricted
		if( $cbo_status==0 && $unique_check==false)
			{
				echo "5";
				exit();
			}
			
		$sql ="update lib_store_location 
		set 
			store_name		='".mysql_real_escape_string($txt_store_name)."', 
			company_name	='".mysql_real_escape_string($cbo_company_name)."', 
			store_location	='".mysql_real_escape_string($txt_store_location)."',
			updated_by		= '".mysql_real_escape_string($user_name)."',
			update_date 	= '$date',
			status_active	='".mysql_real_escape_string($cbo_status)."' 
			where id		='$isupdate'";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3";
		exit();
	}
}

// ----------START buyer info sewing process-----------------
if ($action=="buyer_info_sp"){
	//echo $isupdate."su..re";die; 
   if  ($isupdate==""){//Insert Here
   
		/*$unique_field_name = "buyer_name";
		$table_name = "lib_buyer";
		$query = "buyer_name='$txt_buyer_name'";
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		*/
		$unique_check = check_uniqueness( "buyer_name", "lib_buyer", "buyer_name='$txt_buyer_name'" );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		/*
		$id_field_name = "id";
		$table_name = "lib_buyer";
		$id= return_next_id($id_field_name,$table_name);
		*/
		$id= return_next_id("id","lib_buyer");
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
		$_SESSION['job_no']=$id;

		$sql = "INSERT INTO lib_buyer (
							id,
							buyer_name,
							contact_person,
							buyer_email,
							contact_no,
							web_site,
							address,
							subcontract_party,
							remark,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'".mysql_real_escape_string($txt_buyer_name)."',
							'".mysql_real_escape_string($txt_contact_person)."',
							'".mysql_real_escape_string($txt_buyer_email)."',
							'$txt_contact_no',
							'".mysql_real_escape_string($txt_web_site)."',
							'".mysql_real_escape_string($txt_address)."',
							'".mysql_real_escape_string($cbo_subcontract_party)."',
							'".mysql_real_escape_string($txt_remark)."',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'0'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_"."$id"."_0";		
		exit();
		
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			$_SESSION['job_no']=$isupdate;
			
		$unique_check = check_uniqueness( "buyer_name", "lib_buyer", "buyer_name='$txt_buyer_name'" );
			
		$sql = "UPDATE lib_buyer
				SET
					buyer_name			= '".mysql_real_escape_string($txt_buyer_name)."',
					contact_person		= '".mysql_real_escape_string($txt_contact_person)."',
					buyer_email			= '".mysql_real_escape_string($txt_buyer_email)."',
					contact_no			= '$txt_contact_no',
					web_site			= '".mysql_real_escape_string($txt_web_site)."',
					address				= '".mysql_real_escape_string($txt_address)."',
					subcontract_party 	= '".mysql_real_escape_string($cbo_subcontract_party)."',
					remark				= '".mysql_real_escape_string($txt_remark)."',
					inserted_by 		= '$inserted_by',
					insert_date			= '$insert_date',
					updated_by			= '".mysql_real_escape_string($user_name)."',
					update_date 		= '$date',
					status_active		= '$cbo_status',
					is_deleted			= '0'
					WHERE id = '$isupdate'
				 ";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_"."$id"."_0";
		exit();
	}
}
// ----------END buyer info sewing process-----------------
// ----------START sewing process-----------------
if ($action=="sewing_process"){
	//echo $counter;die;
	//echo "su..re".$update_id;die;
	if($piece_id=="")
	{
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");	
		
		$sql_machine_type="SELECT id,machine_type FROM lib_machine_type WHERE machine_type='$machine_type' AND is_deleted = 0 AND status_active = 1";
		$sql_exe = mysql_query( $sql_machine_type ); 
		$sql_rslt = mysql_fetch_array( $sql_exe );
	
		if($sql_rslt[1])
		{ 
			$id_machine_type=$sql_rslt[0];
		}
		else 
		{
			$id_machine_type= return_next_id("id","lib_machine_type");
	
			$sql="INSERT INTO lib_machine_type (id,machine_type,inserted_by,insert_date,updated_by) VALUES('$id_machine_type','$machine_type','".mysql_real_escape_string($user_name)."','$date_time','$updated_by')";
			mysql_query( $sql ) or die (mysql_error());	
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;	
			//echo 2;
		}

			$process_id= return_next_id("id","hrm_process_mst");
			
			$sql="INSERT INTO hrm_process_mst (id,company_id,process_name,machine_type,rate_for,inserted_by,insert_date,updated_by) 
					VALUES('$process_id','$company_id','$process_name','$id_machine_type','$sewing_rate','".mysql_real_escape_string($user_name)."','$date_time','$updated_by')";
					//rate_for 	inserted_by 	insert_date 	updated_by 	update_date
			mysql_query( $sql ) or die (mysql_error());	
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			//echo 2;
		
			for( $i = 1; $i < $counter; $i++ ) {
			$buyer_id	= "buyer_id_$i";		$buyer_id			= $$buyer_id;
			$style_ref 	= "style_ref_$i";		$style_ref			= $$style_ref;
			$rate		= "rate_$i";			$rate				= $$rate;
			$uom		= "uom_$i";				$uom				= $$uom;
		
			$id_process_rate= return_next_id("id","hrm_process_rate_dtls");
			
			$sql="INSERT INTO hrm_process_rate_dtls (id,process_id,buyer_id,style_ref,rate,uom,inserted_by,insert_date,updated_by) 
					VALUES('$id_process_rate','$process_id','$buyer_id','$style_ref','$rate','$uom','".mysql_real_escape_string($user_name)."','$date_time','$updated_by')";
			//echo $sql;die;
			mysql_query( $sql ) or die (mysql_error());		
			//$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
		}
		$sql_history .= $sql;
		$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo 2;
		exit();
	}
	else
	{
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'");	
		
		// Machine Type
		$sql_machine_type="SELECT id,machine_type FROM lib_machine_type WHERE machine_type='$machine_type' AND is_deleted = 0 AND status_active = 1";
		$sql_exe = mysql_query( $sql_machine_type );
		$sql_rslt = mysql_fetch_array( $sql_exe );
	
		if($sql_rslt[1])
		{ 
			$id_machine_type=$sql_rslt[0];
		}
		else 
		{
			$id_machine_type= $update_id_machine_type;
	
			$sql="UPDATE lib_machine_type SET 
										machine_type='$machine_type',
										updated_by='".mysql_real_escape_string($user_name)."',
										update_date='$date_time' 
										WHERE id=$update_id_machine_type";
			mysql_query( $sql ) or die (mysql_error());	
			$sql_history=encrypt($sql, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			//echo 3; 
			//exit();
		}
		
		// Process mst
		$sql="UPDATE hrm_process_mst SET 
										company_id='$company_id',
										process_name='$process_name',
										machine_type='$id_machine_type',
										rate_for='$sewing_rate',
										updated_by='".mysql_real_escape_string($user_name)."',
										update_date='$date_time'
										WHERE id='$piece_id'";
												
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		
		// Process dtls update
		$exp_db_row_ids_mst=explode(",",$db_row_ids_mst);
		$exp_update_id=explode("_",$update_id);
		
		$j=0;
		for( $i = 1; $i < $counter; $i++ ) 
		{
			$buyer_id	= "buyer_id_$i";		$buyer_id			= $$buyer_id;
			$style_ref 	= "style_ref_$i";		$style_ref			= $$style_ref;
			$rate		= "rate_$i";			$rate				= $$rate;
			$uom		= "uom_$i";				$uom				= $$uom;
			
			if($exp_update_id[$j]!="")
			{

				$sql="UPDATE hrm_process_rate_dtls SET
													buyer_id='$buyer_id',
													style_ref='$style_ref',
													rate='$rate',
													uom='$uom',
													updated_by='".mysql_real_escape_string($user_name)."',
													update_date='$date_time' 
													WHERE id='$exp_db_row_ids_mst[$j]'";
				//echo $sql;die;
				mysql_query( $sql ) or die (mysql_error());		
				//$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			}
			else
			{
				$id_process_rate= return_next_id("id","hrm_process_rate_dtls");
				
				$sql="INSERT INTO hrm_process_rate_dtls (id,process_id,buyer_id,style_ref,rate,uom,inserted_by,insert_date,updated_by) 
						VALUES('$id_process_rate','$piece_id','$buyer_id','$style_ref','$rate','$uom','".mysql_real_escape_string($user_name)."','$date_time','$updated_by')";
				//echo $sql;die;
				mysql_query( $sql ) or die (mysql_error());		
				//$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			}
			$j++;
		}
		echo 3; 
		exit();
	}

}
// ----------END of sewing process-----------------

function return_field_value($fdata,$tdata,$cdata){
$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;
}
?>

