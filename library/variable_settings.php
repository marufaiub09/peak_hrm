﻿<?php 
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
include('../includes/array_function.php');
include('../includes/common_functions.php');

$ConsumptionTabIndex=-1;
extract($_POST);
if(!$TabIndexNo) { $TabIndexNo=0; }


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Cost Sheets</title>
		<link href="../css/style_common.css" rel="stylesheet" type="text/css" />
		<link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/popup_window.js"></script>
		<script type="text/javascript" src="../js/modal.js"></script>
		
		<script type="text/javascript" src="includes/functions_variable_settings.js"></script>
		<script type="text/javascript" src="includes/ajax_submit_variable_settings.js"></script>
        
		<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
		<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
	
		<script type="text/javascript" src="../js/ajaxupload.3.5.js" ></script>
		
        <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		</script>

        
		<script type="text/javascript">
		
	function reset_var_form()
	{
		$('#cbo_company_name_hrm').removeAttr("disabled","disabled");
	}
		
	function onlyNumbers(evt)
	{
		var e = event || evt; // for trans-browser compatibility
		var charCode = e.which || e.keyCode;
	
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
	
	function show_format_salary(type)
	{
		//alert(type);
		if( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();	// code for IE7+, Firefox, Chrome, Opera, Safari
		else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");		// code for IE6, IE5
		
			xmlhttp.onreadystatechange = function() {
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				var response = xmlhttp.responseText;
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(response);
				d.close();
			}
		}
		xmlhttp.open( "GET", "view_pay_format.php?type=" + type, true );
		xmlhttp.send();
	}
				
				
function show_signeture_list()
{
	company_id = $('#cbo_company_name').val();
	module_id =  $('#cbo_module_name').val(); 
	$("#messagebox").removeClass().addClass('messagebox').text('').fadeIn(1000);
	if(company_id==0 || company_id=="")
	{ 
		$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select a Company').addClass('messageboxerror').fadeTo(900,1);
			 $(this).fadeOut(5000);
		});	 
	}
	else  if(module_id==0 || module_id=="")
	{ 
		$("#messagebox").fadeTo(200,0.1,function(){ //start fading the messagebox
			$('#cbo_module_name').focus();
			$(this).html('Please Select a Module').addClass('messageboxerror').fadeTo(900,1);
			 $(this).fadeOut(5000);
		});	 
	}	
	else 
	{ 
		var data  = "module_id="+module_id+"&company_id="+company_id+"&type=get_report_signeture"; 
		$.ajax({
			type: "GET",
			url: "save_update_common_variable_settings.php",
			data: data,
			success: function( data ) {
				eval(data);			
			}
		})
	}
}			
				

			
		</script>
		<!--<script type="text/javascript" src="ui.tabs.paging.js"></script>-->
		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').tabs();
				$('#example').tabs('paging', { cycle: true, follow: true } );
				$('#example').tabs('select',<?php echo $TabIndexNo; ?>);
			});
		</script>
		<style>
			input.file {
			 background-image: url(image.png);
			 width:10px;
			 height:10px;
			}
		</style>
	</head>
	<body>
    	<div align="center">
            <div class="form_caption">
                Variable Settings
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update;
                ?>
           </span>
        	<div id="messagebox" style="background-color:#FF9999; color:#000000; width:700px" align="center"></div>
    	</div>
        
        <div id="example" style="width:1100px; font-size:11px;">
            <ul class="tabs">
                <li><a href="#hrm">HRM</a></li>
                <li><a href="#report_sg">Report Signature</a></li>
                <li style="visibility:hidden"><a href="#work_order">Order Tracking</a></li>
                <li style="visibility:hidden"><a href="#production">Production</a></li>
                <li style="visibility:hidden"><a href="#commercial">Commercial</a></li>
                <li style="visibility:hidden"><a href="#inventory">Inventory</a></li>
                <li style="visibility:hidden"><a href="#accounts">Accounts</a></li>
            </ul>
            <div id="hrm"> <?php include('variable/var_sett_hrm.php'); ?></div>
           <div id="work_order"> <?php include('variable/var_sett_work_order.php'); ?></div>
            <div id="production"> <?php include('variable/var_sett_production.php'); ?></div>
            <div id="commercial"> <?php include('variable/var_sett_commercial.php'); ?></div>
            <div id="inventory"> <?php include('variable/var_sett_inventory.php'); ?></div>
            <div id="accounts"> <?php include('variable/var_sett_accounts.php'); ?></div>
            <div id="report_sg"> <?php include('variable/report_sg.php'); ?></div>
        </div>
	</body>
</html>