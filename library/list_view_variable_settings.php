<script type="text/javascript" src="includes/functions_variable_settings.js"></script>
<script type="text/javascript" src="../juery/lib.js"></script>
<script type="text/javascript" src="../resources/jquery-1.6.2.js"></script>
<link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
<script type="text/javascript" src="../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	
	

<?php
date_default_timezone_set('UTC');
include('../includes/common.php');
include('../includes/array_function.php');

//Company array
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 and status_active=1 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row['company_name'];
	}

$search_string=$_GET["q"];
$type=$_GET["type"];
//echo $search_string;
if ($type==1){   //Production_Production Update Areas
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_production where company_name='$search_string' and variable_list=1 order by id");
      	$r_company1=mysql_fetch_array($sql);
    ?>
	<fieldset>
		<legend>Production Update Areas</legend>
		<div style="width:700px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
			<table cellspacing="0" width="100%" >
				<tr>
					<td width="130" align="left" id="cutting_update">Cutting Update</td>
					<td width="190">
						<select name="cbo_cutting_update" id="cbo_cutting_update" class="combo_boxes" style="width:170px ">
							<?php
							foreach($production_update_areas as $key=>$value):
							?>
							<option value=<? echo "$key";
							 if ($r_company1[cutting_update]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
					<td width="160" align="left" id="printing_emb_production">Printing & Embrd. Prodiction</td>
					<td width="180">
						<select name="cbo_printing_emb_production" id="cbo_printing_emb_production" class="combo_boxes" style="width:170px ">
							<?php
							foreach($production_update_areas as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($r_company1[printing_emb_production]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
				</tr>
				<tr> 
					<td width="130" align="left" id="sewing_production">Sewing Production</td>
					<td width="190">
						<select name="cbo_sewing_production" id="cbo_sewing_production" class="combo_boxes" style="width:170px ">
							<?php
							foreach($production_update_areas as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($r_company1[sewing_production]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
					<td width="160" align="left" id="iron_update">Finishing Input</td>
					<td width="190">
						<select name="cbo_iron_update" id="cbo_iron_update" class="combo_boxes" style="width:170px ">
							<?php
							foreach($production_update_areas as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($r_company1[iron_update]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
				</tr>
				<tr> 
					<td width="130" align="left" id="finishing_update">Finishing Output</td>
					<td width="190">
						<select name="cbo_finishing_update" id="cbo_finishing_update" class="combo_boxes" style="width:170px ">
							<?php
							foreach($production_update_areas as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($r_company1[finishing_update]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
                    <td width="160" align="left" id="ex_factory">Ex-Factory</td>
					<td width="190">
						<select name="cbo_ex_factory" id="cbo_ex_factory" class="combo_boxes" style="width:170px ">
							<?php
							foreach($production_update_areas as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($r_company1[ex_factory]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
				</tr>
                <tr>
                	<td width="130" align="left" id="iron_input">Iron Input</td>
					<td width="190">
						<select name="cbo_iron_input" id="cbo_iron_input" class="combo_boxes" style="width:170px ">
							<?php
							foreach($production_update_areas as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($r_company1[iron_input]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
                </tr>
			</table>
		</div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_proo" id="save_up_proo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
	</fieldset>
</div>

<?
}
if ($type==2){ //Production_Excess Cutting Slab
?>
<div>
	<fieldset>
		<legend>Excess Cutting Slab</legend>
         <div style="width:500px;" align="left">
            <table cellspacing="0" width="100%" >
                <tr>
                   <td width="150" align="center" id="order_quantity" colspan="2">Slab Range Start</td>
					<td width="150" align="center" id="order_quantity" colspan="2">Slab Range End</td>
					<td width="150" align="center" id="excess_percent">Excess %</td>
                </tr>
            </table>
        </div>	
		<div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
			<table cellspacing="0" width="100%" id="yarn_container1" >
				 <?php 
                    $search_string=$_GET["q"];
                    //echo $search_string;
                    //echo "select * from variable_settings_inventory_ile_cost_standard where company_name=$search_string and variable_list=8 order by id";
                    $sql= mysql_db_query($DB, "select * from variable_settings_production_excess_cutting_slab where company_name='$search_string' and variable_list=2 order by id");
                    //$r_company1=mysql_fetch_array($sql);
                    $i=1;
                    $row_count=mysql_num_rows($sql);
                    //echo "Num $row_count";
                    if ($row_count>0 )
                    {
                        while( $rows = mysql_fetch_array( $sql ) ) 
                        {
                ?>
				
                 <tr style="text-decoration:none" align="" bgcolor="<?php echo "$bgcolor"; ?>">
					<td width="150" align="center"><input type="text" name="txt_slab_rang_start<? echo $i; ?>" id="txt_slab_rang_start<? echo $i; ?>" onchange="next_number(<? echo $i; ?>)" value="<? echo $rows["slab_rang_start"]; ?>" class="text_boxes" onKeyPress=" return numbersonly(this,event)" style="width:150px; text-align:right"/></td>
					<td width="150" align="center"><input type="text" name="txt_slab_rang_end<? echo $i; ?>" id="txt_slab_rang_end<? echo $i; ?>" value="<? echo $rows["slab_rang_end"]; ?>" class="text_boxes" onKeyPress=" return numbersonly(this,event)" style="width:150px; text-align:right"/></td>
					<td width="150" align="center"><input type="text" name="txt_excess_percent<? echo $i; ?>" id="txt_excess_percent<? echo $i; ?>" value="<? echo $rows["excess_percent"]; ?>" class="text_boxes_numeric" onfocus="add_variable_row1(<? echo $i; ?>)" onKeyPress=" return numbersonly(this,event)"   style="width:150px; text-align:right"/></td>
				</tr>
                 <?php
					$i=$i+1;
						}
					}
					else
					{
						$i=1;
				?>
                <tr style="text-decoration:none" align="" bgcolor="<?php echo "$bgcolor"; ?>">
					<td width="150" align="center"><input type="text" name="txt_slab_rang_start<? echo $i; ?>" id="txt_slab_rang_start<? echo $i; ?>" onchange="next_number(<? echo $i; ?>)" value="<? echo $rows["slab_rang_start"]; ?>" class="text_boxes" onKeyPress=" return numbersonly(this,event)"  style="width:150px; text-align:right"/></td>
					<td width="150" align="center"><input type="text" name="txt_slab_rang_end<? echo $i; ?>" id="txt_slab_rang_end<? echo $i; ?>" value="<? echo $rows["slab_rang_end"]; ?>" class="text_boxes" onKeyPress=" return numbersonly(this,event)"  style="width:150px; text-align:right"/></td>
					<td width="150" align="center"><input type="text" name="txt_excess_percent<? echo $i; ?>" id="txt_excess_percent<? echo $i; ?>" value="<? echo $rows["excess_percent"]; ?>" class="text_boxes_numeric" onKeyPress=" return numbersonly(this,event)" onfocus="add_variable_row1(<? echo $i; ?>)"   style="width:150px; text-align:right"/></td>
				</tr>
                 <?
					}
				
				?>
			</table>
		</div>	
         <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_proo" id="save_up_proo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
	</fieldset>
</div>

<?
}
if ($type==3){//Production_Fabric in Roll Level
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_production where company_name='$search_string' and variable_list=3 order by id");
      	$r_company1=mysql_fetch_array($sql);
    ?>
	<fieldset>
		<legend>Fabric in Roll Level</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
					<td width="130" align="left" id="fabric_roll_level">Fabric in Roll Level</td>
					<td width="190">
						<select name="cbo_fabric_roll_level" id="cbo_fabric_roll_level" class="combo_boxes" style="width:170px ">
							<?php
							foreach($replacement_lc as $key=>$value):
							?>
							<option value=<? echo "$key";
							 if ($r_company1[fabric_roll_level]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
				</tr>
			</table>
		</div>
         <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_proo" id="save_up_proo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
	</fieldset>
</div>


<?
}
if ($type==4){//Production_Fabric in Machine Level
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_production where company_name='$search_string' and variable_list=4 order by id");
      	$r_company1=mysql_fetch_array($sql);
    ?>
	<fieldset>
		<legend>Fabric in Machine Level</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
					<td width="130" align="left" id="fabric_machine_level">Fabric in Machine Level</td>
					<td width="190">
						<select name="cbo_fabric_machine_level" id="cbo_fabric_machine_level" class="combo_boxes" style="width:170px ">
							<?php
							foreach($replacement_lc as $key=>$value):
							?>
							<option value=<? echo "$key";
							 if ($r_company1[fabric_machine_level]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
				</tr>
			</table>
		</div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_proo" id="save_up_proo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
	</fieldset>
</div>

<?
}
if ($type==13){//Production_Batch Maintained
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_production where company_name='$search_string' and variable_list=13 order by id");
      	$r_company1=mysql_fetch_array($sql);
    ?>
	<fieldset>
		<legend>Batch Maintained</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
					<td width="130" align="left" id="batch_maintained">Batch Maintained</td>
					<td width="190">
						<select name="cbo_batch_maintained" id="cbo_batch_maintained" class="combo_boxes" style="width:170px ">
							<?php
							foreach($replacement_lc as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($r_company1[batch_maintained]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
						</select>
					</td>
				</tr>
			</table>
		</div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_proo" id="save_up_proo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
	</fieldset>
</div>

<?
}
if ($type==5){//Commercial_Garments Export Capacity
//echo "$type";
?>
<div>
	<?php 
		//print_r($_GET);exit();
        $sql= mysql_db_query($DB, "select * from variable_settings_commercial where company_name='$search_string' and variable_list=5 order by id");
      	$r_company1=mysql_fetch_array($sql);
        //print_r($r_company1);
        //exit();	
    ?>
	<fieldset>
		<legend>Garments Export Capacity</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont4" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
					<td width="130" align="left" id="capacity_in_value">Capacity In Value</td>
					<td width="190" align="left"><input type="text" name="txt_capacity_value" id="txt_capacity_value" onKeyPress=" return numbersonly(this,event)" value="<? echo $r_company1[capacity_in_value]; ?>" class="text_boxes_numeric" style="width:159px; text-align:right"/></td>
				</tr>
				<tr>
					<td width="130" align="left" id="currency">Currency</td>
					<td width="190">
                       <select name="cbo_currency_id" id="cbo_currency_id" class="combo_boxes"  style="width:170px ">
							
								<?
								$sql= mysql_db_query($DB, "select currency_name,id from lib_list_currency order by id");
								while ($r_currency=mysql_fetch_array($sql))
								{
								?>
							<option value="<? echo $r_currency["id"]; ?>" <? if($r_company1["currency_id"]==$r_currency["id"]){?>selected<?php }?>><? echo "$r_currency[currency_name]" ?> </option>
							<? } ?>
						</select>	
					</td>
				</tr>
			</table>
		</div>
       <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont4" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_comm" id="save_up_comm" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>
<?
}
if ($type==6){//Commercial_Max BTB Limit
?>

<div>
    <?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_commercial where company_name='$search_string' and variable_list=6 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
    <fieldset>
        <legend>Max BTB Limit</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont4" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td width="100" align="left" id="max_btb_limit">Max BTB Limit</td>
                    <td width="170" align="left"><input type="text" name="txt_max_btb_limit" id="txt_max_btb_limit" onKeyPress=" return numbersonly(this,event)" value="<? echo $r_company1[max_btb_limit]; ?>" class="text_boxes_numeric" style="width:159px; text-align:right"/>%</td>
                </tr>
            </table>
        </div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont1" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_comm" id="save_up_comm" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>

<?
}
if ($type==7){//Commercial_Max PC Limit
?>

<div>
    <?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_commercial where company_name='$search_string' and variable_list=7 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
    <fieldset>
        <legend>Max PC Limit</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont4" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td width="100" align="left" id="max_pc_limit">Max PC Limit</td>
                    <td width="170" align="left"><input type="text" name="txt_max_pc_limit" id="txt_max_pc_limit" value="<? echo $r_company1[max_pc_limit]; ?>" class="text_boxes_numeric" onKeyPress=" return numbersonly(this,event)" style="width:159px; text-align:right"/>%</td>
                </tr>
            </table>
        </div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont1" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_comm" id="save_up_comm" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>

<?
}
if ($type==8){//Inventory_ILE/Landed Cost Standard
?>

<div>
	
    <fieldset>
        <legend>ILE/Landed Cost Standard</legend>
        <div style="width:700px;" align="left">
            <table cellspacing="0" width="100%" >
                <tr>
                    <td width="170" align="center" id="category">Category</td>
                    <td width="170" align="center" id="item_group">Item Group</td>
                    <td width="170" align="center" id="source">Source</td>
                    <td width="170" align="center" id="standard">Standard %</td>
                </tr>
            </table>
        </div>	
        <div style="width:700px; min-height:20px; max-height:400px;" id="variable_list_cont5" align="center">
            <table id="yarn_container" width="100%">
            <?php 
				$search_string=$_GET["q"];
				//echo $search_string;
				//echo "select * from variable_settings_inventory_ile_cost_standard where company_name=$search_string and variable_list=8 order by id";
				$sql= mysql_db_query($DB, "select * from variable_settings_inventory_ile_cost_standard where company_name='$search_string' and variable_list=8 order by id");
				//$r_company1=mysql_fetch_array($sql);
				$i=1;
				$row_count=mysql_num_rows($sql);
				//echo "Num $row_count";
				if ($row_count>0 )
				{
					while( $rows = mysql_fetch_array( $sql ) ) 
					{
						?>
                <tr style="text-decoration:none" align="" bgcolor="<?php echo "$bgcolor"; ?>"> 
                    <td width="170" align="center">
                        <select name="cbo_category<? echo $i; ?>" id="cbo_category<? echo $i; ?>" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($item_category as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                            if ($rows["category"]==$key){?> selected <?php } ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                    <td width="170" align="center">
                        <select name="cbo_item_group<? echo $i; ?>" id="cbo_item_group<? echo $i; ?>" class="combo_boxes"  style="width:170px ">
                            <option value="0">---Select Item Name---</option>
                                <?
                                $company_sql= mysql_db_query($DB, "select 	item_name,id from lib_trim where is_deleted=0  and status_active=1 order by item_name");
                                while ($r_company=mysql_fetch_array($company_sql))
                                {
                                ?>
                            <option value="<? echo $r_company["id"]; ?>" <? if($rows['item_group']==$r_company['id']){?>selected<?php }?>><? echo $r_company['item_name']; ?> </option>
                            <? } ?>
                        </select>
                    </td>
                    <td width="170" align="center">
                        <select name="cbo_source<? echo $i; ?>" id="cbo_source<? echo $i; ?>" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($source as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                            if ($rows["source"]==$key){?> selected <?php } ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                    <td width="170" align="center"><input type="text" name="txt_standard<? echo $i; ?>" id="txt_standard<? echo $i; ?>" value="<? echo $rows["standard"]; ?>" class="text_boxes_numeric" onfocus="add_variable_row(<? echo $i; ?>)"   style="width:160px"/></td>
                </tr>
                <?php
					$i=$i+1;
						}
					}
					else
					{
						$i=1;
				?>
                 <tr style="text-decoration:none" align="" bgcolor="<?php echo "$bgcolor"; ?>"> 
                    <td width="170" align="center">
                        <select name="cbo_category<? echo $i; ?>" id="cbo_category<? echo $i; ?>" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($item_category as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                            if ($rows["category"]==$key){?> selected <?php } ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                    <td width="170" align="center">
                        <select name="cbo_item_group<? echo $i; ?>" id="cbo_item_group<? echo $i; ?>" class="combo_boxes"  style="width:170px ">
                            <option value="0">---Select Item Name---</option>
                                <?
                                $company_sql= mysql_db_query($DB, "select 	item_name,id from lib_trim where is_deleted=0  and status_active=1 order by item_name");
                                while ($r_company=mysql_fetch_array($company_sql))
                                {
                                ?>
                            <option value="<? echo $r_company["id"]; ?>" <? if($cbo_item_group1==$r_company["id"]){?>selected<?php }?>><? echo "$r_company[item_name]" ?> </option>
                            <? } ?>
                        </select>
                    </td>
                    <td width="170" align="center">
                        <select name="cbo_source<? echo $i; ?>" id="cbo_source<? echo $i; ?>" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($source as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                            if ($cbo_source2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                    <td width="170" align="center"><input type="text" name="txt_standard<? echo $i; ?>" id="txt_standard<? echo $i; ?>" value="<? echo $rows["standard"]; ?>" class="text_boxes_numeric" onfocus="add_variable_row(<? echo $i; ?>)"   style="width:160px"/></td>
                </tr>
                <?
					}
				
				?>
                            
            </table>
        </div>
        <div style="width:700px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
        	<table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_invenn" id="save_up_invenn" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>
<?
}
if ($type==9){//Inventory_Hide Opening Stock Flag
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_inventory where company_name='$search_string' and variable_list=9 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
    <fieldset>
        <legend>Hide Opening Stock Flag</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td width="130" align="left" id="hide_opening_stock_flag">Hide Opening Stock Flag</td>
                    <td width="190">
                        <select name="cbo_hide_opening_stock_flag" id="cbo_hide_opening_stock_flag" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($replacement_lc as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                           if ($r_company1[hide_opening_stock_flag]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
        	<table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_invenn" id="save_up_invenn" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>
<?
}
if ($type==10){//Inventory_Item Rate Optional
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_inventory where company_name='$search_string' and variable_list=10 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
    <fieldset>
        <legend>Item Rate Optional</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td width="130" align="left" id="item_rate_optional">Item Rate Optional</td>
                    <td width="190">
                        <select name="cbo_item_rate_optional" id="cbo_item_rate_optional" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($replacement_lc as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                            if ($r_company1[item_rate_optional]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
        	<table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_invenn" id="save_up_invenn" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>

<?
}

if ($type==31){//Accounts Integreted
//echo 'ffdfdf';die;
?>

<div>
	<?php 
       // $sql= mysql_db_query($DB, "select * from variable_settings_accounts where company_name='$search_string' and variable_list=3 order by id");
      // $r_company1=mysql_fetch_array($sql);
    ?>
    <fieldset>
        <legend>Integreted</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td width="130" align="left" id="item_qc">Integreted</td>
                    <td width="190">
                        <select name="cbo_integreted" id="cbo_integreted" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($replacement_lc as $key=>$value):
                            ?>
                            <option value=<? echo "$key";?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
       <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
        	<table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_ac" id="save_up_ac" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>
<?
}


if ($type==11){//Inventory_Item QC
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_inventory where company_name='$search_string' and variable_list=11 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
    <fieldset>
        <legend>Item QC</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td width="130" align="left" id="item_qc">Item QC</td>
                    <td width="190">
                        <select name="cbo_item_qc" id="cbo_item_qc" class="combo_boxes" style="width:170px ">
                            <?php
                            foreach($replacement_lc as $key=>$value):
                            ?>
                            <option value=<? echo "$key";
                            if ($r_company1[item_qc]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
       <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
        	<table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_invenn" id="save_up_invenn" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
    </fieldset>
</div>
<?
}
if ($type==16){//Inventory_Item user given code 

      $sql= mysql_db_query($DB, "select * from variable_settings_inventory where company_name='$search_string' and variable_list=16 order by id");
       
    ?>

<div>
    <fieldset>
        <legend>User Given Code</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
            <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
                <thead> 
                    <th align="center" id="item_category">Item Category</th>
                    <th align="center" id="item_category_status">Status</th>
                </thead>
                <tr class="general">
                    <td>
                       <select name="cbo_item_category" id="cbo_item_category" class="combo_boxes" style="width:155px ">
							<?php
                            foreach($wo_item_category as $key=>$value):
                            ?>
                            <option value=<? echo "$key"; ?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                       </select>
                    </td>
                    <td>
                        <select name="cbo_item_status" id="cbo_item_status" class="combo_boxes" style="width:130px ">
                            <?php
                            foreach($replacement_lc as $key=>$value):
                            ?>
                            <option value=<? echo "$key";?>> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
       <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
        	<table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_invenn" id="save_up_invenn" value="">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
</div>
<div>
	<div style="width:500px">
    	<table cellpadding="0" cellspacing="0" width="100%" class="rpt_table">
        	<thead>
            	<th width="200">Company</th>
                <th width="200">Item Category</th>
                <th>Status</th>
            </thead>
        </table>
    </div>
    <div style="width:500px; overflow-y:scroll; max-height:150px" id="user_given_code_list_vie" align="left">
    	<table cellpadding="0" cellspacing="0" width="100%" class="rpt_table">
        	<?
				$i=1;
				while( $result=mysql_fetch_array($sql))
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";	
						$i++;
				?>
                <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onclick="user_given_code_update('<?php echo $result[id];?>','user_given_code_update');">
                	<td width="200"><? echo $company_details[$result[company_name]]; ?></td>
                    <td width="200"><? echo $wo_item_category[$result[item_category_id]]; ?></td>
                    <td align="center"><? echo $replacement_lc[$result[user_given_code_status]]; ?></td>
                </tr>
                <?
				}
			?>
        </table>
    </div>
</div>    
</fieldset>
<?
}
if ($type==12){//Order Tracking_Sales Year started
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_order_tracking where company_name='$search_string' and variable_list=12 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
	<fieldset>
		<legend>Sales Year started</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
					<td width="100" align="left" id="sales_year_started">Sales Year started</td>
						<td width="190">
						<select name="cbo_sales_year_started_date" id="cbo_sales_year_started_date" class="combo_boxes" style="width:170px ">
							<?php
							foreach($months_name as $key=>$value):
							?>
                        	<option value=<? echo "$key";
                                if ($r_company1[sales_year_started]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                <?		
                                endforeach;
                                ?>
                        </select>
					</td>
				</tr>
			</table>
		</div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_woo" id="save_up_woo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
			</table>
		</div>
	</fieldset>
</div>

<?
}
if ($type==14){//Order Tracking_TNA Integrated
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_order_tracking where company_name='$search_string' and variable_list=14 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
	<fieldset>
		<legend>TNA Integrated</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
					<td width="100" align="left">TNA Integrated</td>
						<td width="190">
						<select name="cbo_tna_integrated" id="cbo_tna_integrated" class="combo_boxes" style="width:170px ">
							<?php
							foreach($replacement_lc as $key=>$value):
							?>
                        	<option value=<? echo "$key";
                                if ($r_company1[tna_integrated]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                <?		
                                endforeach;
                                ?>
                        </select>
					</td>
				</tr>
			</table>
		</div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_woo" id="save_up_woo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
			</table>
		</div>
	</fieldset>
</div>
<?
}
if ($type==15){//Order Tracking_Pre_Costing_Profit_Calculative
?>

<div>
	<?php 
        $sql= mysql_db_query($DB, "select * from variable_settings_order_tracking where company_name='$search_string' and variable_list=15 order by id");
       $r_company1=mysql_fetch_array($sql);
    ?>
	<fieldset>
		<legend>Pre Costing : Profit Calculative</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
					<td width="100" align="left">Profit Calculative</td>
						<td width="190">
						<select name="cbo_profit_calculative" id="cbo_profit_calculative" class="combo_boxes" style="width:170px ">
							<?php
							foreach($replacement_lc as $key=>$value):
							?>
                        	<option value=<? echo "$key";
                                if ($r_company1[profit_calculative]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                <?		
                                endforeach;
                                ?>
                        </select>
					</td>
				</tr>
			</table>
		</div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" >
				<tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_woo" id="save_up_woo" value="<? echo $r_company1["id"]; ?>">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
			</table>
		</div>
	</fieldset>
</div>

<?
}
if ($type==17){//Commercial Possible heads for BTB

     $sql= mysql_db_query($DB, "select * from variable_settings_commercial where company_name='$search_string' and variable_list=17 order by id");
    ?>
<div>
    <fieldset>
        <legend>Possible Heads For BTB</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
            <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
                <thead> 
                    <th>Cost Heads</th>
                    <th>Status</th>
                </thead>
                <tr class="general">
                    <td>
                       <select name="cbo_cost_heads" id="cbo_cost_heads" class="combo_boxes" style="width:155px ">
							<?php
                            foreach($cost_heads as $key=>$value):
                            ?>
                            <option value="<? echo $key; ?>"> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                       </select>
                    </td>
                    <td>
                        <select name="cbo_cost_heads_status" id="cbo_cost_heads_status" class="combo_boxes" style="width:130px ">
                            <?php
                            foreach($replacement_lc as $key=>$value):
                            ?>
                            <option value="<? echo $key;?>"> <? echo "$value" ; ?> </option>
                            <?		
                            endforeach;
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
       <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont5" align="center">
        	<table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;					
                        <input type="hidden" name="save_up_comm" id="save_up_comm" value="">
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id">	
                    </td>					
                </tr>
                <tr>
                    <td align="center" width="320">
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
            </table>
        </div>
<div>
	<div style="width:500px">
    	<table cellpadding="0" cellspacing="0" width="100%" class="rpt_table">
        	<thead>
            	<th width="200">Company</th>
                <th width="200">Cost Heads</th>
                <th>Status</th>
            </thead>
        </table>
    </div>
    <div style="width:500px; overflow-y:scroll; max-height:250px" align="left">
    	<table cellpadding="0" cellspacing="0" width="100%" class="rpt_table">
        	<?
				$i=1;
				while( $result=mysql_fetch_array($sql))
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";	
						$i++;
				?>
                <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onclick="cost_heads_update('<?php echo $result[id];?>','cost_heads_update');">
                	<td width="200"><? echo $company_details[$result[company_name]]; ?></td>
                    <td width="200"><? echo $cost_heads[$result[cost_heads]]; ?></td>
                    <td align="center"><? echo $replacement_lc[$result[cost_heads_status]]; ?></td>
                </tr>
                <?
				}

			?>
        </table>
    </div>
</div>    
</fieldset>
<?
}
if ($type==18){//Order grey Fabric Calulation
 $sql_com= mysql_db_query($DB, "select * from variable_settings_order_tracking where company_name='$search_string' and variable_list=18 order by id");
 if(mysql_num_rows($sql_com)>0) $save_update_value=1; else $save_update_value=0;
?>
<div>
   <fieldset>
		<legend>Process Loss Method</legend>
		<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" class="rpt_table">
				<thead> 
					<th width="200" align="left">Item Category</th>
                    <th width="190" align="left">Process Loss Method</th>
				</thead>
                <?
				$i=1;
				foreach($wo_category as $key=>$value)
				{
					if($key!=0)
					{
						 $sql= mysql_db_query($DB, "select * from variable_settings_order_tracking where company_name='$search_string' and item_category_id='$key' and variable_list=18 order by id");
	 					$row=mysql_fetch_array($sql);
				?>
                        <tr align="center">
                            <td>
                                <select name="item_category_id_<? echo $i; ?>" id="item_category_id_<? echo $i; ?>" class="combo_boxes" style="width:170px ">
                                    <option value="<? echo $key; ?>"><? echo $value; ?></option>
                                </select>
                            </td>
                            <td>
                                <select name="process_loss_method_<? echo $i; ?>" id="process_loss_method_<? echo $i; ?>" class="combo_boxes" style="width:150px ">
                                    <?php
                                    
                                    foreach($process_loss_method as $key=>$value):
                                    ?>
                                    <option value=<? echo "$key";
                                        if ($row[process_loss_method]==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>
                                </select>
                            </td>
                        </tr>
                        <input type="hidden" name="save_up_woo_<? echo $i; ?>" id="save_up_woo_<? echo $i; ?>" value="<? echo $row["id"]; ?>">
                <?
				$i++;}
				}
				?>
			</table>
		</div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
			<table cellspacing="0" width="100%" >
                <tr>
                    <td align="center" width="320">
                    	<input type="hidden" name="save_up_woo" id="save_up_woo" value="<? echo $save_update_value; ?>"><!--For massage show-->
                        <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                        <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                    </td>				
                </tr>
			</table>
		</div>
	</fieldset>
</div>
<?
}

if ($type==555)
{
$data=explode('_',$search_string);
	echo $data[1];
	exit();	
	
/*	$data=explode('_',$search_string);
	echo $data[1];
?>
   
        	<?
            if($data[1]==1)
			{
			?>
            <tr> 
                <td width="130" align="left" id="item_qc">Write</td>
                <td width="190">
                    <select name="cbo_write" id="cbo_write" class="combo_boxes" style="width:170px ">
                    <?php
                    foreach($replacement_lc as $key=>$value):
                    ?>
                    <option value=<? echo "$key";?>> <? echo "$value" ; ?> </option>
                    <?		
                    endforeach;
                    ?>
                    </select>
                </td>
            </tr>
            <? 
			} 
			else if($data[1]==2)
			{
			?>
            <tr> 
                <td width="130" align="left" id="item_qc">Employee Code</td>
                <td width="190">
                    <select name="cbo_code_employe" id="cbo_code_employe" class="combo_boxes" style="width:170px ">
                    <?php
                    foreach($edit_permetion as $key=>$value):
                    ?>
                    <option value=<? echo "$key";?>> <? echo "$value" ; ?> </option>
                    <?		
                    endforeach;
                    ?>
                    </select>
                </td>
            </tr>
            <?
			}
            else if($data[1]==3)
			{
			?>
            <tr> 
                <td width="130" align="left" id="item_qc">Based On Given Code</td>
                <td width="190">
                	<input type="text" name="given_emp_code" id="given_emp_code" width="170px" class="combo_boxes" onKeyPress=" return numbersonly(this,event)" />
                </td>
            </tr>
            <?
			}
            else if($data[1]==4)
			{
			?>
            <tr> 
                <td width="130" align="left" id="item_qc">Blanding Option</td>
                <td width="190">
					<? 
					$i=1;
					foreach ($blanding_option_arr as $key=>$value){
					//for ($i=1;$i<count($blanding_option_arr);$i++){
						?>
                    <input type="checkbox" name="blanding_<? echo $i;?>" id="blanding_<? echo $i;?>" value="<? echo $key; ?>" onclick="js_set_value(<? echo $i; ?>)"><? echo $value; ?><br />
                    <? $i++; } ?>
                </td>
            </tr>
            <?
			}
			?>
     
<?
*/
}
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>