<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

$date=time();
$user_name=$_SESSION['logic_erp']["user_name"];

include('../../includes/common.php');	
extract($_GET);
$sql_history = "";
//Buyer_Info Registrartion and Update Here


if ($action=="buyer_info"){ 
   if  ($isupdate==""){//Insert Here
   
   		$unique_field_name = "buyer_name";
		$table_name = "lib_buyer";
		$query = "buyer_name='$txt_buyer_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
		
		$id_field_name = "id";
		$table_name = "lib_buyer";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
		$_SESSION['job_no']=$id;

		$sql = "INSERT INTO lib_buyer (
							id,
							buyer_name,
							contact_person,
							buyer_email,
							contact_no,
							web_site,
							address,
							subcontract_party,
							remark,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'".mysql_real_escape_string($txt_buyer_name)."',
							'".mysql_real_escape_string($txt_contact_person)."',
							'".mysql_real_escape_string($txt_buyer_email)."',
							'$txt_contact_no',
							'".mysql_real_escape_string($txt_web_site)."',
							'".mysql_real_escape_string($txt_address)."',
							'".mysql_real_escape_string($cbo_subcontract_party)."',
							'".mysql_real_escape_string($txt_remark)."',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'0'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_"."$id"."_0";		
		exit();
		
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
			$_SESSION['job_no']=$isupdate;	
		$sql = "UPDATE lib_buyer
				SET
					buyer_name			= '".mysql_real_escape_string($txt_buyer_name)."',
					contact_person		= '".mysql_real_escape_string($txt_contact_person)."',
					buyer_email			= '".mysql_real_escape_string($txt_buyer_email)."',
					contact_no			= '$txt_contact_no',
					web_site			= '".mysql_real_escape_string($txt_web_site)."',
					address				= '".mysql_real_escape_string($txt_address)."',
					subcontract_party = '".mysql_real_escape_string($cbo_subcontract_party)."',
					remark				= '".mysql_real_escape_string($txt_remark)."',
					inserted_by 		= '$inserted_by',
					insert_date			= '$insert_date',
					updated_by			= '".mysql_real_escape_string($user_name)."',
					update_date 		= '$date',
					status_active		= '$cbo_status',
					is_deleted			= '0'
					WHERE id = '$isupdate'
				 ";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_"."$id"."_0";
		exit();
	}
}

//--------------------------Buyer Task Info Start-----------------------------------------------------------------------

	
if ($action=="buyer_task"){ 
	
	mysql_query("SET CHARACTER SET utf8");
	mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
	
	$company_sql= mysql_db_query($DB, "select id from tna_task_template where lead_time='$lead_time' and material_source='$cbo_material_source' and is_deleted=0  and status_active=1");
	if ($r_company=mysql_fetch_array($company_sql))
	{
		$id_template=$r_company[id];
		//echo "This Task Have Already Exists";  // Duplicate msg
	}
	else
	{
		$id_field_name = "id";
		$table_name = "tna_task_template";
		$id_template= return_next_id($id_field_name,$table_name);
		$sql = "INSERT INTO tna_task_template (
						id,
						lead_time,
						material_source,
						inserted_by,
						insert_date,
						updated_by,
						update_date,
						status_active,
						is_deleted
					) VALUES (
						'$id_template',
						'".mysql_real_escape_string($lead_time)."',
						'".mysql_real_escape_string($cbo_material_source)."',
						'".mysql_real_escape_string($user_name)."',
						'$date',
						'$updated_by',
						'$update_date',
						'1',
						'0'
					)";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history.=$sql;	
	}
	
	if ($isupdate=="")  // Insert Details
	{
		$company_sql= mysql_db_query($DB, "select id from tna_task_details where task_template_id='$id_template' and task_category='$cbo_task_category' and task_name='$task_name' and short_name='".mysql_real_escape_string(strtolower($short_name))."' and is_deleted=0  and status_active=1");
		if ($r_company=mysql_fetch_array($company_sql))
		{
			$id_cld=$r_company[id];
		//	echo "This Task Have Already Exists";  // Duplicate msg
		}
		else
		{
		 	$id_field_name = "id";
			$table_name = "tna_task_details";
			$id_cld= return_next_id($id_field_name,$table_name);
			
			mysql_query("SET CHARACTER SET utf8");
			mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
	
			$sql = "INSERT INTO tna_task_details (
							id,
							task_template_id,
							task_category,
							task_name,
							short_name,
							deadline,
							execution_days,
							responsible,
							notice_before,
							sequence_no,
							for_specific,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id_cld',
							'$id_template',
							'".mysql_real_escape_string($cbo_task_category)."',
							'".mysql_real_escape_string($task_name)."',
							'".mysql_real_escape_string(strtolower($short_name))."',
							'".mysql_real_escape_string($deadline)."',
							'".mysql_real_escape_string($execution_days)."',
							'".mysql_real_escape_string($cbo_responsible)."',
							'".mysql_real_escape_string($notice_before)."',
							'".mysql_real_escape_string($sequence_no)."',
							'".mysql_real_escape_string($cbo_buyer_name)."',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'0'
						)";
			mysql_query( $sql ) or die (mysql_error());		
			
			$sql_history.=$sql;	
			$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			
			echo "2_".encrypt($sql,"logic_erp_2011_2012")."_".$id_template;
			exit();
		}
	 }
	 else
	 {
		 //update task master
		 /*$company_sql= mysql_db_query($DB, "select id from tna_task_details where task_template_id='$system_id' and task_category='$cbo_task_category' and task_name='$task_name' and is_deleted=0  and status_active=1");
		if ($r_company=mysql_fetch_array($company_sql))
		{
			$id_cld=$r_company[id];
			echo "This Task Have Already Exists";  // Duplicate msg
		}
		 else
		{*/
			 $sql = " UPDATE tna_task_details SET
						task_category		= '".mysql_real_escape_string($cbo_task_category)."',
						task_name			= '".mysql_real_escape_string($task_name)."',
						short_name			= '".mysql_real_escape_string($short_name)."',
						deadline			= '".mysql_real_escape_string($deadline)."',
						execution_days		= '".mysql_real_escape_string($execution_days)."',
						responsible			= '".mysql_real_escape_string($cbo_responsible)."',
						notice_before		= '".mysql_real_escape_string($notice_before)."',
						sequence_no			= '".mysql_real_escape_string($sequence_no)."',
						for_specific		= '".mysql_real_escape_string($cbo_buyer_name)."',
						inserted_by 		= '$inserted_by',
						insert_date			= '$insert_date',
						updated_by			= '".mysql_real_escape_string($user_name)."',
						update_date 		= '$date',
						status_active		= '$cbo_status',
						is_deleted			= '0'
						WHERE id = '$isupdate'
					 ";
					 
					 
			mysql_query( $sql ) or die (mysql_error());
			
			$sql_history.=$sql;	
			$sql_history=encrypt($sql_history, "logic_erp_2011_2012");
			$_SESSION['sql_history']=$sql_history;
			
			echo "3_".encrypt($sql,"logic_erp_2011_2012");		
			exit();
		//}
	}
	 
}
//--------------------------Supplier Info Start-----------------------------------------------------------------------

if ($action=="supplier_info"){ 
   if  ($isupdate==""){//Insert Here
   
   		$unique_field_name = "supplier_name";
		$table_name = "lib_supplier";
		$query = "supplier_name='$txt_company_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
   
		$id_field_name = "id";
		$table_name = "lib_supplier";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO lib_supplier (
							id,
							supplier_name,
							contact_person,
							supplier_email,
							contact_no,
							subcontract_party,
							web_site,
							address,
							lib_country_list_id,
							remark,
							individual,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'".mysql_real_escape_string($txt_company_name)."',
							'".mysql_real_escape_string($txt_contact_person)."',
							'".mysql_real_escape_string($txt_supplier_email)."',
							'$txt_contact_no',
							'".mysql_real_escape_string($cbo_subcontract_party)."',
							'".mysql_real_escape_string($txt_web_site)."',
							'".mysql_real_escape_string($txt_address)."',
							'".mysql_real_escape_string($cbo_lib_country_id)."',
							'".mysql_real_escape_string($txt_remark)."',
							'$cbo_individual',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'0'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_"."$id"."_0";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
				
		$sql = "UPDATE lib_supplier
				SET
					supplier_name	    = '".mysql_real_escape_string($txt_company_name)."',
					contact_person		= '".mysql_real_escape_string($txt_contact_person)."',
					supplier_email		= '".mysql_real_escape_string($txt_supplier_email)."',
					contact_no			= '$txt_contact_no',
					subcontract_party	= '".mysql_real_escape_string($cbo_subcontract_party)."',
					web_site			= '".mysql_real_escape_string($txt_web_site)."',
					address				= '".mysql_real_escape_string($txt_address)."',
					lib_country_list_id = '".mysql_real_escape_string($cbo_lib_country_id)."',
					remark				= '".mysql_real_escape_string($txt_remark)."',
					individual			= '$cbo_individual',
					updated_by			= '".mysql_real_escape_string($user_name)."',
					update_date 		= '$date',
					status_active		= '$cbo_status',
					is_deleted			= '0'
					WHERE id = '$isupdate'
				 ";
		mysql_query( $sql ) or die (mysql_error());
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_"."$id"."_0";	
		exit();
	}
}

//--------------------------Supplier Info End-----------------------------------------------------------------------

//--------------------------Other Party Info Start-----------------------------------------------------------------------

if ($action=="other_party_info"){ 
   if  ($isupdate==""){//Insert Here
   
   		$unique_field_name = "other_party_name 	";
		$table_name = "lib_other_party";
		$query = "other_party_name 	='$txt_company_name'";
		
		$unique_check = check_uniqueness( $unique_field_name, $table_name, $query );
		if( $unique_check == false )  // Duplicate
		{
			echo "1";
			exit();
		}
   
		$id_field_name = "id";
		$table_name = "lib_other_party";
		$id= return_next_id($id_field_name,$table_name);
		
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	


		$sql = "INSERT INTO  lib_other_party (
							id,
							other_party_name,
							contact_person,
							other_party_email,
							contact_no,
							web_site,
							address,
							lib_country_list_id,
							remark,
							individual,
							inserted_by,
							insert_date,
							updated_by,
							update_date,
							status_active,
							is_deleted
						) VALUES (
							'$id',
							'".mysql_real_escape_string($txt_company_name)."',
							'".mysql_real_escape_string($txt_contact_person)."',
							'".mysql_real_escape_string($txt_supplier_email)."',
							'$txt_contact_no',
							'".mysql_real_escape_string($txt_web_site)."',
							'".mysql_real_escape_string($txt_address)."',
							'".mysql_real_escape_string($cbo_lib_country_id)."',
							'".mysql_real_escape_string($txt_remark)."',
							'$cbo_individual',
							'".mysql_real_escape_string($user_name)."',
							'$date',
							'$updated_by',
							'$update_date',
							'$cbo_status',
							'0'
						)";
		mysql_query( $sql ) or die (mysql_error());	
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "2_"."$id"."_0";
		exit();
	} else {
		mysql_query("SET CHARACTER SET utf8");
		mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 
				
		$sql = "UPDATE  lib_other_party
				SET
					other_party_name	    = '".mysql_real_escape_string($txt_company_name)."',
					contact_person		= '".mysql_real_escape_string($txt_contact_person)."',
					other_party_email		= '".mysql_real_escape_string($txt_supplier_email)."',
					contact_no			= '$txt_contact_no',
					web_site			= '".mysql_real_escape_string($txt_web_site)."',
					address				= '".mysql_real_escape_string($txt_address)."',
					lib_country_list_id = '".mysql_real_escape_string($cbo_lib_country_id)."',
					remark				= '".mysql_real_escape_string($txt_remark)."',
					individual			= '$cbo_individual',
					updated_by			= '".mysql_real_escape_string($user_name)."',
					update_date 		= '$date',
					status_active		= '$cbo_status',
					is_deleted			= '0'
					WHERE id = '$isupdate'
				 ";
			
		mysql_query( $sql ) or die (mysql_error());
		
		$sql_history=encrypt($sql, "logic_erp_2011_2012");
		$_SESSION['sql_history']=$sql_history;
		echo "3_"."$id"."_0";	
		exit();
	}
}
//--------------------------Other Party Info Info End-----------------------------------------------------------------------



function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}

?>