<? 
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');
$company=$_GET["com"];
//echo "as $company";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cost Sheets</title>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
	
	
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" />
<script>
function showResult_searchs(div,type)
{
	var system_id=document.getElementById('system_id').value;
  	document.getElementById(div).innerHTML="";
	if (window.XMLHttpRequest)
	{	///code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		document.getElementById(div).innerHTML=xmlhttp.responseText;
    	//document.getElementById(div).style.border="1px solid #A5ACB2";
    	}
  	}
  	//alert(type);
	xmlhttp.open("GET","list_view_buyer.php?system_id="+trim(system_id)+"&type="+type,true);
	xmlhttp.send();
}

function js_set_value(system_id, lead_time,material_source,hid_task_category)
{
	//alert(str);
	//alert(str2);
	document.getElementById('hid_system_id').value=system_id;
	document.getElementById('hid_lead_time').value=lead_time;
	document.getElementById('hid_material_source').value=material_source;
	document.getElementById('hid_task_category').value=hid_task_category;


}


function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}


</script>

</head>


<body>
<div align="center" style="width:500px;">
<form name="search_order_frm"  id="search_order_frm">
	<fieldset style="width:500px;">
	<table width="500" cellspacing="0" cellpadding="0" class="rpt_table">
		<thead>
			<th>
				System ID
			</th>
            <th><input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" /></th>
		</thead>
        <tr class="general">
        	<td><input type="text" name="system_id" id="system_id"  class="text_boxes" /></td>
            <td><input type="button" name="btn" id="btn" value="Show" style="width:100px" class="formbutton" onclick="showResult_searchs('search_div','system_id_search')" /></td>
        </tr>
        <tr>
			<td align="center" colspan="2">
				<div id="search_div" style="width:500px; margin-top:10px"></div>
			</td>
		</tr>
        </tr>
			<td >
				<input type="hidden" name="hid_system_id" id="hid_system_id" width="10"    />
                <input type="hidden" name="hid_lead_time" id="hid_lead_time"  width="10"    />
                <input type="hidden" name="hid_material_source" id="hid_material_source"   width="10"    />
                <input type="hidden" name="hid_task_category" id="hid_task_category"   width="10"    />

			</td>
		</tr>
	</table>
	</fieldset>
	</form>
</div>

</body>
