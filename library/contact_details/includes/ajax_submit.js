//Do not change 
function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer"){
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	}else{
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();


//---------------------------Buyer Info-----------------------------------------------------------------------
function fnc_buyer_info(save_perm,edit_perm,delete_perm,approve_perm){
	var txt_mst_id	= escape(document.getElementById('txt_mst_id').value);
	var txt_buyer_name		= escape(document.getElementById('txt_buyer_name').value);
	var txt_contact_person	= escape(document.getElementById('txt_contact_person').value);
	var txt_contact_no  	= escape(document.getElementById('txt_contact_no').value);
	var txt_web_site		= escape(document.getElementById('txt_web_site').value);
	var txt_buyer_email     = escape(document.getElementById('txt_buyer_email').value);
	var txt_address         = escape(document.getElementById('txt_address').value);
	var cbo_subcontract_party 	= escape(document.getElementById('cbo_subcontract_party').value);
	var cbo_status 			= escape(document.getElementById('cbo_status').value);
	var txt_remark 			= escape(document.getElementById('txt_remark').value);
	
	var save_up = escape(document.getElementById('save_up').value);
					
		//alert (edit_perm);			
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (edit_perm==2 && save_up!="") // no edit 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Edit Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
	}
	else if (save_perm==2 && save_up=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have New Entry Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
		
	}
	else if (delete_perm==2 && cbo_status==2 ) 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Delete Permission.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#txt_buyer_name').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_buyer_name').focus();
			$(this).html('Please Insert Buyer Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if($('#cbo_subcontract_party').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_subcontract_party').focus();
			$(this).html('Please Select a Subcontract Party Option').addClass('messageboxerror').fadeTo(900,1);
		});	
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=buyer_info&isupdate='+save_up+
					'&txt_buyer_name='+txt_buyer_name+
					'&txt_contact_person='+txt_contact_person+
					'&txt_contact_no='+txt_contact_no+
					'&txt_web_site='+txt_web_site+
					'&txt_buyer_email='+txt_buyer_email+
					'&txt_address='+txt_address+
					'&cbo_subcontract_party='+cbo_subcontract_party+
					'&cbo_status='+cbo_status+
					'&txt_remark='+txt_remark+
					'&nocache ='+nocache);
		http.onreadystatechange = buyerReply_info;
		http.send(null); 
	}
}

function buyerReply_info() {
		if(http.readyState == 4){ 		
		//var response = http.responseText;	
		
		var response = http.responseText.split('_');	
		 
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Buyer Name, Please Check agian.').addClass('messageboxerror').fadeTo(900,1);
				 
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history('',"library","buyer_info","update","../../");
				
				showResult_multi(' ',' ','1','buyer_list_view');
				 
				document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history(response[2],"library","buyer_info","insert","../../");
				showResult_multi(' ',' ','1','buyer_list_view');
				document.getElementById('save_up').value="";
				
			});
		}
							
		
	//alert(response);
	
	}
}	
//---------------------------Buyer Task Info Start-----------------------------------------------------------------------
function fnc_buyer_task(save_perm,edit_perm,delete_perm,approve_perm){
	var id_m					= escape(document.getElementById('id_m').value);
	var lead_time				= escape(document.getElementById('lead_time').value);
	var cbo_material_source		= escape(document.getElementById('cbo_material_source').value);
	var cbo_task_category		= escape(document.getElementById('cbo_task_category').value);
	var task_name  				= escape(document.getElementById('task_name').value);
	var short_name				= escape(document.getElementById('short_name').value);	
	var deadline				= escape(document.getElementById('deadline').value);
	var execution_days  		= escape(document.getElementById('execution_days').value);
	var cbo_responsible			= escape(document.getElementById('cbo_responsible').value);	
	var notice_before			= escape(document.getElementById('notice_before').value);
	var sequence_no 			= escape(document.getElementById('sequence_no').value);
	var cbo_buyer_name			= escape(document.getElementById('cbo_buyer_name').value);
	var cbo_status				= escape(document.getElementById('cbo_status').value);
	var save_up 				= escape(document.getElementById('save_up').value);
					
		//alert (save_up);	return;		
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (save_up=="" && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up!="" && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( cbo_status==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if($('#cbo_material_source').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_material_source').focus();
			$(this).html('Please Select Material Source').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#lead_time').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#lead_time').focus();
			$(this).html('Please Enter Lead Time').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_task_category').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_task_category').focus();
			$(this).html('Please Select Task Category').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#short_name').val()==''){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_task_category').focus();
			$(this).html('Please Insert Short Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=buyer_task&isupdate='+save_up+
					'&id_m='+id_m+
					'&lead_time='+lead_time+
					'&cbo_material_source='+cbo_material_source+
					'&cbo_task_category='+cbo_task_category+
					'&task_name='+task_name+
					'&short_name='+short_name+
					'&deadline='+deadline+
					'&execution_days='+execution_days+
					'&cbo_responsible='+cbo_responsible+
					'&notice_before='+notice_before+
					'&sequence_no='+sequence_no+
					'&cbo_buyer_name='+cbo_buyer_name+
					'&cbo_status='+cbo_status+
					'&nocache ='+nocache);
		http.onreadystatechange = buyer_taskReply_info;
		http.send(null); 
	}
}

function buyer_taskReply_info() {
		if(http.readyState == 4){ 		
		// var response = http.responseText;	
		
		
		var response = http.responseText.split('_');	
		 //alert(response[2]);
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Duplicate Buyer Task Name, Please Check agian.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','tna','tna_template','update','../../');
                document.getElementById('cbo_task_category').value=0;
				document.getElementById('task_name').value='';
				document.getElementById('short_name').value='';	
				document.getElementById('deadline').value='';
				document.getElementById('execution_days').value='';
				document.getElementById('cbo_responsible').value='';	
				document.getElementById('notice_before').value='';
				document.getElementById('sequence_no').value='';
				document.getElementById('cbo_buyer_name').value='';
				document.getElementById('cbo_status').value='';
	            document.getElementById('save_up').value='';				
				//showResult(document.getElementById('cbo_task_category').value,'3','buyer_task_cont');
				showResult(document.getElementById('system_id').value,'3','buyer_task_cont');
				
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','tna','tna_template','insert','../../');
				document.getElementById('system_id').value=response[2];
				document.getElementById('cbo_task_category').value=0;
				document.getElementById('task_name').value='';
				document.getElementById('short_name').value='';	
				document.getElementById('deadline').value='';
				document.getElementById('execution_days').value='';
				document.getElementById('cbo_responsible').value='';	
				document.getElementById('notice_before').value='';
				document.getElementById('sequence_no').value='';
				document.getElementById('cbo_buyer_name').value='';
				document.getElementById('cbo_status').value='';
	            document.getElementById('save_up').value='';
				document.getElementById('cbo_task_category').focus();
				//showResult(document.getElementById('cbo_task_category').value,'3','buyer_task_cont');
				showResult(document.getElementById('system_id').value,'3','buyer_task_cont');
				
			});
		}
	
	
	}
}	

//---------------------------Buyer Task Info End-----------------------------------------------------------------------

//---------------------------Module Save / Update Style Info-----------------------------------------------------------------------
function fnc_style_info(){
	
	var txt_style_name	= encodeURI(document.getElementById('txt_style_name').value);
	var save_up 			= encodeURI(document.getElementById('save_up').value);
					
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
				
	if($('#txt_style_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_style_name').focus();
			$(this).html('Please Enter Style Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=style_details&isupdate='+save_up+
					'&txt_style_name='+txt_style_name+
								'&nocache ='+nocache);
		http.onreadystatechange = styleReply_info;
		http.send(null); 
	}
}

function styleReply_info() {
	if(http.readyState == 4){ 		
		var response = http.responseText;	
		//alert(response);
		document.getElementById('txt_style_name').value="";
		
	
		if (response==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a User name of this ID, Please use another user name.').addClass('messageboxerror').fadeTo(900,1);
				//document.getElementById('save_up').value="";
			});
		}
		else if (response==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
		else if (response==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('save_up').value="";
			});
		}
							
		
	//alert(response);
	
	}
}	

//---------------------------Supplier Information-----------------------------------------------------------------------
function fnc_supplier_info(save_perm,edit_perm,delete_perm,approve_perm){
	var txt_mst_id				= escape(document.getElementById('txt_mst_id').value);
	var txt_company_name		= escape(document.getElementById('txt_company_name').value);
	var txt_contact_person		= escape(document.getElementById('txt_contact_person').value);
	var txt_contact_no  		= escape(document.getElementById('txt_contact_no').value);
	var txt_remark  			= escape(document.getElementById('txt_remark').value);
	var cbo_subcontract_party  	= escape(document.getElementById('cbo_subcontract_party').value);
	var txt_web_site			= escape(document.getElementById('txt_web_site').value);
	var txt_supplier_email      = escape(document.getElementById('txt_supplier_email').value);
	var txt_address         	= escape(document.getElementById('txt_address').value);
	var cbo_lib_country_id 		= escape(document.getElementById('cbo_lib_country_id').value);
	var cbo_status				= escape(document.getElementById('cbo_status').value);
	var cbo_individual			= escape(document.getElementById('cbo_individual').value);
	var save_up 				= escape(document.getElementById('save_up').value);
					
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (edit_perm==2 && save_up!="") // no edit 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Edit Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
	}
	else if (save_perm==2 && save_up=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have New Entry Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
		
	}
	else if (delete_perm==2 && cbo_status==2 ) 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Delete Permission.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
				
	else if($('#txt_company_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_company_name').focus();
			$(this).html('Please Enter Supplier Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_subcontract_party').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_subcontract_party').focus();
			$(this).html('Please Select a Subcontract Party Option').addClass('messageboxerror').fadeTo(900,1);
		});	
	}
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=supplier_info&isupdate='+save_up+
					'&txt_company_name='+txt_company_name+
					'&txt_contact_person='+txt_contact_person+
					'&txt_contact_no='+txt_contact_no+
					'&txt_web_site='+txt_web_site+
					'&txt_address='+txt_address+
					'&txt_remark='+txt_remark+	
					'&cbo_subcontract_party='+cbo_subcontract_party+		
					'&cbo_lib_country_id='+cbo_lib_country_id+					
					'&txt_supplier_email='+txt_supplier_email+
					'&cbo_status='+cbo_status+	
					'&cbo_individual='+cbo_individual+	
					'&nocache ='+nocache);
		http.onreadystatechange = supplierReply_info;
		http.send(null); 
	}
}

function supplierReply_info() {
	if(http.readyState == 4){ 
	
		var response = http.responseText.split('_');	
	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Supplier name of this ID, Please use another Supplier name.').addClass('messageboxerror').fadeTo(900,1);				
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history(response[2],"library","Supplier_Profile","update","../../");
				showResult_multi(' ',' ','2','supplier_list_view');				
				document.getElementById('save_up').value="";
				
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history(response[2],"library","Supplier_Profile","insert","../../");
				showResult_multi(' ',' ','2','supplier_list_view');
				document.getElementById('save_up').value="";
				
			});
		}
		
	//alert(response);
	
	}
}	


//---------------------------Other Party Information-----------------------------------------------------------------------
function fnc_other_party_info(save_perm,edit_perm,delete_perm,approve_perm){
	var txt_mst_id				= escape(document.getElementById('txt_mst_id').value);
	var txt_company_name		= escape(document.getElementById('txt_company_name').value);
	var txt_contact_person		= escape(document.getElementById('txt_contact_person').value);
	var txt_contact_no  		= escape(document.getElementById('txt_contact_no').value);
	var txt_remark  			= escape(document.getElementById('txt_remark').value);
	var txt_web_site			= escape(document.getElementById('txt_web_site').value);
	var txt_supplier_email      = escape(document.getElementById('txt_supplier_email').value);
	var txt_address         	= escape(document.getElementById('txt_address').value);
	var cbo_lib_country_id 		= escape(document.getElementById('cbo_lib_country_id').value);
	var cbo_status				= escape(document.getElementById('cbo_status').value);
	var cbo_individual				= escape(document.getElementById('cbo_individual').value);
	var save_up 			= escape(document.getElementById('save_up').value);
					
					
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (edit_perm==2 && save_up!="") // no edit 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Edit Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
	}
	else if (save_perm==2 && save_up=="")
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have New Entry Permission.').addClass('messageboxerror').fadeTo(900,1);
		}); 
		
	}
	else if (delete_perm==2 && cbo_status==2 ) 
	{
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			 $(this).html('You do not have Delete Permission.').addClass('messageboxerror').fadeTo(900,1);
		});
	}
				
	else if($('#txt_company_name').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#txt_company_name').focus();
			$(this).html('Please Enter Supplier Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	else{					
		nocache = Math.random();
		http.open('get','save_update_common.php?action=other_party_info&isupdate='+save_up+
					'&txt_company_name='+txt_company_name+
					'&txt_contact_person='+txt_contact_person+
					'&txt_contact_no='+txt_contact_no+
					'&txt_web_site='+txt_web_site+
					'&txt_address='+txt_address+
					'&txt_remark='+txt_remark+	
					'&cbo_lib_country_id='+cbo_lib_country_id+					
					'&txt_supplier_email='+txt_supplier_email+
					'&cbo_status='+cbo_status+	
					'&cbo_individual='+cbo_individual+	
					'&nocache ='+nocache);
		http.onreadystatechange = other_party_reply_info;
		http.send(null); 
	}
}

function other_party_reply_info() {
	if(http.readyState == 4){ 
	
		var response = http.responseText.split('_');	
	
		if (response[0]==1)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('There is a Supplier name of this ID, Please use another Supplier name.').addClass('messageboxerror').fadeTo(900,1);				
			});
		}
		else if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history('',"library","Other_Party_Profile","update","../../");
				showResult_multi(' ',' ','other_party_profile','other_party_list_view');				
				document.getElementById('save_up').value="";
				
			});
		}
		else if (response[0]==2)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				document.getElementById('txt_mst_id').value=response[1];
				save_activities_history('',"library","Other_Party_Profile","insert","../../");
				showResult_multi(' ',' ','other_party_profile','other_party_list_view');
				document.getElementById('save_up').value="";
				
			});
		}
		
	//alert(response);
	
	}
}	
