<?php
session_start();
/*######################################

	Completed By
	Name : Md. Fuad Shahriar
	Date :29/04/2012
		
######################################*/

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}


//--------------------------------------------------------------------------------------------------------------------
	include('../../includes/common.php');
	include('../../includes/array_function.php');
	

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Untitled Document</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
	<script src="includes/ajax_submit.js" type="text/javascript"></script>
	<script src="includes/functions.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>	
	
	<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>
    <link href="../../css/popup_window.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../js/popup_window.js"></script>
	<script type="text/javascript" src="../../js/modal.js"></script>
	
		
	
	<script type="text/javascript">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;		
	
		var selected_id = new Array, selected_name = new Array();
		
		function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			//alert(tbl_row_count);
			tbl_row_count = tbl_row_count - 2;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			//alert(x);
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			 getClientData_buyertask(str,3);
		}
		
		
function load_task_drop_down(str)		
{
	var distId=0;
	load_drop_down(distId,str,'task_info_dropdown');
	
}
	function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}	

function openmypage(page_link,title)
		{
			
				
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'search_system_id.php',title, 'width=550px,height=350px,center=1,resize=0,scrolling=0',' ../')
				emailwindow.onclose=function()
				{
					var theform=this.contentDoc.forms[0];//("search_order_frm"); //Access the form inside the modal window
					var system_id=this.contentDoc.getElementById("hid_system_id"); //Access form field with id="emailfield"
					var lead_time=this.contentDoc.getElementById("hid_lead_time"); //Access form field with id="emailfield"
					var material_source=this.contentDoc.getElementById("hid_material_source");
					var task_category=this.contentDoc.getElementById("hid_task_category");
					document.getElementById('system_id').value=system_id.value;
					document.getElementById('lead_time').value=lead_time.value;
					document.getElementById('cbo_material_source').value=material_source.value;
					document.getElementById('cbo_task_category').value=task_category.value;
                    load_task_drop_down(task_category.value);
					showResult(document.getElementById('system_id').value,'3','buyer_task_cont');

				}
			
			
		}
	</script>
	
	
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:950px;">
		<div style="height:70px; width:100%; margin-bottom:10px;">
            <div class="form_caption">
            TNA Template
            </div>
            <span id="permission_caption">
            <? echo "Your Permissions--> \n".$insert.$update.$delete;
                ?>
           </span>
			<div id="messagebox" align="center">&nbsp;</div>
		</div>
		<!-- Start Field Set -->
		<fieldset style="width:950px ">
			<legend>TNA Template</legend>
			<!-- Start Form -->
			<form name="buyer_task" id="buyer_task" method="" autocomplete="off" action="javascript:fnc_buyer_task(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">
				<fieldset>
					<div style="width:950px; overflow:auto" >
						<table width="950">
                        	<tr>
                            	<td width="100">System ID</td>
                                <td width="100"><input type="text" name="system_id" id="system_id" class="text_boxes" style="width:100px;" ondblclick="openmypage('x','System ID Search')" placeholder="Double Click to Search"/></td>
                                
                                <td width="100" align="right">Lead Time</td>
                                <td width="100"><input type="text" name="lead_time" id="lead_time" class="text_boxes_numeric" style="width:105px;" onKeyPress=" return numbersonly(this,event)"/></td>
                                
                                <td width="100" align="right">Material Source</td>
                                <td width="100">
                                	<select name="cbo_material_source" id="cbo_material_source"  class="combo_boxes" style="width:112px;">
                                    <option value="0">Select</option>
									<?php
                                        foreach($material_source as $key=>$value):
                                        ?>
                                        <option value=<? echo "$key";
                                        if ($material_source2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>
									</select>
                                
                                </td>
                                <td width="100" align="right">Task Category</td>
								<td width="160">
									<!--<select name="cbo_task_category" id="cbo_task_category" class="combo_boxes" style="width:160px" 
                                    onChange="load_task_drop_down(this.value); showResult(this.value,'3','buyer_task_cont');">-->
                                    <select name="cbo_task_category" id="cbo_task_category" class="combo_boxes" style="width:160px" 
                                    onChange="load_task_drop_down(this.value)">
                                     <?php
                                        foreach($task_category as $key=>$value):
                                        ?>
                                        <option value=<? echo "$key";
                                        if ($task_category2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>
                                    </select>
								</td>
                            </tr>
                            <tr>                            	
                                <td>Task Name</td>
                                <td colspan="3" id="task_info_dropdown">
                                	<input type="text"  disabled="disabled" name="task_name" id="task_name" class="text_boxes" style="width:330px;"/>
                                </td>
                                <td align="right">Short Name</td>
                                <td>
                                	<input type="text" name="short_name" id="short_name" class="text_boxes" style="width:100px;"/>
                                </td>
                                <td align="right">Deadline</td>
                                <td>
                                	<input type="text" name="deadline" id="deadline" class="text_boxes" style="width:148px;"/>
                                </td>
                            </tr>
                            <tr>
                            	<td>Execution Days</td>
                                <td>
                                	<input type="text" name="execution_days" id="execution_days" class="text_boxes" style="width:100px;"/>
                                </td>
                                
                                <td align="right">Notice Before</td>
                                <td>
                                	<input type="text" name="notice_before" id="notice_before" class="text_boxes" style="width:105px;"/>
                                </td>
                                <td align="right">Sequence No</td>
                                <td>
                                	<input type="text" name="sequence_no" id="sequence_no" class="text_boxes" style="width:100px;"/>
                                </td>
                                <td align="right">Specific Buyer</td>
								<td>
									<select name="cbo_buyer_name" id="cbo_buyer_name" class="combo_boxes" style="width:160px;">
                                    <?
									 if($buyer_cond=="")
									 {
									?>
										<option value="0">All Buyer</option>
									<?
									 }
									$company_sql= mysql_db_query($DB, "select 	buyer_name,id from lib_buyer where is_deleted=0  and status_active=1 $buyer_cond order by buyer_name");
									while ($r_company=mysql_fetch_array($company_sql))
									{
									?>
								<option value="<? echo $r_company["id"]; ?>" <? if($cbo_buyer_name1==$r_company["id"]){?>selected<?php }?>><? echo "$r_company[buyer_name]" ?> </option>
								<? } ?>
									</select>	
								</td>
                            </tr>
							<tr>
								
								
                                <td align="left">Status</td>
                                <td>
                                    <select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:110px;">
                                     <?php
									 $cbo_po_status2=1;
                                        foreach($status_active as $key=>$value):
                                        ?>
                                        <option value=<? echo "$key";
                                        if ($cbo_po_status2==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
                                        <?		
                                        endforeach;
                                        ?>
                                    </select>
                                </td>
                                <td align="right" style="visibility:hidden">Responsible</td>
                                <td style="visibility:hidden"> 
                                	<select name="cbo_responsible" id="cbo_responsible" class="combo_boxes" style="width:110px;">
										<option value="0">Select Responsible Person</option>
									<?
									$responsible_sql= mysql_db_query($DB, "select email_address ,recipient_name,id from mail_user_setup where is_deleted=0  and status_active=1 order by email_address ");
									while ($r_responsible=mysql_fetch_array($responsible_sql))
									{
									?>
								<option value="<? echo $r_responsible["id"]; ?>"><? echo $r_responsible["recipient_name"]." || ".$r_responsible["email_address"]; ?> </option>
								<? } ?>
									</select>
                                </td>
							</tr>
                            <tr>
                                <td colspan="8" align="center">&nbsp;						
                                    <input type="hidden" name="save_up" id="save_up" >
                                    <input type="hidden" name="id_m" id="id_m">
                                    <input type="hidden" name="hid_task_name" id="hid_task_name">		
                                </td>					
                            </tr>
                            <tr>
                                <td colspan="8" align="center" class="button_container">
                                    <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                                    <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
                                </td>				
                            </tr>
                           
						</table>
					</div>
				</fieldset>
				<div style="height:10px;"></div>
				<fieldset>
					<div style="width:900px;" >
						<table cellspacing="1" width="100%">
                            <tr>
                                <td>
                                    <div id="buyer_task_cont">
                                    </div>
                                </td>
                            </tr>		
						</table>
					</div>
				</fieldset>
				
			</form>
			<!-- End Form -->
		</fieldset>
		<!-- End Field Set -->
	</div>
</body>
</html>


