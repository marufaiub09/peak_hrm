<script type="text/javascript" src="includes/functions.js"></script>
<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>

<?php
date_default_timezone_set('UTC');
include('../../includes/common.php');
include('../../includes/array_function.php');

$search_fld=$_GET["search_string2"];
$search_string=$_GET["search_string"];
$type=$_GET["type"];
extract($_GET);
//buyer_name
$sql = "SELECT * FROM lib_buyer WHERE is_deleted=0 and status_active=1 ORDER BY buyer_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$buyer_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$buyer_details[$row['id']] = $row['buyer_name'];	
}


//echo $q;
if ($type==1){   //Buyer
?>

<div>
	<div style="width:600px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
				<thead>
					<th width="50" align="left">SL No</th>
					<th width="130" align="left">Buyer Name</th>
                    <th width="120" align="left">Sub Con. Party</th>
					<th width="100" align="left">Contact Person</th>
					<th width="80" align="center">Contact NO.</th>
					<th align="center">Email</th>
				</thead>
		</table>
	</div>	
	<div style="width:600px; overflow-y:scroll; min-height:50px; max-height:250px;" id="buyer_list_view" align="left">
		<table  cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<?php
			//$search_fld=$_GET["search_string2"];
			//$search_string=$_GET["search_string"];
			//echo "$search_fld";
			$i=1;
			if($search_string!="")
			{
				if ($search_fld==0)
				{
					$sql= mysql_db_query($DB, "select 	* from lib_buyer where buyer_name like '%$search_string%' and is_deleted=0  order by buyer_name");
				}
				else if ($search_fld==1)
				{
					$sql= mysql_db_query($DB, "select * from lib_buyer where subcontract_party='$search_string' and is_deleted=0");
				}
			
			}
			else{
					$sql= mysql_db_query($DB, "select * from lib_buyer where is_deleted=0  order by buyer_name");
				}
			while ($selectResult = mysql_fetch_array($sql)){
				if ($i%2==0)  
                	$bgcolor="#E9F3FF";
                else
                	$bgcolor="#FFFFFF";	
	
		?>
		
			<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onclick="javascript:getDataBuyerMst(<? echo $selectResult[id]; ?>,1)"> 
				<td width="50" align="center"><?php echo "$i"; ?></td>	
				<td width="130">&nbsp;
					<?php 
						$frm="buyer_info"; 
						
						echo split_string($selectResult[buyer_name],13);
												
					?>
				</td>
                <td width="120">&nbsp; <?php
										$frm=$replacement_lc[$selectResult[subcontract_party]];  
										echo $frm;
								 ?>
                </td>
				<td width="100">&nbsp; <?php echo split_string($selectResult[contact_person],10);  ?></td>
				<td width="80">&nbsp; <?php echo  split_string($selectResult[contact_no],11);  ?></td>
				<td>&nbsp;<?php echo  split_string($selectResult[buyer_email],10);   ?></td>				
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
</div>
<?
}
if ($type==2){   //Supplier
?>

	<div style="width:860px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
				<thead>
					<th width="50"><strong>SL No</strong></th>
					<th width="220"><strong>Supplier Name</strong></th>
                    <th width="110" align="left"><strong>Sub Con. Party</strong></th>
					<th width="150"><strong>Contact Person</strong></th>
					<th width="130"><strong>Contact NO.</strong></th>
					<th width="170"><strong>E-mail</strong></th>
                    <th></th>
				</thead>
		</table>
	</div>
	<div style="width:860px;  overflow-y:scroll; min-height:50px; max-height:250px;" id="supplier_list_view" align="left">
		<table  cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<?php
			$i=1;
			if($search_string!="")
			{
				if ($search_fld==0)
				{
					$sql= mysql_db_query($DB, "select * from lib_supplier where supplier_name like '%$search_string%' and is_deleted=0  order by supplier_name");
				}
				else if ($search_fld==1)
				{
					$sql= mysql_db_query($DB, "select * from lib_supplier where subcontract_party='$search_string' and is_deleted=0 order by supplier_name");
				}
			
			}
			else{
					$sql= mysql_db_query($DB, "select * from lib_supplier where is_deleted=0 order by supplier_name");
				}
			while ($selectResult = mysql_fetch_array($sql)){
				if ($i%2==0)  
                	$bgcolor="#E9F3FF";
                else
               	 	$bgcolor="#FFFFFF";	
							
		?>
		
			<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onclick="javascript:getClientData_suppl(<? echo $selectResult[id]; ?>,2)" > 
				<td width="50" align="center"><?php echo "$i"; ?></td>						
				<td width="220">
					<?php 
						$frm="supplier_info"; 
						echo split_string($selectResult[supplier_name],22);
						//echo "<a href='##' style='text-decoration:none' onclick='javascript:getClientData_suppl($selectResult[id],2)'>$selectResult[supplier_name]</a>"; 
					?>
				</td>
                <td width="110"><?php
									$frm=$replacement_lc[$selectResult[subcontract_party]];  
									echo split_string($frm,11); 
								 ?>
                </td>
				<td width="150"><?php echo split_string($selectResult[contact_person],15); ?></td>
				<td width="130"><?php echo split_string($selectResult[contact_no],13); ?></td>
				<td width="169"><?php echo split_string($selectResult[supplier_email],17); ?></td>
                <td></td>			
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
<?
}
if ($type==3){   //Buyer Task
?>
<?php

//$sql = "SELECT * FROM tna_task_details WHERE status_active = 1 ORDER BY id";
$sql = "SELECT * FROM tna_task_details WHERE task_template_id like '$search_string' and status_active = 1 ORDER BY sequence_no";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$task = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$task[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$task[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<div>
	<div style="width:900px;">
        <table cellspacing="0" width="100%" class="rpt_table">
            <thead>
                <th width="50">SL. No</th>
                <th width="100">Seqn. No</th>
                <th width="150">Task Short Name</th>
                <th width="100">Deadline (Days before Ship)</th>
                <th width="100">Execution Days</th>
                <th width="150">Buyer Specific</th>
                <th width="100">Task Category</th>
                <th>Notice Before</th>
                <!--<td><input type="hidden" name="id_field" id="id_field" /></td>-->
            </thead>
          </table>
      </div>
	<div style="width:900px;   overflow-y:scroll; min-height:160px; max-height:260px;" id="search_div" align="left">
        <table cellspacing="0" width="100%" id="tbl_list_search" class="rpt_table">
			<?php
            $i = 1;
            foreach( $task AS $task_var ) {
				if ($i%2==0)  
                	$bgcolor="#E9F3FF";
                else
               	 	$bgcolor="#FFFFFF";	
            ?>
            <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer"  id="search<?php echo $task_var[id]; ?>"  onclick="js_set_value(<?php echo $task_var[id]; ?>)"> 									<?php // $task=$task_var[id]."_". ?> 
                <td width="50" align="center"><?php echo "$i"; ?></td>
                <td width="100" align="center"><?php echo $task_var[sequence_no]; ?></td>
                <td width="150"><?php echo ucwords($task_var[short_name]); ?></td>
                <td width="100"><?php echo  $task_var[deadline] ; ?></td>
                <td width="100"><?php echo  $task_var[execution_days] ; ?></td>
                <td width="150"><?php echo  $buyer_details[$task_var[for_specific]] ; ?></td>
                <td width="100"><?php echo  $task_category[$task_var[task_category]] ; ?></td>
                <td><?php echo  $task_var[notice_before ] ; ?></td>
            </tr>
            <?php
                $i++;
            }
            ?>
        </table>
    </div>
</div>
<?
}
?>
<?
if ($type=='other_party_profile'){   //Supplier
?>

	<div style="width:860px;" align="left">
		<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
				<thead>
					<th width="50"><strong>SL No</strong></th>
					<th width="220"><strong>Other Party Name</strong></th>
					<th width="150"><strong>Contact Person</strong></th>
					<th width="130"><strong>Contact NO.</strong></th>
					<th width="170"><strong>E-mail</strong></th>
                    <th></th>
				</thead>
		</table>
	</div>
	<div style="width:860px;  overflow-y:scroll; min-height:50px; max-height:250px;" id="supplier_list_view" align="left">
		<table  cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<?php
			$i=1;
			if($search_string!="")
			{
				if ($search_fld==0)
				{
					$sql= mysql_db_query($DB, "select * from  lib_other_party where other_party_name  like '%$search_string%' and is_deleted=0  order by other_party_name");
				}
			}
			else{
					$sql= mysql_db_query($DB, "select * from  lib_other_party where is_deleted=0 order by other_party_name");
				}
			while ($selectResult = mysql_fetch_array($sql)){
				if ($i%2==0)  
                	$bgcolor="#E9F3FF";
                else
               	 	$bgcolor="#FFFFFF";	
							
		?>
		
			<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onclick="javascript:getClientData_other_party(<? echo $selectResult[id]; ?>,'other_party_profile')" > 
				<td width="50" align="center"><?php echo "$i"; ?></td>						
				<td width="220">
					<?php 
						echo split_string($selectResult[other_party_name],22);
						//echo "<a href='##' style='text-decoration:none' onclick='javascript:getClientData_suppl($selectResult[id],2)'>$selectResult[supplier_name]</a>"; 
					?>
				</td>
                
				<td width="150"><?php echo split_string($selectResult[contact_person],15); ?></td>
				<td width="130"><?php echo split_string($selectResult[contact_no],13); ?></td>
				<td width="169"><?php echo split_string($selectResult[other_party_email],17); ?></td>
                <td><? echo $status_active[$selectResult[status_active]]?></td>			
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
<?
}
?>
<?
if ($type=='system_id_search')
{ 
if( $system_id=='') $system_id_s="%%"; else $system_id_s="%".$system_id."%";
$sql_system_id="select * from tna_task_template where Id like '$system_id_s' and is_deleted =0 and status_active=1";
$result_system=mysql_query($sql_system_id) or die( $sql_system_id . "<br />" . mysql_error() );
$sql_task_category="select task_category from tna_task_details where task_template_id like '$system_id_s' and is_deleted =0 and status_active=1";
$result_task_category=mysql_query($sql_system_id) or die( $sql_system_id . "<br />" . mysql_error() );
?>
<div>
	<div style="width:500px">
        <table class="rpt_table" cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <th width="90">System ID</th>
                <th width="80">Lead Time</th>
                <th width="150">Material Source</th>
                <th>Task Category</th>
            </thead>
        </table>
    </div>
	<div style="width:500px; overflow:scroll; max-height:210px" id="tna_list" align="left">
        <table class="rpt_table" cellpadding="0" cellspacing="0" width="100%">
		<?
        $i=1;
        while($row_result_system=mysql_fetch_array($result_system))
        {
			if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
				
			$sql_task_category="select task_category from tna_task_details where task_template_id = '$row_result_system[Id]' and is_deleted =0 and status_active=1";
			$result_task_category=mysql_query($sql_task_category) or die( $sql_task_category . "<br />" . mysql_error() );
			$row_task_category=mysql_fetch_array($result_task_category)
        ?>
            <tr bgcolor="<? echo $bgcolor; ?>" onclick="js_set_value(<? echo $row_result_system[Id]; ?>,<? echo $row_result_system[lead_time]; ?>,<? echo $row_result_system[material_source]; ?>,<? echo $row_task_category[task_category]; ?>); parent.emailwindow.hide()" style="text-decoration:none;cursor:pointer">
                <td width="90"><? echo $row_result_system[Id];  ?></td>
                <td width="80"><? echo $row_result_system[lead_time];  ?></td>
                <td width="150"><? echo $material_source[$row_result_system[material_source]];  ?></td>
                <td><? echo $task_category[$row_task_category[task_category]];  ?></td>
            </tr>
        <?
        $i++;
        }
        ?>
        </table>
    </div>
</div>
<?
}// end if ($type=='system_id_search')
?>


<?
function return_field_value($fdata,$tdata,$cdata){

$sql_data="select $fdata from  $tdata where $cdata";
$sql_data_exe=mysql_query($sql_data);
$sql_data_rslt=mysql_fetch_array($sql_data_exe);
$m_data  = $sql_data_rslt[0];

return $m_data ;

}
?>