<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 28-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../includes/common.php');
include('../../includes/array_function.php');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>CompanyInfo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<script src="includes/ajax_submit.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>

<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>

<script type="text/javascript">

	var save_perm = <? echo $permission[0]; ?>;
	var edit_perm = <? echo $permission[1]; ?>;
	var delete_perm = <? echo $permission[2]; ?>;
	var approve_perm = <? echo $permission[3]; ?>;

  
 <?
	$company_sql= mysql_db_query($DB, "select distinct supplier_name from lib_supplier where is_deleted=0 and status_active=1 order by supplier_name");
		while ($r_company=mysql_fetch_array($company_sql)) {
		$str_supplier_name.= '"'.mysql_real_escape_string($r_company['supplier_name']).'",';
	}
?>

 $(function() {	
				var txt_supplier_name = [<? echo substr($str_supplier_name, 0, -1); ?>];
							
				$("#txt_company_name").autocomplete({
					source: txt_supplier_name 
				});
			})
			
function search_populate(str)
{
	 //alert(str);
	if(str==0)
	{
		document.getElementById('search_by_td_up').innerHTML="Enter Supplier Name";
		document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes"	id="txt_search_common"	value="" onkeyup="showResult_multi(this.value,'+document.getElementById('cbo_search_by').value+',\'2\',\'supplier_list_view\')" />';
	}
	else if(str==1)
	{
		var subcontract_party;
		<?php
		foreach($replacement_lc as $key=>$value)
		{
			echo "subcontract_party += '<option value=\"$key\">".$value."</option>';";
		}
		?>
		document.getElementById('search_by_td_up').innerHTML="Select Subcontract Party";
		document.getElementById('search_by_td').innerHTML='<select	name="txt_search_common"	style="width:240px " class="combo_boxes" id="txt_search_common"	onchange="showResult_multi(this.value,'+document.getElementById('cbo_search_by').value+',\'2\',\'supplier_list_view\')" >'+ subcontract_party +'</select>';
	}
}

function fn_reset_form()
		{
			window.location.reload(true);
		}
</script>

<?php 
	//  RETERIVE THE DATA FROM lib_country_info Table
	$selectCountryInfo =" SELECT id AS countryID,country_name AS countryName FROM lib_country_list";
	$selectCountryInfoResult = mysql_query($selectCountryInfo);
?>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px;">	
    <div>
    	<div class="form_caption">
		Other Party Information
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<fieldset style="width:900px;">
		<legend>Other Party Info</legend>
		<form name="other_party_info" id="other_party_info" method="" action="javascript:fnc_other_party_info(save_perm,edit_perm,delete_perm,approve_perm)" onSubmit="">	
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="50%">
					<table  cellpadding="0" cellspacing="2" width="100%">				
						<tr>
							<td width="30%"> Other Party Name </td>
							<td>
								<input type="text" name="txt_company_name" id="txt_company_name" class="text_boxes" style="width:250px" />								
							</td>
						</tr>			
						<tr>
							<td>
								Contact Person
							</td>
							<td>
								<input type="text" name="txt_contact_person" id="txt_contact_person" class="text_boxes" style="width:250px" />						
							</td>
						</tr>				
						<tr>
							<td>
								Contact No
							</td>
							<td>
								<input type="text" name="txt_contact_no" id="txt_contact_no" class="text_boxes" style="width:250px" />						
							</td>
						</tr>
						
						<tr>
							<td>
								http://www.
							</td>
							<td>
								<input type="text" name="txt_web_site" id="txt_web_site" class="text_boxes" style="width:250px" />						
							</td>
						</tr>
						<tr>
							<td>
								Email
							</td>
							<td>
								<input type="text" name="txt_supplier_email" id="txt_supplier_email" class="text_boxes" style="width:250px;" />						
							</td>
						</tr>
						<tr>
							<td>
								Country Name
							</td>
							<td>
								<select name="cbo_lib_country_id" id="cbo_lib_country_id" class="combo_boxes" style="width:262px" >
										<option	 value="" >Select Country Name </option>
										<?
											$company_sql= mysql_db_query($DB, "select country_name,id from lib_list_country where is_deleted=0  and status_active=1 order by country_name");
											while ($r_company=mysql_fetch_array($company_sql))
											{
											?>
											<option value=<? echo $r_company["id"];
											if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[country_name]" ?> </option>
											<?
											}
										?>	
								</select>						
							</td>
                            
						</tr>
						<tr style="display:none">
							<td>
								Delete
							</td>
							<td>
								<select name="cbo_is_deleted" id="cbo_is_deleted" class="combo_boxes" style="width:86px" >
									<option	 value="0" >No</option>
									<option	 value="1" >Yes</option>							
								</select>
							</td>
						</tr>						
					</table>
				</td>
				<td width="50%" valign="top">
					<table  cellpadding="0" cellspacing="2" width="100%">
					<tr>
						<td>
							Address
						</td>
						<td>
							<textarea name="txt_address" id="txt_address" class="text_area" onKeyUp="block_character_text_area()" style="width:250px; "></textarea>
												
						</td>
					</tr>	
					<tr>
						<td>
							Remark</td>
						<td>
						<textarea name="txt_remark" id="txt_remark" class="text_area" onKeyUp="block_character_text_area()" style="width:250px; "></textarea>
											
						</td>
					</tr>
					
					<tr>
						<td>
							Status
						</td>
						<td>
							<select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:86px" > <!--onFocus="javascript:hide_div('div_user_id');"-->
									<?php
										foreach($status_active as $key=>$value):
										?>
										<option value=<? echo "$key";
										if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
										<?		
										endforeach;
										?>						
							</select>	
						</td>
					</tr>
                    <tr>
                    	<td>Individual</td>
                         <td>
                            <select id="cbo_individual" name="cbo_individual" class="combo_boxes" style="width:86px;">
                                <option value="0" selected="selected">No</option>
                                <option value="1">Yes</option>
                            </select>
                         </td>
                    </tr>
					</table>			
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">&nbsp;			
					<input type="hidden" name="save_up" id="save_up" >
					<input type="hidden" name="txt_mst_id" id="txt_mst_id">	
				</td>					
			</tr>
			<tr>
				<td colspan="2" align="center" class="button_container">
					<input type="submit" value="Save" name="save" style="width:80px" id="save"   class="formbutton"/>&nbsp;&nbsp;
					<input type="reset" value="  Refresh  " style="width:80px" name="reset" id="reset" class="formbutton" onClick="fn_reset_form()"/>	
				</td>					
			</tr>				
			</table>
		</form>	
	</fieldset>	
	
	<div style="width:900px; float:left; margin:auto" align="center">
		<fieldset style="width:900px; margin-top:20px">
			<legend>Press space for Other Party Info</legend>
			<form>
            	<table width="600" cellspacing="2" cellpadding="0" border="0">
                    <tr>
                        <td>Search By:&nbsp;&nbsp;
                            <select name="cbo_search_by" id="cbo_search_by" class="combo_boxes" style="width:130px" onChange="search_populate(this.value)">
                                <option value="0">Other Party Name</option>
                            </select>
                        </td>
                        <td id="search_by_td_up">Enter Other Party Name:&nbsp;&nbsp;
                        </td><input type="hidden" name="id_field" id="id_field" value="" />
                             <input type="hidden" name="txt_search_by_id" id="txt_search_by_id" value="" />
                        <td id="search_by_td">
                            <input type="text" style="width:200px" class="text_boxes" placeholder="Press Space For Search" onKeyUp="showResult_multi(this.value,document.getElementById('cbo_search_by').value,'other_party_profile','other_party_list_view')" name="txt_search_common" id="txt_search_common" />
                        </td>
                    </tr>
					<tr>
						<td colspan="3">
							<div style="width:850px; margin-top:10px" id="other_party_list_view" align="left"></div>
						</td>
					</tr>
				</table>
			</form>
		</fieldset>	
	</div>
</div>
</body>
</html>
