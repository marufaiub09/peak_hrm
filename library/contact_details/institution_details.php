<?php

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
date_default_timezone_set('UTC');
include('../../includes/common.php');

// Auto Complete Suggestion for Style List
$company_sql= mysql_db_query($DB, "select country_name,id from lib_list_country where is_deleted=0 and status_active=1 order by country_name");
while ($r_company=mysql_fetch_array($company_sql)) {
	$str_country_list.= '"'.$r_company['country_name'].'",';
}
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK REL="SHORTCUT ICON" HREF="images/logic_logo.png">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
<script type="text/javascript"></script>
<script src="includes/ajax_submit.js" type="text/javascript"></script>
<script src="includes/functions.js" type="text/javascript"></script>
<script type="text/javascript" src="../../resources/jquery-1.6.2.js"></script>
<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript" src="../../resources/jquery_ui/jquery-1.4.4.min.js"></script>
<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js"></script>

<script type="text/javascript">
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	
$(function() {	
				var str_country_list = [<? echo substr($str_country_list, 0, -1); ?>];			
				$("#txt_country_name").autocomplete({
					source: str_country_list 
				});				
			});
			
</script>

<?php 
	//  RETERIVE THE DATA FROM lib_country_info Table
	$selectCountryInfo ="SELECT id AS countryID,country_name	AS countryName FROM lib_country_list ";
	$selectCountryInfoResult = mysql_query($selectCountryInfo);
?>
</head>

<body style="font-family:verdana; font-size:11px;">
<div align="center" style="width:900px;">	
    <div>
    	<div class="form_caption">
		Institution Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<fieldset style="width:600px;">
		<legend>Library Module</legend>
		<form name="buyer_info" id="buyer_info" method="" action="javascript:fnc_buyer_info()">	
			<table cellpadding="0" cellspacing="0" width="100%">
			  <tr>
				<td width="50%">
					<table  cellpadding="0" cellspacing="2" width="100%">				
						
						<tr>
							<td width="140"> Institute Category </td>
							<td width="200">
								<select type="text" name="" id="" class="combo_boxes" style="width:200px" />
								<option></option>
								</select>								
							</td>
						</tr>		
						<tr>
							<td width="140"> Institute Name </td>
							<td width="200">
								<input type="text" name="txt_buyer_name" id="txt_buyer_name" class="text_boxes" style="width:200px" />
								<!--div id="livesearch" style="width:150px"></div-->
							</td>
						</tr>			
						<tr>
							<td>
								Contact Person
							</td>
							<td>
								<input type="text" name="txt_contact_person" id="txt_contact_person" class="text_boxes" style="width:200px" />						
							</td>
						</tr>				
						<tr>
							<td>
								Contact No
							</td>
							<td>
								<input type="text" name="txt_contact_no" id="txt_contact_no" class="text_boxes" style="width:200px" />						
							</td>
						</tr>
						<tr>
							<td>
								http://www.
							</td>
							<td>
								<input type="text" name="txt_web_site" id="txt_web_site" class="text_boxes" style="width:200px" />						
							</td>
						</tr>
						<tr>
							<td>
								Email
							</td>
							<td>
								<input type="text" name="txt_buyer_email" id="txt_buyer_email" class="text_boxes" style="width:200px;" />						
							</td>
						</tr>
						
						<tr style="display:none">
							<td>
								Delete
							</td>
							<td>
								<select name="cbo_is_deleted" id="cbo_is_deleted" class="combo_boxes" style="width:86px" >
									<option	 value="0" >No</option>
									<option	 value="1" >Yes</option>							
								</select>
							</td>
						</tr>						
					</table>
				</td>
				<td width="50%" valign="top">
					<table  cellpadding="0" cellspacing="2" width="100%">	
					<tr>
						<td>
							Address
						</td>
						<td>
							<textarea name="txt_address" id="txt_address" class="text_area" onKeyUp="block_character_text_area()" style="width:250px;"></textarea>
											
						</td>
					</tr>
					
					<tr>
						<td>
							Remark
						</td>
						<td>
						<textarea name="txt_remark" id="txt_remark" class="text_area" onKeyUp="block_character_text_area()" style="width:250px;"></textarea>
											
						</td>
					</tr>
					<tr>
						<td>
							Status
						</td>
						<td>
							<select name="cbo_status" id="cbo_status" class="combo_boxes" style="width:86px" >
									<option	 value="1" >Visible</option>
									<option	 value="2" >Not visible</option>							
							</select>
							<table>
								<tr>
									<td>
										<div>								
										</div>
									</td>
									<td>
									</td>
								</tr>
							</table>					
						</td>
						
					</tr>
					</table>			
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">&nbsp;						
					<input type="hidden" value="" name="save_up" id="save_up" />
				</td>					
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" style="width:100px" value="Save" name="save" id="save" class="formbutton"/>&nbsp;&nbsp;
					<input type="reset" style="width:100px" value="  Refresh  " name="reset" id="reset" class="formbutton"/>&nbsp;&nbsp;
					<input type="hidden"name="save_up_date" id="save_up_date"/>
				</td>					
			</tr>				
			</table>
		</form>	
	</fieldset>	
	
	<div style="width:900px; float:left; margin:auto" align="center">
		<fieldset style="width:600px; margin-top:20px">
			<legend>Enter search words or press space</legend>
			<form>
				<input type="text" size="30" class="text_boxes" onKeyUp="showResult(this.value,'1','livesearch1')" />
				<div style="width:600px; margin-top:10px" id="livesearch1" align="left"></div>
			</form>
		</fieldset>	
	</div>
</div>
</body>

</html>