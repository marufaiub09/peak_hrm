<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../../includes/common.php'); 
include('../../includes/array_function.php');



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/functions.js" type="text/javascript"></script>
	<script src="includes/ajax_submit_cost_center.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;		
		
		$(document).ready(function() {
			
			$('#group').change(function(){
				if( $(this).val() == 0 ) reset_form('group_details_form');
				else getClientData( $(this).val(),1 );
				});		
			});
	
		function reset_form(form) 
		{ 
			// iterate over all of the inputs for the form
			// element that was passed in
			$('#'+form).find(':input').each(function() {
				var type = this.type;
				var tag = this.tagName.toLowerCase(); // normalize case
				// it's ok to reset the value attr of text inputs,
				// password inputs, and textareas
				if (type == 'text' || type == 'password' || tag == 'textarea')
					this.value = "";
				// checkboxes and radios need to have their checked state cleared
				// but should *not* have their 'value' changed
				else if (type == 'checkbox' || type == 'radio')
				this.checked = false;
				// select elements need to have their 'selectedIndex' property set to -1
				// (this works for both single and multiple select elements)
				else if (type == 'select-one')
					this.selectedIndex = 0;
				else if (type == 'hidden')
					this.value = "";
			});
		}
		$(document).ready(function() {
            populate_data();
        });
		function populate_data() {			
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'form=group',
				success: function(data) {
					$('#data_panel').html( data );					
				}
			});
		}
		
		function fn_reset_form()
		{
			window.location.reload(true);
		}
		
//Numeric Value allow field script
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
   		key = window.event.keyCode;
	else if (e)
    	key = e.which;
	else
   		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
    return true;
	
	// numbers
	else if ((("0123456789.,-").indexOf(keychar) > -1))
   		return true;
	else
    	return false;
}

		
	</script>
	<style>
		#group_details_form input[type="text"] { width:265px; }
		#group_details_form select { width:276px; }
		#group_details_form textarea { width:265px; height:51px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:90%; position:relative; margin-bottom:5px; margin-top:5px">		     
    <div>
    	<div class="form_caption">
		Group Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form id="group_details_form" action="javascript:fnc_group_details(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:90%;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						&nbsp;Group:
						<select name="group" id="group" class="combo_boxes" style="margin-left:120px;">
                          <option value="0">-- Select group to edit --</option>
                          <?
							$company_sql= mysql_db_query($DB, "select group_name,id from lib_group where is_deleted=0  and status_active=1 order by group_name");
							while ($r_company=mysql_fetch_array($company_sql))
							{
						  ?>
                          <option value=<? echo $r_company["id"];
							if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[group_name]" ?> </option>
                         <?
							}
						?>
                        </select></td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:90%;">
			<legend>Group Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="30%">Group Name:</td>
								<td>
									<input type="text" name="group_name" id="group_name" class="text_boxes" value="" />
									<img id="new" src="../../resources/images/new.jpg" title="New Group" alt="New" onclick="reset_form()" style="cursor:pointer; visibility:hidden;" />
								</td>
							</tr>
							<tr>
								<td>Contact Person:</td>
								<td><input type="text" name="contact_person" id="contact_person" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Contact Number:</td>
								<td><input type="text" name="contact_no" id="contact_no" class="text_boxes_numeric" value="" onkeypress="return numbersonly(this,event)" /></td>
							</tr>
							<tr>
								<td>Country:</td>
								<td>
									<select name="country_id" id="country_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<?
											$company_sql= mysql_db_query($DB, "select country_name,id from lib_list_country where is_deleted=0  and status_active=1 order by country_name");
											while ($r_company=mysql_fetch_array($company_sql))
											{
										  ?>
										  <option value=<? echo $r_company["id"];
											if ($cbo_company_name1==$r_company["id"]){?> selected <?php }?>><? echo "$r_company[country_name]" ?> </option>
										 <?
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Website:</td>
								<td><input type="text" name="website" id="website" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><input type="email" name="email" id="email" class="text_boxes" value="" width="5000px" /></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="20%">Address:</td>
								<td><textarea name="address" id="address" class="text_area" onKeyUp="block_character_text_area()"></textarea></td>
							</tr>
							<tr>
								<td>Remark:</td>
								<td><textarea name="remark" id="remark" class="text_area" onKeyUp="block_character_text_area()" style="width:400px;"></textarea></td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>
									<select name="status_active" id="status_active" class="combo_boxes">
										<?php
										foreach($status_active as $key=>$value):
										?>
										<option value=<? echo "$key";
										if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
										<?		
										endforeach;
									?>	
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
                <tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" align="center" style="padding-top:10px;" class="button_container" >
						<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" class="formbutton" value="Cancel" onClick="fn_reset_form()" />
					</td>
				</tr>
			</table>
            <fieldset><div style="margin-top:10px;" id="data_panel" align="left"></div></fieldset>
		</fieldset>
	</form>	
	
	</div>
</body>
</html>
