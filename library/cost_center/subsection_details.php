<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');

//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_cost_center.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;	
		
		var section_details		= new Array();
		var subsection_details	= new Array();
		<?php
		foreach( $section_details AS $section_id => $section ) {
			echo "section_details[$section_id] = new Array();\n";
			foreach( $section AS $key => $value ) {
				if( $key == 'id' || $key == 'department_id' ) echo "section_details[$section_id]['$key'] = $value;\n";
				else echo "section_details[$section_id]['$key'] = '$value';\n";
			}
		}
		foreach( $subsection_details AS $subsection_id => $subsection ) {
			echo "subsection_details[$subsection_id] = new Array();\n";
			foreach( $subsection AS $key => $value ) {
				if( $key == 'id' || $key == 'section_id' ) echo "subsection_details[$subsection_id]['$key'] = $value;\n";
				else echo "subsection_details[$subsection_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			populate_data();
			
			$('#subsection').change(function() {
				if( $(this).val() == 0 ) reset_form();
				else show_subsection( $(this).val() );
			});
			
			$('#section_id').change(function() {
				if( $(this).val() == -1 ) alert('new');
			});
		});
		
		function populate_subsection( section_id ) {
			var options = '<option value="0">-- Select subsection to edit --</option>';
			for( var i = 0; i < subsection_details.length; i++ ) {
				if( subsection_details[i] != undefined && ( section_id == 0 || subsection_details[i]['section_id'] == section_id ) ) {
					options += '<option value="' + subsection_details[i]['id'] + '">' + subsection_details[i]['subsection_name'] + '</option>';
				}
			}
			$('#subsection').html( options );
		}
		
		function show_subsection( subsection_id ) {
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'data=subsection&id=' + subsection_id,
				success: function( data ) {
					var subsection_details = unserialize( data );
					
			$('#new').css( "visibility", "visible" );
			$('#section').val( subsection_details['section_id'] );
			populate_subsection( subsection_details['section_id'] );
			$('#subsection').val( subsection_id );
			$('#subsection_name').val( subsection_details['subsection_name'] );
			$('#section_id').val( subsection_details['section_id'] );
			$('#remark').val( subsection_details['remark'] );
			$('#status_active').val( subsection_details['status_active'] );
			}
			});
		}
		
		function reset_form() {
			$('#new').css( "visibility", "hidden" );
			$('#section').val( 0 );
			$('#subsection').val( 0 );
			$('#subsection_name').val( '' );
			$('#section_id').val( 0 );
			$('#remark').val( '' );
			$('#status_active').val( 1 );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'form=subsection',
				success: function(data) {
					$('#data_panel').html( data );					
				}
			});
		}
	</script>
	<style>
		#subsection_details_form input[type="text"] { width:265px; }
		#subsection_details_form select { width:276px; }
		#subsection_details_form textarea { width:265px; height:60px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%;">
    <div>
    	<div class="form_caption">
		Subsection Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form id="subsection_details_form" action="javascript:fnc_subsection_details(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%">&nbsp;Section:</td>
					<td width="35%">
						<select name="section" id="section" class="combo_boxes" style="margin-left:3px;" onchange="populate_subsection( this.value );">
							<option value="0">-- Select section to filter --</option>
							<?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
							<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
					<td width="10%">&nbsp;Subsection:</td>
					<td>
						<select name="subsection" id="subsection" class="combo_boxes" style="margin-left:6px;">
							<option value="0">-- Select subsection to edit --</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97%; margin-top:10px;">
			<legend>Subsection Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="30%">Subsection Name:</td>
								<td>
									<input type="text" name="subsection_name" id="subsection_name" class="text_boxes" value="" />
									<img id="new" src="../../resources/images/new.jpg" title="New Subsection" alt="New" onclick="reset_form()" style="cursor:pointer; visibility:hidden;" />
								</td>
							</tr>
							<tr>
								<td>Section:</td>
								<td>
									<select name="section_id" id="section_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<option value="-1">-- New --</option>
										<?php foreach( $section_details AS $section ) { if( $section['status_active'] == 1 ) { ?>
										<option value="<?php echo $section['id']; ?>"><?php echo $section['section_name']; ?></option>
										<?php } } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>
									<select name="status_active" id="status_active" class="combo_boxes">
										<?php
											foreach($status_active as $key=>$value):
											?>
											<option value=<? echo "$key";
											if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
											<?		
											endforeach;
										?>	
									</select>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="20%">Remark:</td>
								<td><textarea name="remark" id="remark" class="text_area" onKeyUp="block_character_text_area()" style="resize:none;"></textarea></td>
							</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td colspan="2" >&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-top:10px;"  class="button_container">
						<input type="submit" name="save" id="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" id="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<fieldset><div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div></fieldset>
</body>
</html>
<?php mysql_close( $host_connect ); ?>