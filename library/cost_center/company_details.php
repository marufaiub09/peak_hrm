<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');

//group
$sql = "SELECT * FROM lib_group WHERE  is_deleted = 0 ORDER BY group_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$group_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$group_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$group_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//print_r($group_details);
//company
$sql = "SELECT * FROM lib_company WHERE  is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//countries
$sql = "SELECT * FROM lib_list_country WHERE  is_deleted = 0 ORDER BY country_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$countries = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$countries[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$countries[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_cost_center.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../../js/ajaxupload.3.5.js" ></script>
     
	<script type="text/javascript" charset="utf-8">
		
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
	
		var group_details = company_details = new Array();
		<?php
		foreach( $group_details AS $group_id => $group ) {
			echo "group_details[$group_id] = new Array();\n";
			foreach( $group AS $key => $value ) {
				if( $key == 'id' || $key == 'country_id' ) echo "group_details[$group_id]['$key'] = $value;\n";
				else echo "group_details[$group_id]['$key'] = '$value';\n";
			}
		}
		foreach( $company_details AS $company_id => $company ) {
			echo "company_details[$company_id] = new Array();\n";
			foreach( $company AS $key => $value ) {
				if( $key == 'id' || $key == 'country_id' || $key == 'group_id' ) echo "company_details[$company_id]['$key'] = $value;\n";
				else echo "company_details[$company_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			populate_data();
			
			$('#cbo_company').change(function() {
				if( $(this).val() == 0 ) reset_form();
				else show_company( $(this).val() );
			});
			
			$('#group_id').change(function() {
				if( $(this).val() == -1 ) alert('new');
			});
		});
		
		function populate_company( group_id ) {
			if( group_id != 0 ) {
				var options = '<option value="0">-- Select company to edit --</option>';
				for( var i = 0; i < company_details.length; i++ ) {
					if( company_details[i] != undefined && company_details[i]['group_id'] == group_id ) {
						options += '<option value="' + company_details[i]['id'] + '">' + company_details[i]['company_name'] + '</option>';
					}
				}
				$('#cbo_company').html( options );
			}
		}
		
		function show_company( company_id ) {
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'data=company&id=' + company_id,
				success: function( data ) {
					var comapany_data = unserialize( data );
					$('#new').css( "visibility", "visible" );
					$('#group').val( comapany_data['group_id'] );
					populate_company( comapany_data['group_id'] );
					$('#cbo_company').val( company_id );
					$('#group_id').val( comapany_data['group_id'] );
					$('#company_name').val( comapany_data['company_name'] );
					$('#company_short_name').val( comapany_data['company_short_name'] );
					$('#cbo_service_cost_allocation').val( comapany_data['service_cost_allocation'] );
					$('#cbo_posting_pre_year').val( comapany_data['posting_pre_year'] );
					$('#cbo_statutory_account').val( comapany_data['statutory_account'] );
					$('#txt_contact_person').val( comapany_data['contact_person'] );
					$('#txt_cfo').val( comapany_data['cfo'] );
					$('#cbo_company_nature').val( comapany_data['company_nature'] );
					$('#cbo_core_business').val( comapany_data['core_business'] );
					$('#txt_email').val( comapany_data['email'] );
					$('#txt_website').val( comapany_data['website'] );
										
					$('#txt_ac_code_length').val( comapany_data['ac_code_length'] );
					$('#cbo_profit_center_affected').val( comapany_data['profit_center_affected'] );
										
					$('#txt_plot_no').val( comapany_data['plot_no'] );
					$('#txt_level_no').val( comapany_data['level_no'] );
					$('#txt_road_no').val( comapany_data['road_no'] );
					$('#txt_block_no').val( comapany_data['block_no'] );
					$('#country_id').val( comapany_data['country_id'] );
					$('#txt_province').val( comapany_data['province'] );
					$('#txt_city').val( comapany_data['city'] );
					$('#txt_zip_code').val( comapany_data['zip_code'] );
					$('#ph_num').val( comapany_data['phone_number'] );
					$('#txt_trade_license_no').val( comapany_data['trade_license_no'] );
					$('#txt_incorporation_no').val( comapany_data['incorporation_no'] );
					$('#txt_erc_no').val( comapany_data['erc_no'] );
					$('#txt_irc_no').val( comapany_data['irc_no'] );
					$('#txt_tin_number').val( comapany_data['tin_number'] );
					$('#txt_vat_number').val( comapany_data['vat_number'] );
					$('#txt_epb_reg_no').val( comapany_data['epb_reg_no'] );
					$('#txt_trade_license_renewal').val( comapany_data['trade_license_renewal'] );
					$('#txt_erc_expiry_date').val( comapany_data['erc_expiry_date'] );
					$('#txt_irc_expiry_date').val( comapany_data['irc_expiry_date'] );
					$('#txt_bangladesh_bank_reg_number').val( comapany_data['bangladesh_bank_reg_no'] );
					$('#cbo_status_active').val( comapany_data['status_active'] );
					$('#files').html('<a href="##"> <img src="../../'+comapany_data['logo_location']+'" height="75px" width="100px" alt=""  /></a><br />')
					
					
				}
				
			});
		}
		
		function reset_form() {
			$('#new').css( "visibility", "hidden" );
			$('#group').val( 0 );
			$('#company').val( 0 );
			$('#company_name').val( '' );
			$('#group_id').val( 0 );
			$('#contact_person').val( '' );
			$('#contact_no').val( '' );
			$('#website').val( '' );
			$('#address').val( '' );
			$('#email').val( '' );
			$('#country_id').val( 0 );
			$('#remark').val( '' );
			$('#status_active').val( 1 );
			$('#service_cost_allocation').val( '' );
		}
		
		function populate_data() {			
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'form=company',
				success: function(data) {
					$('#data_panel').html( data );					
				}
			});
		}
		
		
		
		function isNumberKey(evt)
       {
		  /*var txt=document.getElementById('txt_ac_code_length').value;// 
		   if(txt.length>2)
		   {
			  alert('Length Maximum 3 Digit');
			 //  alert(txt.length);
			 return false;
		   }*/
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;
          return true;
       }

var del_file;
var master_tble_id;
	$(function(){
		var btnUpload=$('#upload');
		var status=$('#status');
		
		new AjaxUpload(btnUpload, {
			
			action: 'includes/upload_file.php?category=company&img_size=120',
			name: 'uploadfile',
			onSubmit: function(file, ext)
			{
				 
				if (document.getElementById('cbo_company').value==0)
				{
					alert('No Company Selected, please select a Company');
					return false;
				}
				 if (! (ext && /^(jpg|png|jpeg|bmp)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('Only JPG, PNG or bmp files are allowed');
					return false;
				}
				status.html('<img src="../../images/loading.gif" />');
				 
			},
			onComplete: function(file, response){
				alert(response);
				//alert(response);
				response=response.split("*");
				
				 
				status.text('');
				//var dt=imgno[0]+"_"+ imgno[1];
				//var del_file="<a href='##' onclick=\"remove_this_image('"+ response[0] + "',1)\">Image-"+response[0]+"</a>";
				//Add uploaded file to list
				
				$('#files').html('<a href="##" onckick="openmypage("'+response[1] +'","Image Info"); return false"> <img src="../../'+response[1]+'" height="75px" width="100px" alt=""  /></a><br />'+del_file).addClass('success');
			}
		});
	});

function remove_this_image(str,stype)
{
	 var conf=confirm("Do you really Want to Delete the Image");
	
	//callAlert('Can you get there from here?','This is a Title')
	
	if (conf==1)
	{
		ajax.requestFile = 'includes/delete_image.php?img_id='+str ;	 
		if (stype==1) ajax.onCompletion = showmsg_top;	
		else ajax.onCompletion = showmsg_bot;
		ajax.runAJAX();
	}
}
	</script>
	<style>
		#company_details_form input[type="text"] { width:265px; }
		#company_details_form select { width:276px; }
		#company_details_form textarea { width:265px; height:40px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
		
		 
#upload{
		 padding:5px;
		font-weight:bold; font-size:1.3em;
		font-family:Arial, Helvetica, sans-serif;
		text-align:center;
		background:#999999;
		color:#ffffff;
		border:1px solid #999999;
		width:100px;
		cursor:pointer !important;
		-moz-border-radius:5px; -webkit-border-radius:5px;
	}
	#upload_btm{
		 padding:5px;
		font-weight:bold; font-size:1.3em;
		font-family:Arial, Helvetica, sans-serif;
		text-align:center;
		background:#999999;
		color:#ffffff;
		border:1px solid #999999;
		width:100px;
		cursor:pointer !important;
		-moz-border-radius:5px; -webkit-border-radius:5px;
	}
	.darkbg{
		background:#ddd !important;
	}
	#status{
		font-family:Arial; padding:5px;
	}
	#files{ list-style:none;  }
	#files li{margin-top:15px;  }

	.success{ border:1px solid #CCCCCC;color:#660000; float:left }
	#files_btm{ list-style:none;  }
	#files_btm li{margin-top:15px;  }

	.success_btm{ border:1px solid #CCCCCC;color:#660000; float:left }
	.error{ background:#FFFFFF; border:1px solid #CCCCCC; }
</style>
 
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%; position:relative; height:70px; margin-bottom:5px; margin-top:5px">        
     <div>
    	<div class="form_caption">
		Company Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form id="company_details_form" action="javascript:fnc_company_details(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="14%">&nbsp;Group:</td>
					<td width="35%">
						&nbsp;<select name="group" id="group" class="combo_boxes" style="margin-left:3px;" onchange="populate_company( this.value );">
							<option value="0">-- Select group to filter --</option>
							<?php foreach( $group_details AS $group ) { if( $group['status_active'] == 1 ) { ?>
							<option value="<?php echo $group['id']; ?>"><?php echo $group['group_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
					<td width="10%">&nbsp;Company:</td>
					<td>
						<select name="cbo_company" id="cbo_company" class="combo_boxes" style="margin-left:6px;">
							<option value="0">-- Select company to edit --</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97% ">
			<legend>General Information</legend>
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="130" >Group Name</td>
					<td width="170">
						<select name="group_id" id="group_id" class="combo_boxes" style="width:155px">
								<option value="0">-- Select --</option>
								<option value="-1">-- New --</option>
								<?php foreach( $group_details AS $group ) { if( $group['status_active'] == 1 ) { ?>
								<option value="<?php echo $group['id']; ?>"><?php echo $group['group_name']; ?></option>
								<?php } } ?>
						</select>	
                        	
				
					</td>
					<td width="130" >
						Company Name					
					</td>
					<td width="170"><input type="text" name="company_name" id="company_name" class="text_boxes" value=""  style="width:145px "/></td>
					<td width="130" >
						Company Short Name					
					</td>
					<td width="170"><input type="text" name="company_short_name" id="company_short_name" class="text_boxes" value=""  style="width:145px "/></td>
				</tr>
				<tr>
					<td width="130">Service Cost Allocation	</td>
					<td width="170">
						<select name="cbo_service_cost_allocation" id="cbo_service_cost_allocation" class="combo_boxes"style="width:155px" >
							<?php
								foreach($fancy_item as $key=>$value):
								?>
								<option value=<? echo "$key";
								if ($cbo_service_cost_allocation==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
								<?		
								endforeach;
								?>
						 </select>					
					</td>
					<td width="130">Posting in Previous Yr</td>
					<td width="170">
						<select name="cbo_posting_pre_year" id="cbo_posting_pre_year" class="combo_boxes" style="width:155px">
							<?php foreach($fancy_item as $key=>$value): ?>
							<option value=<? echo "$key"; if ($cbo_posting_pre_year==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<? endforeach; ?>
						 </select>					
					</td>
					<td width="130">Statutory Account</td>
					<td width="170">
						<select name="cbo_statutory_account" id="cbo_statutory_account" class="combo_boxes"style="width:155px" >
							<?php foreach($fancy_item as $key=>$value): ?>
							<option value=<? echo "$key"; if ($cbo_statutory_account==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<? endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="130">CEO	</td>
					<td width="170">
						<input type="text" name="txt_contact_person" id="txt_contact_person" class="text_boxes" value="" style="width:145px " />					
					</td>
					<td width="130">CFO</td>
					<td width="170">
						<input name="txt_cfo" id="txt_cfo" class="text_boxes" style="width:145px" />
					</td>
					<td width="130">Company Nature</td>
					<td width="170">
						  <select name="cbo_company_nature" id="cbo_company_nature" class="combo_boxes" style="width:155px">
							<?php
								foreach($company_nature as $key=>$value):
								?>
								<option value=<? echo "$key";
								if ($cbo_company_nature1==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
								<?		
								endforeach;
								?>
						  </select>					
					</td>
				</tr>
				<tr>
					<td width="130">Core Business</td>
					<td width="170">
						<select name="cbo_core_business" id="cbo_core_business" class="combo_boxes" style="width:155px">
							<?php
								foreach($core_business as $key=>$value):
								?>
								<option value=<? echo "$key";
								if ($cbo_core_business1==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
								<?		
								endforeach;
								?>
						 </select>					
					</td>
					<td width="130">E-mail</td>
					<td width="170" > 
						<input type="email" name="txt_email" id="txt_email" class="text_boxes" value="" style="width:145px " />					
					</td>
					<td width="130">Website</td>
					<td width="170">
						<input type="text" name="txt_website" id="txt_website" class="text_boxes" value="" style="width:145px " />					
					</td>
				</tr>
                <tr>
                	<td width="130">A/C Code Length</td>
					<td width="170">
						<input type="text" onkeypress="return isNumberKey(event)"  name="txt_ac_code_length" id="txt_ac_code_length" class="text_boxes" value="" style="width:145px " />					
					</td>
                    <td width="130">Profit Center Affected</td>
					<td width="170">
                    	<select id="cbo_profit_center_affected" name="cbo_profit_center_affected" class="combo_boxes" style="width:155px;">
                        	<option value="0" selected="selected">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </td>
                </tr>
			</table>
		</fieldset>
		<fieldset style="width:97% ">
			<legend>Address</legend>
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="130" >Plot No</td>
					<td width="170">
						<input type="text" name="txt_plot_no" id="txt_plot_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
					<td width="130">Level No</td>
					<td width="170">
						<input type="text" name="txt_level_no" id="txt_level_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
					<td width="130">Road No</td>
					<td width="170">
						<input type="text" name="txt_road_no" id="txt_road_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
				</tr>
				<tr>
					<td width="130" height="10">Block No</td>
					<td width="170">
						<input type="text" name="txt_block_no" id="txt_block_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
					<td width="130">Country</td>
					<td width="170">
						 <select name="country_id" id="country_id" class="combo_boxes" style="width:155px ">
							<option value="0">-- Select --</option>
							<?php foreach( $countries AS $country ) { ?>
							<option value="<?php echo $country['id']; ?>"><?php echo $country['country_name']; ?></option>
							<?php } ?>
						</select>
					</td>
					<td width="130">Province</td>
					<td width="170">
						<input name="txt_province" id="txt_province" class="text_boxes" style="width:145px">
					</td>
				  </tr>
				<tr>
					<td width="130" height="10">City / Town</td>
					<td width="170">
						<input name="txt_city" id="txt_city" class="text_boxes" style="width:145px">
					</td>
					<td width="130">Zip Code</td>
					<td width="170">
						<input name="txt_zip_code" id="txt_zip_code" class="text_boxes" style="width:145px">
					</td>
                    <td width="130">Phone Number</td>
					<td width="170">
						<input name="ph_num" id="ph_num" class="text_boxes" style="width:145px">
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97% ">
			<legend>Legal Document</legend>
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="130" >Trade License No</td>
					<td width="170">
						<input type="text" name="txt_trade_license_no" id="txt_trade_license_no" class="text_boxes" value=""  style="width:145px "/>
					 </td>
					<td width="130">
						Incorporation No
					</td>
					<td width="170">
						<input type="text" name="txt_incorporation_no" id="txt_incorporation_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
					<td width="130">ERC No</td>
					<td width="170">
						<input type="text" name="txt_erc_no" id="txt_erc_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
				</tr>
				<tr>
					<td width="130" height="10">IRC No</td>
					<td width="170">
						<input type="text" name="txt_irc_no" id="txt_irc_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
					<td width="130">EPB Reg. No</td>
					<td width="170">
						<input type="text" name="txt_epb_reg_no" id="txt_epb_reg_no" class="text_boxes" value=""  style="width:145px "/>
					</td>
					<td width="130">
						Trade License Renewal
					</td>
					<td width="170">
						<input type="text" name="txt_trade_license_renewal" id="txt_trade_license_renewal" class="text_boxes" value=""  style="width:145px "/>
						<script type="text/javascript">
							$( "#txt_trade_license_renewal" ).datepicker({
								dateFormat: 'yy-mm-dd',
								changeMonth: true,
								changeYear: true
							});
						</script>
					</td>
				  </tr>
				<tr>
					<td width="130" height="10">ERC Expiry Date</td>
					<td width="170">
						 <input type="text" name="txt_erc_expiry_date" id="txt_erc_expiry_date" class="text_boxes" value=""  style="width:145px "/>
						 <script type="text/javascript">
							$( "#txt_erc_expiry_date" ).datepicker({
								dateFormat: 'yy-mm-dd',
								changeMonth: true,
								changeYear: true
							});
						</script>
					</td>
					<td width="130">IRC Expiry Date</td>
					<td width="170">
						<input type="text" name="txt_irc_expiry_date" id="txt_irc_expiry_date" class="text_boxes" value=""  style="width:145px "/>
						<script type="text/javascript">
							$( "#txt_irc_expiry_date" ).datepicker({
										dateFormat: 'yy-mm-dd',
										changeMonth: true,
										changeYear: true
									});
						</script>
					</td>
					<td width="130">TIN Number</td>
					<td width="170">
						<input type="text" name="txt_tin_number" id="txt_tin_number" class="text_boxes" value=""  style="width:145px "/>
						
					</td>
				</tr>
				<tr>
					<td width="130" height="10">VAT Number</td>
					<td width="170">
						 <input type="text" name="txt_vat_number" id="txt_vat_number" class="text_boxes" value=""  style="width:145px "/>
					</td>
					<td width="130" height="10">Bangladesh Bank Registration No</td>
					<td width="170">
						 <input type="text" name="txt_bangladesh_bank_reg_number" id="txt_bangladesh_bank_reg_number" class="text_boxes" value=""  style="width:145px "/>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97%">
			<legend>Status</legend>
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="130" >Status</td>
					<td width="170">
					<select name="cbo_status_active" id="cbo_status_active" class="combo_boxes" style="width:155px ">
						<?php
								foreach($status_active as $key=>$value):
								?>
								<option value=<? echo "$key";
								if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
								<?		
								endforeach;
							?>	
					</select>
					</td>
					<td width="130" >&nbsp;</td>
					<td width="170">
                    <div style="padding-top:5px"> <div id="upload" onclick="call_image()"><span>Select Logo</span> </div></div>
												  <div style="width:100px; padding-top:5px" align="center">
														
												  </div>
                    </td>
					<td width="130" >&nbsp;</td>
					<td width="170"> <div id="files" title="Click to Remove" style="width:245px;-moz-border-radius: .7em; border:1px solid; height:120px; background-color:" align="center">
									
									</div><div id="status" align="center"></div>	 </td>
				</tr>
                <tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="6" align="center" style="padding-top:10px;" class="button_container">
						<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<fieldset><div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div></fieldset>
    </div>
</body>
</html>
<?php mysql_close( $host_connect ); ?>