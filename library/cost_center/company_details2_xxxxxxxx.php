<?php
include('../../includes/common.php');
include('../../includes/array_function.php');

if( file_exists( "../../cache/group_details.txt" ) ) {
	$group_details = unserialize( file_get_contents( "../../cache/group_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_group WHERE is_deleted = 0 ORDER BY group_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$group_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$group_details[$row['id']] = $row;
	}
	file_put_contents( "../../cache/group_details.txt", serialize( $group_details ) );
}
if( file_exists( "../../cache/company_details.txt" ) ) {
	$company_details = unserialize( file_get_contents( "../../cache/company_details.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$company_details = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$company_details[$row['id']] = $row;
	}
	file_put_contents( "../../cache/company_details.txt", serialize( $company_details ) );
}
if( file_exists( "../../cache/countries.txt" ) ) {
	$countries = unserialize( file_get_contents( "../../cache/countries.txt" ) );
}
else {
	$sql = "SELECT * FROM lib_list_country WHERE is_deleted = 0 ORDER BY country_name ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	
	$countries = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$countries[$row['id']] = $row;
	}
	file_put_contents( "../../cache/countries.txt", serialize( $countries ) );
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_cost_center.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.1.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		var group_details	= new Array();
		var company_details	= new Array();
		var countries		= new Array();
		<?php
		foreach( $group_details AS $group_id => $group ) {
			echo "group_details[$group_id] = new Array();\n";
			foreach( $group AS $key => $value ) {
				if( $key == 'id' || $key == 'country_id' ) echo "group_details[$group_id]['$key'] = $value;\n";
				else echo "group_details[$group_id]['$key'] = '$value';\n";
			}
		}
		foreach( $company_details AS $company_id => $company ) {
			echo "company_details[$company_id] = new Array();\n";
			foreach( $company AS $key => $value ) {
				if( $key == 'id' || $key == 'country_id' || $key == 'group_id' ) echo "company_details[$company_id]['$key'] = $value;\n";
				else echo "company_details[$company_id]['$key'] = '$value';\n";
			}
		}
		foreach( $countries AS $country_id => $country ) {
			echo "countries[$country_id] = new Array();\n";
			foreach( $country AS $key => $value ) {
				if( $key == 'id' ) echo "countries[$country_id]['$key'] = $value;\n";
				else echo "countries[$country_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			populate_data();
			
			$('#company').change(function() {
				if( $(this).val() == 0 ) reset_form();
				else show_company( $(this).val() );
			});
			
			$('#group_id').change(function() {
				if( $(this).val() == -1 ) alert('new');
			});
		});
		
		function populate_company( group_id ) {
			var options = '<option value="0">-- Select company to edit --</option>';
			for( var i = 0; i < company_details.length; i++ ) {
				if( company_details[i] != undefined && company_details[i]['group_id'] == group_id ) {
					options += '<option value="' + company_details[i]['id'] + '">' + company_details[i]['company_name'] + '</option>';
				}
			}
			$('#cbo_company').html( options );
		}
		
		function show_company( company_id ) {
			$('#new').css( "visibility", "visible" );
			$('#group').val( company_details[company_id]['group_id'] );
			populate_company( company_details[company_id]['group_id'] );
			$('#cbo_company').val( company_id );
			$('#group_id').val( company_details[company_id]['group_id'] );
			$('#company_name').val( company_details[company_id]['company_name'] );
			$('#cbo_service_cost_allocation').val( company_details[company_id]['service_cost_allocation'] );
			$('#cbo_posting_pre_year').val( company_details[company_id]['posting_pre_year'] );
			$('#cbo_statutory_account').val( company_details[company_id]['statutory_account'] );
			$('#txt_contract_person').val( company_details[company_id]['contract_person'] );
			$('#txt_cfo').val( company_details[company_id]['cfo'] );
			$('#cbo_company_nature').val( company_details[company_id]['company_nature'] );
			$('#cbo_core_business').val( company_details[company_id]['core_business'] );
			$('#txt_email').val( company_details[company_id]['email'] );
			$('#txt_website').val( company_details[company_id]['website'] );
			$('#txt_plot_no').val( company_details[company_id]['plot_no'] );
			$('#txt_level_no').val( company_details[company_id]['level_no'] );
			$('#txt_road_no').val( company_details[company_id]['road_no'] );
			$('#txt_block_no').val( company_details[company_id]['block_no'] );
			$('#country_id').val( company_details[company_id]['country_id'] );
			$('#txt_province').val( company_details[company_id]['province'] );
			$('#txt_city').val( company_details[company_id]['city'] );
			$('#txt_zip_code').val( company_details[company_id]['zip_code'] );
			$('#txt_trade_license_no').val( company_details[company_id]['trade_license_no'] );
			$('#txt_incorporation_no').val( company_details[company_id]['incorporation_no'] );
			$('#txt_erc_no').val( company_details[company_id]['erc_no'] );
			$('#txt_irc_no').val( company_details[company_id]['irc_no'] );
			$('#txt_tin_number').val( company_details[company_id]['tin_number'] );
			$('#txt_vat_number').val( company_details[company_id]['vat_number'] );
			$('#txt_epb_reg_no').val( company_details[company_id]['epb_reg_no'] );
			$('#txt_trade_license_renewal').val( company_details[company_id]['trade_license_renewal'] );
			$('#txt_erc_expiry_date').val( company_details[company_id]['erc_expiry_date'] );
			$('#txt_irc_expiry_date').val( company_details[company_id]['irc_expiry_date'] );
			$('#cbo_status_active').val( company_details[company_id]['cbo_status_active'] );
		}
		
		function reset_form() {
			$('#new').css( "visibility", "hidden" );
			$('#group').val( 0 );
			$('#company').val( 0 );
			$('#company_name').val( '' );
			$('#group_id').val( 0 );
			$('#contact_person').val( '' );
			$('#contact_no').val( '' );
			$('#website').val( '' );
			$('#address').val( '' );
			$('#email').val( '' );
			$('#country_id').val( 0 );
			$('#remark').val( '' );
			$('#status_active').val( 1 );
			$('#service_cost_allocation').val( '' );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'form=company',
				success: function(data) {
					$('#data_panel').html( data );
					$('#companies').dataTable({
						"bRetrieve": true,
						"bDestroy": true,
						"bJQueryUI": true,
						"sPaginationType": "full_numbers",
						"aoColumns": [ { "sType": "html" }, null, null, null, null, { "bSortable": false } ],
						"aaSorting": [[ 0, "asc" ]],
						"oLanguage": { "sSearch": "Search all columns:" },
						"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]]
					});
				}
			});
		}
	</script>
	<style>
		#company_details_form input[type="text"] { width:265px; }
		#company_details_form select { width:276px; }
		#company_details_form textarea { width:265px; height:40px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%; position:relative; height:40px; margin-bottom:5px; margin-top:5px">
		<h2>Company Details</h2>
		<div align="center" id="messagebox"></div>	
	</div>
	<form id="company_details_form" action="javascript:fnc_company_details()" method="POST">
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="14%">&nbsp;Group:</td>
					<td width="35%">
						&nbsp;<select name="group" id="group" class="combo_boxes" style="margin-left:3px;" onchange="populate_company( this.value );">
							<option value="0">-- Select group to filter --</option>
							<?php foreach( $group_details AS $group ) { if( $group['status_active'] == 1 ) { ?>
							<option value="<?php echo $group['id']; ?>"><?php echo $group['group_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
					<td width="10%">&nbsp;Company:</td>
					<td>
						<select name="cbo_company" id="cbo_company" class="combo_boxes" style="margin-left:6px;">
							<option value="0">-- Select company to edit --</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		
		<fieldset style="width:97% ">
		<legend>General Information</legend>
		<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="130" >Group Name</td>
				  <td width="170"> <!-- in Libary Contract Details -- Buyer -->
					<select name="group_id" id="group_id" class="combo_boxes" style="width:155px">
							<option value="0">-- Select --</option>
							<option value="-1">-- New --</option>
							<?php foreach( $group_details AS $group ) { if( $group['status_active'] == 1 ) { ?>
							<option value="<?php echo $group['id']; ?>"><?php echo $group['group_name']; ?></option>
							<?php } } ?>
					</select>					
				</td>
				<td width="130" >
					Company Name					
				</td>
				<td width="170"> <!-- in Libary Contract Details -- Buyer -->
				  <input type="text" name="company_name" id="company_name" class="text_boxes" value=""  style="width:145px "/></td>
				<td width="130">Service Cost Allocation	</td>
				<td width="170"> <!-- Entry but pop up & filter existing data-->
					<select name="cbo_service_cost_allocation" id="cbo_service_cost_allocation" class="combo_boxes"style="width:155px" >
					 	<?php
							foreach($fancy_item as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($cbo_service_cost_allocation==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
                     </select>					
				</td>
			</tr>
			<tr>
				<td width="130">Posting in Previous Yr</td>
				<td width="170"> <!-- Defult will be NO -->
					<select name="cbo_posting_pre_year" id="cbo_posting_pre_year" class="combo_boxes" style="width:155px">
						<?php
							foreach($fancy_item as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($cbo_posting_pre_year==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
                     </select>					
				</td>
				<td width="130">Statutory Account</td>
				<td width="170"> <!-- in Libary Bank info -- only lien bank list will be here -->
					<select name="cbo_statutory_account" id="cbo_statutory_account" class="combo_boxes"style="width:155px" >
						<?php
							foreach($fancy_item as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($cbo_statutory_account==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
                    </select>					
				</td>
				<td width="130">CEO	</td>
				<td width="170"> <!-- Calander-->
					<input type="text" name="txt_contract_person" id="txt_contract_person" class="text_boxes" value="" style="width:145px " />					
				</td>
			</tr>
			<tr>
				<td width="130">CFO</td>
				<td width="170"> <!-- Entry Field -->
					<input name="txt_cfo" id="txt_cfo" class="text_boxes" style="width:145px" />
				</td>
				<td width="130">Company Nature</td>
				<td width="170"> <!-- in Libary Currency -- USD will be  By Defult -->
					  <select name="cbo_company_nature" id="cbo_company_nature" class="combo_boxes" style="width:155px">
					  	<?php
							foreach($company_nature as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($cbo_company_nature1==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
					  </select>					
				</td>
				<td width="130">Core Business</td>
				<td width="170"> <!-- Entry Field-->
					<select name="cbo_core_business" id="cbo_core_business" class="combo_boxes" style="width:155px">
						<?php
							foreach($core_business as $key=>$value):
							?>
							<option value=<? echo "$key";
							if ($cbo_core_business1==$key){?> selected <? } ?>> <? echo "$value" ; ?> </option>
							<?		
							endforeach;
							?>
                     </select>					
				</td>
		  	</tr>
			<tr>
				<td width="130">E-mail</td>
				<td width="170" > 
					<input type="text" name="txt_email" id="txt_email" class="text_boxes" value="" style="width:145px " />					
				</td>
				<td width="130">Website</td>
				<td width="170"> <!-- Entry Field-->
					<input type="text" name="txt_website" id="txt_website" class="text_boxes" value="" style="width:145px " />					
				</td>
			</tr>
		</table>
		</fieldset>
		
		<fieldset style="width:97% ">
		<legend>Address</legend>
		<table cellpadding="0" cellspacing="1" width="100%">
			<tr>
				<td width="130" >Plot No</td>
				<td width="170"> <!-- in Libary Contract Details -- Buyer -->
					<input type="text" name="txt_plot_no" id="txt_plot_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
				<td width="130">Level No</td>
				<td width="170"> <!-- Entry but pop up & filter existing data-->
					<input type="text" name="txt_level_no" id="txt_level_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
				<td width="130">Road No</td>
				<td width="170"> <!-- Defult will be NO -->
					<input type="text" name="txt_road_no" id="txt_road_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
			</tr>
			<tr>
				<td width="130" height="10">Block No</td>
				<td width="170"> <!-- in Libary Bank info -- only lien bank list will be here -->
					<input type="text" name="txt_block_no" id="txt_block_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
				<td width="130">Country</td>
				<td width="170"> <!-- Calander-->
					 <select name="country_id" id="country_id" class="combo_boxes" style="width:155px ">
						<option value="0">-- Select --</option>
						<?php foreach( $countries AS $country ) { ?>
						<option value="<?php echo $country['id']; ?>"><?php echo $country['country_name']; ?></option>
						<?php } ?>
					</select>
				</td>
				<td width="130">Province</td>
				<td width="170"> <!-- Entry Field -->
					<input name="txt_province" id="txt_province" class="text_boxes" style="width:145px">
				</td>
			  </tr>
			  <tr>
				<td width="130" height="10">City / Town</td>
				<td width="170"> <!-- in Libary Currency -- USD will be  By Defult -->
				 	<input name="txt_city" id="txt_city" class="text_boxes" style="width:145px">
				</td>
				<td width="130">Zip Code</td>
				<td width="170"> <!-- Entry Field-->
					<input name="txt_zip_code" id="txt_zip_code" class="text_boxes" style="width:145px">
				</td>
			</tr>
		</table>
		</fieldset>
		<fieldset style="width:97% ">
		<legend>Legal Document</legend>
		<table cellpadding="0" cellspacing="1" width="100%">
			<tr>
				<td width="130" >Trade License No</td>
				<td width="170"> <!-- in Libary Contract Details -- Buyer -->
					<input type="text" name="txt_trade_license_no" id="txt_trade_license_no" class="text_boxes" value=""  style="width:145px "/>
				 </td>
				<td width="130">
					Incorporation No
				</td>
				<td width="170"> <!-- Entry but pop up & filter existing data-->
					<input type="text" name="txt_incorporation_no" id="txt_incorporation_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
				<td width="130">ERC No</td>
				<td width="170"> <!-- Defult will be NO -->
					<input type="text" name="txt_erc_no" id="txt_erc_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
			</tr>
			<tr>
				<td width="130" height="10">IRC No</td>
				<td width="170"> <!-- in Libary Bank info -- only lien bank list will be here -->
					<input type="text" name="txt_irc_no" id="txt_irc_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
				<td width="130">EPB Reg. No</td>
				<td width="170"> <!-- Calander-->
					<input type="text" name="txt_epb_reg_no" id="txt_epb_reg_no" class="text_boxes" value=""  style="width:145px "/>
				</td>
				<td width="130">
					Trade License Renewal
				</td>
				<td width="170"> <!-- Entry Field -->
					<input type="text" name="txt_trade_license_renewal" id="txt_trade_license_renewal" class="text_boxes" value=""  style="width:145px "/>
					<script type="text/javascript">
						$( "#txt_trade_license_renewal" ).datepicker({
									dateFormat: 'yy-mm-dd',
									changeMonth: true,
									changeYear: true
								});
					//	alert(document.getElementById('lastname').value)
					</script>
				</td>
			  </tr>
			  <tr>
				<td width="130" height="10">ERC Expiry Date</td>
				<td width="170"> <!-- in Libary Currency -- USD will be  By Defult -->
					 <input type="text" name="txt_erc_expiry_date" id="txt_erc_expiry_date" class="text_boxes" value=""  style="width:145px "/>
					 <script type="text/javascript">
						$( "#txt_erc_expiry_date" ).datepicker({
									dateFormat: 'yy-mm-dd',
									changeMonth: true,
									changeYear: true
								});
					//	alert(document.getElementById('lastname').value)
					</script>
				</td>
				<td width="130">IRC Expiry Date</td>
				<td width="170"> <!-- Entry Field-->
					<input type="text" name="txt_irc_expiry_date" id="txt_irc_expiry_date" class="text_boxes" value=""  style="width:145px "/>
					<script type="text/javascript">
						$( "#txt_irc_expiry_date" ).datepicker({
									dateFormat: 'yy-mm-dd',
									changeMonth: true,
									changeYear: true
								});
					//	alert(document.getElementById('lastname').value)
					</script>
				</td>
				<td width="130">TIN Number</td>
				<td width="170"> <!-- Entry Field-->
					<input type="text" name="txt_tin_number" id="txt_tin_number" class="text_boxes" value=""  style="width:145px "/>
					
				</td>
			</tr>
		
		<tr>
				<td width="130" height="10">VAT Number</td>
				<td width="170"> <!-- in Libary Currency -- USD will be  By Defult -->
					 <input type="text" name="txt_vat_number" id="txt_vat_number" class="text_boxes" value=""  style="width:145px "/>
					 
				</td>
				
				
			</tr>
		
		</table>
		</fieldset>
		
		<fieldset style="width:97% ">
		<legend>Status</legend>
		<table cellpadding="0" cellspacing="1" width="100%">
			<tr>
				<td width="130" >Status</td>
				<td width="170"> <!-- in Libary Contract Details -- Buyer -->
				<select name="cbo_status_active" id="cbo_status_active" class="combo_boxes" style="width:155px ">
					<option value="0">Inactive</option>
					<option value="1" selected>Active</option>
				</select>
				</td>
				<td width="130" >&nbsp;</td>
				<td width="170">&nbsp; </td>
				<td width="130" >&nbsp;</td>
				<td width="170">&nbsp; </td>
			</tr>
			<tr>
				<td colspan="6" align="center" style="padding-top:10px;">
					<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
					<input type="button" name="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
				</td>
			</tr>
		</table>
		</fieldset>
		
		<!--<fieldset style="width:97%">
			
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="30%">Company Name:</td>
								<td>
									<input type="text" name="company_name" id="company_name" class="text_boxes" value="" />
									<img id="new" src="../../resources/images/new.jpg" title="New Company" alt="New" onclick="reset_form()" style="cursor:pointer; visibility:hidden;" />
								</td>
							</tr>
							<tr>
								<td>Contact Person:</td>
								<td><input type="text" name="contact_person" id="contact_person" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Contact Number:</td>
								<td><input type="text" name="contact_no" id="contact_no" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Country:</td>
								<td>
									<select name="country_id" id="country_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<?php foreach( $countries AS $country ) { ?>
										<option value="<?php echo $country['id']; ?>"><?php echo $country['country_name']; ?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Website:</td>
								<td><input type="text" name="website" id="website" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><input type="text" name="email" id="email" class="text_boxes" value="" /></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="20%">Group:</td>
								<td>
									<select name="group_id" id="group_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<option value="-1">-- New --</option>
										<?php foreach( $group_details AS $group ) { if( $group['status_active'] == 1 ) { ?>
										<option value="<?php echo $group['id']; ?>"><?php echo $group['group_name']; ?></option>
										<?php } } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="20%">Address:</td>
								<td><textarea name="address" id="address" class="text_area" style="resize:none;"></textarea></td>
							</tr>
							<tr>
								<td>Remark:</td>
								<td><textarea name="remark" id="remark" class="text_area" style="resize:none;"></textarea></td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>
									<select name="status_active" id="status_active" class="combo_boxes">
										<option value="0">Inactive</option>
										<option value="1" selected>Active</option>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr> 
				<tr>
					<td colspan="2" align="center" style="padding-top:10px;">
						<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>-->
	</form>
	<div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div>
</body>
</html>
<?php mysql_close( $host_connect ); ?>