//Do not change
function unserialize( data ) {
	// Takes a string representation of variable and recreates it  
	// 
	// version: 810.114
	// discuss at: http://phpjs.org/functions/unserialize
	// +     original by: Arpad Ray (mailto:arpad@php.net)
	// +     improved by: Pedro Tainha (http://www.pedrotainha.com)
	// +     bugfixed by: dptr1988
	// +      revised by: d3x
	// +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
	// %            note: Aiming for PHP-compatibility, we have to translate objects to arrays 
	// *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
	// *       returns 1: ['Kevin', 'van', 'Zonneveld']
	// *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
	// *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
	
	var error = function( type, msg, filename, line ) { throw new window[type](msg, filename, line); };
	var read_until = function( data, offset, stopchr ) {
		var buf = [];
		var chr = data.slice( offset, offset + 1 );
		var i = 2;
		
		while( chr != stopchr ) {
			if( ( i + offset ) > data.length ) {
				error( 'Error', 'Invalid' );
			}
			buf.push( chr );
			chr = data.slice( offset + ( i - 1 ), offset + i );
			i += 1;
		}
		return [buf.length, buf.join('')];
	};
	
	var read_chrs = function( data, offset, length ) {
		buf = [];
		for( var i = 0; i < length; i++ ) {
			var chr = data.slice(offset + (i - 1),offset + i);
			buf.push(chr);
		}
		return [buf.length, buf.join('')];
	};
	var _unserialize = function( data, offset ) {
		if( !offset ) offset = 0;
		var buf = [];
		var dtype = ( data.slice( offset, offset + 1 ) ).toLowerCase();
		
		var dataoffset = offset + 2;
		var typeconvert = new Function('x', 'return x');
		var chrs = 0;
		var datalength = 0;
		
		switch( dtype ) {
			case "i":
				typeconvert = new Function('x', 'return parseInt(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "b":
				typeconvert = new Function('x', 'return (parseInt(x) == 1)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "d":
				typeconvert = new Function('x', 'return parseFloat(x)');
				var readData = read_until(data, dataoffset, ';');
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 1;
				break;
			
			case "n":
				readdata = null;
				break;
			
			case "s":
				var ccount = read_until(data, dataoffset, ':');
				var chrs = ccount[0];
				var stringlength = ccount[1];
				dataoffset += chrs + 2;
				
				var readData = read_chrs(data, dataoffset+1, parseInt(stringlength));
				var chrs = readData[0];
				var readdata = readData[1];
				dataoffset += chrs + 2;
				if(chrs != parseInt(stringlength) && chrs != readdata.length) {
					error('SyntaxError', 'String length mismatch');
				}
				break;
			
			case "a":
				var readdata = {};

				var keyandchrs = read_until(data, dataoffset, ':');
				var chrs = keyandchrs[0];
				var keys = keyandchrs[1];
				dataoffset += chrs + 2;
				
				for( var i = 0; i < parseInt( keys ); i++ ) {
					var kprops = _unserialize(data, dataoffset);
					var kchrs = kprops[1];
					var key = kprops[2];
					dataoffset += kchrs;
					
					var vprops = _unserialize(data, dataoffset);
					var vchrs = vprops[1];
					var value = vprops[2];
					dataoffset += vchrs;
					
					readdata[key] = value;
				}
				
				dataoffset += 1;
				break;
			
			default:
				error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
				break;
		}
		return [dtype, dataoffset - offset, typeconvert(readdata)];
	};
	return _unserialize(data, 0)[2];
}

function createObject() {
	var request_type;
	var browser = navigator.appName;
	if( browser == "Microsoft Internet Explorer" ) {
		request_type = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		request_type = new XMLHttpRequest();
	}
	return request_type;
}

var http = createObject();
var field_array = new Array();

function fnc_group_details(save_perm,edit_perm,delete_perm,approve_perm) {				//Group Deatils of Cost Center Save / Update
	var group = $('#group').val();
	var group_name = $('#group_name').val();
	var contact_person = $('#contact_person').val();
	var contact_no = $('#contact_no').val();
	var country_id = $('#country_id').val();
	var website = $('#website').val();
	var email = $('#email').val();
	var address = $('#address').val();
	var remark = $('#remark').val();
	var status_active = $('#status_active').val();
	
	field_array[0] = new Array( 'group', group );
	field_array[1] = new Array( 'group_name', group_name );
	field_array[2] = new Array( 'contact_person', contact_person );
	field_array[3] = new Array( 'contact_no', contact_no );
	field_array[4] = new Array( 'country_id', country_id );
	field_array[5] = new Array( 'website', website );
	field_array[6] = new Array( 'email', email );
	field_array[7] = new Array( 'address', address );
	field_array[8] = new Array( 'remark', remark );
	field_array[9] = new Array( 'status_active', status_active );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (group==0 && save_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (group!=0 && edit_perm==2) // no edit 
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{
		error = true;
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( group_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#group_name').focus();
			$(this).html('Please Enter Group Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + escape( field_array[i][1] );
		}
		http.open( 'GET', 'includes/save_update_cost_center.php?action=group_details' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_group_details;
		http.send(null);
	}
}

function response_group_details() {			//Group Deatils Response of Cost Center Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				if (response[2]!='')
				{
					save_activities_history(response[2],'library','group_details','update','../../');
				}
				$(this).fadeOut(5000);
				populate_data();
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				if (response[2]!='')
				{
					save_activities_history(response[2],'library','group_details','save','../../');
				}
				$(this).fadeOut(5000);
				populate_data();
			});
			$('#group').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}		
		
	}
}

function fnc_company_details(save_perm,edit_perm,delete_perm,approve_perm) {			//Company Deatils of Cost Center Save / Update
	var company = escape(document.getElementById('cbo_company').value);
	var group_id = escape(document.getElementById('group_id').value);
	var company_name = escape(document.getElementById('company_name').value);
	var company_short_name = escape(document.getElementById('company_short_name').value);
	var service_cost_allocation = escape(document.getElementById('cbo_service_cost_allocation').value);
	var posting_pre_year = escape(document.getElementById('cbo_posting_pre_year').value);
	var statutory_account = escape(document.getElementById('cbo_statutory_account').value);
	var contact_person = escape(document.getElementById('txt_contact_person').value);
	var cfo = escape(document.getElementById('txt_cfo').value);
	var company_nature = escape(document.getElementById('cbo_company_nature').value);
	var core_business = escape(document.getElementById('cbo_core_business').value);
	var email = escape(document.getElementById('txt_email').value);
	var website = escape(document.getElementById('txt_website').value);
	var ac_length = escape(document.getElementById('txt_ac_code_length').value);
	var cbo_profit_center_affected = escape(document.getElementById('cbo_profit_center_affected').value);
	var plot_no = escape(document.getElementById('txt_plot_no').value);
	var level_no = escape(document.getElementById('txt_level_no').value);
	var road_no = escape(document.getElementById('txt_road_no').value);
	var block_no = escape(document.getElementById('txt_block_no').value);
	var country_id = escape(document.getElementById('country_id').value);
	var province = escape(document.getElementById('txt_province').value);
	var city = escape(document.getElementById('txt_city').value);
	var zip_code = escape(document.getElementById('txt_zip_code').value);
	var ph_num = escape(document.getElementById('ph_num').value);
	var trade_license_no = escape(document.getElementById('txt_trade_license_no').value);
	var incorporation_no = escape(document.getElementById('txt_incorporation_no').value);
	var erc_no = escape(document.getElementById('txt_erc_no').value);
	var irc_no = escape(document.getElementById('txt_irc_no').value);
	var tin_number = escape(document.getElementById('txt_tin_number').value);
	var vat_number = escape(document.getElementById('txt_vat_number').value);
	var epb_reg_no = escape(document.getElementById('txt_epb_reg_no').value);
	var trade_license_renewal = escape(document.getElementById('txt_trade_license_renewal').value);
	var erc_expiry_date = escape(document.getElementById('txt_erc_expiry_date').value);
	var irc_expiry_date = escape(document.getElementById('txt_irc_expiry_date').value);
	var txt_bangladesh_bank_reg_number = escape(document.getElementById('txt_bangladesh_bank_reg_number').value);
	var status_active = escape(document.getElementById('cbo_status_active').value);

	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (company==0 && save_perm==2) //no save
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (company!=0 && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2) //no update
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( group_id == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#group_id').focus();
			$(this).html('Please select a group').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else if( company_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_name').focus();
			$(this).html('Please Enter Company Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		http.open('get','includes/save_update_cost_center.php?action=company_details'+
				  '&company='+company+
				  '&group_id='+group_id+
				  '&company_name='+company_name+
				  '&company_short_name='+company_short_name+
				  '&service_cost_allocation='+service_cost_allocation+
				  '&posting_pre_year='+posting_pre_year+
				  '&statutory_account='+statutory_account+
				  '&contact_person='+contact_person+
				  '&cfo='+cfo+
				  '&company_nature='+company_nature+
				  '&core_business='+core_business+
				  '&email='+email+
				  '&website='+website+
				  '&cbo_profit_center_affected='+cbo_profit_center_affected+
				  '&ac_length='+ac_length+
				  '&plot_no='+plot_no+
				  '&level_no='+level_no+
				  '&road_no='+road_no+
				  '&block_no='+block_no+
				  '&country_id='+country_id+
				  '&province='+province+
				  '&city='+city+
				  '&zip_code='+zip_code+
				  '&ph_num='+ph_num+
				  '&trade_license_no='+trade_license_no+
				  '&incorporation_no='+incorporation_no+
				  '&erc_no='+erc_no+
				  '&irc_no='+irc_no+
				  '&tin_number='+tin_number+
				  '&vat_number='+vat_number+
				  '&epb_reg_no='+epb_reg_no+
				  '&trade_license_renewal='+trade_license_renewal+
				  '&erc_expiry_date='+erc_expiry_date+
				  '&irc_expiry_date='+irc_expiry_date+
				  '&txt_bangladesh_bank_reg_number='+txt_bangladesh_bank_reg_number+
				  '&status_active='+status_active+
				  '&nocache ='+nocache);
		http.onreadystatechange = response_company_details;
		http.send(null);
	}
}

function response_company_details() {		//Company Deatils Response of Cost Center Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					//save_activities_history('','library','company_details','update','../../');
				
				$(this).fadeOut(5000);
				location.reload();
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					//save_activities_history('','library','company_details','save','../../');
				
				$(this).fadeOut(5000);	
				location.reload();
			});
		}
		//populate_data();
		//reset_form();	
		 	
	}
}

function fnc_location_details(save_perm,edit_perm,delete_perm,approve_perm) {			//Location Deatils of Cost Center Save / Update
	var location = $('#location').val();
	var location_name = $('#location_name').val();
	var contact_person = $('#contact_person').val();
	var contact_no = $('#contact_no').val();
	var country_id = $('#country_id').val();
	var website = $('#website').val();
	var email = $('#email').val();
	var company_id = $('#company_id').val();
	var address = $('#address').val();
	var remark = $('#remark').val();
	var status_active = $('#status_active').val();
	
	field_array[0] = new Array( 'location', location );
	field_array[1] = new Array( 'location_name', location_name );
	field_array[2] = new Array( 'contact_person', contact_person );
	field_array[3] = new Array( 'contact_no', contact_no );
	field_array[4] = new Array( 'country_id', country_id );
	field_array[5] = new Array( 'website', website );
	field_array[6] = new Array( 'email', email );
	field_array[7] = new Array( 'company_id', company_id );
	field_array[8] = new Array( 'address', address );
	field_array[9] = new Array( 'remark', remark );
	field_array[10] = new Array( 'status_active', status_active );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (location==0 && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (location!=0 && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( location_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#location_name').focus();
			$(this).html('Please Enter Location Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + escape( field_array[i][1] );
		}
		http.open( 'GET', 'includes/save_update_cost_center.php?action=location_details' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_location_details;
		http.send(null);
	}
}



function response_location_details() {		//Location Deatils Response of Cost Center Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','location_details','update','../../');
				
				$(this).fadeOut(5000);
				location.reload();  
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','location_details','save','../../');
				
				$(this).fadeOut(5000);
				location.reload();  
			});
			$('#location').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}
				
		     	
		
		//populate_data();
		//reset_form();
	}
}




//Profit Center Deatils of Cost Center Save / Update

function fnc_profit_center_details(save_perm,edit_perm,delete_perm,approve_perm) {	

	var profit_center_id = escape(document.getElementById('profit_center_id').value);
	var profit_center_name = escape(document.getElementById('profit_center_name').value);
	var contact_person = escape(document.getElementById('contact_person').value);
	var contact_no = escape(document.getElementById('contact_no').value);
	var country_id = escape(document.getElementById('country_id').value);
	var website = escape(document.getElementById('website').value);
	var email = escape(document.getElementById('email').value);
	var company_id = escape(document.getElementById('company_id').value);
	var address = escape(document.getElementById('address').value);
	var remark = escape(document.getElementById('remark').value);
	var status_active = escape(document.getElementById('status_active').value);
	

	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (profit_center_id==0 && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (profit_center_id!=0 && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( profit_center_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#profit_center_name').focus();
			$(this).html('Please Enter Profit Center Name').addClass('messageboxerror').fadeTo(900,1);
			return;
		});
	}
	else if( company_id == 0 ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#company_id').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
			return;
		});
	}
	else {
		nocache = Math.random();
		http.open('get','includes/save_update_cost_center.php?action=profit_center_details'+
		
				  '&profit_center_id='+profit_center_id+
				  '&profit_center_name='+profit_center_name+
				  '&contact_person='+contact_person+
				  '&contact_no='+contact_no+
				  '&country_id='+country_id+
				  '&website='+website+
				  '&email='+email+
				  '&company_id='+company_id+
				  '&address='+address+
				  '&remark='+remark+
				  '&status_active='+status_active+
				  '&nocache ='+nocache);
		http.onreadystatechange = response_profit_center_details;
		http.send(null);
	}
}

function response_profit_center_details() {
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		// alert(http.responseText);
		if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Profit Center Name.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		if( response[0] == 4 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Please Select Company Name.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','library','profit_details','save','../../');
				$(this).fadeOut(5000);
			});
			
		}
		else if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				save_activities_history('','library','profit_details1','update','../../');
				$(this).fadeOut(5000);
			});
			
		}
		populate_data();
		reset_form();
		
	}
}



function fnc_division_details(save_perm,edit_perm,delete_perm,approve_perm) {			//Division Deatils of Cost Center Save / Update
	var division = $('#division').val();
	var division_name = $('#division_name').val();
	var location_id = $('#location_id').val();
	var remark = $('#remark').val();
	var status_active = $('#status_active').val();
	
	field_array[0] = new Array( 'division', division );
	field_array[1] = new Array( 'division_name', division_name );
	field_array[2] = new Array( 'location_id', location_id );
	field_array[3] = new Array( 'remark', remark );
	field_array[4] = new Array( 'status_active', status_active );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (division==0 && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (division!=0 && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( division_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#division_name').focus();
			$(this).html('Please Enter Division Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + escape( field_array[i][1] );
		}
		http.open( 'GET', 'includes/save_update_cost_center.php?action=division_details' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_division_details;
		http.send(null);
	}
}

function response_division_details() {		//Division Deatils Response of Cost Center Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','division_details','update','../../');
				location.reload();
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','division_details','save','../../');
				location.reload();
				$(this).fadeOut(5000);
			});
			$('#division').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}		
		//populate_data();
		//reset_form();
		 
	}
}

function fnc_department_details(save_perm,edit_perm,delete_perm,approve_perm) {			//Department Deatils of Cost Center Save / Update
	var department = $('#department').val();
	var department_name = $('#department_name').val();
	var division_id = $('#division_id').val();
	var remark = $('#remark').val();
	var status_active = $('#status_active').val();
	
	field_array[0] = new Array( 'department', department );
	field_array[1] = new Array( 'department_name', department_name );
	field_array[2] = new Array( 'division_id', division_id );
	field_array[3] = new Array( 'remark', remark );
	field_array[4] = new Array( 'status_active', status_active );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (department==0 && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (department!=0 && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( department_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#department_name').focus();
			$(this).html('Please Enter Department Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + escape( field_array[i][1] );
		}
		http.open( 'GET', 'includes/save_update_cost_center.php?action=department_details' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_department_details;
		http.send(null);
	}
}

function response_department_details() {	//Department Deatils Response of Cost Center Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','department_details','update','../../');
				location.reload(); 
				$(this).fadeOut(5000);
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','department_details','save','../../');
				location.reload(); 
				$(this).fadeOut(5000);
			});
			$('#department').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}
				
		//populate_data();
		//reset_form();
		
	}
}

function fnc_section_details(save_perm,edit_perm,delete_perm,approve_perm) {			//Section Deatils of Cost Center Save / Update
	var section = $('#section').val();
	var section_name = $('#section_name').val();
	var department_id = $('#department_id').val();
	var remark = $('#remark').val();
	var status_active = $('#status_active').val();
	
	field_array[0] = new Array( 'section', section );
	field_array[1] = new Array( 'section_name', section_name );
	field_array[2] = new Array( 'department_id', department_id );
	field_array[3] = new Array( 'remark', remark );
	field_array[4] = new Array( 'status_active', status_active );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (section==0 && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (section!=0 && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( section_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#section_name').focus();
			$(this).html('Please Enter Section Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + escape( field_array[i][1] );
		}
		http.open( 'GET', 'includes/save_update_cost_center.php?action=section_details' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_section_details;
		http.send(null);
	}
}

function response_section_details() {		//Section Deatils Response of Cost Center Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','section_details','update','../../');
				
				$(this).fadeOut(5000);
				location.reload(); 
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','section_details','save','../../');
				
				$(this).fadeOut(5000);
				location.reload(); 
			});
			$('#section').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}
				
		//populate_data();
		//reset_form();
		
	}
}

function fnc_subsection_details(save_perm,edit_perm,delete_perm,approve_perm) {			//Subsection Deatils of Cost Center Save / Update
	var subsection = $('#subsection').val();
	var subsection_name = $('#subsection_name').val();
	var section_id = $('#section_id').val();
	var remark = $('#remark').val();
	var status_active = $('#status_active').val();
	
	field_array[0] = new Array( 'subsection', subsection );
	field_array[1] = new Array( 'subsection_name', subsection_name );
	field_array[2] = new Array( 'section_id', section_id );
	field_array[3] = new Array( 'remark', remark );
	field_array[4] = new Array( 'status_active', status_active );
	
	$('#messagebox').removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	
	if (subsection==0 && save_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have save permission.').fadeIn(1000);
	}
	else if (subsection!=0 && edit_perm==2) // no edit 
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have update permission.').fadeIn(1000);
	}
	else if ( status_active==0 && delete_perm==2)
	{		
		$("#messagebox").removeClass().addClass('messageboxerror').text('You do not have delete permission.').fadeIn(1000);		
	}
	else if( subsection_name == '' ) {
		$('#messagebox').fadeTo( 200, 0.1, function() {
			$('#subsection_name').focus();
			$(this).html('Please Enter Subsection Name').addClass('messageboxerror').fadeTo(900,1);
		});
	}
	else {
		nocache = Math.random();
		var data = '';
		for( var i = 0; i < field_array.length; i++ ) {
			data += '&' + field_array[i][0] + '=' + escape( field_array[i][1] );
		}
		http.open( 'GET', 'includes/save_update_cost_center.php?action=subsection_details' + data + '&nocache=' + nocache );
		http.onreadystatechange = response_subsection_details;
		http.send(null);
	}
}

function response_subsection_details() {	//Subsection Deatils Response of Cost Center Save / Update
	if(http.readyState == 4) {
		var response = http.responseText.split('_');	
		
		if( response[0] == 3 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Duplicate Name. Please use another one.').addClass('messageboxerror').fadeTo(900,1);
			});
			return false;
		}
		else if( response[0] == 9 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','subsection_details','update','../../');
				
				$(this).fadeOut(5000);
				location.reload(); 
			});
		}
		else if( response[0] == 2 ) {
			$("#messagebox").fadeTo( 200, 0.1, function() {
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				
					save_activities_history('','library','subsection_details','save','../../');
				
				$(this).fadeOut(5000);
				location.reload(); 
			});
			$('#subsection').append( '<option value="' + response[1] + '">' + field_array[1][1] + '</option>' );
		}
		
		//populate_data();
		//reset_form();
		
	}
}