<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/


session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_cost_center.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var location_details = new Array();
		var division_details = new Array();
		<?php
		foreach( $location_details AS $location_id => $location ) {
			echo "location_details[$location_id] = new Array();\n";
			foreach( $location AS $key => $value ) {
				if( $key == 'id' || $key == 'company_id' ) echo "location_details[$location_id]['$key'] = '$value';\n";
				else echo "location_details[$location_id]['$key'] = '$value';\n";
			}
		}
		foreach( $division_details AS $division_id => $division ) {
			echo "division_details[$division_id] = new Array();\n";
			foreach( $division AS $key => $value ) {
				if( $key == 'id' || $key == 'location_id' ) echo "division_details[$division_id]['$key'] = '$value';\n";
				else echo "division_details[$division_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			populate_data();
			
			$('#division').change(function() {
				if( $(this).val() == 0 ) reset_form();
				else show_division( $(this).val() );
			});
			
			$('#location_id').change(function() {
				if( $(this).val() == -1 ) alert('new');
			});
		});
		
		function populate_division( location_id ) {
			var options = '<option value="0">-- Select division to edit --</option>';
			for( var i = 0; i < division_details.length; i++ ) {
				if( division_details[i] != undefined && division_details[i]['location_id'] == location_id ) {
					options += '<option value="' + division_details[i]['id'] + '">' + division_details[i]['division_name'] + '</option>';
				}
			}
			$('#division').html( options );
		}
		
		function show_division( division_id ) {
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'data=division&id=' + division_id,
				success: function( data ) {
					var division_details = unserialize( data );
					
				$('#new').css( "visibility", "visible" );
				$('#location').val( division_details['location_id'] );
				populate_division( division_details['location_id'] );
				$('#division').val( division_id );
				$('#division_name').val( division_details['division_name'] );
				$('#location_id').val( division_details['location_id'] );
				$('#remark').val( division_details['remark'] );
				$('#status_active').val( division_details['status_active'] );
				}
		 	});
		}
		
		function reset_form() {
			$('#new').css( "visibility", "hidden" );
			$('#location').val( 0 );
			$('#division').val( 0 );
			$('#division_name').val( '' );
			$('#location_id').val( 0 );
			$('#remark').val( '' );
			$('#status_active').val( 1 );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'form=division',
				success: function(data) {
					$('#data_panel').html( data );					
				}
			});
		}
	</script>
	<style>
		#division_details_form input[type="text"] { width:265px; }
		#division_details_form select { width:276px; }
		#division_details_form textarea { width:265px; height:60px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%;">       
    <div>
    	<div class="form_caption">
		Division Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form id="division_details_form" action="javascript:fnc_division_details(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%">&nbsp;Location:</td>
					<td width="35%">
						<select name="location" id="location" class="combo_boxes" style="margin-left:3px;" onchange="populate_division( this.value );">
							<option value="0">-- Select location to filter --</option>
							<?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
							<option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
					<td width="10%">&nbsp;Division:</td>
					<td>
						<select name="division" id="division" class="combo_boxes" style="margin-left:6px;">
							<option value="0">-- Select division to edit --</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97%; margin-top:10px;">
			<legend>Division Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="30%">Division Name:</td>
								<td>
									<input type="text" name="division_name" id="division_name" class="text_boxes" value="" />
									<img id="new" src="../../resources/images/new.jpg" title="New Division" alt="New" onclick="reset_form()" style="cursor:pointer; visibility:hidden;" />
								</td>
							</tr>
							<tr>
								<td>Location:</td>
								<td>
									<select name="location_id" id="location_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<option value="-1">-- New --</option>
										<?php foreach( $location_details AS $location ) { if( $location['status_active'] == 1 ) { ?>
										<option value="<?php echo $location['id']; ?>"><?php echo $location['location_name']; ?></option>
										<?php } } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>
									<select name="status_active" id="status_active" class="combo_boxes">
										<?php
											foreach($status_active as $key=>$value):
											?>
											<option value=<? echo "$key";
											if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
											<?		
											endforeach;
										?>	
									</select>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="4">
							<tr>
								<td width="20%">Remark:</td>
								<td><textarea name="remark" id="remark" class="text_area" onKeyUp="block_character_text_area()" style="resize:none;"></textarea></td>
							</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td colspan="2">&nbsp;</td>				
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-top:10px;" class="button_container">
						<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<fieldset><div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div></fieldset>
</body>
</html>
<?php mysql_close( $host_connect ); ?>