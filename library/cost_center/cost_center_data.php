<?php
session_start();
include('../../includes/common.php');
extract( $_POST );

//group_details
$sql = "SELECT * FROM lib_group WHERE is_deleted = 0 ORDER BY group_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$group_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$group_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$group_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//location_details
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//profit_center_details
$sql = "SELECT * FROM lib_profit_center WHERE is_deleted = 0 ORDER BY profit_center_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$profit_center_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$profit_center_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$profit_center_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//division_details
$sql = "SELECT * FROM lib_division WHERE is_deleted = 0 ORDER BY division_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$division_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$division_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$division_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//subsection_details
$sql = "SELECT * FROM lib_subsection WHERE is_deleted = 0 ORDER BY subsection_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$subsection_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$subsection_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$subsection_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//countries
$sql = "SELECT * FROM lib_list_country WHERE is_deleted = 0 ORDER BY country_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$countries = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$countries[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$countries[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}	

if ($_POST['form'] == "group"){
?>
<div style="width:100%" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<thead>
            <th width="50">SL No</th>
            <th width="100">Group Name</th>
            <th width="150">Contact Person</th>
            <th width="100">Contact No</th>
            <th width="150">Website</th>
            <th>Address</th>            
        </thead>
     </table>
</div>	
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >  
        
<?php
$i=1;

$sql= mysql_db_query($DB, "select * from lib_group where status_active=1 and is_deleted=0 and is_locked=0 order by group_name");

while ($selectResult = mysql_fetch_array($sql)){
	if ($i%2==0)  
		$bgcolor="#E9F3FF";
	else
		$bgcolor="#FFFFFF";	
	
?>				
            <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onclick="javascript:getClientData(<? echo $selectResult[id];?>,1)"> 
                <td width="50"><?php echo "$i"; ?></td>
                <td width="100"> <?php echo split_string($selectResult[group_name],30); ?></td>					
                <td width="150"><?php echo split_string($selectResult[contact_person],20); ?></td>
                <td width="100"><?php echo split_string($selectResult[contact_no],14); ?></td>
                <td width="150"> <?php echo split_string($selectResult[website],20); ?></td>
                <td><?php echo split_string($selectResult[address],60); ?> </td>							
            </tr>
            <?php
            $i++;
            }
            ?>
    </table>
</div>
<?
}
else if( $_POST['form'] == "company" ) { ?>
<div style="width:100%" align="left">
<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
	<thead>		
			<th width="200">Company Name</th>
			<th width="200">Group Name</th>
			<th width="200">Short Name</th>
			<th width="200">Contact Person</th>
			<th width="">Email</th>					
	</thead>
</table>
</div>
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		
		<?php 
			$ii=1;
			
			foreach( $company_details AS $company ) { 
			if ($ii%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			$ii++;
		?>
		<tr bgcolor="<? echo $bgcolor; ?>" id="<?php echo $company['id']; ?>" style="cursor:pointer" onclick="show_company(<?php echo $company['id']; ?>)">
			<td width="200"><?php echo split_string($company['company_name'],35); ?></td>
			<td width="200"><?php if( $company['group_id'] != 0 && array_key_exists( $company['group_id'], $group_details ) ) echo split_string($group_details[$company['group_id']]['group_name'],35); ?></td>
			<td width="200"><?php echo $company['company_short_name']; ?></td>
			<td width="200"><?php echo $company['contract_person']; ?></td>
			<td width=""><?php echo split_string($company['email'],35); ?></td>
            			
		</tr>
		<?php } ?>
</table>
</div>
<?php }
else if( $_POST['form'] == "location" ) { ?>
<div style="width:100%" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<thead>
			<th width="200">Location Name</th>
			<th width="200">Company Name</th>
			<th width="200">Contact Person</th>
			<th width="200">Contact Number</th>
			<th width="">Email</th>			
		</thead>
	</table>
</div>    
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >    
		<?php 
			$ii=1;
			foreach( $location_details AS $location ) {
			if ($ii%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			$ii++;
			
			?>
		<tr bgcolor="<? echo $bgcolor; ?>" id="<?php echo $location['id']; ?>" onclick="show_location(<?php echo $location['id']; ?>)" style="cursor:pointer" >
			<td width="200"><?php echo split_string($location['location_name'],35); ?></td>
			<td width="200"><?php if( $location['company_id'] != 0 ) echo split_string($company_details[$location['company_id']]['company_name'],35); ?></td>
			<td width="200"><?php echo split_string($location['contact_person'],25); ?></td>
			<td width="200"><?php echo split_string($location['contact_no'],25); ?></td>
			<td width=""><?php echo split_string($location['email'],30); ?></td>			
		</tr>
		<?php } ?>	
</table>
</div>
<?php }
//profit center list view
else if( $_POST['form'] == "profit_center" ) { ?>
<div style="width:100%" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<thead>
			<th width="200">Profit_center Name</th>
			<th width="200">Company Name</th>
			<th width="200">Contact Person</th>
			<th width="200">Contact Number</th>
			<th width="">Email</th>			
		</thead>
	</table>
</div>    
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >    
		<?php
			$ii=1;
		 	foreach( $profit_center_details AS $profit_center ) { 
			if ($ii%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			$ii++;
			
		?>
		<tr bgcolor="<? echo $bgcolor; ?>" id="<?php echo $profit_center['id']; ?>" onclick="show_profit_center(<?php echo $profit_center['id']; ?>)" style="cursor:pointer" >
			<td width="200"><?php echo split_string($profit_center['profit_center_name'],35); ?></td>
			<td width="200"><?php if( $profit_center['company_id'] != 0 ) echo split_string($company_details[$profit_center['company_id']]['company_name'],35); ?></td>
			<td width="200"><?php echo split_string($profit_center['contact_person'],25); ?></td>
			<td width="200"><?php echo split_string($profit_center['contact_no'],25); ?></td>
			<td width=""><?php echo split_string($profit_center['email'],30); ?></td>			
		</tr>
		<?php } ?>	
</table>
</div>
<?php
 }

else if( $_POST['form'] == "division" ) { ?>
<div style="width:100%" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<thead>
			<th width="350">Division Name</th>
			<th width="350">Location Name</th>
			<th width="">Remark</th>				
		</thead>
	</table>
</div>    
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<?php
			$ii=1;
			foreach( $division_details AS $division ) { 
			if ($ii%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			$ii++;
			
		?>
		<tr bgcolor="<? echo $bgcolor; ?>" id="<?php echo $division['id']; ?>" onclick="show_division(<?php echo $division['id']; ?>)" style="cursor:pointer" >
			<td width="350"><?php echo split_string($division['division_name'],35); ?></td>
			<td width="350"><?php if( $division['location_id'] != 0 ) echo split_string($location_details[$division['location_id']]['location_name'],35); ?></td>
			<td width=""><?php echo split_string($division['remark'],35); ?></td>			
		</tr>
		<?php } ?>	
</table>
</div>
<?php }
else if( $_POST['form'] == "department" ) { ?>
<div style="width:100%" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<thead>	
			<th width="350">Department Name</th>
			<th width="350">Division Name</th>
			<th width="">Remark</th>			
		</thead>
    </table>
</div>
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
	
		<?php 
			$ii=1;
			foreach( $department_details AS $department ) {
			if ($ii%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			$ii++; 
		?>
		<tr bgcolor="<? echo $bgcolor; ?>" id="<?php echo $department['id']; ?>" onclick="show_department(<?php echo $department['id']; ?>)" style="cursor:pointer">
			<td width="350"><?php echo split_string($department['department_name'],35); ?></td>
			<td width="350"><?php if( $department['division_id'] != 0 ) echo split_string($division_details[$department['division_id']]['division_name'],35); ?></td>
			<td width=""><?php echo split_string($department['remark'],35); ?></td>			
		</tr>
		<?php } ?>
	</table>
</div>
<?php }
else if( $_POST['form'] == "section" ) { ?>
<div style="width:100%" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<thead>	
			<th width="350">Section Name</th>
			<th width="350">Department Name</th>
			<th>Remark</th>
		</thead>
	</table>
</div>
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
    
		<?php 
			$ii=1;
			foreach( $section_details AS $section ) {
			if ($ii%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			$ii++;  
		?>
		<tr bgcolor="<? echo $bgcolor; ?>" id="<?php echo $section['id']; ?>" onclick="show_section(<?php echo $section['id']; ?>)" style="cursor:pointer">
			<td width="350"><?php echo $section['section_name']; ?></td>
			<td width="350"><?php if( $section['department_id'] != 0 ) echo $department_details[$section['department_id']]['department_name']; ?></td>
			<td><?php echo $section['remark']; ?></td>			
		</tr>
		<?php } ?>	
	</table>
</div>
<?php }
else if( $_POST['form'] == "subsection" ) { ?>
<div style="width:100%" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
		<thead>
			<th width="350">Subsection Name</th>
			<th width="350">Section Name</th>
			<th>Remark</th>
		</thead>
	</table>
</div>
<div style="width:100%; overflow-y:scroll; max-height:210px;" align="left">
	<table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" >
    
		<?php
			$ii=1;
			foreach( $subsection_details AS $subsection ) { 
			if ($ii%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			$ii++; 
		?>
		<tr bgcolor="<? echo $bgcolor; ?>" id="<?php echo $subsection['id']; ?>" onclick="show_subsection(<?php echo $subsection['id']; ?>)" style="cursor:pointer">
			<td width="350"><?php echo split_string($subsection['subsection_name'],50); ?></td>
			<td width="350"><?php if( $subsection['section_id'] != 0 ) echo split_string($section_details[$subsection['section_id']]['section_name'],50); ?></td>
			<td><?php echo split_string($subsection['remark'],50); ?></td>			
		</tr>
		<?php } ?>	
	</table>
</div>
<?php
}

if( isset( $data ) ) {
	if( $data == 'company' ) {
		$company_data = $company_details[$id];
		$_SESSION['company_logo_id']=$id;
		print_r( serialize( $company_data ) );
	}
	if( $data == 'location' ) {
		$location_data = $location_details[$id];		
		print_r( serialize( $location_data ) );
	}
	if( $data == 'division' ) {
		$division_data = $division_details[$id];		
		print_r( serialize( $division_data ) );
	}
	if( $data == 'department' ) {
		$department_data = $department_details[$id];		
		print_r( serialize( $department_data ) );
	}
	if( $data == 'section' ) {
		$section_data = $section_details[$id];		
		print_r( serialize( $section_data ) );
	}
	if( $data == 'subsection' ) {
		$subsection_data = $subsection_details[$id];		
		print_r( serialize( $subsection_data ) );
	}
	if( $data == 'profit_center' ) {
		$subsection_data = $subsection_details[$id];		
		print_r( serialize( $subsection_data ) );
	}
}
mysql_close( $host_connect );
?>