<?php

/*######################################

	Completed By
	Name : Bilas Chandra Sarker
	Date : 29-03-12
		
######################################*/

session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');

//department_details
$sql = "SELECT * FROM lib_department WHERE is_deleted = 0 ORDER BY department_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$department_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$department_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$department_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//section_details
$sql = "SELECT * FROM lib_section WHERE is_deleted = 0 ORDER BY section_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$section_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$section_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$section_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_cost_center.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		
		var department_details = new Array();
		var section_details = new Array();
		<?php
		foreach( $department_details AS $department_id => $department ) {
			echo "department_details[$department_id] = new Array();\n";
			foreach( $department AS $key => $value ) {
				if( $key == 'id' || $key == 'division_id' ) echo "department_details[$department_id]['$key'] = $value;\n";
				else echo "department_details[$department_id]['$key'] = '$value';\n";
			}
		}
		foreach( $section_details AS $section_id => $section ) {
			echo "section_details[$section_id] = new Array();\n";
			foreach( $section AS $key => $value ) {
				if( $key == 'id' || $key == 'department_id' ) echo "section_details[$section_id]['$key'] = $value;\n";
				else echo "section_details[$section_id]['$key'] = '$value';\n";
			}
		}
		?>
		
		$(document).ready(function() {
			populate_data();
			
			$('#section').change(function() {
				if( $(this).val() == 0 ) reset_form();
				else show_section( $(this).val() );
			});
			
			$('#department_id').change(function() {
				if( $(this).val() == -1 ) alert('new');
			});
		});
		
		function populate_section( department_id ) {
			var options = '<option value="0">-- Select section to edit --</option>';
			for( var i = 0; i < section_details.length; i++ ) {
				if( section_details[i] != undefined && section_details[i]['department_id'] == department_id ) {
					options += '<option value="' + section_details[i]['id'] + '">' + section_details[i]['section_name'] + '</option>';
				}
			}
			$('#section').html( options );
		}
		
		function show_section( section_id ) {
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'data=section&id=' + section_id,
				success: function( data ) {
					var section_details = unserialize( data );
			$('#new').css( "visibility", "visible" );
			$('#department').val( section_details['department_id'] );
			populate_section( section_details['department_id'] );
			$('#section').val( section_id );
			$('#section_name').val( section_details['section_name'] );
			$('#department_id').val( section_details['department_id'] );
			$('#remark').val( section_details['remark'] );
			$('#status_active').val( section_details['status_active'] );
			}
		});
		}
		
		function reset_form() {
			$('#new').css( "visibility", "hidden" );
			$('#department').val( 0 );
			$('#section').val( 0 );
			$('#section_name').val( '' );
			$('#department_id').val( 0 );
			$('#remark').val( '' );
			$('#status_active').val( 1 );
		}
		
		function populate_data() {
			$('#data_panel').html('<img src="../../resources/images/loading.gif" />');
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'form=section',
				success: function(data) {
					$('#data_panel').html( data );					
				}
			});
		}
	</script>
	<style>
		#section_details_form input[type="text"] { width:265px; }
		#section_details_form select { width:276px; }
		#section_details_form textarea { width:265px; height:60px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%;">    
    <div>
    	<div class="form_caption">
		Section Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form id="section_details_form" action="javascript:fnc_section_details(save_perm,edit_perm,delete_perm,approve_perm)" method="POST">
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%">&nbsp;Department:</td>
					<td width="35%">
						<select name="department" id="department" class="combo_boxes" style="margin-left:3px;" onchange="populate_section( this.value );">
							<option value="0">-- Select department to filter --</option>
							<?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
							<option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
					<td width="10%">&nbsp;Section:</td>
					<td>
						<select name="section" id="section" class="combo_boxes" style="margin-left:6px;">
							<option value="0">-- Select section to edit --</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset>
			<legend>Section Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="30%">Section Name:</td>
								<td>
									<input type="text" name="section_name" id="section_name" class="text_boxes" value="" />
									<img id="new" src="../../resources/images/new.jpg" title="New Section" alt="New" onclick="reset_form()" style="cursor:pointer; visibility:hidden;" />
								</td>
							</tr>
							<tr>
								<td>Department:</td>
								<td>
									<select name="department_id" id="department_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<option value="-1">-- New --</option>
										<?php foreach( $department_details AS $department ) { if( $department['status_active'] == 1 ) { ?>
										<option value="<?php echo $department['id']; ?>"><?php echo $department['department_name']; ?></option>
										<?php } } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>
									<select name="status_active" id="status_active" class="combo_boxes">
										<?php
											foreach($status_active as $key=>$value):
											?>
											<option value=<? echo "$key";
											if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
											<?		
											endforeach;
										?>	
									</select>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="4">
							<tr>
								<td width="20%">Remark:</td>
								<td><textarea name="remark" id="remark" class="text_area" onKeyUp="block_character_text_area()" style="resize:none;"></textarea></td>
							</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="center" style="padding-top:10px;" class="button_container">
						<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<fieldset><div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div></fieldset>
</body>
</html>
<?php mysql_close( $host_connect ); ?>