<?php


session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------
include('../../includes/common.php');
include('../../includes/array_function.php');

//company_details
$sql = "SELECT * FROM lib_company WHERE is_deleted = 0 AND profit_center_affected=1 ORDER BY company_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$company_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$company_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$company_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}
//profit_center_details
$sql = "SELECT * FROM lib_profit_center WHERE is_deleted = 0 ORDER BY profit_center_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$profit_center_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$profit_center_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$profit_center_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

//countries
$sql = "SELECT * FROM lib_list_country WHERE status_active=1 and is_deleted = 0 ORDER BY country_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$countries = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$countries[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$countries[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Logic Payroll Software</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Asrotex, Logic, Payroll, Muntasir" />
	<meta name="description" content="Logic Payroll Software" />
	
	<script src="includes/ajax_submit_cost_center.js" type="text/javascript"></script>
    <script src="includes/functions.js" type="text/javascript"></script>
	<script src="../../includes/update_status.js" type="text/javascript"></script>
	<link href="../../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script src="../../resources/jquery-1.6.2.js" type="text/javascript"></script>
	<link href="../../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
	
	<link href="../../resources/jquery_dataTable/demo_table_jui.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="../../resources/jquery_dataTable/jquery.dataTables.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">	
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;	
		
		var company_details = new Array();
		var profit_center_details = new Array();
		var countries = new Array();
		<?php
		foreach( $company_details AS $company_id => $company ) {
			echo "company_details[$company_id] = new Array();\n";
			foreach( $company AS $key => $value ) {
				if( $key == 'id' || $key == 'country_id' || $key == 'group_id' ) echo "company_details[$company_id]['$key'] = '$value';\n";
				else echo "company_details[$company_id]['$key'] = '$value';\n";
			}
		}
		foreach( $profit_center_details AS $profit_center_id => $profit_center ) {
			echo "profit_center_details[$profit_center_id] = new Array();\n";
			foreach( $profit_center AS $key => $value ) {
				if( $key == 'id' || $key == 'country_id' || $key == 'company_id' ) echo "profit_center_details[$profit_center_id]['$key'] = '$value';\n";
				else echo "profit_center_details[$profit_center_id]['$key'] = '$value';\n";
			}
		}
		foreach( $countries AS $country_id => $country ) {
			echo "countries[$country_id] = new Array();\n";
			foreach( $country AS $key => $value ) {
				if( $key == 'id' ) echo "countries[$country_id]['$key'] = '$value';\n";
				else echo "countries[$country_id]['$key'] = '$value';\n";
			}
		}
		?>
	
		
		$(document).ready(function() {
			populate_data();
			
			$('#profit_center').change(function() {
				if( $(this).val() == 0 ) reset_form();
				
				else show_profit_center( $(this).val() );
			});
			
			$('#company_id').change(function() {
				if( $(this).val() == -1 ) alert('new');
			});
		});
		
		function populate_profit_center( company_id ) {
			//alert(company_id);
			var options = '<option value="0">-- Select Profit Center to edit --</option>';
			for( var i = 0; i < profit_center_details.length; i++ ) {
				if( profit_center_details[i] != undefined && profit_center_details[i]['company_id'] == company_id ) {
					options += '<option value="' + profit_center_details[i]['id'] + '">' + profit_center_details[i]['profit_center_name'] + '</option>';
				}
			}
			$('#profit_center').html( options );
		}
		
	
		function show_profit_center(id,type)
		{	
	
			ajax.requestFile = 'includes/get_data_update.php?getClientId='+id+'&type=profit_center';	// Specifying which file to get
			ajax.onCompletion = show_profit_center_search;	// Specify function that will be executed after file has been found
			ajax.runAJAX();	
			//showResult_subgroup_status();	
		}
		function show_profit_center_search()
		{
			var formObj = document.forms['profit_center_details_form'];
			eval(ajax.response);	
		}

/*

		function show_profit_center( profit_center_id ) {
			//alert (profit_center_id);
			$.ajax({
				
				type: "POST",
				url: "cost_center_data.php",
				data: 'data=profit_center&id=' + profit_center_id,
				success: function( data ) {
					
					var profit_center_details = unserialize( data );
					//alert(profit_center_details);
				$('#new').css( "visibility", "visible" );
				$('#company').val( profit_center_details['company_id'] );
				populate_profit_center( profit_center_details['company_id'] );
				$('#profit_center').val( profit_center_id );
				$('#profit_center_name').val( profit_center_details['profit_center_name'] );
				$('#company_id').val( profit_center_details['company_id'] );
				$('#contact_person').val( profit_center_details['contact_person'] );
				$('#contact_no').val( profit_center_details['contact_no'] );
				$('#website').val( profit_center_details['website'] );
				$('#address').val( profit_center_details['address'] );
				$('#email').val( profit_center_details['email'] );
				$('#country_id').val( profit_center_details['country_id'] );
				$('#remark').val( profit_center_details['remark'] );
				$('#status_active').val( profit_center_details['status_active'] );
				}
			});
		}
	*/
		function reset_form() {
			$('#new').css( "visibility", "hidden" );
			$('#company').val( 0 );
			$('#profit_center_id').val( 0 );
			$('#profit_center').val( 0 );
			$('#profit_center_name').val( '' );
			$('#company_id').val( 0 );
			$('#contact_person').val( '' );
			$('#contact_no').val( '' );
			$('#website').val( '' );
			$('#address').val( '' );
			$('#email').val( '' );
			$('#country_id').val( 0 );
			$('#remark').val( '' );
			$('#status_active').val( 1 );
		}
		
		function populate_data() {			
			$.ajax({
				type: "POST",
				url: "cost_center_data.php",
				data: 'form=profit_center',
				success: function(data) {
					$('#data_panel').html( data );					
				}
			});
		}
	</script>
	<style>
		#profit_center_details_form input[type="text"] { width:265px; }
		#profit_center_details_form select { width:276px; }
		#profit_center_details_form textarea { width:265px; height:40px; }
		#profit_center_details_form input[type="email"] { width:265px; }
		.formbutton { width:100px; }
		.ui-icon { cursor:pointer; }
		.ui-icon-check, .ui-icon-closethick { float:left; }
		.ui-icon-trash { float:right; }
	</style>
</head>

<body style="font-family:verdana; font-size:11px;">
	<div align="center" style="width:99%;">     
    <div>
    	<div class="form_caption">
		Profit Center Details
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update.$delete;
			?>
           </span>
		<div id="messagebox" style="background-color:#FF9999; color:#000000" align="center"></div>
	</div>
	<form name="profit_center_details_form" id="profit_center_details_form" action="javascript:fnc_profit_center_details(save_perm,edit_perm,delete_perm,approve_perm)" >
		<fieldset style="width:97%;">
			<legend>Edit Filter</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%">&nbsp;Company:</td>
					<td width="35%">
						<select name="company" id="company" class="combo_boxes" style="margin-left:3px;" onchange="populate_profit_center( this.value );">
							<option value="0">-- Select company to filter --</option>
							<?php foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
							<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
							<?php } } ?>
						</select>
					</td>
					<td width="10%">&nbsp;Profit Center:</td>
					<td>
						<select name="profit_center" id="profit_center" class="combo_boxes" style="margin-left:6px;">
							<option value="0">-- Select profit center to edit --</option>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="width:97%; margin-top:10px;">
			<legend>Location Information</legend>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="30%">Profit Center Name:</td>
								<td>
									<input type="text" name="profit_center_name" id="profit_center_name" class="text_boxes" value="" />
									<img id="new" src="../../resources/images/new.jpg" title="New Location" alt="New" onclick="reset_form()" style="cursor:pointer; visibility:hidden;" />
                                    <input type="hidden" id="profit_center_id" name="profit_center_id"/>
								</td>
							</tr>
							<tr>
								<td>Contact Person:</td>
								<td><input type="text" name="contact_person" id="contact_person" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Contact Number:</td>
								<td><input type="text" name="contact_no" id="contact_no" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Country:</td>
								<td>
									<select name="country_id" id="country_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<?php foreach( $countries AS $country ) { ?>
										<option value="<?php echo $country['id']; ?>"><?php echo $country['country_name']; ?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Website:</td>
								<td><input type="text" name="website" id="website" class="text_boxes" value="" /></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><input type="email" name="email" id="email" class="text_boxes" value="" /></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="20%">Company:</td>
								<td>
									<select name="company_id" id="company_id" class="combo_boxes">
										<option value="0">-- Select --</option>
										<option value="-1">-- New --</option>
										<?php foreach( $company_details AS $company ) { if( $company['status_active'] == 1 ) { ?>
										<option value="<?php echo $company['id']; ?>"><?php echo $company['company_name']; ?></option>
										<?php } } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="20%">Address:</td>
								<td><textarea name="address" id="address" class="text_area" onKeyUp="block_character_text_area()" style="resize:none;"></textarea></td>
							</tr>
							<tr>
								<td>Remark:</td>
								<td><textarea name="remark" id="remark" class="text_area" onKeyUp="block_character_text_area()" style="resize:none;"></textarea></td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>
									<select name="status_active" id="status_active" class="combo_boxes">
										<?php
											foreach($status_active as $key=>$value):
											?>
											<option value=<? echo "$key";
											if ($key==1){?> selected <? } ?>> <? echo "$value" ; ?> </option>
											<?		
											endforeach;
										?>	
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
                <tr>
					<td colspan="2" align="center" style="padding-top:10px;" class="button_container" >
						<input type="submit" name="save" class="formbutton" value="Save" />&nbsp;&nbsp;
						<input type="button" name="cancel" class="formbutton" value="Cancel" onclick="reset_form()" />
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<fieldset><div id="data_panel" class="demo_jui" align="center" style="width:99%; margin-top:15px;"></div></fieldset>
</body>
</html>
<?php mysql_close( $host_connect ); ?>