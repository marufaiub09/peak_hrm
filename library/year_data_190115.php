
<?php
include('../includes/common.php');
extract( $_POST );

//year
$sql = "SELECT * FROM lib_policy_year WHERE is_deleted = 0 ORDER BY name DESC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$year = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$year[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$year[$row['id']][$key] = mysql_real_escape_string( $value );
	}
	$sql2 = "SELECT * FROM lib_policy_year_periods WHERE year_id = $row[id] AND is_deleted = 0 ORDER BY id ASC";
	$result2 = mysql_query( $sql2 ) or die( $sql2 . "<br />" . mysql_error() );
	$year[$row['id']]['periods'] = array();
	$i = 0;
	while( $row2 = mysql_fetch_assoc( $result2 ) ) {
		$year[$row['id']]['periods'][$i] = array();
		foreach( $row2 AS $key2 => $value2 ) {
			$year[$row['id']]['periods'][$i][$key2] = mysql_real_escape_string( $value2 );
		}
		$i++;
	}
}
//location
$sql = "SELECT * FROM lib_location WHERE is_deleted = 0 ORDER BY location_name ASC";
$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );

$location_details = array();
while( $row = mysql_fetch_assoc( $result ) ) {
	$location_details[$row['id']] = array();
	foreach( $row AS $key => $value ) {
		$location_details[$row['id']][$key] = mysql_real_escape_string( $value );
	}
}

if( $_POST['form'] == "leave" ) {
?>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display" id="leave_year_display">
	<thead>
		<tr>
			<th>Name</th>
			<th>Year Start</th>
			<th>Year End</th>
			<th>Applicable Location</th>  
            <th>Locked</th>       
			<!--<th width="50">Options</th>-->
		</tr>
	</thead>
	<tbody>
		<?php
		foreach( $year AS $y ) { if( $y['type'] == 0 ) {
			$locations = '';
			foreach( $location_details AS $location ) {
				if( $location['leave_year'] == $y['id'] ) {
					$locations .= $location['location_name'] . '<br />';
				}
			}
			$locations = substr( $locations, 0, strlen( $locations ) - 6 );
		?>
		<tr class="gradeA">
			<td><a href="javascript:show_leave_year( <?php echo $y['id']; ?> );"><?php echo $y['name']; ?></a></td>
			<td align="center"><?php echo $y['year_start']; ?></td>
			<td align="center"><?php echo $y['year_end']; ?></td>
			<td align="center"><?php echo $locations; ?></td>  
			<td align="center"><?php echo $y['is_locked']; ?></td>
            <!--
            <td>           
				<a id="status_<?php echo $y['id']; ?>" href="javascript:update_status( 'leave', 'active', '../includes/common.php', '', 'lib_policy_year', <?php echo $y['id']; ?>, <?php echo $y['status_active']; ?> )">
					<span class="ui-icon <?php if( $y['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'leave', 'delete', '../includes/common.php', '', 'lib_location', <?php echo $y['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>             
			</td> 
            -->
		</tr>
		<?php } } ?>
	</tbody>
</table>
<?php }
else if( $_POST['form'] == "salary" ) {
?>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display" id="salary_year_display">
	<thead>
		<tr>
			<th>Name</th>
			<th>Year Start</th>
			<th>Year End</th>
			<th>Status</th>
			<th width="50">Locked</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach( $year AS $y ) { if( $y['type'] == 1 ) {
			
		?>
		<tr class="gradeA">
			<td><a href="javascript:show_salary_year( <?php echo $y['id']; ?> );"><?php echo $y['name']; ?></a></td>
			<td align="center"><?php echo $y['year_start']; ?></td>
			<td align="center"><?php echo $y['year_end']; ?></td>
			<td align="center"><?php echo $y['status_active']; ?></td>
            <td align="center"><?php echo $y['is_locked']; ?></td>
			<!--
            <td>
				<a id="status_<?php echo $y['id']; ?>" href="javascript:update_status( 'salary', 'active', '../includes/common.php', '', 'lib_policy_year', <?php echo $y['id']; ?>, <?php echo $y['status_active']; ?> )">
					<span class="ui-icon <?php if( $y['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'salary', 'delete', '../includes/common.php', '', 'lib_location', <?php echo $y['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
            -->
		</tr>
		<?php } } ?>
	</tbody>
</table>
<?php }
else if( $_POST['form'] == "financial" ) { ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display" id="financial_year_display">
	<thead>
		<tr>
			<th>Name</th>
			<th>Year Start</th>
			<th>Year End</th>
			<th>Active Year</th>
			<th width="50">Is Current</th>
		</tr>
	</thead>
	<tbody>
		<?php
			//year
		$sql_ac = "SELECT * FROM ac_year WHERE is_deleted = 0 and type=3 ORDER BY name DESC";
		$result_ac = mysql_query( $sql_ac ) or die( $sql_ac . "<br />" . mysql_error() );
		
	while( $row2_ac = mysql_fetch_array( $result_ac ) ) {
		?>
		<tr class="gradeA">
			<td><a href="javascript:show_financial_year( <?php echo $row2_ac['id']; ?>,'<?php echo $row2_ac['name']; ?>' );"><?php echo $row2_ac['name']; ?></a></td>
           
			<td align="center"><?php echo $row2_ac[year_start]; ?></td>
			<td align="center"><?php echo $row2_ac[year_end]; ?></td>
			<td align="center"><?php if($row2_ac[status_active]==1)echo 'Active';else echo 'In-Active'; ?></td>
            <td align="center"><?php if($row2_ac[is_locked]==0){echo 'No';}else{echo 'Yes';} ?></td>
			<!--
            <td>
				<a id="status_<?php echo $row2_ac[id]; ?>" href="javascript:update_status( 'salary', 'active', '../includes/common.php', '', 'lib_policy_year', <?php echo $row2_ac[id]; ?>, <?php echo $row2_ac[status_active]; ?> )">
					<span class="ui-icon <?php if( $row2_ac['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'salary', 'delete', '../includes/common.php', '', 'lib_location', <?php echo $row2_ac['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
            -->
		</tr>
		<?php }  ?>
	</tbody>
</table>
<?php }
else if( $_POST['form'] == "tax" ) {
?>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display" id="tax_year_display">
	<thead>
		<tr>
			<th>Name</th>
			<th>Year Start</th>
			<th>Year End</th>
			<th>Applicable Location</th>
			<th width="50">Options</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach( $year AS $y ) { if( $y['type'] == 2 ) {
			$locations = '';
			foreach( $location_details AS $location ) {
				if( $location['leave_year'] == $y['id'] ) {
					$locations .= $location['location_name'] . '<br />';
				}
			}
			$locations = substr( $locations, 0, strlen( $locations ) - 6 );
		?>
		<tr class="gradeA">
			<td><a href="javascript:show_tax_year( <?php echo $y['id']; ?> );"><?php echo $y['name']; ?></a></td>
			<td align="center"><?php echo $y['year_start']; ?></td>
			<td align="center"><?php echo $y['year_end']; ?></td>
			<td align="center"><?php echo $locations; ?></td>
			<td>
				<a id="status_<?php echo $y['id']; ?>" href="javascript:update_status( 'tax', 'active', '../includes/common.php', '', 'lib_policy_year', <?php echo $y['id']; ?>, <?php echo $y['status_active']; ?> )">
					<span class="ui-icon <?php if( $y['status_active'] == 1 ) echo 'ui-icon-check'; else echo 'ui-icon-closethick'; ?>"></span>
				</a>
				<a href="javascript:update_status( 'tax', 'delete', '../includes/common.php', '', 'lib_location', <?php echo $y['id']; ?> )">
					<span class="ui-icon ui-icon-trash"></span>
				</a>
			</td>
		</tr>
		<?php } } ?>
	</tbody>
</table>
<?php }
mysql_close( $host_connect );
?>