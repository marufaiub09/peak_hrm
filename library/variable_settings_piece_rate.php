<?php 
// Sohel
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_GET);
$permission=explode('_',$permission);
 
if ($permission[0]==1 ) $insert="New Entry permission. "; else $insert="";
if ($permission[1]==1 ) $update="Edit permission. "; else $update="";
if ($permission[2]==1 ) $delete="Delete permission. "; else $delete="";
if ($permission[3]==1 ) $approve="Approval permission. "; else $approve="";

//--------------------------------------------------------------------------------------------------------------------

include('../includes/common.php');
include('../includes/array_function.php');
include('../includes/common_functions.php');

$ConsumptionTabIndex=-1;
extract($_POST);
if(!$TabIndexNo) { $TabIndexNo=0; }

	$sql = "SELECT * FROM lib_payroll_head WHERE is_applicable=1 and  is_deleted = 0 and status_active=1 ORDER BY system_head ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$applycable_salary_head = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$applycable_salary_head[$row['id']] = mysql_real_escape_string($row['system_head']);
	}
	
	$sql = "SELECT * FROM lib_payroll_head WHERE salary_head=1 and  is_deleted = 0 and status_active=1 ORDER BY system_head ASC";
	$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
	$jobassignday_salary_head = array();
	while( $row = mysql_fetch_assoc( $result ) ) {
		$jobassignday_salary_head[$row['id']] = mysql_real_escape_string($row['system_head']);
	}
	
	//$common_no_yes=array(0=>"No",1=>"Yes");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Cost Sheets</title>
		<link href="../css/style_common.css" rel="stylesheet" type="text/css" />
		<link href="../css/popup_window.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/popup_window.js"></script>
		<script type="text/javascript" src="../js/modal.js"></script>
		
		<script type="text/javascript" src="includes/functions_variable_settings.js"></script>
		<script type="text/javascript" src="includes/ajax_submit_variable_settings.js"></script>
        
		<link href="../css/style_common.css" rel="stylesheet" type="text/css" media="screen" />
		<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
        <link href="../resources/jquery_ui/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="../resources/jquery_ui/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
        <script src="../resources/jquery-ui-timepicker-addon.js"type="text/javascript" ></script>
	
		<script type="text/javascript" src="../js/ajaxupload.3.5.js" ></script>
        
         <script src="../resources/multiselect/multi_select.js" type="text/javascript"></script>
    	 <link href="../resources/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css" />
		<script src="../resources/multiselect/jquery.multiselect.filter.js" type="text/javascript"></script>
  		<link href="../resources/multiselect/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" />
        <script>
		var save_perm = <? echo $permission[0]; ?>;
		var edit_perm = <? echo $permission[1]; ?>;
		var delete_perm = <? echo $permission[2]; ?>;
		var approve_perm = <? echo $permission[3]; ?>;
		</script>

        
		<script type="text/javascript">


$(document).ready(function(e) {
	  $("#applicable_salaryhead,#offday_payment,#leave_type").multiselect({
 	   	//header: false, 
  	   	selectedText: "# of # selected",
		
	});
	$("#applicable_salaryhead,#offday_payment,#leave_type").multiselectfilter({
	
	});	
 });  
 
function populate_fild(val)
{
	if(val==1)
	{
		$('#txt_assign_day_number').removeAttr('disabled'); 
		$('#cbo_percent_fixed').removeAttr('disabled'); 
		//$('#cbo_jobassignday_salary_head').removeAttr('disabled'); 
	}
	else
	{
		$('#txt_assign_day_number').attr('disabled',true);
		$('#cbo_percent_fixed').attr('disabled',true);
		//$('#cbo_jobassignday_salary_head').attr('disabled',true);
		
		$('#txt_assign_day_number').val('');
		$('#cbo_percent_fixed').val('');
		//$('#cbo_jobassignday_salary_head').val('');
	}	 
}

function populate_fild_salary_head(val)
{
	if(val==1)
	{
		$('#cbo_jobassignday_salary_head').removeAttr('disabled'); 
	}
	else
	{
		$('#cbo_jobassignday_salary_head').attr('disabled',true);
		$('#cbo_jobassignday_salary_head').val('');
	}	 
}

function numbersonly(myfield, e, dec)
		{
			var key;
			var keychar;
			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);
			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			// numbers
			else if ((("0123456789.").indexOf(keychar) > -1))
				return true;
			else
				return false;
		}
		
//---------------------------Variable_Settings_piecerate_Start---------------------------------------------------------------------
function fnc_piecerate_variable_settings(save_perm,edit_perm,delete_perm,approve_perm)
{

	var cbo_company_name				= $('#cbo_company_name').val();
	var cbo_piecerate_wages				= $('#cbo_piecerate_wages').val();
	var applicable_salaryhead			= $('#applicable_salaryhead').val();
	var offday_payment					= $('#offday_payment').val();
	var leave_type						= $('#leave_type').val();
	//alert (leave_type);return;
	var cbo_job_assign					= $('#cbo_job_assign').val();
	var txt_assign_day_number				= $('#txt_assign_day_number').val();
	var cbo_percent_fixed					= $('#cbo_percent_fixed').val();
	var cbo_jobassignday_salary_head		= $('#cbo_jobassignday_salary_head').val();
	var cbo_over_base_salary				= $('#cbo_over_base_salary').val();
	var cbo_under_base_salary				= $('#cbo_under_base_salary').val();
	var save_up_piecerate					= $('#save_up_piecerate').val();
	var cbo_subsidy							= $('#cbo_subsidy').val();
	//alert (cbo_subsidy);return;
				
	$("#messagebox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
	if (save_up_piecerate=='' && save_perm==2)
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have save permission.').fadeIn(1000);
	}
	else if (save_up_piecerate!='' && edit_perm==2) // no edit 
	{
		$("#messagebox").removeClass().addClass('messagebox').text('You do not have update permission.').fadeIn(1000);
	}
	else if($('#cbo_company_name').val()==0){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_company_name').focus();
			$(this).html('Please Select Company Name').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	else if($('#cbo_piecerate_wages').val()==""){						
		$("#messagebox").fadeTo(200,0.1,function(){  //start fading the messagebox
			$('#cbo_piecerate_wages').focus();
			$(this).html('Please Enter piece rate wages field').addClass('messageboxerror').fadeTo(900,1);
		});		
	}
	
	else{					
		nocache = Math.random();
		http.open('get','save_update_common_variable_settings.php?action=piecerate_variable_settings&isupdate='+save_up_piecerate+
					'&cbo_company_name='+cbo_company_name+
					'&cbo_piecerate_wages='+cbo_piecerate_wages+
					'&applicable_salaryhead='+applicable_salaryhead+
					'&offday_payment='+offday_payment+
					'&leave_type='+leave_type+
					'&cbo_job_assign='+cbo_job_assign+
					'&txt_assign_day_number='+txt_assign_day_number+
					'&cbo_percent_fixed='+cbo_percent_fixed+
					'&cbo_jobassignday_salary_head='+cbo_jobassignday_salary_head+
					'&cbo_over_base_salary='+cbo_over_base_salary+
					'&cbo_under_base_salary='+cbo_under_base_salary+
					'&cbo_subsidy='+cbo_subsidy+
					'&nocache ='+nocache);
		http.onreadystatechange = piecerate_variable_settings_reply;
		http.send(null); 
	}
} 

function piecerate_variable_settings_reply() {
	if(http.readyState == 4){
		//alert (http.responseText);
		var response = http.responseText.split('_');
		if (response[0]==3)
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Data Updated Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','variable_setting','variable_setting_hrm','update','../');
				document.getElementById('save_up_piecerate').value=response[1];
			});
		}
		else
		{
			$("#messagebox").fadeTo(200,0.1,function() //start fading the messagebox
			{ 
				$(this).html('Inserted Successfully.').addClass('messagebox_ok').fadeTo(900,1);
				//save_activities_history('','variable_setting','variable_setting_hrm','save','../');
				document.getElementById('save_up_piecerate').value=response[1];
			});
		}
			
	}
} 


function openpage_searchemp(page_link,title)
	{
		if(document.getElementById('cbo_subsidy').value=="")
		{
			var data='';
		}
		else
		{
			var data=document.getElementById('cbo_subsidy').value;
		}	
	
		page_link='search_subsidy_for.php?action=blanding_subsidy_for&data='+data
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=200px,height=200px,center=1,resize=0,scrolling=0',' ')
		
		emailwindow.onclose=function()
		{
			var cutting=this.contentDoc.getElementById("txt_cutting");
			var cutting_rate=this.contentDoc.getElementById("txt_cutting_rate");
			var sewing=this.contentDoc.getElementById("txt_sewing");
			var sewing_rate=this.contentDoc.getElementById("txt_sewing_rate");
			var finishing=this.contentDoc.getElementById("txt_finishing");
			var finishing_rate=this.contentDoc.getElementById("txt_finishing_rate");
			if(cutting_rate.value=='' && sewing_rate.value=='' && finishing_rate.value==''){document.getElementById('cbo_subsidy').value=='';}
			else
			{
			document.getElementById('cbo_subsidy').value = cutting.value+"_"+cutting_rate.value+"**"+sewing.value+"_"+sewing_rate.value+"**"+finishing.value+"_"+finishing_rate.value;
			
			document.getElementById('cbo_subsidy_view').value = "C"+"="+cutting_rate.value+","+"S"+"="+sewing_rate.value+","+"F"+"="+finishing_rate.value;
			}
		}
	}
		


			
    </script>
	<!--<script type="text/javascript" src="ui.tabs.paging.js"></script>-->
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').tabs();
			$('#example').tabs('paging', { cycle: true, follow: true } );
			$('#example').tabs('select',<?php echo $TabIndexNo; ?>);
		});
	</script>
	<style>
	input.file {
	 background-image: url(image.png);
	 width:10px;
	 height:10px;
	}
	</style>
	</head>
<body>
<div align="center" style="width:90%;"> 
    <div align="center">
        <div class="form_caption">
            Variable Settings For Piece Rate
        </div>
        <span id="permission_caption">
        <? echo "Your Permissions--> \n".$insert.$update;
            ?>
       </span>
        <div id="messagebox" style="background-color:#FF9999; color:#000000; width:700px" align="center"></div>
    </div>
    <form name="piecerate_variable_settings" id="piecerate_variable_settings" method="post" action="javascript:fnc_piecerate_variable_settings(save_perm,edit_perm,delete_perm,approve_perm)" autocomplete="off">
    <fieldset style="width:70%;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
             <tr>
                <td>Company Name&nbsp&nbsp&nbsp </td>
                <td>
                    <input type="hidden" id="save_up_piecerate" name="save_up_piecerate" />
                    <select name="cbo_company_name" id="cbo_company_name" onChange="showResult_variable_settings_piecerate(this.value,'getdata_piecerate')" class="combo_boxes"  style="width:300px ">
                        <option value="0">-------------Select Company Name--------------</option>
                            <?
                            $company_sql= mysql_db_query($DB, "select company_name,id from lib_company where is_deleted=0  and status_active=1 order by company_name");
                            while ($r_company=mysql_fetch_array($company_sql))
                            {
                            ?>
                        <option value="<? echo $r_company["id"]; ?>" <? if($r_company1["id"]==$r_company["id"]){?>selected<?php }?>><? echo "$r_company[company_name]" ?> </option>
                        <? } ?>
                    </select>	
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="width:70%; margin-top:10px;">
    <table width="100%" border="0"  cellpadding="2" cellspacing="2">
        <tr>
            <td>Piece Rate Wages:</td>
            <td>
                <select name="cbo_piecerate_wages" id="cbo_piecerate_wages" class="combo_boxes" style="width:150px;">
                    <option value="0">Monthly</option>
                    <option value="1">Weekly</option>
                </select>
            </td>
            <td>Applicable Salary Head:</td>
            <td>
               <select name="applicable_salaryhead"  id="applicable_salaryhead" class="combo_boxes" style="width:130px" multiple="multiple">
                        <?php foreach( $applycable_salary_head AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Off Day Payment:</td>
            <td> 
                <select name="offday_payment" id="offday_payment" class="combo_boxes" style="width:130px;" multiple="multiple">
                        <?php foreach( $piecerate_ofday_payment_arr AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                </select>
            </td>
            <td>Leave Day:</td> 
            <td>
                <select name="leave_type" id="leave_type" class="combo_boxes" style="width:130px;" multiple="multiple">
                        <?php foreach( $piecerate_leave_type AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>No Job Assign Day:</td>
            <td>
                <select name="cbo_job_assign" id="cbo_job_assign" class="combo_boxes" style="width:150px;" onchange="populate_fild(this.value);">
                    <option value="">-- Select --</option>
                        <?php foreach( $common_no_yes AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                </select>
            </td>
            <td>
            <input type="text" name="txt_assign_day_number" id="txt_assign_day_number" value="" class="text_boxes" size="5" onkeypress="return numbersonly(this,event)" disabled="disabled" />
             <select name="cbo_percent_fixed" id="cbo_percent_fixed" class="combo_boxes" style="width:100px;" disabled="disabled" onchange="populate_fild_salary_head(this.value)" >
                     <option value="0">---Select---</option>
                    <option value="1">%</option>
                    <option value="2">Fixed</option>
                </select>               
            </td>
            <td> <!--$jobassignday_salary_head-->
            <select name="cbo_jobassignday_salary_head" id="cbo_jobassignday_salary_head" class="combo_boxes" style="width:150px;" disabled="disabled">
                   <option value="0">-- Select --</option>
                        <?php foreach( $jobassignday_salary_head AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                </select>               
            </td>
        </tr>
        <tr>
            <td>Over Based Salary:</td>
            <td>
            <select name="cbo_over_base_salary" id="cbo_over_base_salary" class="combo_boxes" style="width:150px;">
                    <option value="">-- Select --</option>
                        <?php foreach( $common_no_yes AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                </select>
            </td>
            <td>Under Based Salary:</td>
            <td>
                <select name="cbo_under_base_salary" id="cbo_under_base_salary" class="combo_boxes" style="width:150px;">
                    <option value="">-- Select --</option>
                        <?php foreach( $common_no_yes AS $key=>$value ){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                </select>
            </td>
        </tr>
         <tr>
            <td>Subsidy For</td>
            <td>
            	 <input type="text" name="cbo_subsidy_view" id="cbo_subsidy_view" value="" placeholder="Double Click" readonly="readonly" ondblclick="openpage_searchemp('','Subsidy For')" class="text_boxes" style="width:110px; text-align:right"/>
                <input type="hidden" name="cbo_subsidy" id="cbo_subsidy" value="" />
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>				
        </tr>
        <tr>
            <td colspan="4" align="center" style="padding-top:10px;" class="button_container">
                <input type="submit" value="Save" name="save" style="width:100px" id="save" class="formbutton"/>&nbsp;&nbsp;
                <input type="reset" value="  Refresh  " style="width:100px" name="reset" id="reset" class="formbutton"/>	
            </td>				
        </tr>
    </table>
    </fieldset>
    </form>
</div>
</body>
</html>