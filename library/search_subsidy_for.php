<? 
session_start();
include('../includes/common.php');
include('../includes/array_function.php');

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
 	$search_string=$_GET["m_company"];
	$m_buyer=$_GET["m_buyer"];
	
	extract($_REQUEST);
 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="../resources/jquery-1.6.2.js" type="text/javascript"></script>
<link href="../css/style_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../includes/functions.js"></script>

<link href="../includes/filtergrid.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="../includes/tablefilter.js"></script>

<script>
var data="<? echo $data; ?>";
if(data!=='')
{
data=data.split(",");
}
var selected_id = new Array;

function js_set_value( str,frm ) 
{
	 if(frm==1)
	 {
		 $('#blanding_'+str).attr('checked','true');
	 }
	 
	if( jQuery.inArray( str, selected_id ) == -1 ) 
	{
		selected_id.push( str );
	}
	else 
	{
		for( var i = 0; i < selected_id.length; i++ ) 
		{
			if( selected_id[i] == str ) break;
		}
		selected_id.splice( i, 1 );
	}
	var id = '';
	for( var i = 0; i < selected_id.length; i++ ) 
	{
		id += selected_id[i] + ',';
	}
	id = id.substr( 0, id.length - 1 );
	//alert (id);
	$('#hidden_blanding_option_id').val( id );
}
/*
function js_set_value(id)
	{
		//alert (id);
		var str=id.split("_");
		str=str[0]+'_'+str[1];
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i, 1 );
		}
		var id = ''; var name='';
		for( var i = 0; i < selected_id.length; i++ ) {
			var data_id=selected_id[i].split("_");
			id += data_id[0] + ',';
			name += data_id[1] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		name = name.substr( 0, name.length - 1 );
		//alert (name);
		$('#hidden_blanding_option_id').val( id );
		$('#selected_blanding_option_name').val( name );
	} 
	*/
</script>
</head>
<body>
 
<div align="center">
<form name="search_order_frm"  id="search_order_frm" autocomplete="off">
    <fieldset style="width:155px">
        <legend>Costcenter</legend>
        <input type="hidden" name="hidden_blanding_option_id" id="hidden_blanding_option_id" value="" />
        <input type="hidden" name="selected_blanding_option_name" id="selected_blanding_option_name" />
            <table>
            <?
            //echo $data;
			//$data_rate=array();
			$Data = explode( "**", $data );
			//for($i=0; count($Data)>$i; $i++)
			//{
				$data_rate_Cutting = explode( "_", $Data[0] );
				$data_rate_Sewing = explode( "_", $Data[1] );
				$data_rate_Finishing = explode( "_", $Data[2] );
			//}
			//print_r($data_rate);
			?>
            	
				<!--$sewing_process_rate_arr_subsidy=array(1=>"Cutting",2=>"Sewing",3=>"Finishing");-->
               
					<tr>
                        <td align="left">Cutting
                       <input type="hidden" name="txt_cutting" id="txt_cutting" value="1" />
                        </td>
                        <td><input type="text" name="txt_cutting_rate" id="txt_cutting_rate" value="<? echo $data_rate_Cutting[1];?>"  class="text_boxes_numeric" style="width:40px" onkeypress="return numbersonly(this,event)" />%</td>
					</tr>
                    <tr>
                        <td align="left">Sewing
                       <input type="hidden" name="txt_sewing" id="txt_sewing" value="2" />
                        </td>
                        <td><input type="text" name="txt_sewing_rate" id="txt_sewing_rate"  class="text_boxes_numeric" value="<? echo $data_rate_Sewing[1];?>" style="width:40px" onkeypress="return numbersonly(this,event)" />%</td>
					</tr>
                    <tr>
                        <td align="left">Finishing
                       <input type="hidden" name="txt_finishing" id="txt_finishing" value="3" />
                        </td>
                        <td><input type="text" name="txt_finishing_rate" id="txt_finishing_rate"  class="text_boxes_numeric" value="<? echo $data_rate_Finishing[1];?>" style="width:40px" onkeypress="return numbersonly(this,event)" />%</td>
					</tr>
					
            </table>
        <div style="width:50%;" align="center">
            <input type="button" name="close" onclick="parent.emailwindow.hide();" class="formbutton" value="Close" align="middle" />
        </div>
    </fieldset>
</form>    
</div>
</body>
<script>
for (var k=0; k<data.length; k++)
{
	js_set_value( data[k],1 ) ;
}
</script>
</html>